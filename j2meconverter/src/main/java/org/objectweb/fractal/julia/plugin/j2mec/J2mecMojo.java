package org.objectweb.fractal.julia.plugin.j2mec;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.FileUtils;
import org.codehaus.plexus.util.IOUtil;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.Attribute;
import org.objectweb.asm.ClassAdapter;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodAdapter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 * Goal which transform classes to be j2me-compliant.
 * 
 * @goal j2mec
 * @description Enhances the application data objects.
 * @phase process-classes
 */
public class J2mecMojo extends AbstractMojo {

	/**
	 * The Maven project reference.
	 * 
 	 * @parameter expression="${project}"
 	 * @readonly
	 * @required
	 */
	private MavenProject project;

	/**
	 * @parameter default-value="${project.build.outputDirectory}"
	 */
	private String inputDirectory;
	
	
	/**
	 * @parameter default-value="${project.build.outputDirectory}/j2me"
	 */
	private String outputDirectory;
	
	private Set cldcAPI;

	private int errors;

	private final static String[] METHODS = new String[] {
			"java/util/List,size()I",
			"java/util/List,get(I)Ljava/lang/Object;",
			"java/util/List,contains(Ljava/lang/Object;)Z",
			"java/util/List,set(ILjava/lang/Object;)Ljava/lang/Object;",
			"java/util/List,add(Ljava/lang/Object;)Z",
			"java/util/List,add(ILjava/lang/Object;)V",
			"java/util/List,remove(Ljava/lang/Object;)Z",
			"java/util/List,remove(I)Ljava/lang/Object;",
			"java/util/List,toArray()[Ljava/lang/Object;",
			"java/util/List,toArray([Ljava/lang/Object;)[Ljava/lang/Object;",
			"java/util/Set,size()I",
			"java/util/Set,contains(Ljava/lang/Object;)Z",
			"java/util/Set,add(Ljava/lang/Object;)Z",
			"java/util/Set,toArray()[Ljava/lang/Object;",
			"java/util/Set,toArray([Ljava/lang/Object;)[Ljava/lang/Object;",
			"java/util/Map,size()I",
			"java/util/Map,get(Ljava/lang/Object;)Ljava/lang/Object;",
			"java/util/Map,put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
			"java/util/Map,remove(Ljava/lang/Object;)Ljava/lang/Object;",
			"java/util/Map,keySet()Ljava/util/Set;",
			"java/util/ArrayList,<init>()V", "java/util/HashSet,<init>()V",
			"java/util/HashMap,<init>()V", };

	private final static Map INDEXES = new HashMap();

	static {
		for (int i = 0; i < METHODS.length; ++i) {
			INDEXES.put(METHODS[i], new Integer(i));
		}
	}


	/**
	 * Executes this mojo.
	 */
	public void execute() throws MojoExecutionException {
		
		
		try {
			
				cldcAPI = new HashSet(); //populated within internal Visitors
				
				//XXX @TODO should be able to use a user-provided cldc jar file, if the default one is not good
				InputStream cldcJar = this.getClass().getResourceAsStream("/cldc.jar");
				File cldcTempFile = File.createTempFile("cldcJar", ".jar");
				IOUtil.copy(cldcJar, new FileWriter(cldcTempFile));
				
				cldcTempFile.deleteOnExit();
				
				ZipFile zf = new ZipFile(cldcTempFile);
		        Enumeration e = zf.entries();
		        while (e.hasMoreElements()) {
		          ZipEntry ze = (ZipEntry)e.nextElement();
		          if (ze.getName().endsWith(".class")) {
		        	  getLog().debug("Now visiting: "+ze.getName());
		            new ClassReader(zf.getInputStream(ze)).accept(new ClassAnalyzer(), ClassReader.SKIP_DEBUG);
		          }
		        }
			
				
		} catch (Exception e) {
			getLog().info(e.getMessage());
		}

		int total = 0;
		
		if (inputDirectory==null){
			inputDirectory = project.getBuild().getOutputDirectory();
		}
		
		getLog().info("Input directory: "+inputDirectory);
		
		if (outputDirectory == null){
			outputDirectory = project.getBuild().getOutputDirectory()+File.pathSeparator+"j2me";
		}
		 
		File compiledClassesDirectory = new File(inputDirectory);
		
		try {
			List compiledClasses = FileUtils.getFiles(compiledClassesDirectory, "**/.class", "" );
			getLog().info("Total files:"+compiledClasses.size());
			
			for (int i=0; i <compiledClasses.size() ; i++) {
				File src = (File) compiledClasses.get(i);
				getLog().debug("Source file: "+src.getAbsolutePath());
				//remove the path up to the classes/ directory
				//XXX bugged: the output path 
				String dstPath = src.getAbsolutePath().substring(src.getAbsolutePath().indexOf("/classes"));
				File dst = new File(outputDirectory,dstPath);
				getLog().debug("Dest file: "+dst.getAbsolutePath());
				
				
				ClassReader cr;
				try {
					InputStream is = new FileInputStream(src);
					cr = new ClassReader(is);
				} catch (Exception e) {
					e.printStackTrace();
					getLog().info("Exception occurred while instantiating a ClassReader for inputStream "+e.getMessage());
					continue;
				}
	
				ClassWriter cw = new ClassWriter(0);
				ClassConverter converter = new ClassConverter(cw);
				cr.accept(converter, ClassReader.SKIP_DEBUG);
	
				try {
					dst.getParentFile().mkdirs();
					OutputStream os = new FileOutputStream(dst);
					os.write(cw.toByteArray());
					os.close();
				} catch (Exception e) {
					e.printStackTrace();
					getLog().info("Exception occurred while writing transformed class to byteArray" +e.getMessage() );
					continue;
				}
				++total;
			}
	
			if (errors > 0) {
				throw new MojoExecutionException("");
			} else if (total > 0) {
				if (total == 1) {
					getLog().info("1 class transformed into " + project.getBuild().getOutputDirectory());
				} else {
					getLog().info(total + " classes transformed into " + project.getBuild().getOutputDirectory());
				}
			}
		} catch(Exception e){
			throw new MojoExecutionException("Some error occurred while executing the j2meconverter "+e.getMessage());
		}
				
	

	}

	static String convertDescriptor(final String desc) {
		if (desc.equals("java/util/ArrayList")) {
			return "java/util/Vector";
		} else if (desc.equals("java/util/HashSet")) {
			return "java/util/Hashtable";
		} else if (desc.equals("java/util/HashMap")) {
			return "java/util/Hashtable";
		}
		boolean done;
		String newDesc = desc;
		do {
			done = true;
			int i = newDesc.indexOf("java/util/List");
			if (i != -1) {
				newDesc = newDesc.substring(0, i) + "java/util/Vector"
						+ newDesc.substring(i + 14);
				done = false;
			}
			i = newDesc.indexOf("java/util/Set");
			if (i != -1) {
				newDesc = newDesc.substring(0, i) + "java/util/Hashtable"
						+ newDesc.substring(i + 13);
				done = false;
			}
			i = newDesc.indexOf("java/util/Map");
			if (i != -1) {
				newDesc = newDesc.substring(0, i) + "java/util/Hashtable"
						+ newDesc.substring(i + 13);
				done = false;
			}
		} while (!done);
		return newDesc;
	}

	class ClassAnalyzer implements ClassVisitor, Opcodes {

		private String name;

		public void visit(final int version, final int access,
				final String name, final String signature,
				final String superName, final String[] interfaces) {
			this.name = name;
		}

		public FieldVisitor visitField(int access, String name, String desc,
				String signature, Object value) {
			FieldVisitor fv = null;
			if ((access & (ACC_PUBLIC | ACC_PROTECTED)) != 0) {
				cldcAPI.add(this.name + "," + name);
			}
			return fv;
		}

		public MethodVisitor visitMethod(final int access, final String name,
				final String desc, final String signature,
				final String[] exceptions) {
			if ((access & (ACC_PUBLIC | ACC_PROTECTED)) != 0) {
				cldcAPI.add(this.name + "," + name + desc);
			}
			MethodVisitor mv = null;
			return mv;
		}

		public void visitInnerClass(final String name, final String outerName,
				final String innerName, final int access) {
		}

		public void visitEnd() {
		}

		public void visitSource(String source, String debug) {
		}

		public void visitOuterClass(String owner, String name, String desc) {
		}

		public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
			return null;
		}

		public void visitAttribute(Attribute attr) {
		}
	}

	class ClassConverter extends ClassAdapter implements Opcodes {

		private String name;

		private boolean hasVectorToArray;

		private boolean hasHashtableToArray;

		public ClassConverter(ClassVisitor cv) {
			super(cv);
		}

		public void visit(final int version, final int access,
				final String name, final String signature,
				final String superName, final String[] interfaces) {
			this.name = name;
			List itfList = new ArrayList(Arrays.asList(interfaces));
			itfList.remove("java/io/Serializable");
			String[] itfs = (String[]) itfList.toArray(new String[itfList
					.size()]);
			cv.visit(version, access, name, signature, superName, itfs);
		}

		public FieldVisitor visitField(int access, String name, String desc,
				String signature, Object value) {
			return cv.visitField(access, name, convertDescriptor(desc),
					signature, value);
		}

		public MethodVisitor visitMethod(final int access, final String name,
				final String desc, final String signature,
				final String[] exceptions) {
			if (name.equals("writeObject")
					&& desc.equals("(Ljava/io/ObjectOutputStream;)V")) {
				return null;
			}
			if (name.equals("readObject")
					&& desc.equals("(Ljava/io/ObjectInputStream;)V")) {
				return null;
			}
			if (name.equals("printStackTrace") && !desc.equals("()V")) {
				return null;
			}
			if (name.equals("getInputStream")) {
				MethodVisitor c = cv.visitMethod(access, name, desc, signature,
						exceptions);
				c.visitCode();
				c.visitTypeInsn(NEW, "java/io/IOException");
				c.visitInsn(DUP);
				c.visitMethodInsn(INVOKESPECIAL, "java/io/IOException",
						"<init>", "()V");
				c.visitInsn(ATHROW);
				c.visitMaxs(2, 2);
				c.visitEnd();
				return null;
			}
			if (name.equals("_forName")) {
				MethodVisitor c = cv.visitMethod(access, name, desc, signature,
						exceptions);
				c.visitCode();
				c.visitVarInsn(ALOAD, 1);
				c.visitMethodInsn(INVOKESTATIC, "java/lang/Class", "forName",
						desc);
				c.visitInsn(ARETURN);
				c.visitMaxs(2, 2);
				c.visitEnd();
				return null;
			}
			String newDesc = convertDescriptor(desc);
			return new CodeConverter(this, cv.visitMethod(access, name,
					newDesc, signature, exceptions));
		}

		public void generateVectorToArrayMethod() {
			if (hasVectorToArray) {
				return;
			} else {
				hasVectorToArray = true;
			}
			MethodVisitor c = cv
					.visitMethod(
							ACC_STATIC,
							"toArray",
							"(Ljava/util/Vector;[Ljava/lang/Object;)[Ljava/lang/Object;",
							null, null);
			c.visitCode();
			c.visitInsn(ICONST_0);
			c.visitVarInsn(ISTORE, 2);
			Label l0 = new Label();
			c.visitJumpInsn(GOTO, l0);
			Label l1 = new Label();
			c.visitLabel(l1);
			c.visitVarInsn(ALOAD, 1);
			c.visitVarInsn(ILOAD, 2);
			c.visitVarInsn(ALOAD, 0);
			c.visitVarInsn(ILOAD, 2);
			c.visitMethodInsn(INVOKEVIRTUAL, "java/util/Vector", "elementAt",
					"(I)Ljava/lang/Object;");
			c.visitInsn(AASTORE);
			c.visitIincInsn(2, 1);
			c.visitLabel(l0);
			c.visitVarInsn(ILOAD, 2);
			c.visitVarInsn(ALOAD, 1);
			c.visitInsn(ARRAYLENGTH);
			c.visitJumpInsn(IF_ICMPLT, l1);
			c.visitVarInsn(ALOAD, 1);
			c.visitInsn(ARETURN);
			c.visitMaxs(4, 3);
			c.visitEnd();
		}

		public void generateHashtableToArrayMethod() {
			if (hasHashtableToArray) {
				return;
			} else {
				hasHashtableToArray = true;
			}
			MethodVisitor c = cv
					.visitMethod(
							ACC_STATIC,
							"toArray",
							"(Ljava/util/Hashtable;[Ljava/lang/Object;)[Ljava/lang/Object;",
							null, null);
			c.visitCode();
			c.visitInsn(ICONST_0);
			c.visitVarInsn(ISTORE, 2);
			c.visitVarInsn(ALOAD, 0);
			c.visitMethodInsn(INVOKEVIRTUAL, "java/util/Hashtable", "keys",
					"()Ljava/util/Enumeration;");
			c.visitVarInsn(ASTORE, 3);
			Label l0 = new Label();
			c.visitJumpInsn(GOTO, l0);
			Label l1 = new Label();
			c.visitLabel(l1);
			c.visitVarInsn(ALOAD, 1);
			c.visitVarInsn(ILOAD, 2);
			c.visitIincInsn(2, 1);
			c.visitVarInsn(ALOAD, 3);
			c.visitMethodInsn(INVOKEINTERFACE, "java/util/Enumeration",
					"nextElement", "()Ljava/lang/Object;");
			c.visitInsn(AASTORE);
			c.visitLabel(l0);
			c.visitVarInsn(ALOAD, 3);
			c.visitMethodInsn(INVOKEINTERFACE, "java/util/Enumeration",
					"hasMoreElements", "()Z");
			c.visitJumpInsn(IFNE, l1);
			c.visitVarInsn(ALOAD, 1);
			c.visitInsn(ARETURN);
			c.visitMaxs(3, 4);
			c.visitEnd();
		}
	}

	class CodeConverter extends MethodAdapter implements Opcodes {

		ClassConverter c;

		String lastMethodCallInsn;

		boolean mustNotUseResult;

		public CodeConverter(ClassConverter c, MethodVisitor mv) {
			super(mv);
			this.c = c;
		}

		private void check(int opcode) {
			if (mustNotUseResult && opcode != POP) {
				getLog().info(
						c.name + ": must not use result of "
								+ lastMethodCallInsn);
				++errors;
			}
			mustNotUseResult = false;
		}

		public void visitInsn(final int opcode) {
			check(opcode);
			mv.visitInsn(opcode);
		}

		public void visitIntInsn(final int opcode, final int operand) {
			check(opcode);
			mv.visitIntInsn(opcode, operand);
		}

		public void visitVarInsn(final int opcode, final int var) {
			check(opcode);
			mv.visitVarInsn(opcode, var);
		}

		public void visitTypeInsn(final int opcode, final String desc) {
			check(opcode);
			mv.visitTypeInsn(opcode, convertDescriptor(desc));
		}

		public void visitFieldInsn(final int opcode, final String owner,
				final String name, final String desc) {
			check(opcode);
			if (cldcAPI != null && owner.startsWith("java")) {
				String key = owner + "," + name;
				if (!cldcAPI.contains(key)) {
					getLog().info(
							c.name + ": must not use " + key
									+ "(not in CLDC API)");
					++errors;
				}
			}
			mv.visitFieldInsn(opcode, owner, name, convertDescriptor(desc));
		}

		public void visitMethodInsn(final int opcode, final String owner,
				final String name, final String desc) {
			check(opcode);
			String key = owner + "," + name + desc;
			if (cldcAPI != null && owner.startsWith("java")) {
				if (!cldcAPI.contains(key) && INDEXES.get(key) == null) {
					getLog().info(
							c.name + ": must not use " + key
									+ " (not in CLDC API)");
					++errors;
				}
			}
			String newDesc = convertDescriptor(desc);
			if (owner.startsWith("java/util")
					&& !owner.equals("java/util/Vector")
					&& !owner.equals("java/util/Hashtable")
					&& !owner.equals("java/util/Enumeration")) {
				Integer id = (Integer) INDEXES.get(key);
				if (id == null) {
					getLog().info(c.name + ": unauthorized method " + key);
					++errors;
				}
				lastMethodCallInsn = key;
				switch (id.intValue()) {
				case 0: // "java/util/List,size,()I"
					mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Vector", name,
							desc);
					break;
				case 1: // "java/util/List,get,(I)Ljava/lang/Object;"
					mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Vector", name,
							desc);
					break;
				case 2: // "java/util/List,contains,(Ljava/lang/Object;)Z"
					mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Vector", name,
							desc);
					break;
				case 3: // "java/util/List,set,(ILjava/lang/Object;)Ljava/lang/Object;"
					mustNotUseResult = true;
					mv.visitInsn(SWAP);
					mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Vector",
							"setElementAt", "(Ljava/lang/Object;I)V");
					mv.visitInsn(ACONST_NULL);
					break;
				case 4: // "java/util/List,add,(Ljava/lang/Object;)Z"
					mustNotUseResult = true;
					mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Vector",
							"addElement", "(Ljava/lang/Object;)V");
					mv.visitInsn(ICONST_0);
					break;
				case 5: // "java/util/List,add,(ILjava/lang/Object;)V"
					mv.visitInsn(SWAP);
					mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Vector",
							"insertElementAt", "(Ljava/lang/Object;I)V");
					break;
				case 6: // "java/util/List,remove,(Ljava/lang/Object;)Z"
					mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Vector",
							"removeElement", desc);
					break;
				case 7: // "java/util/List,remove,(I)Ljava/lang/Object;"
					mustNotUseResult = true;
					mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Vector",
							"removeElementAt", "(I)V");
					mv.visitInsn(ACONST_NULL);
					break;
				case 8: // "java/util/List,toArray,()[Ljava/lang/Object;"
					mv.visitInsn(DUP);
					mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Vector",
							"size", "()I");
					mv.visitTypeInsn(ANEWARRAY, "java/lang/Object");
					c.generateVectorToArrayMethod();
					mv
							.visitMethodInsn(INVOKESTATIC, c.name, name,
									"(Ljava/util/Vector;[Ljava/lang/Object;)[Ljava/lang/Object;");
					break;
				case 9: // "java/util/List,toArray,([Ljava/lang/Object;)[Ljava/lang/Object;"
					c.generateVectorToArrayMethod();
					mv
							.visitMethodInsn(INVOKESTATIC, c.name, name,
									"(Ljava/util/Vector;[Ljava/lang/Object;)[Ljava/lang/Object;");
					break;
				case 10: // "java/util/Set,size,()I"
					mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Hashtable",
							name, desc);
					break;
				case 11: // "java/util/Set,contains,(Ljava/lang/Object;)Z"
					mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Hashtable",
							"containsKey", desc);
					break;
				case 12: // "java/util/Set,add,(Ljava/lang/Object;)Z"
					mustNotUseResult = true;
					mv.visitInsn(DUP);
					mv
							.visitMethodInsn(INVOKEVIRTUAL,
									"java/util/Hashtable", "put",
									"(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");
					mv.visitInsn(POP);
					mv.visitInsn(ICONST_0);
					break;
				case 13: // "java/util/Set,toArray,()[Ljava/lang/Object;"
					mv.visitInsn(DUP);
					mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Hashtable",
							"size", "()I");
					mv.visitTypeInsn(ANEWARRAY, "java/lang/Object");
					c.generateHashtableToArrayMethod();
					mv
							.visitMethodInsn(INVOKESTATIC, c.name, name,
									"(Ljava/util/Hashtable;[Ljava/lang/Object;)[Ljava/lang/Object;");
					break;
				case 14: // "java/util/Set,toArray,([Ljava/lang/Object;)[Ljava/lang/Object;"
					c.generateHashtableToArrayMethod();
					mv
							.visitMethodInsn(INVOKESTATIC, c.name, name,
									"(Ljava/util/Hashtable;[Ljava/lang/Object;)[Ljava/lang/Object;");
					break;
				case 15: // "java/util/Map,size,()I"
					mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Hashtable",
							name, desc);
					break;
				case 16: // "java/util/Map,get,(Ljava/lang/Object;)Ljava/lang/Object;"
					mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Hashtable",
							name, desc);
					break;
				case 17: // "java/util/Map,put,(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
					mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Hashtable",
							name, desc);
					break;
				case 18: // "java/util/Map,remove,(Ljava/lang/Object;)Ljava/lang/Object;"
					mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Hashtable",
							name, desc);
					break;
				case 19: // "java/util/Map,keySet,()Ljava/util/Set;"
					// nothing to do: the key set is the map itself (clone
					// needed?)
					break;
				case 20: // "java/util/ArrayList,<init>()V"
					mv.visitMethodInsn(opcode, "java/util/Vector", name, desc);
					break;
				case 21: // "java/util/HashSet,<init>()V"
					mv.visitMethodInsn(opcode, "java/util/Hashtable", name,
							desc);
					break;
				case 22: // "java/util/HashMap,<init>()V"
					mv.visitMethodInsn(opcode, "java/util/Hashtable", name,
							desc);
					break;
				default:
					throw new RuntimeException("Internal error"); // should
																	// not
																	// happen
				}
			} else {
				mv.visitMethodInsn(opcode, owner, name, newDesc);
			}
		}

		public void visitJumpInsn(final int opcode, final Label label) {
			check(opcode);
			mv.visitJumpInsn(opcode, label);
		}

		public void visitLdcInsn(final Object cst) {
			check(LDC);
			mv.visitLdcInsn(cst);
		}

		public void visitIincInsn(final int var, final int increment) {
			check(IINC);
			mv.visitIincInsn(var, increment);
		}

		public void visitTableSwitchInsn(final int min, final int max,
				final Label dflt, final Label labels[]) {
			check(TABLESWITCH);
			mv.visitTableSwitchInsn(min, max, dflt, labels);
		}

		public void visitLookupSwitchInsn(final Label dflt, final int keys[],
				final Label labels[]) {
			check(LOOKUPSWITCH);
			mv.visitLookupSwitchInsn(dflt, keys, labels);
		}

		public void visitMultiANewArrayInsn(final String desc, final int dims) {
			check(MULTIANEWARRAY);
			mv.visitMultiANewArrayInsn(desc, dims);
		}
	}

	
	public MavenProject getProject() 
	{
	         return project;
	     }
	 
	     public void setProject(MavenProject project) 
	     {
	         this.project = project;
	     }
	 
}
