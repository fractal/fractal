/***
 * Fractal API
 * Copyright (C) 2001-2002 France Telecom, INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Thierry Coupaye, Pascal Dechamboux, Romain Lenglet,
 *          Philippe Merle, Jean-Bernard Stefani.
 */

package org.objectweb.fractal.api.control;

import org.objectweb.fractal.api.NoSuchInterfaceException;

/**
 * A component interface to control the bindings of the component to which it
 * belongs. It is implicitely assumed here that the component's type system
 * makes a distinction between "client" and "server" interfaces.
 */

public interface BindingController {

  /**
   * Returns the names of the client interfaces of the component to which this
   * interface belongs.
   *
   * @return the names of the client interfaces of the component to which this
   *      interface belongs.
   */

  String[] listFc ();

  /**
   * Returns the interface to which the given client interface is bound. More
   * precisely, returns the server interface to which the client interface whose
   * name is given is bound. This server interface is necessarily in the same
   * address space as the client interface (see {@link #bindFc bindFc}).
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @return the server interface to which the given interface is bound, or <tt>
   *      null</tt> if it is not bound.
   * @throws NoSuchInterfaceException if the component to which this interface
   *      belongs does not have a client interface whose name is equal to the
   *      given name.
   */

  Object lookupFc (String clientItfName) throws NoSuchInterfaceException;

  /**
   * Binds the client interface whose name is given to a server interface. More
   * precisely, binds the client interface of the component to which this
   * interface belongs, and whose name is equal to the given name, to the given
   * server interface. The given server interface must be in the same address
   * space as the client interface.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @param serverItf a server interface.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be created.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  void bindFc (String clientItfName, Object serverItf) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException;

  /**
   * Unbinds the given client interface. More precisely, unbinds the client
   * interface of the component to which this interface belongs, and whose name
   * is equal to the given name.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be removed.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  void unbindFc (String clientItfName) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException;
}
