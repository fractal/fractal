/***
 * Fractal API
 * Copyright (C) 2001-2002 France Telecom, INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Thierry Coupaye, Pascal Dechamboux, Romain Lenglet,
 *          Philippe Merle, Jean-Bernard Stefani.
 */

package org.objectweb.fractal.api.type;

import org.objectweb.fractal.api.Type;

/**
 * A component interface type. Such a type is made of a name, which is the name
 * of the interface described by this type inside its component (see {@link
 * org.objectweb.fractal.api.Interface#getFcItfName getFcItfName}), a list of
 * method signatures, which describes the methods provided or required by this
 * interface, and various flags that indicates if this interface is provided or
 * required, mandatory or not...
 */

public interface InterfaceType extends Type {

  /**
   * Returns the name of component interfaces of this type. More precisely,
   * because some interfaces can be created dynamically (see {@link
   * #isFcCollectionItf isFcCollectionItf}), this name is in fact a
   * <i>prefix</i> of the real names of interfaces of this type.
   *
   * @return the name of component interfaces of this type inside their
   *      components (see {@link
   *      org.objectweb.fractal.api.Interface#getFcItfName getFcItfName}).
   */

  String getFcItfName ();

  /**
   * Returns the signatures of the methods of interfaces of this type. In Java
   * this method returns the fully qualified name of a Java interface
   * corresponding to these method signatures.
   *
   * @return the signatures of the methods of interfaces of this type.
   */

  String getFcItfSignature ();

  /**
   * Returns <tt>true</tt> if component interfaces of this type are client
   * interfaces.
   *
   * @return <tt>true</tt> if component interfaces of this type are client
   *      interfaces, i.e., interfaces that are required, or <tt>false</tt> if
   *      they are server interfaces, i.e., interfaces that are provided.
   */

  boolean isFcClientItf ();

  /**
   * Returns <tt>true</tt> if component interfaces of this type are optional. A
   * mandatory interface is guaranteed to be "available" when the component
   * is started. For a client interface, this means that the interface is bound,
   * and that it is bound to a mandatory interface. An optional interface may
   * not be available when the component is started (for a client interface, it
   * means that the interface may not be bound).
   *
   * @return <tt>true</tt> if component interfaces of this type are optional.
   */

  boolean isFcOptionalItf ();

  /**
   * Indicates how many interfaces of this type a component may have. A
   * singleton interface type means that a component must have exactly one
   * interface of this type. A collection interface type means that a component
   * may have an arbitrary number of interfaces of this type (whose names must
   * all begin with the name specified in this interface type).
   *
   * @return <tt>false</tt> if this type is a singleton interface type.
   */

  boolean isFcCollectionItf ();
}
