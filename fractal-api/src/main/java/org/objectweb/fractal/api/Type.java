/***
 * Fractal API
 * Copyright (C) 2001-2002 France Telecom, INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Thierry Coupaye, Pascal Dechamboux, Romain Lenglet,
 *          Philippe Merle, Jean-Bernard Stefani.
 */

package org.objectweb.fractal.api;

/**
 * Specifies the minimal interface that all type systems must implement. This
 * interface defines only one method to test if a type is a sub-type of another
 * one.
 */

public interface Type {

  /**
   * Returns <tt>true</tt> if the given type is a sub-type of this type. The
   * relation defined by this method should be:
   * <ul>
   *   <li> reflexive: if <tt>t</tt> is {@link Object#equals equals} to <tt>u
   *   </tt> then <tt>t.isFcSubTypeOf(u)</tt> should return <tt>true</tt> </li>
   *   <!--
   *   <li> anti-symetric: if <tt>t.isFcSubTypeOf(u)</tt> and
   *   <tt>u.isFcSubTypeOf(t)</tt> are <tt>true</tt> then <tt>t</tt> must be
   *   {@link Object#equals equals} to <tt>u</tt> </li>
   *   -->
   *   <li> transitive: if <tt>t.isFcSubTypeOf(u)</tt> and
   *   <tt>u.isFcSubTypeOf(v)</tt> are <tt>true</tt> then
   *   <tt>t.isFcSubTypeOf(v)</tt> must be <tt>true</tt> </li>
   * </ul>
   *
   *
   * @param type the type to be compared to this type.
   * @return <tt>true</tt> if the given type is a sub-type of this type.
   */

  boolean isFcSubTypeOf (Type type);
}
