/***
 * Fractal API
 * Copyright (C) 2001-2002 France Telecom, INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Thierry Coupaye, Pascal Dechamboux, Romain Lenglet,
 *          Philippe Merle, Jean-Bernard Stefani.
 */

package org.objectweb.fractal.api;

import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.factory.Factory;

/**
 * Provides a static method to get a bootstrap component. <i>This class is only
 * required for Java implementations of the Fractal model (level 3.2 at least).
 * In other languages the "well known name" used to get the bootstrap component
 * may take a different form than a static method.</i>
 */

public class Fractal {

  /**
   * Private constructor (uninstantiable class).
   */

  private Fractal () {
  }

  /**
   * Returns a bootstrap component to create other components. This method
   * creates an instance of the class whose name is specified in the
   * <tt>fractal.provider</tt> system property, which much implement the {@link
   * Factory} interface, and returns the component instantiated by this factory.
   *
   * @return a bootstrap component to create other components. This component
   *      provides at least two interfaces named <tt>type-factory</tt> and
   *      <tt>generic-factory</tt> to create component types and components.
   *      The type of the <tt>generic-factory</tt> interface is {@link
   *      org.objectweb.fractal.api.factory.GenericFactory}, or a
   *      sub type of this type, but the type of the <tt>type-factory</tt>
   *      interface is <i>not</i> necessarily {@link
   *      org.objectweb.fractal.api.type.TypeFactory} (or a sub type of this
   *      type).
   * @throws InstantiationException if the bootstrap component cannot be
   *      created.
   */

  public static Component getBootstrapComponent ()
    throws InstantiationException
  {
    String bootTmplClassName = System.getProperty("fractal.provider");
    if (bootTmplClassName == null) {
      throw new InstantiationException(
        "The fractal.provider system property is not defined");
    }
    Factory bootTmpl;
    try {
      Class bootTmplClass = Class.forName(bootTmplClassName);
      bootTmpl = (Factory)bootTmplClass.newInstance();
    } catch (Exception e) {
      throw new InstantiationException(
        "Cannot find or instantiate the '" + bootTmplClassName +
        "' class specified in the fractal.provider system property");
    }
    return bootTmpl.newFcInstance();
  }
}
