/***
 * Fractal API
 * Copyright (C) 2001-2002 France Telecom, INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Thierry Coupaye, Pascal Dechamboux, Romain Lenglet,
 *          Philippe Merle, Jean-Bernard Stefani.
 */

package org.objectweb.fractal.api.control;

/**
 * A component interface to control the lifecycle of the component to which it
 * belongs. The lifecycle of a component is supposed to be an automaton, whose
 * states represent execution states of the component. This interface
 * corresponds to an automaton with two states called {@link #STARTED
 * STARTED} and {@link #STOPPED STOPPED}, where all the 4 four
 * possible transitions are allowed. It is however possible to define completely
 * different lifecycle controller Java interfaces to use completely different
 * automatons, or to define sub interfaces of this interface to define
 * automatons based on this one, but with more states and more transitions. <p>
 *
 * <b>Note:</b> the sub-interfaces of this interface should use the conventions
 * used in this interface, which are the following. The interface contains one
 * method per state in the lifecycle automaton. Each of these methods changes
 * the current state to the state corresponding to its name, if there is a
 * transition from the current state to this state. The interface also contains
 * one field per state. The names and values of these fields correspond to the
 * names of the methods.
 */

public interface LifeCycleController {

  /**
   * The state of a component just after {@link #startFc startFc} has been
   * executed.
   */

  String STARTED = "STARTED";

  /**
   * The state of a component just after {@link #stopFc stopFc} has been
   * executed. This state is also the initial state of a component, i.e., the
   * state of a component just after it has been created.
   */

  String STOPPED = "STOPPED";

  /**
   * Returns the execution state of the component to which this interface
   * belongs.
   *
   * @return the execution state of the component to which this interface
   *      belongs.
   */

  String getFcState ();

  /**
   * Starts the component to which this interface belongs.
   *
   * @throws IllegalLifeCycleException if the transition from the current state
   *      to the {@link #STARTED STARTED} state is not a valid transition of the
   *      life cycle automaton.
   */

  void startFc () throws IllegalLifeCycleException;

  /**
   * Stops the component to which this interface belongs. The result of a method
   * call on a stopped component is undefined, except on its control interfaces
   * (these calls are executed normally).
   *
   * @throws IllegalLifeCycleException if the transition from the current state
   *      to the {@link #STOPPED STOPPED} state is not a valid transition of the
   *      life cycle automaton.
   */

  void stopFc () throws IllegalLifeCycleException;
}
