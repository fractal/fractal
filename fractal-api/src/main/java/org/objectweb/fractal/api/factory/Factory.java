/***
 * Fractal API
 * Copyright (C) 2001-2002 France Telecom, INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Thierry Coupaye, Pascal Dechamboux, Romain Lenglet,
 *          Philippe Merle, Jean-Bernard Stefani.
 */

package org.objectweb.fractal.api.factory;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;

/**
 * A component interface to create components of the same type.
 */

public interface Factory {

  /**
   * Returns the functional type of the components instantiated by this factory.
   *
   * @return the functional type of the components instantiated by this factory.
   */

  Type getFcInstanceType ();

  /**
   * Returns a description of the controller part of the components instantiated
   * by this factory.
   *
   * @return a description of the controller part of the components instantiated
   *      by this factory. This description is implementation specific. If it
   *      is <tt>null</tt> then a "default" controller part will be used.
   */

  Object getFcControllerDesc ();

  /**
   * Returns a description of the content part of the components instantiated
   * by this factory.
   *
   * @return a description of the content part of the components instantiated by
   *      this factory. This description is implementation specific. It may be
   *      <tt>null</tt> to create component with an empty initial content. It
   *      may also be, in Java, the fully qualified name of a Java class, to
   *      creat primitive components.
   */

  Object getFcContentDesc ();

  /**
   * Instantiates a component from this factory.
   *
   * @return the {@link Component} interface of the component instantiated from
   *      this factory.
   * @throws InstantiationException if the component cannot be created.
   */

  Component newFcInstance () throws InstantiationException;
}
