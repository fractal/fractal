/***
 * Fractal API
 * Copyright (C) 2001-2002 France Telecom, INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Thierry Coupaye, Pascal Dechamboux, Romain Lenglet,
 *          Philippe Merle, Jean-Bernard Stefani.
 */

package org.objectweb.fractal.api.type;

import org.objectweb.fractal.api.factory.InstantiationException;

/**
 * A component interface to create component and interface type objects.
 */

public interface TypeFactory {

  /**
   * The <tt>isClient</tt> value to be used in {@link
   * #createFcItfType createFcItfType} to create a server interface type.
   */

  boolean SERVER = false;

  /**
   * The <tt>isClient</tt> value to be used in {@link
   * #createFcItfType createFcItfType} to create a client interface type.
   */

  boolean CLIENT = true;

  /**
   * The <tt>isOptional</tt> value to be used in {@link
   * #createFcItfType createFcItfType} to create a mandatory interface type.
   */

  boolean MANDATORY = false;

  /**
   * The <tt>isOptional</tt> value to be used in {@link
   * #createFcItfType createFcItfType} to create an optional interface type.
   */

  boolean OPTIONAL = true;

  /**
   * The <tt>isCollection</tt> value to be used in {@link
   * #createFcItfType createFcItfType} to create a singleton interface type.
   */

  boolean SINGLE = false;

  /**
   * The <tt>isCollection</tt> value to be used in {@link
   * #createFcItfType createFcItfType} to create a collection interface type.
   */

  boolean COLLECTION = true;

  /**
   * Creates an interface type.
   *
   * @param name the name of interfaces of this type (see {@link
   *       InterfaceType#getFcItfName getFcItfName}).
   * @param signature signatures of the methods of interfaces of this type. In
   *       Java this "signature" is the fully qualified name of a Java interface
   *       corresponding to these method signatures.
   * @param isClient <tt>true</tt> if component interfaces of this type are
   *      client interfaces.
   * @param isOptional <tt>true</tt> if component interfaces of this type are
   *      optional interfaces.
   * @param isCollection <tt>true</tt> if a component may have several
   *      interfaces of this type.
   * @return an interface type initialized with the given values.
   * @throws InstantiationException if the interface type cannot be created.
   */

  InterfaceType createFcItfType (
    String name,
    String signature,
    boolean isClient,
    boolean isOptional,
    boolean isCollection) throws InstantiationException;

  /**
   * Creates a component type.
   *
   * @param interfaceTypes the interface types of the component type to be
   *      created.
   * @return a component type whose {@link ComponentType#getFcInterfaceTypes
   *      getFcInterfaceTypes} method returns an array equal to
   *      <tt>interfaceTypes</tt>.
   * @throws InstantiationException if the component type cannot be created.
   */

  ComponentType createFcType (InterfaceType[] interfaceTypes)
    throws InstantiationException;
}
