/***
 * Fractal API
 * Copyright (C) 2001-2002 France Telecom, INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Thierry Coupaye, Pascal Dechamboux, Romain Lenglet,
 *          Philippe Merle, Jean-Bernard Stefani.
 */

package org.objectweb.fractal.api.control;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;

/**
 * A component interface to control the content of the component to which it
 * belongs. This content is supposed to be made of an unordered, unstructured
 * set of components.
 */

public interface ContentController {

  /**
   * Returns the internal interfaces of the component to which this interface
   * belongs.
   *
   * @return the internal interfaces of the component to which this interface
   *      belongs.
   */

  Object[] getFcInternalInterfaces ();

  /**
   * Returns an internal interface of the component to which this interface
   * belongs.
   *
   * @param interfaceName the name of the internal interface that must be
   *      returned.
   * @return the internal interface of the component to which this interface
   *      belongs, whose name is equal to the given name.
   * @throws NoSuchInterfaceException if there is no such interface.
   */

  Object getFcInternalInterface (String interfaceName)
    throws NoSuchInterfaceException;

  /**
   * Returns the sub-components of this component.
   *
   * @return the {@link Component} interfaces of the sub-components of the
   *      component to which this interface belongs.
   */

  Component[] getFcSubComponents ();

  /**
   * Adds a sub-component to this component. More precisely adds the component
   * whose reference is given as a sub-component of the component to which this
   * interface belongs. If <i>C</i> is the sub-component set returned by {@link
   * #getFcSubComponents getFcSubComponents} just before a call to this
   * method, and <i>C'</i> is the sub-component set just after this call, then
   * <tt>subComponent</tt> is guaranteed to be in <i>C'</i>, but <i>C'</i> is
   * <i>not</i> guaranted to be the union of <i>C</i> and <i>{subComponent}</i>,
   * nor to contain all the elements of <i>C</i>.
   *
   * @param subComponent the component to be added inside this component.
   * @throws IllegalContentException if the given component cannot be added
   *      inside this component.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  void addFcSubComponent (Component subComponent)
    throws IllegalContentException, IllegalLifeCycleException;

  /**
   * Removes a sub-component from this component. More precisely removes the
   * sub-component whose reference is given from the component to which this
   * interface belongs. If <i>C</i> is the sub-component set returned by {@link
   * #getFcSubComponents getFcSubComponents} just before a call to this
   * method, and <i>C'</i> is the sub-component set just after this call, then
   * <tt>subComponent</tt> is guaranteed not to be in <i>C'</i>, but <i>C'</i>
   * is <i>not</i> guaranted to be the difference of <i>C</i> and
   * <i>{subComponent}</i>, nor to contain all the elements of <i>C</i> distinct
   * from <tt>subComponent</tt>.
   *
   * @param subComponent the component to be removed from this component.
   * @throws IllegalContentException if the given component cannot be removed
   *      from this component.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  void removeFcSubComponent (Component subComponent)
    throws IllegalContentException, IllegalLifeCycleException;
}
