/***
 * Fractal API
 * Copyright (C) 2001-2002 France Telecom, INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Thierry Coupaye, Pascal Dechamboux, Romain Lenglet,
 *          Philippe Merle, Jean-Bernard Stefani.
 */

package org.objectweb.fractal.api;

/**
 * A component interface to introspect the external interfaces of the component
 * to which it belongs.
 */

public interface Component {

  /**
   * Returns the type of the component to which this interface belongs. The type
   * system is not specified here, in order to allow many type systems to be
   * used. The basic type system specified in the {@link
   * org.objectweb.fractal.api.type} package should be sufficient in most cases,
   * but another one can be used if necessary.
   *
   * @return the type of the component to which this interface belongs.
   */

  Type getFcType ();

  /**
   * Returns the external interfaces of the component to which this interface
   * belongs.
   *
   * @return the external interfaces of the component to which this interface
   *      belongs, including this interface.
   */

  Object[] getFcInterfaces ();

  /**
   * Returns an external interface of the component to which this interface
   * belongs.
   *
   * @param interfaceName the name of the external interface that must be
   *      returned.
   * @return the external interface of the component to which this interface
   *      belongs, whose name is equal to the given name.
   * @throws NoSuchInterfaceException if there is no such interface.
   */

  Object getFcInterface (String interfaceName) throws NoSuchInterfaceException;
}
