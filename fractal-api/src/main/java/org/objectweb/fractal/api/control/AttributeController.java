/***
 * Fractal API
 * Copyright (C) 2001-2002 France Telecom, INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Thierry Coupaye, Pascal Dechamboux, Romain Lenglet,
 *          Philippe Merle, Jean-Bernard Stefani.
 */

package org.objectweb.fractal.api.control;

/**
 * A component interface to control the attributes of the component to which it
 * belongs. More precisely this interface denotes the component interfaces that
 * can control component attributes: a component interface whose Java type
 * is a Java interface that extends this Java interface is indeed considered as
 * an interface to control the attributes of the component to which it belongs.
 * Such interfaces <i>must</i> only contain getter and setter methods, such
 * as <tt>int getX (); void setX (int x)</tt>, <tt>double getSize (); void
 * setSize (double x)</tt>, and so on. These methods should only be used to
 * configure "primitive" values such as integers or strings: they must not be
 * used to configure bindings (this is the role of the {@link BindingController}
 * interface). For example, they can be used to configure the size of a cache
 * component, the load factor of a hashtable component, the label or color of a
 * button component...
 */

public interface AttributeController {
}
