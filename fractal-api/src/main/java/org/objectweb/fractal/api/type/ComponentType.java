/***
 * Fractal API
 * Copyright (C) 2001-2002 France Telecom, INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Thierry Coupaye, Pascal Dechamboux, Romain Lenglet,
 *          Philippe Merle, Jean-Bernard Stefani.
 */

package org.objectweb.fractal.api.type;

import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.NoSuchInterfaceException;

/**
 * A component type. A component type is just a collection of component
 * interface types, which describes the interfaces that components of this type
 * must or may have at runtime.
 */

public interface ComponentType extends Type {

  /**
   * Returns the types of the interfaces of components of this type.
   *
   * @return the types of the interfaces that components of this type must or
   *      may have at runtime.
   */

  InterfaceType[] getFcInterfaceTypes ();

  /**
   * Returns an interface type of this component type from its name. This method
   * is not strictly necessary, as it can be implemented by using the {@link
   * #getFcInterfaceTypes getFcInterfaceTypes} method. But it is convenient and
   * can be implemented more efficiently than with the previous method. This is
   * why it is specified here.
   *
   * @param name the name of one of the interface types returned by {@link
   *      #getFcInterfaceTypes getFcInterfaceTypes} (see {@link
   *      InterfaceType#getFcItfName getFcItfName}).
   * @return the interface type of this component type whose name is equal to
   *      the given name (see {@link InterfaceType#getFcItfName getFcItfName}).
   * @throws NoSuchInterfaceException if there is no such interface type.
   */

  InterfaceType getFcInterfaceType (String name)
    throws NoSuchInterfaceException;
}
