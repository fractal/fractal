/***
 * Fractal API
 * Copyright (C) 2001-2002 France Telecom, INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Thierry Coupaye, Pascal Dechamboux, Romain Lenglet,
 *          Philippe Merle, Jean-Bernard Stefani.
 */

package org.objectweb.fractal.api;

/**
 * An interface to introspect component interfaces. If a Fractal component
 * supports interface introspection, then the interfaces returned by the
 * {@link Component#getFcInterfaces getFcInterfaces} and {@link
 * Component#getFcInterface(String) getFcInterface} methods can be cast into this
 * Java interface, in order to get their name or their type.
 */

public interface Interface {

  /**
   * Returns the component to which this interface belongs.
   *
   * @return the {@link Component} interface of the component to which this
   *      interface belongs.
   */

  Component getFcItfOwner ();

  /**
   * Returns the name of this interface inside its component. This name is
   * "unique" inside the component: more precisely, all the external
   * interfaces of the component have distinct names, as well as all its
   * internal interfaces. However an internal interface can have the same name
   * as an external interface. Of course, two interfaces in two distinct
   * components can also have the same name.
   *
   * @return the name of this interface inside its component.
   */

  String getFcItfName ();

  /**
   * Returns the type of this interface. The type system is not specified here,
   * in order to allow many type systems to be used. The basic type system
   * specified in the {@link org.objectweb.fractal.api.type} package should be
   * sufficient in most cases, but another one can be used if necessary.
   *
   * @return the type of this interface.
   */

  Type getFcItfType ();

  /**
   * Returns <tt>true</tt> if this interface is an internal interface.
   *
   * @return <tt>true</tt> if this interface is an internal interface.
   */

  boolean isFcInternalItf ();
}
