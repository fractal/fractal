/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import java.util.Hashtable;

import javax.management.ObjectInstance;

public class PropertyHelper {

    private ObjectInstance owner;

    public PropertyHelper(ObjectInstance owner) {
        super();
        this.owner = owner;
    }

    public ObjectInstance getOwner() {
        return owner;
    }

    public void setOwner(ObjectInstance owner) {
        this.owner = owner;
    }

    public String getProperty(String name) {
        return owner.getObjectName().getKeyProperty(name);
    }

    public Hashtable<String, String> getProperties() {
        return owner.getObjectName().getKeyPropertyList();
    }
}
