/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.HashMap;
import java.util.Map;

import javax.management.MBeanOperationInfo;

public class OperationNode extends JMXAbstractNode {

    private MBeanOperationInfo opInfo;

    private static Map<String, String> impact = new HashMap<String, String>();

    static {
        impact.put(String.valueOf(MBeanOperationInfo.ACTION), "ACTION");
        impact.put(String.valueOf(MBeanOperationInfo.ACTION_INFO), "ACTION_INFO");
        impact.put(String.valueOf(MBeanOperationInfo.INFO), "INFO");
        impact.put(String.valueOf(MBeanOperationInfo.UNKNOWN), "UNKNOWN");
    }

    public OperationNode(JMXModel model, MBeanOperationInfo opInfo, String mbeanName) {
        super(model.getNodeKind("operation"), mbeanName);
        checkNotNull(opInfo);
        this.opInfo = opInfo;
    }

    public Object getProperty(String name) {
        if ("name".equals(name)) {
            return getName();
        } else if ("impact".equals(name)) {
            return getImpact();
        } else if ("description".equals(name)) {
            return getDescription();
        } else if ("returnType".equals(name)) {
            return getDescription();
        } else {
            throw new IllegalArgumentException("Invalid property name '" + name + "'.");
        }
    }

    public void setProperty(String name, Object value) {
        // Not writable properties
    }

    public String getName() {
        return opInfo.getName();
    }

    public String getDescription() {
        return opInfo.getDescription();
    }

    public String getImpact() {
        return impact.get(String.valueOf(opInfo.getImpact()));
    }

    @Override
    public String toString() {
        return "#<operation: " + getName() + ">";
    }
}
