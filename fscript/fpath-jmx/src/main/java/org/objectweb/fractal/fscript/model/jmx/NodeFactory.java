/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import java.io.IOException;
import java.net.MalformedURLException;

import javax.management.MBeanNotificationInfo;
import javax.management.MBeanOperationInfo;
import javax.management.ObjectInstance;

public interface NodeFactory {

    /*
     * (non-Javadoc)
     * 
     * @see org.objectweb.fractal.fscript.model.jmx.nodfactorye#createMBeanNode(javax.management.ObjectInstance)
     */
    public abstract MBeanNode createMBeanNode(ObjectInstance obInstance);

    /*
     * (non-Javadoc)
     * 
     * @see org.objectweb.fractal.fscript.model.jmx.nodfactorye#createAttributeNode(javax.management.MBeanAttributeInfo)
     */
    public abstract AttributeNode createAttributeNode(AttributeHelper attributeHelper,
            String name);

    /*
     * (non-Javadoc)
     * 
     * @see org.objectweb.fractal.fscript.model.jmx.nodfactorye#createOperationNode(javax.management.MBeanOperationInfo)
     */
    public abstract OperationNode createOperationNode(MBeanOperationInfo opInfo,
            String mbeanName);

    /*
     * (non-Javadoc)
     * 
     * @see org.objectweb.fractal.fscript.model.jmx.nodfactorye#createNotificationNode(javax.management.MBeanNotificationInfo)
     */
    public abstract NotificationNode createNotificationNode(
            MBeanNotificationInfo notInfo, String mbeanName);

    /*
     * (non-Javadoc)
     * 
     * @see org.objectweb.fractal.fscript.model.jmx.nodfactorye#createDomainNode(java.lang.String)
     */
    public abstract DomainNode createDomainNode(String name);

    public void connect(String url) throws MalformedURLException, IOException;
}
