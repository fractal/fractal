/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.management.MBeanNotificationInfo;

public class NotificationNode extends JMXAbstractNode {

    private MBeanNotificationInfo notInfo;

    public NotificationNode(JMXModel model, MBeanNotificationInfo notInfo,
            String mbeanName) {
        super(model.getNodeKind("notification"), mbeanName);
        checkNotNull(notInfo);
        this.notInfo = notInfo;
    }

    public Object getProperty(String name) {
        if ("name".equals(name)) {
            return getName();
        } else if ("description".equals(name)) {
            return getDescription();
        } else if ("notifTypes".equals(name)) {
            return getNotifTypes();
        } else {
            throw new IllegalArgumentException("Invalid property name '" + name + "'.");
        }
    }

    public void setProperty(String name, Object value) {
        // TODO Set property
    }

    public String getName() {
        return notInfo.getName();
    }

    public String getDescription() {
        return notInfo.getDescription();
    }

    public String getNotifTypes() {
        return notInfo.getNotifTypes().toString();
    }

    @Override
    public String toString() {
        return "#<notification: " + getName() + ">";
    }

}
