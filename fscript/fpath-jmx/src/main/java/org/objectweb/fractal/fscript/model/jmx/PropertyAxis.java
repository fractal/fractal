/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

import org.objectweb.fractal.fscript.model.AbstractAxis;
import org.objectweb.fractal.fscript.model.Node;

public class PropertyAxis extends AbstractAxis {

    public PropertyAxis(JMXModel model) {
        super(model, "property", model.getNodeKind("mbean"), model
                .getNodeKind("property"));
    }

    public Set<Node> selectFrom(Node source) {
        try {
            Set<Node> propertyNodeList = new HashSet<Node>();
            MBeanNode mbean = (MBeanNode) source;
            PropertyHelper propertyHelper = new PropertyHelper(mbean.getMBean());
            Hashtable<String, String> properties = propertyHelper.getProperties();
            for (String propName : properties.keySet()) {
                propertyNodeList.add(new PropertyNode((JMXModel) model, propertyHelper,
                        propName));
            }

            return propertyNodeList;
        } catch (ClassCastException ex) {
            throw new IllegalArgumentException("Invalid node kind.");
        } catch (Exception ex) {
            // TODO Handle other exceptions
            throw new IllegalArgumentException("Invalid node kind");
        }
    }

    public boolean isModifiable() {
        return false;
    }

    public boolean isPrimitive() {
        return false;
    }

}
