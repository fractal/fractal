/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import static org.objectweb.fractal.fscript.types.PrimitiveType.*;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.MalformedURLException;

import javax.management.MBeanNotificationInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanServerConnection;
import javax.management.ObjectInstance;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.objectweb.fractal.fscript.model.BasicModel;
import org.objectweb.fractal.fscript.model.Property;

public class JMXModel extends BasicModel implements NodeFactory {
    private MBeanServerConnection mbsc;
    private String serviceURL; // = "service:jmx:rmi:///jndi/rmi://:9999/jmxrmi";

    public JMXModel() {
    }

    public void connect(String url) throws MalformedURLException, IOException {
        this.serviceURL = url;

        if ("local-vm".equals(url)) {
            mbsc = ManagementFactory.getPlatformMBeanServer();
        } else {
            JMXServiceURL srvcUrl = new JMXServiceURL(serviceURL);
            JMXConnector jmxc = JMXConnectorFactory.connect(srvcUrl, null);
            mbsc = jmxc.getMBeanServerConnection();
        }
    }

    public MBeanServerConnection getConnection() {
        return mbsc;
    }

    @Override
    public String toString() {
        return "JMX model";
    }

    @Override
    protected void createNodeKinds() {
        super.createNodeKinds();
        addKind("domain", new Property("name", STRING, false));
        addKind("mbean", new Property("name", STRING, true), new Property("objectName",
                STRING, true), new Property("className", STRING, true), new Property(
                "description", STRING, true));
        addKind("attribute", new Property("name", STRING, false), new Property("type",
                STRING, false), new Property("value", OBJECT, true), new Property(
                "readable", BOOLEAN, false), new Property("writable", BOOLEAN, false),
                new Property("description", STRING, false), new Property("is", BOOLEAN,
                        false));
        addKind("operation", new Property("name", STRING, false), new Property(
                "description", STRING, false), new Property("impact", STRING, true),
                new Property("returnType", STRING, false));
        addKind("notification", new Property("name", STRING, false), new Property(
                "description", STRING, false), new Property("notifTypes", STRING, false));
        addKind("property", new Property("name", STRING, false), new Property("value",
                STRING, false));
    }

    @Override
    protected void createAxes() {
        super.createAxes();
        addAxis(new DomainAxis(this));
        addAxis(new AttributeAxis(this));
        addAxis(new MBeanAxis(this));
        addAxis(new NotificationAxis(this));
        addAxis(new OperationAxis(this));
        addAxis(new PropertyAxis(this));
    }

    @Override
    protected void createAdditionalProcedures() {
        super.createAdditionalProcedures();
        addProcedure(new NewConnectionAction(this));
        addProcedure(new DomainsFunction(this));
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.objectweb.fractal.fscript.model.jmx.nodfactorye#createMBeanNode(javax.management.ObjectInstance)
     */
    /*
     * (non-Javadoc)
     * 
     * @see org.objectweb.fractal.fscript.model.jmx.NodeFactory#createMBeanNode(javax.management.ObjectInstance)
     */
    public MBeanNode createMBeanNode(ObjectInstance obInstance) {
        return new MBeanNode(this, obInstance);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.objectweb.fractal.fscript.model.jmx.nodfactorye#createAttributeNode(javax.management.MBeanAttributeInfo)
     */
    /*
     * (non-Javadoc)
     * 
     * @see org.objectweb.fractal.fscript.model.jmx.NodeFactory#createAttributeNode(org.objectweb.fractal.fscript.model.jmx.AttributeHelper,
     *      java.lang.String)
     */
    public AttributeNode createAttributeNode(AttributeHelper attributeHelper, String name) {
        return new AttributeNode(this, attributeHelper, name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.objectweb.fractal.fscript.model.jmx.nodfactorye#createOperationNode(javax.management.MBeanOperationInfo)
     */
    /*
     * (non-Javadoc)
     * 
     * @see org.objectweb.fractal.fscript.model.jmx.NodeFactory#createOperationNode(javax.management.MBeanOperationInfo,
     *      java.lang.String)
     */
    public OperationNode createOperationNode(MBeanOperationInfo opInfo, String mbeanName) {
        return new OperationNode(this, opInfo, mbeanName);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.objectweb.fractal.fscript.model.jmx.nodfactorye#createNotificationNode(javax.management.MBeanNotificationInfo)
     */
    /*
     * (non-Javadoc)
     * 
     * @see org.objectweb.fractal.fscript.model.jmx.NodeFactory#createNotificationNode(javax.management.MBeanNotificationInfo,
     *      java.lang.String)
     */
    public NotificationNode createNotificationNode(MBeanNotificationInfo notInfo,
            String mbeanName) {
        return new NotificationNode(this, notInfo, mbeanName);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.objectweb.fractal.fscript.model.jmx.nodfactorye#createDomainNode(java.lang.String)
     */
    /*
     * (non-Javadoc)
     * 
     * @see org.objectweb.fractal.fscript.model.jmx.NodeFactory#createDomainNode(java.lang.String)
     */
    public DomainNode createDomainNode(String name) {
        return new DomainNode(this, name);
    }
}
