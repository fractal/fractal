/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import static org.objectweb.fractal.fscript.diagnostics.Severity.ERROR;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.objectweb.fractal.fscript.ScriptExecutionError;
import org.objectweb.fractal.fscript.diagnostics.Diagnostic;
import org.objectweb.fractal.fscript.interpreter.Context;
import org.objectweb.fractal.fscript.model.Node;
import org.objectweb.fractal.fscript.procedures.NativeProcedure;
import org.objectweb.fractal.fscript.types.NodeSetType;
import org.objectweb.fractal.fscript.types.Signature;

public class DomainsFunction implements NativeProcedure {

    private JMXModel model;

    public DomainsFunction(JMXModel model) {
        super();
        this.model = model;
    }

    public Object apply(List<Object> args, Context ctx) throws ScriptExecutionError {
        Set<Node> result = new HashSet<Node>();
        try {
            if (model.getClass() == null) {
                Diagnostic err = new Diagnostic(ERROR, "JMX Client not connected");
                throw new ScriptExecutionError(err);
            }

            String[] domains = model.getConnection().getDomains();
            for (String domain : domains) {
                result.add(model.createDomainNode(domain));
            }
        } catch (IOException ioe) {
            Diagnostic err = new Diagnostic(ERROR, "Error reading from JMX Server");
            throw new ScriptExecutionError(err);
        }
        return result;
    }

    public String getName() {
        return "domains";
    }

    public Signature getSignature() {
        return new Signature(new NodeSetType(model.getNodeKind("domain")));
    }

    public boolean isPureFunction() {
        return false;
    }

}
