/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectInstance;
import javax.management.ObjectName;

import org.objectweb.fractal.fscript.model.AbstractAxis;
import org.objectweb.fractal.fscript.model.Node;
import org.objectweb.fractal.fscript.model.NodeKind;
import org.objectweb.fractal.fscript.types.UnionType;

public class MBeanAxis extends AbstractAxis {

    private JMXModel model;

    public MBeanAxis(JMXModel model) {
        super(model, "mbean",
                new UnionType(model.getNodeKinds().toArray(new NodeKind[0])), model
                        .getNodeKind("mbean"));

        this.model = model;
    }

    public Set<Node> selectFrom(Node source) {
        Set<Node> result = null;
        try {
            result = new HashSet<Node>();
            JMXAbstractNode jmxSource = (JMXAbstractNode) source;
            String query = jmxSource.getMBeanName();

            Set<ObjectInstance> objs = (model.getConnection().queryMBeans(new ObjectName(
                    query), null));
            for (ObjectInstance obj : objs) {
                MBeanNode mbeanNode = new MBeanNode(model, obj);
                result.add(mbeanNode);
            }
        } catch (ClassCastException cce) {
            throw new IllegalArgumentException("Invalid node kind");
        } catch (MalformedObjectNameException me) {
            throw new IllegalArgumentException("Invalid mbean name");
        } catch (IOException io) {
            // TODO Handle IOException
        }

        return result;
    }

    public boolean isModifiable() {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean isPrimitive() {
        // TODO Auto-generated method stub
        return false;
    }

}
