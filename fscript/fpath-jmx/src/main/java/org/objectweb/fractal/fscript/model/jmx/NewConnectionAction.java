/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import static org.objectweb.fractal.fscript.diagnostics.Severity.ERROR;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import org.objectweb.fractal.fscript.ScriptExecutionError;
import org.objectweb.fractal.fscript.diagnostics.Diagnostic;
import org.objectweb.fractal.fscript.interpreter.Context;
import org.objectweb.fractal.fscript.procedures.NativeProcedure;
import org.objectweb.fractal.fscript.types.PrimitiveType;
import org.objectweb.fractal.fscript.types.Signature;
import org.objectweb.fractal.fscript.types.VoidType;

public class NewConnectionAction implements NativeProcedure {

    private JMXModel model;

    public NewConnectionAction(JMXModel model) {
        super();
        this.model = model;
    }

    public String getName() {
        return "new-connection";
    }

    public Signature getSignature() {
        return new Signature(new VoidType(), PrimitiveType.STRING);
    }

    public boolean isPureFunction() {
        return false;
    }

    public Object apply(List<Object> args, Context ctx) throws ScriptExecutionError {
        String url = (String) args.get(0);
        try {
            model.connect(url);
        } catch (MalformedURLException me) {
            Diagnostic err = new Diagnostic(ERROR, "Invalid URL " + url);
            throw new ScriptExecutionError(err);
        } catch (IOException ioe) {
            Diagnostic err = new Diagnostic(ERROR, "Unable to connect to " + url);
            throw new ScriptExecutionError(err);
        }

        return null;
    }

}
