/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.management.ObjectInstance;

public class PropertyNode extends JMXAbstractNode {

    private String name;
    private PropertyHelper propertyHelper;

    public PropertyNode(JMXModel model, PropertyHelper propertyHelper, String name) {
        super(model.getNodeKind("property"), propertyHelper.getOwner().getObjectName()
                .getCanonicalName());
        checkNotNull(propertyHelper);
        this.propertyHelper = propertyHelper;
        this.name = name;
    }

    public ObjectInstance getOwner() {
        return propertyHelper.getOwner();
    }

    @Override
    public Object getProperty(String name) {
        if ("name".equals(name)) {
            return getName();
        } else if ("value".equals(name)) {
            return getValue();
        } else {
            throw new IllegalArgumentException("Invalid property name '" + name + "'.");
        }
    }

    @Override
    public void setProperty(String name, Object value) {
        // NOT MODIFIABLE
    }

    public String getValue() {
        return propertyHelper.getProperty(name);
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PropertyNode) {
            PropertyNode other = (PropertyNode) obj;
            return (this.propertyHelper.getOwner() == other.propertyHelper.getOwner() && this.name
                    .equals(other.getName()));
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        // String ownerName = this.getOwner().getObjectName().getCanonicalName();

        return "#<property: " + getName() + " = " + getValue() + ">";
    }

}
