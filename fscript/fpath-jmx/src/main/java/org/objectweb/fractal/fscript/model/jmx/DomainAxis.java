/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import org.objectweb.fractal.fscript.model.AbstractAxis;
import org.objectweb.fractal.fscript.model.Node;

public class DomainAxis extends AbstractAxis {

    public DomainAxis(JMXModel model) {
        super(model, "domain", model.getNodeKind("mbean"), model.getNodeKind("domain"));
    }

    public Set<Node> selectFrom(Node source) {
        try {
            Set<Node> domainSet = new TreeSet<Node>();

            JMXAbstractNode sourceNode = (JMXAbstractNode) source;
            if (sourceNode.getDomain() == null)
                throw new IllegalArgumentException("Invalid node kind.");

            ObjectName nameQuery = new ObjectName(sourceNode.getDomain() + ":*");
            Set<ObjectName> obNames = ((JMXModel) model).getConnection().queryNames(
                    nameQuery, null);
            for (ObjectName obName : obNames) {
                domainSet.add(new DomainNode((JMXModel) model, obName.getDomain()));
            }

            return domainSet;
        } catch (ClassCastException ex) {
            throw new IllegalArgumentException("Invalid node kind.");
        } catch (MalformedObjectNameException moe) {
            throw new IllegalArgumentException("Invalid domain name.");
        } catch (IOException ioe) {
            throw new IllegalArgumentException("Error creating domain node.");
        }
    }

    public boolean isModifiable() {
        return false;
    }

    public boolean isPrimitive() {
        return false;
    }
}
