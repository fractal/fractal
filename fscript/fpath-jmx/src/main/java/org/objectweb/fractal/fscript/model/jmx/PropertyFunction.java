/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import static org.objectweb.fractal.fscript.diagnostics.Severity.ERROR;

import java.util.List;

import org.objectweb.fractal.fscript.ScriptExecutionError;
import org.objectweb.fractal.fscript.diagnostics.Diagnostic;
import org.objectweb.fractal.fscript.interpreter.Context;
import org.objectweb.fractal.fscript.model.NodeKind;
import org.objectweb.fractal.fscript.procedures.NativeProcedure;
import org.objectweb.fractal.fscript.types.PrimitiveType;
import org.objectweb.fractal.fscript.types.Signature;

public class PropertyFunction implements NativeProcedure {

    private JMXModel model;

    public PropertyFunction(JMXModel model) {
        this.model = model;
    }

    public String getName() {
        return "property";
    }

    public Signature getSignature() {
        return new Signature(PrimitiveType.STRING, NodeKind.ANY_NODE_KIND,
                PrimitiveType.STRING);
    }

    public boolean isPureFunction() {
        return true;
    }

    public Object apply(List<Object> args, Context ctx) throws ScriptExecutionError {
        if (args.size() < 2) {
            Diagnostic err = new Diagnostic(ERROR, "Wrong number of argument.");
            throw new ScriptExecutionError(err);
        }
        try {
            MBeanNode aNode = (MBeanNode) args.get(0);
            String propName = (String) args.get(1);
            String propVal = aNode.getMBean().getObjectName().getKeyProperty(propName);
            if (propVal == null)
                propVal = "";

            return propVal;
        } catch (ClassCastException cce) {
            Diagnostic err = new Diagnostic(ERROR, "Illegal argument.");
            throw new ScriptExecutionError(err);
        }

    }

}
