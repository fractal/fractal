/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.management.ObjectInstance;

public class MBeanNode extends JMXAbstractNode {

    private final ObjectInstance mbean;

    MBeanNode(JMXModel model, ObjectInstance mbean) {
        super(model.getNodeKind("mbean"), null);
        checkNotNull(mbean);
        this.mbean = mbean;
        super.setDomain(mbean.getObjectName().getDomain());
    }

    public ObjectInstance getMBean() {
        return mbean;
    }

    public Object getProperty(String name) {
        if ("objectName".equals(name)) {
            return getName();
        } else if ("className".equals(name)) {
            return getClassName();
        } else if ("description".equals(name)) {
            return getDescription();
        } else if ("name".equals(name)) {
            return getName();
            // Object nameProperty = mbean.getObjectName().getKeyProperty(name);
            // if (nameProperty == null)
            // return "";
            // else
            // return nameProperty;
        } else {
            throw new IllegalArgumentException("Invalid property name '" + name + "'.");
        }
    }

    public void setProperty(String name, Object value) {
        // TODO Set property

    }

    public String getName() {
        return mbean.getObjectName().getCanonicalName();
    }

    public String getClassName() {
        return mbean.getClassName();
    }

    public String getDescription() {
        // TODO Get description
        return "";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MBeanNode) {
            MBeanNode other = (MBeanNode) obj;
            return this.mbean == other.mbean;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return mbean.hashCode();
    }

    @Override
    public String toString() {
        return "#<mbean: " + getName() + ">";
    }
}
