/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.NoSuchElementException;

import javax.management.ObjectInstance;

public class AttributeNode extends JMXAbstractNode {
    private AttributeHelper attributeHelper;
    private String name;

    public AttributeNode(JMXModel model, AttributeHelper attributeHelper, String name) {
        super(model.getNodeKind("attribute"), attributeHelper.getOwner().getObjectName()
                .getCanonicalName());
        checkNotNull(attributeHelper);
        this.attributeHelper = attributeHelper;
        this.name = name;
    }

    public ObjectInstance getOwner() {
        return attributeHelper.getOwner();
    }

    @Override
    public Object getProperty(String name) {
        if ("name".equals(name)) {
            return getName();
        } else if ("type".equals(name)) {
            return getType();
        } else if ("value".equals(name)) {
            return getValue();
        } else if ("readable".equals(name)) {
            return isReadable();
        } else if ("writable".equals(name)) {
            return isWritable();
        } else if ("description".equals(name)) {
            return getDescription();
        } else if ("is".equals(name)) {
            return isIs();
        } else {
            throw new IllegalArgumentException("Invalid property name '" + name + "'.");
        }
    }

    @Override
    public void setProperty(String name, Object value) {
        checkSetRequest(name, value);
        if ("value".equals(name)) {
            setValue(value);
        } else {
            throw new AssertionError("Should ne be reached.");
        }
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return attributeHelper.getAttributeInfo(name).getType();
    }

    public Object getValue() {
        try {
            return attributeHelper.getAttribute(name);
        } catch (NoSuchElementException e) {
            throw new AssertionError("Attribute " + getName() + " does not exist.");
        } catch (UnsupportedOperationException e) {
            throw e;
        } catch (Exception ex) {
            // TODO Handle exception here
            return null;
        }
    }

    public void setValue(Object value) {
        throw new UnsupportedOperationException();
    }

    public boolean isReadable() {
        return attributeHelper.getAttributeInfo(name).isReadable();
    }

    public boolean isWritable() {
        return attributeHelper.getAttributeInfo(name).isWritable();
    }

    public String getDescription() {
        return attributeHelper.getAttributeInfo(name).getDescription();
    }

    public boolean isIs() {
        return attributeHelper.getAttributeInfo(name).isIs();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AttributeNode) {
            AttributeNode other = (AttributeNode) obj;
            return (this.attributeHelper.getOwner() == other.attributeHelper.getOwner() && this.name
                    .equals(other.getName()));
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return attributeHelper.getOwner().hashCode() * 7 + name.hashCode();
    }

    @Override
    public String toString() {
        // String ownerName = this.getOwner().getObjectName().getCanonicalName();
        return "#<attribute: " + getName() + " = " + getValue() + ">";
    }
}
