/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.management.InstanceNotFoundException;
import javax.management.IntrospectionException;
import javax.management.MBeanInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanServerConnection;
import javax.management.ReflectionException;

import org.objectweb.fractal.fscript.model.AbstractAxis;
import org.objectweb.fractal.fscript.model.Node;

public class OperationAxis extends AbstractAxis {

    public OperationAxis(JMXModel model) {
        super(model, "operation", model.getNodeKind("mbean"), model
                .getNodeKind("operation"));
    }

    public Set<Node> selectFrom(Node source) {
        Set<Node> result = null;
        try {
            MBeanNode mbean = (MBeanNode) source;
            result = new HashSet<Node>();

            // Get MBean operations
            MBeanServerConnection mbsc = ((JMXModel) model).getConnection();
            MBeanInfo mbeanInfo = mbsc.getMBeanInfo(mbean.getMBean().getObjectName());
            for (MBeanOperationInfo opInfo : mbeanInfo.getOperations()) {
                Node node = new OperationNode((JMXModel) model, opInfo, mbean.getName());
                result.add(node);
            }
        } catch (InstanceNotFoundException infe) {
            throw new IllegalArgumentException("No instance found.");
        } catch (ReflectionException rex) {
            throw new IllegalArgumentException("Reflection error.");
        } catch (IntrospectionException iex) {
            throw new IllegalArgumentException("Reflection error.");
        } catch (ClassCastException ex) {
            throw new IllegalArgumentException("Invalid node kind.");
        } catch (IOException ioe) {
            // TODO Handle IOException
        }
        return result;
    }

    public boolean isModifiable() {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean isPrimitive() {
        // TODO Auto-generated method stub
        return false;
    }

}
