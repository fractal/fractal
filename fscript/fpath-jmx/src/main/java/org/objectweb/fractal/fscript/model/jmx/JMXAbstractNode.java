/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import org.objectweb.fractal.fscript.model.AbstractNode;
import org.objectweb.fractal.fscript.model.NodeKind;

public abstract class JMXAbstractNode extends AbstractNode implements Comparable<Object> {

    private String mbeanName;
    private String domain;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public JMXAbstractNode(NodeKind kind, String mbeanName) {
        super(kind);
        this.mbeanName = mbeanName;
    }

    public abstract Object getProperty(String name);

    public abstract void setProperty(String name, Object value);

    public String getMBeanName() {
        return mbeanName;
    }

    public void setMBeanName(String mbeanName) {
        this.mbeanName = mbeanName;
    }

    public int compareTo(Object arg0) {
        // TODO Auto-generated method stub
        return -1;
    }
}
