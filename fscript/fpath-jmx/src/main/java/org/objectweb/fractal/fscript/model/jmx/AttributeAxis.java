/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import java.util.HashSet;
import java.util.Set;

import org.objectweb.fractal.fscript.model.AbstractAxis;
import org.objectweb.fractal.fscript.model.Node;

public class AttributeAxis extends AbstractAxis {

    public AttributeAxis(JMXModel model) {
        super(model, "attribute", model.getNodeKind("mbean"), model
                .getNodeKind("attribute"));
    }

    public Set<Node> selectFrom(Node source) {
        try {
            MBeanNode mbeanNode = (MBeanNode) source;
            Set<Node> result = new HashSet<Node>();
            // Get MBean attributes from connection with mbean name
            AttributeHelper attributeHelper = new AttributeHelper((JMXModel) model,
                    mbeanNode.getMBean());
            Set<String> attrNames = attributeHelper.getAttributesNames();

            for (String attrName : attrNames) {
                AttributeNode node = new AttributeNode((JMXModel) model, attributeHelper,
                        attrName);
                result.add(node);
            }
            return result;
        } catch (ClassCastException ex) {
            throw new IllegalArgumentException("Invalid node kind.");
        } catch (Exception ex) {
            throw new IllegalArgumentException("Invalid node kind.");
        }
    }

    public boolean isModifiable() {
        return false;
    }

    public boolean isPrimitive() {
        return false;
    }
}
