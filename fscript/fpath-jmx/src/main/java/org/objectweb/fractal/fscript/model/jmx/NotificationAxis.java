/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import java.util.HashSet;
import java.util.Set;

import javax.management.MBeanInfo;
import javax.management.MBeanNotificationInfo;
import javax.management.MBeanServerConnection;

import org.objectweb.fractal.fscript.model.AbstractAxis;
import org.objectweb.fractal.fscript.model.Node;

public class NotificationAxis extends AbstractAxis {

    public NotificationAxis(JMXModel model) {
        super(model, "notification", model.getNodeKind("mbean"), model
                .getNodeKind("notification"));
    }

    public Set<Node> selectFrom(Node source) {
        try {
            MBeanNode mbeanNode = (MBeanNode) source;
            Set<Node> result = new HashSet<Node>();

            // Get MBean notifications from connection
            MBeanServerConnection mbsc = ((JMXModel) model).getConnection();
            MBeanInfo mbeanInfo = mbsc.getMBeanInfo(mbeanNode.getMBean().getObjectName());
            for (MBeanNotificationInfo notInfo : mbeanInfo.getNotifications()) {
                Node node = new NotificationNode((JMXModel) model, notInfo, mbeanNode
                        .getName());
                result.add(node);
            }

            return result;
        } catch (ClassCastException ex) {
            throw new IllegalArgumentException("Invalid node kind.");
        } catch (Exception ex) {
            // TODO Throw some exception here
            throw new IllegalArgumentException("Invalid node kind");
        }
    }

    public boolean isModifiable() {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean isPrimitive() {
        // TODO Auto-generated method stub
        return false;
    }

}
