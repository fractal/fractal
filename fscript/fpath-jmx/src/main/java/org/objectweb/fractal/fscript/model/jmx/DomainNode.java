/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import static com.google.common.base.Preconditions.checkNotNull;

public class DomainNode extends JMXAbstractNode {

    private String name;

    public DomainNode(JMXModel model, String name) {
        super(model.getNodeKind("domain"), name + ":*");
        checkNotNull(name);
        this.name = name;
        super.setDomain("*");
    }

    @Override
    public Object getProperty(String name) {
        if ("name".equals(name)) {
            return getName();
        } else {
            throw new IllegalArgumentException("Invalid property name '" + name + "'.");
        }
    }

    @Override
    public void setProperty(String name, Object value) {
        // Not writable properties
    }

    public String getName() {
        return this.name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DomainNode) {
            DomainNode other = (DomainNode) obj;
            return name.equals(other.getName());
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "#<domain: " + getName() + ">";
    }

}
