/*
 * Copyright (c) 2008 Mayleen Lacouture
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jmx;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.ObjectInstance;
import javax.management.ReflectionException;

public class AttributeHelper {

    private JMXModel model;
    private ObjectInstance owner;
    private final Map<String, MBeanAttributeInfo> attributes;

    public AttributeHelper(JMXModel model, ObjectInstance owner) {
        this.model = model;
        this.owner = owner;
        this.attributes = new HashMap<String, MBeanAttributeInfo>();

        getBeanAttributes();
    }

    private void getBeanAttributes() {
        try {
            MBeanInfo mbeanInfo = model.getConnection().getMBeanInfo(
                    owner.getObjectName());
            MBeanAttributeInfo[] attrList = mbeanInfo.getAttributes();
            for (MBeanAttributeInfo attrInfo : attrList) {
                attributes.put(attrInfo.getName(), attrInfo);
            }
        } catch (Exception ex) {
            // TODO Handle exceptions in getBeanAttributes()
        }
    }

    public ObjectInstance getOwner() {
        return owner;
    }

    public void setOwner(ObjectInstance owner) {
        this.owner = owner;
    }

    public Object getAttribute(String attrName) throws NoSuchElementException, Exception {
        try {
            return model.getConnection().getAttribute(owner.getObjectName(), attrName);
        } catch (IOException ioex) {
            throw new Exception("Error while trying to get attribute : " + ioex);
        } catch (AttributeNotFoundException anfe) {
            throw new NoSuchElementException("No attribute named " + attrName + ".");
        } catch (InstanceNotFoundException infe) {
            throw new Exception("Error while trying to get attribute : " + infe);
        } catch (MBeanException mbe) {
            throw new Exception("Error while trying to get attribute : " + mbe);
        } catch (ReflectionException re) {
            throw new Exception("Error while trying to get attribute : " + re);
        }
    }

    public MBeanAttributeInfo getAttributeInfo(String name) throws NoSuchElementException {
        MBeanAttributeInfo attr = attributes.get(name);
        if (attr != null) {
            return attr;
        } else {
            throw new NoSuchElementException("No attribute named " + name + ".");
        }
    }

    /**
     * Finds all the attributes of the target component.
     * 
     * @return the names of all the attributes (readable, writable or both) declared by
     *         the target component in its <code>attribute-controller</code> interface.
     */
    public Set<String> getAttributesNames() {
        return Collections.unmodifiableSet(attributes.keySet());
    }
}
