This module contains an alternative backend for FPath to support navigation in JMX-based
systems instead of Fractal components. some Jade-specific operations. It is implemented as
an alternative JMXModel component to replace the FractalModel component used by default.
Use the o.o.f.fscript.FPathJMX ADL configuration instead of o.o.f.fscript.FScript to use
this extension.

This extension was developped by Mayleen Lacouture as part of her EMOOSE Master's Degree.
