/*
 * Copyright (c) 2004-2005 Universite de Nantes (LINA)
 * Copyright (c) 2005-2006 France Telecom
 * Copyright (c) 2006-2008 ARMINES
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript.model.jade;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.fractal.util.AttributesHelper;
import org.objectweb.fractal.util.Fractal;
import org.ow2.jasmine.jade.fractal.api.control.GenericAttributeController;

/**
 * This class provides methods to discover and manipulate Fractal components attributes in
 * a generic way. It uses reflection to discover attributes, get and set attributes values
 * by name.
 * <p>
 * The attribute discovery process is done on creation, and uses reflection. Because it
 * can be relatively costly, you should normally create only one
 * <code>AttributeHelper</code> instance for each given <code>Component</code>.
 * <p>
 * Typical usage:
 *
 * <pre>
 * Component c = ...;
 * AttributeHelper attr = new AttributeHelper(c);
 * // Iterate on all the attributes and print their values:
 * for (String attrName : attr.getAttributesNames()) {
 *   System.out.println(attrName + &quot; = &quot; + attr.getAttribute(attrName));
 * }
 * // Change the value of an attribute by name:
 * attr.setAttribute(&quot;foo&quot;, &quot;bar&quot;);
 * </pre>
 *
 * @author Pierre-Charles David
 */
public class GenericAttributesHelper extends AttributesHelper{

    /**
     * Creates a new <code>AttributesHelper</code> to access the specified component's
     * attributes.
     *
     * @param c
     *            the component owner of the attributes to access.
     */
    public GenericAttributesHelper(Component c) {
        super(c);
    }

    /**
     * Tests whether the target component has an attribute of a given name.
     *
     * @param name
     *            the name of the attribute to search for.
     * @return <code>true</code> iff the target component has an attribute (readable,
     *         writable or both) named <code>name</code>.
     */
    @Override
    public boolean hasAttribute(String name) {
        try {
            GenericAttributeController gac = (GenericAttributeController)(getComponent().getFcInterface("generic-attribute-controller"));
            return Arrays.asList(gac.listFcAtt()).contains(name);
        } catch (NoSuchInterfaceException e) {
            return false;
        }
    }

}
