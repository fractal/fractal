/*
 * Copyright (c) 2008 Pierre-Charles David
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Pierre-Charles David <pcdavid@gmail.com>
 */
package org.objectweb.fractal.fscript.console;

import static java.text.MessageFormat.format;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.fscript.Library;
import org.objectweb.fractal.fscript.ast.UserProcedure;
import org.objectweb.fractal.fscript.procedures.NativeProcedure;
import org.objectweb.fractal.fscript.procedures.Procedure;
import org.objectweb.fractal.util.ContentControllerHelper;

/**
 * This command prints out information about the procedure whose name is given in
 * parameter.
 * 
 * @author Pierre-Charles David
 */
public class InfoCommand extends AbstractCommand {
    public void execute(String procName) throws Exception {
        Component lib = ContentControllerHelper.getSubComponentByName(fscript, "library");
        if (lib == null) {
            showWarning("Unable to locate the library component.");
            showWarning("The engine used is not compoatible with this command.");
            return;
        }
        Procedure proc = ((Library) lib.getFcInterface("library")).lookup(procName);
        if (proc == null) {
            showWarning("Unknown procedure '" + procName + "'.");
            return;
        }
        String impl = (proc instanceof NativeProcedure) ? "Native" : "User-defined";
        String kind = proc.isPureFunction() ? "function" : "action";
        String def = null;
        if (proc instanceof NativeProcedure) {
            def = proc.getClass().getName();
        } else {
            def = ((UserProcedure) proc).getSourceLocation().toString();
        }
        showMessage(format("{0} {1} \"" + proc.getName() + "\" is defined in " + def, impl,
                kind, def));
        showMessage("Signature: " + proc.getName() + proc.getSignature().toString());
        if (proc instanceof UserProcedure) {
            UserProcedure up = (UserProcedure) proc;
            showMessage("AST: " + up.toString());
        }
    }
}
