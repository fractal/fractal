/*
 * Copyright (c) 2008 Pierre-Charles David
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Pierre-Charles David <pcdavid@gmail.com>
 */
package org.objectweb.fractal.fscript.console;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.fscript.Library;
import org.objectweb.fractal.fscript.ast.UserProcedure;
import org.objectweb.fractal.fscript.procedures.NativeProcedure;
import org.objectweb.fractal.fscript.procedures.Procedure;
import org.objectweb.fractal.util.ContentControllerHelper;

/**
 * This command prints a list of all the currently available procedures.
 *
 * @author Pierre-Charles David
 */
public class ListCommand extends AbstractCommand {

    public void execute(String args) throws Exception {
        Component lib = ContentControllerHelper.getSubComponentByName(fscript, "library");
        if (lib == null) {
            showWarning("Unable to locate the library component.");
            showWarning("The engine used is not compoatible with this command.");
            return;
        }
        Library library = (Library) lib.getFcInterface("library");
        List<String> names = new ArrayList<String>(library.getProceduresNames());
        for (Iterator iter = names.iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            if (name.startsWith("axis:")) {
                iter.remove();
            }
        }
        Collections.sort(names);
        String[][] data = new String[names.size() + 1][3];
        data[0][0] = "Signature";
        data[0][1] = "Kind";
        data[0][2] = "Definition";
        for (int i = 0; i < names.size(); i++) {
            fillProcData(library.lookup(names.get(i)), data[i+1]);
        }
        showTitle("Available procedures");
        showTable(data);
    }

    private void fillProcData(Procedure proc, String[] row) {
        row[0] = proc.getName() + proc.getSignature().toString();
        row[1] = proc.isPureFunction() ? "Function" : "Action";
        if (proc instanceof NativeProcedure) {
            row[2] = proc.getClass().getName();
        } else {
            row[2] = ((UserProcedure) proc).getSourceLocation().toString();
        }
    }
}
