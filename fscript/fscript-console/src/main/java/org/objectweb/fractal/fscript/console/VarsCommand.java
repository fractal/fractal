/*
 * Copyright (c) 2008 Pierre-Charles David
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Pierre-Charles David <pcdavid@gmail.com>
 */
package org.objectweb.fractal.fscript.console;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.objectweb.fractal.fscript.model.Node;

/**
 * This command prints the list of global variables.
 * 
 * @author Pierre-Charles David
 */
public class VarsCommand extends AbstractCommand {
    public void execute(String args) throws Exception {
        List<String> names = new ArrayList<String>(engine.getGlobals());
        for (Iterator iter = names.iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            // Ignore special variables
            if (name.startsWith("*")) {
                iter.remove();
            }
        }
        Collections.sort(names);
        String[][] data = new String[names.size() + 1][2];
        data[0][0] = "Name";
        data[0][1] = "Value";
        for (int i = 0; i < names.size(); i++) {
            String var = names.get(i);
            Object val = engine.getGlobalVariable(var);
            data[i+1][0] = var;
            data[i+1][1] = printString(val);
        }
        showTitle("Global variables");
        showTable(data);
    }

    @SuppressWarnings("unchecked")
    private String printString(Object val) {
        if (val == null) {
            return "null";
        } else if (val instanceof String) {
            return "\"" + ((String) val).replaceAll("\\\"", "\\\\\\\"") + "\"";
        } else if (val instanceof Set<?>) {
            Set<Node> nodes = (Set<Node>) val;
            return "node-set (" + nodes.size() + " elements)";
        } else {
            return String.valueOf(val);
        }
    }
}
