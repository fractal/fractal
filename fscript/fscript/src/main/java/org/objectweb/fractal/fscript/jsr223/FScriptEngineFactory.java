/***
 * OW2 FScript
 * Copyright (C) 2010 INRIA, University of Lille1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s):
 */

package org.objectweb.fractal.fscript.jsr223;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.fscript.FScript;

public class FScriptEngineFactory implements ScriptEngineFactory {
    /** Script extensions managed by the engine */
    public final static List<String> extensions = new ArrayList<String>() {{ add("fscript"); }};

    /** Factory parameters */
    private Map<String, Object> parameters = new HashMap<String, Object>();;

    /** The architecture to load */
    public final String FSCRIPT_ADL = "org.objectweb.fractal.fscript.Jsr223FScript";
    /**
     * Default constructor.
     */
    public FScriptEngineFactory() {
        parameters.put(ScriptEngine.ENGINE, getEngineName());
        parameters.put(ScriptEngine.ENGINE_VERSION, getEngineVersion());
        parameters.put(ScriptEngine.NAME, getEngineName());
        parameters.put(ScriptEngine.LANGUAGE, getLanguageName());
        parameters.put(ScriptEngine.LANGUAGE_VERSION, getLanguageVersion());        
    }
    
    public String getEngineName() {
        return "OW2 FScript";
    }

    public String getEngineVersion() {
        return "1.0";
    }

    public List<String> getExtensions() {
        return extensions;
    }

    public String getLanguageName() {
        return "FScript";
    }

    public String getLanguageVersion() {
        return "2.1";
    }   

    public String getMethodCallSyntax(String obj, String m, String... args) {
        // TODO Auto-generated method stub
        return null;
    }

    public List<String> getMimeTypes() {
        return new ArrayList<String>() {{ add("application/fscript"); add("text/fscript"); }};
    }

    public List<String> getNames() {
        return new ArrayList<String>() {{ add("fscript"); }};
    }

    public String getOutputStatement(String toDisplay) {
        if (toDisplay == null || toDisplay.length() == 0) {
            return "";
        }
        return "echo('" + toDisplay + "');";
    }

    public Object getParameter(String key) {
        return parameters.get(key);
    }

    public String getProgram(String... statements) {
        // TODO Auto-generated method stub
        return null;
    }

    public ScriptEngine getScriptEngine() {
        Component engine;
        
        try {
            engine = (Component) FScript.newEngineFromAdl(FSCRIPT_ADL);
        } catch (Exception e1) {
            e1.printStackTrace();
            String msg = "Cannot instantiate FScript engine component from adl file: " + FSCRIPT_ADL;
            throw new RuntimeException(msg);
        }
        
        try {
            FactoryManager fm = (FactoryManager) engine.getFcInterface("factory-manager");
            fm.setFactory(this);
        } catch (NoSuchInterfaceException e) {
            String msg = "Invalid FScript engine component: missing 'factory-manager' interface.";
            throw new IllegalArgumentException(msg);
        }
        
        try {
            return (ScriptEngine) engine.getFcInterface("engine");
        } catch (NoSuchInterfaceException e) {
            String msg = "Invalid FScript engine component: missing 'engine' interface.";
            throw new IllegalArgumentException(msg);
        }
    }

}
