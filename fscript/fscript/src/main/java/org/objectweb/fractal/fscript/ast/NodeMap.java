/*
 * Copyright (c) 2011 INRIA-SARDES
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 */
package org.objectweb.fractal.fscript.ast;

import com.google.common.base.Joiner;

import java.io.IOException;
import java.util.Map;

/**
 * @author Loris Bouzonnet
 */
public class NodeMap  extends ASTNode {

    private Map<ASTNode, ASTNode> map;

    /**
     * Creates a new <code>ASTNode</code>.
     *
     * @param location the location of the corresponding fragment in the source code. May be null if the
     *                 location is unknown or if there was no actual source code (e.g. the AST is
     *                 constructed programmatically).
     * @param map      the AST nodes representing the content of the map.
     */
    public NodeMap(SourceLocation location, Map<ASTNode, ASTNode> map) {
        super(location);
        this.map = map;
    }

    @Override
    public void accept(final ASTVisitor visitor) {
        visitor.visitMap(this);
    }

    @Override
    protected void toString(final Appendable appendable) throws IOException {
        appendable.append("{");
        Joiner.on(",").join(appendable, map.entrySet());
        appendable.append("}");
    }


    public Map<ASTNode, ASTNode> getValues() {
        return map;
    }
}
