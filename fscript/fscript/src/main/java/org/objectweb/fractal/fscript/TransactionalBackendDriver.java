/*
 * Copyright (c) 2006-2010 ARMINES, INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Pierre-Charles David
 * Contributor: Christophe Demarey
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.fscript;

import static org.objectweb.util.monolog.api.BasicLevel.ERROR;
import static org.objectweb.util.monolog.api.BasicLevel.INFO;
import static org.objectweb.util.monolog.api.BasicLevel.WARN;

import java.util.Map;

import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.objectweb.fractal.fscript.simulation.Simulator;
import org.objectweb.fractal.fscript.simulation.Trace;
import org.objectweb.util.monolog.api.Logger;

/**
 * This class provides transactional abilities to the engine.
 * If a transaction manager is available, it is
 * automatically used to "wrap" each execution request (i.e. <code>execute()</code> or
 * <code>invoke()</code> calls) into a separate transaction.
 * 
 * @author Pierre-Charles David
 */
public abstract class TransactionalBackendDriver {
    /* Some constants used for fractal bindings */
    public static final String SIMULATOR_ITF_NAME = "simulator";    
    public static final String TX_MANAGER_ITF_NAME = "transaction-manager";    
    public static final String LOGGER_ITF_NAME = "logger";    
    
    /** Used if available to wrap the executions in transactions. */
    protected UserTransaction transactionManager;

    /** An optional simulator. */
    protected Simulator simulator;

    /** Used to log various internal message. */
    protected Logger logger;

    
    protected void startTransaction() throws FScriptException {
        if (transactionManager == null) {
            logger.log(WARN, "No transaction manager available");
            return;
        }
        logger.log(INFO, "Starting transaction.");
        try {
            transactionManager.begin();
        } catch (NotSupportedException e) {
            throw new FScriptException("Could not start the transaction.", e);
        } catch (SystemException e) {
            throw new FScriptException("Could not start the transaction.", e);
        }
    }

    protected void commitTransaction() throws FScriptException {
        if (transactionManager == null) {
            // No need to issue a warning if there is no transaction manager, as one has
            // already been issued when starting the transaction.
            return;
        }
        logger.log(INFO, "Commiting transaction.");
        try {
            transactionManager.commit();
        } catch (RollbackException e) {
            throw new FScriptException("Commit failed: transaction rolled back.", e);
        } catch (HeuristicRollbackException e) {
            throw new FScriptException("Commit failed: transaction rolled back.", e);
        } catch (HeuristicMixedException e) {
            throw new FScriptException("Commit failed: transaction partially rolled back.", e);
        } catch (SystemException e) {
            throw new FScriptException("Commit failed: internal error in the TM.", e);
        }
        logger.log(INFO, "Transaction commited.");
    }

    protected void rollbackTransaction(Exception cause) throws FScriptException {
        if (transactionManager == null) {
            // No need to issue a warning if there is no transaction manager, as one has
            // already been issued when starting the transaction.
            return;
        }
        try {
            logger.log(INFO, "Rollbacking transaction.");
            transactionManager.rollback();
        } catch (SystemException se) {
            logger.log(ERROR, "Error during rollback.", se);
            throw new FScriptException("Unable to rollback reconfiguration.", se);
        }
    }

    protected void simulateInvocation(String procName, Object[] args, Map<String, Object> context)
            throws ScriptExecutionError {
        logger.log(INFO, "Starting simulation of " + procName + "().");
        try {
            @SuppressWarnings("unused")
            Trace trace = simulator.simulate(procName, args, context);
            logger.log(INFO, "Simulation OK.");
        } catch (ScriptExecutionError see) {
            logger.log(WARN, "Simulation failed.");
            throw see;
        }
    }

}
