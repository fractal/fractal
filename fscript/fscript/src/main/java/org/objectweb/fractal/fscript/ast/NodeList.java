/*
 * Copyright (c) 2011 INRIA-SARDES
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 */
package org.objectweb.fractal.fscript.ast;

import com.google.common.base.Joiner;

import java.io.IOException;
import java.util.List;

/**
 * @author Loris Bouzonnet
 */
public class NodeList extends ASTNode {

    private java.util.List<ASTNode> nodes;
    /**
     * Creates a new <code>NodeList</code>.
     *
     * @param location the location of the corresponding fragment in the source code. May be null if the
     *                 location is unknown or if there was no actual source code (e.g. the AST is
     *                 constructed programmatically).
     * @param nodes    the AST nodes representing the content of the list.
     */
    public NodeList(SourceLocation location, final java.util.List<ASTNode> nodes) {
        super(location);
        this.nodes = nodes;
    }

    @Override
    public void accept(final ASTVisitor visitor) {
        visitor.visitList(this);
    }

    @Override
    protected void toString(final Appendable appendable) throws IOException {
        appendable.append("[");
        Joiner.on(",").join(appendable, nodes);
        appendable.append("]");
    }

    public List<ASTNode> getValues() {
        return nodes;
    }
}
