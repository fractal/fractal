/***
 * OW2 FScript
 * Copyright (C) 2010 INRIA, University of Lille1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s):
 */

package org.objectweb.fractal.fscript.jsr223;

import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;

/**
 * Proxy used to be able to deal with several script factories with the same extension (".fscript"). 
 */
public class FScriptEngineFactoryProxy implements ScriptEngineFactory {
    /** The name of the property used to get Factory class */
    public static final String SCRIPT_ENGINE_FACTORY_PROPERTY_NAME = "fscript-factory";
    
    /* The Script Engine Factory to use */
    private ScriptEngineFactory factory = null;
    
    /**
     * Default constructor.
     */
    public FScriptEngineFactoryProxy() throws Exception {
        // Get the factory
        String className = System.getProperty(SCRIPT_ENGINE_FACTORY_PROPERTY_NAME);
        if (className != null) {
            Class<?> clazz = Class.forName(className);
            factory = (ScriptEngineFactory) clazz.newInstance();
        } else
            factory = new FScriptEngineFactory(); 
    }
    
    public String getEngineName() {
        return factory.getEngineName();
    }

    public String getEngineVersion() {
        return factory.getEngineVersion();
    }

    public List<String> getExtensions() {
        return factory.getExtensions();
    }

    public String getLanguageName() {
        return factory.getLanguageName();
    }

    public String getLanguageVersion() {
        return factory.getLanguageVersion();
    }   

    public String getMethodCallSyntax(String obj, String m, String... args) {
        return factory.getMethodCallSyntax(obj, m, args);
    }

    public List<String> getMimeTypes() {
        return factory.getMimeTypes();
    }

    public List<String> getNames() {
        return factory.getNames();
    }

    public String getOutputStatement(String toDisplay) {
        return factory.getOutputStatement(toDisplay);
    }

    public Object getParameter(String key) {
        return factory.getParameter(key);
    }

    public String getProgram(String... statements) {
        return factory.getProgram(statements);
    }

    public ScriptEngine getScriptEngine() {
        return factory.getScriptEngine();
    }

}
