/***
 * OW2 FScript
 * Copyright (C) 2010 INRIA, University of Lille1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s):
 */

package org.objectweb.fractal.fscript.jsr223;

import static org.objectweb.util.monolog.api.BasicLevel.ERROR;
import static org.objectweb.util.monolog.api.BasicLevel.INFO;

import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptException;
import javax.script.SimpleBindings;
import javax.script.SimpleScriptContext;
import javax.transaction.UserTransaction;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.fscript.Executor;
import org.objectweb.fractal.fscript.FScriptException;
import org.objectweb.fractal.fscript.FragmentLoader;
import org.objectweb.fractal.fscript.InvalidScriptException;
import org.objectweb.fractal.fscript.ScriptExecutionError;
import org.objectweb.fractal.fscript.ScriptLoader;
import org.objectweb.fractal.fscript.TransactionalBackendDriver;
import org.objectweb.fractal.fscript.simulation.Simulator;
import org.objectweb.util.monolog.api.Logger;

import com.google.common.base.Preconditions;

public class FScriptEngine extends TransactionalBackendDriver
  implements InvocableScriptEngine, FactoryManager, BindingController, InvocationHandler {
    /* Some constants used for fractal bindings */
    public static final String FRAGMENT_LOADER_ITF_NAME = "fragment-loader";
    public static final String EXECUTOR_ITF_NAME = "executor";
    public static final String LOADER_ITF_NAME = "script-loader";
    
    /** This is used by the <code>execute()</code> methods to create temporary procedures
     * out of code fragments supplied by the user. */
    private FragmentLoader fragmentLoader;

    /** The actual execution engine, typically a simple interpreter. */
    private Executor executor;
    
    /** The loader allowing to load procedures into the engine */
    private ScriptLoader loader;

	/** The engine factory */
	private ScriptEngineFactory factory;

	/** The engine context */
	private ScriptContext context;

    /**
     * Default constructor.
     */
    public FScriptEngine() {
        this.context = new SimpleScriptContext();
        this.context.setBindings(new SimpleBindings(), ScriptContext.GLOBAL_SCOPE);
    }
    
    // ========================================================================
    // Private methods
    // ========================================================================

    protected Object evaluate(Reader reader) throws ScriptException {
        Preconditions.checkNotNull(reader);
        String tempProcName;
        try {
            tempProcName = fragmentLoader.loadFragment(reader);
        } catch (InvalidScriptException e) {
            throw new ScriptException(e);
        }
        try {
            return invokeFunction(tempProcName);
        } catch (NoSuchMethodException e) {
            throw new ScriptException(e);
        } finally {
            fragmentLoader.unloadFragment(tempProcName);
        }
    }
    
    protected Map<String, Object> getFullContext() {
        Bindings locals = this.context.getBindings(ScriptContext.ENGINE_SCOPE);
        locals.putAll( this.context.getBindings(ScriptContext.GLOBAL_SCOPE) );
        return locals;
    }

    // ========================================================================
	// Implementation of the ScriptEngine API
	// ========================================================================

	public Object eval(String script) throws ScriptException {
	    return evaluate( new StringReader(script) );
	}

	public Object eval(String script, ScriptContext context)
	throws ScriptException {
	    setContext(context);
	    return evaluate( new StringReader(script) );
	}

    public Object eval(String script, Bindings bindings) throws ScriptException {
        if (script == null || bindings == null) {
            throw new NullPointerException("either script or bindings is null");
        }
        getContext().setBindings(bindings, ScriptContext.ENGINE_SCOPE);
        return evaluate( new StringReader(script) );
    }

	public Object eval(Reader reader) throws ScriptException {
	    try {
            return load(reader);
        } catch (InvalidScriptException e) {
            throw new ScriptException(e);
        }
	}

	public Object eval(Reader reader, ScriptContext context)
			throws ScriptException {
		setContext(context);
		return eval(reader);
	}

	public Object eval(Reader reader, Bindings bindings) throws ScriptException {
		getContext().setBindings(bindings, ScriptContext.ENGINE_SCOPE);
		return eval(reader);
	}

	public void put(String key, Object value) {
		getBindings(ScriptContext.ENGINE_SCOPE).put(key, value);	
	}

	public Object get(String key) {
		return getBindings(ScriptContext.ENGINE_SCOPE).get(key);
	}
	
	// ========================================================================
	// Implementation of the Invocable API
	// ========================================================================

	public ScriptEngineFactory getFactory() {
		return factory;
	}

	public Bindings createBindings() {
		return new SimpleBindings();
	}

	public Bindings getBindings(int scope) {
		return this.context.getBindings(scope);
	}

	public void setBindings(Bindings bindings, int scope) {
		this.context.setBindings(bindings, scope);
	}

	public ScriptContext getContext() {
		return this.context;
	}

	public void setContext(ScriptContext context) {
		this.context = context;
	}

	public <T> T getInterface(Class<T> clasz) {
	    T proxy = null;
	    
	    proxy = (T) java.lang.reflect.Proxy.newProxyInstance(
                        clasz.getClassLoader(),
                        new Class<?>[] { clasz },
                        this );	    
		return proxy;
	}

	public <T> T getInterface(Object thiz, Class<T> clasz) {
		// TODO Auto-generated method stub
		return null;
	}

	public Object invokeFunction(String name, Object... args)
			throws ScriptException, NoSuchMethodException {
        Preconditions.checkNotNull(name, "Missing procedure name.");
        Map<String, Object> ctx = getFullContext();
        if (simulator != null) {
            try {
                simulateInvocation(name, args, ctx);
            } catch (ScriptExecutionError e) {
                throw new ScriptException(e);
            }
        }
        logger.log(INFO, "Starting execution of " + name + "().");
        try {
            startTransaction();
        } catch (FScriptException fse) {
            throw new ScriptException(fse);
        }
        try {
            Object result = executor.invoke(name, args, ctx);
            commitTransaction();
            logger.log(INFO, "Execution finished successfuly.");
            return result;
        } catch (ScriptExecutionError e) {
            FScriptException excpt;
            
            logger.log(ERROR, "Reconfiguration failed.");
            try {
                rollbackTransaction(e);
            } catch (FScriptException fse) {
                throw new ScriptException(fse);
            }
            if (transactionManager != null)
                excpt = new FScriptException("Transaction rolled back.", e);
            else
                excpt = new FScriptException("Reconfiguration failed.", e);
            throw new ScriptException(excpt);
        } catch (FScriptException e) {
            logger.log(ERROR, "Commit failed.", e);
            throw new ScriptException(e);
        } catch (Exception e) {
            FScriptException excpt;
            
            logger.log(ERROR, "Unexpected error during execution: " + e.getMessage());
            try {
                rollbackTransaction(e);
            } catch (FScriptException fse) {
                throw new ScriptException(fse);
            }
            if (transactionManager != null)
                excpt = new FScriptException("Transaction rolled back.", e);
            else
                excpt = new FScriptException("Reconfiguration failed.", e);
            throw new ScriptException(excpt);
        }
	}

	public Object invokeMethod(Object thiz, String name, Object... args)
			throws ScriptException, NoSuchMethodException {
	    // TODO
		// cast object in node
	    // get node name
	    // $node-name.name(args)
		return null;
	}

    // ========================================================================
    // Implementation of the FactoryManager API
    // ========================================================================

	/**
	 * Give a way to fix the factory of this engine (we need a default
	 * empty constructor to be able to instantiate this fractal component).
	 * 
	 * @param factory The ScriptEngineFactory to which this ScriptEngine belongs.
	 */
	public void setFactory(ScriptEngineFactory factory) {
	    this.factory = factory;
	}
	
    // ========================================================================
    // Implementation of the InvocationHandler API
    // ========================================================================

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (args == null) args = new Object[0];
        return invokeFunction(method.getName(), args);
    }

    // ========================================================================
    // Implementation of the ScriptLoader API
    // ========================================================================

    public Set<String> load(Reader source) throws InvalidScriptException {
        return loader.load(source);
    }

    public Set<String> load(String source) throws InvalidScriptException {
        return loader.load(source);
    }
    
    // ========================================================================
    // Fractal bindings
    // ========================================================================

    public String[] listFc() {
        return new String[] { EXECUTOR_ITF_NAME, SIMULATOR_ITF_NAME, FRAGMENT_LOADER_ITF_NAME, TX_MANAGER_ITF_NAME, LOGGER_ITF_NAME, LOADER_ITF_NAME };
    }

    public void bindFc(String itfName, Object srvItf) throws NoSuchInterfaceException,
    IllegalBindingException, IllegalLifeCycleException {
        if (FRAGMENT_LOADER_ITF_NAME.equals(itfName)) {
            this.fragmentLoader = (FragmentLoader) srvItf;
        } else if (EXECUTOR_ITF_NAME.equals(itfName)) {
            this.executor = (Executor) srvItf;
        } else if (SIMULATOR_ITF_NAME.equals(itfName)) {
            this.simulator = (Simulator) srvItf;
        } else if (TX_MANAGER_ITF_NAME.equals(itfName)) {
            this.transactionManager = (UserTransaction) srvItf;
        } else if (LOGGER_ITF_NAME.equals(itfName)) {
            this.logger = (Logger) srvItf;
        } else if (LOADER_ITF_NAME.equals(itfName)) {
            this.loader = (ScriptLoader) srvItf;
        } else {
            throw new NoSuchInterfaceException(itfName);
        }
    }

    public Object lookupFc(String itfName) throws NoSuchInterfaceException {
        if (FRAGMENT_LOADER_ITF_NAME.equals(itfName)) {
            return this.fragmentLoader;
        } else if (EXECUTOR_ITF_NAME.equals(itfName)) {
            return this.executor;
        } else if (SIMULATOR_ITF_NAME.equals(itfName)) {
            return this.simulator;
        } else if (TX_MANAGER_ITF_NAME.equals(itfName)) {
            return this.transactionManager;
        } else if (LOGGER_ITF_NAME.equals(itfName)) {
            return this.logger;
        } else if (LOADER_ITF_NAME.equals(itfName)) {
            return this.loader;
        } else {
            throw new NoSuchInterfaceException(itfName);
        }
    }

    public void unbindFc(String itfName) throws NoSuchInterfaceException, IllegalBindingException,
    IllegalLifeCycleException {
        if (FRAGMENT_LOADER_ITF_NAME.equals(itfName)) {
            this.fragmentLoader = null;
        } else if (EXECUTOR_ITF_NAME.equals(itfName)) {
            this.executor = null;
        } else if (SIMULATOR_ITF_NAME.equals(itfName)) {
            this.simulator = null;
        } else if (TX_MANAGER_ITF_NAME.equals(itfName)) {
            this.transactionManager = null;
        } else if (LOGGER_ITF_NAME.equals(itfName)) {
            this.logger = null;
        } else if (LOADER_ITF_NAME.equals(itfName)) {
            this.loader = null;
        } else {
            throw new NoSuchInterfaceException(itfName);
        }
    }
}
