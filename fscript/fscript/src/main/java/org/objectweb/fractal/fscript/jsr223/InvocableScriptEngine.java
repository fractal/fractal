/***
 * OW2 FScript
 * Copyright (C) 2010 INRIA, University of Lille1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s):
 */
package org.objectweb.fractal.fscript.jsr223;

import javax.script.Invocable;
import javax.script.ScriptEngine;

import org.objectweb.fractal.fscript.ScriptLoader;

/** 
 * Interface used to return a reference to a JSR 223 engine implementing
 * both Invocable and ScriptEngine API.
 */
public interface InvocableScriptEngine extends Invocable, ScriptEngine, ScriptLoader { }
