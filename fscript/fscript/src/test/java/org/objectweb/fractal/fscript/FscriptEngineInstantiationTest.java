/*
 * Copyright (c) 2009 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Christophe Demarey
 */
package org.objectweb.fractal.fscript;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.objectweb.fractal.api.Component;

public class FscriptEngineInstantiationTest {
  @Test
  public void buildFScripteEngine() throws Exception {
      Component fscript = null;
      
      fscript = FScript.newEngineFromAdl();
      assertNotNull(fscript);
      
      fscript = null;
      fscript = FScript.newEngine();
      assertNotNull(fscript);
  }

}
