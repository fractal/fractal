/***
 * OW2 FScript
 * Copyright (C) 2010 INRIA, University of Lille1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s):
 */

package org.objectweb.fractal.fscript.jsr223;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import javax.script.Bindings;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;

import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.fscript.FScriptEngine;

/**
 * Test class used to valid JSR 223 FScript engine. 
 */
public class FScriptEngineTest {
    
    private static ScriptEngine engine;

    @BeforeClass
    public static void getEngine() {
        ScriptEngineManager mgr = new ScriptEngineManager();
        
        engine = mgr.getEngineByExtension("fscript");
    }
    
    @Test
    public void listEngines() throws Exception {
        ScriptEngineManager mgr = new ScriptEngineManager();
        List<ScriptEngineFactory> factories = 
            mgr.getEngineFactories();
        for (ScriptEngineFactory factory: factories) {
          System.out.println("ScriptEngineFactory Info");
          String engName = factory.getEngineName();
          String engVersion = factory.getEngineVersion();
          String langName = factory.getLanguageName();
          String langVersion = factory.getLanguageVersion();
          System.out.printf("\tScript Engine: %s (%s)\n", 
              engName, engVersion);
          List<String> engNames = factory.getNames();
          for(String name: engNames) {
            System.out.printf("\tEngine Alias: %s\n", name);
          }
          System.out.printf("\tLanguage: %s (%s)\n", 
              langName, langVersion);
        }            
    }

	@Test
    public void testEval() throws Exception {
        engine.eval( "echo('FScript JSR223 engine');" );
        assertEquals( Boolean.TRUE, engine.eval("ends-with(\"foobar\", \"bar\")") );
    }

    @Test
    public void testEvalWithBindings() throws Exception {
        Bindings bindings = engine.createBindings();
        bindings.put("value", "bar");
        
        assertEquals( Boolean.TRUE, engine.eval("ends-with(\"foobar\", $value)", bindings) );
        
        bindings = engine.createBindings();
        assertNull( engine.eval("$value", bindings) );
    }
    
    @Test
    public void testGetInterface() throws Exception {
        // register script procedures
        InputStream script = FScriptEngine.class.getResourceAsStream("comanche.fscript");
        engine.eval( new InputStreamReader(script) );        
        
        // call a procedure
        Comanche c = ((Invocable) engine).getInterface( Comanche.class );
        c.buildComanche();
    }
    
}
