/*
 * Copyright (c) 2011 INRIA-SARDES
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 */
package org.objectweb.fractal.fscript;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.api.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Loris Bouzonnet
 */
public class ListMapTest {

    private static FScriptEngine engine;

    @BeforeClass
    public static void setUp() throws Exception {
        Component fscript = FScript.newEngine();
        engine = FScript.getFScriptEngine(fscript);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void list() throws FScriptException {
        List<?> list = Arrays.asList("a",1.0,false,"1","aa",22.0);
        Assert.assertEquals(list,engine.execute("[\"a\",1,false(),\"1\",\"aa\",22]"));
    }


    @SuppressWarnings("unchecked")
    @Test
    public void map() throws FScriptException {
        Map<Object,Object> list = new HashMap<Object, Object>();
        list.put("a",1.0);
        list.put(33.0,true);
        Map<String, String> map = new HashMap<String, String>();
        map.put("map", "pam");
        list.put("bb", Arrays.asList(map));
        Assert.assertEquals(list,engine.execute("{\"a\"=1,33=true(),\"bb\"=[{\"map\"=\"pam\"}]}"));
    }

}
