/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.fractal.bpc.adl;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.bpc.ProtocolController;


/**
 * An implementation of the {@link ProtocolBuilder} interface for Fractal. 
 * This implementation uses the Fractal API to assign a protocol to a component. 
 */

public class FractalProtocolBuilder implements ProtocolBuilder {
 
  /**
   * Assigns a protocol to a component.
   * Obtains a protocol controller of a component and uses it to assign a protocol.
   */
  public void setProtocol (Object component, String protocol, int order)
  {   
    Component comp = (Component)component;
    try {
      ProtocolController protoCont =
        (ProtocolController)comp.getFcInterface("protocol-controller");
      protoCont.setFcProtocol(protocol);
      protoCont.setFcOrder(order);
    } catch (NoSuchInterfaceException exc) {
      System.out.println("Warning: Can't assign a protocol to a component. The component does not have a protocol controller.");
    }
  }
}
