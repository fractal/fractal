/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Pavel Parizek <parizek@nenya.ms.mff.cuni.cz>
 * 
 */
package org.objectweb.fractal.bpc.adl;

import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.bpc.EnvironmentController;


/**
 * An implementation of the {@link EnvironmentBuilder} interface for Fractal. 
 * This implementation uses the Fractal API to assign an environment to a component. 
 */

public class FractalEnvironmentBuilder implements EnvironmentBuilder {
 
  /**
   * Assigns environment-related information to a component.
   * Obtains an environment controller of a component and uses it to assign the information.
   */
  public void setEnvironment (Object component, String valueSetsClass, String userStubCode, Map userDriversCode, String protocol, Map itfStubs)
  {   
    Component comp = (Component) component;
    try 
    {
      EnvironmentController envCont = (EnvironmentController) comp.getFcInterface("environment-controller");
      
      envCont.setFcValueSetsClass(valueSetsClass);
      envCont.setFcUserStubCode(userStubCode);
      envCont.setFcUserDriversCode(userDriversCode);
	  envCont.setFcProtocol(protocol);
	  envCont.setFcItfStubs(itfStubs);
    } 
    catch (NoSuchInterfaceException exc) {
      System.out.println("Warning: Can't assign an environment to a component. The component does not have an environment controller.");
    }
  }
}
