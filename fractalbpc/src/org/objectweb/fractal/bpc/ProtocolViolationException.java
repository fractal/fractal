/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Vladimir Mencl <mencl@nenya.ms.mff.cuni.cz>
 *
 * Created on Apr 26, 2005
 *
 */

package org.objectweb.fractal.bpc;

/**
 * This runtime exception is thrown when a protocol violation is detected.
 * 
 * This exception may be thrown by RuntimeCheckController in response to
 * either an <em>request</em> (method entry) or an <em>response</em> (method exit) event.  
 * In case of method entry, the exception prevents the call from being
 * actually performed; in case of method exit, the exception merely reports
 * the protocol violation detected.
 *     
 * @author Vladimir Mencl
 *
 */
public class ProtocolViolationException extends RuntimeException {

	/**
     * for serialization
     */
    private static final long serialVersionUID = -5240542843558094103L;

    public ProtocolViolationException() {
		super();
	}

	public ProtocolViolationException(String message) {
		super(message);
	}

	public ProtocolViolationException(Throwable cause) {
		super(cause);
	}

	public ProtocolViolationException(String message, Throwable cause) {
		super(message, cause);
	}
}
