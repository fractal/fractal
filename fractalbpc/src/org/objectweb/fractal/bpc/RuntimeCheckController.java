/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Vladimir Mencl <mencl@nenya.ms.mff.cuni.cz>
 *
 * Created on Dec 6, 2004
 *
 */

package org.objectweb.fractal.bpc;

/**
 * This controller provides methods for monitoring method call events on
 * intercepted component interfaces.<p>
 * 
 * The enterMethod and leaveMethod methods are called by interceptors generated
 * by {@link org.objectweb.fractal.bpc.julia.RuntimeCheckInterceptorCodeGenerator}.
 * These interceptors implement the
 * {@link org.objectweb.fractal.bpc.RuntimeCheckInterceptor} interface.<p>
 * 
 * The responsibility of this controller is to set at least the itfInstanceName
 * and itfIsClient properties of these interceptors. The current only
 * implementation,
 * {@link org.objectweb.fractal.bpc.julia.BasicRuntimeCheckControllerMixin}, sets
 * these properties in the
 * {@link org.objectweb.fractal.bpc.julia.BasicRuntimeCheckControllerMixin#initFcController(InitializationContext)}
 * method.<p>
 * 
 * The controller provides the monitored data via the
 * {@link #getFcCurrentMethods()} and {@link #getFcMethodHistory()} methods.<p>
 * 
 * In the future, the interface of this controller may be extended to interact
 * with the runtime protocol checker backend.
 * 
 * @author Vladimir Mencl
 *  
 */

public interface RuntimeCheckController {
	
	/**
	 * called to record an enter-method (<em>request</em>) event on a component interface.
	 * 
	 * @param itfName name of the interface where the event occurred
	 * @param methodName name of the method called
	 * @param isClient whether the interface is a client (<em>required</em>) interface
	 * @param params optionally, the method parameters wrapped into an object array. May be null; the current interceptor implementation always passes null.
	 */
	public void enterFcMethod(String itfName, String methodName,
			boolean isClient, Object params[]);

	/**
	 * called to record an leave-method (<em>response</em>) event on a component interface.
	 * 
	 * @param itfName name of the interface where the event occurred
	 * @param methodName name of the method called
	 * @param isClient whether the interface is a client (<em>required</em>) interface
	 * @param params optionally, the method parameters wrapped into an object array. May be null; the current interceptor implementation always passes null.
	 */
	public void leaveFcMethod(String itfName, String methodName,
			boolean isClient, Object params[]);

	/**
	 * returns the list of all currently open method calls on the component's interfaces.
	 * 
	 * The list includes all methods for which an enterMethod call was processed by the interceptor, but no leaveMethod call has been processed yet.  
	 * @see org.objectweb.fractal.bpc.jmx.RuntimeCheckMonitor#getCurrentMethods()
	 */
	public String[] getFcCurrentMethods();
	
	/**
	 * returns the list of all method call events processed on the component interfaces.
	 * 
	 * The list includes all method call events, captured by the interceptor and
	 * recorded via the enterMethod and leaveMethod calls.  
	 * @see org.objectweb.fractal.bpc.jmx.RuntimeCheckMonitor#getMethodHistory()
	 */
	public String[] getFcMethodHistory();
	
	/**
	 * initializes the runtime checker with a behavior protocol
	 * 
	 * @param protocol the behavior protocol to be used by the runtime checker on this component.
	 */
	public void startFcRtcheck(String protocol);

	/**
	 * 
	 */
	public void stopFcRtcheck();
}
