/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jiri Adamek <adamek@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.fractal.bpc.staticchecker;

/**
 * This class stores the information about an interface instance. <br>
 * <br>
 * <em>IMPORTANT:</em> If an instance of this class describes an interface of
 * the frame instance of the super-component (the one "containing" the
 * architecture), {@link #frameName} has to be set to <code>null</code>.
 */
public class InterfaceInstance {

	public String frameName;
	
	public String interfaceName;

	public boolean isProvided;

	public String methodNames[];

	public InterfaceInstance(String componentName, String interfaceName,
			boolean isProvided, String methodNames[]) {
		this.frameName = componentName;
		this.interfaceName = interfaceName;
		this.isProvided = isProvided;
		this.methodNames = methodNames;
	}

	public boolean isMethodName(String name) {
		for (int i = 0; i < methodNames.length; i++)
			if (name.equals(methodNames[i]))
				return true;
		return false;
	}
	
	/**
	 * This method exists only to implement a temporary hack solving the problem with
	 * method name exstensions of the form _i, where i is an integer. 
	 */
	public void addMethodName(String name) {
		String newMethodNames[] = new String[methodNames.length + 1];
		for (int i = 0; i < methodNames.length; i++)
			newMethodNames[i] = methodNames[i];
		newMethodNames[methodNames.length] = name;
		methodNames = newMethodNames;
	}

}
