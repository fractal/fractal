/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jiri Adamek <adamek@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.fractal.bpc.staticchecker.test;

import org.objectweb.fractal.bpc.staticchecker.*;
import org.objectweb.fractal.bpc.staticchecker.preprocessor.*;

/**
 * This is a testing main class.
 */
public class Main4 {

	public static void main(String params[]) {

		Debug.setLevel(2);

		/* method name lists */

		String ITokenCallbackMethods[] = {"TokenInvalidated"};
		String ITokenMethods[] = {"InvalidateAndSave", "SetValidity"};
		String ITimerCallbackMethods[] = {"Timeout"};
		String ITimerMethods[] = {"SetTimeout", "CancelTimeouts"};
		String ICustomCallbackMethods[] = {"InvalidatingToken"};
		String ILifetimeControllerMethods[] = {"Start"};
		
		/* super component */

		InterfaceInstance ITokenCallback = new InterfaceInstance(null, "ITokenCallback",
				false /* required */, ITokenCallbackMethods);
		InterfaceInstance IToken = new InterfaceInstance(null, "IToken",
				true /* provided */, ITokenMethods);
		InterfaceInstance ITimerCallback = new InterfaceInstance(null, "ITimerCallback",
				true /* provided */, ITimerCallbackMethods);
		InterfaceInstance ITimer = new InterfaceInstance(null, "ITimer",
				false /* required */, ITimerMethods);
		InterfaceInstance ICustomCallback = new InterfaceInstance(null, "ICustomCallback",
				false /* required */, ICustomCallbackMethods);
		InterfaceInstance ILifetimeController = new InterfaceInstance(null, "ILifetimeController",
				true /* provided */, ILifetimeControllerMethods);

		
		String _protocol = "(?ILifetimeController.Start^ ; !ITimer.SetTimeout_1^ ; [?ITimer.SetTimeout_1$, !ILifetimeController.Start$]); (?IToken.InvalidateAndSave {(!ICustomCallback.InvalidatingToken_1 + NULL);!ITimer.CancelTimeouts;!ITokenCallback.TokenInvalidated_1}* | ?ITimerCallback.Timeout { (!ICustomCallback.InvalidatingToken_2 + NULL); !ITokenCallback.TokenInvalidated_2 }* )";

		
		FrameInstance frame = new FrameInstance("ValidityChecker",
				new InterfaceInstance[] {ITokenCallback, IToken, ITimerCallback, ITimer, ICustomCallback, ILifetimeController},
				_protocol, 0);

		
		Parser parser = new Parser();
		try {
			Node n = parser.parse(frame);
		} catch (ParseErrorException e) {
			System.out.println(e.getMessage());
		}
		
	}

}
