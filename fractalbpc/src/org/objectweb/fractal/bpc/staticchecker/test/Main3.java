/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jiri Adamek <adamek@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.fractal.bpc.staticchecker.test;

import org.objectweb.fractal.bpc.staticchecker.*;
import org.objectweb.fractal.bpc.staticchecker.preprocessor.Debug;

/**
 * This is a testing main class.
 */
public class Main3 {

	public static void main(String params[]) {

		Debug.setLevel(2);

		/* method name lists */

		String methodNames1[] = { "x" };

		String methodNames2[] = { "out" };

		/* super component */

		InterfaceInstance _N = new InterfaceInstance(null, "N",
				false /* required */, methodNames2);

		String _protocol = "!N.out*";

		FrameInstance superComponent = new FrameInstance(null,
				new InterfaceInstance[] { _N }, _protocol, 0);

		/* "A" component */

		InterfaceInstance A_I = new InterfaceInstance("A", "I",
				false /* required */, methodNames1);

		String A_protocol = "NULL";

		FrameInstance A = new FrameInstance("A",
				new InterfaceInstance[] { A_I }, A_protocol, 1);

		/* "B" component */

		InterfaceInstance B_J = new InterfaceInstance("B", "J",
				false /* required */, methodNames1);

		String B_protocol = "!J.x*";

		FrameInstance B = new FrameInstance("B",
				new InterfaceInstance[] { B_J }, B_protocol, 2);

		/* "C" component */

		InterfaceInstance C_K = new InterfaceInstance("C", "K",
				true /* provided */, methodNames1);

		String C_protocol = "?K.x*";

		FrameInstance C = new FrameInstance("C",
				new InterfaceInstance[] { C_K }, C_protocol, 3);

		/* "C" component */

		InterfaceInstance D_L = new InterfaceInstance("D", "L",
				true /* provided */, methodNames1);
		InterfaceInstance D_M = new InterfaceInstance("D", "M",
				false /* required */, methodNames2);

		String D_protocol = "?L.x{!M.out}*";

		FrameInstance D = new FrameInstance("D", new InterfaceInstance[] { D_L,
				D_M }, D_protocol, 4);

		/* Subcomponents */

		FrameInstance[] subcomponents = new FrameInstance[] { A, B, C, D };

		/* Bindings */

		Binding[] bindings = new Binding[] { new Binding("A", "I", "C", "K"),
				new Binding("B", "J", "C", "K"),
				//new Binding("B", "J", "D", "L"),
				new Binding("D", "M", null, "N") };

		/* Check it ... */

		CheckingResult result = Checker.check(superComponent, subcomponents,
				bindings);

		System.out.println(result.toString() + "\n");
	}

}
