/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jiri Adamek <adamek@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.fractal.bpc.staticchecker.test;

import org.objectweb.fractal.bpc.staticchecker.preprocessor.Parser;


/**
 * This is a testing main class.
 */
public class Main5 {

	static String[] testNames = {
			"abc",
			"xyz_1",
			"q_35",
			"_36",
			"_",
			"v2",
			"45",
			"q_",
			"_q"
	};
	
	public static void main(String params[]) {

		for (int i = 0; i < testNames.length; i++)
			System.out.println(testNames[i] + ": " + (Parser.checkMethodNameExtension(testNames[i]) ? "true" : "false"));
	}

}
