/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jiri Adamek <adamek@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.fractal.bpc.staticchecker.test;

import org.objectweb.fractal.bpc.staticchecker.*;
import org.objectweb.fractal.bpc.staticchecker.preprocessor.Debug;

/**
 * This is a testing main class.
 */
public class Main8 {

	public static void main(String params[]) {

		Debug.setLevel(2);
		
		/* super component */
		
		FrameInstance superComponent = new FrameInstance(null, new InterfaceInstance[0], "NULL", 0);
		
		/* method names for interfaces of the type "Log" */
		
		String methodNames[] = {"open", "InvalidateAndSave", "close"};
		
		/* "client" component */
		
		InterfaceInstance client_log = new InterfaceInstance("client", "log", false /* required */, methodNames);
		
		String client_protocol = "!log.open_1;!log.InvalidateAndSave_1;!log.InvalidateAndSave_2;!log.close_1";
		
		FrameInstance client = new FrameInstance("client", new InterfaceInstance[] {client_log}, client_protocol, 1);
		
		/* "logger" component */
		
		InterfaceInstance logger_log = new InterfaceInstance("logger", "log", true /* provided */, methodNames);
		
		String logger_protocol = "?log.open_1;?log.InvalidateAndSave_1*;?log.close_1";
		
		FrameInstance logger = new FrameInstance("logger", new InterfaceInstance[] {logger_log}, logger_protocol, 2);
		
		/* Bindings */
		
		Binding[] bindings = new Binding[] {
			new Binding("client", "log", "logger", "log")	
		};
		
		/* Check it ... */
		
		CheckingResult result = Checker.check(superComponent, new FrameInstance[] {client, logger}, bindings);
		
		System.out.println(result.toString() + "\n");
	}
	
}
