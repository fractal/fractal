/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jiri Adamek <adamek@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.fractal.bpc.staticchecker;

/**
 * An instance of this class stores a result of the checking process.
 */
public class CheckingResult {

	public CheckingResult(int errorType, String errorDescription) {
		this.errorType = errorType;
		this.errorDescription = errorDescription;
	}

	public CheckingResult(CheckingResult result) {
		this.errorType = result.errorType;
		this.errorDescription = result.errorDescription;
	}

	public CheckingResult() {
		errorType = ERR_OK;
		errorDescription = "";
	}

	public String toString() {
		switch (errorType) {
		case ERR_OK:
			return "ok";
		case ERR_BADACTIVITY:
			return "bad activity \n" + errorDescription;
		case ERR_NOACTIVITY:
			return "no activity \n" + errorDescription;
		case ERR_DIVERGENCE:
			return "divergence \n" + errorDescription;
		case ERR_NOT_CHECKED:
			return "not checked \n" + errorDescription;
		default:
			/* add assert(false); */
			return null;
		}
	}

	/** 
	 * Returns the type of the error. 
	 * The return value is one of the following:
	 * {@link #ERR_OK}, {@link #ERR_BADACTIVITY}, {@link #ERR_NOACTIVITY},
	 * {@link #ERR_DIVERGENCE}, {@link #ERR_NOT_CHECKED}.
	 */
	public int getErrorType() {
		return errorType;
	}

	/** 
	 * Returns additional description of the error.
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	public int errorType;

	public String errorDescription;

	/** No error occured during the checking process. */
	static public final int ERR_OK = 0;

	/** A bad activity error occured during the checking process. */
	static public final int ERR_BADACTIVITY = 1;

	/** A no activity error occured during the checking process. */
	static public final int ERR_NOACTIVITY = 2;

	/** A divergence error occured during the checking process. */
	static public final int ERR_DIVERGENCE = 3;

	/** The protocols were not checked, as they couldn't be parsed. */
	static public final int ERR_NOT_CHECKED = 4;

}
