/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jiri Adamek <adamek@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.fractal.bpc.staticchecker;

/**
 * This class stores the information about a binding.
 * In more detail, it stores the names of the frame instances and
 * the interface instances on both "ends" of the binding.
 * The direction of the binding is always from the
 * frame/interface instance number 1 to the frame/interface instance number 2.
 * I.e., the interface identified by the pair ({@link #frameInstance1},
 * {@link #interfaceInstance1}) is a required one, while the interface
 * identified by the pair ({@link #frameInstance2}, {@link #interfaceInstance2})
 * is a provided one. <br>
 * <br>
 * <em>IMPORTANT:</em> If one of the frame instances participating on the
 * binding is the frame instance of the super-component (the one "containing"
 * the architecture), than the corresponding field for the frame instance name (
 * {@link #frameInstance1} or {@link #frameInstance2}) has to be set to <code>null</code>.
 */
public class Binding {

	public Binding(String frameInstance1, String interfaceInstance1,
			String frameInstance2, String interfaceInstance2) {
		this.frameInstance1 = frameInstance1;
		this.interfaceInstance1 = interfaceInstance1;
		this.frameInstance2 = frameInstance2;
		this.interfaceInstance2 = interfaceInstance2;
	}

	public String frameInstance1;

	public String interfaceInstance1;

	public String frameInstance2;

	public String interfaceInstance2;
	
	public boolean equals(Object o) {

		if (o == null)
			return false;

		if (!(o instanceof Binding))
			return false;

		Binding b = (Binding) o;

		return (frameInstance1 == null ? b.frameInstance1 == null
				: frameInstance1.equals(b.frameInstance1))
				&& (interfaceInstance1 == null ? b.interfaceInstance1 == null
						: interfaceInstance1.equals(b.interfaceInstance1))
				&& (frameInstance2 == null ? b.frameInstance2 == null
						: frameInstance2.equals(b.frameInstance2))
				&& (interfaceInstance2 == null ? b.interfaceInstance2 == null
						: interfaceInstance2.equals(b.interfaceInstance2));
	}
	
	public int hashCode() {
		return (frameInstance1 == null ? 0 : frameInstance1.hashCode())
				+ interfaceInstance1.hashCode()
				+ (frameInstance2 == null ? 0 : frameInstance2.hashCode())
				+ interfaceInstance2.hashCode();
	}
	
	public String toString() {
		return "<" + (frameInstance1 == null ? "" : frameInstance1) + ":"
				+ interfaceInstance1 + "-"
				+ (frameInstance2 == null ? "" : frameInstance2) + ":"
				+ interfaceInstance2 + ">";
	}

}
