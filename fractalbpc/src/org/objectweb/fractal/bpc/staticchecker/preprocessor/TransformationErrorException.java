/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jiri Adamek <adamek@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.fractal.bpc.staticchecker.preprocessor;

/**
 * This exception is thrown when a transformation error occurs.
 */
public class TransformationErrorException extends Exception {
	
	/** The textual description of the error. */
	public String errorMessage;

	/**
	 * The name of the frame, whose protocol cannot be transformed. If it is the
	 * 'supercomponent', this item is set to null.
	 */
	public String frameName;

	public TransformationErrorException(String errorMessage, String frameName) {
		super("protocol of the "
				+ (frameName == null ? "SUPERCOMPONENT" : "'" + frameName
						+ "' component") + " cannot be transformed:\n"
				+ errorMessage);
		this.errorMessage = errorMessage;
		this.frameName = frameName;
	}
}
