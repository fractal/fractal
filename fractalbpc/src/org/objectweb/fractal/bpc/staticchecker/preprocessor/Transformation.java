/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jiri Adamek <adamek@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.fractal.bpc.staticchecker.preprocessor;

import java.util.Map;
import java.util.Set;
import java.util.Iterator;

import org.objectweb.fractal.bpc.staticchecker.FrameInstance;
import org.objectweb.fractal.bpc.staticchecker.InterfaceInstance;

/**
 * Implements the transformations of behavior protocols.
 */
public class Transformation {
	
	private Map interfaceMap;
	
	private Map reverseInterfaceMap;
	
	public Transformation(Map interfaceMap, Map reverseInterfaceMap) {
		this.interfaceMap = interfaceMap;
		this.reverseInterfaceMap = reverseInterfaceMap;
	}
	
	private static void transformationError(String errorMessage,
			String frameName) throws TransformationErrorException {
		throw new TransformationErrorException(errorMessage, frameName);
	}
	
	private static Name getBindingName(boolean isTo,
			String componentName1, String interfaceName1,
			String componentName2, String interfaceName2, String methodName) {
		if (isTo)
			return new Name(componentName1, interfaceName1, componentName2,
					interfaceName2, methodName);
		else
			return new Name(componentName2, interfaceName2, componentName1,
					interfaceName1, methodName);
	}
	
	/** 
	 * Returns true iff this interface instance is on the 'to' side of all its bindings.
	 */
	public static boolean isTo(InterfaceInstance inInst) {
		if (inInst.frameName == null) return !inInst.isProvided;
		else return inInst.isProvided;
	}
	
	private Node transformEvent(Node node, FrameInstance frame)
			throws TransformationErrorException {
		InterfaceInstance key = frame
				.getInterfaceForName(node.name.interfaceName1);

		Set set;
		if (isTo(key))
			/* 'to' side */
			set = (Set) reverseInterfaceMap.get(key);
		else
			/* 'from' side */
			set = (Set) interfaceMap.get(key);

		if (set.isEmpty()) {
			/* unbound interface */
			node.name = new Name(frame.frameName, node.name.interfaceName1,
					node.name.methodName);
			return node;
		}

		if (set.size() > 1) {
			transformationError(
					"explicitly denoted event on an interface with multiple bindings ('"
							+ node.toString() + ")'", frame.frameName);
		}
		
		Iterator it = set.iterator();
		InterfaceInstance inInst = (InterfaceInstance) it.next();
		
		node.name = getBindingName(isTo(key), inInst.frameName, inInst.interfaceName,
				key.frameName, key.interfaceName, node.name.methodName);
		
		return node;
	}
	
	private Node transformCall(Node node, FrameInstance frame)
			throws TransformationErrorException {

		if (node.left != null)
			transform(node.left, frame);

		InterfaceInstance key = frame
				.getInterfaceForName(node.name.interfaceName1);

		Set set;
		if (isTo(key))
			/* provided interface */
			set = (Set) reverseInterfaceMap.get(key);
		else
			/* required interface */
			set = (Set) interfaceMap.get(key);

		if (set.isEmpty()) {
			/* unbound interface */
			node.name = new Name(frame.frameName, node.name.interfaceName1,
					node.name.methodName);
			return node;
		}

		Iterator it = set.iterator();
		InterfaceInstance inInst = (InterfaceInstance) it.next();
		
		Name originalName = node.name;
		
		node.name = getBindingName(isTo(key), inInst.frameName, inInst.interfaceName,
				key.frameName, key.interfaceName, node.name.methodName);
		
		Node result = node;
				
		while (it.hasNext()) {
			inInst = (InterfaceInstance) it.next();
			Node left = new Node();
			Node subprotocol = node.left == null ? null : node.left.cloneNode();
			left.setCall(node.prefix, getBindingName(isTo(key),
					inInst.frameName, inInst.interfaceName, key.frameName,
					key.interfaceName, originalName.methodName), subprotocol);
			Node right = result;
			result = new Node();
			if (isTo(key))
				result.setBinary("+", left, right);
			else
				result.setBinary("|", left, right);
		}

		return result;
	}
	
	public Node transform(Node node, FrameInstance frame) throws TransformationErrorException {
		
		switch (node.type) {

		case Node.NULL:
			return node;

		case Node.UNARY:
			node.left = transform(node.left, frame);
			return node;

		case Node.BINARY:
			node.left = transform(node.left, frame);
			node.right = transform(node.right, frame);
			return node;

		case Node.EVENT:
			return transformEvent(node, frame);

		case Node.CALL:
			return transformCall(node, frame);

		case Node.ATOMIC:
			for (int i = 0; i < node.eventArray.length; i++) {
				node.eventArray[i] = transform(node.eventArray[i], frame);
			}
			return node;

		default:
			/* TODO add assert(false); */
			return null;
		}
	
	
	}
	
}
