/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jiri Adamek <adamek@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.fractal.bpc.staticchecker.preprocessor;

/**
 * This exception is thrown when a parse error occurs.
 */
public class ParseErrorException extends Exception {

	/*
	 * The following constant defines how big is the relevant part of the
	 * protocol returned by showError.
	 */
	private static int RELEVANT = 20;

	/*
	 * It returns a string consisting of two lines of text: relevant part of the
	 * protocol (the first line) and a "pointer" to the error - the '^' symbol
	 * (the appropriate position on the second line).
	 */
	private static String showError(String protocol, int atPosition) {
		boolean cutBegin = atPosition > RELEVANT;
		boolean cutEnd = protocol.length() - 1 > atPosition + RELEVANT;
		int beginIndex = cutBegin ? atPosition - RELEVANT : 0;
		int endIndex = cutEnd ? atPosition + RELEVANT + 1 : protocol.length();
		String pre = cutBegin ? "... " : "";
		String post = cutEnd ? " ...\n" : "\n";
		int pointerPosition = cutBegin ? pre.length() + RELEVANT : atPosition;
		StringBuffer spaceStringBuffer = new StringBuffer();
		for (int i = 0; i < pointerPosition; i++)
			spaceStringBuffer.append(' ');
		return pre + protocol.substring(beginIndex, endIndex) + post
				+ spaceStringBuffer.toString() + '^';
	}

	/** The textual description of the error. */
	public String errorMessage;

	/** The protocol that couldn't be parsed. */
	public String protocol;
	
	/** The name of the frame instance, whose protocol cannot be parsed 
	 *  (null for the super-component).
	 */

	public String frameInstanceName;
	
	/**
	 * The position in {@link #protocol}where the parse error occured.
	 */
	public int atPosition;

	/**
	 * @param errorMessage
	 *            the textual description of the error
	 * @param protocol
	 *            the protocol that couldn't be parsed
	 * @param atPosition
	 *            the position in <code>protocol</code> where the parse error
	 *            occured
	 */
	public ParseErrorException(String errorMessage, String protocol,
			int atPosition, String frameInstanceName) {
		super("parse error in the protocol of the " +
				(frameInstanceName == null ? "SUPERCOMPONENT" : "'" + frameInstanceName + "' component")
				+ " at position " + atPosition + ":\n" + errorMessage
				+ '\n' + showError(protocol, atPosition));
		this.errorMessage = errorMessage;
		this.protocol = protocol;
		this.atPosition = atPosition;
		this.frameInstanceName = frameInstanceName;
	}
}
