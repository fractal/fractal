/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jiri Adamek <adamek@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.fractal.bpc.staticchecker.preprocessor;

/**
 * Stores a name appearing in a behavior protocol. 
 * The name can have one of the 3 following forms:<br>
 * <pre>
 * interface.method
 * component:interface.method
 * &ltcomponent1:interface1-component2:interface2&gt.method
 * </pre>
 */
public class Name implements Cloneable {
	/**
	 * If this value is stored in {@link #type}, the object represents
	 * an interface name. The name has the form
	 * <br>
	 * <code>
	 * {@link #interfaceName1}.{@link #methodName}
	 * </code>
	 */
	public static final int INTERFACE = 0;

	/**
	 * If this value is stored in {@link #type}, the object represents
	 * a component name - interface name pair.
	 * The name has the form<br> 
	 * <code>
	 * {@link #componentName1}:{@link #interfaceName1}.{@link #methodName}
	 * </code>
	 */
	public static final int COMPONENT = 1;

	/**
	 * If this value is stored in {@link #type}, the object represents
	 * binding name.
	 * The name has the form
	 * <br>
	 * <code>
	 * &lt{@link #componentName1}:{@link #interfaceName1}-{@link #componentName2}:{@link #interfaceName2}&gt.{@link #methodName}
	 * </code>
	 */
	public static final int BINDING = 2;

	/**
	 * The type of the represented name. 
	 * The value is one of the following: {@link #INTERFACE},
	 * {@link #COMPONENT}, or {@link #BINDING}.
	 */
	public int type;

	/** optional **/
	public String componentName1 = null;

	/** mandatory */
	public String interfaceName1;

	/** optional **/
	public String componentName2 = null;

	/** optional */
	public String interfaceName2 = null;

	/** mandatory */
	public String methodName;

	public Name cloneName() {
		try {
			return (Name) super.clone();
		} catch(CloneNotSupportedException e) {
			return null;
		}
	}
	
	public String toString() {
		String cn1 = componentName1 == null ? "" : componentName1;
		String cn2 = componentName2 == null ? "" : componentName2;
		switch (type) {
		case INTERFACE:
			return interfaceName1 + '.' + methodName;
		case COMPONENT:
			return cn1 + ':' + interfaceName1 + '.' + methodName;
		case BINDING:
			return '<' + cn1 + ':' + interfaceName1 + '-'
					+ cn2 + ':' + interfaceName2 + '>' + '.'
					+ methodName;
		default:
			/* unreachable */
			return null;
		}
	}

	public Name(String componentName, String interfaceName, String methodName) {
		this.type = COMPONENT;
		this.componentName1 = componentName;
		this.interfaceName1 = interfaceName;
		this.methodName = methodName;
	}

	public Name(String interfaceName, String methodName) {
		this.type = INTERFACE;
		this.interfaceName1 = interfaceName;
		this.methodName = methodName;
	}

	public Name(String componentName1, String interfaceName1,
			String componentName2, String interfaceName2, String methodName) {
		this.type = BINDING;
		this.componentName1 = componentName1;
		this.interfaceName1 = interfaceName1;
		this.componentName2 = componentName2;
		this.interfaceName2 = interfaceName2;
		this.methodName = methodName;
	}
}
