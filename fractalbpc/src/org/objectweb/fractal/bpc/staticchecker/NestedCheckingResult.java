/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.fractal.bpc.staticchecker;

/**
 * Result of checking of protocols of a whole component hierarchy.
 * Unlike {@link CheckingResult} contains also location where an error occured. 
 */
public class NestedCheckingResult extends CheckingResult {

  /**
   * Constructor of an error result.
   *
   * @param errorLocation Name of the faulty component.
   * @param errorType Type of the error. Has the same meaning as in {@link CheckingResult}.
   * @param errorDescription Textual description of the error. Has the same meaning as in {@link CheckingResult}.
   */
  public NestedCheckingResult(String errorLocation, int errorType, String errorDescription) {
    super(errorType,errorDescription);

    this.errorLocation=errorLocation;
  }
  
  /**
   * Constructor of an error result.
   *
   * @param errorLocation Name of the faulty component.
   * @param result Unpositioned error result.
   */
  public NestedCheckingResult(String errorLocation, CheckingResult result) {
    super(result);

    this.errorLocation=errorLocation;
  }

  /**
   * Constructor of a success result.
   */
  public NestedCheckingResult() {
    super();
    errorLocation = "";
  }
  
  public String toString() {
    return errorLocation + ": " + errorDescription;
  }
  
  /**
   * Returns the name of the faulty component in the case of an error.
   *
   * @return Location of the error.
   */
  public String getErrorLocation() {
    return errorLocation;
  }
	
  /**
   * Location of an error.
   */
  public String errorLocation;
}
