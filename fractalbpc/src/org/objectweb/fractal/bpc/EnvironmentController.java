/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Pavel Parizek <parizek@nenya.ms.mff.cuni.cz>
 * 
 */
package org.objectweb.fractal.bpc;

import java.util.Map;

import org.objectweb.fractal.bpc.adl.Environment;


/**
 * Environment Controller
 * Allows to associate an environment with a component.
 */
public interface EnvironmentController {

  /**
   * @return the name of a class with value sets.
   */
  String getFcValueSetsClass();

  /**
   * @param valueSetsClass the name of a class with value sets.
   */
  void setFcValueSetsClass(String valueSetsClass);

  /**
   * @return a Java code for user stub
   */
  String getFcUserStubCode();

  /**
   * @param userStubCode a Java code for user stub
   */
  void setFcUserStubCode(String userStubCode);

  /**
   * @return a map of event names to strings that represent Java code for user drivers
   */
  Map getFcUserDriversCode();

  /**
   * @param userDriversCode a map of event names to strings that represent Java code for user drivers
   */
  void setFcUserDriversCode(Map userDriversCode);
  
  /**
   * @return the protocol describing the behavior of the environment (not the inverted frame protocol)
   */
  String getFcProtocol();

  /**
   * @param protocol A protocol describing the behavior of the environment (not the inverted frame protocol)
   */
  void setFcProtocol(String protocol);
  
  /**
   * @return a map of fractal interface names to names of stub implementation classes
   */
  Map getFcItfStubs();

  /**
   * @param itfStubs a map fractal interface names to names of stub implementation classes
   */
  void setFcItfStubs(Map itfStubs);
  
}
