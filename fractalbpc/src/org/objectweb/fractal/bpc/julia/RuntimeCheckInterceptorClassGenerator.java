/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Vladimir Mencl <mencl@nenya.ms.mff.cuni.cz>
 *
 * Created on Dec 8, 2004
 *
 */

package org.objectweb.fractal.bpc.julia;

import java.util.List;

import org.objectweb.fractal.julia.asm.ClassGenerationException;
import org.objectweb.fractal.julia.asm.InterceptorClassGenerator;

/**
 * This is a specialized InterceptorClassGenerator that adds the RuntimeCheckInterceptor interface to the list of implemented interfaces.
 * 
 * @see org.objectweb.fractal.julia.asm.AbstractClassGenerator#getImplementedInterfaces() 
 * 
 */
public class RuntimeCheckInterceptorClassGenerator extends
		InterceptorClassGenerator {
	
	protected List getImplementedInterfaces() throws ClassGenerationException {
		List interfaces = super.getImplementedInterfaces();
		String itfName = org.objectweb.asm.Type.getInternalName(org.objectweb.fractal.bpc.RuntimeCheckInterceptor.class); 
		if (!interfaces.contains(itfName))
			interfaces.add(itfName);
		return interfaces;
	}
	
	protected String getSource() {
		return "RTC"+super.getSource();
	}
}
