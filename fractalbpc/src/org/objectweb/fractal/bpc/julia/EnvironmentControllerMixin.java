/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Pavel Parizek <parizek@nenya.ms.mff.cuni.cz>
 * 
 */
package org.objectweb.fractal.bpc.julia;

import java.util.Map;

import org.objectweb.fractal.julia.Controller;

import org.objectweb.fractal.bpc.EnvironmentController;
import org.objectweb.fractal.bpc.adl.Environment;


/**
 * Provides a basic implementation of the {@link EnvironmentController} interface.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>none.</li>
 * </ul>
 */

public abstract class EnvironmentControllerMixin implements EnvironmentController, Controller {


  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private EnvironmentControllerMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  
  public String fcValueSetsClass;
  public String fcUserStubCode;
  public Map fcUserDriversCode;
  public String fcProtocol;
  public Map fcItfStubs;
  
  
  public String getFcValueSetsClass()
  {
      return fcValueSetsClass;
  }

  public void setFcValueSetsClass(String valueSetsClass)
  {
      fcValueSetsClass = valueSetsClass;
  }

  public String getFcUserStubCode()
  {
      return fcUserStubCode;
  }

  public void setFcUserStubCode(String userStubCode)
  {
      fcUserStubCode = userStubCode;
  }

  public Map getFcUserDriversCode()
  {
      return fcUserDriversCode;
  }

  public void setFcUserDriversCode(Map userDriversCode)
  {
      fcUserDriversCode = userDriversCode;
  }

  public String getFcProtocol()
  {
      return fcProtocol;
  }

  public void setFcProtocol(String protocol)
  {
      fcProtocol = protocol;
  }
  
  public Map getFcItfStubs()
  {
      return fcItfStubs;
  }

  public void setFcItfStubs(Map itfStubs)
  {
      fcItfStubs = itfStubs;
  }
  
  
  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

}
