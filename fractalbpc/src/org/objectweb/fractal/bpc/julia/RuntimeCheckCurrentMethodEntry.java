/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Vladimir Mencl <mencl@nenya.ms.mff.cuni.cz>
 *
 * Created on Dec 6, 2004
 *
 */

package org.objectweb.fractal.bpc.julia;

/**
 * This data entry represents a single method in the list of open method calls on the interfaces of a component. 
 * This list is maintained by a {@link org.objectweb.fractal.bpc.RuntimeCheckController}.
 * 
 * The list includes all methods for which an enterMethod call was processed by the interceptor, but no leaveMethod call has been processed yet.  
 * @author Vladimir Mencl
 * @see org.objectweb.fractal.bpc.RuntimeCheckController#getFcCurrentMethods()
 *
 */

public class RuntimeCheckCurrentMethodEntry {
	public String itfName;
	public String methodName;
	public boolean isClient;
	public Object[] params;		

	public RuntimeCheckCurrentMethodEntry(String itfName, String methodName, boolean isClient, Object[] params) {
		this.itfName = itfName;
		this.methodName = methodName;
		this.isClient = isClient;
		this.params = params;
	}
}
