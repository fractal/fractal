/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Vladimir Mencl <mencl@nenya.ms.mff.cuni.cz>
 *
 * Created on Apr 22, 2005
 *
 */

package org.objectweb.fractal.bpc.julia;

import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.bpc.RuntimeCheckController;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;

/**
 * This mixin provides a weaveablePC field of type
 * {@link RuntimeCheckController}to the subsequent Mixins. This mixin requires
 * that the initialization context contains an interface named
 * "protocol-controller" of the type RuntimeCheckController.
 *  
 */

public abstract class UseRuntimeCheckControllerMixin implements Controller {
	RuntimeCheckController weaveableRTC = null;

	public void initFcController(InitializationContext ic)
			throws InstantiationException {
		weaveableRTC = (RuntimeCheckController) ic.getInterface("runtimecheck-controller");
		_super_initFcController(ic);
	}
	
	public abstract void _super_initFcController(InitializationContext ic);
		

}
