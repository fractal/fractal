/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Vladimir Mencl <mencl@nenya.ms.mff.cuni.cz>
 *
 * Created on Apr 22, 2005
 *
 */

package org.objectweb.fractal.bpc.julia;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.bpc.ProtocolController;
import org.objectweb.fractal.bpc.RuntimeCheckController;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator;

/**
 * A mixin extending the LifeCycle controller to communicate with RuntimeCheckController.
 * 
 * The startFc() method intializes the runtime check controller with the component's current protocol.
 * 
 * The stopFc() method 
 * 
 * Requires a reference to the protocol controller and runtimecheck controller.
 * 
 *  
 * @author mencl
 *
 */
public abstract class RuntimeCheckLifeCycleMixin implements Controller, LifeCycleCoordinator {
	ProtocolController _this_weaveablePC = null;
	RuntimeCheckController _this_weaveableRTC = null;
	
	public boolean setFcStarted() throws IllegalLifeCycleException {
		boolean willChange = !getFcState().equals(LifeCycleController.STARTED);
		if (willChange) {
			String protocol = _this_weaveablePC.getFcProtocol();
			if (protocol!=null)	_this_weaveableRTC.startFcRtcheck(protocol);
		};
		return _super_setFcStarted();
	}
	
	public abstract boolean _super_setFcStarted() throws IllegalLifeCycleException;
	
	public boolean setFcStopped() throws IllegalLifeCycleException {
		boolean changed = _super_setFcStopped();
		if (changed) {
			_this_weaveableRTC.stopFcRtcheck();
		}
		return changed;
	}
	
	public abstract boolean _super_setFcStopped() throws IllegalLifeCycleException;
	
	/*
	public void startFc() throws IllegalLifeCycleException {
		String protocol = _this_weaveablePC.getFcProtocol();
		if (protocol!=null)	_this_weaveableRTC.startFcRtcheck(protocol);
		_super_startFc();
	}
	
	public abstract void _super_startFc() throws IllegalLifeCycleException;
	
	public void stopFc() throws IllegalLifeCycleException {
		_super_stopFc();
		_this_weaveableRTC.stopFcRtcheck();
	}
	
	public abstract void _super_stopFc() throws IllegalLifeCycleException;
	*/
}
