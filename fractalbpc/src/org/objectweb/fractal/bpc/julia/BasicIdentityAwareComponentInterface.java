/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Vladimir Mencl <mencl@nenya.ms.mff.cuni.cz>
 *
 * Created on Feb 25, 2005
 *
 */

package org.objectweb.fractal.bpc.julia;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.bpc.IdentityAwareInterceptor;
import org.objectweb.fractal.julia.BasicComponentInterface;

/**
 * 
 * This class should be used as the base class for interface objects associated
 * with identity aware interceptors.
 * 
 * This class extends the behavior of an interface object to inform the
 * interceptor about the identity of the interface, both at the interface
 * creation time, and also when the interface name changes (via
 * {@link #setFcItfName(String)}); the latter behavior is necessary to properly
 * handle interface cloning, used to implement collection interfaces.
 * 
 * @see org.objectweb.fractal.bpc.IdentityAwareInterceptor
 *  
 */
public abstract class BasicIdentityAwareComponentInterface extends
		BasicComponentInterface {

	public BasicIdentityAwareComponentInterface() {
		super();
	}
	public BasicIdentityAwareComponentInterface(Component owner, String name,
			Type type, boolean isInternal, Object impl) {
		super(owner, name, type, isInternal, impl);
		if (!isInternal && (impl!=null) && (impl instanceof IdentityAwareInterceptor)) {
    		//System.err.print("BasicInterface: itfname "+name+ ( isInternal ? "[INTERNAL]": "" ));
    		((IdentityAwareInterceptor)impl).setFcItfInstanceName(name);
    		((IdentityAwareInterceptor)impl).setFcItfInstanceRef(this);
    				    			
    		if (type instanceof org.objectweb.fractal.api.type.InterfaceType) {
    			//System.err.print(", itfIsClient:="+((org.objectweb.fractal.api.type.InterfaceType)type).isFcClientItf());
    			((IdentityAwareInterceptor)impl).setFcItfIsClient(((org.objectweb.fractal.api.type.InterfaceType)type).isFcClientItf());
    		}
    		//System.err.println();
		}	
	}
	public void setFcItfName(String name) {
		super.setFcItfName(name);
		Object impl = getFcItfImpl();
		if (impl!=null && impl instanceof IdentityAwareInterceptor) {
			((IdentityAwareInterceptor)impl).setFcItfInstanceName(name);
		}
	}
}
