/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Vladimir Mencl <mencl@nenya.ms.mff.cuni.cz>
 *
 * Created on Dec 14, 2004
 *
 */

package org.objectweb.fractal.bpc.jmx;

/**
 * Specifies the MBean interface for exposing a Fractal component as an MBean
 *  
 * @author Vladimir Mencl
 *
 */
public interface RuntimeCheckMonitorMBean {
	/**
	 * returns the list of all currently open method calls on the component's interfaces.
	 * 
	 * The list includes all methods for which an enterMethod call was processed by the interceptor, but no leaveMethod call has been processed yet.  
	 * @see org.objectweb.fractal.bpc.RuntimeCheckController#getFcCurrentMethods()
	 */
	public String[] getCurrentMethods();
	/**
	 * returns the list of all method call events processed on the component interfaces.
	 * 
	 * The list includes all method call events, captured by the interceptor and
	 * recorded via the enterMethod and leaveMethod calls.  
	 * @see org.objectweb.fractal.bpc.RuntimeCheckController#getFcMethodHistory()
	 */
	public String[] getMethodHistory();
}
