/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Vladimir Mencl <mencl@nenya.ms.mff.cuni.cz>
 *
 * Created on Dec 14, 2004
 *
 */

package org.objectweb.fractal.bpc.jmx;

//import java.lang.management.ManagementFactory;
import javax.management.ObjectName;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.remote.JMXServiceURL;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;


/*
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
*/

/**
 * Helper class to instantatiate and access an MBeanServer and to register Fractal components as MBeans
 * 
 * The class provides static methods to register an mbean and to manage the MBeanServer.
 * 
 * The MBeanServer reference is stored in a static attribute. 
 * 
 * @author Vladimir Mencl
 *
 */
public class MBeanHelper {

	static MBeanServer mbs = null;
	static JMXConnectorServer cs = null;
	
	private MBeanHelper() {};

	/**
	 * Registers an MBean representing a Fractal component in the MBeanServer.
	 * The JMX {@link javax.management.ObjectName} is constructed with domain
	 * cz.cuni.juliacre.MonitorBean and attribute componentName=name
	 * 
	 * If no MBeanServer reference is available, a new server is created and
	 * exported throw a JMXConnector created with a service URL specified by the
	 * rtcheck.jmx.url (default value is service:jmx:iiop://localhost:8765)
	 * 
	 * @param monitor
	 *            the MBean to register
	 * @param name
	 *            the component name to use as the value of the JMX attribute
	 *            componentName
	 */
	public static void registerMBean(RuntimeCheckMonitor monitor, String name) {

		 try {
			 if (mbs == null) {
			   // mbs = ManagementFactory.getPlatformMBeanServer();
			   mbs = MBeanServerFactory.createMBeanServer();
			   // int port = 9876;
			   // Registry reg = LocateRegistry.createRegistry(port);
			   JMXServiceURL url =
			       //new JMXServiceURL("service:jmx:rmi:///jndi/rmi://localhost:"+port+"/server");
			   	   new JMXServiceURL(System.getProperty("fractal.protocols.rtcheck.jmx.url","service:jmx:iiop://localhost:8765"));
			   cs = JMXConnectorServerFactory.newJMXConnectorServer(url, null, mbs);
			   cs.start();
			   System.out.println("created JMX Connector: "+cs.getAddress());
			 };
			 
			 ObjectName oname = new ObjectName( 
			     "org.objectweb.fractal.bpc.MonitorMBean:componentName=\""+name+"\"");
			 mbs.registerMBean(monitor,oname);
			 
			 // registry: sun.rmi.registry.RegistryImpl
		 }
		 catch (Exception e) { e.printStackTrace(); };
	}
	
	
	/**
	 * stops the connector server created to expose the MBeanServer 
	 */
	public static void stopConnectorServer() {
		try {
			if (cs!=null)
				cs.stop();
		} catch (java.io.IOException e) { e.printStackTrace(); throw new RuntimeException("error stopping connector server", e); };
	}

	
	/**
	 * Sets the reference to the MBeanServer to use in the registerMBean method.
	 * 
	 * If setMBeanServer is not called, a new server will be created upon the
	 * first invocation of registerMBean
	 * 
	 * @param mbeanServer
	 */
	public static void setMBeanServer(MBeanServer mbeanServer) {
		mbs = mbeanServer;
		
	}

}
