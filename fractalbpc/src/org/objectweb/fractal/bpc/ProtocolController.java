/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.fractal.bpc;

/**
 * Protocol Controller
 * Allows to associcate a frame protocol with a component.
 */

public interface ProtocolController {

  /**
   * Returns the frame protocol associated with a component.
   *
   * @return The associated frame protocol
   */
  String getFcProtocol();

  /**
   * Assigns a frame protocol to a component.
   *
   * @param protocol A frame protocol
   */
  void setFcProtocol(String protocol);

  /**
   * Returns the position in the explicit ordering of protocols. The ordering is ascending.
   *
   * @return The position in the explicit ordering
   */
  int getFcOrder();

  /**
   * Assigns a position in the explicit ordering of protocols. The ordering is ascending.
   *
   * @param order A position in the explicit ordering
   */
  void setFcOrder(int order);
}
