/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Vladimir Mencl <mencl@nenya.ms.mff.cuni.cz>
 *
 * Created on Dec 8, 2004
 *
 */

package org.objectweb.fractal.bpc;

import org.objectweb.fractal.julia.ComponentInterface;

/**
 * This interface defines methods used to control an interceptor created by the
 * runtime-check-protocol framework.
 * <p>
 * To correctly monitor the runtime behavior of a component, an interceptor
 * needs to know the identity of the interface it monitors - in particular, the
 * name of the interface and whether the interface is a client or server
 * interface.<p>
 * 
 * For possible future enhancements, the interceptor also keeps the direct
 * reference to the interface object.<p>
 * 
 * The get and set methods of this interface interpret three properties:<br>
 * <tt><strong>String</strong> itfInstanceName;</tt><br>
 * <tt><strong>boolean</strong> itfIsClient;</tt><br>
 * <tt><strong>ComponentInterface</strong> itfInstanceRef;</tt><br>
 * <p>
 * All these properties are set from the initFcController method of BasicRuntimeCheckControllerMixin
 * 
 * This interface is implemented by all interceptors generated by {@link org.objectweb.fractal.bpc.julia.RuntimeCheckInterceptorCodeGenerator}.
 * 
 * @see org.objectweb.fractal.bpc.julia.BasicRuntimeCheckControllerMixin#initFcController(InitializationContext)
 * @author Vladimir Mencl
 *  
 */
public interface RuntimeCheckInterceptor {
	/**
	 * sets the value of the itfInstanceRef property
	 * @return itfInstanceRef
	 */
	public ComponentInterface getFcItfInstanceRef();

	/**
	 * returns the reference to the interface object of the interface this interceptor monitors.
	 * @param itfRef new value of itfRef
	 */
	public void setFcItfInstanceRef(ComponentInterface itfRef);

	/**
	 * returns the name of the interface (instance) this interceptor monitors.
	 * @return itfInstanceName
	 */
	public String getFcItfInstanceName();

	/**
	 * sets the itfInstanceName property
	 * @param newItfInstanceName new value of itsInstanceName
	 */
	public void setFcItfInstanceName(String newItfInstanceName);

	/**
	 * sets the itfIsClient property
	 * @param itfIsClient new value of itfIsClient
	 */
	public void setFcItfIsClient(boolean itfIsClient);

	/**
	 * returns true if the interface is a client interface, false if the interface is a server interface
	 * @return itfIsClient
	 */
	public boolean getFcItfIsClient();
}

