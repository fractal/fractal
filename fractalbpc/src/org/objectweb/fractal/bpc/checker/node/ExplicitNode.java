/* 
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 */

package org.objectweb.fractal.bpc.checker.node;


import java.util.TreeMap;
import java.util.TreeSet;

import org.objectweb.fractal.bpc.checker.state.State;
import org.objectweb.fractal.bpc.checker.state.TransitionPair;
import org.objectweb.fractal.bpc.checker.state.TransitionPairs;
import org.objectweb.fractal.bpc.checker.utils.AnotatedProtocol;

/**
 * Represents an explicit automaton for a parse subtree.
 */
public class ExplicitNode extends TreeNode {

    /** Creates a new instance of ExplicitNode */
    public ExplicitNode(State initial, TreeMap transitions, TreeSet accepting, String protocol, TreeNode original) {
        super(protocol);
        this.initial = initial;
        this.transitions = transitions;
        this.accepting = accepting;

        this.nodes = new TreeNode[0];
        this.original = original;

        this.weight = 1;
    }

    /**
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#getInitial()
     */
    public State getInitial() {
        return initial;
    }

    /**
     * 
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#getTransitions(org.objectweb.fractal.bpc.checker.state.State)
     */
    public TransitionPairs getTransitions(State state) throws InvalidParameterException {
        TransitionPair[] result = (TransitionPair[]) transitions.get(state);
        if (result != null)
            return new TransitionPairs(result);
        else
            return new TransitionPairs(new TransitionPair[0]);

    }

    /**
     * 
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#isAccepting(org.objectweb.fractal.bpc.checker.state.State)
     */
    public boolean isAccepting(org.objectweb.fractal.bpc.checker.state.State state) {
        return accepting.contains(state);
    }

    /**
     * 
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#getWeight()
     */
    public long getWeight() {
        return 1;
    }

    /**
     * 
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#forwardCut(java.util.TreeSet)
     */
    public TreeNode forwardCut(TreeSet livingevents) {
        // no cutting is performed here...
        return this;
    }

    /**
     * 
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#copy()
     */
    public TreeNode copy() {
        // no cutting is performed here...
        return this;
    }

    /**
     * @return the symbolic name of the treenode denoting its type
     */
    public String[] getTypeName() {
        String[] result = { "Explicit", "EXPLICIT" };
        return result;
    }

    /**
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#getAnotatedProtocol(org.objectweb.fractal.bpc.checker.state.State)
     */
    public AnotatedProtocol getAnotatedProtocol(State state) {
        return original.getAnotatedProtocol(state);
    }    
    //--------------------------------------------------------------------------
    /**
     * The initial state.
     */
    final private State initial;
    
    /**
     * The original parse subtree .
     */
    private TreeNode original;

    /**
     * The transition matrix.
     */

    final private TreeMap transitions;

    /**
     * The set of accepting states.
     */
    final private TreeSet accepting;


}
