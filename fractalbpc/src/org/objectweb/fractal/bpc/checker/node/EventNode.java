/* 
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 */

package org.objectweb.fractal.bpc.checker.node;

import java.util.ArrayList;
import java.util.TreeSet;

import org.objectweb.fractal.bpc.checker.DFSR.Options;
import org.objectweb.fractal.bpc.checker.state.*;
import org.objectweb.fractal.bpc.checker.utils.AnotatedProtocol;
import org.objectweb.fractal.bpc.checker.utils.ProtocolUsageMonitor;


/**
 * This is a class extending TreeNode representing an event node within the
 * tree.
 */
public class EventNode extends TreeNode {

    /** Creates a new instance of EventNode */
    public EventNode(int eventindex, ActionRepository repository) {
        super(repository.getItemString(eventindex));
        this.eventIndex = eventindex;

        this.initState = new SimpleState(0);

        this.initTransitions = new TransitionPairs(new TransitionPair[1]);
        this.initTransitions.transitions[0] = new TransitionPair(eventindex, new SimpleState(1));

        this.secondTransitions = new TransitionPairs(new TransitionPair[0]);

        this.nodes = new TreeNode[0];

        this.weight = 2;
        
        this.executed = false;
        
    }

    /**
     * 
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#getInitial()
     */
    public State getInitial() {
        return initState;
    }

    /**
     * 
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#isAccepting(org.objectweb.fractal.bpc.checker.state.State)
     */
    public boolean isAccepting(State state) {
        return (((SimpleState) state).state == 1);
    }

    /**
     * 
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#getTransitions(org.objectweb.fractal.bpc.checker.state.State)
     */
    public TransitionPairs getTransitions(State state) throws InvalidParameterException {
        if (((SimpleState) state).state == 0) {
            if (Options.monitorusage)
                ProtocolUsageMonitor.eventGenerated(eventIndex, this);
            return initTransitions;
        }
        else if (((SimpleState) state).state == 1)
            return secondTransitions;
        else
            throw new InvalidParameterException();
    }

    /**
     * 
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#getWeight()
     */
    public long getWeight() {
        return 2;
    }

    /**
     * 
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#forwardCut(java.util.TreeSet)
     */
    public TreeNode forwardCut(TreeSet livingevents) {
        if (livingevents.contains(new Integer(eventIndex)))
            return this;
        else
            return null;
    }

    /**
     * @return the symbolic name of the treenode denoting its type
     */
    public String[] getTypeName() {
        String[] result = { "Event", "" };
        return result;
    }

    /**
     * @return the eventindex
     */
    public int getEventIndex() {
        return eventIndex;
    }
    
    public int getLeafCount() {
        return 1;
    }

    /**
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#getAnotatedProtocol(org.objectweb.fractal.bpc.checker.state.State)
     */
    public AnotatedProtocol getAnotatedProtocol(State state) {
        String result = new String();
        ArrayList indicesresult = new ArrayList();
        
        result = protocol;
        if (state != null)
            if (((SimpleState)state).state == 0)
                indicesresult.add(new Integer(0));
            else
                indicesresult.add(new Integer(result.length()));
        
        return new AnotatedProtocol(result, indicesresult);
    }

    /**
     * Setter for executed
     */
    public void setExecuted() {
        executed = true;
    }

    /**
     * Getter for executed
     * @return the executed value
     */
    public boolean getExecuted() {
        return executed;
    }
    
    
    //--------------------------------------------------------------------------
    /**
     * event index
     */
    final private int eventIndex;

    /**
     * Simple state for initial state representation
     */
    private SimpleState initState;
    
    /**
     * Transitions for initial state
     */
    final private TransitionPairs initTransitions;

    /**
     * Transitions for the second state
     */
    final private TransitionPairs secondTransitions;
    
    /**
     * true if this event was executed - set by property methods
     */
    private boolean executed;

}
