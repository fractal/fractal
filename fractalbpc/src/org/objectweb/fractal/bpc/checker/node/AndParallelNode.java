/* 
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 */

package org.objectweb.fractal.bpc.checker.node;

import java.util.ArrayList;

import org.objectweb.fractal.bpc.checker.DFSR.CheckingException;
import org.objectweb.fractal.bpc.checker.state.*;
import org.objectweb.fractal.bpc.checker.utils.AnotatedProtocol;


/**
 * Represents andparalell operator
 */
public class AndParallelNode extends TreeNode {

    /** Creates a new instance of AndParallelNode */
    public AndParallelNode(TreeNode[] nodes) {
        super(getProtocol(nodes));
        this.nodes = nodes;
    }

    /**
     * 
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#getInitial()
     */
    public State getInitial() {
        State[] init = new State[nodes.length];

        for (int i = 0; i < nodes.length; ++i) {
            init[i] = nodes[i].getInitial();
        }
        return new CompositeState(init);
    }

    /**
     * 
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#isAccepting(org.objectweb.fractal.bpc.checker.state.State)
     */
    public boolean isAccepting(State state) {

        State[] states = ((CompositeState) state).states;

        for (int i = 0; i < nodes.length; ++i)
            if (!nodes[i].isAccepting(states[i]))
                return false;

        return true;
    }

    /**
     * 
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#getTransitions(org.objectweb.fractal.bpc.checker.state.State)
     */
    public TransitionPairs getTransitions(State state) throws InvalidParameterException, CheckingException {

        CompositeState cstate = (CompositeState) state;
        int statecnt = cstate.states.length;
        int totalcnt = 0;

        TransitionPair[][] transmatrix = new TransitionPair[statecnt][];
        State[] origstates = new State[statecnt];

        // the original states with no transitions
        for (int i = 0; i < statecnt; ++i) {
            origstates[i] = cstate.states[i];
        }

        // prepare the matrix of transitions
        for (int i = 0; i < statecnt; ++i) {
            transmatrix[i] = nodes[i].getTransitions(cstate.states[i]).transitions;
            totalcnt += transmatrix[i].length;
        }

        // fill in the list of transitions
        TransitionPair[] result = new TransitionPair[totalcnt];

        // iteration over all states
        for (int i = 0, idx = 0; i < statecnt; ++i) {

            // iteration over the transition count of the given state
            for (int j = 0; j < transmatrix[i].length; ++j, ++idx) {

                /**
                 * one item of the result the result is array of transitionpairs
                 * of composite states
                 */
                State[] item = new State[statecnt];

                // last iteration again over all states
                for (int k = 0; k < statecnt; ++k) 
                    item[k] = origstates[k];

                item[i] = transmatrix[i][j].state;

                result[idx] = new TransitionPair(transmatrix[i][j].eventIndex, new CompositeState(item));
            }
        }

        return new TransitionPairs(result);
    }

    /**
     * 
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#getWeight()
     */
    public long getWeight() {
        if (weight != -1)
            return weight;
        else {
            weight = 1;
            for (int i = 0; i < nodes.length; ++i)
                weight *= nodes[i].getWeight();

            return weight;
        }
    }

    /**
     * @return the symbolic name of the treenode denoting its type
     */
    public String[] getTypeName() {
        String[] result = { "AndParallel", "|" };
        return result;
    }

    
    /**
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#getAnotatedProtocol(org.objectweb.fractal.bpc.checker.state.State)
     */
    public AnotatedProtocol getAnotatedProtocol(State state) {
        CompositeState cstate = (CompositeState) state;
        int nodecnt = nodes.length;
        String result = new String();
        ArrayList indicesresult = new ArrayList();
        
        result = "(";
        for (int i = 0; i < nodecnt; ++i) {
            AnotatedProtocol subresult = nodes[i].getAnotatedProtocol((cstate != null) ? cstate.states[i] : null);
            
            for (int j = 0; j < subresult.indices.size(); ++j)
                indicesresult.add(new Integer(((Integer)subresult.indices.get(j)).intValue() + result.length()));
            
            result += subresult.protocol;
            
            if (i < nodecnt - 1)
                result += ")|(";
        }
        result += ")";
        
        return new AnotatedProtocol(result, indicesresult);
    }
     

    /**
     * This method is obsolete and used only for debuging, the protocol is
     * stored during the parse process.
     * 
     * @return the protocol of this tree.
     */
    static private String getProtocol(TreeNode[] nodes) {
        String result = new String();
        result = "(" + nodes[0].protocol + ")";

        for (int i = 1; i < nodes.length; ++i) {
            result += " | (" + nodes[i].protocol + ")";
        }

        return result;

    }

}

