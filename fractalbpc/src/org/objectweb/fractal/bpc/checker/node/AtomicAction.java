/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 */

package org.objectweb.fractal.bpc.checker.node;

import java.util.Collection;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * Extending class for a treeset.
 * Implements comparable interface and can be use for 
 * storing within a hashset or whatever
 */
public class AtomicAction extends TreeSet implements Comparable {


    /**
     * @see java.io.Serializable
     */
    private static final long serialVersionUID = 6849295002558293520L;


    /**
     * Creates a new instance of AtomicAction.
     * @param c members of c that are to be added to this AtomicAction
     */
    public AtomicAction(Collection c) {
        super(c);
    }

    /**
     * Creates a new instance of AtomicAction.
     *
     */
    public AtomicAction() {}
    
    
    /**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Object o) {
        if (o instanceof AtomicAction) {
            AtomicAction other = (AtomicAction)o;
            
            if (this.size() < other.size())
                return -1;
            else if (this.size() > other.size())
                return 1;
            
            else {
                Iterator it1 = this.iterator();
                Iterator it2 = other.iterator();
                
                while (it1.hasNext()) {
                    Object o1 = it1.next();
                    Object o2 = it2.next();
                    
                    int result = ((Comparable)o1).compareTo(o2);
                    if (result != 0)
                        return result;
                }
                
                return 0;
            }
        }
        else
            return -1;
    }
    
    
}
