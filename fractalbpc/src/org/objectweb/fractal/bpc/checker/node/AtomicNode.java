/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 */

package org.objectweb.fractal.bpc.checker.node;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

import org.objectweb.fractal.bpc.checker.DFSR.CheckingException;
import org.objectweb.fractal.bpc.checker.state.SimpleState;
import org.objectweb.fractal.bpc.checker.state.State;
import org.objectweb.fractal.bpc.checker.state.TransitionPair;
import org.objectweb.fractal.bpc.checker.state.TransitionPairs;
import org.objectweb.fractal.bpc.checker.utils.AnotatedProtocol;


/**
 * This class represents an atomic action inside of a parse tree. 
 */
public class AtomicNode extends TreeNode {

    /**
     * Creates a new instance of AtomicNode.
     * 
     * @param events the events forming this atomic action
     * @param repository the action repository
     */
    public AtomicNode(int eventindex, TreeSet events, ActionRepository repository) {
        super(getProtocol(events, repository));

        this.eventIndex = eventindex;
        this.initState = null;
        this.initTransitions = new TransitionPairs(new TransitionPair[1]);
        this.initTransitions.transitions[0] = null;
        this.secondTransitions = new TransitionPairs(new TransitionPair[0]);
        this.nodes = new TreeNode[0];
        this.weight = 2;
    }

    /**
     * Creates a new instance of AtomicNode.
     * 
     * @param eventindex eventindex
     * @param repository action repository
     */
    public AtomicNode(int eventindex, ActionRepository repository) {
        this(eventindex, repository.getAtomicEvents(eventindex), repository); 
    }

    /**
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#getWeight()
     */
    public long getWeight() {
        return 2;
    }

    /**
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#getInitial()
     */
    public State getInitial() {
        if (initState == null) {
            initState = new SimpleState(0);
            this.initTransitions.transitions[0] = new TransitionPair(eventIndex, new SimpleState(1));
        }
        
        return initState;
    }

    /**
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#isAccepting(org.objectweb.fractal.bpc.checker.state.State)
     */
    public boolean isAccepting(State state) {
        return (((SimpleState) state).state == 1);
    }

    /**
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#getTransitions(org.objectweb.fractal.bpc.checker.state.State)
     */
    public TransitionPairs getTransitions(State state) throws InvalidParameterException, CheckingException {
        if (((SimpleState) state).state == 0)
            return initTransitions;
        else if (((SimpleState) state).state == 1)
            return secondTransitions;
        else
            throw new InvalidParameterException();
    }

    /**
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#getTypeName()
     */
    public String[] getTypeName() {
        String[] result = { "AtomicAction", "atomic action" };
        return result;
    }
    
    
    public TreeNode forwardCut(TreeSet livingevents) {
        return this;
    }

    /**
     * @see org.objectweb.fractal.bpc.checker.node.TreeNode#getAnotatedProtocol(org.objectweb.fractal.bpc.checker.state.State)
     */
    public AnotatedProtocol getAnotatedProtocol(State state) {
        //SimpleState sstate = (SimpleState) state;
        String result = new String();
        ArrayList indicesresult = new ArrayList();
        
        result = protocol;
        if (state != null)
            if (((SimpleState)state).state == 0)
                indicesresult.add(new Integer(0));
            else
                indicesresult.add(new Integer(result.length()));
        
        return new AnotatedProtocol(result, indicesresult);
    }

    /**
     * @return the eventindex
     */
    public int getEventIndex() {
        return eventIndex;
    }
    
    
    
    /**
     * Creates the protocol (i.e. [_actionlist]) string
     * @param events the events creating this atomic action
     * @param repository the action repository
     * @return the protocol describing this aotmic action
     */
    private static String getProtocol(TreeSet events, ActionRepository repository) {
        String p = new String();
         
        Iterator it = events.iterator();
        
        p = repository.getItemString(((Integer)it.next()).intValue());
        
        while (it.hasNext())
            p = p + ", " + repository.getItemString(((Integer)it.next()).intValue());
            
        p = "[" + p + "]";
        
        return p;
    }
    

    
    
    /**
     * The index of thic atomic action
     */
    private int eventIndex;
    
    /**
     * Simple state for initial state representation
     */
    private SimpleState initState;
    
    /**
     * Transitions for initial state
     */
    final private TransitionPairs initTransitions;

    /**
     * Transitions for the second state
     */
    final private TransitionPairs secondTransitions;

    
}
