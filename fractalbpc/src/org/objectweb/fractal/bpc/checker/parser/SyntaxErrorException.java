/* 
 * $Id$ 
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Stanislav Visnovsky <visnovsky@nenya.ms.mff.cuni.cz>
 */

package org.objectweb.fractal.bpc.checker.parser;

/**
 * The class for syntax error exceptions
 */
public class SyntaxErrorException extends Exception {

    /**
     * @see java.io.Serializable
     */
    private static final long serialVersionUID = 4426531672788432476L;

    /**
     * Creates the instance of a new syntax error exception.
     */
    public SyntaxErrorException() {
        super();
    }

    /**
     * Creates the instance of a new syntax error exception with a given
     * comment.
     * 
     * @param s the comment on the raised exception
     */
    public SyntaxErrorException(String s) {
        super(s);
    }
}
