/*
 * $Id$
 * 
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 */

package org.objectweb.fractal.bpc.checker.parser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * This class implements the ProtocolReader and is
 * able to read the input protocols from a file
 */
public class FileTokenizer extends ProtocolReader {
    
    /**
     * ctor.
     * 
     * @param filename	the name of a file containg the input protocols
     * @throws IOException	when the file cannot be opened
     */
    public FileTokenizer(String filename) throws IOException {
        
        try {
            reader = new FileReader(filename);
        } catch (FileNotFoundException e) {
            throw new IOException();
        }
    }

    /**
     * Reads the next character
     * 
     * @throws IOException whenever a problem with reading the input file occurs
     * @return the character read
     */
    public int read() throws IOException {
        char read = (char)-1;
        while (reader.ready()) {
            read = (char)reader.read();
            
            if ((read == ' ') || (read == '\t') || (read == '\n') || (read == '\r')) 
                ;//read = (char)reader.read();
            
            else if (read == '#') {
                String line = getLine();
                if (line.equals("eop"))
                    return -1;
                else
                    ;// a comment, read further
            }
            else	// a real character   
                break;
                
        }
        
        if (read != -1) {
            ++index;
            return read;
        }
        else
            return -1;
    }
    
    
    /**
     * Checks whether the source string is not yet parsed completelly
     * 
     * @return true if there are character left, false otherwise
     */
    public boolean ready() throws IOException {
        return reader.ready();
    }



    
    /**
     * @see org.objectweb.fractal.bpc.checker.parser.ProtocolReader#getIndex()
     */
    public int getIndex() {
        return index;
    }

    /**
     * @see java.io.Reader#read(char[], int, int)
     */
    public int read(char[] arg0, int arg1, int arg2) throws IOException {
        //not implemented
        throw new IOException("Not implemented");
    }

    /**
     * JDK 1.5.0: java.io.Closeable#close()
     */
    public void close() throws IOException {
        reader.close();
    }
    
    
    /**
     * @see org.objectweb.fractal.bpc.checker.parser.ProtocolReader#resetIndex()
     */
    public void resetIndex() {
        index = 0;
    }

    
    private String getLine() throws IOException {
        String line = new String();
        char read;
        
        while (reader.ready()) {
            read = (char)reader.read();
            if ((read != '\n') && (read != '\r'))
                line += read;
            else
                break;
        }
        
        return line;
    }
    
    
    
    
    /**
     * File reader
     */
    private FileReader reader;
    
    /**
     * Output index
     */
    private int index = 0;

}
