/* 
 * $Id$ 
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 */

package org.objectweb.fractal.bpc.checker.parser;

import java.io.IOException;

/**
 * This class it to replace the originally used StringReader in order to be able
 * to provide information about the index in the string where the current token
 * lies.
 *  
 */
public class StringTokenizer extends ProtocolReader {

    /**
     * Creates a new instance of StringTokenizer.
     * @param source the source string to be read.
     */
    public StringTokenizer(String source) {
        this.source = source;
    }

    /**
     * Reads the next character
     * 
     * @return the character read
     */
    public int read() {
        if (index == source.length())
            return -1;
        else {
			char c = source.charAt(index++);
			if (c == '#') commentMode = true;
			else if ((c == '\n') || (c == '\r')) commentMode = false;
			
			if (commentMode) return ' ';
			else return c;
		}
    }

    /**
     * Checks whether the source string is not yet parsed completelly
     * 
     * @return true if there are character left, false otherwise
     */
    public boolean ready() {
        return source.length() >= index;
    }

    /**
     * @return the index within the string being parsed
     * 
     */
    public int getIndex() {
        return index;
    }

    /**
     * @see java.io.Reader#close()
     */
    public void close() throws IOException {
        //nothing to be done
    }

    /**
     * @see java.io.Reader#read(char[], int, int)
     */
    public int read(char[] cbuf, int off, int len) throws IOException {
        //not implemented
        throw new IOException("Not implemented");
    }

    /**
     * @see org.objectweb.fractal.bpc.checker.parser.ProtocolReader#resetIndex()
     */
    public void resetIndex() {
        // makes nothing
    }    
    //-----------------------------------------------------------------------------------

    /**
     * The string being parsed
     */
    private String source;

    /**
     * The current index within the string being parsed
     */
    int index = 0;

	// comment mode is on from '#' to '\n' or '\r'
	private boolean commentMode = false;

}
