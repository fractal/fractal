/* 
 * $Id$ 
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Stanislav Visnovsky <visnovsky@nenya.ms.mff.cuni.cz>
 */




package org.objectweb.fractal.bpc.checker.parser;

/**
 * The debug enabling class. If the {@link Debug#debug}is set to number higher
 * than 0, all methods prints the arguments, otherwise the output is skipped.
 * The set of methods allowing specification of a debug level to which the
 * message is intended is also provided. Supposed use of debug level: 0 - no
 * debug info 1 - only high level debug about progress 2 - progress of
 * complicated methods 3 - all debug info
 * 
 */
public class Debug {
    /**
     * Indication, if the debugging stuff should be send to the standard output
     */
    public static int debug = 0;

    /**
     * Print a string to the standard output and the new line.
     * 
     * @param a
     *            a string to print
     */
    public static void println(String a) {
        if (debug > 0)
            System.out.println(a);
    }

    public static void println(int level, String a) {
        if (debug >= level)
            System.out.println(a);
    }

    /**
     * Print a new line to the standard output.
     * 
     */
    public static void println() {
        if (debug > 0)
            System.out.println();
    }

    public static void println(int level) {
        if (debug >= level)
            System.out.println();
    }

    /**
     * Print an integer to the standard output and the new line.
     * @param a
     *            an integer to print
     * @param level
     *            debug level the message should be printed at
     */
    public static void println(int level, int a) {
        if (debug >= level)
            System.out.println(a);
    }

    /**
     * Print a string to the standard output.
     * @param a
     *            a string to print
     */
    public static void print(String a) {
        if (debug > 0)
            System.out.print(a);
    }

    public static void print(int level, String a) {
        if (debug >= level)
            System.out.print(a);
    }

    /**
     * Print an integer to the standard output.
     * 
     * @param a
     *            an integer to print
     * @param level
     *            debug level the message should be printed at
     */
    public static void print(int level, int a) {
        if (debug >= level)
            System.out.print(a);
    }

    /**
     * Sets the debug level to the specified value
     * 
     * @param level
     *            the new debug level
     */
    public static void setLevel(int level) {
        debug = level;
    }

    /**
     * Returns the current debug level
     * 
     * @return the current debug level
     */
    public static int getLevel() {
        return debug;
    }
}
