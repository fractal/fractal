/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 */


package org.objectweb.fractal.bpc.checker.state;

import org.objectweb.fractal.bpc.checker.node.ActionRepository;

/**
 * Represents transition form one state to another. The members of this class
 * are declared as public for better performance of the
 * org.objectweb.fractal.bpc.checker.
 */
public class TransitionPair {

    /** Creates a new instance of TransitionPair */
    public TransitionPair(int eventindex, State state) {
        this.eventIndex = eventindex;
        this.state = state;
    }

    /**
     * To string function
     * 
     * @return the string representation of this transition pair
     */
    public String toString() {
        return new String(repository.getItemString(eventIndex) + "->" + state.label);
    }

    /**
     * Event index
     */
    final public int eventIndex;

    /**
     * Pointer to the state
     */
    final public State state;
    
    /**
     * pointer to the repository - for debug reasons
     */
    static public ActionRepository repository;

}
