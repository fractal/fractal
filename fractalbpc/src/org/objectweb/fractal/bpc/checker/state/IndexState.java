/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 */


package org.objectweb.fractal.bpc.checker.state;

/**
 * This class represents the state with an index of the subautomaton and pointer
 * to it. Note that both the index and the pointer are neccessary. The members
 * of this class are declared as public for better performance of the
 * org.objectweb.fractal.bpc.checker.
 */
public class IndexState extends State {

    /** Creates a new instance of IndexState */
    public IndexState(int index, State[] states) {
        this.index = index;
        this.states = states;
    }

    /**
     * @see org.objectweb.fractal.bpc.checker.state.State#equals(org.objectweb.fractal.bpc.checker.state.State)
     */
    public boolean equals(State another) {
        if (!(another instanceof IndexState))
            return false;

        IndexState istate = (IndexState) another;

        if (this.index != istate.index)
            return false;

        if (this.signature == null)
            this.getSignature();

        if (another.signature == null)
            another.getSignature();

        return this.getSignature().equals(istate.getSignature());
    }
    
    /**
     * @see org.objectweb.fractal.bpc.checker.state.State#createSignature()
     */
    protected void createSignature() {
        for (int i = 0; i < states.length; ++i) {
            this.signature.concat(states[i].getSignature());
        }
    }
    

    /**
     * The substates.
     */
    final public State[] states;

    /**
     * the index of the automaton
     */
    final public int index;


}
