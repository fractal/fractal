/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 */

package org.objectweb.fractal.bpc.checker.state;

import java.util.LinkedList;

/**
 * This class is top hold an array of transition pair and a single list of
 * potential bad activity dependecies across atomic actions.
 */

public class TransitionPairs {
    
    /**
     * Class for keeping the items of dependecy list
     */
    static public class IntPair {
        public IntPair() {
        }
        
        public IntPair(int first, int second) {
            this.first = first;
            this.second = second;
        }
            
        public int first;
        public int second;
    }
    
    /**
     * Constructor, dependency list is initially empty (in fact, it is null)
     * @param trans	the transitions to hold
     */
    public TransitionPairs(TransitionPair[] trans) {
        this.transitions = trans;
        this.deps = null;
    }
    
    /**
     * Constructor, dependency list is initially empty (in fact, it is null)
     * @param trans	the transitions to hold
     * @param deps	the atomic actions dependencies
     */
    public TransitionPairs(TransitionPair[] trans, LinkedList deps) {
        this.transitions = trans;
        this.deps = deps;
    }
    
    /**
     * the transitions
     */
    public TransitionPair[] transitions;
    
    /**
     * atomic actions bad activity dependencies
     */
    public LinkedList deps;

}
