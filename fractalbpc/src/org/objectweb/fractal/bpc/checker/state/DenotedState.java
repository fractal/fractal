/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 */

package org.objectweb.fractal.bpc.checker.state;

import org.objectweb.fractal.bpc.checker.DFSR.CheckingException;
import org.objectweb.fractal.bpc.checker.node.AlternativeNode;
import org.objectweb.fractal.bpc.checker.node.InvalidParameterException;

/**
 * This class is to be used by an alternative node.
 * It contains an index identifying the correct child, which state it also
 * contains. The reason for existence of this class to make a reasonable estimate
 * of the entire state signature and keep the signature size as small as possible.
 * The problem is caused by the DeterministicNode used in higher levels of 
 * parse trees, where the signatures of superstates (created as o consequence of
 * "deterministing" of the PT automaton.
 * Please note that this is not an optimization, it is necessary for the signature
 * principle to work.
 */
public class DenotedState extends State {
    
    /**
     * Creates a new instance of DenotedState.
     * 
     * @param index index of the subtree which is the state related to
     * @param state the state of a subtree
     * @param children log(number of children of this state) 
     */
    public DenotedState(int index, State state, int children, AlternativeNode root) {
        this.index = index;
        this.state = state;
        this.children = children;
        this.root = root;
    }
        
    /**
     * @see org.objectweb.fractal.bpc.checker.state.State#createSignature()
     */
    protected void createSignature() {
        boolean acc = root.isAccepting(this) | index == -2;
        if (index == -1)             
            for (int i = 0; i < children; ++i)
                signature.set(i, true);		// the signature has a slot for -1 = 11..11
        
        
        
        else if (acc && !this.hasAncestors())
            for (int i = 0; i < children; ++i)
                signature.set(i, false);     // the signature has a slot for 0 = 00..00
                                             // note here that it is safe and cannot colide with
                                             // a "real" signature from a non-accepting state.
            
        else {
            for (int i = 0; i < children; ++i)
                signature.set(i, (index & (1 << i)) != 0 ? true : false);
                signature.concat(state.getSignature());
        }
    }
    
    /**
     * @see org.objectweb.fractal.bpc.checker.state.State#equals(org.objectweb.fractal.bpc.checker.state.State)
     */
    public boolean equals(State another) {
        if (!(another instanceof DenotedState))
            return false;

        DenotedState dstate = (DenotedState) another;

        if (this.index != dstate.index)
            return false;

        if (this.signature == null)
            this.getSignature();

        if (another.signature == null)
            another.getSignature();

        return this.getSignature().equals(dstate.getSignature());
    }

    /**
     * Checks the current state for ancestors.
     * @return true iff there is at least one ancestor of this state or in strange cases (of Exceptions).
     */
    private boolean hasAncestors() {
        try {
            return  root.getTransitions(this).transitions.length > 0;
        }
        catch (InvalidParameterException e) {
            return true;
        }
        catch (CheckingException e) {
            return true;
        }
    }
    
    /**
     * Index of the subtree to which the state is related
     */
    final public int index;
    
    /**
     * State of a subtree
     */
    final public State state;
    
    /**
     * Number of the children - necessary for signature construction
     */
    private int children;
    
    /**
     * Root node of the corresponding parse subtree
     * Needed for evaluating, whether a state is accepting and setting its signature appropriately.
     */
    private AlternativeNode root;
    

}
