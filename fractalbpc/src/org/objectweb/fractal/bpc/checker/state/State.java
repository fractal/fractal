/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 */


package org.objectweb.fractal.bpc.checker.state;


/**
 * This is abstract class representing a general state of the automaton
 */
abstract public class State implements Comparable {

    /** Creates a new instance of State */
    public State() {
        this.signature = new Signature();
        this.signatureInit = false;
        this.label = "<no_label>";
        this.cycleStart = false;
        //this.signature.accpetingCycleId = -1;
        //this.acceptingReachable = false;
        this.cycleTrace = null;
    }

    /**
     * Comparison of references to State instances. The comparison of states is
     * based on comparison of their signatures denoting the internal structure
     * of the states
     */
     public boolean equals(State another) {
         return this.getSignature().equals(another.getSignature());
     }


    /**
     * @return the signature as the string Note that the entire buffer is
     *         returned
     */
    public Signature getSignature() {
        if (!this.signatureInit) {
            createSignature();
            signatureInit = true;
        }

        //Debug.println("Signature: " + this.signature);
        return this.signature;
    }
    
    /**
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Object o) {
        if (o instanceof State) {
            return this.getSignature().compareTo(((State)o).getSignature());
        }
        else
            return -1;
    }
    
    
    /**
     * 
     * @return true if this state is accpeting reachable
     */
    public boolean isAcceptingReachable() {
        return ((signature.acceptingCycleId >> 63) & 1) != 0;
    }

    /**
     * Creates the signature for this state if needed
     */
    abstract protected void createSignature();
    
    

    //--------------------------------------------------------------------------
    /**
     * Signature of the state. The signature is compute from the substates as
     * string concatenation and prepending the state type sign.
     */
    protected Signature signature;

    /**
     * The label of the state corresponding with the automaton state from
     * visualization.
     */
    public String label;

    /**
     * flag for the start of a cycle
     */
    public boolean cycleStart;

    /**
     * timestamp of this when it was explored for the first time
     */
    public long timestamp;

    /**
     * the trace of the cycle if this is the start point
     */
    public String cycleTrace;
    
    /**
     * Is this.signature already initialized and evaluated?
     */
    private boolean signatureInit;
    
}

