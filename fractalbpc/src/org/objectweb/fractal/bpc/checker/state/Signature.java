/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 */

package org.objectweb.fractal.bpc.checker.state;

/**
 * This class represent the siganture of a state. The nested states do share the
 * same signature array, and differ only in the indices.
 */
public class Signature extends BitList {
    /**
     * The ctor creates the new instance of Signature.
     */
    public Signature() {
        super(size);
        acceptingCycleId = Long.MAX_VALUE;
    }

    /**
     * Sets the default size for the buffer - all sinatures will
     * have the same maximal size.
     * @param _size new size
     */
    public static void setSize(int _size) {
        size = _size;
    }
    
    /**
     * Getter method for signature size.
     * @return the size of all signatures within this run
     */
    public static int getSize() {
        return size;
    }

    /**
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object o) {
        if (o instanceof Signature) 
            return (super.equals(o)); /*&& ((this.acceptingCycleId >>> 63) & 1) == ((((Signature)o).acceptingCycleId >>> 63) & 1);*/
        else
            return false;
            
    }
    
    /**
     * Tests whether this state is accepting reachable.
     */
    public boolean isAcceptingReachable() {
        return ((acceptingCycleId >>> 63) & 1) != 0;
    }
    
    /**
     * Sets this state accepting reachable.
     */
    public void setAcceptingReachable(boolean accepting) {
        if (accepting)
            acceptingCycleId |= ((long)1 << 63);
        else
            acceptingCycleId &= (~((long)1 << 63));
    }
    
    /**
     * Sets the cycle id.
     * @param id cycle id
     */
    public void setCycleId(long id) {
        acceptingCycleId = id | (((acceptingCycleId >>> 63) & 1) << 63);
    }
    
    /**
     * Gets the cycle id.
     * @return the cycle id.
     */
    public long getCycleId() {
        return acceptingCycleId & (~((long)1 << 63));
    }
    
    /**
     * concatenates two signatures
     * @param second the signature to be added to this
     */
    public void concat(Signature second) {
        super.concat(second);
    }
    
    /**
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return super.toString();
    }

    //--------------------------------------------------------------------

    /**
     * the maximal size of all buffers
     */
    static private int size = 0;

    /**
     * denotes that the state with this signature can reach
     * an accepting state
     */
    public long acceptingCycleId;
    
}

