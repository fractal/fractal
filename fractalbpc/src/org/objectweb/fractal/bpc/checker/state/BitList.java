/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 * 
 */


package org.objectweb.fractal.bpc.checker.state;

/**
 * This class implements a simple bit list.
 * 
 */
public class BitList implements Comparable {
    /**
     * Creates a new instance of BitList.
     * @param size size of the bitlist
     */
    public BitList(int size) {
        buffer = new int[1];
        max = size;
        length = 0;
    }
    
    /**
     * Sets a bit on position to the specified value.
     * @param position position of the bit to be set
     * @param bit value (true = 1, false = 0);
     */
    public void set(int position, boolean bit) {
        int idx = position >> 5;
        int off = position & 0x1F;
        
        if (idx >= buffer.length) {
            // need for reallocation
            int size = buffer.length * 2;
            //if (size > max)
            //    size = max;
            
            int[] newbuffer = new int[size];
            
            for (int i = 0; i < buffer.length; ++i)
                newbuffer[i] = buffer[i];
            
            buffer = newbuffer;
        }
        
        if (bit)
            buffer[idx] = buffer[idx] | (1 << (31 - off));
        else
            buffer[idx] = buffer[idx] & ~(1 << (31 - off));
        
        if (position >= length)
            length = position + 1;
    }
    
    /**
     * Gets the value of the bit at specified position.
     * @param position position of the bit 
     * @return the value of the bit at the specified position
     */
    public boolean get(int position) {
        int idx = position >> 5;
        int off = position & 0x1F;
        
        return ((buffer[idx] & (1 << off)) == 0) ? true : false;
    }
    
    /**
     * Compare to other BitList.
     * @param o the other bitlist
     * @return @see java.lang.Object.compareTo(Object)
     */
    public int compareTo(Object o) {
        if (!(o instanceof BitList))
            return -1;
        
        BitList other = (BitList)o;
        
        int min = this.buffer.length < other.buffer.length ? this.buffer.length : other.buffer.length;
        
        for (int i = 0; i < min; ++i) {
            if (this.buffer[i] < other.buffer[i])
               return -1;
            if (this.buffer[i] > other.buffer[i])
                return 1;
        }
        
        if (this.length < other.length)
            return -1;

        if (this.length > other.length)
            return 1;
        
        return 0;
    }
    
    
    /**
     * @see java.lang.Object#equals(Object)
     */
    public boolean equals(Object o) {
        if (!(o instanceof BitList))
            return false;
        
        BitList other = (BitList)o;
        
        if (this.length != other.length)
            return false;

        int min = this.buffer.length;
        
        for (int i = 0; i < min; ++i) 
            if (this.buffer[i] != other.buffer[i])
               return false;

        return true;        
    }
    
    /**
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        int result = 0;
        
        for (int i = 0; i < buffer.length; ++i)
            result = result ^ (buffer[i] << i);
        
        return result;
    }
    
    /**
     * Adds the given signature to this.
     * @param second the signature to be concatenated.
     */
    public void concat(BitList second) {
        int thisidx = this.length >> 5;
        int thisoff = this.length & 0x1F;
        int secondleft = second.length;
        
        
        int needed = ((this.length + second.length) >> 5) + ((((this.length + second.length) & 0x1F) != 0) ? 1 : 0);
        

        if (needed >= buffer.length) {
            // need for reallocation
            int size = (buffer.length * 2) > needed ? (buffer.length * 2) : needed;
            //if (size > max)
            //    size = max;
            
            int[] newbuffer = new int[size];
            
            for (int i = 0; i < buffer.length; ++i)
                newbuffer[i] = buffer[i];
            
            buffer = newbuffer;
        }
        
        
        int i = 0;
        
        while (secondleft > 0) {
            this.buffer[thisidx + i] |= (second.buffer[i] >>> thisoff);
            if ((secondleft - 32 + thisoff > 0) && (thisoff > 0))
                this.buffer[thisidx + i + 1] |= (second.buffer[i] << (32 - thisoff));
            
            ++i;
            secondleft -= 32;
        }
        
        this.length += second.length;
    }

    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
	    String s = new String();
	    
	    for (int i = 0; i < this.length; ++i)
	        s += ((buffer[i >> 5] & (1 << (31 - i))) != 0) ? "1" : "0";  
	    
	    return s;
    }
    

    /**
     * Gets the buffer of the bitlist
     * @return the buffer of the bitlist
     */
    public int[] getBuffer() {
        return buffer;
    }
    
    //-----------------------------------------------------------------------------
    /**
     * the buffer with the bitlist
     */
    private int[] buffer;
    
    /**
     * max size of the buffer
     */
    private int max;
    
    /**
     * index of the highest valid bit
     */
    private int length;
    
}
