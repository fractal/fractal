/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 */


package org.objectweb.fractal.bpc.checker.visualization;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;

import org.objectweb.fractal.bpc.checker.node.*;
import org.objectweb.fractal.bpc.checker.parser.Debug;

/**
 * This class creates a dot-visualization of given parse tree.
 */
public class DotVisualizer {

    /** Creates a new instance of DotVisualizer */
    public DotVisualizer(ActionRepository repository) {
        this.repository = repository;
        this.legend = new String();
    }

    /**
     * Outputs the tree into image
     * 
     * @return 0 if no error occur, else non-zero
     */
    public int visualize(TreeNode node, String outputfile) {
        try {
            explicitidx = 0;
            legend = "";
            FileOutputStream of = new FileOutputStream(outputfile);

            of.write("digraph G {\n  size = \"7,11\";\n".getBytes());

            visualizeNode(node, of, 0);

            of.write(("  label=\"" + legend + "\";\n").getBytes());

            of.write("\n}".getBytes());
            of.close();
            Debug.println("Parse tree written to: " + outputfile);

            return 0;
        } catch (FileNotFoundException e) {
            return 1;
        } catch (SecurityException e) {
            return 2;
        } catch (IOException e) {
            return 3;
        }
    }

    /**
     * This method writes down information about single node and call itself for
     * its children.
     * 
     * @return ErrorCode, 0 if no error occur
     */
    private int visualizeNode(TreeNode node, FileOutputStream of, int nodeindex) throws IOException {
        String[] nodenames = node.getTypeName();
        String nodename = nodenames[0];
        TreeNode[] children;

        String purename = new String(nodename);

        nodename = nodename + nodeindex++;

        if (purename.equals("Event")) {
            nodenames[1] = repository.getItemString(((EventNode) node).getEventIndex());
            of.write(("  " + nodename + " [label=\"" + nodenames[1] + "\", fontname=\"Courier\"];\n").getBytes());
        } else if (purename.equals("Explicit")) {
            of.write(("  " + nodename + " [label=\"Exp" + explicitidx + "\", fontname=\"Courier\", shape=invhouse, style=filled, fillcolor=\"grey85\"];\n").getBytes());
            legend += "Exp" + explicitidx + ": " + node.protocol + "\\n";

            ++explicitidx;
        } else
            of.write(("  " + nodename + " [label=\"" + nodenames[1] + "\", fontname=\"Courier-Bold\"];\n").getBytes());

        children = node.getChildren();

        for (int i = 0; i < children.length; ++i) {
            String[] childnames = children[i].getTypeName();
            String childname = childnames[0];

            childname = childname + nodeindex;
            of.write(("\t" + nodename + " -> " + childname + ";\n").getBytes());
            nodeindex = visualizeNode(children[i], of, nodeindex);

        }

        return nodeindex;
    }

    //-----------------------------------------------------------------------------
    /**
     * The action repository to visualize the event nodes
     */
    private ActionRepository repository;

    /**
     * String for explicit automata legend
     */
    private String legend;

    /**
     * Explicit automata index
     */
    private int explicitidx;

}

