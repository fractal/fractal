/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 */

package org.objectweb.fractal.bpc.checker.DFSR;

import org.objectweb.fractal.bpc.checker.node.*;
import org.objectweb.fractal.bpc.checker.state.*;


/**
 * This interface defines the methods to be implemented by all extensions used by the DFSRtraverser.
 */
public interface IDFSRExtension {

    /**
     * This method is called as the first each time a node is visited.
     */
    public void action(State state) throws InvalidParameterException;

    /**
     * This method is called if a new state is visited.
     * 
     * @return true or false depending on the implementation
     */
    public boolean actionNew(State state) throws InvalidParameterException, CheckingException;

    /**
     * This method is called if a cycle is detected while traversing the state graph. 
     * Note that the actionVisited is not called.
     */
    public void actionCycle(State state) throws InvalidParameterException;

    /**
     * This method is called if an already visited node is being visited. Note
     * that ActionCycle method is not called.
     */
    public void actionVisited(State state) throws InvalidParameterException;

    /**
     * This method is called while returning from the state back to its parent.
     */
    public void actionBack(State state) throws InvalidParameterException;
    
    /**
     * resets the state of the extension to the beginning
     */
    public void reset();
    
}
