/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 */


package org.objectweb.fractal.bpc.checker.DFSR;


import java.io.FileOutputStream;
import java.io.IOException;

import org.objectweb.fractal.bpc.checker.node.ActionRepository;
import org.objectweb.fractal.bpc.checker.node.InvalidParameterException;
import org.objectweb.fractal.bpc.checker.node.TreeNode;
import org.objectweb.fractal.bpc.checker.state.State;
import org.objectweb.fractal.bpc.checker.state.TransitionPair;

/**
 * This class produces an output for dot visualization tool (part of GraphViz by Att).
 * The visualizied subject is the transition graph represented by a TreeNode
 *  
 */
public class DFSRDotVis implements IDFSRExtension {

    /**
     * Creates a new instance of DFSRDotVis.
     * 
     * @param outputfile the name of the output file
     * @param automaton automaton to be visualized
     * @param repository an instance of ActionRepository for printing the action name.
     */
    public DFSRDotVis(FileOutputStream outputfile, TreeNode automaton, ActionRepository repository) {
        this.outputfile = outputfile;
        this.automaton = automaton;
        this.repository = repository;
    }

    /**
     * @see org.objectweb.fractal.bpc.checker.DFSR.IDFSRExtension#action(org.objectweb.fractal.bpc.checker.state.State)
     */
    public void action(State state) throws InvalidParameterException {
    }

    /**
     * @see org.objectweb.fractal.bpc.checker.DFSR.IDFSRExtension#actionNew(org.objectweb.fractal.bpc.checker.state.State)
     */
    public boolean actionNew(State state) throws InvalidParameterException, CheckingException {
        state.label = new String("S" + timestamp);

        try {
            // the state
            boolean acc = automaton.isAccepting(state);
            State initstate = automaton.getInitial();
            boolean init = initstate.getSignature().equals(state.getSignature());
            if (acc)
                outputfile.write(("  \"" + state.getSignature() + "\" [label=\"S" + timestamp + "\", shape=doublecircle, style=filled, fillcolor=\"grey\"];\n").getBytes());
            else
                outputfile.write(("  \"" + state.getSignature() + "\" [label=\"S" + timestamp + "\"];\n").getBytes());

            if (init)
                if (acc)
                    outputfile.write(("  \"" + state.getSignature() + "\" [shape=box, peripheries=2, style=filled, fillcolor=\"grey\"];\n").getBytes());
                else
                    outputfile.write(("  \"" + state.getSignature() + "\" [shape=box];\n").getBytes());

            //... and its transitions
            TransitionPair[] trans = automaton.getTransitions(state).transitions;
            for (int i = 0; i < trans.length; ++i) {
                outputfile.write(("  \"" + state.getSignature() + "\" -> \"" + trans[i].state.getSignature() + "\" [label=\"" + repository.getItemString(trans[i].eventIndex) + "\"];\n").getBytes());
                //if (trans[i].state.label.equals("<no_label>"))
                //    outputfile.write(("  \"" + trans[i].state.getSignature() + "\" [label=\".\"];\n").getBytes());
            }

        } catch (IOException e) {
            throw new AcceptingStateException("Visualization backend: error while writing to the file.");
        }

        state.timestamp = timestamp++;

        
        return automaton.isAccepting(state);
    }

    /**
     * @see org.objectweb.fractal.bpc.checker.DFSR.IDFSRExtension#actionCycle(org.objectweb.fractal.bpc.checker.state.State)
     */
    public void actionCycle(State state) throws InvalidParameterException {
        if (state.getSignature().getCycleId() == Long.MAX_VALUE) {
            state.cycleStart = true;
            state.getSignature().setCycleId(state.timestamp);
        }
    }

    /**
     * @see org.objectweb.fractal.bpc.checker.DFSR.IDFSRExtension#actionVisited(org.objectweb.fractal.bpc.checker.state.State)
     */
    public void actionVisited(State state) throws InvalidParameterException {
    }

    /**
     * @see org.objectweb.fractal.bpc.checker.DFSR.IDFSRExtension#actionBack(org.objectweb.fractal.bpc.checker.state.State)
     */
    public void actionBack(State state) throws InvalidParameterException {
        try {
            if (state.isAcceptingReachable()) {
                outputfile.write(("  \"" + state.getSignature() + "\" [style=filled, fillcolor=\"grey\"];\n").getBytes());
            }
        } catch (IOException e) {
        }

    }
    
    /** 
     * @see org.objectweb.fractal.bpc.checker.DFSR.IDFSRExtension#reset()
     */
    public void reset() {
        timestamp = 0;
    }

    /**
     * File where the description of the automata is stored
     *  
     */
    private FileOutputStream outputfile;

    /**
     * State index
     */
    private int timestamp = 0;

    /**
     * The root if the parse tree.
     */
    private TreeNode automaton;

    /**
     * Action repository.
     */
    private ActionRepository repository;

}
