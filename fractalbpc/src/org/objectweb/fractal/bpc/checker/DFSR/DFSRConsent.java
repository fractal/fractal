/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.fractal.bpc.checker.DFSR;

import org.objectweb.fractal.bpc.checker.node.ConsentNode;
import org.objectweb.fractal.bpc.checker.node.InvalidParameterException;
import org.objectweb.fractal.bpc.checker.state.State;


/**
 * This class perform the checking operation in each state.
 * It is called from the DFSRTraverser that implements the generic DFS algorithm.
 */
public class DFSRConsent implements IDFSRExtension {

    /**
     * Creates a new instance of DFSRConsent.
     * @param automaton the root of the parse tree
     */
    public DFSRConsent(ConsentNode automaton) {
        this.automaton = automaton;
    }

    /**
     * @see org.objectweb.fractal.bpc.checker.DFSR.IDFSRExtension#action(org.objectweb.fractal.bpc.checker.state.State)
     */
    public void action(State state) throws InvalidParameterException {

    }

    /**
     * @see org.objectweb.fractal.bpc.checker.DFSR.IDFSRExtension#actionNew(org.objectweb.fractal.bpc.checker.state.State)
     */
    public boolean actionNew(State state) throws InvalidParameterException, AcceptingStateException {
        state.label = new String("S" + timestamp);
        state.timestamp = timestamp++;
        return automaton.isAccepting(state);
    }

    /**
     * @see org.objectweb.fractal.bpc.checker.DFSR.IDFSRExtension#actionCycle(org.objectweb.fractal.bpc.checker.state.State)
     */
    public void actionCycle(State state) throws InvalidParameterException {
        if (state.getSignature().getCycleId() == Long.MAX_VALUE) {
            state.cycleStart = true;
            state.getSignature().setCycleId(state.timestamp);
        }
    }

    /**
     * @see org.objectweb.fractal.bpc.checker.DFSR.IDFSRExtension#actionVisited(org.objectweb.fractal.bpc.checker.state.State)
     */
    public void actionVisited(State state) throws InvalidParameterException {

    }

    /**
     * @see org.objectweb.fractal.bpc.checker.DFSR.IDFSRExtension#actionBack(org.objectweb.fractal.bpc.checker.state.State)
     */
    public void actionBack(State state) throws InvalidParameterException {

    }
    
    /**
     * @see org.objectweb.fractal.bpc.checker.DFSR.IDFSRExtension#reset()
     */
    public void reset() {
        timestamp = 0;
    }

    //------------------------------
    /**
     * unique cycle id generator
     */
    private long timestamp = 0;

    /**
     * The automaton represented by the consent node of the parse tree.
     */
    private ConsentNode automaton;
}
