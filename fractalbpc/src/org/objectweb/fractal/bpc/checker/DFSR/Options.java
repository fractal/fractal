/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright 2004
 *    Distributed Systems Research Group
 *    Department of Software Engineering
 *    Faculty of Mathematics and Physics
 *    Charles University, Prague
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 */

package org.objectweb.fractal.bpc.checker.DFSR;

import org.objectweb.fractal.bpc.checker.parser.Debug;

import com.martiansoftware.jsap.*;
import com.martiansoftware.jsap.stringparsers.IntegerStringParser;
import com.martiansoftware.jsap.stringparsers.StringStringParser;

/**
 * This class contains the options for the checker program.
 */
public class Options {
    /**
     * Visualization type constants
     */
    public static final int DOTVISUALIZATION = 1;

    /**
     * Action constants
     */
    public static final int ACTIONTESTCOMPLIANCE = 1;

    public static final int ACTIONTESTCONSENT = 2;

    public static final int ACTIONVISUALIZE = 3;
    
    /**
     * infinite activity constant
     */
    public static final int IAYES = 0;
    
    public static final int IANO = 1;
    
    public static final int IANOTRACE = 2;
    
    
    /**
     * Action type - testing or visualization.
     */
    public static int action = ACTIONTESTCOMPLIANCE;

    /**
     * Multinodes optimization
     */
    public static boolean multinodes = true;

    /**
     * Forward cutting optimization
     */
    public static boolean forwardcutting = true;

    /**
     * Explicit automata optimization
     */
    public static boolean explicit = false;

    public static int explicitsize = 1000;

    /**
     * The total amount of memory available to the checker data in MB Note that
     * the program itself need additional 10 - 15 MB for itself.
     */
    public static int totalmemory = 60;

    /**
     * Minimal amount of memory dedicated to the state cache in MB
     */
    public static int statememory = 48;

    /**
     * Visualization option - visualization of the tree 0 stands for no
     * visualization
     */
    public static int visualizationtype = 0;
    
    /**
     * Types of checking while using the consent operator, default = true
     */
    public static boolean badactivity = true;
    public static boolean noactivity = true;
    public static int infiniteactivity = 1;
    
    /**
     * The name of a file containing the input protocols
     */
    public static String inputfile = "";
    
    /**
     * Monitor protocol usage?
     */
    public static boolean monitorusage = false;
    
    /**
     * Parses the command line
     * 
     * @param args
     *            Command-line arguments to be parsed
     */
    public static String[] parseCommandLine(String[] args) {
        JSAP jsap;
        JSAPResult res;
        
        try {
	        jsap = new JSAP();
	        IntegerStringParser isp = JSAP.INTEGER_PARSER;
	        StringStringParser ssp = JSAP.STRING_PARSER;
	        
	        jsap.registerParameter(new FlaggedOption("action", ssp, "test", JSAP.REQUIRED, 'a', "action" ) );
	        jsap.registerParameter(new Switch("disablemultinodes",'m',"nomultinodes") );
	        jsap.registerParameter(new Switch("disableexplicit",'e',"noexplicit") );
	        jsap.registerParameter(new Switch("disableforwardcut",'c',"noforwardcut") );
	        jsap.registerParameter(new Switch("disablebadactivity", 'b', "nobadactivity"));
	        jsap.registerParameter(new Switch("disablenoactivity", 'n', "nonoactivity"));
	        jsap.registerParameter(new FlaggedOption("verbose", ssp, "0", JSAP.NOT_REQUIRED, 'v', "verbose" ) );
	        jsap.registerParameter(new FlaggedOption("infiniteactivity", ssp, "notrace", JSAP.NOT_REQUIRED, 'i', "infiniteactivity"));
	        jsap.registerParameter(new FlaggedOption("file", ssp, "", JSAP.NOT_REQUIRED, 'f', "file" ) );

	        jsap.registerParameter(new FlaggedOption("totalmem", isp, "60", JSAP.NOT_REQUIRED, 't', "totalmem" ) );
	        jsap.registerParameter(new FlaggedOption("cachesize", isp, "48", JSAP.NOT_REQUIRED, 's', "cachesize" ) );
	        
	        UnflaggedOption parameters = new UnflaggedOption("protocolsetc", ssp, "", true, true);

	        jsap.registerParameter(parameters);
	        
	        res = jsap.parse(args);
	        
	        if (!res.success())
	            return null;
	        
        } catch (Exception e) {
            return null;
        }
        
        String _action = res.getString("action");
        
        if (_action.equals("test"))
            action = ACTIONTESTCOMPLIANCE;
        else if (_action.equals("testconsent"))
            action = ACTIONTESTCONSENT;
        //else if (_action.equals("testnoconsent"))
        //    action = ACTIONTESTNOCONSENT;
        else if (_action.equals("visualizedot")) {
            action = ACTIONVISUALIZE;
            visualizationtype = DOTVISUALIZATION;
        }
        else
            return null;
        
        
        if (res.getBoolean("disablemultinodes"))
            multinodes = false;
        
        if (res.getBoolean("disableexplicit"))
            explicit = false;
        
        if (res.getBoolean("disableforwardcut"))
            forwardcutting = false;
        
        if (res.getBoolean("disablebadactivity"))
            badactivity = false;
        
        if (res.getBoolean("disablenoactivity"))
            noactivity = false;
        
        String ia = res.getString("infiniteactivity");
        
        if (ia.equals("yes"))
            infiniteactivity = IAYES;
        else if (ia.equals("no"))
            infiniteactivity = IANO;
        else if (ia.equals("notrace"))
            infiniteactivity = IANOTRACE;
        else
            return null;
        
        String verbose = res.getString("verbose");
        
        if (verbose.equals("0"))
             Debug.setLevel(0);
        else if (verbose.equals("1"))
            Debug.setLevel(2);
        else if (verbose.equals("2"))
            Debug.setLevel(3);
        else
            return null;

        totalmemory = res.getInt("totalmem");
        statememory = res.getInt("cachesize");
        
        Options.inputfile = res.getString("file");
        
        return res.getStringArray("protocolsetc");
    }
}
