/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 *
 */
package org.objectweb.fractal.bpc.checker.DFSR;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Stack;

import org.objectweb.fractal.bpc.checker.node.ActionRepository;
import org.objectweb.fractal.bpc.checker.node.InvalidParameterException;
import org.objectweb.fractal.bpc.checker.node.TreeNode;
import org.objectweb.fractal.bpc.checker.state.State;
import org.objectweb.fractal.bpc.checker.state.TransitionPair;
import org.objectweb.fractal.bpc.checker.utils.ProtocolUsageMonitor;

public class JPFTraverser {
    /**
     * Creates a new instance of DFSRTraverser.
     * 
     * @param node the root of the parse tree
     * @param repository the action repository for printing the action names.
     */
    public JPFTraverser(TreeNode node, ActionRepository repository, PrintStream output) {
        this.node = node;

        this.eventStack = new Stack();
        this.stateStack = new Stack();
        this.repository = repository;
        this.output = output;
        
        current = node.getInitial();
        
    }
    
    /**
     * Notifies the checker module about actions from JPF
     * @param thread the thread id, ignored for now
     * @param event the event in form iface.method
     * @return true if the event can be performed
     */
    public boolean notifyForwardInvoke(int thread, boolean isClientItf, String event) {
        String token;
        
        token = (isClientItf ? "!" : "?") + event + "^";
        
        return makeStep(token);
    }
    
    /**
     * Notifies the checker module about actions from JPF
     * @param thread the thread id, ignored for now
     * @param event the event in form iface.method
     * @return true if the event can be performed
     */
    public boolean notifyForwardReturn(int thread, boolean isClientItf, String event) {
        String token;
        
        token = (isClientItf ? "?" : "!") + event + "$";
        
        return makeStep(token);
    }
    
    /**
     * Notifies the checker module about actions from JPF
     * @param thread the thread id, ignored for now
     * @param event the event in form iface.method
     * @return true if the event can be performed
     */
    public boolean notifyBackwardInvoke(int thread, boolean isClientItf, String event) {
        String token;
        
        token = (isClientItf ? "!" : "?") + event + "^";
        
        return undoStep(token);
    }
    
    /**
     * Notifies the checker module about actions from JPF
     * @param thread the thread id, ignored for now
     * @param event the event in form iface.method
     * @return true if the event can be performed
     */
    public boolean notifyBackwardReturn(int thread, boolean isClientItf, String event) {
        String token;
        
        token = (isClientItf ? "?" : "!") + event + "$";
        
        return undoStep(token);
    }
    
    /**
     * @return true if the protocol is in its final state
     */
    public boolean notifyEnd() {
        return node.isAccepting(current);
    }
    
    
    /**
     * This shouldn't be called here
     * @return nothing
     */
    public boolean wantsBacktrack() {
        throw new UnsupportedOperationException();
    }
    
    
    /**
     * Writes a html file with protocol usage 
     * @param out the output stream to write to 
     * @throws IOException in case of an error when writing to the stream
     */
    public void writeUsageFile(OutputStream out) throws IOException {
        ProtocolUsageMonitor.printUsage(out, node);
    }

    
    /**
     * Performs a forward step instrumented by JPF
     * @param event the event to be processed.
     * @return true if the event can occur according to the frameprotocol
     */
    protected boolean makeStep(String event) {
        boolean found = false;
        //extract the index of this event
        int action;
        
        //System.err.println("Forward: " + event);
        
        try {
              action = repository.getItemIndex(event);
          }
          catch (NullPointerException e) {
              printStack(event);
              return false;
          }
            
          TransitionPair[] transitions = null;
    
          // obtain all possible transitions
          try {
              transitions = node.getTransitions(current).transitions;
          } catch (InvalidParameterException e) {
              //shouldn't appear
              output.println("Internal Checker Error - InvalidParameterException");
              e.printStackTrace();
              return false;
          } catch (CheckingException e) {
              //shouldn't appear
              output.println("Internal Checker Error - CheckingException");
              e.printStackTrace();
              return false;
          }
    
          //find the real transitions within the set of possible transitions
          for (int i = 0; i < transitions.length; ++i) {
              //System.out.println("transition " + repository.getItemString(transitions[i].eventIndex));
              if (transitions[i].eventIndex == action) {
                  eventStack.push(event);
                  stateStack.push(current);
                  current = transitions[i].state;
                  found = true;
                  if (Options.monitorusage)
                      ProtocolUsageMonitor.eventExecuted(transitions[i].eventIndex);

                  break;
              }
          }
    
          if (!found) {// bad activity
              printStack(event);
              return false;
          }
          else 
              return true;
         
    }
    
    /**
     * Performs a backward step instrumented by JPF
     * @param event the event to be backtracked
     * @return true if the event is on the top of the pathstack
     */
    protected boolean undoStep(String event) {
        
        //System.err.println("Backward: " + event);

        if (eventStack.peek().equals(event)) {
            eventStack.pop();
            current = (State)stateStack.peek();
            stateStack.pop();
            return true;
        }
        else {
            printStack(event);
            return false;
        }
    }
    
    
    protected void printStack(String badEvent) {
        if (!wasOutput) {
            wasOutput = true;
            output.println("Protocol error trace start");
            for (int i = 0; i < eventStack.size(); ++i) {
                output.println((String)eventStack.get(i));
            }
			output.println(badEvent);
            output.println("Protocol error trace end");
            
        }
    }
    
    //----------------------------------------------------------------------------
    /**
     * The top of the parse tree
     */
    protected TreeNode node;
    
    /**
     * Action repository
     */
    protected ActionRepository repository;
    
    /**
     * stack of actions on current trace
     */
    protected Stack eventStack;
    
    /**
     * State stack
     */
    protected Stack stateStack;

    
    /**
     * The current state
     */
    protected State current;
    
    /**
     * tag variable for error trace output
     */
    protected boolean wasOutput = false;
    
    /**
     * Stream for printing messages
     */
    protected PrintStream output;

}
