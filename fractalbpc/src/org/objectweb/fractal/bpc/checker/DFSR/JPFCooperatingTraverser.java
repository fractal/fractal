/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 *
 */
package org.objectweb.fractal.bpc.checker.DFSR;

import java.io.PrintStream;

import org.objectweb.fractal.bpc.checker.node.ActionRepository;
import org.objectweb.fractal.bpc.checker.node.InvalidParameterException;
import org.objectweb.fractal.bpc.checker.node.TreeNode;
import org.objectweb.fractal.bpc.checker.state.State;
import org.objectweb.fractal.bpc.checker.state.TransitionPair;
import org.objectweb.fractal.bpc.checker.utils.ProtocolUsageMonitor;

public class JPFCooperatingTraverser extends JPFTraverser {

    public JPFCooperatingTraverser(TreeNode node, ActionRepository repository, PrintStream output) {
        super(node, repository, output);

        this.globalCache = new StateCache(0);
    }
    
    /**
     * @return true if the current state is an already visited one
     * 
     * @see org.objectweb.fractal.bpc.checker.DFSR.JPFTraverser#wantsBacktrack()
     */
    public boolean wantsBacktrack() 
    {
        if (this.eventStack.empty()) return true;
        
        return visitedState || node.isAccepting(current);
    }
    
    
    /**
     * Performs a forward step instrumented by JPF
     * @param event the event to be processed.
     * @return true if the event can occur according to the frameprotocol
     */
    protected boolean makeStep(String event) {
        boolean found = false;
        //extract the index of this event
        int action;
        
        //System.err.println("Forward: " + event);
        
        try {
              action = repository.getItemIndex(event);
          }
          catch (NullPointerException e) {
              printStack(event);
              return false;
          }
            
          TransitionPair[] transitions = null;
    
          // obtain all possible transitions
          try {
              transitions = node.getTransitions(current).transitions;
          } catch (InvalidParameterException e) {
              //shouldn't appear
              output.println("Internal Checker Error - InvalidParameterException");
              e.printStackTrace();
              return false;
          } catch (CheckingException e) {
              //shouldn't appear
              output.println("Internal Checker Error - CheckingException");
              e.printStackTrace();
              return false;
          }
    
          //find the real transitions within the set of possible transitions
          for (int i = 0; i < transitions.length; ++i) {
              
              if (transitions[i].eventIndex == action) {
                  eventStack.push(event);
                  stateStack.push(current);
                  current = transitions[i].state;

                  visitedState = false;
                  try {
                      globalCache.insert(current.getSignature());
                  } catch (ItemAlreadyPresentException e) {
                      // ok, nothing bad is happening
                      visitedState = true;
                  }
                  
                  found = true;
                  if (Options.monitorusage)
                      ProtocolUsageMonitor.eventExecuted(transitions[i].eventIndex);

                  break;
              }
          }
    
          if (!found) {// bad activity
              printStack(event);
              return false;
          }
          else 
              return true;
         
    }
    
    /**
     * Performs a backward step instrumented by JPF
     * @param event the event to be backtracked
     * @return true if the event is on the top of the pathstack
     */
    protected boolean undoStep(String event) {
        
        //System.err.println("Backward: " + event);

        if (eventStack.peek().equals(event)) {
            eventStack.pop();
            current = (State)stateStack.peek();
            stateStack.pop();
            
            visitedState = true;
            
            return true;
        }
        else {
            printStack(event);
            return false;
        }
    }    
    
    
    /**
     * Set of already visited states
     */
    private StateCache globalCache;
    
    /**
     * Is the current state an already visited state
     */
    private boolean visitedState = false;
    

}
