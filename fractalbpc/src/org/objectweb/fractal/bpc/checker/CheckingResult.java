/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 * 
 */
package org.objectweb.fractal.bpc.checker;

/**
 * This class keeps the checking results.
 */
public class CheckingResult {

    /**
     * Creates a new instance of CheckingResult.
     * @param type type of the error found
     * @param description the description of the composaition error found
     */
    public CheckingResult(int type, String description) {
        this.type = type;
        this.description = description;
    }
    
    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return description;
    }
    
    /**
     * The type of the composition error
     */
    public int type;
    
    /**
     * The description of the composition error
     */
    public String description;
    
    public static final int ERR_OK = 0;
    public static final int ERR_BADACTIVITY = 1;
    public static final int ERR_NOACTIVITY = 2;
    public static final int ERR_INFINITEACTIVITY = 3;
    public static final int ERR_SYNTAXERROR = 4;
    public static final int ERR_OTHERERROR = 5;
    
}
