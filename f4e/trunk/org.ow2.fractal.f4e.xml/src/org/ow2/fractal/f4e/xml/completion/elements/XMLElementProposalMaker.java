package org.ow2.fractal.f4e.xml.completion.elements;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.text.contentassist.CompletionProposal;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.swt.graphics.Image;
import org.eclipse.wst.dtd.core.internal.saxparser.CMGroupNode;
import org.eclipse.wst.dtd.core.internal.saxparser.CMNode;
import org.eclipse.wst.dtd.core.internal.saxparser.CMNodeType;
import org.eclipse.wst.dtd.core.internal.saxparser.CMReferenceNode;
import org.eclipse.wst.dtd.core.internal.saxparser.DTDParser;
import org.eclipse.wst.dtd.core.internal.saxparser.ElementDecl;
import org.eclipse.wst.xml.core.internal.document.ElementImpl;
import org.eclipse.wst.xml.core.internal.document.TextImpl;
import org.ow2.fractal.f4e.xml.F4EXmlEditorPlugin;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class XMLElementProposalMaker {
	DTDParser parser;
	
	public XMLElementProposalMaker(){
		try{
			parser = new DTDParser(false);
			parser.setExpandEntityReferences(false);
			// Get the standard.dtd parse it to create the DTD model of the file
			// it is used after to get informations for completion.
			URL url = F4EXmlEditorPlugin.getDefault().getBundle().getEntry("standard.dtd");
			parser.parse(url.toExternalForm());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	/**
	 * Returns all possible XML elements names that start by the incomplete xmlElementName.
	 * 
	 * @param domDoc
	 * @param offset
	 * @param xmlElementName
	 * @return
	 */
	private ICompletionProposal[] provideXMLElementsNamesProposals( Document domDoc, int offset, String xmlElementName) {		
		List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();
		
		
		if("binding".startsWith(xmlElementName)){
			ICompletionProposal xmlElement = getNodeProposal(offset, "binding", xmlElementName, false,true);
			proposals.add(xmlElement);
		}
		
		if("component".startsWith(xmlElementName)){
			ICompletionProposal xmlElement = getNodeProposal(offset, "component", xmlElementName, false, true);
			proposals.add(xmlElement);
		}
		
		if("content".startsWith(xmlElementName)){
			ICompletionProposal xmlElement = getNodeProposal(offset, "content", xmlElementName, false, true);
			proposals.add(xmlElement);
		}
		
		if("controller".startsWith(xmlElementName)){
			ICompletionProposal xmlElement = getNodeProposal(offset, "controller", xmlElementName, false, true);
			proposals.add(xmlElement);
		}
		
		if("definition".startsWith(xmlElementName)){
			ICompletionProposal xmlElement = getNodeProposal(offset, "definition", xmlElementName, false,true);
			proposals.add(xmlElement);
		}
		
		if("interface".startsWith(xmlElementName)){
			ICompletionProposal xmlElement = getNodeProposal(offset, "interface", xmlElementName, false,true);
			proposals.add(xmlElement);
		}
		
		if("logger".startsWith(xmlElementName)){
			ICompletionProposal xmlElement = getNodeProposal(offset, "logger", xmlElementName, false,true);
			proposals.add(xmlElement);
		}
		
		if("template-controller".startsWith(xmlElementName)){
			ICompletionProposal xmlElement = getNodeProposal(offset, "template-controller", xmlElementName, false,true);
			proposals.add(xmlElement);
		}
		
		if("virtual-node".startsWith(xmlElementName)){
			ICompletionProposal xmlElement = getNodeProposal(offset, "virtual-node", xmlElementName, false,true);
			proposals.add(xmlElement);
		}
		
		return proposals.toArray(new ICompletionProposal[]{});
	}
	
	/**
	 * 
	 * @param node
	 * @return the previous sibling of type ElementImpl
	 */
	private ElementImpl getPreviousSiblingElementType(Node node){
		Node sibling = node.getPreviousSibling();
		if(sibling instanceof ElementImpl){
			return (ElementImpl)sibling;
		}else if(sibling == null){
			return null;
		}else{
			return getPreviousSiblingElementType(sibling);
		}
	}
	
	/**
	 * 
	 * @param node
	 * @return the next sibling of type ElementImpl
	 */
	private ElementImpl getNextSiblingElementType(Node node){
		Node sibling = node.getNextSibling();
		if(sibling instanceof ElementImpl){
			return (ElementImpl)sibling;
		}else if(sibling == null){
			return null;
		}else{
			return getNextSiblingElementType(sibling);
		}
	}
	
	/**
	 * 
	 * @param node
	 * @return the parent node of type ElementImpl
	 */
	private ElementImpl getParentElementType(Node node){
		Node sibling = node.getParentNode();
		if(sibling instanceof ElementImpl){
			return (ElementImpl)sibling;
		}else if(sibling == null){
			return null;
		}else{
			return getParentElementType(sibling);
		}
	}
	
	/**
	 * Proposal completion for the following configuration :
	 * </element n> 'cursor position' <element n+1>
	 * </element n> < 'cursor position' <element n+1>
	 * 
	 * @param domDoc
	 * @param offset
	 * @param domElement
	 * @return completion proposal
	 */
	public ICompletionProposal[] makeProposals(Document domDoc, int offset, TextImpl domElement) {
		List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();
		ICompletionProposal xmlElement = null;
		
		Pattern pattern = Pattern.compile("[\\s| ]*(.*)",Pattern.MULTILINE);
		Matcher matcher = pattern.matcher(domElement.getData());
		
		String typedText = null;
		boolean useStartChar = true;
		
		// we skip all space char
		if(matcher.find()){
			typedText = matcher.group(1);
		}
		
		// look if the text start with '<'
		if(typedText != null && typedText.startsWith("<")==true){
			useStartChar = false;
		}
		
		ElementImpl previousSibling = getPreviousSiblingElementType(domElement);
		ElementImpl nextSibling = getNextSiblingElementType(domElement);
		ElementImpl parentNode = getParentElementType(domElement);
		
		String parentNodeName = parentNode!=null?parentNode.getNodeName():null;
		String previousSiblingName = previousSibling!=null?previousSibling.getNodeName():null;
		String nextSiblingName = nextSibling!=null?nextSibling.getNodeName():null;
		
		Object object = null;
		if(parentNodeName != null){
			parser.getElementPool().get(parentNodeName);
		}
		if(object != null){
			ElementDecl elementDecl = (ElementDecl)parser.getElementPool().get(parentNodeName);
			
			CMNode cmnode = elementDecl.getContentModelNode();
			if(cmnode != null && cmnode instanceof CMGroupNode){
				CMGroupNode cmgroupnode = (CMGroupNode)cmnode;
				Vector childs = cmgroupnode.getChildren();
				Iterator iterator = childs.iterator();
				
				boolean passPreviousNode = false;
					if(previousSiblingName == null){
						passPreviousNode = true;
					}
				
				boolean passNextNode = false;
					
				while(iterator.hasNext()){
					Object next = iterator.next();
					if(next instanceof CMReferenceNode){
						CMReferenceNode ref = (CMReferenceNode)next;
						
						
						if(ref.getName().equals(previousSiblingName) && ref.getOccurrence() == CMNodeType.ZERO_OR_MORE){
							passPreviousNode = true;
						}
						if(ref.getName().equals(nextSiblingName) && ref.getOccurrence() == CMNodeType.OPTIONAL){
							passNextNode = true;
						}
						if(passPreviousNode == true && passNextNode==false){
							proposals.add(getNodeProposal(offset,ref.getName(),"",useStartChar,true));
						}
						if(ref.getName().equals(previousSiblingName) && ref.getOccurrence() == CMNodeType.OPTIONAL){
							passPreviousNode = true;
						}
						if(ref.getName().equals(nextSiblingName) && ref.getOccurrence() == CMNodeType.ZERO_OR_MORE){
							passNextNode = true;
						}
					}
				}
			}
		}
		
		return proposals.toArray(new ICompletionProposal[]{});
	}
	
	public ICompletionProposal[] makeProposals(Document domDoc, int offset, ElementImpl domElement) {
		// we check that we are between the '<' and the end of the '<tagname'
		if(offset > domElement.getStartOffset() + domElement.getNodeName().length() + 1){
			return null;
		}
		
		List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();
		ICompletionProposal xmlElement = null;
		
		ElementImpl previousSibling = getPreviousSiblingElementType(domElement);
		ElementImpl nextSibling = getNextSiblingElementType(domElement);
		ElementImpl parentNode = getParentElementType(domElement);
		
		String parentNodeName = parentNode!=null?parentNode.getNodeName():null;
		String previousSiblingName = previousSibling!=null?previousSibling.getNodeName():null;
		String nextSiblingName = nextSibling!=null?nextSibling.getNodeName():null;
		
		//We are at the top
		if(parentNodeName == null && "definition".startsWith(domElement.getNodeName())){
			proposals.add(getNodeProposal(offset,"definition",domElement.getNodeName(),false,false));
			return proposals.toArray(new ICompletionProposal[]{});
		}
		
		Object object = parser.getElementPool().get(parentNodeName);
		if(object != null){
			ElementDecl elementDecl = (ElementDecl)parser.getElementPool().get(parentNodeName);
			
			CMNode cmnode = elementDecl.getContentModelNode();
			if(cmnode != null && cmnode instanceof CMGroupNode){
				CMGroupNode cmgroupnode = (CMGroupNode)cmnode;
				Vector childs = cmgroupnode.getChildren();
				Iterator iterator = childs.iterator();
				
				boolean passPreviousNode = false;
					if(previousSiblingName == null){
						passPreviousNode = true;
					}
				
				boolean passNextNode = false;
					
				while(iterator.hasNext()){
					Object next = iterator.next();
					if(next instanceof CMReferenceNode){
						CMReferenceNode ref = (CMReferenceNode)next;
						
						
						if(ref.getName().equals(previousSiblingName) && ref.getOccurrence() == CMNodeType.ZERO_OR_MORE){
							passPreviousNode = true;
						}
						if(ref.getName().equals(nextSiblingName) && ref.getOccurrence() == CMNodeType.OPTIONAL){
							passNextNode = true;
						}
						if(passPreviousNode == true && passNextNode==false && ref.getName().startsWith(domElement.getNodeName())){
							proposals.add(getNodeProposal(offset,ref.getName(),domElement.getNodeName(),false,false));
						}
						if(ref.getName().equals(previousSiblingName) && ref.getOccurrence() == CMNodeType.OPTIONAL){
							passPreviousNode = true;
						}
						if(ref.getName().equals(nextSiblingName) && ref.getOccurrence() == CMNodeType.ZERO_OR_MORE){
							passNextNode = true;
						}
					}
				}
			}
		}
		
		return proposals.toArray(new ICompletionProposal[]{});
	}
	
	/**
	 * 
	 * @param offset
	 * @param nodeName
	 * @param stringToReplace
	 * @param useStartChar
	 * @param useEndChar
	 * @return
	 */
	private ICompletionProposal getNodeProposal(int offset, String nodeName, String stringToReplace, boolean useStartChar, boolean useEndChar){
		ICompletionProposal xmlElement = null;
		String endChar = useEndChar==false?"":">";
		String startChar = useStartChar==false?"":"<";
		ImageDescriptor imageDescriptor = F4EXmlEditorPlugin.getEmfImage(nodeName.substring(0, 1).toUpperCase() + nodeName.substring(1) + ".gif");
		Image image = null;
		if(imageDescriptor != null){
			image = imageDescriptor.createImage();
		}
		
		xmlElement = new CompletionProposal( 
				startChar + nodeName + endChar, 
				offset - (stringToReplace.length()), 
				stringToReplace.length(), 
				new String(startChar + nodeName + endChar).length(),
				image,
				nodeName,
				null,
				null);
		return xmlElement;
	}
}
