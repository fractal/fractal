package org.ow2.fractal.f4e.xml.ui.region;

import org.eclipse.jface.text.Region;
import org.eclipse.wst.xml.core.internal.document.AttrImpl;

public class AttributeRegion extends Region {
	AttrImpl attribute = null;
	
	public AttributeRegion(int offset, int length, AttrImpl attribute) {
		super(offset, length);
		this.attribute = attribute;
	}

	public AttrImpl getAttribute(){
		return this.attribute;
	}
}
