/******************************************************************************
 * Copyright (c) 2007-2008, EBM WebSourcing
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     EBM WebSourcing - initial API and implementation
 *******************************************************************************/

package org.ow2.fractal.f4e.xml;

import java.io.StringBufferInputStream;
import java.util.Set;

import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.core.commands.operations.IOperationHistoryListener;
import org.eclipse.core.commands.operations.IUndoableOperation;
import org.eclipse.core.commands.operations.OperationHistoryEvent;
import org.eclipse.core.commands.operations.TriggeredOperations;
import org.eclipse.emf.common.notify.impl.NotificationImpl;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.workspace.IWorkspaceCommandStack;
import org.eclipse.emf.workspace.ResourceUndoContext;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.ITextViewerExtension2;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.hyperlink.IHyperlinkDetector;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.wst.sse.core.text.IStructuredPartitions;
import org.eclipse.wst.sse.ui.internal.ExtendedConfigurationBuilder;
import org.eclipse.wst.sse.ui.internal.SSEUIPlugin;
import org.eclipse.wst.sse.ui.internal.provisional.style.LineStyleProvider;
import org.eclipse.wst.sse.ui.internal.taginfo.AnnotationHoverProcessor;
import org.eclipse.wst.sse.ui.internal.taginfo.ProblemAnnotationHoverProcessor;
import org.eclipse.wst.sse.ui.internal.taginfo.TextHoverManager;
import org.eclipse.wst.xml.core.internal.regions.DOMRegionContext;
import org.eclipse.wst.xml.core.text.IXMLPartitions;
import org.eclipse.wst.xml.ui.StructuredTextViewerConfigurationXML;
import org.ow2.fractal.f4e.fractal.notification.IFractalNotification;
import org.ow2.fractal.f4e.fractal.util.FractalResourceImpl;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;
import org.ow2.fractal.f4e.xml.completion.ProxyProcessor;
import org.ow2.fractal.f4e.xml.linestyle.FractalLineStyleProvider;
import org.ow2.fractal.f4e.xml.presentation.FractalBestMatchHover;
import org.ow2.fractal.f4e.xml.presentation.FractalHyperLinkDetector;
import org.ow2.fractal.f4e.xml.presentation.StructuredTextPartitionerForFractal;
import org.ow2.fractal.f4e.xml.statusline.StatusLineLabelProvider;

/**
 * Extends the XML Configuration for Fractal ADL XML files.
 * For the moment, we use it to complete the auto-completion mechanism provided by the WST XML Editor
 * and to provide another status line label provider.
 * 
 * It is also used to change the getTextHover to be able to use TextHovers that implements
 * ITextHoverExtension2.
 * 
 */
public class StructuredTextViewerConfigurationComposite extends StructuredTextViewerConfigurationXML  {

	private StatusLineLabelProvider fStatusLineLabelProvider;
	private LineStyleProvider fLineStyleProvider;
	private String[] fConfiguredContentTypes;
	
	
	
	public StructuredTextViewerConfigurationComposite() {
		super();
	
	}

	/**
	 * We override this method to update the auto-completion mechanism
	 * @see org.eclipse.wst.sse.ui.StructuredTextViewerConfiguration#getContentAssistant(org.eclipse.jface.text.source.ISourceViewer)
	 */
	protected IContentAssistProcessor[] getContentAssistProcessors(ISourceViewer sourceViewer, String partitionType) {
		IContentAssistProcessor[] processors = super.getContentAssistProcessors(sourceViewer, partitionType);
		ProxyProcessor proxyProcessor = new ProxyProcessor( processors );
		
		return new IContentAssistProcessor[] { proxyProcessor };
	}

	public ILabelProvider getStatusLineLabelProvider(ISourceViewer sourceViewer) {
		if( fStatusLineLabelProvider == null )
			fStatusLineLabelProvider = new StatusLineLabelProvider();
		return fStatusLineLabelProvider;
	}
	
	public LineStyleProvider[] getLineStyleProviders(ISourceViewer sourceViewer, String partitionType) {
		LineStyleProvider[] providers = null;
		
		if ((partitionType == IXMLPartitions.XML_DEFAULT) 
				|| (partitionType == IXMLPartitions.XML_CDATA) 
				|| (partitionType == IXMLPartitions.XML_COMMENT) 
				|| (partitionType == IXMLPartitions.XML_DECLARATION) 
				|| (partitionType == IXMLPartitions.XML_PI) 
				|| (partitionType == DOMRegionContext.XML_TAG_ATTRIBUTE_NAME)
				|| (partitionType == DOMRegionContext.XML_TAG_ATTRIBUTE_VALUE)
				|| (partitionType == DOMRegionContext.XML_TAG_NAME)
				) {
			providers = new LineStyleProvider[]{getLineStyleProviderForXML(sourceViewer)};
		}
		return providers;
	}
	
	private LineStyleProvider getLineStyleProviderForXML(ISourceViewer sourceViewer) {
		if (fLineStyleProvider == null) {
			fLineStyleProvider = new FractalLineStyleProvider(sourceViewer);
		}
		return fLineStyleProvider;
	}
	
	public IHyperlinkDetector[] getHyperlinkDetectors(ISourceViewer sourceViewer) {
		if (sourceViewer == null)
			return null;
		
		return new IHyperlinkDetector[] { new FractalHyperLinkDetector() };
	}

	public String[] getConfiguredContentTypes(ISourceViewer sourceViewer) {

		if (fConfiguredContentTypes == null) {
			String[] xmlTypes = StructuredTextPartitionerForFractal.getConfiguredContentTypes();
			fConfiguredContentTypes = new String[xmlTypes.length + 2];
			fConfiguredContentTypes[0] = IStructuredPartitions.DEFAULT_PARTITION;
			fConfiguredContentTypes[1] = IStructuredPartitions.UNKNOWN_PARTITION;
			int index = 0;
			System.arraycopy(xmlTypes, 0, fConfiguredContentTypes, index += 2, xmlTypes.length);
		}
		return fConfiguredContentTypes;
	}


	public ITextHover getTextHover(ISourceViewer sourceViewer,
			String contentType, int stateMask) {
		ITextHover textHover = null;

		/*
		 * Returns a default problem, annotation, and best match hover
		 * depending on stateMask
		 */
		TextHoverManager.TextHoverDescriptor[] hoverDescs = SSEUIPlugin.getDefault().getTextHoverManager().getTextHovers();
		int i = 0;
		while (i < hoverDescs.length && textHover == null) {
			if (hoverDescs[i].isEnabled() && computeStateMask(hoverDescs[i].getModifierString()) == stateMask) {
				String hoverType = hoverDescs[i].getId();
				if (TextHoverManager.PROBLEM_HOVER.equalsIgnoreCase(hoverType))
					textHover = new ProblemAnnotationHoverProcessor();
				else if (TextHoverManager.ANNOTATION_HOVER.equalsIgnoreCase(hoverType))
					textHover = new AnnotationHoverProcessor();
				else if (TextHoverManager.COMBINATION_HOVER.equalsIgnoreCase(hoverType))
					textHover = new FractalBestMatchHover(createDocumentationHover(contentType));
				else if (TextHoverManager.DOCUMENTATION_HOVER.equalsIgnoreCase(hoverType)) {
					textHover = createDocumentationHover(contentType);
				}
			}
			i++;
		}
		return textHover;
	}

	private ITextHover createDocumentationHover(String partitionType) {
		ITextHover textHover = null;
		Object extendedTextHover = ExtendedConfigurationBuilder.getInstance().getConfiguration(ExtendedConfigurationBuilder.DOCUMENTATIONTEXTHOVER, partitionType);
		if (extendedTextHover instanceof ITextHover) {
			textHover = (ITextHover) extendedTextHover;
		}
		return textHover;
	}
	
	public ITextHover getTextHover(ISourceViewer sourceViewer,
			String contentType) {
		return getTextHover(sourceViewer, contentType, ITextViewerExtension2.DEFAULT_HOVER_STATE_MASK);
	}
}
