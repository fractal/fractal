package org.ow2.fractal.f4e.xml.wizard;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.ISetSelectionTarget;
import org.ow2.fractal.f4e.xml.F4EXmlEditorPlugin;
import org.ow2.fractal.f4e.xml.Messages;

/**
 * A wizard to create the skeleton of a Fractal ADL file.
 */
public class FractalCreationWizard extends Wizard implements INewWizard {
	/** The unique page of the wizard. */
	private FractalCreationPage page;
	/** The initial selection. */
	private IStructuredSelection selection;
	/** The workbench. */
	private IWorkbench workbench;
	/** The file to create. */
	private IFile createdFile;
	
	/** The content of the file once it is created (no need of a JET template for this). */
	public final static String FRACTAL_BASIC_TEMPLATE = 
		"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
		+ "\n"
		+ "<!DOCTYPE definition PUBLIC \"-//objectweb.org//DTD Fractal ADL 2.0//EN\" \"classpath://org/objectweb/fractal/adl/xml/standard.dtd\">"
		+ "\n"
		+ "<definition name=\"\">" 
		+ "\n"
		+ "</definition>"; 
	/** The extension for Fractal ADL files. */
	public final static String FRACTAL_EXTENSION = "fractal"; 
	
	
	@Override
	public void addPages() {
		page = new FractalCreationPage( "MainPage", selection ); 
		addPage( page );
	}
	
	@Override
	public boolean performFinish() {
		try {
			// Create the file.
			createdFile = page.createNewFile();	// Already run a WorkspaceModifyOperation.
	
			// Select the file in the current view.
			IWorkbenchWindow workbenchWindow = workbench.getActiveWorkbenchWindow();
			IWorkbenchPage page = workbenchWindow.getActivePage();
			final IWorkbenchPart activePart = page.getActivePart();
			if( activePart instanceof ISetSelectionTarget ) {
				final ISelection targetSelection = new StructuredSelection( createdFile );
				getShell().getDisplay().asyncExec( new Runnable() {
					public void run() {
						 (( ISetSelectionTarget ) activePart ).selectReveal( targetSelection );
					 }
				});
			}
	
			// Open the file in the default editor.	
			try {
				String editorId = 
					workbench.getEditorRegistry().getDefaultEditor( createdFile.getFullPath().toString()).getId();
				page.openEditor( new FileEditorInput( createdFile ), editorId );
			} catch (PartInitException exception) {
				MessageDialog.openError(
						workbenchWindow.getShell(), 
						Messages.ComponentTypeCreationWizard_8, 
						exception.getMessage());
				return false;
			}
	
			return true;
		} catch( Exception e ) {
			F4EXmlEditorPlugin.log( null, e );
		}
		
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench, org.eclipse.jface.viewers.IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
		this.workbench = workbench;
		setWindowTitle( Messages.ComponentTypeCreationWizard_9 );
	}	
	
	/**
	 * 
	 */
	public class FractalCreationPage extends WizardNewFileCreationPage {
		private String selectionName = ""; //$NON-NLS-1$
		

		/**
		 * Constructor.
		 */
		public FractalCreationPage( String pageName, IStructuredSelection selection ) {
			super( pageName, selection );
			setFileExtension( FRACTAL_EXTENSION );
			setTitle( Messages.FractalCreationWizard_7 );
			setDescription( Messages.FractalCreationWizard_6 );
			
			// Initialize the file name with the selection, if the selection is a file.
			// If the file is a componentType file, do not initialize anything.
			if( selection != null 
					&& selection.getFirstElement() != null
					&& selection.getFirstElement() instanceof IFile ) {
				String fileName = ((IFile) selection.getFirstElement()).getName();
				if( !fileName.endsWith( "." + FRACTAL_EXTENSION )) //$NON-NLS-1$
					selectionName = fileName.substring( 0, fileName.lastIndexOf( '.' ));
			}
		}
		
		@Override
		public void createControl( Composite parent ) {
			super.createControl(parent);
			setFileName( selectionName + "." + FRACTAL_EXTENSION ); //$NON-NLS-1$
		}
		
		@Override
		protected boolean validatePage() {
			if( super.validatePage()) {
				
				// The file name (before the extension) can't be empty.
				String name = getFileName();
				if( name.substring( 0, name.lastIndexOf( '.' )).length() == 0 ) {
					setErrorMessage( Messages.ComponentTypeCreationWizard_12 );
					setPageComplete( false );
					return false;
				}
				
				return true;
			}
			
			return false;
		}
		
		@Override
		protected InputStream getInitialContents() {
			InputStream inputStream = new ByteArrayInputStream( FRACTAL_BASIC_TEMPLATE.getBytes());			
			return inputStream;
		}
	}
}
