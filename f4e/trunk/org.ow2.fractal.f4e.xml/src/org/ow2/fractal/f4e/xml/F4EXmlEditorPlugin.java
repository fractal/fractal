package org.ow2.fractal.f4e.xml;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.ow2.fractal.f4e.xml.utils.Utils;

/**
 * The activator class controls the plug-in life cycle.
 */
public class F4EXmlEditorPlugin extends AbstractUIPlugin {
	/** The plug-in ID */
	public static final String PLUGIN_ID = "org.ow2.fractal.f4e.xml";
	
	/** The plug-in ID of the Fractal Edit plug-in (required to reuse icons). */
	public static final String FRACTAL_EDIT_PLUGIN_ID = "org.ow2.fractal.f4e.emf.edit";
	
	private static F4EXmlEditorPlugin plugin;
		
	private static Utils utils = new Utils();
	

	/**
	 * The constructor.
	 */
	public F4EXmlEditorPlugin() {		
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	public static Utils getUtils(){
		return utils;
	}
	/**
	 * Returns the shared instance.
	 * @return the shared instance
	 */
	public static F4EXmlEditorPlugin getDefault() {
		return plugin;
	}
	
	/**
	 * Returns an image descriptor for the image file at the given plug-in relative path.
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor( String path ) {
		return imageDescriptorFromPlugin( PLUGIN_ID, path );
	}
	
	/**
	 * Log an error into the log file.
	 * At least one of the two arguments cannot be null.
	 * 
	 * @param message the message to log.
	 * @param e the exception to log.
	 */	
	public static void log( String message, Exception e ) {
		String errorMessage = message;
		if( errorMessage == null ) {
			if( e != null && e.getMessage() != null ) 
				errorMessage = e.getMessage();
			else
				errorMessage = Messages.FractalXmlEditorPlugin_1 + PLUGIN_ID  + "."; //$NON-NLS-1$
		}
		IStatus status = new Status( IStatus.ERROR, PLUGIN_ID, IStatus.OK, errorMessage, e );
		getDefault().getLog().log( status );
	}
	
	/**
	 * Get Fractal icons provided by the edit plug-in in its "icons/full/obj16/" folder.
	 * 
	 * @param iconFileName the file name of the icon (e.g. "icon.gif"). 
	 * @return the associated image descriptor or null if it was not found.
	 */
	public static ImageDescriptor getEmfImage( String iconFileName ) {
		return imageDescriptorFromPlugin( FRACTAL_EDIT_PLUGIN_ID, "icons/full/obj16/" + iconFileName ); //$NON-NLS-1$
	}
}
