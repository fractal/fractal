package org.ow2.fractal.f4e.xml.utils;

import javax.xml.namespace.QName;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.internal.core.JavaProject;
import org.eclipse.wst.xml.core.internal.document.DocumentImpl;
import org.eclipse.wst.xml.core.internal.document.ElementImpl;
import org.ow2.fractal.f4e.xml.F4EXmlEditorPlugin;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Useful methods used to retrieve information from a composite document.
 */
public class FractalUtils {

	/**
	 * Get the top node of a Fractal document.
	 * Get the <code>definition</code> element from a Fractal ADL XML file.
	 * @param domDoc
	 * @return
	 */
	public static Node getFractalTopNode( Document domDoc ) {		
		NodeList nodes = domDoc.getChildNodes();
		
		// Get composite.
		for( int i=0; i<nodes.getLength(); i++ ) {			
			Node node = nodes.item( i );
			if( node.getLocalName() != null 
					&& node.getLocalName().equalsIgnoreCase( "definition" )) { //$NON-NLS-1$
				return node;
			}
		}
		
		return null;
	}
	
	/**
	 * Get the QName from an "include" node.
	 * Cover the following cases:
	 * <ul>
	 * 	<li>there is no namespace (use the default one).</li>
	 * 	<li>there is a namespace defined in the <i>include</i> element.</li>
	 * 	<li>there is a namespace defined in the composite element.</li></ul>
	 * @param node
	 * @return the QName if it could be found or null otherwise.
	 */
	public static QName getIncludeQName( Node node ) {
		try {
			// Get name.
			String name = node.getAttributes().getNamedItem( "name" ).getNodeValue(); //$NON-NLS-1$
			
			// Get prefix.
			String prefix = null;
			int colonPosition = name.lastIndexOf( ":" ); //$NON-NLS-1$
			if( colonPosition == 0 || colonPosition >= name.length())
				return null;
			
			else if( colonPosition > 0 ) {
				prefix = name.substring( 0, colonPosition ); 
				name = name.substring( colonPosition + 1 );
			}
			
			// Get namespace URI.
			String namespaceUri = null;
			if( prefix != null ) {
				Node namespaceNode = node.getAttributes().getNamedItem( "xmlns:" + prefix ); //$NON-NLS-1$
				
				// Namespace defined in the "include" element.
				if( namespaceNode != null )
					namespaceUri = namespaceNode.getNodeValue();
				
				else {
					// Check in the root element of the composite the prefix namespace.
					Node compositeNode = FractalUtils.getFractalTopNode( node.getOwnerDocument());
					if( compositeNode == null )
						return null;
					
					namespaceNode = compositeNode.getAttributes().getNamedItem( "xmlns:" + prefix ); //$NON-NLS-1$
					if( namespaceNode != null )
						namespaceUri = namespaceNode.getNodeValue();
				}
			}
			
			// Null prefix means we use the default namespace of the document.
			else {
				Node compositeNode = FractalUtils.getFractalTopNode( node.getOwnerDocument());
				if( compositeNode != null )
					namespaceUri = compositeNode.getNamespaceURI();
			}
			
			if( !"".equals( name ) && !"".equals( namespaceUri )) //$NON-NLS-1$ //$NON-NLS-2$
				return new QName( namespaceUri, name );
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Remove the namespace prefix from a string.
	 * @param nodeName not null.
	 * @return the substring right after the first colon or the entire string if no colon was found. 
	 */
	public static String removeNamespacePrefix( String nodeName ) {
		String[] splits = nodeName.split( ":" ); //$NON-NLS-1$
		if( splits.length == 1 )
			return nodeName;
		
		// In any other case, return the string at index 1.		
		return splits[ 1 ];
	}

	/**
	 * Get the namespace prefix of a an element.
	 * 
	 * @param element not null.
	 * @return
	 */
	public static String getNamespacePrefix( String element ) {
		String[] splits = element.split( ":" ); //$NON-NLS-1$
		if( splits.length == 1 )
			return null;
		
		// In any other case, return the string at index 0.		
		return splits[ 0 ];
	}
	
	/**
	 * Capitalize a string.
	 * @param text
	 * @return
	 */
	public static String capitalize( String text ) {
		if( text == null || text.length() == 0 )
			return text;
		
		text = text.toLowerCase();
		Character firstLetter = Character.toUpperCase( text.charAt( 0 ));
		return firstLetter + text.substring( 1 );
	}
	
	/**
	 * Determine if an object is an <code>include</code> node.
	 * @param node
	 * @return
	 */
	public static boolean isIncludeNode( Object node ) {
		if( node == null || !Node.class.isAssignableFrom( node.getClass())) 
			return false;
		
		return isIncludeNode((Node) node );
	}
	
	/**
	 * Determine if a node is an <code>include</code> node.
	 * @param node
	 * @return
	 */
	public static boolean isIncludeNode( Node node ) {
		String name = node.getNodeName();
		if( name == null )
			return false;
		
		if( removeNamespacePrefix( name ).toLowerCase().equals( "include" )) //$NON-NLS-1$
			return true;
		
		return false;
	}
		
	/**
	 * Get the attribute value for a given <b>attribute name</b> in a given <b>node</b>.
	 * Usage: String name = getAttributeValue( myNode, "name" ); 
	 * 
	 * @param node
	 * @param attributeName
	 * @return
	 */
	public static String getAttributeValue( Node node, String attributeName ) {
		try {
			NamedNodeMap attributes = node.getAttributes();
			for( int i=0; i<attributes.getLength(); i++ ) {
				String attrName = attributes.item( i ).getNodeName().toLowerCase(); 
				if( attrName.equals( attributeName.toLowerCase())) { 					
					return attributes.item( i ).getNodeValue();
				}
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
		
		return null;
	}

}
