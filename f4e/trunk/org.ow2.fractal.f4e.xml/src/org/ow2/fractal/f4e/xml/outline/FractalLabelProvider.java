package org.ow2.fractal.f4e.xml.outline;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.ow2.fractal.f4e.xml.ImageRegistry;
import org.ow2.fractal.f4e.xml.utils.FractalUtils;
import org.w3c.dom.Node;


/**
 * The label provider for the outline view.
 * @author Vincent Zurczak - EBM WebSourcing
 */
public class FractalLabelProvider extends LabelProvider {	
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
	 */
	public Image getImage(Object element) {	
		if( element == null || !Node.class.isAssignableFrom( element.getClass()))
			return null;
		
		// Get node name.
		Node node = (Node) element;
		return ImageRegistry.getInstance().getImage( node );
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
	 */
	public String getText(Object element) {
		// Every "if" statement returns.
		if( element == null ) 
			return ""; //$NON-NLS-1$
		
		// String
		else if( element instanceof String )
			return (String) element;
		
		// Node
		else if( element instanceof Node )  {
			Node node = (Node) element;
					
			// Node with no name ?
			if( node.getNodeName() == null )
				return "?"; //$NON-NLS-1$
			String nodeName = FractalUtils.removeNamespacePrefix( node.getNodeName());
			nodeName = nodeName.toLowerCase();
			
			if( nodeName.equals( "" )) //$NON-NLS-1$
				return ""; //$NON-NLS-1$
			
			// cases: composite, component, service, reference, property
			else if( nodeName.equals( "composite" ) //$NON-NLS-1$
					|| nodeName.equals( "component" ) //$NON-NLS-1$
					|| nodeName.equals( "service" ) //$NON-NLS-1$
					|| nodeName.equals( "reference" ) //$NON-NLS-1$
					|| nodeName.equals( "property" )) { //$NON-NLS-1$
				
				String name = FractalUtils.getAttributeValue( node, "name" ); //$NON-NLS-1$
				if( name == null )
					return "?"; //$NON-NLS-1$
				
				name = FractalUtils.capitalize( name );
				return name;
			}
			
			// case include
			else if( nodeName.equals( "include" )) { //$NON-NLS-1$
				String includeName = FractalUtils.getAttributeValue( node, "name" ); //$NON-NLS-1$
				if( includeName == null )
					return "include ?"; //$NON-NLS-1$
				
				includeName = FractalUtils.removeNamespacePrefix( includeName );
				includeName = FractalUtils.capitalize( includeName );
				return "include " + includeName; //$NON-NLS-1$
			}
			
			// case wire
			else if( nodeName.equals( "wire" )) { //$NON-NLS-1$
				String wireSrc = FractalUtils.getAttributeValue( node, "source" ); //$NON-NLS-1$
				if( wireSrc == null )
					wireSrc = ""; //$NON-NLS-1$
				
				String wireTarget = FractalUtils.getAttributeValue( node, "target" ); //$NON-NLS-1$
				if( wireTarget == null )
					wireTarget = "";  //$NON-NLS-1$
				
				if( wireSrc.equals( "" ) && wireTarget.equals( "" )) //$NON-NLS-1$ //$NON-NLS-2$
					return "";				 //$NON-NLS-1$
				return wireSrc + " > " + wireTarget; //$NON-NLS-1$
			}
			
			// case implementation
			else if( nodeName.startsWith( "implementation." ) //$NON-NLS-1$
					|| nodeName.startsWith( "binding." ) //$NON-NLS-1$
					|| nodeName.startsWith( "interface." ))							 //$NON-NLS-1$
				return FractalUtils.removeNamespacePrefix( node.getNodeName());
			
			// case constraining type
			else if( nodeName.equals( "constrainingtype" )) { //$NON-NLS-1$
				String ctName = FractalUtils.getAttributeValue( node, "name" ); //$NON-NLS-1$
				if( ctName == null )
					return "Constraining Type ?"; //$NON-NLS-1$
				
				ctName = FractalUtils.removeNamespacePrefix( ctName );
				ctName = FractalUtils.capitalize( ctName );
				return "Constraining Type " + ctName; //$NON-NLS-1$
			}
			
			// case intent
			else if( nodeName.equals( "intent" )) { //$NON-NLS-1$
				String name = FractalUtils.getAttributeValue( node, "name" ); //$NON-NLS-1$
				if( name == null )
					return "Intent ?"; //$NON-NLS-1$
								
				name = FractalUtils.removeNamespacePrefix( name );
				name = FractalUtils.capitalize( name );
				return "Intent " + name; //$NON-NLS-1$
			}
			
			// case policy set
			else if( nodeName.equals( "policyset" )) { //$NON-NLS-1$
				String name = FractalUtils.getAttributeValue( node, "name" ); //$NON-NLS-1$
				if( name == null )
					return "Policy Set ?"; //$NON-NLS-1$
								
				name = FractalUtils.removeNamespacePrefix( name );
				name = FractalUtils.capitalize( name );
				return "Policy Set " + name; //$NON-NLS-1$
			}
			
			// case binding type
			else if( nodeName.equals( "bindingtype" )) { //$NON-NLS-1$
				String type = FractalUtils.getAttributeValue( node, "type" ); //$NON-NLS-1$
				if( type == null )
					return "Binding Type ?"; //$NON-NLS-1$
								
				type = FractalUtils.removeNamespacePrefix( type );
				type= FractalUtils.capitalize( type );
				return "Binding Type " + type; //$NON-NLS-1$
			}
			
			// case implementation type
			else if( nodeName.equals( "implementationtype" )) { //$NON-NLS-1$
				String type = FractalUtils.getAttributeValue( node, "type" ); //$NON-NLS-1$
				if( type == null )
					return "Implementation Type ?"; //$NON-NLS-1$
								
				type = FractalUtils.removeNamespacePrefix( type );
				type= FractalUtils.capitalize( type );
				return "Implementation Type " + type; //$NON-NLS-1$
			}
			
			// case run as
			else if( nodeName.equals( "runas" )) { //$NON-NLS-1$
				String role = FractalUtils.getAttributeValue( node, "role" ); //$NON-NLS-1$
				if( role == null )
					return "Run As"; //$NON-NLS-1$
								
				role = FractalUtils.removeNamespacePrefix( role );
				role= FractalUtils.capitalize( role );
				return "Run As " + role; //$NON-NLS-1$
			}
			
			// case permit all
			else if( nodeName.equals( "permitall" )) { //$NON-NLS-1$				
				return "Permit All"; //$NON-NLS-1$
			}
			
			// case deny all
			else if( nodeName.equals( "denyall" )) { //$NON-NLS-1$				
				return "Deny All"; //$NON-NLS-1$
			}
			
			// case component type
			else if( nodeName.equals( "componenttype" )) { //$NON-NLS-1$				
				return "Component Type"; //$NON-NLS-1$
			}
			
			// Any other case: remove the namespace and capitalize the name.
			else {
				nodeName = FractalUtils.removeNamespacePrefix( nodeName );
				nodeName = FractalUtils.capitalize( nodeName );
				return nodeName;
			}
		}
		
		return ""; //$NON-NLS-1$
	}
}
