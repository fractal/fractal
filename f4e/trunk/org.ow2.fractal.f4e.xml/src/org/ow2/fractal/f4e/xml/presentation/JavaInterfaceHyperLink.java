package org.ow2.fractal.f4e.xml.presentation;

import java.io.File;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.internal.core.JavaProject;
import org.eclipse.jdt.internal.ui.text.java.hover.JavadocHover;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.hyperlink.IHyperlink;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.ResourceUtil;
import org.eclipse.ui.part.FileEditorInput;
import org.ow2.fractal.f4e.xml.F4EXmlEditorPlugin;

public class JavaInterfaceHyperLink implements IHyperlink{
	private String fJavaFileName;
	private IRegion fRegion;
	private JavaProject fJavaProject;
	
	public JavaInterfaceHyperLink(IRegion region, JavaProject javaProject ,String javaFileName){
		Assert.isNotNull(javaFileName);
		Assert.isNotNull(region);

		fRegion= region;
		fJavaFileName= javaFileName;
		fJavaProject = javaProject;
	}
	
	public IRegion getHyperlinkRegion() {
		// TODO Auto-generated method stub
		return fRegion;
	}

	public String getHyperlinkText() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getTypeLabel() {
		// TODO Auto-generated method stub
		return null;
	}

	public void open() {
		openJavaFile(fJavaProject,fJavaFileName);
	}

	protected void openJavaFile(IJavaProject javaProject, final String javaFilename){
		try{
			final IJavaElement javaElement = javaProject.findElement(new Path(javaFilename.replace('.', '/') + ".java"));
			if(javaElement != null){
				JavaUI.openInEditor(javaElement);
			}
		}catch(Exception e){
			F4EXmlEditorPlugin.log(e.getMessage(), e);
		}
	}
	
	private IWorkbenchPage getActivePage() {
		IWorkbenchPage result = null;
		
		IWorkbench bench = PlatformUI.getWorkbench();
		if (bench != null) {
			IWorkbenchWindow window = bench.getActiveWorkbenchWindow();
			
			if (window != null) {
				result = window.getActivePage();
			}
		}
		
		return result;
	}
}
