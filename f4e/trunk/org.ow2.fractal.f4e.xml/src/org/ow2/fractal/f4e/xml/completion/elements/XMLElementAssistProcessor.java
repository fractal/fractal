package org.ow2.fractal.f4e.xml.completion.elements;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.contentassist.IContextInformationValidator;
import org.eclipse.wst.sse.ui.internal.StructuredTextViewer;
import org.eclipse.wst.xml.core.internal.document.ElementImpl;
import org.eclipse.wst.xml.core.internal.document.TextImpl;
import org.ow2.fractal.f4e.xml.completion.AbstractProcessor;
import org.ow2.fractal.f4e.xml.utils.Utils;
import org.ow2.fractal.f4e.xml.utils.Utils.ContextData;



public class XMLElementAssistProcessor extends AbstractProcessor{
	private Utils contextInformationUtils = new Utils();
	
	private XMLElementProposalMaker xmlElementProposalMaker;
	
	public ICompletionProposal[] computeCompletionProposals(ITextViewer viewer, int offset) {
		ICompletionProposal[] completionProposals = null;
		defineProposalMaker();
		
		try {
			IDocument document = viewer.getDocument();
			((StructuredTextViewer)viewer).getInput();
			if(contextInformationUtils.isInXMLElement(offset, document)){
				String xmlElementName = contextInformationUtils.getCurrentValue(offset, document);
				ContextData previousXMLElement = contextInformationUtils.getPreviousXMLElementName(offset, document);
			}
			
			Object domElement = contextInformationUtils.getDOMElement(offset, document);
			
			if(domElement instanceof TextImpl){	
				completionProposals = xmlElementProposalMaker.makeProposals(domDoc, offset, (TextImpl)domElement);
			}else if(domElement instanceof ElementImpl){
				completionProposals = xmlElementProposalMaker.makeProposals(domDoc, offset, (ElementImpl)domElement);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
			
		return completionProposals;
	}

	
	private void defineProposalMaker() {
		if( xmlElementProposalMaker == null )
			xmlElementProposalMaker = new XMLElementProposalMaker();
	}
	
	public IContextInformation[] computeContextInformation(ITextViewer viewer,
			int offset) {
		// TODO Auto-generated method stub
		return null;
	}

	public char[] getCompletionProposalAutoActivationCharacters() {
		// TODO Auto-generated method stub
		return null;
	}

	public char[] getContextInformationAutoActivationCharacters() {
		// TODO Auto-generated method stub
		return null;
	}

	public IContextInformationValidator getContextInformationValidator() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getErrorMessage() {
		// TODO Auto-generated method stub
		return null;
	}

}
