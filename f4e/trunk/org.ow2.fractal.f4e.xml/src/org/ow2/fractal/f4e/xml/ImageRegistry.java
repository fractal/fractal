/******************************************************************************
 * Copyright (c) 2007-2008, EBM WebSourcing
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     EBM WebSourcing - initial API and implementation
 *******************************************************************************/

package org.ow2.fractal.f4e.xml;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.ow2.fractal.f4e.xml.utils.FractalUtils;
import org.w3c.dom.Node;

/**
 * This class manages images used in this editor (mainly in the outline view).
 * It is in charge of their creation and their destruction.
 * 
 * @author Vincent Zurczak - EBM WebSourcing
 */
public class ImageRegistry {
	/** The unique instance of this class. */
	private static ImageRegistry instance;
	
	
	//
	// Image Descriptors.	
	private final ImageDescriptor attributeImgDesc = 
		F4EXmlEditorPlugin.getEmfImage( "Attribute.gif" ); //$NON-NLS-1$
	private final ImageDescriptor attributesControllerImgDesc = 
		F4EXmlEditorPlugin.getEmfImage( "AttributesController.gif" ); //$NON-NLS-1$
	private final ImageDescriptor bindingImgDesc = 
		F4EXmlEditorPlugin.getEmfImage( "Binding.gif" ); //$NON-NLS-1$
	private final ImageDescriptor commentImgDesc = 
		F4EXmlEditorPlugin.getEmfImage( "Comment.gif" ); //$NON-NLS-1$
	private final ImageDescriptor componentImgDesc = 
		F4EXmlEditorPlugin.getEmfImage( "Component.gif" ); //$NON-NLS-1$
	private final ImageDescriptor contentImgDesc = 
		F4EXmlEditorPlugin.getEmfImage( "Content.gif" ); //$NON-NLS-1$
	private final ImageDescriptor controllerImgDesc = 
		F4EXmlEditorPlugin.getEmfImage( "Controller.gif" ); //$NON-NLS-1$
	private final ImageDescriptor coordinatesImgDesc = 
		F4EXmlEditorPlugin.getEmfImage( "Coordinates.gif" ); //$NON-NLS-1$
	private final ImageDescriptor definitionImgDesc = 
		F4EXmlEditorPlugin.getEmfImage( "Definition.gif" ); //$NON-NLS-1$
	private final ImageDescriptor interfaceClientImgDesc = 
		F4EXmlEditorPlugin.getEmfImage( "Interface-in.gif" ); //$NON-NLS-1$
	private final ImageDescriptor interfaceServerImgDesc = 
		F4EXmlEditorPlugin.getEmfImage( "Interface-out.gif" ); //$NON-NLS-1$
	private final ImageDescriptor loggerImgDesc = 
		F4EXmlEditorPlugin.getEmfImage( "Logger.gif" ); //$NON-NLS-1$
	private final ImageDescriptor virtualNodeImgDesc = 
		F4EXmlEditorPlugin.getEmfImage( "VirtualNode.gif" ); //$NON-NLS-1$
	
	//
	// Images
	public Image attributeImg;
	public Image attributesControllerImg;
	public Image bindingImg;
	public Image commentImg;
	public Image componentImg;
	public Image contentImg;
	public Image controllerImg;
	public Image definitionImg;
	public Image loggerImg;
	public Image virtualNodeImg;
	public Image interfaceClientImg;
	public Image interfaceServerImg;
	
	
	/**
	 * The private constructor of this class.
	 * It creates the images from their descriptors.
	 */
	private ImageRegistry() {
		attributeImg = attributeImgDesc.createImage();
		attributesControllerImg = attributesControllerImgDesc.createImage();
		bindingImg = bindingImgDesc.createImage();
		commentImg = commentImgDesc.createImage();
		componentImg = componentImgDesc.createImage();
		contentImg = contentImgDesc.createImage();
		controllerImg = controllerImgDesc.createImage();
		definitionImg = definitionImgDesc.createImage();
		loggerImg = loggerImgDesc.createImage();
		virtualNodeImg = virtualNodeImgDesc.createImage();
		interfaceClientImg = interfaceClientImgDesc.createImage();
		interfaceServerImg = interfaceServerImgDesc.createImage();
	}
	
	/**
	 * @return the unique instance of this class.
	 */
	public static ImageRegistry getInstance() {
		if( instance == null )
			instance = new ImageRegistry();
		return instance;
	}
	
	/**
	 * Returns the associated image with the node argument.
	 * The association is generally based on the node name, but also in some cases on the parent type
	 * (composite or component). If no image was found for the node, then <b>null</b> is returned.
	 * 
	 * @param node the XML node whose associated image must be returned.
	 * @return the associated image or null if none was found.
	 */
	public Image getImage( Node node ) {
		
		if( node.getNodeName() == null )
			return null;
		String nodeName = FractalUtils.removeNamespacePrefix( node.getNodeName());
		nodeName = nodeName.toLowerCase();
		
		// There is two Fractal icons, one for the client interfaces, 
		// an other one for server interface
		// TODO doesn't work if the role is inherited as long the xml editor is not synchronized with emf
		if( nodeName.equals( "interface" )){
			Node role = node.getAttributes().getNamedItem("role");
			if(role == null){
				return ImageRegistry.getInstance().interfaceServerImg;
			}else{
				if(role.getNodeValue().equalsIgnoreCase("client")){
					return ImageRegistry.getInstance().interfaceClientImg;
				}else{
					return ImageRegistry.getInstance().interfaceServerImg;
				}
			}
		}
		if( nodeName.equals( "attributes" )) //$NON-NLS-1$
			return ImageRegistry.getInstance().attributesControllerImg;
		if( nodeName.equals( "attribute" )) //$NON-NLS-1$
			return ImageRegistry.getInstance().attributeImg;
		if( nodeName.equals( "virtual-node" )) //$NON-NLS-1$
			return ImageRegistry.getInstance().virtualNodeImg;
		if( nodeName.equals( "definition" )) //$NON-NLS-1$
			return ImageRegistry.getInstance().definitionImg;
		if( nodeName.equals( "component" )) //$NON-NLS-1$
			return ImageRegistry.getInstance().componentImg;
		if( nodeName.equals( "logger" )) //$NON-NLS-1$
			return ImageRegistry.getInstance().loggerImg;
		if( nodeName.equals( "controller" )) //$NON-NLS-1$
			return ImageRegistry.getInstance().controllerImg;
		if( nodeName.equals( "content" )) //$NON-NLS-1$
			return ImageRegistry.getInstance().contentImg;
		if( nodeName.equals( "template-controller" )) //$NON-NLS-1$
			return ImageRegistry.getInstance().controllerImg;
		if( nodeName.equals( "binding" )) //$NON-NLS-1$
			return ImageRegistry.getInstance().bindingImg;
		
		return null;
	}
	
	/**
	 * Delete the images.
	 * @see Object#finalize()
	 */
	@Override	
	protected void finalize() throws Throwable {
		attributeImg.dispose();
		attributesControllerImg.dispose();
		bindingImg.dispose();
		commentImg.dispose();
		componentImg.dispose();
		contentImg.dispose();
		controllerImg.dispose();
		definitionImg.dispose();
		loggerImg.dispose();
		virtualNodeImg.dispose();
		interfaceClientImg.dispose();
		interfaceServerImg.dispose();
		// Call to super.
		super.finalize();
	}
}
