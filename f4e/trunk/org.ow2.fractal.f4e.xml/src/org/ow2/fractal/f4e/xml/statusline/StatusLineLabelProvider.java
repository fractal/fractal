package org.ow2.fractal.f4e.xml.statusline;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.ow2.fractal.f4e.xml.ImageRegistry;
import org.ow2.fractal.f4e.xml.utils.FractalUtils;
import org.w3c.dom.Node;

/**
 * The label provider for the status line.
 * It is used by both the outline view and the editor.
 * 
 */
public class StatusLineLabelProvider extends LabelProvider {
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
	 */
	public Image getImage(Object element) {
		if( element == null || !Node.class.isAssignableFrom( element.getClass()))
			return null;
		
		// Get node name.
		Node node = (Node) element;
		return ImageRegistry.getInstance().getImage( node );
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
	 */
	public String getText(Object element) {
		// Every "if" statement returns.
		if( element == null ) 
			return ""; //$NON-NLS-1$
		
		// String
		else if( element instanceof String )
			return (String) element;
		
		// Node
		else if( element instanceof Node )  {
			StringBuffer s = new StringBuffer();
			Node node = (Node) element;
			while( node != null ) {
				if (node.getNodeType() != Node.DOCUMENT_NODE) {
					String name = node.getNodeName();
					name = FractalUtils.removeNamespacePrefix( name );
					s.insert( 0, name );
				}
				
				node = node.getParentNode();
				if (node != null && node.getNodeType() != Node.DOCUMENT_NODE)
					s.insert( 0, IPath.SEPARATOR );
			}				
			return s.toString();
		}
		return ""; //$NON-NLS-1$
	}
}
