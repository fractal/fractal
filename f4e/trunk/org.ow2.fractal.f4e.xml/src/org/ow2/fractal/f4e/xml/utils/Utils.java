package org.ow2.fractal.f4e.xml.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jdt.internal.core.JavaProject;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.wst.common.internal.emf.resource.EMF2DOMRenderer;
import org.eclipse.wst.common.internal.emf.resource.TranslatorResource;
import org.eclipse.wst.sse.core.StructuredModelManager;
import org.eclipse.wst.sse.core.internal.provisional.IStructuredModel;
import org.eclipse.wst.xml.core.internal.document.DocumentImpl;
import org.eclipse.wst.xml.core.internal.document.ElementImpl;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;
import org.ow2.fractal.f4e.xml.F4EXmlEditorPlugin;
import org.ow2.fractal.f4e.xml.Messages;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Utils {
	
	public static Utils getInstance(){
		return SingletonHolder.contextUtils;
	}
	
	private static class SingletonHolder{
		public static Utils contextUtils = new Utils();
	}
	
	/**
	 * Determines whether the given offset is in attribute position, and if so, returns its value.
	 * The text is considered as an attribute if we find a part of text preceding the offset
	 * which starts with a single-quote or a double-quote and which does not contain any
	 * XML symbol such as '<', '>'.
	 *
	 * Besides, the character at the offset position should not be one of the special
	 * characters wrote above. 
	 * 
	 * The returned object contains the typed-in attribute value (from its beginning to the given offset).
	 * 
	 * @param offset 
	 * @param document
	 * @return a ContextData object if the offset is in attribute position, null otherwise.
	 */
	public ContextData getAttributeValue(int offset, IDocument document) {
		String temp = ""; //$NON-NLS-1$

		/* Here is the idea of the algorithm:
		 * get any previous offset until we have either
		 * 		- offset == 0
		 * 	or
		 * 		- the character at this offset is '<', '>', a single or a double quote.
		 */
		try {
			ITypedRegion region = document.getPartition( offset );
			int partitionOffset = region.getOffset();

			/* ensure we check before the character at the offset */
			if( partitionOffset > 0 )
				partitionOffset --;
			else
				return null;

			/* and start with this character */
			char c = document.get( partitionOffset, 1 ).charAt( 0 );
			while( partitionOffset > 0
					&& c != '>' 
					&& c != '<' 
					&& c != '\'' 
					&& c != '\"' ) {
				temp += c;
				partitionOffset --;
				c = document.get( partitionOffset, 1 ).charAt( 0 );
			}

			// if c is not one of the allowed characters, then this is not an attribute
			if( c != '\'' && c != '"' )		// TODO: manage all the white spaces: '\n'...
				return null;

			/* From now, we have found a xxx-quote. We are into an attribute if, and only if this an opening xxx-quote.
			 * Said differently, we must look at if before the quote, there are only spaces and tabulations or a "=" symbol.
			 */
			if( partitionOffset > 0 )
				partitionOffset --;
			else
				return null;

			c = document.get( partitionOffset, 1 ).charAt( 0 );
			while( partitionOffset > 0 && Character.isWhitespace( c )) {
				partitionOffset --;
				c = document.get( partitionOffset, 1 ).charAt( 0 );
			}
			if( c != '=' ) {
				return null;
			}

			/* Ok, we have found an opening quote, this is an attribute...
			 * but we have read the attribute value in the reversed order, so let's reverse it. */
			temp = new StringBuffer( temp ).reverse().toString();			
			return new ContextData( temp, partitionOffset );
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Gets the attribute name whose value was discovered previously. 
	 * It is better to call {@link Utils#getAttributeValue(int, IDocument)}
	 * before and ensure that it did not return null.
	 * 
	 * @param offset
	 * @param document
	 */
	public ContextData getAttributeName( int offset, IDocument document ) {
		String temp = ""; //$NON-NLS-1$

		/* Here is the idea of the algorithm:
		 * get any previous offset until we have either
		 * 		- offset == 0
		 * 	or
		 * 		- the character at this offset is a white space (space, tab, cr...)
		 */
		try {
			/* ensure we check before the character at the offset */
			if( offset > 0 )
				offset --;
			else {
				F4EXmlEditorPlugin.log( 
						Messages.AttributeValueAssistProcessor_0 +
						Messages.AttributeValueAssistProcessor_1,
					null );
				return null;
			}
			
			/* remove white spaces */
			char c = document.get( offset, 1 ).charAt( 0 );
			while( offset > 0 && Character.isWhitespace( c )) { 
				offset --;
				c = document.get( offset, 1 ).charAt( 0 );
			}
			if( offset == 0 ) {
				F4EXmlEditorPlugin.log( 
						Messages.AttributeValueAssistProcessor_2 +
						Messages.AttributeValueAssistProcessor_3,
					null );
				return null;
			}

			/* now, we are reading in the reversed-order the name of the attribute */
			while( offset > 0 && !Character.isWhitespace( c )) {
				temp += c;
				offset --;
				c = document.get( offset, 1 ).charAt( 0 );
			}

			/* Let's reverse the attribute name to get it in the correct order. */
			temp = new StringBuffer( temp ).reverse().toString();
			return new ContextData( temp, offset );
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		
		/* register it to compute proposals */
		return null;
	}

	/**
	 * Gets the mark-up name associated with the attribute name we found previously.
	 * Precondition: {@link #getAttributeName(int, IDocument)} was called and did not 
	 * return null or {@link #isInMarkup(int, IDocument)} returned true.
	 * 
	 * @param offset
	 * @param document
	 */
	public ContextData getXMLElementName(int offset, IDocument document) {
		String temp = ""; //$NON-NLS-1$

		/* Here is the idea of the algorithm:
		 * get any previous offset until we have either
		 * 		- offset == 0
		 * 	or
		 * 		- the character at this offset is a '<' or a '?'
		 */
		try {
			/* ensure we check before the character at the offset */
			if( offset > 0 )
				offset --;
			else {
				F4EXmlEditorPlugin.log( 
						Messages.AttributeValueAssistProcessor_4 +
						Messages.AttributeValueAssistProcessor_5,
					null );
				return null;
			}

			/* now, we are reading in the reversed-order the name of the attribute */
			char c = document.get( offset, 1 ).charAt( 0 );
			while( offset > 0 && c != '<' && c != '?' ) {
				temp += c;
				offset --;
				c = document.get( offset, 1 ).charAt( 0 );
			}

			/* Now, extract the markup name: read from the end in the reversed-order until the first white-space. */
			int tempLength = temp.length() - 1;
			
			// There is now char after the '<' character
			if(tempLength==-1){
				return new ContextData( "", 0 );
			}
			
			c = temp.charAt( tempLength );
			
			/* remove white spaces between '>' and the beginning of the markup */
			while( tempLength > 0 && Character.isWhitespace( c )) {
				tempLength --;
				c = temp.charAt( tempLength );
			}
			
			/* copy markup name in the right reading order */
			String result = ""; //$NON-NLS-1$
			while( tempLength > 0 && !Character.isWhitespace( c )) {	 
				result += c;
				tempLength --;
				c = temp.charAt( tempLength );
			}
			if( tempLength == 0 && !Character.isWhitespace( c )) {
				result += c;
			}

			/* register it to compute proposals */	
			return new ContextData( result, 0 );
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Gets the name of the previous unclosed mark-up before the offset.
	 * If the offset is already into a mark-up, it will be skipped and the previous mark-up will be returned.
	 * 
	 * @param offset
	 * @param document
	 */
	public ContextData getPreviousXMLElementName( int offset, IDocument document ) {
		
		try {
			if( offset > 0 )
				offset --;
			else {
				F4EXmlEditorPlugin.log( 
						Messages.AttributeValueAssistProcessor_4 +
						Messages.AttributeValueAssistProcessor_5,
					null );
				return null;
			}
			
			// Skip useless text (current mark-up and white spaces).
			char c = document.get( offset, 1 ).charAt( 0 );
			while( offset > 0 && c != '<' && c != '?' && c != '>' ) {
				offset --;
				c = document.get( offset, 1 ).charAt( 0 );
			}
			
			// Case: we are into a mark-up.
			if( c == '<' || c == '?' ) {
				while( offset > 0 && c != '>' ) {
					offset --;
					c = document.get( offset, 1 ).charAt( 0 );
				}
			}
			if( offset == 0 )
				return null;
			
			// From now, skip any closed mark-up.			
			String xmlContentPattern = "[\\w:\\.=/\"\\s]"; //$NON-NLS-1$
			Pattern closedMarkupPattern1 = 
				Pattern.compile( "<" + xmlContentPattern + "*/>", Pattern.MULTILINE ); //$NON-NLS-1$ //$NON-NLS-2$
			Pattern closedMarkupPattern2 = 
				Pattern.compile( 
						"<" + xmlContentPattern + "*>" + xmlContentPattern +	//$NON-NLS-1$ //$NON-NLS-2$ 
						"*</" + xmlContentPattern + "*>",	//$NON-NLS-1$ //$NON-NLS-2$
						Pattern.MULTILINE );  
			// We do not check if the opening and the closing mark-ups have the same name.
			
			Pattern closingMarkupPattern = 
				Pattern.compile( "</" + xmlContentPattern + "*>", Pattern.MULTILINE );	//$NON-NLS-1$ //$NON-NLS-2$					
			Pattern piPattern = 
				Pattern.compile( "<\\?(.*)\\?>", Pattern.MULTILINE );	//$NON-NLS-1$
			
			boolean isUnclosedMarkup = false;
			String temp = null;
			String previousTemp = ""; //$NON-NLS-1$
			while( !isUnclosedMarkup ) {
				temp = ""; //$NON-NLS-1$					
				if( offset == 0 )
					return null;
				
				while( offset > 0 && c != '<' ) {
					temp += c;
					offset --;
					c = document.get( offset, 1 ).charAt( 0 );
				}
				
				if( c == '<' ) {
					temp += c;
					offset --;
					c = document.get( offset, 1 ).charAt( 0 );
				}
				
				temp = new StringBuffer( temp ).reverse().toString();
				temp += previousTemp;
				Matcher matcher = closedMarkupPattern1.matcher( temp );
				if( matcher.find())
					continue;
				
				// Comment or processing instruction...
				if( temp.startsWith( "<!--" ) && temp.endsWith( "-->" )) //$NON-NLS-1$ //$NON-NLS-2$
					continue;
				
				matcher = piPattern.matcher( temp );
				if( matcher.find())
					continue;
				
				// Complex-Mark-up ?
				matcher = closedMarkupPattern2.matcher( temp );
				if( matcher.find()) {
					previousTemp = ""; //$NON-NLS-1$
					continue;
				}
				
				// Without the next lines, closedMarkupPattern2 has no chance to ever match.
				matcher = closingMarkupPattern.matcher( temp );
				if( matcher.find()) {
					previousTemp = temp;	
					continue;
				}
				
				isUnclosedMarkup = true;
			}
				
			// Extract the mark-up name.
			temp = temp.trim();
			int i=0;
			while( i<temp.length()) {
				if( Character.isWhitespace( temp.charAt( i )))
					break;
				i++;
			}
			temp = temp.substring( 1, i );	// remove '<' at the beginning...
				
			return new ContextData( temp, offset );
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Returns the text from the first forbidden character until the given offset.
	 * Forbidden characters are '<', '>', '?' and white spaces.
	 * 
	 * @param offset
	 * @param document
	 * @return
	 */
	public String getCurrentValue( int offset, IDocument document ) {
		try {
			if( offset > 0 )
				offset --;
			else {
				F4EXmlEditorPlugin.log( 
						Messages.AttributeValueAssistProcessor_4 +
						Messages.ContextInformationUtils_8,
					null );
				return ""; //$NON-NLS-1$
			}
			
			String temp = ""; //$NON-NLS-1$
			char c = document.get( offset, 1 ).charAt( 0 );
			while( offset > 0 && c != '<' && c != '>' && c != '?' && !Character.isWhitespace( c )) {
				temp += c;
				offset --;
				c = document.get( offset, 1 ).charAt( 0 );
			}
			
			return new StringBuffer( temp ).reverse().toString();
		} 
		catch( BadLocationException e ) {
			e.printStackTrace();
		}
		
		return ""; //$NON-NLS-1$
	}
	
	/**
	 * Returns true if the offset is right after a mark-up name.
	 * More precisely, we check if the offset is in the following configuration:
	 * 
	 * [opening markup symbol '<'] [zero or more white spaces] [markup-name] [cursor offset]
	 * 
	 * @param offset
	 * @param document
	 * @return
	 */
	public boolean isTypingXMLElementName( int offset, IDocument document ) {
		try {
			if( offset > 0 )
				offset --;
			else {
				F4EXmlEditorPlugin.log( 
						Messages.AttributeValueAssistProcessor_4 +
						Messages.ContextInformationUtils_12,
					null );
				return false;
			}
			
			String temp = ""; //$NON-NLS-1$
			char c = document.get( offset, 1 ).charAt( 0 );
			if( Character.isWhitespace( c ))
				return false;
			
			while( offset > 0 && c != '<' && c != '>' && c != '?' ) {
				temp += c;
				offset --;
				c = document.get( offset, 1 ).charAt( 0 );
			}
			
			if( c == '>' || offset == 0 )
				return false;
			
			// 'temp' contains the text from '<' until offset.
			// Check if 'temp' contains any whitespace. If so, the user is not typing in the mark-up name.
			for( char cc : temp.toCharArray()) {
				if( Character.isWhitespace( cc ))
					return false;
			}
			
			return true;
		} 
		catch( BadLocationException e ) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	/**
	 * Returns true if the offset is right after an attribute name.
	 * More precisely, we check if the offset is in the following configuration:
	 * 
	 * [opening markup symbol '<'] [any-text] [zero or more white spaces] [attribute-name] [cursor offset]
	 * 
	 * @param offset
	 * @param document
	 * @return
	 */
	 public boolean isTypingAttributeName( int offset, IDocument document ) {
		try {
			if( offset > 0 )
				offset --;
			else {
				F4EXmlEditorPlugin.log( 
						Messages.AttributeValueAssistProcessor_4 +
						Messages.ContextInformationUtils_14,
					null );
				return false;
			}
			
			String temp = ""; //$NON-NLS-1$
			char c = document.get( offset, 1 ).charAt( 0 );			
			while( offset > 0 && c != '<' && c != '>' && c != '?' ) {
				temp += c;
				offset --;
				c = document.get( offset, 1 ).charAt( 0 );
			}
			
			if( c == '>' || offset == 0 )
				return false;
			
			// 'temp' contains the text from '<' until offset.
			// Check if 'temp' contains any whitespace. If so, the user is typing in an attribute name.
			for( char cc : temp.toCharArray()) {
				if( Character.isWhitespace( cc ))
					return true;
			}
			
			// If we are here, the user is typing in a mark-up name.
			return false;
		} 
		catch( BadLocationException e ) {
			e.printStackTrace();
		}
		
		return false;
	}
	 
	/**
	 * Returns true if the offset is in a mark-up.
	 * More precisely, we check if the offset is in the following configuration:
	 * 
	 * [opening markup symbol '<'] [any character except '?', '<', '>'] [cursor offset]
	 * 
	 * @param offset
	 * @param document
	 * @return
	 */
	 public boolean isInXMLElement( int offset, IDocument document ) {
		try {
			if( offset > 0 )
				offset --;
			else {
				F4EXmlEditorPlugin.log( 
						Messages.AttributeValueAssistProcessor_4 +
						Messages.ContextInformationUtils_16,
					null );
				return false;
			}
			
			char c = document.get( offset, 1 ).charAt( 0 );			
			while( offset > 0 && c != '<' && c != '>' && c != '?' ) {
				offset --;
				c = document.get( offset, 1 ).charAt( 0 );
			}
				
			if( c == '>' || offset == 0 )
				return false;
									
			return true;
		} 
		catch( BadLocationException e ) {
			e.printStackTrace();
		}
			
		return false;
	}
 	
	
	/**
	 * A ContextData object is made up of two values: a string value and an integer offset.
	 * This class is used to store contextual information from the document and allow further search.
	 * 
	 *  
	 * Let's take as example {@link Utils#getAttributeName(int, IDocument)}.
	 * This method returns a ContextData object. In this object, the value will be the attribute name.
	 * And the offset will be the initial position of the attribute name in the document. 
	 * Thus, if you want to get additional information from the document (e.g. the name of the mark-up which
	 * contains this attribute), you will be able to use this offset as starting point for your search.
	 */
	public class ContextData {
		/** The asked value. */
		private String value;
		/** The position of the value in the document. */
		private int offset;
		
		/**
		 * @param value
		 * @param offset
		 */
		public ContextData(String value, int offset) {
			this.offset = offset;
			this.value = value;
		}
		/**
		 * @return the value
		 */
		public String getValue() {
			return value;
		}
		/**
		 * @return the offset
		 */
		public int getOffset() {
			return offset;
		}
	}
	
	/**
	 * @param offset
	 * @param document
	 * @return the DOM object matching the given offset
	 */
	public Object getDOMElement(int offset, IDocument document){
		
		Object o = StructuredModelManager.getModelManager().getExistingModelForRead(document).getIndexedRegion(offset);
		//getEMFObject((ElementImpl)o);
		return o;
	}
	
	/**
	 * 
	 * @param offset
	 * @param document
	 * @return the offset of the first previous '>' character
	 */
	public int getPreviousEndCharOffset( int offset, IDocument document ) {
		try {
			if( offset > 0 )
				offset --;
			else {
				F4EXmlEditorPlugin.log( 
						Messages.AttributeValueAssistProcessor_4 +
						Messages.ContextInformationUtils_12,
					null );
				return -1;
			}
			
			String temp = ""; 
			char c = document.get( offset, 1 ).charAt( 0 );
			
			while( offset > 0 && c != '>') {
				temp += c;
				offset --;
				c = document.get( offset, 1 ).charAt( 0 );
			}
			
			if( c == '>' )
				return offset;
			
			return -1;
		} 
		catch( BadLocationException e ) {
			e.printStackTrace();
		}
		
		return -1;
	}
	
	/**
	 * 
	 * @param offset
	 * @param document
	 * @return the closest previous Dom Element
	 */
	public Object getLatestDOMElement(int offset, IDocument document){
		IStructuredModel model = StructuredModelManager.getModelManager().getExistingModelForRead(document);
		
		while(offset > 0){
			Object node = model.getIndexedRegion(offset);
			
			if(node instanceof ElementImpl){
				return node;
			}else{
				offset--;
			}
		}
		return null;
	}
	
	public JavaProject getJavaProject(ElementImpl element){
		return getJavaProject(((DocumentImpl)element.getOwnerDocument()).getModel().getBaseLocation());
	}
	/**
	 * 
	 * @param uri
	 * @return
	 */
	public JavaProject getJavaProject(String uri){
		JavaProject javaProject =null;
		try{
			IPath path = new Path(uri);
			IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
			IProject project = file.getProject();
			IProjectNature javaNature = project.getNature("org.eclipse.jdt.core.javanature");
			if(javaNature instanceof JavaProject){
				javaProject = (JavaProject)javaNature;
			}
			
		}catch(Exception e){
			F4EXmlEditorPlugin.log(e.getMessage(), e);
		}
		return javaProject;
	}
	
	public Integer getChildPosition(ElementImpl parent, ElementImpl child){
		NodeList list = parent.getChildNodes();
		int i =0;
		int j=0;
		while(i+j<list.getLength()){
			Node node = list.item(i+j);
			if(node == child){
				return i;
			}
			if(node instanceof ElementImpl){
				i++;
			}else{
				j++;
			}
		}
		return null;
	}
	
	/**
	 * Return a list of integer which indicate how to access to the element
	 * from the root element. e.g [1,2] indicates that to access to the element
	 * from the root element we have to get the first child, then the second child.
	 * 
	 * @param element
	 * @return a list of child position
	 */
	public List<Integer> getPath(final ElementImpl element){
		List<Integer> path = new ArrayList<Integer>();
		Node parent = null;
		ElementImpl child = element;
		parent  = child.getParentNode();
		while(parent != null && parent instanceof ElementImpl){
			Integer pos = getChildPosition((ElementImpl)parent,child);
			if(pos != null){
				path.add(pos);
			}else{
				return null;
			}
			child = (ElementImpl)parent;
			parent = parent.getParentNode();
		}
		return path;
	}
	
	/**
	 * TODO
	 * Try to return the EObject that is the same position that the DOM element.
	 * Requires that the DOM tree and the EMF tree are similar.
	 * Note that the XML editor is not synchronized with the EMF editor.
	 * So this method can fails, or return false result if the 
	 * tree structure is not similar.
	 * 
	 * @param element
	 * @return
	 */
	public EObject getEMFObject(ElementImpl element){
		EObject resultEObject = null;
		String path = ((DocumentImpl)element.getOwnerDocument()).getModel().getBaseLocation();
		URI uri = URI.createPlatformResourceURI(path,true);
		if(uri != null){
	
			Definition definition = FractalTransactionalEditingDomain.getResourceSet().getDefinition(uri);
			getPath(element);
		}
		return resultEObject;
	}
	

	/**
	 * TODO
	 * @param path
	 * @return
	 */
	public EObject getEMFObject(List<Integer> path){
		return null;
	}
	
	/**
	 * TODO
	 * @param document
	 * @return
	 */
	public Resource getResource(IDocument document){
		return null;
//		Resource r = FractalTransactionalEditingDomain.getResourceSet().getResource(URI.createPlatformResourceURI(((IFileEditorInput)getEditorInput()).getFile().getFullPath().toOSString()), true);
//		return r;
	}
}
