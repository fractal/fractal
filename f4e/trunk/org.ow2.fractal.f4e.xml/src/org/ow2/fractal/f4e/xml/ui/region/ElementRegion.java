package org.ow2.fractal.f4e.xml.ui.region;

import org.eclipse.jface.text.Region;
import org.eclipse.wst.xml.core.internal.document.ElementImpl;

public class ElementRegion extends Region{
	ElementImpl element = null;
	
	public ElementRegion(int offset, int length, ElementImpl element) {
		super(offset, length);
		this.element = element;
	}

	public ElementImpl getElement(){
		return this.element;
	}	
	
}
