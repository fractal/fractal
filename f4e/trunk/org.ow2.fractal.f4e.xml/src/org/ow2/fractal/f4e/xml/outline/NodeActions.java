package org.ow2.fractal.f4e.xml.outline;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.ow2.fractal.f4e.xml.F4EXmlEditorPlugin;
import org.ow2.fractal.f4e.xml.Messages;
import org.ow2.fractal.f4e.xml.utils.FractalUtils;
import org.ow2.fractal.f4e.xml.utils.ResourceExplorer;
import org.w3c.dom.Node;

/**
 * Fill in context menus for nodes in the outline view.
 */
public class NodeActions {
	/** The action top open an included file. */
	private Action openFileAction;
	/** The action to refresh the viewer in the outline view. */
	private Action refreshViewerAction;
	/** 
	 * The action to show element properties in the properties view. 
	 * It is assumed that the outline and the properties views are not in the same workbench part.
	 */
	private Action showPropertiesAction;
	
	/** */
	final static ImageDescriptor showPropertiesActionImgDesc = 
		F4EXmlEditorPlugin.getImageDescriptor( "icons/full/elcl16/properties.gif" ); //$NON-NLS-1$
	/** */
	final static ImageDescriptor refreshViewerActionImgDesc = 
		F4EXmlEditorPlugin.getImageDescriptor( "icons/full/elcl16/refresh.gif" ); //$NON-NLS-1$
	/** */
	final static ImageDescriptor openFileActionImgDesc = 
		PlatformUI.getWorkbench().getSharedImages().getImageDescriptor( ISharedImages.IMG_OBJ_FOLDER );
	
	
	/**
	 * Fill in the context menu.
	 * Actions depend on the selected node in the outline view.
	 * 
	 * @param treeViewer the tree viewer of the outline view.
	 * @param manager the menu manager.
	 */
	public void fillInContextMenu( final TreeViewer treeViewer, IMenuManager manager ) {
		
		// open action
		if( openFileAction == null ) {
			openFileAction = new Action( Messages.NodeActions_1, openFileActionImgDesc ) {
				@Override
				public void run () {
					try {
						ISelection selection = treeViewer.getSelection();
						Object selected = ((IStructuredSelection) selection ).getFirstElement();
						
						IFile editedFile = ResourceExplorer.getIFileFromEditor();									
//						IFile includedFile = IncludesUtils.getIncludedFile( selected, editedFile.getProject());
//						if( !includedFile.getName().endsWith( ".fractal" )) //$NON-NLS-1$
//							F4EXmlEditorPlugin.log( Messages.NodeActions_4, null );
						
//						IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
//						IEditorDescriptor desc = PlatformUI.getWorkbench().getEditorRegistry().getDefaultEditor( includedFile.getName());
//						page.openEditor( new FileEditorInput( includedFile ), desc.getId());
					}
					catch( Exception e ) {
						F4EXmlEditorPlugin.log( null, e );
					}
				}
			};
		}
		
		// update action
		if( refreshViewerAction == null ) {
			refreshViewerAction = new Action( Messages.NodeActions_2, refreshViewerActionImgDesc ) {
				@Override
				public void run () {
					treeViewer.refresh();
				}	
			};
		}
		
		// show properties action
		if( showPropertiesAction == null ) {
			showPropertiesAction = new Action( Messages.NodeActions_3, showPropertiesActionImgDesc ) {				
				@Override
				public void run() {
					try {
						// Get current selection.
						ISelection selection = treeViewer.getSelection();						
						
						// Show properties view.
						IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
						page.showView( "org.eclipse.ui.views.PropertySheet" ); //$NON-NLS-1$
						
						// Show outline view.						
						page.showView( "org.eclipse.ui.views.ContentOutline" ); //$NON-NLS-1$
						
						// Select the previous selection in the outline.
						treeViewer.setSelection( selection );
					} 
					catch( Exception e ) {
						F4EXmlEditorPlugin.log( null, e );
					}
				}
			};
		}
		
				
		// Add actions.
		ISelection selection = treeViewer.getSelection();
		Object selected = ((IStructuredSelection) selection ).getFirstElement();
		
		// For include nodes only.
		if( FractalUtils.isIncludeNode( selected )) {
			manager.add( openFileAction );
			manager.add( refreshViewerAction );
		}
		
		// For all nodes.
		// Bug: children nodes from an include node do not have any property. Remove the action for them.
		if( selected != null 
				&& Node.class.isAssignableFrom( selected.getClass()))
			manager.add( showPropertiesAction );
	}	
}
