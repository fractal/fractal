package org.ow2.fractal.f4e.xml.core.modelhandler;

import org.eclipse.wst.sse.core.internal.document.IDocumentCharsetDetector;
import org.eclipse.wst.sse.core.internal.document.IDocumentLoader;
import org.eclipse.wst.sse.core.internal.ltk.modelhandler.AbstractModelHandler;
import org.eclipse.wst.sse.core.internal.ltk.modelhandler.IModelHandler;
import org.eclipse.wst.sse.core.internal.modelhandler.PluginContributedFactoryReader;
import org.eclipse.wst.sse.core.internal.provisional.IModelLoader;
import org.eclipse.wst.xml.core.internal.encoding.XMLDocumentCharsetDetector;
import org.ow2.fractal.f4e.xml.core.encoding.FractalDocumentLoader;
import org.ow2.fractal.f4e.xml.core.encoding.FractalModelLoader;

public class ModelHandlerForFractal  extends AbstractModelHandler implements IModelHandler {
	/** 
	 * Needs to match what's in plugin registry. 
	 * In fact, can be overwritten at run time with 
	 * what's in registry! (so should never be 'final')
	 */
	public static String FractalContentTypeID = "org.ow2.fractal.f4e.xml.fractalsource"; //$NON-NLS-1$
	/**
	 * Needs to match what's in plugin registry. 
	 * In fact, can be overwritten at run time with 
	 * what's in registry! (so should never be 'final')
	 */
	private static String ModelHandlerID_FRACTAL = "org.ow2.fractal.f4e.xml.core.modelhandler"; //$NON-NLS-1$


	public ModelHandlerForFractal() {
		super();
		setId(ModelHandlerID_FRACTAL);
		
		setAssociatedContentTypeId(FractalContentTypeID);
	}

	public IDocumentCharsetDetector getEncodingDetector() {
		return new XMLDocumentCharsetDetector();
	}
	
	public IModelLoader getModelLoader() {
		return new FractalModelLoader();
	}

	public IDocumentLoader getDocumentLoader() {
		return new FractalDocumentLoader();
	}

}