package org.ow2.fractal.f4e.xml;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.ow2.fractal.f4e.xml.messages"; //$NON-NLS-1$
	
	public static String FractalCreationWizard_6;

	public static String FractalCreationWizard_7;

	public static String FractalXmlEditorPlugin_1;
	
	public static String AttributeValueAssistProcessor_0;
	public static String AttributeValueAssistProcessor_1;
	public static String AttributeValueAssistProcessor_2;
	public static String AttributeValueAssistProcessor_3;
	public static String AttributeValueAssistProcessor_4;
	public static String AttributeValueAssistProcessor_5;
	
	public static String ComponentTypeCreationWizard_12;

	public static String ComponentTypeCreationWizard_8;

	public static String ComponentTypeCreationWizard_9;

	public static String ContextInformationUtils_12;

	public static String ContextInformationUtils_14;

	public static String ContextInformationUtils_16;

	public static String ContextInformationUtils_8;

	public static String NodeActions_1;
	public static String NodeActions_2;
	public static String NodeActions_3;
	public static String NodeActions_4;
	public static String FractalContentOutlineConfiguration_3;
	public static String FractalContentOutlineConfiguration_4;
	public static String FractalContentOutlineConfiguration_5;
	public static String FractalContentOutlineConfiguration_6;
	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}

}
