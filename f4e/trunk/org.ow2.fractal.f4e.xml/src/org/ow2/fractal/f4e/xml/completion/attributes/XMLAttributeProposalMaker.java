package org.ow2.fractal.f4e.xml.completion.attributes;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.text.contentassist.CompletionProposal;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.ow2.fractal.f4e.xml.utils.ResourceExplorer;
import org.w3c.dom.Document;

public class XMLAttributeProposalMaker {
	/** The file being currently edited. */
	private IFile editedFile;
	
	/**
	 * The constructor initializes the resources.
	 * @see #defineWorkspaceResources()
	 */
	public XMLAttributeProposalMaker() {
		defineWorkspaceResources();
	}
	
	
	/**
	 * Initialize resources.
	 * It consists in retrieving the edited file from the active editor and in
	 * getting prepared to make proposals using <code>include</code> mark-ups.
	 */
	private void defineWorkspaceResources () {			
		// Edited IFile.
		editedFile = ResourceExplorer.getIFileFromEditor();
			
		// Create the utils for includes. 
		IProject project = editedFile.getProject();
	}
	
	
	protected ICompletionProposal[] provideInterfaceXMLAttributesNamesProposals(Document domDoc, int offset, String attributeName){
		List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();
		
		if( "name".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
						"name", 
						offset - attributeName.length(), 
						attributeName.length(), 
						"name".length());		
			proposals.add(proposal);
		}
		
		if( "role".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
					"role", 
					offset - attributeName.length(), 
					attributeName.length(), 
					"role".length());		
			proposals.add(proposal);
		}
		
		if( "signature".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
					"signature", 
					offset - attributeName.length(), 
					attributeName.length(), 
					"signature".length());		
			proposals.add(proposal);
		}
		
		if( "cardinality".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
					"cardinality", 
					offset - attributeName.length(), 
					attributeName.length(), 
					"cardinality".length());		
			proposals.add(proposal);
		}
		
		if( "contingency".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
					"contingency", 
					offset - attributeName.length(), 
					attributeName.length(), 
					"contingency".length());		
			proposals.add(proposal);
		}
		return proposals.toArray(new ICompletionProposal[]{});
	}
	
	protected ICompletionProposal[] provideAttributesXMLAttributesNamesProposals(Document domDoc, int offset, String attributeName){
		List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();
		
		if( "signature".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
						"signature", 
						offset - attributeName.length(), 
						attributeName.length(), 
						"signature".length());		
			proposals.add(proposal);
		}
		return proposals.toArray(new ICompletionProposal[]{});
	}
	
	protected ICompletionProposal[] provideAttributeXMLAttributesNamesProposals(Document domDoc, int offset, String attributeName){
		List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();
		
		if( "name".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
						"name", 
						offset - attributeName.length(), 
						attributeName.length(), 
						"name".length());		
			proposals.add(proposal);
		}
		
		if( "value".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
						"value", 
						offset - attributeName.length(), 
						attributeName.length(), 
						"value".length());		
			proposals.add(proposal);
		}
		
		return proposals.toArray(new ICompletionProposal[]{});
	}
	
	protected ICompletionProposal[] provideVirtualNodeXMLAttributesNamesProposals(Document domDoc, int offset, String attributeName){
		List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();
		
		if( "name".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
						"name", 
						offset - attributeName.length(), 
						attributeName.length(), 
						"name".length());		
			proposals.add(proposal);
		}
		return proposals.toArray(new ICompletionProposal[]{});
	}
	
	protected ICompletionProposal[] provideDefinitionXMLAttributesNamesProposals(Document domDoc, int offset, String attributeName){
		List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();
		
		if( "name".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
						"name", 
						offset - attributeName.length(), 
						attributeName.length(), 
						"name".length());		
			proposals.add(proposal);
		}
		
		if( "arguments".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
						"arguments", 
						offset - attributeName.length(), 
						attributeName.length(), 
						"arguments".length());		
			proposals.add(proposal);
		}
		
		if( "extends".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
						"extends", 
						offset - attributeName.length(), 
						attributeName.length(), 
						"extends".length());		
			proposals.add(proposal);
		}
		
		return proposals.toArray(new ICompletionProposal[]{});
	}
	
	protected ICompletionProposal[] provideComponentXMLAttributesNamesProposals(Document domDoc, int offset, String attributeName){
		List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();
		
		if( "name".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
						"name", 
						offset - attributeName.length(), 
						attributeName.length(), 
						"name".length());		
			proposals.add(proposal);
		}
		
		if( "definition".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
						"definition", 
						offset - attributeName.length(), 
						attributeName.length(), 
						"definition".length());		
			proposals.add(proposal);
		}
		
		return proposals.toArray(new ICompletionProposal[]{});
	}
	
	protected ICompletionProposal[] provideLoggerXMLAttributesNamesProposals(Document domDoc, int offset, String attributeName){
		List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();
		
		if( "name".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
						"name", 
						offset - attributeName.length(), 
						attributeName.length(), 
						"name".length());		
			proposals.add(proposal);
		}
		return proposals.toArray(new ICompletionProposal[]{});
	}
	
	protected ICompletionProposal[] provideControllerXMLAttributesNamesProposals(Document domDoc, int offset, String attributeName){
		List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();
		
		if( "desc".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
						"desc", 
						offset - attributeName.length(), 
						attributeName.length(), 
						"desc".length());		
			proposals.add(proposal);
		}
		return proposals.toArray(new ICompletionProposal[]{});
	}
	
	protected ICompletionProposal[] provideContentXMLAttributesNamesProposals(Document domDoc, int offset, String attributeName){
		List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();
		
		if( "class".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
						"class", 
						offset - attributeName.length(), 
						attributeName.length(), 
						"class".length());		
			proposals.add(proposal);
		}
		return proposals.toArray(new ICompletionProposal[]{});
	}
	
	protected ICompletionProposal[] provideTemplateControllerXMLAttributesNamesProposals(Document domDoc, int offset, String attributeName){
		List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();
		
		if( "desc".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
						"desc", 
						offset - attributeName.length(), 
						attributeName.length(), 
						"desc".length());		
			proposals.add(proposal);
		}
		return proposals.toArray(new ICompletionProposal[]{});
	}
	
	protected ICompletionProposal[] provideBindingXMLAttributesNamesProposals(Document domDoc, int offset, String attributeName){
		List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();
		
		if( "client".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
						"client", 
						offset - attributeName.length(), 
						attributeName.length(), 
						"client".length());		
			proposals.add(proposal);
		} 
		if( "server".startsWith(attributeName)) {		
			ICompletionProposal proposal = new CompletionProposal( 
						"server", 
						offset - attributeName.length(), 
						attributeName.length(), 
						"server".length());		
			proposals.add(proposal);
		}
		return proposals.toArray(new ICompletionProposal[]{});
	}
	
	/**
	 *  The public facade to get proposals for XML Attributes names.
	 *  
	 * @param domDoc
	 * @param markupName
	 * @param attributeName
	 * @param offset
	 * @return
	 */
	public ICompletionProposal[] makeProposals( Document domDoc, String markupName, String attributeName, int offset ) {
		ICompletionProposal[] proposals = null;
		if(markupName.equalsIgnoreCase("interface")){
			proposals = provideInterfaceXMLAttributesNamesProposals(domDoc,offset,attributeName);
		}else if(markupName.equalsIgnoreCase("attributes")){
			proposals = provideAttributesXMLAttributesNamesProposals(domDoc,offset,attributeName);
		}else if(markupName.equalsIgnoreCase("attribute")){
			proposals = provideAttributeXMLAttributesNamesProposals(domDoc,offset,attributeName);
		}else if(markupName.equalsIgnoreCase("virtual-node")){
			proposals = provideVirtualNodeXMLAttributesNamesProposals(domDoc,offset,attributeName);
		}else if(markupName.equalsIgnoreCase("definition")){
			proposals = provideDefinitionXMLAttributesNamesProposals(domDoc,offset,attributeName);
		}else if(markupName.equalsIgnoreCase("component")){
			proposals = provideComponentXMLAttributesNamesProposals(domDoc,offset,attributeName);
		}else if(markupName.equalsIgnoreCase("logger")){
			proposals = provideLoggerXMLAttributesNamesProposals(domDoc,offset,attributeName);
		}else if(markupName.equalsIgnoreCase("controller")){
			proposals = provideControllerXMLAttributesNamesProposals(domDoc,offset,attributeName);
		}else if(markupName.equalsIgnoreCase("content")){
			proposals = provideContentXMLAttributesNamesProposals(domDoc,offset,attributeName);
		}else if(markupName.equalsIgnoreCase("template-controller")){
			proposals = provideTemplateControllerXMLAttributesNamesProposals(domDoc,offset,attributeName);
		}else if(markupName.equalsIgnoreCase("binding")){
			proposals = provideBindingXMLAttributesNamesProposals(domDoc,offset,attributeName);
		}
		return proposals;
	}
	
	
	/**
	 * Provide auto-completion proposals for a Interface element.
	 * 
	 * @param domDoc
	 * @param offset 
	 * @param attributeName
	 * @param attributeValue
	 * @return proposals for <code>wire</code> mark-ups.
	 */
	private ICompletionProposal[] provideInterfaceXMLAttributesValuesProposals( Document domDoc, int offset, String attributeName, String attributeValue ) {		
		
		if( attributeName.equals( "role" ) ) {		
				ICompletionProposal client = new CompletionProposal( 
							"client", 
							offset - attributeValue.length(), 
							attributeValue.length(), 
							"client".length());		
				
				ICompletionProposal server = new CompletionProposal( 
						"server", 
						offset - attributeValue.length(), 
						attributeValue.length(), 
						"server".length());		
				return new ICompletionProposal[] { client, server };
		}else if(attributeName.equals( "cardinality")){
			ICompletionProposal collection = new CompletionProposal( 
					"collection", 
					offset - attributeValue.length(), 
					attributeValue.length(), 
					"collection".length());		
		
			ICompletionProposal singleton = new CompletionProposal( 
					"singleton", 
					offset - attributeValue.length(), 
					attributeValue.length(), 
					"singleton".length());		
			return new ICompletionProposal[] { collection, singleton };
		}else if(attributeName.equals("contingency")){
			ICompletionProposal optional = new CompletionProposal( 
					"optional", 
					offset - attributeValue.length(), 
					attributeValue.length(), 
					"optional".length());		
		
			ICompletionProposal mandatory = new CompletionProposal( 
					"mandatory", 
					offset - attributeValue.length(), 
					attributeValue.length(), 
					"mandatory".length());		
			return new ICompletionProposal[] { mandatory, optional };
		}else if(attributeName.endsWith("signature")){
			
		}
		return new ICompletionProposal[ 0 ];
	}
	
	private ICompletionProposal[] provideAttributesXMLAttributesValuesProposals( Document domDoc, int offset, String attributeName, String attributeValue ) {		
		// TODO
		return null;
	}
	
	private ICompletionProposal[] provideAttributeXMLAttributesValuesProposals( Document domDoc, int offset, String attributeName, String attributeValue ) {		
		// TODO
		return null;
	}
	
	private ICompletionProposal[] provideVirtualNodeXMLAttributesValuesProposals( Document domDoc, int offset, String attributeName, String attributeValue ) {		
		// TODO
		return null;
	}
	
	private ICompletionProposal[] provideDefinitionXMLAttributesValuesProposals( Document domDoc, int offset, String attributeName, String attributeValue ) {		
		// TODO
		return null;
	}
	
	private ICompletionProposal[] provideComponentXMLAttributesValuesProposals( Document domDoc, int offset, String attributeName, String attributeValue ) {		
		// TODO
		return null;
	}
	
	private ICompletionProposal[] provideLoggerXMLAttributesValuesProposals( Document domDoc, int offset, String attributeName, String attributeValue ) {		
		// TODO
		return null;
	}
	
	private ICompletionProposal[] provideControllerXMLAttributesValuesProposals( Document domDoc, int offset, String attributeName, String attributeValue ) {		
		// TODO
		return null;
	}
	
	private ICompletionProposal[] provideContentXMLAttributesValuesProposals( Document domDoc, int offset, String attributeName, String attributeValue ) {		
		// TODO
		return null;
	}
	
	private ICompletionProposal[] provideTemplateControllerXMLAttributesValuesProposals( Document domDoc, int offset, String attributeName, String attributeValue ) {		
		// TODO
		return null;
	}
	
	private ICompletionProposal[] provideBindingXMLAttributesValuesProposals( Document domDoc, int offset, String attributeName, String attributeValue ) {		
		// TODO
		return null;
	}
	/**
	 * The public facade to get proposals for XML Attributes values.
	 * 
	 * @param domDoc the IDocument managed by the editor.
	 * @param markupName the first previous mark-up name near of the offset position in the document.
	 * @param attributeName the attribute name 
	 * @param attributeValue the attribute value, i.e. what the user already typed in.
	 * @param offset the document offset.
	 * @return a list of proposals for auto-completion, adapted to the context.
	 */
	public ICompletionProposal[] makeProposals( Document domDoc, String markupName, String attributeName, String attributeValue, int offset ) {
		try {
			
			// remove namespaces...
			String[] names = markupName.split( ":" ); //$NON-NLS-1$
			if( names.length > 0 )
				markupName = names[ names.length-1 ];				
			
			// make proposals
			if(markupName.equalsIgnoreCase("interface")){
				return provideInterfaceXMLAttributesValuesProposals(domDoc,offset,attributeName, attributeValue);
			}else if(markupName.equalsIgnoreCase("attributes")){
				return provideAttributesXMLAttributesNamesProposals(domDoc,offset,attributeName);
			}else if(markupName.equalsIgnoreCase("attribute")){
				return provideAttributeXMLAttributesNamesProposals(domDoc,offset,attributeName);
			}else if(markupName.equalsIgnoreCase("virtual-node")){
				return provideVirtualNodeXMLAttributesNamesProposals(domDoc,offset,attributeName);
			}else if(markupName.equalsIgnoreCase("definition")){
				return provideDefinitionXMLAttributesNamesProposals(domDoc,offset,attributeName);
			}else if(markupName.equalsIgnoreCase("component")){
				return provideComponentXMLAttributesNamesProposals(domDoc,offset,attributeName);
			}else if(markupName.equalsIgnoreCase("logger")){
				return provideLoggerXMLAttributesNamesProposals(domDoc,offset,attributeName);
			}else if(markupName.equalsIgnoreCase("controller")){
				return provideControllerXMLAttributesNamesProposals(domDoc,offset,attributeName);
			}else if(markupName.equalsIgnoreCase("content")){
				return provideContentXMLAttributesNamesProposals(domDoc,offset,attributeName);
			}else if(markupName.equalsIgnoreCase("template-controller")){
				return provideTemplateControllerXMLAttributesNamesProposals(domDoc,offset,attributeName);
			}else if(markupName.equalsIgnoreCase("binding")){
				return provideBindingXMLAttributesNamesProposals(domDoc,offset,attributeName);
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
		}		
		return null;
	}
}
