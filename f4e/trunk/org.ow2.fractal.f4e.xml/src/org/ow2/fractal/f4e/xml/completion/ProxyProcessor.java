package org.ow2.fractal.f4e.xml.completion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.contentassist.IContextInformationValidator;
import org.eclipse.wst.sse.core.StructuredModelManager;
import org.eclipse.wst.sse.core.internal.provisional.IStructuredModel;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMModel;
import org.ow2.fractal.f4e.xml.completion.attributes.XMLAttributeAssistProcessor;
import org.ow2.fractal.f4e.xml.completion.elements.XMLElementAssistProcessor;
import org.ow2.fractal.f4e.xml.utils.FractalUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class ProxyProcessor implements IContentAssistProcessor {
	/** The additional content assistants provided by this editor. */
	private AbstractProcessor[] customProcessors;
	/** The content assistants this editor has inherited. */
	private IContentAssistProcessor[] inheritedProcessors;
	
	
	/** 
	 * Used to sort proposals alphabetically.
	 * However, XML instructions and comments proposals are placed in last positions.
	 */
	private static final Comparator<ICompletionProposal> proposalComparator = 
		new Comparator<ICompletionProposal> () {
			public int compare(ICompletionProposal o1, ICompletionProposal o2) {
				try {
					// '<!' < '<?' < "xsi..." < "xmlns..." < anything else... 
					// Same first symbol implies we will decide by checking the displayed string.
					int value1 = getValueOf( o1.getAdditionalProposalInfo());
					int value2 = getValueOf( o2.getAdditionalProposalInfo());
					
					if( value1 == 0 && value2 != 0 )
						return -1;
					if( value1 != 0 && value2 == 0 )
						return 1;
					if( value1 != 0 && value2 != 0 )
						return value2 - value1;
					
					// Here, either none of them start with a specific prefix or they have the same prefix.				
					// Alphabetical order.
					String text1 = o1.getDisplayString();
					String text2 = o2.getDisplayString();
					
					return text1.compareTo( text2 );
				}
				catch( Exception e ) {
					e.printStackTrace();
				}
				
				return 0;
			}	
			
			/**
			 * Give a value to strings beginning with a special character.
			 * Non-special strings get 0 as return.
			 * 
			 * @param string
			 * @return
			 */
			private int getValueOf( String string ) {
				if( string == null || string.equals( "" )) //$NON-NLS-1$
					return 0;
				
				if( string.startsWith( "&lt;!" )) //$NON-NLS-1$
					return 1;
				if( string.startsWith( "&lt;?" )) //$NON-NLS-1$
					return 2;
				if( string.startsWith( "xsi" )) //$NON-NLS-1$
					return 3;
				if( string.startsWith( "xmlns" )) //$NON-NLS-1$
					return 4;
				
				return 0;
			}
	};
	
	
	/**
	 * The constructor creates all the content assistants provided by this editor.
	 * @param processors the content assistants inherited from the parent XML editor.
	 */
	public ProxyProcessor( IContentAssistProcessor[] processors ) {
		inheritedProcessors = processors;
		customProcessors = new AbstractProcessor[ 2 ];
		                                          
		customProcessors[ 0 ] = new XMLAttributeAssistProcessor();
		customProcessors[ 1 ] = new XMLElementAssistProcessor();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#computeCompletionProposals(org.eclipse.jface.text.ITextViewer, int)
	 */
	public ICompletionProposal[] computeCompletionProposals( ITextViewer viewer, int offset ) {
		List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal> ();
		
		// Get DOM model and send it to the content assistants (access rules issues require minimal use).
		IStructuredModel model = StructuredModelManager.getModelManager().getExistingModelForRead( viewer.getDocument());
		
		IDOMModel domModel = (IDOMModel) model;
		Document domDoc = domModel.getDocument();

		// Get proposals from content assistants.
		for( AbstractProcessor processor : customProcessors ) {
			ICompletionProposal[] prop = processor.computeCompletionProposals( viewer, offset, domDoc );
			if( prop != null ) 
				proposals.addAll( Arrays.asList( prop ));
		}

		if( inheritedProcessors != null ) {
			for( IContentAssistProcessor processor : inheritedProcessors ) {
				ICompletionProposal[] prop = processor.computeCompletionProposals( viewer, offset );
				if( prop != null ) {
					
					// Patch of the poor...
				//	List<ICompletionProposal> cleanProposals = removeUselessElements( Arrays.asList( prop ), domDoc );					
				//	proposals.addAll( cleanProposals );
				}
			}
		}
				
		// Remove duplicates in proposals.
		// Proposals are duplicates if they have the same "displayed text".
		Map<String, ICompletionProposal> noDuplicateProposals = new HashMap<String, ICompletionProposal> ();
		for( ICompletionProposal proposal : proposals ) {	
			if( !noDuplicateProposals.containsKey( proposal.getDisplayString()))
				noDuplicateProposals.put( proposal.getDisplayString(), proposal );
		}
		proposals = new ArrayList<ICompletionProposal> ( noDuplicateProposals.size());
		proposals.addAll( noDuplicateProposals.values());
		
		// Sort proposals.
		Collections.sort( proposals, proposalComparator );
		
		ICompletionProposal[] result = new ICompletionProposal[ proposals.size() ];
		return proposals.toArray( result );
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#computeContextInformation(org.eclipse.jface.text.ITextViewer, int)
	 */
	public IContextInformation[] computeContextInformation( ITextViewer viewer, int offset ) {
		List<IContextInformation> contexts = new ArrayList<IContextInformation> ();
		
		for( IContentAssistProcessor processor : customProcessors ) {
			IContextInformation[] prop = processor.computeContextInformation( viewer, offset );
			if( prop != null )
				contexts.addAll( Arrays.asList( prop ));
		}
		
		if( inheritedProcessors != null ) {
			for( IContentAssistProcessor processor : inheritedProcessors ) {
				IContextInformation[] prop = processor.computeContextInformation( viewer, offset );
				if( prop != null )
					contexts.addAll( Arrays.asList( prop ));
			}
		}
				
		IContextInformation[] result = new IContextInformation[ contexts.size() ];
		return contexts.toArray( result );
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#getCompletionProposalAutoActivationCharacters()
	 */
	public char[] getCompletionProposalAutoActivationCharacters() {
		Set<Character> chars = new HashSet<Character> ();
		
		for( IContentAssistProcessor processor : customProcessors ) {
			char[] cs = processor.getCompletionProposalAutoActivationCharacters ();
			if( cs == null )
				continue;
				
			for( char c : cs )
				chars.add( new Character( c ));
		}
			
		if( inheritedProcessors != null ) {
			for( IContentAssistProcessor processor : inheritedProcessors ) {
				char[] cs = processor.getCompletionProposalAutoActivationCharacters ();
				if( cs == null )
					continue;
					
				for( char c : cs )
					chars.add( new Character( c ));
			}	
		}
		
		char[] result = new char[ chars.size() ];
		int index = 0;
		for( Character c : chars )  
			result[ index++ ] = c.charValue();
		
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#getContextInformationAutoActivationCharacters()
	 */
	public char[] getContextInformationAutoActivationCharacters() {
		Set<Character> chars = new HashSet<Character> ();
		
		for( IContentAssistProcessor processor : customProcessors ) {
			char[] cs = processor.getContextInformationAutoActivationCharacters();
			if( cs == null )
				continue;

			for( char c : cs )
				chars.add( new Character( c ));
		}

		if( inheritedProcessors != null ) {
			for( IContentAssistProcessor processor : inheritedProcessors ) {
				char[] cs = processor.getContextInformationAutoActivationCharacters();
				if( cs == null )
					continue;
	
				for( char c : cs )
					chars.add( new Character( c ));
			}
		}
		
		char[] result = new char[ chars.size() ];
		int index = 0;
		for( Character c : chars )  
			result[ index++ ] = c.charValue();
		
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#getContextInformationValidator()
	 */
	public IContextInformationValidator getContextInformationValidator() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#getErrorMessage()
	 */
	public String getErrorMessage() {
		
		// This is why we have added content assistants in a specific order in the constructor.
		for( IContentAssistProcessor processor : customProcessors ) {
			String errorMessage = processor.getErrorMessage();
			if( errorMessage != null )
				return errorMessage;
		}
		
		if( inheritedProcessors == null )
			return null;
		
		for( IContentAssistProcessor processor : inheritedProcessors ) {
			String errorMessage = processor.getErrorMessage();
			if( errorMessage != null )
				return errorMessage;
		}
		
		return null;
	}
}
