package org.ow2.fractal.f4e.xml.utils.filters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IJavaProject;

/**
 * Get resources to skip (avoid binary folders, as in Java projects).
 */
public class SkippedResources {
	
	/**
	 * Get the folders to skip in a given project (e.g. binary folders in Java projects).
	 * @param project
	 * @return
	 */
	public static Set<IFolder> getResourcesToSkip( IProject project ) {		
		IJavaProject javaProject = JavaProjectUtils.getJavaProject( project );
		if( javaProject == null )
			return Collections.emptySet();
		
		return JavaProjectUtils.getBinaryFolders( javaProject );
	}
	
	/**
	 * Get the sub-folders to skip in a given folder (e.g. binary folders in Java projects).
	 * @param folder
	 * @return
	 */
	public static Set<IFolder> getResourcesToSkip( IFolder folder ) {
		Set<IFolder> result = getResourcesToSkip( folder.getProject());
		IPath folderPath = folder.getProjectRelativePath();
		
		// Remove folders which are not into the folder argument.
		// We process it by checking their paths.
		for( IFolder projectFolder : result ) {
			IPath projectFolderPath = projectFolder.getProjectRelativePath();			
			if( !folderPath.isPrefixOf( projectFolderPath ))
				result.remove( projectFolder );
		}
		
		return result;
	}
	
	/**
	 * Remove from a list of files the files which are in <i>folders to skip</i>.
	 * @param filesToFilter files of a same project.
	 * @return the initial list of files without the files being in folders to skip.
	 */
	public static List<IFile> filterAllowedFiles( List<IFile> filesToFilter ) {
		if( filesToFilter.isEmpty())
			return Collections.emptyList();		
		
		List<IFile> result = new ArrayList<IFile> ();
		Set<IFolder> foldersToSkip = getResourcesToSkip( filesToFilter.get( 0 ).getProject());
		
		// Only add files which are not into the folders to skip.
		// We process it by checking their paths.
		for( IFile file : filesToFilter ) {
			IPath filePath = file.getProjectRelativePath();			
			boolean fileIsInFolder = false;
			
			for( IFolder projectFolder : foldersToSkip ) {
				IPath folderPath = projectFolder.getProjectRelativePath();
				if( folderPath.isPrefixOf( filePath )) {
					fileIsInFolder = true;
					break;
				}					
			}
			
			// fileIsInFolder = true <=> the file is not in any "forbidden folder"
			if( !fileIsInFolder )
				result.add( file );
		}
		
		return result;
	}
}
