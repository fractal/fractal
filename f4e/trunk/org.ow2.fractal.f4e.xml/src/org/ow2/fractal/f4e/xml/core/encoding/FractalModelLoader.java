package org.ow2.fractal.f4e.xml.core.encoding;

import org.eclipse.core.resources.IFile;
import org.eclipse.wst.sse.core.internal.document.IDocumentLoader;
import org.eclipse.wst.sse.core.internal.encoding.ContentTypeEncodingPreferences;
import org.eclipse.wst.sse.core.internal.provisional.IModelLoader;
import org.eclipse.wst.sse.core.internal.provisional.IStructuredModel;
import org.eclipse.wst.xml.core.internal.document.DOMModelImpl;
import org.eclipse.wst.xml.core.internal.modelhandler.XMLModelLoader;
import org.eclipse.wst.xml.core.internal.provisional.contenttype.ContentTypeIdForXML;

public class FractalModelLoader extends XMLModelLoader {

	public IModelLoader newInstance() {
		return new FractalModelLoader();
	}

	public IDocumentLoader getDocumentLoader() {
		if (documentLoaderInstance == null) {
			documentLoaderInstance = new FractalDocumentLoader();
		}
		return documentLoaderInstance;
	}
	
	public IStructuredModel newModel() {
		return new DOMModelImpl();
	}
}
