package org.ow2.fractal.f4e.xml.outline;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.wst.sse.core.internal.provisional.IModelStateListener;
import org.eclipse.wst.sse.core.internal.provisional.IStructuredModel;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMDocument;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMModel;
import org.ow2.fractal.f4e.xml.utils.FractalUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * The content provider for the outline view of the Fractal XML editor.
 * Uses the editor input as input (and therefore it uses internal classes of WTP).
 * 
 */
public class FractalContentProvider implements ITreeContentProvider {	
	// Basis.
	/** The edited file. */
	private IFile editedFile;
	/** The tree viewer. */
	private Viewer viewer;
	
	/** 
	 * Key = child node, value = parent node 
	 * (used for includes only, since we break the XML tree when using several DOMs). 
	 * 
	 * If this map contains an element with key "k", this means the parent of "k" in the
	 * outline view is its value "v". "v" must to be an "include" node.
	 */
	private Map<Node, Node> includeNodes = new HashMap<Node, Node> ();
	
	// Listeners.
	/** The listener waits model changes and updates the outline view. */
	private IModelStateListener modelListener = new IModelStateListener() {
		
		public void modelChanged(IStructuredModel model) {
			refreshViewer();
			
		}

		public void modelReinitialized(IStructuredModel structuredModel) {
			try {
				refreshViewer();
			} catch( Exception e ) {
				e.printStackTrace();
			}
		}

		public void modelDirtyStateChanged(IStructuredModel model, boolean isDirty) {}
		public void modelAboutToBeChanged(IStructuredModel model) {
		}
		public void modelAboutToBeReinitialized(IStructuredModel structuredModel) {}
		public void modelResourceDeleted(IStructuredModel model) {}
		public void modelResourceMoved(IStructuredModel oldModel, IStructuredModel newModel) {
		}
	};
	
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
	 */
	public Object[] getChildren( Object parentElement ) {		
		// 
		// Children for a Node element
		if( parentElement instanceof Node ) {		
			Node node = (Node) parentElement;
			if( node.getNodeType() == Node.ELEMENT_NODE 
					&& node.getNodeName() != null ) {
					
				NodeList children = node.getChildNodes();
				if( children == null )
					return new Object[ 0 ];
					
				ArrayList<Node> result = new ArrayList<Node> ();
				for( int i=0; i<children.getLength(); i++ ) {
					Node child = children.item( i );
					if( child.getNodeType() == Node.ELEMENT_NODE 
							&& node.getNodeName() != null )
						result.add( child );
				}
				
				return result.toArray();
			}
		}		
		return new Object[0];
	}	

	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
	 */
	public Object getParent(Object element) {
		if( element instanceof Node ) {		
			Node node = (Node) element;
			if( node.getNodeType() == Node.ELEMENT_NODE 
					&& node.getNodeName() != null ) {
				
				if( !includeNodes.containsKey( node ))
					return node.getParentNode();
				else
					return includeNodes.get( node );
			}
		}		
		return null;
	}

	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
	 */
	public boolean hasChildren(Object element) {
		return getChildren( element ).length > 0;
	}

	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
	 */
	public Object[] getElements(Object inputElement) {
		// The root is usually an instance of an XMLStructuredModel in which case we want to extract the document.
		if( inputElement instanceof IDOMModel ) {
			IDOMDocument topNode = ((IDOMModel) inputElement ).getDocument();
			
		
			String workspaceRelativePath = topNode.getModel().getBaseLocation();
			IResource resource = ResourcesPlugin.getWorkspace().getRoot().findMember( workspaceRelativePath );
			if( IFile.class.isAssignableFrom( resource.getClass()))
				defineWorkspaceResources((IFile) resource );
			
			// Get the composite for root element.
			Node root = topNode.getFirstChild();
			while( root != null 
					&& root.getNodeType() != Node.ELEMENT_NODE ){
				root = root.getNextSibling();	
			}
			
			if( root != null 
					&& root.getNodeName() != null
					&& FractalUtils.removeNamespacePrefix( root.getNodeName())
						.toLowerCase().equals( "definition" )) {
				
//				IFractalResource fresource = (IFractalResource)FractalTransactionalEditingDomain.getEditingDomain().getResourceSet().getResource(URI.createPlatformResourceURI(resource.getFullPath().toOSString(),true), true);
//				new EMF2DOMSSEAdapter(root, new EMF2DOMRenderer(){}, new Translator(root.getNodeName(),fresource.getRootDefinition().eClass()));
				return new Object[] { root };
			}

		}		
		return new Object[0];
	}

	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	public void dispose() {
		// The modelListener should be removed in "inputChanged".
	}

	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
	 */
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		this.viewer = viewer;
		
		// Remove the model modelListener from the old input.
		if( oldInput != null && oldInput instanceof IDOMModel ) {
			((IDOMModel) oldInput ).removeModelStateListener( modelListener );
		}
		
		// Clear includes.
		includeNodes.clear();
		
		// Add the model modelListener on the new input.
		if( newInput != null && newInput instanceof IDOMModel ) {
			((IDOMModel) newInput ).addModelStateListener( modelListener );
		}
	}
	
	
	/**
	 * Refreshes the tree viewer.
	 */
	private void refreshViewer() {
		if( viewer != null )
			viewer.refresh();
	}
	
	/**
	 * Define workspace resources.
	 * Gets the edited file and gets prepared for includes management.
	 * @param file not null.
	 */
	private void defineWorkspaceResources ( IFile file ) {
		editedFile = file;
			
		// Create the utils for includes. 
		IProject project = editedFile.getProject();
	}
}
