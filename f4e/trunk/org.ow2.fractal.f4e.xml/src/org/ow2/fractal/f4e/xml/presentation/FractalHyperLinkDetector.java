package org.ow2.fractal.f4e.xml.presentation;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.internal.core.JavaProject;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.hyperlink.IHyperlink;
import org.eclipse.jface.text.hyperlink.IHyperlinkDetector;
import org.eclipse.jface.text.hyperlink.IHyperlinkDetectorExtension;
import org.eclipse.wst.xml.core.internal.document.AttrImpl;
import org.eclipse.wst.xml.core.internal.document.DocumentImpl;
import org.eclipse.wst.xml.core.internal.document.ElementImpl;
import org.ow2.fractal.f4e.xml.F4EXmlEditorPlugin;
import org.ow2.fractal.f4e.xml.utils.Utils;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class FractalHyperLinkDetector implements IHyperlinkDetector, IHyperlinkDetectorExtension, IAdaptable {
	private Utils contextInformationUtils = new Utils();
	
	public IHyperlink[] detectHyperlinks(ITextViewer textViewer,
			IRegion region, boolean canShowMultipleHyperlinks) {
		// TODO Auto-generated method stub
		
		IDocument document = textViewer.getDocument();
	
		Object object = contextInformationUtils.getDOMElement(region.getOffset(), document);
		
		if(object instanceof ElementImpl){
			ElementImpl element = (ElementImpl)object;
			
			NamedNodeMap map = element.getAttributes();
			for(int i=0; i<map.getLength();i++){
				Node node = map.item(i);
				if(node instanceof AttrImpl){
					AttrImpl attr = (AttrImpl)node;
					
					if(attr.getValueRegionStartOffset() < region.getOffset() &&
							attr.getValueRegionStartOffset() + attr.getValue().length() + 2 > region.getOffset()){
						
						if("signature".equals(attr.getName())){
							JavaProject javaProject = Utils.getInstance().getJavaProject(element);
							return new IHyperlink[]{new JavaInterfaceHyperLink(region,javaProject,attr.getValue())};
						}else if("class".equals(attr.getName())){
							JavaProject javaProject = Utils.getInstance().getJavaProject(element);
							return new IHyperlink[]{new JavaInterfaceHyperLink(region,javaProject,attr.getValue())};
						}else if("extends".equals(attr.getName())){
							
						}else if("definition".equals(attr.getName())){
							
						}
					}
				}
			}
			
		}
		return null;
	}

	/**
	 * The context of this hyperlink detector.
	 */
	private IAdaptable fContext;

	/**
	 * Sets this hyperlink detector's context which
	 * is responsible to provide the adapters.
	 * 
	 * @param context the context for this hyperlink detector
	 * @throws IllegalArgumentException if the context is <code>null</code>
	 * @throws IllegalStateException if this method is called more than once
	 */
	public final void setContext(IAdaptable context) throws IllegalStateException, IllegalArgumentException {
		Assert.isLegal(context != null);
		if (fContext != null)
			throw new IllegalStateException();
		fContext= context;
	}

	/*
	 * @see org.eclipse.jface.text.hyperlink.IHyperlinkDetectorExtension#dispose()
	 */
	public void dispose() {
		fContext= null;
	}

	/**
	 * Returns an object which is an instance of the given class
	 * and provides additional context for this hyperlink detector.
	 *
	 * @param adapterClass the adapter class to look up
	 * @return an instance that can be cast to the given class, 
	 *			or <code>null</code> if this object does not
	 *			have an adapter for the given class
	 */
	public Object getAdapter(Class adapterClass) {
		Assert.isLegal(adapterClass != null);
		if (fContext != null)
			return fContext.getAdapter(adapterClass);
		return null;
	}
	
}
