package org.ow2.fractal.f4e.xml.utils.filters;

import java.util.Set;
import java.util.TreeSet;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;

/**
 * Useful methods to get information about Java projects.
 */
public class JavaProjectUtils {
	
	/**
	 * Determine whether an IProject is a Java Project.
	 * @param iProject the iProject to check.
	 * @return the IJavaProject associated to this IProject if it is a Java project, null otherwise.
	 */
	public static IJavaProject getJavaProject( IProject iProject ) {
		try {
			if( iProject.hasNature( JavaCore.NATURE_ID )) {				
				IJavaProject javaProject = JavaCore.create( iProject );
				return javaProject;
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Find all its binary output folders of a Java project.
	 * A Java project may have several binary output folders, one per source folder.
	 * @param javaProject
	 * @return
	 */
	public static Set<IFolder> getBinaryFolders( IJavaProject javaProject ) {
		Set<IFolder> binaryFolders = new TreeSet<IFolder> ();
		
		try {
			// Get default output folder location.
			IPath outputPath = javaProject.getOutputLocation();
			if( outputPath != null ) {
				IFolder folder = ResourcesPlugin.getWorkspace().getRoot().getFolder( outputPath );
				if( folder.exists()) {
					binaryFolders.add( folder );
				}
			}
			
			// Find all the output folders by looking for source folders.
			for( IClasspathEntry entry : javaProject.getRawClasspath()) {						
				if( entry.getEntryKind() == IClasspathEntry.CPE_SOURCE ) {
					outputPath = entry.getOutputLocation();
					if( outputPath == null )
						continue;
					
					IFolder folder = ResourcesPlugin.getWorkspace().getRoot().getFolder( outputPath );
					if( folder.exists()) {
						binaryFolders.add( folder );
					}
				}
			}
		} catch( JavaModelException e ) {
			e.printStackTrace();
		}
		return binaryFolders;
	}
	
	/**
	 * @param folder
	 * @return return true if the folder argument is a or in binary folder.
	 */
	public static boolean isBinaryFolder( IFolder folder ) {
		IProject iProject = folder.getProject();
		IJavaProject javaProject = getJavaProject( iProject );
		if( javaProject == null )
			return false;
		
		Set<IFolder> binaryFolders = getBinaryFolders( javaProject );
		return binaryFolders.contains( folder );
	}
}
