package org.ow2.fractal.f4e.xml.outline;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.wst.sse.ui.views.contentoutline.ContentOutlineConfiguration;
import org.ow2.fractal.f4e.xml.F4EXmlEditorPlugin;
import org.ow2.fractal.f4e.xml.Messages;
import org.ow2.fractal.f4e.xml.statusline.StatusLineLabelProvider;
import org.ow2.fractal.f4e.xml.utils.FractalUtils;
import org.w3c.dom.Node;


/**
 * The content outline configuration for the Fractal XML editor.
 */
public class FractalContentOutlineConfiguration extends ContentOutlineConfiguration { 
	/** The content provider. */
	private IContentProvider contentProvider;
	/** The label provider for the tree viewer. */
	private ILabelProvider labelProvider;
	/** The label provider for the status line. */
	private ILabelProvider statusLineLabelProvider;
	/** The menu listener for the context menus in the outline view. */
	private IMenuListener nodeMenuListener;
	
	
	public FractalContentOutlineConfiguration() {
		super();
	}
	
	@Override
	public IContentProvider getContentProvider(TreeViewer viewer) {
		if( contentProvider == null ) 
			contentProvider = new FractalContentProvider();
		return contentProvider;
	}
	
	@Override
	public ILabelProvider getLabelProvider(TreeViewer viewer) { 
		if( labelProvider == null )
			labelProvider = new FractalLabelProvider();
		return labelProvider;
	}
	
	@Override
	public ILabelProvider getStatusLineLabelProvider(TreeViewer treeViewer) {
		if( statusLineLabelProvider == null )
			statusLineLabelProvider = new StatusLineLabelProvider();
		return statusLineLabelProvider;
	}
	
	@Override
	protected IContributionItem[] createToolbarContributions(TreeViewer viewer) {
		viewer.setComparator( new NodeComparator());
		
		IContributionItem collapseAllItem = new ActionContributionItem(new CollapseTreeAction(viewer));
		IContributionItem expandAllItem = new ActionContributionItem(new ExpandTreeAction(viewer));
		
		IContributionItem[] items = new IContributionItem[] { collapseAllItem, expandAllItem };		
		return items;
	}
	
	@Override
	public IMenuListener getMenuListener(TreeViewer viewer) {
		if( nodeMenuListener == null )
			nodeMenuListener = new NodeMenuListener( viewer );
		return nodeMenuListener;		
	}
	
	
	
	/* Outline ACTIONS */
	
	/** */
	private final static ImageDescriptor EXPAND_ALL_E =
		F4EXmlEditorPlugin.imageDescriptorFromPlugin( 
				F4EXmlEditorPlugin.PLUGIN_ID, 
				"icons/full/elcl16/expandall.gif" ); //$NON-NLS-1$
	
	/** */
	private final static ImageDescriptor COLLAPSE_ALL_E =
		F4EXmlEditorPlugin.imageDescriptorFromPlugin( 
				F4EXmlEditorPlugin.PLUGIN_ID, 
				"icons/full/elcl16/collapseall.gif" ); //$NON-NLS-1$
	
	/** */
	private final static ImageDescriptor COLLAPSE_ALL_D = 
		F4EXmlEditorPlugin.imageDescriptorFromPlugin( 
				F4EXmlEditorPlugin.PLUGIN_ID, 
				"icons/full/dlcl16/collapseall.gif" ); //$NON-NLS-1$
	
	/**
	 * Add an expand action to help with navigation.
	 */
	private class ExpandTreeAction extends Action {
		private TreeViewer fTreeViewer;

		public ExpandTreeAction(TreeViewer viewer) {
			super( Messages.FractalContentOutlineConfiguration_3, EXPAND_ALL_E );
			setToolTipText( Messages.FractalContentOutlineConfiguration_4 );
			fTreeViewer = viewer;
		}

		public void run() {
			super.run();
			fTreeViewer.expandAll();
		}
	}
	
	/**
	 * Add a collapse action to help with navigation.
	 */
	private class CollapseTreeAction extends Action {
		private TreeViewer fTreeViewer;

		public CollapseTreeAction(TreeViewer viewer) {
			super( Messages.FractalContentOutlineConfiguration_5, COLLAPSE_ALL_E );			
			setDisabledImageDescriptor( COLLAPSE_ALL_D );
			setToolTipText( Messages.FractalContentOutlineConfiguration_6 );
			fTreeViewer = viewer;
		}

		public void run() {
			super.run();
			fTreeViewer.collapseAll();
		}
	}
	
	/**
	 * Order composite elements in the outline view.
	 */
	private class NodeComparator extends ViewerComparator {
		static final int DEFAULT_CATEGORY = 0;
		static final int COMPONENT_CATEGORY = 1;
		static final int SERVICE_CATEGORY = 2;
		static final int REFERENCE_CATEGORY = 3;
		static final int WIRE_CATEGORY = 4;
		static final int PROPERTY_CATEGORY = 5;
		static final int INCLUDE_CATEGORY = 6;
		static final int IMPLEMENTATION_CATEGORY = 7;		
		static final int BINDING_CATEGORY = 8;
		static final int INTERFACE_CATEGORY = 9;		
		
		@Override
		public int category(Object element) {
			// Element not null and is a node.
			if( element == null 
					|| !( element instanceof Node ))			
			return DEFAULT_CATEGORY;
			
			// Depending on the node...
			String nodeName = 
				FractalUtils.removeNamespacePrefix( ((Node) element).getNodeName());
			nodeName = nodeName.toLowerCase();
			
			if( nodeName.equals( "component" )) //$NON-NLS-1$
				return COMPONENT_CATEGORY;
			if( nodeName.equals( "service" )) //$NON-NLS-1$
				return SERVICE_CATEGORY;
			if( nodeName.equals( "reference" )) //$NON-NLS-1$
				return REFERENCE_CATEGORY;
			if( nodeName.equals( "wire" )) //$NON-NLS-1$
				return WIRE_CATEGORY;
			if( nodeName.equals( "property" )) //$NON-NLS-1$
				return PROPERTY_CATEGORY;
			if( nodeName.equals( "include" )) //$NON-NLS-1$
				return INCLUDE_CATEGORY;
			if( nodeName.startsWith( "implementation." )) //$NON-NLS-1$
				return IMPLEMENTATION_CATEGORY;
			if( nodeName.startsWith( "interface." )) //$NON-NLS-1$
				return INTERFACE_CATEGORY;
			if( nodeName.startsWith( "binding." )) //$NON-NLS-1$
				return BINDING_CATEGORY;
			
			return DEFAULT_CATEGORY;
		}
	}
	
	
	
	/* Node menu actions */
	
	/**
	 * A class which implements IMenuListener and which helps in filling context menus in the outline view.
	 */
	private class NodeMenuListener implements IMenuListener {
		private TreeViewer fTreeViewer;
		private NodeActions actions = new NodeActions();
		
		/**
		 * The constructor keeps the tree viewer.
		 * @param viewer
		 */
		public NodeMenuListener(TreeViewer viewer) {
			super();
			fTreeViewer = viewer;
		}

		/**
		 * Called when the menu is about to be shown.
		 * Delegates context menus filling to {@link NodeActions}.
		 * 
		 * @param manager
		 * @see NodeActions#fillInContextMenu(TreeViewer, IMenuManager)
		 */
		public void menuAboutToShow(IMenuManager manager) {
			manager.setRemoveAllWhenShown( true );
			actions.fillInContextMenu( fTreeViewer, manager );
		}
	}
}
