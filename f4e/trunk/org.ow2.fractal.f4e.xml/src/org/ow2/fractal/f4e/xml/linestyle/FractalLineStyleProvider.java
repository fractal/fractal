package org.ow2.fractal.f4e.xml.linestyle;

import java.util.Arrays;
import java.util.Collection;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.wst.sse.core.internal.provisional.text.IStructuredDocumentRegion;
import org.eclipse.wst.sse.core.internal.provisional.text.ITextRegion;
import org.eclipse.wst.sse.core.internal.provisional.text.ITextRegionCollection;
import org.eclipse.wst.sse.core.internal.provisional.text.ITextRegionList;
import org.eclipse.wst.sse.ui.internal.preferences.ui.ColorHelper;
import org.eclipse.wst.xml.core.internal.document.ElementImpl;
import org.eclipse.wst.xml.core.internal.regions.DOMRegionContext;
import org.eclipse.wst.xml.ui.internal.style.IStyleConstantsXML;
import org.eclipse.wst.xml.ui.internal.style.LineStyleProviderForXML;
import org.ow2.fractal.f4e.xml.utils.Utils;

public class FractalLineStyleProvider extends LineStyleProviderForXML{
	private Utils contextInformationUtils = new Utils();
	IDocument document;
	
	public FractalLineStyleProvider(ISourceViewer sourceViewer) {
		super();
		initializeStyle();
		document = sourceViewer.getDocument();
	}
	
	
	public boolean prepareRegions(ITypedRegion typedRegion, int lineRequestStart, int lineRequestLength, Collection holdResults) {
		final int partitionStartOffset = typedRegion.getOffset();
		final int partitionLength = typedRegion.getLength();
		IStructuredDocumentRegion structuredDocumentRegion = getDocument().getRegionAtCharacterOffset(partitionStartOffset);
		boolean handled = false;
		final int partitionEndOffset = partitionStartOffset + partitionLength - 1;
		while (structuredDocumentRegion != null && structuredDocumentRegion.getStartOffset() <= partitionEndOffset) {

			ITextRegion region = null;
			ITextRegionList regions = structuredDocumentRegion.getRegions();
			int nRegions = regions.size();
			StyleRange styleRange = null;
			for (int i = 0; i < nRegions; i++) {
				region = regions.get(i);
				TextAttribute attr = null;
				TextAttribute previousAttr = null;
				
				if (structuredDocumentRegion.getStartOffset(region) > partitionEndOffset)
					break;
				if (structuredDocumentRegion.getEndOffset(region) <= partitionStartOffset)
					continue;
				
				if (region instanceof ITextRegionCollection) {
					//boolean handledCollection = (super.prepareTextRegion((ITextRegionCollection) region, partitionStartOffset, partitionLength, holdResults));
					//handled = (!handled) ? handledCollection : handled;
				} else {

					attr = getAttributeFor(structuredDocumentRegion, region);
					if (attr != null) {
						handled = true;
						styleRange = createStyleRange(structuredDocumentRegion, region, attr, partitionStartOffset, partitionLength);
						holdResults.add(styleRange);
					}
				}
			}
			structuredDocumentRegion = structuredDocumentRegion.getNext();
		}
		return handled;
	}
	
	private StyleRange createStyleRange(ITextRegionCollection flatNode, ITextRegion region, TextAttribute attr, int startOffset, int length) {
		int start = flatNode.getStartOffset(region);
		if (start < startOffset)
			start = startOffset;
		
		// Base the text end offset off of the, possibly adjusted, start
		int textEnd = start + flatNode.getText(region).length();
		int maxOffset = startOffset + length;
		
		int end = flatNode.getEndOffset(region);
		// Use the end of the text in the region to avoid applying background color to trailing whitespace
		if(textEnd < end)
			end = textEnd;
		// instead of end-start?
		if (end > maxOffset)
			end = maxOffset;
		StyleRange result = new StyleRange(start, end - start, attr.getForeground(), attr.getBackground(), attr.getStyle());
		if((attr.getStyle() & TextAttribute.STRIKETHROUGH) != 0) {
			result.strikeout = true;
		}
		if((attr.getStyle() & TextAttribute.UNDERLINE) != 0) {
			result.underline = true;
		}
		return result;

	}
	
	protected TextAttribute getAttributeFor(ITextRegionCollection collection, ITextRegion region) {
		if(collection != null && region != null){
			String type = region.getType();
			Object object = contextInformationUtils.getDOMElement(collection.getStartOffset(), document);
			if(object instanceof ElementImpl && (type == DOMRegionContext.XML_TAG_NAME || (type == DOMRegionContext.XML_TAG_OPEN) || (type == DOMRegionContext.XML_END_TAG_OPEN) || (type == DOMRegionContext.XML_TAG_CLOSE) || (type == DOMRegionContext.XML_EMPTY_TAG_CLOSE))){
				ElementImpl element = (ElementImpl)object;
				if("interface".startsWith(element.getNodeName())){
					return (TextAttribute) getTextAttributes().get(IFractalStyleConstantsXML.INTERFACE_TAG);
				}else if("definition".startsWith(element.getNodeName())){
					return (TextAttribute) getTextAttributes().get(IFractalStyleConstantsXML.DEFINITION_TAG);
				}else if("component".startsWith(element.getNodeName())){
					return (TextAttribute) getTextAttributes().get(IFractalStyleConstantsXML.COMPONENT_TAG);
				}else if("binding".startsWith(element.getNodeName())){
					return (TextAttribute) getTextAttributes().get(IFractalStyleConstantsXML.BINDING_TAG);
				}else if("content".startsWith(element.getNodeName())){
					return (TextAttribute) getTextAttributes().get(IFractalStyleConstantsXML.CONTENT_TAG);
				}else if("attributes".startsWith(element.getNodeName())){
					return (TextAttribute) getTextAttributes().get(IFractalStyleConstantsXML.ATTRIBUTES_TAG);
				}else if("attribute".startsWith(element.getNodeName())){
					return (TextAttribute) getTextAttributes().get(IFractalStyleConstantsXML.ATTRIBUTE_TAG);
				}else if("controller".startsWith(element.getNodeName())){
					return (TextAttribute) getTextAttributes().get(IFractalStyleConstantsXML.CONTROLLER_TAG);
				}else if("template-controller".startsWith(element.getNodeName())){
					return (TextAttribute) getTextAttributes().get(IFractalStyleConstantsXML.TEMPLATE_CONTROLLER_TAG);
				}else if("logger".startsWith(element.getNodeName())){
					return (TextAttribute) getTextAttributes().get(IFractalStyleConstantsXML.LOGGER_TAG);
				}else if("virtual-node".startsWith(element.getNodeName())){
					return (TextAttribute) getTextAttributes().get(IFractalStyleConstantsXML.VIRTUAL_NODE_TAG);
				}else if("coordinates".startsWith(element.getNodeName())){
					return (TextAttribute) getTextAttributes().get(IFractalStyleConstantsXML.COORDINATES_TAG);
				}
			}
		}
		return getAttributeFor(region);
	}
	
	protected TextAttribute getAttributeFor(ITextRegion region) {
		/**
		 * a method to centralize all the "format rules" for regions
		 * specifically associated for how to "open" the region.
		 */
		// not sure why this is coming through null, but just to catch it
		if (region == null) {
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.CDATA_TEXT);
		}
		String type = region.getType();
		if ((type == DOMRegionContext.XML_CONTENT) || (type == DOMRegionContext.XML_DOCTYPE_INTERNAL_SUBSET)) {
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.XML_CONTENT);
		}
		else if ((type == DOMRegionContext.XML_TAG_OPEN) || (type == DOMRegionContext.XML_END_TAG_OPEN) || (type == DOMRegionContext.XML_TAG_CLOSE) || (type == DOMRegionContext.XML_EMPTY_TAG_CLOSE)) {
			return (TextAttribute) getTextAttributes().get(IFractalStyleConstantsXML.TAG_BORDER);
		}
		else if ((type == DOMRegionContext.XML_CDATA_OPEN) || (type == DOMRegionContext.XML_CDATA_CLOSE)) {
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.CDATA_BORDER);
		}
		else if (type == DOMRegionContext.XML_CDATA_TEXT) {
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.CDATA_TEXT);
		}
		else if (type == DOMRegionContext.XML_TAG_ATTRIBUTE_NAME) {
			return (TextAttribute) getTextAttributes().get(IFractalStyleConstantsXML.TAG_ATTRIBUTE_NAME);
		}
		else if (type == DOMRegionContext.XML_DOCTYPE_DECLARATION) {
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.TAG_NAME);
		}
		else if (type == DOMRegionContext.XML_TAG_NAME) {
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.TAG_NAME);
		}
		else if ((type == DOMRegionContext.XML_TAG_ATTRIBUTE_VALUE)) {
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.TAG_ATTRIBUTE_VALUE);
		}
		else if (type == DOMRegionContext.XML_TAG_ATTRIBUTE_EQUALS) {
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.TAG_ATTRIBUTE_EQUALS);
		}
		else if ((type == DOMRegionContext.XML_COMMENT_OPEN) || (type == DOMRegionContext.XML_COMMENT_CLOSE)) {
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.COMMENT_BORDER);
		}
		else if (type == DOMRegionContext.XML_COMMENT_TEXT) {
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.COMMENT_TEXT);
		}
		else if (type == DOMRegionContext.XML_DOCTYPE_NAME) {
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.DOCTYPE_NAME);
		}
		else if ((type == DOMRegionContext.XML_CHAR_REFERENCE) || (type == DOMRegionContext.XML_ENTITY_REFERENCE) || (type == DOMRegionContext.XML_PE_REFERENCE)) {
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.ENTITY_REFERENCE);
		}
		else if (type == DOMRegionContext.XML_PI_CONTENT) {
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.PI_CONTENT);
		}
		else if ((type == DOMRegionContext.XML_PI_OPEN) || (type == DOMRegionContext.XML_PI_CLOSE)) {
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.PI_BORDER);
		}
		else if ((type == DOMRegionContext.XML_DECLARATION_OPEN) || (type == DOMRegionContext.XML_DECLARATION_CLOSE)) {
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.DECL_BORDER);
		}
		else if (type == DOMRegionContext.XML_DOCTYPE_EXTERNAL_ID_SYSREF) {
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.DOCTYPE_EXTERNAL_ID_SYSREF);
		}
		else if (type == DOMRegionContext.XML_DOCTYPE_EXTERNAL_ID_PUBREF) {
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.DOCTYPE_EXTERNAL_ID_PUBREF);
		}
		else if ((type == DOMRegionContext.XML_DOCTYPE_EXTERNAL_ID_PUBLIC) || (type == DOMRegionContext.XML_DOCTYPE_EXTERNAL_ID_SYSTEM)) {
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.DOCTYPE_EXTERNAL_ID);
		}
		else if (type == DOMRegionContext.UNDEFINED) {
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.CDATA_TEXT);
		}
		else if (type == DOMRegionContext.WHITE_SPACE) {
			// white space is normall not on its own ... but when it is, we'll
			// treat as content
			return (TextAttribute) getTextAttributes().get(IStyleConstantsXML.XML_CONTENT);
		}
		else {
			// default, return null to signal "not handled"
			// in which case, other factories should be tried
			return null;
		}
	}
	
	protected void handlePropertyChange(PropertyChangeEvent event) {
		String styleKey = null;

		if (event != null) {
			String prefKey = event.getProperty();
			// check if preference changed is a style preference
			if (IStyleConstantsXML.TAG_NAME.equals(prefKey)) {
				styleKey = IStyleConstantsXML.TAG_NAME;
			}
			else if (IStyleConstantsXML.TAG_BORDER.equals(prefKey)) {
				styleKey = IStyleConstantsXML.TAG_BORDER;
			}
			else if (IStyleConstantsXML.TAG_ATTRIBUTE_NAME.equals(prefKey)) {
				styleKey = IStyleConstantsXML.TAG_ATTRIBUTE_NAME;
			}
			else if (IStyleConstantsXML.TAG_ATTRIBUTE_VALUE.equals(prefKey)) {
				styleKey = IStyleConstantsXML.TAG_ATTRIBUTE_VALUE;
			}
			else if (IStyleConstantsXML.TAG_ATTRIBUTE_EQUALS.equals(prefKey)) {
				styleKey = IStyleConstantsXML.TAG_ATTRIBUTE_EQUALS;
			}
			else if (IStyleConstantsXML.COMMENT_BORDER.equals(prefKey)) {
				styleKey = IStyleConstantsXML.COMMENT_BORDER;
			}
			else if (IStyleConstantsXML.COMMENT_TEXT.equals(prefKey)) {
				styleKey = IStyleConstantsXML.COMMENT_TEXT;
			}
			else if (IStyleConstantsXML.ENTITY_REFERENCE.equals(prefKey)) {
				styleKey = IStyleConstantsXML.ENTITY_REFERENCE;
			}
			else if (IStyleConstantsXML.CDATA_BORDER.equals(prefKey)) {
				styleKey = IStyleConstantsXML.CDATA_BORDER;
			}
			else if (IStyleConstantsXML.CDATA_TEXT.equals(prefKey)) {
				styleKey = IStyleConstantsXML.CDATA_TEXT;
			}
			else if (IStyleConstantsXML.DECL_BORDER.equals(prefKey)) {
				styleKey = IStyleConstantsXML.DECL_BORDER;
			}
			else if (IStyleConstantsXML.DOCTYPE_EXTERNAL_ID.equals(prefKey)) {
				styleKey = IStyleConstantsXML.DOCTYPE_EXTERNAL_ID;
			}
			else if (IStyleConstantsXML.DOCTYPE_EXTERNAL_ID_PUBREF.equals(prefKey)) {
				styleKey = IStyleConstantsXML.DOCTYPE_EXTERNAL_ID_PUBREF;
			}
			else if (IStyleConstantsXML.DOCTYPE_EXTERNAL_ID_SYSREF.equals(prefKey)) {
				styleKey = IStyleConstantsXML.DOCTYPE_EXTERNAL_ID_SYSREF;
			}
			else if (IStyleConstantsXML.DOCTYPE_NAME.equals(prefKey)) {
				styleKey = IStyleConstantsXML.DOCTYPE_NAME;
			}
			else if (IStyleConstantsXML.PI_CONTENT.equals(prefKey)) {
				styleKey = IStyleConstantsXML.PI_CONTENT;
			}
			else if (IStyleConstantsXML.PI_BORDER.equals(prefKey)) {
				styleKey = IStyleConstantsXML.PI_BORDER;
			}
			else if (IStyleConstantsXML.XML_CONTENT.equals(prefKey)) {
				styleKey = IStyleConstantsXML.XML_CONTENT;
			}
		}

		if (styleKey != null) {
			// overwrite style preference with new value
			addTextAttribute(styleKey);
			super.handlePropertyChange(event);
		}
	}
	
	protected void initializeStyle(){
		if (getColorPreferences() != null) {
			String[] stylePrefs = new String[6];
			stylePrefs[0] = ColorHelper.toRGBString(new RGB(0xF2,0x92,0x15));
			stylePrefs[1] = null;//ColorHelper.toRGBString(new RGB(0xFF,0xFF,0xFF));
			stylePrefs[2] = Boolean.toString(true);
			stylePrefs[3] = Boolean.toString(false);
			stylePrefs[4] = Boolean.toString(false);
			stylePrefs[5] = Boolean.toString(false);
			String style = ColorHelper.packStylePreferences(stylePrefs.clone());
			getColorPreferences().putValue(IFractalStyleConstantsXML.DEFINITION_TAG, style);
			getColorPreferences().putValue(IFractalStyleConstantsXML.COMPONENT_TAG, style);
			
			stylePrefs[0] = ColorHelper.toRGBString(new RGB(0x66,0xbe,0x22));
			style = ColorHelper.packStylePreferences(stylePrefs.clone());
			getColorPreferences().putValue(IFractalStyleConstantsXML.COMMENT_TAG, style);
			
			stylePrefs[0] = ColorHelper.toRGBString(new RGB(0x09,0xA1,0x3D));
			style = ColorHelper.packStylePreferences(stylePrefs.clone());
			getColorPreferences().putValue(IFractalStyleConstantsXML.INTERFACE_TAG, style);
			
			stylePrefs[0] = ColorHelper.toRGBString(new RGB(0x60,0x33,0xF4));
			style = ColorHelper.packStylePreferences(stylePrefs.clone());
			getColorPreferences().putValue(IFractalStyleConstantsXML.BINDING_TAG, style);
			
			stylePrefs[0] = ColorHelper.toRGBString(new RGB(0xaa,0xa1,0x52));
			style = ColorHelper.packStylePreferences(stylePrefs.clone());
			getColorPreferences().putValue(IFractalStyleConstantsXML.CONTENT_TAG, style);
			
			stylePrefs[0] = ColorHelper.toRGBString(new RGB(0x23,0x75,0xd6));
			style = ColorHelper.packStylePreferences(stylePrefs.clone());
			getColorPreferences().putValue(IFractalStyleConstantsXML.ATTRIBUTES_TAG, style);
			
			stylePrefs[0] = ColorHelper.toRGBString(new RGB(0x48,0xac,0xd6));
			style = ColorHelper.packStylePreferences(stylePrefs.clone());
			getColorPreferences().putValue(IFractalStyleConstantsXML.ATTRIBUTE_TAG, style);
			
			stylePrefs[0] = ColorHelper.toRGBString(new RGB(0xa9,0x48,0xd6));
			style = ColorHelper.packStylePreferences(stylePrefs.clone());
			getColorPreferences().putValue(IFractalStyleConstantsXML.CONTROLLER_TAG, style);
			stylePrefs[0] = ColorHelper.toRGBString(new RGB(0x5e,0x48,0xd6));
			style = ColorHelper.packStylePreferences(stylePrefs.clone());
			getColorPreferences().putValue(IFractalStyleConstantsXML.TEMPLATE_CONTROLLER_TAG, style);
			
			stylePrefs[0] = ColorHelper.toRGBString(new RGB(0xd6,0x51,0x48));
			style = ColorHelper.packStylePreferences(stylePrefs.clone());
			getColorPreferences().putValue(IFractalStyleConstantsXML.LOGGER_TAG, style);
			
			stylePrefs[0] = ColorHelper.toRGBString(new RGB(0xd6,0x6b,0x48));
			style = ColorHelper.packStylePreferences(stylePrefs.clone());
			getColorPreferences().putValue(IFractalStyleConstantsXML.VIRTUAL_NODE_TAG, style);
			
			stylePrefs[0] = ColorHelper.toRGBString(new RGB(0x66,0x33,0x22));
			style = ColorHelper.packStylePreferences(stylePrefs.clone());
			getColorPreferences().putValue(IFractalStyleConstantsXML.COORDINATES_TAG, style);
			
			stylePrefs[0] = ColorHelper.toRGBString(new RGB(0x55,0x55,0x55));
			stylePrefs[2] = Boolean.toString(false);
			style = ColorHelper.packStylePreferences(stylePrefs.clone());
			getColorPreferences().putValue(IFractalStyleConstantsXML.TAG_ATTRIBUTE_NAME, style);
			getColorPreferences().putValue(IFractalStyleConstantsXML.TAG_BORDER, style);
		}
	}
	
	protected void loadColors() {
		super.loadColors();
		addTextAttribute(IFractalStyleConstantsXML.DEFINITION_TAG);
		addTextAttribute(IFractalStyleConstantsXML.COMPONENT_TAG);
		addTextAttribute(IFractalStyleConstantsXML.INTERFACE_TAG);
		addTextAttribute(IFractalStyleConstantsXML.BINDING_TAG);
		addTextAttribute(IFractalStyleConstantsXML.CONTENT_TAG);
		addTextAttribute(IFractalStyleConstantsXML.ATTRIBUTES_TAG);
		addTextAttribute(IFractalStyleConstantsXML.ATTRIBUTE_TAG);
		addTextAttribute(IFractalStyleConstantsXML.CONTROLLER_TAG);
		addTextAttribute(IFractalStyleConstantsXML.TEMPLATE_CONTROLLER_TAG);
		addTextAttribute(IFractalStyleConstantsXML.LOGGER_TAG);
		addTextAttribute(IFractalStyleConstantsXML.VIRTUAL_NODE_TAG);
		addTextAttribute(IFractalStyleConstantsXML.COORDINATES_TAG);	
		
		addTextAttribute(IFractalStyleConstantsXML.TAG_ATTRIBUTE_NAME);	
		addTextAttribute(IFractalStyleConstantsXML.TAG_BORDER);	
	}
}
