package org.ow2.fractal.f4e.xml.presentation;

import org.eclipse.jdt.internal.ui.JavaPluginImages;
import org.eclipse.jdt.internal.ui.text.java.hover.JavadocHover;
import org.eclipse.jdt.ui.PreferenceConstants;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.internal.text.html.BrowserInformationControl;
import org.eclipse.jface.internal.text.html.BrowserInformationControlInput;
import org.eclipse.jface.internal.text.html.BrowserInput;
import org.eclipse.jface.internal.text.html.HTMLPrinter;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.text.AbstractReusableInformationControlCreator;
import org.eclipse.jface.text.DefaultInformationControl;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.ITextHoverExtension;
import org.eclipse.jface.text.ITextHoverExtension2;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.information.IInformationProviderExtension2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.wst.sse.ui.internal.derived.HTMLTextPresenter;
import org.eclipse.wst.xml.core.internal.document.AttrImpl;
import org.eclipse.wst.xml.core.internal.document.ElementImpl;
import org.ow2.fractal.f4e.xml.F4EXmlEditorPlugin;
import org.ow2.fractal.f4e.xml.ui.region.AttributeRegion;
import org.ow2.fractal.f4e.xml.utils.Utils;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * This class is used when cursor hovers above a XML attribute. 
 * It allows to display tips info.
 * 
 * It is called by the documentationTextHover extension element of the
 * editorConfiguration extension point.
 * @author Yann Davin
 *
 */
public class XMLAttributeValueHover implements ITextHoverExtension, IInformationProviderExtension2, ITextHoverExtension2, ITextHover{
	protected IInformationControlCreator fPresenterControlCreator = null;

	public String getHoverInfo(ITextViewer textViewer, IRegion hoverRegion) {
		StringBuffer buf = new StringBuffer();
	
		//TODO remove static image
		String imageName = "file:///home/yann/workspace-fractal-eclipse/org.ow2.fractal.f4e.xml/icons/full/obj16/attribute_obj.gif";
		
		if (hoverRegion instanceof AttributeRegion) {
			AttributeRegion region = (AttributeRegion)hoverRegion;
	 			                       //	 //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			HTMLPrinter.insertPageProlog(buf,0,null,new RGB(0xf5,0xf5,0xb5),null);   
									  buf.append("<p><font size=\"2\"><b>");
									  buf.append("<img").append(" src='").append(imageName).append("'/>");
									  buf.append(region.getAttribute().getName());
			                          buf.append(": </b></font>");
			                          buf.append("<font size=\"2\" color=\"09a13d\">");
			                          buf.append(region.getAttribute().getValue());
			                          buf.append("</font></p>");
			                          HTMLPrinter.addPageEpilog(buf);
			                    }
		
		return buf.toString();
	}

	public IRegion getHoverRegion(ITextViewer textViewer, int offset) {
		IDocument document = textViewer.getDocument();
		
		Object object = F4EXmlEditorPlugin.getUtils().getDOMElement(offset, document);
		
		if(object instanceof ElementImpl){
			ElementImpl element = (ElementImpl)object;
			Utils.getInstance().getEMFObject(element);
			NamedNodeMap map = element.getAttributes();
			for(int i=0; i<map.getLength();i++){
				Node node = map.item(i);
				if(node instanceof AttrImpl){
					AttrImpl attr = (AttrImpl)node;
					
					if(attr.getValueRegionStartOffset() < offset &&
							attr.getValueRegionStartOffset() + attr.getValue().length() + 2 > offset){
						return new AttributeRegion(attr.getValueRegionStartOffset(), attr.getValue().length(), attr);
					}
				}
			}
			
		};
		
		return null;
	}

	public Object getHoverInfo2(ITextViewer textViewer, final IRegion hoverRegion) {

	    // Start with the string returned by the older getHoverInfo()
	    final String selection = getHoverInfo(textViewer, hoverRegion);

	    return new BrowserInformationControlInput(null){
	    	public String getHtml(){
	    		return selection;
	    	}

			public Object getInputElement() {
				// TODO Auto-generated method stub
				if(hoverRegion instanceof AttributeRegion){
					return ((AttributeRegion)hoverRegion).getAttribute();
				}
				return selection;
			}

			@Override
			public String getInputName() {
				// TODO Auto-generated method stub
				return "test";
			}
	    	
	    	
	    };
	}

	IInformationControlCreator fHoverControlCreator = null;
	public IInformationControlCreator getHoverControlCreator() {
		// TODO Auto-generated method stub
		if (fHoverControlCreator == null) {
			fHoverControlCreator = new HoverControlCreator(
					getInformationPresenterControlCreator());
		}
		return fHoverControlCreator;

	}
	
	private static final class OpenDeclarationAction extends Action {
		private final BrowserInformationControl fInfoControl;

		public OpenDeclarationAction(BrowserInformationControl infoControl) {
			fInfoControl= infoControl;
			//setText(JavaHoverMessages.JavadocHover_openDeclaration);
      		
			JavaPluginImages.setLocalImageDescriptors(this, "goto_input.gif"); //$NON-NLS-1$ //TODO: better images
		}

		public void run() {
			
			BrowserInformationControlInput infoInput = fInfoControl.getInput();
//			JavaUI.openInEditor(infoInput.getElement());
			//			JavadocBrowserInformationControlInput infoInput= (JavadocBrowserInformationControlInput) fInfoControl.getInput(); //TODO: check cast
			fInfoControl.notifyDelayedInputChange(null);
			fInfoControl.dispose(); //FIXME: should have protocol to hide, rather than dispose

		}
	}
	
	private static final class BackAction extends Action {
		private final BrowserInformationControl fInfoControl;

		public BackAction(final BrowserInformationControl infoControl) {
			fInfoControl = infoControl;
			setText("Previous");
			final ISharedImages images = PlatformUI.getWorkbench()
					.getSharedImages();
			setImageDescriptor(images
					.getImageDescriptor(ISharedImages.IMG_TOOL_BACK));
			setDisabledImageDescriptor(images
					.getImageDescriptor(ISharedImages.IMG_TOOL_BACK_DISABLED));

			update();
		}

		
		public void run() {
			final BrowserInformationControlInput previous = (BrowserInformationControlInput) fInfoControl
					.getInput().getPrevious();
			if (previous != null) {
				fInfoControl.setInput(previous);
			}
		}

		public void update() {
			final BrowserInformationControlInput current = fInfoControl
					.getInput();

			if (current != null && current.getPrevious() != null) {
				final BrowserInput previous = current.getPrevious();
				setToolTipText(String.format("Go back to %s", previous
						.getInputName()));
				setEnabled(true);
			} else {
				setToolTipText("");
				setEnabled(false);
			}
		}
	}

	/**
	 * Action to go forward to the next input in the hover control.
	 */
	@SuppressWarnings("restriction")
	private static final class ForwardAction extends Action {
		private final BrowserInformationControl fInfoControl;

		public ForwardAction(final BrowserInformationControl infoControl) {
			fInfoControl = infoControl;
			setText("Next");
			final ISharedImages images = PlatformUI.getWorkbench()
					.getSharedImages();
			setImageDescriptor(images
					.getImageDescriptor(ISharedImages.IMG_TOOL_FORWARD));
			setDisabledImageDescriptor(images
					.getImageDescriptor(ISharedImages.IMG_TOOL_FORWARD_DISABLED));

			update();
		}

		
		public void run() {
			final BrowserInformationControlInput next = (BrowserInformationControlInput) fInfoControl
					.getInput().getNext();
			if (next != null) {
				fInfoControl.setInput(next);
			}
		}

		public void update() {
			final BrowserInformationControlInput current = fInfoControl
					.getInput();

			if (current != null && current.getNext() != null) {
				setToolTipText(String.format("Go to next %s", current.getNext()
						.getInputName()));
				setEnabled(true);
			} else {
				setToolTipText("");
				setEnabled(false);
			}
		}
	}

	
	public IInformationControlCreator getInformationPresenterControlCreator() {
		if (fPresenterControlCreator == null) {
			fPresenterControlCreator = new PresenterControlCreator();
		}
		return fPresenterControlCreator;
	}
	
	
	public static final class PresenterControlCreator extends
	AbstractReusableInformationControlCreator {

		@SuppressWarnings("restriction")
		@Override
		protected IInformationControl doCreateInformationControl(
				final Shell parent) {
			if (BrowserInformationControl.isAvailable(parent)) {
				final ToolBarManager tbm = new ToolBarManager(SWT.FLAT);

				final String font = PreferenceConstants.APPEARANCE_JAVADOC_FONT;
				final BrowserInformationControl iControl = new BrowserInformationControl(
						parent, font, tbm);
				
				final BackAction backAction = new BackAction(iControl);
				backAction.setEnabled(false);
				tbm.add(backAction);
				final ForwardAction forwardAction = new ForwardAction(iControl);
				tbm.add(forwardAction);
				forwardAction.setEnabled(false);
				
				final OpenDeclarationAction openDeclarationAction= new OpenDeclarationAction(iControl);
				tbm.add(openDeclarationAction);
				
				tbm.update(true);

				return iControl;
			} else {
				return new DefaultInformationControl(parent, EditorsUI
						.getTooltipAffordanceString(), new HTMLTextPresenter(
						true));
			}

		}
	};

	
	public static final class HoverControlCreator extends
	AbstractReusableInformationControlCreator {
		IInformationControlCreator fInformationPresenterControlCreator;
		
		public HoverControlCreator(
				final IInformationControlCreator informationPresenterControlCreator) {
				fInformationPresenterControlCreator = informationPresenterControlCreator;
		}

		@SuppressWarnings("restriction")
		@Override
		protected IInformationControl doCreateInformationControl(
				final Shell parent) {
			if (BrowserInformationControl.isAvailable(parent)) {
				
				BrowserInformationControl browserInformationControl = 
					new BrowserInformationControl(parent,
						JFaceResources.DIALOG_FONT, EditorsUI
						.getTooltipAffordanceString()) {
				
					public IInformationControlCreator getInformationPresenterControlCreator() {
						return fInformationPresenterControlCreator;
					}
					
					protected void createContent(Composite parent) {
						super.createContent(parent);
					}
				};
				
				return browserInformationControl;
			} else {
				return new DefaultInformationControl(parent, EditorsUI
						.getTooltipAffordanceString(), new HTMLTextPresenter(
								true));
			}
		}
	};


}
