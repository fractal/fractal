package org.ow2.fractal.f4e.xml.linestyle;

public interface IFractalStyleConstantsXML {
	public static final String DEFINITION_TAG = "definition_tag";
	public static final String COMMENT_TAG = "comment_tag";
	public static final String INTERFACE_TAG = "interface_tag";
	public static final String COMPONENT_TAG = "component_tag";
	public static final String BINDING_TAG = "binding_tag";
	public static final String CONTENT_TAG = "content_tag";
	public static final String ATTRIBUTES_TAG = "attributes_tag";
	public static final String CONTROLLER_TAG = "controller_tag";
	public static final String TEMPLATE_CONTROLLER_TAG = "template_controller_tag";
	public static final String LOGGER_TAG = "logger_tag";
	public static final String VIRTUAL_NODE_TAG = "virtual_node_tag";
	public static final String COORDINATES_TAG = "coordinates_tag";
	public static final String ATTRIBUTE_TAG = "attribute_tag";
	
	
	public static final String TAG_ATTRIBUTE_NAME = "tag_attribute_name";
	public static final String TAG_BORDER = "tag_border";
}
