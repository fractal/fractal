package org.ow2.fractal.f4e.xml.presentation;

import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.util.EventObject;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.core.commands.operations.IOperationHistoryListener;
import org.eclipse.core.commands.operations.IUndoableOperation;
import org.eclipse.core.commands.operations.OperationHistoryEvent;
import org.eclipse.core.commands.operations.TriggeredOperations;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.command.CommandStackListener;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.IWorkspaceCommandStack;
import org.eclipse.emf.workspace.ResourceUndoContext;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentListener;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.texteditor.DocumentProviderRegistry;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.wst.common.internal.emf.resource.TranslatorResource;
import org.eclipse.wst.sse.core.StructuredModelManager;
import org.eclipse.wst.sse.core.internal.provisional.IModelStateListener;
import org.eclipse.wst.sse.core.internal.provisional.IStructuredModel;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.util.IFractalResource;

public class FractalXMLMultiPageEditorPart extends org.eclipse.wst.xml.ui.internal.tabletree.XMLMultiPageEditorPart{

	
	public FractalXMLMultiPageEditorPart(){
		super();
	}

	public IDocument getInputDocument(){
		IEditorInput input = getEditorInput();
		IDocumentProvider docProvider = DocumentProviderRegistry.getDefault().getDocumentProvider( input );
		IDocument document = docProvider.getDocument( input );
		return document;
	}
	
	
	protected void createSourcePage() throws PartInitException {
		super.createSourcePage();
	}
		
	protected void createPages() {
		super.createPages();
	}
}
