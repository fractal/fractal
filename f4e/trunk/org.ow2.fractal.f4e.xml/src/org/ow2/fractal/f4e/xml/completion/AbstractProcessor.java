package org.ow2.fractal.f4e.xml.completion;

import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.w3c.dom.Document;

/**
 * An abstract class that any custom content-assistant of this plug-in should extend.
 */

public abstract class AbstractProcessor implements IContentAssistProcessor {
	/** The underlying model object of the edited file. */
	protected Document domDoc;

	
	/**
	 * Compute proposals at the given offset.
	 * @see IContentAssistProcessor#computeCompletionProposals(ITextViewer, int)
	 * 
	 * @param viewer
	 * @param offset
	 * @param domDoc the underlying model object of the edited file.
	 * @return
	 */
	public ICompletionProposal[] computeCompletionProposals( ITextViewer viewer, int offset, Document domDoc ) {
		this.domDoc = domDoc;
		return computeCompletionProposals( viewer, offset );
	}
}
