package org.ow2.fractal.f4e.xml.completion.attributes;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.contentassist.IContextInformationValidator;
import org.eclipse.jface.text.source.ISourceViewer;
import org.ow2.fractal.f4e.xml.completion.AbstractProcessor;
import org.ow2.fractal.f4e.xml.utils.Utils;
import org.ow2.fractal.f4e.xml.utils.Utils.ContextData;

public class XMLAttributeAssistProcessor extends AbstractProcessor {
	/** */
	private Utils contextInformationUtils = new Utils();
	/** Cannot be created on creation, the editor must have been initialized. */
	private XMLAttributeProposalMaker xmlAttributeProposalMaker;
	/** */
	private boolean isXMLAttribute;
	
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#computeCompletionProposals(org.eclipse.jface.text.ITextViewer, int)
	 */
	public ICompletionProposal[] computeCompletionProposals( ITextViewer viewer, int offset ) {
		ICompletionProposal[] completionProposals = null;
		isXMLAttribute = false;
		defineProposalMaker();

		try {
			IDocument document = viewer.getDocument();
			
			if(!contextInformationUtils.isInXMLElement(offset, document))
				return null;
			
			if(contextInformationUtils.isTypingXMLElementName(offset, document))
				return null;
				
			String markupName = contextInformationUtils.getXMLElementName(offset, document).getValue();
			ContextData xmlAttributeValueContext = contextInformationUtils.getAttributeValue( offset, document ); 
			
			if(contextInformationUtils.isTypingAttributeName(offset, document) && (xmlAttributeValueContext == null)){
				String xmlAttributeName = contextInformationUtils.getCurrentValue(offset, document);
				completionProposals = xmlAttributeProposalMaker.makeProposals(domDoc, markupName, xmlAttributeName, offset);
			}else{
			
			if( xmlAttributeValueContext == null )
				return null;
				
			ContextData xmlAttributeNameContext = contextInformationUtils.getAttributeName( xmlAttributeValueContext.getOffset(), document );
			if( xmlAttributeNameContext == null )
				return null;
			
			isXMLAttribute = true;
			ContextData xmlElementNameContext = contextInformationUtils.getXMLElementName( xmlAttributeNameContext.getOffset(), document );
			if( xmlElementNameContext == null )
				return null;
			
			completionProposals = xmlAttributeProposalMaker.makeProposals( 
					domDoc, 
					xmlElementNameContext.getValue(),
					xmlAttributeNameContext.getValue(),
					xmlAttributeValueContext.getValue(),
					offset );
			}
			
			return completionProposals;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/** */
	private void defineProposalMaker() {
		if( xmlAttributeProposalMaker == null )
			xmlAttributeProposalMaker = new XMLAttributeProposalMaker();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#computeContextInformation(org.eclipse.jface.text.ITextViewer, int)
	 */
	public IContextInformation[] computeContextInformation(ITextViewer viewer, int offset) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#getCompletionProposalAutoActivationCharacters()
	 */
	public char[] getCompletionProposalAutoActivationCharacters() {
		return new char[] { '/', ':' };		// '/' for relative URIs and ':' for includes.
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#getContextInformationAutoActivationCharacters()
	 */
	public char[] getContextInformationAutoActivationCharacters() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#getContextInformationValidator()
	 */
	public IContextInformationValidator getContextInformationValidator() {
		return null;
	}

	/**
	 * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#getErrorMessage()
	 * If the given offset was in attribute position,
	 * we have to prevent other processors from displaying an awkward error message, in case
	 * where these completion processors do not have any proposal to make.
	 *
	 * If there is no proposal from this processor, we return null (no message) and only the
	 * (optional) error message of one of the following processors will be displayed.
	 *
	 * @return null if we are not in an attribute position, "" otherwise.
	 */
	public String getErrorMessage() {
		return ( !isXMLAttribute ) ? null : ""; //$NON-NLS-1$
	}
}
