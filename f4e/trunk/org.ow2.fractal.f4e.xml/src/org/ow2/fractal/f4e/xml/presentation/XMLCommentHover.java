package org.ow2.fractal.f4e.xml.presentation;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.ITextHoverExtension;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.Region;
import org.eclipse.wst.xml.core.internal.document.AttrImpl;
import org.eclipse.wst.xml.core.internal.document.CommentImpl;
import org.eclipse.wst.xml.core.internal.document.ElementImpl;
import org.ow2.fractal.f4e.xml.utils.Utils;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class XMLCommentHover implements ITextHover{

	public String getHoverInfo(ITextViewer textViewer, IRegion hoverRegion) {
		// TODO Auto-generated method stub
		Object object = Utils.getInstance().getDOMElement(hoverRegion.getOffset(), textViewer.getDocument());
		String info = "";
		if(object instanceof CommentImpl){
			CommentImpl comment = (CommentImpl)object;
			return comment.getData();
		}
		try{
			
			return textViewer.getDocument().get(hoverRegion.getOffset(), hoverRegion.getLength());
		}catch(Exception e){
			return null;
		}
	}
	
	public IRegion getHoverRegion(ITextViewer textViewer, int offset) {
		IDocument document = textViewer.getDocument();
		
		Object object = Utils.getInstance().getDOMElement(offset, document);
		
		if(object instanceof CommentImpl){
			CommentImpl comment = (CommentImpl)object;
			return new Region(comment.getStartOffset(), comment.getLength());
		}
		
		return null;
	}

}
