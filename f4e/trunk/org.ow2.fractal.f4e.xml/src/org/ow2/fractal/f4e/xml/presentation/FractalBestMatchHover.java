package org.ow2.fractal.f4e.xml.presentation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.text.DefaultInformationControl;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.ITextHoverExtension;
import org.eclipse.jface.text.ITextHoverExtension2;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.wst.sse.ui.internal.Logger;
import org.eclipse.wst.sse.ui.internal.taginfo.AnnotationHoverProcessor;
import org.eclipse.wst.sse.ui.internal.taginfo.DebugInfoHoverProcessor;
import org.eclipse.wst.sse.ui.internal.taginfo.ProblemAnnotationHoverProcessor;

/**
 * Custom BestMatchHover that handle TextHovers that implements ITextHoverExtension2.
 * 
 * @author Yann Davin
 *
 */
public class FractalBestMatchHover implements ITextHover, ITextHoverExtension, ITextHoverExtension2  {

	
	private ITextHover fBestMatchHover; // current best match text hover
	private ITextHover fTagInfoHover; // documentation/information hover
	private List fTextHovers; // list of text hovers to consider in best
	// match

	public FractalBestMatchHover(ITextHover infotaghover) {
		fTagInfoHover = infotaghover;
	}
	
	/**
	 * Create a list of text hovers applicable to this best match hover
	 * processor
	 * 
	 * @return List of ITextHover - in abstract class this is empty list
	 */
	private List createTextHoversList() {
		List hoverList = new ArrayList();
		// if currently debugging, then add the debug hover to the list of
		// best match
		if (Logger.isTracing(DebugInfoHoverProcessor.TRACEFILTER)) {
			hoverList.add(new DebugInfoHoverProcessor());
		}

		hoverList.add(new ProblemAnnotationHoverProcessor());
		if (fTagInfoHover != null) {
			hoverList.add(fTagInfoHover);
		}
		hoverList.add(new AnnotationHoverProcessor());
		return hoverList;
	}

	public IInformationControlCreator getHoverControlCreator() {
		IInformationControlCreator creator = null;

		if (fBestMatchHover instanceof ITextHoverExtension) {
			creator = ((ITextHoverExtension) fBestMatchHover).getHoverControlCreator();
		}
		return creator;
	}

	public String getHoverInfo(ITextViewer viewer, IRegion hoverRegion) {
		String displayText = null;

		// already have a best match hover picked out from getHoverRegion call
		if (fBestMatchHover != null) {
			displayText = fBestMatchHover.getHoverInfo(viewer, hoverRegion);
		}
		// either had no best match hover or best match hover returned null
		if (displayText == null) {
			// go through list of text hovers and return first display string
			Iterator i = getTextHovers().iterator();
			while ((i.hasNext()) && (displayText == null)) {
				ITextHover hover = (ITextHover) i.next();
				displayText = hover.getHoverInfo(viewer, hoverRegion);
			}
		}
		return displayText;
	}

	public Object getHoverInfo2(ITextViewer viewer, IRegion hoverRegion) {
		Object displayObject = null;

		// already have a best match hover picked out from getHoverRegion call
		if (fBestMatchHover != null && fBestMatchHover instanceof ITextHoverExtension2) {
			displayObject = ((ITextHoverExtension2)fBestMatchHover).getHoverInfo2(viewer, hoverRegion);
		}else{
			displayObject = getHoverInfo(viewer,hoverRegion);
		}
		return displayObject;
	}
	
	public IRegion getHoverRegion(ITextViewer viewer, int offset) {
		IRegion hoverRegion = null;

		// go through list of text hovers and return first hover region
		ITextHover hover = null;
		Iterator i = getTextHovers().iterator();
		while ((i.hasNext()) && (hoverRegion == null)) {
			hover = (ITextHover) i.next();
			hoverRegion = hover.getHoverRegion(viewer, offset);
		}

		// store the text hover processor that found region
		if (hoverRegion != null)
			fBestMatchHover = hover;
		else
			fBestMatchHover = null;

		return hoverRegion;
	}


	
	
	private List getTextHovers() {
		if (fTextHovers == null) {
			fTextHovers = createTextHoversList();
		}
		return fTextHovers;
	}
	

	public IInformationControlCreator getInformationPresenterControlCreator() {
		return new IInformationControlCreator() {
			public IInformationControl createInformationControl(Shell shell) {
				return new DefaultInformationControl(shell, true);
			}
		};
	}

}
