package org.ow2.fractal.f4e.xml.core.encoding;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.text.IDocumentPartitioner;
import org.eclipse.wst.sse.core.internal.document.IDocumentLoader;
import org.eclipse.wst.sse.core.internal.encoding.ContentTypeEncodingPreferences;
import org.eclipse.wst.xml.core.internal.encoding.XMLDocumentLoader;
import org.eclipse.wst.xml.core.internal.provisional.contenttype.ContentTypeIdForXML;
import org.ow2.fractal.f4e.xml.core.modelhandler.ModelHandlerForFractal;
import org.ow2.fractal.f4e.xml.presentation.StructuredTextPartitionerForFractal;

public class FractalDocumentLoader extends XMLDocumentLoader{

	public IDocumentLoader newInstance() {
		return new FractalDocumentLoader();
	}
	
	public IDocumentPartitioner getDefaultDocumentPartitioner() {
		return new StructuredTextPartitionerForFractal();
	}
	
	protected String getPreferredNewLineDelimiter(IFile file) {
		String delimiter = ContentTypeEncodingPreferences.getPreferredNewLineDelimiter(ModelHandlerForFractal.FractalContentTypeID);
		if (delimiter == null)
			delimiter = super.getPreferredNewLineDelimiter(file);
		return delimiter;
	}
}
