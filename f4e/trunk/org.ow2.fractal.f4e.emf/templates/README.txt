Modifications in Class.javajet

All our model object has a Helper which contains helper method, and doing some technical stuff, like updating
merging or parsing.

Line 91: Our model object interface extends IAdaptable and their IXXXHelper interfaces
<%if (isImplementation) {%>
public<%if (genClass.isAbstract()) {%> abstract<%}%> class <%=genClass.getClassName()%><%=genClass.getTypeParameters().trim()%><%=genClass.getClassExtends()%><%=genClass.getClassImplements()%> 
<%} else {%>
<%String open="<";%>
<%String close=">";%>
<%String adapterName = "org.ow2.fractal.f4e.fractal.adapter.helper.I" +  genClass.getInterfaceName() + "Helper";%>
<%String interfaceHelperAdapterName = "org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter"; %>
public interface <%=genClass.getInterfaceName()%><%=genClass.getTypeParameters().trim()%><%=genClass.getInterfaceExtends()%>, <%=genModel.getImportedName("org.eclipse.core.runtime.IAdaptable")%>, <%=genModel.getImportedName("org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter")%>
<%}%>


Line 1902: Implemantation of the getAdapter and getHelper method
<%if (isImplementation) {%>
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	 <%if (genModel.useClassOverrideAnnotation()) {%>
		@Override
  	<%}%>
	public Object getAdapter(<%=genModel.getImportedName("java.lang.Class")%> adapter) {
		<%=genModel.getImportedName("java.lang.Object")%> object = null;
		<%String adapterName = "org.ow2.fractal.f4e.fractal.adapter.helper.I" +  genClass.getInterfaceName() + "Helper";%>
		
		if(adapter == <%=genModel.getImportedName("org.ow2.fractal.f4e.fractal.adapter.helper.IHelper")%>.class ||
			adapter == <%=genModel.getImportedName(adapterName)%>.class ){
			object = <%=genModel.getImportedName("org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory")%>.getInstance().adapt(this);
		}
		
		return object;
	}
<%}%>

<%if (isImplementation) {%>
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public I<%=genClass.getInterfaceName()%>Helper getHelper() {
		return (I<%=genClass.getInterfaceName()%>Helper)getAdapter(I<%=genClass.getInterfaceName()%>Helper.class);
	}
<%} else {%>
	/** 
	 * @generated
	 */
	<%String adapterName = "org.ow2.fractal.f4e.fractal.adapter.helper.I" +  genClass.getInterfaceName() + "Helper";%>
	<%=genModel.getImportedName(adapterName)%> getHelper();
<%}%>