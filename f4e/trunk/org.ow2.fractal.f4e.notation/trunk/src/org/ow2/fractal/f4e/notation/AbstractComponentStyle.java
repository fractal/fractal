/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.notation;

import org.eclipse.gmf.runtime.notation.Style;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Component Style</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedExtends <em>Collapsed Extends</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedSubComponents <em>Collapsed Sub Components</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedContent <em>Collapsed Content</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedAttributes <em>Collapsed Attributes</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedOthers <em>Collapsed Others</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.fractal.f4e.notation.NotationPackage#getAbstractComponentStyle()
 * @model
 * @generated
 */
public interface AbstractComponentStyle extends Style {
	/**
	 * Returns the value of the '<em><b>Collapsed Extends</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collapsed Extends</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collapsed Extends</em>' attribute.
	 * @see #setCollapsedExtends(Boolean)
	 * @see org.ow2.fractal.f4e.notation.NotationPackage#getAbstractComponentStyle_CollapsedExtends()
	 * @model default="true"
	 * @generated
	 */
	Boolean getCollapsedExtends();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedExtends <em>Collapsed Extends</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Collapsed Extends</em>' attribute.
	 * @see #getCollapsedExtends()
	 * @generated
	 */
	void setCollapsedExtends(Boolean value);

	/**
	 * Returns the value of the '<em><b>Collapsed Sub Components</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collapsed Sub Components</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collapsed Sub Components</em>' attribute.
	 * @see #setCollapsedSubComponents(Boolean)
	 * @see org.ow2.fractal.f4e.notation.NotationPackage#getAbstractComponentStyle_CollapsedSubComponents()
	 * @model default="false"
	 * @generated
	 */
	Boolean getCollapsedSubComponents();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedSubComponents <em>Collapsed Sub Components</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Collapsed Sub Components</em>' attribute.
	 * @see #getCollapsedSubComponents()
	 * @generated
	 */
	void setCollapsedSubComponents(Boolean value);

	/**
	 * Returns the value of the '<em><b>Collapsed Content</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collapsed Content</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collapsed Content</em>' attribute.
	 * @see #setCollapsedContent(Boolean)
	 * @see org.ow2.fractal.f4e.notation.NotationPackage#getAbstractComponentStyle_CollapsedContent()
	 * @model default="false"
	 * @generated
	 */
	Boolean getCollapsedContent();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedContent <em>Collapsed Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Collapsed Content</em>' attribute.
	 * @see #getCollapsedContent()
	 * @generated
	 */
	void setCollapsedContent(Boolean value);

	/**
	 * Returns the value of the '<em><b>Collapsed Attributes</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collapsed Attributes</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collapsed Attributes</em>' attribute.
	 * @see #setCollapsedAttributes(Boolean)
	 * @see org.ow2.fractal.f4e.notation.NotationPackage#getAbstractComponentStyle_CollapsedAttributes()
	 * @model default="true"
	 * @generated
	 */
	Boolean getCollapsedAttributes();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedAttributes <em>Collapsed Attributes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Collapsed Attributes</em>' attribute.
	 * @see #getCollapsedAttributes()
	 * @generated
	 */
	void setCollapsedAttributes(Boolean value);

	/**
	 * Returns the value of the '<em><b>Collapsed Others</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collapsed Others</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collapsed Others</em>' attribute.
	 * @see #setCollapsedOthers(Boolean)
	 * @see org.ow2.fractal.f4e.notation.NotationPackage#getAbstractComponentStyle_CollapsedOthers()
	 * @model default="true"
	 * @generated
	 */
	Boolean getCollapsedOthers();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedOthers <em>Collapsed Others</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Collapsed Others</em>' attribute.
	 * @see #getCollapsedOthers()
	 * @generated
	 */
	void setCollapsedOthers(Boolean value);

} // AbstractComponentStyle
