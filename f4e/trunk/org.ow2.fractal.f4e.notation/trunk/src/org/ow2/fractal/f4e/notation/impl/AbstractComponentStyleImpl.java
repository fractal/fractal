/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.notation.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.ow2.fractal.f4e.notation.AbstractComponentStyle;
import org.ow2.fractal.f4e.notation.NotationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Component Style</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.notation.impl.AbstractComponentStyleImpl#getCollapsedExtends <em>Collapsed Extends</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.notation.impl.AbstractComponentStyleImpl#getCollapsedSubComponents <em>Collapsed Sub Components</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.notation.impl.AbstractComponentStyleImpl#getCollapsedContent <em>Collapsed Content</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.notation.impl.AbstractComponentStyleImpl#getCollapsedAttributes <em>Collapsed Attributes</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.notation.impl.AbstractComponentStyleImpl#getCollapsedOthers <em>Collapsed Others</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AbstractComponentStyleImpl extends EObjectImpl implements AbstractComponentStyle {
	/**
	 * The default value of the '{@link #getCollapsedExtends() <em>Collapsed Extends</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollapsedExtends()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean COLLAPSED_EXTENDS_EDEFAULT = Boolean.TRUE;

	/**
	 * The cached value of the '{@link #getCollapsedExtends() <em>Collapsed Extends</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollapsedExtends()
	 * @generated
	 * @ordered
	 */
	protected Boolean collapsedExtends = COLLAPSED_EXTENDS_EDEFAULT;

	/**
	 * The default value of the '{@link #getCollapsedSubComponents() <em>Collapsed Sub Components</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollapsedSubComponents()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean COLLAPSED_SUB_COMPONENTS_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getCollapsedSubComponents() <em>Collapsed Sub Components</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollapsedSubComponents()
	 * @generated
	 * @ordered
	 */
	protected Boolean collapsedSubComponents = COLLAPSED_SUB_COMPONENTS_EDEFAULT;

	/**
	 * The default value of the '{@link #getCollapsedContent() <em>Collapsed Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollapsedContent()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean COLLAPSED_CONTENT_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getCollapsedContent() <em>Collapsed Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollapsedContent()
	 * @generated
	 * @ordered
	 */
	protected Boolean collapsedContent = COLLAPSED_CONTENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getCollapsedAttributes() <em>Collapsed Attributes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollapsedAttributes()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean COLLAPSED_ATTRIBUTES_EDEFAULT = Boolean.TRUE;

	/**
	 * The cached value of the '{@link #getCollapsedAttributes() <em>Collapsed Attributes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollapsedAttributes()
	 * @generated
	 * @ordered
	 */
	protected Boolean collapsedAttributes = COLLAPSED_ATTRIBUTES_EDEFAULT;

	/**
	 * The default value of the '{@link #getCollapsedOthers() <em>Collapsed Others</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollapsedOthers()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean COLLAPSED_OTHERS_EDEFAULT = Boolean.TRUE;

	/**
	 * The cached value of the '{@link #getCollapsedOthers() <em>Collapsed Others</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollapsedOthers()
	 * @generated
	 * @ordered
	 */
	protected Boolean collapsedOthers = COLLAPSED_OTHERS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractComponentStyleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return NotationPackage.Literals.ABSTRACT_COMPONENT_STYLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCollapsedExtends() {
		return collapsedExtends;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCollapsedExtends(Boolean newCollapsedExtends) {
		Boolean oldCollapsedExtends = collapsedExtends;
		collapsedExtends = newCollapsedExtends;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_EXTENDS, oldCollapsedExtends, collapsedExtends));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCollapsedSubComponents() {
		return collapsedSubComponents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCollapsedSubComponents(Boolean newCollapsedSubComponents) {
		Boolean oldCollapsedSubComponents = collapsedSubComponents;
		collapsedSubComponents = newCollapsedSubComponents;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_SUB_COMPONENTS, oldCollapsedSubComponents, collapsedSubComponents));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCollapsedContent() {
		return collapsedContent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCollapsedContent(Boolean newCollapsedContent) {
		Boolean oldCollapsedContent = collapsedContent;
		collapsedContent = newCollapsedContent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_CONTENT, oldCollapsedContent, collapsedContent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCollapsedAttributes() {
		return collapsedAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCollapsedAttributes(Boolean newCollapsedAttributes) {
		Boolean oldCollapsedAttributes = collapsedAttributes;
		collapsedAttributes = newCollapsedAttributes;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_ATTRIBUTES, oldCollapsedAttributes, collapsedAttributes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCollapsedOthers() {
		return collapsedOthers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCollapsedOthers(Boolean newCollapsedOthers) {
		Boolean oldCollapsedOthers = collapsedOthers;
		collapsedOthers = newCollapsedOthers;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_OTHERS, oldCollapsedOthers, collapsedOthers));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_EXTENDS:
				return getCollapsedExtends();
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_SUB_COMPONENTS:
				return getCollapsedSubComponents();
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_CONTENT:
				return getCollapsedContent();
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_ATTRIBUTES:
				return getCollapsedAttributes();
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_OTHERS:
				return getCollapsedOthers();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_EXTENDS:
				setCollapsedExtends((Boolean)newValue);
				return;
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_SUB_COMPONENTS:
				setCollapsedSubComponents((Boolean)newValue);
				return;
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_CONTENT:
				setCollapsedContent((Boolean)newValue);
				return;
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_ATTRIBUTES:
				setCollapsedAttributes((Boolean)newValue);
				return;
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_OTHERS:
				setCollapsedOthers((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_EXTENDS:
				setCollapsedExtends(COLLAPSED_EXTENDS_EDEFAULT);
				return;
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_SUB_COMPONENTS:
				setCollapsedSubComponents(COLLAPSED_SUB_COMPONENTS_EDEFAULT);
				return;
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_CONTENT:
				setCollapsedContent(COLLAPSED_CONTENT_EDEFAULT);
				return;
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_ATTRIBUTES:
				setCollapsedAttributes(COLLAPSED_ATTRIBUTES_EDEFAULT);
				return;
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_OTHERS:
				setCollapsedOthers(COLLAPSED_OTHERS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_EXTENDS:
				return COLLAPSED_EXTENDS_EDEFAULT == null ? collapsedExtends != null : !COLLAPSED_EXTENDS_EDEFAULT.equals(collapsedExtends);
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_SUB_COMPONENTS:
				return COLLAPSED_SUB_COMPONENTS_EDEFAULT == null ? collapsedSubComponents != null : !COLLAPSED_SUB_COMPONENTS_EDEFAULT.equals(collapsedSubComponents);
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_CONTENT:
				return COLLAPSED_CONTENT_EDEFAULT == null ? collapsedContent != null : !COLLAPSED_CONTENT_EDEFAULT.equals(collapsedContent);
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_ATTRIBUTES:
				return COLLAPSED_ATTRIBUTES_EDEFAULT == null ? collapsedAttributes != null : !COLLAPSED_ATTRIBUTES_EDEFAULT.equals(collapsedAttributes);
			case NotationPackage.ABSTRACT_COMPONENT_STYLE__COLLAPSED_OTHERS:
				return COLLAPSED_OTHERS_EDEFAULT == null ? collapsedOthers != null : !COLLAPSED_OTHERS_EDEFAULT.equals(collapsedOthers);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (collapsedExtends: ");
		result.append(collapsedExtends);
		result.append(", collapsedSubComponents: ");
		result.append(collapsedSubComponents);
		result.append(", collapsedContent: ");
		result.append(collapsedContent);
		result.append(", collapsedAttributes: ");
		result.append(collapsedAttributes);
		result.append(", collapsedOthers: ");
		result.append(collapsedOthers);
		result.append(')');
		return result.toString();
	}

} //AbstractComponentStyleImpl
