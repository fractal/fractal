/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.notation;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.ow2.fractal.f4e.notation.NotationFactory
 * @model kind="package"
 * @generated
 */
public interface NotationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "notation";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.ow2.org/fractal/f4e/1.0.0/notation";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "notation";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	NotationPackage eINSTANCE = org.ow2.fractal.f4e.notation.impl.NotationPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.notation.impl.AbstractComponentStyleImpl <em>Abstract Component Style</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.notation.impl.AbstractComponentStyleImpl
	 * @see org.ow2.fractal.f4e.notation.impl.NotationPackageImpl#getAbstractComponentStyle()
	 * @generated
	 */
	int ABSTRACT_COMPONENT_STYLE = 0;

	/**
	 * The feature id for the '<em><b>Collapsed Extends</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT_STYLE__COLLAPSED_EXTENDS = org.eclipse.gmf.runtime.notation.NotationPackage.STYLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Collapsed Sub Components</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT_STYLE__COLLAPSED_SUB_COMPONENTS = org.eclipse.gmf.runtime.notation.NotationPackage.STYLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Collapsed Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT_STYLE__COLLAPSED_CONTENT = org.eclipse.gmf.runtime.notation.NotationPackage.STYLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Collapsed Attributes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT_STYLE__COLLAPSED_ATTRIBUTES = org.eclipse.gmf.runtime.notation.NotationPackage.STYLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Collapsed Others</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT_STYLE__COLLAPSED_OTHERS = org.eclipse.gmf.runtime.notation.NotationPackage.STYLE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Abstract Component Style</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT_STYLE_FEATURE_COUNT = org.eclipse.gmf.runtime.notation.NotationPackage.STYLE_FEATURE_COUNT + 5;


	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.notation.AbstractComponentStyle <em>Abstract Component Style</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Component Style</em>'.
	 * @see org.ow2.fractal.f4e.notation.AbstractComponentStyle
	 * @generated
	 */
	EClass getAbstractComponentStyle();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedExtends <em>Collapsed Extends</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Collapsed Extends</em>'.
	 * @see org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedExtends()
	 * @see #getAbstractComponentStyle()
	 * @generated
	 */
	EAttribute getAbstractComponentStyle_CollapsedExtends();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedSubComponents <em>Collapsed Sub Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Collapsed Sub Components</em>'.
	 * @see org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedSubComponents()
	 * @see #getAbstractComponentStyle()
	 * @generated
	 */
	EAttribute getAbstractComponentStyle_CollapsedSubComponents();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedContent <em>Collapsed Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Collapsed Content</em>'.
	 * @see org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedContent()
	 * @see #getAbstractComponentStyle()
	 * @generated
	 */
	EAttribute getAbstractComponentStyle_CollapsedContent();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedAttributes <em>Collapsed Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Collapsed Attributes</em>'.
	 * @see org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedAttributes()
	 * @see #getAbstractComponentStyle()
	 * @generated
	 */
	EAttribute getAbstractComponentStyle_CollapsedAttributes();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedOthers <em>Collapsed Others</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Collapsed Others</em>'.
	 * @see org.ow2.fractal.f4e.notation.AbstractComponentStyle#getCollapsedOthers()
	 * @see #getAbstractComponentStyle()
	 * @generated
	 */
	EAttribute getAbstractComponentStyle_CollapsedOthers();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	NotationFactory getNotationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.notation.impl.AbstractComponentStyleImpl <em>Abstract Component Style</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.notation.impl.AbstractComponentStyleImpl
		 * @see org.ow2.fractal.f4e.notation.impl.NotationPackageImpl#getAbstractComponentStyle()
		 * @generated
		 */
		EClass ABSTRACT_COMPONENT_STYLE = eINSTANCE.getAbstractComponentStyle();

		/**
		 * The meta object literal for the '<em><b>Collapsed Extends</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_COMPONENT_STYLE__COLLAPSED_EXTENDS = eINSTANCE.getAbstractComponentStyle_CollapsedExtends();

		/**
		 * The meta object literal for the '<em><b>Collapsed Sub Components</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_COMPONENT_STYLE__COLLAPSED_SUB_COMPONENTS = eINSTANCE.getAbstractComponentStyle_CollapsedSubComponents();

		/**
		 * The meta object literal for the '<em><b>Collapsed Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_COMPONENT_STYLE__COLLAPSED_CONTENT = eINSTANCE.getAbstractComponentStyle_CollapsedContent();

		/**
		 * The meta object literal for the '<em><b>Collapsed Attributes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_COMPONENT_STYLE__COLLAPSED_ATTRIBUTES = eINSTANCE.getAbstractComponentStyle_CollapsedAttributes();

		/**
		 * The meta object literal for the '<em><b>Collapsed Others</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_COMPONENT_STYLE__COLLAPSED_OTHERS = eINSTANCE.getAbstractComponentStyle_CollapsedOthers();

	}

} //NotationPackage
