package org.ow2.fractal.f4e.fractal.diagram.edit.parts;

import org.eclipse.draw2d.Border;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.core.edithelpers.CreateElementRequestAdapter;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IBorderItemEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CreationEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.DragDropEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.internal.properties.Properties;
import org.eclipse.gmf.runtime.diagram.ui.l10n.DiagramUIMessages;
import org.eclipse.gmf.runtime.diagram.ui.requests.ChangePropertyValueRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewAndElementRequest;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Color;
import org.ow2.fractal.f4e.diagram.custom.edit.parts.AbstractComponentEditPart;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.DefinitionResizableEditPolicy;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.InterfaceBorderItemSelectionEditPolicy;
import org.ow2.fractal.f4e.diagram.custom.figures.AbstractComponentNameWrappingLabel;
import org.ow2.fractal.f4e.diagram.custom.figures.AbstractComponentShape;
import org.ow2.fractal.f4e.diagram.custom.figures.AlphaDropShadowBorder;
import org.ow2.fractal.f4e.diagram.custom.figures.ComponentBorder;
import org.ow2.fractal.f4e.diagram.custom.figures.ContainerShape;
import org.ow2.fractal.f4e.diagram.custom.layouts.AutosizeContainerLayout;
import org.ow2.fractal.f4e.diagram.custom.layouts.InterfaceBorderItemLocator;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.DefinitionCanonicalEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.DefinitionItemSemanticEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalVisualIDRegistry;
import org.ow2.fractal.f4e.fractal.diagram.providers.FractalElementTypes;

/**
 * @generated
 */
public class DefinitionEditPart extends AbstractComponentEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 1001;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public DefinitionEditPart(View view) {
		super(view);
	}

	/**
	 * @generated NOT
	 */
	protected void createDefaultEditPolicies() {
		installEditPolicy(EditPolicyRoles.CREATION_ROLE,
				new CreationEditPolicy() {
					public Command getCommand(Request request) {
						if (understandsRequest(request)) {
							if (request instanceof CreateViewAndElementRequest) {
								CreateElementRequestAdapter adapter = ((CreateViewAndElementRequest) request)
										.getViewAndElementDescriptor()
										.getCreateElementRequestAdapter();
								IElementType type = (IElementType) adapter
										.getAdapter(IElementType.class);
								if (type == FractalElementTypes.DefinitionCall_2001) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(DefinitionCompartmentDefinitionExtendsEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								if (type == FractalElementTypes.Content_2003) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(DefinitionCompartmentDefinitionContentEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								if (type == FractalElementTypes.Content_2012) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(DefinitionCompartmentDefinitionContentEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								if (type == FractalElementTypes.VirtualNode_2008) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(DefinitionCompartmentDefinitionOthersEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								if (type == FractalElementTypes.Controller_2007) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(DefinitionCompartmentDefinitionOthersEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								if (type == FractalElementTypes.VirtualNode_2016) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(DefinitionCompartmentDefinitionOthersEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								if (type == FractalElementTypes.Controller_2015) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(DefinitionCompartmentDefinitionOthersEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								
								// Start Yann hand modif	
								// When user click on the popup button to create component
								if (type == FractalElementTypes.Component_2002) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(DefinitionCompartmentDefinitionSubComponentsEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								if (type == FractalElementTypes.AttributesController_2004) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(DefinitionCompartmentDefinitionAttributesEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								if (type == FractalElementTypes.Attribute_2005) {
									IGraphicalEditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(DefinitionCompartmentDefinitionAttributesEditPart.VISUAL_ID));
									EditPart compartmentAttributes = null;
									if(compartmentEditPart != null){
										IGraphicalEditPart attributesEditPart = compartmentEditPart.getChildBySemanticHint(FractalVisualIDRegistry
												.getType(AttributesControllerEditPart.VISUAL_ID));
										if(attributesEditPart != null){
											compartmentAttributes = attributesEditPart.getChildBySemanticHint(FractalVisualIDRegistry
													.getType(AttributesControllerCompartmentAttributesControllerEditPart.VISUAL_ID));
										}
										
									}
									return compartmentAttributes == null ? null
											: compartmentAttributes
													.getCommand(request);
								}
								//End Yann hand modif
							}
							return super.getCommand(request);
						}
						return null;
					}
				});
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new DefinitionItemSemanticEditPolicy());
		installEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE,
				new DragDropEditPolicy());
		installEditPolicy(EditPolicyRoles.CANONICAL_ROLE,
				new DefinitionCanonicalEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());

		// Start yann hand modifs
		installEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE,
				getPrimaryDragEditPolicy());
		
		
		removeEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE);
		
		// Remove the connection handles
		removeEditPolicy(EditPolicyRoles.CONNECTION_HANDLES_ROLE);
		
		// End yann hand modifs
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated NOT
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		LayoutEditPolicy lep = new LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				if (child instanceof IBorderItemEditPart) {
					// Start Hand Modif Yann
					return new InterfaceBorderItemSelectionEditPolicy();
					// End Hand Modif Yann
				}
				EditPolicy result = child
						.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated
	 */
	protected IFigure createNodeShape() {
		DefinitionFigure figure = new DefinitionFigure();
		return primaryShape = figure;
	}

	/**
	 * @generated
	 */
	public DefinitionFigure getPrimaryShape() {
		return (DefinitionFigure) primaryShape;
	}

	/**
	 * @generated NOT
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		// Start yann hand modif
		org.ow2.fractal.f4e.notation.AbstractComponentStyle style = (org.ow2.fractal.f4e.notation.AbstractComponentStyle) this
				.getNotationView().getStyle(
						org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE
								.getAbstractComponentStyle());
		// End yann hand modif
		
		if (childEditPart instanceof DefinitionNameEditPart) {
			((DefinitionNameEditPart) childEditPart).setLabel(getPrimaryShape()
					.getFigureLabelDefinitionName());
			return true;
		}
		if (childEditPart instanceof DefinitionCompartmentDefinitionExtendsEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureDefinitionExtendsAreaFigure();
			if (pane.getLayoutManager() == null) {
				AutosizeContainerLayout layout = new AutosizeContainerLayout();
				pane.setLayoutManager(layout);
			}
			pane
					.add(((DefinitionCompartmentDefinitionExtendsEditPart) childEditPart)
							.getFigure());
			// Start yann hand modifs.
			pane.setVisible(style != null ? !style.getCollapsedExtends()
							: true);
			//End yann hand modifs.
			return true;
		}
		if (childEditPart instanceof DefinitionCompartmentDefinitionSubComponentsEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureDefinitionSubComponentsAreaFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane
					.add(((DefinitionCompartmentDefinitionSubComponentsEditPart) childEditPart)
							.getFigure());
			// Start yann hand modifs.
			pane.setVisible(style != null ? !style.getCollapsedSubComponents()
							: true);
			//End yann hand modifs.
			return true;
		}
		if (childEditPart instanceof DefinitionCompartmentDefinitionContentEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureDefinitionContentAreaFigure();
			if (pane.getLayoutManager() == null) {
				AutosizeContainerLayout layout = new AutosizeContainerLayout();
				pane.setLayoutManager(layout);
			}
			pane
					.add(((DefinitionCompartmentDefinitionContentEditPart) childEditPart)
							.getFigure());
			// Start yann hand modifs.
			pane.setVisible(style != null ? !style.getCollapsedContent()
							: true);
			//End yann hand modifs.
			return true;
		}
		if (childEditPart instanceof DefinitionCompartmentDefinitionAttributesEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureDefinitionAttributesAreaFigure();
			if (pane.getLayoutManager() == null) {
				AutosizeContainerLayout layout = new AutosizeContainerLayout();	
				pane.setLayoutManager(layout);
			}
			pane
					.add(((DefinitionCompartmentDefinitionAttributesEditPart) childEditPart)
							.getFigure());
			// Start yann hand modifs.
			pane.setVisible(style != null ? !style.getCollapsedAttributes()
							: true);
			//End yann hand modifs.
			return true;
		}
		if (childEditPart instanceof DefinitionCompartmentDefinitionOthersEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureDefinitionOthersAreaFigure();
			if (pane.getLayoutManager() == null) {
				AutosizeContainerLayout layout = new AutosizeContainerLayout();
				pane.setLayoutManager(layout);
			}
			pane
					.add(((DefinitionCompartmentDefinitionOthersEditPart) childEditPart)
							.getFigure());
			// Start yann hand modifs.
			pane.setVisible(style != null ? !style.getCollapsedOthers()
							: true);
			//End yann hand modifs.
			return true;
		}
		if (childEditPart instanceof InterfaceEditPart) {
			// Start yann hand modifs.
			InterfaceBorderItemLocator locator = new InterfaceBorderItemLocator(
					(InterfaceEditPart)childEditPart);
			// End yann hand modifs.
			getBorderedFigure().getBorderItemContainer().add(
					((InterfaceEditPart) childEditPart).getFigure(), locator);
			return true;
		}
		if (childEditPart instanceof Interface2EditPart) {
			// Start yann hand modifs.
			InterfaceBorderItemLocator locator = new InterfaceBorderItemLocator(
					(Interface2EditPart)childEditPart);
			// End yann hand modifs.
			getBorderedFigure().getBorderItemContainer().add(
					((Interface2EditPart) childEditPart).getFigure(), locator);
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {

		if (childEditPart instanceof DefinitionCompartmentDefinitionExtendsEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureDefinitionExtendsAreaFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane
					.remove(((DefinitionCompartmentDefinitionExtendsEditPart) childEditPart)
							.getFigure());
			return true;
		}
		if (childEditPart instanceof DefinitionCompartmentDefinitionSubComponentsEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureDefinitionSubComponentsAreaFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane
					.remove(((DefinitionCompartmentDefinitionSubComponentsEditPart) childEditPart)
							.getFigure());
			return true;
		}
		if (childEditPart instanceof DefinitionCompartmentDefinitionContentEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureDefinitionContentAreaFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane
					.remove(((DefinitionCompartmentDefinitionContentEditPart) childEditPart)
							.getFigure());
			return true;
		}
		if (childEditPart instanceof DefinitionCompartmentDefinitionAttributesEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureDefinitionAttributesAreaFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane
					.remove(((DefinitionCompartmentDefinitionAttributesEditPart) childEditPart)
							.getFigure());
			return true;
		}
		if (childEditPart instanceof DefinitionCompartmentDefinitionOthersEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureDefinitionOthersAreaFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane
					.remove(((DefinitionCompartmentDefinitionOthersEditPart) childEditPart)
							.getFigure());
			return true;
		}
		if (childEditPart instanceof InterfaceEditPart) {
			getBorderedFigure().getBorderItemContainer().remove(
					((InterfaceEditPart) childEditPart).getFigure());
			return true;
		}
		if (childEditPart instanceof Interface2EditPart) {
			getBorderedFigure().getBorderItemContainer().remove(
					((Interface2EditPart) childEditPart).getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {

		if (editPart instanceof DefinitionCompartmentDefinitionExtendsEditPart) {
			return getPrimaryShape().getFigureDefinitionExtendsAreaFigure();
		}
		if (editPart instanceof DefinitionCompartmentDefinitionSubComponentsEditPart) {
			return getPrimaryShape()
					.getFigureDefinitionSubComponentsAreaFigure();
		}
		if (editPart instanceof DefinitionCompartmentDefinitionContentEditPart) {
			return getPrimaryShape().getFigureDefinitionContentAreaFigure();
		}
		if (editPart instanceof DefinitionCompartmentDefinitionAttributesEditPart) {
			return getPrimaryShape().getFigureDefinitionAttributesAreaFigure();
		}
		if (editPart instanceof DefinitionCompartmentDefinitionOthersEditPart) {
			return getPrimaryShape().getFigureDefinitionOthersAreaFigure();
		}
		if (editPart instanceof InterfaceEditPart) {
			return getBorderedFigure().getBorderItemContainer();
		}
		if (editPart instanceof Interface2EditPart) {
			return getBorderedFigure().getBorderItemContainer();
		}
		return super.getContentPaneFor(editPart);
	}

	/**
	 * @generated NOT
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(getMapMode()
				.DPtoLP(40), getMapMode().DPtoLP(40));
		
		// Start yann hand modifs
		AlphaDropShadowBorder shadowBorder = new AlphaDropShadowBorder();
		shadowBorder.setShouldDrawDropShadow(true);
		result.setBorder(shadowBorder);
		//End yann hand modifs
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createMainFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane.
	 * Respects layout one may have set for generated figure.
	 * @param nodeShape instance of generated figure class
	 * @generated NOT
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			// Start yann hand modifs
			// layout.setSpacing(getMapMode().DPtoLP(5));
			// End yann hand modifs
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(FractalVisualIDRegistry
				.getType(DefinitionNameEditPart.VISUAL_ID));
	}

	/**
	 * @generated
	 */
	public class DefinitionFigure extends AbstractComponentShape {

		/**
		 * @generated
		 */
		private AbstractComponentShape fFigureDefinitionFigure;
		/**
		 * @generated
		 */
		private AbstractComponentNameWrappingLabel fFigureLabelDefinitionName;
		/**
		 * @generated
		 */
		private ContainerShape fFigureDefinitionExtendsAreaFigure;
		/**
		 * @generated
		 */
		private ContainerShape fFigureDefinitionSubComponentsAreaFigure;
		/**
		 * @generated
		 */
		private ContainerShape fFigureDefinitionAttributesAreaFigure;
		/**
		 * @generated
		 */
		private ContainerShape fFigureDefinitionOthersAreaFigure;
		/**
		 * @generated
		 */
		private ContainerShape fFigureDefinitionContentAreaFigure;

		/**
		 * @generated
		 */
		public DefinitionFigure() {

			this.setBorder(createBorder0());
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents() {

			fFigureLabelDefinitionName = new AbstractComponentNameWrappingLabel();

			fFigureLabelDefinitionName.setMaximumSize(new Dimension(
					getMapMode().DPtoLP(50000000), getMapMode().DPtoLP(50)));

			this.add(fFigureLabelDefinitionName);

			fFigureDefinitionExtendsAreaFigure = new ContainerShape();

			this.add(fFigureDefinitionExtendsAreaFigure);

			fFigureDefinitionSubComponentsAreaFigure = new ContainerShape();

			this.add(fFigureDefinitionSubComponentsAreaFigure);

			fFigureDefinitionContentAreaFigure = new ContainerShape();
			
			this.add(fFigureDefinitionContentAreaFigure);

			fFigureDefinitionAttributesAreaFigure = new ContainerShape();

			this.add(fFigureDefinitionAttributesAreaFigure);

			fFigureDefinitionOthersAreaFigure = new ContainerShape();

			this.add(fFigureDefinitionOthersAreaFigure);

		}

		/**
		 * @generated
		 */
		private Border createBorder0() {
			ComponentBorder result = new ComponentBorder();

			return result;
		}

		/**
		 * @generated
		 */
		private boolean myUseLocalCoordinates = false;

		/**
		 * @generated
		 */
		protected boolean useLocalCoordinates() {
			return myUseLocalCoordinates;
		}

		/**
		 * @generated
		 */
		protected void setUseLocalCoordinates(boolean useLocalCoordinates) {
			myUseLocalCoordinates = useLocalCoordinates;
		}

		/**
		 * @generated
		 */
		public AbstractComponentShape getFigureDefinitionFigure() {
			return fFigureDefinitionFigure;
		}

		/**
		 * @generated
		 */
		public AbstractComponentNameWrappingLabel getFigureLabelDefinitionName() {
			return fFigureLabelDefinitionName;
		}

		/**
		 * @generated
		 */
		public ContainerShape getFigureDefinitionExtendsAreaFigure() {
			return fFigureDefinitionExtendsAreaFigure;
		}

		/**
		 * @generated
		 */
		public ContainerShape getFigureDefinitionSubComponentsAreaFigure() {
			return fFigureDefinitionSubComponentsAreaFigure;
		}

		/**
		 * @generated
		 */
		public ContainerShape getFigureDefinitionAttributesAreaFigure() {
			return fFigureDefinitionAttributesAreaFigure;
		}

		/**
		 * @generated
		 */
		public ContainerShape getFigureDefinitionOthersAreaFigure() {
			return fFigureDefinitionOthersAreaFigure;
		}

		/**
		 * @generated
		 */
		public ContainerShape getFigureDefinitionContentAreaFigure() {
			return fFigureDefinitionContentAreaFigure;
		}

	}

	public EditPolicy getPrimaryDragEditPolicy() {
		return new DefinitionResizableEditPolicy();
	}

	// Start Yann Hand Modif
	protected void handleNotificationEvent(Notification notification) {
		super.handleNotificationEvent(notification);
		Object feature = notification.getFeature();
		
		if(notification.getNotifier() instanceof Binding){
			((CanonicalEditPolicy) (this.getRoot().getContents())
					.getEditPolicy(EditPolicyRoles.CANONICAL_ROLE)).refresh();
			return;
		}
		
		// When merged bindings or binding are modified we have to update their graphical representation.
		if (notification.getNotifier() instanceof Definition
				&& feature instanceof EReference
				&&( (((EReference) feature).getFeatureID() == FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS) ||
				((EReference) feature).getFeatureID() == FractalPackage.ABSTRACT_COMPONENT__BINDINGS)) {
//			((CanonicalEditPolicy) (this.getRoot().getContents())
//					.getEditPolicy(EditPolicyRoles.CANONICAL_ROLE)).refresh();
//			if(notification.getEventType() == notification.ADD){
//				this.addListenerFilter(((EObject)notification.getNewValue()).toString(), this, (EObject)notification.getNewValue());
//			}else if(notification.getEventType() == notification.ADD_MANY){
//				EList<Binding> list = (EList<Binding>)notification.getNewValue();
//				for(int i=0; i<list.size(); i++){
//					this.addListenerFilter(((EObject)((EList<Binding>)notification.getNewValue()).get(i)).toString(), this, ((EList<Binding>)notification.getNewValue()).get(i));
//				}
//			}else if(notification.getEventType() == notification.REMOVE ){
//				this.removeListenerFilter(((EObject)notification.getNewValue()).toString());
//				((CanonicalEditPolicy) (this.getRoot().getContents())
//						.getEditPolicy(EditPolicyRoles.CANONICAL_ROLE)).refresh();
//			}else if(notification.getEventType() == notification.REMOVE_MANY){
//				EList<Binding> list = (EList<Binding>)notification.getNewValue();
//				for(int i=0; i<list.size(); i++){
//					this.removeListenerFilter(((EObject)((EList<Binding>)notification.getNewValue()).get(i)).toString());
//				}
//				((CanonicalEditPolicy) (this.getRoot().getContents())
//						.getEditPolicy(EditPolicyRoles.CANONICAL_ROLE)).refresh();
//			}
		}

		if (notification.getNewValue() instanceof Boolean) {
			collapse((Boolean) notification.getNewValue(), feature);
		}
		
	}

	
	protected Command getCommand(EditPart editPart, Boolean expand) {
		ChangePropertyValueRequest request = new ChangePropertyValueRequest(
				DiagramUIMessages.PropertyDescriptorFactory_CollapseCompartment,
				Properties.ID_COLLAPSED, expand);
		return editPart.getCommand(request);
	}

	public void collapse(boolean collapse, Object feature) {
		EditPart editPart = null;
		ContainerShape shape = null;

		if (org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE
				.getAbstractComponentStyle_CollapsedExtends().equals(feature)) {
			shape = getPrimaryShape().getFigureDefinitionExtendsAreaFigure();
		} else if (org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE
				.getAbstractComponentStyle_CollapsedSubComponents().equals(
						feature)) {
			shape = getPrimaryShape()
					.getFigureDefinitionSubComponentsAreaFigure();
		} else if (org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE
				.getAbstractComponentStyle_CollapsedContent().equals(feature)) {
			shape = getPrimaryShape().getFigureDefinitionContentAreaFigure();
		} else if (org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE
				.getAbstractComponentStyle_CollapsedAttributes()
				.equals(feature)) {
			shape = getPrimaryShape().getFigureDefinitionAttributesAreaFigure();
		} else if (org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE
				.getAbstractComponentStyle_CollapsedOthers().equals(feature)) {
			shape = getPrimaryShape().getFigureDefinitionOthersAreaFigure();
		}

		if (shape != null) {
			shape.setVisible(!collapse);
		}
	}
	
	protected void refreshChildren() {
		// TODO Auto-generated method stub
		super.refreshChildren();
		
		//the only way I found to correctly update bindings figure after Fractal model merge update
		((CanonicalEditPolicy) this.getRoot().getContents()
				.getEditPolicy(EditPolicyRoles.CANONICAL_ROLE)).refresh();
	}
	
	/*
	 * Fill the component with the the given color
	 * @see org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart#setBackgroundColor(org.eclipse.swt.graphics.Color)
	 */
	protected void setBackgroundColor(Color color) {
		getPrimaryShape().setBackgroundColor(color);
	}
	
	/*
	 * Fill the definition border with the given color
	 * @see org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart#setForegroundColor(org.eclipse.swt.graphics.Color)
	 */
	protected void setForegroundColor(Color color) {
		getPrimaryShape().setForegroundColor(color);
		((ComponentBorder)this.getPrimaryShape().getBorder()).setColor(color);
	}
	//End Yann Hand modif
}
