package org.ow2.fractal.f4e.fractal.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.AppearancePreferencePage;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramAppearancePreferencePage extends AppearancePreferencePage {

	/**
	 * @generated
	 */
	public DiagramAppearancePreferencePage() {
		setPreferenceStore(FractalDiagramEditorPlugin.getInstance()
				.getPreferenceStore());
	}
}
