package org.ow2.fractal.f4e.fractal.diagram.edit.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ListCompartmentEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CreationEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.figures.ResizableCompartmentFigure;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.CustomDragDropEditPolicy;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.FractalEditPolicies;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.ResizableCompartmentWithoutHandleEditPolicy;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.compartments.OthersCompartmentsEditPolicy;
import org.ow2.fractal.f4e.diagram.custom.figures.ResizableCompartmentWithoutScrollBarFigure;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.ComponentCompartmentComponentOthersCanonicalEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.ComponentCompartmentComponentOthersItemSemanticEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.part.Messages;

/**
 * @generated
 */
public class ComponentCompartmentComponentOthersEditPart extends
		ListCompartmentEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 5010;

	/**
	 * @generated
	 */
	public ComponentCompartmentComponentOthersEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected boolean hasModelChildrenChanged(Notification evt) {
		return false;
	}

	/**
	 * @generated
	 */
	public String getCompartmentName() {
		return Messages.ComponentCompartmentComponentOthersEditPart_title;
	}

	/**
	 * @generated
	 */
	public IFigure createFigure() {
		ResizableCompartmentWithoutScrollBarFigure result =  new ResizableCompartmentWithoutScrollBarFigure(getCompartmentName(), getMapMode());;
		
		ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
		result.getContentPane().setLayoutManager(layout);
		return result;
	}

	/**
	 * @generated NOT
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		// Custom policy which hide the handle of the colapsible compartment.
		installEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE,
				new ResizableCompartmentWithoutHandleEditPolicy());
		// End yann hand modifs.
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new ComponentCompartmentComponentOthersItemSemanticEditPolicy());
		installEditPolicy(EditPolicyRoles.CREATION_ROLE,
				new CreationEditPolicy());
		// Start yann hand modifs
		installEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE,
				new CustomDragDropEditPolicy(){
					// we can only move/d&d  content
					protected boolean canBeDropped(EditPart dropedObject) {
						return dropedObject instanceof VirtualNodeEditPart ||
						dropedObject instanceof ControllerEditPart;
					}
		});
		
		// Policy to handle menu bar actions to show/hide compartments
		installEditPolicy(FractalEditPolicies.OTHERS_COMPARTMENTS_ROLE,
				new OthersCompartmentsEditPolicy());
		
		// End yann hand modifs
		installEditPolicy(EditPolicyRoles.CANONICAL_ROLE,
				new ComponentCompartmentComponentOthersCanonicalEditPolicy());
	}

	/**
	 * @generated
	 */
	protected void setRatio(Double ratio) {
		if (getFigure().getParent().getLayoutManager() instanceof ConstrainedToolbarLayout) {
			super.setRatio(ratio);
		}
	}

}
