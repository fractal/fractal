package org.ow2.fractal.f4e.fractal.diagram.edit.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ListCompartmentEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CreationEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.DragDropEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.figures.ResizableCompartmentFigure;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.CustomDragDropEditPolicy;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.ResizableCompartmentWithoutHandleEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.DefinitionCompartmentDefinitionAttributesCanonicalEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.DefinitionCompartmentDefinitionAttributesItemSemanticEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.part.Messages;

/**
 * @generated NOT
 *  Extends ListCompartmentEditPart rather than ShapeCompartmentEditPart.
 */
public class DefinitionCompartmentDefinitionAttributesEditPart extends
		ListCompartmentEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 5004;

	/**
	 * @generated
	 */
	public DefinitionCompartmentDefinitionAttributesEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	public String getCompartmentName() {
		return Messages.DefinitionCompartmentDefinitionAttributesEditPart_title;
	}

	/**
	 * @generated
	 */
	public IFigure createFigure() {
		ResizableCompartmentFigure result = (ResizableCompartmentFigure) super
				.createFigure();
		result.setTitleVisibility(false);
		return result;
	}

	/**
	 * @generated NOT
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		// Custom policy which hide the handle of the colapsible compartment.
		installEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE,
				new ResizableCompartmentWithoutHandleEditPolicy());
		// End yann hand modifs.
		installEditPolicy(
				EditPolicyRoles.SEMANTIC_ROLE,
				new DefinitionCompartmentDefinitionAttributesItemSemanticEditPolicy());
		installEditPolicy(EditPolicyRoles.CREATION_ROLE,
				new CreationEditPolicy());
		// Start yann hand modifs
		installEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE,
				new CustomDragDropEditPolicy(){
					// we can only move/d&d attributes controller
					protected boolean canBeDropped(EditPart dropedObject) {
						return dropedObject instanceof AttributesControllerEditPart;
					}
		});
		// End yann hand modifs
		installEditPolicy(
				EditPolicyRoles.CANONICAL_ROLE,
				new DefinitionCompartmentDefinitionAttributesCanonicalEditPolicy());
	}

	/**
	 * @generated
	 */
	protected void setRatio(Double ratio) {
		if (getFigure().getParent().getLayoutManager() instanceof ConstrainedToolbarLayout) {
			super.setRatio(ratio);
		}
	}

	protected boolean hasModelChildrenChanged(Notification evt) {
		return true;
	}
}
