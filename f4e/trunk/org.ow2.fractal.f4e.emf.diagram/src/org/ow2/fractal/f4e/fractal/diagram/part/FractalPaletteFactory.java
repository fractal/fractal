package org.ow2.fractal.f4e.fractal.diagram.part;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteGroup;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeConnectionTool;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeCreationTool;
import org.ow2.fractal.f4e.fractal.diagram.providers.FractalElementTypes;

/**
 * @generated
 */
public class FractalPaletteFactory {

	/**
	 * @generated
	 */
	public void fillPalette(PaletteRoot paletteRoot) {
		paletteRoot.add(createFractal1Group());
	}

	/**
	 * Creates "fractal" palette tool group
	 * @generated
	 */
	private PaletteContainer createFractal1Group() {
		PaletteGroup paletteContainer = new PaletteGroup(
				Messages.Fractal1Group_title);
		paletteContainer.add(createComponent1CreationTool());
		paletteContainer.add(createInterface2CreationTool());
		paletteContainer.add(createBinding3CreationTool());
		paletteContainer.add(createAttributesController4CreationTool());
		paletteContainer.add(createAttribute5CreationTool());
		paletteContainer.add(createContent6CreationTool());
		paletteContainer.add(createVirtualNode7CreationTool());
		paletteContainer.add(createController8CreationTool());
		return paletteContainer;
	}

	/**
	 * @generated
	 */
	private ToolEntry createComponent1CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(FractalElementTypes.Component_2002);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Component1CreationTool_title,
				Messages.Component1CreationTool_desc, types);
		entry.setSmallIcon(FractalElementTypes
				.getImageDescriptor(FractalElementTypes.Component_2002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createInterface2CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(FractalElementTypes.Interface_2009);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Interface2CreationTool_title,
				Messages.Interface2CreationTool_desc, types);
		entry.setSmallIcon(FractalElementTypes
				.getImageDescriptor(FractalElementTypes.Interface_2009));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createBinding3CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(FractalElementTypes.Binding_3001);
		LinkToolEntry entry = new LinkToolEntry(
				Messages.Binding3CreationTool_title,
				Messages.Binding3CreationTool_desc, types);
		entry.setSmallIcon(FractalElementTypes
				.getImageDescriptor(FractalElementTypes.Binding_3001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAttributesController4CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(FractalElementTypes.AttributesController_2004);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AttributesController4CreationTool_title,
				Messages.AttributesController4CreationTool_desc, types);
		entry
				.setSmallIcon(FractalElementTypes
						.getImageDescriptor(FractalElementTypes.AttributesController_2004));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAttribute5CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(FractalElementTypes.Attribute_2005);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Attribute5CreationTool_title,
				Messages.Attribute5CreationTool_desc, types);
		entry.setSmallIcon(FractalElementTypes
				.getImageDescriptor(FractalElementTypes.Attribute_2005));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createContent6CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(FractalElementTypes.Content_2003);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Content6CreationTool_title,
				Messages.Content6CreationTool_desc, types);
		entry.setSmallIcon(FractalElementTypes
				.getImageDescriptor(FractalElementTypes.Content_2003));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createVirtualNode7CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(FractalElementTypes.VirtualNode_2008);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.VirtualNode7CreationTool_title,
				Messages.VirtualNode7CreationTool_desc, types);
		entry.setSmallIcon(FractalElementTypes
				.getImageDescriptor(FractalElementTypes.VirtualNode_2008));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createController8CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(FractalElementTypes.Controller_2007);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Controller8CreationTool_title,
				Messages.Controller8CreationTool_desc, types);
		entry.setSmallIcon(FractalElementTypes
				.getImageDescriptor(FractalElementTypes.Controller_2007));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private static class NodeToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List elementTypes;

		/**
		 * @generated
		 */
		private NodeToolEntry(String title, String description,
				List elementTypes) {
			super(title, description, null, null);
			this.elementTypes = elementTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeCreationTool(elementTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}

	/**
	 * @generated
	 */
	private static class LinkToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List relationshipTypes;

		/**
		 * @generated
		 */
		private LinkToolEntry(String title, String description,
				List relationshipTypes) {
			super(title, description, null, null);
			this.relationshipTypes = relationshipTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeConnectionTool(relationshipTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}
}
