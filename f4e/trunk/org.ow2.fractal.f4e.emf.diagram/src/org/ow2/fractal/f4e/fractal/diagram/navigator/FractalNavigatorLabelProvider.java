package org.ow2.fractal.f4e.fractal.diagram.navigator;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Attribute2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Attribute3EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributeEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributeName2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributeName3EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributeNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesController2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerSignature2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerSignatureEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Binding2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.BindingEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Component2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentName2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Content2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ContentClass2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ContentClassEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Controller2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ControllerEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ControllerType2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ControllerTypeEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCallEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Interface2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.InterfaceEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.InterfaceName2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.InterfaceNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.SystemEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.VirtualNode2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.VirtualNodeEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.VirtualNodeName2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.VirtualNodeNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.WrappingLabelEditPart;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalDiagramEditorPlugin;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalVisualIDRegistry;
import org.ow2.fractal.f4e.fractal.diagram.providers.FractalElementTypes;
import org.ow2.fractal.f4e.fractal.diagram.providers.FractalParserProvider;

/**
 * @generated
 */
public class FractalNavigatorLabelProvider extends LabelProvider implements
		ICommonLabelProvider, ITreePathLabelProvider {

	/**
	 * @generated
	 */
	static {
		FractalDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put(
						"Navigator?UnknownElement", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
		FractalDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put(
						"Navigator?ImageNotFound", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof FractalNavigatorItem
				&& !isOwnView(((FractalNavigatorItem) element).getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	 * @generated
	 */
	public Image getImage(Object element) {
		if (element instanceof FractalNavigatorGroup) {
			FractalNavigatorGroup group = (FractalNavigatorGroup) element;
			return FractalDiagramEditorPlugin.getInstance().getBundledImage(
					group.getIcon());
		}

		if (element instanceof FractalNavigatorItem) {
			FractalNavigatorItem navigatorItem = (FractalNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		return super.getImage(element);
	}

	/**
	 * @generated
	 */
	public Image getImage(View view) {
		switch (FractalVisualIDRegistry.getVisualID(view)) {
		case SystemEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Diagram?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?System", FractalElementTypes.System_79); //$NON-NLS-1$
		case DefinitionEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?Definition", FractalElementTypes.Definition_1001); //$NON-NLS-1$
		case DefinitionCallEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?DefinitionCall", FractalElementTypes.DefinitionCall_2001); //$NON-NLS-1$
		case ComponentEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?Component", FractalElementTypes.Component_2002); //$NON-NLS-1$
		case ContentEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?Content", FractalElementTypes.Content_2003); //$NON-NLS-1$
		case AttributesControllerEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?AttributesController", FractalElementTypes.AttributesController_2004); //$NON-NLS-1$
		case AttributeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?Attribute", FractalElementTypes.Attribute_2005); //$NON-NLS-1$
		case Attribute2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?Attribute", FractalElementTypes.Attribute_2006); //$NON-NLS-1$
		case ControllerEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?Controller", FractalElementTypes.Controller_2007); //$NON-NLS-1$
		case VirtualNodeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?VirtualNode", FractalElementTypes.VirtualNode_2008); //$NON-NLS-1$
		case InterfaceEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?Interface", FractalElementTypes.Interface_2009); //$NON-NLS-1$
		case Interface2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?Interface", FractalElementTypes.Interface_2010); //$NON-NLS-1$
		case Component2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?Component", FractalElementTypes.Component_2011); //$NON-NLS-1$
		case Content2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?Content", FractalElementTypes.Content_2012); //$NON-NLS-1$
		case AttributesController2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?AttributesController", FractalElementTypes.AttributesController_2013); //$NON-NLS-1$
		case Attribute3EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?Attribute", FractalElementTypes.Attribute_2014); //$NON-NLS-1$
		case Controller2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?Controller", FractalElementTypes.Controller_2015); //$NON-NLS-1$
		case VirtualNode2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?VirtualNode", FractalElementTypes.VirtualNode_2016); //$NON-NLS-1$
		case BindingEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?Binding", FractalElementTypes.Binding_3001); //$NON-NLS-1$
		case Binding2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://org.ow2.fractal/f4e/fractal.ecore/1.0.0?Binding", FractalElementTypes.Binding_3002); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = FractalDiagramEditorPlugin.getInstance()
				.getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null && elementType != null
				&& FractalElementTypes.isKnownElementType(elementType)) {
			image = FractalElementTypes.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public String getText(Object element) {
		if (element instanceof FractalNavigatorGroup) {
			FractalNavigatorGroup group = (FractalNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof FractalNavigatorItem) {
			FractalNavigatorItem navigatorItem = (FractalNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		return super.getText(element);
	}

	/**
	 * @generated
	 */
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (FractalVisualIDRegistry.getVisualID(view)) {
		case SystemEditPart.VISUAL_ID:
			return getSystem_79Text(view);
		case DefinitionEditPart.VISUAL_ID:
			return getDefinition_1001Text(view);
		case DefinitionCallEditPart.VISUAL_ID:
			return getDefinitionCall_2001Text(view);
		case ComponentEditPart.VISUAL_ID:
			return getComponent_2002Text(view);
		case ContentEditPart.VISUAL_ID:
			return getContent_2003Text(view);
		case AttributesControllerEditPart.VISUAL_ID:
			return getAttributesController_2004Text(view);
		case AttributeEditPart.VISUAL_ID:
			return getAttribute_2005Text(view);
		case Attribute2EditPart.VISUAL_ID:
			return getAttribute_2006Text(view);
		case ControllerEditPart.VISUAL_ID:
			return getController_2007Text(view);
		case VirtualNodeEditPart.VISUAL_ID:
			return getVirtualNode_2008Text(view);
		case InterfaceEditPart.VISUAL_ID:
			return getInterface_2009Text(view);
		case Interface2EditPart.VISUAL_ID:
			return getInterface_2010Text(view);
		case Component2EditPart.VISUAL_ID:
			return getComponent_2011Text(view);
		case Content2EditPart.VISUAL_ID:
			return getContent_2012Text(view);
		case AttributesController2EditPart.VISUAL_ID:
			return getAttributesController_2013Text(view);
		case Attribute3EditPart.VISUAL_ID:
			return getAttribute_2014Text(view);
		case Controller2EditPart.VISUAL_ID:
			return getController_2015Text(view);
		case VirtualNode2EditPart.VISUAL_ID:
			return getVirtualNode_2016Text(view);
		case BindingEditPart.VISUAL_ID:
			return getBinding_3001Text(view);
		case Binding2EditPart.VISUAL_ID:
			return getBinding_3002Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	 * @generated
	 */
	private String getSystem_79Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getDefinition_1001Text(View view) {
		IAdaptable hintAdapter = new FractalParserProvider.HintAdapter(
				FractalElementTypes.Definition_1001,
				(view.getElement() != null ? view.getElement() : view),
				FractalVisualIDRegistry
						.getType(DefinitionNameEditPart.VISUAL_ID));
		IParser parser = ParserService.getInstance().getParser(hintAdapter);

		if (parser != null) {
			return parser.getPrintString(hintAdapter, ParserOptions.NONE
					.intValue());
		} else {
			FractalDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 4017); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}

	}

	/**
	 * @generated
	 */
	private String getDefinitionCall_2001Text(View view) {

		IAdaptable hintAdapter = new FractalParserProvider.HintAdapter(
				FractalElementTypes.DefinitionCall_2001,
				(view.getElement() != null ? view.getElement() : view),
				FractalVisualIDRegistry
						.getType(WrappingLabelEditPart.VISUAL_ID));
		IParser parser = ParserService.getInstance().getParser(hintAdapter);
		if (parser != null) {
			return parser.getPrintString(hintAdapter, ParserOptions.NONE
					.intValue());
		} else {
			return ""; //$NON-NLS-1$
		}

	}

	/**
	 * @generated
	 */
	private String getComponent_2002Text(View view) {
		IAdaptable hintAdapter = new FractalParserProvider.HintAdapter(
				FractalElementTypes.Component_2002,
				(view.getElement() != null ? view.getElement() : view),
				FractalVisualIDRegistry
						.getType(ComponentNameEditPart.VISUAL_ID));
		IParser parser = ParserService.getInstance().getParser(hintAdapter);

		if (parser != null) {
			return parser.getPrintString(hintAdapter, ParserOptions.NONE
					.intValue());
		} else {
			FractalDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 4016); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}

	}

	/**
	 * @generated
	 */
	private String getContent_2003Text(View view) {
		IAdaptable hintAdapter = new FractalParserProvider.HintAdapter(
				FractalElementTypes.Content_2003,
				(view.getElement() != null ? view.getElement() : view),
				FractalVisualIDRegistry.getType(ContentClassEditPart.VISUAL_ID));
		IParser parser = ParserService.getInstance().getParser(hintAdapter);

		if (parser != null) {
			return parser.getPrintString(hintAdapter, ParserOptions.NONE
					.intValue());
		} else {
			FractalDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 4002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}

	}

	/**
	 * @generated
	 */
	private String getAttributesController_2004Text(View view) {
		IAdaptable hintAdapter = new FractalParserProvider.HintAdapter(
				FractalElementTypes.AttributesController_2004,
				(view.getElement() != null ? view.getElement() : view),
				FractalVisualIDRegistry
						.getType(AttributesControllerSignatureEditPart.VISUAL_ID));
		IParser parser = ParserService.getInstance().getParser(hintAdapter);

		if (parser != null) {
			return parser.getPrintString(hintAdapter, ParserOptions.NONE
					.intValue());
		} else {
			FractalDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 4005); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}

	}

	/**
	 * @generated
	 */
	private String getAttribute_2005Text(View view) {
		IAdaptable hintAdapter = new FractalParserProvider.HintAdapter(
				FractalElementTypes.Attribute_2005,
				(view.getElement() != null ? view.getElement() : view),
				FractalVisualIDRegistry
						.getType(AttributeNameEditPart.VISUAL_ID));
		IParser parser = ParserService.getInstance().getParser(hintAdapter);

		if (parser != null) {
			return parser.getPrintString(hintAdapter, ParserOptions.NONE
					.intValue());
		} else {
			FractalDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 4003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}

	}

	/**
	 * @generated
	 */
	private String getAttribute_2006Text(View view) {
		IAdaptable hintAdapter = new FractalParserProvider.HintAdapter(
				FractalElementTypes.Attribute_2006,
				(view.getElement() != null ? view.getElement() : view),
				FractalVisualIDRegistry
						.getType(AttributeName2EditPart.VISUAL_ID));
		IParser parser = ParserService.getInstance().getParser(hintAdapter);

		if (parser != null) {
			return parser.getPrintString(hintAdapter, ParserOptions.NONE
					.intValue());
		} else {
			FractalDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 4004); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}

	}

	/**
	 * @generated
	 */
	private String getController_2007Text(View view) {
		IAdaptable hintAdapter = new FractalParserProvider.HintAdapter(
				FractalElementTypes.Controller_2007,
				(view.getElement() != null ? view.getElement() : view),
				FractalVisualIDRegistry
						.getType(ControllerTypeEditPart.VISUAL_ID));
		IParser parser = ParserService.getInstance().getParser(hintAdapter);

		if (parser != null) {
			return parser.getPrintString(hintAdapter, ParserOptions.NONE
					.intValue());
		} else {
			FractalDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 4006); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}

	}

	/**
	 * @generated
	 */
	private String getVirtualNode_2008Text(View view) {
		IAdaptable hintAdapter = new FractalParserProvider.HintAdapter(
				FractalElementTypes.VirtualNode_2008,
				(view.getElement() != null ? view.getElement() : view),
				FractalVisualIDRegistry
						.getType(VirtualNodeNameEditPart.VISUAL_ID));
		IParser parser = ParserService.getInstance().getParser(hintAdapter);

		if (parser != null) {
			return parser.getPrintString(hintAdapter, ParserOptions.NONE
					.intValue());
		} else {
			FractalDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 4007); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}

	}

	/**
	 * @generated
	 */
	private String getInterface_2009Text(View view) {
		IAdaptable hintAdapter = new FractalParserProvider.HintAdapter(
				FractalElementTypes.Interface_2009,
				(view.getElement() != null ? view.getElement() : view),
				FractalVisualIDRegistry
						.getType(InterfaceNameEditPart.VISUAL_ID));
		IParser parser = ParserService.getInstance().getParser(hintAdapter);

		if (parser != null) {
			return parser.getPrintString(hintAdapter, ParserOptions.NONE
					.intValue());
		} else {
			FractalDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 4008); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}

	}

	/**
	 * @generated
	 */
	private String getInterface_2010Text(View view) {
		IAdaptable hintAdapter = new FractalParserProvider.HintAdapter(
				FractalElementTypes.Interface_2010,
				(view.getElement() != null ? view.getElement() : view),
				FractalVisualIDRegistry
						.getType(InterfaceName2EditPart.VISUAL_ID));
		IParser parser = ParserService.getInstance().getParser(hintAdapter);

		if (parser != null) {
			return parser.getPrintString(hintAdapter, ParserOptions.NONE
					.intValue());
		} else {
			FractalDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 4009); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}

	}

	/**
	 * @generated
	 */
	private String getComponent_2011Text(View view) {
		IAdaptable hintAdapter = new FractalParserProvider.HintAdapter(
				FractalElementTypes.Component_2011,
				(view.getElement() != null ? view.getElement() : view),
				FractalVisualIDRegistry
						.getType(ComponentName2EditPart.VISUAL_ID));
		IParser parser = ParserService.getInstance().getParser(hintAdapter);

		if (parser != null) {
			return parser.getPrintString(hintAdapter, ParserOptions.NONE
					.intValue());
		} else {
			FractalDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 4015); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}

	}

	/**
	 * @generated
	 */
	private String getContent_2012Text(View view) {
		IAdaptable hintAdapter = new FractalParserProvider.HintAdapter(
				FractalElementTypes.Content_2012,
				(view.getElement() != null ? view.getElement() : view),
				FractalVisualIDRegistry
						.getType(ContentClass2EditPart.VISUAL_ID));
		IParser parser = ParserService.getInstance().getParser(hintAdapter);

		if (parser != null) {
			return parser.getPrintString(hintAdapter, ParserOptions.NONE
					.intValue());
		} else {
			FractalDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 4010); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}

	}

	/**
	 * @generated
	 */
	private String getAttributesController_2013Text(View view) {
		IAdaptable hintAdapter = new FractalParserProvider.HintAdapter(
				FractalElementTypes.AttributesController_2013,
				(view.getElement() != null ? view.getElement() : view),
				FractalVisualIDRegistry
						.getType(AttributesControllerSignature2EditPart.VISUAL_ID));
		IParser parser = ParserService.getInstance().getParser(hintAdapter);

		if (parser != null) {
			return parser.getPrintString(hintAdapter, ParserOptions.NONE
					.intValue());
		} else {
			FractalDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 4012); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}

	}

	/**
	 * @generated
	 */
	private String getAttribute_2014Text(View view) {
		IAdaptable hintAdapter = new FractalParserProvider.HintAdapter(
				FractalElementTypes.Attribute_2014,
				(view.getElement() != null ? view.getElement() : view),
				FractalVisualIDRegistry
						.getType(AttributeName3EditPart.VISUAL_ID));
		IParser parser = ParserService.getInstance().getParser(hintAdapter);

		if (parser != null) {
			return parser.getPrintString(hintAdapter, ParserOptions.NONE
					.intValue());
		} else {
			FractalDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 4011); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}

	}

	/**
	 * @generated
	 */
	private String getController_2015Text(View view) {
		IAdaptable hintAdapter = new FractalParserProvider.HintAdapter(
				FractalElementTypes.Controller_2015,
				(view.getElement() != null ? view.getElement() : view),
				FractalVisualIDRegistry
						.getType(ControllerType2EditPart.VISUAL_ID));
		IParser parser = ParserService.getInstance().getParser(hintAdapter);

		if (parser != null) {
			return parser.getPrintString(hintAdapter, ParserOptions.NONE
					.intValue());
		} else {
			FractalDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 4013); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}

	}

	/**
	 * @generated
	 */
	private String getVirtualNode_2016Text(View view) {
		IAdaptable hintAdapter = new FractalParserProvider.HintAdapter(
				FractalElementTypes.VirtualNode_2016,
				(view.getElement() != null ? view.getElement() : view),
				FractalVisualIDRegistry
						.getType(VirtualNodeName2EditPart.VISUAL_ID));
		IParser parser = ParserService.getInstance().getParser(hintAdapter);

		if (parser != null) {
			return parser.getPrintString(hintAdapter, ParserOptions.NONE
					.intValue());
		} else {
			FractalDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 4014); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}

	}

	/**
	 * @generated
	 */
	private String getBinding_3001Text(View view) {
		Binding domainModelElement = (Binding) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getClient();
		} else {
			FractalDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 3001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getBinding_3002Text(View view) {
		Binding domainModelElement = (Binding) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getClient();
		} else {
			FractalDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 3002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return SystemEditPart.MODEL_ID.equals(FractalVisualIDRegistry
				.getModelID(view));
	}

}
