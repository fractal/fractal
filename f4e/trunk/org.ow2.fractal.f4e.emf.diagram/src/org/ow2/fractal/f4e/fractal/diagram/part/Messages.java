package org.ow2.fractal.f4e.fractal.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	 * @generated
	 */
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages() {
	}

	/**
	 * @generated
	 */
	public static String FractalCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String FractalCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String FractalCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String FractalCreationWizard_DomainModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String FractalCreationWizard_DomainModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String FractalCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String FractalCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String FractalCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String FractalDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String FractalDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String FractalDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String FractalDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String FractalDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String FractalDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String FractalDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String FractalDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String FractalDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String FractalDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String FractalDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String FractalDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String FractalDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String FractalInitDiagramFileAction_InitDiagramFileResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String FractalInitDiagramFileAction_InitDiagramFileResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String FractalInitDiagramFileAction_InitDiagramFileWizardTitle;

	/**
	 * @generated
	 */
	public static String FractalInitDiagramFileAction_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String FractalNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String FractalNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String FractalNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String FractalNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String FractalNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String FractalNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String FractalNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String FractalNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String FractalNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String FractalNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String FractalNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String FractalDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String FractalDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String FractalDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String FractalDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String FractalDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String FractalElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String Fractal1Group_title;

	/**
	 * @generated
	 */
	public static String Component1CreationTool_title;

	/**
	 * @generated
	 */
	public static String Component1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Interface2CreationTool_title;

	/**
	 * @generated
	 */
	public static String Interface2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Binding3CreationTool_title;

	/**
	 * @generated
	 */
	public static String Binding3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AttributesController4CreationTool_title;

	/**
	 * @generated
	 */
	public static String AttributesController4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Attribute5CreationTool_title;

	/**
	 * @generated
	 */
	public static String Attribute5CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Content6CreationTool_title;

	/**
	 * @generated
	 */
	public static String Content6CreationTool_desc;

	/**
	 * @generated
	 */
	public static String VirtualNode7CreationTool_title;

	/**
	 * @generated
	 */
	public static String VirtualNode7CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Controller8CreationTool_title;

	/**
	 * @generated
	 */
	public static String Controller8CreationTool_desc;

	/**
	 * @generated
	 */
	public static String DefinitionCompartmentDefinitionExtendsEditPart_title;

	/**
	 * @generated
	 */
	public static String DefinitionCompartmentDefinitionSubComponentsEditPart_title;

	/**
	 * @generated
	 */
	public static String DefinitionCompartmentDefinitionContentEditPart_title;

	/**
	 * @generated
	 */
	public static String DefinitionCompartmentDefinitionAttributesEditPart_title;

	/**
	 * @generated
	 */
	public static String DefinitionCompartmentDefinitionOthersEditPart_title;

	/**
	 * @generated
	 */
	public static String ComponentCompartmentComponentExtendsEditPart_title;

	/**
	 * @generated
	 */
	public static String ComponentCompartmentComponentSubComponentsEditPart_title;

	/**
	 * @generated
	 */
	public static String ComponentCompartmentComponentContentEditPart_title;

	/**
	 * @generated
	 */
	public static String ComponentCompartmentComponentAttributesEditPart_title;

	/**
	 * @generated
	 */
	public static String ComponentCompartmentComponentOthersEditPart_title;

	/**
	 * @generated
	 */
	public static String AttributesControllerCompartmentAttributesControllerEditPart_title;

	/**
	 * @generated
	 */
	public static String ComponentMergedCompartmentComponentSubComponentsEditPart_title;

	/**
	 * @generated
	 */
	public static String ComponentMergedCompartmentComponentContentEditPart_title;

	/**
	 * @generated
	 */
	public static String ComponentMergedCompartmentComponentAttributesEditPart_title;

	/**
	 * @generated
	 */
	public static String ComponentMergedCompartmentComponentOthersEditPart_title;

	/**
	 * @generated
	 */
	public static String AttributesControllerMergedCompartmentAttributesControllerEditPart_title;

	/**
	 * @generated
	 */
	public static String CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_System_79_links;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Interface_2009_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Interface_2009_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Interface_2010_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Interface_2010_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Binding_3001_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Binding_3001_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Binding_3002_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Binding_3002_source;

	/**
	 * @generated
	 */
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnexpectedValueTypeMessage;

	/**
	 * @generated
	 */
	public static String AbstractParser_WrongStringConversionMessage;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnknownLiteralMessage;

	/**
	 * @generated
	 */
	public static String MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String FractalModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String FractalModelingAssistantProviderMessage;

	//TODO: put accessor fields manually	
}
