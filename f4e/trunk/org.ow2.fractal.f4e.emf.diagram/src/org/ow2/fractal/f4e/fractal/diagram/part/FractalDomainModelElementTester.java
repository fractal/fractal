package org.ow2.fractal.f4e.fractal.diagram.part;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.ow2.fractal.f4e.fractal.FractalPackage;

/**
 * @generated
 */
public class FractalDomainModelElementTester extends PropertyTester {

	/**
	 * @generated
	 */
	public boolean test(Object receiver, String method, Object[] args,
			Object expectedValue) {
		if (false == receiver instanceof EObject) {
			return false;
		}
		EObject eObject = (EObject) receiver;
		EClass eClass = eObject.eClass();
		if (eClass == FractalPackage.eINSTANCE.getSystem()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getAbstractComponent()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getParameter()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getValue()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getValueElement()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getConstant()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getReference()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getFormalParameter()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getDefinition()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getDefinitionCall()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getEffectiveParameter()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getInterface()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getComponent()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getContent()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getAttributesController()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getAttribute()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getBinding()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getController()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getTemplateController()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getComment()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getCoordinates()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getLogger()) {
			return true;
		}
		if (eClass == FractalPackage.eINSTANCE.getVirtualNode()) {
			return true;
		}
		return false;
	}

}
