package org.ow2.fractal.f4e.fractal.diagram.edit.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ListCompartmentEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CreationEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.DragDropEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.figures.ResizableCompartmentFigure;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.ResizableCompartmentWithoutHandleEditPolicy;
import org.ow2.fractal.f4e.diagram.custom.figures.ResizableCompartmentWithoutScrollBarFigure;
import org.ow2.fractal.f4e.diagram.custom.layouts.AutosizeContainerStackLayout;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.ComponentCompartmentComponentExtendsCanonicalEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.ComponentCompartmentComponentExtendsItemSemanticEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.part.Messages;

/**
 * @generated
 */
public class ComponentCompartmentComponentExtendsEditPart extends
		ListCompartmentEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 5006;

	/**
	 * @generated
	 */
	public ComponentCompartmentComponentExtendsEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected boolean hasModelChildrenChanged(Notification evt) {
		return false;
	}

	/**
	 * @generated
	 */
	public String getCompartmentName() {
		return Messages.ComponentCompartmentComponentExtendsEditPart_title;
	}

	/**
	 * @generated
	 */
	public IFigure createFigure() {
		ResizableCompartmentFigure result = new ResizableCompartmentWithoutScrollBarFigure(getCompartmentName(), getMapMode());
		AutosizeContainerStackLayout layout = new AutosizeContainerStackLayout();
		
		result.getContentPane().setLayoutManager(layout);
		return result;
	}

	/**
	 * @generated NOT
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		// Custom policy which hide the handle of the colapsible compartment.
		installEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE,
				new ResizableCompartmentWithoutHandleEditPolicy(){
					public boolean isDragAllowed() {
						// TODO Auto-generated method stub
						return false;
					}
		});
		// End yann hand modifs.
		installEditPolicy(
				EditPolicyRoles.SEMANTIC_ROLE,
				new ComponentCompartmentComponentExtendsItemSemanticEditPolicy());
		installEditPolicy(EditPolicyRoles.CREATION_ROLE,
				new CreationEditPolicy());
		installEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE,
				new DragDropEditPolicy());
		installEditPolicy(EditPolicyRoles.CANONICAL_ROLE,
				new ComponentCompartmentComponentExtendsCanonicalEditPolicy());
	}

	/**
	 * @generated
	 */
	protected void setRatio(Double ratio) {
		if (getFigure().getParent().getLayoutManager() instanceof ConstrainedToolbarLayout) {
			super.setRatio(ratio);
		}
	}

}
