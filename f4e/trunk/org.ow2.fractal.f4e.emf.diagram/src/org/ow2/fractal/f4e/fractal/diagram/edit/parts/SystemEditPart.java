package org.ow2.fractal.f4e.fractal.diagram.edit.parts;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gef.Request;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.SystemCanonicalEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.SystemItemSemanticEditPolicy;
import org.ow2.fractal.f4e.fractal.notification.IFractalNotification;

/**
 * @generated
 */
public class SystemEditPart extends DiagramEditPart {

	/**
	 * @generated
	 */
	public final static String MODEL_ID = "Fractal"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 79;

	/**
	 * @generated
	 */
	public SystemEditPart(View view) {
		super(view);
		view.getElement().eAdapters().add(new BindingListener());
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new SystemItemSemanticEditPolicy());
		installEditPolicy(EditPolicyRoles.CANONICAL_ROLE,
				new SystemCanonicalEditPolicy());
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.POPUPBAR_ROLE);
	}

	protected void handleNotificationEvent(Notification event) {
		// TODO Auto-generated method stub
		super.handleNotificationEvent(event);
	}

	/**
	 * Listener to update the graphical bindings when bindings in the model are modified.
	 * 
	 * @author Yann Davin
	 *
	 */
	class BindingListener extends EContentAdapter {

		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);
			
			// For unknown reasons SystemEditPart.this.getRoot() can be null,
			// So we prenvent this case.
			if(SystemEditPart.this.getRoot() != null){
				if(notification.getEventType() == IFractalNotification.UPDATE_MERGED_ELEMENTS){
					((CanonicalEditPolicy) SystemEditPart.this.getRoot().getContents()
							.getEditPolicy(EditPolicyRoles.CANONICAL_ROLE)).refresh();
				}

				if(notification.getNotifier() instanceof AbstractComponent &&
						notification.getFeature() instanceof EReference &&
						(((EReference)notification.getFeature()).getFeatureID() == FractalPackage.ABSTRACT_COMPONENT__BINDINGS ||
								((EReference)notification.getFeature()).getFeatureID() == FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS	)
								&& (notification.getEventType() == Notification.REMOVE || notification.getEventType() == Notification.REMOVE_MANY ||
										notification.getEventType() == Notification.ADD || notification.getEventType() == Notification.ADD_MANY)){
					((CanonicalEditPolicy) SystemEditPart.this.getRoot().getContents()
							.getEditPolicy(EditPolicyRoles.CANONICAL_ROLE)).refresh();
				}

				if(notification.getNotifier() instanceof Binding && notification.getFeature() instanceof EReference &&
						(((EReference)notification.getFeature()).getFeatureID() == FractalPackage.BINDING__CLIENT_INTERFACE ||
								((EReference)notification.getFeature()).getFeatureID() == FractalPackage.BINDING__SERVER_INTERFACE	)){
					((CanonicalEditPolicy) SystemEditPart.this.getRoot().getContents()
							.getEditPolicy(EditPolicyRoles.CANONICAL_ROLE)).refresh();
				}
			}
		}
	}
}
