package org.ow2.fractal.f4e.fractal.diagram.edit.policies;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Controller2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.VirtualNode2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalDiagramUpdater;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalNodeDescriptor;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalVisualIDRegistry;

/**
 * @generated
 */
public class ComponentMergedCompartmentComponentOthersCanonicalEditPolicy
		extends CanonicalEditPolicy {

	/**
	 * @generated
	 */
	Set myFeaturesToSynchronize;

	/**
	 * @generated
	 */
	protected List getSemanticChildrenList() {
		View viewObject = (View) getHost().getModel();
		List result = new LinkedList();
		for (Iterator it = FractalDiagramUpdater
				.getComponentMergedCompartmentComponentOthers_5015SemanticChildren(
						viewObject).iterator(); it.hasNext();) {
			result.add(((FractalNodeDescriptor) it.next()).getModelElement());
		}
		return result;
	}

	/**
	 * @generated
	 */
	protected boolean isOrphaned(Collection semanticChildren, final View view) {
		int visualID = FractalVisualIDRegistry.getVisualID(view);
		switch (visualID) {
		case Controller2EditPart.VISUAL_ID:
		case VirtualNode2EditPart.VISUAL_ID:
			if (!semanticChildren.contains(view.getElement())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected String getDefaultFactoryHint() {
		return null;
	}

	/**
	 * @generated
	 */
	protected Set getFeaturesToSynchronize() {
		if (myFeaturesToSynchronize == null) {
			myFeaturesToSynchronize = new HashSet();
			myFeaturesToSynchronize.add(FractalPackage.eINSTANCE
					.getAbstractComponent_MergedController());
			myFeaturesToSynchronize.add(FractalPackage.eINSTANCE
					.getAbstractComponent_MergedVirtualNode());
		}
		return myFeaturesToSynchronize;
	}

}
