package org.ow2.fractal.f4e.fractal.diagram.part;

import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramActionBarContributor;

/**
 * @generated
 */
public class FractalDiagramActionBarContributor extends
		DiagramActionBarContributor {

	/**
	 * @generated
	 */
	protected Class getEditorClass() {
		return FractalDiagramEditor.class;
	}

	/**
	 * @generated
	 */
	protected String getEditorId() {
		return FractalDiagramEditor.ID;
	}
}
