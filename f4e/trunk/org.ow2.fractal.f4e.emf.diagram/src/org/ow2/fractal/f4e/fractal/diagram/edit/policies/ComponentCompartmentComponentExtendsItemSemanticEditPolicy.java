package org.ow2.fractal.f4e.fractal.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyRequest;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.diagram.edit.commands.DefinitionCallCreateCommand;
import org.ow2.fractal.f4e.fractal.diagram.providers.FractalElementTypes;

/**
 * @generated
 */
public class ComponentCompartmentComponentExtendsItemSemanticEditPolicy extends
		FractalBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (FractalElementTypes.DefinitionCall_2001 == req.getElementType()) {
			if (req.getContainmentFeature() == null) {
				req.setContainmentFeature(FractalPackage.eINSTANCE
						.getAbstractComponent_ExtendsAST());
			}
			return getGEFWrapper(new DefinitionCallCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	// Start yann hand modifs
	protected boolean shouldProceed(DestroyRequest destroyRequest) {
		// TODO Auto-generated method stub
		return false;
	}
	// End yann hand modifs
}
