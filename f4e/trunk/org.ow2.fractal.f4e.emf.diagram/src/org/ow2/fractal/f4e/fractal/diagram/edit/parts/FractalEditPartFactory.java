package org.ow2.fractal.f4e.fractal.diagram.edit.parts;

import org.eclipse.draw2d.DelegatingLayout;
import org.eclipse.draw2d.FigureUtilities;
import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.Layer;
import org.eclipse.draw2d.LayeredPane;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramRootEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.diagram.ui.render.editparts.RenderedDiagramRootEditPart;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.draw2d.ui.render.internal.graphics.RenderedMapModeGraphics;
import org.eclipse.gmf.runtime.draw2d.ui.render.internal.graphics.RenderedScaledGraphics;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.ow2.fractal.f4e.diagram.custom.router.ConnectionLayerExEx;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalVisualIDRegistry;

/**
 * @generated
 */
public class FractalEditPartFactory implements EditPartFactory {
	
	/**
	 * @generated NOT
	 */
	// [SDR]
	public static final String EXTERNAL_NODE_LABELS_LAYER = "External Node Labels"; //$NON-NLS-1$

	// [/SDR]

	/**
	 * @generated NOT Makes sure the custom ConnectionLayerExEx is set so tha
	 *            the custom connection routers can be used.
	 * 
	 * @param root
	 *            The root editpart on which the printable layers are registered
	 */
	// [SDR]
	public static String LABEL_LAYER="Labels Layer";
	
	public static void setupConnectionLayerExEx(DiagramRootEditPart root) {
		LayeredPane printableLayers = (LayeredPane) root
				.getLayer(LayerConstants.PRINTABLE_LAYERS);
		Layer connlayer = printableLayers
				.getLayer(LayerConstants.CONNECTION_LAYER);
		if (connlayer == null || connlayer instanceof ConnectionLayerExEx) {
			return;
		}
		
		printableLayers.removeLayer(LayerConstants.CONNECTION_LAYER);
		// printableLayers.addLayerAfter(new ConnectionLayerExEx(),
		// LayerConstants.CONNECTION_LAYER,
		// printableLayers.getLayer(DiagramRootEditPart.DECORATION_PRINTABLE_LAYER));
		// for some reason the code above does not replace the connection
		// layer in the same place where it was.
		// the heavy code below works.
		Layer decorationLayer = printableLayers
				.getLayer(DiagramRootEditPart.DECORATION_PRINTABLE_LAYER);
		printableLayers
				.removeLayer(DiagramRootEditPart.DECORATION_PRINTABLE_LAYER);
		printableLayers.addLayerBefore(new ConnectionLayerExEx(),
				LayerConstants.CONNECTION_LAYER, printableLayers
						.getLayer(LayerConstants.PRIMARY_LAYER));
		printableLayers.addLayerBefore(decorationLayer,
				DiagramRootEditPart.DECORATION_PRINTABLE_LAYER, printableLayers
						.getLayer(LayerConstants.CONNECTION_LAYER));

		FreeformLayer extLabelsLayer = new FreeformLayer();
		extLabelsLayer.setLayoutManager(new DelegatingLayout());
		printableLayers.addLayerAfter(extLabelsLayer,
				FractalEditPartFactory.EXTERNAL_NODE_LABELS_LAYER,
				LayerConstants.PRIMARY_LAYER);
		LayeredPane scalableLayers = (LayeredPane) root
				.getLayer(LayerConstants.SCALABLE_LAYERS);
		FreeformLayer scaledFeedbackLayer = new FreeformLayer();
		scaledFeedbackLayer.setEnabled(false);
		scalableLayers.addLayerAfter(scaledFeedbackLayer,
				LayerConstants.SCALED_FEEDBACK_LAYER,
				DiagramRootEditPart.DECORATION_UNPRINTABLE_LAYER);
	}
	
	/**
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			switch (FractalVisualIDRegistry.getVisualID(view)) {

			case SystemEditPart.VISUAL_ID:
				return new SystemEditPart(view);

			case DefinitionEditPart.VISUAL_ID:
				return new DefinitionEditPart(view);

			case DefinitionNameEditPart.VISUAL_ID:
				return new DefinitionNameEditPart(view);

			case DefinitionCallEditPart.VISUAL_ID:
				return new DefinitionCallEditPart(view);

			case WrappingLabelEditPart.VISUAL_ID:
				return new WrappingLabelEditPart(view);

			case ComponentEditPart.VISUAL_ID:
				return new ComponentEditPart(view);

			case ComponentNameEditPart.VISUAL_ID:
				return new ComponentNameEditPart(view);

			case ContentEditPart.VISUAL_ID:
				return new ContentEditPart(view);

			case ContentClassEditPart.VISUAL_ID:
				return new ContentClassEditPart(view);

			case AttributesControllerEditPart.VISUAL_ID:
				return new AttributesControllerEditPart(view);

			case AttributesControllerSignatureEditPart.VISUAL_ID:
				return new AttributesControllerSignatureEditPart(view);

			case AttributeEditPart.VISUAL_ID:
				return new AttributeEditPart(view);

			case AttributeNameEditPart.VISUAL_ID:
				return new AttributeNameEditPart(view);

			case Attribute2EditPart.VISUAL_ID:
				return new Attribute2EditPart(view);

			case AttributeName2EditPart.VISUAL_ID:
				return new AttributeName2EditPart(view);

			case ControllerEditPart.VISUAL_ID:
				return new ControllerEditPart(view);

			case ControllerTypeEditPart.VISUAL_ID:
				return new ControllerTypeEditPart(view);

			case VirtualNodeEditPart.VISUAL_ID:
				return new VirtualNodeEditPart(view);

			case VirtualNodeNameEditPart.VISUAL_ID:
				return new VirtualNodeNameEditPart(view);

			case InterfaceEditPart.VISUAL_ID:
				return new org.ow2.fractal.f4e.diagram.custom.edit.parts.InterfaceEditPart(view);

			case InterfaceNameEditPart.VISUAL_ID:
				return new InterfaceNameEditPart(view);

			case Interface2EditPart.VISUAL_ID:
				return new org.ow2.fractal.f4e.diagram.custom.edit.parts.MergedInterfaceEditPart(view);

			case InterfaceName2EditPart.VISUAL_ID:
				return new InterfaceName2EditPart(view);

			case Component2EditPart.VISUAL_ID:
				return new Component2EditPart(view);

			case ComponentName2EditPart.VISUAL_ID:
				return new ComponentName2EditPart(view);

			case Content2EditPart.VISUAL_ID:
				return new Content2EditPart(view);

			case ContentClass2EditPart.VISUAL_ID:
				return new ContentClass2EditPart(view);

			case AttributesController2EditPart.VISUAL_ID:
				return new AttributesController2EditPart(view);

			case AttributesControllerSignature2EditPart.VISUAL_ID:
				return new AttributesControllerSignature2EditPart(view);

			case Attribute3EditPart.VISUAL_ID:
				return new Attribute3EditPart(view);

			case AttributeName3EditPart.VISUAL_ID:
				return new AttributeName3EditPart(view);

			case Controller2EditPart.VISUAL_ID:
				return new Controller2EditPart(view);

			case ControllerType2EditPart.VISUAL_ID:
				return new ControllerType2EditPart(view);

			case VirtualNode2EditPart.VISUAL_ID:
				return new VirtualNode2EditPart(view);

			case VirtualNodeName2EditPart.VISUAL_ID:
				return new VirtualNodeName2EditPart(view);

			case DefinitionCompartmentDefinitionExtendsEditPart.VISUAL_ID:
				return new DefinitionCompartmentDefinitionExtendsEditPart(view);

			case DefinitionCompartmentDefinitionSubComponentsEditPart.VISUAL_ID:
				return new DefinitionCompartmentDefinitionSubComponentsEditPart(
						view);

			case DefinitionCompartmentDefinitionContentEditPart.VISUAL_ID:
				return new DefinitionCompartmentDefinitionContentEditPart(view);

			case DefinitionCompartmentDefinitionAttributesEditPart.VISUAL_ID:
				return new DefinitionCompartmentDefinitionAttributesEditPart(
						view);

			case DefinitionCompartmentDefinitionOthersEditPart.VISUAL_ID:
				return new DefinitionCompartmentDefinitionOthersEditPart(view);

			case ComponentCompartmentComponentExtendsEditPart.VISUAL_ID:
				return new ComponentCompartmentComponentExtendsEditPart(view);

			case ComponentCompartmentComponentSubComponentsEditPart.VISUAL_ID:
				return new ComponentCompartmentComponentSubComponentsEditPart(
						view);

			case ComponentCompartmentComponentContentEditPart.VISUAL_ID:
				return new ComponentCompartmentComponentContentEditPart(view);

			case ComponentCompartmentComponentAttributesEditPart.VISUAL_ID:
				return new ComponentCompartmentComponentAttributesEditPart(view);

			case ComponentCompartmentComponentOthersEditPart.VISUAL_ID:
				return new ComponentCompartmentComponentOthersEditPart(view);

			case AttributesControllerCompartmentAttributesControllerEditPart.VISUAL_ID:
				return new AttributesControllerCompartmentAttributesControllerEditPart(
						view);

			case ComponentMergedCompartmentComponentSubComponentsEditPart.VISUAL_ID:
				return new ComponentMergedCompartmentComponentSubComponentsEditPart(
						view);

			case ComponentMergedCompartmentComponentContentEditPart.VISUAL_ID:
				return new ComponentMergedCompartmentComponentContentEditPart(
						view);

			case ComponentMergedCompartmentComponentAttributesEditPart.VISUAL_ID:
				return new ComponentMergedCompartmentComponentAttributesEditPart(
						view);

			case ComponentMergedCompartmentComponentOthersEditPart.VISUAL_ID:
				return new ComponentMergedCompartmentComponentOthersEditPart(
						view);

			case AttributesControllerMergedCompartmentAttributesControllerEditPart.VISUAL_ID:
				return new AttributesControllerMergedCompartmentAttributesControllerEditPart(
						view);

			case BindingEditPart.VISUAL_ID:
				return new BindingEditPart(view);

			case Binding2EditPart.VISUAL_ID:
				return new Binding2EditPart(view);
			}
		}
		return createUnrecognizedEditPart(context, model);
	}

	/**
	 * @generated
	 */
	private EditPart createUnrecognizedEditPart(EditPart context, Object model) {
		// Handle creation of unrecognized child node EditParts here
		return null;
	}

	/**
	 * @generated
	 */
	public static CellEditorLocator getTextCellEditorLocator(
			ITextAwareEditPart source) {
		if (source.getFigure() instanceof WrappingLabel)
			return new TextCellEditorLocator((WrappingLabel) source.getFigure());
		else {
			return new LabelCellEditorLocator((Label) source.getFigure());
		}
	}

	/**
	 * @generated
	 */
	static private class TextCellEditorLocator implements CellEditorLocator {

		/**
		 * @generated
		 */
		private WrappingLabel wrapLabel;

		/**
		 * @generated
		 */
		public TextCellEditorLocator(WrappingLabel wrapLabel) {
			this.wrapLabel = wrapLabel;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getWrapLabel() {
			return wrapLabel;
		}

		/**
		 * @generated
		 */
		public void relocate(CellEditor celleditor) {
			Text text = (Text) celleditor.getControl();
			Rectangle rect = getWrapLabel().getTextBounds().getCopy();
			getWrapLabel().translateToAbsolute(rect);
			if (getWrapLabel().isTextWrapOn()
					&& getWrapLabel().getText().length() > 0) {
				rect.setSize(new Dimension(text.computeSize(rect.width,
						SWT.DEFAULT)));
			} else {
				int avr = FigureUtilities.getFontMetrics(text.getFont())
						.getAverageCharWidth();
				rect.setSize(new Dimension(text.computeSize(SWT.DEFAULT,
						SWT.DEFAULT)).expand(avr * 2, 0));
			}
			if (!rect.equals(new Rectangle(text.getBounds()))) {
				text.setBounds(rect.x, rect.y, rect.width, rect.height);
			}
		}
	}

	/**
	 * @generated
	 */
	private static class LabelCellEditorLocator implements CellEditorLocator {

		/**
		 * @generated
		 */
		private Label label;

		/**
		 * @generated
		 */
		public LabelCellEditorLocator(Label label) {
			this.label = label;
		}

		/**
		 * @generated
		 */
		public Label getLabel() {
			return label;
		}

		/**
		 * @generated
		 */
		public void relocate(CellEditor celleditor) {
			Text text = (Text) celleditor.getControl();
			Rectangle rect = getLabel().getTextBounds().getCopy();
			getLabel().translateToAbsolute(rect);
			int avr = FigureUtilities.getFontMetrics(text.getFont())
					.getAverageCharWidth();
			rect.setSize(new Dimension(text.computeSize(SWT.DEFAULT,
					SWT.DEFAULT)).expand(avr * 2, 0));
			if (!rect.equals(new Rectangle(text.getBounds()))) {
				text.setBounds(rect.x, rect.y, rect.width, rect.height);
			}
		}
	}
}
