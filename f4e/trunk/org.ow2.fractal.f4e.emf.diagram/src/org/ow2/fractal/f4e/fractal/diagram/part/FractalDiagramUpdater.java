package org.ow2.fractal.f4e.fractal.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Attribute;
import org.ow2.fractal.f4e.fractal.AttributesController;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.Content;
import org.ow2.fractal.f4e.fractal.Controller;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.System;
import org.ow2.fractal.f4e.fractal.VirtualNode;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Attribute2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Attribute3EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributeEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesController2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerCompartmentAttributesControllerEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerMergedCompartmentAttributesControllerEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Binding2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.BindingEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Component2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentAttributesEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentExtendsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentOthersEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentSubComponentsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentAttributesEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentOthersEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentSubComponentsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Content2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Controller2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ControllerEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCallEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionAttributesEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionExtendsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionOthersEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionSubComponentsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Interface2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.InterfaceEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.SystemEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.VirtualNode2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.VirtualNodeEditPart;
import org.ow2.fractal.f4e.fractal.diagram.providers.FractalElementTypes;

/**
 * @generated
 */
public class FractalDiagramUpdater {

	/**
	 * @generated
	 */
	public static List getSemanticChildren(View view) {
		switch (FractalVisualIDRegistry.getVisualID(view)) {
		case DefinitionEditPart.VISUAL_ID:
			return getDefinition_1001SemanticChildren(view);
		case ComponentEditPart.VISUAL_ID:
			return getComponent_2002SemanticChildren(view);
		case Component2EditPart.VISUAL_ID:
			return getComponent_2011SemanticChildren(view);
		case DefinitionCompartmentDefinitionExtendsEditPart.VISUAL_ID:
			return getDefinitionCompartmentDefinitionExtends_5001SemanticChildren(view);
		case DefinitionCompartmentDefinitionSubComponentsEditPart.VISUAL_ID:
			return getDefinitionCompartmentDefinitionSubComponents_5002SemanticChildren(view);
		case DefinitionCompartmentDefinitionContentEditPart.VISUAL_ID:
			return getDefinitionCompartmentDefinitionContent_5003SemanticChildren(view);
		case DefinitionCompartmentDefinitionAttributesEditPart.VISUAL_ID:
			return getDefinitionCompartmentDefinitionAttributes_5004SemanticChildren(view);
		case DefinitionCompartmentDefinitionOthersEditPart.VISUAL_ID:
			return getDefinitionCompartmentDefinitionOthers_5005SemanticChildren(view);
		case ComponentCompartmentComponentExtendsEditPart.VISUAL_ID:
			return getComponentCompartmentComponentExtends_5006SemanticChildren(view);
		case ComponentCompartmentComponentSubComponentsEditPart.VISUAL_ID:
			return getComponentCompartmentComponentSubComponents_5007SemanticChildren(view);
		case ComponentCompartmentComponentContentEditPart.VISUAL_ID:
			return getComponentCompartmentComponentContent_5008SemanticChildren(view);
		case ComponentCompartmentComponentAttributesEditPart.VISUAL_ID:
			return getComponentCompartmentComponentAttributes_5009SemanticChildren(view);
		case ComponentCompartmentComponentOthersEditPart.VISUAL_ID:
			return getComponentCompartmentComponentOthers_5010SemanticChildren(view);
		case AttributesControllerCompartmentAttributesControllerEditPart.VISUAL_ID:
			return getAttributesControllerCompartmentAttributesController_5011SemanticChildren(view);
		case ComponentMergedCompartmentComponentSubComponentsEditPart.VISUAL_ID:
			return getComponentMergedCompartmentComponentSubComponents_5012SemanticChildren(view);
		case ComponentMergedCompartmentComponentContentEditPart.VISUAL_ID:
			return getComponentMergedCompartmentComponentContent_5013SemanticChildren(view);
		case ComponentMergedCompartmentComponentAttributesEditPart.VISUAL_ID:
			return getComponentMergedCompartmentComponentAttributes_5014SemanticChildren(view);
		case ComponentMergedCompartmentComponentOthersEditPart.VISUAL_ID:
			return getComponentMergedCompartmentComponentOthers_5015SemanticChildren(view);
		case AttributesControllerMergedCompartmentAttributesControllerEditPart.VISUAL_ID:
			return getAttributesControllerMergedCompartmentAttributesController_5016SemanticChildren(view);
		case SystemEditPart.VISUAL_ID:
			return getSystem_79SemanticChildren(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getDefinition_1001SemanticChildren(View view) {
		if (!view.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Definition modelElement = (Definition) view.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getInterfaces().iterator(); it
				.hasNext();) {
			Interface childElement = (Interface) it.next();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == InterfaceEditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator it = modelElement.getMergedInterfaces().iterator(); it
				.hasNext();) {
			Interface childElement = (Interface) it.next();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == Interface2EditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		
		return result;
	}

	/**
	 * @generated
	 */
	public static List getComponent_2002SemanticChildren(View view) {
		if (!view.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Component modelElement = (Component) view.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getInterfaces().iterator(); it
				.hasNext();) {
			Interface childElement = (Interface) it.next();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == InterfaceEditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator it = modelElement.getMergedInterfaces().iterator(); it
				.hasNext();) {
			Interface childElement = (Interface) it.next();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == Interface2EditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getComponent_2011SemanticChildren(View view) {
		if (!view.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Component modelElement = (Component) view.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getMergedInterfaces().iterator(); it
				.hasNext();) {
			Interface childElement = (Interface) it.next();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == Interface2EditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getDefinitionCompartmentDefinitionExtends_5001SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Definition modelElement = (Definition) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getExtendsAST().iterator(); it
				.hasNext();) {
			DefinitionCall childElement = (DefinitionCall) it.next();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == DefinitionCallEditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getDefinitionCompartmentDefinitionSubComponents_5002SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Definition modelElement = (Definition) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getSubComponents().iterator(); it
				.hasNext();) {
			Component childElement = (Component) it.next();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ComponentEditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator it = modelElement.getMergedSubComponents().iterator(); it
				.hasNext();) {
			Component childElement = (Component) it.next();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == Component2EditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getDefinitionCompartmentDefinitionContent_5003SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Definition modelElement = (Definition) containerView.getElement();
		List result = new LinkedList();
		{
			Content childElement = modelElement.getContent();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ContentEditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
			}
		}
		{
			Content childElement = modelElement.getMergedContent();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == Content2EditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getDefinitionCompartmentDefinitionAttributes_5004SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Definition modelElement = (Definition) containerView.getElement();
		List result = new LinkedList();
		{
			AttributesController childElement = modelElement
					.getAttributesController();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == AttributesControllerEditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
			}
		}
		{
			AttributesController childElement = modelElement
					.getMergedAttributesController();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == AttributesController2EditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getDefinitionCompartmentDefinitionOthers_5005SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Definition modelElement = (Definition) containerView.getElement();
		List result = new LinkedList();
		{
			VirtualNode childElement = modelElement.getVirtualNode();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == VirtualNodeEditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
			}
		}
		{
			Controller childElement = modelElement.getController();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ControllerEditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
			}
		}
		{
			VirtualNode childElement = modelElement.getMergedVirtualNode();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == VirtualNode2EditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
			}
		}
		{
			Controller childElement = modelElement.getMergedController();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == Controller2EditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getComponentCompartmentComponentExtends_5006SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Component modelElement = (Component) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getExtendsAST().iterator(); it
				.hasNext();) {
			DefinitionCall childElement = (DefinitionCall) it.next();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == DefinitionCallEditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getComponentCompartmentComponentSubComponents_5007SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Component modelElement = (Component) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getSubComponents().iterator(); it
				.hasNext();) {
			Component childElement = (Component) it.next();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ComponentEditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator it = modelElement.getMergedSubComponents().iterator(); it
				.hasNext();) {
			Component childElement = (Component) it.next();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == Component2EditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getComponentCompartmentComponentContent_5008SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Component modelElement = (Component) containerView.getElement();
		List result = new LinkedList();
		{
			Content childElement = modelElement.getContent();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ContentEditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
			}
		}
		{
			Content childElement = modelElement.getMergedContent();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == Content2EditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
			}
		}
		return result;
	}

	/**
	 * @generated NOT
	 */
	public static List getComponentCompartmentComponentAttributes_5009SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Component modelElement = (Component) containerView.getElement();
		List result = new LinkedList();
		{
			AttributesController childElement = modelElement
					.getAttributesController();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == AttributesControllerEditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
			}
		}
		{
			// We want to show the merged attributes controller only is
			// there is no attributes controller
			if(result.isEmpty()){
				AttributesController childElement = modelElement
					.getMergedAttributesController();
				int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
						childElement);
				if (visualID == AttributesController2EditPart.VISUAL_ID) {
					result.add(new FractalNodeDescriptor(childElement, visualID));
				}
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getComponentCompartmentComponentOthers_5010SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Component modelElement = (Component) containerView.getElement();
		List result = new LinkedList();
		{
			Controller childElement = modelElement.getController();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ControllerEditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
			}
		}
		{
			VirtualNode childElement = modelElement.getVirtualNode();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == VirtualNodeEditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
			}
		}
		{
			Controller childElement = modelElement.getMergedController();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == Controller2EditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
			}
		}
		{
			VirtualNode childElement = modelElement.getMergedVirtualNode();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == VirtualNode2EditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getAttributesControllerCompartmentAttributesController_5011SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		AttributesController modelElement = (AttributesController) containerView
				.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getAttributes().iterator(); it
				.hasNext();) {
			Attribute childElement = (Attribute) it.next();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == AttributeEditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		{
			for (Iterator it = modelElement.getMergedAttributes().iterator(); it
			.hasNext();) {
				Attribute childElement = (Attribute) it.next();
				int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
				childElement);
				if (visualID == Attribute2EditPart.VISUAL_ID) {
					result.add(new FractalNodeDescriptor(childElement, visualID));
					continue;
				}
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getComponentMergedCompartmentComponentSubComponents_5012SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Component modelElement = (Component) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getMergedSubComponents().iterator(); it
				.hasNext();) {
			Component childElement = (Component) it.next();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == Component2EditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getComponentMergedCompartmentComponentContent_5013SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Component modelElement = (Component) containerView.getElement();
		List result = new LinkedList();
		{
			Content childElement = modelElement.getMergedContent();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == Content2EditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getComponentMergedCompartmentComponentAttributes_5014SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Component modelElement = (Component) containerView.getElement();
		List result = new LinkedList();
		{
			AttributesController childElement = modelElement
					.getMergedAttributesController();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == AttributesController2EditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getComponentMergedCompartmentComponentOthers_5015SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Component modelElement = (Component) containerView.getElement();
		List result = new LinkedList();
		{
			Controller childElement = modelElement.getMergedController();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == Controller2EditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
			}
		}
		{
			VirtualNode childElement = modelElement.getMergedVirtualNode();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == VirtualNode2EditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getAttributesControllerMergedCompartmentAttributesController_5016SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		AttributesController modelElement = (AttributesController) containerView
				.getElement();
		List result = new LinkedList();
		{
			for (Iterator it = modelElement.getMergedAttributes().iterator(); it
			.hasNext();) {
				Attribute childElement = (Attribute) it.next();
				int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
				childElement);
				if (visualID == Attribute3EditPart.VISUAL_ID) {
					result.add(new FractalNodeDescriptor(childElement, visualID));
					continue;
				}
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSystem_79SemanticChildren(View view) {
		if (!view.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		System modelElement = (System) view.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getDefinitions().iterator(); it
				.hasNext();) {
			Definition childElement = (Definition) it.next();
			int visualID = FractalVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == DefinitionEditPart.VISUAL_ID) {
				result.add(new FractalNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getContainedLinks(View view) {
		switch (FractalVisualIDRegistry.getVisualID(view)) {
		case SystemEditPart.VISUAL_ID:
			return getSystem_79ContainedLinks(view);
		case DefinitionEditPart.VISUAL_ID:
			return getDefinition_1001ContainedLinks(view);
		case DefinitionCallEditPart.VISUAL_ID:
			return getDefinitionCall_2001ContainedLinks(view);
		case ComponentEditPart.VISUAL_ID:
			return getComponent_2002ContainedLinks(view);
		case ContentEditPart.VISUAL_ID:
			return getContent_2003ContainedLinks(view);
		case AttributesControllerEditPart.VISUAL_ID:
			return getAttributesController_2004ContainedLinks(view);
		case AttributeEditPart.VISUAL_ID:
			return getAttribute_2005ContainedLinks(view);
		case Attribute2EditPart.VISUAL_ID:
			return getAttribute_2006ContainedLinks(view);
		case ControllerEditPart.VISUAL_ID:
			return getController_2007ContainedLinks(view);
		case VirtualNodeEditPart.VISUAL_ID:
			return getVirtualNode_2008ContainedLinks(view);
		case InterfaceEditPart.VISUAL_ID:
			return getInterface_2009ContainedLinks(view);
		case Interface2EditPart.VISUAL_ID:
			return getInterface_2010ContainedLinks(view);
		case Component2EditPart.VISUAL_ID:
			return getComponent_2011ContainedLinks(view);
		case Content2EditPart.VISUAL_ID:
			return getContent_2012ContainedLinks(view);
		case AttributesController2EditPart.VISUAL_ID:
			return getAttributesController_2013ContainedLinks(view);
		case Attribute3EditPart.VISUAL_ID:
			return getAttribute_2014ContainedLinks(view);
		case Controller2EditPart.VISUAL_ID:
			return getController_2015ContainedLinks(view);
		case VirtualNode2EditPart.VISUAL_ID:
			return getVirtualNode_2016ContainedLinks(view);
		case BindingEditPart.VISUAL_ID:
			return getBinding_3001ContainedLinks(view);
		case Binding2EditPart.VISUAL_ID:
			return getBinding_3002ContainedLinks(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getIncomingLinks(View view) {
		switch (FractalVisualIDRegistry.getVisualID(view)) {
		case DefinitionEditPart.VISUAL_ID:
			return getDefinition_1001IncomingLinks(view);
		case DefinitionCallEditPart.VISUAL_ID:
			return getDefinitionCall_2001IncomingLinks(view);
		case ComponentEditPart.VISUAL_ID:
			return getComponent_2002IncomingLinks(view);
		case ContentEditPart.VISUAL_ID:
			return getContent_2003IncomingLinks(view);
		case AttributesControllerEditPart.VISUAL_ID:
			return getAttributesController_2004IncomingLinks(view);
		case AttributeEditPart.VISUAL_ID:
			return getAttribute_2005IncomingLinks(view);
		case Attribute2EditPart.VISUAL_ID:
			return getAttribute_2006IncomingLinks(view);
		case ControllerEditPart.VISUAL_ID:
			return getController_2007IncomingLinks(view);
		case VirtualNodeEditPart.VISUAL_ID:
			return getVirtualNode_2008IncomingLinks(view);
		case InterfaceEditPart.VISUAL_ID:
			return getInterface_2009IncomingLinks(view);
		case Interface2EditPart.VISUAL_ID:
			return getInterface_2010IncomingLinks(view);
		case Component2EditPart.VISUAL_ID:
			return getComponent_2011IncomingLinks(view);
		case Content2EditPart.VISUAL_ID:
			return getContent_2012IncomingLinks(view);
		case AttributesController2EditPart.VISUAL_ID:
			return getAttributesController_2013IncomingLinks(view);
		case Attribute3EditPart.VISUAL_ID:
			return getAttribute_2014IncomingLinks(view);
		case Controller2EditPart.VISUAL_ID:
			return getController_2015IncomingLinks(view);
		case VirtualNode2EditPart.VISUAL_ID:
			return getVirtualNode_2016IncomingLinks(view);
		case BindingEditPart.VISUAL_ID:
			return getBinding_3001IncomingLinks(view);
		case Binding2EditPart.VISUAL_ID:
			return getBinding_3002IncomingLinks(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getOutgoingLinks(View view) {
		switch (FractalVisualIDRegistry.getVisualID(view)) {
		case DefinitionEditPart.VISUAL_ID:
			return getDefinition_1001OutgoingLinks(view);
		case DefinitionCallEditPart.VISUAL_ID:
			return getDefinitionCall_2001OutgoingLinks(view);
		case ComponentEditPart.VISUAL_ID:
			return getComponent_2002OutgoingLinks(view);
		case ContentEditPart.VISUAL_ID:
			return getContent_2003OutgoingLinks(view);
		case AttributesControllerEditPart.VISUAL_ID:
			return getAttributesController_2004OutgoingLinks(view);
		case AttributeEditPart.VISUAL_ID:
			return getAttribute_2005OutgoingLinks(view);
		case Attribute2EditPart.VISUAL_ID:
			return getAttribute_2006OutgoingLinks(view);
		case ControllerEditPart.VISUAL_ID:
			return getController_2007OutgoingLinks(view);
		case VirtualNodeEditPart.VISUAL_ID:
			return getVirtualNode_2008OutgoingLinks(view);
		case InterfaceEditPart.VISUAL_ID:
			return getInterface_2009OutgoingLinks(view);
		case Interface2EditPart.VISUAL_ID:
			return getInterface_2010OutgoingLinks(view);
		case Component2EditPart.VISUAL_ID:
			return getComponent_2011OutgoingLinks(view);
		case Content2EditPart.VISUAL_ID:
			return getContent_2012OutgoingLinks(view);
		case AttributesController2EditPart.VISUAL_ID:
			return getAttributesController_2013OutgoingLinks(view);
		case Attribute3EditPart.VISUAL_ID:
			return getAttribute_2014OutgoingLinks(view);
		case Controller2EditPart.VISUAL_ID:
			return getController_2015OutgoingLinks(view);
		case VirtualNode2EditPart.VISUAL_ID:
			return getVirtualNode_2016OutgoingLinks(view);
		case BindingEditPart.VISUAL_ID:
			return getBinding_3001OutgoingLinks(view);
		case Binding2EditPart.VISUAL_ID:
			return getBinding_3002OutgoingLinks(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getSystem_79ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getDefinition_1001ContainedLinks(View view) {
		Definition modelElement = (Definition) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getContainedTypeModelFacetLinks_Binding_3001(modelElement));
		result
				.addAll(getContainedTypeModelFacetLinks_Binding_3002(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getDefinitionCall_2001ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getComponent_2002ContainedLinks(View view) {
		Component modelElement = (Component) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getContainedTypeModelFacetLinks_Binding_3001(modelElement));
		result
				.addAll(getContainedTypeModelFacetLinks_Binding_3002(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getContent_2003ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getAttributesController_2004ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getAttribute_2005ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getAttribute_2006ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getController_2007ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getVirtualNode_2008ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getInterface_2009ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getInterface_2010ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getComponent_2011ContainedLinks(View view) {
		Component modelElement = (Component) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getContainedTypeModelFacetLinks_Binding_3001(modelElement));
		result
				.addAll(getContainedTypeModelFacetLinks_Binding_3002(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getContent_2012ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getAttributesController_2013ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getAttribute_2014ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getController_2015ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getVirtualNode_2016ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getBinding_3001ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getBinding_3002ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getDefinition_1001IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getDefinitionCall_2001IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getComponent_2002IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getContent_2003IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getAttributesController_2004IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getAttribute_2005IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getAttribute_2006IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getController_2007IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getVirtualNode_2008IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getInterface_2009IncomingLinks(View view) {
		Interface modelElement = (Interface) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_Binding_3001(modelElement,
				crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Binding_3002(modelElement,
				crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getInterface_2010IncomingLinks(View view) {
		Interface modelElement = (Interface) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_Binding_3001(modelElement,
				crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Binding_3002(modelElement,
				crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getComponent_2011IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getContent_2012IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getAttributesController_2013IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getAttribute_2014IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getController_2015IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getVirtualNode_2016IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getBinding_3001IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getBinding_3002IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getDefinition_1001OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getDefinitionCall_2001OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getComponent_2002OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getContent_2003OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getAttributesController_2004OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getAttribute_2005OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getAttribute_2006OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getController_2007OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getVirtualNode_2008OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getInterface_2009OutgoingLinks(View view) {
		Interface modelElement = (Interface) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_Binding_3001(modelElement));
		result
				.addAll(getOutgoingTypeModelFacetLinks_Binding_3002(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getInterface_2010OutgoingLinks(View view) {
		Interface modelElement = (Interface) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_Binding_3001(modelElement));
		result
				.addAll(getOutgoingTypeModelFacetLinks_Binding_3002(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getComponent_2011OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getContent_2012OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getAttributesController_2013OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getAttribute_2014OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getController_2015OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getVirtualNode_2016OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getBinding_3001OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getBinding_3002OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	private static Collection getContainedTypeModelFacetLinks_Binding_3001(
			AbstractComponent container) {
		Collection result = new LinkedList();
		for (Iterator links = container.getBindings().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Binding) {
				continue;
			}
			Binding link = (Binding) linkObject;
			if (BindingEditPart.VISUAL_ID != FractalVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Interface dst = link.getClientInterface();
			Interface src = link.getServerInterface();
			result
					.add(new FractalLinkDescriptor(src, dst, link,
							FractalElementTypes.Binding_3001,
							BindingEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getContainedTypeModelFacetLinks_Binding_3002(
			AbstractComponent container) {
		Collection result = new LinkedList();
		for (Iterator links = container.getMergedBindings().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Binding) {
				continue;
			}
			Binding link = (Binding) linkObject;
			if (Binding2EditPart.VISUAL_ID != FractalVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Interface dst = link.getClientInterface();
			Interface src = link.getServerInterface();
			result.add(new FractalLinkDescriptor(src, dst, link,
					FractalElementTypes.Binding_3002,
					Binding2EditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getIncomingTypeModelFacetLinks_Binding_3001(
			Interface target, Map crossReferences) {
		Collection result = new LinkedList();
		Collection settings = (Collection) crossReferences.get(target);
		for (Iterator it = settings.iterator(); it.hasNext();) {
			EStructuralFeature.Setting setting = (EStructuralFeature.Setting) it
					.next();
			if (setting.getEStructuralFeature() != FractalPackage.eINSTANCE
					.getBinding_ClientInterface()
					|| false == setting.getEObject() instanceof Binding) {
				continue;
			}
			Binding link = (Binding) setting.getEObject();
			if (BindingEditPart.VISUAL_ID != FractalVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Interface src = link.getServerInterface();
			result
					.add(new FractalLinkDescriptor(src, target, link,
							FractalElementTypes.Binding_3001,
							BindingEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getIncomingTypeModelFacetLinks_Binding_3002(
			Interface target, Map crossReferences) {
		Collection result = new LinkedList();
		Collection settings = (Collection) crossReferences.get(target);
		for (Iterator it = settings.iterator(); it.hasNext();) {
			EStructuralFeature.Setting setting = (EStructuralFeature.Setting) it
					.next();
			if (setting.getEStructuralFeature() != FractalPackage.eINSTANCE
					.getBinding_ClientInterface()
					|| false == setting.getEObject() instanceof Binding) {
				continue;
			}
			Binding link = (Binding) setting.getEObject();
			if (Binding2EditPart.VISUAL_ID != FractalVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Interface src = link.getServerInterface();
			result.add(new FractalLinkDescriptor(src, target, link,
					FractalElementTypes.Binding_3002,
					Binding2EditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getOutgoingTypeModelFacetLinks_Binding_3001(
			Interface source) {
		AbstractComponent container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof AbstractComponent) {
				container = (AbstractComponent) element;
			}
		}
		if (container == null) {
			return Collections.EMPTY_LIST;
		}
		Collection result = new LinkedList();
		for (Iterator links = container.getBindings().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Binding) {
				continue;
			}
			Binding link = (Binding) linkObject;
			if (BindingEditPart.VISUAL_ID != FractalVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Interface dst = link.getClientInterface();
			Interface src = link.getServerInterface();
			if (src != source) {
				continue;
			}
			result
					.add(new FractalLinkDescriptor(src, dst, link,
							FractalElementTypes.Binding_3001,
							BindingEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getOutgoingTypeModelFacetLinks_Binding_3002(
			Interface source) {
		AbstractComponent container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof AbstractComponent) {
				container = (AbstractComponent) element;
			}
		}
		if (container == null) {
			return Collections.EMPTY_LIST;
		}
		Collection result = new LinkedList();
		for (Iterator links = container.getMergedBindings().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Binding) {
				continue;
			}
			Binding link = (Binding) linkObject;
			if (Binding2EditPart.VISUAL_ID != FractalVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Interface dst = link.getClientInterface();
			Interface src = link.getServerInterface();
			if (src != source) {
				continue;
			}
			result.add(new FractalLinkDescriptor(src, dst, link,
					FractalElementTypes.Binding_3002,
					Binding2EditPart.VISUAL_ID));
		}
		return result;
	}

}
