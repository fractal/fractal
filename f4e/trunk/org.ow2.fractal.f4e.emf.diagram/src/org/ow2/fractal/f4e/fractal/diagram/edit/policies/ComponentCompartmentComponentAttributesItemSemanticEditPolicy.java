package org.ow2.fractal.f4e.fractal.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyRequest;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.diagram.edit.commands.AttributesController2CreateCommand;
import org.ow2.fractal.f4e.fractal.diagram.edit.commands.AttributesControllerCreateCommand;
import org.ow2.fractal.f4e.fractal.diagram.providers.FractalElementTypes;

/**
 * @generated
 */
public class ComponentCompartmentComponentAttributesItemSemanticEditPolicy
		extends FractalBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (FractalElementTypes.AttributesController_2004 == req
				.getElementType()) {
			if (req.getContainmentFeature() == null) {
				req.setContainmentFeature(FractalPackage.eINSTANCE
						.getAbstractComponent_AttributesController());
			}
			return getGEFWrapper(new AttributesControllerCreateCommand(req));
		}
		if (FractalElementTypes.AttributesController_2013 == req
				.getElementType()) {
			if (req.getContainmentFeature() == null) {
				req.setContainmentFeature(FractalPackage.eINSTANCE
						.getAbstractComponent_MergedAttributesController());
			}
			return getGEFWrapper(new AttributesController2CreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	// Start yann hand modifs
	protected boolean shouldProceed(DestroyRequest destroyRequest) {
		// TODO Auto-generated method stub
		return false;
	}
	// End yann hand modifs
}
