package org.ow2.fractal.f4e.fractal.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyRequest;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.diagram.edit.commands.Attribute2CreateCommand;
import org.ow2.fractal.f4e.fractal.diagram.edit.commands.AttributeCreateCommand;
import org.ow2.fractal.f4e.fractal.diagram.providers.FractalElementTypes;

/**
 * @generated
 */
public class AttributesControllerCompartmentAttributesControllerItemSemanticEditPolicy
		extends FractalBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (FractalElementTypes.Attribute_2005 == req.getElementType()) {
			if (req.getContainmentFeature() == null) {
				req.setContainmentFeature(FractalPackage.eINSTANCE
						.getAttributesController_Attributes());
			}
			return getGEFWrapper(new AttributeCreateCommand(req));
		}
		if (FractalElementTypes.Attribute_2006 == req.getElementType()) {
			if (req.getContainmentFeature() == null) {
				req.setContainmentFeature(FractalPackage.eINSTANCE
						.getAttributesController_MergedAttributes());
			}
			return getGEFWrapper(new Attribute2CreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	// Start yann hand modifs
	protected boolean shouldProceed(DestroyRequest destroyRequest) {
		// TODO Auto-generated method stub
		return false;
	}
	// End yann hand modifs
}
