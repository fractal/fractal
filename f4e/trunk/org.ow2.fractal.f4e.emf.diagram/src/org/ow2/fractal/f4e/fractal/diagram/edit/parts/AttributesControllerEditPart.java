package org.ow2.fractal.f4e.fractal.diagram.edit.parts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.handles.NonResizableHandleKit;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.core.edithelpers.CreateElementRequestAdapter;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CreationEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewAndElementRequest;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.diagram.custom.figures.AbstractComponentNameWrappingLabel;
import org.ow2.fractal.f4e.diagram.custom.figures.AttributesControllerShape;
import org.ow2.fractal.f4e.diagram.custom.figures.ContainerShape;
import org.ow2.fractal.f4e.diagram.custom.layouts.AutosizeContainerLayout;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.AttributesControllerItemSemanticEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalVisualIDRegistry;
import org.ow2.fractal.f4e.fractal.diagram.providers.FractalElementTypes;

/**
 * @generated
 */
public class AttributesControllerEditPart extends ShapeNodeEditPart{

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 2004;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public AttributesControllerEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		installEditPolicy(EditPolicyRoles.CREATION_ROLE,
				new CreationEditPolicy() {
					public Command getCommand(Request request) {
						if (understandsRequest(request)) {
							if (request instanceof CreateViewAndElementRequest) {
								CreateElementRequestAdapter adapter = ((CreateViewAndElementRequest) request)
										.getViewAndElementDescriptor()
										.getCreateElementRequestAdapter();
								IElementType type = (IElementType) adapter
										.getAdapter(IElementType.class);
								if (type == FractalElementTypes.Attribute_2005) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(AttributesControllerCompartmentAttributesControllerEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								if (type == FractalElementTypes.Attribute_2006) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(AttributesControllerCompartmentAttributesControllerEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
							}
							return super.getCommand(request);
						}
						return null;
					}
				});
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new AttributesControllerItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		
		installEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE,
				new NonResizableEditPolicy() {

					protected List createSelectionHandles() {
						List handles = new ArrayList();
						NonResizableHandleKit.addMoveHandle(
								(GraphicalEditPart) getHost(), handles);
						return handles;
					}

					public Command getCommand(Request request) {
						return null;
					}

					public boolean understandsRequest(Request request) {
						return false;
					}
				});
		
		// Remove the connection handles
		removeEditPolicy(EditPolicyRoles.CONNECTION_HANDLES_ROLE);
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		LayoutEditPolicy lep = new LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				EditPolicy result = child
						.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated
	 */
	protected IFigure createNodeShape() {
		AttributesFigure figure = new AttributesFigure();
		return primaryShape = figure;
	}

	/**
	 * @generated
	 */
	public AttributesFigure getPrimaryShape() {
		return (AttributesFigure) primaryShape;
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof AttributesControllerSignatureEditPart) {
			((AttributesControllerSignatureEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureLabelAttributesControllerSignature());
			return true;
		}
		if (childEditPart instanceof AttributesControllerCompartmentAttributesControllerEditPart) {
			IFigure pane = getPrimaryShape().getFigureAttributesArea();
			
			if (pane.getLayoutManager() == null) {
				AutosizeContainerLayout layout = new AutosizeContainerLayout();
				pane.setLayoutManager(layout);
			}
			
			pane
					.add(((AttributesControllerCompartmentAttributesControllerEditPart) childEditPart)
							.getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {

		if (childEditPart instanceof AttributesControllerCompartmentAttributesControllerEditPart) {
			IFigure pane = getPrimaryShape().getFigureAttributesArea();
			
			if (pane.getLayoutManager() == null) {
				ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
				//ComponentAttributesAreaLayout layout = new ComponentAttributesAreaLayout(getMapMode());
				pane.setLayoutManager(layout);
			}
			pane
					.remove(((AttributesControllerCompartmentAttributesControllerEditPart) childEditPart)
							.getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {

		if (editPart instanceof AttributesControllerCompartmentAttributesControllerEditPart) {
			return getPrimaryShape().getFigureAttributesArea();
		}
		return super.getContentPaneFor(editPart);
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(getMapMode()
				.DPtoLP(0), getMapMode().DPtoLP(0));
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createNodeFigure() {
		NodeFigure figure = createNodePlate();
		
		figure.setLayoutManager(new ConstrainedToolbarLayout());
		
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane.
	 * Respects layout one may have set for generated figure.
	 * @param nodeShape instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(getMapMode().DPtoLP(5));
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(FractalVisualIDRegistry
				.getType(AttributesControllerSignatureEditPart.VISUAL_ID));
	}

	/**
	 * @generated
	 */
	public class AttributesFigure extends AttributesControllerShape {

		/**
		 * @generated
		 */
		private AttributesControllerShape fFigureAttributesFigure;
		/**
		 * @generated
		 */
		private AbstractComponentNameWrappingLabel fFigureLabelAttributesControllerSignature;
		/**
		 * @generated
		 */
		private ContainerShape fFigureAttributesArea;

		/**
		 * @generated
		 */
		public AttributesFigure() {

			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents() {
			
			fFigureLabelAttributesControllerSignature = new AbstractComponentNameWrappingLabel();

//			fFigureLabelAttributesControllerSignature
//					.setMaximumSize(new Dimension(
//							getMapMode().DPtoLP(50000000), getMapMode().DPtoLP(
//									20)));

			this.add(fFigureLabelAttributesControllerSignature);
		
			fFigureAttributesArea = new ContainerShape();
			
			this.add(fFigureAttributesArea);
			

		}

		/**
		 * @generated
		 */
		private boolean myUseLocalCoordinates = false;

		/**
		 * @generated
		 */
		protected boolean useLocalCoordinates() {
			return myUseLocalCoordinates;
		}

		/**
		 * @generated
		 */
		protected void setUseLocalCoordinates(boolean useLocalCoordinates) {
			myUseLocalCoordinates = useLocalCoordinates;
		}

		/**
		 * @generated
		 */
		public AttributesControllerShape getFigureAttributesFigure() {
			return fFigureAttributesFigure;
		}

		/**
		 * @generated
		 */
		public AbstractComponentNameWrappingLabel getFigureLabelAttributesControllerSignature() {
			return fFigureLabelAttributesControllerSignature;
		}

		/**
		 * @generated
		 */
		public ContainerShape getFigureAttributesArea() {
			return fFigureAttributesArea;
		}

	}

}
