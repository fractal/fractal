package org.ow2.fractal.f4e.fractal.diagram.edit.parts;

import java.util.Iterator;

import org.eclipse.draw2d.Border;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.core.edithelpers.CreateElementRequestAdapter;
import org.eclipse.gmf.runtime.diagram.ui.editparts.AbstractBorderItemEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IBorderItemEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CreationEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.DragDropEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewAndElementRequest;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Color;
import org.ow2.fractal.f4e.diagram.custom.edit.parts.AbstractComponentEditPart;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.ComponentResizableEditPolicy;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.InterfaceBorderItemSelectionEditPolicy;
import org.ow2.fractal.f4e.diagram.custom.figures.AbstractComponentNameWrappingLabel;
import org.ow2.fractal.f4e.diagram.custom.figures.AbstractComponentShape;
import org.ow2.fractal.f4e.diagram.custom.figures.AlphaDropShadowBorder;
import org.ow2.fractal.f4e.diagram.custom.figures.ComponentBorder;
import org.ow2.fractal.f4e.diagram.custom.figures.ContainerShape;
import org.ow2.fractal.f4e.diagram.custom.figures.InterfaceShape;
import org.ow2.fractal.f4e.diagram.custom.layouts.AutosizeContainerLayout;
import org.ow2.fractal.f4e.diagram.custom.layouts.InterfaceBorderItemLocator;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.ComponentCanonicalEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.ComponentItemSemanticEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalVisualIDRegistry;
import org.ow2.fractal.f4e.fractal.diagram.providers.FractalElementTypes;

/**
 * @generated
 */
public class ComponentEditPart extends AbstractComponentEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 2002;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public ComponentEditPart(View view) {
		super(view);
	}

	/**
	 * @generated NOT
	 */
	protected void createDefaultEditPolicies() {
		installEditPolicy(EditPolicyRoles.CREATION_ROLE,
				new CreationEditPolicy() {
					public Command getCommand(Request request) {
						if (understandsRequest(request)) {
							if (request instanceof CreateViewAndElementRequest) {
								CreateElementRequestAdapter adapter = ((CreateViewAndElementRequest) request)
										.getViewAndElementDescriptor()
										.getCreateElementRequestAdapter();
								IElementType type = (IElementType) adapter
										.getAdapter(IElementType.class);
								if (type == FractalElementTypes.DefinitionCall_2001) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(ComponentCompartmentComponentExtendsEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								if (type == FractalElementTypes.Content_2003) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(ComponentCompartmentComponentContentEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								if (type == FractalElementTypes.Content_2012) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(ComponentCompartmentComponentContentEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								if (type == FractalElementTypes.Controller_2007) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(ComponentCompartmentComponentOthersEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								if (type == FractalElementTypes.VirtualNode_2008) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(ComponentCompartmentComponentOthersEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								if (type == FractalElementTypes.Controller_2015) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(ComponentCompartmentComponentOthersEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								if (type == FractalElementTypes.VirtualNode_2016) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(ComponentCompartmentComponentOthersEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								
								// Start Yann hand modif
								// When user click on the popup button to create component
								if (type == FractalElementTypes.Component_2002) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(ComponentCompartmentComponentSubComponentsEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								if (type == FractalElementTypes.AttributesController_2004) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(ComponentCompartmentComponentAttributesEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								if (type == FractalElementTypes.Attribute_2005) {
									IGraphicalEditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(ComponentCompartmentComponentAttributesEditPart.VISUAL_ID));
									EditPart compartmentAttributes = null;
									if(compartmentEditPart != null){
										IGraphicalEditPart attributesEditPart = compartmentEditPart.getChildBySemanticHint(FractalVisualIDRegistry
												.getType(AttributesControllerEditPart.VISUAL_ID));
										if(attributesEditPart != null){
											compartmentAttributes = attributesEditPart.getChildBySemanticHint(FractalVisualIDRegistry
													.getType(AttributesControllerCompartmentAttributesControllerEditPart.VISUAL_ID));
										}
										
									}
									return compartmentAttributes == null ? null
											: compartmentAttributes
													.getCommand(request);
								}
								//End Yann hand modif
							}
							return super.getCommand(request);
						}
						return null;
					}
				});
		super.createDefaultEditPolicies();
		
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new ComponentItemSemanticEditPolicy());
		
		installEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE,
				new DragDropEditPolicy());
		
		installEditPolicy(EditPolicyRoles.CANONICAL_ROLE,
				new ComponentCanonicalEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		
		// A component can't be drop in a other component,
		// it can be only drop in the subcomponent compartment
		// so we avoid the drag and drop policy for components
		removeEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE);
		
		// Remove the connection handles
		removeEditPolicy(EditPolicyRoles.CONNECTION_HANDLES_ROLE);
		
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated NOT
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		LayoutEditPolicy lep = new LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				if (child instanceof IBorderItemEditPart) {
					// Start Hand Modif Yann
					return new InterfaceBorderItemSelectionEditPolicy();
					// End Hand Modif Yann
				}
				EditPolicy result = child
						.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated
	 */
	protected IFigure createNodeShape() {
		ComponentFigure figure = new ComponentFigure();
		return primaryShape = figure;
	}

	/**
	 * @generated
	 */
	public ComponentFigure getPrimaryShape() {
		return (ComponentFigure) primaryShape;
	}

	/**
	 * @generated NOT
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		// Start yann hand modif
		org.ow2.fractal.f4e.notation.AbstractComponentStyle style = (org.ow2.fractal.f4e.notation.AbstractComponentStyle) this
				.getNotationView().getStyle(
						org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE
								.getAbstractComponentStyle());
		// End yann hand modif
		
		if (childEditPart instanceof ComponentNameEditPart) {
			((ComponentNameEditPart) childEditPart).setLabel(getPrimaryShape()
					.getFigureLabelComponentName());
			return true;
		}
		if (childEditPart instanceof ComponentCompartmentComponentExtendsEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureComponentExtendsAreaFigure();
			
			if (pane.getLayoutManager() == null) {
				AutosizeContainerLayout layout = new AutosizeContainerLayout();
				pane.setLayoutManager(layout);
			}
			
			pane
					.add(((ComponentCompartmentComponentExtendsEditPart) childEditPart)
							.getFigure());
			// Start yann hand modifs.
			pane
					.setVisible(style != null ? !style.getCollapsedExtends()
							: true);
			//End yann hand modifs.
			return true;
		}
		if (childEditPart instanceof ComponentCompartmentComponentSubComponentsEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureComponentSubComponentsAreaFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			
			pane
					.add(((ComponentCompartmentComponentSubComponentsEditPart) childEditPart)
							.getFigure());
			// Start yann hand modifs.
			pane
					.setVisible(style != null ? !style.getCollapsedSubComponents()
							: true);
			//End yann hand modifs.
			return true;
		}
		if (childEditPart instanceof ComponentCompartmentComponentContentEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureComponentContentAreaFigure();
//			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			// We customize the layout for the content container.
			// As the content container contains only one child, a label
			// we stretch it.
			if (pane.getLayoutManager() == null) {
				AutosizeContainerLayout layout = new AutosizeContainerLayout();
				pane.setLayoutManager(layout);
			}
			pane
					.add(((ComponentCompartmentComponentContentEditPart) childEditPart)
							.getFigure());
			// Start yann hand modifs.
			pane
					.setVisible(style != null ? !style.getCollapsedContent()
							: true);
			//End yann hand modifs.
			return true;
		}
		if (childEditPart instanceof ComponentCompartmentComponentAttributesEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureComponentAttributesAreaFigure();

			// Start yann hand modifs.
			if (pane.getLayoutManager() == null) {
				AutosizeContainerLayout layout = new AutosizeContainerLayout();
				
				pane.setLayoutManager(layout);
			}
			pane
					.add(((ComponentCompartmentComponentAttributesEditPart) childEditPart)
							.getFigure());
			pane
					.setVisible(style != null ? !style.getCollapsedAttributes()
							: true);
			
			//End yann hand modifs.
			return true;
		}
		if (childEditPart instanceof ComponentCompartmentComponentOthersEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureComponentOthersAreaFigure();
		
			if (pane.getLayoutManager() == null) {
				AutosizeContainerLayout layout = new AutosizeContainerLayout();
				pane.setLayoutManager(layout);
			}
			
			pane
					.add(((ComponentCompartmentComponentOthersEditPart) childEditPart)
							.getFigure());
			// Start yann hand modifs.
			pane
					.setVisible(style != null ? !style.getCollapsedOthers()
							: true);
			//End yann hand modifs.
			return true;
		}
		if (childEditPart instanceof InterfaceEditPart) {
			// Start yann hand modifs.
			InterfaceBorderItemLocator locator = new InterfaceBorderItemLocator(
					(InterfaceEditPart)childEditPart);
			// End yann hand modifs.
			getBorderedFigure().getBorderItemContainer().add(
					((InterfaceEditPart) childEditPart).getFigure(), locator);
			return true;
		}
		if (childEditPart instanceof Interface2EditPart) {
			// Start yann hand modifs.
			InterfaceBorderItemLocator locator = new InterfaceBorderItemLocator(
					(Interface2EditPart) childEditPart);
			// End yann hand modifs.
			getBorderedFigure().getBorderItemContainer().add(
					((Interface2EditPart) childEditPart).getFigure(), locator);
			return true;
		}
		return false;
	}


	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {

		if (childEditPart instanceof ComponentCompartmentComponentExtendsEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureComponentExtendsAreaFigure();
			setupContentPane(pane);
			pane
					.remove(((ComponentCompartmentComponentExtendsEditPart) childEditPart)
							.getFigure());
			return true;
		}
		if (childEditPart instanceof ComponentCompartmentComponentSubComponentsEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureComponentSubComponentsAreaFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane
					.remove(((ComponentCompartmentComponentSubComponentsEditPart) childEditPart)
							.getFigure());
			return true;
		}
		if (childEditPart instanceof ComponentCompartmentComponentContentEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureComponentContentAreaFigure();
			setupContentPane(pane);
			pane
					.remove(((ComponentCompartmentComponentContentEditPart) childEditPart)
							.getFigure());
			return true;
		}
		if (childEditPart instanceof ComponentCompartmentComponentAttributesEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureComponentAttributesAreaFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane
					.remove(((ComponentCompartmentComponentAttributesEditPart) childEditPart)
							.getFigure());
			return true;
		}
		if (childEditPart instanceof ComponentCompartmentComponentOthersEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureComponentOthersAreaFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane
					.remove(((ComponentCompartmentComponentOthersEditPart) childEditPart)
							.getFigure());
			return true;
		}
		if (childEditPart instanceof InterfaceEditPart) {
			getBorderedFigure().getBorderItemContainer().remove(
					((InterfaceEditPart) childEditPart).getFigure());
			return true;
		}
		if (childEditPart instanceof Interface2EditPart) {
			getBorderedFigure().getBorderItemContainer().remove(
					((Interface2EditPart) childEditPart).getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {

		if (editPart instanceof ComponentCompartmentComponentExtendsEditPart) {
			return getPrimaryShape().getFigureComponentExtendsAreaFigure();
		}
		if (editPart instanceof ComponentCompartmentComponentSubComponentsEditPart) {
			return getPrimaryShape()
					.getFigureComponentSubComponentsAreaFigure();
		}
		if (editPart instanceof ComponentCompartmentComponentContentEditPart) {
			return getPrimaryShape().getFigureComponentContentAreaFigure();
		}
		if (editPart instanceof ComponentCompartmentComponentAttributesEditPart) {
			return getPrimaryShape().getFigureComponentAttributesAreaFigure();
		}
		if (editPart instanceof ComponentCompartmentComponentOthersEditPart) {
			return getPrimaryShape().getFigureComponentOthersAreaFigure();
		}
		if (editPart instanceof InterfaceEditPart) {
			return getBorderedFigure().getBorderItemContainer();
		}
		if (editPart instanceof Interface2EditPart) {
			return getBorderedFigure().getBorderItemContainer();
		}
		return super.getContentPaneFor(editPart);
	}

	/**
	 * @generated NOT
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(getMapMode()
				.DPtoLP(50), getMapMode().DPtoLP(50));
		
		// Start yann hand modifs
		AlphaDropShadowBorder shadowBorder = new AlphaDropShadowBorder();
		shadowBorder.setShouldDrawDropShadow(true);
		result.setBorder(shadowBorder);
		//End yann hand modifs
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createMainFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane.
	 * Respects layout one may have set for generated figure.
	 * @param nodeShape instance of generated figure class
	 * @generated NOT
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			//ComponentContrainedLayout layout = new ComponentContrainedLayout();
			// Start yann hand modifs
			layout.setSpacing(getMapMode().DPtoLP(0));
			// End yann hand modifs
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(FractalVisualIDRegistry
				.getType(ComponentNameEditPart.VISUAL_ID));
	}

	/**
	 * @generated
	 */
	public class ComponentFigure extends AbstractComponentShape {

		/**
		 * @generated
		 */
		private AbstractComponentNameWrappingLabel fFigureLabelComponentName;
		/**
		 * @generated
		 */
		private AbstractComponentShape fFigureComponentFigure;
		/**
		 * @generated
		 */
		private ContainerShape fFigureComponentExtendsAreaFigure;
		/**
		 * @generated
		 */
		private ContainerShape fFigureComponentSubComponentsAreaFigure;
		/**
		 * @generated
		 */
		private ContainerShape fFigureComponentAttributesAreaFigure;
		/**
		 * @generated
		 */
		private ContainerShape fFigureComponentOthersAreaFigure;
		/**
		 * @generated
		 */
		private ContainerShape fFigureComponentContentAreaFigure;

		/**
		 * @generated
		 */
		public ComponentFigure() {

			this.setBorder(createBorder0());
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents() {

			fFigureLabelComponentName = new AbstractComponentNameWrappingLabel();
			
			this.add(fFigureLabelComponentName);

			fFigureComponentExtendsAreaFigure = new ContainerShape();
			
			this.add(fFigureComponentExtendsAreaFigure);

			fFigureComponentSubComponentsAreaFigure = new ContainerShape();

			this.add(fFigureComponentSubComponentsAreaFigure);

			fFigureComponentContentAreaFigure = new ContainerShape(){
				public Rectangle getClientArea(Rectangle rect) {
					return super.getClientArea(rect);
				}
				
			};
				
			this.add(fFigureComponentContentAreaFigure);
			
			fFigureComponentAttributesAreaFigure = new ContainerShape(){

				public Rectangle getBounds() {
					// TODO Auto-generated method stub
					return super.getBounds();
				}

				public Rectangle getClientArea(Rectangle rect) {
					// TODO Auto-generated method stub
					return super.getClientArea(rect);
				}
				
			};
			
			this.add(fFigureComponentAttributesAreaFigure);

			fFigureComponentOthersAreaFigure = new ContainerShape();
		
			this.add(fFigureComponentOthersAreaFigure);

		}

		/**
		 * @generated
		 */
		private Border createBorder0() {
			ComponentBorder result = new ComponentBorder();
			return result;
		}

		/**
		 * @generated
		 */
		private boolean myUseLocalCoordinates = false;

		/**
		 * @generated
		 */
		protected boolean useLocalCoordinates() {
			return myUseLocalCoordinates;
		}

		/**
		 * @generated
		 */
		protected void setUseLocalCoordinates(boolean useLocalCoordinates) {
			myUseLocalCoordinates = useLocalCoordinates;
		}

		/**
		 * @generated
		 */
		public AbstractComponentNameWrappingLabel getFigureLabelComponentName() {
			return fFigureLabelComponentName;
		}

		/**
		 * @generated
		 */
		public AbstractComponentShape getFigureComponentFigure() {
			return fFigureComponentFigure;
		}

		/**
		 * @generated
		 */
		public ContainerShape getFigureComponentExtendsAreaFigure() {
			return fFigureComponentExtendsAreaFigure;
		}

		/**
		 * @generated
		 */
		public ContainerShape getFigureComponentSubComponentsAreaFigure() {
			return fFigureComponentSubComponentsAreaFigure;
		}

		/**
		 * @generated
		 */
		public ContainerShape getFigureComponentAttributesAreaFigure() {
			return fFigureComponentAttributesAreaFigure;
		}

		/**
		 * @generated
		 */
		public ContainerShape getFigureComponentOthersAreaFigure() {
			return fFigureComponentOthersAreaFigure;
		}

		/**
		 * @generated
		 */
		public ContainerShape getFigureComponentContentAreaFigure() {
			return fFigureComponentContentAreaFigure;
		}

		public Dimension getMinimumSize(int wHint, int hHint) {
			Dimension minSize = super.getMinimumSize(wHint, hHint);
			int minHeigth = calculateMinimumSizeWithInterfaces().height;
			if(minSize.height<minHeigth){
				minSize.height = minHeigth;
			}
			return minSize;//calculateMinimumSizeWithInterfaces();
		}

		/**
		 * The sub component container section should have
	     * a minimum size > to the sum of the size of its server/client interfaces
	     */
		protected Dimension calculateMinimumSizeWithInterfaces() {
			int sumServerInterfacesHeight = 0;
			int sumClientInterfacesHeight = 0;
			

			Iterator<EditPart> iterator = ComponentEditPart.this.getChildren().iterator();
			while(iterator.hasNext()){
				EditPart c = iterator.next();
				if(c instanceof AbstractBorderItemEditPart) {
					EObject eObject =  ((View)c.getModel()).getElement();
					IFigure interfaceFigure = ((AbstractBorderItemEditPart)c).getContentPane();
					if(interfaceFigure instanceof InterfaceShape){
						if(((InterfaceShape)interfaceFigure).getRole() == InterfaceShape.Role.SERVER){
							sumServerInterfacesHeight += interfaceFigure.getSize().height + 5;
						}else{
							sumClientInterfacesHeight += interfaceFigure.getSize().height + 5;
						}
					}
				}
			}
		
			int maxSumInterfacesHeight = sumClientInterfacesHeight>sumServerInterfacesHeight?sumClientInterfacesHeight:sumServerInterfacesHeight;
			
			return new Dimension(0,maxSumInterfacesHeight);
		}
		
		
	}

	// Start yann hand modif
	public EditPolicy getPrimaryDragEditPolicy() {
		return new ComponentResizableEditPolicy();
	}

	public void collapse(boolean collapse, Object feature) {
		EditPart editPart = null;
		ContainerShape shape = null;
		if (org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE
				.getAbstractComponentStyle_CollapsedExtends().equals(feature)) {
			shape = getPrimaryShape().getFigureComponentExtendsAreaFigure();
		} else if (org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE
				.getAbstractComponentStyle_CollapsedSubComponents().equals(
						feature)) {
			shape = getPrimaryShape()
					.getFigureComponentSubComponentsAreaFigure();
		} else if (org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE
				.getAbstractComponentStyle_CollapsedContent().equals(feature)) {
			shape = getPrimaryShape().getFigureComponentContentAreaFigure();
		} else if (org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE
				.getAbstractComponentStyle_CollapsedAttributes()
				.equals(feature)) {
			shape = getPrimaryShape().getFigureComponentAttributesAreaFigure();
		} else if (org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE
				.getAbstractComponentStyle_CollapsedOthers().equals(feature)) {
			shape = getPrimaryShape().getFigureComponentOthersAreaFigure();
		}

		if (shape != null) {
			shape.setVisible(!collapse);
		}
	}

	protected void handleNotificationEvent(Notification notification) {
		super.handleNotificationEvent(notification);

		if (notification.getNewValue() instanceof Boolean) {
			Object feature = notification.getFeature();
			collapse((Boolean) notification.getNewValue(), feature);
		}
		
		if(notification.getNotifier() instanceof Component
			&& notification.getFeatureID(Component.class) == FractalPackage.COMPONENT__SHARED){
			if(notification.getNewValue() != null){
				this.setBackgroundColor(ColorConstants.yellow);
			}else{
				// TODO;
			}
		}
	}

	public void refresh() {
		// TODO Auto-generated method stub
		super.refresh();
	}

	protected void refreshChildren() {
		// TODO Auto-generated method stub
		super.refreshChildren();
		
		//the only way I found to correctly update bindings figure after Fractal model merge update
		((CanonicalEditPolicy) this.getRoot().getContents()
				.getEditPolicy(EditPolicyRoles.CANONICAL_ROLE)).refresh();
	}
	
	/*
	 * Fill the component with the the given color
	 * @see org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart#setBackgroundColor(org.eclipse.swt.graphics.Color)
	 */
	protected void setBackgroundColor(Color color) {
		getPrimaryShape().setBackgroundColor(color);
	}	
	
	/*
	 * Fill the component border with the given color
	 * @see org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart#setForegroundColor(org.eclipse.swt.graphics.Color)
	 */
	protected void setForegroundColor(Color color) {
		getPrimaryShape().setForegroundColor(color);
		((ComponentBorder)this.getPrimaryShape().getBorder()).setColor(color);
	}
	// End yann hand modif
	
	protected void refreshBounds() {
		int width = ((Integer) getStructuralFeatureValue(NotationPackage.eINSTANCE.getSize_Width())).intValue();
		int height = ((Integer) getStructuralFeatureValue(NotationPackage.eINSTANCE.getSize_Height())).intValue();
		Dimension size = new Dimension(width, height);
		int x = ((Integer) getStructuralFeatureValue(NotationPackage.eINSTANCE.getLocation_X())).intValue();
		int y = ((Integer) getStructuralFeatureValue(NotationPackage.eINSTANCE.getLocation_Y())).intValue();
		Point loc = new Point(x, y);
		((GraphicalEditPart) getParent()).setLayoutConstraint(
			this,
			getFigure(),
			new Rectangle(loc, size));
	}
}
