package org.ow2.fractal.f4e.fractal.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.PrintingPreferencePage;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramPrintingPreferencePage extends PrintingPreferencePage {

	/**
	 * @generated
	 */
	public DiagramPrintingPreferencePage() {
		setPreferenceStore(FractalDiagramEditorPlugin.getInstance()
				.getPreferenceStore());
	}
}
