package org.ow2.fractal.f4e.fractal.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.CreateElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.FractalFactory;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.Role;
import org.ow2.fractal.f4e.fractal.adapter.helper.HelperAdapter;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.FractalBaseItemSemanticEditPolicy;

/**
 * @generated
 */
public class BindingCreateCommand extends CreateElementCommand {

	/**
	 * @generated
	 */
	private final EObject source;

	/**
	 * @generated
	 */
	private final EObject target;

	/**
	 * @generated
	 */
	private AbstractComponent container;

	/**
	 * @generated NOT
	 */
	public BindingCreateCommand(CreateRelationshipRequest request,
			EObject source, EObject target) {
		super(request);
		this.source = source;
		this.target = target;
		if (request.getContainmentFeature() == null) {
			setContainmentFeature(FractalPackage.eINSTANCE
					.getAbstractComponent_Bindings());
		}

		// Start yann hand modifs
		//		// Find container element for the new link.
		//		// Climb up by containment hierarchy starting from the source
		//		// and return the first element that is instance of the container class.
		//		for (EObject element = source; element != null; element = element
		//				.eContainer()) {
		//			if (element instanceof AbstractComponent) {
		//				container = (AbstractComponent) element;
		//				super.setElementToEdit(container);
		//				break;
		//			}
		//		}
		// End yann hand modifs
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (source == null && target == null) {
			return false;
		}
		if (source != null && false == source instanceof Interface) {
			return false;
		}
		if (target != null && false == target instanceof Interface) {
			return false;
		}
		if (getSource() == null) {
			return true; // link creation is in progress; source is not defined yet
		}
		// Start yann hand modifs
		// target may be null here but it's possible to check constraint
//		if (getContainer() == null) {
//			return false;
//		}
		if(getSource() == getTarget() && getSource() != null){
			return false;
		}
		// End yann modifs
		return FractalBaseItemSemanticEditPolicy.LinkConstraints
				.canCreateBinding_3001(getContainer(), getSource(), getTarget());
	}

	/**
	 * @generated NOT
	 */
	protected EObject doDefaultElementCreation() {
		
		// Start yann hand modifs
		Binding newElement = initializeNewBinding(getSource(), getTarget());
		// End yann hand modifs

		//newElement.setServerInterface(getSource());
		//newElement.setClientInterface(getTarget());
		return newElement;
	}

	/**
	 * @generated
	 */
	protected EClass getEClassToEdit() {
		return FractalPackage.eINSTANCE.getAbstractComponent();
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException(
					"Invalid arguments in create link command"); //$NON-NLS-1$
		}
		return super.doExecuteWithResult(monitor, info);
	}

	/**
	 * @generated
	 */
	protected ConfigureRequest createConfigureRequest() {
		ConfigureRequest request = super.createConfigureRequest();
		request.setParameter(CreateRelationshipRequest.SOURCE, getSource());
		request.setParameter(CreateRelationshipRequest.TARGET, getTarget());
		return request;
	}

	/**
	 * @generated
	 */
	protected void setElementToEdit(EObject element) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @generated
	 */
	protected Interface getSource() {
		return (Interface) source;
	}

	/**
	 * @generated
	 */
	protected Interface getTarget() {
		return (Interface) target;
	}

	/**
	 * @generated
	 */
	public AbstractComponent getContainer() {
		return container;
	}

	private AbstractComponent getAbstractComponentParent(
			AbstractComponent component) {
		AbstractComponent result = null;
		if (component instanceof AbstractComponent) {
			if (component.eContainer() != null
					&& component.eContainer() instanceof AbstractComponent) {
				result = (AbstractComponent) component.eContainer();
			}
		}
		return result;
	}
	
	private Binding initializeNewBinding(EObject sourceInterface,
			EObject targetInterface) {
		Binding binding = null;
		if (source instanceof Interface && target instanceof Interface) {
			AbstractComponent sourceAbstractComponent = null;
			AbstractComponent targetAbstractComponent = null;
			AbstractComponent sourceAbstractComponentParent = null;
			AbstractComponent targetAbstractComponentParent = null;

			if (sourceInterface.eContainer() instanceof AbstractComponent
					&& targetInterface.eContainer() instanceof AbstractComponent) {
				sourceAbstractComponent = (AbstractComponent) sourceInterface
						.eContainer();
				targetAbstractComponent = (AbstractComponent) targetInterface
						.eContainer();
				sourceAbstractComponentParent = getAbstractComponentParent(sourceAbstractComponent);
				targetAbstractComponentParent = getAbstractComponentParent(targetAbstractComponent);
				
				Role sourceRole = HelperAdapter.isMergedElement(sourceInterface)?
						((Interface) sourceInterface).getRole():
							((Interface) sourceInterface).getMergedRole();
				
				// If a binding is created with two interfaces that belong to the same abstract component,
				// binding can belongs to the interfaces AbstractComponent or to its parent.
				// The solution chosen to solve the problem is that a binding created by selecting first
				// the client interface will belong to the interface component, else to its parent.
				if (sourceAbstractComponent != null
						&& sourceAbstractComponent == targetAbstractComponent) {
					if (sourceRole == Role.CLIENT) {
						// If the user select the server interface as source,
						// it is treated as a internal client interface,
						// and the binding will belong to the interface parent component.
						//
						//            +----------+
						//            |          |
						//          |=|=|------|=|=|
						//            |          |
						//            +----------+   
						//
						//
						//
						container = sourceAbstractComponent;
						if(container.eContainingFeature() == FractalPackage.eINSTANCE.getAbstractComponent_MergedSubComponents()){
							return null;
						}
						binding = FractalFactory.eINSTANCE.createBinding();
						container.getBindings().add(binding);

						binding.setServerInterface(getSource());
						binding.setServer(((Interface) sourceInterface)
								.getName());
						binding.setClientInterface(getTarget());
						binding.setClient(((Interface) targetInterface)
								.getName());
					} else if (sourceRole == Role.SERVER){
						//
						//			  +-----+
						// server +-|=|     |=|-+
						//        |   +-----+   |
						//        |             |
						//        +-------------+
						//
						if (sourceAbstractComponentParent != null) {
							container = sourceAbstractComponentParent;
							if(container.eContainingFeature() == FractalPackage.eINSTANCE.getAbstractComponent_MergedSubComponents()){
								return null;
							}
							binding = FractalFactory.eINSTANCE.createBinding();
							container.getBindings().add(binding);
							
							binding.setServerInterface(getSource());
							binding.setServer(sourceAbstractComponent.getName() + "." + ((Interface) sourceInterface)
									.getName());
							binding.setClientInterface(getTarget());
							binding.setClient(targetAbstractComponent.getName() + "." + ((Interface) targetInterface)
									.getName());
						} 
					}
				} else if ((sourceAbstractComponent != null && targetAbstractComponent != null)
						&& (sourceAbstractComponent != targetAbstractComponent)
						&& (sourceAbstractComponentParent != null && targetAbstractComponentParent != null)
						&& (sourceAbstractComponentParent == targetAbstractComponentParent)) {
					// bindings between interfaces of two sub components
					//
					//		+--------------------------------+
					//    	|                                |
					//      |     +-----+         +------+   |
					//      |     |     |-|-----|-|      |   |
					//      |     +-----+         +------+   |
					//      |                                |
					//      +--------------------------------+
					//
					//
					// we don't check Role interface compatibility,
					// it should be done before.
					container = sourceAbstractComponentParent;
					if(container.eContainingFeature() == FractalPackage.eINSTANCE.getAbstractComponent_MergedSubComponents()){
						return null;
					}
					binding = FractalFactory.eINSTANCE.createBinding();
					container.getBindings().add(binding);

					if (((Interface) sourceInterface).getRole() == Role.SERVER) {
					//	binding.setServerInterface(getSource());
						binding.setServer(sourceAbstractComponent.getName() + "." + ((Interface) sourceInterface)
								.getName());
					//	binding.setClientInterface(getTarget());
						binding.setClient(targetAbstractComponent.getName() + "." + ((Interface) targetInterface)
								.getName());
					} else {
						//binding.setServerInterface(getTarget());
						binding.setServer(targetAbstractComponent.getName() + "." + ((Interface) targetInterface)
								.getName());
					//	binding.setClientInterface(getSource());
						binding.setClient(sourceAbstractComponent.getName() + "." + ((Interface) sourceInterface)
								.getName());
					}
				}else if ((sourceAbstractComponent != null && targetAbstractComponent != null)
				&& (sourceAbstractComponent != targetAbstractComponent)
				&& (sourceAbstractComponentParent != targetAbstractComponentParent)) {
				// binding between a interface of a component and a interface of a subcomponent
				//
				//      +---------------+
				//      |               |
				//      |    +----+     |
				//    |-|--|-|    |     |
				//      |    +----+     |
				//      |               | 
				//      +---------------+
				//
				if(sourceAbstractComponent == targetAbstractComponentParent){
					container = sourceAbstractComponent;
					if(container.eContainingFeature() == FractalPackage.eINSTANCE.getAbstractComponent_MergedSubComponents()){
						return null;
					}
					binding = FractalFactory.eINSTANCE.createBinding();
					container.getBindings().add(binding);
					if (((Interface) sourceInterface).getRole() == Role.SERVER &&
						((Interface) targetInterface).getRole() == Role.SERVER) {
						binding.setServerInterface(getTarget());
						binding.setServer(targetAbstractComponent.getName() + "." + ((Interface) targetInterface)
								.getName());
						binding.setClientInterface(getSource());
						binding.setClient("this." + ((Interface) sourceInterface)
								.getName());
					}else if(((Interface)sourceInterface).getRole() == Role.CLIENT &&
							((Interface) targetInterface).getRole() == Role.CLIENT ){
						binding.setServerInterface(getSource());
						binding.setServer("this." + ((Interface) sourceInterface)
								.getName());
						binding.setClientInterface(getTarget());
						binding.setClient(targetAbstractComponent.getName() + "." +  ((Interface) targetInterface)
								.getName());
					}
				}else if(targetAbstractComponent == sourceAbstractComponentParent){
					container = targetAbstractComponent;
					if(container.eContainingFeature() == FractalPackage.eINSTANCE.getAbstractComponent_MergedSubComponents()){
						return null;
					}
					binding = FractalFactory.eINSTANCE.createBinding();
					container.getBindings().add(binding);
					if (((Interface) sourceInterface).getRole() == Role.SERVER &&
							((Interface) targetInterface).getRole() == Role.SERVER) {
							binding.setServerInterface(getSource());
							binding.setServer(sourceAbstractComponent.getName() + "." +  ((Interface) sourceInterface)
									.getName());
							binding.setClientInterface(getTarget());
							binding.setClient("this." + ((Interface) targetInterface)
									.getName());
						}else if(((Interface)sourceInterface).getRole() == Role.CLIENT &&
								((Interface) targetInterface).getRole() == Role.CLIENT ){
							binding.setServerInterface(getTarget());
							binding.setServer("this." + ((Interface) targetInterface)
									.getName());
							binding.setClientInterface(getSource());
							binding.setClient(sourceAbstractComponent.getName() + "." + ((Interface) sourceInterface)
									.getName());
						}
				}
			}
			}
		}
		return binding;
	}
}
