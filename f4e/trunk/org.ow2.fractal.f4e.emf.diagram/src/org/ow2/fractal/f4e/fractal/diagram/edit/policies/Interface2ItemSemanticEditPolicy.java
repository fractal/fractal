package org.ow2.fractal.f4e.fractal.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;
import org.ow2.fractal.f4e.fractal.diagram.edit.commands.Binding2CreateCommand;
import org.ow2.fractal.f4e.fractal.diagram.edit.commands.Binding2ReorientCommand;
import org.ow2.fractal.f4e.fractal.diagram.edit.commands.BindingCreateCommand;
import org.ow2.fractal.f4e.fractal.diagram.edit.commands.BindingReorientCommand;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Binding2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.BindingEditPart;
import org.ow2.fractal.f4e.fractal.diagram.providers.FractalElementTypes;

/**
 * @generated
 */
public class Interface2ItemSemanticEditPolicy extends
		FractalBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	protected Command getDestroyElementCommand(DestroyElementRequest req) {
		CompoundCommand cc = getDestroyEdgesCommand();
		addDestroyShortcutsCommand(cc);
		cc.add(getGEFWrapper(new DestroyElementCommand(req)));
		return cc.unwrap();
	}

	/**
	 * @generated
	 */
	protected Command getCreateRelationshipCommand(CreateRelationshipRequest req) {
		Command command = req.getTarget() == null ? getStartCreateRelationshipCommand(req)
				: getCompleteCreateRelationshipCommand(req);
		return command != null ? command : super
				.getCreateRelationshipCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getStartCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (FractalElementTypes.Binding_3001 == req.getElementType()) {
			return getGEFWrapper(new BindingCreateCommand(req, req.getSource(),
					req.getTarget()));
		}
		if (FractalElementTypes.Binding_3002 == req.getElementType()) {
			return getGEFWrapper(new Binding2CreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		return null;
	}

	/**
	 * @generated
	 */
	protected Command getCompleteCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (FractalElementTypes.Binding_3001 == req.getElementType()) {
			return getGEFWrapper(new BindingCreateCommand(req, req.getSource(),
					req.getTarget()));
		}
		if (FractalElementTypes.Binding_3002 == req.getElementType()) {
			return getGEFWrapper(new Binding2CreateCommand(req,
					req.getSource(), req.getTarget()));
		}
		return null;
	}

	/**
	 * Returns command to reorient EClass based link. New link target or source
	 * should be the domain model element associated with this node.
	 * 
	 * @generated
	 */
	protected Command getReorientRelationshipCommand(
			ReorientRelationshipRequest req) {
		switch (getVisualID(req)) {
		case BindingEditPart.VISUAL_ID:
			return getGEFWrapper(new BindingReorientCommand(req));
		case Binding2EditPart.VISUAL_ID:
			return getGEFWrapper(new Binding2ReorientCommand(req));
		}
		return super.getReorientRelationshipCommand(req);
	}

	protected boolean shouldProceed(DestroyRequest destroyRequest) {
		return false;
	}
}
