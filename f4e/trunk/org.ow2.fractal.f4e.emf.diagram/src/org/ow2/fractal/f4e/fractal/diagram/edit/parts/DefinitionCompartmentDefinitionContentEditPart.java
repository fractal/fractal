package org.ow2.fractal.f4e.fractal.diagram.edit.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ListCompartmentEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CreationEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.figures.ResizableCompartmentFigure;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.CustomDragDropEditPolicy;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.FractalEditPolicies;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.ResizableCompartmentWithoutHandleEditPolicy;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.compartments.ContentCompartmentsEditPolicy;
import org.ow2.fractal.f4e.diagram.custom.figures.ResizableCompartmentWithoutScrollBarFigure;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.DefinitionCompartmentDefinitionContentCanonicalEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.DefinitionCompartmentDefinitionContentItemSemanticEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.part.Messages;

/**
 * @generated
 */
public class DefinitionCompartmentDefinitionContentEditPart extends
		ListCompartmentEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 5003;

	/**
	 * @generated
	 */
	public DefinitionCompartmentDefinitionContentEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected boolean hasModelChildrenChanged(Notification evt) {
		return false;
	}

	/**
	 * @generated
	 */
	public String getCompartmentName() {
		return Messages.DefinitionCompartmentDefinitionContentEditPart_title;
	}

	/**
	 * @generated
	 */
	public IFigure createFigure() {
		// Customize the content compartment to remove scrollbar
		// and to remove borders margin
		ResizableCompartmentFigure result = new ResizableCompartmentWithoutScrollBarFigure(getCompartmentName(), getMapMode());
		result.setTitleVisibility(false);
		result.setBorder(null);
		ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
		result.getContentPane().setLayoutManager(layout);
		return result;
	}

	/**
	 * @generated NOT
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		// Custom policy which hide the handle of the colapsible compartment.
		installEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE,
				new ResizableCompartmentWithoutHandleEditPolicy());
		// End yann hand modifs.
		installEditPolicy(
				EditPolicyRoles.SEMANTIC_ROLE,
				new DefinitionCompartmentDefinitionContentItemSemanticEditPolicy());
		installEditPolicy(EditPolicyRoles.CREATION_ROLE,
				new CreationEditPolicy());
		// Start yann hand modifs
		installEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE,
				new CustomDragDropEditPolicy(){
					// we can only move/d&d attributes controller
					protected boolean canBeDropped(EditPart dropedObject) {
						return dropedObject instanceof ContentEditPart;
					}
		});
		
		// Policy to handle menu bar actions to show/hide compartments
		installEditPolicy(FractalEditPolicies.CONTENT_COMPARTMENTS_ROLE,
				new ContentCompartmentsEditPolicy());
		// End yann hand modifs
		installEditPolicy(EditPolicyRoles.CANONICAL_ROLE,
				new DefinitionCompartmentDefinitionContentCanonicalEditPolicy());
	}

	/**
	 * @generated
	 */
	protected void setRatio(Double ratio) {
		if (getFigure().getParent().getLayoutManager() instanceof ConstrainedToolbarLayout) {
			super.setRatio(ratio);
		}
	}
	
	protected void addChild(EditPart child, int index) {
		// The content compartment can have only two child
		// the content or the mergedContent.
		// Only one of them is visible, the content.
		if(getChildren().size()==1){
			super.addChild(child, index);
			if(getChildren().get(0) instanceof Content2EditPart){
				((Content2EditPart)getChildren().get(0)).getFigure().setVisible(false);
			}else if(getChildren().get(1) instanceof Content2EditPart){
				((Content2EditPart)getChildren().get(1)).getFigure().setVisible(false);
			}
		}else if(getChildren().size()==0){
			super.addChild(child, index);
		}
	}

	protected void removeChild(EditPart child) {
		super.removeChild(child);
		
		if(getChildren().size()==1){
			if(getChildren().get(0) instanceof Content2EditPart){
				((Content2EditPart)getChildren().get(0)).getFigure().setVisible(true);
			}
		}
	}


}
