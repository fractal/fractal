package org.ow2.fractal.f4e.fractal.diagram.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.modelingassistant.ModelingAssistantProvider;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Attribute2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentAttributesEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentSubComponentsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentAttributesEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentSubComponentsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Content2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionAttributesEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionSubComponentsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Interface2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.InterfaceEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.SystemEditPart;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalDiagramEditorPlugin;
import org.ow2.fractal.f4e.fractal.diagram.part.Messages;

/**
 * @generated
 */
public class FractalModelingAssistantProvider extends ModelingAssistantProvider {

	/**
	 * @generated NOT
	 */
	public List getTypesForPopupBar(IAdaptable host) {
		IGraphicalEditPart editPart = (IGraphicalEditPart) host
				.getAdapter(IGraphicalEditPart.class);
		if (editPart instanceof DefinitionEditPart) {
			List types = new ArrayList();
			// Start yann hand modifs.
			types.add(FractalElementTypes.Interface_2009);
			types.add(FractalElementTypes.Component_2002);
			//types.add(FractalElementTypes.Interface_2010);
			//types.add(FractalElementTypes.DefinitionCall_2001);
			types.add(FractalElementTypes.AttributesController_2004);
			types.add(FractalElementTypes.Attribute_2005);
			types.add(FractalElementTypes.Content_2003);
			//types.add(FractalElementTypes.Content_2012);
			types.add(FractalElementTypes.VirtualNode_2008);
			types.add(FractalElementTypes.Controller_2007);
			//types.add(FractalElementTypes.VirtualNode_2016);
			//types.add(FractalElementTypes.Controller_2015);
			// End yann hand modifs.
			return types;
		}
		if (editPart instanceof ComponentEditPart) {
			List types = new ArrayList();
			// Start yann hand modifs.
			types.add(FractalElementTypes.Interface_2009);
			types.add(FractalElementTypes.Component_2002);
			//types.add(FractalElementTypes.Interface_2010);
			//types.add(FractalElementTypes.DefinitionCall_2001);
			types.add(FractalElementTypes.AttributesController_2004);
			types.add(FractalElementTypes.Attribute_2005);
			types.add(FractalElementTypes.Content_2003);
			//types.add(FractalElementTypes.Content_2012);
			types.add(FractalElementTypes.Controller_2007);
			types.add(FractalElementTypes.VirtualNode_2008);
			//types.add(FractalElementTypes.Controller_2015);
			//types.add(FractalElementTypes.VirtualNode_2016);
			// End yann hand modifs.
			return types;
		}
		if (editPart instanceof AttributesControllerEditPart) {
			List types = new ArrayList();
			// Start yann hand modifs.
			types.add(FractalElementTypes.Attribute_2005);
			//types.add(FractalElementTypes.Attribute_2006);
			// End yann hand modifs.
			return types;
		}
		if (editPart instanceof Content2EditPart) {
			List types = new ArrayList();
			// Start yann hand modifs.
			//types.add(FractalElementTypes.Interface_2010);
			//types.add(FractalElementTypes.Content_2012);
			//types.add(FractalElementTypes.Controller_2015);
			//types.add(FractalElementTypes.VirtualNode_2016);
			// End yann hand modifs.
			return types;
		}
		if (editPart instanceof Attribute2EditPart) {
			List types = new ArrayList();
			// Start yann hand modifs.
			//types.add(FractalElementTypes.Attribute_2014);
			// End yann hand modifs.
			return types;
		}
		if (editPart instanceof DefinitionCompartmentDefinitionSubComponentsEditPart) {
			List types = new ArrayList();
			//types.add(FractalElementTypes.Component_2002);
			// Start yann hand modifs.
			//types.add(FractalElementTypes.Component_2011);
			// End yann hand modifs.
			return types;
		}
		if (editPart instanceof DefinitionCompartmentDefinitionAttributesEditPart) {
			List types = new ArrayList();
			// Start yann hand modifs.
			types.add(FractalElementTypes.AttributesController_2004);
			//types.add(FractalElementTypes.AttributesController_2013);
			// End yann hand modifs.
			return types;
		}
		if (editPart instanceof ComponentCompartmentComponentSubComponentsEditPart) {
			List types = new ArrayList();
			// Start yann hand modifs.
			//types.add(FractalElementTypes.Component_2002);
			//types.add(FractalElementTypes.Component_2011);
			// End yann hand modifs.
			return types;
		}
		if (editPart instanceof ComponentCompartmentComponentAttributesEditPart) {
			List types = new ArrayList();
			// Start yann hand modifs.
			types.add(FractalElementTypes.AttributesController_2004);
			//types.add(FractalElementTypes.AttributesController_2013);
			// End yann hand modifs.
			return types;
		}
		if (editPart instanceof ComponentMergedCompartmentComponentSubComponentsEditPart) {
			List types = new ArrayList();
			// Start yann hand modifs.
			//types.add(FractalElementTypes.Component_2011);
			// End yann hand modifs.
			return types;
		}
		if (editPart instanceof ComponentMergedCompartmentComponentAttributesEditPart) {
			List types = new ArrayList();
			// Start yann hand modifs.
			// types.add(FractalElementTypes.AttributesController_2013);
			// End yann hand modifs.
			return types;
		}
		if (editPart instanceof SystemEditPart) {
			List types = new ArrayList();
			// Start yann hand modifs.
			// types.add(FractalElementTypes.Definition_1001);
			// End yann hand modifs.
			return types;
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated NOT
	 */
	public List getRelTypesOnSource(IAdaptable source) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof InterfaceEditPart) {
			List types = new ArrayList();
			types.add(FractalElementTypes.Binding_3001);
		//	types.add(FractalElementTypes.Binding_3002);
			return types;
		}
		if (sourceEditPart instanceof Interface2EditPart) {
			List types = new ArrayList();
			types.add(FractalElementTypes.Binding_3001);
		//	types.add(FractalElementTypes.Binding_3002);
			return types;
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated NOT
	 */
	public List getRelTypesOnTarget(IAdaptable target) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		if (targetEditPart instanceof InterfaceEditPart) {
			List types = new ArrayList();
			types.add(FractalElementTypes.Binding_3001);
		//	types.add(FractalElementTypes.Binding_3002);
			return types;
		}
		if (targetEditPart instanceof Interface2EditPart) {
			List types = new ArrayList();
			types.add(FractalElementTypes.Binding_3001);
		//	types.add(FractalElementTypes.Binding_3002);
			return types;
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated NOT
	 */
	public List getRelTypesOnSourceAndTarget(IAdaptable source,
			IAdaptable target) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof InterfaceEditPart) {
			List types = new ArrayList();
			if (targetEditPart instanceof InterfaceEditPart) {
				types.add(FractalElementTypes.Binding_3001);
			}
			if (targetEditPart instanceof Interface2EditPart) {
				types.add(FractalElementTypes.Binding_3001);
			}
//			if (targetEditPart instanceof InterfaceEditPart) {
//				types.add(FractalElementTypes.Binding_3002);
//			}
//			if (targetEditPart instanceof Interface2EditPart) {
//				types.add(FractalElementTypes.Binding_3002);
//			}
			return types;
		}
		if (sourceEditPart instanceof Interface2EditPart) {
			List types = new ArrayList();
			if (targetEditPart instanceof InterfaceEditPart) {
				types.add(FractalElementTypes.Binding_3001);
			}
			if (targetEditPart instanceof Interface2EditPart) {
				types.add(FractalElementTypes.Binding_3001);
			}
//			if (targetEditPart instanceof InterfaceEditPart) {
//				types.add(FractalElementTypes.Binding_3002);
//			}
//			if (targetEditPart instanceof Interface2EditPart) {
//				types.add(FractalElementTypes.Binding_3002);
//			}
			return types;
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getTypesForSource(IAdaptable target,
			IElementType relationshipType) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		if (targetEditPart instanceof InterfaceEditPart) {
			List types = new ArrayList();
			if (relationshipType == FractalElementTypes.Binding_3001) {
				types.add(FractalElementTypes.Interface_2009);
			}
			if (relationshipType == FractalElementTypes.Binding_3001) {
				types.add(FractalElementTypes.Interface_2010);
			}
//			if (relationshipType == FractalElementTypes.Binding_3002) {
//				types.add(FractalElementTypes.Interface_2009);
//			}
//			if (relationshipType == FractalElementTypes.Binding_3002) {
//				types.add(FractalElementTypes.Interface_2010);
//			}
			return types;
		}
		if (targetEditPart instanceof Interface2EditPart) {
			List types = new ArrayList();
			if (relationshipType == FractalElementTypes.Binding_3001) {
				types.add(FractalElementTypes.Interface_2009);
			}
			if (relationshipType == FractalElementTypes.Binding_3001) {
				types.add(FractalElementTypes.Interface_2010);
			}
//			if (relationshipType == FractalElementTypes.Binding_3002) {
//				types.add(FractalElementTypes.Interface_2009);
//			}
//			if (relationshipType == FractalElementTypes.Binding_3002) {
//				types.add(FractalElementTypes.Interface_2010);
//			}
			return types;
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated NOT
	 */
	public List getTypesForTarget(IAdaptable source,
			IElementType relationshipType) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof InterfaceEditPart) {
			List types = new ArrayList();
			if (relationshipType == FractalElementTypes.Binding_3001) {
				types.add(FractalElementTypes.Interface_2009);
			}
			if (relationshipType == FractalElementTypes.Binding_3001) {
				types.add(FractalElementTypes.Interface_2010);
			}
			// Start yann hand modifs
//			if (relationshipType == FractalElementTypes.Binding_3002) {
//				types.add(FractalElementTypes.Interface_2009);
//			}
//			if (relationshipType == FractalElementTypes.Binding_3002) {
//				types.add(FractalElementTypes.Interface_2010);
//			}
			// End yann hand modifs
			return types;
		}
		if (sourceEditPart instanceof Interface2EditPart) {
			List types = new ArrayList();
			if (relationshipType == FractalElementTypes.Binding_3001) {
				types.add(FractalElementTypes.Interface_2009);
			}
			if (relationshipType == FractalElementTypes.Binding_3001) {
				types.add(FractalElementTypes.Interface_2010);
			}
			// Start yann hand modifs
//			if (relationshipType == FractalElementTypes.Binding_3002) {
//				types.add(FractalElementTypes.Interface_2009);
//			}
//			if (relationshipType == FractalElementTypes.Binding_3002) {
//				types.add(FractalElementTypes.Interface_2010);
//			}
			// End yann hand modifs.
			return types;
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public EObject selectExistingElementForSource(IAdaptable target,
			IElementType relationshipType) {
		return selectExistingElement(target, getTypesForSource(target,
				relationshipType));
	}

	/**
	 * @generated
	 */
	public EObject selectExistingElementForTarget(IAdaptable source,
			IElementType relationshipType) {
		return selectExistingElement(source, getTypesForTarget(source,
				relationshipType));
	}

	/**
	 * @generated
	 */
	protected EObject selectExistingElement(IAdaptable host, Collection types) {
		if (types.isEmpty()) {
			return null;
		}
		IGraphicalEditPart editPart = (IGraphicalEditPart) host
				.getAdapter(IGraphicalEditPart.class);
		if (editPart == null) {
			return null;
		}
		Diagram diagram = (Diagram) editPart.getRoot().getContents().getModel();
		Collection elements = new HashSet();
		for (Iterator it = diagram.getElement().eAllContents(); it.hasNext();) {
			EObject element = (EObject) it.next();
			if (isApplicableElement(element, types)) {
				elements.add(element);
			}
		}
		if (elements.isEmpty()) {
			return null;
		}
		return selectElement((EObject[]) elements.toArray(new EObject[elements
				.size()]));
	}

	/**
	 * @generated
	 */
	protected boolean isApplicableElement(EObject element, Collection types) {
		IElementType type = ElementTypeRegistry.getInstance().getElementType(
				element);
		return types.contains(type);
	}

	/**
	 * @generated
	 */
	protected EObject selectElement(EObject[] elements) {
		Shell shell = Display.getCurrent().getActiveShell();
		ILabelProvider labelProvider = new AdapterFactoryLabelProvider(
				FractalDiagramEditorPlugin.getInstance()
						.getItemProvidersAdapterFactory());
		ElementListSelectionDialog dialog = new ElementListSelectionDialog(
				shell, labelProvider);
		dialog.setMessage(Messages.FractalModelingAssistantProviderMessage);
		dialog.setTitle(Messages.FractalModelingAssistantProviderTitle);
		dialog.setMultipleSelection(false);
		dialog.setElements(elements);
		EObject selected = null;
		if (dialog.open() == Window.OK) {
			selected = (EObject) dialog.getFirstResult();
		}
		return selected;
	}
}
