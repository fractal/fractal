package org.ow2.fractal.f4e.fractal.diagram.edit.parts;

import org.eclipse.draw2d.Border;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ComponentEditPolicy;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gef.requests.GroupRequest;
import org.eclipse.gmf.runtime.diagram.core.edithelpers.CreateElementRequestAdapter;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IBorderItemEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CreationEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.DragDropEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewAndElementRequest;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Color;
import org.ow2.fractal.f4e.diagram.custom.edit.parts.AbstractComponentEditPart;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.InterfaceBorderItemSelectionEditPolicy;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.MergedComponentResizableEditPolicy;
import org.ow2.fractal.f4e.diagram.custom.figures.AbstractComponentNameWrappingLabel;
import org.ow2.fractal.f4e.diagram.custom.figures.AbstractComponentShape;
import org.ow2.fractal.f4e.diagram.custom.figures.AlphaDropShadowBorder;
import org.ow2.fractal.f4e.diagram.custom.figures.ComponentBorder;
import org.ow2.fractal.f4e.diagram.custom.figures.ContainerShape;
import org.ow2.fractal.f4e.diagram.custom.figures.IFractalShape;
import org.ow2.fractal.f4e.diagram.custom.layouts.AutosizeContainerLayout;
import org.ow2.fractal.f4e.diagram.custom.layouts.InterfaceBorderItemLocator;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.Component2CanonicalEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.Component2ItemSemanticEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalVisualIDRegistry;
import org.ow2.fractal.f4e.fractal.diagram.providers.FractalElementTypes;
import org.ow2.fractal.f4e.fractal.notification.IFractalNotification;

/**
 * EditPart for merged Components.
 * 
 * @generated NOT
 */
public class Component2EditPart extends AbstractComponentEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 2011;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public Component2EditPart(View view) {
		super(view);
	}

	/**
	 * @generated NOT
	 */
	protected void createDefaultEditPolicies() {
		installEditPolicy(EditPolicyRoles.CREATION_ROLE,
				new CreationEditPolicy() {
					public Command getCommand(Request request) {
						
						if (understandsRequest(request)) {
							if (request instanceof CreateViewAndElementRequest) {
								CreateElementRequestAdapter adapter = ((CreateViewAndElementRequest) request)
										.getViewAndElementDescriptor()
										.getCreateElementRequestAdapter();
								IElementType type = (IElementType) adapter
										.getAdapter(IElementType.class);
								if (type == FractalElementTypes.Content_2012) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(ComponentMergedCompartmentComponentContentEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								if (type == FractalElementTypes.Controller_2015) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(ComponentMergedCompartmentComponentOthersEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
								if (type == FractalElementTypes.VirtualNode_2016) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(ComponentMergedCompartmentComponentOthersEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
							}
							return super.getCommand(request);
						}
						return null;
					}
				});
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new Component2ItemSemanticEditPolicy());
		
		installEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE,
				new DragDropEditPolicy());
		
		installEditPolicy(EditPolicyRoles.CANONICAL_ROLE,
				new Component2CanonicalEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
	
		// Start yann hand modifs
		// We can't delete a merged component
		 installEditPolicy(EditPolicy.COMPONENT_ROLE,
				  new ComponentEditPolicy(){
				    protected Command getDeleteCommand(GroupRequest request) {
				      return null;
				    }
				  });
		 
		 
		removeEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE);
		 
		// Remove the connection handles
		removeEditPolicy(EditPolicyRoles.CONNECTION_HANDLES_ROLE);
		
		// End yann hand modifs
		
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated NOT
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		LayoutEditPolicy lep = new LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				if (child instanceof IBorderItemEditPart) {
					// Start Hand Modif Yann
					return new InterfaceBorderItemSelectionEditPolicy();
					// End Hand Modif Yann
				}
				EditPolicy result = child
						.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated
	 */
	protected IFigure createNodeShape() {
		MergedComponentFigure figure = new MergedComponentFigure();
		return primaryShape = figure;
	}

	/**
	 * @generated
	 */
	public MergedComponentFigure getPrimaryShape() {
		return (MergedComponentFigure) primaryShape;
	}

	/**
	 * @generated NOT
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		// Start yann hand modif
		org.ow2.fractal.f4e.notation.AbstractComponentStyle style = (org.ow2.fractal.f4e.notation.AbstractComponentStyle) this
				.getNotationView().getStyle(
						org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE
								.getAbstractComponentStyle());
		// End yann hand modif
		
		if (childEditPart instanceof ComponentName2EditPart) {
			((ComponentName2EditPart) childEditPart).setLabel(getPrimaryShape()
					.getFigureLabelMergedComponentName());
			return true;
		}
		if (childEditPart instanceof ComponentMergedCompartmentComponentSubComponentsEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureMergedComponentSubComponentsAreaFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane
					.add(((ComponentMergedCompartmentComponentSubComponentsEditPart) childEditPart)
							.getFigure());
			// Start yann hand modifs.
			pane.setVisible(style != null ? !style.getCollapsedSubComponents()
							: true);
			//End yann hand modifs.
			return true;
		}
		if (childEditPart instanceof ComponentMergedCompartmentComponentContentEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureMergedComponentContentAreaFigure();
			if (pane.getLayoutManager() == null) {
				AutosizeContainerLayout layout = new AutosizeContainerLayout();
				pane.setLayoutManager(layout);
			}
			pane
					.add(((ComponentMergedCompartmentComponentContentEditPart) childEditPart)
							.getFigure());
			// Start yann hand modifs.
			pane.setVisible(style != null ? !style.getCollapsedContent()
							: true);
			//End yann hand modifs.
			return true;
		}
		if (childEditPart instanceof ComponentMergedCompartmentComponentAttributesEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureMergedComponentAttributesAreaFigure();
			
			if (pane.getLayoutManager() == null) {
				AutosizeContainerLayout layout = new AutosizeContainerLayout();
				pane.setLayoutManager(layout);
			}
			
			pane
					.add(((ComponentMergedCompartmentComponentAttributesEditPart) childEditPart)
							.getFigure());
			// Start yann hand modifs.
			pane.setVisible(style != null ? !style.getCollapsedAttributes()
							: true);
			//End yann hand modifs.
			return true;
		}
		if (childEditPart instanceof ComponentMergedCompartmentComponentOthersEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureMergedComponentOthersAreaFigure();
			if (pane.getLayoutManager() == null) {
				AutosizeContainerLayout layout = new AutosizeContainerLayout();
				
				pane.setLayoutManager(layout);
			}
			pane
					.add(((ComponentMergedCompartmentComponentOthersEditPart) childEditPart)
							.getFigure());
			// Start yann hand modifs.
			pane.setVisible(style != null ? !style.getCollapsedOthers()
							: true);
			//End yann hand modifs.
			return true;
		}
		if (childEditPart instanceof Interface2EditPart) {
			// Start yann hand modifs.
			InterfaceBorderItemLocator locator = new InterfaceBorderItemLocator((Interface2EditPart)childEditPart);
			// End yann hand modifs.
			getBorderedFigure().getBorderItemContainer().add(
					((Interface2EditPart) childEditPart).getFigure(), locator);
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {

		if (childEditPart instanceof ComponentMergedCompartmentComponentSubComponentsEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureMergedComponentSubComponentsAreaFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane
					.remove(((ComponentMergedCompartmentComponentSubComponentsEditPart) childEditPart)
							.getFigure());
			return true;
		}
		if (childEditPart instanceof ComponentMergedCompartmentComponentContentEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureMergedComponentContentAreaFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane
					.remove(((ComponentMergedCompartmentComponentContentEditPart) childEditPart)
							.getFigure());
			return true;
		}
		if (childEditPart instanceof ComponentMergedCompartmentComponentAttributesEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureMergedComponentAttributesAreaFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane
					.remove(((ComponentMergedCompartmentComponentAttributesEditPart) childEditPart)
							.getFigure());
			return true;
		}
		if (childEditPart instanceof ComponentMergedCompartmentComponentOthersEditPart) {
			IFigure pane = getPrimaryShape()
					.getFigureMergedComponentOthersAreaFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane
					.remove(((ComponentMergedCompartmentComponentOthersEditPart) childEditPart)
							.getFigure());
			return true;
		}
		if (childEditPart instanceof Interface2EditPart) {
			getBorderedFigure().getBorderItemContainer().remove(
					((Interface2EditPart) childEditPart).getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {

		if (editPart instanceof ComponentMergedCompartmentComponentSubComponentsEditPart) {
			return getPrimaryShape()
					.getFigureMergedComponentSubComponentsAreaFigure();
		}
		if (editPart instanceof ComponentMergedCompartmentComponentContentEditPart) {
			return getPrimaryShape()
					.getFigureMergedComponentContentAreaFigure();
		}
		if (editPart instanceof ComponentMergedCompartmentComponentAttributesEditPart) {
			return getPrimaryShape()
					.getFigureMergedComponentAttributesAreaFigure();
		}
		if (editPart instanceof ComponentMergedCompartmentComponentOthersEditPart) {
			return getPrimaryShape().getFigureMergedComponentOthersAreaFigure();
		}
		if (editPart instanceof Interface2EditPart) {
			return getBorderedFigure().getBorderItemContainer();
		}
		return super.getContentPaneFor(editPart);
	}

	/**
	 * @generated NOT
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(getMapMode()
				.DPtoLP(40), getMapMode().DPtoLP(40));
		
		// Start yann hand modifs
		AlphaDropShadowBorder shadowBorder = new AlphaDropShadowBorder();
		shadowBorder.setShouldDrawDropShadow(true);
		result.setBorder(shadowBorder);
		//End yann hand modifs
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createMainFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane.
	 * Respects layout one may have set for generated figure.
	 * @param nodeShape instance of generated figure class
	 * @generated NOT
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			// Start Yann dans modifs
			//layout.setSpacing(getMapMode().DPtoLP(5));
			// End Yann dans modifs
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(FractalVisualIDRegistry
				.getType(ComponentName2EditPart.VISUAL_ID));
	}

	/**
	 * @generated NOT
	 */
	public class MergedComponentFigure extends AbstractComponentShape {

		/**
		 * @generated
		 */
		private AbstractComponentNameWrappingLabel fFigureLabelMergedComponentName;
		/**
		 * @generated
		 */
		private AbstractComponentShape fFigureMergedComponentFigure;
		/**
		 * @generated
		 */
		private ContainerShape fFigureMergedComponentExtendsAreaFigure;
		/**
		 * @generated
		 */
		private ContainerShape fFigureMergedComponentSubComponentsAreaFigure;
		/**
		 * @generated
		 */
		private ContainerShape fFigureMergedComponentAttributesAreaFigure;
		/**
		 * @generated
		 */
		private ContainerShape fFigureMergedComponentOthersAreaFigure;
		/**
		 * @generated
		 */
		private ContainerShape fFigureMergedComponentContentAreaFigure;

		/**
		 * @generated
		 */
		public MergedComponentFigure() {

			this.setBorder(createBorder0());
			createContents();
		}

		/**
		 * @generated NOT
		 */
		private void createContents() {

			fFigureLabelMergedComponentName = new AbstractComponentNameWrappingLabel();

			this.add(fFigureLabelMergedComponentName);

			fFigureMergedComponentSubComponentsAreaFigure = new ContainerShape(false);

			this.add(fFigureMergedComponentSubComponentsAreaFigure);

			fFigureMergedComponentContentAreaFigure = new ContainerShape(false);
		
			this.add(fFigureMergedComponentContentAreaFigure);

			fFigureMergedComponentAttributesAreaFigure = new ContainerShape(false);

			this.add(fFigureMergedComponentAttributesAreaFigure);

			fFigureMergedComponentOthersAreaFigure = new ContainerShape(false);

			this.add(fFigureMergedComponentOthersAreaFigure);
		}
		
		/**
		 * @generated
		 */
		private Border createBorder0() {
			ComponentBorder result = new ComponentBorder();

			return result;
		}
		
		/**
		 * @generated
		 */
		private boolean myUseLocalCoordinates = false;

		/**
		 * @generated
		 */
		protected boolean useLocalCoordinates() {
			return myUseLocalCoordinates;
		}

		/**
		 * @generated
		 */
		protected void setUseLocalCoordinates(boolean useLocalCoordinates) {
			myUseLocalCoordinates = useLocalCoordinates;
		}

		/**
		 * @generated
		 */
		public AbstractComponentNameWrappingLabel getFigureLabelMergedComponentName() {
			return fFigureLabelMergedComponentName;
		}

		/**
		 * @generated
		 */
		public AbstractComponentShape getFigureMergedComponentFigure() {
			return fFigureMergedComponentFigure;
		}

		/**
		 * @generated
		 */
		public ContainerShape getFigureMergedComponentSubComponentsAreaFigure() {
			return fFigureMergedComponentSubComponentsAreaFigure;
		}

		/**
		 * @generated
		 */
		public ContainerShape getFigureMergedComponentAttributesAreaFigure() {
			return fFigureMergedComponentAttributesAreaFigure;
		}

		/**
		 * @generated
		 */
		public ContainerShape getFigureMergedComponentOthersAreaFigure() {
			return fFigureMergedComponentOthersAreaFigure;
		}

		/**
		 * @generated
		 */
		public ContainerShape getFigureMergedComponentContentAreaFigure() {
			return fFigureMergedComponentContentAreaFigure;
		}

	}

	// Start yann hand modif
	public EditPolicy getPrimaryDragEditPolicy() {
		return new MergedComponentResizableEditPolicy();
	}

	public void collapse(boolean collapse, Object feature) {
		EditPart editPart = null;
		ContainerShape shape = null;
		if (org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE
				.getAbstractComponentStyle_CollapsedSubComponents().equals(
						feature)) {
			shape = getPrimaryShape()
					.getFigureMergedComponentSubComponentsAreaFigure();
		} else if (org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE
				.getAbstractComponentStyle_CollapsedContent().equals(feature)) {
			shape = getPrimaryShape().getFigureMergedComponentContentAreaFigure();
		} else if (org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE
				.getAbstractComponentStyle_CollapsedAttributes()
				.equals(feature)) {
			shape = getPrimaryShape().getFigureMergedComponentAttributesAreaFigure();
		} else if (org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE
				.getAbstractComponentStyle_CollapsedOthers().equals(feature)) {
			shape = getPrimaryShape().getFigureMergedComponentOthersAreaFigure();
		}

		if (shape != null) {
			shape.setVisible(!collapse);
		}
	}

	protected void handleNotificationEvent(Notification notification) {
		super.handleNotificationEvent(notification);

		if (notification.getNewValue() instanceof Boolean) {
			Object feature = notification.getFeature();
			collapse((Boolean) notification.getNewValue(), feature);
		}
		
		if(notification.getEventType() == IFractalNotification.SET_IS_SHARED_COMPONENT &&
			notification.getNewValue() instanceof Boolean){
			EObject object = ((View)Component2EditPart.this.getModel()).getElement();
			if(object != null && object instanceof Component ){
				if(notification.getNewValue() instanceof Boolean && (Boolean)notification.getNewValue() == true){
					setForegroundColor(org.eclipse.draw2d.ColorConstants.yellow);
				}else{
					setForegroundColor(IFractalShape.GRAY_FOR_MERGED_ELEMENT);
				}
			}
		}
	}
	
	protected void setBackgroundColor(Color color) {
		getPrimaryShape().setBackgroundColor(color);
	}	

	protected void setForegroundColor(Color color) {
		getPrimaryShape().setForegroundColor(color);
		((ComponentBorder)this.getPrimaryShape().getBorder()).setColor(color);
	}
	
	protected void refreshForegroundColor() {
		//We set the border color in yellow if the component is a merged one
		// and only if the user hasn't change the border color else
		// we keep the user color
		EObject object = ((View)Component2EditPart.this.getModel()).getElement();
		if(object != null && object instanceof Component &&
				getPrimaryShape().getBackgroundColor().equals(IFractalShape.GRAY_FOR_MERGED_ELEMENT)){
			if(((Component)object).getHelper().isShared() == true){
				setForegroundColor(org.eclipse.draw2d.ColorConstants.yellow);
			}
		}else{
			super.refreshForegroundColor();
		}
	}
	// End yann hand modif
}
