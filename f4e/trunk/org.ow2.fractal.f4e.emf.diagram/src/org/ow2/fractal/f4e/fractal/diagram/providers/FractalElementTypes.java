package org.ow2.fractal.f4e.fractal.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalDiagramEditorPlugin;

/**
 * @generated
 */
public class FractalElementTypes extends ElementInitializers {

	/**
	 * @generated
	 */
	private FractalElementTypes() {
	}

	/**
	 * @generated
	 */
	private static Map elements;

	/**
	 * @generated
	 */
	private static ImageRegistry imageRegistry;

	/**
	 * @generated
	 */
	private static Set KNOWN_ELEMENT_TYPES;

	/**
	 * @generated
	 */
	public static final IElementType System_79 = getElementType("org.ow2.fractal.f4e.emf.diagram.System_79"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Definition_1001 = getElementType("org.ow2.fractal.f4e.emf.diagram.Definition_1001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType DefinitionCall_2001 = getElementType("org.ow2.fractal.f4e.emf.diagram.DefinitionCall_2001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Component_2002 = getElementType("org.ow2.fractal.f4e.emf.diagram.Component_2002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Content_2003 = getElementType("org.ow2.fractal.f4e.emf.diagram.Content_2003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType AttributesController_2004 = getElementType("org.ow2.fractal.f4e.emf.diagram.AttributesController_2004"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Attribute_2005 = getElementType("org.ow2.fractal.f4e.emf.diagram.Attribute_2005"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Attribute_2006 = getElementType("org.ow2.fractal.f4e.emf.diagram.Attribute_2006"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType Controller_2007 = getElementType("org.ow2.fractal.f4e.emf.diagram.Controller_2007"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType VirtualNode_2008 = getElementType("org.ow2.fractal.f4e.emf.diagram.VirtualNode_2008"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType Interface_2009 = getElementType("org.ow2.fractal.f4e.emf.diagram.Interface_2009"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Interface_2010 = getElementType("org.ow2.fractal.f4e.emf.diagram.Interface_2010"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType Component_2011 = getElementType("org.ow2.fractal.f4e.emf.diagram.Component_2011"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType Content_2012 = getElementType("org.ow2.fractal.f4e.emf.diagram.Content_2012"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType AttributesController_2013 = getElementType("org.ow2.fractal.f4e.emf.diagram.AttributesController_2013"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType Attribute_2014 = getElementType("org.ow2.fractal.f4e.emf.diagram.Attribute_2014"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType Controller_2015 = getElementType("org.ow2.fractal.f4e.emf.diagram.Controller_2015"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType VirtualNode_2016 = getElementType("org.ow2.fractal.f4e.emf.diagram.VirtualNode_2016"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType Binding_3001 = getElementType("org.ow2.fractal.f4e.emf.diagram.Binding_3001"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType Binding_3002 = getElementType("org.ow2.fractal.f4e.emf.diagram.Binding_3002"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	private static ImageRegistry getImageRegistry() {
		if (imageRegistry == null) {
			imageRegistry = new ImageRegistry();
		}
		return imageRegistry;
	}

	/**
	 * @generated
	 */
	private static String getImageRegistryKey(ENamedElement element) {
		return element.getName();
	}

	/**
	 * @generated
	 */
	private static ImageDescriptor getProvidedImageDescriptor(
			ENamedElement element) {
		if (element instanceof EStructuralFeature) {
			EStructuralFeature feature = ((EStructuralFeature) element);
			EClass eContainingClass = feature.getEContainingClass();
			EClassifier eType = feature.getEType();
			if (eContainingClass != null && !eContainingClass.isAbstract()) {
				element = eContainingClass;
			} else if (eType instanceof EClass
					&& !((EClass) eType).isAbstract()) {
				element = eType;
			}
		}
		if (element instanceof EClass) {
			EClass eClass = (EClass) element;
			if (!eClass.isAbstract()) {
				return FractalDiagramEditorPlugin.getInstance()
						.getItemImageDescriptor(
								eClass.getEPackage().getEFactoryInstance()
										.create(eClass));
			}
		}
		// TODO : support structural features
		return null;
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		String key = getImageRegistryKey(element);
		ImageDescriptor imageDescriptor = getImageRegistry().getDescriptor(key);
		if (imageDescriptor == null) {
			imageDescriptor = getProvidedImageDescriptor(element);
			if (imageDescriptor == null) {
				imageDescriptor = ImageDescriptor.getMissingImageDescriptor();
			}
			getImageRegistry().put(key, imageDescriptor);
		}
		return imageDescriptor;
	}

	/**
	 * @generated
	 */
	public static Image getImage(ENamedElement element) {
		String key = getImageRegistryKey(element);
		Image image = getImageRegistry().get(key);
		
		if (image == null) {
			ImageDescriptor imageDescriptor = getProvidedImageDescriptor(element);
			if (imageDescriptor == null) {
				imageDescriptor = ImageDescriptor.getMissingImageDescriptor();
			}
			getImageRegistry().put(key, imageDescriptor);
			image = getImageRegistry().get(key);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		ENamedElement element = getElement(hint);
		if (element == null) {
			return null;
		}
		return getImageDescriptor(element);
	}

	/**
	 * @generated
	 */
	public static Image getImage(IAdaptable hint) {
		ENamedElement element = getElement(hint);
		if (element == null) {
			return null;
		}
		return getImage(element);
	}

	/**
	 * Returns 'type' of the ecore object associated with the hint.
	 * 
	 * @generated
	 */
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap();

			elements.put(System_79, FractalPackage.eINSTANCE.getSystem());

			elements.put(Definition_1001, FractalPackage.eINSTANCE
					.getDefinition());

			elements.put(DefinitionCall_2001, FractalPackage.eINSTANCE
					.getDefinitionCall());

			elements.put(Component_2002, FractalPackage.eINSTANCE
					.getComponent());

			elements.put(Content_2003, FractalPackage.eINSTANCE.getContent());

			elements.put(AttributesController_2004, FractalPackage.eINSTANCE
					.getAttributesController());

			elements.put(Attribute_2005, FractalPackage.eINSTANCE
					.getAttribute());

			elements.put(Attribute_2006, FractalPackage.eINSTANCE
					.getAttribute());

			elements.put(Controller_2007, FractalPackage.eINSTANCE
					.getController());

			elements.put(VirtualNode_2008, FractalPackage.eINSTANCE
					.getVirtualNode());

			elements.put(Interface_2009, FractalPackage.eINSTANCE
					.getInterface());

			elements.put(Interface_2010, FractalPackage.eINSTANCE
					.getInterface());

			elements.put(Component_2011, FractalPackage.eINSTANCE
					.getComponent());

			elements.put(Content_2012, FractalPackage.eINSTANCE.getContent());

			elements.put(AttributesController_2013, FractalPackage.eINSTANCE
					.getAttributesController());

			elements.put(Attribute_2014, FractalPackage.eINSTANCE
					.getAttribute());

			elements.put(Controller_2015, FractalPackage.eINSTANCE
					.getController());

			elements.put(VirtualNode_2016, FractalPackage.eINSTANCE
					.getVirtualNode());

			elements.put(Binding_3001, FractalPackage.eINSTANCE.getBinding());

			elements.put(Binding_3002, FractalPackage.eINSTANCE.getBinding());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	 * @generated
	 */
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	 * @generated
	 */
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet();
			KNOWN_ELEMENT_TYPES.add(System_79);
			KNOWN_ELEMENT_TYPES.add(Definition_1001);
			KNOWN_ELEMENT_TYPES.add(DefinitionCall_2001);
			KNOWN_ELEMENT_TYPES.add(Component_2002);
			KNOWN_ELEMENT_TYPES.add(Content_2003);
			KNOWN_ELEMENT_TYPES.add(AttributesController_2004);
			KNOWN_ELEMENT_TYPES.add(Attribute_2005);
			KNOWN_ELEMENT_TYPES.add(Attribute_2006);
			KNOWN_ELEMENT_TYPES.add(Controller_2007);
			KNOWN_ELEMENT_TYPES.add(VirtualNode_2008);
			KNOWN_ELEMENT_TYPES.add(Interface_2009);
			KNOWN_ELEMENT_TYPES.add(Interface_2010);
			KNOWN_ELEMENT_TYPES.add(Component_2011);
			KNOWN_ELEMENT_TYPES.add(Content_2012);
			KNOWN_ELEMENT_TYPES.add(AttributesController_2013);
			KNOWN_ELEMENT_TYPES.add(Attribute_2014);
			KNOWN_ELEMENT_TYPES.add(Controller_2015);
			KNOWN_ELEMENT_TYPES.add(VirtualNode_2016);
			KNOWN_ELEMENT_TYPES.add(Binding_3001);
			KNOWN_ELEMENT_TYPES.add(Binding_3002);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

}
