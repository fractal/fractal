package org.ow2.fractal.f4e.fractal.diagram.edit.parts;

import java.beans.PropertyChangeEvent;

import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.ConnectionLayer;
import org.eclipse.draw2d.ConnectionRouter;
import org.eclipse.draw2d.FreeformLayeredPane;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITreeBranchEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.RoutingStyle;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.diagram.custom.figures.BindingConnectionEx;
import org.ow2.fractal.f4e.diagram.custom.layouts.BindingConnectionAnchorSource;
import org.ow2.fractal.f4e.diagram.custom.layouts.BindingConnectionAnchorTarget;
import org.ow2.fractal.f4e.diagram.custom.router.ConnectionLayerExEx;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.BindingItemSemanticEditPolicy;

/**
 * @generated
 */
public class BindingEditPart extends ConnectionNodeEditPart implements
		ITreeBranchEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 3001;

	/**
	 * @generated
	 */
	public BindingEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new BindingItemSemanticEditPolicy());
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected Connection createConnectionFigure() {
		return new BindingConnectionEx();
	}

	/**
	 * @generated
	 */
	public BindingConnectionEx getPrimaryShape() {
		return (BindingConnectionEx) getFigure();
	}	
	
	
	protected ConnectionAnchor getSourceConnectionAnchor() {
		return new BindingConnectionAnchorSource(this);
	}

	protected ConnectionAnchor getTargetConnectionAnchor() {
		return new BindingConnectionAnchorTarget(this);
	}

	private ConnectionRouter rectilinearRouter = null;
	
	protected void installRouter() {
//		ConnectionLayer cLayer = (ConnectionLayer) getLayer(LayerConstants.CONNECTION_LAYER);
//		
//		RoutingStyle style = (RoutingStyle) ((View) getModel())
//				.getStyle(NotationPackage.Literals.ROUTING_STYLE);
//		if (style != null && cLayer instanceof ConnectionLayerExEx) {
//			ConnectionLayerExEx cLayerEx = (ConnectionLayerExEx) cLayer;
//			//if (Routing.RECTILINEAR_LITERAL == style.getRouting()) {
//				if (rectilinearRouter == null) {
//					rectilinearRouter = cLayerEx
//							.getScaWireEdgeRectilinearRouter();
//				}
//				getConnectionFigure().setConnectionRouter(rectilinearRouter);
//				//((BindingConnectionEx) getFigure()).setRouterIsRectilinear(true);
//				refreshRouterChange();
//			//	return;
//			//}
//		}
//		//((BindingConnectionEx) getFigure()).setRouterIsRectilinear(false);
		super.installRouter();
	}
	
	protected void fireSelectionChanged() {
		super.fireSelectionChanged();
		
		//update the binding color when it is selected
		this.refreshForegroundColor();
	}
	
	protected void refreshForegroundColor() {
		super.refreshForegroundColor();
		
		// If the binding is selected we put it in orange
		// so it is easier to see it when there is a lot of bindings.
		if (getSelected() != EditPart.SELECTED_NONE) {
			this.setForegroundColor(BindingConnectionEx.SELECTED_COLOR);
		}else{
			this.setForegroundColor(this.getFigure().getForegroundColor());
		}
	}
}
