package org.ow2.fractal.f4e.fractal.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.core.providers.AbstractViewProvider;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Attribute2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Attribute3EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributeEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributeName2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributeName3EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributeNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesController2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerCompartmentAttributesControllerEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerMergedCompartmentAttributesControllerEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerSignature2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerSignatureEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Binding2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.BindingEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Component2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentAttributesEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentExtendsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentOthersEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentSubComponentsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentAttributesEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentOthersEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentSubComponentsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentName2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Content2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ContentClass2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ContentClassEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Controller2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ControllerEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ControllerType2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ControllerTypeEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCallEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionAttributesEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionExtendsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionOthersEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionSubComponentsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Interface2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.InterfaceEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.InterfaceName2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.InterfaceNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.SystemEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.VirtualNode2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.VirtualNodeEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.VirtualNodeName2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.VirtualNodeNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.WrappingLabelEditPart;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalVisualIDRegistry;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.Attribute2ViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.Attribute3ViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.AttributeName2ViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.AttributeName3ViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.AttributeNameViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.AttributeViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.AttributesController2ViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.AttributesControllerCompartmentAttributesControllerViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.AttributesControllerMergedCompartmentAttributesControllerViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.AttributesControllerSignature2ViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.AttributesControllerSignatureViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.AttributesControllerViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.Binding2ViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.BindingViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.Component2ViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.ComponentCompartmentComponentAttributesViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.ComponentCompartmentComponentContentViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.ComponentCompartmentComponentExtendsViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.ComponentCompartmentComponentOthersViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.ComponentCompartmentComponentSubComponentsViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.ComponentMergedCompartmentComponentAttributesViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.ComponentMergedCompartmentComponentContentViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.ComponentMergedCompartmentComponentOthersViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.ComponentMergedCompartmentComponentSubComponentsViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.ComponentName2ViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.ComponentNameViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.ComponentViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.Content2ViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.ContentClass2ViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.ContentClassViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.ContentViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.Controller2ViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.ControllerType2ViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.ControllerTypeViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.ControllerViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.DefinitionCallViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.DefinitionCompartmentDefinitionAttributesViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.DefinitionCompartmentDefinitionContentViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.DefinitionCompartmentDefinitionExtendsViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.DefinitionCompartmentDefinitionOthersViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.DefinitionCompartmentDefinitionSubComponentsViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.DefinitionNameViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.DefinitionViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.Interface2ViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.InterfaceName2ViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.InterfaceNameViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.InterfaceViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.SystemViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.VirtualNode2ViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.VirtualNodeName2ViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.VirtualNodeNameViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.VirtualNodeViewFactory;
import org.ow2.fractal.f4e.fractal.diagram.view.factories.WrappingLabelViewFactory;

/**
 * @generated
 */
public class FractalViewProvider extends AbstractViewProvider {

	/**
	 * @generated
	 */
	protected Class getDiagramViewClass(IAdaptable semanticAdapter,
			String diagramKind) {
		EObject semanticElement = getSemanticElement(semanticAdapter);
		if (SystemEditPart.MODEL_ID.equals(diagramKind)
				&& FractalVisualIDRegistry.getDiagramVisualID(semanticElement) != -1) {
			return SystemViewFactory.class;
		}
		return null;
	}

	/**
	 * @generated
	 */
	protected Class getNodeViewClass(IAdaptable semanticAdapter,
			View containerView, String semanticHint) {
		if (containerView == null) {
			return null;
		}
		IElementType elementType = getSemanticElementType(semanticAdapter);
		EObject domainElement = getSemanticElement(semanticAdapter);
		int visualID;
		if (semanticHint == null) {
			// Semantic hint is not specified. Can be a result of call from CanonicalEditPolicy.
			// In this situation there should be NO elementType, visualID will be determined
			// by VisualIDRegistry.getNodeVisualID() for domainElement.
			if (elementType != null || domainElement == null) {
				return null;
			}
			visualID = FractalVisualIDRegistry.getNodeVisualID(containerView,
					domainElement);
		} else {
			visualID = FractalVisualIDRegistry.getVisualID(semanticHint);
			if (elementType != null) {
				// Semantic hint is specified together with element type.
				// Both parameters should describe exactly the same diagram element.
				// In addition we check that visualID returned by VisualIDRegistry.getNodeVisualID() for
				// domainElement (if specified) is the same as in element type.
				if (!FractalElementTypes.isKnownElementType(elementType)
						|| (!(elementType instanceof IHintedType))) {
					return null; // foreign element type
				}
				String elementTypeHint = ((IHintedType) elementType)
						.getSemanticHint();
				if (!semanticHint.equals(elementTypeHint)) {
					return null; // if semantic hint is specified it should be the same as in element type
				}
				if (domainElement != null
						&& visualID != FractalVisualIDRegistry.getNodeVisualID(
								containerView, domainElement)) {
					return null; // visual id for node EClass should match visual id from element type
				}
			} else {
				// Element type is not specified. Domain element should be present (except pure design elements).
				// This method is called with EObjectAdapter as parameter from:
				//   - ViewService.createNode(View container, EObject eObject, String type, PreferencesHint preferencesHint) 
				//   - generated ViewFactory.decorateView() for parent element
				if (!SystemEditPart.MODEL_ID.equals(FractalVisualIDRegistry
						.getModelID(containerView))) {
					return null; // foreign diagram
				}
				switch (visualID) {
				case DefinitionEditPart.VISUAL_ID:
				case DefinitionCallEditPart.VISUAL_ID:
				case ComponentEditPart.VISUAL_ID:
				case ContentEditPart.VISUAL_ID:
				case AttributesControllerEditPart.VISUAL_ID:
				case AttributeEditPart.VISUAL_ID:
				case ControllerEditPart.VISUAL_ID:
				case VirtualNodeEditPart.VISUAL_ID:
				case InterfaceEditPart.VISUAL_ID:
				case Attribute2EditPart.VISUAL_ID:
				case Interface2EditPart.VISUAL_ID:
				case Component2EditPart.VISUAL_ID:
				case Content2EditPart.VISUAL_ID:
				case AttributesController2EditPart.VISUAL_ID:
				case Attribute3EditPart.VISUAL_ID:
				case Controller2EditPart.VISUAL_ID:
				case VirtualNode2EditPart.VISUAL_ID:
					if (domainElement == null
							|| visualID != FractalVisualIDRegistry
									.getNodeVisualID(containerView,
											domainElement)) {
						return null; // visual id in semantic hint should match visual id for domain element
					}
					break;
				case DefinitionNameEditPart.VISUAL_ID:
				case DefinitionCompartmentDefinitionExtendsEditPart.VISUAL_ID:
				case DefinitionCompartmentDefinitionSubComponentsEditPart.VISUAL_ID:
				case DefinitionCompartmentDefinitionContentEditPart.VISUAL_ID:
				case DefinitionCompartmentDefinitionAttributesEditPart.VISUAL_ID:
				case DefinitionCompartmentDefinitionOthersEditPart.VISUAL_ID:
					if (DefinitionEditPart.VISUAL_ID != FractalVisualIDRegistry
							.getVisualID(containerView)
							|| containerView.getElement() != domainElement) {
						return null; // wrong container
					}
					break;
				case WrappingLabelEditPart.VISUAL_ID:
					if (DefinitionCallEditPart.VISUAL_ID != FractalVisualIDRegistry
							.getVisualID(containerView)
							|| containerView.getElement() != domainElement) {
						return null; // wrong container
					}
					break;
				case ComponentNameEditPart.VISUAL_ID:
				case ComponentCompartmentComponentExtendsEditPart.VISUAL_ID:
				case ComponentCompartmentComponentSubComponentsEditPart.VISUAL_ID:
				case ComponentCompartmentComponentContentEditPart.VISUAL_ID:
				case ComponentCompartmentComponentAttributesEditPart.VISUAL_ID:
				case ComponentCompartmentComponentOthersEditPart.VISUAL_ID:
					if (ComponentEditPart.VISUAL_ID != FractalVisualIDRegistry
							.getVisualID(containerView)
							|| containerView.getElement() != domainElement) {
						return null; // wrong container
					}
					break;
				case ContentClassEditPart.VISUAL_ID:
					if (ContentEditPart.VISUAL_ID != FractalVisualIDRegistry
							.getVisualID(containerView)
							|| containerView.getElement() != domainElement) {
						return null; // wrong container
					}
					break;
				case AttributesControllerSignatureEditPart.VISUAL_ID:
				case AttributesControllerCompartmentAttributesControllerEditPart.VISUAL_ID:
					if (AttributesControllerEditPart.VISUAL_ID != FractalVisualIDRegistry
							.getVisualID(containerView)
							|| containerView.getElement() != domainElement) {
						return null; // wrong container
					}
					break;
				case AttributeNameEditPart.VISUAL_ID:
					if (AttributeEditPart.VISUAL_ID != FractalVisualIDRegistry
							.getVisualID(containerView)
							|| containerView.getElement() != domainElement) {
						return null; // wrong container
					}
					break;
				case AttributeName2EditPart.VISUAL_ID:
					if (Attribute2EditPart.VISUAL_ID != FractalVisualIDRegistry
							.getVisualID(containerView)
							|| containerView.getElement() != domainElement) {
						return null; // wrong container
					}
					break;
				case ControllerTypeEditPart.VISUAL_ID:
					if (ControllerEditPart.VISUAL_ID != FractalVisualIDRegistry
							.getVisualID(containerView)
							|| containerView.getElement() != domainElement) {
						return null; // wrong container
					}
					break;
				case VirtualNodeNameEditPart.VISUAL_ID:
					if (VirtualNodeEditPart.VISUAL_ID != FractalVisualIDRegistry
							.getVisualID(containerView)
							|| containerView.getElement() != domainElement) {
						return null; // wrong container
					}
					break;
				case InterfaceNameEditPart.VISUAL_ID:
					if (InterfaceEditPart.VISUAL_ID != FractalVisualIDRegistry
							.getVisualID(containerView)
							|| containerView.getElement() != domainElement) {
						return null; // wrong container
					}
					break;
				case InterfaceName2EditPart.VISUAL_ID:
					if (Interface2EditPart.VISUAL_ID != FractalVisualIDRegistry
							.getVisualID(containerView)
							|| containerView.getElement() != domainElement) {
						return null; // wrong container
					}
					break;
				case ComponentName2EditPart.VISUAL_ID:
				case ComponentMergedCompartmentComponentSubComponentsEditPart.VISUAL_ID:
				case ComponentMergedCompartmentComponentContentEditPart.VISUAL_ID:
				case ComponentMergedCompartmentComponentAttributesEditPart.VISUAL_ID:
				case ComponentMergedCompartmentComponentOthersEditPart.VISUAL_ID:
					if (Component2EditPart.VISUAL_ID != FractalVisualIDRegistry
							.getVisualID(containerView)
							|| containerView.getElement() != domainElement) {
						return null; // wrong container
					}
					break;
				case ContentClass2EditPart.VISUAL_ID:
					if (Content2EditPart.VISUAL_ID != FractalVisualIDRegistry
							.getVisualID(containerView)
							|| containerView.getElement() != domainElement) {
						return null; // wrong container
					}
					break;
				case AttributesControllerSignature2EditPart.VISUAL_ID:
				case AttributesControllerMergedCompartmentAttributesControllerEditPart.VISUAL_ID:
					if (AttributesController2EditPart.VISUAL_ID != FractalVisualIDRegistry
							.getVisualID(containerView)
							|| containerView.getElement() != domainElement) {
						return null; // wrong container
					}
					break;
				case AttributeName3EditPart.VISUAL_ID:
					if (Attribute3EditPart.VISUAL_ID != FractalVisualIDRegistry
							.getVisualID(containerView)
							|| containerView.getElement() != domainElement) {
						return null; // wrong container
					}
					break;
				case ControllerType2EditPart.VISUAL_ID:
					if (Controller2EditPart.VISUAL_ID != FractalVisualIDRegistry
							.getVisualID(containerView)
							|| containerView.getElement() != domainElement) {
						return null; // wrong container
					}
					break;
				case VirtualNodeName2EditPart.VISUAL_ID:
					if (VirtualNode2EditPart.VISUAL_ID != FractalVisualIDRegistry
							.getVisualID(containerView)
							|| containerView.getElement() != domainElement) {
						return null; // wrong container
					}
					break;
				default:
					return null;
				}
			}
		}
		return getNodeViewClass(containerView, visualID);
	}

	/**
	 * @generated
	 */
	protected Class getNodeViewClass(View containerView, int visualID) {
		if (containerView == null
				|| !FractalVisualIDRegistry.canCreateNode(containerView,
						visualID)) {
			return null;
		}
		switch (visualID) {
		case DefinitionEditPart.VISUAL_ID:
			return DefinitionViewFactory.class;
		case DefinitionNameEditPart.VISUAL_ID:
			return DefinitionNameViewFactory.class;
		case DefinitionCallEditPart.VISUAL_ID:
			return DefinitionCallViewFactory.class;
		case WrappingLabelEditPart.VISUAL_ID:
			return WrappingLabelViewFactory.class;
		case ComponentEditPart.VISUAL_ID:
			return ComponentViewFactory.class;
		case ComponentNameEditPart.VISUAL_ID:
			return ComponentNameViewFactory.class;
		case ContentEditPart.VISUAL_ID:
			return ContentViewFactory.class;
		case ContentClassEditPart.VISUAL_ID:
			return ContentClassViewFactory.class;
		case AttributesControllerEditPart.VISUAL_ID:
			return AttributesControllerViewFactory.class;
		case AttributesControllerSignatureEditPart.VISUAL_ID:
			return AttributesControllerSignatureViewFactory.class;
		case AttributeEditPart.VISUAL_ID:
			return AttributeViewFactory.class;
		case AttributeNameEditPart.VISUAL_ID:
			return AttributeNameViewFactory.class;
		case Attribute2EditPart.VISUAL_ID:
			return Attribute2ViewFactory.class;
		case AttributeName2EditPart.VISUAL_ID:
			return AttributeName2ViewFactory.class;
		case ControllerEditPart.VISUAL_ID:
			return ControllerViewFactory.class;
		case ControllerTypeEditPart.VISUAL_ID:
			return ControllerTypeViewFactory.class;
		case VirtualNodeEditPart.VISUAL_ID:
			return VirtualNodeViewFactory.class;
		case VirtualNodeNameEditPart.VISUAL_ID:
			return VirtualNodeNameViewFactory.class;
		case InterfaceEditPart.VISUAL_ID:
			return InterfaceViewFactory.class;
		case InterfaceNameEditPart.VISUAL_ID:
			return InterfaceNameViewFactory.class;
		case Interface2EditPart.VISUAL_ID:
			return Interface2ViewFactory.class;
		case InterfaceName2EditPart.VISUAL_ID:
			return InterfaceName2ViewFactory.class;
		case Component2EditPart.VISUAL_ID:
			return Component2ViewFactory.class;
		case ComponentName2EditPart.VISUAL_ID:
			return ComponentName2ViewFactory.class;
		case Content2EditPart.VISUAL_ID:
			return Content2ViewFactory.class;
		case ContentClass2EditPart.VISUAL_ID:
			return ContentClass2ViewFactory.class;
		case AttributesController2EditPart.VISUAL_ID:
			return AttributesController2ViewFactory.class;
		case AttributesControllerSignature2EditPart.VISUAL_ID:
			return AttributesControllerSignature2ViewFactory.class;
		case Attribute3EditPart.VISUAL_ID:
			return Attribute3ViewFactory.class;
		case AttributeName3EditPart.VISUAL_ID:
			return AttributeName3ViewFactory.class;
		case Controller2EditPart.VISUAL_ID:
			return Controller2ViewFactory.class;
		case ControllerType2EditPart.VISUAL_ID:
			return ControllerType2ViewFactory.class;
		case VirtualNode2EditPart.VISUAL_ID:
			return VirtualNode2ViewFactory.class;
		case VirtualNodeName2EditPart.VISUAL_ID:
			return VirtualNodeName2ViewFactory.class;
		case DefinitionCompartmentDefinitionExtendsEditPart.VISUAL_ID:
			return DefinitionCompartmentDefinitionExtendsViewFactory.class;
		case DefinitionCompartmentDefinitionSubComponentsEditPart.VISUAL_ID:
			return DefinitionCompartmentDefinitionSubComponentsViewFactory.class;
		case DefinitionCompartmentDefinitionContentEditPart.VISUAL_ID:
			return DefinitionCompartmentDefinitionContentViewFactory.class;
		case DefinitionCompartmentDefinitionAttributesEditPart.VISUAL_ID:
			return DefinitionCompartmentDefinitionAttributesViewFactory.class;
		case DefinitionCompartmentDefinitionOthersEditPart.VISUAL_ID:
			return DefinitionCompartmentDefinitionOthersViewFactory.class;
		case ComponentCompartmentComponentExtendsEditPart.VISUAL_ID:
			return ComponentCompartmentComponentExtendsViewFactory.class;
		case ComponentCompartmentComponentSubComponentsEditPart.VISUAL_ID:
			return ComponentCompartmentComponentSubComponentsViewFactory.class;
		case ComponentCompartmentComponentContentEditPart.VISUAL_ID:
			return ComponentCompartmentComponentContentViewFactory.class;
		case ComponentCompartmentComponentAttributesEditPart.VISUAL_ID:
			return ComponentCompartmentComponentAttributesViewFactory.class;
		case ComponentCompartmentComponentOthersEditPart.VISUAL_ID:
			return ComponentCompartmentComponentOthersViewFactory.class;
		case AttributesControllerCompartmentAttributesControllerEditPart.VISUAL_ID:
			return AttributesControllerCompartmentAttributesControllerViewFactory.class;
		case ComponentMergedCompartmentComponentSubComponentsEditPart.VISUAL_ID:
			return ComponentMergedCompartmentComponentSubComponentsViewFactory.class;
		case ComponentMergedCompartmentComponentContentEditPart.VISUAL_ID:
			return ComponentMergedCompartmentComponentContentViewFactory.class;
		case ComponentMergedCompartmentComponentAttributesEditPart.VISUAL_ID:
			return ComponentMergedCompartmentComponentAttributesViewFactory.class;
		case ComponentMergedCompartmentComponentOthersEditPart.VISUAL_ID:
			return ComponentMergedCompartmentComponentOthersViewFactory.class;
		case AttributesControllerMergedCompartmentAttributesControllerEditPart.VISUAL_ID:
			return AttributesControllerMergedCompartmentAttributesControllerViewFactory.class;
		}
		return null;
	}

	/**
	 * @generated
	 */
	protected Class getEdgeViewClass(IAdaptable semanticAdapter,
			View containerView, String semanticHint) {
		IElementType elementType = getSemanticElementType(semanticAdapter);
		if (!FractalElementTypes.isKnownElementType(elementType)
				|| (!(elementType instanceof IHintedType))) {
			return null; // foreign element type
		}
		String elementTypeHint = ((IHintedType) elementType).getSemanticHint();
		if (elementTypeHint == null) {
			return null; // our hint is visual id and must be specified
		}
		if (semanticHint != null && !semanticHint.equals(elementTypeHint)) {
			return null; // if semantic hint is specified it should be the same as in element type
		}
		int visualID = FractalVisualIDRegistry.getVisualID(elementTypeHint);
		EObject domainElement = getSemanticElement(semanticAdapter);
		if (domainElement != null
				&& visualID != FractalVisualIDRegistry
						.getLinkWithClassVisualID(domainElement)) {
			return null; // visual id for link EClass should match visual id from element type
		}
		return getEdgeViewClass(visualID);
	}

	/**
	 * @generated
	 */
	protected Class getEdgeViewClass(int visualID) {
		switch (visualID) {
		case BindingEditPart.VISUAL_ID:
			return BindingViewFactory.class;
		case Binding2EditPart.VISUAL_ID:
			return Binding2ViewFactory.class;
		}
		return null;
	}

	/**
	 * @generated
	 */
	private IElementType getSemanticElementType(IAdaptable semanticAdapter) {
		if (semanticAdapter == null) {
			return null;
		}
		return (IElementType) semanticAdapter.getAdapter(IElementType.class);
	}
}
