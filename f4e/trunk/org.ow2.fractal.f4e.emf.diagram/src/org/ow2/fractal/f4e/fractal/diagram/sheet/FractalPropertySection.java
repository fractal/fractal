package org.ow2.fractal.f4e.fractal.diagram.sheet;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.core.commands.operations.IOperationHistory;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.transaction.NotificationFilter;
import org.eclipse.emf.transaction.RunnableWithResult;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.ui.provider.TransactionalPropertySource;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.properties.sections.AbstractModelerPropertySection;
import org.eclipse.gmf.runtime.emf.ui.properties.sections.PropertySheetEntry;
import org.eclipse.gmf.runtime.emf.ui.properties.sections.UndoableModelPropertySheetEntry;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.IPropertySourceProvider;
import org.eclipse.ui.views.properties.PropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.ow2.fractal.f4e.diagram.custom.providers.FractalPropertySource;
import org.ow2.fractal.f4e.diagram.custom.providers.MergedPropertyDescriptor;
import org.ow2.fractal.f4e.fractal.adapter.helper.HelperAdapter;
import org.ow2.fractal.f4e.fractal.plugin.FractalPlugin;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;

/**
 * Customize the generated FractalPropertySection to suppress alternate color in the property view.
 * Only merged properties, and unset properties overriden by a merged properties should have a GRAY color in the property view.
 * @generated NOT
 */
public class FractalPropertySection extends AbstractModelerPropertySection implements
		IPropertySourceProvider {

	protected class FractalUndoableModelPropertySheetEntry extends UndoableModelPropertySheetEntry{
		 public FractalUndoableModelPropertySheetEntry(IOperationHistory operationHistory) {
		       super(operationHistory);
		 }
		 
		public IPropertyDescriptor getDescriptor() {
			// TODO Auto-generated method stub
			return super.getDescriptor();
		}


		protected PropertySheetEntry createChildEntry() {
			// TODO Auto-generated method stub
			 return new FractalUndoableModelPropertySheetEntry(getOperationHistory());
		}
		
		 public void resetPropertyValue() {
			 	setValue(SetCommand.UNSET_VALUE);
		 }

		@Override
		public TransactionalEditingDomain getEditingDomain() {
			// TODO Auto-generated method stub
			return FractalTransactionalEditingDomain.getEditingDomain();
		}
		 
	}
	/**
     * the property sheet page for this section
     */
    protected PropertySheetPage page;
  
    /* (non-Javadoc)
     * @see org.eclipse.ui.views.properties.tabbed.ISection#createControls(org.eclipse.swt.widgets.Composite, org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage)
     */
    public void createControls(final Composite parent,
            TabbedPropertySheetPage aTabbedPropertySheetPage) {
        super.createControls(parent, aTabbedPropertySheetPage);
        Composite composite = getWidgetFactory()
                .createFlatFormComposite(parent);
        FormData data = null;

        String tableLabelStr = getTableLabel();
        CLabel tableLabel = null;
        if (tableLabelStr != null && tableLabelStr.length() > 0) {
            tableLabel = getWidgetFactory().createCLabel(composite,
                    tableLabelStr);
            data = new FormData();
            data.left = new FormAttachment(0, 0);
            data.top = new FormAttachment(0, 0);
            tableLabel.setLayoutData(data);
        }

        page = new PropertySheetPage();
      
        FractalUndoableModelPropertySheetEntry root = new FractalUndoableModelPropertySheetEntry(
            OperationHistoryFactory.getOperationHistory());
        
        root.setPropertySourceProvider(getPropertySourceProvider());
        page.setRootEntry(root);

        page.createControl(composite);
        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        if (tableLabel == null) {
            data.top = new FormAttachment(0, 0);
        } else {
            data.top = new FormAttachment(tableLabel, 0, SWT.BOTTOM);
        }
        data.bottom = new FormAttachment(100, 0);
        data.height = 100;
        data.width = 100;
        page.getControl().setLayoutData(data);
       
        // We don't want alternate colors in the properties view
        ((Tree)page.getControl()).setLinesVisible(false);
        setActionBars(aTabbedPropertySheetPage.getSite().getActionBars());
    }

    /**
     * Sets and prepares the actionBars for this section
     *  
     * @param actionBars the action bars for this page
     * @see org.eclipse.gmf.runtime.common.ui.properties.TabbedPropertySheetPage#setActionBars(org.eclipse.ui.IActionBars)
     */   
    public void setActionBars(IActionBars actionBars) {
        
        actionBars.getMenuManager().removeAll();
        actionBars.getToolBarManager().removeAll();
        actionBars.getStatusLineManager().removeAll();

        page.makeContributions(actionBars.getMenuManager(), actionBars
                .getToolBarManager(), actionBars.getStatusLineManager());
        
        actionBars.getToolBarManager().update(true);

    }


    /**
     * Returns the label for the table. The default implementation returns null,
     * that is, there is no label.
     * 
     * @return The label for the table
     */
    protected String getTableLabel() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.views.properties.tabbed.ISection#dispose()
     */
    public void dispose() {
        super.dispose();

        if (page != null) {
            page.dispose();
            page = null;
        }

    }

 
    /* (non-Javadoc)
     * @see org.eclipse.ui.views.properties.tabbed.ISection#refresh()
     */
    static final Color GRAY = new Color(null, 240,240,240);
    static final Color WHITE = new Color(null, 255,255,255);
  
    
    public void refresh() {
    	try {
		FractalTransactionalEditingDomain.getEditingDomain().runExclusive(new RunnableWithResult.Impl() {
				public void run() {
			

    	page.refresh();
        if(((Tree)page.getControl()).getItems().length>0){
        	TreeItem treeItems[] = ((Tree)page.getControl()).getItems();
        	for(int i = 0; i< treeItems.length; i++){
        		Object data = ((Tree)page.getControl()).getItem(i).getData();
        		if(data instanceof FractalUndoableModelPropertySheetEntry){
        			FractalUndoableModelPropertySheetEntry entry = (FractalUndoableModelPropertySheetEntry)data;
        			if(entry.getDescriptor() instanceof MergedPropertyDescriptor){
        	    		EObject object = ((MergedPropertyDescriptor)entry.getDescriptor()).getEObject();
        	    		EStructuralFeature feature = ((MergedPropertyDescriptor)entry.getDescriptor()).getFeature();
        	    		if(HelperAdapter.isMergedFeature(object, feature.getFeatureID())){
        	    			// If its a merged feature
        	    			((Tree)page.getControl()).getItem(i).setBackground(GRAY);
        	    		}else if(!object.eIsSet(feature) && HelperAdapter.hasMergedFeature(object, feature.getFeatureID())){
        	    			// if the feature is unset
        	    			((Tree)page.getControl()).getItem(i).setBackground(GRAY);
        	    		}else{
        	    			((Tree)page.getControl()).getItem(i).setBackground(WHITE);
        	    		}
        	    	}
        		}
        	}
        }
        
		
				}
			});
		} catch (InterruptedException e) {
			FractalPlugin.getDefault().getLog().log(new Status(IStatus.ERROR,FractalPlugin.PLUGIN_ID,e.getMessage(),e));
		}
    	//	Object[] objects = data.getValue();
    }

   
    /* (non-Javadoc)
     * @see org.eclipse.ui.views.properties.tabbed.ISection#shouldUseExtraSpace()
     */
    public boolean shouldUseExtraSpace() {
        return true;
    }

    /**
     * Update if nessesary, upon receiving the model event.
     * 
     * @see #aboutToBeShown()
     * @see #aboutToBeHidden()
     * @param notification -
     *            even notification
     * @param element -
     *            element that has changed
     */
    public void update(final Notification notification, EObject element) {
    	if (!isDisposed()) {
			postUpdateRequest(new Runnable() {

				public void run() {
					if (!isDisposed() && !isNotifierDeleted(notification))
						refresh();
				}
			});
		}
	}
   
    /* (non-Javadoc)
     * @see org.eclipse.gmf.runtime.emf.core.edit.IDemuxedMListener#getFilter()
     */
    public NotificationFilter getFilter() {
        return NotificationFilter.createEventTypeFilter(Notification.SET).or(
            NotificationFilter.createEventTypeFilter(Notification.UNSET)).or(
            NotificationFilter.createEventTypeFilter(Notification.ADD)).or(
            NotificationFilter.createEventTypeFilter(Notification.ADD_MANY))
            .or(NotificationFilter.createEventTypeFilter(Notification.REMOVE))
            .or(
                NotificationFilter
                    .createEventTypeFilter(Notification.REMOVE_MANY)).and(
                NotificationFilter.createNotifierTypeFilter(EObject.class));
    }

   
    /*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gmf.runtime.diagram.ui.properties.sections.AbstractModelerPropertySection#addToEObjectList(java.lang.Object)
	 */
    protected boolean addToEObjectList(Object object) {
        /* not implemented */
    	return true;
    }  
    
    
	
    
    
	/**
	 * @generated NOT
	 */
	public IPropertySource getPropertySource(Object object) {
		if (object instanceof IPropertySource) {
			return (IPropertySource) object;
		}
	
		AdapterFactory af = getAdapterFactory(object);
		if (af != null) {
			IItemPropertySource ips = (IItemPropertySource) af.adapt(object,
					IItemPropertySource.class);
			if (ips != null) {
				// Start yann hand modifs
				return new TransactionalPropertySource(FractalTransactionalEditingDomain.getEditingDomain(), new FractalPropertySource(object, ips));
				// End yann hand modifs
				//return new PropertySource(object, ips);
			}
		}
		if (object instanceof IAdaptable) {
			return (IPropertySource) ((IAdaptable) object)
					.getAdapter(IPropertySource.class);
		}
		return null;
	}

	/**
	 * @generated NOT
	 */
	protected IPropertySourceProvider getPropertySourceProvider() {
		return this;
	}

	/**
	 * Modify/unwrap selection.
	 * @generated NOT
	 */
	protected Object transformSelection(Object selected) {

		if (selected instanceof EditPart) {
			Object model = ((EditPart) selected).getModel();
			return model instanceof View ? ((View) model).getElement() : null;
		}
		if (selected instanceof View) {
			return ((View) selected).getElement();
		}
		if (selected instanceof IAdaptable) {
			View view = (View) ((IAdaptable) selected).getAdapter(View.class);
			if (view != null) {
				return view.getElement();
			}
		}
		return selected;
	}

	protected void setSuperInput(IWorkbenchPart part, ISelection selection){
		 IEditingDomainProvider provider = (IEditingDomainProvider) part
            .getAdapter(IEditingDomainProvider.class);
        if (provider != null) {
            EditingDomain theEditingDomain = provider.getEditingDomain();
            if (theEditingDomain instanceof TransactionalEditingDomain) {
                setEditingDomain((TransactionalEditingDomain) theEditingDomain);
            }
        }
        
        // Set the eObject for the section, too. The workbench part may not
		// adapt to IEditingDomainProvider, in which case the selected EObject
		// will be used to derive the editing domain.
		if (!selection.isEmpty() && selection instanceof IStructuredSelection) {
            Object firstElement = ((IStructuredSelection) selection)
                .getFirstElement();
            
            if (firstElement != null) {
            	EObject adapted = unwrap(firstElement);
            	
	            if (adapted != null) {
	                setEObject(adapted);
	            }
            }
        }
        
        page.selectionChanged(part, selection);
	}
	/**
	 * @generated NOT
	 */
	public void setInput(IWorkbenchPart part, ISelection selection) {
		if (selection.isEmpty()
				|| false == selection instanceof StructuredSelection) {
		setSuperInput(part, selection);
			return;
		}
		final StructuredSelection structuredSelection = ((StructuredSelection) selection);
		ArrayList transformedSelection = new ArrayList(structuredSelection
				.size());
		for (Iterator it = structuredSelection.iterator(); it.hasNext();) {
			Object r = transformSelection(it.next());
			if (r != null) {
				transformedSelection.add(r);
			}
		}
		setSuperInput(part, new StructuredSelection(transformedSelection));
	//	super.setInput(part, new StructuredSelection(transformedSelection));
	}

	/**
	 * @generated
	 */
	protected AdapterFactory getAdapterFactory(Object object) {
		if (getEditingDomain() instanceof AdapterFactoryEditingDomain) {
			return ((AdapterFactoryEditingDomain) getEditingDomain())
					.getAdapterFactory();
		}
		TransactionalEditingDomain editingDomain = TransactionUtil
				.getEditingDomain(object);
		if (editingDomain != null) {
			return ((AdapterFactoryEditingDomain) editingDomain)
					.getAdapterFactory();
		}
		return null;
	}
}
