package org.ow2.fractal.f4e.fractal.diagram.edit.parts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.handles.NonResizableHandleKit;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.core.edithelpers.CreateElementRequestAdapter;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CreationEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewAndElementRequest;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.diagram.custom.figures.AbstractComponentNameWrappingLabel;
import org.ow2.fractal.f4e.diagram.custom.figures.AttributesControllerShape;
import org.ow2.fractal.f4e.diagram.custom.figures.ContainerShape;
import org.ow2.fractal.f4e.diagram.custom.layouts.AutosizeContainerLayout;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.AttributesController2ItemSemanticEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalVisualIDRegistry;
import org.ow2.fractal.f4e.fractal.diagram.providers.FractalElementTypes;

/**
 * @generated
 */
public class AttributesController2EditPart extends ShapeNodeEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 2013;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public AttributesController2EditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		installEditPolicy(EditPolicyRoles.CREATION_ROLE,
				new CreationEditPolicy() {
					public Command getCommand(Request request) {
						if (understandsRequest(request)) {
							if (request instanceof CreateViewAndElementRequest) {
								CreateElementRequestAdapter adapter = ((CreateViewAndElementRequest) request)
										.getViewAndElementDescriptor()
										.getCreateElementRequestAdapter();
								IElementType type = (IElementType) adapter
										.getAdapter(IElementType.class);
								if (type == FractalElementTypes.Attribute_2014) {
									EditPart compartmentEditPart = getChildBySemanticHint(FractalVisualIDRegistry
											.getType(AttributesControllerMergedCompartmentAttributesControllerEditPart.VISUAL_ID));
									return compartmentEditPart == null ? null
											: compartmentEditPart
													.getCommand(request);
								}
							}
							return super.getCommand(request);
						}
						return null;
					}
				});
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new AttributesController2ItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		
		installEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE,
				new NonResizableEditPolicy() {

					protected List createSelectionHandles() {
						List handles = new ArrayList();
						NonResizableHandleKit.addMoveHandle(
								(GraphicalEditPart) getHost(), handles);
						return handles;
					}

					public Command getCommand(Request request) {
						return null;
					}

					public boolean understandsRequest(Request request) {
						return false;
					}
				});
		
		// Remove the connection handles
		removeEditPolicy(EditPolicyRoles.CONNECTION_HANDLES_ROLE);
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		LayoutEditPolicy lep = new LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				EditPolicy result = child
						.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated
	 */
	protected IFigure createNodeShape() {
		MergedAttributesFigure figure = new MergedAttributesFigure();
		return primaryShape = figure;
	}

	/**
	 * @generated
	 */
	public MergedAttributesFigure getPrimaryShape() {
		return (MergedAttributesFigure) primaryShape;
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof AttributesControllerSignature2EditPart) {
			((AttributesControllerSignature2EditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureLabelMergedAttributesControllerSignature());
			return true;
		}
		if (childEditPart instanceof AttributesControllerMergedCompartmentAttributesControllerEditPart) {
			IFigure pane = getPrimaryShape().getFigureMergedAttributesArea();
			
			if (pane.getLayoutManager() == null) {
				AutosizeContainerLayout layout = new AutosizeContainerLayout();
				pane.setLayoutManager(layout);
			}
			
			pane
					.add(((AttributesControllerMergedCompartmentAttributesControllerEditPart) childEditPart)
							.getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {

		if (childEditPart instanceof AttributesControllerMergedCompartmentAttributesControllerEditPart) {
			IFigure pane = getPrimaryShape().getFigureMergedAttributesArea();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane
					.remove(((AttributesControllerMergedCompartmentAttributesControllerEditPart) childEditPart)
							.getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {

		if (editPart instanceof AttributesControllerMergedCompartmentAttributesControllerEditPart) {
			return getPrimaryShape().getFigureMergedAttributesArea();
		}
		return super.getContentPaneFor(editPart);
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(getMapMode()
				.DPtoLP(40), getMapMode().DPtoLP(40));
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createNodeFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane.
	 * Respects layout one may have set for generated figure.
	 * @param nodeShape instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(getMapMode().DPtoLP(5));
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(FractalVisualIDRegistry
				.getType(AttributesControllerSignature2EditPart.VISUAL_ID));
	}

	/**
	 * @generated
	 */
	public class MergedAttributesFigure extends AttributesControllerShape {

		/**
		 * @generated
		 */
		private AttributesControllerShape fFigureMergedAttributesFigure;
		/**
		 * @generated
		 */
		private AbstractComponentNameWrappingLabel fFigureLabelMergedAttributesControllerSignature;
		/**
		 * @generated
		 */
		private ContainerShape fFigureMergedAttributesArea;

		/**
		 * @generated
		 */
		public MergedAttributesFigure() {
			this.setEditable(false);
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents() {

			fFigureLabelMergedAttributesControllerSignature = new AbstractComponentNameWrappingLabel();
			
			fFigureLabelMergedAttributesControllerSignature
					.setMaximumSize(new Dimension(
							getMapMode().DPtoLP(50000000), getMapMode().DPtoLP(
									50)));
			fFigureLabelMergedAttributesControllerSignature.setEditable(false);
			this.add(fFigureLabelMergedAttributesControllerSignature);

			fFigureMergedAttributesArea = new ContainerShape(false);
			fFigureMergedAttributesArea.setEditable(false);
			this.add(fFigureMergedAttributesArea);

		}

		/**
		 * @generated
		 */
		private boolean myUseLocalCoordinates = false;

		/**
		 * @generated
		 */
		protected boolean useLocalCoordinates() {
			return myUseLocalCoordinates;
		}

		/**
		 * @generated
		 */
		protected void setUseLocalCoordinates(boolean useLocalCoordinates) {
			myUseLocalCoordinates = useLocalCoordinates;
		}

		/**
		 * @generated
		 */
		public AttributesControllerShape getFigureMergedAttributesFigure() {
			return fFigureMergedAttributesFigure;
		}

		/**
		 * @generated
		 */
		public AbstractComponentNameWrappingLabel getFigureLabelMergedAttributesControllerSignature() {
			return fFigureLabelMergedAttributesControllerSignature;
		}

		/**
		 * @generated
		 */
		public ContainerShape getFigureMergedAttributesArea() {
			return fFigureMergedAttributesArea;
		}

	}

}
