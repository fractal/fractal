package org.ow2.fractal.f4e.fractal.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.RulerGridPreferencePage;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramRulersAndGridPreferencePage extends RulerGridPreferencePage {

	/**
	 * @generated
	 */
	public DiagramRulersAndGridPreferencePage() {
		setPreferenceStore(FractalDiagramEditorPlugin.getInstance()
				.getPreferenceStore());
	}
}
