package org.ow2.fractal.f4e.fractal.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.System;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Attribute2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Attribute3EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributeEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributeName2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributeName3EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributeNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesController2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerCompartmentAttributesControllerEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerMergedCompartmentAttributesControllerEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerSignature2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerSignatureEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Binding2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.BindingEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Component2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentAttributesEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentExtendsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentOthersEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentSubComponentsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentAttributesEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentOthersEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentSubComponentsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentName2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Content2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ContentClass2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ContentClassEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Controller2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ControllerEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ControllerType2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ControllerTypeEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCallEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionAttributesEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionExtendsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionOthersEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionSubComponentsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Interface2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.InterfaceEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.InterfaceName2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.InterfaceNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.SystemEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.VirtualNode2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.VirtualNodeEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.VirtualNodeName2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.VirtualNodeNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.WrappingLabelEditPart;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class FractalVisualIDRegistry {

	/**
	 * @generated
	 */
	private static final String DEBUG_KEY = "org.ow2.fractal.f4e.emf.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (SystemEditPart.MODEL_ID.equals(view.getType())) {
				return SystemEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return org.ow2.fractal.f4e.fractal.diagram.part.FractalVisualIDRegistry
				.getVisualID(view.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(
					Platform.getDebugOption(DEBUG_KEY))) {
				FractalDiagramEditorPlugin.getInstance().logError(
						"Unable to parse view type as a visualID number: "
								+ type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID) {
		return String.valueOf(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (FractalPackage.eINSTANCE.getSystem().isSuperTypeOf(
				domainElement.eClass())
				&& isDiagram((System) domainElement)) {
			return SystemEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @generated NOT
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = org.ow2.fractal.f4e.fractal.diagram.part.FractalVisualIDRegistry
				.getModelID(containerView);
		if (!SystemEditPart.MODEL_ID.equals(containerModelID)) {
			return -1;
		}
		int containerVisualID;
		if (SystemEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = org.ow2.fractal.f4e.fractal.diagram.part.FractalVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = SystemEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case DefinitionEditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getInterface().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_Interfaces()) {
				return InterfaceEditPart.VISUAL_ID;
			}
			if (FractalPackage.eINSTANCE.getInterface().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_MergedInterfaces()) {
				return Interface2EditPart.VISUAL_ID;
			}
			break;
		case ComponentEditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getInterface().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_Interfaces()) {
				return InterfaceEditPart.VISUAL_ID;
			}
			if (FractalPackage.eINSTANCE.getInterface().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_MergedInterfaces()) {
				return Interface2EditPart.VISUAL_ID;
			}
			break;
		case Component2EditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getInterface().isSuperTypeOf(
					domainElement.eClass())) {
				return Interface2EditPart.VISUAL_ID;
			}
			break;
		case DefinitionCompartmentDefinitionExtendsEditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getDefinitionCall().isSuperTypeOf(
					domainElement.eClass())) {
				return DefinitionCallEditPart.VISUAL_ID;
			}
			break;
		case DefinitionCompartmentDefinitionSubComponentsEditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getComponent().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_SubComponents()) {
				return ComponentEditPart.VISUAL_ID;
			}
			if (FractalPackage.eINSTANCE.getComponent().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_MergedSubComponents()) {
				return Component2EditPart.VISUAL_ID;
			}
			break;
		case DefinitionCompartmentDefinitionContentEditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getContent().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_Content()) {
				return ContentEditPart.VISUAL_ID;
			}
			if (FractalPackage.eINSTANCE.getContent().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_MergedContent()) {
				return Content2EditPart.VISUAL_ID;
			}
			break;
		case DefinitionCompartmentDefinitionAttributesEditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getAttributesController()
					.isSuperTypeOf(domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_AttributesController()) {
				return AttributesControllerEditPart.VISUAL_ID;
			}
			if (FractalPackage.eINSTANCE.getAttributesController()
					.isSuperTypeOf(domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_MergedAttributesController()) {
				return AttributesController2EditPart.VISUAL_ID;
			}
			break;
		case DefinitionCompartmentDefinitionOthersEditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getVirtualNode().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_VirtualNode()) {
				return VirtualNodeEditPart.VISUAL_ID;
			}
			if (FractalPackage.eINSTANCE.getController().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_Controller()) {
				return ControllerEditPart.VISUAL_ID;
			}
			if (FractalPackage.eINSTANCE.getVirtualNode().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_MergedVirtualNode()) {
				return VirtualNode2EditPart.VISUAL_ID;
			}
			if (FractalPackage.eINSTANCE.getController().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_MergedController()) {
				return Controller2EditPart.VISUAL_ID;
			}
			break;
		case ComponentCompartmentComponentExtendsEditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getDefinitionCall().isSuperTypeOf(
					domainElement.eClass())) {
				return DefinitionCallEditPart.VISUAL_ID;
			}
			break;
		case ComponentCompartmentComponentSubComponentsEditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getComponent().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_SubComponents()) {
				return ComponentEditPart.VISUAL_ID;
			}
			if (FractalPackage.eINSTANCE.getComponent().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_MergedSubComponents()) {
				return Component2EditPart.VISUAL_ID;
			}
			break;
		case ComponentCompartmentComponentContentEditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getContent().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_Content()) {
				return ContentEditPart.VISUAL_ID;
			}
			if (FractalPackage.eINSTANCE.getContent().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_MergedContent()) {
				return Content2EditPart.VISUAL_ID;
			}
			break;
		case ComponentCompartmentComponentAttributesEditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getAttributesController()
					.isSuperTypeOf(domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_AttributesController()) {
				return AttributesControllerEditPart.VISUAL_ID;
			}
			if (FractalPackage.eINSTANCE.getAttributesController()
					.isSuperTypeOf(domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_MergedAttributesController()) {
				return AttributesController2EditPart.VISUAL_ID;
			}
			break;
		case ComponentCompartmentComponentOthersEditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getController().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_Controller()) {
				return ControllerEditPart.VISUAL_ID;
			}
			if (FractalPackage.eINSTANCE.getVirtualNode().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_VirtualNode()) {
				return VirtualNodeEditPart.VISUAL_ID;
			}
			if (FractalPackage.eINSTANCE.getController().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_MergedController()) {
				return Controller2EditPart.VISUAL_ID;
			}
			if (FractalPackage.eINSTANCE.getVirtualNode().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAbstractComponent_MergedVirtualNode()) {
				return VirtualNode2EditPart.VISUAL_ID;
			}
			break;
		case AttributesControllerCompartmentAttributesControllerEditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getAttribute().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
				.getAttributesController_Attributes()) {
				return AttributeEditPart.VISUAL_ID;
			}
			if (FractalPackage.eINSTANCE.getAttribute().isSuperTypeOf(
					domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
					.getAttributesController_MergedAttributes()) {
				return Attribute2EditPart.VISUAL_ID;
			}
			break;
		case ComponentMergedCompartmentComponentSubComponentsEditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getComponent().isSuperTypeOf(
					domainElement.eClass())) {
				return Component2EditPart.VISUAL_ID;
			}
			break;
		case ComponentMergedCompartmentComponentContentEditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getContent().isSuperTypeOf(
					domainElement.eClass())) {
				return Content2EditPart.VISUAL_ID;
			}
			break;
		case ComponentMergedCompartmentComponentAttributesEditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getAttributesController()
					.isSuperTypeOf(domainElement.eClass())) {
				return AttributesController2EditPart.VISUAL_ID;
			}
			break;
		case ComponentMergedCompartmentComponentOthersEditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getController().isSuperTypeOf(
					domainElement.eClass())) {
				return Controller2EditPart.VISUAL_ID;
			}
			if (FractalPackage.eINSTANCE.getVirtualNode().isSuperTypeOf(
					domainElement.eClass())) {
				return VirtualNode2EditPart.VISUAL_ID;
			}
			break;
		case AttributesControllerMergedCompartmentAttributesControllerEditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getAttribute().isSuperTypeOf(
					domainElement.eClass())) {
				return Attribute3EditPart.VISUAL_ID;
			}
			break;
		case SystemEditPart.VISUAL_ID:
			if (FractalPackage.eINSTANCE.getDefinition().isSuperTypeOf(
					domainElement.eClass())) {
				return DefinitionEditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = org.ow2.fractal.f4e.fractal.diagram.part.FractalVisualIDRegistry
				.getModelID(containerView);
		if (!SystemEditPart.MODEL_ID.equals(containerModelID)) {
			return false;
		}
		int containerVisualID;
		if (SystemEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = org.ow2.fractal.f4e.fractal.diagram.part.FractalVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = SystemEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case DefinitionEditPart.VISUAL_ID:
			if (DefinitionNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (DefinitionCompartmentDefinitionExtendsEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (DefinitionCompartmentDefinitionSubComponentsEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (DefinitionCompartmentDefinitionContentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (DefinitionCompartmentDefinitionAttributesEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (DefinitionCompartmentDefinitionOthersEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (InterfaceEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Interface2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case DefinitionCallEditPart.VISUAL_ID:
			if (WrappingLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ComponentEditPart.VISUAL_ID:
			if (ComponentNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ComponentCompartmentComponentExtendsEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ComponentCompartmentComponentSubComponentsEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ComponentCompartmentComponentContentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ComponentCompartmentComponentAttributesEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ComponentCompartmentComponentOthersEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (InterfaceEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Interface2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ContentEditPart.VISUAL_ID:
			if (ContentClassEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case AttributesControllerEditPart.VISUAL_ID:
			if (AttributesControllerSignatureEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (AttributesControllerCompartmentAttributesControllerEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case AttributeEditPart.VISUAL_ID:
			if (AttributeNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Attribute2EditPart.VISUAL_ID:
			if (AttributeName2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ControllerEditPart.VISUAL_ID:
			if (ControllerTypeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case VirtualNodeEditPart.VISUAL_ID:
			if (VirtualNodeNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case InterfaceEditPart.VISUAL_ID:
			if (InterfaceNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Interface2EditPart.VISUAL_ID:
			if (InterfaceName2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Component2EditPart.VISUAL_ID:
			if (ComponentName2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ComponentMergedCompartmentComponentSubComponentsEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ComponentMergedCompartmentComponentContentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ComponentMergedCompartmentComponentAttributesEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ComponentMergedCompartmentComponentOthersEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Interface2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Content2EditPart.VISUAL_ID:
			if (ContentClass2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case AttributesController2EditPart.VISUAL_ID:
			if (AttributesControllerSignature2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (AttributesControllerMergedCompartmentAttributesControllerEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Attribute3EditPart.VISUAL_ID:
			if (AttributeName3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Controller2EditPart.VISUAL_ID:
			if (ControllerType2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case VirtualNode2EditPart.VISUAL_ID:
			if (VirtualNodeName2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case DefinitionCompartmentDefinitionExtendsEditPart.VISUAL_ID:
			if (DefinitionCallEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case DefinitionCompartmentDefinitionSubComponentsEditPart.VISUAL_ID:
			if (ComponentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Component2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case DefinitionCompartmentDefinitionContentEditPart.VISUAL_ID:
			if (ContentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Content2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case DefinitionCompartmentDefinitionAttributesEditPart.VISUAL_ID:
			if (AttributesControllerEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (AttributesController2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case DefinitionCompartmentDefinitionOthersEditPart.VISUAL_ID:
			if (VirtualNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ControllerEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (VirtualNode2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Controller2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ComponentCompartmentComponentExtendsEditPart.VISUAL_ID:
			if (DefinitionCallEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ComponentCompartmentComponentSubComponentsEditPart.VISUAL_ID:
			if (ComponentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Component2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ComponentCompartmentComponentContentEditPart.VISUAL_ID:
			if (ContentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Content2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ComponentCompartmentComponentAttributesEditPart.VISUAL_ID:
			if (AttributesControllerEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (AttributesController2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ComponentCompartmentComponentOthersEditPart.VISUAL_ID:
			if (ControllerEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (VirtualNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Controller2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (VirtualNode2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case AttributesControllerCompartmentAttributesControllerEditPart.VISUAL_ID:
			if (AttributeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Attribute2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ComponentMergedCompartmentComponentSubComponentsEditPart.VISUAL_ID:
			if (Component2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ComponentMergedCompartmentComponentContentEditPart.VISUAL_ID:
			if (Content2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ComponentMergedCompartmentComponentAttributesEditPart.VISUAL_ID:
			if (AttributesController2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ComponentMergedCompartmentComponentOthersEditPart.VISUAL_ID:
			if (Controller2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (VirtualNode2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case AttributesControllerMergedCompartmentAttributesControllerEditPart.VISUAL_ID:
			if (Attribute3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SystemEditPart.VISUAL_ID:
			if (DefinitionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (FractalPackage.eINSTANCE.getBinding().isSuperTypeOf(
				domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
				.getAbstractComponent_Bindings()) {
			return BindingEditPart.VISUAL_ID;
		}
		if (FractalPackage.eINSTANCE.getBinding().isSuperTypeOf(
				domainElement.eClass()) && domainElement.eContainmentFeature() == FractalPackage.eINSTANCE
				.getAbstractComponent_MergedBindings()) {
			return Binding2EditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(System element) {
		return true;
	}

}
