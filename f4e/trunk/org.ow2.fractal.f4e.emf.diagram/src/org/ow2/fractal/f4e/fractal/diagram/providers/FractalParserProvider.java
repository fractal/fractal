package org.ow2.fractal.f4e.fractal.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.diagram.custom.parsers.AttributesControllerParser;
import org.ow2.fractal.f4e.diagram.custom.parsers.ContentParser;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributeName2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributeName3EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributeNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerSignature2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerSignatureEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentName2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ContentClass2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ContentClassEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ControllerType2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ControllerTypeEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.InterfaceName2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.InterfaceNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.VirtualNodeName2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.VirtualNodeNameEditPart;
import org.ow2.fractal.f4e.fractal.diagram.parsers.MessageFormatParser;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalVisualIDRegistry;

/**
 * @generated
 */
public class FractalParserProvider extends AbstractProvider implements
		IParserProvider {

	/**
	 * @generated
	 */
	private IParser definitionName_4017Parser;

	/**
	 * @generated
	 */
	private IParser getDefinitionName_4017Parser() {
		if (definitionName_4017Parser == null) {
			definitionName_4017Parser = createDefinitionName_4017Parser();
		}
		return definitionName_4017Parser;
	}

	/**
	 * @generated
	 */
	protected IParser createDefinitionName_4017Parser() {
		EAttribute[] features = new EAttribute[] { FractalPackage.eINSTANCE
				.getAbstractComponent_Name(), };
		MessageFormatParser parser = new MessageFormatParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	private IParser componentName_4016Parser;

	/**
	 * @generated
	 */
	private IParser getComponentName_4016Parser() {
		if (componentName_4016Parser == null) {
			componentName_4016Parser = createComponentName_4016Parser();
		}
		return componentName_4016Parser;
	}

	/**
	 * @generated
	 */
	protected IParser createComponentName_4016Parser() {
		EAttribute[] features = new EAttribute[] { FractalPackage.eINSTANCE
				.getAbstractComponent_Name(), };
		MessageFormatParser parser = new MessageFormatParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	private IParser componentName_4015Parser;

	/**
	 * @generated
	 */
	private IParser getComponentName_4015Parser() {
		if (componentName_4015Parser == null) {
			componentName_4015Parser = createComponentName_4015Parser();
		}
		return componentName_4015Parser;
	}

	/**
	 * @generated NOT
	 */
	protected IParser createComponentName_4015Parser() {
		EAttribute[] features = new EAttribute[] { FractalPackage.eINSTANCE
				.getAbstractComponent_MergedName(), };
		MessageFormatParser parser = new MessageFormatParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	private IParser contentClass_4010Parser;

	/**
	 * @generated
	 */
	private IParser getContentClass_4010Parser() {
		if (contentClass_4010Parser == null) {
			contentClass_4010Parser = createContentClass_4010Parser();
		}
		return contentClass_4010Parser;
	}

	/**
	 * @generated NOT
	 */
	protected IParser createContentClass_4010Parser() {
		EAttribute[] features = new EAttribute[] { FractalPackage.eINSTANCE
				.getContent_MergedClass(), };
		MessageFormatParser parser = new MessageFormatParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	private IParser attributesControllerSignature_4012Parser;

	/**
	 * @generated
	 */
	private IParser getAttributesControllerSignature_4012Parser() {
		if (attributesControllerSignature_4012Parser == null) {
			attributesControllerSignature_4012Parser = createAttributesControllerSignature_4012Parser();
		}
		return attributesControllerSignature_4012Parser;
	}

	/**
	 * @generated NOT
	 */
	protected IParser createAttributesControllerSignature_4012Parser() {
		EAttribute[] features = new EAttribute[] { FractalPackage.eINSTANCE
				.getAttributesController_MergedSignature(), };
		MessageFormatParser parser = new MessageFormatParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	private IParser attributeName_4011Parser;

	/**
	 * @generated
	 */
	private IParser getAttributeName_4011Parser() {
		if (attributeName_4011Parser == null) {
			attributeName_4011Parser = createAttributeName_4011Parser();
		}
		return attributeName_4011Parser;
	}

	/**
	 * @generated NOT
	 */
	protected IParser createAttributeName_4011Parser() {
		EAttribute[] features = new EAttribute[] { FractalPackage.eINSTANCE
				.getAttribute_MergedName(), };
		MessageFormatParser parser = new MessageFormatParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	private IParser controllerType_4013Parser;

	/**
	 * @generated
	 */
	private IParser getControllerType_4013Parser() {
		if (controllerType_4013Parser == null) {
			controllerType_4013Parser = createControllerType_4013Parser();
		}
		return controllerType_4013Parser;
	}

	/**
	 * @generated NOT
	 */
	protected IParser createControllerType_4013Parser() {
		EAttribute[] features = new EAttribute[] { FractalPackage.eINSTANCE
				.getController_MergedType(), };
		MessageFormatParser parser = new MessageFormatParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	private IParser virtualNodeName_4014Parser;

	/**
	 * @generated
	 */
	private IParser getVirtualNodeName_4014Parser() {
		if (virtualNodeName_4014Parser == null) {
			virtualNodeName_4014Parser = createVirtualNodeName_4014Parser();
		}
		return virtualNodeName_4014Parser;
	}

	/**
	 * @generated NOT
	 */
	protected IParser createVirtualNodeName_4014Parser() {
		EAttribute[] features = new EAttribute[] { FractalPackage.eINSTANCE
				.getVirtualNode_MergedName(), };
		MessageFormatParser parser = new MessageFormatParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	private IParser contentClass_4002Parser;

	/**
	 * @generated
	 */
	private IParser getContentClass_4002Parser() {
		if (contentClass_4002Parser == null) {
			contentClass_4002Parser = createContentClass_4002Parser();
		}
		return contentClass_4002Parser;
	}

	/**
	 * @generated NOT
	 */
	protected IParser createContentClass_4002Parser() {
		EAttribute[] features = new EAttribute[] { FractalPackage.eINSTANCE
				.getContent_Class(), };
		ContentParser parser = new ContentParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	private IParser attributesControllerSignature_4005Parser;

	/**
	 * @generated
	 */
	private IParser getAttributesControllerSignature_4005Parser() {
		if (attributesControllerSignature_4005Parser == null) {
			attributesControllerSignature_4005Parser = createAttributesControllerSignature_4005Parser();
		}
		return attributesControllerSignature_4005Parser;
	}

	/**
	 * @generated NOT
	 */
	protected IParser createAttributesControllerSignature_4005Parser() {
		EAttribute[] features = new EAttribute[] { FractalPackage.eINSTANCE
				.getAttributesController_Signature(), };
		AttributesControllerParser parser = new AttributesControllerParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	private IParser attributeName_4003Parser;

	/**
	 * @generated
	 */
	private IParser getAttributeName_4003Parser() {
		if (attributeName_4003Parser == null) {
			attributeName_4003Parser = createAttributeName_4003Parser();
		}
		return attributeName_4003Parser;
	}

	/**
	 * @generated
	 */
	protected IParser createAttributeName_4003Parser() {
		EAttribute[] features = new EAttribute[] { FractalPackage.eINSTANCE
				.getAttribute_Name(), };
		MessageFormatParser parser = new MessageFormatParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	private IParser attributeName_4004Parser;

	/**
	 * @generated
	 */
	private IParser getAttributeName_4004Parser() {
		if (attributeName_4004Parser == null) {
			attributeName_4004Parser = createAttributeName_4004Parser();
		}
		return attributeName_4004Parser;
	}

	/**
	 * @generated NOT
	 */
	protected IParser createAttributeName_4004Parser() {
		EAttribute[] features = new EAttribute[] { FractalPackage.eINSTANCE
				.getAttribute_MergedName(), };
		MessageFormatParser parser = new MessageFormatParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	private IParser controllerType_4006Parser;

	/**
	 * @generated
	 */
	private IParser getControllerType_4006Parser() {
		if (controllerType_4006Parser == null) {
			controllerType_4006Parser = createControllerType_4006Parser();
		}
		return controllerType_4006Parser;
	}

	/**
	 * @generated
	 */
	protected IParser createControllerType_4006Parser() {
		EAttribute[] features = new EAttribute[] { FractalPackage.eINSTANCE
				.getController_Type(), };
		MessageFormatParser parser = new MessageFormatParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	private IParser virtualNodeName_4007Parser;

	/**
	 * @generated
	 */
	private IParser getVirtualNodeName_4007Parser() {
		if (virtualNodeName_4007Parser == null) {
			virtualNodeName_4007Parser = createVirtualNodeName_4007Parser();
		}
		return virtualNodeName_4007Parser;
	}

	/**
	 * @generated
	 */
	protected IParser createVirtualNodeName_4007Parser() {
		EAttribute[] features = new EAttribute[] { FractalPackage.eINSTANCE
				.getVirtualNode_Name(), };
		MessageFormatParser parser = new MessageFormatParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	private IParser interfaceName_4008Parser;

	/**
	 * @generated
	 */
	private IParser getInterfaceName_4008Parser() {
		if (interfaceName_4008Parser == null) {
			interfaceName_4008Parser = createInterfaceName_4008Parser();
		}
		return interfaceName_4008Parser;
	}

	/**
	 * @generated
	 */
	protected IParser createInterfaceName_4008Parser() {
		EAttribute[] features = new EAttribute[] { FractalPackage.eINSTANCE
				.getInterface_Name(), };
		MessageFormatParser parser = new MessageFormatParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	private IParser interfaceName_4009Parser;

	/**
	 * @generated
	 */
	private IParser getInterfaceName_4009Parser() {
		if (interfaceName_4009Parser == null) {
			interfaceName_4009Parser = createInterfaceName_4009Parser();
		}
		return interfaceName_4009Parser;
	}

	/**
	 * @generated NOT
	 */
	protected IParser createInterfaceName_4009Parser() {
		EAttribute[] features = new EAttribute[] { FractalPackage.eINSTANCE
				.getInterface_MergedName(), };
		MessageFormatParser parser = new MessageFormatParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	protected IParser getParser(int visualID) {
		switch (visualID) {
		case DefinitionNameEditPart.VISUAL_ID:
			return getDefinitionName_4017Parser();
		case ComponentNameEditPart.VISUAL_ID:
			return getComponentName_4016Parser();
		case ContentClassEditPart.VISUAL_ID:
			return getContentClass_4002Parser();
		case AttributesControllerSignatureEditPart.VISUAL_ID:
			return getAttributesControllerSignature_4005Parser();
		case AttributeNameEditPart.VISUAL_ID:
			return getAttributeName_4003Parser();
		case AttributeName2EditPart.VISUAL_ID:
			return getAttributeName_4004Parser();
		case ControllerTypeEditPart.VISUAL_ID:
			return getControllerType_4006Parser();
		case VirtualNodeNameEditPart.VISUAL_ID:
			return getVirtualNodeName_4007Parser();
		case InterfaceNameEditPart.VISUAL_ID:
			return getInterfaceName_4008Parser();
		case InterfaceName2EditPart.VISUAL_ID:
			return getInterfaceName_4009Parser();
		case ComponentName2EditPart.VISUAL_ID:
			return getComponentName_4015Parser();
		case ContentClass2EditPart.VISUAL_ID:
			return getContentClass_4010Parser();
		case AttributesControllerSignature2EditPart.VISUAL_ID:
			return getAttributesControllerSignature_4012Parser();
		case AttributeName3EditPart.VISUAL_ID:
			return getAttributeName_4011Parser();
		case ControllerType2EditPart.VISUAL_ID:
			return getControllerType_4013Parser();
		case VirtualNodeName2EditPart.VISUAL_ID:
			return getVirtualNodeName_4014Parser();
		}
		return null;
	}

	/**
	 * @generated
	 */
	public IParser getParser(IAdaptable hint) {
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null) {
			return getParser(FractalVisualIDRegistry.getVisualID(vid));
		}
		View view = (View) hint.getAdapter(View.class);
		if (view != null) {
			return getParser(FractalVisualIDRegistry.getVisualID(view));
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (FractalElementTypes.getElement(hint) == null) {
				return false;
			}
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static class HintAdapter extends ParserHintAdapter {

		/**
		 * @generated
		 */
		private final IElementType elementType;

		/**
		 * @generated
		 */
		public HintAdapter(IElementType type, EObject object, String parserHint) {
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		 * @generated
		 */
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter)) {
				return elementType;
			}
			return super.getAdapter(adapter);
		}
	}

}
