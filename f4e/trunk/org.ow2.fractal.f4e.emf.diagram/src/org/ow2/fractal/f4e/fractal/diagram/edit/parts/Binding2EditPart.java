package org.ow2.fractal.f4e.fractal.diagram.edit.parts;

import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ComponentEditPolicy;
import org.eclipse.gef.editpolicies.FeedbackHelper;
import org.eclipse.gef.requests.GroupRequest;
import org.eclipse.gef.requests.ReconnectRequest;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITreeBranchEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.internal.editpolicies.ConnectionEditPolicy;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.diagram.custom.figures.BindingConnectionEx;
import org.ow2.fractal.f4e.diagram.custom.figures.MergedBindingConnectionEx;
import org.ow2.fractal.f4e.diagram.custom.layouts.BindingConnectionAnchorSource;
import org.ow2.fractal.f4e.diagram.custom.layouts.BindingConnectionAnchorTarget;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.Binding2ItemSemanticEditPolicy;

/**
 * EditPart for merged Bindings
 * 
 * @generated NOT
 */
public class Binding2EditPart extends ConnectionNodeEditPart implements
		ITreeBranchEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 3002;

	/**
	 * @generated
	 */
	public Binding2EditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		
		
		
		// Start yann hand modifs
		// We don't want to be able to delete the merged element.
		installEditPolicy(EditPolicy.CONNECTION_ROLE,
	            new ConnectionEditPolicy(){

					protected Command createDeleteSemanticCommand(
							GroupRequest deleteRequest) {
						// TODO Auto-generated method stub
						return null;
					}

					protected Command createDeleteViewCommand(
							GroupRequest deleteRequest) {
						// TODO Auto-generated method stub
						return null;
					}
			
		});
		
		// We don't want to be able to change the source or target connection element.
		 installEditPolicy(EditPolicy.CONNECTION_ENDPOINTS_ROLE,
		            new org.eclipse.gef.editpolicies.ConnectionEndpointEditPolicy(){

						protected FeedbackHelper getFeedbackHelper(
								ReconnectRequest request) {
							// TODO Auto-generated method stub
							return null;
						}
			 
		 });
		 
		 installEditPolicy(EditPolicy.COMPONENT_ROLE,
				  new ComponentEditPolicy(){
				    protected Command getDeleteCommand(GroupRequest request) {
				      return null;
				    }
				  });
		// End yann hand modifs
		
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new Binding2ItemSemanticEditPolicy());
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated NOT
	 */
	protected Connection createConnectionFigure() {
		return new MergedBindingConnectionEx();
	}

	/**
	 * @generated NOT
	 */
	public MergedBindingConnectionEx getPrimaryShape() {
		// Start yann hand modifs
		return (MergedBindingConnectionEx) getFigure();
		// End yann hand modifs
	}

	protected ConnectionAnchor getSourceConnectionAnchor() {
		return new BindingConnectionAnchorSource(this);
	}

	protected ConnectionAnchor getTargetConnectionAnchor() {
		return new BindingConnectionAnchorTarget(this);
	}
	
	protected void fireSelectionChanged() {
		super.fireSelectionChanged();
		
		//update the binding color when it is selected
		this.refreshForegroundColor();
	}
	
	protected void refreshForegroundColor() {
		super.refreshForegroundColor();
		
		// If the binding is selected we put it in orange
		// so it is easier to see it when there is a lot of bindings.
		if (getSelected() != EditPart.SELECTED_NONE) {
			this.setForegroundColor(BindingConnectionEx.SELECTED_COLOR);
		}else{
			this.setForegroundColor(this.getFigure().getForegroundColor());
		}
	}
}
