package org.ow2.fractal.f4e.fractal.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.gmf.runtime.notation.View;

/**
 * @generated
 */
public class BindingItemSemanticEditPolicy extends
		FractalBaseItemSemanticEditPolicy {

	
	/**
	 * @generated
	 */
	protected Command getDestroyElementCommand(DestroyElementRequest req) {
		return getGEFWrapper(new DestroyElementCommand(req));
	}
	
	protected Command getSetCommand(SetRequest req) {
		// TODO Auto-generated method stub
		return super.getSetCommand(req);
	}

	protected Command getSemanticCommand(IEditCommandRequest request) {
		// TODO Auto-generated method stub
		return super.getSemanticCommand(request);
	}

}
