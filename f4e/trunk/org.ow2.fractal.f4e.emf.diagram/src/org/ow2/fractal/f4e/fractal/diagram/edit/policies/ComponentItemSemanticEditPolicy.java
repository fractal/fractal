package org.ow2.fractal.f4e.fractal.diagram.edit.policies;

import java.util.Iterator;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.diagram.edit.commands.Interface2CreateCommand;
import org.ow2.fractal.f4e.fractal.diagram.edit.commands.InterfaceCreateCommand;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesController2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.AttributesControllerEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Component2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentAttributesEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentExtendsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentOthersEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentSubComponentsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Content2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Controller2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ControllerEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCallEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Interface2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.InterfaceEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.VirtualNode2EditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.VirtualNodeEditPart;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalVisualIDRegistry;
import org.ow2.fractal.f4e.fractal.diagram.providers.FractalElementTypes;

/**
 * @generated
 */
public class ComponentItemSemanticEditPolicy extends
		FractalBaseItemSemanticEditPolicy {


	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (FractalElementTypes.Interface_2009 == req.getElementType()) {
			if (req.getContainmentFeature() == null) {
				req.setContainmentFeature(FractalPackage.eINSTANCE
						.getAbstractComponent_Interfaces());
			}
			return getGEFWrapper(new InterfaceCreateCommand(req));
		}
		if (FractalElementTypes.Interface_2010 == req.getElementType()) {
			if (req.getContainmentFeature() == null) {
				req.setContainmentFeature(FractalPackage.eINSTANCE
						.getAbstractComponent_MergedInterfaces());
			}
			return getGEFWrapper(new Interface2CreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyElementCommand(DestroyElementRequest req) {
		CompoundCommand cc = getDestroyEdgesCommand();
		addDestroyChildNodesCommand(cc);
		addDestroyShortcutsCommand(cc);
		cc.add(getGEFWrapper(new DestroyElementCommand(req)));
		return cc.unwrap();
	}

	/**
	 * @generated NOT
	 */
	protected void addDestroyChildNodesCommand(CompoundCommand cmd) {
		View view = (View) getHost().getModel();
		EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
		if (annotation != null) {
			return;
		}
		for (Iterator it = view.getChildren().iterator(); it.hasNext();) {
			Node node = (Node) it.next();
			switch (FractalVisualIDRegistry.getVisualID(node)) {
			case InterfaceEditPart.VISUAL_ID:
				cmd.add(getDestroyElementCommand(node));
				break;
			case Interface2EditPart.VISUAL_ID:
				cmd.add(getDestroyElementCommand(node));
				break;
			case ComponentCompartmentComponentExtendsEditPart.VISUAL_ID:
				for (Iterator cit = node.getChildren().iterator(); cit
						.hasNext();) {
					Node cnode = (Node) cit.next();
					switch (FractalVisualIDRegistry.getVisualID(cnode)) {
					case DefinitionCallEditPart.VISUAL_ID:
						cmd.add(getDestroyElementCommand(cnode));
						break;
					}
				}
				break;
			case ComponentCompartmentComponentSubComponentsEditPart.VISUAL_ID:
				for (Iterator cit = node.getChildren().iterator(); cit
						.hasNext();) {
					Node cnode = (Node) cit.next();
					switch (FractalVisualIDRegistry.getVisualID(cnode)) {
					case ComponentEditPart.VISUAL_ID:
						cmd.add(getDestroyElementCommand(cnode));
						break;
					case Component2EditPart.VISUAL_ID:
						cmd.add(getDestroyElementCommand(cnode));
						break;
					}
				}
				break;
			case ComponentCompartmentComponentContentEditPart.VISUAL_ID:
				for (Iterator cit = node.getChildren().iterator(); cit
						.hasNext();) {
					Node cnode = (Node) cit.next();
					switch (FractalVisualIDRegistry.getVisualID(cnode)) {
					case ContentEditPart.VISUAL_ID:
						cmd.add(getDestroyElementCommand(cnode));
						break;
//	 					It is not possible to destroy merged element
//					case Content2EditPart.VISUAL_ID:
//						cmd.add(getDestroyElementCommand(cnode));
//						break;
					}
				}
				break;
			case ComponentCompartmentComponentAttributesEditPart.VISUAL_ID:
				for (Iterator cit = node.getChildren().iterator(); cit
						.hasNext();) {
					Node cnode = (Node) cit.next();
					switch (FractalVisualIDRegistry.getVisualID(cnode)) {
					case AttributesControllerEditPart.VISUAL_ID:
						cmd.add(getDestroyElementCommand(cnode));
						break;
//	 					It is not possible to destroy merged element
//					case AttributesController2EditPart.VISUAL_ID:
//						cmd.add(getDestroyElementCommand(cnode));
//						break;
					}
				}
				break;
			case ComponentCompartmentComponentOthersEditPart.VISUAL_ID:
				for (Iterator cit = node.getChildren().iterator(); cit
						.hasNext();) {
					Node cnode = (Node) cit.next();
					switch (FractalVisualIDRegistry.getVisualID(cnode)) {
					case ControllerEditPart.VISUAL_ID:
						cmd.add(getDestroyElementCommand(cnode));
						break;
					case VirtualNodeEditPart.VISUAL_ID:
						cmd.add(getDestroyElementCommand(cnode));
						break;
//	 					It is not possible to destroy merged element
//					case Controller2EditPart.VISUAL_ID:
//						cmd.add(getDestroyElementCommand(cnode));
//						break;
//					case VirtualNode2EditPart.VISUAL_ID:
//						cmd.add(getDestroyElementCommand(cnode));
//						break;
					}
				}
				break;
			}
		}
	}

	protected boolean shouldProceed(DestroyRequest destroyRequest) {
		// TODO Auto-generated method stub
		return super.shouldProceed(destroyRequest);
	}

	
	protected Command getDuplicateCommand(DuplicateElementsRequest req) {
		// TODO Auto-generated method stub
		return super.getDuplicateCommand(req);
	}

}
