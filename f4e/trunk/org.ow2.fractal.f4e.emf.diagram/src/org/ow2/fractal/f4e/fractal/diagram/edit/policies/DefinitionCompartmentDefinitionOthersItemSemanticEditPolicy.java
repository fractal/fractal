package org.ow2.fractal.f4e.fractal.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyRequest;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.diagram.edit.commands.Controller2CreateCommand;
import org.ow2.fractal.f4e.fractal.diagram.edit.commands.ControllerCreateCommand;
import org.ow2.fractal.f4e.fractal.diagram.edit.commands.VirtualNode2CreateCommand;
import org.ow2.fractal.f4e.fractal.diagram.edit.commands.VirtualNodeCreateCommand;
import org.ow2.fractal.f4e.fractal.diagram.providers.FractalElementTypes;

/**
 * @generated
 */
public class DefinitionCompartmentDefinitionOthersItemSemanticEditPolicy extends
		FractalBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (FractalElementTypes.VirtualNode_2008 == req.getElementType()) {
			if (req.getContainmentFeature() == null) {
				req.setContainmentFeature(FractalPackage.eINSTANCE
						.getAbstractComponent_VirtualNode());
			}
			return getGEFWrapper(new VirtualNodeCreateCommand(req));
		}
		if (FractalElementTypes.Controller_2007 == req.getElementType()) {
			if (req.getContainmentFeature() == null) {
				req.setContainmentFeature(FractalPackage.eINSTANCE
						.getAbstractComponent_Controller());
			}
			return getGEFWrapper(new ControllerCreateCommand(req));
		}
		if (FractalElementTypes.VirtualNode_2016 == req.getElementType()) {
			if (req.getContainmentFeature() == null) {
				req.setContainmentFeature(FractalPackage.eINSTANCE
						.getAbstractComponent_MergedVirtualNode());
			}
			return getGEFWrapper(new VirtualNode2CreateCommand(req));
		}
		if (FractalElementTypes.Controller_2015 == req.getElementType()) {
			if (req.getContainmentFeature() == null) {
				req.setContainmentFeature(FractalPackage.eINSTANCE
						.getAbstractComponent_MergedController());
			}
			return getGEFWrapper(new Controller2CreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	// Start yann hand modifs
	protected boolean shouldProceed(DestroyRequest destroyRequest) {
		// TODO Auto-generated method stub
		return false;
	}
	// End yann hand modifs
}
