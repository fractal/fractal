package org.ow2.fractal.f4e.fractal.diagram.edit.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LayoutManager;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeCompartmentEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CreationEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.figures.ResizableCompartmentFigure;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.CustomDragDropEditPolicy;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.FractalEditPolicies;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.ResizableCompartmentWithoutHandleEditPolicy;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.compartments.SubComponentsCompartmentsEditPolicy;
import org.ow2.fractal.f4e.diagram.custom.layouts.SubComponentContainerLayout;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.ComponentMergedCompartmentComponentSubComponentsCanonicalEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.edit.policies.ComponentMergedCompartmentComponentSubComponentsItemSemanticEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.part.Messages;

/**
 * @generated
 */
public class ComponentMergedCompartmentComponentSubComponentsEditPart extends
		ShapeCompartmentEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 5012;

	/**
	 * @generated
	 */
	public ComponentMergedCompartmentComponentSubComponentsEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	public String getCompartmentName() {
		return Messages.ComponentMergedCompartmentComponentSubComponentsEditPart_title;
	}

	/**
	 * @generated
	 */
	public IFigure createFigure() {
		ResizableCompartmentFigure result = (ResizableCompartmentFigure) super
				.createFigure();
		result.setTitleVisibility(false);
		return result;
	}

	/**
	 * @generated NOT
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		// Custom policy which hide the handle of the colapsible compartment.
		installEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE,
				new ResizableCompartmentWithoutHandleEditPolicy());
		// End yann hand modifs.
		installEditPolicy(
				EditPolicyRoles.SEMANTIC_ROLE,
				new ComponentMergedCompartmentComponentSubComponentsItemSemanticEditPolicy());
		installEditPolicy(EditPolicyRoles.CREATION_ROLE,
				new CreationEditPolicy());
		
		// Start yann hand modifs
		installEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE,
				new CustomDragDropEditPolicy(){
					// we can only move/d&d merged components hold by this container.
					protected boolean canBeDropped(EditPart dropedObject) {
						return dropedObject.getParent() == this.getHost() && (dropedObject instanceof Component2EditPart);
					}
		});
		
		// Policy to handle menu bar actions to show/hide compartments
		installEditPolicy(FractalEditPolicies.SUBCOMPONENTS_COMPARTMENTS_ROLE,
				new SubComponentsCompartmentsEditPolicy());
		// End yann hand modifs
		
		installEditPolicy(
				EditPolicyRoles.CANONICAL_ROLE,
				new ComponentMergedCompartmentComponentSubComponentsCanonicalEditPolicy());
	}

	/**
	 * @generated
	 */
	protected void setRatio(Double ratio) {
		if (getFigure().getParent().getLayoutManager() instanceof ConstrainedToolbarLayout) {
			super.setRatio(ratio);
		}
	}

	/**
	 * Custom layout to place sub components
	 */
	protected LayoutManager getLayoutManager() {
		return new SubComponentContainerLayout(this.getViewer().getVisualPartMap());
	}
}
