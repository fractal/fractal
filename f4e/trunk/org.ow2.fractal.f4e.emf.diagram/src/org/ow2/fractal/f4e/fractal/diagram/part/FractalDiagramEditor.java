package org.ow2.fractal.f4e.fractal.diagram.part;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.FreeformViewport;
import org.eclipse.draw2d.Layer;
import org.eclipse.draw2d.LayeredPane;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.KeyHandler;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.RootEditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gmf.runtime.common.ui.services.marker.MarkerNavigationService;
import org.eclipse.gmf.runtime.diagram.core.preferences.PreferencesHint;
import org.eclipse.gmf.runtime.diagram.ui.actions.ActionIds;
import org.eclipse.gmf.runtime.diagram.ui.actions.internal.ArrangeAction;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramRootEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IDiagramPreferenceSupport;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.internal.parts.DiagramGraphicalViewerKeyHandler;
import org.eclipse.gmf.runtime.diagram.ui.internal.parts.DirectEditKeyHandler;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramGraphicalViewer;
import org.eclipse.gmf.runtime.diagram.ui.parts.IDiagramGraphicalViewer;
import org.eclipse.gmf.runtime.diagram.ui.providers.DiagramContextMenuProvider;
import org.eclipse.gmf.runtime.diagram.ui.requests.ArrangeRequest;
import org.eclipse.gmf.runtime.diagram.ui.resources.editor.document.IDiagramDocument;
import org.eclipse.gmf.runtime.diagram.ui.resources.editor.document.IDocument;
import org.eclipse.gmf.runtime.diagram.ui.resources.editor.document.IDocumentProvider;
import org.eclipse.gmf.runtime.diagram.ui.resources.editor.parts.DiagramDocumentEditor;
import org.eclipse.gmf.runtime.diagram.ui.services.editpart.EditPartService;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorMatchingStrategy;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.SaveAsDialog;
import org.eclipse.ui.ide.IGotoMarker;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.ShowInContext;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;
import org.ow2.fractal.f4e.diagram.custom.router.ConnectionLayerExEx;
import org.ow2.fractal.f4e.diagram.custom.ui.outline.FractalDiagramOutlinePage;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentSubComponentsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionSubComponentsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.FractalEditPartFactory;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.SystemEditPart;
import org.ow2.fractal.f4e.fractal.diagram.navigator.FractalNavigatorItem;
import org.ow2.fractal.f4e.fractal.presentation.util.DefinitionLocatorHelper;
import org.ow2.fractal.f4e.fractal.util.SDefinitionLoadLocator;

/**
 * @generated
 */
public class FractalDiagramEditor extends DiagramDocumentEditor implements
		IGotoMarker {

	/**
	 * @generated
	 */
	public static final String ID = "org.ow2.fractal.f4e.fractal.diagram.part.FractalDiagramEditorID"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final String CONTEXT_ID = "org.ow2.fractal.f4e.fractal.diagram.ui.diagramContext"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public FractalDiagramEditor() {
		super(true);
	}

	/**
	 * @generated
	 */
	protected String getContextID() {
		return CONTEXT_ID;
	}

	/**
	 * @generated
	 */
	protected PaletteRoot createPaletteRoot(PaletteRoot existingPaletteRoot) {
		PaletteRoot root = super.createPaletteRoot(existingPaletteRoot);
		new FractalPaletteFactory().fillPalette(root);
		return root;
	}

	/**
	 * @generated
	 */
	protected PreferencesHint getPreferencesHint() {
		return FractalDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT;
	}

	/**
	 * @generated
	 */
	public String getContributorId() {
		return FractalDiagramEditorPlugin.ID;
	}

	/**
	 * @generated
	 */
	protected IDocumentProvider getDocumentProvider(IEditorInput input) {
		if (input instanceof IFileEditorInput
				|| input instanceof URIEditorInput) {
			return FractalDiagramEditorPlugin.getInstance()
					.getDocumentProvider();
		}
		return super.getDocumentProvider(input);
	}

	/**
	 * @generated
	 */
	public TransactionalEditingDomain getEditingDomain() {
		IDocument document = getEditorInput() != null ? getDocumentProvider()
				.getDocument(getEditorInput()) : null;
		if (document instanceof IDiagramDocument) {
			return ((IDiagramDocument) document).getEditingDomain();
		}
		return super.getEditingDomain();
	}

	/**
	 * @generated NOT
	 */
	protected void setDocumentProvider(IEditorInput input) {
		if (input instanceof IFileEditorInput
				|| input instanceof URIEditorInput) {

			// Start Yann hand modif
			// Add the folder which contains the current .fractald file 
			// as default location to find .fractal files.
			if (input instanceof IFileEditorInput)
				SDefinitionLoadLocator.getInstance().addHelper(
						URI.createPlatformResourceURI(
								((IFileEditorInput) input).getFile()
										.getFullPath().removeFileExtension().addFileExtension("fractal").toOSString(), true),
						new DefinitionLocatorHelper(input));

			// End Yann hand modif

			setDocumentProvider(FractalDiagramEditorPlugin.getInstance()
					.getDocumentProvider());
		} else {
			super.setDocumentProvider(input);
		}
	}

	/**
	 * @generated
	 */
	public void gotoMarker(IMarker marker) {
		MarkerNavigationService.getInstance().gotoMarker(this, marker);
	}

	/**
	 * @generated
	 */
	public boolean isSaveAsAllowed() {
		return true;
	}

	/**
	 * @generated
	 */
	public void doSaveAs() {
		performSaveAs(new NullProgressMonitor());
	}

	/**
	 * @generated
	 */
	protected void performSaveAs(IProgressMonitor progressMonitor) {
		Shell shell = getSite().getShell();
		IEditorInput input = getEditorInput();
		SaveAsDialog dialog = new SaveAsDialog(shell);
		IFile original = input instanceof IFileEditorInput ? ((IFileEditorInput) input)
				.getFile()
				: null;
		if (original != null) {
			dialog.setOriginalFile(original);
		}
		dialog.create();
		IDocumentProvider provider = getDocumentProvider();
		if (provider == null) {
			// editor has been programmatically closed while the dialog was open
			return;
		}
		if (provider.isDeleted(input) && original != null) {
			String message = NLS.bind(
					Messages.FractalDiagramEditor_SavingDeletedFile, original
							.getName());
			dialog.setErrorMessage(null);
			dialog.setMessage(message, IMessageProvider.WARNING);
		}
		if (dialog.open() == Window.CANCEL) {
			if (progressMonitor != null) {
				progressMonitor.setCanceled(true);
			}
			return;
		}
		IPath filePath = dialog.getResult();
		if (filePath == null) {
			if (progressMonitor != null) {
				progressMonitor.setCanceled(true);
			}
			return;
		}
		IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		IFile file = workspaceRoot.getFile(filePath);
		final IEditorInput newInput = new FileEditorInput(file);
		// Check if the editor is already open
		IEditorMatchingStrategy matchingStrategy = getEditorDescriptor()
				.getEditorMatchingStrategy();
		IEditorReference[] editorRefs = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage()
				.getEditorReferences();
		for (int i = 0; i < editorRefs.length; i++) {
			if (matchingStrategy.matches(editorRefs[i], newInput)) {
				MessageDialog.openWarning(shell,
						Messages.FractalDiagramEditor_SaveAsErrorTitle,
						Messages.FractalDiagramEditor_SaveAsErrorMessage);
				return;
			}
		}
		boolean success = false;
		try {
			provider.aboutToChange(newInput);
			getDocumentProvider(newInput).saveDocument(progressMonitor,
					newInput,
					getDocumentProvider().getDocument(getEditorInput()), true);
			success = true;
		} catch (CoreException x) {
			IStatus status = x.getStatus();
			if (status == null || status.getSeverity() != IStatus.CANCEL) {
				ErrorDialog.openError(shell,
						Messages.FractalDiagramEditor_SaveErrorTitle,
						Messages.FractalDiagramEditor_SaveErrorMessage, x
								.getStatus());
			}
		} finally {
			provider.changed(newInput);
			if (success) {
				setInput(newInput);
			}
		}
		if (progressMonitor != null) {
			progressMonitor.setCanceled(!success);
		}
	}

	/**
	 * @generated
	 */
	public ShowInContext getShowInContext() {
		return new ShowInContext(getEditorInput(), getNavigatorSelection());
	}

	/**
	 * @generated
	 */
	private ISelection getNavigatorSelection() {
		IDiagramDocument document = getDiagramDocument();
		if (document == null) {
			return StructuredSelection.EMPTY;
		}
		
//		if(document.getDiagram() == null || (document.getDiagram() != null && document.getDiagram().eResource() == null)){
//			return StructuredSelection.EMPTY;
//		}
		
		Diagram diagram = document.getDiagram();
		
		IFile file = WorkspaceSynchronizer.getFile(diagram.eResource());
		if (file != null) {
			FractalNavigatorItem item = new FractalNavigatorItem(diagram, file,
					false);
			return new StructuredSelection(item);
		}
		return StructuredSelection.EMPTY;
	}

	/**
	 * @generated NOT
	 */
	protected void configureGraphicalViewer() {
		super.configureGraphicalViewer();
		

	        IDiagramGraphicalViewer viewer = getDiagramGraphicalViewer();
	      
	        RootEditPart rootEP = EditPartService.getInstance().createRootEditPart(
	            getDiagram());
	        if (rootEP instanceof IDiagramPreferenceSupport) {
	            ((IDiagramPreferenceSupport) rootEP)
	                .setPreferencesHint(getPreferencesHint());
	        }

	        if (getDiagramGraphicalViewer() instanceof DiagramGraphicalViewer) {
	            ((DiagramGraphicalViewer) getDiagramGraphicalViewer())
	                .hookWorkspacePreferenceStore(getWorkspaceViewerPreferenceStore());
	        }
	        
	        viewer.setRootEditPart(rootEP);
	  
	        viewer.setEditPartFactory(EditPartService.getInstance());
	       
	        KeyHandler viewerKeyHandler = new DiagramGraphicalViewerKeyHandler(viewer)
	            .setParent(getKeyHandler());
	        viewer.setKeyHandler(new DirectEditKeyHandler(viewer)
	            .setParent(viewerKeyHandler));
	        ((FigureCanvas) viewer.getControl())
	            .setScrollBarVisibility(FigureCanvas.ALWAYS);
	    
		
		DiagramEditorContextMenuProvider provider = new DiagramEditorContextMenuProvider(
				this, getDiagramGraphicalViewer());
		getDiagramGraphicalViewer().setContextMenu(provider);
		getSite().registerContextMenu(ActionIds.DIAGRAM_EDITOR_CONTEXT_MENU,
				provider, getDiagramGraphicalViewer());
		
		DiagramRootEditPart root = (DiagramRootEditPart)getDiagramGraphicalViewer().getRootEditPart();
		
				LayeredPane printableLayers = (LayeredPane) root
		.getLayer(LayerConstants.PRINTABLE_LAYERS);
		Layer connlayer = printableLayers
		.getLayer(LayerConstants.CONNECTION_LAYER);
//		Layer primlayer = printableLayers
//		.getLayer(LayerConstants.PRIMARY_LAYER);
//		
//		printableLayers.removeLayer(LayerConstants.CONNECTION_LAYER);
//		printableLayers.addLayerAfter(connlayer,
//				LayerConstants.CONNECTION_LAYER, printableLayers
//						.getLayer(LayerConstants.PRIMARY_LAYER));
		
		// Add custom layer to initialize custom connection router
		//FractalEditPartFactory.setupConnectionLayerExEx((DiagramRootEditPart)getDiagramGraphicalViewer().getRootEditPart());
	}

    
    public Object getAdapter(Class type) {
        if (type == IContentOutlinePage.class) {
            return new FractalDiagramOutlinePage(this);
        }else{
        	return super.getAdapter(type);
        }
    }

	protected void initializeGraphicalViewerContents() {
		// TODO Auto-generated method stub
		super.initializeGraphicalViewerContents();
		
		IPath diagramFilePath = new Path(this.getDiagram().eResource().getURI().toPlatformString(true));
		IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(diagramFilePath);
		try{
		IMarker markers[] = file.findMarkers("org.ow2.fractal.f4e.gmf.resource", false, IResource.DEPTH_INFINITE);
		if(markers.length > 0){
				arrangeAll();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	private void arrangeAll(){
		ArrangeRequest arrangeRequest = new 
		ArrangeRequest(ActionIds.ACTION_ARRANGE_ALL);

		    SystemEditPart rootEP = (SystemEditPart) 
		    getDiagramGraphicalViewer().getRootEditPart().getContents();
		    
		    IGraphicalEditPart componentsContainer =
		    ((DefinitionEditPart)rootEP.getChildren().get(0)).
		    getChildBySemanticHint(FractalVisualIDRegistry
					.getType(DefinitionCompartmentDefinitionSubComponentsEditPart.VISUAL_ID));

		    
		    Iterator iterator = componentsContainer.getChildren().iterator();
		    while(iterator.hasNext()){
		    	IGraphicalEditPart next =( IGraphicalEditPart)iterator.next();
		    	FractalDiagramEditorUtil.arrange(next);
		    }
		    
		    List l = new ArrayList();

		    l.add( componentsContainer);
		   
		    arrangeRequest.setPartsToArrange(l);


		    Command cmd = componentsContainer.getCommand(arrangeRequest);


		    getCommandStack().execute(cmd);
	}
}
