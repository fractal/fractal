package org.ow2.fractal.f4e.fractal.diagram.navigator;

import org.eclipse.jface.viewers.ViewerSorter;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalVisualIDRegistry;

/**
 * @generated
 */
public class FractalNavigatorSorter extends ViewerSorter {

	/**
	 * @generated
	 */
	private static final int GROUP_CATEGORY = 5018;

	/**
	 * @generated
	 */
	public int category(Object element) {
		if (element instanceof FractalNavigatorItem) {
			FractalNavigatorItem item = (FractalNavigatorItem) element;
			return FractalVisualIDRegistry.getVisualID(item.getView());
		}
		return GROUP_CATEGORY;
	}

}
