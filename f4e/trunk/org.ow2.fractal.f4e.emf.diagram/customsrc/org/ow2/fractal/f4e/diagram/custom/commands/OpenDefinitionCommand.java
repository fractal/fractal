package org.ow2.fractal.f4e.diagram.custom.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalDiagramEditorUtil;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.util.IFractalResource;
import org.ow2.fractal.f4e.fractal.util.visitor.UpdateMergeVisitor;

public class OpenDefinitionCommand extends AbstractTransactionalCommand{
	private Definition definition;
	
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		
		//URI uri = SDefinitionLoadLocator.getInstance().getLoadLocation(definition.eResource().getURI(),definition);
		URI diagramURI = definition.eResource().getURI().trimFileExtension().appendFileExtension("fractal_diagram");
	
		Resource resource = FractalTransactionalEditingDomain.getEditingDomain().getResourceSet().createResource(diagramURI);
		
		try{
			FractalDiagramEditorUtil.openDiagram(resource);
		}catch(Exception e){
			e.printStackTrace();
		}
		return CommandResult.newOKCommandResult();
	}

	public OpenDefinitionCommand(Definition definition) {
	      super(FractalTransactionalEditingDomain.getEditingDomain(), "Open definition diagram", null);
	      this.definition = definition;
	   }
}
