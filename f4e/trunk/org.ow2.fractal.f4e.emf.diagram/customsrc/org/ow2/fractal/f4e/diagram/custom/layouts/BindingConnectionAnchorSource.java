package org.ow2.fractal.f4e.diagram.custom.layouts;

import org.eclipse.draw2d.AbstractConnectionAnchor;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.Role;
import org.ow2.fractal.f4e.fractal.adapter.helper.HelperAdapter;


/**
 * 
 * @author Yann Davin
 *
 */
public class BindingConnectionAnchorSource extends AbstractConnectionAnchor implements
		ConnectionAnchor {
	ConnectionEditPart bindingEditPart;
	
	public BindingConnectionAnchorSource(ConnectionEditPart bindingEditPart) {
		super(((GraphicalEditPart)bindingEditPart.getSource()).getFigure());
		this.bindingEditPart = bindingEditPart;
	}

	public Point getLocation(Point reference) {
		return this.getReferencePoint();
	}

	public Point getReferencePoint() {
		if(getOwner() == null){
			return null;
		}
		GraphicalEditPart sourceEditPart =(GraphicalEditPart)bindingEditPart.getSource();
		GraphicalEditPart targetEditPart = (GraphicalEditPart)bindingEditPart.getTarget();
		
		IFigure source =  sourceEditPart.getFigure();
		IFigure target = targetEditPart!=null?targetEditPart.getFigure():null;
		
		if(source == null || target == null){
			return null;
		}
		
		Interface sourceItf = ((Interface)((View)sourceEditPart.getModel()).getElement());
		Interface targetItf = ((Interface)((View)targetEditPart.getModel()).getElement());
		
		Point ref = source.getBounds().getCenter();
		
		Role sourceRole = HelperAdapter.isMergedElement(sourceItf)?
				sourceItf.getRole():
					sourceItf.getMergedRole();
				
		// A binding between two interfaces of the same component
		if(sourceItf.eContainer() == targetItf.eContainer()){
			if(sourceRole == Role.CLIENT){
				// TODO remove the constant, calculate it from the figure
				ref.x = source.getBounds().x;
			}else{
				ref.x = source.getBounds().x + source.getBounds().width - 15;
			}
		}else if(targetItf.eContainer()!= null && sourceItf.eContainer() ==targetItf.eContainer().eContainer()){
			ref.x = source.getBounds().x;
		}else{
			ref.x = source.getBounds().x + source.getBounds().width - 15;
		}
		
		source.translateToAbsolute(ref);
		return ref;
	}

}
