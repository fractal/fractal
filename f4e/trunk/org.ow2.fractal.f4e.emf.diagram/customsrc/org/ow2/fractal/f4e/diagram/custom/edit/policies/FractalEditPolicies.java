package org.ow2.fractal.f4e.diagram.custom.edit.policies;

/**
 * Declaration of the customized edit policies identifiers related
 * to the custom edit policies.
 * 
 * @author Yann Davin
 *
 */
public interface FractalEditPolicies {
	public static final String INTERFACE_LABELS_ROLE = "InterfaceLabelsRole";
	public static final String CONTENT_COMPARTMENTS_ROLE = "ContentCompartmentsRole";
	public static final String SUBCOMPONENTS_COMPARTMENTS_ROLE = "SubComponentsCompartmentsRole";
	public static final String ATTRIBUTES_CONTROLLER_COMPARTMENTS_ROLE = "AttributesControllerCompartmentsRole";
	public static final String OTHERS_COMPARTMENTS_ROLE = "OthersCompartmentsRole";
}
