package org.ow2.fractal.f4e.diagram.custom.providers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.graph.DirectedGraph;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.gmf.runtime.draw2d.ui.graph.GMFDirectedGraphLayout;
import org.ow2.fractal.f4e.diagram.custom.edit.parts.AbstractComponentEditPart;
import org.ow2.fractal.f4e.diagram.custom.layouts.InterfacesSize;

public class FractalDirectedGraphLayout extends GMFDirectedGraphLayout {	
	public FractalDirectedGraphLayout() {
		super();
	}

	
	public void postProcessGraph(DirectedGraph graph) {
		super.postProcessGraph(graph);
		Iterator<Node> nodes = graph.nodes.iterator();
		int x = 0;
		int space = 20;
		while(nodes.hasNext()){
			Node node = nodes.next();
			if(node.data instanceof AbstractComponentEditPart){
				AbstractComponentEditPart nodeEditPart = (AbstractComponentEditPart)node.data;
				Dimension preferedSize = nodeEditPart.getMainFigure().getPreferredSize();
				InterfacesSize interfacesSize = nodeEditPart.getInterfacesSize();
				node.width =preferedSize.width;
				node.height=preferedSize.height;
				
				node.x = x + interfacesSize.maxServerInterfaceLength;
				x+=node.width+interfacesSize.maxClientInterfaceLength + interfacesSize.maxServerInterfaceLength + space;
			}
		}
	}
	
	public InterfacesSize getNodeSizeWithInterfaces(Node node){
		InterfacesSize result = new InterfacesSize(0,0);
		if(node.data instanceof AbstractComponentEditPart){
			result = ((AbstractComponentEditPart)node.data).getInterfacesSize();
		}
		return result;
	}
	
	public void moveConflictNode(DirectedGraph graph, Node node){
		// TODO
	}
	
	public List<Node> getConflictNodes(DirectedGraph graph){
		List<Node> result = new ArrayList<Node>();
		Iterator<Node> nodes = graph.nodes.iterator();
		while(nodes.hasNext()){
			Node next = nodes.next();
			getConflictNodes(graph,next);
		}
		return result;
	}
	
	
	public List<Node> getConflictNodes(DirectedGraph graph, Node node){
		List<Node> result = new ArrayList<Node>();
		Iterator<Node> nodes = graph.nodes.iterator();
		while(nodes.hasNext()){
			Node next = nodes.next();
			if(!node.equals(next) && isInConflict(next, node)){
				result.add(next);
			}
		}
		
		return result;
	}
	
	public boolean isInConflict(Node n1, Node n2){
		if(n1.data instanceof AbstractComponentEditPart && 
		   n2.data instanceof AbstractComponentEditPart){
			return ((AbstractComponentEditPart)n1.data).isInConflict((AbstractComponentEditPart)n2.data);
		}
		return false;
	}
}
