/*
 * Copyright (c) 2005-2007 Obeo
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Obeo - initial API and implementation
 */
package org.ow2.fractal.f4e.diagram.custom.figures;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * Define a shape to represent the virtual node of a Fractal component
 * 
 * @author Yann Davin
 * 
 */
public class VirtualNodeShape extends Shape {
	public VirtualNodeShape(){
	}
	
	protected void fillShape(Graphics graphics) {
	}

	public Rectangle getBounds() {
		return super.getBounds().getCopy();
	}

	protected void outlineShape(Graphics graphics) {
	}

}
