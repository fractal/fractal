package org.ow2.fractal.f4e.diagram.custom.edit.policies;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.ow2.fractal.f4e.diagram.custom.ui.handles.AttributesCollapseHandle;
import org.ow2.fractal.f4e.diagram.custom.ui.handles.ContentCollapseHandle;
import org.ow2.fractal.f4e.diagram.custom.ui.handles.OthersCollapseHandle;
import org.ow2.fractal.f4e.diagram.custom.ui.handles.SubComponentsCollapseHandle;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentAttributesEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentOthersEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentSubComponentsEditPart;

/**
 * Customization of the edit policy in order to add 
 * the show/hide custom handles for the sub compartments.
 * 
 * @author Yann Davin
 *
 */
public class MergedComponentResizableEditPolicy extends AbstractComponentResizableEditPolicy {
	
	/**
	 * Creates a new vertical ResizableCompartmentEditPolicy
	 */
	public MergedComponentResizableEditPolicy() {
		super();
	}
	/**
	 * This method is used to get the collapse handle(s). Subclasses can
	 * override to provide different collapse handles
	 * 
	 * @return a list of collapse handles
	 */
	protected List createCollapseHandles() {
		IGraphicalEditPart part = (IGraphicalEditPart) getHost();

		List collapseHandles = new ArrayList();
		collapseHandles.add(new SubComponentsCollapseHandle(part,ComponentMergedCompartmentComponentSubComponentsEditPart.VISUAL_ID));
		collapseHandles.add(new ContentCollapseHandle(part,ComponentMergedCompartmentComponentContentEditPart.VISUAL_ID));
		collapseHandles.add(new AttributesCollapseHandle(part,ComponentMergedCompartmentComponentAttributesEditPart.VISUAL_ID));
		collapseHandles.add(new OthersCollapseHandle(part,ComponentMergedCompartmentComponentOthersEditPart.VISUAL_ID));
		return collapseHandles;
	}

}
