package org.ow2.fractal.f4e.diagram.custom.layouts;

public interface IFractalLayout {
	// constant for maximum label length
	public static int MAX_LABEL_LENGTH=50000;
}
