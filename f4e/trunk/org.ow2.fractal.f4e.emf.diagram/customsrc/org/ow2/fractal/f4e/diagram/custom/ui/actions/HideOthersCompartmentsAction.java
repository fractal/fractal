package org.ow2.fractal.f4e.diagram.custom.ui.actions;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.ui.IWorkbenchPage;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.l10n.FractalDiagramUIActionsPluginImages;
import org.ow2.fractal.f4e.diagram.custom.ui.requests.FractalRequestConstants;
import org.ow2.fractal.f4e.diagram.custom.ui.requests.ToggleCompartmentsRequest;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentOthersEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentOthersEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionOthersEditPart;

public class HideOthersCompartmentsAction extends ShowHideCompartmentsAction {

	public HideOthersCompartmentsAction(IWorkbenchPage workbenchPage) {
		super(workbenchPage);
	}
	
	protected boolean isValid(EditPart editPart) {
		return 	editPart instanceof ComponentCompartmentComponentOthersEditPart ||
				editPart instanceof DefinitionCompartmentDefinitionOthersEditPart ||
				editPart instanceof ComponentMergedCompartmentComponentOthersEditPart;
	}
	
	public void init() {
		super.init();
		setText(FractalDiagramUIActionsMessages.HideOthersCompartmentsAction_label);
		setId(FractalActionIds.ACTION_HIDE_OTHERS_COMPARTMENTS);
		setToolTipText(FractalDiagramUIActionsMessages.HideOthersCompartmentsAction_toolTip);
		setImageDescriptor(FractalDiagramUIActionsPluginImages.DESC_HIDE_OTHERS_COMPARTMENTS);
		setDisabledImageDescriptor(FractalDiagramUIActionsPluginImages.DESC_HIDE_OTHERS_COMPARTMENTS_DISABLED);
	}	

	protected Request createTargetRequest() {
		return new ToggleCompartmentsRequest(false,FractalRequestConstants.REQ_TOGGLE_OTHERS_COMPARMENTS);
	}
}
