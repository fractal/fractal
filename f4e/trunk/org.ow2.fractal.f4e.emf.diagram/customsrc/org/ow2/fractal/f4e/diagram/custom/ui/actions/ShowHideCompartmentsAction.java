package org.ow2.fractal.f4e.diagram.custom.ui.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gmf.runtime.diagram.ui.actions.DiagramAction;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IWorkbenchPage;
import org.ow2.fractal.f4e.diagram.custom.edit.parts.InterfaceNameEditPart;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.l10n.FractalDiagramUIActionsPluginImages;
import org.ow2.fractal.f4e.diagram.custom.ui.requests.ToggleInterfaceLabelsRequest;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionContentEditPart;

public abstract class ShowHideCompartmentsAction extends DiagramAction {

	public ShowHideCompartmentsAction(IWorkbenchPage workbenchPage) {
		super(workbenchPage);
	}
	
	protected abstract Request createTargetRequest();

	protected boolean isSelectionListener() {
		return true;
	}
	
	abstract protected boolean isValid(EditPart editPart);
	
	protected List<EditPart> getAllCompartmentsEditPart(EditPart root){
		List<EditPart> result= new ArrayList<EditPart>();
		Iterator<EditPart> iterator = root.getChildren().iterator();
		while(iterator.hasNext()){
			EditPart child = iterator.next();
			if(isValid(child)){
				result.add(child);
			}else{
				result.addAll(getAllCompartmentsEditPart(child));
			}
		}
		return result;
	}
	
	protected List createOperationSet() {
		List selection = getSelectedObjects();
				
		List<EditPart> compartments = new ArrayList<EditPart>();
		Iterator<?> iterator = selection.iterator();
		while(iterator.hasNext()){
			Object editPart = iterator.next();
			if(editPart instanceof EditPart){
				compartments.addAll(getAllCompartmentsEditPart((EditPart)editPart));
			}
		}
		
		Iterator<EditPart> selectedEPs = compartments.iterator();
		
		List targetedEPs = new ArrayList();
		while (selectedEPs.hasNext()) {
		    EditPart selectedEP = (EditPart)selectedEPs.next();
	    	targetedEPs.addAll(getTargetEditParts(selectedEP));
		}
		return targetedEPs.isEmpty() ? Collections.EMPTY_LIST : targetedEPs;
	}
	
}
