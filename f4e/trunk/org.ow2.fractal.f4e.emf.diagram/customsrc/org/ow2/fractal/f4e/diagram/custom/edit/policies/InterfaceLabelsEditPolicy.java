package org.ow2.fractal.f4e.diagram.custom.edit.policies;

import org.eclipse.core.commands.operations.IUndoableOperation;
import org.eclipse.core.resources.ICommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.AbstractEditPolicy;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.diagram.core.commands.SetPropertyCommand;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.internal.properties.Properties;
import org.eclipse.gmf.runtime.diagram.ui.l10n.DiagramUIMessages;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.notation.LayoutConstraint;
import org.eclipse.gmf.runtime.notation.Location;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.diagram.custom.edit.parts.InterfaceNameEditPart;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.FractalDiagramUIActionsMessages;
import org.ow2.fractal.f4e.diagram.custom.ui.requests.FractalRequestConstants;
import org.ow2.fractal.f4e.diagram.custom.ui.requests.ToggleInterfaceLabelsRequest;

/**
 * Edit policy that is used to understand the 
 * custom label show/hide command.
 *  
 * @author Yann Davin
 *
 */
public class InterfaceLabelsEditPolicy extends AbstractEditPolicy {

	/**
	 * Understands the RequestConstants.REQ_TOGGLE_INTERFACE_LABELS request. 
	 * @param request the request
	 * @return true if RequestConstants.REQ_TOGGLE_INTERFACE_LABELS.equals(request.getType())
	 * and false otherwise.
	 */
	public boolean understandsRequest(Request request) {
		return FractalRequestConstants.REQ_TOGGLE_INTERFACE_LABELS.equals(request
				.getType());
	}
	/**
	 * Returns a <code>Command<code> which changes the visibility for 
	 * the interface labels owned by the host.
	 * @param request the request
	 * @return the property change commands
	 */
	public Command getCommand(Request request) {
		if (FractalRequestConstants.REQ_TOGGLE_INTERFACE_LABELS.equals(request.getType())) {
			final boolean showHide = ((ToggleInterfaceLabelsRequest) request)
					.showInterfaceLabel();		
           Command command = new Command(){

			public boolean canExecute() {
				return true;
			}

			public boolean canUndo() {
				return false;
			}

			public void execute() {
				((InterfaceNameEditPart)getHost()).setVisibility(showHide);
			}

			public String getLabel() {
				return super.getLabel();
			}
        	   
           };
           
           return command;
		}
		return super.getCommand(request);
	}
    
   protected TransactionalEditingDomain getEditingDomain() {
       return ((IGraphicalEditPart) getHost()).getEditingDomain();
   }
   
	/**
	 * If the request returns an executable command the host is returned, otherwise null.
	 * @param request
	 * @return getHost() if the request is supported or null.
	 */
	public EditPart getTargetEditPart(Request request) {
		if (understandsRequest(request)) {
			Command command = getHost().getCommand(
					request);
			if (command != null && command.canExecute())
				return getHost();
		}
		return null;
	}
}