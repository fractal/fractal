package org.ow2.fractal.f4e.diagram.custom.ui;


import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.search.IJavaSearchConstants;
import org.eclipse.jdt.core.search.IJavaSearchScope;
import org.eclipse.jdt.core.search.SearchEngine;
import org.eclipse.jdt.internal.ui.dialogs.FilteredTypesSelectionDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.IProgressService;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.Role;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IAbstractComponentHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelper;



/**
 * The Class FractalCellEditor.
 */
public class FractalJavaClassCellEditor extends FractalPropertyValueCellEditor {

	public FractalJavaClassCellEditor(Composite parent, EObject modelObject, EStructuralFeature feature) {
		super(parent,modelObject, feature);
	}

	protected Object openDialogBox(Control cellEditorWindow) {
		String signature = "";	
		IProgressService service= PlatformUI.getWorkbench().getProgressService();
			IPackageFragmentRoot root= null; //fAccessorPackage.getSelectedFragmentRoot();
			
			IJavaSearchScope scope= root != null ? SearchEngine.createJavaSearchScope(new IJavaElement[] { root }) : SearchEngine.createWorkspaceScope();
			
			FilteredTypesSelectionDialog  dialog= new FilteredTypesSelectionDialog (cellEditorWindow.getShell(), false, 
				service, scope, IJavaSearchConstants.CLASS);
			dialog.setTitle(UIDialogBoxMessages.NLSJavaClass_SelectionDialog_Title); 
			dialog.setMessage(UIDialogBoxMessages.NLSJavaClass_SelectionDialog_Select); 
			if(getSignature()!=null){
				dialog.setInitialPattern((String)getSignature());
			}
			if (dialog.open() == Window.OK) {
				IType selectedType= (IType) dialog.getFirstResult();
				if (selectedType != null) {
					signature = selectedType.getPackageFragment().getElementName() + "." + selectedType.getElementName();
				}
			}
			
			return signature;
	}

	protected Object getSignature(){
		return ((EObject)modelObject).eGet((EStructuralFeature)feature);
	}
}
