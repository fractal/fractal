package org.ow2.fractal.f4e.diagram.custom.ui.handles;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.gef.DragTracker;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;

public class ContentCollapseHandle extends CompartmentCollapseHandle{
	static final int HORIZONTAL_POSITION = 3;
	
	public ContentCollapseHandle(IGraphicalEditPart editPart, int compartmentVisualID){
		super(editPart,compartmentVisualID);
	}
	
	protected DragTracker createDragTracker(){
		return new ContentCollapseTracker(this);
	}
	
	protected EAttribute getAttribute(){
		return org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE.getAbstractComponentStyle_CollapsedContent();
	}
		
	protected int getHorizontalPosition(){
		return HORIZONTAL_POSITION;
	}
}
