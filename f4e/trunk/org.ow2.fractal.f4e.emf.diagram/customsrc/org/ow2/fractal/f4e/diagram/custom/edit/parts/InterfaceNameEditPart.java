package org.ow2.fractal.f4e.diagram.custom.edit.parts;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.ui.editparts.CompartmentEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.internal.editpolicies.DelegatingMouseEventsEditPolicy;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.FractalEditPolicies;
import org.ow2.fractal.f4e.diagram.custom.edit.policies.InterfaceLabelsEditPolicy;

/**
 * Common super class for the interface and merged interface name.
 * The setVisibility method has been put public 
 * in order to show/hide interface label.
 * 
 * @author Yann Davin
 *
 */
public class InterfaceNameEditPart extends CompartmentEditPart{
	public InterfaceNameEditPart(EObject model) {
		super(model);
	}

	public void setVisibility(boolean vis) {
		super.setVisibility(vis);
	}
	
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(FractalEditPolicies.INTERFACE_LABELS_ROLE,
			new InterfaceLabelsEditPolicy());
	}
	
	
}
