package org.ow2.fractal.f4e.diagram.custom.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.swt.graphics.Color;

public interface IFractalShape {
	static Color WHITE = new Color(null,255,255,255);
	static Color GRAY_FOR_MERGED_ELEMENT = ColorConstants.lightGray;
	static Color LIGHT_GRAY = ColorConstants.lightGray;
	static Color DARK_GRAY = ColorConstants.darkGray;
	static Color BLACK = new Color(null,0,0,0);
	static Color RED = new Color(null,240,10,10);
	static Color GREEN = new Color(null,10,240,10);
	static Color GRAY = new Color(null,150,150,150);
}
