package org.ow2.fractal.f4e.diagram.custom.edit.policies;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.ow2.fractal.f4e.diagram.custom.ui.handles.AttributesCollapseHandle;
import org.ow2.fractal.f4e.diagram.custom.ui.handles.ContentCollapseHandle;
import org.ow2.fractal.f4e.diagram.custom.ui.handles.ExtendsCollapseHandle;
import org.ow2.fractal.f4e.diagram.custom.ui.handles.OthersCollapseHandle;
import org.ow2.fractal.f4e.diagram.custom.ui.handles.SubComponentsCollapseHandle;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionAttributesEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionExtendsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionOthersEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionSubComponentsEditPart;

/**
 * Customization of the edit policy in order to add 
 * the show/hide custom handles for the sub compartments.
 * 
 * @author Yann Davin
 *
 */
public class DefinitionResizableEditPolicy extends AbstractComponentResizableEditPolicy {
	
	/**
	 * Creates a new vertical ResizableCompartmentEditPolicy
	 */
	public DefinitionResizableEditPolicy() {
		super();
	}
	/**
	 * This method is used to get the collapse handle(s). Subclasses can
	 * override to provide different collapse handles
	 * 
	 * @return a list of collapse handles
	 */
	protected List createCollapseHandles() {
		IGraphicalEditPart part = (IGraphicalEditPart) getHost();

		List collapseHandles = new ArrayList();
		collapseHandles.add(new ExtendsCollapseHandle(part,DefinitionCompartmentDefinitionExtendsEditPart.VISUAL_ID));
		collapseHandles.add(new SubComponentsCollapseHandle(part,DefinitionCompartmentDefinitionSubComponentsEditPart.VISUAL_ID));
		collapseHandles.add(new ContentCollapseHandle(part,DefinitionCompartmentDefinitionContentEditPart.VISUAL_ID));
		collapseHandles.add(new AttributesCollapseHandle(part,DefinitionCompartmentDefinitionAttributesEditPart.VISUAL_ID));
		collapseHandles.add(new OthersCollapseHandle(part,DefinitionCompartmentDefinitionOthersEditPart.VISUAL_ID));
		return collapseHandles;
	}

}
