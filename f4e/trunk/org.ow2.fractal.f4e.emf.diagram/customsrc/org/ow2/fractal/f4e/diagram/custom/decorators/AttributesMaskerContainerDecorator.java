package org.ow2.fractal.f4e.diagram.custom.decorators;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.services.decorator.AbstractDecorator;
import org.eclipse.gmf.runtime.diagram.ui.services.decorator.IDecoratorTarget;
import org.eclipse.gmf.runtime.draw2d.ui.figures.HashedCircle;
import org.eclipse.gmf.runtime.notation.View;

public class AttributesMaskerContainerDecorator extends AbstractDecorator{
	
	public AttributesMaskerContainerDecorator(IDecoratorTarget decoratorTarget){
		super(decoratorTarget);
	}
	
	public void activate(){
		refresh();
	}
	
	public void refresh(){
		EditPart editPart = (EditPart)getDecoratorTarget().getAdapter(EditPart.class);
		View view = (View)getDecoratorTarget().getAdapter(View.class);
		
		HashedCircle circle = new HashedCircle(HashedCircle.HashType.X,20);
		circle.setBackgroundColor(ColorConstants.black);
		circle.setFill(true);
		//setDecoration(getDecoratorTarget().addShapeDecoration(circle, IDecoratorTarget.Direction.NORTH_EAST, -10, false));
		
	}
}
