package org.ow2.fractal.f4e.diagram.custom.ui.handles;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.gef.DragTracker;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;

public class ExtendsCollapseHandle extends CompartmentCollapseHandle{
	static final int HORIZONTAL_POSITION = 1;
	
	public ExtendsCollapseHandle(IGraphicalEditPart editPart, int compartmentVisualID){
		super(editPart,compartmentVisualID);
	}
	
	protected DragTracker createDragTracker(){
		return new ExtendsCollapseTracker(this);
	}
	
	protected EAttribute getAttribute(){
		return org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE.getAbstractComponentStyle_CollapsedExtends();
	}
	
	
	protected int getHorizontalPosition(){
		return HORIZONTAL_POSITION;
	}
}
