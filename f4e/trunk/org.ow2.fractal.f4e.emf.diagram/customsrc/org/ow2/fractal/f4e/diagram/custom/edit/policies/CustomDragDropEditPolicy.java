package org.ow2.fractal.f4e.diagram.custom.edit.policies;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.DragDropEditPolicy;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.Component2EditPart;

public abstract class CustomDragDropEditPolicy extends DragDropEditPolicy {
	
	protected Command getDropCommand(ChangeBoundsRequest request) {
		Iterator<EditPart> iterator = request.getEditParts().iterator();
		List<EditPart> correctEditParts = new ArrayList<EditPart>();
		while(iterator.hasNext()){
			EditPart next = iterator.next();
			if(canBeDropped(next)){
				correctEditParts.add(next);
			}
		}
		request.setEditParts(correctEditParts);
		return super.getDropCommand(request);
	}
	
	protected abstract boolean canBeDropped(EditPart dropedObject);
}
