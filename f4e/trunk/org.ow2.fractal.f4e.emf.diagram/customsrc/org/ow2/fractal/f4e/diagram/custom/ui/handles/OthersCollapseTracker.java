package org.ow2.fractal.f4e.diagram.custom.ui.handles;

import org.eclipse.emf.ecore.EAttribute;


public class OthersCollapseTracker extends CompartmentCollapseTracker{
	
	public OthersCollapseTracker(ICompartmentCollapseHandle handle){
		super(handle);
	}
	
	protected EAttribute getAttribute(){
		return org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE.getAbstractComponentStyle_CollapsedOthers();
	}
	
	protected String getCommandName() {
		return "Collapse the others compartment"; //$NON-NLS-1$
	}

	protected String getDebugName() {
		return "Collapse the others compartment tool"; //$NON-NLS-1$
	}
}
