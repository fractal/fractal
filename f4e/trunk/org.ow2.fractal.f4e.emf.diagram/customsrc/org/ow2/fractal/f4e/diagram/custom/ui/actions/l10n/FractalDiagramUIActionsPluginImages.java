package org.ow2.fractal.f4e.diagram.custom.ui.actions.l10n;

import org.eclipse.jface.resource.ImageDescriptor;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalDiagramEditorPlugin;

public class FractalDiagramUIActionsPluginImages {
	/**
	 * The icons root directory.
	 */
	private static final String PREFIX_ROOT = "icons/";
	
	private static final String PREFIX_ENABLED = PREFIX_ROOT + "ctool16/";

	public static final ImageDescriptor DESC_SHOW_INTERFACE_LABELS = create(PREFIX_ENABLED + "show_interface_labels.png");
	public static final ImageDescriptor DESC_SHOW_INTERFACE_LABELS_DISABLED = create(PREFIX_ENABLED + "show_interface_labels_disabled.png");
	public static final ImageDescriptor DESC_HIDE_INTERFACE_LABELS = create(PREFIX_ENABLED + "hide_interface_labels.png");
	public static final ImageDescriptor DESC_HIDE_INTERFACE_LABELS_DISABLED = create(PREFIX_ENABLED + "hide_interface_labels_disabled.png");
	
	public static final ImageDescriptor DESC_HIDE_SUBCOMPONENTS_COMPARTMENTS = create(PREFIX_ENABLED + "hide_subcomponents_compartments.png");
	public static final ImageDescriptor DESC_HIDE_SUBCOMPONENTS_COMPARTMENTS_DISABLED = create(PREFIX_ENABLED + "hide_subcomponents_compartments_disabled.png");
	public static final ImageDescriptor DESC_SHOW_SUBCOMPONENTS_COMPARTMENTS = create(PREFIX_ENABLED + "show_subcomponents_compartments.png");
	public static final ImageDescriptor DESC_SHOW_SUBCOMPONENTS_COMPARTMENTS_DISABLED = create(PREFIX_ENABLED + "show_subcomponents_compartments_disabled.png");
	
	public static final ImageDescriptor DESC_HIDE_ATTRIBUTES_CONTROLLER_COMPARTMENTS = create(PREFIX_ENABLED + "hide_attributes_controller_compartments.png");
	public static final ImageDescriptor DESC_HIDE_ATTRIBUTES_CONTROLLER_COMPARTMENTS_DISABLED = create(PREFIX_ENABLED + "hide_attributes_controller_compartments_disabled.png");
	public static final ImageDescriptor DESC_SHOW_ATTRIBUTES_CONTROLLER_COMPARTMENTS = create(PREFIX_ENABLED + "show_attributes_controller_compartments.png");
	public static final ImageDescriptor DESC_SHOW_ATTRIBUTES_CONTROLLER_COMPARTMENTS_DISABLED = create(PREFIX_ENABLED + "show_attributes_controller_compartments_disabled.png");
	
	public static final ImageDescriptor DESC_HIDE_CONTENT_COMPARTMENTS = create(PREFIX_ENABLED + "hide_content_compartments.png");
	public static final ImageDescriptor DESC_HIDE_CONTENT_COMPARTMENTS_DISABLED = create(PREFIX_ENABLED + "hide_content_compartments_disabled.png");
	public static final ImageDescriptor DESC_SHOW_CONTENT_COMPARTMENTS = create(PREFIX_ENABLED + "show_content_compartments.png");
	public static final ImageDescriptor DESC_SHOW_CONTENT_COMPARTMENTS_DISABLED = create(PREFIX_ENABLED + "show_content_compartments_disabled.png");
	
	public static final ImageDescriptor DESC_HIDE_OTHERS_COMPARTMENTS = create(PREFIX_ENABLED + "hide_others_compartments.png");
	public static final ImageDescriptor DESC_HIDE_OTHERS_COMPARTMENTS_DISABLED = create(PREFIX_ENABLED + "hide_others_compartments_disabled.png");
	public static final ImageDescriptor DESC_SHOW_OTHERS_COMPARTMENTS = create(PREFIX_ENABLED + "show_others_compartments.png");
	public static final ImageDescriptor DESC_SHOW_OTHERS_COMPARTMENTS_DISABLED = create(PREFIX_ENABLED + "show_others_compartments_disabled.png");
	
	public static final ImageDescriptor DESC_FRACTAL_MENU = create(PREFIX_ENABLED + "fractal_menu.png");
	public static final ImageDescriptor DESC_FRACTAL_MENU_DISABLED = create(PREFIX_ENABLED + "fractal_menu_disabled.png");
	
	/**
	 * Creates the image descriptor from the filename given.
	 * 
	 * @param imageName
	 *            the full filename of the image
	 * @return the new image descriptor
	 */
	private static ImageDescriptor create(String imageName) {
		return FractalDiagramEditorPlugin.getBundledImageDescriptor(imageName);
	}

}
