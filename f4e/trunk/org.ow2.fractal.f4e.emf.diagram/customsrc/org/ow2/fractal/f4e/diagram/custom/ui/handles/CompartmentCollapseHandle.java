package org.ow2.fractal.f4e.diagram.custom.ui.handles;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.draw2d.Cursors;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Locator;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.TreeSearch;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.handles.AbstractHandle;
import org.eclipse.gmf.runtime.diagram.core.listener.DiagramEventBroker;
import org.eclipse.gmf.runtime.diagram.core.listener.NotificationListener;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.internal.figures.CollapseFigure;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalVisualIDRegistry;

/**
 * Custom handle to collapse child compartment of definition and component.
 * Collapse handles are normally hold by the compartment, in our case
 * for usability we need that they are hold by the compartment parent. 
 * 
 * @author Yann Davin
 */
public abstract class CompartmentCollapseHandle extends AbstractHandle
	implements PropertyChangeListener, NotificationListener, ICompartmentCollapseHandle {
	
	/**
	 * Horizontal space between two handles.
	 */
	protected static final int HORIZONTAL_SPACE = 5;
	
	/** 
	 * Positions the supplied figure in its owner's top left corner offset by [1,1] 
	 */
	protected class CollapseHandleLocator implements Locator {
		public void relocate(IFigure target) {
			Rectangle theBounds = getOwnerFigure().getClientArea().getCopy();          
			getOwnerFigure().translateToAbsolute(theBounds);
            target.translateToRelative(theBounds);
            Point point = theBounds.getLocation().getCopy();
            point.x += getXPosition();
			target.setLocation(point);            
		}
	}

	/** handle figure dimension */
	public static Dimension SIZE = new Dimension(11, 11);

	/** the handle figure */
	protected CollapseFigure collapseFigure = null;

	protected abstract EAttribute getAttribute();
	
	protected IGraphicalEditPart compartmentEditPart;
	
	protected void intializeCompartmentEditPart(IGraphicalEditPart editPart, int compartmentVisualID){
		compartmentEditPart = (IGraphicalEditPart)editPart.getChildBySemanticHint(FractalVisualIDRegistry.getType(compartmentVisualID));
	}
	
	
	public IGraphicalEditPart getCompartmentEditPart(){
		return compartmentEditPart;
	}
	
	public IGraphicalEditPart getEditPart(){
		return (IGraphicalEditPart)getOwner();
	}
	/**
     * Creates a new Compartment Collapse Handle
     * 
     * @param owner
     */
    public CompartmentCollapseHandle(IGraphicalEditPart owner, int compartmentVisualID) {
    	setOwner(owner);
        setLocator(new CollapseHandleLocator());
        setCursor(Cursors.ARROW);
      
        setSize(SIZE);
        setLayoutManager(new StackLayout());
        intializeCompartmentEditPart(owner,compartmentVisualID);
        
        if (owner != null && owner.getParent() != null
            && owner.getParent() instanceof IGraphicalEditPart)
            add(collapseFigure = new CollapseFigure(((IGraphicalEditPart) owner
                .getParent()).getFigure()));
        else
            add(collapseFigure = new CollapseFigure());

        View view = owner.getNotationView();
        if (view != null) {
        	org.ow2.fractal.f4e.notation.AbstractComponentStyle style = (org.ow2.fractal.f4e.notation.AbstractComponentStyle) view
                .getStyle(org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE.getAbstractComponentStyle());
            
            if (style != null) {
               // collapseFigure.setCollapsed((Boolean)style.eGet(org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE.getAbstractComponentStyle_CollapsedSubComponentsCompartment()));
                collapseFigure.setCollapsed((Boolean)style.eGet(getAttribute()));
            	return;
            }
        }
        collapseFigure.setCollapsed(false);
    }

	/**
	 * @see org.eclipse.draw2d.IFigure#findFigureAt(int, int, TreeSearch)
	 */
	public IFigure findFigureAt(int x, int y, TreeSearch search) {
		IFigure found = super.findFigureAt(x, y, search);
		return (collapseFigure.equals(found)) ? this : found;
	}

	/**
	 * @see org.eclipse.gef.handles.AbstractHandle#createDragTracker()
	 */
	protected abstract DragTracker createDragTracker();
		//return new CompartmentCollapseTracker((IGraphicalEditPart)getOwner());
	
	/**
	 * return the x position relative to the top right corner point of the related edit part figure
	 */
	protected int getXPosition(){
		return getSize().width * getHorizontalPosition() + getHorizontalPosition()*HORIZONTAL_SPACE;
	}

	abstract protected int getHorizontalPosition();
	
	/**
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		if (getAttribute().equals(evt.getPropertyName())){
			collapseFigure.setCollapsed(
				((Boolean) evt.getNewValue()).booleanValue());
		}else if(org.eclipse.gmf.runtime.diagram.ui.internal.properties.Properties.ID_COLLAPSED.equals(evt.getPropertyName())){
			
		}
	}
	
	/**
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	public void notifyChanged(Notification notification) {
		if (getAttribute().equals(notification.getFeature())){
			collapseFigure.setCollapsed((Boolean)notification.getNewValue());
		}
	}

	/**
	 * @see org.eclipse.draw2d.IFigure#addNotify()
	 */
	public void addNotify() {
		super.addNotify();
		IGraphicalEditPart owner = (IGraphicalEditPart) getOwner();
		View view = owner.getNotationView();
		
		if (view!=null){
			getDiagramEventBroker().addNotificationListener(owner.getNotationView(),this);
		//	getDiagramEventBroker().addNotificationListener(compartmentView,CompartmentCollapseHandle.this);
		}
	}

	/**
	 * @see org.eclipse.draw2d.IFigure#removeNotify()
	 */
	public void removeNotify() {
		IGraphicalEditPart owner = (IGraphicalEditPart) getOwner();
		getDiagramEventBroker().removeNotificationListener(owner.getNotationView(),this);
		//getDiagramEventBroker().removeNotificationListener(getCompartmentEditPart().getNotationView(),this);
		super.removeNotify();
	}
	
    private DiagramEventBroker getDiagramEventBroker() {
        TransactionalEditingDomain theEditingDomain = ((IGraphicalEditPart) getOwner())
            .getEditingDomain();
        if (theEditingDomain != null) {
            return DiagramEventBroker.getInstance(theEditingDomain);
        }
        
        return null;
    }
}
