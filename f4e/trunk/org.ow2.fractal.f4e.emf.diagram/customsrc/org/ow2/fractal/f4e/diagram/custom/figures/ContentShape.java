package org.ow2.fractal.f4e.diagram.custom.figures;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * Define a figure to represent the Fractal component content
 * 
 * @author Yann Davin
 * 
 */
public class ContentShape extends MergedShape {
	
	public ContentShape(){
	}
	
	protected void fillShape(Graphics graphics) {
		graphics.setXORMode(true);
		Rectangle r = getBounds().getCopy();
		graphics.fillRectangle(r);
	}

	public Rectangle getBounds() {
		return super.getBounds().getCopy();
	}

	protected void outlineShape(Graphics graphics) {
	}

}
