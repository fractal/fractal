package org.ow2.fractal.f4e.diagram.custom.layouts;

/**
 * Class uses to return the maximal interfaces size of a component.
 * 
 * @author Yann Davin
 *
 */
public class InterfacesSize{
	public int maxClientInterfaceLength;
	public int maxServerInterfaceLength;
	
	public InterfacesSize(int maxServerInterfaceLength, int maxClientInterfaceLength){
		this.maxServerInterfaceLength = maxServerInterfaceLength;
		this.maxClientInterfaceLength = maxClientInterfaceLength;
	}
}
