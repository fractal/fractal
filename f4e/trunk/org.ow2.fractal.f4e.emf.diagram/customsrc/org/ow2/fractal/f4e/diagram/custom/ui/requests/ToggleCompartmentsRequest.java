package org.ow2.fractal.f4e.diagram.custom.ui.requests;

import org.eclipse.gef.Request;
import org.eclipse.gmf.runtime.diagram.ui.requests.RequestConstants;

public class ToggleCompartmentsRequest extends Request {

	/** show or hide flag */
	private boolean showComparment;

	/**
	 * Constructor
	 * 
	 * @param showConnectionLabel
	 *            to show/hide the labels
	 */
	
	public ToggleCompartmentsRequest(boolean showComparment, Object requestType) {
		super(requestType);
		this.showComparment = showComparment;
	}

	/**
	 * gets the show/hide flag.
	 * 
	 * @return <code>true</code> or <code>flase</code>
	 */
	public final boolean showCompartment() {
		return showComparment;
	}
}
