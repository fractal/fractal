package org.ow2.fractal.f4e.diagram.custom.figures;


import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Path;

/**
 * Figure to display custom border around component and definition figures.
 * @author Yann Davin
 *
 */
public class ComponentBorder extends LineBorder implements IFractalShape {
	protected Rectangle externalBorder = new Rectangle();
	private boolean gradient = true;
	static int BORDER_WITH = 10;
	static int GRADIENT_DELTA = 30;
	
	public ComponentBorder(){
		super(IFractalShape.LIGHT_GRAY, BORDER_WITH,Graphics.LINE_DOT);
	}
	
	public void setGradient(boolean gradient){
		this.gradient = gradient;
	}
	
	public boolean isGradient(){
		return this.gradient;
	}
	
	public void drawGradientBorder(IFigure figure, Graphics graphics, Rectangle rectangle){
		//graphics.pushState();
		Color foreground = getColor();
		Color background = new Color(null,getColor().getRed()-GRADIENT_DELTA,getColor().getGreen()-GRADIENT_DELTA,getColor().getBlue()-GRADIENT_DELTA);
		graphics.setForegroundColor(getColor());
		graphics.setBackgroundColor(background);
		
		Path topPath = new Path(null);
		topPath.moveTo(rectangle.x-1,rectangle.y);
		topPath.lineTo(rectangle.x+rectangle.width+1, rectangle.y);
		topPath.lineTo(rectangle.x+rectangle.width-BORDER_WITH, rectangle.y+BORDER_WITH);
		topPath.lineTo(rectangle.x+BORDER_WITH, rectangle.y+BORDER_WITH);
		
		Rectangle topRectangle = new Rectangle(rectangle.x, rectangle.y, rectangle.width, BORDER_WITH+1 );
		graphics.setClip(topPath);
		graphics.fillGradient(topRectangle,true);
		
		
		
		Path leftPath = new Path(null);
		leftPath.moveTo(rectangle.x,  rectangle.y);
		leftPath.lineTo(rectangle.x, rectangle.y+ rectangle.height);
		leftPath.lineTo(rectangle.x+BORDER_WITH, rectangle.y+rectangle.height-BORDER_WITH);
		leftPath.lineTo(rectangle.x+BORDER_WITH, rectangle.y+BORDER_WITH-1);
		
		Rectangle leftRectangle = new Rectangle(rectangle.x, rectangle.y, BORDER_WITH+1, rectangle.height );
		graphics.setClip(leftPath);
		graphics.fillGradient(leftRectangle,false);
		
		
		graphics.setBackgroundColor(foreground);
		graphics.setForegroundColor(background);
		Path bottomPath = new Path(null);
		bottomPath.moveTo(rectangle.x+BORDER_WITH, rectangle.y+rectangle.height-BORDER_WITH-1);
		bottomPath.lineTo(rectangle.x+rectangle.width-BORDER_WITH+1, rectangle.y+rectangle.height-BORDER_WITH);
		bottomPath.lineTo(rectangle.x+rectangle.width+1, rectangle.y+rectangle.height);
		bottomPath.lineTo(rectangle.x, rectangle.y+rectangle.height);
		
		Rectangle bottomRectangle = new Rectangle(rectangle.x, rectangle.y+rectangle.height-BORDER_WITH, rectangle.width, BORDER_WITH+1 );
		graphics.setClip(bottomPath);
		graphics.fillGradient(bottomRectangle,true);
		
		Path rigthPath = new Path(null);
		rigthPath.moveTo(rectangle.x+rectangle.width,  rectangle.y);
		rigthPath.lineTo(rectangle.x+rectangle.width, rectangle.y+ rectangle.height);
		rigthPath.lineTo(rectangle.x+rectangle.width-BORDER_WITH, rectangle.y+rectangle.height-BORDER_WITH);
		rigthPath.lineTo(rectangle.x+rectangle.width-BORDER_WITH, rectangle.y+BORDER_WITH-1);
		
		Rectangle rigthRectangle = new Rectangle(rectangle.x+rectangle.width-BORDER_WITH, rectangle.y, BORDER_WITH, rectangle.height );
		graphics.setClip(rigthPath);
		graphics.fillGradient(rigthRectangle,false);
		
		graphics.setClip(rectangle);
		graphics.setForegroundColor(IFractalShape.BLACK);
		graphics.setLineWidthFloat(1.0F);
		graphics.drawRectangle(figure.getClientArea());
		graphics.setLineWidthFloat(2.0F);
		graphics.drawRectangle(externalBorder);
		graphics.setForegroundColor(foreground);
	}
	
	public void paint(IFigure figure, Graphics graphics, Insets insets) {
		if (getColor() != null)
			graphics.setForegroundColor(getColor());
		
		externalBorder = figure.getBounds().getCopy();
		
		if(isGradient() == true){
			drawGradientBorder(figure, graphics, externalBorder);
		}else{
			graphics.drawRectangle(figure.getClientArea());
			graphics.drawRectangle(externalBorder);
		}
	}
}
