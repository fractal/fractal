package org.ow2.fractal.f4e.diagram.custom.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * Graphical figure for the container that will
 * contain Attribute elements
 * 
 * @author Yann Davin
 * 
 */
public class AttributesControllerShape extends MergedShape{
	public AttributesControllerShape(){
		setForegroundColor(WHITE);
		setBackgroundColor(LIGHT_GRAY);
	}

	public Rectangle getBounds() {
		Rectangle r = super.getBounds();
		Rectangle result = new Rectangle(r.x, r.y , r.width, r.height);
		return result;
	}

	protected void outlineShape(Graphics graphics) {
	}

}
