/**
 * <copyright>
 *
 * Copyright (c) 2002-2007 IBM Corporation and others.
 * All rights reserved.   This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   IBM - Initial API and implementation
 *
 * </copyright>
 *
 * $Id: FeatureEditorDialog.java,v 1.11 2007/03/23 17:36:45 marcelop Exp $
 * modified by Yann Davin yann.davin@gmail.com
 */
package org.ow2.fractal.f4e.diagram.custom.ui;


import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.EMFEditPlugin;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedImage;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.ItemProvider;
import org.eclipse.emf.edit.ui.EMFEditUIPlugin;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.nebula.widgets.compositetable.CompositeTable;
import org.eclipse.swt.nebula.widgets.compositetable.GridRowLayout;
import org.eclipse.swt.nebula.widgets.compositetable.IRowContentProvider;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.FormalParameter;
import org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl;
import org.ow2.fractal.f4e.fractal.provider.FractalEditPlugin;
import org.ow2.fractal.f4e.fractal.util.FractalResourceSetImpl;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.util.IDefinitionLocatorHelper;
import org.ow2.fractal.f4e.fractal.util.IFractalResource;
import org.ow2.fractal.f4e.fractal.util.SDefinitionLoadLocator;


public class FractalExtendSelectionDialog extends Dialog
{
  protected ILabelProvider filteredListLabelProvider;
  protected ComposeableAdapterFactory valuesAdapterFactory;
  protected IContentProvider valuesContentProvider;
  protected ILabelProvider valuesLabelProvider;
  protected TableViewer extendsTableViewer;
  protected ComposeableAdapterFactory choicesAdapterFactory;
  protected IContentProvider choicesContentProvider;
  protected ILabelProvider choicesLabelProvider;
  
  protected Object object;
  protected EClassifier eClassifier;
  protected String displayName;
  protected ItemProvider values;
  protected List<URI> choiceOfValues;
  protected List<String> result;
  protected boolean multiLine;
  
  protected FractalExtendSelectionDialog.TaskFilter filter;
  /**
	 * Implements a {@link ViewFilter} based on content typed in the filter
	 * field
	 */
	class TaskFilter extends ViewerFilter {

		private Pattern pattern;

		public void setFilterText(final String filterText) {
			String newText = filterText + "*"; 
			if (newText.trim().equals("")) { 
				this.pattern = null;
			} else {
				newText = newText.replace("\\", "\\\\"); 
				newText = newText.replace(".", "\\.");
				newText = newText.replace("*", ".*"); 
				newText = newText.replace("?", ".?");
				this.pattern = Pattern.compile(newText, Pattern.CASE_INSENSITIVE);
			}
		}

		
		public boolean select(final Viewer viewer, final Object parentElement, final Object element) {
			boolean returnValue = true;
			if (this.pattern != null) {
				returnValue = this.pattern.matcher(getText(element)).matches()
				|| this.pattern.matcher(getText(element)).matches();
			}
			return returnValue;
		}
	}
  
	protected String getText(Object object){
		if(object instanceof ItemProvider){
			return ((ItemProvider)object).getText();
		}
		return null;
	}
	
	protected String getTextForTextInput(Object object){
		if(object instanceof ChoiceItemProvider){
			return ((ChoiceItemProvider)object).getDefinitionName();
		}
		return "";
	}
	
  protected class ArgumentItem{
	  String name;
	  String value;
	  
	public ArgumentItem(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public String toString() {
		// TODO Auto-generated method stub
		return this.name + "=>" + this.value;
	}
	
  }
  
  protected class ExtendItem{
	  String definition;
	  List<ArgumentItem> arguments = new ArrayList<ArgumentItem>();
	  
	public ExtendItem(String definition){
		this.definition = definition;
	}
	
	public String getDefinition() {
		return definition;
	}
	
	public void setDefinition(String definition) {
		this.definition = definition;
	}
	
	public List<ArgumentItem> getArguments() {
		return arguments;
	}
	
	public void setArguments(List<ArgumentItem> arguments) {
		this.arguments = arguments;
	}

	public String toString() {
		// TODO Auto-generated method stub
		String toString = this.definition;
		
		if(arguments.size()>0){
			toString += '(';

			for(ArgumentItem argument: arguments){
				if(argument.getValue() != null){
					toString+=argument.toString() + ',';
				}
			}
			if(arguments.size()>0 && toString.charAt(toString.length()-1)==','){
				toString = toString.substring(0, toString.length()-1);
			}
			toString += ')';
		}
		return toString;
	}

  }
  
	class ChoiceItemProvider extends ItemProvider{	
		URI definitionURI;	
		
		public ChoiceItemProvider(URI uri){
				this.definitionURI = uri;
			}
		  public String getText(Object object) {
			  if(definitionURI.lastSegment().endsWith(".fractal")){
				  return definitionURI.lastSegment().substring(0, definitionURI.lastSegment().length() - ".fractal".length()) + "\t[" + definitionURI.toString() + "]";	
			  }else{
				  return definitionURI.toString();
			  }
		  }

		  public String getDefinitionName(){
			 return definitionURI.lastSegment().substring(0, definitionURI.lastSegment().length() - ".fractal".length());
		  }
		  
		  public Object getImage(Object object) {
			  // TODO Auto-generated method stub
			  String image = "full/obj16/Definition";
			  return overlayImage(object, FractalEditPlugin.INSTANCE.getImage(image));
		  }

		  protected Object overlayImage(Object object, Object image)
		  {
			  if (AdapterFactoryEditingDomain.isControlled(object))
			  {
				  List<Object> images = new ArrayList<Object>(2);
				  images.add(image);
				  images.add(EMFEditPlugin.INSTANCE.getImage("full/ovr16/ControlledObject"));
				  image = new ComposedImage(images);
			  }
			  return image;
		  }
		  
		  public URI getURI(){
			return this.definitionURI;  
		  }
	  };
	  
	  class ExtendItemProvider extends ItemProvider {
		  ExtendItem extendItem;
		  KeyListener keyListener;
		  
		  public ExtendItemProvider(ExtendItem extendItem){
			  this.extendItem = extendItem;
		  }
		  
		  public String getText(Object object) {
			 return extendItem.toString();
		  }

		  public Object getImage(Object object) {
			  // TODO Auto-generated method stub
			  String image = "full/obj16/Definition";
			  return overlayImage(object, FractalEditPlugin.INSTANCE.getImage(image));
		  }

		  protected Object overlayImage(Object object, Object image)
		  {
			  if (AdapterFactoryEditingDomain.isControlled(object))
			  {
				  List<Object> images = new ArrayList<Object>(2);
				  images.add(image);
				  images.add(EMFEditPlugin.INSTANCE.getImage("full/ovr16/ControlledObject"));
				  image = new ComposedImage(images);
			  }
			  return image;
		  }
		  
		  public ExtendItem getExtendItem(){
			  return extendItem;
		  }
	  }
	  
  protected List<URI> getAllFractalDefinitions(URI resourceURI){
	  IDefinitionLocatorHelper helper = SDefinitionLoadLocator.getInstance().getHelper(resourceURI);
	  List<URI> definitions = helper.getAllFractalDefinitions();
	  return definitions;
  }
  
  public FractalExtendSelectionDialog
    (Shell parent, 
     Object object, Object feature)
  {
    super(parent);
   
    setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX);
//    this.labelProvider = labelProvider;
//    this.object = object;
//    this.eClassifier = eClassifier;
//    this.displayName = displayName;
   // this.choiceOfValues = new ArrayList<IFile>();
      this.choiceOfValues = null;
      this.multiLine = true;
      
      this.choicesAdapterFactory = new ComposedAdapterFactory(Collections.<AdapterFactory>emptyList());
      
      this.filteredListLabelProvider = new LabelProvider() {
		public Image getImage(Object element) {
			Object adapter = choicesAdapterFactory.adapt(element, IItemLabelProvider.class);
			if(adapter != null && adapter instanceof IItemLabelProvider){
				URL url = (URL)((IItemLabelProvider)adapter).getImage(element);
				ImageDescriptor desc = ImageDescriptor.createFromURL(url);
				return desc.createImage();
				//return (Image)((IItemLabelProvider)adapter).getImage(element);
			}
			return null;
		}
		
		public String getText(Object element) {
			Object adapter = choicesAdapterFactory.adapt(element, IItemLabelProvider.class);
			if(adapter != null && adapter instanceof IItemLabelProvider){
				return ((IItemLabelProvider)adapter).getText(element);
			}
			return element.toString();
		}
	};
	
	this.choicesContentProvider = new AdapterFactoryContentProvider(choicesAdapterFactory);
	this.choicesLabelProvider = new AdapterFactoryLabelProvider(choicesAdapterFactory);
	
	
	this.filter = new FractalExtendSelectionDialog.TaskFilter();
	this.choiceOfValues = new ArrayList<URI>();
	this.choiceOfValues.addAll(getAllFractalDefinitions(((EObject)object).eResource().getURI()));

	this.valuesAdapterFactory = new ComposedAdapterFactory(Collections.<AdapterFactory>emptyList());
	values = new ItemProvider(valuesAdapterFactory);
    valuesContentProvider = new AdapterFactoryContentProvider(valuesAdapterFactory);
    this.valuesLabelProvider = new AdapterFactoryLabelProvider(valuesAdapterFactory);
    initializeValues(object);
    
  }

  /**
   * 
   * @param definition
   */
  protected void initializeValues(Object abstractComponent){
	  // We don't manage the case where the parameter name contains reference ex:
	  // 'org.test.definition(arg${1}=>3)'
	  if(abstractComponent instanceof AbstractComponentImpl){
		  for(DefinitionCall definitionCall:((AbstractComponentImpl)abstractComponent).getExtendsAST()){
			  if(definitionCall.getDefinitionName() != null){
				  String name = definitionCall.getDefinitionName().getHelper().toString();
				  ExtendItem extendItem = new ExtendItem(name);
				  if(definitionCall.getDefinition() != null){
					  for(FormalParameter parameter:definitionCall.getDefinition().getArgumentsAST()){
						 String parameterName = parameter.getName().getHelper().toString();
						 String parameterValue = definitionCall.getHelper().getValue(parameterName);
						 ArgumentItem argument = new ArgumentItem(parameterName);
						 argument.setValue(parameterValue);
						 extendItem.getArguments().add(argument);
					  }
				  }
				  values.getChildren().add(new ExtendItemProvider(extendItem));
			  }
		  }
	  }
  }
  
  protected void configureShell(Shell shell) 
  {
    super.configureShell(shell);
  }

  protected Control createDialogArea(Composite parent) 
  {
    Composite contents = (Composite)super.createDialogArea(parent);

    GridLayout contentsGridLayout = (GridLayout)contents.getLayout();
    contentsGridLayout.numColumns = 3;

    GridData contentsGridData = (GridData)contents.getLayoutData();
    contentsGridData.horizontalAlignment = SWT.FILL;
    contentsGridData.verticalAlignment = SWT.FILL;

    Composite choiceComposite = new Composite(contents, SWT.NONE);
    {
      GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
      data.horizontalAlignment = SWT.END;
      choiceComposite.setLayoutData(data);

      GridLayout layout = new GridLayout();
      data.horizontalAlignment = SWT.FILL;
      layout.marginHeight = 0;
      layout.marginWidth = 0;
      layout.numColumns = 1;
      choiceComposite.setLayout(layout);
    }

    final Label message = new Label(choiceComposite, SWT.NONE);
	message.setText(UIDialogBoxMessages.NLSFractalDefinition_SelectionDialog_Select);
	final Text filterText = new Text(choiceComposite, SWT.SINGLE | SWT.BORDER);
	filterText.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false));
	
    Label choiceLabel = new Label(choiceComposite, SWT.NONE);
    choiceLabel.setText
      (choiceOfValues == null ? 
         EMFEditUIPlugin.INSTANCE.getString("_UI_Value_label") : 
         EMFEditUIPlugin.INSTANCE.getString("_UI_Choices_label"));
    GridData choiceLabelGridData = new GridData();
    choiceLabelGridData.verticalAlignment = SWT.FILL;
    choiceLabelGridData.horizontalAlignment = SWT.FILL;
    choiceLabel.setLayoutData(choiceLabelGridData);

    final Table choiceTable = choiceOfValues == null ? null : new Table(choiceComposite, SWT.MULTI | SWT.BORDER);
    if (choiceTable != null)
    {
      GridData choiceTableGridData = new GridData();
      choiceTableGridData.widthHint = Display.getCurrent().getBounds().width / 5;
      choiceTableGridData.heightHint = Display.getCurrent().getBounds().height / 3;
      choiceTableGridData.verticalAlignment = SWT.FILL;
      choiceTableGridData.horizontalAlignment = SWT.FILL;
      choiceTableGridData.grabExcessHorizontalSpace= true;
      choiceTableGridData.grabExcessVerticalSpace= true;
      choiceTable.setLayoutData(choiceTableGridData);
    }

   
    final TableViewer choiceTableViewer = choiceOfValues == null ? null : new TableViewer(choiceTable);
    if (choiceTableViewer != null)
    {
      choiceTableViewer.setContentProvider(choicesContentProvider);
      choiceTableViewer.setLabelProvider(choicesLabelProvider);
      List<ItemProvider> listChoices = new ArrayList<ItemProvider>();
      for(final URI definitionURI: choiceOfValues){
    	  listChoices.add(new ChoiceItemProvider(definitionURI));
      }
      choiceTableViewer.setInput(new ItemProvider(listChoices));
      choiceTableViewer.addFilter(filter);
    }
    
    filterText.addKeyListener(new KeyAdapter() {
		
		public void keyPressed(final KeyEvent e) {
			if (e.keyCode == SWT.ARROW_DOWN) {
				choiceTableViewer.getControl().setFocus();
				filter.setFilterText(filterText.getText());
			}
		}

	});
    
    filterText.addModifyListener(new ModifyListener() {

		public void modifyText(final ModifyEvent e) {
			filter.setFilterText(filterText.getText());
			choiceTableViewer.refresh(false);
		}

	});
    
    // We use multi even for a single line because we want to respond to the enter key.
    //
    int style = multiLine ?
      SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER :
      SWT.MULTI | SWT.BORDER;
    final Text choiceText = choiceOfValues == null ? new Text(choiceComposite, style) : null;
    if (choiceText != null)
    {
      GridData choiceTextGridData = new GridData();
      choiceTextGridData.widthHint = Display.getCurrent().getBounds().width / 5;
      choiceTextGridData.verticalAlignment = SWT.BEGINNING;
      choiceTextGridData.horizontalAlignment = SWT.FILL;
      choiceTextGridData.grabExcessHorizontalSpace = true;
      if (multiLine)
      {
        choiceTextGridData.verticalAlignment = SWT.FILL;
        choiceTextGridData.grabExcessVerticalSpace = true;
      }
      choiceText.setLayoutData(choiceTextGridData);
    }

    Composite controlButtons = new Composite(contents, SWT.NONE);
    GridData controlButtonsGridData = new GridData();
    controlButtonsGridData.verticalAlignment = SWT.FILL;
    controlButtonsGridData.horizontalAlignment = SWT.FILL;
    controlButtons.setLayoutData(controlButtonsGridData);

    GridLayout controlsButtonGridLayout = new GridLayout();
    controlButtons.setLayout(controlsButtonGridLayout);

    new Label(controlButtons, SWT.NONE);
    
    final Button addButton = new Button(controlButtons, SWT.PUSH);
    addButton.setText(EMFEditUIPlugin.INSTANCE.getString("_UI_Add_label"));
    GridData addButtonGridData = new GridData();
    addButtonGridData.verticalAlignment = SWT.FILL;
    addButtonGridData.horizontalAlignment = SWT.FILL;
    addButton.setLayoutData(addButtonGridData);

    final Button removeButton = new Button(controlButtons, SWT.PUSH);
    removeButton.setText(EMFEditUIPlugin.INSTANCE.getString("_UI_Remove_label"));
    GridData removeButtonGridData = new GridData();
    removeButtonGridData.verticalAlignment = SWT.FILL;
    removeButtonGridData.horizontalAlignment = SWT.FILL;
    removeButton.setLayoutData(removeButtonGridData);
    
    Label spaceLabel = new Label(controlButtons, SWT.NONE);
    GridData spaceLabelGridData = new GridData();
    spaceLabelGridData.verticalSpan = 2;
    spaceLabel.setLayoutData(spaceLabelGridData);
    
    final Button upButton = new Button(controlButtons, SWT.PUSH);
    upButton.setText(EMFEditUIPlugin.INSTANCE.getString("_UI_Up_label"));
    GridData upButtonGridData = new GridData();
    upButtonGridData.verticalAlignment = SWT.FILL;
    upButtonGridData.horizontalAlignment = SWT.FILL;
    upButton.setLayoutData(upButtonGridData);

    final Button downButton = new Button(controlButtons, SWT.PUSH);
    downButton.setText(EMFEditUIPlugin.INSTANCE.getString("_UI_Down_label"));
    GridData downButtonGridData = new GridData();
    downButtonGridData.verticalAlignment = SWT.FILL;
    downButtonGridData.horizontalAlignment = SWT.FILL;
    downButton.setLayoutData(downButtonGridData);

    Composite extendsComposite = new Composite(contents, SWT.NONE);
    {
      GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
      data.horizontalAlignment = SWT.FILL;
      extendsComposite.setLayoutData(data);

      GridLayout layout = new GridLayout();
      data.horizontalAlignment = SWT.FILL;
      layout.marginHeight = 0;
      layout.marginWidth = 0;
      layout.numColumns = 1;
      extendsComposite.setLayout(layout);
    }

    Label extendsLabel = new Label(extendsComposite,SWT.NONE);
    extendsLabel.setText(EMFEditUIPlugin.INSTANCE.getString("_UI_Feature_label"));
    GridData extendsLabelGridData = new GridData();
    extendsLabelGridData.horizontalSpan = 2;
    extendsLabelGridData.horizontalAlignment = SWT.FILL;
    extendsLabelGridData.verticalAlignment = SWT.FILL;
    
   
    extendsLabel.setLayoutData(extendsLabelGridData);
    
    final Table extendsTable = new Table(extendsComposite, SWT.MULTI | SWT.BORDER);
    GridData extendsTableGridData = new GridData();
    extendsTableGridData.widthHint = Display.getCurrent().getBounds().width / 5;
    extendsTableGridData.heightHint = Display.getCurrent().getBounds().height / 3;
    extendsTableGridData.verticalAlignment = SWT.FILL;
    extendsTableGridData.horizontalAlignment = SWT.FILL;
    extendsTableGridData.grabExcessHorizontalSpace= true;
    extendsTableGridData.grabExcessVerticalSpace= true;
    extendsTable.setLayoutData(extendsTableGridData);

    extendsTableViewer = new TableViewer(extendsTable);
    extendsTableViewer.setContentProvider(valuesContentProvider);
    extendsTableViewer.setLabelProvider(valuesLabelProvider);
    extendsTableViewer.setInput(values);
   
    final CompositeTable table = new CompositeTable(extendsComposite, SWT.MULTI | SWT.BORDER);
    GridData argsTableGridData = new GridData();
    argsTableGridData.widthHint = Display.getCurrent().getBounds().width / 5;
   
    argsTableGridData.verticalAlignment = SWT.FILL;
    argsTableGridData.horizontalAlignment = SWT.FILL;
    argsTableGridData.grabExcessHorizontalSpace= true;
    argsTableGridData.grabExcessVerticalSpace= true;
    
    table.setLayoutData(argsTableGridData);
    new Header(table, SWT.NONE); // Just drop the Header and Row in that order...
    new Row(table, SWT.NONE).setExtendTableViewer(extendsTableViewer);

    table.setRunTime(true);
    table.setNumRowsInCollection(10);
    table.setMaxRowsVisible(1);
  
    table.addRowContentProvider(new IRowContentProvider() {
		public void refresh(CompositeTable sender, int currentObjectOffset, Control rowControl) {
			Row row = (Row) rowControl;
			
			//row.value.setText(swtCommitters[currentObjectOffset].last);
//			row.firstName.setText(((ExtendItemProvider)values.getElements().get(currentObjectOffset)).getExtendItem().t);
//			row.lastName.setText(swtCommitters[currentObjectOffset].last);
		}
		
    });
    
    argsTableGridData.heightHint = table.getNumRowsInCollection()*table.getRowControl().getSize().y;
    
    if (!values.getChildren().isEmpty())
    {
      extendsTableViewer.setSelection(new StructuredSelection(values.getChildren().get(0)));
    }
    
    if (choiceTableViewer != null)
    {
      choiceTableViewer.addDoubleClickListener(new IDoubleClickListener()
        {
          public void doubleClick(DoubleClickEvent event)
          {
            if (addButton.isEnabled())
            {
              addButton.notifyListeners(SWT.Selection, null);
            }
            filterText.setText(getTextForTextInput(((IStructuredSelection)event.getSelection()).getFirstElement()));
          }
        });

      choiceTableViewer.addSelectionChangedListener(new ISelectionChangedListener()
      {
        public void selectionChanged(SelectionChangedEvent event)
        {
//        if(((IStructuredSelection)event.getSelection()).size()==1){
//          filterText.setText(getText(((IStructuredSelection)event.getSelection()).getFirstElement()));
//        }
        }
      });
      
      extendsTableViewer.addDoubleClickListener(new IDoubleClickListener()
      {
        public void doubleClick(DoubleClickEvent event)
        {}
      });
      
      extendsTableViewer.addSelectionChangedListener(new ISelectionChangedListener()
      {
    	  public void selectionChanged(SelectionChangedEvent event){
    		  if (choiceTableViewer != null)
              {
    			 IStructuredSelection selection = (IStructuredSelection)extendsTableViewer.getSelection();
                
                if(!selection.isEmpty() && selection.size()>1){
                	IStructuredSelection s = new StructuredSelection(Arrays.asList(selection.getFirstElement()));
                	extendsTableViewer.setSelection(s);
                	updateArguments(extendsTableViewer, table, selection.getFirstElement());
                }else if(selection.size() == 1){
                	updateArguments(extendsTableViewer, table, selection.getFirstElement());
              	}else if(selection.isEmpty()){
              	 
              	 // table.setNumRowsInCollection(0);    
          		  table.setMaxRowsVisible(0);
          		
          		  
                }
              }
    	  }
      });
    }
    
    if (choiceText != null)
    {
      choiceText.addKeyListener(
        new KeyAdapter()
        {
  
          public void keyPressed(KeyEvent event)
          {
            if (!multiLine && (event.character == '\r' || event.character == '\n'))
            {
              try
              {
                Object value = EcoreUtil.createFromString((EDataType)eClassifier, choiceText.getText());
                values.getChildren().add(value);
                choiceText.setText("");
                extendsTableViewer.setSelection(new StructuredSelection(value));
                event.doit = false;
              }
              catch (RuntimeException exception)
              {
                // Ignore
              }
            }
            else if (event.character == '\33')
            {
              choiceText.setText("");
              event.doit = false;
            }
          }
        });
    }
        
    upButton.addSelectionListener(
      new SelectionAdapter()
      {
  
        public void widgetSelected(SelectionEvent event)
        {
          IStructuredSelection selection = (IStructuredSelection)extendsTableViewer.getSelection();
          int minIndex = 0;
          for (Iterator<?> i = selection.iterator(); i.hasNext();)
          {
            Object value = i.next();
            int index = values.getChildren().indexOf(value);
            values.getChildren().move(Math.max(index - 1, minIndex++), value);
          }
        }
      });

    downButton.addSelectionListener(
      new SelectionAdapter()
      {
     
        public void widgetSelected(SelectionEvent event)
        {
          IStructuredSelection selection = (IStructuredSelection)extendsTableViewer.getSelection();
          int maxIndex = values.getChildren().size() - selection.size();
          for (Iterator<?> i = selection.iterator(); i.hasNext();)
          {
            Object value = i.next();
            int index = values.getChildren().indexOf(value);
            values.getChildren().move(Math.min(index + 1, maxIndex++), value);
          }
        }
      });

    addButton.addSelectionListener(
      new SelectionAdapter()
      {
        // event is null when choiceTableViewer is double clicked
     
        public void widgetSelected(SelectionEvent event)
        {
          if (choiceTableViewer != null)
          {
            IStructuredSelection selection = (IStructuredSelection)choiceTableViewer.getSelection();
            for (Iterator<?> i = selection.iterator(); i.hasNext();)
            {
              Object value = i.next();
              if(value instanceof ChoiceItemProvider){ 
            	boolean containResource = false;
            	FractalResourceSetImpl resourceSet = FractalTransactionalEditingDomain.getResourceSet();
            	
            	if(FractalTransactionalEditingDomain.getResourceSet().getURIResourceMap().containsKey(((ChoiceItemProvider)value).getURI())){
            		containResource = true;
            	}
            	  
            	Resource resource = FractalTransactionalEditingDomain.getResourceSet().getResource(((ChoiceItemProvider)value).getURI(), true, false);
            	
            	if(resource instanceof IFractalResource){
            		Definition definition = ((IFractalResource)resource).getRootDefinition();
            		if(definition != null){
            			String name = definition.getName();
            			ExtendItem extendItem = new ExtendItem(name);
            			for(FormalParameter parameter: definition.getArgumentsAST()){
            				String parameterName = parameter.getName().getHelper().toString();
            				 extendItem.getArguments().add(new ArgumentItem(parameterName));
            			  }
            			values.getChildren().add(new ExtendItemProvider(extendItem));
            		}
            	}
            	if(containResource == false){
            		resource.getResourceSet().getResources().remove(resource);
            		resource = null;
            	}
                 
              }
//              if (!values.getChildren().contains(value))
//              {
              		
//              }
            }
           
           // extendsTableViewer.setSelection(selection);
          }
          else if (choiceText != null)
          {
            try
            {
              Object value = EcoreUtil.createFromString((EDataType)eClassifier, choiceText.getText());
              values.getChildren().add(value);
              choiceText.setText("");
              extendsTableViewer.setSelection(new StructuredSelection(value));
            }
            catch (RuntimeException exception)
            {
              // Ignore
            }
          }
        }
      });

    removeButton.addSelectionListener(
      new SelectionAdapter()
      {
        // event is null when featureTableViewer is double clicked 
      
        public void widgetSelected(SelectionEvent event)
        {
          IStructuredSelection selection = (IStructuredSelection)extendsTableViewer.getSelection();
          Object firstValue = null;
          for (Iterator<?> i = selection.iterator(); i.hasNext();)
          {
            Object value = i.next();
            if (firstValue == null)
            {
              firstValue = value;
            }
            values.getChildren().remove(value);
          }

          if (!values.getChildren().isEmpty())
          {
            extendsTableViewer.setSelection(new StructuredSelection(values.getChildren().get(0)));
          }

          if (choiceTableViewer != null)
          {
            choiceTableViewer.setSelection(selection);
          }
          else if (choiceText != null)
          {
            if (firstValue != null)
            {
              String value = EcoreUtil.convertToString((EDataType)eClassifier, firstValue);
              choiceText.setText(value);
            }
          }
        }
      });    
    return contents;
  }

  protected void updateArguments(TableViewer valuesTableViewer, CompositeTable argumentsTable,  Object value){
      
	  if(value instanceof ExtendItemProvider){ 
		  ExtendItemProvider item = (ExtendItemProvider)value;

		  if(item.getExtendItem().getArguments().size()==0){
			  // setting the num rows to 0 raises a red message in the widgets
			  // so we set the num to 1 and we set widget visibility to false;
			  argumentsTable.setNumRowsInCollection(1);    
			  argumentsTable.getRowControls()[0].setVisible(false);
		  }else{
			  argumentsTable.setNumRowsInCollection(item.getExtendItem().getArguments().size());
			  argumentsTable.setMaxRowsVisible(item.getExtendItem().getArguments().size());
			  argumentsTable.getRowControls()[0].setVisible(true);
		  }
	
		  argumentsTable.getParent().layout();
		  Control[] controls = argumentsTable.getRowControls();
		  int j =0;
		  for(ArgumentItem parameter: item.getExtendItem().getArguments()){
			  Row row = (Row)controls[j];
			  row.setArgument(parameter);
			  row.setExtendItemProvider(item);
			  j++;
		  }
	  }
  }
  
 
  protected void okPressed()
  {
    super.okPressed();
  }

 
  public boolean close()
  {
	  choicesContentProvider.dispose();
	  valuesContentProvider.dispose();
	  choicesLabelProvider.dispose();
	  valuesLabelProvider.dispose();
	  
    return super.close();
  }

  public String getResult()
  {
	  String result = "";
	  if(values.getChildren()!=null){
		  for(int i =0; i<values.getChildren().size();i++){
			  result+=',' + getText(values.getChildren().get(i));
		  }
	  }
	  if(result.length()>0 && result.charAt(0) == ','){
		  result=result.substring(1, result.length());
	  }
    return result;
  }
  
  private static class Header extends Composite {
		public Header(Composite parent, int style) {
			super(parent, style);
          setLayout(new GridRowLayout(new int[] { 160, 100 }, false));
			new Label(this, SWT.NULL).setText("Parameter");
			new Label(this, SWT.NULL).setText("Value");
		}
	}
	
	private static class Row extends Composite {
		public Row(Composite parent, int style) {
			super(parent, style);
			
			
          setLayout(new GridRowLayout(new int[] { 160, 100 }, false));
			name = new Text(this, SWT.NULL);
			name.setEditable(false);
			name.setEnabled(false);
			value = new Text(this, SWT.NULL);
			
			name.addKeyListener(new KeyListener(){
				public void keyPressed(KeyEvent e) {
				}

				public void keyReleased(KeyEvent e) {
				}
				
			});
			
			value.addKeyListener(new KeyListener(){
				public void keyPressed(KeyEvent e) {
				}

				public void keyReleased(KeyEvent e) {
					if(argument != null){
						argument.value = value.getText();
						
						if(extendItemProvider != null){
							Row.this.extendTreeViewer.refresh(extendItemProvider);
						}
					}
				}
				
			});
			
			this.addDisposeListener(new DisposeListener(){

				public void widgetDisposed(DisposeEvent e) {
					name.dispose();
					value.dispose();
					
				}
				
			});
		}
		
		public void setArgument(ArgumentItem argument){
			this.argument = argument;
			
			if(argument != null){
				if(argument.name != null){
					name.setText(argument.name);
				}else{
					name.setText("");
				}
				
				if(argument.value != null){
					value.setText(argument.value);
				}else{
					value.setText("");
				}
			}
		}
		
		
		public void setExtendItemProvider(ExtendItemProvider extendItemProvider){
			this.extendItemProvider = extendItemProvider;
		}
		
		public void setExtendTableViewer(TableViewer extendTreeViewer){
			this.extendTreeViewer = extendTreeViewer;
		}
		
		private ArgumentItem argument;
		private ExtendItemProvider extendItemProvider;
		public final Text name;
		public final Text value;
		private static TableViewer extendTreeViewer;
	}

}
