package org.ow2.fractal.f4e.diagram.custom.clipboard;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.gmf.runtime.emf.clipboard.core.ClipboardUtil;
import org.eclipse.gmf.runtime.emf.clipboard.core.CopyOperation;
import org.eclipse.gmf.runtime.emf.clipboard.core.IClipboardSupport;
import org.eclipse.gmf.runtime.emf.clipboard.core.OverrideCopyOperation;
import org.eclipse.gmf.runtime.emf.clipboard.core.OverridePasteChildOperation;
import org.eclipse.gmf.runtime.emf.clipboard.core.PasteAction;
import org.eclipse.gmf.runtime.emf.clipboard.core.PasteChildOperation;
import org.eclipse.gmf.runtime.emf.clipboard.core.PasteOption;
import org.eclipse.gmf.runtime.emf.core.clipboard.AbstractClipboardSupport;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.providers.internal.copypaste.ConnectorViewPasteOperation;
import org.eclipse.gmf.runtime.notation.providers.internal.copypaste.PositionalGeneralViewPasteOperation;
import org.ow2.fractal.f4e.fractal.FractalPackage;

public class ClipboardSupport extends AbstractClipboardSupport {

	public void destroy(EObject eObject) {
		DestroyElementCommand.destroy(eObject);
	}
	
	/**
	 * By default, there are no collisions in pasting.
	 * 
	 * @return the {@link PasteAction#ADD}action, always
	 */
	public PasteAction getPasteCollisionAction(EClass eClass) {
		return PasteAction.CLONE;
	}

	/**
	 * By default, the following paste options are supported:
	 * <ul>
	 * <li>{@link PasteOption#NORMAL}: always</li>
	 * <li>{@link PasteOption#PARENT}: never</li>
	 * <li>{@link PasteOption#DISTANT}: if and only only if the
	 * <code>eStructuralFeature</code> is a
	 * {@link org.eclipse.gmf.runtime.notation.View}'s reference to its semantic
	 * {@linkplain org.eclipse.gmf.runtime.notation.View#getElement() element}</li>
	 * </ul>
	 */
	public boolean hasPasteOption(EObject contextEObject,
			EStructuralFeature eStructuralFeature, PasteOption pasteOption) {
		boolean result = false;
		if (pasteOption.equals(PasteOption.NORMAL)) {
			result = true;
		} else if (pasteOption.equals(PasteOption.PARENT)) {
			//disable the copy-parent functionality completely.
			result = false;
		} else if (pasteOption.equals(PasteOption.DISTANT)) {
			if (eStructuralFeature == null) {
				result = false;
			} else {
				result = NotationPackage.eINSTANCE.getView_Element().equals(
					eStructuralFeature);
			}
		} else {
			result = false;
		}
		return false;
	}

	/**
	 * By default, transient and derived references are never copied, and
	 * containment references and the
	 * {@linkplain org.eclipse.gmf.runtime.notation.View#getElement() element}reference
	 * always are copied.
	 */
	public boolean isCopyAlways(EObject context, EReference eReference,
			Object value) {
		boolean result = false;
		
		if ((eReference.isTransient()) || (eReference.isDerived())) {
			result = false;
		} else  if(eReference.equals(NotationPackage.eINSTANCE.getView_Element())){
			result = true;
		}else{
			result = eReference.isContainment();
		}
		
		return false;
	}

	/**
	 * By default, don't provide any child paste override behaviour.
	 */
	public boolean shouldOverrideChildPasteOperation(EObject parentElement,
			EObject childEObject) {
		return (childEObject.eClass().getEPackage() == NotationPackage.eINSTANCE);
	}

	/**
	 * By default, don't provide any copy override behaviour.
	 */
	public boolean shouldOverrideCopyOperation(Collection eObjects, Map hintMap) {
		return false;
	}

	private boolean shouldAllowPaste(
			PasteChildOperation overriddenChildPasteOperation) {
		boolean result = true;
		Object object = overriddenChildPasteOperation.getParentTarget().getObject();
		if(object instanceof View){
			View view = (View)object;
			EObject eObject = view.getElement();
			
			// The diagram should contains only one Definition root element.
			// So we avoid the copy of object if the target is a the definition parent System element.
			if(eObject != null && (
					eObject.eClass() == FractalPackage.eINSTANCE.getSystem()
			)){
				result = false;
			}
		}
		return result;
	}

	/**
	 * By default, don't provide any child paste override behaviour.
	 * 
	 * @return <code>null</code>, always
	 */
	public OverridePasteChildOperation getOverrideChildPasteOperation(
			PasteChildOperation overriddenChildPasteOperation) {
		OverridePasteChildOperation result = null;
		if (shouldAllowPaste(overriddenChildPasteOperation)) {
			EObject eObject = overriddenChildPasteOperation.getEObject();
			if (eObject instanceof org.eclipse.gmf.runtime.notation.Node) {
				org.eclipse.gmf.runtime.notation.Node node = (org.eclipse.gmf.runtime.notation.Node) eObject;
				EObject element = node.getElement();
				if ((element != null)) {
					result =  new ElementPasteOperation(
						overriddenChildPasteOperation, true);
					
				} else {
					result= new ElementPasteOperation(
						overriddenChildPasteOperation, false);
				}
			} else if (eObject instanceof Edge) {
				result = new ConnectorViewPasteOperation(
					overriddenChildPasteOperation);
			}
		}
		
		return result;
	}

	/**
	 * By default, don't provide any copy override behaviour.
	 * 
	 * @return <code>null</code>, always
	 */
	public OverrideCopyOperation getOverrideCopyOperation(
			CopyOperation overriddenCopyOperation) {
		return null;
	}

	/**
	 * By default, don't exclude any objects from the copy operation.
	 * 
	 * @return an empty collection
	 */
	public Collection getExcludedCopyObjects(Set eObjects) {
		return Collections.EMPTY_SET;
	}

	/**
	 * By default, just get the resource that contains the object.
	 */
	public XMLResource getResource(EObject eObject) {
		XMLResource eResource = (XMLResource) eObject.eResource();
		if (eResource == null) {
			if (eObject instanceof View) {
				EObject element = ((View) eObject).getElement();
				if ((element != null)) {
					return (XMLResource) element.eResource();
				}
			}
		}
		return eResource;
	}

	/**
	 * By default, we always copy all contents of an object.
	 * 
	 * @return <code>true</code>
	 */
	public boolean shouldSaveContainmentFeature(EObject eObj) {
		boolean result = true;
		if (EcorePackage.eINSTANCE.getEClassifiers().contains(eObj.eClass())) {
			result= false;
		}
		try {
			eObj.eResource().getURIFragment(eObj);
		} catch (Exception ex) {
			result= false;
		}
		return false;
	}

	/**
	 * By default, there is no post processing to be done.
	 */
	public void performPostPasteProcessing(Set pastedEObjects) {
		// nothing to do
	}

	static Diagram getContainingDiagram(View view) {
		EObject current = view;
		while (current != null) {
			if (current instanceof Diagram) {
				return (Diagram) current;
			}
			current = current.eContainer();
		}
		return null;
	}

	public static EObject getSemanticPasteTarget(View view) {
		Diagram diagram = getContainingDiagram(view);
		if (diagram != null) {
			return diagram.getElement();
		}
		return null;
	}
}
