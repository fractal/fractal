package org.ow2.fractal.f4e.diagram.custom.ui.handles;

import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;

public interface ICompartmentCollapseHandle {

	/**
	 * @return the editPart of the compartment associated to this handle
	 */
	public IGraphicalEditPart getCompartmentEditPart();
	
	/**
	 * 
	 * @return the editPart which holds this handle
	 */
	public IGraphicalEditPart getEditPart();
}
