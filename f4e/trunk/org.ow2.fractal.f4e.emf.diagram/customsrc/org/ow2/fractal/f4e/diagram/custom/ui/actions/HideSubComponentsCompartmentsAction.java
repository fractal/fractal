package org.ow2.fractal.f4e.diagram.custom.ui.actions;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.ui.IWorkbenchPage;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.l10n.FractalDiagramUIActionsPluginImages;
import org.ow2.fractal.f4e.diagram.custom.ui.requests.FractalRequestConstants;
import org.ow2.fractal.f4e.diagram.custom.ui.requests.ToggleCompartmentsRequest;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentSubComponentsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentSubComponentsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionSubComponentsEditPart;

public class HideSubComponentsCompartmentsAction extends ShowHideCompartmentsAction {

	public HideSubComponentsCompartmentsAction(IWorkbenchPage workbenchPage) {
		super(workbenchPage);
	}
	
	protected boolean isValid(EditPart editPart) {
		return 	editPart instanceof ComponentCompartmentComponentSubComponentsEditPart ||
				editPart instanceof DefinitionCompartmentDefinitionSubComponentsEditPart ||
				editPart instanceof ComponentMergedCompartmentComponentSubComponentsEditPart;
	}
	
	public void init() {
		super.init();
		setText(FractalDiagramUIActionsMessages.HideSubComponentsCompartmentsAction_label);
		setId(FractalActionIds.ACTION_HIDE_SUBCOMPONENTS_COMPARTMENTS);
		setToolTipText(FractalDiagramUIActionsMessages.HideSubComponentsCompartmentsAction_toolTip);
		setImageDescriptor(FractalDiagramUIActionsPluginImages.DESC_HIDE_SUBCOMPONENTS_COMPARTMENTS);
		setDisabledImageDescriptor(FractalDiagramUIActionsPluginImages.DESC_HIDE_SUBCOMPONENTS_COMPARTMENTS_DISABLED);
	}	

	protected Request createTargetRequest() {
		return new ToggleCompartmentsRequest(false,FractalRequestConstants.REQ_TOGGLE_SUBCOMPONENTS_COMPARMENTS);
	}
}
