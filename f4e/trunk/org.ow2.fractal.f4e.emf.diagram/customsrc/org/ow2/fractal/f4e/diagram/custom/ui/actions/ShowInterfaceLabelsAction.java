package org.ow2.fractal.f4e.diagram.custom.ui.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gmf.runtime.diagram.ui.actions.DiagramAction;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeCompartmentEditPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.ToggleConnectionLabelsRequest;
import org.eclipse.ui.IWorkbenchPage;
import org.ow2.fractal.f4e.diagram.custom.edit.parts.InterfaceNameEditPart;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.l10n.FractalDiagramUIActionsPluginImages;
import org.ow2.fractal.f4e.diagram.custom.ui.requests.ToggleInterfaceLabelsRequest;

public class ShowInterfaceLabelsAction extends DiagramAction {

	public ShowInterfaceLabelsAction(IWorkbenchPage workbenchPage) {
		super(workbenchPage);
	}
	
	public void init() {
		super.init();
		setText(FractalDiagramUIActionsMessages.ShowInterfaceLabelsAction_label);
		setId(FractalActionIds.ACTION_SHOW_INTERFACE_LABELS);
		setToolTipText(FractalDiagramUIActionsMessages.ShowInterfaceLabelsAction_toolTip);
		setImageDescriptor(FractalDiagramUIActionsPluginImages.DESC_SHOW_INTERFACE_LABELS);
		setDisabledImageDescriptor(FractalDiagramUIActionsPluginImages.DESC_SHOW_INTERFACE_LABELS_DISABLED);
	}	

	protected Request createTargetRequest() {
		return new ToggleInterfaceLabelsRequest(true);
	}

	protected boolean isSelectionListener() {
		return true;
	}
	
	private List<InterfaceNameEditPart> getAllInterfaceNameEditPart(EditPart root){
		List<InterfaceNameEditPart> result= new ArrayList<InterfaceNameEditPart>();
		Iterator<EditPart> iterator = root.getChildren().iterator();
		while(iterator.hasNext()){
			EditPart child = iterator.next();
			if(child instanceof InterfaceNameEditPart){
				result.add((InterfaceNameEditPart)child);
			}else{
				result.addAll(getAllInterfaceNameEditPart(child));
			}
		}
		return result;
	}
	
	protected List createOperationSet() {
		List selection = getSelectedObjects();
		if (selection.isEmpty() || !(selection.get(0) instanceof EditPart)) {
			return Collections.EMPTY_LIST;
		} 		
		
		List<InterfaceNameEditPart> interfaceLabels = getAllInterfaceNameEditPart((EditPart)selection.get(0));
		
		Iterator<InterfaceNameEditPart> selectedEPs = interfaceLabels.iterator();
		
		List targetedEPs = new ArrayList();
		while (selectedEPs.hasNext()) {
		    EditPart selectedEP = (EditPart)selectedEPs.next();
	    	targetedEPs.addAll(getTargetEditParts(selectedEP));
		}
		return targetedEPs.isEmpty() ? Collections.EMPTY_LIST : targetedEPs;
	}
}
