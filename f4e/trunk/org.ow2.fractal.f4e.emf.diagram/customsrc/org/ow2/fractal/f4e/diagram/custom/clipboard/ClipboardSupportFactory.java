package org.ow2.fractal.f4e.diagram.custom.clipboard;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.gmf.runtime.emf.clipboard.core.ClipboardSupportUtil;
import org.eclipse.gmf.runtime.emf.clipboard.core.IClipboardSupport;
import org.eclipse.gmf.runtime.emf.clipboard.core.IClipboardSupportFactory;
import org.eclipse.gmf.runtime.emf.clipboard.core.internal.ClipboardSupportManager;

/**
 * Clipboard operation helper for the Fractal graphical editor.
 * 
 * @author Yann Davin
 *
 */
public class ClipboardSupportFactory implements IClipboardSupportFactory{
	private final IClipboardSupport support = new ClipboardSupport();

	public ClipboardSupportFactory(){
		super();
	
	}
	
	public IClipboardSupport newClipboardSupport(EPackage package1) {
		return support;
	}
	
}
