package org.ow2.fractal.f4e.diagram.custom.edit.policies;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.diagram.core.edithelpers.CreateElementRequestAdapter;
import org.eclipse.gmf.runtime.diagram.core.util.ViewUtil;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.commands.SemanticCreateCommand;
import org.eclipse.gmf.runtime.diagram.ui.editparts.INodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.GraphicalNodeEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateConnectionViewAndElementRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.EditCommandRequestWrapper;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.notation.View;

/**
 * Edit policy created in order to solve the GMF double binding bug creation.
 * 
 * @author Yann Davin
 *
 */
public class InterfaceGraphicalNodeEditPolicy extends GraphicalNodeEditPolicy{

	protected Command getConnectionAndRelationshipCompleteCommand(
			CreateConnectionViewAndElementRequest request) {
			// get the element descriptor
			CreateElementRequestAdapter requestAdapter = request
					.getConnectionViewAndElementDescriptor().getCreateElementRequestAdapter();
			// get the semantic request
			CreateRelationshipRequest createElementRequest = (CreateRelationshipRequest) requestAdapter
					.getAdapter(CreateRelationshipRequest.class);
			
			createElementRequest.setPrompt(!request.isUISupressed());
			
			// complete the semantic request by filling in the source and
			// destination
			INodeEditPart targetEP = getConnectionCompleteEditPart(request);
			View sourceView = (View)request.getSourceEditPart().getModel();
			View targetView = (View)targetEP.getModel();
			
			// resolve the source
			EObject source = ViewUtil.resolveSemanticElement(sourceView);
			if (source == null) {
				source = sourceView;
			}
			createElementRequest.setSource(source);
			
			// resolve the target
			EObject target = ViewUtil.resolveSemanticElement(targetView);
			if (target == null) {
				target = targetView;
			}
			createElementRequest.setTarget(target);
			
			// get the create element request based on the elementdescriptor's
			// request
			Command createElementCommand = targetEP
					.getCommand(new EditCommandRequestWrapper(
							(CreateRelationshipRequest) requestAdapter
									.getAdapter(CreateRelationshipRequest.class), request.getExtendedData()));
			
			// create the create semantic element wrapper command
			if (null == createElementCommand)
				return null;
			
			SemanticCreateCommand semanticCommand = new SemanticCreateCommand(
				requestAdapter, createElementCommand);
			// get the view command
			Command viewCommand = getConnectionCompleteCommand(request);
			if (null == viewCommand)
				return null;
			// form the compound command and return
			CompositeCommand cc = new CompositeCommand(semanticCommand.getLabel());
			cc.compose( semanticCommand );
			
			//cc.compose( new CommandProxy(viewCommand) );
			return new ICommandProxy(cc);
	}
}
