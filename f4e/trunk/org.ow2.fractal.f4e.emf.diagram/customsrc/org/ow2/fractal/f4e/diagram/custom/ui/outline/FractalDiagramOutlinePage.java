package org.ow2.fractal.f4e.diagram.custom.ui.outline;

import org.eclipse.emf.ecoretools.diagram.ui.outline.AbstractDiagramsOutlinePage;
import org.eclipse.emf.ecoretools.diagram.ui.outline.AbstractModelNavigator;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.IPageSite;
import org.ow2.fractal.f4e.fractal.diagram.part.FractalDiagramEditorPlugin;

/**
 * A customized outline page for rendering both a Thumbnail view of the editor
 * and/or a tree structure of the underlying model
 * 
 * @author Yann Davin
 * 
 * Copied from:
 * @author <a href="mailto:david.sciamma@anyware-tech.com">David Sciamma</a>
 * @author <a href="mailto:jacques.lescot@anyware-tech.com">Jacques LESCOT</a>
 */
public class FractalDiagramOutlinePage extends AbstractDiagramsOutlinePage {

	/**
	 * Constructor
	 * 
	 * @param editor
	 *            the Editor
	 */
	public FractalDiagramOutlinePage(DiagramEditor editor) {
		super(editor);
	}

	/**
	 * @see org.eclipse.emf.ecoretools.diagram.ui.outline.AbstractDiagramsOutlinePage#createNavigator(org.eclipse.swt.widgets.Composite,
	 *      org.eclipse.ui.part.IPageSite)
	 */
	protected AbstractModelNavigator createNavigator(Composite parent, IPageSite pageSite) {
		return new FractalModelNavigator(parent, getEditor(), pageSite);
	}

	/**
	 * @see org.eclipse.emf.ecoretools.diagram.ui.outline.AbstractDiagramsOutlinePage#getPreferenceStore()
	 */
	protected IPreferenceStore getPreferenceStore() {
		return FractalDiagramEditorPlugin.getInstance().getPreferenceStore();
	}

	/**
	 * @see org.eclipse.emf.ecoretools.diagram.ui.outline.AbstractDiagramsOutlinePage#getEditorID()
	 */
	protected String getEditorID() {
		return "org.ow2.fractal.f4e.fractal.diagram.part.FractalDiagramEditorID"; //$NON-NLS-1$
	}
}
