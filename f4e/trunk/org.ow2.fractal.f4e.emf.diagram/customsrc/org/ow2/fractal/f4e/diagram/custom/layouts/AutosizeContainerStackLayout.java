package org.ow2.fractal.f4e.diagram.custom.layouts;

import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.mapmode.IMapMode;

/**
 * Layout that adjust the vertical size of the container to the sum of
 * the size of its children.
 * 
 * It is used by the by the AttributesControllerCompartment and
 * by the ExtendsCompartment
 * 
 * @author Yann Davin
 * 
 */
public class AutosizeContainerStackLayout extends ConstrainedToolbarLayout{

	public AutosizeContainerStackLayout(){
		super();
	};
	
	protected Dimension calculatePreferredSize(IFigure figure, int wHint, int hHint) {
		if (wHint > -1)
			wHint = Math.max(0, wHint - figure.getInsets().getWidth());
		if (hHint > -1)
			hHint = Math.max(0, hHint - figure.getInsets().getHeight());
		Dimension d = new Dimension();
		List children = figure.getChildren();
		IFigure child;
		for (int i = 0; i < children.size(); i++) {
			child = (IFigure)children.get(i);
			if ( child.isVisible()){
				Dimension dCopy = d.getCopy();
				dCopy.union(child.getPreferredSize(wHint, hHint));
				d.height+=child.getMinimumSize().height;
				d.width=dCopy.width;
			}
		}
		
		return d;
	}
}
