package org.ow2.fractal.f4e.diagram.custom.figures;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * Graphical representation for the Attribute element.
 * 
 * @author Yann Davin
 * 
 */
public class AttributeShape extends MergedShape{

	protected void fillShape(Graphics graphics) {
		graphics.fillRectangle(getBounds());
	}

	public Rectangle getBounds() {
		Rectangle r = super.getBounds();
		Rectangle result = new Rectangle(r.x, r.y , r.width, r.height);
		return result;
	}

	protected void outlineShape(Graphics graphics) {
		this.setLineWidth(1);
		Rectangle r = this.getBounds();
	}
}
