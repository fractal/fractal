package org.ow2.fractal.f4e.diagram.custom.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * Graphical representation for the 
 * Fractal ADL extends element.
 * 
 * @author Yann Davin
 * 
 */
public class DefinitionCallShape extends Shape implements IFractalShape {
	
	public DefinitionCallShape(){
		setForegroundColor(ColorConstants.black);
		setBackgroundColor(WHITE);
	}
	
	protected void fillShape(Graphics graphics) {
		graphics.setXORMode(false);
	}

	public Rectangle getBounds() {
		Rectangle r = super.getBounds();
		Rectangle result = new Rectangle(r.x, r.y , r.width -1, r.height-1);
		return result;
	}
	
	protected void outlineShape(Graphics graphics) {
		this.setLineWidth(1);
	}

}
