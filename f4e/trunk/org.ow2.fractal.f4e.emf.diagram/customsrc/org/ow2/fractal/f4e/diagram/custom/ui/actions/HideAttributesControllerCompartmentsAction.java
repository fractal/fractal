package org.ow2.fractal.f4e.diagram.custom.ui.actions;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.ui.IWorkbenchPage;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.l10n.FractalDiagramUIActionsPluginImages;
import org.ow2.fractal.f4e.diagram.custom.ui.requests.FractalRequestConstants;
import org.ow2.fractal.f4e.diagram.custom.ui.requests.ToggleCompartmentsRequest;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentAttributesEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentAttributesEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionAttributesEditPart;

public class HideAttributesControllerCompartmentsAction extends ShowHideCompartmentsAction {

	/**
	 * @param workbenchPage
	 */
	public HideAttributesControllerCompartmentsAction(IWorkbenchPage workbenchPage) {
		super(workbenchPage);
	}
	
	protected boolean isValid(EditPart editPart) {
		return 	editPart instanceof ComponentCompartmentComponentAttributesEditPart ||
				editPart instanceof DefinitionCompartmentDefinitionAttributesEditPart ||
				editPart instanceof ComponentMergedCompartmentComponentAttributesEditPart;
	}
	
	/**
	 * Add text and image descriptors.
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#init()
	 */
	public void init() {
		super.init();
		setText(FractalDiagramUIActionsMessages.HideAttributesControllerCompartmentsAction_label);
		setId(FractalActionIds.ACTION_HIDE_ATTRIBUTES_CONTROLLER_COMPARTMENTS);
		setToolTipText(FractalDiagramUIActionsMessages.HideAttributesControllerCompartmentsAction_toolTip);
		setImageDescriptor(FractalDiagramUIActionsPluginImages.DESC_HIDE_ATTRIBUTES_CONTROLLER_COMPARTMENTS);
		setDisabledImageDescriptor(FractalDiagramUIActionsPluginImages.DESC_HIDE_ATTRIBUTES_CONTROLLER_COMPARTMENTS_DISABLED);
	}	

	/**
	 * Returns an instance of <code>ToggleConnectionLabelsRequest</code>
	 * 
	 * @return the request
	 * @see org.eclipse.gmf.runtime.diagram.ui.actions.DiagramAction#createTargetRequest()
	 */
	protected Request createTargetRequest() {
		return new ToggleCompartmentsRequest(false,FractalRequestConstants.REQ_TOGGLE_ATTRIBUTES_CONTROLLER_COMPARMENTS);
	}
}
