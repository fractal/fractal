package org.ow2.fractal.f4e.diagram.custom.ui.actions;

import org.eclipse.osgi.util.NLS;

// TODO externalize strings
public class FractalDiagramUIActionsMessages {
	private static final String BUNDLE_NAME = "org.ow2.fractal.f4e.diagram.custom.ui.actions.FractalDiagramUIActionsMessages";
	public static String HideInterfaceLabelsAction_label;
	public static String HideInterfaceLabelsAction_toolTip;
	
	public static String ShowInterfaceLabelsAction_label;
	public static String ShowInterfaceLabelsAction_toolTip;
	
	public static String ShowFractalActionMenu_ShowFractalText;
	public static String ShowFractalActionMenu_ShowFractalTooltip;
	
	public static String Command_hideLabel_Label;
	
	
	public static String HideSubComponentsCompartmentsAction_label;
	public static String HideSubComponentsCompartmentsAction_toolTip;
	public static String ShowSubComponentsCompartmentsAction_label;
	public static String ShowSubComponentsCompartmentsAction_toolTip;
	
	public static String HideAttributesControllerCompartmentsAction_label;
	public static String HideAttributesControllerCompartmentsAction_toolTip;
	public static String ShowAttributesControllerCompartmentsAction_label;
	public static String ShowAttributesControllerCompartmentsAction_toolTip;
	
	public static String HideContentCompartmentsAction_label;
	public static String HideContentCompartmentsAction_toolTip;
	public static String ShowContentCompartmentsAction_label;
	public static String ShowContentCompartmentsAction_toolTip;
	
	public static String HideOthersCompartmentsAction_label;
	public static String HideOthersCompartmentsAction_toolTip;
	public static String ShowOthersCompartmentsAction_label;
	public static String ShowOthersCompartmentsAction_toolTip;
	
	static {
		NLS.initializeMessages(BUNDLE_NAME, FractalDiagramUIActionsMessages.class);
	}
}
