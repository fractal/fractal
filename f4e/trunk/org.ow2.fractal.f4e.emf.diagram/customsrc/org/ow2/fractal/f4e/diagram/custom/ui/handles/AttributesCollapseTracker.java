package org.ow2.fractal.f4e.diagram.custom.ui.handles;

import org.eclipse.emf.ecore.EAttribute;


public class AttributesCollapseTracker extends CompartmentCollapseTracker{
	
	public AttributesCollapseTracker(ICompartmentCollapseHandle handle){
		super(handle);
	}
	
	protected EAttribute getAttribute(){
		return org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE.getAbstractComponentStyle_CollapsedAttributes();
	}
	
	protected String getCommandName() {
		return "Collapse the attributes compartment"; //$NON-NLS-1$
	}

	protected String getDebugName() {
		return "Collapse the sub attributes compartment tool"; //$NON-NLS-1$
	}
}
