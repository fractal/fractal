package org.ow2.fractal.f4e.diagram.custom.providers;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.ui.provider.PropertyDescriptor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Composite;

public class MergedPropertyDescriptor extends PropertyDescriptor{
	 public MergedPropertyDescriptor(Object object, IItemPropertyDescriptor itemPropertyDescriptor){
		 super(object,itemPropertyDescriptor);
	 }
	 
	 public CellEditor createPropertyEditor(Composite composite) 
	 {
		return super.createPropertyEditor(composite);
	 }
	 
	 public EObject getEObject(){
		 return (EObject)this.object;
	 }
	 
	 public EStructuralFeature getFeature(){
		 return (EStructuralFeature)this.itemPropertyDescriptor.getFeature(object);
	 }
}