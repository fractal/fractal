package org.ow2.fractal.f4e.diagram.custom.figures;


import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;
import org.eclipse.swt.graphics.Color;

/**
 * Custom figure for binding graphical representation. 
 * @author Yann Davin
 *
 */
public class BindingConnectionEx extends PolylineConnectionEx{
	protected static int WIDTH = 3;
	protected enum Type { SOURCE, TARGET }
	
	// When a binding is selected we use this orange color
	public static Color SELECTED_COLOR = new Color(null,0xff,0xb1,0x08);
	
	public static Color DEFAULT_COLOR = IFractalShape.LIGHT_GRAY;
	
	public BindingConnectionEx(){
		setLineWidth(WIDTH);
		setLineStyle(Graphics.LINE_SOLID);
	}
	
	public Rectangle getBounds() {
		return super.getBounds();
	}
	
	public PointList getSmoothPoints() {
		return super.getSmoothPoints();
	}
	
	protected void outlineShape(Graphics g) {
		// Set alpha to be able to see interface label
		// that can be displayed behind the binding
		g.setAlpha(100);
		super.outlineShape(g);
	}
}
