package org.ow2.fractal.f4e.diagram.custom.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.draw2d.ui.figures.RectangularDropShadowLineBorder;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;

/**
 * 
 * @author Yann Davin
 * 
 */
public class AbstractComponentShape extends Shape implements IFractalShape {

	public AbstractComponentShape(){
		setForegroundColor(LIGHT_GRAY);
		setBackgroundColor(WHITE);
		new RectangularDropShadowLineBorder(5);
	}
	
	protected void fillShape(Graphics graphics) {
		Rectangle r = getBounds().getCopy();
		
		Color background = graphics.getBackgroundColor();
		int red = background.getRed()+100>255?255:background.getRed();
		int green = background.getGreen()+100>255?255:background.getGreen();
		int blue = background.getBlue()+100>255?255:background.getBlue();
		
		Color deltaColor = new Color(null,red,green,blue);
		Color foreground = graphics.getForegroundColor();
		graphics.setBackgroundColor(deltaColor);
		graphics.setForegroundColor(background);
		graphics.fillGradient(r, true);
		graphics.setBackgroundColor(background);
		graphics.setForegroundColor(foreground);
		
	}

	public Rectangle getBounds() {
		Rectangle r = super.getBounds();
		Rectangle result = new Rectangle(r.x-1, r.y , r.width+1, r.height);
		return result;
	}

	protected void outlineShape(Graphics graphics) {
	}

	public void setBackgroundColor(Color bg) {
		super.setBackgroundColor(bg);
	}

	public void setForegroundColor(Color fg) {
		super.setForegroundColor(fg);
	}
}
