package org.ow2.fractal.f4e.diagram.custom.ui.actions;

import org.eclipse.gmf.runtime.common.ui.action.ActionMenuManager;
import org.eclipse.jface.action.Action;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.l10n.FractalDiagramUIActionsPluginImages;

public class FractalMenuManager extends ActionMenuManager {

	/**
	 * The fractal menu action containing the UI for the fractal menu manager
	 */
	private static class FractalMenuAction extends Action {
		public FractalMenuAction() {
			setText(FractalDiagramUIActionsMessages.ShowFractalActionMenu_ShowFractalText);
			setToolTipText(FractalDiagramUIActionsMessages.ShowFractalActionMenu_ShowFractalTooltip);

			setImageDescriptor(FractalDiagramUIActionsPluginImages.DESC_FRACTAL_MENU);
			setDisabledImageDescriptor(FractalDiagramUIActionsPluginImages.DESC_FRACTAL_MENU_DISABLED);
			setHoverImageDescriptor(FractalDiagramUIActionsPluginImages.DESC_FRACTAL_MENU);
		}
	}

	/**
	 * Creates a new instance of the compartment menu manager
	 */
	public FractalMenuManager() {
		super(FractalActionIds.MENU_FRACTAL, new FractalMenuAction(), true);
	}


}
