package org.ow2.fractal.f4e.diagram.custom.ui.handles;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.tools.AbstractTool;
import org.eclipse.gmf.runtime.diagram.ui.l10n.DiagramUIMessages;
import org.eclipse.gmf.runtime.diagram.ui.requests.ChangePropertyValueRequest;
import org.eclipse.gmf.runtime.emf.core.util.PackageUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.ow2.fractal.f4e.notation.AbstractComponentStyle;


/** Custom CompartmentCollapseTracker
 * 
 * @author Yann Davin
 */
public abstract class CompartmentCollapseTracker extends AbstractTool
	implements DragTracker {

	private ICompartmentCollapseHandle handle;
	
	/**
	 * 
	 * @param editPart a graphical edit part with a AbstractComponentStyle (typically DefinitionEditPart and ComponentEditPart)
	 */
	public CompartmentCollapseTracker(ICompartmentCollapseHandle handle) {
		this.handle = handle;
	}

	protected abstract EAttribute getAttribute();
	
	protected List createOperationSet() {
		List list = new ArrayList(1);
		list.add(handle.getEditPart());
		return list;
	}
	
	// Hide the specified child compartment
	protected Command getCommand(Boolean expand) {
		ChangePropertyValueRequest request = new ChangePropertyValueRequest(
			DiagramUIMessages.PropertyDescriptorFactory_CollapseCompartment,
			getPropertyName(), expand);
		return handle.getEditPart().getCommand(request);
	}
	
	// Collapse the compartment
	protected Command getCollapseCommand(Boolean expand) {
		ChangePropertyValueRequest request = new ChangePropertyValueRequest(
			DiagramUIMessages.PropertyDescriptorFactory_CollapseCompartment,
			org.eclipse.gmf.runtime.diagram.ui.internal.properties.Properties.ID_COLLAPSED, expand);
		return handle.getCompartmentEditPart().getCommand(request);
	}

	protected String getPropertyName(){
		return PackageUtil.getID(getAttribute());
	}
	
	protected String getCommandName() {
		return "Collapse Compartment"; //$NON-NLS-1$
	}

	protected String getDebugName() {
		return "Collapse Compartment Tool"; //$NON-NLS-1$
	}

	/**
	 * @see org.eclipse.gef.tools.AbstractTool#handleButtonDown(int)
	 */
	protected boolean handleButtonDown(int button) {
		View view  = handle.getEditPart().getNotationView();
		if (view!=null){
			AbstractComponentStyle style = (AbstractComponentStyle)view.getStyle(org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE.getAbstractComponentStyle());
			if (style != null) {
				Boolean newValue = (Boolean)style.eGet(getAttribute())? Boolean.FALSE : Boolean.TRUE;
				
				setCurrentCommand(getCollapseCommand(newValue));
				executeCurrentCommand();
				
				setCurrentCommand(getCommand(newValue));
				executeCurrentCommand();
				
				return true;
			} 
		}
		return false;
	}

	/**
	 * @see org.eclipse.gef.tools.AbstractTool#handleKeyDown(org.eclipse.swt.events.KeyEvent)
	 */
	protected boolean handleKeyDown(KeyEvent e) {
		if (e.keyCode == SWT.ARROW_RIGHT || e.keyCode == SWT.ARROW_LEFT) {
			Boolean b =
				e.keyCode == SWT.ARROW_RIGHT ? Boolean.FALSE : Boolean.TRUE;
			setCurrentCommand(getCommand(b));
			executeCurrentCommand();
			return true;
		}
		return false;
	}

}
