package org.ow2.fractal.f4e.diagram.custom.layouts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.ow2.fractal.f4e.diagram.custom.figures.InterfaceShape;

/**
 * Custom constrained layout to display correctly (on the right or on the left side) 
 * the label of an interface.
 * 
 * This layout is customized for the InterfaceFigure type.
 * @author Yann Davin
 *
 */
public class InterfaceConstrainedToolbarLayout extends ConstrainedToolbarLayout{
	static final int INTERFACE_IMAGE_WIDTH = 15;
	
	public void layout(IFigure parent) {
		// The parent is always a InterfaceFigure.
		super.layout(parent);
		
		// when labels are set not visible there is no children
		if(parent.getChildren().isEmpty()){
			return;
		}
		
		// We get the label.
		WrappingLabel label = (WrappingLabel)parent.getChildren().get(0);
		InterfaceShape interfaceShape = (InterfaceShape)parent;
		Rectangle newBounds = parent.getBounds();
	
		// If the interface is a client interface we must display the
		// label on the right else on the left.
		if(interfaceShape.getRole() == InterfaceShape.Role.CLIENT){
			label.getBounds().x += INTERFACE_IMAGE_WIDTH;
			label.getBounds().width = newBounds.width - INTERFACE_IMAGE_WIDTH;
		}else{
			label.getBounds().x = newBounds.x;
			label.getBounds().width = newBounds.width - INTERFACE_IMAGE_WIDTH;
		}
	}
	
	protected Dimension calculatePreferredSize(
			IFigure container,
			int wHint,
			int hHint) {
		Insets insets = container.getInsets();
		if (!container.isVisible())
			return new Dimension(insets.getWidth(),insets.getHeight());
		if (isHorizontal()) {
			wHint = -1;
			if (hHint >= 0)
				hHint = Math.max(0, hHint - insets.getHeight());
		} else {
			hHint = -1;
			if (wHint >= 0)
				wHint = Math.max(0, wHint - insets.getWidth());
		}

		List children = getChildren(container);
		Dimension prefSize =
			calculateChildrenSize(children, wHint, hHint, true);
		// Do a second pass, if necessary
		if (wHint >= 0 && prefSize.width > wHint) {
			prefSize =
				calculateChildrenSize(children, prefSize.width, hHint, true);
		} else if (hHint >= 0 && prefSize.width > hHint) {
			prefSize =
				calculateChildrenSize(children, wHint, prefSize.width, true);
		}

		prefSize.height += Math.max(0, children.size() - 1) * spacing;
		return transposer
			.t(prefSize)
			.expand(insets.getWidth() + INTERFACE_IMAGE_WIDTH, insets.getHeight())
			.union(getBorderPreferredSize(container));
	}
	/**
	 * Calculates either the preferred or minimum children size
	 */
	private Dimension calculateChildrenSize(
		List children,
		int wHint,
		int hHint,
		boolean preferred) {
		Dimension childSize;
		IFigure child;
		int height = 0, width = 0;
		for (int i = 0; i < children.size(); i++) {
			child = (IFigure) children.get(i);
			childSize =
				transposer.t(
					preferred
						? child.getPreferredSize(wHint, hHint)
						: child.getMinimumSize(wHint, hHint));
			height += childSize.height;
			width = Math.max(width, childSize.width);
		}
		
		return new Dimension(width, height);
	}

	/**
	 * Gets the list of children after applying the layout options of
	 * ignore invisible children & reverse children
	 */
	private List getChildren(IFigure container) {
		List children = new ArrayList(container.getChildren());
		if (getIgnoreInvisibleChildren()) {
			Iterator iter = children.iterator();
			while (iter.hasNext()) {
				IFigure f = (IFigure) iter.next();
				if (!f.isVisible())
					iter.remove();
			}
		}
		if (isReversed())
			Collections.reverse(children);
		return children;
	}

}
