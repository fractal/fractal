package org.ow2.fractal.f4e.diagram.custom.ui.handles;

import org.eclipse.emf.ecore.EAttribute;


public class SubComponentsCollapseTracker extends CompartmentCollapseTracker{
	
	public SubComponentsCollapseTracker(ICompartmentCollapseHandle handle){
		super(handle);
	}
	
	protected EAttribute getAttribute(){
		return org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE.getAbstractComponentStyle_CollapsedSubComponents();
	}
	
	protected String getCommandName() {
		return "Collapse the sub components compartment"; //$NON-NLS-1$
	}

	protected String getDebugName() {
		return "Collapse the sub components compartment tool"; //$NON-NLS-1$
	}
}
