package org.ow2.fractal.f4e.diagram.custom.edit.parts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.AbstractBorderItemEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.AbstractBorderedShapeEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.diagram.custom.figures.InterfaceShape;
import org.ow2.fractal.f4e.diagram.custom.layouts.InterfacesSize;

public abstract class AbstractComponentEditPart extends AbstractBorderedShapeEditPart {

	public AbstractComponentEditPart(View view) {
		super(view);
	}


	/**
     * Return the list of figures that overlap the given component figure.
     * 
     * @param containerFigure
     * @param componentFigure
     * @return
     */
    public List<EditPart> getConflictFigures(Point moveDelta){
    	List<EditPart> result = new ArrayList<EditPart>();
    	
    	Iterator<EditPart> children = this.getParent().getChildren().iterator();
	    EditPart child;
	    Rectangle componentWithInterfaces = getComponentSizeWithInterfaces(this);
	    componentWithInterfaces.x += moveDelta.x;
	    componentWithInterfaces.y += moveDelta.y;
	    
	    while (children.hasNext()) {
	    	child = (EditPart)children.next();
	    	if(child instanceof AbstractComponentEditPart){
	    		Rectangle childWithInterfaces = getComponentSizeWithInterfaces((AbstractComponentEditPart)child);
	    		if(	child != this &&
	    				childWithInterfaces.intersects(componentWithInterfaces)){
	    			result.add(child);
	    		}
	    	}
	    }
    	return result;
    }
    
    public boolean isInConflict(AbstractComponentEditPart component){
    	return isInConflict(component, this);
    }
    
    /**
     * Return true if c1 position is in conflict with c2
     * @param c1
     * @param c2
     * @return
     */
    public boolean isInConflict(AbstractComponentEditPart c1, AbstractComponentEditPart c2){
    	boolean result = false;
    	Rectangle c1WithInterfaces = getComponentSizeWithInterfaces(c1);
    	Rectangle c2WithInterfaces = getComponentSizeWithInterfaces(c2);
    	
    	return c1WithInterfaces.intersects(c2WithInterfaces);
    }
    
    public Rectangle getComponentSizeWithInterfaces(AbstractComponentEditPart component){
    	Rectangle result = component.getFigure().getBounds().getCopy();

   		int maxServerInterfacesWidth = 0;
		int maxClientInterfacesWidth = 0;
		
		Iterator<EditPart> iterator = component.getChildren().iterator();
		while(iterator.hasNext()){
			EditPart c = iterator.next();
			if(c instanceof AbstractBorderItemEditPart) {
				EObject eObject =  ((View)c.getModel()).getElement();
				IFigure interfaceFigure = ((AbstractBorderItemEditPart)c).getContentPane();
				if(interfaceFigure instanceof InterfaceShape){
					if(((InterfaceShape)interfaceFigure).getRole() == InterfaceShape.Role.SERVER){
						maxServerInterfacesWidth = 
							maxServerInterfacesWidth<interfaceFigure.getSize().width?
								interfaceFigure.getSize().width:maxServerInterfacesWidth;
					}else{
						maxClientInterfacesWidth = 
							maxClientInterfacesWidth<interfaceFigure.getSize().width?
								interfaceFigure.getSize().width:maxClientInterfacesWidth;
					}
				}
			}
		}
			
		result.x-=maxServerInterfacesWidth;
    	result.width+=maxClientInterfacesWidth+maxServerInterfacesWidth;
    	return result;
    }
    
    public InterfacesSize getInterfacesSize(){
    	return getInterfacesSize(this);
    }
    
    /**
     * 
     * @param component
     * @return
     */
    public InterfacesSize getInterfacesSize(AbstractComponentEditPart component){
    	InterfacesSize result = new InterfacesSize(0,0);

   		int maxServerInterfacesWidth = 0;
		int maxClientInterfacesWidth = 0;
		
		Iterator<EditPart> iterator = component.getChildren().iterator();
		while(iterator.hasNext()){
			EditPart c = iterator.next();
			if(c instanceof AbstractBorderItemEditPart) {
				EObject eObject =  ((View)c.getModel()).getElement();
				IFigure interfaceFigure = ((AbstractBorderItemEditPart)c).getContentPane();
				if(interfaceFigure instanceof InterfaceShape){
					if(((InterfaceShape)interfaceFigure).getRole() == InterfaceShape.Role.SERVER){
						maxServerInterfacesWidth = 
							maxServerInterfacesWidth<interfaceFigure.getSize().width?
								interfaceFigure.getSize().width:maxServerInterfacesWidth;
					}else{
						maxClientInterfacesWidth = 
							maxClientInterfacesWidth<interfaceFigure.getSize().width?
								interfaceFigure.getSize().width:maxServerInterfacesWidth;
					}
				}
			}
		}
			
		result.maxServerInterfaceLength=maxServerInterfacesWidth;
    	result.maxClientInterfaceLength=maxClientInterfacesWidth;
    	return result;
    }
    
}
