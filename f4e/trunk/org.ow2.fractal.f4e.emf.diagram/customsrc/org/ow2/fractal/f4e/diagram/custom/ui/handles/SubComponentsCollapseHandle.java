package org.ow2.fractal.f4e.diagram.custom.ui.handles;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.gef.DragTracker;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;

public class SubComponentsCollapseHandle extends CompartmentCollapseHandle{
	static final int HORIZONTAL_POSITION = 2;
	
	public SubComponentsCollapseHandle(IGraphicalEditPart editPart, int compartmentVisualID){
		super(editPart,compartmentVisualID);
	}
	
	protected DragTracker createDragTracker(){
		return new SubComponentsCollapseTracker(this);
	}
	
	protected EAttribute getAttribute(){
		return org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE.getAbstractComponentStyle_CollapsedSubComponents();
	}
	
	protected int getHorizontalPosition(){
		return HORIZONTAL_POSITION;
	}
}
