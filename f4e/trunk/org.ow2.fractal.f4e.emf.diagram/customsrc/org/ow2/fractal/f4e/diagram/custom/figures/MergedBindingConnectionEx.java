package org.ow2.fractal.f4e.diagram.custom.figures;


import org.eclipse.draw2d.Graphics;
import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;

/**
 * Graphical representation for merged bindings.
 * @author Yann Davin
 *
 */
public class MergedBindingConnectionEx extends PolylineConnectionEx{

	public MergedBindingConnectionEx(){
		setLineWidth(3);
		setXOR(false);
		setLineStyle(Graphics.LINE_DOT);
	}
	
	protected void outlineShape(Graphics g) {
		g.setAlpha(100);
		super.outlineShape(g);
	}
}
