package org.ow2.fractal.f4e.diagram.custom.edit.policies.compartments;

import org.eclipse.emf.ecore.EAttribute;
import org.ow2.fractal.f4e.diagram.custom.ui.requests.FractalRequestConstants;

public class SubComponentsCompartmentsEditPolicy extends CompartmentsEditPolicy {
	protected Object getRequestType() {
		return FractalRequestConstants.REQ_TOGGLE_SUBCOMPONENTS_COMPARMENTS;
	}
	
	protected EAttribute getAttribute(){
		return org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE.getAbstractComponentStyle_CollapsedSubComponents();
	}
}