package org.ow2.fractal.f4e.diagram.custom.figures;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Shape;

/**
 * Abstract class that figures that can be associated to merged
 * elements should extends. It allows to change the color
 * of the figure in gray via the setEditable method.
 * 
 * @author Yann Davin
 *
 */
public abstract class MergedShape extends Shape implements IFractalShape{

	  private boolean editable=true;
		public void setEditable(boolean editable){
			this.editable = editable;
			updateBackground();
		}
		
		public boolean getEditable(){
			return this.editable;
		}
		
		private void updateBackground(){
			if(editable == true){
				setBackgroundColor(WHITE);
			}else{
				setBackgroundColor(GRAY_FOR_MERGED_ELEMENT);
			}
		}
		
		protected void fillShape(Graphics graphics) {
			if(getEditable() == false){
				graphics.pushState();
				setBackgroundColor(GRAY_FOR_MERGED_ELEMENT);
				graphics.fillRectangle(this.getBounds().getCopy());
				graphics.popState();
			}
		}
}
