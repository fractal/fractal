package org.ow2.fractal.f4e.diagram.custom.ui.handles;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.gef.DragTracker;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;

public class AttributesCollapseHandle extends CompartmentCollapseHandle{
	final static int HORIZONTAL_POSITION = 4;
	
	public AttributesCollapseHandle(IGraphicalEditPart editPart,int compartmentVisualID){
		super(editPart,compartmentVisualID);
	}
	
	protected DragTracker createDragTracker(){
		return new AttributesCollapseTracker(this);
	}
	
	protected EAttribute getAttribute(){
		return org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE.getAbstractComponentStyle_CollapsedAttributes();
	}
	
	protected int getHorizontalPosition(){
		return HORIZONTAL_POSITION;
	}
	
}
