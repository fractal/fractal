package org.ow2.fractal.f4e.diagram.custom.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.ScrollPane;
import org.eclipse.gmf.runtime.diagram.ui.figures.ResizableCompartmentFigure;
import org.eclipse.gmf.runtime.draw2d.ui.internal.figures.AnimatableScrollPane;
import org.eclipse.gmf.runtime.draw2d.ui.internal.figures.OverlayScrollPaneLayout;
import org.eclipse.gmf.runtime.draw2d.ui.mapmode.IMapMode;

/**
 * Figure for compartment without scroll bar.
 * For example for the attributes compartment we don't want a scrollbar,
 * all attributes must be visible and the compartment should update its size 
 * to the number of children.
 * 
 * @author Yann Davin
 *
 */
public class ResizableCompartmentWithoutScrollBarFigure extends ResizableCompartmentFigure{
	
	public ResizableCompartmentWithoutScrollBarFigure(String compartmentTitle,
			IMapMode mm) {
		super(compartmentTitle, mm);
		setBorder(null);
		setTitleVisibility(false);
	}

	protected AnimatableScrollPane createScrollpane(IMapMode mm) {
	        scrollPane = new AnimatableScrollPane(){
				public void setVerticalScrollBarVisibility(int v) {
					super.setVerticalScrollBarVisibility(v);
				}
	        	
	        };
	        scrollPane.getViewport().setContentsTracksWidth(false);
	        scrollPane.getViewport().setContentsTracksHeight(false);
	        scrollPane.setLayoutManager(new OverlayScrollPaneLayout());
	        scrollPane.setVerticalScrollBarVisibility(ScrollPane.NEVER);
	        scrollPane.setHorizontalScrollBarVisibility(ScrollPane.NEVER);
	        scrollPane.setContents(new Figure());
	        int half_minClient = getMinClientSize()/2;
	        scrollPane.getContents().setBorder(null);            
	        return (AnimatableScrollPane)scrollPane;
	    } 
}
