package org.ow2.fractal.f4e.diagram.custom.ui.requests;

import org.eclipse.gef.Request;
import org.eclipse.gmf.runtime.diagram.ui.requests.RequestConstants;

public class ToggleInterfaceLabelsRequest extends Request {

	/** show or hide flag */
	private boolean showInterfaceLabel;

	/**
	 * Constructor
	 * 
	 * @param showConnectionLabel
	 *            to show/hide the labels
	 */
	
	public ToggleInterfaceLabelsRequest(boolean showInterfaceLabel) {
		super(FractalRequestConstants.REQ_TOGGLE_INTERFACE_LABELS);
		this.showInterfaceLabel = showInterfaceLabel;
		
	}

	/**
	 * gets the show/hide flag.
	 * 
	 * @return <code>true</code> or <code>flase</code>
	 */
	public final boolean showInterfaceLabel() {
		return showInterfaceLabel;
	}
}
