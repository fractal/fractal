package org.ow2.fractal.f4e.diagram.custom.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Path;

/**
 * Define the shape used to represent a Fractal client interface.
 * 
 * @author Yann Davin 
 * 
 */
public class InterfaceShape extends Shape implements IFractalShape {
	
	private int[] myCachedPath = new int[18];
	public enum Role {CLIENT,SERVER};
	
	private boolean editable = true;
	private Role role = Role.CLIENT;
	
	private boolean gradient = true;

	private Path serverPath = new Path(null);
	private Path clientPath = new Path(null);
	
	private static int Y = 5;
	private static int X = 5;
	
	public InterfaceShape(){
		super();
		setForegroundColor(BLACK);
		setBackgroundColor(GREEN);
		
		initializePaths();
	}
	
	private void initializePaths(){
		clientPath.moveTo(0,Y);
		clientPath.lineTo(0,2*Y);
		clientPath.lineTo(2*X,2*Y);
		clientPath.lineTo(2*X,3*Y);
		clientPath.lineTo(3*X,3*Y);
		clientPath.lineTo(3*X,0);
		clientPath.lineTo(2*X,0);
		clientPath.lineTo(2*X,Y);
		clientPath.lineTo(0,Y);
		
		
		serverPath.moveTo(0,0);
		serverPath.lineTo(0,3*Y);
		serverPath.lineTo(X,3*Y);
		serverPath.lineTo(X,2*Y);
		serverPath.lineTo(3*X,2*Y);
		serverPath.lineTo(3*X,Y);
		serverPath.lineTo(X,Y);
		serverPath.lineTo(X,0);
		serverPath.lineTo(0,0);
	}
	
	private void updatePath(Path path, int x, int y){
		float points[] = path.getPathData().points;
		//update x
		for(int i=0; i<points.length;i+=2){
			points[i]+=x;
		}
		
		for(int i=1; i<points.length;i+=2){
			points[i]+=y;
		}
		
		path.getPathData().points = points;
	}
	
	public void setRole(InterfaceShape.Role role){
		this.role = role;
		updateBackground();
	}
	
	private void updateBackground(){	
		setBackgroundColor(getInterfaceColor());
	}
	
	public Color getInterfaceColor(){
		Color result = null;
		if(editable == true){
			if(role == Role.CLIENT){
				result = GREEN;
			}else{
				result= RED;
			}
		}else{
			result = ColorConstants.darkGray;
		}
		return result;
	}
	
	public void setEditable(boolean editable){
		this.editable = editable;
		updateBackground();
	}
	
	public InterfaceShape.Role getRole(){
		return this.role;
	}
	
	protected void fillClientShape(Graphics graphics){
		Rectangle r = this.getBounds();
		int y = 5;
		int x = 5;
		setPathPoint(0, r.x + 0, r.y + y);
		setPathPoint(1, r.x + 0, r.y + 2*y);
		setPathPoint(2, r.x + 2*x, r.y + 2*y);
		setPathPoint(3, r.x + 2*x, r.y + 3*y);
		setPathPoint(4, r.x + 3*x, r.y + 3*y);
		setPathPoint(5, r.x + 3*x, r.y + 0);
		setPathPoint(6, r.x + 2*x, r.y + 0);
		setPathPoint(7, r.x + 2*x, r.y + y);
		setPathPoint(8, r.x + 0, r.y + y);
		
		
		graphics.fillPolygon(myCachedPath);
	}
	
	
	protected void gradientClientShape(Graphics graphics){
		Rectangle r = this.getBounds();
		int y = 5;
		int x = 5;
	
		Path path = new Path(null);
		path.moveTo(r.x + 0, r.y + y);
		path.lineTo(r.x + 0, r.y + 2*y);
		path.lineTo(r.x + 2*x, r.y + 2*y);
		path.lineTo(r.x + 2*x, r.y + 3*y);
		path.lineTo(r.x + 3*x, r.y + 3*y);
		path.lineTo(r.x + 3*x, r.y + 0);
		path.lineTo(r.x + 2*x, r.y + 0);
		path.lineTo(r.x + 2*x, r.y + y);
		path.lineTo(r.x + 0, r.y + y);
		
		Color color = getBackgroundColor();
		graphics.setBackgroundColor(color);
		graphics.setForegroundColor(IFractalShape.WHITE);
		graphics.setClip(path);
		graphics.fillGradient(r, true);
		
		graphics.setForegroundColor(color);
		graphics.setLineWidth(2);
		graphics.drawPath(path);
		
	}
	
	protected void fillServerShape(Graphics graphics) {
		Rectangle r = this.getBounds();
		int y = 5;
		int x = 5;
		int x0 = r.x + r.width - 15;
		setPathPoint(0, x0 + 0, r.y + 0);
		setPathPoint(1, x0 + 0, r.y + 3*y);
		setPathPoint(2, x0 + x, r.y + 3*y);
		setPathPoint(3, x0 + x, r.y + 2*y);
		setPathPoint(4, x0 + 3*x, r.y + 2*y);
		setPathPoint(5, x0 + 3*x, r.y + y);
		setPathPoint(6, x0 + x, r.y + y);
		setPathPoint(7, x0 + x, r.y + 0);
		setPathPoint(8, x0 + 0, r.y + 0);
		graphics.fillPolygon(myCachedPath);
		
	}
	
	protected void gradientServerShape(Graphics graphics) {
		Rectangle r = this.getBounds();
		int y = 5;
		int x = 5;
		int x0 = r.x + r.width - 15;
		
		Path path = new Path(null);
		path.moveTo(x0 + 0, r.y + 0);
		path.lineTo(x0 + 0, r.y + 3*y);
		path.lineTo(x0 + x, r.y + 3*y);
		path.lineTo(x0 + x, r.y + 2*y);
		path.lineTo(x0 + 3*x, r.y + 2*y);
		path.lineTo(x0 + 3*x, r.y + y);
		path.lineTo(x0 + x, r.y + y);
		path.lineTo(x0 + x, r.y + 0);
		path.lineTo(x0 + 0, r.y + 0);
		
		Color color = getBackgroundColor();
		graphics.setBackgroundColor(color);
		graphics.setForegroundColor(IFractalShape.WHITE);
		graphics.setClip(path);
		graphics.fillGradient(r, true);
		
		graphics.setForegroundColor(color);
		graphics.setLineWidth(2);
		graphics.drawPath(path);	
	}
	
	/**
	 * @see Shape#fillShape(Graphics)
	 */
	protected void fillShape(Graphics graphics) {
		if(this.role == Role.CLIENT){
			if(isGradient()){
				gradientClientShape(graphics);
			}else{
				fillClientShape(graphics);
			}
		}else{
			if(isGradient()){
				gradientServerShape(graphics);
			}else{
				fillServerShape(graphics);
			}
		}
	}

	
	protected void outlineServerShape(Graphics graphics) {
	}
	
	
	protected void outlineClientShape(Graphics graphics) {	
	}
	
	/**
	 * @see Shape#outlineShape(Graphics)
	 */
	protected void outlineShape(Graphics graphics) {
	
		if(this.role == Role.CLIENT){
			outlineClientShape(graphics);
		}else{
			outlineServerShape(graphics);
		}
		
	}

	private void setPathPoint(int index, int x, int y) {
		myCachedPath[index * 2] = x;
		myCachedPath[index * 2 + 1] = y;
	}

	public void setBackgroundColor(Color bg) {
		if(!bg.equals(ColorConstants.white))
			super.setBackgroundColor(bg);
	}
	
	public boolean isGradient() {
		return gradient;
	}

	public void setGradient(boolean gradient) {
		this.gradient = gradient;
	}
}
