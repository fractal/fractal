package org.ow2.fractal.f4e.diagram.custom.providers;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.ui.provider.PropertyDescriptor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Composite;
import org.ow2.fractal.f4e.diagram.custom.ui.FractalJavaSignatureCellEditor;

/** 
 * Modified property descriptor used for all Fractal 'signature' properties.
 * So when a property named 'signature' is displayed in the property page
 * we use a custom cell to display the 'signature' value.
 * That custom cell display a little button that allows to open the completion dialog box.
 *
 * @author Yann Davin
 */
public class FractalJavaSignaturePropertyDescriptor  extends MergedPropertyDescriptor{
	
	public FractalJavaSignaturePropertyDescriptor(Object object, IItemPropertyDescriptor itemPropertyDescriptor)
	  {
	    super(object,itemPropertyDescriptor);
	 
	  }
	
	public CellEditor createPropertyEditor(Composite composite) {
		 if (!itemPropertyDescriptor.canSetProperty(object))
		    {
		      return null;
		    }
		 return new FractalJavaSignatureCellEditor(composite, (EObject)object,  (EStructuralFeature)itemPropertyDescriptor.getFeature(object));
	}
}
