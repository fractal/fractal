package org.ow2.fractal.f4e.diagram.custom.edit.policies;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.ScrollPane;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartListener;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.ResizableCompartmentEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.figures.ResizableCompartmentFigure;
import org.eclipse.gmf.runtime.diagram.ui.figures.ShapeCompartmentFigure;

/**
 * Custom edit policy that is used to hide default GMF compartment handle.
 * @author Yann Davin
 *
 */
public class ResizableCompartmentWithoutHandleEditPolicy extends ResizableCompartmentEditPolicy{

	protected List createCollapseHandles() {
		// TODO Auto-generated method stub
		List collapseHandles = new ArrayList();
		return collapseHandles;
	}

	 private EditPartListener hostListener;

	 /**
	  * @see org.eclipse.gef.editpolicies.SelectionEditPolicy#addSelectionListener()
	  */
	 protected void addSelectionListener() {
		 super.addSelectionListener();
		 // We want to hide scroll bar when the compartment is not selected
		 hostListener = new EditPartListener.Stub() {
			 public void selectedStateChanged(EditPart part) {
				if(getHost().getSelected() == EditPart.SELECTED_PRIMARY || (getHost().getParent().getSelected() == EditPart.SELECTED_PRIMARY || getHost().getParent().getSelected() == EditPart.SELECTED)){
					((ResizableCompartmentFigure)((GraphicalEditPart)getHost()).getFigure()).getScrollPane().setHorizontalScrollBarVisibility(ScrollPane.ALWAYS);
					((ResizableCompartmentFigure)((GraphicalEditPart)getHost()).getFigure()).getScrollPane().setVerticalScrollBarVisibility(ScrollPane.ALWAYS);
				}else{
					((ResizableCompartmentFigure)((GraphicalEditPart)getHost()).getFigure()).getScrollPane().setHorizontalScrollBarVisibility(ScrollPane.NEVER);
					((ResizableCompartmentFigure)((GraphicalEditPart)getHost()).getFigure()).getScrollPane().setVerticalScrollBarVisibility(ScrollPane.NEVER);
					
				}
			 }
		 };
		 getHost().addEditPartListener(hostListener);
	 }
	 
	 
}
