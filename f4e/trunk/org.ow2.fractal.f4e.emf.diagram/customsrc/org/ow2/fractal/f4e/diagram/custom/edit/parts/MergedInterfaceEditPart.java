package org.ow2.fractal.f4e.diagram.custom.edit.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.diagram.custom.figures.InterfaceShape;
import org.ow2.fractal.f4e.diagram.custom.layouts.InterfaceConstrainedToolbarLayout;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.Role;

/**
 * Customization of the MergedInterfaceEditPart in order to change
 * the merged interface figure when the interface role is changed,
 * and change its color if it is a merged interface.
 * 
 * The role of a merged interface can be changed even if the figure 
 * is not editable, for example when the original interface role is modified and
 * the merge recalculated.
 * 
 * @author Yann Davin
 *
 */
public class MergedInterfaceEditPart extends org.ow2.fractal.f4e.fractal.diagram.edit.parts.Interface2EditPart{
	
	public MergedInterfaceEditPart(View view) {
		super(view);
	}
	
	private InterfaceShape interfaceShape; 
	protected NodeFigure createNodeFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		interfaceShape = (InterfaceShape)createNodeShape();
		
		if(((View)getModel()).getElement() != null && ((View)getModel()).getElement() instanceof Interface){
			Interface interface_ = (Interface)((View)getModel()).getElement();
			if(interface_.isMerged()==true){
				interfaceShape.setEditable(false);
			}
			if(interface_.getRole() == Role.SERVER){
				interfaceShape.setRole(InterfaceShape.Role.SERVER);
			}
		}
		
		figure.add(interfaceShape);
		contentPane = setupContentPane(interfaceShape);
		return figure;
	}
	
	protected void handleNotificationEvent(Notification notification) {
		// TODO Auto-generated method stub
		if(notification.getEventType() == Notification.SET 
				&& notification.getNotifier() instanceof Interface
				&& notification.getFeature() instanceof EAttribute 
				&& ((EAttribute)notification.getFeature()).getName().equals("role")){
			if(notification.getNewValue() instanceof Role && 
				((Role)notification.getNewValue()).equals(Role.SERVER) &&
				notification.getOldValue() instanceof Role && 
				((Role)notification.getOldValue()).equals(Role.CLIENT)){
				if(interfaceShape != null){
					interfaceShape.setRole(InterfaceShape.Role.SERVER);
					interfaceShape.revalidate();
					interfaceShape.repaint();
				}
			}else if(notification.getNewValue() instanceof Role && 
					((Role)notification.getNewValue()).equals(Role.CLIENT) &&
					notification.getOldValue() instanceof Role && 
					((Role)notification.getOldValue()).equals(Role.SERVER)){
				if(interfaceShape != null){
					interfaceShape.setRole(InterfaceShape.Role.CLIENT);
					interfaceShape.revalidate();
					interfaceShape.repaint();
				}
			}
		}
		super.handleNotificationEvent(notification);
	}
	
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			InterfaceConstrainedToolbarLayout layout = new InterfaceConstrainedToolbarLayout();
			layout.setSpacing(getMapMode().DPtoLP(5));
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}
}
