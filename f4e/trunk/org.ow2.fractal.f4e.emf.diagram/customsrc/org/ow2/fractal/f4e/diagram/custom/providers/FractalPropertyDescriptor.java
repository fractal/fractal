package org.ow2.fractal.f4e.diagram.custom.providers;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.ui.provider.PropertyDescriptor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Composite;
import org.ow2.fractal.f4e.diagram.custom.ui.FractalPropertyValueCellEditor;

public class FractalPropertyDescriptor  extends PropertyDescriptor{
	 
	public FractalPropertyDescriptor(EObject object, IItemPropertyDescriptor itemPropertyDescriptor)
	  {
	    super(object,itemPropertyDescriptor);
	 
	  }
	
	
	public CellEditor createPropertyEditor(Composite composite) {
		 if (!itemPropertyDescriptor.canSetProperty(object))
		    {
		      return null;
		    }
		 return new FractalPropertyValueCellEditor(composite, (EObject)object,  (EStructuralFeature)itemPropertyDescriptor.getFeature(object));
	}
}
