package org.ow2.fractal.f4e.diagram.custom.ui.actions;

public interface FractalActionIds {
	public final String ACTION_HIDE_INTERFACE_LABELS = "hideInterfaceLabels";
	public final String ACTION_SHOW_INTERFACE_LABELS = "showInterfaceLabels";
	public final String ACTION_HIDE_SUBCOMPONENTS_COMPARTMENTS = "hideSubComponentsCompartments";
	public final String ACTION_SHOW_SUBCOMPONENTS_COMPARTMENTS = "showSubComponentsCompartments";
	public final String ACTION_HIDE_ATTRIBUTES_CONTROLLER_COMPARTMENTS = "hideAttributesControllerCompartments";
	public final String ACTION_SHOW_ATTRIBUTES_CONTROLLER_COMPARTMENTS = "showAttributesControllerCompartments";
	public final String ACTION_HIDE_CONTENT_COMPARTMENTS = "hideContentCompartments";
	public final String ACTION_SHOW_CONTENT_COMPARTMENTS = "showContentCompartments";
	public final String ACTION_HIDE_OTHERS_COMPARTMENTS = "hideOthersCompartments";
	public final String ACTION_SHOW_OTHERS_COMPARTMENTS = "showOthersCompartments";
	
	public final String MENU_FRACTAL = "fractalMenu";
}
