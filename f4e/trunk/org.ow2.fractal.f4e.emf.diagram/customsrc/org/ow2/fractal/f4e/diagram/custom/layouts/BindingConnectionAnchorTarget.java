/*
 * Copyright (c) 2005-2007 Obeo
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Obeo - initial API and implementation
 */
package org.ow2.fractal.f4e.diagram.custom.layouts;

import org.eclipse.draw2d.AbstractConnectionAnchor;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.Role;
import org.ow2.fractal.f4e.fractal.adapter.helper.HelperAdapter;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.InterfaceEditPart.InterfaceFigure;

/**
 * 
 * @author Yann Davin
 *
 */
public class BindingConnectionAnchorTarget extends AbstractConnectionAnchor
		implements ConnectionAnchor {

	ConnectionEditPart bindingEditPart;
	
	public BindingConnectionAnchorTarget(ConnectionEditPart bindingEditPart) {
		super(bindingEditPart.getTarget()!=null?((GraphicalEditPart)bindingEditPart.getTarget()).getFigure():null);
		this.bindingEditPart = bindingEditPart;
	}

	public Point getLocation(Point reference) {
		return this.getReferencePoint();
	}

	public Point getReferencePoint() {
		if(getOwner() == null){
			return null;
		}
		
		GraphicalEditPart sourceEditPart =(GraphicalEditPart)bindingEditPart.getSource();
		GraphicalEditPart targetEditPart = (GraphicalEditPart)bindingEditPart.getTarget();
		
		IFigure source =  sourceEditPart.getFigure();
		IFigure target = targetEditPart!=null?targetEditPart.getFigure():null;
		
		if(source == null || target == null){
			return null;
		}
		
		Interface sourceItf = ((Interface)((View)sourceEditPart.getModel()).getElement());
		Interface targetItf = ((Interface)((View)targetEditPart.getModel()).getElement());
		
		Point ref = target.getBounds().getCenter();
		
		Role targetRole = HelperAdapter.isMergedElement(targetItf)?
				targetItf.getRole():
					targetItf.getMergedRole();
				
		// A binding between two interfaces of the same component
		if(sourceItf.eContainer() == targetItf.eContainer()){
			if(targetRole == Role.CLIENT){
				// TODO remove the constant, calculate it from the figure
				ref.x = target.getBounds().x + 15;
			}else{
				ref.x = target.getBounds().x + target.getBounds().width;
			}
		}else if(sourceItf.eContainer()!= null && targetItf.eContainer() == sourceItf.eContainer().eContainer()){
			ref.x = target.getBounds().x + target.getBounds().width;
		}else{
			ref.x = target.getBounds().x + 15;
		}
		
		target.translateToAbsolute(ref);
		return ref;
		
	}

}
