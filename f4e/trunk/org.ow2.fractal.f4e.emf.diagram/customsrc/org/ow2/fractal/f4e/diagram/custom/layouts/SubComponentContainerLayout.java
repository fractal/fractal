package org.ow2.fractal.f4e.diagram.custom.layouts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.AbstractBorderItemEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.AbstractBorderedShapeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.figures.BorderedNodeFigure;
import org.eclipse.gmf.runtime.diagram.ui.figures.CanonicalShapeCompartmentLayout;
import org.eclipse.gmf.runtime.notation.View;
import org.ow2.fractal.f4e.diagram.custom.figures.InterfaceShape;

public class SubComponentContainerLayout  extends CanonicalShapeCompartmentLayout {
	
	public SubComponentContainerLayout(Map map) {
		super(map);
	}
	
	/**
	 * Layout that place automatically the sub components in horizontal
	 * position, as an array of one dimension.
	 * Children can be moved vertically, but not horizontally.
	 * 
	 * @param parent
	 */
	protected void horizontalLayout(IFigure parent){
		 Iterator children = parent.getChildren().iterator();
	       Point offset = getOrigin(parent);
	       IFigure f;
	       int x=0;
	       int y=0;
	       
	       while (children.hasNext()) {
	           f = (IFigure)children.next();
	           Rectangle bounds = (Rectangle)getConstraint(f);
	           if (bounds == null) continue;

	           int widthHint = bounds.width;
	           int heightHint = bounds.height;
	           if (widthHint == -1 || heightHint == -1) {
	               Dimension _preferredSize = f.getPreferredSize(widthHint, heightHint);
	               bounds = bounds.getCopy();
	               if (widthHint == -1)
	                   bounds.width = _preferredSize.width;
	               if (heightHint == -1)
	                   bounds.height = _preferredSize.height;
	           }
	           Dimension min = f.getMinimumSize(widthHint, heightHint);
	           Dimension max = f.getMaximumSize();
	           
	           if (min.width>bounds.width)
	               bounds.width = min.width;
	           else if (max.width < bounds.width)
	               bounds.width = max.width;
	           
	           if (min.height>bounds.height)
	               bounds.height = min.height;
	           else if (max.height < bounds.height)
	               bounds.height = max.height;
	           bounds = bounds.getTranslated(offset);
	         
	           int maxInterfaceServerWidth = 0;
	           int maxInterfaceClientWidth = 0;
	           
	           if(f instanceof BorderedNodeFigure){
	        	   BorderedNodeFigure nodeFigure = (BorderedNodeFigure)f;
	        	   nodeFigure.getExtendedBounds();
	        	   Object object = this.getVisualPartMap().get(nodeFigure);
	        	   
	        	  
	        	   
	        	   if(object != null && object instanceof AbstractBorderedShapeEditPart){
	        		   AbstractBorderedShapeEditPart editPart = (AbstractBorderedShapeEditPart)object;
	        		 
	        		   Iterator<EditPart> iterator = editPart.getChildren().iterator();
	        		   while(iterator.hasNext()){
	        			   EditPart child = iterator.next();
	        			   if(child instanceof AbstractBorderItemEditPart) {
	        				  EObject eObject =  ((View)child.getModel()).getElement();
	        				 IFigure interfaceFigure = ((AbstractBorderItemEditPart)child).getContentPane();
	        				 if(interfaceFigure instanceof InterfaceShape){
	        					 if(((InterfaceShape)interfaceFigure).getRole() == InterfaceShape.Role.SERVER){
	        						 if(interfaceFigure.getBounds().width > maxInterfaceServerWidth){
	        							 maxInterfaceServerWidth = interfaceFigure.getBounds().width;
	        						 }
	        					 }else{
	        						 if(interfaceFigure.getBounds().width > maxInterfaceClientWidth){
	        							 maxInterfaceClientWidth = interfaceFigure.getBounds().width;
	        						 }
	        					 }
	        				 }
	        			   }
	        		   }
	        		   
	        	   }
	        	   bounds.x = x + maxInterfaceServerWidth;
	        	   f.setBounds(bounds);
	           }else{
	        	   f.setBounds(bounds);
	           }
	           x += bounds.width + maxInterfaceServerWidth + maxInterfaceClientWidth;
	       }
	}
	
	protected void defaultLayout(IFigure parent){
    	Iterator<IFigure> children = parent.getChildren().iterator();
    	Point offset = getOrigin(parent);
    	IFigure f;
    	while (children.hasNext()) {
    		  f = (IFigure)children.next();
	           Rectangle bounds = (Rectangle)getConstraint(f);
	           if (bounds == null) continue;

	           int widthHint = bounds.width;
	           int heightHint = bounds.height;
	           if (widthHint == -1 || heightHint == -1) {
	               Dimension _preferredSize = f.getPreferredSize(widthHint, heightHint);
	               bounds = bounds.getCopy();
	               if (widthHint == -1)
	                   bounds.width = _preferredSize.width;
	               if (heightHint == -1)
	                   bounds.height = _preferredSize.height;
	           }
	           Dimension min = f.getMinimumSize(widthHint, heightHint);
	           Dimension max = f.getMaximumSize();
	           
	           if (min.width>bounds.width)
	               bounds.width = min.width;
	           else if (max.width < bounds.width)
	               bounds.width = max.width;
	           
	           if (min.height>bounds.height)
	               bounds.height = min.height;
	           else if (max.height < bounds.height)
	               bounds.height = max.height;
	           bounds = bounds.getTranslated(offset);
	           f.setBounds(bounds);
	           
	       	  // Overwrite default layout to avoid overlapping between components, and
	       	  // use the interfaces length in the layout calculus.
	       	 
	           //moveConflictFigures(parent,f);	       		
    	}
    	
	}
	
    public void layout(IFigure parent) {
//    	horizontalLayout(parent);
    	//defaultLayout(parent);
    	super.layout(parent);
    }
    
    /**
     * Determine the components that overlap the given component figure, and
     * move them on the right.
     * 
     * @param parent : the container figure that contains the subcomponents
     * @param figure : the sub component figure
     */
    public void moveConflictFigures(IFigure parent, IFigure figure){
    	List<IFigure> conflictFigures = getConflictFigures(parent, figure);
		InterfacesSize currentInterfacesSize = getInterfacesSize(figure);
		InterfacesSize nextInterfacesSize;
		int stop=0;
		while(!conflictFigures.isEmpty()){
			IFigure next = conflictFigures.get(0);
			
			Rectangle nextBounds = next.getBounds().getCopy();
			nextInterfacesSize = getInterfacesSize(next);
			
			nextBounds.x = figure.getBounds().x + figure.getBounds().width
				+ currentInterfacesSize.maxClientInterfaceLength 
				+ nextInterfacesSize.maxServerInterfaceLength;
					
			conflictFigures.remove(0);
			stop++;
			next.setBounds(nextBounds);
			moveConflictFigures(parent, next);

		}
    }
    
    /**
     * Return the list of figures that overlap the given component figure.
     * 
     * @param containerFigure
     * @param componentFigure
     * @return
     */
    protected List<IFigure> getConflictFigures(IFigure containerFigure, IFigure componentFigure){
    	List<IFigure> result = new ArrayList<IFigure>();
    	
    	Iterator<IFigure> children = containerFigure.getChildren().iterator();
	    IFigure child;
	    Rectangle componentWithInterfaces = getComponentSizeWithInterfaces(componentFigure);
	     while (children.hasNext()) {
	    	 child = (IFigure)children.next();
	    	 if(child instanceof BorderedNodeFigure){
	    		 BorderedNodeFigure nodeFigure = (BorderedNodeFigure)child;
	        	   nodeFigure.getExtendedBounds();
	        	   Object object = this.getVisualPartMap().get(nodeFigure);
	        	  
	        	   if(object != null && object instanceof AbstractBorderedShapeEditPart){
	        		   // The child is a component
	        		   AbstractBorderedShapeEditPart editPart = (AbstractBorderedShapeEditPart)object;
	        		   Rectangle component = getComponentSizeWithInterfaces(editPart);
	        		   if(	child != componentFigure &&
	        			   component.intersects(componentWithInterfaces)){
	        			   result.add(child);
	        		   }
	        	   }
	    	 }
	     }
	     
    	return result;
    }
    
    protected Rectangle getComponentSizeWithInterfaces(IFigure componentFigure){
    	Rectangle result = componentFigure.getBounds();
    	if(componentFigure instanceof BorderedNodeFigure){
   		 BorderedNodeFigure nodeFigure = (BorderedNodeFigure)componentFigure;
       	   nodeFigure.getExtendedBounds();
       	   Object object = this.getVisualPartMap().get(nodeFigure);
       	  
       	   if(object != null && object instanceof AbstractBorderedShapeEditPart){
       		   result = getComponentSizeWithInterfaces((AbstractBorderedShapeEditPart)object);
       	   }
    	}
       	return result;  
    }
    
    protected Rectangle getComponentSizeWithInterfaces(AbstractBorderedShapeEditPart component){
    	Rectangle result = component.getFigure().getBounds().getCopy();

   		int maxServerInterfacesWidth = 0;
		int maxClientInterfacesWidth = 0;
		
		Iterator<EditPart> iterator = component.getChildren().iterator();
		while(iterator.hasNext()){
			EditPart c = iterator.next();
			if(c instanceof AbstractBorderItemEditPart) {
				EObject eObject =  ((View)c.getModel()).getElement();
				IFigure interfaceFigure = ((AbstractBorderItemEditPart)c).getContentPane();
				if(interfaceFigure instanceof InterfaceShape){
					if(((InterfaceShape)interfaceFigure).getRole() == InterfaceShape.Role.SERVER){
						maxServerInterfacesWidth = 
							maxServerInterfacesWidth<interfaceFigure.getSize().width?
								interfaceFigure.getSize().width:maxServerInterfacesWidth;
					}else{
						maxClientInterfacesWidth = 
							maxClientInterfacesWidth<interfaceFigure.getSize().width?
								interfaceFigure.getSize().width:maxServerInterfacesWidth;
					}
				}
			}
		}
			
		result.x-=maxServerInterfacesWidth;
    	result.width+=maxClientInterfacesWidth+maxServerInterfacesWidth;
    	return result;
    }
    
    /**
     * Return the maximal client and server interfaces length for the given component figure.
     * 
     * @param componentFigure
     * @return
     */
    protected InterfacesSize getInterfacesSize(IFigure componentFigure){
    	InterfacesSize result = new InterfacesSize(0, 0);
    	if(componentFigure instanceof BorderedNodeFigure){
   		 BorderedNodeFigure nodeFigure = (BorderedNodeFigure)componentFigure;
       	   nodeFigure.getExtendedBounds();
       	   Object object = this.getVisualPartMap().get(nodeFigure);
       	  
       	   if(object != null && object instanceof AbstractBorderedShapeEditPart){
       		   result = getInterfacesSize((AbstractBorderedShapeEditPart)object);
       	   }
    	}
       	return result;  
    }
    
    /**
     * 
     * @param component
     * @return
     */
    public InterfacesSize getInterfacesSize(AbstractBorderedShapeEditPart component){
    	InterfacesSize result = new InterfacesSize(0,0);

   		int maxServerInterfacesWidth = 0;
		int maxClientInterfacesWidth = 0;
		
		Iterator<EditPart> iterator = component.getChildren().iterator();
		while(iterator.hasNext()){
			EditPart c = iterator.next();
			if(c instanceof AbstractBorderItemEditPart) {
				EObject eObject =  ((View)c.getModel()).getElement();
				IFigure interfaceFigure = ((AbstractBorderItemEditPart)c).getContentPane();
				if(interfaceFigure instanceof InterfaceShape){
					if(((InterfaceShape)interfaceFigure).getRole() == InterfaceShape.Role.SERVER){
						maxServerInterfacesWidth = 
							maxServerInterfacesWidth<interfaceFigure.getSize().width?
								interfaceFigure.getSize().width:maxServerInterfacesWidth;
					}else{
						maxClientInterfacesWidth = 
							maxClientInterfacesWidth<interfaceFigure.getSize().width?
								interfaceFigure.getSize().width:maxServerInterfacesWidth;
					}
				}
			}
		}
			
		result.maxServerInterfaceLength=maxServerInterfacesWidth;
    	result.maxClientInterfaceLength=maxClientInterfacesWidth;
    	return result;
    }
    
    public class InterfacesSize{
    	int maxClientInterfaceLength;
    	int maxServerInterfaceLength;
    	
    	public InterfacesSize(int maxServerInterfaceLength, int maxClientInterfaceLength){
    		this.maxServerInterfaceLength = maxServerInterfaceLength;
    		this.maxClientInterfaceLength = maxClientInterfaceLength;
    	}
    }
    
}
