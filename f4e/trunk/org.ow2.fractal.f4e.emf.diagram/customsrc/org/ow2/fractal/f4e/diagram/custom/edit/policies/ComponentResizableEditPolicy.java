package org.ow2.fractal.f4e.diagram.custom.edit.policies;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.ow2.fractal.f4e.diagram.custom.edit.parts.AbstractComponentEditPart;
import org.ow2.fractal.f4e.diagram.custom.ui.handles.AttributesCollapseHandle;
import org.ow2.fractal.f4e.diagram.custom.ui.handles.ContentCollapseHandle;
import org.ow2.fractal.f4e.diagram.custom.ui.handles.ExtendsCollapseHandle;
import org.ow2.fractal.f4e.diagram.custom.ui.handles.OthersCollapseHandle;
import org.ow2.fractal.f4e.diagram.custom.ui.handles.SubComponentsCollapseHandle;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentAttributesEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentExtendsEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentOthersEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentSubComponentsEditPart;

/**
 * Customization of the edit policy in order to add 
 * the show/hide custom handles for the sub compartments.
 * 
 * @author Yann Davin
 *
 */
public class ComponentResizableEditPolicy extends AbstractComponentResizableEditPolicy {
	
	/**
	 * Creates a new vertical ResizableCompartmentEditPolicy
	 */
	public ComponentResizableEditPolicy() {
		super();
	}
	/**
	 * This method is used to get the collapse handle(s). Subclasses can
	 * override to provide different collapse handles
	 * 
	 * @return a list of collapse handles
	 */
	protected List createCollapseHandles() {
		IGraphicalEditPart part = (IGraphicalEditPart) getHost();

		List collapseHandles = new ArrayList();
		collapseHandles.add(new ExtendsCollapseHandle(part,ComponentCompartmentComponentExtendsEditPart.VISUAL_ID));
		collapseHandles.add(new SubComponentsCollapseHandle(part,ComponentCompartmentComponentSubComponentsEditPart.VISUAL_ID));
		collapseHandles.add(new ContentCollapseHandle(part,ComponentCompartmentComponentContentEditPart.VISUAL_ID));
		collapseHandles.add(new AttributesCollapseHandle(part,ComponentCompartmentComponentAttributesEditPart.VISUAL_ID));
		collapseHandles.add(new OthersCollapseHandle(part,ComponentCompartmentComponentOthersEditPart.VISUAL_ID));
		return collapseHandles;
	}
	
	
	protected Command getMoveCommand(ChangeBoundsRequest request) {
		ChangeBoundsRequest req = new ChangeBoundsRequest(REQ_MOVE_CHILDREN);
		req.setEditParts(getHost());
		
		req.setMoveDelta(request.getMoveDelta());
		req.setSizeDelta(request.getSizeDelta());
		req.setLocation(request.getLocation());
		req.setExtendedData(request.getExtendedData());
		
		
		// forbid overlapping it causes problem
		// to the layout.
		//List<EditPart> conflictEditParts = ((AbstractComponentEditPart)this.getHost()).getConflictFigures(request.getMoveDelta());
		//if(!conflictEditParts.isEmpty()){
		//	return null;
		//}
		
		return getHost().getParent().getCommand(req);
	}

	
	protected Command getAutoSizeCommand(Request request) {
		// TODO Auto-generated method stub
		return super.getAutoSizeCommand(request);
	}
	
}
