package org.ow2.fractal.f4e.diagram.custom.edit.policies.compartments;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.editpolicies.AbstractEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.l10n.DiagramUIMessages;
import org.eclipse.gmf.runtime.diagram.ui.requests.ChangePropertyValueRequest;
import org.eclipse.gmf.runtime.emf.core.util.PackageUtil;
import org.ow2.fractal.f4e.diagram.custom.ui.requests.ToggleCompartmentsRequest;

/**
 * Policy to handle show/hide compartments actions coming from the Fractal menu bar.
 * @author Yann Davin
 * 
 */
public abstract class CompartmentsEditPolicy extends AbstractEditPolicy {

	protected abstract Object getRequestType();
	
	/**
	 * 
	 * @return the attribute of the <code>NotationPackage<code> that we want to hide
	 */
	protected abstract EAttribute getAttribute();
	
	protected String getPropertyName(){
		return PackageUtil.getID(getAttribute());
	}
	
	public boolean understandsRequest(Request request) {
		return getRequestType().equals(request
				.getType());
	}
	
	public Command getCommand(Request request) {
		if (getRequestType().equals(request.getType())) {
			final boolean showHide = ((ToggleCompartmentsRequest) request)
					.showCompartment();
          
			ChangePropertyValueRequest showHiderequest = new ChangePropertyValueRequest(
					DiagramUIMessages.PropertyDescriptorFactory_CollapseCompartment,
					getPropertyName(), !showHide);
			
			ChangePropertyValueRequest collapseRequest = new ChangePropertyValueRequest(
					DiagramUIMessages.PropertyDescriptorFactory_CollapseCompartment,
					org.eclipse.gmf.runtime.diagram.ui.internal.properties.Properties.ID_COLLAPSED, !showHide);
			
			if(getHost().getParent() != null){
				Command toggleCommand = getHost().getParent().getCommand(showHiderequest);
				CompoundCommand result = new CompoundCommand();
				result.setLabel(toggleCommand.getLabel());
				result.add(getHost().getCommand(collapseRequest));
				result.add(getHost().getParent().getCommand(showHiderequest));
				return result;
			}
		}
		return super.getCommand(request);
	}
    
   protected TransactionalEditingDomain getEditingDomain() {
       return ((IGraphicalEditPart) getHost()).getEditingDomain();
   }
   
	public EditPart getTargetEditPart(Request request) {
		if (understandsRequest(request)) {
			Command command = getHost().getCommand(
					request);
			if (command != null && command.canExecute()){
				return getHost();
			}
		}
		return null;
	}
}