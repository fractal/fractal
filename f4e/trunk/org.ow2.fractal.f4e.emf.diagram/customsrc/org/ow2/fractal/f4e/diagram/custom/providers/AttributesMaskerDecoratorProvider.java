package org.ow2.fractal.f4e.diagram.custom.providers;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.diagram.ui.services.decorator.CreateDecoratorsOperation;
import org.eclipse.gmf.runtime.diagram.ui.services.decorator.IDecoratorProvider;
import org.eclipse.gmf.runtime.diagram.ui.services.decorator.IDecoratorTarget;
import org.ow2.fractal.f4e.diagram.custom.decorators.AttributesMaskerContainerDecorator;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionEditPart;


public class AttributesMaskerDecoratorProvider extends AbstractProvider implements IDecoratorProvider{
	
	public void createDecorators(IDecoratorTarget decoratorTarget){
		EditPart editPart = (EditPart)decoratorTarget.getAdapter(EditPart.class);
		if(editPart != null && editPart instanceof DefinitionEditPart){
			GraphicalEditPart gEP = (GraphicalEditPart)editPart;
			decoratorTarget.installDecorator("ATTRIBUTES_MASKER_DECORATOR", new AttributesMaskerContainerDecorator(decoratorTarget));
		}
	}
	
	public boolean provides(IOperation operation){
		if(!(operation instanceof CreateDecoratorsOperation)){
			return false;
		}
		
		IDecoratorTarget decoratorTarget = ((CreateDecoratorsOperation)operation).getDecoratorTarget();
		EditPart ep = (EditPart)decoratorTarget.getAdapter(EditPart.class);
		if(ep instanceof GraphicalEditPart && ep instanceof DefinitionEditPart){
			GraphicalEditPart gEP = (GraphicalEditPart)ep;
			return true;
		}
		
		return false;
	}
}
