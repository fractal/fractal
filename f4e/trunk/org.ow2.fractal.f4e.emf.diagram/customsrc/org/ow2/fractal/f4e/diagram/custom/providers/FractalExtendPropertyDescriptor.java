package org.ow2.fractal.f4e.diagram.custom.providers;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Composite;
import org.ow2.fractal.f4e.diagram.custom.ui.FractalExtendCellEditor;

/**
 * Modified property descriptor used for the Fractal 'extends' and 'definition' properties.
 * So when the 'extends'|'definition' property is displayed in the property page
 * we use a custom cell to display the 'extends'|'definition' value.
 * That custom cell display a little button that allows to open the completion dialog box.
 * 
 * @author Yann Davin
 *
 */
public class FractalExtendPropertyDescriptor  extends MergedPropertyDescriptor{
	 
	public FractalExtendPropertyDescriptor(Object object, IItemPropertyDescriptor itemPropertyDescriptor)
	  {
	    super(object,itemPropertyDescriptor);
	 
	  }
	
	
	public CellEditor createPropertyEditor(Composite composite) {
		 if (!itemPropertyDescriptor.canSetProperty(object))
		    {
		      return null;
		    }
		 return new FractalExtendCellEditor(composite, (EObject)object,  (EStructuralFeature)itemPropertyDescriptor.getFeature(object));
	}

	public Object getId() {
		return super.getId();
	}
	
}
