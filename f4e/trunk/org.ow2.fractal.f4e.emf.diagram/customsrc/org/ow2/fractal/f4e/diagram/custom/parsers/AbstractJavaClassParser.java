package org.ow2.fractal.f4e.diagram.custom.parsers;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ISemanticParser;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.ow2.fractal.f4e.fractal.Content;
import org.ow2.fractal.f4e.fractal.diagram.parsers.MessageFormatParser;

public abstract class AbstractJavaClassParser extends MessageFormatParser implements ISemanticParser{

	protected JavaClassCompletion contentAssist = new JavaClassCompletion();
	
	public AbstractJavaClassParser(EAttribute[] features){
		super(features);
	}
	
	public boolean areSemanticElementsAffected(EObject listener, Object notification) {
		
//		if (notification instanceof Notification) {
//			Object feature = ((Notification) notification).getFeature();
//			if (feature instanceof EStructuralFeature) {
//				EStructuralFeature featureImpl = (EStructuralFeature) feature;
////				return featureImpl.getName().startsWith(Extension.METACLASS_ROLE_PREFIX);
//			}
//		}
		return false;
	}

	public List<?> getSemanticElementsBeingParsed(EObject eObject) {
		List<EObject> result = new LinkedList<EObject>();
		result.add(eObject);
		return result;
	}

	public IContentAssistProcessor getCompletionProcessor(IAdaptable subject) {
		EObject content = (EObject)((EObjectAdapter)subject).getRealObject();
		contentAssist.setContext(content);
		return contentAssist;
	}
}