package org.ow2.fractal.f4e.diagram.custom.parsers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.WorkingCopyOwner;
import org.eclipse.jdt.core.search.IJavaSearchConstants;
import org.eclipse.jdt.core.search.IJavaSearchScope;
import org.eclipse.jdt.core.search.SearchEngine;
import org.eclipse.jdt.core.search.SearchPattern;
import org.eclipse.jdt.internal.core.search.JavaSearchTypeNameMatch;
import org.eclipse.jdt.internal.corext.util.TypeNameMatchCollector;
import org.eclipse.jdt.internal.ui.JavaPluginImages;
import org.eclipse.jface.contentassist.IContentAssistSubjectControl;
import org.eclipse.jface.contentassist.ISubjectControlContentAssistProcessor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.CompletionProposal;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.contentassist.IContextInformationValidator;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;

public class JavaClassCompletion implements IContentAssistProcessor, ISubjectControlContentAssistProcessor{

	protected static final ICompletionProposal[] NO_PROPOSALS = new ICompletionProposal[0];

	private static final IContextInformation[] NO_CONTEXTS = new IContextInformation[0];

	protected EObject myContext;

	public void setContext(EObject context) {
		myContext = context;
	}

	public Iterable<String> computeContextProposals(EObject context, String prefix){
		List<String> result = new ArrayList<String>();
		if(prefix.length()==0){
			return result;
		}
		
		 IEditorPart editor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		 IFile file = (IFile) editor.getEditorInput().getAdapter(IFile.class) ;
		 IProject project = file.getProject();
		 IJavaProject javaProject = null;	
		 try{
			if(project.hasNature(JavaCore.NATURE_ID) == true){
			  javaProject = (IJavaProject)project.getNature(JavaCore.NATURE_ID);
			}
			}catch(Exception e){
				e.printStackTrace();
			}
			
		IJavaSearchScope scope= javaProject != null ? SearchEngine.createJavaSearchScope(new IJavaElement[] { javaProject }) : SearchEngine.createWorkspaceScope();
		SearchEngine engine= new SearchEngine((WorkingCopyOwner) null);
		Collection<JavaSearchTypeNameMatch> c = new ArrayList<JavaSearchTypeNameMatch>();
		
		try {
			
			engine.searchAllTypeNames("*".toCharArray(),
					SearchPattern.R_PATTERN_MATCH,
					(prefix+"*").toCharArray(),
					SearchPattern.R_PATTERN_MATCH,
					IJavaSearchConstants.CLASS,
					scope,
					new TypeNameMatchCollector(c),
					IJavaSearchConstants.WAIT_UNTIL_READY_TO_SEARCH,
					new NullProgressMonitor());
		} catch(Exception e){
			e.printStackTrace();
		}
		
		Iterator<JavaSearchTypeNameMatch> iterator = c.iterator();
		while(iterator.hasNext()){
			JavaSearchTypeNameMatch match = iterator.next();
			result.add(match.getFullyQualifiedName());
		}
		
		return result;
	}
	
	public ICompletionProposal[] computeCompletionProposals(IContentAssistSubjectControl subjectControl, int offset) {
		if (myContext == null) {
			return NO_PROPOSALS;
		}
		Point selection = subjectControl.getSelectedRange();
		int selectionStart = selection.x;
		int selectionLength = selection.y;
		String prefix = getPrefix(subjectControl, selectionStart);
		String proposalPrefix = getProposalPrefix(prefix);
		int prefixLength = proposalPrefix.length();

		List<ICompletionProposal> result = new LinkedList<ICompletionProposal>();
		for (String next : computeContextProposals(myContext,prefix)) {
			ICompletionProposal proposal = new CompletionProposal(next, selectionStart - prefixLength, selectionLength + prefixLength, next.length(), JavaPluginImages.getDescriptor(JavaPluginImages.IMG_OBJS_CLASS).createImage(), next, null, null);
			result.add(proposal);
		}
		return result.toArray(NO_PROPOSALS);
	}

	public IContextInformation[] computeContextInformation(IContentAssistSubjectControl contentAssistSubjectControl, int documentOffset) {
		return NO_CONTEXTS;
	}

	public ICompletionProposal[] computeCompletionProposals(ITextViewer viewer, int offset) {
		throw new UnsupportedOperationException("use ISubjectControlContentAssistProcessor instead");
	}

	public IContextInformation[] computeContextInformation(ITextViewer viewer, int offset) {
		return NO_CONTEXTS;
	}

	public char[] getCompletionProposalAutoActivationCharacters() {
		return null;
	}

	public char[] getContextInformationAutoActivationCharacters() {
		return null;
	}

	public IContextInformationValidator getContextInformationValidator() {
		return null;
	}

	public String getErrorMessage() {
		return null;
	}

	protected final String getPrefix(IContentAssistSubjectControl subjectControl, int offset) {
		IDocument doc = subjectControl.getDocument();
		if (doc == null || offset > doc.getLength()) {
			throw new IllegalStateException("Bad content assist subject control: " + doc);
		}
		try {
			return doc.get(0, offset);
		} catch (BadLocationException e) {
			throw new IllegalStateException(e);
		}
	}
	
	protected String getProposalPrefix(String prefix) {
		return prefix;
	}

}
