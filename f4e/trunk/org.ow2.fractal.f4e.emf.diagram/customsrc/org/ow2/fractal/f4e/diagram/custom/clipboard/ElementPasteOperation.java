package org.ow2.fractal.f4e.diagram.custom.clipboard;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.gmf.runtime.diagram.ui.parts.IDiagramGraphicalViewer;
import org.eclipse.gmf.runtime.emf.clipboard.core.ObjectInfo;
import org.eclipse.gmf.runtime.emf.clipboard.core.PasteChildOperation;
import org.eclipse.gmf.runtime.emf.clipboard.core.PostPasteChildOperation;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.providers.internal.copypaste.PositionalGeneralViewPasteOperation;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;

public class ElementPasteOperation extends PositionalGeneralViewPasteOperation{

	public ElementPasteOperation(
			PasteChildOperation overriddenChildPasteOperation,
			boolean shouldPasteAlwaysCopyObject) {
		super(overriddenChildPasteOperation, shouldPasteAlwaysCopyObject);
	}
	
	public PasteChildOperation getPostPasteOperation() {

		return new PostPasteChildOperation(this, EMPTY_ARRAY) {

			public void paste()
				throws Exception {
				//unset connectors before pasting so that it won't affect
				//real connectors especially if they happen to belong to the
				// same
				//target diagram				
				Node view = (Node) getEObject();
				
				view.eUnset(NotationPackage.eINSTANCE.getView_SourceEdges());
				view.eUnset(NotationPackage.eINSTANCE.getView_TargetEdges());

				//now paste view
				EObject pastedElement = doPasteInto(getParentEObject());
				//did we succeed?
				if (pastedElement != null) {
					setPastedElement(pastedElement);
					addPastedElement(pastedElement);
				} else {
					addPasteFailuresObject(getEObject());
				}
			}

			protected boolean shouldPasteAlwaysCopyObject(
					ObjectInfo alwaysCopyObjectInfo) {
				return ElementPasteOperation.this
					.shouldPasteAlwaysCopyObject(alwaysCopyObjectInfo);
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.gmf.runtime.emf.core.internal.copypaste.PasteChildOperation#makeAuxiliaryChildPasteProcess(org.eclipse.gmf.runtime.emf.core.internal.copypaste.ObjectInfo)
			 */
			protected PasteChildOperation makeAuxiliaryChildPasteProcess(
					ObjectInfo auxiliaryChildEObjectInfo) {				
				EObject semanticPasteTarget = ClipboardSupport
					.getSemanticPasteTarget((View) getPastedElement());
				if (semanticPasteTarget == null) {
					return null;
				}
				return new PasteChildOperation(getParentPasteProcess().clone(
					semanticPasteTarget), auxiliaryChildEObjectInfo);
			}

			public PasteChildOperation getPostPasteOperation() {
				List operations = getAlwaysCopyObjectPasteOperations();
				return new PostPasteChildOperation(this, operations);
			}
		};
	}

}
