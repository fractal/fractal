package org.ow2.fractal.f4e.diagram.custom.ui.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.ui.IWorkbenchPage;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.l10n.FractalDiagramUIActionsPluginImages;
import org.ow2.fractal.f4e.diagram.custom.ui.requests.FractalRequestConstants;
import org.ow2.fractal.f4e.diagram.custom.ui.requests.ToggleCompartmentsRequest;
import org.ow2.fractal.f4e.diagram.custom.ui.requests.ToggleInterfaceLabelsRequest;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentCompartmentComponentContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.ComponentMergedCompartmentComponentContentEditPart;
import org.ow2.fractal.f4e.fractal.diagram.edit.parts.DefinitionCompartmentDefinitionContentEditPart;

public class HideContentCompartmentsAction extends ShowHideCompartmentsAction {

	/**
	 * @param workbenchPage
	 */
	public HideContentCompartmentsAction(IWorkbenchPage workbenchPage) {
		super(workbenchPage);
	}
	
	protected boolean isValid(EditPart editPart) {
		return 	editPart instanceof ComponentCompartmentComponentContentEditPart ||
				editPart instanceof DefinitionCompartmentDefinitionContentEditPart ||
				editPart instanceof ComponentMergedCompartmentComponentContentEditPart;
	}
	
	/**
	 * Add text and image descriptors.
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#init()
	 */
	public void init() {
		super.init();
		setText(FractalDiagramUIActionsMessages.HideContentCompartmentsAction_label);
		setId(FractalActionIds.ACTION_HIDE_CONTENT_COMPARTMENTS);
		setToolTipText(FractalDiagramUIActionsMessages.HideContentCompartmentsAction_toolTip);
		setImageDescriptor(FractalDiagramUIActionsPluginImages.DESC_HIDE_CONTENT_COMPARTMENTS);
		setDisabledImageDescriptor(FractalDiagramUIActionsPluginImages.DESC_HIDE_CONTENT_COMPARTMENTS_DISABLED);
	}	

	/**
	 * Returns an instance of <code>ToggleConnectionLabelsRequest</code>
	 * 
	 * @return the request
	 * @see org.eclipse.gmf.runtime.diagram.ui.actions.DiagramAction#createTargetRequest()
	 */
	protected Request createTargetRequest() {
		return new ToggleCompartmentsRequest(false,FractalRequestConstants.REQ_TOGGLE_CONTENT_COMPARMENTS);
	}
}
