package org.ow2.fractal.f4e.diagram.custom.ui;

import org.eclipse.osgi.util.NLS;

// TODO externalize strings
public class UIDialogBoxMessages {
	private static final String BUNDLE_NAME = "org.ow2.fractal.f4e.diagram.custom.ui.UIDialogBoxMessages";

	public static String NLSJavaClass_SelectionDialog_Title;
	public static String NLSJavaClass_SelectionDialog_Select;

	public static String NLSJavaInterface_SelectionDialog_Title;
	public static String NLSJavaInterface_SelectionDialog_Select;

	public static String NLSFractalDefinition_SelectionDialog_Title;
	public static String NLSFractalDefinition_SelectionDialog_Select;
		
	static {
		NLS.initializeMessages(BUNDLE_NAME, UIDialogBoxMessages.class);
	}
}
