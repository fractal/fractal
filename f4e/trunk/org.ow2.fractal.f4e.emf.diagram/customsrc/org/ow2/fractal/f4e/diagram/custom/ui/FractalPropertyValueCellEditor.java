package org.ow2.fractal.f4e.diagram.custom.ui;


import java.awt.RenderingHints.Key;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Text;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.Role;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IAbstractComponentHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelper;



/**
 * The Class FractalPropertyValueCellEditor.
 */
public class FractalPropertyValueCellEditor extends CellEditor {

	/** Image registry key for three dot image (value <code>"cell_editor_dots_button_image"</code>). */
	public static final String CELL_EDITOR_IMG_DOTS_BUTTON = "cell_editor_dots_button_image";//$NON-NLS-1$

	/** The editor control. */
	protected Composite editor;


	/** The text. */
	protected Text text;

	/** The button. */
	protected Button button;

	/** Listens for 'focusLost' events and  fires the 'apply' event as long as the focus wasn't lost because the dialog was opened. */
	protected FocusListener buttonFocusListener;
	protected FocusListener textFocusListener;
	
	/** The value of this cell editor; initially <code>null</code>. */
	private Object value = null;

	protected EObject modelObject;
	
	protected EStructuralFeature feature;
	
	static {
		ImageRegistry reg = JFaceResources.getImageRegistry();
		reg.put(CELL_EDITOR_IMG_DOTS_BUTTON, ImageDescriptor.createFromFile(
				DialogCellEditor.class, "images/dots_button.gif"));//$NON-NLS-1$
	}

	/**
	 * Internal class for laying out the dialog.
	 */
	private class DialogCellLayout extends Layout {

		/* (non-Javadoc)
		 * @see org.eclipse.swt.widgets.Layout#layout(org.eclipse.swt.widgets.Composite, boolean)
		 */
		public void layout(Composite editor, boolean force) {
			Rectangle bounds = editor.getClientArea();
			Point size = button.computeSize(SWT.DEFAULT, SWT.DEFAULT, force);
			if (text != null) {
				text.setBounds(0, 0, bounds.width - size.x, bounds.height);
			}
			button.setBounds(bounds.width - size.x, 0, size.x, bounds.height);
		}

		/* (non-Javadoc)
		 * @see org.eclipse.swt.widgets.Layout#computeSize(org.eclipse.swt.widgets.Composite, int, int, boolean)
		 */
		public Point computeSize(Composite editor, int wHint, int hHint,
				boolean force) {
			if (wHint != SWT.DEFAULT && hHint != SWT.DEFAULT) {
				return new Point(wHint, hHint);
			}
			Point contentsSize = text.computeSize(SWT.DEFAULT, SWT.DEFAULT,
					force);
			Point buttonSize = button.computeSize(SWT.DEFAULT, SWT.DEFAULT,
					force);
			// Just return the button width to ensure the button is not clipped
			// if the label is long.
			// The label will just use whatever extra width there is
			Point result = new Point(buttonSize.x, Math.max(contentsSize.y,
					buttonSize.y));
			return result;
		}
		
		
	}

	/** Default DialogCellEditor style. */
	private static final int defaultStyle = SWT.NONE;

	/**
	 * Creates a new dialog cell editor with no control.
	 * 
	 * @since 2.1
	 */
	private FractalPropertyValueCellEditor() {
		setStyle(defaultStyle);
	}

	/**
	 * Creates a new dialog cell editor parented under the given control.
	 * The cell editor value is <code>null</code> initially, and has no
	 * validator.
	 * 
	 * @param parent the parent control
	 */
	public FractalPropertyValueCellEditor(Composite parent, EObject modelObject, EStructuralFeature feature) {
		this(parent,modelObject, defaultStyle);
		this.feature = feature;
	}

	/**
	 * Creates a new dialog cell editor parented under the given control.
	 * The cell editor value is <code>null</code> initially, and has no
	 * validator.
	 * 
	 * @param parent the parent control
	 * @param style the style bits
	 * 
	 * @since 2.1
	 */
	protected FractalPropertyValueCellEditor(Composite parent, EObject modelObject , int style) {
		super(parent,  style);
		this.modelObject = modelObject;
	
	}



	/**
	 * Creates the controls used to show the value of this cell editor.
	 * <p>
	 * The default implementation of this framework method creates
	 * a label widget, using the same font and background color as the parent control.
	 * </p>
	 * <p>
	 * Subclasses may reimplement.  If you reimplement this method, you
	 * should also reimplement <code>updateContents</code>.
	 * </p>
	 * 
	 * @param cell the control for this cell editor
	 * 
	 * @return the underlying control
	 */
	protected Text createContents(Composite cell) {
		text =  new Text(cell, SWT.NONE);
		text.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent e) {
				if(e.character == '\r'){
					doSetValue(text.getText());
					FractalPropertyValueCellEditor.this.focusLost();
				}
			}

			public void keyReleased(KeyEvent e) {
				
			}
			
		});
		text.addDisposeListener(new DisposeListener(){

			public void widgetDisposed(DisposeEvent e) {
			}
			
		});
		return text;
	}

	/* (non-Javadoc)
	 * Method declared on CellEditor.
	 */
	protected Control createControl(Composite parent) {

		Font font = parent.getFont();
		Color bg = parent.getBackground();

		editor = new Composite(parent, getStyle());
		editor.setFont(font);
		editor.setBackground(bg);
		editor.setLayout(new DialogCellLayout());

		text = createContents(editor);
		//textListener = new MyFilteringAdapter(text);
		text.addFocusListener(getTextFocusListener());
		
	//	textListener = new MyFilteringAdapter(text);

		button = createButton(editor);
		button.setFont(font);

	
		  
		button.addFocusListener(getButtonFocusListener());

		button.addMouseListener(new MouseListener(){

			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub

			}
			
			public void mouseDown(MouseEvent e) {
				Object newValue = openDialogBox(editor);
				
				if (newValue != null) {
					boolean newValidState = isCorrect(newValue);
					if (newValidState) {
						markDirty();
						doSetValue(newValue);
					} else {
						// try to insert the current value into the error message.
						setErrorMessage(MessageFormat.format(getErrorMessage(),
								new Object[] { newValue.toString() }));
					}
					try{
						fireApplyEditorValue();
					}catch(Exception ee){
						ee.printStackTrace();
					}
				}
			}

			public void mouseUp(MouseEvent e) {
				// TODO Auto-generated method stub

			}

		});

		 
		setValueValid(true);

		return editor;
	}

	/* (non-Javadoc)
	 * 
	 * Override in order to remove the button's focus listener if the celleditor
	 * is deactivating.
	 * 
	 * @see org.eclipse.jface.viewers.CellEditor#deactivate()
	 */
	public void deactivate() {
		if (text != null && !text.isDisposed()) {
			text.removeFocusListener(textFocusListener);
			button.removeFocusListener(buttonFocusListener);
		}
		super.deactivate();
	}

	/* (non-Javadoc)
	 * Method declared on CellEditor.
	 */
	protected Object doGetValue() {
		return value;
	}

	/* (non-Javadoc)
	 * Method declared on CellEditor.
	 * The focus is set to the cell editor's button. 
	 */
	protected void doSetFocus() {
		text.setFocus();
		
		text.addFocusListener(getTextFocusListener());
		button.addFocusListener(getButtonFocusListener());
	}

	/**
	 * Return a listener for button focus.
	 * 
	 * @return FocusListener
	 */
	private FocusListener getButtonFocusListener() {
		if (buttonFocusListener == null) {
			buttonFocusListener = new FocusListener() {

				/* (non-Javadoc)
				 * @see org.eclipse.swt.events.FocusListener#focusGained(org.eclipse.swt.events.FocusEvent)
				 */
				public void focusGained(FocusEvent e) {
					// Do nothing
				
				}

				/* (non-Javadoc)
				 * @see org.eclipse.swt.events.FocusListener#focusLost(org.eclipse.swt.events.FocusEvent)
				 */
				public void focusLost(FocusEvent e) {
				
					//FractalPropertyValueCellEditor.this.focusLost();
				}
			};
		}

		return buttonFocusListener;
	}
	
	
	private FocusListener getTextFocusListener() {
		if(textFocusListener == null) {
			textFocusListener = new FocusListener() {

				public void focusGained(FocusEvent e) {
				}
 
				public void focusLost(FocusEvent e) {
					doSetValue(text.getText());
					//FractalPropertyValueCellEditor.this.focusLost();
					
				}
			};
		}

		return textFocusListener;
	}


	/* (non-Javadoc)
	 * Method declared on CellEditor.
	 */
	protected void doSetValue(Object value) {
		this.value = value;
		updateContents(value);
	}


	/**
	 * Updates the controls showing the value of this cell editor.
	 * <p>
	 * The default implementation of this framework method just converts
	 * the passed object to a string using <code>toString</code> and
	 * sets this as the text of the label widget.
	 * </p>
	 * <p>
	 * Subclasses may reimplement.  If you reimplement this method, you
	 * should also reimplement <code>createContents</code>.
	 * </p>
	 * 
	 * @param value the new value of this cell editor
	 */
	protected void updateContents(Object value) {
		if (text == null) {
			return;
		}

		String defaultValue = "";
		if (value != null) {
			defaultValue = value.toString();
		}
		
		if(!text.isDisposed()){
			text.setText(defaultValue);
		}
	}

	/**
	 * Creates the button.
	 * 
	 * @param parent the parent
	 * 
	 * @return the button
	 */
	protected Button createButton(Composite parent) {
		Button result = new Button(parent, SWT.DOWN);
		result.setText("..."); 
		return result;
	}

	protected List<? extends Object> getInitialElementSelections(){
		
		List<Interface> result = new ArrayList<Interface>();
		if(this.modelObject instanceof Binding && 
				((Binding)this.modelObject).eContainer() != null && 
				((Binding)this.modelObject).eContainer() instanceof AbstractComponent){
			AbstractComponent container = (AbstractComponent)((Binding)this.modelObject).eContainer();
			IHelper<?> helper = HelperAdapterFactory.getInstance().adapt(container);
			
			if(helper != null && feature == FractalPackage.eINSTANCE.getBinding_Client()){
				//result.addAll(((IAbstractComponentHelper)container.getAdapter(IAbstractComponentHelper.class)).getInterfaces(Role.SERVER));
				result.addAll(((IAbstractComponentHelper)helper).getInterfaces(Role.SERVER));
			
				Iterator<Component> iterator = container.getSubComponents().iterator();
				while(iterator.hasNext()){
					Component component = iterator.next();
					helper = HelperAdapterFactory.getInstance().adapt(component);
					if(helper != null){
						result.addAll(((IAbstractComponentHelper)helper).getInterfaces(Role.CLIENT));
					}
				}
			}else if(helper != null && feature == FractalPackage.eINSTANCE.getBinding_Server()){
				result.addAll(((IAbstractComponentHelper)helper).getInterfaces(Role.CLIENT));
				
				Iterator<Component> iterator = container.getSubComponents().iterator();
				while(iterator.hasNext()){
					Component component = iterator.next();
					helper = HelperAdapterFactory.getInstance().adapt(component);
					if(helper != null){
						result.addAll(((IAbstractComponentHelper)helper).getInterfaces(Role.SERVER));
					}
				}
			}
		}
		return result;
	}
	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.DialogCellEditor#openDialogBox(org.eclipse.swt.widgets.Control)
	 */

	/**
	 * Open dialog box.
	 * 
	 * @param cellEditorWindow the cell editor window
	 * 
	 * @return the object
	 */
	protected Object openDialogBox(Control cellEditorWindow) {
		// final ModelObject returnValue = ((ModelObject) getValue());
		// opening the dialog
		FractalSelectionDialog dialog = new FractalSelectionDialog(
				cellEditorWindow.getShell(), modelObject);
		dialog.setInitialElementSelections(getInitialElementSelections());
	
		if (dialog.open() == IDialogConstants.OK_ID) {
			return dialog.getFirstResult();
		}
		
		return dialog.getFirstResult();
	}


}
