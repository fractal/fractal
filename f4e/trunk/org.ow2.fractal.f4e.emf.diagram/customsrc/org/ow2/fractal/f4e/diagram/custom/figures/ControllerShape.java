package org.ow2.fractal.f4e.diagram.custom.figures;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * Graphical representation for the controller of
 * a definition/component
 * 
 * @author Yann Davin
 * 
 */
public class ControllerShape extends Shape {

	protected void fillShape(Graphics graphics) {
		graphics.fillRectangle(getBounds());
	}

	public Rectangle getBounds() {
		Rectangle r = super.getBounds();
		Rectangle result = new Rectangle(r.x, r.y , r.width, r.height);
		return result;
	}

	protected void outlineShape(Graphics graphics) {
		this.setLineWidth(1);
		Rectangle r = this.getBounds();
		r.setSize(r.width - 1, r.height - 1);
	}

}
