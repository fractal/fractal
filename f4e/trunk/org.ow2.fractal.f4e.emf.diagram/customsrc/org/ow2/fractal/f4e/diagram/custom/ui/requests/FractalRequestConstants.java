package org.ow2.fractal.f4e.diagram.custom.ui.requests;

public class FractalRequestConstants {
	/**
	 * constants for toggle interface labels request
	 */
	public static final String 	REQ_TOGGLE_INTERFACE_LABELS = "toggle_interface_labels";
	
	public static final String 	REQ_TOGGLE_SUBCOMPONENTS_COMPARMENTS = "toggle_subcomponents_comparments";
	public static final String 	REQ_TOGGLE_ATTRIBUTES_CONTROLLER_COMPARMENTS = "toggle_attributes_controller_comparments";
	public static final String 	REQ_TOGGLE_CONTENT_COMPARMENTS = "toggle_content_comparments";
	public static final String 	REQ_TOGGLE_OTHERS_COMPARMENTS = "toggle_others_comparments";
}
