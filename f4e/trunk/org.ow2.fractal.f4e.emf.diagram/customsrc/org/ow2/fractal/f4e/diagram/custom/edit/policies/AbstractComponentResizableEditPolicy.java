package org.ow2.fractal.f4e.diagram.custom.edit.policies;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartListener;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ResizableCompartmentEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.ResizableShapeEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.figures.BorderedNodeFigure;
import org.eclipse.gmf.runtime.diagram.ui.figures.ResizableCompartmentFigure;

/**
 * Custom edit policy to manage compartment handles to show/hide sub compartments. 
 * @author Yann Davin
 *
 */
public abstract class AbstractComponentResizableEditPolicy extends ResizableShapeEditPolicy {
	
	/**
	 * Creates a new vertical ResizableCompartmentEditPolicy
	 */
	public AbstractComponentResizableEditPolicy() {
		super();
	}
	/**
	 * This method is used to get the collapse handle(s). Subclasses can
	 * override to provide different collapse handles
	 * 
	 * @return a list of collapse handles
	 */
	protected abstract List createCollapseHandles();
	
	/**
	 * @see org.eclipse.gef.editpolicies.SelectionHandlesEditPolicy#createSelectionHandles()
	 */
	protected List createSelectionHandles() {
		List handles = super.createSelectionHandles();
		IGraphicalEditPart part = (IGraphicalEditPart) getHost();
		List selectionHandles = new ArrayList();
		selectionHandles.addAll(createCollapseHandles());
		selectionHandles.addAll(handles);
		return selectionHandles;
	}
	
	/**
	 * @return the <code>ResizableCompartmentFigure</code> that is the
	 *         corresponding figure for the host edit part.
	 */
	private ResizableCompartmentFigure getCompartmentFigure() {
		ResizableCompartmentFigure compartmentFigure = null;
		if (getGraphicalEditPart() instanceof ResizableCompartmentEditPart) {
			compartmentFigure = ((ResizableCompartmentEditPart) getGraphicalEditPart())
				.getCompartmentFigure();
		} else if (getGraphicalEditPart().getFigure() instanceof ResizableCompartmentFigure) {
			compartmentFigure = (ResizableCompartmentFigure) getGraphicalEditPart()
				.getFigure();
		}
		// TODO: remove later. this is a temporary fix for defect
		// RATLC00522565
		// eventually we will put the BorderedNodeFigure inside the resizable
		// compartment
		else if (getGraphicalEditPart().getFigure() instanceof BorderedNodeFigure) {
			BorderedNodeFigure gpf = (BorderedNodeFigure) getGraphicalEditPart()
				.getFigure();
			IFigure f = gpf.getMainFigure();
			if (f instanceof ResizableCompartmentFigure) {
				compartmentFigure = (ResizableCompartmentFigure) f;
			}
		}

		return compartmentFigure;
	}

	/**
	 * @see org.eclipse.gef.editpolicies.SelectionEditPolicy#showSelection()
	 */
	protected void showSelection() {
		super.showSelection();
		if (getHost().getSelected() != EditPart.SELECTED_NONE) {
//			ResizableCompartmentFigure compartmentFigure = getCompartmentFigure();
//			if (compartmentFigure != null) {
//				compartmentFigure.setSelected(true);
//			}
		}
	}

	/**
	 * @see org.eclipse.gef.editpolicies.SelectionEditPolicy#hideSelection()
	 */
	protected void hideSelection() {
		super.hideSelection();
		if (getHost().getSelected() == EditPart.SELECTED_NONE) {
//			ResizableCompartmentFigure compartmentFigure = getCompartmentFigure();
//			if (compartmentFigure != null) {
//				compartmentFigure.setSelected(false);
//			}
		}
	}
    
    /* (non-Javadoc)
     * @see org.eclipse.gef.editpolicies.SelectionEditPolicy#activate()
     */
    public void activate() {
        super.activate();
        if (getHost().getParent().getSelected() != EditPart.SELECTED_NONE)
            setSelectedState();
    }

    private EditPartListener hostListener;

	private EditPartListener parentListener;

	/**
	 * @see org.eclipse.gef.editpolicies.SelectionEditPolicy#addSelectionListener()
	 */
	protected void addSelectionListener() {
		hostListener = new EditPartListener.Stub() {
			
			public void selectedStateChanged(EditPart part) {
				setSelectedState();
				setFocus(part.hasFocus());
			}
		};
		getHost().addEditPartListener(hostListener);

		parentListener = new EditPartListener.Stub() {

			public void selectedStateChanged(EditPart part) {
				setSelectedState();
			}
		};
		getParentGraphicEditPart().addEditPartListener(parentListener);

	}

	/**
	 * @see org.eclipse.gef.editpolicies.SelectionEditPolicy#removeSelectionListener()
	 */
	protected void removeSelectionListener() {
		getHost().removeEditPartListener(hostListener);
		getParentGraphicEditPart().removeEditPartListener(parentListener);
	}

	/**
	 * Determine the select state of the policy based on: 1- The select state of
	 * the compartment editpart 2- The select state of the parent graphic
	 * editpart 3- The visibility state of the compartment editpart
	 */
	protected void setSelectedState() {
		int hostState = getHost().getSelected();
		int topState = EditPart.SELECTED_NONE;

		if (getGraphicalEditPart().getTopGraphicEditPart() != null) {
			topState = getGraphicalEditPart().getTopGraphicEditPart().getSelected();
		}

		boolean vis = getGraphicalEditPart().getNotationView().isVisible();

		if (vis
			&& ((hostState != EditPart.SELECTED_NONE || topState != EditPart.SELECTED_NONE)))
			setSelectedState(EditPart.SELECTED);
		else
			setSelectedState(EditPart.SELECTED_NONE);
	}

	private IGraphicalEditPart getParentGraphicEditPart() {
		return (IGraphicalEditPart) getGraphicalEditPart().getParent();
	}

	private IGraphicalEditPart getGraphicalEditPart() {
		return (IGraphicalEditPart) getHost();
	}

}
