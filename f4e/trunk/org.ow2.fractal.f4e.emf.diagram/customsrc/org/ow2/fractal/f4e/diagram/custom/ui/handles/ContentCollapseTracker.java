package org.ow2.fractal.f4e.diagram.custom.ui.handles;

import org.eclipse.emf.ecore.EAttribute;


public class ContentCollapseTracker extends CompartmentCollapseTracker{
	
	public ContentCollapseTracker(ICompartmentCollapseHandle handle){
		super(handle);
	}
	
	protected EAttribute getAttribute(){
		return org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE.getAbstractComponentStyle_CollapsedContent();
	}
	
	protected String getCommandName() {
		return "Collapse the content compartment"; //$NON-NLS-1$
	}

	protected String getDebugName() {
		return "Collapse the content compartment tool"; //$NON-NLS-1$
	}
}
