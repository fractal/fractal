package org.ow2.fractal.f4e.diagram.custom.ui;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.IOpenListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.OpenEvent;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.SelectionStatusDialog;
import org.ow2.fractal.f4e.fractal.provider.ExtendedFractalItemProviderAdapterFactory;
import org.ow2.fractal.f4e.fractal.provider.InterfaceDescriptor;

/**
 * 
 * @author Yann Davin
 * 
 */
public class FractalSelectionDialog extends SelectionStatusDialog {
	final ExtendedFractalItemProviderAdapterFactory factory = new ExtendedFractalItemProviderAdapterFactory();
	Object modelObject = null;
	
	/**
	 * Implements a {@link ViewFilter} based on content typed in the filter
	 * field
	 */
	class TaskFilter extends ViewerFilter {

		private Pattern pattern;

		public void setFilterText(final String filterText) {
			String newText = filterText + "*"; 
			if (newText.trim().equals("")) { 
				this.pattern = null;
			} else {
				newText = newText.replace("\\", "\\\\"); 
				newText = newText.replace(".", "\\.");
				newText = newText.replace("*", ".*"); 
				newText = newText.replace("?", ".?");
				this.pattern = Pattern.compile(newText, Pattern.CASE_INSENSITIVE);
			}
		}

		
		public boolean select(final Viewer viewer, final Object parentElement, final Object element) {
			boolean returnValue = true;
			if (this.pattern != null) {
				returnValue = this.pattern.matcher(getText(element)).matches()
				|| this.pattern.matcher(getText(element)).matches();
			}
			return returnValue;
		}
	}

	TableViewer viewer;



	public FractalSelectionDialog(final Shell parent, final Object modelObject) {
		super(parent);
		this.modelObject = modelObject;
		setShellStyle(getShellStyle() | SWT.RESIZE);
	}

	protected void configureShell(final Shell shell) {
		shell.setText("Interface Selection"); //$NON-NLS-1$
		super.configureShell(shell);
	}

	

	
	protected Control createDialogArea(final Composite parent) {
		final Composite area = (Composite) super.createDialogArea(parent);

		final Label message = new Label(area, SWT.NONE);
		message.setText("&Select a Interface (? = any character, * = any String):");
		final Text filterText = new Text(area, SWT.SINGLE | SWT.BORDER);
		filterText.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false));

		final Label matches = new Label(area, SWT.NONE);
		matches.setText("&Matching Interfaces:"); 
		this.viewer = new TableViewer(area, SWT.SINGLE | SWT.BORDER);
		final Control control = this.viewer.getControl();
		final GridData gd = new GridData(SWT.FILL, SWT.FILL, true, true);
		control.setLayoutData(gd);
		gd.widthHint = 400;
		gd.heightHint = 200;



		final LabelProvider filteredListLabelProvider = new LabelProvider() {
			
			public Image getImage(Object element) {
				Object adapter = factory.adapt(element, IItemLabelProvider.class);
				if(adapter != null && adapter instanceof IItemLabelProvider){
					URL url = (URL)((IItemLabelProvider)adapter).getImage(element);
					ImageDescriptor desc = ImageDescriptor.createFromURL(url);
					return desc.createImage();
					//return (Image)((IItemLabelProvider)adapter).getImage(element);
				}
				return null; //Activator.getDefault().getImage(((Locale) element).getCountry());
			}
			
			public String getText(Object element) {
				Object adapter = factory.adapt(element, IItemLabelProvider.class);
				if(adapter != null && adapter instanceof IItemLabelProvider){
					return (String)((IItemLabelProvider)adapter).getText(new InterfaceDescriptor(modelObject, element));
				}
				return null;
			}


		};
		this.viewer.setLabelProvider(filteredListLabelProvider);
		this.viewer.setContentProvider(new ArrayContentProvider());
		
		this.viewer.setInput(getInitialElementSelections());
	
		final FractalSelectionDialog.TaskFilter filter = new FractalSelectionDialog.TaskFilter();
		this.viewer.addFilter(filter);

		this.viewer.addOpenListener(new IOpenListener() {

			public void open(final OpenEvent event) {
				if (getOkButton().getEnabled()) {
					okPressed();
				}
			}

		});

		filterText.addKeyListener(new KeyAdapter() {
			
			
			public void keyPressed(final KeyEvent e) {
				if (e.keyCode == SWT.ARROW_DOWN) {
					FractalSelectionDialog.this.viewer.getControl().setFocus();
					filter.setFilterText(filterText.getText());
				}
			}

		});
		
		
		filterText.addModifyListener(new ModifyListener() {

			public void modifyText(final ModifyEvent e) {
				filter.setFilterText(filterText.getText());
				FractalSelectionDialog.this.viewer.refresh(false);
				
				final Object first = FractalSelectionDialog.this.viewer.getElementAt(0);
				
				if (first != null) {
					updateStatus(new Status(IStatus.INFO, "Fractal",getText(first) + " selected")); 
				} else {
					updateStatus(new Status(IStatus.ERROR,"Fractal","A interface must be selected"));  
				}
			}

		});
		
		
		this.viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(final SelectionChangedEvent event) {
				if (!event.getSelection().isEmpty()) {
					String text = "";
					text = getText((((IStructuredSelection) event.getSelection())).getFirstElement());
					updateStatus(new Status(IStatus.INFO, "Fractal",text + " selected")); 
				} else {
					updateStatus(new Status(IStatus.ERROR,"Fractal","A interface must be selected"));  
				}

			}
		});
		//this.viewer.setSelection(new StructuredSelection(this.viewer.getElementAt(0)));
		return area;
	}

	
	protected void computeResult() {
		List<String> result = new ArrayList<String>();
		String text = "";
		text = getText((((IStructuredSelection) viewer.getSelection()).getFirstElement()));
		result.add(text);
		setResult(result);
	}
	
	protected String getText(Object object){
		Object adapter = factory.adapt(object, IItemLabelProvider.class);
		if(adapter != null && adapter instanceof IItemLabelProvider){
			return (String)((IItemLabelProvider)adapter).getText(new InterfaceDescriptor(modelObject, object));
		}
		return null;
	}
}
