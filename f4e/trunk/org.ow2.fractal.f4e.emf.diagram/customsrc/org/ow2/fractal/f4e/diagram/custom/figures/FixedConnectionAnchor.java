package org.ow2.fractal.f4e.diagram.custom.figures;

import org.eclipse.draw2d.AbstractConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.ScalableFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PrecisionPoint;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * That class is used to hold binding figure correctly on
 * the interfaces figures
 * 
 * @author Yann Davin
 */
public class FixedConnectionAnchor extends AbstractConnectionAnchor {

	private int y = 0;
	
	public enum Position {LEFT, RIGHT}
	
	Position position= Position.RIGHT;
	
	/**
	 * @param owner
	 * @param position LEFT or RIGHT, indicate the side where the anchor will be set.
	 * @param y the y position where the anchor will be set.
	 */
	public FixedConnectionAnchor(IFigure owner, Position position, int y) {
		super(owner);
		this.position = position;
		this.y = y;
	}

	/**
	 * @see org.eclipse.draw2d.AbstractConnectionAnchor#ancestorMoved(IFigure)
	 */
	public void ancestorMoved(IFigure figure) {
		if (figure instanceof ScalableFigure)
			return;
		super.ancestorMoved(figure);
	}

	public Point getLocation(Point reference) {
		Rectangle r = getOwner().getBounds();
		int x, y;

		if(position == Position.LEFT){
			x = r.x;
		}else{
			x = r.right();
		}
		
		y = r.y + this.y;
		
		Point p = new PrecisionPoint(x, y);
		getOwner().translateToAbsolute(p);
		return p;
	}

	public Point getReferencePoint() {
		return getLocation(null);
	}

	
	public boolean equals(Object o) {
		if (o instanceof FixedConnectionAnchor) {
			FixedConnectionAnchor fa = (FixedConnectionAnchor) o;

			if (fa.position.equals(this.position) &&
					fa.y == this.y &&
					fa.getOwner() == this.getOwner()) {
				return true;
			}
		}

		return false;
	}
}