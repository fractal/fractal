/******************************************************************************
 * Copyright (c) 2006, Intalio Inc.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Intalio Inc. - initial API and implementation
 *******************************************************************************/

/**
 * Date             Author              Changes
 * Jul 12, 2006     hmalphettes         Created
 **/

package org.ow2.fractal.f4e.diagram.custom.router;

import org.eclipse.draw2d.ConnectionLayer;
import org.eclipse.draw2d.ConnectionRouter;

/**
 * Extended to be able to customize the routers. Not sure what are the
 * side-effects of not extending the GMF layer.
 * 
 * @author hmalphettes
 * @author <a href="http://www.intalio.com">&copy; Intalio, Inc.</a>
 */
public class ConnectionLayerExEx extends ConnectionLayer {// ConnectionLayerEx {

	protected ConnectionRouter scaPromoteServiceEdgesRectilinearRouter;
	protected ConnectionRouter scaPromoteReferenceEdgesRectilinearRouter;
	protected ConnectionRouter scaWireEdgesRectilinearRouter;

	public ConnectionLayerExEx() {
	}

	/**
	 * Provides an access point to the rectilinear router for sca promote
	 * (service) edges for the entire layer. Each connection will contain a
	 * reference to this router so that the router can keep track of overlapping
	 * connections and reroute accordingly.
	 * 
	 * @return the <code>ConnectionRouter</code> that handles sca promote
	 *         (service) edges rectilinear style routing.
	 */
	public ConnectionRouter getScaPromoteServiceEdgeRectilinearRouter() {
		if (scaPromoteServiceEdgesRectilinearRouter == null) {
			scaPromoteServiceEdgesRectilinearRouter = new RectilinearRouterEx();
			((RectilinearRouterEx) scaPromoteServiceEdgesRectilinearRouter).normalizeBehavior = RectilinearRouterEx.NORMALIZE_ON_HORIZONTAL_CENTER;
		}
		return scaPromoteServiceEdgesRectilinearRouter;
	}

	/**
	 * Provides an access point to the rectilinear router for sca promote
	 * (reference) edges for the entire layer. Each connection will contain a
	 * reference to this router so that the router can keep track of overlapping
	 * connections and reroute accordingly.
	 * 
	 * @return the <code>ConnectionRouter</code> that handles sca promote
	 *         (reference) edges rectilinear style routing.
	 */
	public ConnectionRouter getScaPromoteReferenceEdgeRectilinearRouter() {
		if (scaPromoteReferenceEdgesRectilinearRouter == null) {
			scaPromoteReferenceEdgesRectilinearRouter = new RectilinearRouterEx();
			((RectilinearRouterEx) scaPromoteReferenceEdgesRectilinearRouter).normalizeBehavior = RectilinearRouterEx.NORMALIZE_ON_HORIZONTAL_CENTER_REVERSE;
		}
		return scaPromoteReferenceEdgesRectilinearRouter;
	}

	/**
	 * Provides an access point to the rectilinear router for sca wire edges for
	 * the entire layer. Each connection will contain a reference to this router
	 * so that the router can keep track of overlapping connections and reroute
	 * accordingly.
	 * 
	 * @return the <code>ConnectionRouter</code> that handles sca wiring edges
	 *         rectilinear style routing.
	 */
	public ConnectionRouter getScaWireEdgeRectilinearRouter() {
		if (scaWireEdgesRectilinearRouter == null) {
			scaWireEdgesRectilinearRouter = new RectilinearRouterEx();
			((RectilinearRouterEx) scaWireEdgesRectilinearRouter).normalizeBehavior = RectilinearRouterEx.NORMALIZE_ON_HORIZONTAL_CENTER;
		}
		return scaWireEdgesRectilinearRouter;
	}

}
