package org.ow2.fractal.f4e.diagram.custom.ui.handles;

import org.eclipse.emf.ecore.EAttribute;


public class ExtendsCollapseTracker extends CompartmentCollapseTracker{
	
	public ExtendsCollapseTracker(ICompartmentCollapseHandle handle){
		super(handle);
	}
	
	protected EAttribute getAttribute(){
		return org.ow2.fractal.f4e.notation.NotationPackage.eINSTANCE.getAbstractComponentStyle_CollapsedExtends();
	}
	
	protected String getCommandName() {
		return "Collapse the extends compartment"; //$NON-NLS-1$
	}

	protected String getDebugName() {
		return "Collapse the extends compartment tool"; //$NON-NLS-1$
	}
}
