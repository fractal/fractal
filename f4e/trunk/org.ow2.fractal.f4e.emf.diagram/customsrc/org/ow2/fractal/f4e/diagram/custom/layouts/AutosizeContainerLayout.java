package org.ow2.fractal.f4e.diagram.custom.layouts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.mapmode.IMapMode;

/**
 * Layout that prevent a container to be extended when
 * the container figure is greater than him :
 * 
 * Ex : 
 * Previously :
 * +-----------+
 * |+---------+|
 * ||         ||
 * ||container||
 * ||         ||
 * |+---------+|
 * +-----------+
 * 
 * With the AutosizeContainerLayout
 * +-----------+
 * |+---------+|
 * ||container||
 * |+---------+|
 * |           |
 * |           |
 * +-----------+
 * 
 * @author Yann Davin
 *
 */
public class AutosizeContainerLayout extends ConstrainedToolbarLayout{
	
	public AutosizeContainerLayout(){
		super();
	}
	
	protected Dimension calculatePreferredSize(IFigure container, int wHint, int hHint) {
		Dimension r = super.calculatePreferredSize(container,wHint,hHint);
		
		container.setMaximumSize(new Dimension(IFractalLayout.MAX_LABEL_LENGTH,r.height));
		return r;
	}

	public Dimension calculateMinimumSize(IFigure container, int wHint,
			int hHint) {
		Dimension r = super.calculatePreferredSize(container,wHint,hHint);
		return r;
	}
	
}