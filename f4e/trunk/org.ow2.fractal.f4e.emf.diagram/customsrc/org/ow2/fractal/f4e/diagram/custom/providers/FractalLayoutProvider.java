package org.ow2.fractal.f4e.diagram.custom.providers;

import org.eclipse.draw2d.graph.DirectedGraphLayout;
import org.eclipse.gmf.runtime.diagram.ui.providers.TopDownProvider;

public class FractalLayoutProvider extends TopDownProvider{
	
	protected boolean supportsBorderNodes() {
    	return true;
    }
	
	public FractalLayoutProvider(){
		super();
	}

	protected DirectedGraphLayout createGraphLayout() {
		return new FractalDirectedGraphLayout();
	}

}
