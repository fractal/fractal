package org.ow2.fractal.f4e.diagram.custom.providers;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.ui.provider.PropertyDescriptor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Composite;
import org.ow2.fractal.f4e.diagram.custom.ui.FractalJavaClassCellEditor;

/** 
 * Modified property descriptor used for the Fractal 'content' property.
 * So when the 'content' property is displayed in the property page
 * we use a custom cell to display the 'content' value.
 * That custom cell display a little button that allows to open the completion dialog box.
 *
 * @author Yann Davin
 */
public class FractalJavaClassPropertyDescriptor  extends PropertyDescriptor{
	
	public FractalJavaClassPropertyDescriptor(Object object, IItemPropertyDescriptor itemPropertyDescriptor)
	  {
	    super(object,itemPropertyDescriptor);
	 
	  }
	
	public CellEditor createPropertyEditor(Composite composite) {
		 if (!itemPropertyDescriptor.canSetProperty(object))
		    {
		      return null;
		    }
		 return new FractalJavaClassCellEditor(composite, (EObject)object,  (EStructuralFeature)itemPropertyDescriptor.getFeature(object));
	}
}
