package org.ow2.fractal.f4e.diagram.custom.ui.providers;

import org.eclipse.gmf.runtime.common.ui.util.IWorkbenchPartDescriptor;
import org.eclipse.gmf.runtime.diagram.ui.providers.DiagramContributionItemProvider;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.ui.IWorkbenchPage;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.FractalActionIds;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.FractalMenuManager;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.HideAttributesControllerCompartmentsAction;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.HideContentCompartmentsAction;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.HideInterfaceLabelsAction;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.HideOthersCompartmentsAction;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.HideSubComponentsCompartmentsAction;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.ShowAttributesControllerCompartmentsAction;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.ShowContentCompartmentsAction;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.ShowInterfaceLabelsAction;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.ShowOthersCompartmentsAction;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.ShowSubComponentsCompartmentsAction;

/**
 * Provides new actions in the toolbar.
 * This class is used by the contributionItemProvider extension point in the plugin.xml.
 * 
 * @author Yann Davin
 *
 */
public class FractalDiagramContributionItemProvider extends DiagramContributionItemProvider{

	protected IAction createAction(String actionId,IWorkbenchPartDescriptor partDescriptor) {
		IWorkbenchPage workbenchPage = partDescriptor.getPartPage();
		if (actionId.equals(FractalActionIds.ACTION_HIDE_INTERFACE_LABELS))
			return new HideInterfaceLabelsAction(workbenchPage);
		else if(actionId.equals(FractalActionIds.ACTION_SHOW_INTERFACE_LABELS)){
			return new ShowInterfaceLabelsAction(workbenchPage);
		}else if(actionId.equals(FractalActionIds.ACTION_HIDE_SUBCOMPONENTS_COMPARTMENTS)){
			return new HideSubComponentsCompartmentsAction(workbenchPage);
		}else if(actionId.equals(FractalActionIds.ACTION_SHOW_SUBCOMPONENTS_COMPARTMENTS)){
			return new ShowSubComponentsCompartmentsAction(workbenchPage);
		}else if(actionId.equals(FractalActionIds.ACTION_HIDE_ATTRIBUTES_CONTROLLER_COMPARTMENTS)){
			return new HideAttributesControllerCompartmentsAction(workbenchPage);
		}else if(actionId.equals(FractalActionIds.ACTION_SHOW_ATTRIBUTES_CONTROLLER_COMPARTMENTS)){
			return new ShowAttributesControllerCompartmentsAction(workbenchPage);
		}else if(actionId.equals(FractalActionIds.ACTION_HIDE_CONTENT_COMPARTMENTS)){
			return new HideContentCompartmentsAction(workbenchPage);
		}else if(actionId.equals(FractalActionIds.ACTION_SHOW_CONTENT_COMPARTMENTS)){
			return new ShowContentCompartmentsAction(workbenchPage);
		}else if(actionId.equals(FractalActionIds.ACTION_HIDE_OTHERS_COMPARTMENTS)){
			return new HideOthersCompartmentsAction(workbenchPage);
		}else if(actionId.equals(FractalActionIds.ACTION_SHOW_OTHERS_COMPARTMENTS)){
			return new ShowOthersCompartmentsAction(workbenchPage);
		}else{
			return super.createAction(actionId, partDescriptor);
		}
	}

	protected IMenuManager createMenuManager(String menuId, IWorkbenchPartDescriptor partDescriptor) {
		if (menuId.equals(FractalActionIds.MENU_FRACTAL)){
			return new FractalMenuManager();
		}

		return super.createMenuManager(menuId, partDescriptor);

	}
}
