package org.ow2.fractal.f4e.diagram.custom.ui.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gmf.runtime.diagram.ui.actions.DiagramAction;
import org.eclipse.ui.IWorkbenchPage;
import org.ow2.fractal.f4e.diagram.custom.edit.parts.InterfaceNameEditPart;
import org.ow2.fractal.f4e.diagram.custom.ui.actions.l10n.FractalDiagramUIActionsPluginImages;
import org.ow2.fractal.f4e.diagram.custom.ui.requests.ToggleInterfaceLabelsRequest;

public class HideInterfaceLabelsAction extends DiagramAction {

	/**
	 * @param workbenchPage
	 */
	public HideInterfaceLabelsAction(IWorkbenchPage workbenchPage) {
		super(workbenchPage);
	}
	
	/**
	 * Add text and image descriptors.
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#init()
	 */
	public void init() {
		super.init();
		setText(FractalDiagramUIActionsMessages.HideInterfaceLabelsAction_label);
		setId(FractalActionIds.ACTION_HIDE_INTERFACE_LABELS);
		setToolTipText(FractalDiagramUIActionsMessages.HideInterfaceLabelsAction_toolTip);
		setImageDescriptor(FractalDiagramUIActionsPluginImages.DESC_HIDE_INTERFACE_LABELS);
		setDisabledImageDescriptor(FractalDiagramUIActionsPluginImages.DESC_HIDE_INTERFACE_LABELS_DISABLED);
	}	

	/**
	 * Returns an instance of <code>ToggleConnectionLabelsRequest</code>
	 * 
	 * @return the request
	 * @see org.eclipse.gmf.runtime.diagram.ui.actions.DiagramAction#createTargetRequest()
	 */
	protected Request createTargetRequest() {
		return new ToggleInterfaceLabelsRequest(false);
	}

	/**
	 * Registers this as a selection listener
	 * @return true
	 * @see org.eclipse.gmf.runtime.diagram.ui.actions.DiagramAction#isSelectionListener()
	 */
	protected boolean isSelectionListener() {
		return true;
	}
	
	private List<InterfaceNameEditPart> getAllInterfaceNameEditPart(EditPart root){
		List<InterfaceNameEditPart> result= new ArrayList<InterfaceNameEditPart>();
		Iterator<EditPart> iterator = root.getChildren().iterator();
		while(iterator.hasNext()){
			EditPart child = iterator.next();
			if(child instanceof InterfaceNameEditPart){
				result.add((InterfaceNameEditPart)child);
			}else{
				result.addAll(getAllInterfaceNameEditPart(child));
			}
		}
		return result;
	}
	
	/** 
	 * Filters the selected objects and returns only InterfaceEditParts that understand
	 * the property change request to hide labels.
	 * @return the operation set 
	 */
	protected List createOperationSet() {
		List selection = getSelectedObjects();
		if (selection.isEmpty() || !(selection.get(0) instanceof EditPart)) {
			return Collections.EMPTY_LIST;
		} 		
		
		List<InterfaceNameEditPart> interfaceLabels = getAllInterfaceNameEditPart((EditPart)selection.get(0));
		
		Iterator<InterfaceNameEditPart> selectedEPs = interfaceLabels.iterator();
		
		List targetedEPs = new ArrayList();
		while (selectedEPs.hasNext()) {
		    EditPart selectedEP = (EditPart)selectedEPs.next();
	    	targetedEPs.addAll(getTargetEditParts(selectedEP));
		}
		return targetedEPs.isEmpty() ? Collections.EMPTY_LIST : targetedEPs;
	}
	
}
