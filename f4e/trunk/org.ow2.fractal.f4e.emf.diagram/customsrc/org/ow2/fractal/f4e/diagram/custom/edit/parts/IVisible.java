package org.ow2.fractal.f4e.diagram.custom.edit.parts;

/**
 * Interface to be able to set visible or not a IGraphicalEditPart.
 * Has been created in order to mask the containers of the Definition and Component figure.
 * 
 * @author Yann Davin
 */

public interface IVisible {
	public void setVisibility(boolean vis);
	
	public void setCollapsed(boolean collapsed);

}
