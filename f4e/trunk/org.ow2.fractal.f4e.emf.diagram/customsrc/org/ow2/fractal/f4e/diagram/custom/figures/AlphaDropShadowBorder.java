package org.ow2.fractal.f4e.diagram.custom.figures;

import org.eclipse.draw2d.AbstractBackground;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.diagram.ui.figures.DiagramColorConstants;
import org.eclipse.gmf.runtime.draw2d.ui.figures.DropShadowBorder;
import org.eclipse.gmf.runtime.draw2d.ui.figures.IPolygonAnchorableFigure;
import org.eclipse.gmf.runtime.draw2d.ui.mapmode.MapModeUtil;
import org.eclipse.swt.graphics.Color;

/**
 * Class used to add shadow border on components.
 *  
 * @author Yann Davin copy from the work of :
 * @author <a href="mailto:simon.bernard@anyware-tech.com">Simon Bernard</a>
 */
public class AlphaDropShadowBorder extends AbstractBackground implements DropShadowBorder {

	private static final int DEFAULT_SHIFT_VALUE = 1;

	private static final Color SHADOW_COLOR = DiagramColorConstants.diagramDarkGray;

	private static final int DEFAULT_TRANSPARENCY = 65;

	private boolean shouldDrawShadow = true;

	private int shift = DEFAULT_SHIFT_VALUE;

	public void setShouldDrawDropShadow(boolean drawDropShadow) {
		shouldDrawShadow = drawDropShadow;
	}

	public boolean shouldDrawDropShadow() {
		return shouldDrawShadow;
	}

	/**
	 * Method for determining the inset the border will take up on the shape.
	 * 
	 * @param figure
	 *            Figure that will be inset from the border
	 * @return Insets the Insets for the border on the given figure.
	 */
	public Insets getInsets(IFigure figure) {
		Insets insetsNew = new Insets();
		insetsNew.top = 0;
		insetsNew.left = 0;
		insetsNew.bottom = MapModeUtil.getMapMode(figure).DPtoLP(shift * 2);
		insetsNew.right = MapModeUtil.getMapMode(figure).DPtoLP(shift * 2);
		return insetsNew;
	}

	public Insets getTransparentInsets(IFigure figure) {
		Insets insetsNew = new Insets();
		insetsNew.top = 0;
		insetsNew.left = 0;
		insetsNew.bottom = MapModeUtil.getMapMode(figure).DPtoLP(shift * 2);
		insetsNew.right = MapModeUtil.getMapMode(figure).DPtoLP(shift * 2);
		return insetsNew;
	}

	public void paintBackground(IFigure figure, Graphics graphics, Insets insets) {
		if (shouldDrawDropShadow()) {
			graphics.pushState();
			graphics.setBackgroundColor(SHADOW_COLOR);
			graphics.setAlpha(DEFAULT_TRANSPARENCY);

			if (figure instanceof IPolygonAnchorableFigure) {
				PointList polygonPoints = ((IPolygonAnchorableFigure) figure).getPolygonPoints();
				polygonPoints.translate(shift, shift);
				graphics.fillPolygon(polygonPoints);
				polygonPoints.translate(shift, shift);
				graphics.fillPolygon(polygonPoints);
			} else {
				Rectangle bounds = figure.getBounds().getCopy();
				bounds.translate(shift, shift);
				graphics.fillRoundRectangle(bounds, 0, 0);
				bounds.translate(shift, shift);
				graphics.fillRoundRectangle(bounds, 0, 0);
			}

			graphics.popState();
		}
	}
}
