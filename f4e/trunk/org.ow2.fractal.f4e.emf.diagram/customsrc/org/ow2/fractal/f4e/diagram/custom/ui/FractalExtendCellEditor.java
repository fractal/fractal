/**
 * <copyright>
 *
 * Copyright (c) 2002-2007 IBM Corporation and others.
 * All rights reserved.   This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   IBM - Initial API and implementation
 *
 * </copyright>
 *
 * $Id: FeatureEditorDialog.java,v 1.11 2007/03/23 17:36:45 marcelop Exp $
 */
package org.ow2.fractal.f4e.diagram.custom.ui;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;


public class FractalExtendCellEditor extends FractalPropertyValueCellEditor{
	
	public FractalExtendCellEditor(Composite parent, EObject modelObject, EStructuralFeature feature){
		super(parent,modelObject,feature);
	}
	
	protected Object openDialogBox(Control cellEditorWindow) {
		// final ModelObject returnValue = ((ModelObject) getValue());
		// opening the dialog
		FractalExtendSelectionDialog dialog = new FractalExtendSelectionDialog(
				cellEditorWindow.getShell(), modelObject, feature);
		if (dialog.open() == IDialogConstants.OK_ID) {
			
			String value =  ((String)dialog.getResult());
			return value;
		}
		
		return "";
	}

	public void dispose() {
		super.dispose();
	}
}
