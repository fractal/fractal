package org.ow2.fractal.f4e.diagram.custom.providers;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.ui.provider.PropertySource;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.ow2.fractal.f4e.fractal.FractalPackage;

public class FractalPropertySource extends PropertySource{
	 public FractalPropertySource(Object object, IItemPropertySource itemPropertySource)
	 {
	   super(object,itemPropertySource);
	 }
	 
	 /**
	  * Checks the type of attribute and create the appropriate descriptor
	  */
	protected IPropertyDescriptor createPropertyDescriptor(
			IItemPropertyDescriptor itemPropertyDescriptor) {
		// TODO Auto-generated method stub
		Object feature = itemPropertyDescriptor.getFeature(object);
		
		if (feature instanceof EAttribute && 
				(feature == FractalPackage.eINSTANCE.getBinding_Client() ||
						feature == FractalPackage.eINSTANCE.getBinding_Server()))
		{
			return new FractalPropertyDescriptor((EObject)object, itemPropertyDescriptor);
		}else if(feature == FractalPackage.Literals.CONTENT__CLASS){
			return new FractalJavaClassPropertyDescriptor((EObject)object, itemPropertyDescriptor);
		}else if(feature == FractalPackage.Literals.DEFINITION__EXTENDS || 
				feature == FractalPackage.Literals.COMPONENT__DEFINITION){
			return new FractalExtendPropertyDescriptor(object, itemPropertyDescriptor);
		}else if(feature instanceof EAttribute && ((EAttribute)feature).getName().equals("signature")){
			if(object instanceof EObject){
				return new FractalJavaSignaturePropertyDescriptor((EObject)object, itemPropertyDescriptor);
			}else{
				return super.createPropertyDescriptor(itemPropertyDescriptor);
			}
		}else if(object instanceof EObject && ((EObject)object).eClass().getEPackage() == FractalPackage.eINSTANCE){
			return new MergedPropertyDescriptor(object, itemPropertyDescriptor);
		}else{
			return super.createPropertyDescriptor(itemPropertyDescriptor);
		}
	}
}
