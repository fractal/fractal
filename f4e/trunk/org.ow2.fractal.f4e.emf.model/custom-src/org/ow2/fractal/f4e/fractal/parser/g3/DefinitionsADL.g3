grammar DefinitionsADL;
options {
  output=AST;
  ASTLabelType=CommonTree;
}
@header {
  package org.ow2.fractal.f4e.fractal.parser;
  
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.Constant;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.EffectiveParameter;
import org.ow2.fractal.f4e.fractal.FormalParameter;
import org.ow2.fractal.f4e.fractal.FractalFactory;
import org.ow2.fractal.f4e.fractal.Reference;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.ValueElement;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IAbstractComponentHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IDefinitionHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IValueHelper;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.plugin.FractalPlugin;
import org.ow2.fractal.f4e.fractal.util.IFractalResource;
import org.ow2.fractal.f4e.fractal.util.SDefinitionLoadLocator;

}
@lexer::header {
  package org.ow2.fractal.f4e.fractal.parser;
}

@members {
	Stack<Object> stack = new Stack<Object>();
	Component component = null;
	
	public DefinitionsADLParser(TokenStream input, Component component){
		super(input);
		this.component = component;
	}
	
	public void pushReference(String name){
		Reference reference = FractalFactory.eINSTANCE.createReference();
		Definition definition = component.getHelper().getParentDefinition();
    		reference.setName(name);
    		if(definition != null){
    			FormalParameter formalParameter = definition.getHelper().getParameter(name);
    			reference.setReference(formalParameter);
    		}
		stack.push(reference);
	}
	
	public void pushConstant(String name){
		
		Constant constant = FractalFactory.eINSTANCE.createConstant();
		constant.setConstant(name);
		stack.push(constant);
	}
	
	public void pushParameter(){
		
		EffectiveParameter parameter = FractalFactory.eINSTANCE.createEffectiveParameter();		
		
		if(!stack.empty()){
			if(stack.lastElement() instanceof Value){
				parameter.setValue((Value)stack.pop());
				parameter.setName(null);
				
				if(!stack.empty()){
					if(stack.lastElement() instanceof Value){
						parameter.setName((Value)stack.pop());
					}
				}
			}
		}
		
		stack.push(parameter);
	}
	
	public void pushParameterWithoutName(){
		EffectiveParameter parameter = FractalFactory.eINSTANCE.createEffectiveParameter();		
		
		if(!stack.empty()){
			if(stack.lastElement() instanceof Value){
				parameter.setValue((Value)stack.pop());
				parameter.setName(null);
			}
		}
		
		stack.push(parameter);
	}
	
	public void pushValue(){
		
		Value value = FractalFactory.eINSTANCE.createValue();
		
		IAbstractComponentHelper componentHelper = (IAbstractComponentHelper)HelperAdapterFactory.getInstance().adapt(component);
		if(componentHelper != null){
			value.setContextDefinition(componentHelper.getParentDefinition());
		}
		
		popValueElements(value);
		
		stack.push(value);	
	}	
	
	private void popValueElements(Value value){
		if(!stack.empty() && (stack.lastElement() instanceof ValueElement)){
			ValueElement elem = (ValueElement)stack.pop();
			popValueElements(value);
			value.getElements().add(elem);		
		}
	}
	
	
	public void pushDefinitionCall(){
		
		DefinitionCall definitionCall = FractalFactory.eINSTANCE.createDefinitionCall();
		
		popParameters(definitionCall);
			
		if(!stack.empty()){
			if(stack.lastElement() instanceof Value){
				Value definitionName = (Value)stack.pop();
				definitionCall.setDefinitionName(definitionName);
				
				IValueHelper helper = (IValueHelper)HelperAdapterFactory.getInstance().adapt(definitionName);
				if(helper != null){
						String definition = helper.toString();
    					
    					IFractalResource fractalResource = this.component.eResource()!= null && 
						this.component.eResource() instanceof IFractalResource?
								(IFractalResource)this.component.eResource():
								null;
								if(fractalResource!=null && fractalResource.shouldLoadExtends()){	
							
    					 	ResourceSet resourceSet = this.component.eResource()!=null?this.component.eResource().getResourceSet():null;
        					 	if(resourceSet != null){
        					 		URI parentDefinitionURI = SDefinitionLoadLocator.getInstance().getLoadLocation(this.component.eResource().getURI(),definition);
        		    				if(SDefinitionLoadLocator.getInstance().getHelper(this.component.eResource().getURI()) != null ){
        		    					SDefinitionLoadLocator.getInstance().addHelper(parentDefinitionURI, SDefinitionLoadLocator.getInstance().getHelper(this.component.eResource().getURI()));
        		    				}
        		    				
        		    		Resource resource = null;
        		    		//URI fileToLoadURI = SDefinitionLoadLocator.getInstance().getLoadLocation(parentDefinitionURI,definition);
        		    		if(parentDefinitionURI != null){
        		    			resource = resourceSet.getResource(parentDefinitionURI,true);
        		    		}else{
        		    			FractalPlugin.getDefault().getLog().log(new Status(IStatus.WARNING,FractalPlugin.PLUGIN_ID,"The Fractal ADL definition : " +  definition + " can't be found"));
        		    		}
    					 	
    						if(resource == null){
    							definitionCall.setDefinition(null);
    						}else{
    							Definition eDefinition = ((IFractalResource)resource).getRootDefinition();
            				
    							((IFractalResource)resource).addDependency(component.eResource());
    							//the getResource checks that our resource starts with a definition
    							definitionCall.setDefinition(eDefinition);
    						}
    					 	}
						}
				}
			}
		}
		
		stack.push(definitionCall);
	}	
	
	private void popParameters(DefinitionCall definitionCall){
		if(!stack.empty() && (stack.lastElement() instanceof EffectiveParameter)){
			EffectiveParameter param = (EffectiveParameter)stack.pop();
			popParameters(definitionCall);
			definitionCall.getParameters().add(param);
		}
	}
	
	private void popDefinitionCalls(){
		if(component != null){
			popDefinitionCalls(component);
		}
	}
	
	private void popDefinitionCalls(Component component){
		if(!stack.empty() && (stack.lastElement() instanceof DefinitionCall)){
			DefinitionCall def = (DefinitionCall)stack.pop();
			popDefinitionCalls(component);
			component.getExtendsAST().add(def);
		}
	}
	
	private boolean shared = false;
	
	private void setShared(){
		IAbstractComponentHelper componentHelper = (IAbstractComponentHelper)HelperAdapterFactory.getInstance().adapt(component);
		
		if(componentHelper != null){
			Definition definition = componentHelper.getParentDefinition();
		
			if(definition != null){
    				IDefinitionHelper definitionHelper = definition.getHelper();
    				if(definitionHelper != null){
    					Component sharedComponent = definitionHelper.getComponent(component.getDefinition(),true);
    					component.setShared(sharedComponent);
    				}
    			}
		}	
	}
}

prog
    : (definitionsADL {popDefinitionCalls();} )? EOF
    ;
	
definitionsADL
	: shared {setShared();} | definitionADL {pushDefinitionCall();}( ',' definitionADL{pushDefinitionCall();})* 
	;

shared 
	: (REFERENCE|NAME) ('/' (REFERENCE|NAME))+
	;

definitionADL
	: definitionName {pushValue();} ('(' effectiveParameters ')' )? 
	;

definitionName
	: (name=NAME {pushConstant($name.text);})? (  ref=REFERENCE {pushReference($ref.text);}  (name=NAME {pushConstant($name.text);})?)*
	;
	
effectiveParameters
	:  valuesList | parametersList
	;

valuesList 
	: parameterValue{pushValue();pushParameterWithoutName();} ( ',' parameterValue {pushValue();pushParameterWithoutName();})*
	;
	
parameterValue
	: (name=NAME {pushConstant($name.text);} | val=VALUE {pushConstant($val.text);})? (  ref=REFERENCE {pushReference($ref.text);}  (name=NAME {pushConstant($name.text);} | val=VALUE {pushConstant($val.text);})?)*
	;
	
parametersList
	:  parameter{pushParameter();} ( ',' parameter{pushParameter();} )*
	;

parameter
	: parameterName {pushValue();} '=>' parameterValue {pushValue();}
	;

parameterName
	: (name=NAME {pushConstant($name.text);})? ( ref=REFERENCE {pushReference($ref.text);}  (name=NAME {pushConstant($name.text);})?)*
	;
			
REFERENCE 
	: '${' ( options {greedy=false;} : 'a'..'z' | 'A'..'Z' | '0'..'9' | '-' | '_' )+ '}' {setText(getText().substring(2, getText().length()-1));} 
	;

VALUE 
	: '\'' ( options {greedy=false;} : . )* '\''
	;

NAME 
	: ('a'..'z' | 'A'..'Z' | '0'..'9' | '-' | '_' | '.' | ' '  )+
	;

NEWLINE : '\r'? '\n' ;

WS : (' ' | '\t' | '\n' | '\r')+ {skip();} ;