/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.ow2.fractal.f4e.fractal.Controller;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IControllerHelper;


public class ControllerAdapter extends EContentAdapter {
   
	private ControllerAdapter(){
		super();
	}
	
    /* (non-Javadoc)
     * @see org.eclipse.emf.ecore.util.EContentAdapter#notifyChanged(org.eclipse.emf.common.notify.Notification)
     */
    public void notifyChanged(Notification notification) {
    	super.notifyChanged(notification);
    	if(!(notification.getNotifier() instanceof Controller)){
    		return;
    	}
    	
    	int type = notification.getEventType();
    	int featureID = notification.getFeatureID(Controller.class);
    
    	IControllerHelper membraneHelper = ((Controller)notification.getNotifier()).getHelper();
    	
    	switch(featureID){
    		case FractalPackage.CONTROLLER__TYPE:
    			switch(type){
					case Notification.SET:
						if(membraneHelper != null){
							membraneHelper.update(FractalPackage.CONTROLLER__TYPE_AST, null);
						}
					break;
    			}
    		break;
    	}
    	
    }
    
    private static class SingletonHolder {
		private static ControllerAdapter instance = new ControllerAdapter();
	}
    
    public static ControllerAdapter getInstance(){
    	return SingletonHolder.instance;
    }
    
}
