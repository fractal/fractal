package org.ow2.fractal.f4e.fractal.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.util.ExtendedMetaData;
import org.ow2.fractal.f4e.fractal.Definition;


// TODO: Auto-generated Javadoc
/**
 * The Interface FractalResourceSet.
 */
public interface FractalResourceSet {
	
	/**
	 * Gets the resource, and add it to the specified resource set.
	 * This method has been created to solve referenced fractal files.
	 * 
	 * Add a dependency relation between the fractal file
	 * that will be created and the given resource.
	 * resource depends on the future loaded resource.
	 * 
	 * @param uri the uri of the fractal file
	 * @param resource the resource on which the resource will be added
	 * 
	 * @return the resource
	 */
	
	public Definition getDefinition(URI uri);
	
	public ExtendedMetaData getFractalMetaData();
}
