/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter.helper;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.transaction.RecordingCommand;
import org.ow2.fractal.f4e.fractal.Content;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;

// TODO: Auto-generated Javadoc
/**
 * The Class ContentHelperAdapter.
 */
public class ContentHelperAdapter extends HelperAdapter<Content>
	implements IContentHelper {
	
	/**
	 * Instantiates a new content helper adapter.
	 * 
	 * @param content the content
	 */
	public ContentHelperAdapter(Content content) {
		super(content);
	}

	public boolean isAdapterForType(Object type) {
		return  super.isAdapterForType(type) || type == IContentHelper.class;
	}
	
	protected void updateClass(){
		t.setClassAST(ValueHelperAdapter.getValue(t.getClass_(),t.getAbstractComponent()));	
	}
	
	public void update(final int featureID, IProgressMonitor monitor){
		Command cmd = new RecordingCommand(FractalTransactionalEditingDomain
				.getEditingDomain()) {
			protected void doExecute() {
				switch (featureID) {
					case FractalPackage.CONTENT__CLASS_AST:
							updateClass();
						break;
					default:
						break;
				}
			}
		};

		FractalTransactionalEditingDomain.getEditingDomain().getCommandStack()
				.execute(cmd);
	}

	public int[] getAttributeIDs() {
		return new int[]{FractalPackage.CONTENT__CLASS};
	}
	
	
}
