/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */

package org.ow2.fractal.f4e.fractal.adapter.helper;

import java.util.ArrayList;
import java.util.List;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.TokenRewriteStream;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.notification.IFractalNotification;
import org.ow2.fractal.f4e.fractal.parser.DefinitionsADLLexer;
import org.ow2.fractal.f4e.fractal.parser.DefinitionsADLParser;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;


public class ComponentHelperAdapter extends AbstractComponentHelperAdapter<Component> implements IComponentHelper{
	
	// Use to indicate that merged component is related to a shared component.
	boolean isShared;
	
	public ComponentHelperAdapter(Component component){
		super(component);
	}
	
	public boolean isShared(){
		return isShared;
	}
	
	public void setShared(boolean shared){
		if(shared != this.isShared){
			boolean oldValue = this.isShared;
			this.isShared = shared;
			t.eNotify(new ENotificationImpl((InternalEObject)t, IFractalNotification.SET_IS_SHARED_COMPONENT, null, oldValue, isShared, false));
		}
	}
	
	public boolean isAdapterForType(Object type) {
		return  super.isAdapterForType(type) || type == IComponentHelper.class;
	}

	public void notifyChanged(Notification notification) {
		// TODO Auto-generated method stub
		super.notifyChanged(notification);
		if(!(notification.getNotifier() instanceof Component)){
			return;
		}
		
		if(notification.getNotifier() == t){
			int type = notification.getEventType();
			int featureID = notification.getFeatureID(AbstractComponent.class);
    	
			if(notification.getNotifier() == t){
				switch(featureID){
				case FractalPackage.COMPONENT__PARENT:
					// When a component is loaded, the parent is not yet known
					// when the parent become available, we have to update the AST of the name
					// which can contained reference to formal arguments of the parent. 
					updateName();
					update(FractalPackage.ABSTRACT_COMPONENT__NAME_AST, null);
					break;
				case FractalPackage.COMPONENT__DEFINITION:{
					switch(type){
					case Notification.SET:
						updateDefinition();
						//updateMerge();
						//setShouldUpdateExtends(true);
						break;
					}
					break;
				}
				}
			}
		}
	}

	private static final DefinitionsADLLexer lexer = new DefinitionsADLLexer();
	private static final TokenRewriteStream tokens = new TokenRewriteStream(lexer);
	private final DefinitionsADLParser grammar = new DefinitionsADLParser(tokens,t);
	
	protected void updateDefinition(){
		//t.getExtendsAST().clear();
		
		if(t.getDefinition() == null){
			return;
		}
		
		if(t.getDefinition().equals("")){
			t.getExtendsAST().clear();
			t.setDefinition(null);
			t.setShared(null);
			return;
		}
		
		
		final ArrayList<DefinitionCall> toRemove = new ArrayList<DefinitionCall>();
		toRemove.addAll(t.getExtendsAST());
		CharStream s = new ANTLRStringStream(t.getDefinition());
		try{
		//DefinitionsADLLexer lexer = new DefinitionsADLLexer(s);
		lexer.setCharStream(s);
		
		final TokenRewriteStream tokens = new TokenRewriteStream(lexer);
		//tokens.setTokenSource(lexer);
		// we pass the current component 't' to the parser
		// we create the emf objects related to the current adl definition,
		// directly in the parser.
		TransactionalEditingDomain domain = FractalTransactionalEditingDomain
		.getEditingDomain();

		domain.getCommandStack().execute(new RecordingCommand(domain) {
			protected void doExecute() {
				try{
				//DefinitionsADLParser grammar = new DefinitionsADLParser(tokens,t);
				grammar.setTokenStream(tokens);
				grammar.prog();
				t.getExtendsAST().removeAll(toRemove);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	public String getAbsoluteName(String separator){
		String prefix = "";
		if(t.getParent()!=null){
			if(t.getParent() instanceof Component){
				IComponentHelper helper = (ComponentHelperAdapter)HelperAdapterFactory.getInstance().adapt(t.getParent());
				if(helper != null){
					prefix += helper.getAbsoluteName(separator) + separator;
				}
			}
		}
		return prefix + t.getName();
	}
	
	public void update(final int featureID, IProgressMonitor monitor) {
		Command cmd = new RecordingCommand(FractalTransactionalEditingDomain
				.getEditingDomain()) {
			protected void doExecute() {
				switch (featureID) {
				case FractalPackage.COMPONENT__NAME_AST:
					updateName();
					break;
				case FractalPackage.COMPONENT__EXTENDS_AST:
					updateDefinition();
					break;
				default:
					break;
				}
			}
		};

		switch (featureID) {
		case FractalPackage.COMPONENT__EXTENDS_AST:
			FractalTransactionalEditingDomain.getEditingDomain()
					.getCommandStack().execute(cmd);
			break;
		default:
			super.update(featureID,monitor);
			break;
		}
	}
	
	public void merge(List<Node<? extends EObject>> srcNodes, boolean override, IProgressMonitor monitor){
			super.merge(srcNodes, override, monitor);
	}
}

