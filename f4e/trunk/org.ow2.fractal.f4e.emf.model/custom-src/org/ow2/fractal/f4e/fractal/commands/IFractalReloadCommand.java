package org.ow2.fractal.f4e.fractal.commands;

public interface IFractalReloadCommand extends IFractalCommand{
	public void reload();
}
