package org.ow2.fractal.f4e.extension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectEList;
import org.eclipse.emf.ecore.util.EcoreEList;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap.Entry;
import org.eclipse.emf.ecore.xml.type.AnyType;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.plugin.FractalPlugin;
import org.ow2.fractal.f4e.fractal.util.FractalResourceFactoryImpl;

public class FractalADLExtension {
	private static final String FRACTAL_ADL_EXENSION_POINT_ID="org.ow2.fractal.f4e.emf.fractalADLExtension";
	
	public static List<EFactory> getRegisterExtensions(){
		List<EFactory> result = new ArrayList<EFactory>();
		try{
			IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(FRACTAL_ADL_EXENSION_POINT_ID);
			for(IConfigurationElement e: config){
				Object o = e.getAttribute("uri");
				if(o instanceof String){
					EFactory factory = EPackage.Registry.INSTANCE.getEFactory((String)o);
					if(factory != null){
						result.add(factory);
					}
				}
			}
		}catch(Exception e){
			FractalPlugin.getDefault().getLog().log(new Status(IStatus.ERROR,FractalPlugin.PLUGIN_ID,"",e));
		}
		return result;
	}
	
	public static EFactory getFactory(EObject peekObject, EStructuralFeature feature){
		List<EFactory> factories = getRegisterExtensions();
		Iterator<EFactory> iterator = factories.iterator();
		while(iterator.hasNext()){
			EFactory factory = iterator.next();
			// We search extension that has a classifier with the same name that the element
			// that contain the unknown element and we look if it has a feature with the same name that the
			// given one
			EClassifier classifier = factory.getEPackage().getEClassifier(peekObject.eClass().getName());
			if(classifier != null && classifier instanceof EClass){
				if(((EClass)classifier).getEStructuralFeature(feature.getName()) != null){
					return factory;
				}
			}
		}
		return null;
	}
	
	private static Map<AnyType, EObject> newElements = new HashMap<AnyType,EObject>();
	
	private static void processUnknownElement(EObject peekObject, AnyType unknownElement){
		Iterator<Entry> elements = unknownElement.getMixed().iterator();
		while(elements.hasNext()){
			Entry entry = elements.next();
			EFactory factory = getFactory(peekObject, entry.getEStructuralFeature());
			if(factory != null){
				EClass class_ = (EClass)factory.getEPackage().getEClassifier(peekObject.eClass().getName());
				EStructuralFeature feature = class_.getEStructuralFeature(entry.getEStructuralFeature().getName());
				EObject newObject = null;
				boolean addNewObject = false;
				if(newElements.get(unknownElement) == null){
					newObject = factory.create((EClass)feature.getEType());
					newElements.put(unknownElement, newObject);
					addNewObject = true;
				}else{
					newObject = newElements.get(unknownElement);
				}
				EStructuralFeature existingFeature = peekObject.eClass().getEStructuralFeature(feature.getName());
				if(existingFeature == null && peekObject.eClass().getEStructuralFeature("any") != null){
					FeatureMap any = (FeatureMap)peekObject.eGet(peekObject.eClass().getEStructuralFeature("any"));
					EStructuralFeature newFeature = getEStructuralFeature(any, feature.getName());
					if(newFeature == null){
						newFeature = FractalResourceFactoryImpl.getExtendedMetaData().demandFeature(FractalPackage.eNS_URI, feature.getName(), true);
						if(addNewObject){
							if(feature.getUpperBound()>0){
								any.addAll(newFeature, new EcoreEList.Dynamic((InternalEObject)peekObject,feature));
								((EList)any.get(newFeature,true)).add(newObject);
							}else{
								any.add(newFeature, newObject);
							}
						}
					}else{
						if(feature.getUpperBound()>0){
							((EList)any.get(newFeature,true)).add(newObject);
						}else{
							any.add(newFeature, newObject);
						}
					}
					//newFeature.setChangeable(feature.isChangeable());
					//newFeature.setEType(feature.getEType());
					//newFeature.setUpperBound(feature.getUpperBound());
					
				}
				
				processUnknownElement(newObject, (AnyType)entry.getValue());
			}
		}
		
		Iterator<Entry> attributes = unknownElement.getAnyAttribute().iterator();
		while(attributes.hasNext()){
			Entry entry = attributes.next();
			
			EStructuralFeature existingFeature = peekObject.eClass().getEStructuralFeature(entry.getEStructuralFeature().getName());
			if(existingFeature != null){
				peekObject.eSet(existingFeature, entry.getValue());
			}
		}
	}
	
	public static EStructuralFeature getEStructuralFeature(FeatureMap any, String featureName){
		Iterator<Entry> iterator = any.listIterator();
		while(iterator.hasNext()){
			Entry entry = iterator.next();
			if(entry.getEStructuralFeature().getName().equals(featureName)){
				return entry.getEStructuralFeature();
			}
		}
		return null;
	}
	
	public static void handleUnknowElement(EObject peekObject, AnyType unknownElement){
		newElements.clear();
		processUnknownElement(peekObject, unknownElement);
	}
}
