/**
 * <copyright>
 *
 * Copyright (c) 2005, 2007 IBM Corporation and others.
 * All rights reserved.   This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   IBM - Initial API and implementation
 *
 * </copyright>
 *
 * $Id: OCLResource.java,v 1.14 2007/10/15 22:19:22 cdamus Exp $
 */


package org.ow2.fractal.f4e.fractal.ocl.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.ocl.OCL;
import org.eclipse.ocl.OCLInput;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.Constraint;
import org.ow2.fractal.f4e.fractal.ocl.adapter.ValidatorAdapter;


/**
 * A resource for loading and saving OCL expressions.
 */
public class OCLResource
	extends XMIResourceImpl {

    private static Map<String, Object> saveOptions = new java.util.HashMap<String, Object>();
    private OCL ocl;
    private URI oclFile;
    private List<Object> constraints;
    
    static {
        saveOptions.put(XMLResource.OPTION_SAVE_TYPE_INFORMATION, true);
    }
    
	/**
	 * Initializes me with my URI.
	 * 
	 * @param uri my URI
	 */
	public OCLResource(URI uri) {
		super(URI.createFileURI(uri.toFileString() + ".xmi"));
		
		oclFile = uri;
		//oclResource = new OCLResource(URI.createFileURI(uri.toFileString() + ".xmi"));
		ocl = ValidatorAdapter.getInstance().getOCL();//org.eclipse.ocl.OCL.newInstance(org.eclipse.ocl.ecore.EcoreEnvironmentFactory.INSTANCE,this);		
		
		try{
			this.loadOclFile();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Loads an OCL expression from the specified <code>path</code>.  The
	 * OCL expression is converted to a string using a custom AST visitor that
	 * renders the string representation.
	 * 
	 * @param path the absolute path of the XMI file to load
	 * @return the string representation of the OCL expression, if found
	 * @throws IOException if anything goes wrong in loading the XMI file
	 */
	public static String load(String path) throws IOException {
	/*	String result = null;
		
		ResourceSet rset = new ResourceSetImpl();
		
		// create and load the resource
		OCLResource res = new OCLResource(URI.createFileURI(path));
		rset.getResources().add(res);
		
		res.load(Collections.EMPTY_MAP);
		
		OCLExpression<Object> expr = res.getOCLExpression();
		if (expr != null) {
			result = expr.accept(ToStringVisitor.getInstance(expr));
		}
		
		return result;*/
		return null;
	}
	
	
	/**
	 * Load ocl file.
	 * 
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParserException the parser exception
	 */
	private void loadOclFile()
	throws IOException, ParserException {
		OCLInput oclInput = new OCLInput(new FileInputStream(oclFile.toFileString()));
		
		constraints = ocl.parse(oclInput);
		
		Iterator<Object> it = constraints.iterator();
		
		while(it.hasNext()){
			Constraint c = (Constraint)it.next();
			this.getContents().add(c);
		}
			
	}
	
	public void save() throws IOException{
		
		this.save(saveOptions);
	}
	
}
