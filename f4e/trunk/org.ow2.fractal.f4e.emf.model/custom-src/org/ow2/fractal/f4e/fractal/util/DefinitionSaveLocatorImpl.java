/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.util;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

import org.eclipse.emf.common.util.URI;

// TODO: Auto-generated Javadoc
/**
 * The Class DefinitionSaveLocatorImpl is used to find the save location
 * of a definition. User can specify a location map.
 * The key of this map is a regexp, the value a location.
 * When we save a definition, we search the first regexp which matches the
 * definition name, and we save it at the location associated with the regexp key.
 * 
 * Note : that is the definition name is something like helloworld.Definition, and
 * the location src, the save file will be src/helloworld/Definition.fractal
 */
public class DefinitionSaveLocatorImpl implements DefinitionSaveLocator {
	
	/** The Constant DEFAULT_SAVE_LOCATION. */
	private static final String DEFAULT_SAVE_LOCATION="src";
	
	/** The Constant DEFAULT_SAVE_PATTERN. */
	private static final String DEFAULT_SAVE_PATTERN=".*";

	/** The Constant FRACTAL_FILE_EXTENTION. */
	private static final String FRACTAL_FILE_EXTENTION=".fractal";
	
	/** The pattern location map. */
	Map<Pattern,String> patternLocationMap = new HashMap<Pattern, String>();

	/**
	 * Instantiates a new definition save locator impl.
	 * 
	 * @param patternLocationMap the pattern location map
	 */
	public DefinitionSaveLocatorImpl(Map<String, String> patternLocationMap) {
		initializePattern(patternLocationMap);
	}

	/**
	 * Initialize pattern.
	 * 
	 * @param locationMap the location map
	 */
	private void initializePattern(Map<String, String> locationMap){
		if(locationMap == null || locationMap.isEmpty()){
			patternLocationMap.put(Pattern.compile(DEFAULT_SAVE_PATTERN), DEFAULT_SAVE_LOCATION);
		}else{

			Iterator<String> iterator = locationMap.keySet().iterator();

			while(iterator.hasNext()){
				String regexp = iterator.next();
				Pattern pattern = Pattern.compile(regexp);
				patternLocationMap.put(pattern, locationMap.get(regexp));
			}
		}
	}

	/**
	 * Gets the base dir.
	 * 
	 * @param prefixDefinitionName the prefix definition name
	 * 
	 * @return the base dir
	 */
	private String getBaseDir(String prefixDefinitionName){
		String result = "";
		boolean notFound = true;
		Iterator<Pattern> iterator = patternLocationMap.keySet().iterator();

		while(iterator.hasNext()){
			Pattern pattern = iterator.next();
			if(pattern.matcher(prefixDefinitionName).matches()){
				result = patternLocationMap.get(pattern);
			}
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see org.ow2.fractal.adl.extended.fractaladlextended.util.DefinitionSaveLocator#getSaveLocation(java.lang.String)
	 */
	public URI getSaveLocation(String definitionName){	
		String path = new String();
		String[] splits = definitionName.split("\\.");

		for(int i=0; i< splits.length -1; i++){
			path = path + splits[i] + File.separator;
		}

		path = getBaseDir(path) + File.separator + path + splits[splits.length-1] + FRACTAL_FILE_EXTENTION;

		return URI.createFileURI(path);
	}
}
