package org.ow2.fractal.f4e.fractal.util;

import java.util.List;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMLResource;

public interface IFractalXMLResource extends XMLResource{
	/** The Constant FRACTAL_COMMENTS_ANNOTATION. 
	 * When we read a fractal xml file we store comments
	 * in a eAnnotation of the next element that will be red.
	 * The source field of the annotation is "comments"
	 **/
	static final String FRACTAL_COMMENTS_ANNOTATION = "comments";
	
	/** The Constant FRACTAL_ADL_FILENAME_EXTENSION. */
	static final String FRACTAL_ADL_FILENAME_EXTENSION = "fractal";
	
	/** The Constant FRACTAL_DOCTYPE_PUBLIC_ID. 
	 *  by default we ignore the dtd at load time.
	 *  When we save a fractal file we set the dtd to the fractal standard dtd.
	 * */
	static final String FRACTAL_DOCTYPE_PUBLIC_ID = "-//objectweb.org//DTD Fractal ADL 2.0//EN";
	
	/** The Constant FRACTAL_DOCTYPE_SYSTEM_ID. */
	static final String FRACTAL_DOCTYPE_SYSTEM_ID =  "classpath://org/objectweb/fractal/adl/xml/standard.dtd";
	
	/** This constant can be used at load time, if it is set to false we will not try to load the parents definitions*/
	static final String FRACTAL_SHOULD_LOAD_EXTENDS = "org.ow2.fractal.f4e.options.shouldLoadExtends";
	
	/** This constant can be used as key for Fractal resource loading.
	 *  
	 *  Transactional support can be set to false when we want to load
	 *  a fractal resource without eclipse. 
	 */
	public static final String TRANSACTIONAL_SUPPORT_OPTION="org.ow2.fractal.f4e.options.transactional";
}
