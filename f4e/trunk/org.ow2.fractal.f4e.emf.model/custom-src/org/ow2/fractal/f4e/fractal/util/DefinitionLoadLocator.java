/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.ow2.fractal.f4e.fractal.plugin.FractalPlugin;

// TODO: Auto-generated Javadoc
/**
 * The Class DefinitionLoadLocator.
 * This class is used to manage the location where the fractal definitions
 * must be searched.
 * 
 * The user can define a list of locations.
 * Fractal definitions will be searched in these locations.
 * 
 * It will be also possible to create a location map, with regexp as key
 * and location as value. The regexp try to match the fractal definition if it is
 * the case, the associated location is returned.
 * 
 * For the moment, search only in the current directory.
 */
public class DefinitionLoadLocator {
	
	/** The Constant DEFAULT_LOAD_LOCATION. */
	private static final String DEFAULT_LOAD_LOCATION="./";
	
	/** The Constant DEFAULT_LOAD_PATTERN. */
	private static final String DEFAULT_LOAD_PATTERN=".*";
	
	/** The Constant FRACTAL_FILE_EXTENTION. */
	private static final String FRACTAL_FILE_EXTENTION=".fractal";
	
	private URIConverter converter;
	
	/** The locations. */
	Collection<String> locations = new ArrayList<String>();
	
	Map<URI, IDefinitionLocatorHelper> helpers = new HashMap<URI, IDefinitionLocatorHelper>();
	
	/**
	 * Instantiates a new definition load locator.
	 */
	public DefinitionLoadLocator(){
		converter = new FractalExtensibleURIConverterImpl();
		
		//relative path
		locations.add("");
		
		//absolute path
		locations.add("file://");
		
		//classpath
		locations.add("classpath:/");
		
		//jar files
		//locations.add("jar:file:///test/definition.jar!/");
		
	}
	
	/**
	 * Sets the locations.
	 * 
	 * @param locations the locations
	 */
	public void addLocations(List<String> locations){
		this.locations = locations;
	}
	
	
	/**
	 * Adds the location.
	 * 
	 * @param location the location
	 */
	public void addLocation(String location){
		if(!locations.contains(location)){
			locations.add(location);
		}
	}
	
	
	/**
	 * Gets the complete location.
	 * 
	 * Return the complete location of a fractal definition from a
	 * partial path corresponding to a definition and a list of default locations,
	 * where the definitions will be searched.
	 * 
	 * For example if the uncompletePath is "helloworld/Definition1.fractal",
	 * and the locations contains "src" and "./" dirctory, we concat
	 * the default location with the uncompletePath. The first location
	 * that holds the file is used.
	 * 
	 * @param uncompletePath the partial path is the string that represents a fractal
	 * definition. It is something like this : helloworld/Definition1.fractal
	 * 
	 * @return the complete location.
	 */
	private URI getCompleteLocation(URI resourceURI, String uncompletePath){
		Iterator<String > iterator = helpers.get(resourceURI) != null?
				helpers.get(resourceURI).getLocations(resourceURI).iterator():locations.iterator();
	
		while(iterator.hasNext()){
			String location = iterator.next();
			URI uri = URI.createURI(location + uncompletePath);
			if(!uri.isPlatform()){
				URI.createPlatformResourceURI(location + uncompletePath, true);
			}else{
				URI.createPlatformResourceURI(uri.devicePath() + uncompletePath, true);
			}
			if(converter.exists(uri, null)){
				return uri;
			}
		}	
		
		return null;	
	}
	

	
	/**
	 * Gets the load location.
	 * 
	 * @param definitionName the definition name
	 * 
	 * @return the load location
	 */
	public URI getLoadLocation(URI resourceURI, String definitionName){	
		URI result = null;
		String path = new String();
		
		path = definitionName.replace('.', File.separatorChar) + FRACTAL_FILE_EXTENTION;
		
		result =  getCompleteLocation(resourceURI, path);

		return result;
	}
	
	public void addHelper(URI resourceURI, IDefinitionLocatorHelper helper){
		helpers.put(resourceURI, helper);
	}
	
	public void removeHelper(URI resourceURI){
		helpers.remove(resourceURI);
	}
	
	public IDefinitionLocatorHelper getHelper(URI resourceURI){
		return helpers.get(resourceURI);
	}
}
