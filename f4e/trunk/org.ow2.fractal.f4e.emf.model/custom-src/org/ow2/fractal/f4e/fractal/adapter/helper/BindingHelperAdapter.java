/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter.helper;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.TokenRewriteStream;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.Constant;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.ValueElement;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.parser.InterfaceADLLexer;
import org.ow2.fractal.f4e.fractal.parser.InterfaceADLParser;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;


// TODO: Auto-generated Javadoc
/**
 * The Class BindingHelperAdapter.
 */
public class BindingHelperAdapter extends HelperAdapter<Binding>
	implements IBindingHelper {
	
	
	/**
	 * Instantiates a new binding helper adapter.
	 * 
	 * @param binding the binding
	 */
	public BindingHelperAdapter(Binding binding) {
		super(binding);
	}

	/* (non-Javadoc)
	 * @see org.ow2.fractal.adl.extended.fractalextended.adapter.helper.HelperAdapter#isAdapterForType(java.lang.Object)
	 */
	
	public boolean isAdapterForType(Object type) {
		return  super.isAdapterForType(type) || type == IBindingHelper.class || type == IMerge.class;
	}
	
	protected void updateClient(){
		Value value = getInterfaceValue(t.getClient());
		t.setClientAST(value);
		t.setClientInterface(getInterface(value));
	}
	
	protected void updateServer(){
		Value value = getInterfaceValue(t.getServer());
		t.setServerAST(value);
		t.setServerInterface(getInterface(value));
	}
	
	
	private final static InterfaceADLLexer lexer = new InterfaceADLLexer();
	private final static TokenRewriteStream tokens = new TokenRewriteStream(lexer);
	//private final parser = new InterfaceADLParser(tokens,t);
	
	/**
	 * Parse the ADL string which contains a interface name, and return
	 * a Value which is the model representation of the string.
	 * 
	 * @param string the string
	 * 
	 * @return the interface value
	 */
	public Value getInterfaceValue(String string){
		Value value = null;
		
		//value = cache.get(string);
		//if(value != null) return value;
		
		if(t.eContainer() != null && string != null){
			IAbstractComponentHelper helper = (IAbstractComponentHelper)HelperAdapterFactory.getInstance().adapt(t.eContainer());
			
			if(helper != null){
				Definition definition = helper.getParentDefinition();
				
				try{
					CharStream s = new ANTLRStringStream(string);
					//InterfaceADLLexer lexer = new InterfaceADLLexer(s);
					lexer.setCharStream(s);
					TokenRewriteStream tokens = new TokenRewriteStream(lexer);
					
					InterfaceADLParser grammar = new InterfaceADLParser(tokens, definition);
					
					value = grammar.getValue();
					
				//	cache.put(string, value);
				}catch(Exception e){
					System.err.println(e.getMessage());
				}
			}
		}
		return value;
	}
	
	
	/**
	 * Search and return the Interface specified by the interfaceValue.
	 * It search in the set of interfaces of the binding parent component,
	 * in the set of interfaces of the children of the parent component.
	 * 
	 * Note that we also look in the extended definition.
	 * 
	 * @param interfaceValue the interface value
	 * 
	 * @return the interface
	 */
	public Interface getInterface(Value interfaceValue){
		if(interfaceValue == null) return null;
		
		Vector<ValueElement> elements = new Vector<ValueElement>(interfaceValue.getElements());
		
		if(elements.size()>0 && elements.firstElement() instanceof Constant && 
				((Constant)elements.firstElement()).getConstant().endsWith("this")){
				elements.remove(elements.firstElement());
				
				//we also remove the '.' after the 'this'
				elements.remove(elements.firstElement());
		}
		AbstractComponent abstractComponent = t.eContainer()!=null&&t.eContainer() instanceof AbstractComponent?(AbstractComponent)t.eContainer():null;
		
		if(abstractComponent!=null){
			IAbstractComponentHelper helper = (AbstractComponentHelperAdapter)HelperAdapterFactory.getInstance().adapt(abstractComponent);
			if(helper != null){
				return helper.getInterface(elements);
			}
		}
		
		return null;
	}
	
	
	private String getInterfaceName(Interface itf){
		if(itf.getAbstractComponent() == t.getAbstractComponent()){
			if(itf != null){
				return "this" + "." + itf.getName();
			}
		}else{
			if(itf != null){
				if(itf.getAbstractComponent() != null && itf.getAbstractComponent() instanceof Component){
					return itf.getAbstractComponent().getName() + "." + itf.getName();
				}
			}
		}
		return "";
	}
	
	public String getClientInterfaceName(){
		return getInterfaceName(t.getClientInterface());
	}
	
	public String getServerInterfaceName(){
		return getInterfaceName(t.getServerInterface());
	}
	
	
	/*private static Interface getInterface(AbstractComponent abstractComponent, Vector<ValueElement> elements){
		if(!elements.isEmpty()){
			AbstractComponentHelper helper = (AbstractComponentHelper)HelperAdapterFactory.getInstance().adapt(abstractComponent,AbstractComponentHelperAdapter.class);	
			ValueElement elem = elements.firstElement();
			String value = elem instanceof Constant ? ((Constant)elem).getConstant() : ((Reference)elem).getName();
			if(helper != null){
				if(elements.firstElement() == elements.lastElement()){
					return helper.getInterface(value);
				}else{
					Component subComponent = helper.getSubComponent(value);
					if(subComponent != null){
						elements.remove(elements.firstElement());
						return getInterface(subComponent,elements);
					}
				}
			}
		}
		return null;
	}*/
	
	public void update(final int featureID, IProgressMonitor monitor){
		Command cmd = new RecordingCommand(FractalTransactionalEditingDomain
				.getEditingDomain()) {
			protected void doExecute() {
				switch (featureID) {
					case FractalPackage.BINDING__CLIENT_AST:
							updateClient();
						break;
					case FractalPackage.BINDING__SERVER_AST:
							updateServer();
						break;
					default:
						break;
				}
			}
		};

		FractalTransactionalEditingDomain.getEditingDomain().getCommandStack()
				.execute(cmd);
	}
	
	
	public Binding getElementWithSameName(List<Binding> bindings){
		Binding result = null;
		if(bindings == null || t == null){
			return result;
		}
		
		Iterator<Binding> iterator = bindings.iterator();
		
		while(iterator.hasNext()){
			Binding nextBinding = iterator.next();
			if((nextBinding.getClient() != null) &&
					(nextBinding.getServer() != null) &&
					(t.getClient() != null) &&
					(t.getServer() != null) &&
				nextBinding.getClient().equals(t.getClient()) &&
				nextBinding.getServer().equals(t.getServer())){
				result = nextBinding;
				break;
			}
		}
		
		return result;
	}
	
	public void merge(List<Node<? extends EObject>> srcNodes, boolean override, IProgressMonitor monitor){
		// TODO : do the merge for all bindings
//		if(override == true){
//			if(t.getServer() == null || (t.getServer() != null && !t.getServer().equals(srcNodes.get(0).getT().eGet(FractalPackage.Literals.BINDING__SERVER)))){
//				t.eSet(FractalPackage.Literals.BINDING__SERVER, srcNodes.get(0).getT().eGet(FractalPackage.Literals.BINDING__SERVER));
//			}
//			if(t.getClient() == null ||(t.getClient() != null && !t.getClient().equals(srcNodes.get(0).getT().eGet(FractalPackage.Literals.BINDING__CLIENT)))){
//				t.eSet(FractalPackage.Literals.BINDING__CLIENT,srcNodes.get(0).getT().eGet(FractalPackage.Literals.BINDING__CLIENT));
//			}
//		}
//    	t.getHelper().update(FractalPackage.BINDING__CLIENT_AST);
//    	t.getHelper().update(FractalPackage.BINDING__SERVER_AST);
		if(override == true){
		//if(t.getServer() == null || (t.getServer() != null && !t.getServer().equals(srcNodes.get(0).getT().eGet(FractalPackage.Literals.BINDING__SERVER)))){
			t.eSet(FractalPackage.Literals.BINDING__MERGED_SERVER, ((Value)srcNodes.get(0).getT().eGet(FractalPackage.Literals.BINDING__SERVER_AST)).getHelper().resolve(srcNodes.get(0).getEffectiveParameters()));
		//}
		
		//if(t.getClient() == null ||(t.getClient() != null && !t.getClient().equals(srcNodes.get(0).getT().eGet(FractalPackage.Literals.BINDING__CLIENT)))){
			t.eSet(FractalPackage.Literals.BINDING__MERGED_CLIENT,((Value)srcNodes.get(0).getT().eGet(FractalPackage.Literals.BINDING__CLIENT_AST)).getHelper().resolve(srcNodes.get(0).getEffectiveParameters()));
		//}
		
		Value valueClient = getInterfaceValue(t.getMergedClient());
		t.setClientInterface(getInterface(valueClient));
		
		Value valueServer = getInterfaceValue(t.getMergedServer());
		t.setServerInterface(getInterface(valueServer));
		}
	}
	
	
	public int[] getAttributeIDs(){
		return new int[]{FractalPackage.BINDING__CLIENT, FractalPackage.BINDING__SERVER};
	}
}
