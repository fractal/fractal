/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.util;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.ow2.fractal.f4e.fractal.System;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.FractalPackage;

/**
 * Definition Save class is used to save fractal definitions.
 * The definition can be saved in xmi or fractal ADL format.
 */
public class DefinitionSaveImpl {
	ResourceSet resourceSet;
	
	
	public DefinitionSaveImpl(){
		EPackage.Registry.INSTANCE.put(FractalPackage.eINSTANCE.getNsURI(),
				FractalPackage.eINSTANCE);

		resourceSet = new FractalResourceSetImpl();
	}

	/**
	 * Save a definition in a .fractal file.
	 * 
	 * @param definition
	 */
	public void save(Definition definition, Map<?, ?> options) throws IOException{
		if(definition == null || definition.getName() == null){
			return;
		}
		
		Map<String, String> map = (Map<String,String>)options.get(DefinitionSaveLocator.OPTION_FRACTAL_SAVE_LOCATOR_MAP);
		DefinitionSaveLocator definitionSaveLocator = new DefinitionSaveLocatorImpl(map);
		
		Resource createdResource = 
			resourceSet.createResource(definitionSaveLocator.getSaveLocation(definition.getName()));
		
		createdResource.getContents().add(definition);
		createdResource.save(options);
	}
	
	/**
	 * Save all definitions present in the root System element
	 * @param system
	 * @param options
	 */
	public void save(System system, Map<?, ?> options) throws IOException{
		Iterator<Definition> iterator = system.getDefinitions().iterator();
		
		while(iterator.hasNext()){
			Definition definition = iterator.next();
			
             EcoreUtil.Copier copier = new EcoreUtil.Copier();
             EObject definitionCopy = copier.copy(definition);
             copier.copyReferences();
             
			this.save((Definition)definitionCopy, options);
		}
	}

}
