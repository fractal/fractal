package org.ow2.fractal.f4e.fractal.adapter.helper.marker;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.ui.EclipseUIPlugin;
import org.eclipse.emf.common.util.Diagnostic;

/**
 * 
 * @author Yann Davin
 */
public class MarkerHelper implements IMarkerHelper{
	private boolean hasErrors = false;
	private boolean hasWarnings = false;
	
	public boolean hasErrors(){
		return hasErrors;
	}
	
	public void setHasErrors(boolean hasErrors){
		this.hasErrors = hasErrors;
	}
	
	public boolean hasWarnings(){
		return hasWarnings;
	}
	
	public void setHasWarnings(boolean hasWarnings){
		this.hasWarnings = hasWarnings;
	}
	
	private Diagnostic diagnostic;
	public void setDiagnostic(Diagnostic diagnostic){
		this.diagnostic = diagnostic;
	}
	
	public Diagnostic getDiagnostic(){
		return diagnostic;
	}
}
