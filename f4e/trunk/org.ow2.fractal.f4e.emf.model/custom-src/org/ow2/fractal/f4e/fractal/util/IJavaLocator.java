package org.ow2.fractal.f4e.fractal.util;

import org.eclipse.emf.common.util.URI;

/**
 * 
 * @author Yann Davin
 *
 */
public interface IJavaLocator {

	/** From the uri of the fractal file that will be opened,
	 * find the related project and add all jar presents in the project classpath
	 * as default search locations for the DefinitionLoader. 
	 * 
	 * @param resourceURI the resource uri
	 * 
	 * @generated
	 */
	public void addDefaultLocations(URI resourceURI);
}
