/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.util;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.BasicExtendedMetaData;
import org.eclipse.emf.ecore.util.ExtendedMetaData;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.ow2.fractal.f4e.fractal.FractalPackage;


// TODO: Auto-generated Javadoc
/**
 * The Class FractalAdlExtendedMetaData.
 */
public class FractalMetaData extends BasicExtendedMetaData
{
 
  /** The xml map. */
  protected XMLResource.XMLMap xmlMap;

  /**
   * Instantiates a new fractal adl extended meta data.
   * 
   * @param xmlMap the xml map
   */
  public FractalMetaData(XMLResource.XMLMap xmlMap)
  {
    super();
    this.xmlMap = xmlMap;
    
  }
  
  /* (non-Javadoc)
   * @see org.eclipse.emf.ecore.util.BasicExtendedMetaData#getNamespace(org.eclipse.emf.ecore.EPackage)
   */
 public String getNamespace(EPackage ePackage)
  {
	//  return "";
    return super.getNamespace(ePackage);
  }

  /* (non-Javadoc)
   * @see org.eclipse.emf.ecore.util.BasicExtendedMetaData#getPackage(java.lang.String)
   */
  public EPackage getPackage(String namespace)
  {
	  if(namespace == null || namespace.equals("")){
		  return FractalPackage.eINSTANCE;
	  }else{
		  return super.getPackage(namespace);
  	  }
  }

/* (non-Javadoc)
   * @see org.eclipse.emf.ecore.util.BasicExtendedMetaData#getName(org.eclipse.emf.ecore.EClassifier)
   */
  public String getName(EClassifier eClassifier)
  {
	  if(super.getName(eClassifier).equals("Definition")){
		  return "definition";
	  }
    XMLResource.XMLInfo info = xmlMap.getInfo(eClassifier);
    if (info != null)
    {
      String name = info.getName();
      if (name != null)
      {
        return info.getName();
      }
    }
    return super.getName(eClassifier);
  }
//  
//  /* (non-Javadoc)
//   * @see org.eclipse.emf.ecore.util.BasicExtendedMetaData#getName(org.eclipse.emf.ecore.EStructuralFeature)
//   */
  public String getName(EStructuralFeature eStructuralFeature)
  {
	
    XMLResource.XMLInfo info = xmlMap.getInfo(eStructuralFeature);
    if (info != null)
    {
      String name = info.getName();
      if (name != null)
      {
        return info.getName();
      }
    }
    return super.getName(eStructuralFeature);
  }
//
//  /* (non-Javadoc)
//   * @see org.eclipse.emf.ecore.util.BasicExtendedMetaData#getFeatureKind(org.eclipse.emf.ecore.EStructuralFeature)
//   */
  public int getFeatureKind(EStructuralFeature feature)
  {
    XMLResource.XMLInfo info = xmlMap.getInfo(feature);
    if (info != null)
    {
      switch (info.getXMLRepresentation())
      {
        case XMLResource.XMLInfo.ELEMENT:
          return ExtendedMetaData.ELEMENT_FEATURE;
        case XMLResource.XMLInfo.ATTRIBUTE:
           return ExtendedMetaData.ATTRIBUTE_FEATURE;
       }
     }
     return super.getFeatureKind(feature);
   }

  
public EClassifier getType(String namespace, String name) {
	// TODO Auto-generated method stub
	return super.getType(namespace, name);
}

protected boolean isFeatureKindSpecific() {
	return false;
  }

@Override
public EReference getXMLNSPrefixMapFeature(EClass class1) {
	// TODO Auto-generated method stub
	return super.getXMLNSPrefixMapFeature(class1);
}
     
 }

