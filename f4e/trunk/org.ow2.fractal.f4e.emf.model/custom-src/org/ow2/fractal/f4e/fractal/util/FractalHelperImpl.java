/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
 package org.ow2.fractal.f4e.fractal.util;

 import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMLHelperImpl;
import org.ow2.fractal.f4e.extension.FractalADLExtension;
import org.ow2.fractal.f4e.extension.FractalADLExtensionHelper;
import org.ow2.fractal.f4e.fractal.FractalPackage;


 public class FractalHelperImpl extends XMLHelperImpl
 {
	 
   public FractalHelperImpl(XMLResource resource)
   {
     super(resource);
   }
   
   //  Remove xmi and other prefixes for root element serialization. 
   
   public List<String> getPrefixes(EPackage package1) {
	   return super.getPrefixes(package1);
	   //return new ArrayList<String>();
   }

   public String getNamespaceURI(String prefix) {
	   return super.getNamespaceURI(prefix);
	   //return "";
   }

  
   public EStructuralFeature getFeature(EClass class1, String namespaceURI,
		   String name, boolean isElement) {  
	   EStructuralFeature feature = super.getFeature(class1, namespaceURI, name, isElement);
	 
	   // If we don't find the feature we look in registred FractalADL extensions
	   if(feature == null ){
		   FractalADLExtensionHelper helper = new FractalADLExtensionHelper();
		   feature = helper.searchEStructuralFeature(class1, name);
	   }
	   
	  return feature;
   }
   
 }
 