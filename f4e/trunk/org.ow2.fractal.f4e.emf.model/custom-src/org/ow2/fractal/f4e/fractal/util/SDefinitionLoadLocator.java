package org.ow2.fractal.f4e.fractal.util;

/**
 * Singleton class which can be used to setup locations where fractal files
 * can be found. This class can be used outside eclipse environment.
 * 
 * @author Yann Davin
 *
 */
public class SDefinitionLoadLocator extends DefinitionLoadLocator{
	private SDefinitionLoadLocator(){
		super();
	}
	
	/**
	 * Gets the single instance of DefinitionLoadLocator.
	 * 
	 * @return single instance of DefinitionLoadLocator
	 */
	public static DefinitionLoadLocator getInstance(){
		return SingletonHolder.instance;
	}
	
	/**
	 * The Class SingletonHolder.
	 */
	private static class SingletonHolder {
		
		/** The instance. */
		private static SDefinitionLoadLocator instance = new SDefinitionLoadLocator();
	}
}
