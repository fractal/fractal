/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.util;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.commands.operations.IOperationHistoryListener;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.ExtendedMetaData;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.IWorkspaceCommandStack;
import org.eclipse.gmf.runtime.emf.core.resources.GMFResourceFactory;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Shell;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.factory.OperationHistoryListenerAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.DefinitionHelperAdapter;
import org.ow2.fractal.f4e.fractal.plugin.FractalPlugin;
import org.ow2.fractal.f4e.fractal.util.visitor.LoadADLFileJob;


public class FractalResourceSetImpl extends ResourceSetImpl implements FractalResourceSet {
// Tried to extends ProjectResourceSetImpl
//	public IProject getProject() {
//		IProject project =null;
//		IWorkbench iworkbench = PlatformUI.getWorkbench();
//		if (iworkbench == null){
//			IWorkbenchWindow iworkbenchwindow = iworkbench.getActiveWorkbenchWindow();
//			if (iworkbenchwindow == null) {
//				IWorkbenchPage iworkbenchpage = iworkbenchwindow.getActivePage();
//				if (iworkbenchpage == null){
//					IEditorPart editor = iworkbenchpage.getActiveEditor();
//					 IEditorInput input = editor.getEditorInput();
//				      if (!(input instanceof IFileEditorInput)){
//				    	  return null;
//				      }
//				      else{
//				    	  IFile file = ((IFileEditorInput)input).getFile();
//				    	  project =  file.getProject();
//				      }
//				}
//			}
//		}
//		
//		return project;
//	}
//
//	ResourceSetWorkbenchSynchronizer synchronizer;
//	public ResourceSetWorkbenchSynchronizer getSynchronizer() {
//		// TODO Auto-generated method stub
//		if(synchronizer == null || (synchronizer.getProject() != getProject())){
//			synchronizer = new ResourceSetWorkbenchEditSynchronizer(this,getProject());
//		}
//		return synchronizer;
//	}

	public FractalResourceSetImpl(){
		super();
		setURIResourceMap(new HashMap<URI, Resource>());
		
		getResourceFactoryRegistry().getExtensionToFactoryMap()
        .put("ecore", new EcoreResourceFactoryImpl());
	
		getResourceFactoryRegistry().getExtensionToFactoryMap()
        .put("xmi", new XMIResourceFactoryImpl());
	
		getResourceFactoryRegistry().getExtensionToFactoryMap()
        .put("fractal", new FractalResourceFactoryImpl());

//		Factory that allows synchronization between DOM<->EMF.
//		It is unstable, so we don't use it
//		getResourceFactoryRegistry().getExtensionToFactoryMap()
//        .put("fractal", new FractalTranslatorResourceFactory());
		
		getResourceFactoryRegistry().getExtensionToFactoryMap()
        .put("fractal_diagram", new GMFResourceFactory());
		
	}
	
	public ExtendedMetaData getFractalMetaData(){
		return FractalResourceFactoryImpl.getExtendedMetaData();
	}
	
	public Definition getDefinition(URI uri){
		Resource resource = getResource(uri,true);
		if(resource instanceof IFractalResource){
			return ((IFractalResource)resource).getRootDefinition();
		}else{
			return null;
		}
	}
	
	public Resource getResource(URI uri, Resource resource){
		Resource loadedResource = getResource(uri, true);
		
		if(loadedResource instanceof IFractalResource && resource instanceof IFractalResource){
			((IFractalResource)loadedResource).addDependency(resource);
		}
		
		return loadedResource;
	}
	
	public Resource getResource(URI uri, boolean loadOnDemand, boolean shouldLoadExtends) {
		getLoadOptions().put(IFractalXMLResource.FRACTAL_SHOULD_LOAD_EXTENDS,shouldLoadExtends);
		Resource resource = getResource(uri, loadOnDemand);
		// we reset the FRACTAL_SHOULD_LOAD_EXTENDS to its default value
		getLoadOptions().put(IFractalXMLResource.FRACTAL_SHOULD_LOAD_EXTENDS,true);
		return resource;
	}
	
	public Resource getResource(URI uri, boolean loadOnDemand) {
	
		Map<URI, Resource> map = getURIResourceMap();
		 boolean inResourceSet = true;
		 
		 if(uri == null){
			 FractalPlugin.getDefault().getLog().log(new Status(IStatus.ERROR,FractalPlugin.PLUGIN_ID,"",new NullPointerException("URI == null")));
		 }
		 
		 //If it is not a fractal file we delegate the loading
		 if(!uri.fileExtension().equals("fractal")){
			 return super.getResource(uri, loadOnDemand);
		 }
		 
		 // We should update model at loading time, when the resource is loaded
		 // and when it has its resourceSet initialized.
		 if (map != null)
		 {
		    Resource resource = map.get(uri);
		    if(resource == null){
		    	inResourceSet = false;
		    }
		 }
		
		final Resource resource = super.getResource(uri, true);
		
		if(resource instanceof IFractalResource && ((IFractalResource)resource).getRootDefinition() == null){
			map.remove(resource.getURI());
			resource.eAdapters().clear();
			resource.unload();
			return null;
		}
		
		TransactionalEditingDomain domain = FractalTransactionalEditingDomain
		.getEditingDomain();
		
		// The resource is loaded for the first time in the resource set
		if(inResourceSet == false && resource instanceof IFractalResource){	
			if(!(getLoadOptions().get(IFractalXMLResource.TRANSACTIONAL_SUPPORT_OPTION) != null &&
					(Boolean)getLoadOptions().get(IFractalXMLResource.TRANSACTIONAL_SUPPORT_OPTION) == false)){
					// We attach the operation history listener
					IOperationHistoryListener listener = (IOperationHistoryListener)OperationHistoryListenerAdapterFactory.getInstance().adapt(resource);
					((IWorkspaceCommandStack)FractalTransactionalEditingDomain.getEditingDomain().getCommandStack()).getOperationHistory().addOperationHistoryListener(listener);
			}
			
			// We have to update merge information when the resource has been loaded.
			DefinitionHelperAdapter helper = (DefinitionHelperAdapter)HelperAdapterFactory.getInstance().adapt(((IFractalResource)resource).getRootDefinition());
		//	helper.update(FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS, null);
		//	helper.update(FractalPackage.ABSTRACT_COMPONENT__MERGED_INTERFACES, null);
		//	helper.update(FractalPackage.ABSTRACT_COMPONENT__BINDINGS, null);
		//	helper.update(FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS, null);
		}
		
		return resource;
	}
	
	public Resource createResource(URI uri) {
		return super.createResource(uri);
	}

	
	protected Resource demandCreateResource(URI uri) {
		return super.demandCreateResource(uri);
	}

	public EList<Resource> getResources() {
		return super.getResources();
	}

//	public Resource createResource(URI uri, String contentType) {
//		return ((FractalTranslatorResourceFactory)getResourceFactoryRegistry().getExtensionToFactoryMap().get("fractal")).createResource(uri);
//	}
	
	
}
