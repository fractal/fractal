package org.ow2.fractal.f4e.fractal.util;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;

public interface IFractalResourceHandler {
	public void handleChangedResources(Collection<Resource> changedResources);
	public void handleRemovedResources(Collection<Resource> removedResources);
	public void handleMovedResources(Map<Resource, URI> movedResources);
}
