/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.util;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.emf.ecore.xmi.XMLHelper;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.EMOFExtendedMetaData;
import org.eclipse.emf.ecore.xmi.impl.XMLSaveImpl;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperComments;

 // TODO: Auto-generated Javadoc
/**
  * The Class FractalAdlExtendedSaveImpl.
  */
 public class FractalSaveImpl extends XMLSaveImpl
 {
   
   /**
    * Instantiates a new fractal adl extended save impl.
    * 
    * @param helper the helper
    */
   public FractalSaveImpl(XMLHelper helper)
   {
     super(helper);
   }

   
   /* (non-Javadoc)
    * @see org.eclipse.emf.ecore.xmi.impl.XMLSaveImpl#saveTypeAttribute(org.eclipse.emf.ecore.EClass)
    */
   protected void saveTypeAttribute(EClass eClass)
   {
     if (eClass != EcorePackage.eINSTANCE.getEAttribute() && eClass != EcorePackage.eINSTANCE.getEReference())
     {
       super.saveTypeAttribute(eClass);
     }
   }
   
 
   /* (non-Javadoc)
    * @see org.eclipse.emf.ecore.xmi.impl.XMLSaveImpl#getDatatypeValue(java.lang.Object, org.eclipse.emf.ecore.EStructuralFeature, boolean)
    */
   protected String getDatatypeValue(Object value, EStructuralFeature f, boolean isAttribute)
   {
     String result = super.getDatatypeValue(value, f, isAttribute);
     if (f == EcorePackage.eINSTANCE.getETypedElement_UpperBound() && "-1".equals(result))
     {
       result = "*";
     }
     return result;
   }

   /**
    * Save extension feature.
    * 
    * @param o the o
    * @param f the f
    */
   protected void saveExtensionFeature(EObject o, EStructuralFeature f)
   {
     doc.startElement(f.getName());
     EDataType eDataType = (EDataType)f.getEType();
     doc.endContentElement(EcoreFactory.eINSTANCE.convertToString(eDataType, o.eGet(f)));
   }

   /* (non-Javadoc)
    * @see org.eclipse.emf.ecore.xmi.impl.XMLSaveImpl#saveContainedMany(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature)
    */
   protected void saveContainedMany(EObject o, EStructuralFeature f)
   {
     if (f == EcorePackage.eINSTANCE.getEModelElement_EAnnotations())
     {
       doc.startElement(EMOFExtendedMetaData.XMI_EXTENSION_ELEMENT);
       doc.addAttribute(EMOFExtendedMetaData.XMI_EXTENDER_ATTRIBUTE, EcorePackage.eNS_URI);
       super.saveContainedMany(o, f);
       doc.endElement();
     }
     else
     {
       super.saveContainedMany(o, f);
     }
   }


/**
 * Save comments.
 * 
 * @param object the object
 */
protected void saveComments(EObject object){
	 IHelperComments helper = HelperAdapterFactory.getInstance().adapt(object);
	 if(helper != null && helper.getComments() != null){
		 Iterator<String> iterator = helper.getComments().iterator();
		 while(iterator.hasNext()){
			 doc.addComment(iterator.next());
			 doc.addLine();
		 }
	}
}
   

protected Object writeTopObject(EObject top) {
	saveComments(top);
	return super.writeTopObject(top);
}



/* (non-Javadoc)
 * @see org.eclipse.emf.ecore.xmi.impl.XMLSaveImpl#writeTopObjects(java.util.List)
 */
public Object writeTopObjects(List contents)
   {
	
    // doc.startElement(XMI_TAG_NS);
     Object mark = doc.mark();

     for (int i = 0, size = contents.size(); i < size; i++)
     {
       EObject top = (EObject)contents.get(i);
       EClass eClass = top.eClass();
       if (eClass == EcorePackage.eINSTANCE.getEAnnotation())
       {
         EAnnotation annotation = (EAnnotation)top;
         if (!annotation.getSource().equals(EMOFExtendedMetaData.EMOF_PACKAGE_NS_URI)) continue;

         doc.startElement(EMOFExtendedMetaData.EMOF_TAG);
         doc.addAttribute(idAttributeName, helper.getID(annotation));
         doc.addAttribute(EMOFExtendedMetaData.EMOF_TAG_NAME, (String)annotation.getDetails().get(EMOFExtendedMetaData.EMOF_TAG_NAME));
         doc.addAttribute(EMOFExtendedMetaData.EMOF_TAG_VALUE, (String)annotation.getDetails().get(EMOFExtendedMetaData.EMOF_TAG_VALUE));

         InternalEList values = (InternalEList)annotation.getReferences();
         if (!values.isEmpty())
         {
           if (sameDocMany(annotation, EcorePackage.eINSTANCE.getEAnnotation_References()) == CROSS_DOC)
           {
             for (Iterator iter = values.basicIterator(); iter.hasNext(); )
             {
               EObject value = (EObject)iter.next();
               doc.startElement(EMOFExtendedMetaData.EMOF_TAG_ELEMENT);
               doc.addAttribute(XMLResource.HREF, helper.getHREF(value));
               doc.endEmptyElement();
             }
           }
           else
           {
             StringBuffer ids = new StringBuffer();
             for (Iterator iter = values.basicIterator();; )
             {
               EObject value = (EObject)iter.next();
               ids.append(helper.getIDREF(value));
               if (!iter.hasNext()) break;
               ids.append(" ");
             }
             doc.addAttribute(EMOFExtendedMetaData.EMOF_TAG_ELEMENT, ids.toString());
           }
         }
         doc.endElement();
       }
       else
       {
         String name = helper.getQName(eClass);
         doc.startElement(name);
         saveElementID(top);
       }
     }

     doc.endElement();
     return mark;
   }



	/* (non-Javadoc)
	 * @see org.eclipse.emf.ecore.xmi.impl.XMLSaveImpl#shouldSaveFeature(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EStructuralFeature)
	 */
	protected boolean shouldSaveFeature(EObject o, EStructuralFeature f) {
		int id = o.eClass().getClassifierID();
		int fid = f.getFeatureID();
		
		if( id == FractalPackage.INTERFACE && fid == FractalPackage.INTERFACE__ROLE){
			return true;
		}
		
		/*
		 * We only want a partial serialization of the model to be conform to
		 * the standard Fractal ADL language. 
		 */
		if( f.getEType() == FractalPackage.Literals.VALUE ||
			f.getEType() == FractalPackage.Literals.CONSTANT ||
			f.getEType() == FractalPackage.Literals.REFERENCE ||
			f.getEType() == FractalPackage.Literals.VALUE_ELEMENT ||
			f.getEType() == FractalPackage.Literals.PARAMETER ||
			f.getEType() == FractalPackage.Literals.FORMAL_PARAMETER ||
			f.getEType() == FractalPackage.Literals.EFFECTIVE_PARAMETER ||
			f.getEType() == FractalPackage.Literals.DEFINITION_CALL){
			return false;
		}
		
		if( id == FractalPackage.ATTRIBUTE && 
				(fid == FractalPackage.ATTRIBUTE__NAME_AST ||
				 fid == FractalPackage.ATTRIBUTE__VALUE_AST)){
			return false;
		}
		
		if(id == FractalPackage.ATTRIBUTES_CONTROLLER &&
				fid == FractalPackage.ATTRIBUTES_CONTROLLER__SIGNATURE_AST){
			return false;
		}
		
		if(id == FractalPackage.CONTROLLER &&
				fid == FractalPackage.CONTROLLER__TYPE_AST){
			return false;
		}
		
		if(id == FractalPackage.CONTENT && 
				fid == FractalPackage.CONTENT__CLASS_AST){
			return false;
		}
		
		if(id == FractalPackage.INTERFACE &&
				(fid == FractalPackage.INTERFACE__SIGNATURE_AST ||
				 fid == FractalPackage.INTERFACE__NAME_AST)){
			return false;
		}
		
		if(id == FractalPackage.BINDING &&
				(fid == FractalPackage.BINDING__CLIENT_AST ||
				 fid == FractalPackage.BINDING__SERVER_AST ||
				 fid == FractalPackage.BINDING__CLIENT_INTERFACE ||
				 fid == FractalPackage.BINDING__SERVER_INTERFACE )){
			return false;
		}
		
		if(id == FractalPackage.COMPONENT && 
				(fid == FractalPackage.COMPONENT__SHARED ||
				 fid == FractalPackage.COMPONENT__NAME_AST ||
				 fid == FractalPackage.COMPONENT__EXTENDS_AST )){
			return false;
		}
		
		if(id == FractalPackage.DEFINITION &&
				(fid == FractalPackage.DEFINITION__ARGUMENTS_AST ||
				 fid == FractalPackage.DEFINITION__EXTENDS_AST ||
				 fid == FractalPackage.DEFINITION__NAME_AST )
		){
			return false;
		}
		return super.shouldSaveFeature(o, f);
	}
	
	protected void saveElement(EObject o, EStructuralFeature f)
	  {
		// save comments
		saveComments(o);
		super.saveElement(o,f);
	  }
 }