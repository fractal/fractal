package org.ow2.fractal.f4e.fractal.adapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.core.commands.operations.IOperationHistoryListener;
import org.eclipse.core.commands.operations.IUndoContext;
import org.eclipse.core.commands.operations.IUndoableOperation;
import org.eclipse.core.commands.operations.OperationHistoryEvent;
import org.eclipse.core.commands.operations.TriggeredOperations;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.workspace.EMFCommandOperation;
import org.eclipse.emf.workspace.IWorkspaceCommandStack;
import org.eclipse.emf.workspace.ResourceUndoContext;
import org.eclipse.emf.workspace.impl.WorkspaceCommandStackImpl;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gmf.runtime.emf.core.util.CrossReferenceAdapter;
import org.ow2.fractal.f4e.fractal.commands.IFractalReloadCommand;
import org.ow2.fractal.f4e.fractal.commands.IFractalSaveCommand;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.util.IDirty;


/**
 * This adapter is used to listen operations done on the resource attached to it.
 * Each resource can have only one OperationHistoryListener.
 * 
 * Each editor which has as main resource the same one that the one attached to this adapter,
 * should register its undoContext with the addUndoContext method.
 * 
 * Each operation done on the resource is added to the undoContext of all editor which work
 * on the same resource.
 * 
 * This adapter manage a dirty state. It is useful for editors working on the same resource.
 * When emf or gmf commands occurs, we set the state 'dirty' to true, it means that the model
 * as been modified and that we can save the resource to save the modifications.
 * 
 * When the resource is saved or reload all informations
 * present in the model are saved, there is no modifications to save, the dirty state is set
 * to false.
 * 
 * @author yann
 *
 */
public class OperationHistoryListenerAdapter extends AdapterImpl implements IOperationHistoryListener, IDirty {
	private ArrayList<IUndoContext> objectsUndoContext = new ArrayList<IUndoContext> ();
	
	private Long lastSaveModificationStamp = null;
	private Long lastOperationModificationStamp = null;
	public OperationHistoryListenerAdapter(){
		super();
		
	}
	
	public void historyNotification(final OperationHistoryEvent event) {
		// We filter only emf and gmf command that affect our Fractal resource
	
		switch(event.getEventType()) {
			case OperationHistoryEvent.DONE:
			case OperationHistoryEvent.UNDONE:
			case OperationHistoryEvent.REDONE:
				Set<Resource> affectedResources = ResourceUndoContext.getAffectedResources(
						event.getOperation());
				// GMF uses triggeredOperations 
				if(event.getOperation() instanceof TriggeredOperations){
					affectedResources = ResourceUndoContext.getAffectedResources(
							((TriggeredOperations)event.getOperation()).getTriggeringOperation());
				}
				
				// We are only interested by command which modify the resource associated to this adapter.
				if (affectedResources.contains(getTarget())) {
					// If the resource is in an archive we do nothing
					if(getTarget().getURI().isArchive()){
						return;
					}
					
					IFile file = WorkspaceSynchronizer.getFile(getTarget());
					lastOperationModificationStamp =  file.getModificationStamp();
					
					
					final IUndoableOperation operation = event.getOperation();
					
					// remove the default undo context so that we can have
					//     independent undo/redo of independent resource changes
//					operation.removeContext(((IWorkspaceCommandStack)
//							 FractalTransactionalEditingDomain.getEditingDomain().getCommandStack()).getDefaultUndoContext());

					// we add the command to all editor's undoContexts associated with
					// the resource associated to this adapter
//					Iterator<IUndoContext> iterator = objectsUndoContext.iterator();
//					while(iterator.hasNext()){
//					//	operation.addContext(iterator.next());
//					}
					
					// Editor working on a Fractal Resource must call FractalSaveCommand when
					// they save the resource.
					// When the resource is saved, we remove the dirty state.
					if(	event.getOperation()!= null && 
						event.getOperation() instanceof EMFCommandOperation &&
						((EMFCommandOperation)event.getOperation()).getCommand() != null &&
						(((EMFCommandOperation)event.getOperation()).getCommand() instanceof IFractalSaveCommand ||
						((EMFCommandOperation)event.getOperation()).getCommand() instanceof  IFractalReloadCommand) ){
						
						lastSaveModificationStamp = file.getModificationStamp();
						setDirty(false);
						
					}else if(event.getOperation()!= null &&
							event.getOperation() instanceof EMFCommandOperation){
						// When a definition extends another,
						// they are linked by an EReference. When
						// a command occurs in the parent definition
						// the extended definition is set as command context, it's the normal EMF strategy.
						// 
						// In our case we want to set the editor in a dirty only if the command
						// affect objects that are in the editor resource.
						if(isResourceAffected((EMFCommandOperation)event.getOperation())){
							lastSaveModificationStamp = file.getModificationStamp();
							setDirty(true);
						}
					}else{
						setDirty(true);
					}
				}
				break;
			}
		}

	
	private boolean isResourceAffected(EMFCommandOperation operation){
		Collection<?> affectedObjects = operation.getCommand().getAffectedObjects();
		Iterator<?> iterator = affectedObjects.iterator();
		while(iterator.hasNext()){
			Object next = iterator.next();
			if(next instanceof EObject){
				if(((EObject)next).eResource() == getTarget()){
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean isAdapterForType(Object type) {
		return type instanceof Class?((Class)type).getName().equals(OperationHistoryListenerAdapter.class.getName()):false;
	}

	public Resource getTarget() {
		// TODO Auto-generated method stub
		return (Resource)super.getTarget();
	}

	public void addUndoContext(IUndoContext undocontext){
		if(!objectsUndoContext.contains(undocontext)){
			objectsUndoContext.add(undocontext);
		}
	}
	
	public void removeUndoContext(IUndoContext undocontext){
		if(objectsUndoContext.contains(undocontext)){
			objectsUndoContext.remove(undocontext);
		}
	}
	
	private boolean dirty = false;

	public boolean isDirty() {
		return dirty;
	}


	public void setDirty(boolean dirty) {
		this.dirty = dirty;
	}

	
	public long lastOperationModificationStamp(){
		return lastOperationModificationStamp;
	}
	
	public boolean isExternalLastChange(){
		boolean result = true;
		
		IFile file = WorkspaceSynchronizer.getFile(getTarget());
		long stamp =  file.getModificationStamp();
		if(lastOperationModificationStamp != null){
			result = stamp != lastOperationModificationStamp;
		}else{
			lastOperationModificationStamp = stamp;
		}
		
		return result;
	}
}
