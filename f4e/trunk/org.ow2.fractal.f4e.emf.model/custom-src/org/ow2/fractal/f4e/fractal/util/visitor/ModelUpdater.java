/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.util.visitor;

import org.eclipse.emf.ecore.EObject;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.AbstractComponentHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.IAttributeHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IAttributesControllerHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IBindingHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IComponentHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IContentHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IControllerHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IDefinitionHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IInterfaceHelper;

public class ModelUpdater implements Visitor{
	
	/* (non-Javadoc)
	 * @see org.ow2.fractal.adl.extended.fractaladlextended.util.visitor.Visitor#visit(org.eclipse.emf.ecore.EObject)
	 */
	public void visit(EObject eObject){
		//java.lang.System.out.println(eObject);
			
		int id = eObject.eClass().getClassifierID();
		switch(id){
			case FractalPackage.SYSTEM:
				
				break;
			
			case FractalPackage.DEFINITION:{
			
				IDefinitionHelper helper = (IDefinitionHelper)HelperAdapterFactory.getInstance().adapt(eObject);
				if(helper != null){
					helper.update(FractalPackage.DEFINITION__NAME_AST, null);
					helper.update(FractalPackage.DEFINITION__ARGUMENTS_AST, null);
					helper.update(FractalPackage.DEFINITION__EXTENDS_AST, null);
					for(DefinitionCall dc: ((Definition)eObject).getExtendsAST()){
						visit(dc);
					}
					
					for(Component c: ((Definition)eObject).getSubComponents()){
						visit(c);
					}
				}
			}break;
			
			case FractalPackage.COMPONENT:{
				IComponentHelper helper = (IComponentHelper)HelperAdapterFactory.getInstance().adapt(eObject);
				if(helper != null){
					helper.update(FractalPackage.COMPONENT__NAME_AST, null);
					helper.update(FractalPackage.COMPONENT__EXTENDS_AST, null);
					for(DefinitionCall dc: ((Component)eObject).getExtendsAST()){
						visit(dc);
					}
					
					for(Component c: ((Component)eObject).getSubComponents()){
						visit(c);
					}
				}
			}break;
			
			case FractalPackage.INTERFACE:{
				IInterfaceHelper helper = (IInterfaceHelper)HelperAdapterFactory.getInstance().adapt(eObject);
				if(helper != null){
					helper.update(FractalPackage.INTERFACE__NAME_AST, null);
					helper.update(FractalPackage.INTERFACE__SIGNATURE_AST, null);
				}
			}break;
			
			case FractalPackage.CONTENT:{
				IContentHelper helper = (IContentHelper)HelperAdapterFactory.getInstance().adapt(eObject);
				if(helper != null){
					helper.update(FractalPackage.CONTENT__CLASS_AST, null);
				}
			}break;
			
			case FractalPackage.CONTROLLER:{
				IControllerHelper helper = (IControllerHelper)HelperAdapterFactory.getInstance().adapt(eObject);
				if(helper != null){
					helper.update(FractalPackage.CONTROLLER__TYPE_AST, null);
				}
			}break;
			
			case FractalPackage.ATTRIBUTES_CONTROLLER:{
				IAttributesControllerHelper helper = (IAttributesControllerHelper)HelperAdapterFactory.getInstance().adapt(eObject);
				if(helper != null){
					helper.update(FractalPackage.ATTRIBUTES_CONTROLLER__SIGNATURE_AST, null);
				}
			}break;
			
			case FractalPackage.ATTRIBUTE:{
				IAttributeHelper helper = (IAttributeHelper)HelperAdapterFactory.getInstance().adapt(eObject);
				if(helper != null){
					helper.update(FractalPackage.ATTRIBUTE__NAME_AST, null);
					helper.update(FractalPackage.ATTRIBUTE__VALUE_AST, null);
				}
			}break;
			
			case FractalPackage.BINDING:{
				IBindingHelper helper = (IBindingHelper)HelperAdapterFactory.getInstance().adapt(eObject);
				if(helper != null){
					helper.update(FractalPackage.BINDING__CLIENT_AST,null);
					helper.update(FractalPackage.BINDING__SERVER_AST, null);
				}
			}break;
			
			case FractalPackage.DEFINITION_CALL:{
				Definition definition = ((DefinitionCall)eObject).getDefinition();
				if(definition!=null){
					visit(definition);
				}
			}break;
		}
	}
}
