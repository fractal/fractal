package org.ow2.fractal.f4e.fractal.adapter.helper.marker;

import org.eclipse.emf.common.util.Diagnostic;

public interface IMarkerHelper {
	public boolean hasErrors();
	public void setHasErrors(boolean hasErrors);
	public boolean hasWarnings();
	public void setHasWarnings(boolean hasWarnings);
	
	public void setDiagnostic(Diagnostic diagnostic);
	public Diagnostic getDiagnostic();
}
