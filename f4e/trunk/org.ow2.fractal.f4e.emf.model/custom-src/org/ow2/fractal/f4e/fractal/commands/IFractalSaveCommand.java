package org.ow2.fractal.f4e.fractal.commands;

public interface IFractalSaveCommand extends IFractalCommand{
	public void save();
}
