/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.transaction.RecordingCommand;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.Cardinality;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.Constant;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.FormalParameter;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.Reference;
import org.ow2.fractal.f4e.fractal.Role;
import org.ow2.fractal.f4e.fractal.ValueElement;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.plugin.FractalPlugin;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;



// TODO: Auto-generated Javadoc
/**
 * The Class AbstractComponentHelperAdapter.
 */
/**
 * @author Yann Davin
 *
 * @param <T>
 */
public class AbstractComponentHelperAdapter<T extends AbstractComponent> extends HelperAdapter<T>
implements IAbstractComponentHelper<T>{
	protected Map<DefinitionCall,IAbstractComponentListener> parentsListeners = new HashMap<DefinitionCall, IAbstractComponentListener>();
	
	/**
	 * Instantiates a new abstract component helper adapter.
	 * 
	 * @param abstractComponent the abstract component
	 */
	public AbstractComponentHelperAdapter(T t) {
		super(t);
			this.tempMergedFeatures.put(FractalPackage.ABSTRACT_COMPONENT__INTERFACES, new ArrayList<EObject>());
		
			this.tempMergedFeatures.put(FractalPackage.ABSTRACT_COMPONENT__BINDINGS, new ArrayList<EObject>());
		
			this.tempMergedFeatures.put(FractalPackage.ABSTRACT_COMPONENT__SUB_COMPONENTS, new ArrayList<EObject>());
			
			this.tempMergedFeatures.put(FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER, new ArrayList<EObject>());
	}

	/* (non-Javadoc)
	 * @see org.ow2.fractal.adl.extended.fractalextended.adapter.helper.HelperAdapter#isAdapterForType(java.lang.Object)
	 */
	
	public boolean isAdapterForType(Object type) {
		return super.isAdapterForType(type) || type == IAbstractComponentHelper.class;
	}

	/**
	 * @return the definition which holds the abstractComponent 't'
	 */
	public Definition getParentDefinition(){
		return (Definition)getParent(t);
	}

	public List<Interface> getInterfaces(Role role){
		List<Interface> result = new ArrayList<Interface>();
		Iterator<Interface> iterator = t.getInterfaces().iterator();
		while(iterator.hasNext()){
			Interface itf = iterator.next();
			if(itf.getRole().equals(role)){
				result.add(itf);
			}
		}
		
		iterator = t.getMergedInterfaces().iterator();
		while(iterator.hasNext()){
			Interface itf = iterator.next();
			if(itf.getRole().equals(role)){
				result.add(itf);
			}
		}
		
		return result;
	}
	
	
	/**
	 * Return the top definition that holds the given
	 * abstractComponent.
	 * 
	 * @param abstractComponent the abstract component
	 * 
	 * @return the parent
	 */
	protected AbstractComponent getParent(AbstractComponent abstractComponent){
		if(abstractComponent instanceof Definition){
			return abstractComponent;
		}else if(abstractComponent instanceof Component){
			return getParent((AbstractComponent)((Component)abstractComponent).eContainer());
		}else{
			return null; //There is only two Types which extends abstractComponent
		}
	}
	
	
	/**
	 * Return the interface of name interfaceName is it exists in the
	 * interfaces or merged interfaces set.
	 * 
	 * If the interface name ends with numbers we search for a interface
	 * with cardinality set to COLLECTION.
	 */
	public Interface getInterface(String interfaceName){
		Iterator<Interface> iterator = t.getInterfaces().iterator();
		
		// we search the interface in the interfaces.
		while(iterator.hasNext()){
			Interface interface_ = iterator.next();
			if(interface_.getName() != null && interface_.getName().equals(interfaceName)){
				return interface_;
			}else if(interface_.getCardinality() != null && interface_.getCardinality().equals(Cardinality.COLLECTION) &&
					interfaceName.matches(".*[^\\d][0-9]+$")){
				return interface_;
			}
		}
		
		// We search into the inherited merged interfaces.
		iterator = t.getMergedInterfaces().iterator();
		while(iterator.hasNext()){
			Interface interface_ = iterator.next();
			if(interface_.getName() != null && interface_.getMergedName().equals(interfaceName)){
				return interface_;
			}else if(interface_.getCardinality() != null && interface_.getCardinality().equals(Cardinality.COLLECTION) &&
					interfaceName.matches(".*[^\\d][0-9]+$")){
				return interface_;
			}
		}
		
		return null;
	}
	
	/**
	 * Gets the interface identified by the AST resulted from the binding client or server parsing.
	 * @param elements
	 */
	public Interface getInterface(Vector<ValueElement> elements){
		
		Interface result = null;
		
		if(!elements.isEmpty()){
			ValueElement elem = elements.firstElement();
		
			String value = elem instanceof Constant ? ((Constant)elem).getConstant() : "${" + ((Reference)elem).getName() + "}";
			
			
				if(elements.firstElement() == elements.lastElement()){
					Interface interface_ = t.getHelper().getInterface(value);
					result =  interface_;
				}else{
					Component subComponent = t.getHelper().getSubComponent(value);
					Vector<ValueElement> elementsCopy = new Vector<ValueElement>(elements);
				
					if(subComponent != null){
						//remove the first element ex: c1.itf1 we remove c1
						elementsCopy.remove(elementsCopy.firstElement());
						//now we remove the '.'
						elementsCopy.remove(elementsCopy.firstElement());
					
						result = subComponent.getHelper().getInterface(elementsCopy);
					}
			}
		}
		
		return result;
	}
	
	/**
	 * Return the component of name componentName if its exists in the
	 * sub-components or merged sub-components set.
	 */
	public Component getSubComponent(String componentName){
		Iterator<Component> iterator = t.getSubComponents().iterator();
		
		while(iterator.hasNext()){
			Component component = iterator.next();
			if(component.getName() != null && component.getName().equals(componentName)){
				return component;
			}
		}
		
		iterator = t.getMergedSubComponents().iterator();
		
		while(iterator.hasNext()){
			Component component = iterator.next();
			if(component.getName() != null && component.getMergedName().equals(componentName)){
				return component;
			}
		}
		return null;
	}
	
	/**
	 * Gets the sub-component identified by the name 'name'.
	 * 
	 * @param abstractComponent the abstract component
	 * @param name
	 * 
	 * @return the sub-component
	 */
	@SuppressWarnings("unused")
	static private Component getSubComponent(AbstractComponent abstractComponent, String name){
		Component resultComponent = null;
		Iterator<Component> iterator = abstractComponent.getSubComponents().iterator();
		
		while(iterator.hasNext()){
			Component subComponent = iterator.next();
			
			if(subComponent != null && subComponent.getName().equals(name)){
				return subComponent;
			}
		}
		
		return resultComponent;
	}
	
	/**
	 * Return the sub-component identified by the 'componentPath',
	 * the componentPath is just a list of components.
	 * 
	 * For example if the componentPath has two components named 'c1' and 'c2',
	 * we will search in the current component the sub-component  c1,
	 * then in c1 the sub-component c2 and we return c2.
	 * 
	 * @param componentPath
	 * @return
	 */
	public Component getSubComponent(List<Component> componentPath, HashMap<String,String> effectiveParameters){
		Component result = null;
		if(componentPath.size()>0){
			int i = 0;
			Component component = getComponentWithSameName(t.getSubComponents(), componentPath.get(i), effectiveParameters);
			if(component != null){
					if(componentPath.size()==i+1){
						result = component;
					}else{
						result = component.getHelper().getSubComponent(componentPath.subList(i+1, componentPath.size()), effectiveParameters);
					}
			}
		}
		return result;
	}
	
	/**
	 * @return all interfaces. Interfaces returned are not merged. For example if a definition D1 has a interface Itf1
	 * and extended a definition D2 which also has a interface Itf1 we return the interface of D1. But the returned interface
	 * is not merged with the Itf1 of the definition D2.
	 */
	public List<Interface> getAllInterfaces(){
		return getAllInterfaces(t,null);
	}
	
	private boolean isContained(List<? extends EObject> eObjects, String objectName){
		boolean result = false;
		Iterator<? extends EObject> iterator = eObjects.iterator();
		
		while(iterator.hasNext()){
			EObject next = iterator.next();
			Object nameADL = next.eGet(next.eClass().getEStructuralFeature("name"));
			if(nameADL != null && nameADL instanceof String && ((String)nameADL).endsWith(objectName)){
				result = true;
				break;
			}
		}
		return result;
	}
	
	public List<Interface> getAllInterfaces(AbstractComponent abstractComponent,List<Interface> interfaces){
		List<Interface> interfacesList = new ArrayList<Interface>();
		
		Iterator<Interface> iterator = abstractComponent.getInterfaces().iterator();
		
		// we first add the interfaces that belongs to the abstract component.
		while(iterator.hasNext()){
			Interface interface_ = iterator.next();
			
			// interface is added only if it isn't extended by the child abstract component.
			if(interfaces == null || (interfaces!=null && !isContained(interfaces,interface_.getName()))){
					interfacesList.add(interface_);
			}
		}
		
		EList<DefinitionCall> definitionCallList = abstractComponent.getExtendsAST();
		Iterator<DefinitionCall> iteratorDefinitionCall = definitionCallList.iterator();
		
		// we add all interfaces of inherited from parent definitions.
		while(iteratorDefinitionCall.hasNext()){
			DefinitionCall definitionCall = iteratorDefinitionCall.next();
			AbstractComponent definition = (AbstractComponent)definitionCall.getDefinition();
			if(definitionCall != null && definition != null){
				interfacesList.addAll(getAllInterfaces(definitionCall.getDefinition(),interfacesList));
			}
		}
		
		// if it is a component we add interfaces of inherited definition component.
		Definition definition = abstractComponent.getHelper().getParentDefinition();
		if(definition != abstractComponent){
			
		}
		
		return interfacesList;
	}
	
	
	public List<Component> getAllSubComponents(){
		return null;
	}
	
	/**
	 * Merge. Return (list1) - (list1 intersection list2)
	 * 
	 * @param list1 the list1
	 * @param list2 the list2
	 * 
	 * @return the list< string>
	 */
	static public List<String> merge(List<String> list1, List<String> list2){
		List<String> result = new ArrayList<String>();
		result.addAll(list2);
		
		Iterator<String> it = list1.iterator();
		while(it.hasNext()){
			String string = it.next();
			if(!result.contains(string)){
				result.add(string);
			}
		}
		return result;
	}
	
	
	private boolean shouldUpdateExtends = false;
	protected boolean getShouldUpdateExtends(){
		return shouldUpdateExtends;
	}
	
	protected void setShouldUpdateExtends(boolean b){
		shouldUpdateExtends = b;
	}
	
	/**
	 * We process notifications to automatically calculate the merge,
	 * and update dependency between the definition/component.
	 * The method was removed (commented) due to performance issue.
	 */
	public void notifyChanged(Notification notification) {
		super.notifyChanged(notification);
//		if(!(notification.getNotifier() instanceof AbstractComponent)){
//			return;
//		}
//		
//		if(notification.getNotifier() == t && (t.eContainmentFeature() != FractalPackage.Literals.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS ) && t.eContainer() != null){
//			int type = notification.getEventType();
//			int featureID = notification.getFeatureID(AbstractComponent.class);
//			
//			// Merged feature are updated when non merged feature are modified,
//			// so we don't want to update merged feature when merged feature are modified.
//			if(isMergedFeature(featureID)){
//				return;
//			}
//			if(notification.getNotifier() == t){
//				switch(featureID){
//					
//					// When we extends a definition, we add a listener that
//					// will update merged interfaces when a interface is added/removed/modified on the parent definition. 
//					case FractalPackage.ABSTRACT_COMPONENT__EXTENDS_AST:
//						Object object = null;
//						switch(type){
//							case Notification.ADD:
//								object = notification.getNewValue();
//								if(object instanceof DefinitionCall && ((DefinitionCall)object).getDefinition() != null){
//									IAbstractComponentListener listener = new MergerAdapter();
//									parentsListeners.put(((DefinitionCall)object), listener);
//									((DefinitionCall)object).getDefinition().eAdapters().add(listener);
//								
//								//	updateMerge();
//								//	updateChildrenMerge();
//								}
//								
//								
//								break;
//							case Notification.ADD_MANY:
//								object =notification.getNewValue();
//								if(object instanceof EList && object != null){
//									Iterator<?> iterator = ((EList<?>)object).iterator();
//									while(iterator.hasNext()){
//										DefinitionCall definitionCall = (DefinitionCall)iterator.next();
//										if(definitionCall.getDefinition() != null){
//											IAbstractComponentListener listener = new MergerAdapter();
//											parentsListeners.put(((DefinitionCall)definitionCall), listener);
//											((DefinitionCall)definitionCall).getDefinition().eAdapters().add(listener);
//										}
//									}
//									
//								//	updateMerge();
//								//	updateChildrenMerge();
//								}
//								break;
//							case Notification.REMOVE:
//								object = notification.getOldValue();
//								if(object instanceof DefinitionCall && ((DefinitionCall)object).getDefinition() != null){
//									IAbstractComponentListener listener = parentsListeners.get(((DefinitionCall)object));
//									((DefinitionCall)object).getDefinition().eAdapters().remove(listener);
//									
//								//	updateMerge();
//								////	updateChildrenMerge();
//								}
//								break;
//							case Notification.REMOVE_MANY:
//								object = notification.getOldValue();
//								if(object instanceof EList && object != null){
//									Iterator<?> iterator = ((EList<?>)object).iterator();
//									while(iterator.hasNext()){
//										DefinitionCall definitionCall = (DefinitionCall)iterator.next();
//										if(definitionCall.getDefinition() != null){
//											IAbstractComponentListener listener = parentsListeners.get(((DefinitionCall)definitionCall));
//											((DefinitionCall)definitionCall).getDefinition().eAdapters().remove(listener);
//										}
//									}
//									
//								//	updateMerge();
//								//	updateChildrenMerge();
//								}
//								break;
//						}
//						
//					switch(type){
//					case Notification.ADD:
//					case Notification.ADD_MANY:
//					case Notification.REMOVE:
//					case Notification.REMOVE_MANY:
//						// If components are added or removed we have to update the merged components of the current component/definition,
//						// and merged components of components/definitions that extend the enclosing definition of the modified component/definition
//						
//						Definition definition = this.getParentDefinition();
//						if(definition != null && definition != t){
//						//	definition.getHelper().update(FractalPackage.ABSTRACT_COMPONENT__BINDINGS);
//						//	definition.getHelper().update(FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS);
//						}
//					
//						break;
//						
//					}
//					break;
//					
//					case FractalPackage.ABSTRACT_COMPONENT__INTERFACES:
//						switch(type){
//							case Notification.ADD:
//							case Notification.REMOVE:
//							//	updateMerge();
//							//	updateChildrenMerge();
//								
//								// If we add a interface in a component we have to update the children
//								// of the parent definition.
//								Definition definition = getParentDefinition();
//								if(definition != getTarget()){
//							//		definition.getHelper().updateChildren(FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS);
//								}
//								break;
//						
//						}
//					break;
//					
//					case FractalPackage.ABSTRACT_COMPONENT__NAME:
//						switch(type){
//							case Notification.SET:
//								updateName();
//								// If the name of a component changes we have to update the merged components of the component definitions,
//								// and update all merged components of definitions or components that extend the modified component parent definition.
//								Definition definition = this.getParentDefinition();
//								if(definition != null){
//							 //   		update(FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS);
//							 //   		definition.getHelper().update(FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS);
//							 //   		definition.getHelper().updateChildren(FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS);
//							 //   		definition.getHelper().update(FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS);
//							//    		definition.getHelper().updateChildren(FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS);
//								}
//								break;
//						}
//					break;
//					
//					case FractalPackage.ABSTRACT_COMPONENT__NAME_AST:
//						switch(type){
//						case Notification.SET:
//							Definition definition = this.getParentDefinition();
//							if(definition != null){
//							//	update(FractalPackage.ABSTRACT_COMPONENT__BINDINGS);
//							//	definition.getHelper().update(FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS);
//							//	definition.getHelper().updateChildren(FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS);
//							//	definition.getHelper().update(FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS);
//							//	definition.getHelper().updateChildren(FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS);
//							}
//							break;
//						}
//						break;
//					
//					case FractalPackage.ABSTRACT_COMPONENT__SUB_COMPONENTS:
//						switch(type){
//							case Notification.ADD:
//							case Notification.ADD_MANY:
//							case Notification.REMOVE:
//							case Notification.REMOVE_MANY:
//								// If components are added or removed we have to update the merged components of the current component/definition,
//								// and merged components of components/definitions that extend the enclosing definition of the modified component/definition
//							//	update(FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS);
//								Definition definition = this.getParentDefinition();
//								if(definition != null){
//							//		update(FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS);
//						    //		definition.getHelper().update(FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS);
//						    //		definition.getHelper().updateChildren(FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS);
//						    //		definition.getHelper().update(FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS);
//						    //		definition.getHelper().updateChildren(FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS);
//						    		
//								}
//							break;
//						}
//					break;
//					
//					case FractalPackage.ABSTRACT_COMPONENT__BINDINGS:
//						switch(type){
//							case Notification.ADD:
//							case Notification.ADD_MANY:
//							case Notification.REMOVE:
//							case Notification.REMOVE_MANY:
//							//	update(FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS);
//							//	updateChildren(FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS);
//						}
//						break;
//				
//					default:
//						if(hasMergedFeature(featureID)){
//							//update(getMergedFeatureID(featureID));
//							//updateChildren(getMergedFeatureID(featureID));
//						}
//				}
//			}
//		}
	}
	
	
	/**
	 * When a abstract component extends an definition we add a MergerAdapter to the 
	 * parent definition.
	 * When a event occurs on the parent definition which can have an impact 
	 * on the merged elements of the abstract component, the update methods of the MergedAdapter
	 * are called.
	 *
	 */
	public class MergerAdapter extends AdapterImpl implements IAbstractComponentListener{
		public IHelper<?> getHelper(){
			return AbstractComponentHelperAdapter.this;
		}
		
		public boolean isAdapterForType(Object type) {
			return type == IAbstractComponentListener.class;
		}

		public void update(final int featureID){
			//AbstractComponentHelperAdapter.this.update(featureID);
		}
		
		public void updateChildren(final int featureID){
			//AbstractComponentHelperAdapter.this.updateChildren(featureID);
		}
		
		public void updateMerge(){
			// A parent has changed we have to update the parent nodes
			// before the merge update
			cachedParentNodes = null;
			//AbstractComponentHelperAdapter.this.updateMerge();
		}
		
		public void updateChildrenMerge(){
			//AbstractComponentHelperAdapter.this.updateChildrenMerge();
		}
	};

	/**
	 * Functor performing modifications on the model without modifications on the related resource
	 */
	private class Update implements java.lang.Runnable{
		public void run(){
			boolean state = false;
			//Updating merge informations should not modify the resource
			if(t.eResource() != null && t.eResource().isModified() == false){
				state = t.eResource().isModified();
			}
			
			update();
			
			if(t.eResource() != null){
				t.eResource().setModified(state);
			}
		}
		
		protected void update(){
			
		}
	};
	
	
	private class UpdateChildren implements java.lang.Runnable{
		public void run(){
			boolean state = false;
			//Modification on merge interfaces does not modify the resource
			if(t.eResource() != null && t.eResource().isModified() == false){
				state = t.eResource().isModified();
			}

			Iterator<Adapter> iterator = t.eAdapters().iterator();
			while(iterator.hasNext()){
				Adapter adapter = iterator.next();
				if(adapter instanceof IAbstractComponentListener){
					this.update((IAbstractComponentListener)adapter);
				}
			}

			if(t.eResource() != null){
				t.eResource().setModified(state);
			}
		}
		
		protected void update(IAbstractComponentListener adapter){
			
		}
	}
	
	
	protected Component getComponentWithSameName(List<Component> listComponents,Component component, HashMap<String,String> effectiveParameters ){
		Component result = null;
		if(listComponents == null){
			return result;
		}
		
		Iterator<Component> iterator = listComponents.iterator();
		
		while(iterator.hasNext()){
			Component nextComponent = iterator.next();
			if(nextComponent.getNameAST() != null && nextComponent.getNameAST().getHelper().resolve(effectiveParameters).equals(component.getName())){
				result = nextComponent;
				break;
			} else
			if(nextComponent.getName() != null && nextComponent.getName().equals(component.getName())){
				result = nextComponent;
				break;
			}
		}
		
		return result;
	}
	
	protected void updateName(){
		t.setNameAST(ValueHelperAdapter.getValue(t.getName(), t));
	}
	
	// When merged interfaces or interfaces are modified we have to update bindings.
	protected void updateBindings(final IProgressMonitor monitor){
		boolean state = false;
		if(t.eResource() != null && t.eResource().isModified() == false){
			state = t.eResource().isModified();
		}
		
		Iterator<Binding> iterator = t.getBindings().iterator();
		while(iterator.hasNext()){
			final Binding binding = iterator.next();
				Command cmd = new RecordingCommand(FractalTransactionalEditingDomain.getEditingDomain()) {
			        protected void doExecute(){
			        	binding.getHelper().update(FractalPackage.BINDING__CLIENT_AST, monitor);
			        	binding.getHelper().update(FractalPackage.BINDING__SERVER_AST, monitor);
			        } 
			    };
			 FractalTransactionalEditingDomain.getEditingDomain().getCommandStack().execute(cmd); 
				
			}
		
		if(t.eResource() != null){
			t.eResource().setModified(state);
		}
	}
	
	protected void updateChildrenBindings(){
		boolean state = false;
		if(t.eResource() != null && t.eResource().isModified() == false){
			state = t.eResource().isModified();
		}
		
		Iterator<Adapter> iterator = t.eAdapters().iterator();
		while(iterator.hasNext()){
			Adapter adapter = iterator.next();
			if(adapter instanceof IAbstractComponentListener){
				((IAbstractComponentListener)adapter).update(FractalPackage.ABSTRACT_COMPONENT__BINDINGS);
				((IAbstractComponentListener)adapter).updateChildren(FractalPackage.ABSTRACT_COMPONENT__BINDINGS);
			}
		}
		
		if(t.eResource() != null){
			t.eResource().setModified(state);
		}
	}
	
	protected void updateChildrenMergedInterfaces(){
		boolean state = false;
		//Modification on merged interfaces does not modify the resource
		if(t.eResource() != null && t.eResource().isModified() == false){
			state = t.eResource().isModified();
		}
		
		Iterator<Adapter> iterator = t.eAdapters().iterator();
		while(iterator.hasNext()){
			Adapter adapter = iterator.next();
			
			if(adapter instanceof IAbstractComponentListener){
				((IAbstractComponentListener)adapter).update(FractalPackage.ABSTRACT_COMPONENT__MERGED_INTERFACES);
				((IAbstractComponentListener)adapter).updateChildren(FractalPackage.ABSTRACT_COMPONENT__MERGED_INTERFACES);
			}
		}
		
		if(t.eResource() != null){
			t.eResource().setModified(state);
		}
	}
	
	
	protected void updateChildrenMergedBindings(){
		boolean state = false;
		//Modification on merged bindingsd does not modify the resource
		if(t.eResource() != null && t.eResource().isModified() == false){
			state = t.eResource().isModified();
		}
		
		Iterator<Adapter> iterator = t.eAdapters().iterator();
		while(iterator.hasNext()){
			Adapter adapter = iterator.next();
			if(adapter instanceof IAbstractComponentListener){
				((IAbstractComponentListener)adapter).update(FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS);
				((IAbstractComponentListener)adapter).updateChildren(FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS);
			}
		}
		
		if(t.eResource() != null){
			t.eResource().setModified(state);
		}
	}
	
	protected void updateChildrenMergedComponents(){
		boolean state = false;
		//Modification on merged components does not modify the resource
		if(t.eResource() != null && t.eResource().isModified() == false){
			state = t.eResource().isModified();
		}
		
		Iterator<Adapter> iterator = t.eAdapters().iterator();
		while(iterator.hasNext()){
			Adapter adapter = iterator.next();
			if(adapter instanceof IAbstractComponentListener){
				((IAbstractComponentListener)adapter).update(FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS);
				((IAbstractComponentListener)adapter).update(FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER);
				((IAbstractComponentListener)adapter).update(FractalPackage.ABSTRACT_COMPONENT__BINDINGS);
				((IAbstractComponentListener)adapter).updateChildren(FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS);
			}
		}
		
		if(t.eResource() != null){
			t.eResource().setModified(state);
		}
	}
	
	protected void cleanMergedFeatures(List<? extends EObject> createdMergedFeatures, EStructuralFeature feature){
		synchronized (((EList<?>)t.eGet(feature))) {
		// After a merging, we have to clean previous merged elements.
		// but we have to keep previous merged elements which are still present on the new merge.
		// for example if previous merged component were C1,C2
		// if the new merge contains only C1, we want to suppress only C2.
		// We can't clean all previous merged (C1, C2) elements and adding C1
		// because the side effect will be to destroy the C1 graphical element,
		// and a new one will be created at a different position.
		Iterator<?> iterator = ((EList<?>)t.eGet(feature)).iterator();
		List<EObject> objectsToRemove = new ArrayList<EObject>();
		while(iterator.hasNext()){
			EObject next = (EObject)iterator.next();
			if(!createdMergedFeatures.contains(next)){
				objectsToRemove.add(next);
			}
		}
		((EList<?>)t.eGet(feature)).removeAll(objectsToRemove);
		}
	}
	
	private static  List<Component> getComponentPath(EObject eObject, List<Component> list){
		if(list == null){
			list = new ArrayList<Component>();
		}
		
		if(eObject.eContainer() != null && eObject.eContainer() instanceof Component){
			getComponentPath((Component)eObject.eContainer(),list);
		}
		
		if(eObject instanceof Component){
			list.add((Component)eObject);
		}
		
		return list;
	}
	
	public  List<Component> getPath(){
		return getComponentPath(getObject(),null);
	}
	
	
	protected List<Node<? extends AbstractComponent>> cachedParentNodes = null;
	
	/**
	 * Search all inherited components with same name by looking inside
	 * all parents definitions of the component container.
	 * @param component
	 * @return
	 */
	public List<Node<? extends AbstractComponent>> getNodesToMerge(HashMap<String, String> effectiveParameters){
		if(cachedParentNodes != null){
			return cachedParentNodes;
		}else{
			return getNodesToMerge(new ArrayList<Component>(), effectiveParameters);
		}
	}
		
	/**
	 * Return abstract components from which the current abstract component reference by this.t can inherit elements.
	 * @param componentPath
	 * @return
	 */
	protected List<Node<? extends AbstractComponent>> getNodesToMerge(List<Component> componentPath, HashMap<String,String> effectiveParameters){
		
		List<Node<? extends AbstractComponent>> result = new ArrayList<Node<? extends AbstractComponent>>();
		
		
		if(t instanceof Component){
			Component c = (Component)t;
			if(((Component) t).getShared() == null){
				AbstractComponent eContainer = (AbstractComponent)t.eContainer();
				AbstractComponentHelperAdapter<?> componentHelper = (AbstractComponentHelperAdapter<?>)HelperAdapterFactory.getInstance().adapt(eContainer);
				if(componentHelper!=null ){
					ArrayList<Component> newComponentPath = new ArrayList<Component>();
					newComponentPath.add((Component)t);
					newComponentPath.addAll(componentPath);
					result.addAll(componentHelper.getNodesToMerge(newComponentPath,effectiveParameters));
				}
			}else{
				// If the component is a reference to a other component (shared component)
				// We process the component as component which extends the referenced component and all its parents
				if(componentPath!=null && componentPath.isEmpty()){
					result.add(new Node<Component>(((Component)t).getShared(),effectiveParameters));
					result.addAll(((AbstractComponentHelperAdapter)((Component)t).getShared().getHelper()).getNodesToMerge(effectiveParameters));
				}
				// If we try to merge a subcomponent of a shared component we first need to get
				// the related subcomponent of the referenced component, after we process the subcomponent
				// as a component which extends the referenced subComponent and all its parents
				else if(componentPath!=null && !componentPath.isEmpty()){
					Component subComponentInSharedComponent =  ((AbstractComponentHelperAdapter)((Component)t).getShared().getHelper()).getSubComponent(componentPath, effectiveParameters);
					if(subComponentInSharedComponent != null){
						result.add(new Node<Component>(subComponentInSharedComponent,effectiveParameters));
						IComponentHelper helper = subComponentInSharedComponent.getHelper();
						if(helper != null){
							result.addAll(((ComponentHelperAdapter)helper).getNodesToMerge(effectiveParameters));
						}
					}
				}
			}
		}
			
			EList<DefinitionCall> definitionCalls = t.getExtendsAST();
			for(int i=definitionCalls.size()-1;i>=0;i--){
				DefinitionCall definitionCall = definitionCalls.get(i);
				if(definitionCall.getDefinition() != null){
					DefinitionHelperAdapter definitionHelper = (DefinitionHelperAdapter)HelperAdapterFactory.getInstance().adapt(definitionCall.getDefinition());
					
					ArrayList<Component> newComponentPath = new ArrayList<Component>();
					//newComponentPath.add((Component)t);
					newComponentPath.addAll(componentPath);
					
					if(newComponentPath.size()>0){
						for(int j=0;j<newComponentPath.size();j++){
							Component subComponent = null;
							if(j<newComponentPath.size() -1){
								subComponent = definitionHelper.getSubComponent(newComponentPath.subList(0, j+1), definitionCall.getHelper().getResolvedParameters(effectiveParameters));
							}else{
								subComponent =  definitionHelper.getSubComponent(newComponentPath,definitionCall.getHelper().getResolvedParameters(effectiveParameters));
							}
							if(subComponent != null){
								ComponentHelperAdapter subComponentHelper = (ComponentHelperAdapter)HelperAdapterFactory.getInstance().adapt(subComponent);
								result.addAll(subComponentHelper.getNodesToMerge(componentPath.subList(j+1, componentPath.size()), definitionCall.getHelper().getResolvedParameters(effectiveParameters)));
								if(j>=newComponentPath.size()-1){
									result.add(new Node<Component>(subComponent, definitionCall.getHelper().getResolvedParameters(effectiveParameters)));
								}
							}
						}
						result.addAll(definitionHelper.getNodesToMerge(newComponentPath,definitionCall.getHelper().getResolvedParameters(effectiveParameters)));
					}else{
						if(definitionCall.getDefinition() != null){
							DefinitionHelperAdapter parentDefinitionHelper = (DefinitionHelperAdapter)HelperAdapterFactory.getInstance().adapt(definitionCall.getDefinition());
							result.add(new Node<Definition>(definitionCall.getDefinition(),definitionCall.getHelper().getResolvedParameters(effectiveParameters)));
							result.addAll(parentDefinitionHelper.getNodesToMerge(definitionCall.getHelper().getResolvedParameters(effectiveParameters)));
						}
					}
				}
			}
		return result;
	}

	public HashMap<String,String> getResolvedFormalParameters(HashMap<String,String> effectiveParameters){
		HashMap<String,String> result = new HashMap<String,String>();
		Definition parentDefinition = t.getHelper().getParentDefinition();
		if(parentDefinition != null){
			Iterator<FormalParameter> iterator = parentDefinition.getArgumentsAST().iterator();
			while(iterator.hasNext()){
				FormalParameter next = iterator.next();
				String name = next.getName().getHelper().resolve(effectiveParameters);
				String value = "";
				if(next.getValue() != null){
					 value = next.getValue().getHelper().resolve(effectiveParameters);
				}else{
					if(effectiveParameters.get(name) != null){
						value = effectiveParameters.get(name);
					}
				}
				result.put(name,value);
			}
		}
		return result;
	}
	
	

	
	protected void mergeFeature(HashMap<String,String> effectiveParameters, int featureIDs[], IProgressMonitor monitor){	
		List<Node<? extends AbstractComponent>> nodesToMerge = this.getNodesToMerge(effectiveParameters);

		/**
		 * HashMap parentNodes key : feature ID value : all inherited nodes
		 * related to the key that contain the inherited elements and effective
		 * parameters to use to resolve their potential references
		 */
		HashMap<Integer, List<Node<? extends EObject>>> parentNodes = new HashMap<Integer, List<Node<? extends EObject>>>();


		/**
		 * initialization of the parentNodes map
		 */
		for (int j = nodesToMerge.size() - 1; j >= 0; j--) {
			Node<? extends AbstractComponent> extendedNode = nodesToMerge
					.get(j);
			AbstractComponent nodeToExtend = extendedNode.getT();
			HashMap<String, String> extendedNodeEffectiveParameters = extendedNode
					.getEffectiveParameters();

			for (int k = 0; k < featureIDs.length; k++) {
				EStructuralFeature feature = nodeToExtend.eClass()
						.getEStructuralFeature(featureIDs[k]);
			//	EStructuralFeature mergedFeature = node.eClass()
			//			.getEStructuralFeature(getMergedFeatureID(featureIDs[k]));
				Object object = nodeToExtend.eGet(feature);

				if (feature instanceof EReference && object instanceof List) {
					for (int i = 0; i < ((List<?>) nodeToExtend.eGet(nodeToExtend
							.eClass().getEStructuralFeature(featureIDs[k])))
							.size(); i++) {
						EObject element = (EObject) ((List<?>) nodeToExtend
								.eGet(nodeToExtend.eClass()
										.getEStructuralFeature(featureIDs[k])))
								.get(i);

						if (parentNodes.get(featureIDs[k]) == null) {
							parentNodes.put(featureIDs[k],
									new ArrayList<Node<? extends EObject>>());
						}
						parentNodes.get(featureIDs[k]).add(
								new Node<EObject>(element,
										extendedNodeEffectiveParameters));

					}
				} else if (feature instanceof EReference
						&& feature.getUpperBound() == 1 && object != null) {
					EObject element = ((EObject) object);

					if (parentNodes.get(featureIDs[k]) == null) {
						parentNodes.put(featureIDs[k],
								new ArrayList<Node<? extends EObject>>());
					}
					parentNodes.get(featureIDs[k]).add(
							new Node<EObject>(element,
									extendedNodeEffectiveParameters));

				} else if (feature instanceof EAttribute) {

				}
			}
			
		}

		
		for (int i = 0; i < featureIDs.length; i++) {
			EStructuralFeature feature = t.eClass().getEStructuralFeature(
					featureIDs[i]);

			if (feature instanceof EReference
					&& feature.getUpperBound() == ETypedElement.UNBOUNDED_MULTIPLICITY
					|| feature.getUpperBound() > 1 
					&& !isASTFeature(feature.getFeatureID())) {
				if (hasMergedFeature(featureIDs[i])) {
					Object listFeature = getObject().eGet(
							t.eClass().getEStructuralFeature(featureIDs[i]));
					// ((List)getObject().eGet(t.eClass().getEStructuralFeature(
					// getMergedFeature(featureIDs[i])))).clear();

					// We merge all elements of the current feature
					for (int j = 0; j < ((List<?>) listFeature).size(); j++) {
						EObject elementToMerge = (EObject) ((List<?>) listFeature)
								.get(j);
						if (elementToMerge instanceof IHelperAdapter) {
							Node<? extends EObject> nodeToMerge = new Node<EObject>(
									elementToMerge, effectiveParameters);
	
							// We get all inherited elements having the same name
							List<Node<? extends EObject>> nodeToMergeParentNodesList = new ArrayList<Node<? extends EObject>>();
							if (parentNodes.get(featureIDs[i]) != null) {
								nodeToMergeParentNodesList = HelperAdapter
										.getNodeWithSameName(parentNodes
												.get(featureIDs[i]),
												nodeToMerge);
							}
							
							// For shared components the nodeToMergeParentNodesList is null
							// in most of cases because we don't have parents definition having
							// a component with the same name.
							// But we need to merge the shared component with the referenced component
							// to to this we patch the nodeToMergeParentNodesList with the referenced component.
							if(elementToMerge instanceof Component){
								if(((Component)elementToMerge).getShared() != null){
									nodeToMergeParentNodesList.clear();
									nodeToMergeParentNodesList.add(new Node<Component>(((Component)elementToMerge).getShared(),effectiveParameters));
								}
							}
							
							// We merge the current element with the inherited elements
									((HelperAdapter<?>) ((IHelperAdapter) elementToMerge)
											.getHelper()).merge(
											nodeToMergeParentNodesList, false, monitor);
								
							
								if(parentNodes.get(featureIDs[i]) != null ){
								parentNodes.get(featureIDs[i]).removeAll(
										nodeToMergeParentNodesList);
								}
							
						}
					}

					// Now we have to add inherited elements not present in the merged feature
					List<EObject> mergedElementsToRetain = new ArrayList<EObject>();
					Object mergedFeature = getObject().eGet(getObject().eClass().getEStructuralFeature(getMergedFeatureID(featureIDs[i])));

					if (parentNodes.get(featureIDs[i]) != null) {
						List<List<Node<? extends EObject>>> otherNodes = HelperAdapter.getNodesWithSameName(parentNodes.get(featureIDs[i]));
						Iterator<List<Node<? extends EObject>>> iterator = otherNodes.iterator();
						
						while (iterator.hasNext()) {
							List<Node<? extends EObject>> next = iterator.next();
							if (next != null && next.get(0) != null) {
								
								// We checks that the EReference has a multiplicity > 1
								// So the with the default implementation eGet will return a List of EObject 
								@SuppressWarnings("unchecked")
								List<? extends EObject> list = ((HelperAdapter<?>) ((IHelperAdapter) next.get(0).getT()).getHelper()).getElementsWithSameName(
												(List<? extends EObject>) getObject().eGet(getObject().eClass().getEStructuralFeature(getMergedFeatureID(featureIDs[i]))));
								
								if (!list.isEmpty()) {
									Iterator<? extends EObject> existingMergedElementsIterator = list
											.iterator();
									while (existingMergedElementsIterator.hasNext()) {
										EObject existingMergedElement = existingMergedElementsIterator.next();
										copyAttributeIDs(next.get(0).getT(),
												existingMergedElement, next.get(0)
														.getEffectiveParameters(),monitor);
										
										((HelperAdapter<?>) ((IHelperAdapter) existingMergedElement).getHelper()).merge(next, true, monitor);
										if(!mergedElementsToRetain.contains(existingMergedElement)){
											mergedElementsToRetain.add(existingMergedElement);
										}
									}
								} else {
									EObject newMergedElement = t.eClass().getEStructuralFeature(featureIDs[i]).getEType().getEPackage()
											.getEFactoryInstance().create((EClass) t.eClass().getEStructuralFeature(featureIDs[i]).getEType());

									copyAttributeIDs(next.get(0).getT(),
											newMergedElement, next.get(0)
													.getEffectiveParameters(),monitor);
									
									
									if (mergedFeature instanceof List) {
										((List) mergedFeature).add(newMergedElement);
									} else {
										mergedFeature = newMergedElement;
									}
									
									mergedElementsToRetain.add(newMergedElement);
									((HelperAdapter<?>) ((IHelperAdapter) newMergedElement)
											.getHelper()).merge(next, true, monitor);
								}
							}
						}
						if (mergedFeature instanceof List) {
							((List<?>) mergedFeature).retainAll(mergedElementsToRetain);
						}
					} else {
						((List<?>) mergedFeature).clear();
					}
				}
			} else if (feature instanceof EReference
					&& feature.getUpperBound() == 1 
					&& !isASTFeature(feature.getFeatureID())) {
				if (hasMergedFeature(featureIDs[i])) {
					Object uniqueFeature = getObject().eGet(
							t.eClass().getEStructuralFeature(featureIDs[i]));

					EObject elementToMerge = (EObject)uniqueFeature;
					if (elementToMerge instanceof IHelperAdapter) {
						Node<EObject> nodeToMerge = new Node<EObject>(elementToMerge, effectiveParameters);
						List<Node<? extends EObject>> nodeToMergeParentNodesList = new ArrayList<Node<? extends EObject>>();
						if (parentNodes.get(featureIDs[i]) != null) {
							nodeToMergeParentNodesList = HelperAdapter.getNodeWithSameName(parentNodes.get(featureIDs[i]),nodeToMerge);
						}
							
						((HelperAdapter<?>) ((IHelperAdapter) elementToMerge).getHelper()).merge(nodeToMergeParentNodesList, false, monitor);
						
						if(parentNodes.get(featureIDs[i]) != null){
							parentNodes.get(featureIDs[i]).removeAll(
									nodeToMergeParentNodesList);
						}
					}

					EStructuralFeature mergedFeature =
							getObject().eClass().getEStructuralFeature(
									getMergedFeatureID(featureIDs[i]));

					if (parentNodes.get(featureIDs[i]) != null) {

						List<List<Node<? extends EObject>>> otherNodes = HelperAdapter
								.getNodesWithSameName(parentNodes
										.get(featureIDs[i]));
						Iterator<List<Node<? extends EObject>>> iterator = otherNodes
								.iterator();
						while (iterator.hasNext()) {
							List<Node<? extends EObject>> next = iterator.next();
							if (next != null && next.get(0) != null) {
								EObject existingUniqueFeature = (EObject)getObject().eGet(getObject().eClass().getEStructuralFeature(featureIDs[i]));
								if(existingUniqueFeature != null && hasSameName(next.get(0), new Node<EObject>(existingUniqueFeature, effectiveParameters))){
									((HelperAdapter<?>) ((IHelperAdapter) existingUniqueFeature)
											.getHelper()).merge(next, true, monitor);
									getObject().eSet(mergedFeature, null);
								}else if(existingUniqueFeature == null){
									EObject newMergedElement = t.eClass().getEStructuralFeature(featureIDs[i]).getEType().getEPackage().getEFactoryInstance()
											.create((EClass) t.eClass().getEStructuralFeature(featureIDs[i]).getEType());
									copyAttributeIDs(next.get(0).getT(),
											newMergedElement, next.get(0)
													.getEffectiveParameters(),monitor);
									getObject().eSet(mergedFeature, newMergedElement);
									
									((HelperAdapter<?>) ((IHelperAdapter) newMergedElement)
											.getHelper()).merge(next, true, monitor);
								}
							}
						}
					}else {
						getObject().eSet(mergedFeature, null);
					}
				}
			}
		}
	}
	
	/**
	 * Update the merged features related to the features ID.
	 * Example : a call with new int[]{FractalPackage.ABSTRACT_COMPONENT__INTERFACES} updates 
	 * the related FractalPackage.ABSTRACT_COMPONENT__MERGED_INTERFACES feature.
	 * @param featureIDs non merged features ID
	 */
	protected void updateMerge(final int featureIDs[], final IProgressMonitor monitor){
		Update update = new Update(){
			protected void update(){
				mergeFeature(new HashMap<String,String>(), featureIDs, monitor);
			}
		};
		
		update.run();
	}
	
	/**
	 * @param featureID of a merged feature
	 */
	protected void updateChildrenMerge(final int featureID){
		UpdateChildren update = new UpdateChildren(){
			protected void update(IAbstractComponentListener adapter){
				adapter.update(featureID);
				adapter.updateChildren(featureID);
			}
		};
		
		update.run();
	}
	
	
	public void updateChildrenMerge(){
		Command cmd = new RecordingCommand(FractalTransactionalEditingDomain
				.getEditingDomain()){
			protected void doExecute() {
				UpdateChildren update = new UpdateChildren(){
					protected void update(IAbstractComponentListener adapter){
						adapter.updateMerge();
						adapter.updateChildrenMerge();
					}
				};
				update.run();
			}
		};
		
		FractalTransactionalEditingDomain.getEditingDomain().getCommandStack()
		.execute(cmd);
	}
	
	public void updateMerge(final IProgressMonitor monitor){
		
		Command cmd = new RecordingCommand(FractalTransactionalEditingDomain
				.getEditingDomain()){
			protected void doExecute() {
				updateMerge(new int[]{
						FractalPackage.ABSTRACT_COMPONENT__INTERFACES,
						FractalPackage.ABSTRACT_COMPONENT__SUB_COMPONENTS,
						FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER,
						FractalPackage.ABSTRACT_COMPONENT__BINDINGS,
						FractalPackage.ABSTRACT_COMPONENT__CONTENT,
						FractalPackage.ABSTRACT_COMPONENT__CONTROLLER,
						FractalPackage.ABSTRACT_COMPONENT__LOGGER,
						FractalPackage.ABSTRACT_COMPONENT__VIRTUAL_NODE}, monitor);
				updateBindings(monitor);
				
			}
		};
		
		FractalTransactionalEditingDomain.getEditingDomain().getCommandStack()
		.execute(cmd);
	}
	
	public void update(final int featureID, final IProgressMonitor monitor){
		
		Command cmd = new RecordingCommand(FractalTransactionalEditingDomain
				.getEditingDomain()) {
			protected void doExecute() {
				switch (featureID) {
				case FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__BINDINGS:
					updateBindings(monitor);
					break;
				case FractalPackage.ABSTRACT_COMPONENT__COMMENTS:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__CONTENT:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__CONTROLLER:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__COORDINATES:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__EXTENDS_AST:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__INTERFACES:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__LOGGER:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER:
					updateMerge(new int[]{FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER}, monitor);
					break;
				case FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS:
					updateMerge(new int[]{FractalPackage.ABSTRACT_COMPONENT__BINDINGS}, monitor);
					break;
				case FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTENT:
					updateMerge(new int[]{FractalPackage.ABSTRACT_COMPONENT__CONTENT}, monitor);
					break;
				case FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTROLLER:
					updateMerge(new int[]{FractalPackage.ABSTRACT_COMPONENT__CONTROLLER}, monitor);
					break;
				case FractalPackage.ABSTRACT_COMPONENT__MERGED_INTERFACES:
					updateMerge(new int[]{FractalPackage.ABSTRACT_COMPONENT__INTERFACES}, monitor);
					break;
				case FractalPackage.ABSTRACT_COMPONENT__MERGED_LOGGER:
					updateMerge(new int[]{FractalPackage.ABSTRACT_COMPONENT__LOGGER}, monitor);
					break;
				case FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS:
					updateMerge(new int[]{
								FractalPackage.ABSTRACT_COMPONENT__INTERFACES,
								FractalPackage.ABSTRACT_COMPONENT__SUB_COMPONENTS,
								FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER,
								FractalPackage.ABSTRACT_COMPONENT__BINDINGS,
								FractalPackage.ABSTRACT_COMPONENT__CONTENT,
								FractalPackage.ABSTRACT_COMPONENT__CONTROLLER}, monitor);
					break;
				case FractalPackage.ABSTRACT_COMPONENT__MERGED_TEMPLATE_CONTROLLER:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__MERGED_VIRTUAL_NODE:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__NAME:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__NAME_AST:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__SUB_COMPONENTS:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__TEMPLATE_CONTROLLER:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__VIRTUAL_NODE:
					break;
				}
			}
		};

		
		//FractalTransactionalEditingDomain.getEditingDomain().getCommandStack()
		//		.execute(cmd);
	}
	
	public void updateChildren(final int featureID){
		Command cmd = new RecordingCommand(FractalTransactionalEditingDomain
				.getEditingDomain()) {
			protected void doExecute() {
				switch (featureID) {
				case FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__BINDINGS:
					//updateChildrenBindings();
					break;
				case FractalPackage.ABSTRACT_COMPONENT__COMMENTS:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__CONTENT:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__CONTROLLER:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__COORDINATES:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__EXTENDS_AST:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__INTERFACES:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__LOGGER:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER:
					//updateChildrenMerge(FractalPackage.ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER);
					break;
				case FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS:
					//updateChildrenMergedBindings();
					break;
				case FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTENT:
					//updateChildrenMerge(FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTENT);
					break;
				case FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTROLLER:
					//updateChildrenMerge(FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTROLLER);
					break;
				case FractalPackage.ABSTRACT_COMPONENT__MERGED_INTERFACES:
					//updateChildrenMerge(FractalPackage.ABSTRACT_COMPONENT__MERGED_INTERFACES);
					//updateChildrenMergedInterfaces();
					break;
				case FractalPackage.ABSTRACT_COMPONENT__MERGED_LOGGER:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS:
					//updateChildrenMerge(FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS);
					//updateChildrenMergedComponents();
					break;
				case FractalPackage.ABSTRACT_COMPONENT__MERGED_TEMPLATE_CONTROLLER:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__MERGED_VIRTUAL_NODE:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__NAME:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__NAME_AST:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__SUB_COMPONENTS:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__TEMPLATE_CONTROLLER:
					break;
				case FractalPackage.ABSTRACT_COMPONENT__VIRTUAL_NODE:
					break;
				}
			}
		};

		FractalTransactionalEditingDomain.getEditingDomain().getCommandStack()
				.execute(cmd);
	}

//	public AbstractComponent getElementWithSameName(List<AbstractComponent> components) {
//		AbstractComponent result = null;
//		if(components == null){
//			return result;
//		}
//		
//		Iterator<AbstractComponent> iterator = components.iterator();
//		
//		while(iterator.hasNext()){
//			AbstractComponent nextComponent = iterator.next();
//			if(nextComponent.getName() != null && nextComponent.getName().equals(t.getName())){
//				result = nextComponent;
//				break;
//			}
//		}
//		
//		return result;
//	}
	
	public void merge(List<Node<? extends EObject>> srcNodes, boolean override, IProgressMonitor monitor){
		try{
			if(monitor != null){
				String name = t.getMergedName() != null?t.getMergedName():t.getName();
				monitor.subTask(name);
				monitor.beginTask(name, IProgressMonitor.UNKNOWN);
			}
		Iterator<Node<? extends EObject>> iterator = srcNodes.iterator();
		while(iterator.hasNext()){
			Node<? extends EObject> node = iterator.next();
			//AbstractComponent next = (AbstractComponent)node.getT();
			mergeFeature(node.getEffectiveParameters(), new int[]{
					FractalPackage.ABSTRACT_COMPONENT__INTERFACES,
					FractalPackage.ABSTRACT_COMPONENT__SUB_COMPONENTS,
					FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER,
					FractalPackage.ABSTRACT_COMPONENT__BINDINGS,
					FractalPackage.ABSTRACT_COMPONENT__CONTENT,
					FractalPackage.ABSTRACT_COMPONENT__CONTROLLER,
					FractalPackage.ABSTRACT_COMPONENT__VIRTUAL_NODE}, monitor);
		}
		}catch(Exception e){
			FractalPlugin.getDefault().getLog().log(new Status(IStatus.ERROR, FractalPlugin.PLUGIN_ID, e.getMessage(), e));
		}finally{
			if(monitor != null){
				monitor.done();
			}
		}
		
	}
	
	
	protected static final EStructuralFeature uniqueFeatures[] = new EStructuralFeature[]{
		FractalPackage.Literals.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER,
		FractalPackage.Literals.ABSTRACT_COMPONENT__CONTENT,
		FractalPackage.Literals.ABSTRACT_COMPONENT__CONTROLLER,
		FractalPackage.Literals.ABSTRACT_COMPONENT__VIRTUAL_NODE,
		FractalPackage.Literals.ABSTRACT_COMPONENT__LOGGER,
		FractalPackage.Literals.ABSTRACT_COMPONENT__TEMPLATE_CONTROLLER,
	};
	
	public EStructuralFeature[] getUniqueFeatures(){
		return uniqueFeatures;
	}
	
	public void copyAttributeIDs(EObject src, EObject dest, HashMap<String, String> effectiveParameters, IProgressMonitor monitor){
		if(src instanceof Component){
			if((((Component)src).getShared() != null ||
					((Component)src).getHelper().isShared())){
				((Component)dest).getHelper().setShared(true);
			}else{
				((Component)dest).getHelper().setShared(false);
			}
		}
		super.copyAttributeIDs(src, dest, effectiveParameters, monitor);
	}
}
