/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.util;

 import org.eclipse.emf.ecore.xmi.XMLHelper;
import org.eclipse.emf.ecore.xmi.impl.SAXWrapper;
import org.eclipse.emf.ecore.xmi.impl.XMLLoadImpl;
import org.xml.sax.helpers.DefaultHandler;


 // TODO: Auto-generated Javadoc
/**
  * The Class FractalAdlExtendedLoadImpl.
  */
 public class FractalLoadImpl extends XMLLoadImpl
 {
   
   /**
    * Instantiates a new fractal adl extended load impl.
    * 
    * @param helper the helper
    */
   public FractalLoadImpl(XMLHelper helper)
   {
     super(helper);
   }

   /* (non-Javadoc)
    * @see org.eclipse.emf.ecore.xmi.impl.XMLLoadImpl#makeDefaultHandler()
    */
   protected DefaultHandler makeDefaultHandler()
   {
     return new SAXWrapper(new FractalHandler(resource, helper, options));
   }

   
 }