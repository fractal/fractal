/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter.helper;

import java.util.Iterator;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.System;

// TODO: Auto-generated Javadoc
/**
 * The Class SystemHelperAdapter.
 */
public class SystemHelperAdapter extends HelperAdapter<System> implements ISystemHelper{
	
	/**
	 * Instantiates a new system helper adapter.
	 * 
	 * @param system the system
	 */
	public SystemHelperAdapter(System system){
		super(system);
	}
	
	/* (non-Javadoc)
	 * @see org.ow2.fractal.adl.extended.fractalextended.adapter.helper.SystemHelper#getDefinition(java.lang.String)
	 */
	public Definition getDefinition(String definitionName){
    	Definition resultDefinition = null;
    	if(t == null || definitionName == null || definitionName.equals("")) 
    		return resultDefinition;
    	
    	EList<Definition> definitions = t.getDefinitions();
    	Iterator<Definition> iterator = definitions.iterator();
    	boolean notFound = true;
    	
    	while(iterator.hasNext() && notFound){
    		Definition definition = iterator.next();
    		if(definition.getName() != null && definition.getName().equals(definitionName)){
    			resultDefinition = definition;
    			notFound = false;
    		}
    	}
    	return resultDefinition;
    }
	
	/* (non-Javadoc)
	 * @see org.ow2.fractal.adl.extended.fractalextended.adapter.helper.HelperAdapter#isAdapterForType(java.lang.Object)
	 */
	
	public boolean isAdapterForType(Object type) {
		return type == SystemHelperAdapter.class;
	}
	
	public void update(int featureID, IProgressMonitor monitor){
		
	}
}
