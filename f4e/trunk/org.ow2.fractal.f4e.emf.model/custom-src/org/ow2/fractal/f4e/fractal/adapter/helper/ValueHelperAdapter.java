/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter.helper;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.TokenRewriteStream;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Constant;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.EffectiveParameter;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Reference;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.ValueElement;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.parser.ValueADLLexer;
import org.ow2.fractal.f4e.fractal.parser.ValueADLParser;


// TODO: Auto-generated Javadoc
/**
 * The Class ValueHelperAdapter.
 */
public class ValueHelperAdapter extends HelperAdapter<Value> implements IValueHelper{
//	private static Map<String, Value> cache = new TreeMap<String, Value>();
	
	/**
	 * Instantiates a new value helper adapter.
	 * 
	 * @param value the value
	 */
	public ValueHelperAdapter(Value value){
		super(value);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	
	public String toString(){
		EList<ValueElement> elements = t.getElements();
		
		Iterator<ValueElement> iterator = elements.iterator();
		
		StringBuffer stringBuffer = new StringBuffer();
		
		while(iterator.hasNext()){
			ValueElement element = iterator.next();
			if(element.eClass().getClassifierID() == FractalPackage.CONSTANT){
				stringBuffer.append(((Constant)element).getConstant());
			}else if(element.eClass().getClassifierID() == FractalPackage.REFERENCE){
					stringBuffer.append("${" + ((Reference)element).getName() + "}");
			}
		}
		
		return stringBuffer.toString();
	}
	
	/* (non-Javadoc)
	 * @see org.ow2.fractal.adl.extended.fractalextended.adapter.helper.HelperAdapter#isAdapterForType(java.lang.Object)
	 */
	
	public boolean isAdapterForType(Object type) {
		return type == ValueHelperAdapter.class;
	}
	
	/**
	 * Create a Value model element from a fractal ADL string which represents a value.
	 * 
	 * @param string the string is a fractal adl value ex : "foo-${arg1}".
	 * see ValueADL.g3.
	 * 
	 * @param abstractComponent the abstractComponent,
	 * it is needed to find the definition which holds the value. This definition
	 * will be used to resolve arguments declared in the value.
	 * 
	 * @return the value which is a model representation of the fractal adl value string.
	 * For example for the "foo-${arg1}" string, a Value element with two sub element one Constant
	 * "foo-" and one Reference "arg1". 
	 */
	public static Value getValue(String string, AbstractComponent abstractComponent){
		Value value = null;
		
//		value = cache.get(string);
//		if(value != null) return value;
		if(string == null){
			return null;
		}
		
		Definition definition = null;
		if(abstractComponent != null){
			IAbstractComponentHelper helper = (IAbstractComponentHelper)HelperAdapterFactory.getInstance().adapt(abstractComponent);
			
			if(helper != null){
				definition = helper.getParentDefinition();
			}
		}
		
		try{
			CharStream s = new ANTLRStringStream(string);
			ValueADLLexer lexer = new ValueADLLexer(s);

			TokenRewriteStream tokens = new TokenRewriteStream(lexer);
			ValueADLParser grammar = new ValueADLParser(tokens, definition);

			value = grammar.getValue();

			//					cache.put(string, value);
		}catch(Exception e){
			e.printStackTrace();
		}
			
		
		return value;
	}
	
	protected String getValue(List<EffectiveParameter> parameters, String parameterName){
		Iterator<EffectiveParameter> iterator = parameters.iterator();
		while(iterator.hasNext()){
			EffectiveParameter parameter = iterator.next();
			if(parameter.getName().getHelper().toString().equals(parameterName)){
				return parameter.getValue().getHelper().toString();
			}
		}
		return null;
	}
	
	public String resolve(List<EffectiveParameter> parameters){
		EList<ValueElement> elements = t.getElements();
		
		Iterator<ValueElement> iterator = elements.iterator();
		
		StringBuffer stringBuffer = new StringBuffer();
		
		while(iterator.hasNext()){
			ValueElement element = iterator.next();
			if(element.eClass().getClassifierID() == FractalPackage.CONSTANT){
				stringBuffer.append(((Constant)element).getConstant());
			}else if(element.eClass().getClassifierID() == FractalPackage.REFERENCE){
				String resolvedReference = getValue(parameters, ((Reference)element).getName());
				if(resolvedReference != null){
					stringBuffer.append(resolvedReference);
				}else{
					stringBuffer.append("${" + ((Reference)element).getName() + "}");
				}
			}
		}
		
		return stringBuffer.toString();
	}
	
	
	public String resolve(HashMap<String,String> parameters){
		EList<ValueElement> elements = t.getElements();
		
		Iterator<ValueElement> iterator = elements.iterator();
		
		StringBuffer stringBuffer = new StringBuffer();
		
		while(iterator.hasNext()){
			ValueElement element = iterator.next();
			if(element.eClass().getClassifierID() == FractalPackage.CONSTANT){
				stringBuffer.append(((Constant)element).getConstant());
			}else if(element.eClass().getClassifierID() == FractalPackage.REFERENCE){
				String resolvedReference = parameters.get(((Reference)element).getName());
				if(resolvedReference != null){
					stringBuffer.append(resolvedReference);
				}else if(((Reference)element).getReference() != null && ((Reference)element).getReference().getValue()!=null){
					stringBuffer.append(((Reference)element).getReference().getValue().getHelper().toString());
				}else{
					stringBuffer.append("${" + ((Reference)element).getName() + "}");
				}
			}
		}
		return stringBuffer.toString();
	}
	
	public void update(int featureID, IProgressMonitor monitor){
		
	}
}
