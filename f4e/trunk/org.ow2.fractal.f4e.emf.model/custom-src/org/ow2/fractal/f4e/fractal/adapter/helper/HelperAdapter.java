/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */

package org.ow2.fractal.f4e.fractal.adapter.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.transaction.RecordingCommand;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.adapter.helper.marker.IMarkerHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.marker.MarkerHelper;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;

/**
 * The Class HelperAdapter.
 * 
 * @param <T>  * 
 * @author Yann Davin
 */
public abstract class HelperAdapter<T extends EObject> extends AdapterImpl implements IHelper<T>{
	/** The t. */
	protected T t;
	
	
	/** In the Fractal ADL we can add xml comments. 'comments' is the list of xml comments attached
	 * to the target model element. 
	 */
	protected List<String> comments;
	
	/**
	 * Instantiates a new helper adapter.
	 * 
	 * @param t the t
	 */
	public HelperAdapter(T t){
		this.t = t;
	}
	
	/* (non-Javadoc)
	 * @see org.ow2.fractal.adl.extended.fractalextended.adapter.helper.Helper#getObject()
	 */
	public T getObject(){
		return this.t;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.common.notify.impl.AdapterImpl#isAdapterForType(java.lang.Object)
	 */
	public boolean isAdapterForType(Object type) {
		return type == IHelper.class;
	}

	/**
	 * Return the list of xml comments attached to the target model element.
	 */
	public List<String> getComments() {
		return comments;
	}

	/**
	 * Attach xml comments to the target model element.
	 */
	public void setComments(List<String> comments) {
		this.comments = comments;
	}	
	
	protected HashMap<String,String> effectiveParameters = new HashMap<String,String>();
	public HashMap<String,String> getEffectiveParameters(){
		return effectiveParameters;
	}
	
	protected HashMap<Integer, List<EObject>> tempMergedFeatures = new HashMap<Integer, List<EObject>>();
	
	
	public static boolean isMergedElement(EObject eObject){
		if(eObject.eContainer() != null && eObject.eContainingFeature()!=null){
			return isMergedFeature(eObject.eContainer(), eObject.eContainingFeature().getFeatureID());
		}
		return false;
	}
	
	protected boolean hasMergedFeature(int featureID){
		return hasMergedFeature(getObject(), featureID);
	}
	
	public static boolean hasMergedFeature(EObject object, int featureID){
		EStructuralFeature eStructuralFeature = object.eClass().getEStructuralFeature(featureID);
		if(eStructuralFeature == null) return false;
		
		String featureName = object.eClass().getEStructuralFeature(featureID).getName();
		String firstCharacter = featureName.substring(0, 1);
		String rest = featureName.substring(1, featureName.length());
		EStructuralFeature mergedFeature = object.eClass().getEStructuralFeature("merged" + firstCharacter.toUpperCase() + rest);
		return !(mergedFeature==null);
	}
	
	public boolean isMergedFeature(int featureID){
		return isMergedFeature(getObject(),featureID);
	}
	
	public static boolean isMergedFeature(EObject eObject, int featureID){
		if(featureID < 0){
			return false;
		}
		String featureName = eObject.eClass().getEStructuralFeature(featureID).getName();
		return featureName.startsWith("merged");
	}
	
	public boolean isASTFeature(int featureID){
		return isASTFeature(getObject(), featureID);
	}
	
	public static boolean isASTFeature(EObject eObject, int featureID){
		if(featureID < 0){
			return false;
		}
		String featureName = eObject.eClass().getEStructuralFeature(featureID).getName();
		return featureName.endsWith("AST");
	}
	
	public boolean hasASTFeature(int featureID){
		return hasASTFeature(getObject(),featureID);
	}
	
	public static boolean hasASTFeature(EObject eObject, int featureID){
		String featureName = eObject.eClass().getEStructuralFeature(featureID).getName();
		EStructuralFeature ASTFeature = eObject.eClass().getEStructuralFeature(featureName + "AST");
		return !(ASTFeature==null);
	}
	
	public static Integer getASTFeature(EObject eObject, int featureID){
		String featureName = eObject.eClass().getEStructuralFeature(featureID).getName();
		EStructuralFeature ASTFeature = eObject.eClass().getEStructuralFeature(featureName + "AST");
		if(ASTFeature != null){
			return ASTFeature.getFeatureID();
		}else{
			return null;
		}
	}
	
	
	static protected boolean isNamedElement(EObject eObject){
		boolean result = false;
		Iterator<EAttribute> iterator = eObject.eClass().getEAllAttributes().iterator();
		while(iterator.hasNext()){
			EAttribute attribute = iterator.next();
			if(attribute.getName().equals("name")){
				result = true;
				break;
			}
		}
		return result;
	}
	
	/**
	 * By default we return the feature ID of the attribute named 'name' if it exists,
	 * else if the related object has only one attribute with 1 as cardinality we return it.
	 * Attribute IDs are assumed to be of type EString
	 * @return the feature ID of the attribute that are the key of the elements.
	 */
	public int[] getAttributeIDs(){
		return getAttributeIDs(getObject());
	}
	
	
	/**
	 * 
	 * @param <T>
	 * @param object
	 * @return
	 */
	static public <T extends EObject> int[] getAttributeIDs(T object){
		if(isNamedElement(object)){
			return  new int[]{object.eClass().getEStructuralFeature("name").getFeatureID()};
		}else{
			List<EAttribute> attributes = object.eClass().getEAllAttributes();
			if(attributes.size() == 1 && attributes.get(0).getUpperBound()==1){
				return new int[]{attributes.get(0).getFeatureID()};
			}
		}
		return new int[]{};
	}
	
	/**
	 * 
	 * @param featureID
	 * @return the featureID of the related merge feature.
	 *  Example if we pass ABSTRACT_COMPONENT_INTERFACES it returns ABSTRACT_COMPONENT_MERGED_INTERFACES.
	 *  The related merged feature must have the same name that its related feature prefixed with 'merged'
	 */
	protected Integer getMergedFeatureID(int featureID){
		return getMergedFeatureID(getObject(),featureID);
	}
	
	public static Integer getMergedFeatureID(EObject object, int featureID){
		String featureName = object.eClass().getEStructuralFeature(featureID).getName();
		String firstCharacter = featureName.substring(0, 1);
		String rest = featureName.substring(1, featureName.length());
		EStructuralFeature mergedFeature = object.eClass().getEStructuralFeature("merged" + firstCharacter.toUpperCase() + rest);
		return mergedFeature.getFeatureID();
	}
	
	public static EStructuralFeature getMergedFeature(EObject object, EStructuralFeature feature){
		String featureName = feature.getName();
		String firstCharacter = featureName.substring(0, 1);
		String rest = featureName.substring(1, featureName.length());
		EStructuralFeature mergedFeature = object.eClass().getEStructuralFeature("merged" + firstCharacter.toUpperCase() + rest);
		return mergedFeature;
	}
	
	
	public static EStructuralFeature getFeatureOfMergedFeature(EObject object, EStructuralFeature mergedFeature){
		String featureName = mergedFeature.getName();
		String trunk = featureName.substring(7);
		String firstCharacter = featureName.substring(6, 7);
		EStructuralFeature feature = object.eClass().getEStructuralFeature(firstCharacter.toLowerCase() + trunk);
		return feature;
	}
	
	public EStructuralFeature[] getUniqueFeatures(){
		return new EStructuralFeature[]{};
	}
	
	public void merge(List<Node<? extends EObject>> srcNodes, boolean override, IProgressMonitor monitor){
		HashMap<Integer, List<EObject>> tempMergedElements = new HashMap<Integer,List<EObject>>();

		Iterator<Node<? extends EObject>> iteratorSrcNodes = srcNodes.iterator();
		while(iteratorSrcNodes.hasNext()){
			final Node<? extends EObject> node = iteratorSrcNodes.next();
			EObject srcNode = node.getT();

			List<EAttribute> attributes = getObject().eClass().getEAllAttributes();
			Iterator<EAttribute> iterator = attributes.iterator();
			while(iterator.hasNext()){
				final EAttribute attribute = iterator.next();
				
				/**
				 * We merge all attributes functional attributes.
				 * Technical attributes any, anyAttributes, merged, and AST are ignored.
				 * Keys attributes are also ignored, they can be copied using copyAttributeIDs.
				 */
				if( !attribute.getName().equals("any") &&
						!attribute.getName().equals("anyAttributes") &&
						!attribute.getName().startsWith("merged") &&
						!attribute.getName().endsWith("AST")){
					if(override == true){
						if(hasASTFeature(srcNode, attribute.getFeatureID())){
							final Object value =  srcNode.eGet(srcNode.eClass().getEStructuralFeature(getASTFeature(srcNode, attribute.getFeatureID())));
							if(value instanceof Value){
								Command cmd = new RecordingCommand(FractalTransactionalEditingDomain.getEditingDomain()) {
							        protected void doExecute(){
							        	getObject().eSet(getObject().eClass().getEStructuralFeature(getMergedFeatureID(getObject(),attribute.getFeatureID())),((Value)value).getHelper().resolve(node.getEffectiveParameters()));
							        }
							    };
							 FractalTransactionalEditingDomain.getEditingDomain().getCommandStack().execute(cmd); 
								
							}
						}
					}else{
						if(getObject().eIsSet(attribute)){
							if(hasMergedFeature(attribute.getFeatureID())){
								getObject().eSet(getObject().eClass().getEStructuralFeature(getMergedFeatureID(attribute.getFeatureID())), srcNode.eGet(attribute));
							}
						}
					}
				}
			}

			List<EReference> containments = getObject().eClass().getEAllContainments();
			Iterator<EReference> iteratorContainment = containments.iterator();

			while(iteratorContainment.hasNext()){
				EReference reference = iteratorContainment.next();
				if(hasMergedFeature(reference.getFeatureID())){
					int mergedFeatureID = getMergedFeatureID(reference.getFeatureID());
					EStructuralFeature mergedFeature = getObject().eClass().getEStructuralFeature(mergedFeatureID);
					
					if(tempMergedElements.get(mergedFeatureID) == null){
						tempMergedElements.put(mergedFeatureID, new ArrayList());
					}

					if(reference.getUpperBound() == ETypedElement.UNBOUNDED_MULTIPLICITY || reference.getUpperBound() > 1){
						Object object = srcNode.eGet(reference);
						if(object instanceof List){
							Iterator<?> iteratorObject = ((List)object).iterator();
							while(iteratorObject.hasNext()){
								Object next = iteratorObject.next();
								if(next instanceof IHelperAdapter){
									EObject sameElement = ((HelperAdapter<?>)(((IHelperAdapter)next).getHelper())).getElementWithSameName((List)getObject().eGet(reference), node.getEffectiveParameters());
									if(sameElement != null){
										((HelperAdapter)((IHelperAdapter)sameElement).getHelper()).merge(node.getEffectiveParameters(), (EObject)next, override);
									}else{
										EObject sameMergedElement = ((HelperAdapter<?>)(((IHelperAdapter)next).getHelper())).getElementWithSameName((List)getObject().eGet(mergedFeature), node.getEffectiveParameters());
										if(sameMergedElement != null){
											((HelperAdapter)((IHelperAdapter)sameMergedElement).getHelper()).merge(node.getEffectiveParameters(), (EObject)next, override);
										}else{
											EObject newObject = ((EObject)next).eClass().getEPackage().getEFactoryInstance().create(((EObject)next).eClass());
											copyAttributeIDs((EObject)next, newObject, node.getEffectiveParameters(), monitor);
											((List)getObject().eGet(mergedFeature)).add(newObject);
											tempMergedElements.get(mergedFeatureID).add(newObject);
											((HelperAdapter)((IHelperAdapter)newObject).getHelper()).merge(node.getEffectiveParameters(), (EObject)next, override);
										}
									}
								}
							}
						}
					}else if(reference.getUpperBound() == 1){

					}
				}
			}
		}
	
		if(srcNodes.isEmpty()){
			List<EAttribute> attributes = getObject().eClass().getEAllAttributes();
			Iterator<EAttribute> iterator = attributes.iterator();
			while(iterator.hasNext()){
				final EAttribute attribute = iterator.next();
				if(hasMergedFeature(attribute.getFeatureID())){
					//getObject().eSet(getObject().eClass().getEStructuralFeature(getMergedFeature(attribute.getFeatureID())), null);
				}
			}
			
			List<EReference> containments = getObject().eClass().getEAllContainments();
			Iterator<EReference> iteratorContainment = containments.iterator();
			while(iteratorContainment.hasNext()){
				EReference reference = iteratorContainment.next();
				if(hasMergedFeature(reference.getFeatureID())){
					int mergedFeatureID = getMergedFeatureID(reference.getFeatureID());
					EStructuralFeature mergedFeature = getObject().eClass().getEStructuralFeature(mergedFeatureID);

					if(reference.getUpperBound() == ETypedElement.UNBOUNDED_MULTIPLICITY || reference.getUpperBound() > 1){
						((List)getObject().eGet(getObject().eClass().getEStructuralFeature(mergedFeatureID))).clear();
					}
				}
			}
					
		}
		
		Iterator<Integer> keyIterator = tempMergedElements.keySet().iterator();
		while(keyIterator.hasNext()){
			int featureID = keyIterator.next(); 
			
			//cleanMergedFeatures(tempMergedElements.get(featureID),getObject().eClass().getEStructuralFeature(featureID));
		}
	}
	
	protected void cleanMergedFeatures(List<? extends EObject> createdMergedFeatures, EStructuralFeature feature){
		synchronized (((EList<? extends EObject>)t.eGet(feature))) {
			
		Iterator<? extends EObject> iterator = ((EList<? extends EObject>)t.eGet(feature)).iterator();
		List objectsToRemove = new ArrayList();
		while(iterator.hasNext()){
			EObject next = iterator.next();
			if(!createdMergedFeatures.contains(next)){
				objectsToRemove.add(next);
			}
		}
		((EList<? extends EObject>)getObject().eGet(feature)).removeAll(objectsToRemove);
		}
	}
	
	public void merge(final HashMap<String,String> parameters,final T srcNode, boolean override){
		List<EAttribute> attributes = getObject().eClass().getEAllAttributes();
		Iterator<EAttribute> iterator = attributes.iterator();
		while(iterator.hasNext()){
			final EAttribute attribute = iterator.next();
			
			if( !attribute.getName().equals("any") &&
				!attribute.getName().equals("anyAttributes") &&
				!attribute.getName().startsWith("merged") &&
				!attribute.getName().endsWith("AST") ){
			
					if(hasASTFeature(srcNode, attribute.getFeatureID())){
						final Object value =  srcNode.eGet(srcNode.eClass().getEStructuralFeature(getASTFeature(srcNode, attribute.getFeatureID())));
						if(value instanceof Value){
							Command cmd = new RecordingCommand(FractalTransactionalEditingDomain.getEditingDomain()) {
						        protected void doExecute(){
						        	getObject().eSet(getObject().eClass().getEStructuralFeature(getMergedFeatureID(getObject(),attribute.getFeatureID())),((Value)value).getHelper().resolve(parameters));
						        }
						    };
						 FractalTransactionalEditingDomain.getEditingDomain().getCommandStack().execute(cmd); 
							
						}
					}else{
						getObject().eSet(getObject().eClass().getEStructuralFeature(getMergedFeatureID(getObject(),attribute.getFeatureID())), srcNode.eGet(attribute));
					}
			}
		}
		
		List<EReference> containments = getObject().eClass().getEAllContainments();
		Iterator<EReference> iteratorContainment = containments.iterator();
	
		while(iteratorContainment.hasNext()){
			EReference reference = iteratorContainment.next();
			if(hasMergedFeature(reference.getFeatureID())){
				int mergedFeatureID = getMergedFeatureID(reference.getFeatureID());
				EStructuralFeature mergedFeature = getObject().eClass().getEStructuralFeature(mergedFeatureID);
				
				if(reference.getUpperBound() == ETypedElement.UNBOUNDED_MULTIPLICITY || reference.getUpperBound() > 1){
					Object object = srcNode.eGet(reference);
					if(object instanceof List){
						Iterator<?> iteratorObject = ((List)object).iterator();
						while(iteratorObject.hasNext()){
							Object next = iteratorObject.next();
							if(next instanceof IHelperAdapter){
								EObject sameElement = ((HelperAdapter<?>)(((IHelperAdapter)next).getHelper())).getElementWithSameName((List)getObject().eGet(reference), parameters);
								if(sameElement != null){
									((HelperAdapter)((IHelperAdapter)sameElement).getHelper()).merge(parameters, (EObject)next, override);
								}else{
									EObject newObject = ((EObject)next).eClass().getEPackage().getEFactoryInstance().create(((EObject)next).eClass());
									((List)getObject().eGet(mergedFeature)).add(newObject);
									((HelperAdapter)((IHelperAdapter)newObject).getHelper()).merge(parameters, (EObject)next, override);
								}
							}
						}
					}
				}else if(reference.getUpperBound() == 1){
					
				}
			}
		}
		
	}
	
	/**
	 * Copy attribute IDs from src to dest.
	 * Basically attribute IDs features are assumed to be EString.
	 * They also have a related AST feature of type Value, and a related merged feature of type EString.
	 * 
	 * We first copy the ID feature of type EString,
	 * We resolve the associated AST feature of the ID of the source with the given effective parameters. 
	 * We set the associated ID merged feature the destination to the result of the previous resolution.
	 * 
	 * @param src
	 * @param dest
	 * @param effectiveParameters
	 */
	public void copyAttributeIDs(EObject src, EObject dest, HashMap<String, String> effectiveParameters, IProgressMonitor monitor){
		if(src instanceof IHelperAdapter && 
		   dest instanceof IHelperAdapter && 
		   ((IHelperAdapter)src).getHelper() instanceof IMerge &&
		   ((IHelperAdapter)dest).getHelper() instanceof IMerge &&
		   src.eClass().equals(dest.eClass())){
			int attributeIDs[] = ((IMerge)((IHelperAdapter)src).getHelper()).getAttributeIDs();
			for(int i=0; i<attributeIDs.length;i++){
				Integer attributeASTID = getASTFeature(src, attributeIDs[i]);
				if(attributeASTID != null){
					EStructuralFeature featureAST = src.eClass().getEStructuralFeature(attributeASTID);
					Object objectAST = src.eGet(src.eClass().getEStructuralFeature(attributeASTID));
					if(objectAST != null && objectAST instanceof Value){
						dest.eSet(dest.eClass().getEStructuralFeature(attributeIDs[i]), src.eGet(src.eClass().getEStructuralFeature(attributeIDs[i])));
						if(((IHelperAdapter)dest).getHelper() instanceof IMerge && hasMergedFeature(attributeIDs[i])){
							dest.eSet(dest.eClass().getEStructuralFeature(getMergedFeatureID(dest,attributeIDs[i])),((Value)objectAST).getHelper().resolve(effectiveParameters));
						}
					}else{
						dest.eSet(dest.eClass().getEStructuralFeature(attributeIDs[i]), src.eGet(src.eClass().getEStructuralFeature(attributeIDs[i])));
						if(((IHelperAdapter)dest).getHelper() instanceof IMerge && hasMergedFeature(attributeIDs[i])){
							dest.eSet(dest.eClass().getEStructuralFeature(getMergedFeatureID(dest,attributeIDs[i])),src.eGet(src.eClass().getEStructuralFeature(attributeIDs[i])));
						}
					}
				}else{
					dest.eSet(dest.eClass().getEStructuralFeature(attributeIDs[i]), src.eGet(src.eClass().getEStructuralFeature(attributeIDs[i])));
				}
				Integer ASTFeature = HelperAdapter.getASTFeature(dest, attributeIDs[i]);
				if(ASTFeature !=null && dest instanceof IHelperAdapter){
					((IHelper)((IHelperAdapter)dest).getHelper()).update(ASTFeature, monitor);
				}
			}
		}
	}
	
	public T getElementWithSameName(List<T> elements, HashMap<String,String> effectiveParameters) {	
		int attributeIDs[] = getAttributeIDs();
		
		List<EStructuralFeature> attributes = new ArrayList<EStructuralFeature>();
		for(int i=0; i< attributeIDs.length; i++){
			attributes.add(getObject().eClass().getEStructuralFeature(attributeIDs[i]));
		}
			Iterator<T> iterator = elements.iterator();
			while(iterator.hasNext()){
				T next = iterator.next();
				
				boolean equal = true;
				for(int j=0; j<attributes.size(); j++){
					Integer attributeAST = getASTFeature(next, attributes.get(j).getFeatureID());
					Value valueAST = null;
					if(attributeAST != null){
						Object objectAST = next.eGet(next.eClass().getEStructuralFeature(attributeAST));
						if(objectAST != null && objectAST instanceof Value){
							valueAST = (Value)objectAST;
						}
					}
					
					if(valueAST != null){
						if(next.eGet(attributes.get(j)) != null && !next.eGet(attributes.get(j)).equals(valueAST.getHelper().resolve(effectiveParameters))){
							equal = false;
						}else if(next.eGet(attributes.get(j)) == null && next.eGet(attributes.get(j)) != null){
							equal = false;
						}
					}else{
						if(next.eGet(attributes.get(j)) != null && !next.eGet(attributes.get(j)).equals(getObject().eGet(attributes.get(j)))){
							equal = false;
						}else if(next.eGet(attributes.get(j)) == null && next.eGet(attributes.get(j)) != null){
							equal = false;
						}
					}
				}
				if(equal == true){
					return next;
				}
			}
		return null;
	}	
	
	static public  List<Node<? extends EObject>> getNodeWithSameName(List<Node<? extends EObject>> list, Node<? extends EObject> node){
		List<Node<? extends EObject>> result = new ArrayList<Node<? extends EObject>>();
		Iterator<Node<? extends EObject>> iterator = list.iterator();
		while(iterator.hasNext()){
			Node<? extends EObject> next = iterator.next();
			if(hasSameName(next, node)){
				result.add(next);
			}
		}
		return result;
	}
	
	static public List<List<Node<? extends EObject>>> getNodesWithSameName(final List<Node<? extends EObject>> list){
		List<List<Node<? extends EObject>>> result = new ArrayList<List<Node<? extends EObject>>>();
		
		Iterator<Node<? extends EObject>> iterator = list.iterator();
		while(iterator.hasNext()){
			Node<? extends EObject> next = iterator.next();
			
			boolean added = false;
			Iterator<List<Node<? extends EObject>>> listIterator = result.iterator();
			while(listIterator.hasNext() && added != true){
				List<Node<? extends EObject>> nextList = listIterator.next();
				if(nextList.get(0) != null && hasSameName(nextList.get(0), next)){
					nextList.add(next);
					added = true;
				}
			}
			
			if(added == false){
				List<Node<? extends EObject>> newList = new ArrayList<Node<? extends EObject>>();
				newList.add(next);
				result.add(newList);
			}
		}
		
		return result;
	}
	
	static public boolean hasSameName(Node<? extends EObject> node1, Node<? extends EObject> node2){
		boolean equal = true;
		
		int attributeIDs[] = ((IMerge)((IHelperAdapter)node1.getT()).getHelper()).getAttributeIDs();
		
		List<EStructuralFeature> attributes = new ArrayList<EStructuralFeature>();
		for(int i=0; i< attributeIDs.length; i++){
			attributes.add(node1.getT().eClass().getEStructuralFeature(attributeIDs[i]));
		}
		
		for(int j=0; j<attributes.size(); j++){
			Integer attributeAST = getASTFeature(node1.getT(), attributes.get(j).getFeatureID());
			Value node1ValueAST = null;
			Value node2ValueAST = null;
			
			if(attributeAST != null){
				Object objectASTNode1 = node1.getT().eGet(node1.getT().eClass().getEStructuralFeature(attributeAST));
				if(objectASTNode1 != null && objectASTNode1 instanceof Value){
					node1ValueAST = (Value)objectASTNode1;
				}
				
				Object objectASTNode2 = node2.getT().eGet(node2.getT().eClass().getEStructuralFeature(attributeAST));
				if(objectASTNode2 != null && objectASTNode2 instanceof Value){
					node2ValueAST = (Value)objectASTNode2;
				}
			}
			
			String attributeNode1 = null;
			String attributeNode2 = null;
			
			
			if(node1ValueAST != null){
				attributeNode1 = node1ValueAST.getHelper().resolve(node1.getEffectiveParameters());
			}else{
				attributeNode1 = (String)node1.getT().eGet(attributes.get(j));
			}
			
			if(node2ValueAST != null){
				attributeNode2 = node2ValueAST.getHelper().resolve(node2.getEffectiveParameters());
			}else{
				attributeNode2 = (String)node2.getT().eGet(attributes.get(j));
			}
			
			if(attributeNode1 != null && attributeNode2 != null){
				equal = equal && attributeNode1.equals(attributeNode2);
			}else{
				equal = false;
			}
		}
		
		return equal;
	}
	
	
	public <R extends EObject> List<R> getElementsWithSameName(List<R> elements) {
		List<R> result = new ArrayList<R>();
		
		int attributeIDs[] = getAttributeIDs();
		
		List<EStructuralFeature> attributes = new ArrayList<EStructuralFeature>();
		for(int i=0; i< attributeIDs.length; i++){
			attributes.add(getObject().eClass().getEStructuralFeature(attributeIDs[i]));
		}
			Iterator<R> iterator = elements.iterator();
			while(iterator.hasNext()){
				R next = iterator.next();
				
				boolean equal = true;
				for(int j=0; j<attributes.size(); j++){
//					Integer attributeAST = getASTFeature(next, attributes.get(j).getFeatureID());
//					Value valueAST = null;
//					if(attributeAST != null){
//						Object objectAST = next.eGet(next.eClass().getEStructuralFeature(attributeAST));
//						if(objectAST != null && objectAST instanceof Value){
//							valueAST = (Value)objectAST;
//						}
//					}
					
						if(getObject().eGet(attributes.get(j)) != null && getObject().eGet(attributes.get(j)).equals(next.eGet(attributes.get(j)))){
							equal = equal & true;
						}else if(next.eGet(attributes.get(j)) == null && getObject().eGet(attributes.get(j)) == null){
							equal = equal & true;
						}else{
							equal = false;
						}
					
				}
				if(equal == true){
					result.add(next);
				}
			}
		return result;
	}
	
	public List<T> getElementsWithSameName(List<T> elements, HashMap<String,String> effectiveParameters) {
		List<T> result = new ArrayList<T>();
		
		int attributeIDs[] = getAttributeIDs();
		
		List<EStructuralFeature> attributes = new ArrayList<EStructuralFeature>();
		for(int i=0; i< attributeIDs.length; i++){
			attributes.add(getObject().eClass().getEStructuralFeature(attributeIDs[i]));
		}
			Iterator<T> iterator = elements.iterator();
			while(iterator.hasNext()){
				T next = iterator.next();
				
				boolean equal = false;
				for(int j=0; j<attributes.size(); j++){
					Integer attributeAST = getASTFeature(next, attributes.get(j).getFeatureID());
					Value valueAST = null;
					if(attributeAST != null){
						Object objectAST = next.eGet(next.eClass().getEStructuralFeature(attributeAST));
						if(objectAST != null && objectAST instanceof Value){
							valueAST = (Value)objectAST;
						}
					}
					
					if(valueAST != null){
						if(getObject().eGet(attributes.get(j)) != null && getObject().eGet(attributes.get(j)).equals(valueAST.getHelper().resolve(effectiveParameters))){
							equal = true;
						}else if(getObject().eGet(attributes.get(j)) == null && valueAST.getHelper().resolve(effectiveParameters).equals("")){
							equal = true;
						}
					}else{
						if(getObject().eGet(attributes.get(j)) != null && getObject().eGet(attributes.get(j)).equals(next.eGet(attributes.get(j)))){
							equal = true;
						}else if(next.eGet(attributes.get(j)) == null){
							equal = true;
						}
					}
				}
				if(equal == true){
					result.add(next);
				}
			}
		return result;
	}
	
	
	private IMarkerHelper markerHelper = new MarkerHelper();
	public IMarkerHelper getMarkerHelper(){
		return markerHelper;
	}
	
	/**
	 * Get the first Abstract container found that hold the eObject in its children hierarchy.
	 * @param eObject
	 * @return the first AbstractComponent container found.
	 */
	static public AbstractComponent getAbstractComponentContainer(EObject eObject){
		if(eObject instanceof AbstractComponent){
			return (AbstractComponent)eObject;
		}else{
			if(eObject.eContainer() != null){
				return getAbstractComponentContainer(eObject.eContainer());
			}else{
				return null;
			}
		}
	}
	
	public AbstractComponent getAbstractComponentContainer(){
		return getAbstractComponentContainer(getObject());
	}
	
	/**
	 * Find the abstractComponent that holds the given eObject, and update
	 * the merge of its children.
	 * 
	 * @param eObject
	 */
	static public void updateChildrenMerge(EObject eObject){
		// Get the abstract component container
		AbstractComponent container = getAbstractComponentContainer(eObject);
		if(container != null){
			container.getHelper().updateChildrenMerge();
		}
	}
	
	public void updateChildrenMerge(){
		updateChildrenMerge(getObject());
	}
}
