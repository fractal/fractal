package org.ow2.fractal.f4e.fractal.util.visitor;

import java.util.Calendar;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.ui.progress.IProgressConstants;
import org.ow2.fractal.f4e.fractal.plugin.FractalPlugin;

public class LoadADLFileJob extends Job {
	private URI uri;
	
	public LoadADLFileJob(URI uri){
		super("Load ADL File");
		this.uri = uri;
		this.setProperty(IProgressConstants.ACTION_PROPERTY, new MergeAction());
		this.setProperty(IProgressConstants.KEEP_PROPERTY, true); 
		this.setProperty(IProgressConstants.NO_IMMEDIATE_ERROR_PROMPT_PROPERTY, true);
	}
	
	public void execute(){
		
	}
	
	 protected IStatus run( final IProgressMonitor monitor) {
		String name = "Load ADL File";
		final SubMonitor progress = SubMonitor.convert(monitor);
		
		
		monitor.beginTask(name, IProgressMonitor.UNKNOWN);
		
		Calendar start = Calendar.getInstance();
		
		try{
			execute();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			monitor.done();
		}
		//mergeAction.setOk(true);
		Calendar end = Calendar.getInstance();
		
		
		//complete();
		return  new Status(IStatus.OK, FractalPlugin.PLUGIN_ID, uri.lastSegment() + " loading finished : " + (end.getTimeInMillis() - start.getTimeInMillis()) + "ms");
	 }
}
