/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.Role;
import org.ow2.fractal.f4e.fractal.ValueElement;


// TODO: Auto-generated Javadoc
/**
 * The Interface AbstractComponentHelper.
 */
/**
 * @author yann
 * @model
 */
public interface IAbstractComponentHelper<T extends AbstractComponent> extends IHelper<T>, IMerge<AbstractComponent>{
	
	/**
	 * Gets the parent definition.
	 * 
	 * @return the parent definition which holds 
	 * the abstractComponent held by the helper.
	 * @model default=""
	 */
	public Definition getParentDefinition();
	
	
	/**
	 * Gets the sub component of the given name.
	 * We search only on the first level scope of the abstractComponent.
	 * We don't search in the subcomponents, or
	 * in the definitions the abstractComponent extends.
	 * 
	 * @param componentName the component name
	 * 
	 * @return the sub component
	 * @model default=""
	 */
	public Component getSubComponent(String componentName);
	

	/**
	 * Return the sub-component identified by the 'componentPath',
	 * the componentPath is just a list of components.
	 * 
	 * For example if the componentPath has two components named 'c1' and 'c2',
	 * we will search in the current component the sub-component  c1,
	 * then in c1 the sub-component c2 and we return c2.
	 * 
	 * @param componentPath
	 * @return
	 * @model default=""
	 */
	public Component getSubComponent(List<Component> componentPath, HashMap<String,String> effectiveParameters);
	
	
	/**
	 * Gets the interface of the given name.
	 * We search only on the first level scope of the abstractComponent.
	 * We don't search in the subcomponents, or
	 * in the definitions the abstractComponent extends.
	 * 
	 * Note that Fractal has collection interface,
	 * so if you pass a interface with a name like 'itf8'
	 * if we found a interface with the name 'itf' and the
	 * cardinality='collection' we return it. 
	 * 
	 * @param interfaceName the interface name
	 * 
	 * @return the interface
	 * @model default=""
	 */
	public Interface getInterface(String interfaceName);
	
	/**
	 * Search the interface in itself, its children, and 
	 * in the definitions the it extends.
	 * 
	 * @param elements resulted from the parsing. example elements for 
	 * "c1.itf2" : Constant 'c1', Constant '.', Constant 'itf2'
	 * 
	 * @return the interface
	 * @model default=""
	 */
	public Interface getInterface(Vector<ValueElement> elements);
	
	/**
	 * @param role
	 * @return the list of interfaces and merged interfaces with the role 'role'
	 * @model default=""
	 */
	public List<Interface> getInterfaces(Role role);
	
	/**
	 * update the specified feature of the abstract components which extends the
	 * abstract component associated to this helper.
	 * 
	 * @param featureID
	 * @model default=""
	 */
	public void updateChildren(int featureID);
	
	/**
	 * 
	 * @param effectiveParameters
	 * @return
	 * @model default=""
	 */
	public HashMap<String,String> getResolvedFormalParameters(HashMap<String,String> effectiveParameters);
	
	/**
	 * 
	 */
	public void updateChildrenMerge();
}
