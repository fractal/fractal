/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.ISystemHelper;
import org.ow2.fractal.f4e.fractal.System;
public class SystemAdapter extends EContentAdapter {
   	
	private SystemAdapter(){
	}
	
    /* (non-Javadoc)
     * @see org.eclipse.emf.ecore.util.EContentAdapter#notifyChanged(org.eclipse.emf.common.notify.Notification)
     */
    public void notifyChanged(Notification notification) {
    	super.notifyChanged(notification);
		/*java.lang.System.out.println("Notification : " + 
				((EObject)notification.getNotifier()).eClass().getName() +
				" " +
				EMFEventType.getInstance(notification.getEventType()).getName() +
				" " +
				((ENamedElement)notification.getFeature()).getName());
		
		Diagnostic d = Diagnostician.INSTANCE.validate((EObject)notification.getNotifier());
		*/
		
    	if(!(notification.getNotifier() instanceof System))
    		return;
    	
    	int type = notification.getEventType();
    	int featureID = notification.getFeatureID(System.class);
    	
    	System system = (System)notification.getNotifier();
    	ISystemHelper systemHelper = (ISystemHelper)HelperAdapterFactory.getInstance().adapt(system);
    	
    	switch(featureID){
    		case FractalPackage.SYSTEM__DEFINITIONS:
    			
    			switch(type){
    				case Notification.ADD:
    					
    				break;
    				
    				case Notification.ADD_MANY:
    					
        			break;
        			
    				case Notification.REMOVE:
    				
    				break;
    				
    				case Notification.REMOVE_MANY:
    				break;
    			}
    		break;
    	}
    }
    
    private static class SingletonHolder {
		private static SystemAdapter instance = new SystemAdapter();
	}
    
    public static SystemAdapter getInstance(){
    	return SingletonHolder.instance;
    }
}
