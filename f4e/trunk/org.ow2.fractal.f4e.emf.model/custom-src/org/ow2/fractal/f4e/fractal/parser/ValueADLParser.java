// $ANTLR 3.0.1 custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3 2009-06-11 16:09:16

  package org.ow2.fractal.f4e.fractal.parser;
  
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ow2.fractal.f4e.fractal.Constant;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.FormalParameter;
import org.ow2.fractal.f4e.fractal.FractalFactory;
import org.ow2.fractal.f4e.fractal.Reference;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.ValueElement;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IDefinitionHelper;




import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;


import org.antlr.runtime.tree.*;

public class ValueADLParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "STRING", "RESTRICTED_STRING", "REFERENCE", "ESC", "NEWLINE", "WS"
    };
    public static final int WS=9;
    public static final int NEWLINE=8;
    public static final int ESC=7;
    public static final int RESTRICTED_STRING=5;
    public static final int REFERENCE=6;
    public static final int EOF=-1;
    public static final int STRING=4;

        public ValueADLParser(TokenStream input) {
            super(input);
        }
        
    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    public TreeAdaptor getTreeAdaptor() {
        return adaptor;
    }

    public String[] getTokenNames() { return tokenNames; }
    public String getGrammarFileName() { return "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3"; }


    	Stack<Object> stack = new Stack<Object>();
    	Definition definition = null;
    	IDefinitionHelper helper = null;
    	
    	public ValueADLParser(TokenStream input, Definition contextDefinition){
    		super(input);
    		this.definition = contextDefinition;
    		
    		if(definition != null){
    			helper = (IDefinitionHelper)HelperAdapterFactory.getInstance().adapt(definition);
    		}
    	}
    	
    	public Value getValue() throws Exception{
    		// launch the parser
    	
    		value();	
    		
    		Value value = FractalFactory.eINSTANCE.createValue();
    		value.setContextDefinition(definition);
    		
    		popValueElements(value);
    		
    		return value;
    	}
    	
    	private void popValueElements(Value value){
    		if(!stack.empty()){
    			Object object = stack.pop();
    			if(object instanceof ValueElement){
    				popValueElements(value);
    				value.getElements().add((ValueElement)object);
    			}
    		}
    	}
    	
    	public void pushReference(String reference){
    		Reference eReference = FractalFactory.eINSTANCE.createReference();
    		if(helper != null){
    			FormalParameter formalParameter = helper.getParameter(reference);
    			eReference.setReference(formalParameter);
    		}	
    		eReference.setName(reference);
    		// TODO add warning when there is not parameter found in the definition
    		stack.push(eReference);
    	}
    	
    	public void pushConstant(String constant){
    		Constant eConstant = FractalFactory.eINSTANCE.createConstant();
    		eConstant.setConstant(constant);
    		stack.push(eConstant);
    	}
    	


    public static class value_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start value
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:89:1: value : (ref= reference | str= string )* ;
    public final value_return value() throws RecognitionException {
        value_return retval = new value_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        reference_return ref = null;

        string_return str = null;



        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:90:2: ( (ref= reference | str= string )* )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:90:4: (ref= reference | str= string )*
            {
            root_0 = (CommonTree)adaptor.nil();

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:90:4: (ref= reference | str= string )*
            loop1:
            do {
                int alt1=3;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==REFERENCE) ) {
                    alt1=1;
                }
                else if ( ((LA1_0>=STRING && LA1_0<=RESTRICTED_STRING)) ) {
                    alt1=2;
                }


                switch (alt1) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:90:5: ref= reference
            	    {
            	    pushFollow(FOLLOW_reference_in_value59);
            	    ref=reference();
            	    _fsp--;

            	    adaptor.addChild(root_0, ref.getTree());
            	    pushReference(input.toString(ref.start,ref.stop));

            	    }
            	    break;
            	case 2 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:90:49: str= string
            	    {
            	    pushFollow(FOLLOW_string_in_value67);
            	    str=string();
            	    _fsp--;

            	    adaptor.addChild(root_0, str.getTree());
            	    pushConstant(input.toString(str.start,str.stop));

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end value

    public static class string_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start string
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:93:1: string : ( STRING | RESTRICTED_STRING );
    public final string_return string() throws RecognitionException {
        string_return retval = new string_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token set1=null;

        CommonTree set1_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:94:2: ( STRING | RESTRICTED_STRING )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:
            {
            root_0 = (CommonTree)adaptor.nil();

            set1=(Token)input.LT(1);
            if ( (input.LA(1)>=STRING && input.LA(1)<=RESTRICTED_STRING) ) {
                input.consume();
                adaptor.addChild(root_0, adaptor.create(set1));
                errorRecovery=false;
            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recoverFromMismatchedSet(input,mse,FOLLOW_set_in_string0);    throw mse;
            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end string

    public static class reference_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start reference
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:97:1: reference : REFERENCE ;
    public final reference_return reference() throws RecognitionException {
        reference_return retval = new reference_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token REFERENCE2=null;

        CommonTree REFERENCE2_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:98:2: ( REFERENCE )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:98:4: REFERENCE
            {
            root_0 = (CommonTree)adaptor.nil();

            REFERENCE2=(Token)input.LT(1);
            match(input,REFERENCE,FOLLOW_REFERENCE_in_reference97); 
            REFERENCE2_tree = (CommonTree)adaptor.create(REFERENCE2);
            adaptor.addChild(root_0, REFERENCE2_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end reference


 

    public static final BitSet FOLLOW_reference_in_value59 = new BitSet(new long[]{0x0000000000000072L});
    public static final BitSet FOLLOW_string_in_value67 = new BitSet(new long[]{0x0000000000000072L});
    public static final BitSet FOLLOW_set_in_string0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_REFERENCE_in_reference97 = new BitSet(new long[]{0x0000000000000002L});

}