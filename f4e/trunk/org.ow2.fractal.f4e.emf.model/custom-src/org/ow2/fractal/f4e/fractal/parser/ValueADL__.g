lexer grammar ValueADL;
@header {
  package org.ow2.fractal.f4e.fractal.parser;
  
}

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3" 101
REFERENCE 
	: '${' ( options {greedy=false;} : 'a'..'z' | 'A'..'Z' | '0'..'9' | '-' | '_' )+ '}' {setText(getText().substring(2, getText().length()-1));} 
	;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3" 105
STRING 
	: '\'' (ESC | ~('\\' | '\''))* '\''
	;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3" 109
protected
ESC 
	: '\\' .
	;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3" 114
RESTRICTED_STRING : ( ~('$' | '{' | '}'))+ ;


// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3" 117
NEWLINE : '\r'? '\n' ;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3" 119
WS : (' ' | '\t' | '\n' | '\r')+ {skip();} ;