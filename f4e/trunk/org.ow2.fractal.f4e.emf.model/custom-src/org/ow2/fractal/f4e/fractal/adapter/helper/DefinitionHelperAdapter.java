/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.TokenRewriteStream;
import org.antlr.runtime.tree.CommonTree;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.RecordingCommand;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.FormalParameter;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.parser.ArgumentsADLLexer;
import org.ow2.fractal.f4e.fractal.parser.ArgumentsADLParser;
import org.ow2.fractal.f4e.fractal.parser.ExtendsADLLexer;
import org.ow2.fractal.f4e.fractal.parser.ExtendsADLParser;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.util.SDefinitionLoadLocator;


public class DefinitionHelperAdapter extends AbstractComponentHelperAdapter<Definition> implements IDefinitionHelper{	
	public DefinitionHelperAdapter(Definition definition){
		super(definition);
	}
	
	public boolean isAdapterForType(Object type) {
		return  super.isAdapterForType(type) || type == IDefinitionHelper.class;
	}
	
	protected void updateName(){
		t.setNameAST(ValueHelperAdapter.getValue(t.getName(), t));
	}
	
	/* (non-Javadoc)
	 * @see org.ow2.fractal.adl.extended.fractalextended.adapter.helper.DefinitionHelper#updateArguments()
	 */
	protected void updateArguments(){
		String argumentsADL = t.getArguments();
		t.getArgumentsAST().clear();
		
		if(argumentsADL == null || argumentsADL.equals(""))
			return;
		
		try {
				
			CharStream input = new ANTLRStringStream(argumentsADL);
			ArgumentsADLLexer lexer = new ArgumentsADLLexer(input);
			
			TokenRewriteStream tokens = new TokenRewriteStream(lexer);
			ArgumentsADLParser grammar = new ArgumentsADLParser(tokens,t);
			grammar.prog();
			
			}catch(Exception e){
				e.printStackTrace();
			}
	}
	
	private void cleanExtends(){
		// clean the dependencies to the Extends definition
		//Each time we modify the extendsADL string of a definition
		//we must clean the extends relation and recreate it.
		
		EList<DefinitionCall> definitions = t.getExtendsAST();
		Iterator<DefinitionCall> iterator = definitions.iterator();
		t.getExtendsAST().clear();
	}
	
	protected static boolean isConflict(Resource resource, String definitionName){
		URI uri = SDefinitionLoadLocator.getInstance().getLoadLocation(resource.getURI(),definitionName);
		return uri.toString().equals(resource.getURI().toString());
	}
	
	
	private static final ExtendsADLLexer lexer = new ExtendsADLLexer();
	private static final TokenRewriteStream tokens = new TokenRewriteStream();
	private final ExtendsADLParser grammar = new ExtendsADLParser(tokens,t);
	
	/**
	 * parse the extendsADL string to create in the model
	 * the associated DefinitionCall elements.
	 */
	protected void updateExtends(){
		String extendsADL = t.getExtends();
		//cleanExtends();
		
		if(extendsADL == null || extendsADL.equals("")){
			cleanExtends();
			return;
		}
		
		try {
			ArrayList<DefinitionCall> toRemove = new ArrayList<DefinitionCall>();
			toRemove.addAll(t.getExtendsAST());
			CharStream input = new ANTLRStringStream(extendsADL);
			lexer.setCharStream(input);
		
			TokenRewriteStream tokens = new TokenRewriteStream(lexer);
			//tokens.setTokenSource(lexer);
			grammar.setTokenStream(tokens);
			//ExtendsADLParser grammar = new ExtendsADLParser(tokens,t);
			grammar.prog();
			t.getExtendsAST().removeAll(toRemove);
			}catch(Exception e){
				e.printStackTrace();
			}
	}
	
	public FormalParameter getParameter(String name){
		EList<FormalParameter> eList = t.getArgumentsAST();
		
		Iterator<FormalParameter> iterator = eList.iterator();
		
		while(iterator.hasNext()){
			FormalParameter formalParameter = iterator.next();
			IParameterHelper helper = (IParameterHelper)HelperAdapterFactory.getInstance().adapt(formalParameter);
			if(helper != null){
				if(helper.getName().equals(name)){
					return formalParameter;
				}
			}
		}
		
		return null;
	}
	
	public HashMap<String,String> getArguments(){
		HashMap<String, String> result = new HashMap<String,String>();
		Iterator<FormalParameter> iterator = getObject().getArgumentsAST().iterator();
		while(iterator.hasNext()){
			FormalParameter next = iterator.next();
			String name = next.getName().getHelper().toString();
			String value = "";
			if(next.getValue() != null){
				value = next.getValue().getHelper().toString();
			}
			result.put(name, value);
		}
		return result;
	}
	
	/**
	 * Search in the definition helped by this helper
	 * the component pointed by the path 'path'.
	 * 
	 * ex :
	 * this/c1  : return the component name c1. c1 is at the root level.  
	 * c1/c2    : return the component c2 subcomponent of c1. this is not needed
	 * then the component is at a deeper level than the root level.
	 */
	public Component getComponent(String path, boolean lookInMerged){
		Component resultComponent = null;
		Vector<String> splits = new Vector<String>(java.util.Arrays.asList(path.split("/")));
	
		if(splits != null){
			if(splits.get(0).equals("this") || splits.get(0).equals(".")){
				splits.remove(0);
			}
		}
		
		resultComponent = getComponent(t,splits, lookInMerged);
		
		return resultComponent;
	}
	
	private Component getComponent(AbstractComponent abstractComponent, Vector<String> path, boolean lookInMerged){
		Component resultComponent = null;
		
		if(path.isEmpty()){
			return null;
		}
		
		EList<Component> subComponents = abstractComponent.getSubComponents();
		if(lookInMerged){
			subComponents.addAll(abstractComponent.getMergedSubComponents());
		}
		Iterator<Component> iterator = subComponents.iterator();
		
		String componentName = path.firstElement();
		path.remove(path.firstElement());
		
		boolean notFound = true;
		while(iterator.hasNext() && notFound){
			Component component = iterator.next();
			if(component.getName().equals(componentName)){
				notFound = false;
				resultComponent = component;
			}
		}
		
		if(resultComponent != null && !path.isEmpty()){
			resultComponent = getComponent(resultComponent,path,lookInMerged);
		}
		
		return resultComponent;
	}
	
	public void printTree(CommonTree t, int indent) {
		if ( t != null ) {
			StringBuffer sb = new StringBuffer(indent);
			for ( int i = 0; i < indent; i++ )
				sb = sb.append("   ");
			for ( int i = 0; i < t.getChildCount(); i++ ) {
				java.lang.System.out.println(sb.toString() + t.getChild(i).toString());
				printTree((CommonTree)t.getChild(i), indent+1);
			}
		}
	}
//	static final TreeAdaptor adaptor = new CommonTreeAdaptor() {
//		public Object create(Token payload) {
//			return new CommonTree(payload);
//		}
//	};
	
	/**
	 * Return a list containing definition parents.
	 * For example if a definition D1 extends D2,D3 it
	 * will return the list [D2,D3]. Grand-parents are not included. 
	 */
	public List<Definition> getParents(){
		List<Definition> result = new ArrayList<Definition>();
		
		Iterator<DefinitionCall> iterator = t.getExtendsAST().iterator();
		while(iterator.hasNext()){
			DefinitionCall definitionCall = iterator.next();
			if(definitionCall != null && definitionCall.getDefinition() != null){
				result.add(definitionCall.getDefinition());
			}
		}
		
		return result;
	}
	
	public void update(final int featureID, IProgressMonitor monitor){
		Command cmd = new RecordingCommand(FractalTransactionalEditingDomain
				.getEditingDomain()) {
			protected void doExecute() {
				switch (featureID) {
				case FractalPackage.DEFINITION__EXTENDS_AST:
					updateExtends();
					break;
				case FractalPackage.DEFINITION__ARGUMENTS_AST:
					updateArguments();
					break;
				case FractalPackage.DEFINITION__NAME_AST:
					updateName();
				default:
					break;
				}
			}
		};
		
		switch (featureID) {
			case FractalPackage.DEFINITION__ARGUMENTS_AST:
			case FractalPackage.DEFINITION__EXTENDS_AST:
			case FractalPackage.DEFINITION__NAME_AST:
				FractalTransactionalEditingDomain.getEditingDomain().getCommandStack()
				.execute(cmd);
				break;
			default:
				super.update(featureID, monitor);
				break;
		}
	}
}
