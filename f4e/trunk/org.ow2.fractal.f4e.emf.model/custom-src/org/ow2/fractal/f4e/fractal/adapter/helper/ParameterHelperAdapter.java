/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter.helper;

import org.eclipse.core.runtime.IProgressMonitor;
import org.ow2.fractal.f4e.fractal.Parameter;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;

public class ParameterHelperAdapter extends HelperAdapter<Parameter> implements IParameterHelper<Parameter>{
	public ParameterHelperAdapter(Parameter parameter){
		super(parameter);
	}

	public String getName(){
		Value value = t.getName();
		IValueHelper helper = (IValueHelper)HelperAdapterFactory.getInstance().adapt(value);
		
		if(helper != null){
			return helper.toString();
		}
		
		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.ow2.fractal.adl.extended.fractalextended.adapter.helper.ParameterHelper#getValue()
	 */
	public String getValue(){
		Value value = t.getValue();
		IValueHelper helper = (IValueHelper)HelperAdapterFactory.getInstance().adapt(value);
		
		if(helper != null){
			return helper.toString();
		}
		
		return null;
	}
	
	
	public boolean isAdapterForType(Object type) {
		return type == ParameterHelperAdapter.class;
	}

	public void update(int featureID, IProgressMonitor monitor){
		
	}
}
