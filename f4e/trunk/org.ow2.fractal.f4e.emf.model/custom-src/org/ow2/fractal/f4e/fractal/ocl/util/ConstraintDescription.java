package org.ow2.fractal.f4e.fractal.ocl.util;

import java.util.ArrayList;
import java.util.List;

public class ConstraintDescription {
	List<String> commentLines = new ArrayList<String>();
	
	public void addCommentLine(String line) {
		commentLines.add(line);
	}
	
	public String getDescription(){
		String result = new String();
		for(String line: commentLines){
			result += line;
		}
		return result;
	}
};
