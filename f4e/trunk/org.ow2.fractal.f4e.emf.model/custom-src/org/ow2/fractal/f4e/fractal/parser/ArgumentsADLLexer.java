// $ANTLR 3.0.1 custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3 2009-06-11 16:09:15

  package org.ow2.fractal.f4e.fractal.parser;
  
 


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class ArgumentsADLLexer extends Lexer {
    public static final int WS=7;
    public static final int NEWLINE=6;
    public static final int T8=8;
    public static final int T9=9;
    public static final int VALUE=5;
    public static final int Tokens=10;
    public static final int EOF=-1;
    public static final int ARGUMENT_STRING=4;
    public ArgumentsADLLexer() {;} 
    public ArgumentsADLLexer(CharStream input) {
        super(input);
    }
    public String getGrammarFileName() { return "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3"; }

    // $ANTLR start T8
    public final void mT8() throws RecognitionException {
        try {
            int _type = T8;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:8:4: ( ',' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:8:6: ','
            {
            match(','); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T8

    // $ANTLR start T9
    public final void mT9() throws RecognitionException {
        try {
            int _type = T9;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:9:4: ( '=' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:9:6: '='
            {
            match('='); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T9

    // $ANTLR start VALUE
    public final void mVALUE() throws RecognitionException {
        try {
            int _type = VALUE;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:102:2: ( '\\'' ( options {greedy=false; } : . )* '\\'' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:102:4: '\\'' ( options {greedy=false; } : . )* '\\''
            {
            match('\''); 
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:102:9: ( options {greedy=false; } : . )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0=='\'') ) {
                    alt1=2;
                }
                else if ( ((LA1_0>='\u0000' && LA1_0<='&')||(LA1_0>='(' && LA1_0<='\uFFFE')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:102:37: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            match('\''); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end VALUE

    // $ANTLR start ARGUMENT_STRING
    public final void mARGUMENT_STRING() throws RecognitionException {
        try {
            int _type = ARGUMENT_STRING;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:105:17: ( (~ ( '=' | '\\'' | '\\\"' ) )+ )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:105:19: (~ ( '=' | '\\'' | '\\\"' ) )+
            {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:105:19: (~ ( '=' | '\\'' | '\\\"' ) )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>='\u0000' && LA2_0<='!')||(LA2_0>='#' && LA2_0<='&')||(LA2_0>='(' && LA2_0<='<')||(LA2_0>='>' && LA2_0<='\uFFFE')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:105:20: ~ ( '=' | '\\'' | '\\\"' )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='<')||(input.LA(1)>='>' && input.LA(1)<='\uFFFE') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end ARGUMENT_STRING

    // $ANTLR start NEWLINE
    public final void mNEWLINE() throws RecognitionException {
        try {
            int _type = NEWLINE;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:107:9: ( ( '\\r' )? '\\n' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:107:11: ( '\\r' )? '\\n'
            {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:107:11: ( '\\r' )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0=='\r') ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:107:11: '\\r'
                    {
                    match('\r'); 

                    }
                    break;

            }

            match('\n'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end NEWLINE

    // $ANTLR start WS
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:109:4: ( ( ' ' | '\\t' | '\\n' | '\\r' )+ )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:109:6: ( ' ' | '\\t' | '\\n' | '\\r' )+
            {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:109:6: ( ' ' | '\\t' | '\\n' | '\\r' )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>='\t' && LA4_0<='\n')||LA4_0=='\r'||LA4_0==' ') ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);

            skip();

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end WS

    public void mTokens() throws RecognitionException {
        // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:1:8: ( T8 | T9 | VALUE | ARGUMENT_STRING | NEWLINE | WS )
        int alt5=6;
        alt5 = dfa5.predict(input);
        switch (alt5) {
            case 1 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:1:10: T8
                {
                mT8(); 

                }
                break;
            case 2 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:1:13: T9
                {
                mT9(); 

                }
                break;
            case 3 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:1:16: VALUE
                {
                mVALUE(); 

                }
                break;
            case 4 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:1:22: ARGUMENT_STRING
                {
                mARGUMENT_STRING(); 

                }
                break;
            case 5 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:1:38: NEWLINE
                {
                mNEWLINE(); 

                }
                break;
            case 6 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:1:46: WS
                {
                mWS(); 

                }
                break;

        }

    }


    protected DFA5 dfa5 = new DFA5(this);
    static final String DFA5_eotS =
        "\1\uffff\1\10\2\uffff\3\7\2\uffff";
    static final String DFA5_eofS =
        "\11\uffff";
    static final String DFA5_minS =
        "\2\0\2\uffff\3\11\2\uffff";
    static final String DFA5_maxS =
        "\2\ufffe\2\uffff\3\40\2\uffff";
    static final String DFA5_acceptS =
        "\2\uffff\1\2\1\3\3\uffff\1\4\1\1";
    static final String DFA5_specialS =
        "\11\uffff}>";
    static final String[] DFA5_transitionS = {
            "\11\7\1\6\1\5\2\7\1\4\22\7\1\6\1\7\1\uffff\4\7\1\3\4\7\1\1\20"+
            "\7\1\2\uffc1\7",
            "\42\7\1\uffff\4\7\1\uffff\25\7\1\uffff\uffc1\7",
            "",
            "",
            "\1\6\1\5\2\uffff\1\6\22\uffff\1\6",
            "\2\6\2\uffff\1\6\22\uffff\1\6",
            "\2\6\2\uffff\1\6\22\uffff\1\6",
            "",
            ""
    };

    static final short[] DFA5_eot = DFA.unpackEncodedString(DFA5_eotS);
    static final short[] DFA5_eof = DFA.unpackEncodedString(DFA5_eofS);
    static final char[] DFA5_min = DFA.unpackEncodedStringToUnsignedChars(DFA5_minS);
    static final char[] DFA5_max = DFA.unpackEncodedStringToUnsignedChars(DFA5_maxS);
    static final short[] DFA5_accept = DFA.unpackEncodedString(DFA5_acceptS);
    static final short[] DFA5_special = DFA.unpackEncodedString(DFA5_specialS);
    static final short[][] DFA5_transition;

    static {
        int numStates = DFA5_transitionS.length;
        DFA5_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA5_transition[i] = DFA.unpackEncodedString(DFA5_transitionS[i]);
        }
    }

    class DFA5 extends DFA {

        public DFA5(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 5;
            this.eot = DFA5_eot;
            this.eof = DFA5_eof;
            this.min = DFA5_min;
            this.max = DFA5_max;
            this.accept = DFA5_accept;
            this.special = DFA5_special;
            this.transition = DFA5_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T8 | T9 | VALUE | ARGUMENT_STRING | NEWLINE | WS );";
        }
    }
 

}