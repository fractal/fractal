/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter.helper;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.transaction.RecordingCommand;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.AttributesController;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;


// TODO: Auto-generated Javadoc
/**
 * The Class AttributesControllerHelperAdapter.
 */
public class AttributesControllerHelperAdapter extends HelperAdapter<AttributesController>
	implements IAttributesControllerHelper {
	
	/**
	 * Instantiates a new attributes controller helper adapter.
	 * 
	 * @param attributesController the attributes controller
	 */
	public AttributesControllerHelperAdapter(AttributesController attributesController) {
		super(attributesController);
	}

	/* (non-Javadoc)
	 * @see org.ow2.fractal.adl.extended.fractalextended.adapter.helper.HelperAdapter#isAdapterForType(java.lang.Object)
	 */
	
	public boolean isAdapterForType(Object type) {
		return super.isAdapterForType(type) || type == IAttributesControllerHelper.class;
	}
	
	
	public void notifyChanged(Notification notification) {
		// TODO Auto-generated method stub
		super.notifyChanged(notification);
		
    	if(notification.getNotifier() != this.getObject()){
    		return;
    	}
    	
    	int type = notification.getEventType();
    	int featureID = notification.getFeatureID(AttributesController.class);
    	
    	switch(type){
    	case Notification.SET:
    	case Notification.UNSET:
    	case Notification.ADD:
    	case Notification.ADD_MANY:
    	case Notification.REMOVE:
    	case Notification.REMOVE_MANY:
    		switch(featureID){
    		case FractalPackage.ATTRIBUTES_CONTROLLER__SIGNATURE_AST:
    		case FractalPackage.ATTRIBUTES_CONTROLLER__ATTRIBUTES:
    			AbstractComponent parent = this.getObject().getAbstractComponent();
    			if(parent != null){
//    				parent.getHelper().update(FractalPackage.ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER);
//  				parent.getHelper().updateChildren(FractalPackage.ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER);
    			}	
    		default:	
    			break;
    		}
    		break;
    	}
	}

	
	protected void updateSignature(){
		t.setSignatureAST(ValueHelperAdapter.getValue(t.getSignature(), (AbstractComponent)t.eContainer()));
	}
	
	public void update(final int featureID, IProgressMonitor monitor){
		Command cmd = new RecordingCommand(FractalTransactionalEditingDomain
				.getEditingDomain()) {
			protected void doExecute() {
				switch (featureID) {
					case FractalPackage.ATTRIBUTES_CONTROLLER__SIGNATURE_AST:
							updateSignature();
						break;
					default:
						break;
				}
			}
		};

		FractalTransactionalEditingDomain.getEditingDomain().getCommandStack()
				.execute(cmd);
	}
	
}
