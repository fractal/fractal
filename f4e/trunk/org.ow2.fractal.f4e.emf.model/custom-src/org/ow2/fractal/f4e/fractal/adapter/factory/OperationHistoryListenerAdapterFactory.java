package org.ow2.fractal.f4e.fractal.adapter.factory;

import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.ow2.fractal.f4e.fractal.adapter.OperationHistoryListenerAdapter;

public class OperationHistoryListenerAdapterFactory extends AdapterFactoryImpl {

	/**
	 * Gets the single instance of HelperAdapterFactory.
	 * 
	 * @return single instance of HelperAdapterFactory
	 */
	public static OperationHistoryListenerAdapterFactory getInstance(){
		return SingletonHolder.instance;
	}
	
	/**
	 * The Class SingletonHolder.
	 */
	private static class SingletonHolder {
		
		/** The instance. */
		private static OperationHistoryListenerAdapterFactory instance = new OperationHistoryListenerAdapterFactory();
	}
	
	public boolean isFactoryForType(Object type) {
		// TODO Auto-generated method stub
		return type instanceof OperationHistoryListenerAdapter;
	}

	
	protected OperationHistoryListenerAdapter createAdapter(Notifier target) {
		// TODO Auto-generated method stub
		return new OperationHistoryListenerAdapter();
	}


	public OperationHistoryListenerAdapter adapt(Resource target) {
		// TODO Auto-generated method stub
		return (OperationHistoryListenerAdapter)super.adapt(target, OperationHistoryListenerAdapter.class);
	}
	
}
