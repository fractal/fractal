package org.ow2.fractal.f4e.fractal.util;

import java.util.List;

import org.eclipse.emf.common.util.URI;

public interface IDefinitionLocator {
	public URI getLoadLocation(String definitionName);
	public void addLocation(String location);
	public void addLocations(List<String> locations);
}
