lexer grammar OCLComments;
@header {
  package org.ow2.fractal.f4e.fractal.parser;
  
}

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3" 82
CONSTRAINT_LINE: '--' WS 'description:' STRING{setText($STRING.text);} '\n';
// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3" 83
SEVERITY_LINE: '--' WS 'severity:' SEVERITY WS* '\n';

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3" 85
SEVERITY: WS ('ERROR' | 'WARNING');

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3" 87
COMMENT_LINE: '--' WS ~('description:') STRING{setText($STRING.text);} '\n'; 

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3" 89
CODE_LINE:  STRING '\n'; 

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3" 91
STRING: (WS | CHARACTER)*;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3" 93
CHARACTER: ~('\t' | ' ' | '\r' | '\n');

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3" 95
WS: ('\t' | ' ' )+ ;