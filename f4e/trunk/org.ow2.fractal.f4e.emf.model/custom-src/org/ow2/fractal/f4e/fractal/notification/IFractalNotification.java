package org.ow2.fractal.f4e.fractal.notification;

import org.eclipse.emf.common.notify.Notification;

public interface IFractalNotification {
	int SET_MERGED_INTERFACE_ROLE = Notification.EVENT_TYPE_COUNT +1;
	int UPDATE_MERGED_ELEMENTS = Notification.EVENT_TYPE_COUNT +2;
	int UPDATE_VALIDATION_DECORATION = Notification.EVENT_TYPE_COUNT +3;
	int UPDATE_TREE_EDITOR = Notification.EVENT_TYPE_COUNT +4;
	int SET_IS_SHARED_COMPONENT = Notification.EVENT_TYPE_COUNT +5;
}
