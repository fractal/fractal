// $ANTLR 3.0.1 custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3 2009-06-11 16:09:16

  package org.ow2.fractal.f4e.fractal.parser;
  


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class InterfaceADLLexer extends Lexer {
    public static final int WS=9;
    public static final int NEWLINE=8;
    public static final int ESC=7;
    public static final int RESTRICTED_STRING=5;
    public static final int REFERENCE=6;
    public static final int T10=10;
    public static final int Tokens=11;
    public static final int EOF=-1;
    public static final int STRING=4;
    public InterfaceADLLexer() {;} 
    public InterfaceADLLexer(CharStream input) {
        super(input);
    }
    public String getGrammarFileName() { return "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3"; }

    // $ANTLR start T10
    public final void mT10() throws RecognitionException {
        try {
            int _type = T10;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:7:5: ( '.' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:7:7: '.'
            {
            match('.'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T10

    // $ANTLR start REFERENCE
    public final void mREFERENCE() throws RecognitionException {
        try {
            int _type = REFERENCE;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:100:2: ( '${' ( options {greedy=false; } : ~ ( '=' | '\\'' | '\\\"' ) )+ '}' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:100:4: '${' ( options {greedy=false; } : ~ ( '=' | '\\'' | '\\\"' ) )+ '}'
            {
            match("${"); 

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:100:9: ( options {greedy=false; } : ~ ( '=' | '\\'' | '\\\"' ) )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0=='}') ) {
                    alt1=2;
                }
                else if ( ((LA1_0>='\u0000' && LA1_0<='!')||(LA1_0>='#' && LA1_0<='&')||(LA1_0>='(' && LA1_0<='<')||(LA1_0>='>' && LA1_0<='|')||(LA1_0>='~' && LA1_0<='\uFFFE')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:100:37: ~ ( '=' | '\\'' | '\\\"' )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='<')||(input.LA(1)>='>' && input.LA(1)<='\uFFFE') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);

            match('}'); 
            setText(getText().substring(2, getText().length()-1));

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end REFERENCE

    // $ANTLR start STRING
    public final void mSTRING() throws RecognitionException {
        try {
            int _type = STRING;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:104:2: ( '\\'' ( ESC | ~ ( '\\\\' | '\\'' ) )* '\\'' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:104:4: '\\'' ( ESC | ~ ( '\\\\' | '\\'' ) )* '\\''
            {
            match('\''); 
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:104:9: ( ESC | ~ ( '\\\\' | '\\'' ) )*
            loop2:
            do {
                int alt2=3;
                int LA2_0 = input.LA(1);

                if ( (LA2_0=='\\') ) {
                    alt2=1;
                }
                else if ( ((LA2_0>='\u0000' && LA2_0<='&')||(LA2_0>='(' && LA2_0<='[')||(LA2_0>=']' && LA2_0<='\uFFFE')) ) {
                    alt2=2;
                }


                switch (alt2) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:104:10: ESC
            	    {
            	    mESC(); 

            	    }
            	    break;
            	case 2 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:104:16: ~ ( '\\\\' | '\\'' )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFE') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            match('\''); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end STRING

    // $ANTLR start ESC
    public final void mESC() throws RecognitionException {
        try {
            int _type = ESC;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:109:2: ( '\\\\' . )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:109:4: '\\\\' .
            {
            match('\\'); 
            matchAny(); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end ESC

    // $ANTLR start RESTRICTED_STRING
    public final void mRESTRICTED_STRING() throws RecognitionException {
        try {
            int _type = RESTRICTED_STRING;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:112:19: ( (~ ( '\\.' ) )+ )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:112:21: (~ ( '\\.' ) )+
            {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:112:21: (~ ( '\\.' ) )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>='\u0000' && LA3_0<='-')||(LA3_0>='/' && LA3_0<='\uFFFE')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:112:22: ~ ( '\\.' )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='-')||(input.LA(1)>='/' && input.LA(1)<='\uFFFE') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end RESTRICTED_STRING

    // $ANTLR start NEWLINE
    public final void mNEWLINE() throws RecognitionException {
        try {
            int _type = NEWLINE;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:115:9: ( ( '\\r' )? '\\n' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:115:11: ( '\\r' )? '\\n'
            {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:115:11: ( '\\r' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='\r') ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:115:11: '\\r'
                    {
                    match('\r'); 

                    }
                    break;

            }

            match('\n'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end NEWLINE

    // $ANTLR start WS
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:117:4: ( ( ' ' | '\\t' | '\\n' | '\\r' )+ )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:117:6: ( ' ' | '\\t' | '\\n' | '\\r' )+
            {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:117:6: ( ' ' | '\\t' | '\\n' | '\\r' )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>='\t' && LA5_0<='\n')||LA5_0=='\r'||LA5_0==' ') ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);

            skip();

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end WS

    public void mTokens() throws RecognitionException {
        // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:1:8: ( T10 | REFERENCE | STRING | ESC | RESTRICTED_STRING | NEWLINE | WS )
        int alt6=7;
        alt6 = dfa6.predict(input);
        switch (alt6) {
            case 1 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:1:10: T10
                {
                mT10(); 

                }
                break;
            case 2 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:1:14: REFERENCE
                {
                mREFERENCE(); 

                }
                break;
            case 3 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:1:24: STRING
                {
                mSTRING(); 

                }
                break;
            case 4 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:1:31: ESC
                {
                mESC(); 

                }
                break;
            case 5 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:1:35: RESTRICTED_STRING
                {
                mRESTRICTED_STRING(); 

                }
                break;
            case 6 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:1:53: NEWLINE
                {
                mNEWLINE(); 

                }
                break;
            case 7 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:1:61: WS
                {
                mWS(); 

                }
                break;

        }

    }


    protected DFA6 dfa6 = new DFA6(this);
    static final String DFA6_eotS =
        "\2\uffff\6\10\1\uffff\3\10\1\15\1\uffff\1\17\1\uffff\1\10\1\uffff"+
        "\1\10\1\21";
    static final String DFA6_eofS =
        "\24\uffff";
    static final String DFA6_minS =
        "\1\0\1\uffff\1\173\2\0\3\11\1\uffff\4\0\1\uffff\1\0\1\uffff\1\0"+
        "\1\uffff\2\0";
    static final String DFA6_maxS =
        "\1\ufffe\1\uffff\1\173\2\ufffe\3\40\1\uffff\4\ufffe\1\uffff\1\ufffe"+
        "\1\uffff\1\ufffe\1\uffff\2\ufffe";
    static final String DFA6_acceptS =
        "\1\uffff\1\1\6\uffff\1\5\4\uffff\1\3\1\uffff\1\4\1\uffff\1\2\2\uffff";
    static final String DFA6_specialS =
        "\24\uffff}>";
    static final String[] DFA6_transitionS = {
            "\11\10\1\7\1\6\2\10\1\5\22\10\1\7\3\10\1\2\2\10\1\3\6\10\1\1"+
            "\55\10\1\4\uffa2\10",
            "",
            "\1\11",
            "\47\13\1\14\6\13\1\15\55\13\1\12\uffa2\13",
            "\56\16\1\17\uffd0\16",
            "\1\7\1\6\2\uffff\1\7\22\uffff\1\7",
            "\2\7\2\uffff\1\7\22\uffff\1\7",
            "\2\7\2\uffff\1\7\22\uffff\1\7",
            "",
            "\42\20\1\uffff\4\20\1\uffff\6\20\1\21\16\20\1\uffff\uffc1\20",
            "\56\22\1\15\uffd0\22",
            "\47\13\1\14\6\13\1\15\55\13\1\12\uffa2\13",
            "\56\10\1\uffff\uffd0\10",
            "",
            "\56\10\1\uffff\uffd0\10",
            "",
            "\42\20\1\uffff\4\20\1\uffff\6\20\1\21\16\20\1\uffff\77\20\1"+
            "\23\uff81\20",
            "",
            "\47\13\1\14\6\13\1\15\55\13\1\12\uffa2\13",
            "\42\20\1\10\4\20\1\10\6\20\1\uffff\16\20\1\10\77\20\1\23\uff81"+
            "\20"
    };

    static final short[] DFA6_eot = DFA.unpackEncodedString(DFA6_eotS);
    static final short[] DFA6_eof = DFA.unpackEncodedString(DFA6_eofS);
    static final char[] DFA6_min = DFA.unpackEncodedStringToUnsignedChars(DFA6_minS);
    static final char[] DFA6_max = DFA.unpackEncodedStringToUnsignedChars(DFA6_maxS);
    static final short[] DFA6_accept = DFA.unpackEncodedString(DFA6_acceptS);
    static final short[] DFA6_special = DFA.unpackEncodedString(DFA6_specialS);
    static final short[][] DFA6_transition;

    static {
        int numStates = DFA6_transitionS.length;
        DFA6_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA6_transition[i] = DFA.unpackEncodedString(DFA6_transitionS[i]);
        }
    }

    class DFA6 extends DFA {

        public DFA6(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 6;
            this.eot = DFA6_eot;
            this.eof = DFA6_eof;
            this.min = DFA6_min;
            this.max = DFA6_max;
            this.accept = DFA6_accept;
            this.special = DFA6_special;
            this.transition = DFA6_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T10 | REFERENCE | STRING | ESC | RESTRICTED_STRING | NEWLINE | WS );";
        }
    }
 

}