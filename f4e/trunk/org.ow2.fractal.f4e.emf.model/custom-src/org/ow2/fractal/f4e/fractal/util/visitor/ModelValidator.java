/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.util.visitor;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.Diagnostician;

// TODO: Auto-generated Javadoc
/**
 * The Class ModelValidator.
 */
public class ModelValidator implements Visitor{
	
	/* (non-Javadoc)
	 * @see org.ow2.fractal.adl.extended.fractaladlextended.util.visitor.Visitor#visit(org.eclipse.emf.ecore.EObject)
	 */
	public void visit(EObject eObject){
	//	System.out.println("============VALIDATE============");
	//	System.out.println(eObject);
		Diagnostic d = Diagnostician.INSTANCE.validate(eObject);
	//	System.out.println("================================");
	}
}