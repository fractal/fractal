package org.ow2.fractal.f4e.fractal.adapter.helper;

import org.eclipse.emf.common.notify.Adapter;

/**
 * 
 * @author Yann Davin
 * @model
 */
public interface IAbstractComponentListener extends Adapter.Internal{
	/**
	 * 
	 * @param featureID
	 * @model default=""
	 */
	public void update(final int featureID);
	
	/**
	 * 
	 * @param featureID
	 * @model default=""
	 */
	public void updateChildren(final int featureID);
	
	/**
	 * @model default="" 
	 */
	public void updateMerge();
	
	/**
	 * @model default=""
	 */
	public void updateChildrenMerge();
}
