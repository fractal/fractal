/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.util;

import java.util.Map;
import java.util.Vector;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.util.ExtendedMetaData;
import org.eclipse.emf.ecore.xmi.FeatureNotFoundException;
import org.eclipse.emf.ecore.xmi.XMLHelper;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.SAXXMLHandler;
import org.eclipse.emf.ecore.xml.type.AnyType;
import org.ow2.fractal.f4e.fractal.FractalFactory;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperComments;

/**
 * The Class FractalAdlExtendedHandler. When we load a fractal file, the root
 * element created is a Definition. But in our model, the first element must be
 * a System. So we handle the end of the parsing, to add a System element as
 * root.
 */
public class FractalHandler extends SAXXMLHandler {
	
	protected Vector<String> comments = new Vector<String>();

	/**
	 * Instantiates a new fractal adl extended handler.
	 * 
	 * @param xmiResource
	 *            the xmi resource
	 * @param helper
	 *            the helper
	 * @param options
	 *            the options
	 */
	public FractalHandler(XMLResource xmlResource,
			XMLHelper helper, Map options) {
		super(xmlResource, helper, options);
	}
	
	protected IFractalResource getResource(){
		return (IFractalResource)xmlResource;
	}
	
	public void startElement(String uri, String localName, String name) {
		uri = FractalFactory.eINSTANCE.getEPackage().getNsURI();
		super.startElement(uri, localName, name);
	}

	public void endElement(String uri, String localName, String name) {
		// TODO Auto-generated method stub
		super.endElement(uri, localName, name);
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.ecore.xmi.impl.XMLHandler#getFactoryForPrefix(java.lang.String)
	 */
	protected EFactory getFactoryForPrefix(String prefix) {
		EFactory returnFactory = super.getFactoryForPrefix(prefix);
		return returnFactory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.ecore.xmi.impl.XMLHandler#endDocument()
	 */

	public void endDocument() {
		super.endDocument();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.ecore.xmi.impl.XMLHandler#createObject(org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.ecore.EStructuralFeature)
	 */

	protected void createObject(EObject peekObject, EStructuralFeature feature) {
		super.createObject(peekObject, feature);
		
	}

	protected EObject createObject(EFactory eFactory, EClassifier type, boolean documentRoot){
		EObject object =  super.createObject(eFactory, type, documentRoot);
		return object;
	}
	/**
	 * Adds the comments.
	 * 
	 * @param object
	 *            the object
	 */
	protected void addComments(EObject object) {
		IHelperComments helper = HelperAdapterFactory.getInstance().adapt(object);
		if (helper != null) {
			helper.setComments(new Vector<String>(comments));
		}
		comments.clear();
	}

	protected void processObject(EObject object) {
		super.processObject(object);
		addComments(object);		
	}

	protected void processTopObject(EObject object) {
		super.processTopObject(object);
	}

	protected void handleComment(String comment) {
		comments.add(comment);
	}
	
	public void comment(char[] ch, int start, int length) {
		// We don't allows mixed element but
		// We want to be able to handle comments
		// even if our element are
		// not mixed
		handleComment(new String(ch, start, length));
	}

	protected void handleUnknownFeature(String prefix, String name,
			boolean isElement, EObject peekObject, String value) {
//		EStructuralFeature anyFeature = peekObject.eClass().getEStructuralFeature("any");
//		if(anyFeature != null && !FractalResourceFactoryImpl.getExtendedMetaData().INSTANCE.getWildcards(anyFeature).contains("##any")){
//			FractalResourceFactoryImpl.getExtendedMetaData().INSTANCE.setWildcards(anyFeature,Collections.singletonList("##any"));
//		}
		super.handleUnknownFeature(prefix, name, isElement, peekObject, value);
//		FractalADLExtension.handleUnknowElement(peekObject,eObjectToExtensionMap.get(peekObject));
	}
	
	/**
	 * 
	 */
	protected void recordUnknownFeature(String prefix, String name,
			boolean isElement, EObject peekObject, String value) {
		if (isElement) {
			AnyType anyType = getExtension(peekObject);
			int objectsIndex = objects.size();
			objects.push(anyType);
			int mixedTargetsIndex = mixedTargets.size();
			mixedTargets.push(anyType.getAny());
			int typesIndex = types.size();
			types.push(UNKNOWN_FEATURE_TYPE);

			handleFeature(prefix, name);

			objects.remove(objectsIndex);
			mixedTargets.remove(mixedTargetsIndex);
			types.remove(typesIndex);
		}else{
			AnyType anyType = getExtension(peekObject);
			setAttribValue(anyType, prefix == null ? name : prefix + ":" + name, value);
		}
	}

	protected void handleFeature(String prefix, String name) {
		 EObject peekObject = objects.peekEObject();

		    // This happens when processing an element with simple content that has elements content even though it shouldn't.
		    //
		    if (peekObject == null)
		    {
		      types.push(ERROR_TYPE);
		      error
		        (new FeatureNotFoundException
		          (name,
		           null,
		           getLocation(),
		           getLineNumber(),
		           getColumnNumber()));
		      return;
		    }

		    EStructuralFeature feature = getFeature(peekObject, prefix, name, true);
		    if (feature != null)
		    {
		      int kind = helper.getFeatureKind(feature);
		      if (kind == XMLHelper.DATATYPE_SINGLE || kind == XMLHelper.DATATYPE_IS_MANY)
		      {
		        objects.push(null);
		        mixedTargets.push(null);
		        types.push(feature);
		        if (!isNull())
		        {
		          text = new StringBuffer();
		        }
		      }
		      else if (extendedMetaData != null)
		      {
		        EReference eReference = (EReference)feature;
		        boolean isContainment = eReference.isContainment();      
		        if (!isContainment && !eReference.isResolveProxies() && extendedMetaData.getFeatureKind(feature) != ExtendedMetaData.UNSPECIFIED_FEATURE)
		        {
		          isIDREF = true;
		          objects.push(null);
		          mixedTargets.push(null);
		          types.push(feature);
		          text = new StringBuffer();
		        }
		        else
		        {
		          createObject(peekObject, feature);
		          EObject childObject = objects.peekEObject();
		          if (childObject != null)
		          {
		            if (isContainment)
		            {
		              EStructuralFeature simpleFeature = extendedMetaData.getSimpleFeature(childObject.eClass());
		              if (simpleFeature != null)
		              {
		                isSimpleFeature = true;
		                isIDREF = simpleFeature instanceof EReference;
		                objects.push(null);
		                mixedTargets.push(null);
		                types.push(simpleFeature);
		                text = new StringBuffer();
		              }
		            }
		            else if (!childObject.eIsProxy())
		            {
		              text = new StringBuffer();
		            }
		          }
		        }
		      }
		      else 
		      {
		        createObject(peekObject, feature);
		      }
		    }
		    else
		    {
		      // Try to get a general-content feature.
		      // Use a pattern that's not possible any other way.
		      //
		      if (xmlMap != null && (feature = getFeature(peekObject, null, "", true)) != null)
		      {

		        EFactory eFactory = getFactoryForPrefix(prefix);

		        // This is for the case for a local unqualified element that has been bound.
		        //
		        if (eFactory == null)
		        {
		          eFactory = feature.getEContainingClass().getEPackage().getEFactoryInstance();
		        }

		        EObject newObject = null;
		        if (useNewMethods)
		        {
		          newObject = createObject(eFactory, helper.getType(eFactory, name), false);
		        }
		        else
		        {
		            newObject = createObjectFromFactory(eFactory, name);
		        }
		        newObject = validateCreateObjectFromFactory(eFactory, name, newObject, feature);
		        if (newObject != null)
		        {
		          setFeatureValue(peekObject, feature, newObject);
		        }
		        processObject(newObject);
		      }
		      else
		      {
		        // This handles the case of a substitution group.
		        //
		        if (xmlMap != null)
		        {
		          EFactory eFactory = getFactoryForPrefix(prefix);
		          EObject newObject = createObjectFromFactory(eFactory, name);
		          validateCreateObjectFromFactory(eFactory, name, newObject);
		          if (newObject != null)
		          {
		            for (EReference eReference : peekObject.eClass().getEAllReferences())
		            {
		              if (eReference.getEType().isInstance(newObject))
		              {
		                setFeatureValue(peekObject, eReference, newObject);
		                processObject(newObject);
		                return;
		              }
		            }
		          }
		        }
		       
		        handleUnknownFeature(prefix, name, true, peekObject, null);
		      }
		    }
		   
	}
	
	
	/**
	   * Create an object based on the type of the given feature.
	   */
	  protected EObject createObjectFromFeatureType(EObject peekObject, EStructuralFeature feature)
	  {
	    String typeName = null;
	    EFactory factory = null;
	    EClassifier eType = null;
	    EObject obj = null;

	    if (feature != null && (eType = feature.getEType()) != null)
	    {
	      if (useNewMethods)
	      {
	        if (extendedMetaData != null && eType == EcorePackage.Literals.EOBJECT && extendedMetaData.getFeatureKind(feature) != ExtendedMetaData.UNSPECIFIED_FEATURE)
	        {
	          eType = anyType;
	          typeName = extendedMetaData.getName(anyType);
	          factory = anyType.getEPackage().getEFactoryInstance();
	        }
	        else
	        {
	          factory = eType.getEPackage().getEFactoryInstance();
	          typeName = extendedMetaData == null ? eType.getName() : extendedMetaData.getName(eType);
	        }
	        obj = createObject(factory, eType, false);
	      }
	      else
	      {

	        if (extendedMetaData != null && eType == EcorePackage.Literals.EOBJECT && extendedMetaData.getFeatureKind(feature) != ExtendedMetaData.UNSPECIFIED_FEATURE)
	        {
	          typeName = extendedMetaData.getName(anyType);
	          factory = anyType.getEPackage().getEFactoryInstance();
	        }
	        else
	        {
	          EClass eClass = (EClass)eType;
	          typeName = extendedMetaData == null ? eClass.getName() : extendedMetaData.getName(eClass);
	          factory = eClass.getEPackage().getEFactoryInstance();
	        }
	        obj = createObjectFromFactory(factory, typeName);
	      }   
	    }
	    
	    obj = validateCreateObjectFromFactory(factory, typeName, obj, feature);

	    if (obj != null)
	    {
	      setFeatureValue(peekObject, feature, obj);
	    }

	    processObject(obj);
	    return obj;
	  }
	  
	 
}