package org.ow2.fractal.f4e.fractal.util;

import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gmf.runtime.emf.core.GMFEditingDomainFactory;


public class FractalTransactionalEditingDomain extends GMFEditingDomainFactory{
	protected TransactionalEditingDomain domain;
	protected WorkspaceSynchronizer workspaceSynchronizer;
	protected FractalResourceSetImpl resourceSet;
	
	protected FractalTransactionalEditingDomain(){	
		//domain = WorkspaceEditingDomainFactory.INSTANCE.createEditingDomain(new FractalAdlExtendedResourceSetImpl(),OperationHistoryFactory.getOperationHistory());
		resourceSet = new FractalResourceSetImpl();

		domain = createEditingDomain(resourceSet,OperationHistoryFactory.getOperationHistory());
		//configure(domain);
		domain.setID("org.ow2.fractal.f4e.emf.diagram.EditingDomain");
		TransactionalEditingDomain.Registry.INSTANCE.add(domain.getID(), domain);
		
		workspaceSynchronizer = new WorkspaceSynchronizer(domain,new FractalWorkspaceSynchronizer());
	}
	

	/**
	 * Gets the single instance of FractalTransactionalEditingDomain.
	 * 
	 * @return single instance of FractalTransactionalEditingDomain
	 */
	public static TransactionalEditingDomain getEditingDomain(){
		return SingletonHolder.instance.domain;
	
	}
	
	public static FractalResourceSetImpl getResourceSet(){
		return SingletonHolder.instance.resourceSet;
	}
	/**
	 * The Class SingletonHolder.
	 */
	private static class SingletonHolder {
		/** The instance. */
		private static FractalTransactionalEditingDomain instance = new FractalTransactionalEditingDomain();
	}
}
