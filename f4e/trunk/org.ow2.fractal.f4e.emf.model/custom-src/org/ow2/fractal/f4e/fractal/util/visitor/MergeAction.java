package org.ow2.fractal.f4e.fractal.util.visitor;

import javax.security.auth.Refreshable;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.ow2.fractal.f4e.fractal.plugin.FractalPlugin;

/**
 * The completion task, which either shows a success dialog, or an error dialog,
 * depending on the ok state set and then notifies the {@link Refreshable} to
 * refresh its contents
 * @author robertb
 */
public class MergeAction extends Action {
  private boolean ok;
  private String okTitle;
  private String okMsg;
  private String failMsg;
  private String failTitle;
  private Throwable throwable;

  private String pluginId;

  /**
   * @param pluginId
   */
  public MergeAction() {
    this.pluginId = FractalPlugin.PLUGIN_ID;
  }

  public void setOk(boolean ok) {
    this.ok = ok;
  }

  public void setOkTitle(String okTitle) {
    this.okTitle = okTitle;
  }

  public void setOkMsg(String okMsg) {
    this.okMsg = okMsg;
  }

  public void setFailMsg(String failMsg) {
    this.failMsg = failMsg;
  }

  public void setFailTitle(String failTitle) {
    this.failTitle = failTitle;
  }

  public void setThrowable(Throwable throwable) {
    this.throwable = throwable;
  }

  public void run() {
//    if (ok) {
//    	 MessageDialog.openInformation(new Shell(), "Job Status", "Merge done");   
//    } else {
//      Status status = new Status(IStatus.ERROR, pluginId,
//                throwable.getLocalizedMessage(), throwable);
//      ErrorDialog.openError(new Shell(), failTitle, failMsg, status);
//    }
  }
}
