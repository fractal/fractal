package org.ow2.fractal.f4e.fractal.adapter.helper;

import java.util.HashMap;

import org.eclipse.emf.ecore.EObject;

public class Node<T extends EObject> {
	private T t;
	private HashMap<String,String> effectiveParameters;
	
	public Node(T t, HashMap<String,String> effectiveParameters){
		this.t = t;
		this.effectiveParameters = effectiveParameters;
	}
	
	public T getT() {
		return t;
	}
	
	public void setT(T t) {
		this.t = t;
	}
	
	public HashMap<String,String> getEffectiveParameters() {
		return effectiveParameters;
	}
	
	public void setEffectiveParameters(HashMap<String,String> effectiveParameters) {
		this.effectiveParameters = effectiveParameters;
	}

}
