package org.ow2.fractal.f4e.fractal.util;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Diagnostic;
import org.ow2.fractal.f4e.fractal.Definition;


public interface IFractalResource extends IAdaptable{
	/**
	 * Update model visit the Fractal ADL model
	 * parse the Fractal ADL String and create needed model elements.
	 */
	public void updateModel();

	/**
	 * Add a resource which depends on this one.
	 * When this resource is modified it call updateModel
	 * on all dependent resources.
	 * 
	 * In practice it is used when we need to load a Fractal file during
	 * extends or definition parsing. The current definition or component which extends
	 * others definition is depend of the parent definitions.
	 * So we call parentDefinition.addDependency(currentDefinition).
	 * @param a resource which depends on this one.
	 */
	public void addDependency(Resource resource);
	
	public void removeDependency(Resource resource);
	
	public List<Resource> getDependencies();
	
	/**
	 * A Fractal file defines one definition.
	 * The getRootDefinition return Definition model element
	 * of the definition described in the Fractal file associated to this resource.
	 * @return
	 */
	public Definition getRootDefinition();
	
	/**
	 * Unload the current resource, and reload (parse) the Fractal file associated with
	 * its resource, add update all other Fractal resources depend on this one.
	 * 
	 * Method used when the Fractal file associated with the current Resource
	 * is modified without EMF, for example in text mode. When we detach a modification
	 * on the fractal associated with this resource we reload it.
	 * @throws IOException
	 */
	public void reload() throws  IOException;

	
	public EList<Diagnostic> getErrors();
	
	public boolean shouldLoadExtends();
	
	public Map getDefaultLoadOptions();
}
