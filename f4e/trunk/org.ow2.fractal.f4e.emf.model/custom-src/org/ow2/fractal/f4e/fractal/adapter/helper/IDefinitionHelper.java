/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter.helper;

import java.util.HashMap;
import java.util.List;

import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.FormalParameter;
import org.ow2.fractal.f4e.fractal.Interface;



// TODO: Auto-generated Javadoc
/**
 * The Interface DefinitionHelper.
 * @model
 */
public interface IDefinitionHelper extends IAbstractComponentHelper<Definition>{
	
	/**
	 * Gets the parameter.
	 * 
	 * @param name the name
	 * 
	 * @return the parameter
	 * @model default=""
	 */
	public FormalParameter getParameter(String name);
	
	/**
	 * 
	 * @return a map containing all arguments names and their values. 
	 */
	public HashMap<String,String> getArguments();
	
	/**
	 * Gets the component.
	 * 
	 * @param path the path
	 * 
	 * @return the component
	 * @model default=""
	 */
	public Component getComponent(String path, boolean lookInMerged);
	
	/**
	 * 
	 * @return the list of its direct parents.
	 */
	public List<Definition> getParents();
	
}
