/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.URIHandlerImpl;

// TODO: Auto-generated Javadoc
/**
 * URIHandler to be able to load .fractal file located in the classpath.
 * URIs must have the following form : classpath:/package/subpackage/file.fractal
 * 
 * For the moment  we can only load via the createInputStream method. 
 * The save method has to be implemented.
 */

public class ClasspathURIHandlerImpl extends URIHandlerImpl
{
  
  /** The Constant CLASSPATH_SCHEME. */
  public static final String CLASSPATH_SCHEME="classpath";
	
  /**
   * Creates an instance.
   */
  public ClasspathURIHandlerImpl()
  {
    super();
  }

  /* (non-Javadoc)
   * @see org.eclipse.emf.ecore.resource.impl.URIHandlerImpl#canHandle(org.eclipse.emf.common.util.URI)
   */
  
  public boolean canHandle(URI uri)
  {
	  if(uri != null && uri.scheme() != null){
		  return 	uri.scheme().equals("classpath");
	  }else{
		  return false;
	  }
  }

  /**
   * Creates an output stream for the file path and returns it.
   * <p>
   * This implementation allocates a {@link FileOutputStream} and creates subdirectories as necessary.
   * </p>
   * 
   * @param uri the uri
   * @param options the options
   * 
   * @return an open output stream.
   * 
   * @throws IOException Signals that an I/O exception has occurred.
   * 
   * @exception IOException if there is a problem obtaining an open output stream.
   */
  
  public OutputStream createOutputStream(URI uri, Map<?, ?> options) throws IOException
  {
    String filePath = uri.toFileString();
    final File file = new File(filePath);
    String parent = file.getParent();
    if (parent != null)
    {
      new File(parent).mkdirs();
    }
    final Map<Object, Object> response = getResponse(options);
    OutputStream outputStream =
      new FileOutputStream(file)
      {
        
        public void close() throws IOException
        {
          try
          {
            super.close();
          }
          finally
          {
            if (response != null)
            {
              response.put(URIConverter.RESPONSE_TIME_STAMP_PROPERTY, file.lastModified());
            }
          }
        }
      };
    return outputStream;
  }

  
  /**
   * Checks if is classpath.
   * 
   * @param uri the uri
   * 
   * @return true, if is classpath
   */
  private static boolean isClasspath(URI uri){
	  return uri.scheme().equals(CLASSPATH_SCHEME);
  }
  
  /**
   * To classpath string.
   * 
   * @param uri the uri
   * 
   * @return the string
   */
  public static String toClasspathString(URI uri){
	  if (isClasspath(uri))
	    {
	      StringBuffer result = new StringBuffer();
	     
	      for (int i = 0, len =  uri.segmentCount(); i < len ; i++)
	      {
	        result.append('/').append(uri.segments()[i]);
	      }
	      result.deleteCharAt(0);
	      return result.toString();
	    }
	    return null;
  }
  
  public static boolean exists(URI uri){
	  String classpath = toClasspathString(uri);
	  
	  return ClassLoader.getSystemClassLoader().getResource(classpath) != null ? true : false;
  }
  
  /**
   * Creates an input stream for the file path and returns it.
   * <p>
   * This implementation allocates a {@link FileInputStream}.
   * </p>
   * 
   * @param uri the uri
   * @param options the options
   * 
   * @return an open input stream.
   * 
   * @throws IOException Signals that an I/O exception has occurred.
   * 
   * @exception IOException if there is a problem obtaining an open input stream.
   */
  
  public InputStream createInputStream(URI uri, Map<?, ?> options) throws IOException
  {
    String filePath = toClasspathString(uri);
    InputStream inputStream =  ClassLoader.getSystemClassLoader().getSystemResourceAsStream(filePath);
 
    Map<Object, Object> response = getResponse(options);
    if (response != null)
    {
      //response.put(URIConverter.RESPONSE_TIME_STAMP_PROPERTY, file.lastModified());
    }
    return inputStream;
  }

  /* (non-Javadoc)
   * @see org.eclipse.emf.ecore.resource.impl.URIHandlerImpl#delete(org.eclipse.emf.common.util.URI, java.util.Map)
   */
  
  public void delete(URI uri, Map<?, ?> options) throws IOException
  {
	  //TODO update for classloader
    String filePath = uri.toFileString();
    File file = new File(filePath);
    file.delete();
  }

  /* (non-Javadoc)
   * @see org.eclipse.emf.ecore.resource.impl.URIHandlerImpl#exists(org.eclipse.emf.common.util.URI, java.util.Map)
   */
  
  public boolean exists(URI uri, Map<?, ?> options)
  {
    return exists(uri);
  }

  /* (non-Javadoc)
   * @see org.eclipse.emf.ecore.resource.impl.URIHandlerImpl#getAttributes(org.eclipse.emf.common.util.URI, java.util.Map)
   */
  
  public Map<String, ?> getAttributes(URI uri, Map<?, ?> options)
  {
	//TODO update for classloader
    Map<String, Object> result = new HashMap<String, Object>();
    String filePath = uri.toFileString();
    File file = new File(filePath);
    if (file.exists())
    {
      Set<String> requestedAttributes = getRequestedAttributes(options);
      if (requestedAttributes == null || requestedAttributes.contains(URIConverter.ATTRIBUTE_TIME_STAMP))
      {
        result.put(URIConverter.ATTRIBUTE_TIME_STAMP, file.lastModified());
      }
      if (requestedAttributes == null || requestedAttributes.contains(URIConverter.ATTRIBUTE_LENGTH))
      {
        result.put(URIConverter.ATTRIBUTE_LENGTH, file.length());
      }
      if (requestedAttributes == null || requestedAttributes.contains(URIConverter.ATTRIBUTE_READ_ONLY))
      {
        result.put(URIConverter.ATTRIBUTE_READ_ONLY, !file.canWrite());
      }
      if (requestedAttributes == null || requestedAttributes.contains(URIConverter.ATTRIBUTE_HIDDEN))
      {
        result.put(URIConverter.ATTRIBUTE_HIDDEN, file.isHidden());
      }
      if (requestedAttributes == null || requestedAttributes.contains(URIConverter.ATTRIBUTE_DIRECTORY))
      {
        result.put(URIConverter.ATTRIBUTE_DIRECTORY, file.isDirectory());
      }
    }
    return result;
  }

  /* (non-Javadoc)
   * @see org.eclipse.emf.ecore.resource.impl.URIHandlerImpl#setAttributes(org.eclipse.emf.common.util.URI, java.util.Map, java.util.Map)
   */
  
  public void setAttributes(URI uri, Map<String, ?> attributes, Map<?, ?> options) throws IOException
  {
	//TODO update for classloader
    String filePath = uri.toFileString();
    File file = new File(filePath);
    if (file.exists())
    {
      Long timeStamp = (Long)attributes.get(URIConverter.ATTRIBUTE_TIME_STAMP);
      if (timeStamp != null && !file.setLastModified(timeStamp))
      {
        throw new IOException("Could not set the timestamp for the file '" + file +"'");
      }
      Boolean isReadOnly = (Boolean)attributes.get(URIConverter.ATTRIBUTE_READ_ONLY);
      if (Boolean.TRUE.equals(isReadOnly) && !file.setReadOnly())
      {
        throw new IOException("Could not set the file '" + file +"' to be read only");
      }
    }
    else
    {
      throw new FileNotFoundException("The file '" + file + "' does not exist");
    }
  }
}

