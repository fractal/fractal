// $ANTLR 3.0.1 custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3 2009-06-11 16:09:16

  package org.ow2.fractal.f4e.fractal.parser;
  
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ow2.fractal.f4e.fractal.Constant;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.FormalParameter;
import org.ow2.fractal.f4e.fractal.FractalFactory;
import org.ow2.fractal.f4e.fractal.Reference;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.ValueElement;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IDefinitionHelper;




import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;


import org.antlr.runtime.tree.*;

public class InterfaceADLParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "STRING", "RESTRICTED_STRING", "REFERENCE", "ESC", "NEWLINE", "WS", "'.'"
    };
    public static final int WS=9;
    public static final int NEWLINE=8;
    public static final int ESC=7;
    public static final int RESTRICTED_STRING=5;
    public static final int REFERENCE=6;
    public static final int EOF=-1;
    public static final int STRING=4;

        public InterfaceADLParser(TokenStream input) {
            super(input);
        }
        
    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    public TreeAdaptor getTreeAdaptor() {
        return adaptor;
    }

    public String[] getTokenNames() { return tokenNames; }
    public String getGrammarFileName() { return "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3"; }


    	Stack<Object> stack = new Stack<Object>();
    	Definition definition = null;
    	IDefinitionHelper helper = null;
    	
    	public InterfaceADLParser(TokenStream input, Definition contextDefinition){
    		super(input);
    		this.definition = contextDefinition;
    		
    		if(definition != null){
    			helper = (IDefinitionHelper)HelperAdapterFactory.getInstance().adapt(definition);
    		}
    	}
    	
    	public Value getValue() throws Exception{
    		// launch the parser
    	
    		interface_();	
    		
    		Value value = FractalFactory.eINSTANCE.createValue();
    		value.setContextDefinition(definition);
    		
    		popValueElements(value);
    		
    		return value;
    	}
    	
    	private void popValueElements(Value value){
    		if(!stack.empty()){
    			Object object = stack.pop();
    			if(object instanceof ValueElement){
    				popValueElements(value);
    				value.getElements().add((ValueElement)object);
    			}
    		}
    	}
    	
    	public void pushReference(String reference){
    		Reference eReference = FractalFactory.eINSTANCE.createReference();
    		if(helper != null){
    			FormalParameter formalParameter = helper.getParameter(reference);
    			eReference.setReference(formalParameter);
    			eReference.setName(reference);
    		}
    		// TODO add warning when there is not parameter found in the definition
    		stack.push(eReference);
    	}
    	
    	public void pushConstant(String constant){
    		Constant eConstant = FractalFactory.eINSTANCE.createConstant();
    		eConstant.setConstant(constant);
    		stack.push(eConstant);
    	}
    	


    public static class interface__return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start interface_
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:87:1: interface_ : (ref= reference | str= string ) ( '.' (ref= reference | str= string )+ )* EOF ;
    public final interface__return interface_() throws RecognitionException {
        interface__return retval = new interface__return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token char_literal1=null;
        Token EOF2=null;
        reference_return ref = null;

        string_return str = null;


        CommonTree char_literal1_tree=null;
        CommonTree EOF2_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:88:2: ( (ref= reference | str= string ) ( '.' (ref= reference | str= string )+ )* EOF )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:88:4: (ref= reference | str= string ) ( '.' (ref= reference | str= string )+ )* EOF
            {
            root_0 = (CommonTree)adaptor.nil();

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:88:4: (ref= reference | str= string )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==REFERENCE) ) {
                alt1=1;
            }
            else if ( ((LA1_0>=STRING && LA1_0<=RESTRICTED_STRING)) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("88:4: (ref= reference | str= string )", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:88:5: ref= reference
                    {
                    pushFollow(FOLLOW_reference_in_interface_51);
                    ref=reference();
                    _fsp--;

                    adaptor.addChild(root_0, ref.getTree());
                    pushReference(input.toString(ref.start,ref.stop));

                    }
                    break;
                case 2 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:88:49: str= string
                    {
                    pushFollow(FOLLOW_string_in_interface_59);
                    str=string();
                    _fsp--;

                    adaptor.addChild(root_0, str.getTree());
                    pushConstant(input.toString(str.start,str.stop));

                    }
                    break;

            }

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:88:88: ( '.' (ref= reference | str= string )+ )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==10) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:88:90: '.' (ref= reference | str= string )+
            	    {
            	    char_literal1=(Token)input.LT(1);
            	    match(input,10,FOLLOW_10_in_interface_66); 
            	    char_literal1_tree = (CommonTree)adaptor.create(char_literal1);
            	    adaptor.addChild(root_0, char_literal1_tree);

            	    pushConstant(".");
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:88:115: (ref= reference | str= string )+
            	    int cnt2=0;
            	    loop2:
            	    do {
            	        int alt2=3;
            	        int LA2_0 = input.LA(1);

            	        if ( (LA2_0==REFERENCE) ) {
            	            alt2=1;
            	        }
            	        else if ( ((LA2_0>=STRING && LA2_0<=RESTRICTED_STRING)) ) {
            	            alt2=2;
            	        }


            	        switch (alt2) {
            	    	case 1 :
            	    	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:88:116: ref= reference
            	    	    {
            	    	    pushFollow(FOLLOW_reference_in_interface_73);
            	    	    ref=reference();
            	    	    _fsp--;

            	    	    adaptor.addChild(root_0, ref.getTree());
            	    	    pushReference(input.toString(ref.start,ref.stop));

            	    	    }
            	    	    break;
            	    	case 2 :
            	    	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:88:160: str= string
            	    	    {
            	    	    pushFollow(FOLLOW_string_in_interface_81);
            	    	    str=string();
            	    	    _fsp--;

            	    	    adaptor.addChild(root_0, str.getTree());
            	    	    pushConstant(input.toString(str.start,str.stop));

            	    	    }
            	    	    break;

            	    	default :
            	    	    if ( cnt2 >= 1 ) break loop2;
            	                EarlyExitException eee =
            	                    new EarlyExitException(2, input);
            	                throw eee;
            	        }
            	        cnt2++;
            	    } while (true);


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            EOF2=(Token)input.LT(1);
            match(input,EOF,FOLLOW_EOF_in_interface_89); 
            EOF2_tree = (CommonTree)adaptor.create(EOF2);
            adaptor.addChild(root_0, EOF2_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end interface_

    public static class string_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start string
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:91:1: string : ( STRING | RESTRICTED_STRING );
    public final string_return string() throws RecognitionException {
        string_return retval = new string_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token set3=null;

        CommonTree set3_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:92:2: ( STRING | RESTRICTED_STRING )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:
            {
            root_0 = (CommonTree)adaptor.nil();

            set3=(Token)input.LT(1);
            if ( (input.LA(1)>=STRING && input.LA(1)<=RESTRICTED_STRING) ) {
                input.consume();
                adaptor.addChild(root_0, adaptor.create(set3));
                errorRecovery=false;
            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recoverFromMismatchedSet(input,mse,FOLLOW_set_in_string0);    throw mse;
            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end string

    public static class reference_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start reference
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:95:1: reference : REFERENCE ;
    public final reference_return reference() throws RecognitionException {
        reference_return retval = new reference_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token REFERENCE4=null;

        CommonTree REFERENCE4_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:96:2: ( REFERENCE )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3:96:4: REFERENCE
            {
            root_0 = (CommonTree)adaptor.nil();

            REFERENCE4=(Token)input.LT(1);
            match(input,REFERENCE,FOLLOW_REFERENCE_in_reference115); 
            REFERENCE4_tree = (CommonTree)adaptor.create(REFERENCE4);
            adaptor.addChild(root_0, REFERENCE4_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end reference


 

    public static final BitSet FOLLOW_reference_in_interface_51 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_string_in_interface_59 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_10_in_interface_66 = new BitSet(new long[]{0x0000000000000070L});
    public static final BitSet FOLLOW_reference_in_interface_73 = new BitSet(new long[]{0x0000000000000470L});
    public static final BitSet FOLLOW_string_in_interface_81 = new BitSet(new long[]{0x0000000000000470L});
    public static final BitSet FOLLOW_EOF_in_interface_89 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_string0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_REFERENCE_in_reference115 = new BitSet(new long[]{0x0000000000000002L});

}