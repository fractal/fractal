/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter.helper;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.transaction.RecordingCommand;
import org.ow2.fractal.f4e.fractal.Controller;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;


// TODO: Auto-generated Javadoc
/**
 * The Class MembraneHelperAdapter.
 */
public class ControllerHelperAdapter extends HelperAdapter<Controller>
	implements IControllerHelper {
	
	/**
	 * Instantiates a new membrane helper adapter.
	 * 
	 * @param membrane the membrane
	 */
	public ControllerHelperAdapter(Controller controller) {
		super(controller);
	}

	/* (non-Javadoc)
	 * @see org.ow2.fractal.adl.extended.fractalextended.adapter.helper.HelperAdapter#isAdapterForType(java.lang.Object)
	 */
	
	public boolean isAdapterForType(Object type) {
		return  super.isAdapterForType(type) || type == IControllerHelper.class;
	}
	
	protected void updateType(){
		t.setTypeAST(ValueHelperAdapter.getValue(t.getType(),t.getAbstractComponent()));
	}
	
	public void update(final int featureID, IProgressMonitor monitor){
		Command cmd = new RecordingCommand(FractalTransactionalEditingDomain
				.getEditingDomain()) {
			protected void doExecute() {
				switch (featureID) {
					case FractalPackage.CONTROLLER__TYPE_AST:
							updateType();
						break;
					default:
						break;
				}
			}
		};

		FractalTransactionalEditingDomain.getEditingDomain().getCommandStack()
				.execute(cmd);
	}
	
	public int[] getAttributeIDs() {
		return new int[]{FractalPackage.CONTROLLER__TYPE};
	}
}
