// $ANTLR 3.0.1 custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3 2008-11-24 10:20:40

  package org.ow2.fractal.f4e.fractal.parser;
  


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class OCLCommentsLexer extends Lexer {
    public static final int CONSTRAINT_LINE=4;
    public static final int CODE_LINE=6;
    public static final int WS=8;
    public static final int SEVERITY=10;
    public static final int SEVERITY_LINE=7;
    public static final int Tokens=12;
    public static final int EOF=-1;
    public static final int COMMENT_LINE=5;
    public static final int CHARACTER=11;
    public static final int STRING=9;
    public OCLCommentsLexer() {;} 
    public OCLCommentsLexer(CharStream input) {
        super(input);
    }
    public String getGrammarFileName() { return "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3"; }

    // $ANTLR start CONSTRAINT_LINE
    public final void mCONSTRAINT_LINE() throws RecognitionException {
        try {
            int _type = CONSTRAINT_LINE;
            Token STRING1=null;

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:82:16: ( '--' WS 'description:' STRING '\\n' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:82:18: '--' WS 'description:' STRING '\\n'
            {
            match("--"); 

            mWS(); 
            match("description:"); 

            int STRING1Start23 = getCharIndex();
            mSTRING(); 
            STRING1 = new CommonToken(input, Token.INVALID_TOKEN_TYPE, Token.DEFAULT_CHANNEL, STRING1Start23, getCharIndex()-1);
            setText(STRING1.getText());
            match('\n'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end CONSTRAINT_LINE

    // $ANTLR start SEVERITY_LINE
    public final void mSEVERITY_LINE() throws RecognitionException {
        try {
            int _type = SEVERITY_LINE;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:83:14: ( '--' WS 'severity:' SEVERITY ( WS )* '\\n' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:83:16: '--' WS 'severity:' SEVERITY ( WS )* '\\n'
            {
            match("--"); 

            mWS(); 
            match("severity:"); 

            mSEVERITY(); 
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:83:45: ( WS )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0=='\t'||LA1_0==' ') ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:83:45: WS
            	    {
            	    mWS(); 

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            match('\n'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end SEVERITY_LINE

    // $ANTLR start SEVERITY
    public final void mSEVERITY() throws RecognitionException {
        try {
            int _type = SEVERITY;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:85:9: ( WS ( 'ERROR' | 'WARNING' ) )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:85:11: WS ( 'ERROR' | 'WARNING' )
            {
            mWS(); 
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:85:14: ( 'ERROR' | 'WARNING' )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0=='E') ) {
                alt2=1;
            }
            else if ( (LA2_0=='W') ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("85:14: ( 'ERROR' | 'WARNING' )", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:85:15: 'ERROR'
                    {
                    match("ERROR"); 


                    }
                    break;
                case 2 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:85:25: 'WARNING'
                    {
                    match("WARNING"); 


                    }
                    break;

            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end SEVERITY

    // $ANTLR start COMMENT_LINE
    public final void mCOMMENT_LINE() throws RecognitionException {
        try {
            int _type = COMMENT_LINE;
            Token STRING2=null;

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:87:13: ( '--' WS ~ ( 'description:' ) STRING '\\n' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:87:15: '--' WS ~ ( 'description:' ) STRING '\\n'
            {
            match("--"); 

            mWS(); 
            if ( (input.LA(1)>='\u0000' && input.LA(1)<='\uFFFE') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }

            int STRING2Start77 = getCharIndex();
            mSTRING(); 
            STRING2 = new CommonToken(input, Token.INVALID_TOKEN_TYPE, Token.DEFAULT_CHANNEL, STRING2Start77, getCharIndex()-1);
            setText(STRING2.getText());
            match('\n'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end COMMENT_LINE

    // $ANTLR start CODE_LINE
    public final void mCODE_LINE() throws RecognitionException {
        try {
            int _type = CODE_LINE;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:89:10: ( STRING '\\n' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:89:13: STRING '\\n'
            {
            mSTRING(); 
            match('\n'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end CODE_LINE

    // $ANTLR start STRING
    public final void mSTRING() throws RecognitionException {
        try {
            int _type = STRING;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:91:7: ( ( WS | CHARACTER )* )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:91:9: ( WS | CHARACTER )*
            {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:91:9: ( WS | CHARACTER )*
            loop3:
            do {
                int alt3=3;
                int LA3_0 = input.LA(1);

                if ( (LA3_0=='\t'||LA3_0==' ') ) {
                    alt3=1;
                }
                else if ( ((LA3_0>='\u0000' && LA3_0<='\b')||(LA3_0>='\u000B' && LA3_0<='\f')||(LA3_0>='\u000E' && LA3_0<='\u001F')||(LA3_0>='!' && LA3_0<='\uFFFE')) ) {
                    alt3=2;
                }


                switch (alt3) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:91:10: WS
            	    {
            	    mWS(); 

            	    }
            	    break;
            	case 2 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:91:15: CHARACTER
            	    {
            	    mCHARACTER(); 

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end STRING

    // $ANTLR start CHARACTER
    public final void mCHARACTER() throws RecognitionException {
        try {
            int _type = CHARACTER;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:93:10: (~ ( '\\t' | ' ' | '\\r' | '\\n' ) )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:93:12: ~ ( '\\t' | ' ' | '\\r' | '\\n' )
            {
            if ( (input.LA(1)>='\u0000' && input.LA(1)<='\b')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\u001F')||(input.LA(1)>='!' && input.LA(1)<='\uFFFE') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end CHARACTER

    // $ANTLR start WS
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:95:3: ( ( '\\t' | ' ' )+ )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:95:5: ( '\\t' | ' ' )+
            {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:95:5: ( '\\t' | ' ' )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0=='\t'||LA4_0==' ') ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:
            	    {
            	    if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end WS

    public void mTokens() throws RecognitionException {
        // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:1:8: ( CONSTRAINT_LINE | SEVERITY_LINE | SEVERITY | COMMENT_LINE | CODE_LINE | STRING | CHARACTER | WS )
        int alt5=8;
        alt5 = dfa5.predict(input);
        switch (alt5) {
            case 1 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:1:10: CONSTRAINT_LINE
                {
                mCONSTRAINT_LINE(); 

                }
                break;
            case 2 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:1:26: SEVERITY_LINE
                {
                mSEVERITY_LINE(); 

                }
                break;
            case 3 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:1:40: SEVERITY
                {
                mSEVERITY(); 

                }
                break;
            case 4 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:1:49: COMMENT_LINE
                {
                mCOMMENT_LINE(); 

                }
                break;
            case 5 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:1:62: CODE_LINE
                {
                mCODE_LINE(); 

                }
                break;
            case 6 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:1:72: STRING
                {
                mSTRING(); 

                }
                break;
            case 7 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:1:79: CHARACTER
                {
                mCHARACTER(); 

                }
                break;
            case 8 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:1:89: WS
                {
                mWS(); 

                }
                break;

        }

    }


    protected DFA5 dfa5 = new DFA5(this);
    static final String DFA5_eotS =
        "\4\4\2\uffff\13\4\1\5\1\uffff\6\4\1\uffff\4\4\1\uffff\5\4\1\50\3"+
        "\4\1\uffff\3\4\1\50\22\4\1\uffff\2\4\1\uffff\5\4\1\uffff\1\4\1\uffff"+
        "\1\4";
    static final String DFA5_eofS =
        "\114\uffff";
    static final String DFA5_minS =
        "\4\0\2\uffff\14\0\1\uffff\6\0\1\uffff\4\0\1\uffff\11\0\1\uffff\26"+
        "\0\1\uffff\2\0\1\uffff\5\0\1\uffff\1\0\1\uffff\1\0";
    static final String DFA5_maxS =
        "\4\ufffe\2\uffff\14\ufffe\1\uffff\6\ufffe\1\uffff\4\ufffe\1\uffff"+
        "\11\ufffe\1\uffff\26\ufffe\1\uffff\2\ufffe\1\uffff\5\ufffe\1\uffff"+
        "\1\ufffe\1\uffff\1\ufffe";
    static final String DFA5_acceptS =
        "\4\uffff\1\6\1\5\14\uffff\1\4\6\uffff\1\4\4\uffff\1\4\11\uffff\1"+
        "\3\26\uffff\1\1\2\uffff\1\1\5\uffff\1\2\1\uffff\1\2\1\uffff";
    static final String DFA5_specialS =
        "\114\uffff}>";
    static final String[] DFA5_transitionS = {
            "\11\3\1\2\1\5\2\3\1\uffff\22\3\1\2\14\3\1\1\uffd1\3",
            "\11\10\1\7\1\5\2\10\1\uffff\22\10\1\7\14\10\1\6\uffd1\10",
            "\11\10\1\2\1\5\2\10\1\uffff\22\10\1\2\44\10\1\11\21\10\1\12"+
            "\uffa7\10",
            "\11\10\1\7\1\5\2\10\1\uffff\22\10\1\7\uffde\10",
            "",
            "",
            "\11\10\1\13\1\5\2\10\1\uffff\22\10\1\13\uffde\10",
            "\11\10\1\7\1\5\2\10\1\uffff\22\10\1\7\uffde\10",
            "\11\10\1\7\1\5\2\10\1\uffff\22\10\1\7\uffde\10",
            "\11\10\1\7\1\5\2\10\1\uffff\22\10\1\7\61\10\1\14\uffac\10",
            "\11\10\1\7\1\5\2\10\1\uffff\22\10\1\7\40\10\1\15\uffbd\10",
            "\11\23\1\16\1\21\2\23\1\22\22\23\1\16\103\23\1\17\16\23\1\20"+
            "\uff8b\23",
            "\11\10\1\7\1\5\2\10\1\uffff\22\10\1\7\61\10\1\24\uffac\10",
            "\11\10\1\7\1\5\2\10\1\uffff\22\10\1\7\61\10\1\25\uffac\10",
            "\11\32\1\26\1\31\2\32\1\22\22\32\1\26\103\32\1\27\16\32\1\30"+
            "\uff8b\32",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\104\35\1\33\uff99\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\104\35\1\37\uff99\35",
            "\15\22\1\uffff\ufff1\22",
            "",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\uffde\35",
            "\11\10\1\7\1\5\2\10\1\uffff\22\10\1\7\56\10\1\40\uffaf\10",
            "\11\10\1\7\1\5\2\10\1\uffff\22\10\1\7\55\10\1\41\uffb0\10",
            "\11\32\1\26\1\31\2\32\1\22\22\32\1\26\103\32\1\27\16\32\1\30"+
            "\uff8b\32",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\104\35\1\33\uff99\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\104\35\1\37\uff99\35",
            "",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\uffde\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\122\35\1\42\uff8b\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\uffde\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\uffde\35",
            "",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\125\35\1\43\uff88\35",
            "\11\10\1\7\1\5\2\10\1\uffff\22\10\1\7\61\10\1\44\uffac\10",
            "\11\10\1\7\1\5\2\10\1\uffff\22\10\1\7\50\10\1\45\uffb5\10",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\102\35\1\46\uff9b\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\104\35\1\47\uff99\35",
            "\11\10\1\7\1\5\2\10\1\uffff\22\10\1\7\uffde\10",
            "\11\10\1\7\1\5\2\10\1\uffff\22\10\1\7\55\10\1\51\uffb0\10",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\121\35\1\52\uff8c\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\121\35\1\53\uff8c\35",
            "",
            "\11\10\1\7\1\5\2\10\1\uffff\22\10\1\7\46\10\1\54\uffb7\10",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\110\35\1\55\uff95\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\110\35\1\56\uff95\35",
            "\11\10\1\7\1\5\2\10\1\uffff\22\10\1\7\uffde\10",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\117\35\1\57\uff8e\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\123\35\1\60\uff8a\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\123\35\1\61\uff8a\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\130\35\1\62\uff85\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\110\35\1\63\uff95\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\31\35\1\64\uffc4\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\116\35\1\65\uff8f\35",
            "\11\35\1\66\1\36\2\35\1\uffff\22\35\1\66\uffde\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\115\35\1\67\uff90\35",
            "\11\35\1\66\1\36\2\35\1\uffff\22\35\1\66\44\35\1\70\21\35\1"+
            "\71\uffa7\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\31\35\1\72\uffc4\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\61\35\1\73\uffac\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\40\35\1\74\uffbd\35",
            "\11\76\1\75\1\77\2\76\1\uffff\22\76\1\75\uffde\76",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\61\35\1\100\uffac\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\61\35\1\101\uffac\35",
            "\11\76\1\75\1\77\2\76\1\uffff\22\76\1\75\uffde\76",
            "\11\76\1\75\1\77\2\76\1\uffff\22\76\1\75\uffde\76",
            "",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\56\35\1\103\uffaf\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\55\35\1\104\uffb0\35",
            "",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\61\35\1\105\uffac\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\50\35\1\106\uffb5\35",
            "\11\35\1\107\1\110\2\35\1\uffff\22\35\1\107\uffde\35",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\55\35\1\111\uffb0\35",
            "\11\35\1\107\1\110\2\35\1\uffff\22\35\1\107\uffde\35",
            "",
            "\11\35\1\34\1\36\2\35\1\uffff\22\35\1\34\46\35\1\113\uffb7\35",
            "",
            "\11\35\1\107\1\110\2\35\1\uffff\22\35\1\107\uffde\35"
    };

    static final short[] DFA5_eot = DFA.unpackEncodedString(DFA5_eotS);
    static final short[] DFA5_eof = DFA.unpackEncodedString(DFA5_eofS);
    static final char[] DFA5_min = DFA.unpackEncodedStringToUnsignedChars(DFA5_minS);
    static final char[] DFA5_max = DFA.unpackEncodedStringToUnsignedChars(DFA5_maxS);
    static final short[] DFA5_accept = DFA.unpackEncodedString(DFA5_acceptS);
    static final short[] DFA5_special = DFA.unpackEncodedString(DFA5_specialS);
    static final short[][] DFA5_transition;

    static {
        int numStates = DFA5_transitionS.length;
        DFA5_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA5_transition[i] = DFA.unpackEncodedString(DFA5_transitionS[i]);
        }
    }

    class DFA5 extends DFA {

        public DFA5(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 5;
            this.eot = DFA5_eot;
            this.eof = DFA5_eof;
            this.min = DFA5_min;
            this.max = DFA5_max;
            this.accept = DFA5_accept;
            this.special = DFA5_special;
            this.transition = DFA5_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( CONSTRAINT_LINE | SEVERITY_LINE | SEVERITY | COMMENT_LINE | CODE_LINE | STRING | CHARACTER | WS );";
        }
    }
 

}