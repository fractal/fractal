/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter.helper;

import java.util.HashMap;
import java.util.List;

import org.ow2.fractal.f4e.fractal.EffectiveParameter;
import org.ow2.fractal.f4e.fractal.Value;


// TODO: Auto-generated Javadoc
/**
 * The Interface ValueHelper.
 * @model
 */
public interface IValueHelper extends IHelper<Value>{
	
	/**
	 * Return the string representation of the Value element.
	 * The string is the concatenation of its elements.
	 * For instance : If a Value has two ValueElement, one Constant c.constant = "hello-"
	 * and one Reference r.name = "arg" the value return by toString is :
	 * "hello-${arg}"
	 * 
	 * @return the string
	 */
	public String toString();
	
	/**
	 * 
	 * @param effectiveParameter
	 * @return
	 * @model default=""
	 */
	public String resolve(List<EffectiveParameter> effectiveParameter);
	
	/**
	 * 
	 * @param parameters contains as keys parameters names, and as values their effective values
	 * @return the resolved value where arguments reference are replaced by parameters effective values
	 * @model default=""
	 */
	public String resolve(HashMap<String,String> parameters);
}
