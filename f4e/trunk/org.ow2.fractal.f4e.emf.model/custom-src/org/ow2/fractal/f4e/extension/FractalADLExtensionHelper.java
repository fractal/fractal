package org.ow2.fractal.f4e.extension;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.ow2.fractal.f4e.fractal.util.FractalResourceFactoryImpl;

public class FractalADLExtensionHelper {
	
	
	public List<EStructuralFeature> getNotExistingEStructuralFeature(EClass eClass){
		List<EStructuralFeature> resultList = new ArrayList<EStructuralFeature>();
		List<EFactory> factories = FractalADLExtension.getRegisterExtensions();
		for(EFactory factory : factories){
			resultList.addAll(this.getNotExistingEStructuralFeature(factory.getEPackage(),eClass));
		}
		return resultList;
	}
	
	/**
	 * 
	 * @param ePackage : the package of the extension
	 * @param eClass : the classifier of a Fractal element model
	 * @return
	 */
	public List<EStructuralFeature> getNotExistingEStructuralFeature(EPackage ePackage, EClass eClass){
		List<EStructuralFeature> resultList = new ArrayList<EStructuralFeature>();
		EClassifier eClassifier = ePackage.getEClassifier(eClass.getName());
		if(eClassifier != null && eClassifier instanceof EClass){
			EClass extendedEClass = (EClass)eClassifier;
			for(EStructuralFeature feature: extendedEClass.getEStructuralFeatures()){
				if(eClass.getEStructuralFeature(feature.getName()) == null){
					EStructuralFeature newFeature = newEStructuralFeature(ePackage,feature);
					if(newFeature != null){
						resultList.add(newFeature);
					}
				}
			}
		}
		return resultList;
	}
	
	/**
	 * 
	 * @param eClass
	 * @param featureName
	 * @return the first feature of name featureName found in an extensions classifier of the same name than the eClass
	 */
	public EStructuralFeature searchEStructuralFeature(EClass eClass, String featureName){
		EStructuralFeature feature = null;
		List<EFactory> factories = FractalADLExtension.getRegisterExtensions();
		for(EFactory factory : factories){
			EClassifier classifier = factory.getEPackage().getEClassifier(eClass.getName());
			if(classifier != null && classifier instanceof EClass){
				EStructuralFeature extensionFeature = ((EClass)classifier).getEStructuralFeature(featureName);
				if(extensionFeature != null){
					feature = newEStructuralFeature(factory.getEPackage(), extensionFeature);
				}
			}
		}
		return feature;
	}
	
	public EStructuralFeature newEStructuralFeature(EPackage ePackage, EStructuralFeature feature){
		EStructuralFeature newFeature = null;
		EPackage pak = EPackage.Registry.INSTANCE.getEPackage("http://org.ow2.fractal/f4e/extension.ecore/1.0.0");
		if(feature instanceof EReference){
			newFeature = 
				FractalResourceFactoryImpl.getExtendedMetaData().demandFeature(ePackage.getNsPrefix(), feature.getName(), true,true);
			((EReference)newFeature).setResolveProxies(false);
			((EReference)newFeature).setContainment(true);
			
		}else if(feature instanceof EAttribute){
			newFeature = 
				FractalResourceFactoryImpl.getExtendedMetaData().demandFeature("", feature.getName(), false,false);
		}

		if(newFeature != null){
			
			newFeature.setLowerBound(feature.getLowerBound());
			newFeature.setUpperBound(EReference.UNSPECIFIED_MULTIPLICITY);
			newFeature.setChangeable(true);
			
			newFeature.setEType(feature.getEType());
			newFeature.setOrdered(feature.isOrdered());
			newFeature.setUnique(feature.isUnique());

			newFeature.setChangeable(feature.isChangeable());
			newFeature.setVolatile(feature.isVolatile());
			newFeature.setTransient(feature.isTransient());
			//newFeature.setDefaultValueLiteral(feature.getDefaultValueLiteral());
			//newFeature.setDefaultValue(feature.getDefaultValue());
			newFeature.setUnsettable(feature.isUnsettable());
			newFeature.setDerived(feature.isDerived());
		}
		return newFeature;
	}	
}
