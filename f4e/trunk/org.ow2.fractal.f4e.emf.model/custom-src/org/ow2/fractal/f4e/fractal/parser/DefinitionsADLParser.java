// $ANTLR 3.0.1 custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3 2009-06-11 16:09:16

  package org.ow2.fractal.f4e.fractal.parser;
  
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.Constant;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.EffectiveParameter;
import org.ow2.fractal.f4e.fractal.FormalParameter;
import org.ow2.fractal.f4e.fractal.FractalFactory;
import org.ow2.fractal.f4e.fractal.Reference;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.ValueElement;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IAbstractComponentHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IDefinitionHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IValueHelper;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.plugin.FractalPlugin;
import org.ow2.fractal.f4e.fractal.util.IFractalResource;
import org.ow2.fractal.f4e.fractal.util.SDefinitionLoadLocator;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;


import org.antlr.runtime.tree.*;

public class DefinitionsADLParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "REFERENCE", "NAME", "VALUE", "NEWLINE", "WS", "','", "'/'", "'('", "')'", "'=>'"
    };
    public static final int NAME=5;
    public static final int WS=8;
    public static final int NEWLINE=7;
    public static final int REFERENCE=4;
    public static final int VALUE=6;
    public static final int EOF=-1;

        public DefinitionsADLParser(TokenStream input) {
            super(input);
        }
        
    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    public TreeAdaptor getTreeAdaptor() {
        return adaptor;
    }

    public String[] getTokenNames() { return tokenNames; }
    public String getGrammarFileName() { return "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3"; }


    	Stack<Object> stack = new Stack<Object>();
    	Component component = null;
    	
    	public DefinitionsADLParser(TokenStream input, Component component){
    		super(input);
    		this.component = component;
    	}
    	
    	public void pushReference(String name){
    		Reference reference = FractalFactory.eINSTANCE.createReference();
    		Definition definition = component.getHelper().getParentDefinition();
        		reference.setName(name);
        		if(definition != null){
        			FormalParameter formalParameter = definition.getHelper().getParameter(name);
        			reference.setReference(formalParameter);
        		}
    		stack.push(reference);
    	}
    	
    	public void pushConstant(String name){
    		
    		Constant constant = FractalFactory.eINSTANCE.createConstant();
    		constant.setConstant(name);
    		stack.push(constant);
    	}
    	
    	public void pushParameter(){
    		
    		EffectiveParameter parameter = FractalFactory.eINSTANCE.createEffectiveParameter();		
    		
    		if(!stack.empty()){
    			if(stack.lastElement() instanceof Value){
    				parameter.setValue((Value)stack.pop());
    				parameter.setName(null);
    				
    				if(!stack.empty()){
    					if(stack.lastElement() instanceof Value){
    						parameter.setName((Value)stack.pop());
    					}
    				}
    			}
    		}
    		
    		stack.push(parameter);
    	}
    	
    	public void pushParameterWithoutName(){
    		EffectiveParameter parameter = FractalFactory.eINSTANCE.createEffectiveParameter();		
    		
    		if(!stack.empty()){
    			if(stack.lastElement() instanceof Value){
    				parameter.setValue((Value)stack.pop());
    				parameter.setName(null);
    			}
    		}
    		
    		stack.push(parameter);
    	}
    	
    	public void pushValue(){
    		
    		Value value = FractalFactory.eINSTANCE.createValue();
    		
    		IAbstractComponentHelper componentHelper = (IAbstractComponentHelper)HelperAdapterFactory.getInstance().adapt(component);
    		if(componentHelper != null){
    			value.setContextDefinition(componentHelper.getParentDefinition());
    		}
    		
    		popValueElements(value);
    		
    		stack.push(value);	
    	}	
    	
    	private void popValueElements(Value value){
    		if(!stack.empty() && (stack.lastElement() instanceof ValueElement)){
    			ValueElement elem = (ValueElement)stack.pop();
    			popValueElements(value);
    			value.getElements().add(elem);		
    		}
    	}
    	
    	
    	public void pushDefinitionCall(){
    		
    		DefinitionCall definitionCall = FractalFactory.eINSTANCE.createDefinitionCall();
    		
    		popParameters(definitionCall);
    			
    		if(!stack.empty()){
    			if(stack.lastElement() instanceof Value){
    				Value definitionName = (Value)stack.pop();
    				definitionCall.setDefinitionName(definitionName);
    				
    				IValueHelper helper = (IValueHelper)HelperAdapterFactory.getInstance().adapt(definitionName);
    				if(helper != null){
    						String definition = helper.toString();
        					
        					IFractalResource fractalResource = this.component.eResource()!= null && 
    						this.component.eResource() instanceof IFractalResource?
    								(IFractalResource)this.component.eResource():
    								null;
    								if(fractalResource!=null && fractalResource.shouldLoadExtends()){	
    							
        					 	ResourceSet resourceSet = this.component.eResource()!=null?this.component.eResource().getResourceSet():null;
            					 	if(resourceSet != null){
            					 		URI parentDefinitionURI = SDefinitionLoadLocator.getInstance().getLoadLocation(this.component.eResource().getURI(),definition);
            		    				if(SDefinitionLoadLocator.getInstance().getHelper(this.component.eResource().getURI()) != null ){
            		    					SDefinitionLoadLocator.getInstance().addHelper(parentDefinitionURI, SDefinitionLoadLocator.getInstance().getHelper(this.component.eResource().getURI()));
            		    				}
            		    				
            		    		Resource resource = null;
            		    		//URI fileToLoadURI = SDefinitionLoadLocator.getInstance().getLoadLocation(parentDefinitionURI,definition);
            		    		if(parentDefinitionURI != null){
            		    			resource = resourceSet.getResource(parentDefinitionURI,true);
            		    		}else{
            		    			FractalPlugin.getDefault().getLog().log(new Status(IStatus.WARNING,FractalPlugin.PLUGIN_ID,"The Fractal ADL definition : " +  definition + " can't be found"));
            		    		}
        					 	
        						if(resource == null){
        							definitionCall.setDefinition(null);
        						}else{
        							Definition eDefinition = ((IFractalResource)resource).getRootDefinition();
                				
        							((IFractalResource)resource).addDependency(component.eResource());
        							//the getResource checks that our resource starts with a definition
        							definitionCall.setDefinition(eDefinition);
        						}
        					 	}
    						}
    				}
    			}
    		}
    		
    		stack.push(definitionCall);
    	}	
    	
    	private void popParameters(DefinitionCall definitionCall){
    		if(!stack.empty() && (stack.lastElement() instanceof EffectiveParameter)){
    			EffectiveParameter param = (EffectiveParameter)stack.pop();
    			popParameters(definitionCall);
    			definitionCall.getParameters().add(param);
    		}
    	}
    	
    	private void popDefinitionCalls(){
    		if(component != null){
    			popDefinitionCalls(component);
    		}
    	}
    	
    	private void popDefinitionCalls(Component component){
    		if(!stack.empty() && (stack.lastElement() instanceof DefinitionCall)){
    			DefinitionCall def = (DefinitionCall)stack.pop();
    			popDefinitionCalls(component);
    			component.getExtendsAST().add(def);
    		}
    	}
    	
    	private boolean shared = false;
    	
    	private void setShared(){
    		IAbstractComponentHelper componentHelper = (IAbstractComponentHelper)HelperAdapterFactory.getInstance().adapt(component);
    		
    		if(componentHelper != null){
    			Definition definition = componentHelper.getParentDefinition();
    		
    			if(definition != null){
        				IDefinitionHelper definitionHelper = definition.getHelper();
        				if(definitionHelper != null){
        					Component sharedComponent = definitionHelper.getComponent(component.getDefinition(),true);
        					component.setShared(sharedComponent);
        				}
        			}
    		}	
    	}


    public static class prog_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start prog
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:222:1: prog : ( definitionsADL )? EOF ;
    public final prog_return prog() throws RecognitionException {
        prog_return retval = new prog_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token EOF2=null;
        definitionsADL_return definitionsADL1 = null;


        CommonTree EOF2_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:223:5: ( ( definitionsADL )? EOF )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:223:7: ( definitionsADL )? EOF
            {
            root_0 = (CommonTree)adaptor.nil();

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:223:7: ( definitionsADL )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( ((LA1_0>=REFERENCE && LA1_0<=NAME)||LA1_0==9||LA1_0==11) ) {
                alt1=1;
            }
            else if ( (LA1_0==EOF) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:223:8: definitionsADL
                    {
                    pushFollow(FOLLOW_definitionsADL_in_prog52);
                    definitionsADL1=definitionsADL();
                    _fsp--;

                    adaptor.addChild(root_0, definitionsADL1.getTree());
                    popDefinitionCalls();

                    }
                    break;

            }

            EOF2=(Token)input.LT(1);
            match(input,EOF,FOLLOW_EOF_in_prog59); 
            EOF2_tree = (CommonTree)adaptor.create(EOF2);
            adaptor.addChild(root_0, EOF2_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end prog

    public static class definitionsADL_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start definitionsADL
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:226:1: definitionsADL : ( shared | definitionADL ( ',' definitionADL )* );
    public final definitionsADL_return definitionsADL() throws RecognitionException {
        definitionsADL_return retval = new definitionsADL_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token char_literal5=null;
        shared_return shared3 = null;

        definitionADL_return definitionADL4 = null;

        definitionADL_return definitionADL6 = null;


        CommonTree char_literal5_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:227:2: ( shared | definitionADL ( ',' definitionADL )* )
            int alt3=2;
            switch ( input.LA(1) ) {
            case NAME:
                {
                int LA3_1 = input.LA(2);

                if ( (LA3_1==EOF||LA3_1==REFERENCE||LA3_1==9||LA3_1==11) ) {
                    alt3=2;
                }
                else if ( (LA3_1==10) ) {
                    alt3=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("226:1: definitionsADL : ( shared | definitionADL ( ',' definitionADL )* );", 3, 1, input);

                    throw nvae;
                }
                }
                break;
            case REFERENCE:
                {
                int LA3_2 = input.LA(2);

                if ( (LA3_2==10) ) {
                    alt3=1;
                }
                else if ( (LA3_2==EOF||(LA3_2>=REFERENCE && LA3_2<=NAME)||LA3_2==9||LA3_2==11) ) {
                    alt3=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("226:1: definitionsADL : ( shared | definitionADL ( ',' definitionADL )* );", 3, 2, input);

                    throw nvae;
                }
                }
                break;
            case EOF:
            case 9:
            case 11:
                {
                alt3=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("226:1: definitionsADL : ( shared | definitionADL ( ',' definitionADL )* );", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:227:4: shared
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_shared_in_definitionsADL74);
                    shared3=shared();
                    _fsp--;

                    adaptor.addChild(root_0, shared3.getTree());
                    setShared();

                    }
                    break;
                case 2 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:227:28: definitionADL ( ',' definitionADL )*
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_definitionADL_in_definitionsADL80);
                    definitionADL4=definitionADL();
                    _fsp--;

                    adaptor.addChild(root_0, definitionADL4.getTree());
                    pushDefinitionCall();
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:227:65: ( ',' definitionADL )*
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( (LA2_0==9) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:227:67: ',' definitionADL
                    	    {
                    	    char_literal5=(Token)input.LT(1);
                    	    match(input,9,FOLLOW_9_in_definitionsADL85); 
                    	    char_literal5_tree = (CommonTree)adaptor.create(char_literal5);
                    	    adaptor.addChild(root_0, char_literal5_tree);

                    	    pushFollow(FOLLOW_definitionADL_in_definitionsADL87);
                    	    definitionADL6=definitionADL();
                    	    _fsp--;

                    	    adaptor.addChild(root_0, definitionADL6.getTree());
                    	    pushDefinitionCall();

                    	    }
                    	    break;

                    	default :
                    	    break loop2;
                        }
                    } while (true);


                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end definitionsADL

    public static class shared_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start shared
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:230:1: shared : ( REFERENCE | NAME ) ( '/' ( REFERENCE | NAME ) )+ ;
    public final shared_return shared() throws RecognitionException {
        shared_return retval = new shared_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token set7=null;
        Token char_literal8=null;
        Token set9=null;

        CommonTree set7_tree=null;
        CommonTree char_literal8_tree=null;
        CommonTree set9_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:231:2: ( ( REFERENCE | NAME ) ( '/' ( REFERENCE | NAME ) )+ )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:231:4: ( REFERENCE | NAME ) ( '/' ( REFERENCE | NAME ) )+
            {
            root_0 = (CommonTree)adaptor.nil();

            set7=(Token)input.LT(1);
            if ( (input.LA(1)>=REFERENCE && input.LA(1)<=NAME) ) {
                input.consume();
                adaptor.addChild(root_0, adaptor.create(set7));
                errorRecovery=false;
            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recoverFromMismatchedSet(input,mse,FOLLOW_set_in_shared103);    throw mse;
            }

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:231:21: ( '/' ( REFERENCE | NAME ) )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==10) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:231:22: '/' ( REFERENCE | NAME )
            	    {
            	    char_literal8=(Token)input.LT(1);
            	    match(input,10,FOLLOW_10_in_shared110); 
            	    char_literal8_tree = (CommonTree)adaptor.create(char_literal8);
            	    adaptor.addChild(root_0, char_literal8_tree);

            	    set9=(Token)input.LT(1);
            	    if ( (input.LA(1)>=REFERENCE && input.LA(1)<=NAME) ) {
            	        input.consume();
            	        adaptor.addChild(root_0, adaptor.create(set9));
            	        errorRecovery=false;
            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recoverFromMismatchedSet(input,mse,FOLLOW_set_in_shared112);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end shared

    public static class definitionADL_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start definitionADL
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:234:1: definitionADL : definitionName ( '(' effectiveParameters ')' )? ;
    public final definitionADL_return definitionADL() throws RecognitionException {
        definitionADL_return retval = new definitionADL_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token char_literal11=null;
        Token char_literal13=null;
        definitionName_return definitionName10 = null;

        effectiveParameters_return effectiveParameters12 = null;


        CommonTree char_literal11_tree=null;
        CommonTree char_literal13_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:235:2: ( definitionName ( '(' effectiveParameters ')' )? )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:235:4: definitionName ( '(' effectiveParameters ')' )?
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_definitionName_in_definitionADL129);
            definitionName10=definitionName();
            _fsp--;

            adaptor.addChild(root_0, definitionName10.getTree());
            pushValue();
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:235:34: ( '(' effectiveParameters ')' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==11) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:235:35: '(' effectiveParameters ')'
                    {
                    char_literal11=(Token)input.LT(1);
                    match(input,11,FOLLOW_11_in_definitionADL134); 
                    char_literal11_tree = (CommonTree)adaptor.create(char_literal11);
                    adaptor.addChild(root_0, char_literal11_tree);

                    pushFollow(FOLLOW_effectiveParameters_in_definitionADL136);
                    effectiveParameters12=effectiveParameters();
                    _fsp--;

                    adaptor.addChild(root_0, effectiveParameters12.getTree());
                    char_literal13=(Token)input.LT(1);
                    match(input,12,FOLLOW_12_in_definitionADL138); 
                    char_literal13_tree = (CommonTree)adaptor.create(char_literal13);
                    adaptor.addChild(root_0, char_literal13_tree);


                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end definitionADL

    public static class definitionName_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start definitionName
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:238:1: definitionName : (name= NAME )? (ref= REFERENCE (name= NAME )? )* ;
    public final definitionName_return definitionName() throws RecognitionException {
        definitionName_return retval = new definitionName_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token name=null;
        Token ref=null;

        CommonTree name_tree=null;
        CommonTree ref_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:239:2: ( (name= NAME )? (ref= REFERENCE (name= NAME )? )* )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:239:4: (name= NAME )? (ref= REFERENCE (name= NAME )? )*
            {
            root_0 = (CommonTree)adaptor.nil();

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:239:4: (name= NAME )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==NAME) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:239:5: name= NAME
                    {
                    name=(Token)input.LT(1);
                    match(input,NAME,FOLLOW_NAME_in_definitionName156); 
                    name_tree = (CommonTree)adaptor.create(name);
                    adaptor.addChild(root_0, name_tree);

                    pushConstant(name.getText());

                    }
                    break;

            }

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:239:45: (ref= REFERENCE (name= NAME )? )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==REFERENCE) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:239:48: ref= REFERENCE (name= NAME )?
            	    {
            	    ref=(Token)input.LT(1);
            	    match(input,REFERENCE,FOLLOW_REFERENCE_in_definitionName167); 
            	    ref_tree = (CommonTree)adaptor.create(ref);
            	    adaptor.addChild(root_0, ref_tree);

            	    pushReference(ref.getText());
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:239:91: (name= NAME )?
            	    int alt7=2;
            	    int LA7_0 = input.LA(1);

            	    if ( (LA7_0==NAME) ) {
            	        alt7=1;
            	    }
            	    switch (alt7) {
            	        case 1 :
            	            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:239:92: name= NAME
            	            {
            	            name=(Token)input.LT(1);
            	            match(input,NAME,FOLLOW_NAME_in_definitionName175); 
            	            name_tree = (CommonTree)adaptor.create(name);
            	            adaptor.addChild(root_0, name_tree);

            	            pushConstant(name.getText());

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end definitionName

    public static class effectiveParameters_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start effectiveParameters
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:242:1: effectiveParameters : ( valuesList | parametersList );
    public final effectiveParameters_return effectiveParameters() throws RecognitionException {
        effectiveParameters_return retval = new effectiveParameters_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        valuesList_return valuesList14 = null;

        parametersList_return parametersList15 = null;



        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:243:2: ( valuesList | parametersList )
            int alt9=2;
            alt9 = dfa9.predict(input);
            switch (alt9) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:243:5: valuesList
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_valuesList_in_effectiveParameters194);
                    valuesList14=valuesList();
                    _fsp--;

                    adaptor.addChild(root_0, valuesList14.getTree());

                    }
                    break;
                case 2 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:243:18: parametersList
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_parametersList_in_effectiveParameters198);
                    parametersList15=parametersList();
                    _fsp--;

                    adaptor.addChild(root_0, parametersList15.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end effectiveParameters

    public static class valuesList_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start valuesList
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:246:1: valuesList : parameterValue ( ',' parameterValue )* ;
    public final valuesList_return valuesList() throws RecognitionException {
        valuesList_return retval = new valuesList_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token char_literal17=null;
        parameterValue_return parameterValue16 = null;

        parameterValue_return parameterValue18 = null;


        CommonTree char_literal17_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:247:2: ( parameterValue ( ',' parameterValue )* )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:247:4: parameterValue ( ',' parameterValue )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_parameterValue_in_valuesList210);
            parameterValue16=parameterValue();
            _fsp--;

            adaptor.addChild(root_0, parameterValue16.getTree());
            pushValue();pushParameterWithoutName();
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:247:60: ( ',' parameterValue )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==9) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:247:62: ',' parameterValue
            	    {
            	    char_literal17=(Token)input.LT(1);
            	    match(input,9,FOLLOW_9_in_valuesList215); 
            	    char_literal17_tree = (CommonTree)adaptor.create(char_literal17);
            	    adaptor.addChild(root_0, char_literal17_tree);

            	    pushFollow(FOLLOW_parameterValue_in_valuesList217);
            	    parameterValue18=parameterValue();
            	    _fsp--;

            	    adaptor.addChild(root_0, parameterValue18.getTree());
            	    pushValue();pushParameterWithoutName();

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end valuesList

    public static class parameterValue_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start parameterValue
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:250:1: parameterValue : (name= NAME | val= VALUE )? (ref= REFERENCE (name= NAME | val= VALUE )? )* ;
    public final parameterValue_return parameterValue() throws RecognitionException {
        parameterValue_return retval = new parameterValue_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token name=null;
        Token val=null;
        Token ref=null;

        CommonTree name_tree=null;
        CommonTree val_tree=null;
        CommonTree ref_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:251:2: ( (name= NAME | val= VALUE )? (ref= REFERENCE (name= NAME | val= VALUE )? )* )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:251:4: (name= NAME | val= VALUE )? (ref= REFERENCE (name= NAME | val= VALUE )? )*
            {
            root_0 = (CommonTree)adaptor.nil();

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:251:4: (name= NAME | val= VALUE )?
            int alt11=3;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==NAME) ) {
                alt11=1;
            }
            else if ( (LA11_0==VALUE) ) {
                alt11=2;
            }
            switch (alt11) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:251:5: name= NAME
                    {
                    name=(Token)input.LT(1);
                    match(input,NAME,FOLLOW_NAME_in_parameterValue236); 
                    name_tree = (CommonTree)adaptor.create(name);
                    adaptor.addChild(root_0, name_tree);

                    pushConstant(name.getText());

                    }
                    break;
                case 2 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:251:45: val= VALUE
                    {
                    val=(Token)input.LT(1);
                    match(input,VALUE,FOLLOW_VALUE_in_parameterValue244); 
                    val_tree = (CommonTree)adaptor.create(val);
                    adaptor.addChild(root_0, val_tree);

                    pushConstant(val.getText());

                    }
                    break;

            }

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:251:84: (ref= REFERENCE (name= NAME | val= VALUE )? )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==REFERENCE) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:251:87: ref= REFERENCE (name= NAME | val= VALUE )?
            	    {
            	    ref=(Token)input.LT(1);
            	    match(input,REFERENCE,FOLLOW_REFERENCE_in_parameterValue255); 
            	    ref_tree = (CommonTree)adaptor.create(ref);
            	    adaptor.addChild(root_0, ref_tree);

            	    pushReference(ref.getText());
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:251:130: (name= NAME | val= VALUE )?
            	    int alt12=3;
            	    int LA12_0 = input.LA(1);

            	    if ( (LA12_0==NAME) ) {
            	        alt12=1;
            	    }
            	    else if ( (LA12_0==VALUE) ) {
            	        alt12=2;
            	    }
            	    switch (alt12) {
            	        case 1 :
            	            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:251:131: name= NAME
            	            {
            	            name=(Token)input.LT(1);
            	            match(input,NAME,FOLLOW_NAME_in_parameterValue263); 
            	            name_tree = (CommonTree)adaptor.create(name);
            	            adaptor.addChild(root_0, name_tree);

            	            pushConstant(name.getText());

            	            }
            	            break;
            	        case 2 :
            	            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:251:171: val= VALUE
            	            {
            	            val=(Token)input.LT(1);
            	            match(input,VALUE,FOLLOW_VALUE_in_parameterValue271); 
            	            val_tree = (CommonTree)adaptor.create(val);
            	            adaptor.addChild(root_0, val_tree);

            	            pushConstant(val.getText());

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end parameterValue

    public static class parametersList_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start parametersList
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:254:1: parametersList : parameter ( ',' parameter )* ;
    public final parametersList_return parametersList() throws RecognitionException {
        parametersList_return retval = new parametersList_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token char_literal20=null;
        parameter_return parameter19 = null;

        parameter_return parameter21 = null;


        CommonTree char_literal20_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:255:2: ( parameter ( ',' parameter )* )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:255:5: parameter ( ',' parameter )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_parameter_in_parametersList290);
            parameter19=parameter();
            _fsp--;

            adaptor.addChild(root_0, parameter19.getTree());
            pushParameter();
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:255:33: ( ',' parameter )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==9) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:255:35: ',' parameter
            	    {
            	    char_literal20=(Token)input.LT(1);
            	    match(input,9,FOLLOW_9_in_parametersList295); 
            	    char_literal20_tree = (CommonTree)adaptor.create(char_literal20);
            	    adaptor.addChild(root_0, char_literal20_tree);

            	    pushFollow(FOLLOW_parameter_in_parametersList297);
            	    parameter21=parameter();
            	    _fsp--;

            	    adaptor.addChild(root_0, parameter21.getTree());
            	    pushParameter();

            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end parametersList

    public static class parameter_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start parameter
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:258:1: parameter : parameterName '=>' parameterValue ;
    public final parameter_return parameter() throws RecognitionException {
        parameter_return retval = new parameter_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token string_literal23=null;
        parameterName_return parameterName22 = null;

        parameterValue_return parameterValue24 = null;


        CommonTree string_literal23_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:259:2: ( parameterName '=>' parameterValue )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:259:4: parameterName '=>' parameterValue
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_parameterName_in_parameter312);
            parameterName22=parameterName();
            _fsp--;

            adaptor.addChild(root_0, parameterName22.getTree());
            pushValue();
            string_literal23=(Token)input.LT(1);
            match(input,13,FOLLOW_13_in_parameter316); 
            string_literal23_tree = (CommonTree)adaptor.create(string_literal23);
            adaptor.addChild(root_0, string_literal23_tree);

            pushFollow(FOLLOW_parameterValue_in_parameter318);
            parameterValue24=parameterValue();
            _fsp--;

            adaptor.addChild(root_0, parameterValue24.getTree());
            pushValue();

            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end parameter

    public static class parameterName_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start parameterName
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:262:1: parameterName : (name= NAME )? (ref= REFERENCE (name= NAME )? )* ;
    public final parameterName_return parameterName() throws RecognitionException {
        parameterName_return retval = new parameterName_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token name=null;
        Token ref=null;

        CommonTree name_tree=null;
        CommonTree ref_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:263:2: ( (name= NAME )? (ref= REFERENCE (name= NAME )? )* )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:263:4: (name= NAME )? (ref= REFERENCE (name= NAME )? )*
            {
            root_0 = (CommonTree)adaptor.nil();

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:263:4: (name= NAME )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==NAME) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:263:5: name= NAME
                    {
                    name=(Token)input.LT(1);
                    match(input,NAME,FOLLOW_NAME_in_parameterName334); 
                    name_tree = (CommonTree)adaptor.create(name);
                    adaptor.addChild(root_0, name_tree);

                    pushConstant(name.getText());

                    }
                    break;

            }

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:263:45: (ref= REFERENCE (name= NAME )? )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==REFERENCE) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:263:47: ref= REFERENCE (name= NAME )?
            	    {
            	    ref=(Token)input.LT(1);
            	    match(input,REFERENCE,FOLLOW_REFERENCE_in_parameterName344); 
            	    ref_tree = (CommonTree)adaptor.create(ref);
            	    adaptor.addChild(root_0, ref_tree);

            	    pushReference(ref.getText());
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:263:90: (name= NAME )?
            	    int alt16=2;
            	    int LA16_0 = input.LA(1);

            	    if ( (LA16_0==NAME) ) {
            	        alt16=1;
            	    }
            	    switch (alt16) {
            	        case 1 :
            	            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3:263:91: name= NAME
            	            {
            	            name=(Token)input.LT(1);
            	            match(input,NAME,FOLLOW_NAME_in_parameterName352); 
            	            name_tree = (CommonTree)adaptor.create(name);
            	            adaptor.addChild(root_0, name_tree);

            	            pushConstant(name.getText());

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end parameterName


    protected DFA9 dfa9 = new DFA9(this);
    static final String DFA9_eotS =
        "\6\uffff";
    static final String DFA9_eofS =
        "\6\uffff";
    static final String DFA9_minS =
        "\2\4\1\uffff\1\4\1\uffff\1\4";
    static final String DFA9_maxS =
        "\2\15\1\uffff\1\15\1\uffff\1\15";
    static final String DFA9_acceptS =
        "\2\uffff\1\1\1\uffff\1\2\1\uffff";
    static final String DFA9_specialS =
        "\6\uffff}>";
    static final String[] DFA9_transitionS = {
            "\1\3\1\1\1\2\2\uffff\1\2\2\uffff\1\2\1\4",
            "\1\3\4\uffff\1\2\2\uffff\1\2\1\4",
            "",
            "\1\3\1\5\1\2\2\uffff\1\2\2\uffff\1\2\1\4",
            "",
            "\1\3\4\uffff\1\2\2\uffff\1\2\1\4"
    };

    static final short[] DFA9_eot = DFA.unpackEncodedString(DFA9_eotS);
    static final short[] DFA9_eof = DFA.unpackEncodedString(DFA9_eofS);
    static final char[] DFA9_min = DFA.unpackEncodedStringToUnsignedChars(DFA9_minS);
    static final char[] DFA9_max = DFA.unpackEncodedStringToUnsignedChars(DFA9_maxS);
    static final short[] DFA9_accept = DFA.unpackEncodedString(DFA9_acceptS);
    static final short[] DFA9_special = DFA.unpackEncodedString(DFA9_specialS);
    static final short[][] DFA9_transition;

    static {
        int numStates = DFA9_transitionS.length;
        DFA9_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA9_transition[i] = DFA.unpackEncodedString(DFA9_transitionS[i]);
        }
    }

    class DFA9 extends DFA {

        public DFA9(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 9;
            this.eot = DFA9_eot;
            this.eof = DFA9_eof;
            this.min = DFA9_min;
            this.max = DFA9_max;
            this.accept = DFA9_accept;
            this.special = DFA9_special;
            this.transition = DFA9_transition;
        }
        public String getDescription() {
            return "242:1: effectiveParameters : ( valuesList | parametersList );";
        }
    }
 

    public static final BitSet FOLLOW_definitionsADL_in_prog52 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_prog59 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_shared_in_definitionsADL74 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_definitionADL_in_definitionsADL80 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_9_in_definitionsADL85 = new BitSet(new long[]{0x0000000000000A32L});
    public static final BitSet FOLLOW_definitionADL_in_definitionsADL87 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_set_in_shared103 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_10_in_shared110 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_set_in_shared112 = new BitSet(new long[]{0x0000000000000402L});
    public static final BitSet FOLLOW_definitionName_in_definitionADL129 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_11_in_definitionADL134 = new BitSet(new long[]{0x0000000000003270L});
    public static final BitSet FOLLOW_effectiveParameters_in_definitionADL136 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_definitionADL138 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NAME_in_definitionName156 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_REFERENCE_in_definitionName167 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_NAME_in_definitionName175 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_valuesList_in_effectiveParameters194 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parametersList_in_effectiveParameters198 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parameterValue_in_valuesList210 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_9_in_valuesList215 = new BitSet(new long[]{0x0000000000000272L});
    public static final BitSet FOLLOW_parameterValue_in_valuesList217 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_NAME_in_parameterValue236 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_VALUE_in_parameterValue244 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_REFERENCE_in_parameterValue255 = new BitSet(new long[]{0x0000000000000072L});
    public static final BitSet FOLLOW_NAME_in_parameterValue263 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_VALUE_in_parameterValue271 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_parameter_in_parametersList290 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_9_in_parametersList295 = new BitSet(new long[]{0x0000000000002030L});
    public static final BitSet FOLLOW_parameter_in_parametersList297 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_parameterName_in_parameter312 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_parameter316 = new BitSet(new long[]{0x0000000000000072L});
    public static final BitSet FOLLOW_parameterValue_in_parameter318 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NAME_in_parameterName334 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_REFERENCE_in_parameterName344 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_NAME_in_parameterName352 = new BitSet(new long[]{0x0000000000000012L});

}