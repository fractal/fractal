package org.ow2.fractal.f4e.fractal.util;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.util.URI;

public interface IDefinitionLocatorHelper {
	
	/**
	 * @param resourceURI
	 * @return
	 */
	Collection<String> getLocations(URI resourceURI);
	
	List<URI> getAllFractalDefinitions();
}
