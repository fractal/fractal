// $ANTLR 3.0.1 custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3 2009-06-11 16:09:16

  package org.ow2.fractal.f4e.fractal.parser;
  
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.ow2.fractal.f4e.fractal.Constant;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.adapter.helper.IComponentHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.ComponentHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.IValueHelper;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.EffectiveParameter;
import org.ow2.fractal.f4e.fractal.FormalParameter;
import org.ow2.fractal.f4e.fractal.FractalFactory;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.ValueElement;
import org.ow2.fractal.f4e.fractal.parser.ArgumentsADLLexer;
import org.ow2.fractal.f4e.fractal.parser.ArgumentsADLParser;
import org.ow2.fractal.f4e.fractal.parser.ExtendsADLLexer;
import org.ow2.fractal.f4e.fractal.parser.ExtendsADLParser;
import org.ow2.fractal.f4e.fractal.plugin.FractalPlugin;
import org.ow2.fractal.f4e.fractal.util.SDefinitionLoadLocator;
import org.ow2.fractal.f4e.fractal.util.FractalResourceSetImpl;
import org.ow2.fractal.f4e.fractal.adapter.helper.DefinitionHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.IDefinitionHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IAbstractComponentHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.AbstractComponentHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.util.SDefinitionLoadLocator;
import org.ow2.fractal.f4e.fractal.util.FractalResourceSet;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.util.IFractalResource;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;


import org.antlr.runtime.tree.*;

public class ExtendsADLParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "NAME", "REFERENCE", "VALUE", "NEWLINE", "WS", "','", "'('", "')'", "'=>'"
    };
    public static final int NAME=4;
    public static final int WS=8;
    public static final int NEWLINE=7;
    public static final int REFERENCE=5;
    public static final int VALUE=6;
    public static final int EOF=-1;

        public ExtendsADLParser(TokenStream input) {
            super(input);
        }
        
    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    public TreeAdaptor getTreeAdaptor() {
        return adaptor;
    }

    public String[] getTokenNames() { return tokenNames; }
    public String getGrammarFileName() { return "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3"; }


    	Stack<Object> stack = new Stack<Object>();
    	Definition definition = null;
    	
    	public ExtendsADLParser(TokenStream input, Definition definition){
    		super(input);
    		this.definition = definition;
    	}
    	
    	public void pushReference(String name){
    		//Reference reference = FractalFactory.eINSTANCE.createReference();
    		//reference.setReference(null);
    		//TODO add args ref support
    		Constant constant = FractalFactory.eINSTANCE.createConstant();
    		constant.setConstant(name);
    		stack.push(constant);
    	}
    	
    	public void pushConstant(String name){
    		
    		Constant constant = FractalFactory.eINSTANCE.createConstant();
    		constant.setConstant(name);
    		stack.push(constant);
    	}
    	
    	public void pushParameter(){
    		
    		EffectiveParameter parameter = FractalFactory.eINSTANCE.createEffectiveParameter();		
    		
    		if(!stack.empty()){
    			if(stack.lastElement() instanceof Value){
    				parameter.setValue((Value)stack.pop());
    				parameter.setName(null);
    				
    				if(!stack.empty()){
    					if(stack.lastElement() instanceof Value){
    						parameter.setName((Value)stack.pop());
    					}
    				}
    			}
    		}
    		
    		stack.push(parameter);
    	}
    	
    	public void pushValue(){
    		
    		Value value = FractalFactory.eINSTANCE.createValue();
    		
    		value.setContextDefinition(definition);
    		
    		popValueElements(value);
    		
    		stack.push(value);	
    	}	
    	
    	private void popValueElements(Value value){
    		if(!stack.empty() && (stack.lastElement() instanceof ValueElement)){
    			ValueElement elem = (ValueElement)stack.pop();
    			popValueElements(value);
    			value.getElements().add(elem);		
    		}
    	}
    	
    	/**
    	 * We create a definitionCall, set its 'name' with
    	 * the Value on the stack.
    	 * We load the parent definition, and set the
    	 * 'definition' field with it.
    	 *
    	 * Finaly we push the DefinitionCall on the stack.
    	 */
    	public void pushName(){
    		Value value = FractalFactory.eINSTANCE.createValue();
    		
    		value.setContextDefinition(definition);
    		popValueElements(value);
    		
    		DefinitionCall definitionCall = FractalFactory.eINSTANCE.createDefinitionCall();
    		Value definitionName = value;
    		definitionCall.setDefinitionName(definitionName);
    				
    		IValueHelper helper = (IValueHelper)HelperAdapterFactory.getInstance().adapt(definitionName);
        		if(helper != null){
        			String definition = helper.toString();
        			IFractalResource fractalResource = this.definition.eResource()!= null && 
        										this.definition.eResource() instanceof IFractalResource?
        												(IFractalResource)this.definition.eResource():
        												null;
        			if(fractalResource!=null && fractalResource.shouldLoadExtends()){								
        				ResourceSet resourceSet = this.definition.eResource()!=null?this.definition.eResource().getResourceSet():null;
            			if(resourceSet != null){
            				URI parentDefinitionURI = SDefinitionLoadLocator.getInstance().getLoadLocation(this.definition.eResource().getURI(),definition);
            				if(SDefinitionLoadLocator.getInstance().getHelper(this.definition.eResource().getURI()) != null ){
            					SDefinitionLoadLocator.getInstance().addHelper(parentDefinitionURI, SDefinitionLoadLocator.getInstance().getHelper(this.definition.eResource().getURI()));
            				}
            				Resource resource = null;
            				if(parentDefinitionURI != null){
            					resource = resourceSet.getResource(parentDefinitionURI,true);
            				}
            				
            				if(resource == null){	
            					definitionCall.setDefinition(null);
            					FractalPlugin.getDefault().getLog().log(new Status(IStatus.WARNING,FractalPlugin.PLUGIN_ID,"The Fractal ADL definition : " +  definition + " can't be found"));
            				}else{
            					//the getResource checks that our resource starts with a definition
            					((IFractalResource)resource).addDependency(this.definition.eResource());
            					definitionCall.setDefinition(((IFractalResource)resource).getRootDefinition());
            				}
            			}
        			}
    		}
    		
    		stack.push(definitionCall);	
    	}
    	
    	public void popDefinitionCall(){
    		if(!stack.empty() && (stack.firstElement() instanceof DefinitionCall)){
    			DefinitionCall definitionCall = (DefinitionCall)stack.get(0);
    			popParameters(definitionCall);
    			definition.getExtendsAST().add(definitionCall);
    		}
    		stack.pop();
    	}	
    	
    	private void popParameters(DefinitionCall definitionCall){
    		if(!stack.empty() && (stack.lastElement() instanceof EffectiveParameter)){
    			EffectiveParameter param = (EffectiveParameter)stack.pop();
    			popParameters(definitionCall);
    			definitionCall.getParameters().add(param);
    		}
    	}
    	
    	private void popDefinitionCalls(){
    		if(definition != null){
    			popDefinitionCalls(definition);
    		}
    	}
    	
    	private void popDefinitionCalls(Definition definition){
    		if(!stack.empty() && (stack.lastElement() instanceof DefinitionCall)){
    			DefinitionCall def = (DefinitionCall)stack.pop();
    			popDefinitionCalls(definition);
    			definition.getExtendsAST().add(def);
    		}
    	}


    public static class prog_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start prog
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:201:1: prog : ( definitionsADL )? EOF ;
    public final prog_return prog() throws RecognitionException {
        prog_return retval = new prog_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token EOF2=null;
        definitionsADL_return definitionsADL1 = null;


        CommonTree EOF2_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:202:5: ( ( definitionsADL )? EOF )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:202:7: ( definitionsADL )? EOF
            {
            root_0 = (CommonTree)adaptor.nil();

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:202:7: ( definitionsADL )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( ((LA1_0>=NAME && LA1_0<=REFERENCE)||(LA1_0>=9 && LA1_0<=10)) ) {
                alt1=1;
            }
            else if ( (LA1_0==EOF) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:202:8: definitionsADL
                    {
                    pushFollow(FOLLOW_definitionsADL_in_prog52);
                    definitionsADL1=definitionsADL();
                    _fsp--;

                    adaptor.addChild(root_0, definitionsADL1.getTree());

                    }
                    break;

            }

            EOF2=(Token)input.LT(1);
            match(input,EOF,FOLLOW_EOF_in_prog58); 
            EOF2_tree = (CommonTree)adaptor.create(EOF2);
            adaptor.addChild(root_0, EOF2_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end prog

    public static class definitionsADL_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start definitionsADL
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:205:1: definitionsADL : definitionADL ( ',' definitionADL )* ;
    public final definitionsADL_return definitionsADL() throws RecognitionException {
        definitionsADL_return retval = new definitionsADL_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token char_literal4=null;
        definitionADL_return definitionADL3 = null;

        definitionADL_return definitionADL5 = null;


        CommonTree char_literal4_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:206:2: ( definitionADL ( ',' definitionADL )* )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:206:4: definitionADL ( ',' definitionADL )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_definitionADL_in_definitionsADL73);
            definitionADL3=definitionADL();
            _fsp--;

            adaptor.addChild(root_0, definitionADL3.getTree());
            popDefinitionCall();
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:206:40: ( ',' definitionADL )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==9) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:206:42: ',' definitionADL
            	    {
            	    char_literal4=(Token)input.LT(1);
            	    match(input,9,FOLLOW_9_in_definitionsADL78); 
            	    char_literal4_tree = (CommonTree)adaptor.create(char_literal4);
            	    adaptor.addChild(root_0, char_literal4_tree);

            	    pushFollow(FOLLOW_definitionADL_in_definitionsADL80);
            	    definitionADL5=definitionADL();
            	    _fsp--;

            	    adaptor.addChild(root_0, definitionADL5.getTree());
            	    popDefinitionCall();

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end definitionsADL

    public static class definitionADL_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start definitionADL
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:209:1: definitionADL : definitionName ( '(' effectiveParameters ')' )? ;
    public final definitionADL_return definitionADL() throws RecognitionException {
        definitionADL_return retval = new definitionADL_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token char_literal7=null;
        Token char_literal9=null;
        definitionName_return definitionName6 = null;

        effectiveParameters_return effectiveParameters8 = null;


        CommonTree char_literal7_tree=null;
        CommonTree char_literal9_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:210:2: ( definitionName ( '(' effectiveParameters ')' )? )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:210:4: definitionName ( '(' effectiveParameters ')' )?
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_definitionName_in_definitionADL95);
            definitionName6=definitionName();
            _fsp--;

            adaptor.addChild(root_0, definitionName6.getTree());
            pushName();
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:210:33: ( '(' effectiveParameters ')' )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==10) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:210:34: '(' effectiveParameters ')'
                    {
                    char_literal7=(Token)input.LT(1);
                    match(input,10,FOLLOW_10_in_definitionADL100); 
                    char_literal7_tree = (CommonTree)adaptor.create(char_literal7);
                    adaptor.addChild(root_0, char_literal7_tree);

                    pushFollow(FOLLOW_effectiveParameters_in_definitionADL102);
                    effectiveParameters8=effectiveParameters();
                    _fsp--;

                    adaptor.addChild(root_0, effectiveParameters8.getTree());
                    char_literal9=(Token)input.LT(1);
                    match(input,11,FOLLOW_11_in_definitionADL104); 
                    char_literal9_tree = (CommonTree)adaptor.create(char_literal9);
                    adaptor.addChild(root_0, char_literal9_tree);


                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end definitionADL

    public static class definitionName_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start definitionName
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:213:1: definitionName : (name= NAME )? (ref= REFERENCE (name= NAME )? )* ;
    public final definitionName_return definitionName() throws RecognitionException {
        definitionName_return retval = new definitionName_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token name=null;
        Token ref=null;

        CommonTree name_tree=null;
        CommonTree ref_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:214:2: ( (name= NAME )? (ref= REFERENCE (name= NAME )? )* )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:214:4: (name= NAME )? (ref= REFERENCE (name= NAME )? )*
            {
            root_0 = (CommonTree)adaptor.nil();

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:214:4: (name= NAME )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==NAME) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:214:5: name= NAME
                    {
                    name=(Token)input.LT(1);
                    match(input,NAME,FOLLOW_NAME_in_definitionName122); 
                    name_tree = (CommonTree)adaptor.create(name);
                    adaptor.addChild(root_0, name_tree);

                    pushConstant(name.getText());

                    }
                    break;

            }

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:214:45: (ref= REFERENCE (name= NAME )? )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==REFERENCE) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:214:48: ref= REFERENCE (name= NAME )?
            	    {
            	    ref=(Token)input.LT(1);
            	    match(input,REFERENCE,FOLLOW_REFERENCE_in_definitionName133); 
            	    ref_tree = (CommonTree)adaptor.create(ref);
            	    adaptor.addChild(root_0, ref_tree);

            	    pushReference(ref.getText());
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:214:91: (name= NAME )?
            	    int alt5=2;
            	    int LA5_0 = input.LA(1);

            	    if ( (LA5_0==NAME) ) {
            	        alt5=1;
            	    }
            	    switch (alt5) {
            	        case 1 :
            	            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:214:92: name= NAME
            	            {
            	            name=(Token)input.LT(1);
            	            match(input,NAME,FOLLOW_NAME_in_definitionName141); 
            	            name_tree = (CommonTree)adaptor.create(name);
            	            adaptor.addChild(root_0, name_tree);

            	            pushConstant(name.getText());

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end definitionName

    public static class effectiveParameters_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start effectiveParameters
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:217:1: effectiveParameters : ( valuesList | parametersList );
    public final effectiveParameters_return effectiveParameters() throws RecognitionException {
        effectiveParameters_return retval = new effectiveParameters_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        valuesList_return valuesList10 = null;

        parametersList_return parametersList11 = null;



        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:218:2: ( valuesList | parametersList )
            int alt7=2;
            alt7 = dfa7.predict(input);
            switch (alt7) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:218:5: valuesList
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_valuesList_in_effectiveParameters160);
                    valuesList10=valuesList();
                    _fsp--;

                    adaptor.addChild(root_0, valuesList10.getTree());

                    }
                    break;
                case 2 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:218:18: parametersList
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    pushFollow(FOLLOW_parametersList_in_effectiveParameters164);
                    parametersList11=parametersList();
                    _fsp--;

                    adaptor.addChild(root_0, parametersList11.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end effectiveParameters

    public static class valuesList_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start valuesList
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:221:1: valuesList : parameterValue ( ',' parameterValue )* ;
    public final valuesList_return valuesList() throws RecognitionException {
        valuesList_return retval = new valuesList_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token char_literal13=null;
        parameterValue_return parameterValue12 = null;

        parameterValue_return parameterValue14 = null;


        CommonTree char_literal13_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:222:2: ( parameterValue ( ',' parameterValue )* )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:222:4: parameterValue ( ',' parameterValue )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_parameterValue_in_valuesList176);
            parameterValue12=parameterValue();
            _fsp--;

            adaptor.addChild(root_0, parameterValue12.getTree());
            pushValue();pushParameter();
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:222:49: ( ',' parameterValue )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==9) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:222:51: ',' parameterValue
            	    {
            	    char_literal13=(Token)input.LT(1);
            	    match(input,9,FOLLOW_9_in_valuesList181); 
            	    char_literal13_tree = (CommonTree)adaptor.create(char_literal13);
            	    adaptor.addChild(root_0, char_literal13_tree);

            	    pushFollow(FOLLOW_parameterValue_in_valuesList183);
            	    parameterValue14=parameterValue();
            	    _fsp--;

            	    adaptor.addChild(root_0, parameterValue14.getTree());
            	    pushValue();pushParameter();

            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end valuesList

    public static class parameterValue_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start parameterValue
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:225:1: parameterValue : (name= NAME | val= VALUE )? (ref= REFERENCE (name= NAME | val= VALUE )? )* ;
    public final parameterValue_return parameterValue() throws RecognitionException {
        parameterValue_return retval = new parameterValue_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token name=null;
        Token val=null;
        Token ref=null;

        CommonTree name_tree=null;
        CommonTree val_tree=null;
        CommonTree ref_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:226:2: ( (name= NAME | val= VALUE )? (ref= REFERENCE (name= NAME | val= VALUE )? )* )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:226:4: (name= NAME | val= VALUE )? (ref= REFERENCE (name= NAME | val= VALUE )? )*
            {
            root_0 = (CommonTree)adaptor.nil();

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:226:4: (name= NAME | val= VALUE )?
            int alt9=3;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==NAME) ) {
                alt9=1;
            }
            else if ( (LA9_0==VALUE) ) {
                alt9=2;
            }
            switch (alt9) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:226:5: name= NAME
                    {
                    name=(Token)input.LT(1);
                    match(input,NAME,FOLLOW_NAME_in_parameterValue202); 
                    name_tree = (CommonTree)adaptor.create(name);
                    adaptor.addChild(root_0, name_tree);

                    pushConstant(name.getText());

                    }
                    break;
                case 2 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:226:45: val= VALUE
                    {
                    val=(Token)input.LT(1);
                    match(input,VALUE,FOLLOW_VALUE_in_parameterValue210); 
                    val_tree = (CommonTree)adaptor.create(val);
                    adaptor.addChild(root_0, val_tree);

                    pushConstant(val.getText());

                    }
                    break;

            }

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:226:84: (ref= REFERENCE (name= NAME | val= VALUE )? )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==REFERENCE) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:226:87: ref= REFERENCE (name= NAME | val= VALUE )?
            	    {
            	    ref=(Token)input.LT(1);
            	    match(input,REFERENCE,FOLLOW_REFERENCE_in_parameterValue221); 
            	    ref_tree = (CommonTree)adaptor.create(ref);
            	    adaptor.addChild(root_0, ref_tree);

            	    pushReference(ref.getText());
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:226:130: (name= NAME | val= VALUE )?
            	    int alt10=3;
            	    int LA10_0 = input.LA(1);

            	    if ( (LA10_0==NAME) ) {
            	        alt10=1;
            	    }
            	    else if ( (LA10_0==VALUE) ) {
            	        alt10=2;
            	    }
            	    switch (alt10) {
            	        case 1 :
            	            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:226:131: name= NAME
            	            {
            	            name=(Token)input.LT(1);
            	            match(input,NAME,FOLLOW_NAME_in_parameterValue229); 
            	            name_tree = (CommonTree)adaptor.create(name);
            	            adaptor.addChild(root_0, name_tree);

            	            pushConstant(name.getText());

            	            }
            	            break;
            	        case 2 :
            	            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:226:171: val= VALUE
            	            {
            	            val=(Token)input.LT(1);
            	            match(input,VALUE,FOLLOW_VALUE_in_parameterValue237); 
            	            val_tree = (CommonTree)adaptor.create(val);
            	            adaptor.addChild(root_0, val_tree);

            	            pushConstant(val.getText());

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end parameterValue

    public static class parametersList_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start parametersList
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:229:1: parametersList : parameter ( ',' parameter )* ;
    public final parametersList_return parametersList() throws RecognitionException {
        parametersList_return retval = new parametersList_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token char_literal16=null;
        parameter_return parameter15 = null;

        parameter_return parameter17 = null;


        CommonTree char_literal16_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:230:2: ( parameter ( ',' parameter )* )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:230:5: parameter ( ',' parameter )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_parameter_in_parametersList256);
            parameter15=parameter();
            _fsp--;

            adaptor.addChild(root_0, parameter15.getTree());
            pushParameter();
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:230:33: ( ',' parameter )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==9) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:230:35: ',' parameter
            	    {
            	    char_literal16=(Token)input.LT(1);
            	    match(input,9,FOLLOW_9_in_parametersList261); 
            	    char_literal16_tree = (CommonTree)adaptor.create(char_literal16);
            	    adaptor.addChild(root_0, char_literal16_tree);

            	    pushFollow(FOLLOW_parameter_in_parametersList263);
            	    parameter17=parameter();
            	    _fsp--;

            	    adaptor.addChild(root_0, parameter17.getTree());
            	    pushParameter();

            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end parametersList

    public static class parameter_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start parameter
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:233:1: parameter : parameterName '=>' parameterValue ;
    public final parameter_return parameter() throws RecognitionException {
        parameter_return retval = new parameter_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token string_literal19=null;
        parameterName_return parameterName18 = null;

        parameterValue_return parameterValue20 = null;


        CommonTree string_literal19_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:234:2: ( parameterName '=>' parameterValue )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:234:4: parameterName '=>' parameterValue
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_parameterName_in_parameter278);
            parameterName18=parameterName();
            _fsp--;

            adaptor.addChild(root_0, parameterName18.getTree());
            pushValue();
            string_literal19=(Token)input.LT(1);
            match(input,12,FOLLOW_12_in_parameter282); 
            string_literal19_tree = (CommonTree)adaptor.create(string_literal19);
            adaptor.addChild(root_0, string_literal19_tree);

            pushFollow(FOLLOW_parameterValue_in_parameter284);
            parameterValue20=parameterValue();
            _fsp--;

            adaptor.addChild(root_0, parameterValue20.getTree());
            pushValue();

            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end parameter

    public static class parameterName_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start parameterName
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:237:1: parameterName : (name= NAME )? (ref= REFERENCE (name= NAME )? )* ;
    public final parameterName_return parameterName() throws RecognitionException {
        parameterName_return retval = new parameterName_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token name=null;
        Token ref=null;

        CommonTree name_tree=null;
        CommonTree ref_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:238:2: ( (name= NAME )? (ref= REFERENCE (name= NAME )? )* )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:238:4: (name= NAME )? (ref= REFERENCE (name= NAME )? )*
            {
            root_0 = (CommonTree)adaptor.nil();

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:238:4: (name= NAME )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==NAME) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:238:5: name= NAME
                    {
                    name=(Token)input.LT(1);
                    match(input,NAME,FOLLOW_NAME_in_parameterName300); 
                    name_tree = (CommonTree)adaptor.create(name);
                    adaptor.addChild(root_0, name_tree);

                    pushConstant(name.getText());

                    }
                    break;

            }

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:238:45: (ref= REFERENCE (name= NAME )? )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==REFERENCE) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:238:47: ref= REFERENCE (name= NAME )?
            	    {
            	    ref=(Token)input.LT(1);
            	    match(input,REFERENCE,FOLLOW_REFERENCE_in_parameterName310); 
            	    ref_tree = (CommonTree)adaptor.create(ref);
            	    adaptor.addChild(root_0, ref_tree);

            	    pushReference(ref.getText());
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:238:90: (name= NAME )?
            	    int alt14=2;
            	    int LA14_0 = input.LA(1);

            	    if ( (LA14_0==NAME) ) {
            	        alt14=1;
            	    }
            	    switch (alt14) {
            	        case 1 :
            	            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:238:91: name= NAME
            	            {
            	            name=(Token)input.LT(1);
            	            match(input,NAME,FOLLOW_NAME_in_parameterName318); 
            	            name_tree = (CommonTree)adaptor.create(name);
            	            adaptor.addChild(root_0, name_tree);

            	            pushConstant(name.getText());

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end parameterName


    protected DFA7 dfa7 = new DFA7(this);
    static final String DFA7_eotS =
        "\6\uffff";
    static final String DFA7_eofS =
        "\6\uffff";
    static final String DFA7_minS =
        "\1\4\1\5\1\uffff\1\4\1\uffff\1\5";
    static final String DFA7_maxS =
        "\2\14\1\uffff\1\14\1\uffff\1\14";
    static final String DFA7_acceptS =
        "\2\uffff\1\1\1\uffff\1\2\1\uffff";
    static final String DFA7_specialS =
        "\6\uffff}>";
    static final String[] DFA7_transitionS = {
            "\1\1\1\3\1\2\2\uffff\1\2\1\uffff\1\2\1\4",
            "\1\3\3\uffff\1\2\1\uffff\1\2\1\4",
            "",
            "\1\5\1\3\1\2\2\uffff\1\2\1\uffff\1\2\1\4",
            "",
            "\1\3\3\uffff\1\2\1\uffff\1\2\1\4"
    };

    static final short[] DFA7_eot = DFA.unpackEncodedString(DFA7_eotS);
    static final short[] DFA7_eof = DFA.unpackEncodedString(DFA7_eofS);
    static final char[] DFA7_min = DFA.unpackEncodedStringToUnsignedChars(DFA7_minS);
    static final char[] DFA7_max = DFA.unpackEncodedStringToUnsignedChars(DFA7_maxS);
    static final short[] DFA7_accept = DFA.unpackEncodedString(DFA7_acceptS);
    static final short[] DFA7_special = DFA.unpackEncodedString(DFA7_specialS);
    static final short[][] DFA7_transition;

    static {
        int numStates = DFA7_transitionS.length;
        DFA7_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA7_transition[i] = DFA.unpackEncodedString(DFA7_transitionS[i]);
        }
    }

    class DFA7 extends DFA {

        public DFA7(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 7;
            this.eot = DFA7_eot;
            this.eof = DFA7_eof;
            this.min = DFA7_min;
            this.max = DFA7_max;
            this.accept = DFA7_accept;
            this.special = DFA7_special;
            this.transition = DFA7_transition;
        }
        public String getDescription() {
            return "217:1: effectiveParameters : ( valuesList | parametersList );";
        }
    }
 

    public static final BitSet FOLLOW_definitionsADL_in_prog52 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_prog58 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_definitionADL_in_definitionsADL73 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_9_in_definitionsADL78 = new BitSet(new long[]{0x0000000000000632L});
    public static final BitSet FOLLOW_definitionADL_in_definitionsADL80 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_definitionName_in_definitionADL95 = new BitSet(new long[]{0x0000000000000402L});
    public static final BitSet FOLLOW_10_in_definitionADL100 = new BitSet(new long[]{0x0000000000001A70L});
    public static final BitSet FOLLOW_effectiveParameters_in_definitionADL102 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_definitionADL104 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NAME_in_definitionName122 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_REFERENCE_in_definitionName133 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_NAME_in_definitionName141 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_valuesList_in_effectiveParameters160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parametersList_in_effectiveParameters164 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parameterValue_in_valuesList176 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_9_in_valuesList181 = new BitSet(new long[]{0x0000000000000272L});
    public static final BitSet FOLLOW_parameterValue_in_valuesList183 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_NAME_in_parameterValue202 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_VALUE_in_parameterValue210 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_REFERENCE_in_parameterValue221 = new BitSet(new long[]{0x0000000000000072L});
    public static final BitSet FOLLOW_NAME_in_parameterValue229 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_VALUE_in_parameterValue237 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_parameter_in_parametersList256 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_9_in_parametersList261 = new BitSet(new long[]{0x0000000000001030L});
    public static final BitSet FOLLOW_parameter_in_parametersList263 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_parameterName_in_parameter278 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_parameter282 = new BitSet(new long[]{0x0000000000000072L});
    public static final BitSet FOLLOW_parameterValue_in_parameter284 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NAME_in_parameterName300 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_REFERENCE_in_parameterName310 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_NAME_in_parameterName318 = new BitSet(new long[]{0x0000000000000022L});

}