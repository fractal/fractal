package org.ow2.fractal.f4e.fractal.util;

import java.util.HashMap;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.ui.CommonUIPlugin;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.action.ValidateAction.EclipseResourcesUtil;

/**
 * Util class to manage markers for OCL validation
 * 
 * @author Yann Davin
 */
public class FractalResourcesUtil extends EclipseResourcesUtil {
	HashMap<EObject, IMarker> hashMap = new HashMap<EObject, IMarker>();
	
	protected void createMarkers(IResource resource, Diagnostic diagnostic, Diagnostic parentDiagnostic) throws CoreException
	  {
	    if (resource != null && resource.exists())
	    {
	      // the diagnostic.getData().get(0) holds the eObject on which is concerned by the diagnostic
	      IMarker marker = resource.createMarker(getMarkerID(diagnostic.getData().get(0)));
	      int severity = diagnostic.getSeverity();
	      if (severity < Diagnostic.WARNING)
	      {
	        marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_INFO);
	      }
	      else if (severity < Diagnostic.ERROR)
	      {
	        marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_WARNING);
	      }
	      else
	      {
	        marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
	      }
	          
	      String message = composeMessage(diagnostic, parentDiagnostic);
	      if (message != null)
	      {
	        marker.setAttribute(IMarker.MESSAGE, message);
	      }
	          
	      adjustMarker(marker, diagnostic, parentDiagnostic);
	    }
	  }

	/**
	 * Delete markers that have been add by validations on the eObject
	 * 
	 * @param eObject
	 */
	 public void deleteMarkers(EObject eObject)
	  {
	    deleteMarkers(eObject, false, IResource.DEPTH_ZERO);
	  }

	 /**
	  * 
	  * @param eObject
	  * @param includeSubtypes
	  * @param depth
	  */
	  public void deleteMarkers(EObject eObject, boolean includeSubtypes, int depth)
	  {
	    deleteMarkers(getFile(eObject.eResource()), eObject, includeSubtypes, depth);
	  }
	  
	  /**
	   * @param resource
	   * @param eObject
	   * @param includeSubtypes
	   * @param depth
	   */
	  protected void deleteMarkers(IResource resource, EObject eObject, boolean includeSubtypes, int depth)
	  {
	    if (resource != null && resource.exists())
	    {
	      try
	      {
	        resource.deleteMarkers(getMarkerID(eObject), includeSubtypes, depth);
	      }
	      catch (CoreException e)
	      {
	        CommonUIPlugin.INSTANCE.log(e);
	      }
	    }
	  }  
	
	/**
	 * @param object
	 * @return the marker ID
	 */
	protected String getMarkerID(Object object) {
		return object.getClass().getName() + "@" + Integer.toHexString(object.hashCode());
	}
	
	
}
