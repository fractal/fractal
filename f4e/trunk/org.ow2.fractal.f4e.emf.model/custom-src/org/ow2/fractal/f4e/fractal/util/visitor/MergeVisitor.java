/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.util.visitor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.transaction.RecordingCommand;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Attribute;
import org.ow2.fractal.f4e.fractal.AttributesController;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.Comment;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.Constant;
import org.ow2.fractal.f4e.fractal.Content;
import org.ow2.fractal.f4e.fractal.Controller;
import org.ow2.fractal.f4e.fractal.Coordinates;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.EffectiveParameter;
import org.ow2.fractal.f4e.fractal.FormalParameter;
import org.ow2.fractal.f4e.fractal.FractalFactory;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.Logger;
import org.ow2.fractal.f4e.fractal.Parameter;
import org.ow2.fractal.f4e.fractal.Reference;
import org.ow2.fractal.f4e.fractal.TemplateController;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.ValueElement;
import org.ow2.fractal.f4e.fractal.VirtualNode;
import org.ow2.fractal.f4e.fractal.adapter.helper.HelperAdapter;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;

/**
 * 
 * Class used to transform a Definition with merged elements in a standard merged Definition.
 * The elements of the merged features are moved in the non merged related features ex: 'merged interfaces' are moved in 'interfaces'.
 * AST features are set to null. 
 * 
 * @author Yann Davin
 * 
 */
public class MergeVisitor {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static FractalPackage modelPackage;

	protected Resource resource;
	
	protected EObject cursor;
	
	
	/**
	 * @param resource that will hold the merged copy
	 */
	public MergeVisitor(Resource resource) {
		if (modelPackage == null) {
			modelPackage = FractalPackage.eINSTANCE;
		}
		this.resource = resource;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public EObject doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected EObject doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected EObject doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case FractalPackage.SYSTEM: {
				org.ow2.fractal.f4e.fractal.System system = (org.ow2.fractal.f4e.fractal.System)theEObject;
				EObject result = caseSystem(system);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.ABSTRACT_COMPONENT: {
				AbstractComponent abstractComponent = (AbstractComponent)theEObject;
				EObject result = caseAbstractComponent(abstractComponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.PARAMETER: {
				Parameter parameter = (Parameter)theEObject;
				EObject result = caseParameter(parameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.VALUE: {
				Value value = (Value)theEObject;
				EObject result = caseValue(value);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.VALUE_ELEMENT: {
				ValueElement valueElement = (ValueElement)theEObject;
				EObject result = caseValueElement(valueElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.CONSTANT: {
				Constant constant = (Constant)theEObject;
				EObject result = caseConstant(constant);
				if (result == null) result = caseValueElement(constant);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.REFERENCE: {
				Reference reference = (Reference)theEObject;
				EObject result = caseReference(reference);
				if (result == null) result = caseValueElement(reference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.FORMAL_PARAMETER: {
				FormalParameter formalParameter = (FormalParameter)theEObject;
				EObject result = caseFormalParameter(formalParameter);
				if (result == null) result = caseParameter(formalParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.DEFINITION: {
				Definition definition = (Definition)theEObject;
				EObject result = caseDefinition(definition);
				if (result == null) result = caseAbstractComponent(definition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.DEFINITION_CALL: {
				DefinitionCall definitionCall = (DefinitionCall)theEObject;
				EObject result = caseDefinitionCall(definitionCall);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.EFFECTIVE_PARAMETER: {
				EffectiveParameter effectiveParameter = (EffectiveParameter)theEObject;
				EObject result = caseEffectiveParameter(effectiveParameter);
				if (result == null) result = caseParameter(effectiveParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.INTERFACE: {
				Interface interface_ = (Interface)theEObject;
				EObject result = caseInterface(interface_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.COMPONENT: {
				Component component = (Component)theEObject;
				EObject result = caseComponent(component);
				if (result == null) result = caseAbstractComponent(component);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.CONTENT: {
				Content content = (Content)theEObject;
				EObject result = caseContent(content);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.ATTRIBUTES_CONTROLLER: {
				AttributesController attributesController = (AttributesController)theEObject;
				EObject result = caseAttributesController(attributesController);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.ATTRIBUTE: {
				Attribute attribute = (Attribute)theEObject;
				EObject result = caseAttribute(attribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.BINDING: {
				Binding binding = (Binding)theEObject;
				EObject result = caseBinding(binding);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.CONTROLLER: {
				Controller controller = (Controller)theEObject;
				EObject result = caseController(controller);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.TEMPLATE_CONTROLLER: {
				TemplateController templateController = (TemplateController)theEObject;
				EObject result = caseTemplateController(templateController);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.COMMENT: {
				Comment comment = (Comment)theEObject;
				EObject result = caseComment(comment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.COORDINATES: {
				Coordinates coordinates = (Coordinates)theEObject;
				EObject result = caseCoordinates(coordinates);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.LOGGER: {
				Logger logger = (Logger)theEObject;
				EObject result = caseLogger(logger);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.VIRTUAL_NODE: {
				VirtualNode virtualNode = (VirtualNode)theEObject;
				EObject result = caseVirtualNode(virtualNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>System</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>System</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseSystem(org.ow2.fractal.f4e.fractal.System object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public AbstractComponent caseAbstractComponent(AbstractComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseParameter(Parameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseValue(Value object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Value Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Value Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseValueElement(ValueElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseConstant(Constant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseReference(Reference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseFormalParameter(FormalParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseDefinition(final Definition object) {
		Command cmd = new RecordingCommand(FractalTransactionalEditingDomain
				.getEditingDomain()) {
			protected void doExecute() {
				Transform(object);
				object.eSet(FractalPackage.eINSTANCE.getDefinition_Extends(), null);
				resource.getContents().add(object);
			}
		};
	
		FractalTransactionalEditingDomain.getEditingDomain().getCommandStack().execute(cmd);
		return object;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Definition Call</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Definition Call</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseDefinitionCall(DefinitionCall object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Effective Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Effective Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseEffectiveParameter(EffectiveParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseInterface(Interface object) {
		Transform(object);
		object.setNameAST(null);
		object.setSignatureAST(null);
		
//		if(object.getMergedName() != null){
//			object.setMergedName(object.getMergedName());
//		}
		
		return object;
	}
	
	protected void Transform(EObject eObject){
		try{
		for(EStructuralFeature feature:eObject.eClass().getEAllStructuralFeatures()){
			
			if(HelperAdapter.isASTFeature(eObject, feature.getFeatureID())){
				Object object = eObject.eGet(feature);
				if(object instanceof EObject){
					EcoreUtil.remove((EObject)object);
				}else if(object instanceof List){
					while(((List)object).size()>0){
						if(((List)object).get(0) instanceof EObject){
							EcoreUtil.remove((EObject)((List)object).get(0));
						}
					}
				}
			}else if(HelperAdapter.isMergedFeature(eObject, feature.getFeatureID())){
				EStructuralFeature relatedFeature = HelperAdapter.getFeatureOfMergedFeature(eObject, feature);
				
				Object object = eObject.eGet(feature);
				if(object instanceof List){
					
					while(((EList)object).size()>0){
						Object elem = ((EList)object).get(0);
						if(elem instanceof EObject){
							((EList)eObject.eGet(feature)).remove(elem);
							((EList)eObject.eGet(relatedFeature)).add(elem);
							doSwitch((EObject)elem);
						}
					}
				}else{
					Object nonMergedObject = eObject.eGet(relatedFeature);
					if((nonMergedObject == null || (nonMergedObject instanceof String && ((String)nonMergedObject).equals(""))) && object != null){
						eObject.eSet(relatedFeature, object);
						if(object instanceof EObject){
							doSwitch((EObject)object);
						}
					}else if(nonMergedObject != null && nonMergedObject instanceof EObject){
						doSwitch((EObject)nonMergedObject);
					}
				}
			}else if(feature instanceof EReference && !((EReference)feature).isContainment()){
				//eObject.eSet(feature, null);
			}else if(eObject.eGet(feature) instanceof EObject){
				doSwitch((EObject)eObject.eGet(feature));
			}else if(eObject.eGet(feature) instanceof List){
				for(Object object:(List)eObject.eGet(feature)){
					if(object instanceof EObject){
						doSwitch((EObject)object);
					}
				}
			}
		}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseComponent(Component object) {
		Transform(object);
		object.eSet(FractalPackage.eINSTANCE.getDefinition_Extends(), null);
		return object;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Content</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Content</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseContent(Content object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attributes Controller</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attributes Controller</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseAttributesController(AttributesController object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseAttribute(Attribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binding</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binding</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseBinding(Binding object) {
		object.setClientAST(null);
		object.setServerAST(null);
		object.setClientInterface(null);
		object.setServerInterface(null);
		
		if(object.getMergedClient() != null){
			object.setClient(object.getMergedClient());
		}
		
		if(object.getMergedServer() != null){
			object.setServer(object.getMergedServer());
		}
		
		return object;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Controller</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Controller</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseController(Controller object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Template Controller</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Template Controller</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseTemplateController(TemplateController object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Comment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Comment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseComment(Comment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Coordinates</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Coordinates</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseCoordinates(Coordinates object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Logger</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Logger</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseLogger(Logger object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Virtual Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Virtual Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public EObject caseVirtualNode(VirtualNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public EObject defaultCase(EObject object) {
		Transform(object);
		return object;
		//return EcoreUtil.copy(object);
	}
}
