lexer grammar DefinitionsADL;
@header {
  package org.ow2.fractal.f4e.fractal.parser;
}

T9 : ',' ;
T10 : '/' ;
T11 : '(' ;
T12 : ')' ;
T13 : '=>' ;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3" 266
REFERENCE 
	: '${' ( options {greedy=false;} : 'a'..'z' | 'A'..'Z' | '0'..'9' | '-' | '_' )+ '}' {setText(getText().substring(2, getText().length()-1));} 
	;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3" 270
VALUE 
	: '\'' ( options {greedy=false;} : . )* '\''
	;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3" 274
NAME 
	: ('a'..'z' | 'A'..'Z' | '0'..'9' | '-' | '_' | '.' | ' '  )+
	;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3" 278
NEWLINE : '\r'? '\n' ;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/DefinitionsADL.g3" 280
WS : (' ' | '\t' | '\n' | '\r')+ {skip();} ;