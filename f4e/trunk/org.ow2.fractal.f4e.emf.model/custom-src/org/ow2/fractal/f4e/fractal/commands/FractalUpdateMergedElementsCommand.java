package org.ow2.fractal.f4e.fractal.commands;

import java.util.Map;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.util.IExceptionHandler;
import org.ow2.fractal.f4e.fractal.util.IFractalResource;

public class FractalUpdateMergedElementsCommand extends RecordingCommand implements IFractalUpdateMergedElementsCommand{
	private IFractalResource resource;
	private Map<Object, Object> saveOptions;
	private IExceptionHandler handler;
	private Runnable runnable;
	
	public FractalUpdateMergedElementsCommand(Runnable runnable){
		super(FractalTransactionalEditingDomain.getEditingDomain(),"Save Fractal Resource");
		this.resource = resource;
		this.saveOptions = saveOptions;
		this.handler = handler;
		this.runnable = runnable;
	}
	
	private FractalUpdateMergedElementsCommand(TransactionalEditingDomain domain){
		super(domain);
	}
	
	private FractalUpdateMergedElementsCommand(TransactionalEditingDomain domain, String label) {
		super(domain,label);
	}
	
	public void update(){
		FractalTransactionalEditingDomain.getEditingDomain().getCommandStack().execute(this);
	}
	
	protected void doExecute() {
		try {
			runnable.run();
		}
		catch (Exception exception) {
			if(handler != null){
				handler.handleException(exception);
			}else{
				exception.printStackTrace();
			}
		}
	}
}
