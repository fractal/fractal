/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter.factory;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.EObject;
import org.ow2.fractal.f4e.fractal.Attribute;
import org.ow2.fractal.f4e.fractal.AttributesController;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.Content;
import org.ow2.fractal.f4e.fractal.Controller;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.EffectiveParameter;
import org.ow2.fractal.f4e.fractal.FormalParameter;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.Parameter;
import org.ow2.fractal.f4e.fractal.System;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.VirtualNode;
import org.ow2.fractal.f4e.fractal.adapter.helper.AttributeHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.AttributesControllerHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.BindingHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.ComponentHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.ContentHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.ControllerHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.DefinitionCallHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.DefinitionHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.HelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.IAttributeHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IAttributesControllerHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IBindingHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IComponentHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IContentHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IControllerHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IDefinitionCallHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IDefinitionHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IInterfaceHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IParameterHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.ISystemHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IValueHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IVirtualNodeHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.InterfaceHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.ParameterHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.SystemHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.ValueHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.VirtualNodeHelperAdapter;
import org.ow2.fractal.f4e.fractal.util.FractalAdapterFactory;
import org.ow2.fractal.f4e.fractal.util.FractalSwitch;



/**
 * A factory for creating HelperAdapter objects.
 */
public class HelperAdapterFactory  extends FractalAdapterFactory {
		
		/**
		 * Instantiates a new helper adapter factory.
		 */
		private HelperAdapterFactory(){	
		}
		
		/**
		 * Gets the single instance of HelperAdapterFactory.
		 * 
		 * @return single instance of HelperAdapterFactory
		 */
		public static HelperAdapterFactory getInstance(){
			return SingletonHolder.instance;
		}
		
		/**
		 * The Class SingletonHolder.
		 */
		private static class SingletonHolder {
			
			/** The instance. */
			private static HelperAdapterFactory instance = new HelperAdapterFactory();
		}
		
		/** The model switch. */
		protected FractalSwitch<HelperAdapter<?>> modelSwitch =
			new FractalSwitch<HelperAdapter<?>>() {
				
				public HelperAdapter<?> caseSystem(System object) {
					return createSystemAdapter(object);
				}
				
				
				public HelperAdapter<?> caseDefinition(Definition object) {
					return createDefinitionAdapter(object);
				}
				
				
				public HelperAdapter<?> caseDefinitionCall(DefinitionCall object) {
					return createDefinitionCallAdapter(object);
				}
				
				
				public HelperAdapter<?> caseComponent(Component object) {
					return createComponentAdapter(object);
				}
				
				
				public HelperAdapter<?> caseParameter(Parameter object) {
					return createParameterAdapter(object);
				}
				
				
				public HelperAdapter<?> caseFormalParameter(FormalParameter object) {
					return createParameterAdapter(object);
				}
				
				
				public HelperAdapter<?> caseEffectiveParameter(EffectiveParameter object) {
					return createParameterAdapter(object);
				}
				
				
				public HelperAdapter<?> caseValue(Value object) {
					return createValueAdapter(object);
				}
				
				
				public HelperAdapter<?> caseBinding(Binding object) {
					return createBindingAdapter(object);
				}
				
				
				public HelperAdapter<?> caseInterface(Interface object) {
					return createInterfaceAdapter(object);
				}
				
				
				public HelperAdapter<?> caseContent(Content object) {
					return createContentAdapter(object);
				}
				
				
				public HelperAdapter<?> caseController(Controller object) {
					return createControllerAdapter(object);
				}
				
				
				public HelperAdapter<?> caseAttributesController(AttributesController object) {
					return createAttributesControllerAdapter(object);
				}
				
				
				public HelperAdapter<?> caseAttribute(Attribute object) {
					return createAttributeAdapter(object);
				}
				
				public HelperAdapter<?> caseVirtualNode(VirtualNode object) {
					return createVirtualNodeAdapter(object);
				}
				
		};
		
		/**
		 * Creates a new HelperAdapter object.
		 * 
		 * @param system the system
		 * 
		 * @return the system helper adapter
		 */
		public SystemHelperAdapter createSystemAdapter(System system) {
			return new SystemHelperAdapter(system);
		}
		
		/**
		 * Creates a new HelperAdapter object.
		 * 
		 * @param definition the definition
		 * 
		 * @return the definition helper adapter
		 */
		public DefinitionHelperAdapter createDefinitionAdapter(Definition definition) {
			return new DefinitionHelperAdapter(definition);
		}
		
		/**
		 * Creates a new HelperAdapter object.
		 * 
		 * @param definitionCall the definition call
		 * 
		 * @return the definition call helper adapter
		 */
		public DefinitionCallHelperAdapter createDefinitionCallAdapter(DefinitionCall definitionCall) {
			return new DefinitionCallHelperAdapter(definitionCall);
		}
		
		
		/**
		 * Creates a new HelperAdapter object.
		 * 
		 * @param component the component
		 * 
		 * @return the component helper adapter
		 */
		public ComponentHelperAdapter createComponentAdapter(Component component) {
			return new ComponentHelperAdapter(component);
		}
		
		/**
		 * Creates a new HelperAdapter object.
		 * 
		 * @param parameter the parameter
		 * 
		 * @return the parameter helper adapter
		 */
		public ParameterHelperAdapter createParameterAdapter(Parameter parameter) {
			return new ParameterHelperAdapter(parameter);
		}
		
		/**
		 * Creates a new HelperAdapter object.
		 * 
		 * @param value the value
		 * 
		 * @return the value helper adapter
		 */
		public ValueHelperAdapter createValueAdapter(Value value) {
			return new ValueHelperAdapter(value);
		}
		
		/**
		 * Creates a new HelperAdapter object.
		 * 
		 * @param binding the binding
		 * 
		 * @return the binding helper adapter
		 */
		public BindingHelperAdapter createBindingAdapter(Binding binding) {
			return new BindingHelperAdapter(binding);
		}
		
		/**
		 * Creates a new HelperAdapter object.
		 * 
		 * @param interface_ the interface_
		 * 
		 * @return the interface helper adapter
		 */
		public InterfaceHelperAdapter createInterfaceAdapter(Interface interface_) {
			return new InterfaceHelperAdapter(interface_);
		}
		
		/**
		 * Creates a new HelperAdapter object.
		 * 
		 * @param content the content
		 * 
		 * @return the content helper adapter
		 */
		public ContentHelperAdapter createContentAdapter(Content content) {
			return new ContentHelperAdapter(content);
		}
		
		/**
		 * Creates a new HelperAdapter object.
		 * 
		 * @param membrane the membrane
		 * 
		 * @return the membrane helper adapter
		 */
		public ControllerHelperAdapter createControllerAdapter(Controller membrane) {
			return new ControllerHelperAdapter(membrane);
		}
		
		/**
		 * Creates a new HelperAdapter object.
		 * 
		 * @param attributesController the attributes controller
		 * 
		 * @return the attributes controller helper adapter
		 */
		public AttributesControllerHelperAdapter createAttributesControllerAdapter(AttributesController attributesController) {
			return new AttributesControllerHelperAdapter(attributesController);
		}
		
		/**
		 * Creates a new HelperAdapter object.
		 * 
		 * @param attribute the attribute
		 * 
		 * @return the attribute helper adapter
		 */
		public AttributeHelperAdapter createAttributeAdapter(Attribute attribute) {
			return new AttributeHelperAdapter(attribute);
		}
		
		
		public VirtualNodeHelperAdapter createVirtualNodeAdapter(VirtualNode virtualNode) {
			return new VirtualNodeHelperAdapter(virtualNode);
		}
		
		protected boolean shouldHaveAdapter(EObject eObject){
			boolean result = false;
			if(eObject == null){
				return false;
			}
			int id = eObject.eClass().getClassifierID();
			switch(id){
				case FractalPackage.SYSTEM:
				case FractalPackage.DEFINITION:
				case FractalPackage.DEFINITION_CALL:
				case FractalPackage.COMPONENT:
				case FractalPackage.PARAMETER : 
				case FractalPackage.FORMAL_PARAMETER : 
				case FractalPackage.EFFECTIVE_PARAMETER : 
				case FractalPackage.BINDING:
				case FractalPackage.INTERFACE:
				case FractalPackage.CONTENT:
				case FractalPackage.CONTROLLER:
				case FractalPackage.ATTRIBUTES_CONTROLLER:
				case FractalPackage.ATTRIBUTE:
				case FractalPackage.VIRTUAL_NODE:
				case FractalPackage.VALUE:
					result = true;
					break;
			}
			return result;
		}
		
		/**
		 * Get the helper associated to the eObject.
		 * If the eObject doesn't have a helper yet, a new one is created 
		 * if it is possible.
		 * 
		 * @param eObject the e object
		 * 
		 * @return the helper<?>
		 */
		public IHelper<?> adapt(EObject eObject)
		{
			if(shouldHaveAdapter(eObject) == false){
				return null;
			}
			
			int id = eObject.eClass().getClassifierID();
			Adapter adapter = null;
			
			switch(id){
				case FractalPackage.SYSTEM:
					adapter = adapt(eObject,ISystemHelper.class);
					break;
				case FractalPackage.DEFINITION:
					adapter = adapt(eObject,IDefinitionHelper.class);
					break;
				case FractalPackage.DEFINITION_CALL:
					adapter = adapt(eObject,IDefinitionCallHelper.class);
					break;
				case FractalPackage.COMPONENT:
					adapter = adapt(eObject,IComponentHelper.class);
					break;
				case FractalPackage.PARAMETER : 
					adapter = adapt(eObject,IParameterHelper.class);
					break;
				case FractalPackage.FORMAL_PARAMETER : 
					adapter = adapt(eObject,IParameterHelper.class);
					break;
				case FractalPackage.EFFECTIVE_PARAMETER : 
					adapter = adapt(eObject,IParameterHelper.class);
					break;
				case FractalPackage.BINDING:
					adapter = adapt(eObject,IBindingHelper.class);
					break;
				case FractalPackage.INTERFACE:
					adapter = adapt(eObject,IInterfaceHelper.class);
					break;
				case FractalPackage.CONTENT:
					adapter = adapt(eObject,IContentHelper.class);
					break;
				case FractalPackage.CONTROLLER:
					adapter = adapt(eObject,IControllerHelper.class);
					break;
				case FractalPackage.ATTRIBUTES_CONTROLLER:
					adapter = adapt(eObject,IAttributesControllerHelper.class);
					break;
				case FractalPackage.ATTRIBUTE:
					adapter = adapt(eObject,IAttributeHelper.class);
					break;
				case FractalPackage.VIRTUAL_NODE:
					adapter = adapt(eObject,IVirtualNodeHelper.class);
					break;
				case FractalPackage.VALUE:
					adapter = adapt(eObject,IValueHelper.class);
					break;
			}
			
			if(adapter == null){
				return null;	
				//throw(new NullPointerException("The eObject : " + eObject + " should have a HelperAdapter."));
			}
			
			return (IHelper<?>)adapter;
		}
		
		public Adapter createAdapter(Notifier target) {
			Adapter adapter = modelSwitch.doSwitch((EObject)target);
			
			if(adapter != null && !target.eAdapters().contains(adapter)){
				target.eAdapters().add(adapter);
			}
			return adapter;
		}
}
