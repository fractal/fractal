/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */

package org.ow2.fractal.f4e.fractal.ocl.adapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.ui.MarkerHelper;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.ocl.Query;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.helper.OCLHelper;
import org.eclipse.ocl.utilities.UMLReflection;
import org.ow2.fractal.f4e.fractal.Attribute;
import org.ow2.fractal.f4e.fractal.AttributesController;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.Content;
import org.ow2.fractal.f4e.fractal.Controller;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.TemplateController;
import org.ow2.fractal.f4e.fractal.VirtualNode;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;
import org.ow2.fractal.f4e.fractal.notification.IFractalNotification;
import org.ow2.fractal.f4e.fractal.ocl.util.ConstraintDescription;
import org.ow2.fractal.f4e.fractal.util.FractalResourcesUtil;

/**
 * The Class ValidatorAdapter.
 */
public class ValidatorAdapter extends EObjectValidator {

	/** The helper. */
	private static OCLHelper<EClassifier, EOperation, ?, Constraint> helper;

	/** The OC l_ env. */
	private static OCL OCL_ENV;

	/** The contraints. */
	private static Map<Constraint,ConstraintDescription> constraints = new HashMap<Constraint,ConstraintDescription>();

	private MarkerHelper markerHelper = new MarkerHelper();
	
	private FractalResourcesUtil fractalResourcesUtil = new FractalResourcesUtil();
	/**
	 * Instantiates a new validator adapter.
	 */
	private ValidatorAdapter(){	
		super();
		OCL_ENV = OCL.newInstance(EcoreEnvironmentFactory.INSTANCE);
		helper = OCL_ENV.createOCLHelper();
		helper.setContext(FractalPackage.Literals.SYSTEM);
	}

	/**
	 * Gets the oCL.
	 * 
	 * @return the oCL
	 */
	public OCL getOCL(){
		return OCL_ENV;
	}

	/**
	 * Gets the single instance of ValidatorAdapter.
	 * 
	 * @return single instance of ValidatorAdapter
	 */
	public static ValidatorAdapter getInstance(){
		return SingletonHolder.instance;
	}

	/**
	 * The Class SingletonHolder.
	 */
	private static class SingletonHolder {

		/** The instance. */
		private static ValidatorAdapter instance = new ValidatorAdapter();
	}


	public Constraint[] getConstraints(){
		return constraints.keySet().toArray(new Constraint[]{});
	}
	
	public Map<Constraint,ConstraintDescription> getConstraintsMap(){
		return constraints;
	}
	/*

 	public boolean validate(EObject eObject, DiagnosticChain diagnostics,
         Map context) {
     return validate(eObject.eClass(), eObject, diagnostics, context);
 	}


	private void processed(EObject eObject, Map context, IStatus status) {
        if (context != null) {
            context.put(eObject, status);
        }
    }

    private boolean hasProcessed(EObject eObject, Map context) {
        boolean result = false;

        if (context != null) {
            while (eObject != null) {
                if (context.containsKey(eObject)) {
                    result = true;
                    eObject = null;
                } else {
                    eObject = eObject.eContainer();
                }
            }
        }

        return result;
    }

    private void appendDiagnostics(IStatus status, DiagnosticChain diagnostics) {
        if (status.isMultiStatus()) {
            IStatus[] children = status.getChildren();

            for (int i = 0; i < children.length; i++) {
                appendDiagnostics(children[i], diagnostics);
            }
        } else if (status instanceof IConstraintStatus) {
            diagnostics.add(new BasicDiagnostic(
                status.getSeverity(),
                status.getPlugin(),
                status.getCode(),
                status.getMessage(),
                ((IConstraintStatus) status).getResultLocus().toArray()));
        }
    }

    public boolean validate(EClass eClass, EObject eObject,
            DiagnosticChain diagnostics, Map context) {
        super.validate(eClass, eObject, diagnostics, context);

        IStatus status = Status.OK_STATUS;

        if (diagnostics != null) {
            if (!hasProcessed(eObject, context)) {
                status = batchValidator.validate(
                    eObject,
                    new NullProgressMonitor());

                processed(eObject, context, status);

                appendDiagnostics(status, diagnostics);
            }
        }

        return status.isOK();
    }
	 */

	/**
	 * Processed.
	 * 
	 * @param eObject the e object
	 * @param context the context
	 * @param status the status
	 */
	private void processed(EObject eObject, Map context, IStatus status) {
		if (context != null) {
			context.put(eObject, status);
		}
	}

	/**
	 * Checks for processed.
	 * 
	 * @param eObject the e object
	 * @param context the context
	 * 
	 * @return true, if successful
	 */
	private boolean hasProcessed(EObject eObject, Map context) {
		boolean result = false;

		if (context != null) {
			while (eObject != null) {
				if (context.containsKey(eObject)) {
					result = true;
					eObject = null;
				} else {
					eObject = eObject.eContainer();
				}
			}
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.ecore.util.EObjectValidator#getEPackage()
	 */
	
	protected EPackage getEPackage() {
		return FractalPackage.eINSTANCE;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.ecore.util.EObjectValidator#validate(int, java.lang.Object, org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 */
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
		case FractalPackage.DEFINITION:
			return validateDefinition((Definition)value, diagnostics, context);
		case FractalPackage.COMPONENT:
			return validateComponent((Component)value, diagnostics, context);
		case FractalPackage.INTERFACE:
			return validateInterface((Interface)value, diagnostics, context);
		case FractalPackage.BINDING:
			return validateBinding((Binding)value, diagnostics, context);
		case FractalPackage.ATTRIBUTES_CONTROLLER:
			return validateAttributesController((AttributesController)value, diagnostics, context);
		case FractalPackage.ATTRIBUTE:
			return validateAttribute((Attribute)value, diagnostics, context);
		case FractalPackage.CONTENT:
			return validateContent((Content)value, diagnostics, context);
		case FractalPackage.CONTROLLER:
			return validateController((Controller)value, diagnostics, context);
		case FractalPackage.TEMPLATE_CONTROLLER:
			return validateTemplateController((TemplateController)value, diagnostics, context);
		case FractalPackage.VIRTUAL_NODE:
			return validateVirtualNode((VirtualNode)value, diagnostics, context);
		default: return true;
		}
//		IStatus status = Status.OK_STATUS;
//		if (!hasProcessed((EObject)value, context)) {
//			processed((EObject)value, context, status);
//			return 	validate((EObject)value,diagnostics,context);
//		}
	}

	public boolean validateDefinition(Definition definition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(definition, diagnostics, context);
	}
	
	public boolean validateComponent(Component component, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(component, diagnostics, context);
	}
	
	public boolean validateInterface(Interface interface_, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(interface_, diagnostics, context);
	}
	
	public boolean validateBinding(Binding binding, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(binding, diagnostics, context);
	}
	
	
	public boolean validateAttributesController(AttributesController attributesController, DiagnosticChain diagnostics, Map<Object, Object> context){
		return validate_EveryDefaultConstraint(attributesController, diagnostics, context);
	}

	public boolean validateAttribute(Attribute attribute, DiagnosticChain diagnostics, Map<Object, Object> context){
		return validate_EveryDefaultConstraint(attribute, diagnostics, context);
	}

	public boolean validateContent(Content content, DiagnosticChain diagnostics, Map<Object, Object> context){
		return validate_EveryDefaultConstraint(content, diagnostics, context);
	}

	public boolean validateController(Controller controller, DiagnosticChain diagnostics, Map<Object, Object> context){
		return validate_EveryDefaultConstraint(controller, diagnostics, context);
	}

	public boolean validateTemplateController(TemplateController templateController, DiagnosticChain diagnostics, Map<Object, Object> context){
		return validate_EveryDefaultConstraint(templateController, diagnostics, context);
	}

	public boolean validateVirtualNode(VirtualNode virtualNode, DiagnosticChain diagnostics, Map<Object, Object> context){
		return validate_EveryDefaultConstraint(virtualNode, diagnostics, context);
	}
	
	public boolean validate_EveryDefaultConstraint(EObject object, DiagnosticChain theDiagnostics, Map<Object, Object> context){
		super.validate_EveryDefaultConstraint(object, theDiagnostics, context);
		
		// we suppress all previous markers present on the object
		fractalResourcesUtil.deleteMarkers(object);
		
		boolean result = validate(object, theDiagnostics, context);
		//result = result ;
			if(object instanceof IHelperAdapter){
				boolean oldValue = ((IHelperAdapter)object).getHelper().getMarkerHelper().hasErrors();
				if(result == false){
					((IHelperAdapter)object).getHelper().getMarkerHelper().setHasErrors(true);
				}else{
					((IHelperAdapter)object).getHelper().getMarkerHelper().setHasErrors(false);
				}
			
				// Notification to update the problem icon on the element icon.
				object.eNotify(new ENotificationImpl((InternalEObject)object, IFractalNotification.UPDATE_VALIDATION_DECORATION, FractalPackage.INTERFACE__ROLE, result, oldValue, false));
			}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.emf.ecore.util.EObjectValidator#validate(org.eclipse.emf.ecore.EObject, org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 */
	public boolean validate(EObject eObject, DiagnosticChain diagnostics, Map context) {
		boolean result = true;

		
		for(Constraint constraint: getConstraints()){
			
			helper.setContext(constraint.getSpecification().getContextVariable().getType());
			Query<EClassifier, ?, ?> query = OCL_ENV.createQuery(constraint);
			List<String> list = new ArrayList<String>();
			
			if(isInvariant(constraint)){
				if(constraint.getSpecification().getContextVariable().getType() == eObject.eClass() ||
						eObject.eClass().getEAllSuperTypes().contains(constraint.getSpecification().getContextVariable().getType())){
			
					if (!query.check(eObject)) {
						if (diagnostics != null) {
							String description = getConstraintsMap().get(constraint)!=null?getConstraintsMap().get(constraint).getDescription():constraint.getName();
							BasicDiagnostic diagnostic = 
								new BasicDiagnostic(Diagnostic.ERROR,
										EcoreUtil.getURI(eObject).toString(), 0, EcorePlugin.INSTANCE
										.getString(
												"_UI_GenericConstraint_diagnostic",
												new Object[] {
														description,
														getObjectLabel(eObject,
																context) }),
																new Object[] { eObject});
							diagnostics.add(diagnostic);
							
							System.err.println("Constraint : " + constraint.getSpecification().getBodyExpression() + " is not respected.");

							fractalResourcesUtil.createMarkers(eObject.eResource(), diagnostic);
							
						}
						
						result= false;
					}
				}
			}
		}
	
		return result;
	}

	/**
	 * Gets the helper.
	 * 
	 * @return the helper
	 */
	public OCLHelper<EClassifier, EOperation, ?, Constraint> getHelper(){
		return helper;
	}

	/**
	 * Checks if is invariant.
	 * 
	 * @param constraint the constraint
	 * 
	 * @return true, if is invariant
	 */
	private boolean isInvariant(Constraint constraint) {
		return UMLReflection.INVARIANT.equals(constraint.getStereotype());
	}

	/**
	 * Checks if is definition.
	 * 
	 * @param constraint the constraint
	 * 
	 * @return true, if is definition
	 */
	private boolean isDefinition(Constraint constraint) {
		return UMLReflection.DEFINITION.equals(constraint.getStereotype());
	}

	/**
	 * Checks if is derivation.
	 * 
	 * @param constraint the constraint
	 * 
	 * @return true, if is derivation
	 */
	private boolean isDerivation(Constraint constraint) {
		return UMLReflection.DERIVATION.equals(constraint.getStereotype());
	}
	
}
