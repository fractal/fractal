package org.ow2.fractal.f4e.fractal.util;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.swt.widgets.Display;
import org.ow2.fractal.f4e.fractal.adapter.OperationHistoryListenerAdapter;
import org.ow2.fractal.f4e.fractal.adapter.factory.OperationHistoryListenerAdapterFactory;
import org.ow2.fractal.f4e.fractal.commands.FractalReloadCommand;
import org.ow2.fractal.f4e.fractal.commands.IFractalReloadCommand;
import org.ow2.fractal.f4e.fractal.util.visitor.UpdateMergeJob;
import org.ow2.fractal.f4e.fractal.util.visitor.UpdateMergeVisitor;


public class FractalWorkspaceSynchronizer implements WorkspaceSynchronizer.Delegate{

	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	public boolean handleResourceChanged(Resource resource) {
		// TODO Auto-generated method stub
		if(resource instanceof IFractalResource){
			//FractalTransactionalEditingDomain.getResourceSetListener().addChangedResource(resource);
			
			if(resource instanceof IFractalResource){
				class ResourceChangedHandler implements Runnable{
					private Resource resource;
					public ResourceChangedHandler(Resource resource){
						this.resource = resource;
					}
					
					public void run() {
						OperationHistoryListenerAdapter helper =  OperationHistoryListenerAdapterFactory.getInstance().adapt(resource);
						
						// We reload the resource only if there was modifications not
						// executed by editors on the Fractal file (the resource)
						if(helper.isExternalLastChange()){
							IFractalReloadCommand reloadCommand = new FractalReloadCommand(((IFractalResource)resource),null);
							reloadCommand.reload();
						}
					}
				};
				
				// We reload and update the merge asynchronously
				Thread t = new Thread(new ResourceChangedHandler(resource));
				t.setPriority(Thread.MIN_PRIORITY);
				t.start();
			}
		}
		return true;
	}

	public boolean handleResourceDeleted(Resource resource) {
		if(resource instanceof IFractalResource){
			
			
			class ResourceDeletedHandler implements Runnable{
				private Resource resource;
				
				public ResourceDeletedHandler(Resource resource){
					this.resource = resource;
				}
				
				public void run() {
					try{
						Command cmd = new RecordingCommand(FractalTransactionalEditingDomain
								.getEditingDomain()) {
							protected void doExecute() {
									ResourceSet resourceSet = resource.getResourceSet();
										if(resourceSet!=null){
											resource.getResourceSet().getResources().remove(resource);
										}
										try{
											resource.delete(null);
										}catch(Exception e){
											e.printStackTrace();
										}
							}
						};
						FractalTransactionalEditingDomain.getEditingDomain().getCommandStack()
						.execute(cmd);
						
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			};
			
			Display.getDefault().asyncExec(new ResourceDeletedHandler(resource));
			
		}
		return true;
	}

	public boolean handleResourceMoved(Resource resource, final URI newURI) {
		if(resource instanceof IFractalResource){
			class ResourceMovedHandler implements Runnable{
				private Resource resource;
				public ResourceMovedHandler(Resource resource){
					this.resource = resource;
				}
				
				public void run() {
					IDefinitionLocatorHelper definitionLocatorHelper = SDefinitionLoadLocator.getInstance().getHelper(resource.getURI());
					if(definitionLocatorHelper != null){
						SDefinitionLoadLocator.getInstance().removeHelper(resource.getURI());
						SDefinitionLoadLocator.getInstance().addHelper(newURI, definitionLocatorHelper);
					}
					
					resource.setURI(newURI);
//					OperationHistoryListenerAdapter helper =  OperationHistoryListenerAdapterFactory.getInstance().adapt(resource);
					
//					IFractalReloadCommand reloadCommand = new FractalReloadCommand(((IFractalResource)resource),null);
//					reloadCommand.reload();
				
				}
			};
			
			Display.getDefault().asyncExec(new ResourceMovedHandler(resource));
			
		}
		return true;
	}
			
}
