/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */

package org.ow2.fractal.f4e.fractal.ocl.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.TokenRewriteStream;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.ocl.OCLInput;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.helper.OCLHelper;
import org.eclipse.ocl.utilities.UMLReflection;
import org.ow2.fractal.f4e.fractal.ocl.adapter.ValidatorAdapter;
import org.ow2.fractal.f4e.fractal.parser.OCLCommentsLexer;
import org.ow2.fractal.f4e.fractal.parser.OCLCommentsParser;
import org.ow2.fractal.f4e.fractal.plugin.FractalPlugin;


/**
 * The Class OCLConstraintLoader.
 */
public class OCLConstraintLoader {
	
	/** The ocl. */
	private OCL ocl;
	
	/**
	 * Instantiates a new oCL constraint loader.
	 */
	public OCLConstraintLoader(){
		ocl = ValidatorAdapter.getInstance().getOCL();
	}
	
	/**
	 * Load.
	 * 
	 * @param oclFileURL the ocl file url
	 */
	public void load(IPath oclFilePath){
		try{
			List<ConstraintDescription> constraintDescriptions = loadConstraintDescriptions(this.getClass().getClassLoader().getResourceAsStream(oclFilePath.toString()));
			load(this.getClass().getClassLoader().getResourceAsStream(oclFilePath.toString()), constraintDescriptions);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public List<ConstraintDescription> loadConstraintDescriptions(InputStream inputStream){
		// Get all constraints description
		try{
		ANTLRInputStream input = new ANTLRInputStream(inputStream);
		OCLCommentsLexer lexer = new OCLCommentsLexer(input);
		
		TokenRewriteStream tokens = new TokenRewriteStream(lexer);
		OCLCommentsParser grammar = new OCLCommentsParser(tokens);
		grammar.parse();
		inputStream.close();
		return grammar.getConstraintDescriptions();
		}catch(Exception e){
			FractalPlugin.getDefault().getLog().log(new Status(IStatus.ERROR,FractalPlugin.PLUGIN_ID,"OCL constraint descriptions parsing fails",e));
		}
		return null;
	}
	
	/**
	 * Load.
	 * 
	 * @param inputStream the input stream
	 */
	public void load(InputStream inputStream, List<ConstraintDescription> descriptions){
		try{
    		// Create constraints
			OCLInput oclInput = new OCLInput(inputStream);
			List<Constraint> constraints = ocl.parse(oclInput);
			
			OCLHelper<EClassifier, EOperation, ?, Constraint> helper = ValidatorAdapter.getInstance().getHelper();
			
			int i=0;
			for (Constraint constraint : constraints) {	
					if (isInvariant(constraint)) {
						constraint.setName(constraint.getSpecification().getBodyExpression().toString());	
						ConstraintDescription desc = descriptions.get(i);
						ValidatorAdapter.getInstance().getConstraintsMap().put(constraint, desc);
						i++;
					}
			}
			inputStream.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Checks if is invariant.
	 * 
	 * @param constraint the constraint
	 * 
	 * @return true, if is invariant
	 */
	private boolean isInvariant(Constraint constraint) {
		return UMLReflection.INVARIANT.equals(constraint.getStereotype());
	}
	
}
