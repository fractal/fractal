/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.util.visitor;

import java.util.Iterator;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Attribute;
import org.ow2.fractal.f4e.fractal.AttributesController;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.Content;
import org.ow2.fractal.f4e.fractal.Controller;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.EffectiveParameter;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.Reference;
import org.ow2.fractal.f4e.fractal.System;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.util.FractalSwitch;

public class ModelVisitor <T> extends FractalSwitch<T> {
	Visitor visitor;
	EcoreUtil.Copier copier = new EcoreUtil.Copier();
	Resource targetResource;
	
	public ModelVisitor(Class c){
		super();
		try{
			visitor = (Visitor)c.newInstance();
		}catch(Exception e){
			java.lang.System.err.println(e.getMessage());
		}
	}
	
	/**
	 * 
	 * @return a merged Definition
	 */
	public void mergeDefinition(Definition sourceDefinition){
	}
	
	/* (non-Javadoc)
	 * @see org.ow2.fractal.adl.extended.fractaladlextended.util.FractalAdlExtendedSwitch#caseAttribute(org.ow2.fractal.adl.extended.fractaladlextended.Attribute)
	 */
	
	public T caseAttribute(Attribute object) {
		visitor.visit(object);
		
		doSwitch(object.getNameAST());
		doSwitch(object.getValueAST());
		
		return super.caseAttribute(object);
	}

	
	public T caseAttributesController(AttributesController object) {
		visitor.visit(object);
		doSwitch(object.getAttributes());
		return super.caseAttributesController(object);
	}

	
	public T caseBinding(Binding object) {
		visitor.visit(object);
		return super.caseBinding(object);
	}

	
	public T caseComponent(Component object) {
		visitor.visit(object);
		
		doSwitch(object.getSubComponents());
		doSwitch(object.getAttributesController());
		doSwitch(object.getContent());
		doSwitch(object.getInterfaces());
		doSwitch(object.getBindings());
		doSwitch(object.getController());
		
		return super.caseComponent(object);
	}

	
	public T caseAbstractComponent(AbstractComponent object) {
		visitor.visit(object);
		return super.caseAbstractComponent(object);
	}

	
	public T caseContent(Content object) {	
		visitor.visit(object);
		return super.caseContent(object);
	}

	
	public T caseDefinition(Definition object) {
		visitor.visit(object);
	
		doSwitch(object.getSubComponents());
		doSwitch(object.getAttributesController());
		doSwitch(object.getContent());
		doSwitch(object.getInterfaces());
		doSwitch(object.getBindings());
		doSwitch(object.getController());
		
		return null;
	}

	
	public T caseInterface(Interface object) {
		visitor.visit(object);
		
		doSwitch(object.getNameAST());
		doSwitch(object.getSignatureAST());
		
		return super.caseInterface(object);
	}

	
	public T caseMembrane(Controller object) {
		visitor.visit(object);
		return super.caseController(object);
	}

	
	public T caseSystem(System object) {
		visitor.visit(object);
		
		doSwitch(object.getDefinitions());
		
		return super.caseSystem(object);
	}
	
	
	public T caseDefinitionCall(DefinitionCall object) {
		visitor.visit(object);
		
		doSwitch(object.getParameters());
		return super.caseDefinitionCall(object);
	}
	
	
	public T caseEffectiveParameter(EffectiveParameter object) {
		visitor.visit(object);
		
		doSwitch(object.getName());
		doSwitch(object.getValue());
		
		return super.caseEffectiveParameter(object);
	}
	
	
	public T caseValue(Value object) {
		visitor.visit(object);
		
		doSwitch(object.getElements());
		
		return super.caseValue(object);
	}
	
	
	public T caseReference(Reference object) {
		visitor.visit(object);
		return super.caseReference(object);
	}
	
	
	public T doSwitch(EObject theEObject) {
		if(theEObject == null) return null;
		return doSwitch(theEObject.eClass(), theEObject);
	}
	
	protected T doSwitch(EList<?> eList) {
		Iterator<?> iterator = eList.iterator();
		while(iterator.hasNext()){
			Object elem = iterator.next();
			
			if(elem instanceof EObject){
				doSwitch((EObject)elem);
			}
		}
		return null;
	}
	
	
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case FractalPackage.ATTRIBUTE: {
				Attribute attribute = (Attribute)theEObject;
				T result = caseAttribute(attribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.ATTRIBUTES_CONTROLLER: {
				AttributesController attributesController = (AttributesController)theEObject;
				T result = caseAttributesController(attributesController);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.CONTROLLER: {
				Controller controller = (Controller)theEObject;
				T result = caseController(controller);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.CONTENT: {
				Content content = (Content)theEObject;
				T result = caseContent(content);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.INTERFACE: {
				Interface interface_ = (Interface)theEObject;
				T result = caseInterface(interface_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.ABSTRACT_COMPONENT: {
				AbstractComponent abstractComponent = (AbstractComponent)theEObject;
				T result = caseAbstractComponent(abstractComponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.BINDING: {
				Binding binding = (Binding)theEObject;
				T result = caseBinding(binding);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.COMPONENT: {
				Component component = (Component)theEObject;
				T result = caseComponent(component);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.DEFINITION_CALL: {
				return null;
				/*DefinitionCall definitionCall = (DefinitionCall)theEObject;
				T result = caseDefinitionCall(definitionCall);
				if (result == null) result = defaultCase(theEObject);
				return result;*/
			}
			case FractalPackage.DEFINITION: {
				Definition definition = (Definition)theEObject;
				T result = caseDefinition(definition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.SYSTEM: {
				System system = (System)theEObject;
				T result = caseSystem(system);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FractalPackage.PARAMETER: {
				return null;
				/*
				Parameter parameter = (Parameter)theEObject;
				T result = caseParameter(parameter);
				if (result == null) result = defaultCase(theEObject);
				return result;*/
			}
			case FractalPackage.EFFECTIVE_PARAMETER: {
				return null;
				
				/*
				EffectiveParameter effectiveParameter = (EffectiveParameter)theEObject;
				T result = caseEffectiveParameter(effectiveParameter);
				//if (result == null) result = caseParameter(effectiveParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;*/
			}
			case FractalPackage.FORMAL_PARAMETER: {
				return null;
				/*
				FormalParameter formalParameter = (FormalParameter)theEObject;
				T result = caseFormalParameter(formalParameter);
				//if (result == null) result = caseParameter(formalParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;*/
			}
			case FractalPackage.VALUE: {
				//We don't need to visit VALUE, as it is 
				// automatically created and should not be modified
				// manually
				return null;
				
				/*Value value = (Value)theEObject;
				T result = caseValue(value);
				if (result == null) result = defaultCase(theEObject);
				return result;*/
			}
			case FractalPackage.VALUE_ELEMENT: {
				return null;
				/*ValueElement valueElement = (ValueElement)theEObject;
				T result = caseValueElement(valueElement);
				if (result == null) result = defaultCase(theEObject);
				return result;*/
			}
			case FractalPackage.REFERENCE: {
				return null;
				
				/*Reference reference = (Reference)theEObject;
				T result = caseReference(reference);
				//if (result == null) result = caseValueElement(reference);
				if (result == null) result = defaultCase(theEObject);
				return result;*/
			}
			
			case FractalPackage.CONSTANT: {
				return null;
				
				/*
				Constant constant = (Constant)theEObject;
				T result = caseConstant(constant);
				//if (result == null) result = caseValueElement(constant);
				if (result == null) result = defaultCase(theEObject);
				return result;*/
			}
			default: return defaultCase(theEObject);
		}
	}
}
