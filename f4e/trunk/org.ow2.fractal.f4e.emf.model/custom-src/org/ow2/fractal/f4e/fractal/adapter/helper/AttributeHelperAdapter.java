/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter.helper;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.transaction.RecordingCommand;
import org.ow2.fractal.f4e.fractal.Attribute;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;


public class AttributeHelperAdapter extends HelperAdapter<Attribute>
	implements IAttributeHelper {
	
	public AttributeHelperAdapter(Attribute attribute) {
		super(attribute);
	}

	
	public boolean isAdapterForType(Object type) {
		return super.isAdapterForType(type) || type == IAttributeHelper.class;
	}
	
	/**
	 * Update the parsing of the 'value' attribute string.
	 */
	protected void updateValue(){
		if(t.getAttributesController() != null){
			t.setValueAST(ValueHelperAdapter.getValue(t.getValue(), t.getAttributesController().getAbstractComponent()));
		}
	}
	
	/**
	 * Update the parsing of the 'name' attribute string.
	 */
	protected void updateName(){
		if(t.getAttributesController() != null){
			t.setNameAST(ValueHelperAdapter.getValue(t.getName(), t.getAttributesController().getAbstractComponent()));
		}
	}
	
	public void update(final int featureID, IProgressMonitor monitor){
		Command cmd = new RecordingCommand(FractalTransactionalEditingDomain
				.getEditingDomain()) {
			protected void doExecute() {
				switch (featureID) {
					case FractalPackage.ATTRIBUTE__VALUE_AST:
							updateValue();
							
							// We have to update the Fractal ADL files 
							// which have a dependency relationship with 
							// the AbstractComponent container of this helper's model element.
							//updateChildrenMerge();
						break;
					case FractalPackage.ATTRIBUTE__NAME_AST:
							updateName();
						//	updateChildrenMerge();
						break;
					default:
						break;
				}
			}
		};

		FractalTransactionalEditingDomain.getEditingDomain().getCommandStack()
				.execute(cmd);
	}
	
}
