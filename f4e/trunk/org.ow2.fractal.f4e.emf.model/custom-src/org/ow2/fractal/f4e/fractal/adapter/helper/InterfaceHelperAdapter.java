/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter.helper;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.transaction.RecordingCommand;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.notification.IFractalNotification;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;

public class InterfaceHelperAdapter extends HelperAdapter<Interface>
	implements IInterfaceHelper {
	
	public InterfaceHelperAdapter(Interface interface_) {
		super(interface_);
	}

	public boolean isAdapterForType(Object type) {
		return super.isAdapterForType(type) || type == InterfaceHelperAdapter.class || type == IMerge.class;
	}
	
	public void updateSignature(){
		t.setSignatureAST(ValueHelperAdapter.getValue(t.getSignature(),t.getAbstractComponent()));	
	}
	
	public void notifyChanged(Notification notification) {
		// TODO Auto-generated method stub
		super.notifyChanged(notification);
		
    	if(notification.getNotifier() != this.getObject()){
    		return;
    	}
    	
    	int type = notification.getEventType();
    	int featureID = notification.getFeatureID(Interface.class);
    	
    	switch(type){
    	case Notification.SET:
    	case Notification.UNSET:
    	case Notification.ADD:
    	case Notification.ADD_MANY:
    	case Notification.REMOVE:
    	case Notification.REMOVE_MANY:
    		switch(featureID){
    			case FractalPackage.INTERFACE__NAME:
    			case FractalPackage.INTERFACE__CARDINALITY:
    			case FractalPackage.INTERFACE__CONTINGENCY:
    			case FractalPackage.INTERFACE__ROLE:
    			case FractalPackage.INTERFACE__SIGNATURE:
    				// When a interface is modified we must update all merged interfaces
        			// of children of the interface abstract component.
        			// merged components must be also updated because its merged interfaces can have been modified too.
        		AbstractComponent parent = this.getObject().getAbstractComponent();
        		if(parent != null){
//        				parent.getHelper().update(FractalPackage.ABSTRACT_COMPONENT__MERGED_INTERFACES);
//        				parent.getHelper().update(FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS);
//        				parent.getHelper().update(FractalPackage.ABSTRACT_COMPONENT__BINDINGS);
//        				parent.getHelper().update(FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS);
//        				parent.getHelper().updateChildren(FractalPackage.ABSTRACT_COMPONENT__MERGED_INTERFACES);
//        				parent.getHelper().updateChildren(FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS);
//        				parent.getHelper().updateChildren(FractalPackage.ABSTRACT_COMPONENT__BINDINGS);
//        				parent.getHelper().updateChildren(FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS);
//        				
//        				Definition parentDefinition = parent.getHelper().getParentDefinition();
//        				if(parentDefinition != parent){
//        					parentDefinition.getHelper().updateChildren(FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS);
//        				}
        		}	
    			default:	
    	    		break;
    		}
    		
    	default:	
    		break;
    	}
	}
	/* (non-Javadoc)
	 * @see org.ow2.fractal.adl.extended.fractalextended.adapter.helper.InterfaceHelper#updateName()
	 */
	public void updateName(){
		t.setNameAST(ValueHelperAdapter.getValue(t.getName(),t.getAbstractComponent()));
	}
	
	public void update(final int featureID, IProgressMonitor monitor){
		Command cmd = new RecordingCommand(FractalTransactionalEditingDomain
				.getEditingDomain()) {
			protected void doExecute() {
				switch (featureID) {
					case FractalPackage.INTERFACE__NAME_AST:
							updateName();
						break;
					case FractalPackage.INTERFACE__SIGNATURE_AST:
							updateSignature();
						break;
					default:
						break;
				}
			}
		};

		FractalTransactionalEditingDomain.getEditingDomain().getCommandStack()
				.execute(cmd);
	}
	
	
//	public Interface getElementWithSameName(List<Interface> interfaces){
//		Interface result = null;
//		if(interfaces == null){
//			return result;
//		}
//		
//		Iterator<Interface> iterator = interfaces.iterator();
//		
//		while(iterator.hasNext()){
//			Interface nextInterface = iterator.next();
//			if(nextInterface.getName() != null && nextInterface.getName().equals(t.getName())){
//				result = nextInterface;
//				break;
//			}
//		}
//		
//		return result;
//	}
	
	public void merge(HashMap<String,String> parameters, Interface srcNode, boolean override, HashMap<Integer, List<EObject>> tempFeatures){
		if(override == true){
		     t.setRole(srcNode.getRole());
		}else{
			if(!t.eIsSet(FractalPackage.eINSTANCE.getInterface_Role())){
				t.setMergedRole(srcNode.getRole());
				t.eNotify(new ENotificationImpl((InternalEObject)t, IFractalNotification.SET_MERGED_INTERFACE_ROLE, FractalPackage.INTERFACE__ROLE, t.getRole(), srcNode.getRole(), false));
			}
		}

		if(override == true){
			t.setCardinality(srcNode.getCardinality());    
		}else{
			if(!t.eIsSet(FractalPackage.eINSTANCE.getInterface_Cardinality())){
				t.setMergedCardinality(srcNode.getCardinality());
			}
		}

		if(override == true){
		       t.setContingency(srcNode.getContingency());
		}else{
			if(!t.eIsSet(FractalPackage.eINSTANCE.getInterface_Contingency())){
			 	t.setContingency(srcNode.getContingency());
			}
		}

		if(override == true){
		        t.setSignature(srcNode.getSignature());
		}else{
			if(!t.eIsSet(FractalPackage.eINSTANCE.getInterface_Signature())){
				t.setMergedSignature(srcNode.getSignatureAST().getHelper().resolve(parameters));
			}
		}
	}
	
	
	public void merge(List<Node<? extends EObject>> srcNodes, boolean override, IProgressMonitor monitor){
		Iterator<Node<? extends EObject>> iterator = srcNodes.iterator();
		while(iterator.hasNext()){
			Node<? extends EObject> node = iterator.next();
			EObject interface_ = node.getT();
			
			if(override == true){
				if(!t.eIsSet(FractalPackage.eINSTANCE.getInterface_Role())){
					t.eSet(FractalPackage.Literals.INTERFACE__MERGED_ROLE, interface_.eGet(FractalPackage.Literals.INTERFACE__ROLE));
				}
			}else{
				if(!t.eIsSet(FractalPackage.eINSTANCE.getInterface_Role())){
					t.eSet(FractalPackage.Literals.INTERFACE__MERGED_ROLE, interface_.eGet(FractalPackage.Literals.INTERFACE__ROLE));
					t.eNotify(new ENotificationImpl((InternalEObject)t, IFractalNotification.SET_MERGED_INTERFACE_ROLE, FractalPackage.INTERFACE__ROLE, t.getRole(), interface_.eGet(FractalPackage.Literals.INTERFACE__ROLE), false));
				}
			}

			if(override == true){
				t.eSet(FractalPackage.Literals.INTERFACE__MERGED_CARDINALITY, interface_.eGet(FractalPackage.Literals.INTERFACE__CARDINALITY));
			}else{
				if(!t.eIsSet(FractalPackage.eINSTANCE.getInterface_Cardinality())){
					t.eSet(FractalPackage.Literals.INTERFACE__MERGED_CARDINALITY, interface_.eGet(FractalPackage.Literals.INTERFACE__CARDINALITY));
				}
			}

			if(override == true){
				t.eSet(FractalPackage.Literals.INTERFACE__MERGED_CONTINGENCY, interface_.eGet(FractalPackage.Literals.INTERFACE__CONTINGENCY));
			}else{
				if(!t.eIsSet(FractalPackage.eINSTANCE.getInterface_Contingency())){
					t.eSet(FractalPackage.Literals.INTERFACE__MERGED_CONTINGENCY, interface_.eGet(FractalPackage.Literals.INTERFACE__CONTINGENCY));
				}
			}

			if(override == true){
				if(interface_.eGet(FractalPackage.Literals.INTERFACE__SIGNATURE_AST) != null){
					t.eSet(FractalPackage.Literals.INTERFACE__MERGED_SIGNATURE,((IValueHelper)((IHelperAdapter)interface_.eGet(FractalPackage.Literals.INTERFACE__SIGNATURE_AST)).getHelper()).resolve(node.getEffectiveParameters()));
				}else{
					t.eSet(FractalPackage.Literals.INTERFACE__MERGED_SIGNATURE, interface_.eGet(FractalPackage.Literals.INTERFACE__SIGNATURE));
				}
			}else{
				if(!t.eIsSet(FractalPackage.eINSTANCE.getInterface_Signature())){
					if(interface_.eGet(FractalPackage.Literals.INTERFACE__SIGNATURE_AST) != null){
						t.eSet(FractalPackage.Literals.INTERFACE__MERGED_SIGNATURE,((IValueHelper)((IHelperAdapter)interface_.eGet(FractalPackage.Literals.INTERFACE__SIGNATURE_AST)).getHelper()).resolve(node.getEffectiveParameters()));
					}else{
						t.eSet(FractalPackage.Literals.INTERFACE__MERGED_SIGNATURE, interface_.eGet(FractalPackage.Literals.INTERFACE__SIGNATURE));
					}
				}
			}
		}
	}
}
