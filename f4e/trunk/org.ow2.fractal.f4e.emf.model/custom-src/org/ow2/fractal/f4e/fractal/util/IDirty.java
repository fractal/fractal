package org.ow2.fractal.f4e.fractal.util;

import org.eclipse.core.commands.operations.IUndoableOperation;
import org.eclipse.core.commands.operations.UndoContext;

/**
 * A Fractal resource can be edited by more than one editor.
 * When a editor makes a change in the model it must be marked dirty
 * and all other editor working on the same model must be marked dirty.
 * 
 * When a editor save the resource all editors working on the same
 * resource must remove their dirty marker.
 * 
 * @author yann
 *
 */
public interface IDirty {
	public boolean isDirty();
	public void setDirty(boolean dirty);
}
