package org.ow2.fractal.f4e.fractal.util;

public interface IExceptionHandler {
	public Object handleException(Exception e);
}
