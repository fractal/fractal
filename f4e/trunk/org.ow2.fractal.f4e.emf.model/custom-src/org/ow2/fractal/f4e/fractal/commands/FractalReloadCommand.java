package org.ow2.fractal.f4e.fractal.commands;

import java.util.Map;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.util.IExceptionHandler;
import org.ow2.fractal.f4e.fractal.util.IFractalResource;
import org.ow2.fractal.f4e.fractal.util.visitor.UpdateMergeVisitor;

public class FractalReloadCommand extends RecordingCommand implements IFractalReloadCommand{

	private IFractalResource resource;
	private Map<Object, Object> saveOptions;
	private IExceptionHandler handler;
	
	public FractalReloadCommand(IFractalResource resource, IExceptionHandler handler){
		super(FractalTransactionalEditingDomain.getEditingDomain(),"Save Fractal Resource");
		this.resource = resource;
		this.saveOptions = saveOptions;
		this.handler = handler;
	}
	
	private FractalReloadCommand(TransactionalEditingDomain domain){
		super(domain);
	}
	
	private FractalReloadCommand(TransactionalEditingDomain domain, String label) {
		super(domain,label);
	}
	
	public void reload(){
		FractalTransactionalEditingDomain.getEditingDomain().getCommandStack().execute(this);
	}
	
	protected void doExecute() {
		try {
			resource.reload();
		}
		catch (Exception exception) {
			if(handler != null){
				handler.handleException(exception);
			}else{
				exception.printStackTrace();
			}
		}
	}
	
}
