/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter.factory;


import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.EObject;
import org.ow2.fractal.f4e.fractal.adapter.AttributeAdapter;
import org.ow2.fractal.f4e.fractal.adapter.AttributesControllerAdapter;
import org.ow2.fractal.f4e.fractal.adapter.BindingAdapter;
import org.ow2.fractal.f4e.fractal.adapter.ComponentAdapter;
import org.ow2.fractal.f4e.fractal.adapter.ContentAdapter;
import org.ow2.fractal.f4e.fractal.adapter.ControllerAdapter;
import org.ow2.fractal.f4e.fractal.adapter.DefinitionAdapter;
import org.ow2.fractal.f4e.fractal.adapter.DefinitionCallAdapter;
import org.ow2.fractal.f4e.fractal.adapter.InterfaceAdapter;
import org.ow2.fractal.f4e.fractal.adapter.SystemAdapter;
import org.ow2.fractal.f4e.fractal.util.FractalAdapterFactory;


/**
 * To be compatible with standard fractal ADL
 * some model element like Binding has ADL strings clientADL
 * and serverADL. Because our model is extended, we need to catch
 * event when user modify these strings to parse them and create
 * transparently the model elements that are the result of the parsing.
 * 
 * This factory create adapter for each element of the model which contains ADL string.
 * These created adapters catch the user event on ADL strings,
 * and update the model.
 * 
 */
public class EventAdapterFactory extends FractalAdapterFactory {
	
	/**
	 * Instantiates a new adapter factory.
	 */
	private EventAdapterFactory(){	
	}
	
	/**
	 * Gets the single instance of AdapterFactory.
	 * 
	 * @return single instance of AdapterFactory
	 */
	public static EventAdapterFactory getInstance(){
		return SingletonHolder.instance;
	}
	
	/**
	 * The Class SingletonHolder.
	 */
	private static class SingletonHolder {
		
		/** The instance. */
		private static EventAdapterFactory instance = new EventAdapterFactory();
	}

	public Adapter createSystemAdapter() {
		return SystemAdapter.getInstance();
	}
	
	public Adapter createDefinitionAdapter() {
		return DefinitionAdapter.getInstance();
	}
	
	public Adapter createDefinitionCallAdapter() {
		return DefinitionCallAdapter.getInstance();
	}
	public Adapter createComponentAdapter() {
		return ComponentAdapter.getInstance();
	}
	
	public Adapter createBindingAdapter() {
		return BindingAdapter.getInstance();
	}
	
	public Adapter createInterfaceAdapter() {
		return InterfaceAdapter.getInstance();
	}
	
	public Adapter createContentAdapter() {
		return ContentAdapter.getInstance();
	}
	
	
	public Adapter createControllerAdapter() {
		return ControllerAdapter.getInstance();
	}
	
	public Adapter createAttributesControllerAdapter() {
		return AttributesControllerAdapter.getInstance();
	}
	
	public Adapter createAttributeAdapter() {
		return AttributeAdapter.getInstance();
	}
	
	
	public Adapter createAdapter(Notifier target) {
		Adapter adapter = modelSwitch.doSwitch((EObject)target);
		if(adapter != null && !target.eAdapters().contains(adapter)){
			target.eAdapters().add(adapter);
		}
		return adapter;
	}
}
