/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.util;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.ecore.resource.ContentHandler;
import org.eclipse.emf.ecore.resource.URIHandler;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;

/**
 * The Class FractalAdlExtendedExtensibleURIConverterImpl.
 * 
 * This class is used to add our ClasspathURIHandlerImpl to the URIHandlers.
 * When we try to load a file with the "classpath://" protocol, we have to indicate
 * that we have to use our ClasspathURIHandlerImpl to load the file.
 */
public class FractalExtensibleURIConverterImpl extends ExtensibleURIConverterImpl{
	
	/**
	 * Instantiates a new fractal adl extended extensible uri converter impl.
	 */
	public FractalExtensibleURIConverterImpl(){
		 this(URIHandler.DEFAULT_HANDLERS, ContentHandler.Registry.INSTANCE.contentHandlers());
	}
	
	/**
	 * Instantiates a new fractal adl extended extensible uri converter impl.
	 * 
	 * @param uriHandlers the uri handlers
	 * @param contentHandlers the content handlers
	 */
	public FractalExtensibleURIConverterImpl(Collection<URIHandler> uriHandlers, Collection<ContentHandler> contentHandlers)
	  {
		getURIHandlers().clear();
		getURIHandlers().add(new ClasspathURIHandlerImpl());
	    getURIHandlers().addAll(uriHandlers);
	    getContentHandlers().addAll(contentHandlers);
	  }
	
}
