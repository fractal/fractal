// $ANTLR 3.0.1 custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3 2008-11-24 10:20:40

  package org.ow2.fractal.f4e.fractal.parser;
  
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ow2.fractal.f4e.fractal.ocl.util.ConstraintDescription;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;


import org.antlr.runtime.tree.*;

public class OCLCommentsParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "CONSTRAINT_LINE", "COMMENT_LINE", "CODE_LINE", "SEVERITY_LINE", "WS", "STRING", "SEVERITY", "CHARACTER"
    };
    public static final int CONSTRAINT_LINE=4;
    public static final int CODE_LINE=6;
    public static final int WS=8;
    public static final int SEVERITY=10;
    public static final int SEVERITY_LINE=7;
    public static final int EOF=-1;
    public static final int CHARACTER=11;
    public static final int COMMENT_LINE=5;
    public static final int STRING=9;

        public OCLCommentsParser(TokenStream input) {
            super(input);
        }
        
    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    public TreeAdaptor getTreeAdaptor() {
        return adaptor;
    }

    public String[] getTokenNames() { return tokenNames; }
    public String getGrammarFileName() { return "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3"; }



    	public static void main(String[] args){
        	try {
        		//ANTLRStringStream input = new ANTLRStringStream("-- test commentS ;");
        		ANTLRFileStream input = new ANTLRFileStream("custom-src/org/ow2/fractal/f4e/fractal/ocl/constraints.ocl");
        		OCLCommentsLexer lexer = new OCLCommentsLexer(input);
        		
        		TokenRewriteStream tokens = new TokenRewriteStream(lexer);
        		OCLCommentsParser grammar = new OCLCommentsParser(tokens);
        		grammar.parse();
        		grammar.print();
    			}catch(Exception e){
    				e.printStackTrace();
    			}
        }
    	
    	Stack<Object> stack = new Stack<Object>();
    	List<ConstraintDescription> constraintDescriptions = new ArrayList<ConstraintDescription>();
    	
    	public void pushCommentLine(String commentLine){
    		if(inDescription == true && !constraintDescriptions.isEmpty()){
    			constraintDescriptions.get(constraintDescriptions.size()-1).addCommentLine(commentLine);
    		}
    	}	
    	
    	private boolean inDescription = false;
    	
    	public void pushConstraintDescription(String description){
    		inDescription = true;
    		ConstraintDescription newDescription = new ConstraintDescription();
    		constraintDescriptions.add(newDescription);
    		newDescription.addCommentLine(description);
    	}
    	
    	public void endDescription(){
    		if(inDescription==true){
    			inDescription=false;
    		}
    	}
    	
    	public List<ConstraintDescription> getConstraintDescriptions(){
    		return constraintDescriptions;
    	}
    	
    	public void print(){
    		for(ConstraintDescription constraintDescription : constraintDescriptions){
    			System.out.println(constraintDescription.hashCode() + " : "+ constraintDescription.getDescription());
    		}
    	}


    public static class parse_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start parse
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:75:1: parse : ( CONSTRAINT_LINE | COMMENT_LINE | CODE_LINE | SEVERITY_LINE )+ EOF ;
    public final parse_return parse() throws RecognitionException {
        parse_return retval = new parse_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token CONSTRAINT_LINE1=null;
        Token COMMENT_LINE2=null;
        Token CODE_LINE3=null;
        Token SEVERITY_LINE4=null;
        Token EOF5=null;

        CommonTree CONSTRAINT_LINE1_tree=null;
        CommonTree COMMENT_LINE2_tree=null;
        CommonTree CODE_LINE3_tree=null;
        CommonTree SEVERITY_LINE4_tree=null;
        CommonTree EOF5_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:75:6: ( ( CONSTRAINT_LINE | COMMENT_LINE | CODE_LINE | SEVERITY_LINE )+ EOF )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:75:8: ( CONSTRAINT_LINE | COMMENT_LINE | CODE_LINE | SEVERITY_LINE )+ EOF
            {
            root_0 = (CommonTree)adaptor.nil();

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:75:8: ( CONSTRAINT_LINE | COMMENT_LINE | CODE_LINE | SEVERITY_LINE )+
            int cnt1=0;
            loop1:
            do {
                int alt1=5;
                switch ( input.LA(1) ) {
                case CONSTRAINT_LINE:
                    {
                    alt1=1;
                    }
                    break;
                case COMMENT_LINE:
                    {
                    alt1=2;
                    }
                    break;
                case CODE_LINE:
                    {
                    alt1=3;
                    }
                    break;
                case SEVERITY_LINE:
                    {
                    alt1=4;
                    }
                    break;

                }

                switch (alt1) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:75:9: CONSTRAINT_LINE
            	    {
            	    CONSTRAINT_LINE1=(Token)input.LT(1);
            	    match(input,CONSTRAINT_LINE,FOLLOW_CONSTRAINT_LINE_in_parse49); 
            	    CONSTRAINT_LINE1_tree = (CommonTree)adaptor.create(CONSTRAINT_LINE1);
            	    adaptor.addChild(root_0, CONSTRAINT_LINE1_tree);

            	    pushConstraintDescription(CONSTRAINT_LINE1.getText());

            	    }
            	    break;
            	case 2 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:76:4: COMMENT_LINE
            	    {
            	    COMMENT_LINE2=(Token)input.LT(1);
            	    match(input,COMMENT_LINE,FOLLOW_COMMENT_LINE_in_parse55); 
            	    COMMENT_LINE2_tree = (CommonTree)adaptor.create(COMMENT_LINE2);
            	    adaptor.addChild(root_0, COMMENT_LINE2_tree);

            	    pushCommentLine(COMMENT_LINE2.getText());

            	    }
            	    break;
            	case 3 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:77:4: CODE_LINE
            	    {
            	    CODE_LINE3=(Token)input.LT(1);
            	    match(input,CODE_LINE,FOLLOW_CODE_LINE_in_parse61); 
            	    CODE_LINE3_tree = (CommonTree)adaptor.create(CODE_LINE3);
            	    adaptor.addChild(root_0, CODE_LINE3_tree);

            	    endDescription();

            	    }
            	    break;
            	case 4 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/OCLComments.g3:78:4: SEVERITY_LINE
            	    {
            	    SEVERITY_LINE4=(Token)input.LT(1);
            	    match(input,SEVERITY_LINE,FOLLOW_SEVERITY_LINE_in_parse68); 
            	    SEVERITY_LINE4_tree = (CommonTree)adaptor.create(SEVERITY_LINE4);
            	    adaptor.addChild(root_0, SEVERITY_LINE4_tree);


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);

            EOF5=(Token)input.LT(1);
            match(input,EOF,FOLLOW_EOF_in_parse74); 
            EOF5_tree = (CommonTree)adaptor.create(EOF5);
            adaptor.addChild(root_0, EOF5_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end parse


 

    public static final BitSet FOLLOW_CONSTRAINT_LINE_in_parse49 = new BitSet(new long[]{0x00000000000000F0L});
    public static final BitSet FOLLOW_COMMENT_LINE_in_parse55 = new BitSet(new long[]{0x00000000000000F0L});
    public static final BitSet FOLLOW_CODE_LINE_in_parse61 = new BitSet(new long[]{0x00000000000000F0L});
    public static final BitSet FOLLOW_SEVERITY_LINE_in_parse68 = new BitSet(new long[]{0x00000000000000F0L});
    public static final BitSet FOLLOW_EOF_in_parse74 = new BitSet(new long[]{0x0000000000000002L});

}