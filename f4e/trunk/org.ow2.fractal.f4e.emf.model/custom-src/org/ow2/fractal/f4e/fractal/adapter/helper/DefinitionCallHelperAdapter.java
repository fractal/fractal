/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter.helper;

import java.util.HashMap;
import java.util.Iterator;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.EffectiveParameter;
import org.ow2.fractal.f4e.fractal.FormalParameter;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class DefinitionCallHelperAdapter.
 */
public class DefinitionCallHelperAdapter extends HelperAdapter<DefinitionCall> implements IDefinitionCallHelper{
	
	/**
	 * Instantiates a new definition call helper adapter.
	 * 
	 * @param definitionCall the definition call
	 */
	public DefinitionCallHelperAdapter(DefinitionCall definitionCall){
		super(definitionCall);
	}
	
	/* (non-Javadoc)
	 * @see org.ow2.fractal.adl.extended.fractalextended.adapter.helper.HelperAdapter#isAdapterForType(java.lang.Object)
	 */
	
	public boolean isAdapterForType(Object type) {
		return  super.isAdapterForType(type) || type == IDefinitionCallHelper.class;
	}
	
	
	public String toADLString(){
		if(t.getAbstractComponent() instanceof Component){
			return toADLString("=");
		}else if(t.getAbstractComponent() instanceof Definition){
			return toADLString("=>");
		}else{
			return "";
		}
	}
	
	private String toADLString(String affectationSymbols){
		String result = "";
		
		if(t.getDefinitionName() != null){
			result += HelperAdapterFactory.getInstance().adapt(t.getDefinitionName()).toString();
		}
		
		EList<EffectiveParameter> parameters = t.getParameters();
		java.util.Iterator<EffectiveParameter> iterator = parameters.iterator();
		
		if(t.getParameters().size()>0){
			result +="(";
		}
		while(iterator.hasNext()){
			EffectiveParameter parameter = iterator.next();
			if(parameter.getName() != null){
				result += HelperAdapterFactory.getInstance().adapt(parameter.getName()).toString();
				if(parameter.getValue() != null){
					result += affectationSymbols;
					result += HelperAdapterFactory.getInstance().adapt(parameter.getValue()).toString();
				}
			}else{
				if(parameter.getValue() != null){
					result += HelperAdapterFactory.getInstance().adapt(parameter.getValue()).toString();
				}
			}
		}
		if(t.getParameters().size()>0){
			result +=")";
		}
		
		return result;
	}
	
	
	public HashMap<String,String> getResolvedParameters(HashMap<String,String> resolvedParameters){
		HashMap<String,String> result = new HashMap<String,String>();
		
		// We add the formal parameter and their default value of the definition that we extend
		Definition calledDefinition =getObject().getDefinition();
		if(calledDefinition != null){
			Iterator<FormalParameter> iterator = calledDefinition.getArgumentsAST().iterator();
			
			int i = 0;
			while(iterator.hasNext()){
				FormalParameter next = iterator.next();
				
				
				if(next.getName() != null){
					String name = next.getName().getHelper().toString();
					String value = "";
					if(next.getValue() != null){
						value = next.getValue().getHelper().toString();
					}
					
					result.put(name, value);
				}
				
				i++;
			}
		}
		
		// We override default value with the ones passed by the definition call
		Iterator<EffectiveParameter> iterator = getObject().getParameters().iterator();
		boolean sameArgsNumber = calledDefinition.getArgumentsAST().size() == t.getParameters().size();
		int i = 0;
		while(iterator.hasNext()){
			EffectiveParameter next = iterator.next();
			
			String name = null;
			
			if(next.getName() != null){
				// When args are passed via : "name=>value,name=>value"
				name = next.getName().getHelper().toString();
			}else if(sameArgsNumber){
				// When args are passed via : "value,value,value"
				if(calledDefinition.getArgumentsAST().get(i).getName() != null){
					name = calledDefinition.getArgumentsAST().get(i).getName().getHelper().toString();
				}
			}
			
			if(name != null){
				String value = next.getValue().getHelper().resolve(resolvedParameters);
				result.put(name, value);
			}
			i++;
		}
		return result;
	}
	
	public void update(int featureID, IProgressMonitor monitor){
		
	}
	
	/**
	 * 
	 * @return true is the definition is called with only with arguments values 'calleddefinition(value1,value2...)'
	 *  else false
	 */
	public boolean isPassValuesOnly(){
		if(t.getParameters() != null && !t.getParameters().isEmpty() && t.getParameters().get(0).getName() == null){
			return true;
		}else{
			return false;
		}
	}
	
	
	public boolean isPassNamesValues(){
		if(t.getParameters() != null && !t.getParameters().isEmpty() && t.getParameters().get(0).getName() != null){
			return true;
		}else{
			return false;
		}
	}
	
	public String getValue(String resolvedParameterName){
		String value="";
		if(isPassValuesOnly()==true){
			if(t.getDefinition() != null && t.getDefinition().getArgumentsAST()!=null){
				int i=0;
				for(FormalParameter parameter:t.getDefinition().getArgumentsAST()){
					if(parameter.getName().getHelper().toString().equals(resolvedParameterName)){
						if(i<=t.getParameters().size() && t.getParameters().get(i) != null){
							value = t.getParameters().get(i).getValue().getHelper().toString();
							break;
						}
					}
					i++;
				}
			}
		}else if(isPassNamesValues()==true){
			for(EffectiveParameter parameter:t.getParameters()){
				if(parameter.getName().getHelper().resolve(new HashMap<String,String>()).equals(resolvedParameterName)){
					value = parameter.getValue().getHelper().toString();
					break;
				}
			}
		}
		return value;
	}
}
