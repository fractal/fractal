/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.adapter;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.AbstractComponentHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IInterfaceHelper;

public class InterfaceAdapter extends EContentAdapter {
   
	private InterfaceAdapter(){
		super();
	}
	
    /* (non-Javadoc)
     * @see org.eclipse.emf.ecore.util.EContentAdapter#notifyChanged(org.eclipse.emf.common.notify.Notification)
     */
    public void notifyChanged(final Notification notification) {
    	super.notifyChanged(notification);
    	if(!(notification.getNotifier() instanceof Interface)){
    		return;
    	}
    	
    	int type = notification.getEventType();
    	int featureID = notification.getFeatureID(Interface.class);
    
    	Interface interface_ = (Interface)notification.getNotifier();
   
    	switch(featureID){
    		case FractalPackage.INTERFACE__SIGNATURE:
    			switch(type){
					case Notification.SET:
						interface_.getHelper().update(FractalPackage.INTERFACE__SIGNATURE_AST, null);
					break;
    			}
    		break;
    		case FractalPackage.INTERFACE__NAME:
    			switch(type){
					case Notification.SET:
						interface_.getHelper().update(FractalPackage.INTERFACE__NAME_AST, null);
					break;
    			}
    		break;
    	}
    }
    
    private static class SingletonHolder {
		private static InterfaceAdapter instance = new InterfaceAdapter();
	}
    
    public static InterfaceAdapter getInstance(){
    	return SingletonHolder.instance;
    }
    
}
