package org.ow2.fractal.f4e.fractal.adapter.helper;

/**
 * 
 * @author Yann Davin
 * @model
 */
public interface IHelperAdapter {
	
	/**
	 * @return
	 * @model default=""
	 */
	public IHelper<?> getHelper();
}
