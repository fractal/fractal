package org.ow2.fractal.f4e.fractal.adapter.helper;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * 
 * @author Yann Davin
 *
 * @param <T>
 * @model
 */
public interface IMerge<T extends EObject> {
	//public T getElementWithSameName(List<T> elements);
	
	/**
	 * @return the attribute used to discriminate the element. By default the 'name' attribute
	 * @model default=""
	 */
	public int[] getAttributeIDs();
	
	/**
	 * @return unique features that have an associated merged feature.
	 * @model default=""
	 */
	public EStructuralFeature[] getUniqueFeatures();
	
	/**
	 * @param srcNodes contains nodes inherited, the HashMap<String,String> associated to each inherited nodes 
	 * contains the effective parameters used.
	 * @param override
	 * @model default=""
	 */
	public void merge(List<Node<? extends EObject>> srcNodes, boolean override, IProgressMonitor monitor);
}
