lexer grammar ExtendsADL;
@header {
  package org.ow2.fractal.f4e.fractal.parser;
}

T9 : ',' ;
T10 : '(' ;
T11 : ')' ;
T12 : '=>' ;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3" 241
REFERENCE 
	: '${' ( options {greedy=false;} : 'a'..'z' | 'A'..'Z' | '0'..'9' | '-' | '_' )+ '}'
	;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3" 245
VALUE 
	: '\'' ( options {greedy=false;} : . )* '\''
	;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3" 249
NAME 
	: ('a'..'z' | 'A'..'Z' | '0'..'9' | '-' | '_' | '.' )+
	;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3" 253
NEWLINE : '\r'? '\n' ;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3" 255
WS : (' ' | '\t' | '\n' | '\r')+ {skip();} ;