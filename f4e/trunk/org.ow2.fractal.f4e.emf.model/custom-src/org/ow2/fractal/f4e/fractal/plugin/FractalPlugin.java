package org.ow2.fractal.f4e.fractal.plugin;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.emf.ecore.EPackage;
import org.osgi.framework.BundleContext;
import org.ow2.fractal.f4e.extension.FractalADLExtension;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.ocl.adapter.ValidatorAdapter;
import org.ow2.fractal.f4e.fractal.ocl.util.OCLConstraintLoader;

/**
 * The activator class controls the plug-in life cycle
 */
public class FractalPlugin extends Plugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.ow2.fractal.f4e.fractal.plugin";

	// The shared instance
	private static FractalPlugin plugin;
	
	/** The Constant OCL_FILE. */
	static final String OCL_FILE = "/org/ow2/fractal/f4e/fractal/ocl/constraints.ocl";	
	
	/**
	 * The constructor
	 */
	public FractalPlugin() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		
		
		
		EPackage.Registry.INSTANCE.put(FractalPackage.eINSTANCE.getNsURI(),
                 FractalPackage.eINSTANCE);
 
		 // register the validator adapter.
		 ValidatorAdapter validator = ValidatorAdapter.getInstance();
		 ValidatorAdapter.Registry.INSTANCE.put(
				FractalPackage.eINSTANCE,
				validator);

		 try{
			 // load the ocl constraints;
			
			 IPath path =  new Path(FileLocator.toFileURL(getBundle().getResource(OCL_FILE)).getPath());
			 OCLConstraintLoader oclConstraintLoader = new OCLConstraintLoader();
			 oclConstraintLoader.load(new Path(OCL_FILE));
		 }catch(Exception e){
			 System.err.println(e.getMessage());
		 }
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static FractalPlugin getDefault() {
		return plugin;
	}
}
