lexer grammar InterfaceADL;
@header {
  package org.ow2.fractal.f4e.fractal.parser;
  
}

T10 : '.' ;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3" 99
REFERENCE 
	: '${' ( options {greedy=false;} : ~('='|'\''|'\"') )+ '}' {setText(getText().substring(2, getText().length()-1));} 
	;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3" 103
STRING 
	: '\'' (ESC | ~('\\' | '\''))* '\''
	;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3" 107
protected
ESC 
	: '\\' .
	;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3" 112
RESTRICTED_STRING : (~('\.'))+ ;


// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3" 115
NEWLINE : '\r'? '\n' ;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/InterfaceADL.g3" 117
WS : (' ' | '\t' | '\n' | '\r')+ {skip();} ;