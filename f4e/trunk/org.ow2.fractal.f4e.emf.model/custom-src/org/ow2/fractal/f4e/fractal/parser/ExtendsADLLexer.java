// $ANTLR 3.0.1 custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3 2009-06-11 16:09:16

  package org.ow2.fractal.f4e.fractal.parser;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class ExtendsADLLexer extends Lexer {
    public static final int NAME=4;
    public static final int WS=8;
    public static final int NEWLINE=7;
    public static final int REFERENCE=5;
    public static final int T10=10;
    public static final int T11=11;
    public static final int T12=12;
    public static final int T9=9;
    public static final int VALUE=6;
    public static final int Tokens=13;
    public static final int EOF=-1;
    public ExtendsADLLexer() {;} 
    public ExtendsADLLexer(CharStream input) {
        super(input);
    }
    public String getGrammarFileName() { return "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3"; }

    // $ANTLR start T9
    public final void mT9() throws RecognitionException {
        try {
            int _type = T9;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:6:4: ( ',' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:6:6: ','
            {
            match(','); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T9

    // $ANTLR start T10
    public final void mT10() throws RecognitionException {
        try {
            int _type = T10;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:7:5: ( '(' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:7:7: '('
            {
            match('('); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T10

    // $ANTLR start T11
    public final void mT11() throws RecognitionException {
        try {
            int _type = T11;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:8:5: ( ')' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:8:7: ')'
            {
            match(')'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T11

    // $ANTLR start T12
    public final void mT12() throws RecognitionException {
        try {
            int _type = T12;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:9:5: ( '=>' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:9:7: '=>'
            {
            match("=>"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T12

    // $ANTLR start REFERENCE
    public final void mREFERENCE() throws RecognitionException {
        try {
            int _type = REFERENCE;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:242:2: ( '${' ( options {greedy=false; } : 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '-' | '_' )+ '}' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:242:4: '${' ( options {greedy=false; } : 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '-' | '_' )+ '}'
            {
            match("${"); 

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:242:9: ( options {greedy=false; } : 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '-' | '_' )+
            int cnt1=0;
            loop1:
            do {
                int alt1=6;
                switch ( input.LA(1) ) {
                case '}':
                    {
                    alt1=6;
                    }
                    break;
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                case 'g':
                case 'h':
                case 'i':
                case 'j':
                case 'k':
                case 'l':
                case 'm':
                case 'n':
                case 'o':
                case 'p':
                case 'q':
                case 'r':
                case 's':
                case 't':
                case 'u':
                case 'v':
                case 'w':
                case 'x':
                case 'y':
                case 'z':
                    {
                    alt1=1;
                    }
                    break;
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                case 'I':
                case 'J':
                case 'K':
                case 'L':
                case 'M':
                case 'N':
                case 'O':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                case 'V':
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                    {
                    alt1=2;
                    }
                    break;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    {
                    alt1=3;
                    }
                    break;
                case '-':
                    {
                    alt1=4;
                    }
                    break;
                case '_':
                    {
                    alt1=5;
                    }
                    break;

                }

                switch (alt1) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:242:37: 'a' .. 'z'
            	    {
            	    matchRange('a','z'); 

            	    }
            	    break;
            	case 2 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:242:48: 'A' .. 'Z'
            	    {
            	    matchRange('A','Z'); 

            	    }
            	    break;
            	case 3 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:242:59: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;
            	case 4 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:242:70: '-'
            	    {
            	    match('-'); 

            	    }
            	    break;
            	case 5 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:242:76: '_'
            	    {
            	    match('_'); 

            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);

            match('}'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end REFERENCE

    // $ANTLR start VALUE
    public final void mVALUE() throws RecognitionException {
        try {
            int _type = VALUE;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:246:2: ( '\\'' ( options {greedy=false; } : . )* '\\'' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:246:4: '\\'' ( options {greedy=false; } : . )* '\\''
            {
            match('\''); 
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:246:9: ( options {greedy=false; } : . )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0=='\'') ) {
                    alt2=2;
                }
                else if ( ((LA2_0>='\u0000' && LA2_0<='&')||(LA2_0>='(' && LA2_0<='\uFFFE')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:246:37: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            match('\''); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end VALUE

    // $ANTLR start NAME
    public final void mNAME() throws RecognitionException {
        try {
            int _type = NAME;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:250:2: ( ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '-' | '_' | '.' )+ )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:250:4: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '-' | '_' | '.' )+
            {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:250:4: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '-' | '_' | '.' )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>='-' && LA3_0<='.')||(LA3_0>='0' && LA3_0<='9')||(LA3_0>='A' && LA3_0<='Z')||LA3_0=='_'||(LA3_0>='a' && LA3_0<='z')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:
            	    {
            	    if ( (input.LA(1)>='-' && input.LA(1)<='.')||(input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end NAME

    // $ANTLR start NEWLINE
    public final void mNEWLINE() throws RecognitionException {
        try {
            int _type = NEWLINE;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:253:9: ( ( '\\r' )? '\\n' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:253:11: ( '\\r' )? '\\n'
            {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:253:11: ( '\\r' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='\r') ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:253:11: '\\r'
                    {
                    match('\r'); 

                    }
                    break;

            }

            match('\n'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end NEWLINE

    // $ANTLR start WS
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:255:4: ( ( ' ' | '\\t' | '\\n' | '\\r' )+ )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:255:6: ( ' ' | '\\t' | '\\n' | '\\r' )+
            {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:255:6: ( ' ' | '\\t' | '\\n' | '\\r' )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>='\t' && LA5_0<='\n')||LA5_0=='\r'||LA5_0==' ') ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);

            skip();

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end WS

    public void mTokens() throws RecognitionException {
        // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:1:8: ( T9 | T10 | T11 | T12 | REFERENCE | VALUE | NAME | NEWLINE | WS )
        int alt6=9;
        switch ( input.LA(1) ) {
        case ',':
            {
            alt6=1;
            }
            break;
        case '(':
            {
            alt6=2;
            }
            break;
        case ')':
            {
            alt6=3;
            }
            break;
        case '=':
            {
            alt6=4;
            }
            break;
        case '$':
            {
            alt6=5;
            }
            break;
        case '\'':
            {
            alt6=6;
            }
            break;
        case '-':
        case '.':
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        case 'A':
        case 'B':
        case 'C':
        case 'D':
        case 'E':
        case 'F':
        case 'G':
        case 'H':
        case 'I':
        case 'J':
        case 'K':
        case 'L':
        case 'M':
        case 'N':
        case 'O':
        case 'P':
        case 'Q':
        case 'R':
        case 'S':
        case 'T':
        case 'U':
        case 'V':
        case 'W':
        case 'X':
        case 'Y':
        case 'Z':
        case '_':
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'e':
        case 'f':
        case 'g':
        case 'h':
        case 'i':
        case 'j':
        case 'k':
        case 'l':
        case 'm':
        case 'n':
        case 'o':
        case 'p':
        case 'q':
        case 'r':
        case 's':
        case 't':
        case 'u':
        case 'v':
        case 'w':
        case 'x':
        case 'y':
        case 'z':
            {
            alt6=7;
            }
            break;
        case '\r':
            {
            int LA6_8 = input.LA(2);

            if ( (LA6_8=='\n') ) {
                int LA6_9 = input.LA(3);

                if ( ((LA6_9>='\t' && LA6_9<='\n')||LA6_9=='\r'||LA6_9==' ') ) {
                    alt6=9;
                }
                else {
                    alt6=8;}
            }
            else {
                alt6=9;}
            }
            break;
        case '\n':
            {
            int LA6_9 = input.LA(2);

            if ( ((LA6_9>='\t' && LA6_9<='\n')||LA6_9=='\r'||LA6_9==' ') ) {
                alt6=9;
            }
            else {
                alt6=8;}
            }
            break;
        case '\t':
        case ' ':
            {
            alt6=9;
            }
            break;
        default:
            NoViableAltException nvae =
                new NoViableAltException("1:1: Tokens : ( T9 | T10 | T11 | T12 | REFERENCE | VALUE | NAME | NEWLINE | WS );", 6, 0, input);

            throw nvae;
        }

        switch (alt6) {
            case 1 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:1:10: T9
                {
                mT9(); 

                }
                break;
            case 2 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:1:13: T10
                {
                mT10(); 

                }
                break;
            case 3 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:1:17: T11
                {
                mT11(); 

                }
                break;
            case 4 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:1:21: T12
                {
                mT12(); 

                }
                break;
            case 5 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:1:25: REFERENCE
                {
                mREFERENCE(); 

                }
                break;
            case 6 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:1:35: VALUE
                {
                mVALUE(); 

                }
                break;
            case 7 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:1:41: NAME
                {
                mNAME(); 

                }
                break;
            case 8 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:1:46: NEWLINE
                {
                mNEWLINE(); 

                }
                break;
            case 9 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ExtendsADL.g3:1:54: WS
                {
                mWS(); 

                }
                break;

        }

    }


 

}