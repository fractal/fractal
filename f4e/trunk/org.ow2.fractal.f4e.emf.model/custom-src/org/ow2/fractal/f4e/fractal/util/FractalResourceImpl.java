/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */

package org.ow2.fractal.f4e.fractal.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.operations.IOperationHistoryListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;
import org.eclipse.emf.ecore.xmi.DOMHelper;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.XMLHelper;
import org.eclipse.emf.ecore.xmi.XMLLoad;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.XMLSave;
import org.eclipse.emf.ecore.xmi.impl.XMLHelperImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.FractalFactory;
import org.ow2.fractal.f4e.fractal.System;
import org.ow2.fractal.f4e.fractal.adapter.factory.OperationHistoryListenerAdapterFactory;
import org.ow2.fractal.f4e.fractal.util.visitor.EventAdapterLoader;
import org.ow2.fractal.f4e.fractal.util.visitor.LoadADLFileJob;
import org.ow2.fractal.f4e.fractal.util.visitor.ModelUpdater;
import org.ow2.fractal.f4e.fractal.util.visitor.ModelVisitor;


// TODO: Auto-generated Javadoc
/**
 * The Class FractalAdlExtendedResourceImpl.
 */

public class FractalResourceImpl extends XMLResourceImpl implements IFractalXMLResource, IFractalResource
 {

	boolean shouldLoadExtends = true;
	
	/** The definition save impl. */
	DefinitionSaveImpl definitionSaveImpl = new DefinitionSaveImpl();
	
	FractalDOMHandlerImpl fractalDOMHandler= new FractalDOMHandlerImpl();
  
	
	/**
    * Instantiates a new fractal adl extended resource impl.
    */
   public FractalResourceImpl()
   {
     super();
     if(this.publicId == null || this.systemId ==null){
    	 setDoctypeInfo(FRACTAL_DOCTYPE_PUBLIC_ID, FRACTAL_DOCTYPE_SYSTEM_ID);
     }
   }
   
   /**
    * Instantiates a new fractal adl extended resource impl.
    * 
    * @param uri the uri
    */
   public FractalResourceImpl(URI uri)
   {
     super(uri);
     setTrackingModification(true);
     if(this.publicId == null || this.systemId ==null){
    	 setDoctypeInfo(FRACTAL_DOCTYPE_PUBLIC_ID, FRACTAL_DOCTYPE_SYSTEM_ID);
     }
   }

   /* (non-Javadoc)
    * @see org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl#createXMLHelper()
    */
   protected XMLHelper createXMLHelper()
   {
     return new XMLHelperImpl(this);
   }

   /* (non-Javadoc)
    * @see org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl#createXMLLoad()
    */
   protected XMLLoad createXMLLoad()
   {
	  return new FractalLoadImpl(new FractalHelperImpl(this));
   }
   
   /* (non-Javadoc)
    * @see org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl#createXMLSave()
    */
   protected XMLSave createXMLSave()
   {
     return new FractalSaveImpl(new FractalHelperImpl(this){

		// Fractal ADL doesn't use prefixes,
    	// so we don't serialize them
		public List<String> getPrefixes(EPackage package1) {
			
			return new ArrayList<String>();
		}

		public String getNamespaceURI(String prefix) {
			return "";
		}

		protected String getPrefix(EPackage package1, boolean mustHavePrefix) {
			return "";
		}
    	 
     });
   }

	/* (non-Javadoc)
	 * @see org.eclipse.emf.ecore.resource.impl.ResourceImpl#getURIConverter()
	 */
	
	protected URIConverter getURIConverter() {
		// TODO Auto-generated method stub
		return new FractalExtensibleURIConverterImpl();
	}
	
	/**
	 * The Class FractalAdlExtendedURIConverter.
	 */
	public class FractalAdlExtendedURIConverter extends ExtensibleURIConverterImpl {
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.ecore.resource.impl.ResourceImpl#basicSetResourceSet(org.eclipse.emf.ecore.resource.ResourceSet, org.eclipse.emf.common.notify.NotificationChain)
	 */
	
	public NotificationChain basicSetResourceSet(ResourceSet resourceSet,
			NotificationChain notifications) {
		// TODO Auto-generated method stub
		return super.basicSetResourceSet(resourceSet, notifications);
	}
   
	
	private void simpleUpdateModel(){
		Definition definition = getRootDefinition();

		if (definition != null) {
			ModelVisitor<ModelUpdater> updater = new ModelVisitor<ModelUpdater>(
					ModelUpdater.class);
			updater.doSwitch(definition);

			// Once the model is updated we add event listener adapter
			// on model
			// elements.
			// these adapters update the model when ADL strings change.
			ModelVisitor<EventAdapterLoader> adapterLoader = new ModelVisitor<EventAdapterLoader>(
					EventAdapterLoader.class);
			adapterLoader.doSwitch(definition);
		}
	}
	// Visit all the Fractal ADL strings and create related objects model
	// for example if a Definition has its extends field extendsADL="blabla"
	// the updateModel method will parse this string and create a DefinitionCall
	// element with a reference to the blabla Definition if it exists
	public void updateModel() {
		boolean state = isModified();
		
		if(getResourceSet() != null &&
				getResourceSet().getLoadOptions().get(IFractalXMLResource.TRANSACTIONAL_SUPPORT_OPTION) != null &&
				(Boolean)getResourceSet().getLoadOptions().get(IFractalXMLResource.TRANSACTIONAL_SUPPORT_OPTION) == false){
			simpleUpdateModel();
		}else{
		
		TransactionalEditingDomain domain = FractalTransactionalEditingDomain
				.getEditingDomain();

		domain.getCommandStack().execute(new RecordingCommand(domain,"Update Model") {
			protected void doExecute() {
				simpleUpdateModel();
			}
		});
		}
		setModified(state);
	}
	
	public Definition getRootDefinition(){
		if(this.getContents() != null && 
			this.getContents().size()>0 && this.getContents().get(0) != null &&
			this.getContents().get(0) instanceof System && 
			((System)this.getContents().get(0)).getDefinitions().size()>0 &&
			((System)this.getContents().get(0)).getDefinitions().get(0) != null){
			return (Definition)((System)this.getContents().get(0)).getDefinitions().get(0);
		}else{
			return null;
		}
	}
	
	// List to store the resources (fractal ADL files) dependent on this Resource
	protected List<Resource> dependencies = new ArrayList<Resource>();
	public void addDependency(Resource resource){
		if(!dependencies.contains(resource))
		dependencies.add(resource);
	}
	
	public void removeDependency(Resource resource){
		if(dependencies.contains(resource)){
			dependencies.remove(resource);
		}
	}
	
	public List<Resource> getDependencies(){
		return dependencies;
	}

	private void addRootElement(){
		if (getContents().get(0) instanceof Definition) {
			System system = FractalFactory.eINSTANCE
			.createSystem();
			Definition definition = (Definition) getContents().get(0);
			getContents().add(0, system);
			system.getDefinitions().add(definition);
		}
	}
	
	public void load(Map<?, ?> options) throws IOException{
		if(!isLoaded()){
			super.load(options);
		}
	}
	
	 public void doLoad(InputStream inputStream, Map<?, ?> options) throws IOException{
		 if(getResourceSet() != null &&
			(Boolean)options.get(IFractalXMLResource.FRACTAL_SHOULD_LOAD_EXTENDS) == false){
				this.shouldLoadExtends = false;
			}else{
				this.shouldLoadExtends = true;
			}
		 
		//if(!isLoaded()){
			super.doLoad(inputStream,options);
		
			if(getResourceSet() != null &&
					(Boolean)options.get(IFractalXMLResource.TRANSACTIONAL_SUPPORT_OPTION) == false){
					addRootElement();
			}else{
			TransactionalEditingDomain domain = FractalTransactionalEditingDomain
						.getEditingDomain();

				domain.getCommandStack().execute(new RecordingCommand(domain) {
					protected void doExecute() {
						addRootElement();
					}
				});
			}
			updateModel();
		//}
		setModified(false);
	}

	public void save(Map<?, ?> options) throws IOException {
		super.save(options);
	}

	public void reload() throws  IOException {
		List<Resource> associatedResources = getDependencies();
		this.unload();
		this.load(this.getDefaultLoadOptions());
		this.updateModel();
		
		// We also need to refresh all resources (fractal files) that
		// have a dependency relation (extends or definition) with
		// the reloaded resource.
		Iterator<Resource> iterator = associatedResources.iterator();
		while(iterator.hasNext()){
			Resource r = iterator.next();
			if(r instanceof IFractalResource){
				((IFractalResource) r).updateModel();
			}
		}
		
		setModified(false);
	}
	

	protected void doUnload() {
		// TODO Auto-generated method stub
		super.doUnload();
	}
	
	public void delete(Map<?,?> options) throws IOException{
		List<Resource> associatedResources = getDependencies();
		
		try{
			// In order to delete file we first get its absolute path
			IPath absolutePath = Path.fromOSString(ResourcesPlugin.getWorkspace().getRoot().getLocation().toOSString() + Path.fromOSString(getURI().toPlatformString(true)));
			getURIConverter().delete(URI.createFileURI(absolutePath.toOSString()), mergeMaps(options, defaultDeleteOptions));
		}catch(Exception exception){
			exception.printStackTrace();
		}
		
		unload();
	    ResourceSet resourceSet = getResourceSet();
	    if (resourceSet != null)
	    {
	      resourceSet.getResources().remove(this);
	    }
		
		Iterator<Resource> iterator = associatedResources.iterator();
		
		while(iterator.hasNext()){
			Resource r = iterator.next();
			if(r instanceof IFractalResource){
				((IFractalResource) r).updateModel();
			}
		}
	}

	public Object getAdapter(Class adapter) {
		// TODO Auto-generated method stub
		Object object = null;
		if(adapter == IOperationHistoryListener.class){
			object = OperationHistoryListenerAdapterFactory.getInstance().adapt(this);
		}
		return object;
	}
	
	public boolean shouldLoadExtends(){
		return  shouldLoadExtends;
	}
	
	public DOMHelper getDOMHelper() {
		// TODO Auto-generated method stub
		return super.getDOMHelper();
	}
	
	public Map<Object, Object> getDefaultLoadOptions() {
		 if (defaultLoadOptions == null)
		    {
		      defaultLoadOptions = new HashMap<Object, Object>();
		      defaultLoadOptions.put(IFractalXMLResource.FRACTAL_SHOULD_LOAD_EXTENDS,true);
		      defaultLoadOptions.put(IFractalXMLResource.TRANSACTIONAL_SUPPORT_OPTION, true);
			    
			   	defaultLoadOptions.put(XMLResource.OPTION_RECORD_UNKNOWN_FEATURE, true);
			   
			    defaultLoadOptions.put(XMLResource.OPTION_EXTENDED_META_DATA, FractalResourceFactoryImpl.getExtendedMetaData());
			   
			    defaultLoadOptions.put(XMLResource.OPTION_USE_LEXICAL_HANDLER, Boolean.TRUE);
			   
			    HashMap parserFeatures = new HashMap(2);
			    
			    //TODO improve dtd support
			    parserFeatures.put("http://xml.org/sax/features/external-parameter-entities", Boolean.FALSE);
			    parserFeatures.put("http://apache.org/xml/features/nonvalidating/load-external-dtd", Boolean.FALSE);
			    parserFeatures.put("http://xml.org/sax/features/validation", Boolean.FALSE);
			    defaultLoadOptions.put(XMLResource.OPTION_PARSER_FEATURES, parserFeatures);
			    
			    // we don't want to serialize the xmi:type information
			    // ex : we don't want  <controller xsi:type="TemplateMembrane"/> but
			    // <template-controller />
			    // To do this we use our own XMLTypeInfo FractalXMLTypeInfo
			    defaultLoadOptions.put(XMLResource.OPTION_SAVE_TYPE_INFORMATION, Boolean.FALSE);
		    }
		 return defaultLoadOptions;
	}
	
	public Map<Object, Object> getDefaultSaveOptions() {
		 if (defaultSaveOptions == null)
		    {
		      defaultSaveOptions = new HashMap<Object, Object>();
		 
		 defaultSaveOptions.put(XMLResource.OPTION_RECORD_UNKNOWN_FEATURE, true);
		    
		   
		 defaultSaveOptions.put(XMLResource.OPTION_EXTENDED_META_DATA, FractalResourceFactoryImpl.getExtendedMetaData());

		 defaultSaveOptions.put(XMLResource.OPTION_LINE_WIDTH, new Integer(80));
		 defaultSaveOptions.put(XMLResource.OPTION_SAVE_DOCTYPE, Boolean.TRUE);
		 defaultSaveOptions.put(XMIResource.OPTION_USE_XMI_TYPE, Boolean.TRUE);
		    
		    defaultSaveOptions.put(XMLResource.OPTION_SAVE_TYPE_INFORMATION, Boolean.FALSE);
		    }
		 return defaultSaveOptions;
	}
	
} //FractalResourceImpl
