/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal.util;

import java.util.HashMap;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceFactoryImpl;
import org.eclipse.emf.ecore.util.ExtendedMetaData;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMLInfoImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLMapImpl;
import org.ow2.fractal.f4e.fractal.FractalPackage;


// TODO: Auto-generated Javadoc
/**
 * The Class FractalAdlExtendedResourceFactoryImpl.
 */
public class FractalResourceFactoryImpl extends ResourceFactoryImpl {
	
	/** The extended meta data. */
	private static ExtendedMetaData extendedMetaData = initializeMetaData();

	public static ExtendedMetaData getExtendedMetaData(){
		return extendedMetaData;
	}
	
	private static ExtendedMetaData initializeMetaData(){
		XMLResource.XMLMap xmlMap = new XMLMapImpl();
		
		xmlMap.add(FractalPackage.eINSTANCE.getSystem(), createXMLInfoNull());
		xmlMap.add(FractalPackage.eINSTANCE.getSystem_Definitions(), createXMLInfo("definition"));
		
		// Definition, Definition_Extends, Definition_VExtends
		xmlMap.add(FractalPackage.eINSTANCE.getDefinition(), createXMLInfo("definition"));
		xmlMap.add(FractalPackage.eINSTANCE.getDefinition_Arguments(), createXMLInfoAttribute("arguments"));
		xmlMap.add(FractalPackage.eINSTANCE.getDefinition_Extends(), createXMLInfoAttribute("extends"));
		xmlMap.add(FractalPackage.eINSTANCE.getDefinition_ArgumentsAST(), createXMLInfoNull());
		
		// Component
		xmlMap.add(FractalPackage.eINSTANCE.getComponent_Definition(), createXMLInfoAttribute("definition"));
		xmlMap.add(FractalPackage.eINSTANCE.getComponent_Shared(),  createXMLInfoNull());
		
		// AbstractComponent
		xmlMap.add(FractalPackage.eINSTANCE.getAbstractComponent_NameAST(), createXMLInfoNull());
		xmlMap.add(FractalPackage.eINSTANCE.getAbstractComponent_Name(), createXMLInfoAttribute("name"));
		xmlMap.add(FractalPackage.eINSTANCE.getAbstractComponent_ExtendsAST(), createXMLInfoNull());
		xmlMap.add(FractalPackage.eINSTANCE.getAbstractComponent_Comments(), createXMLInfo("comment"));
		xmlMap.add(FractalPackage.eINSTANCE.getAbstractComponent_AttributesController(), createXMLInfo("attributes"));
		xmlMap.add(FractalPackage.eINSTANCE.getAbstractComponent_Content(), createXMLInfo("content"));
		xmlMap.add(FractalPackage.eINSTANCE.getAbstractComponent_Controller(), createXMLInfo("controller"));
		xmlMap.add(FractalPackage.eINSTANCE.getAbstractComponent_TemplateController(), createXMLInfo("template-controller"));
		xmlMap.add(FractalPackage.eINSTANCE.getAbstractComponent_Interfaces(), createXMLInfo("interface"));
		xmlMap.add(FractalPackage.eINSTANCE.getAbstractComponent_SubComponents(), createXMLInfo("component"));
		xmlMap.add(FractalPackage.eINSTANCE.getAbstractComponent_Bindings(), createXMLInfo("binding"));
		xmlMap.add(FractalPackage.eINSTANCE.getAbstractComponent_Logger(), createXMLInfo("logger"));
		xmlMap.add(FractalPackage.eINSTANCE.getAbstractComponent_VirtualNode(), createXMLInfo("virtual-node"));
		xmlMap.add(FractalPackage.eINSTANCE.getAbstractComponent_Coordinates(), createXMLInfo("coordinates"));
		
		// Interface
		xmlMap.add(FractalPackage.eINSTANCE.getInterface_Comments(), createXMLInfo("comment"));
		xmlMap.add(FractalPackage.eINSTANCE.getInterface_Cardinality(), createXMLInfoAttribute("cardinality"));
		xmlMap.add(FractalPackage.eINSTANCE.getInterface_Role(), createXMLInfoAttribute("role"));
		xmlMap.add(FractalPackage.eINSTANCE.getInterface_Contingency(), createXMLInfoAttribute("contingency"));
		xmlMap.add(FractalPackage.eINSTANCE.getInterface_Name(), createXMLInfoAttribute("name"));
		xmlMap.add(FractalPackage.eINSTANCE.getInterface_Signature(), createXMLInfoAttribute("signature"));
		xmlMap.add(FractalPackage.eINSTANCE.getInterface_NameAST(), createXMLInfoNull());
		xmlMap.add(FractalPackage.eINSTANCE.getInterface_SignatureAST(), createXMLInfoNull());
		
		// Binding
		xmlMap.add(FractalPackage.eINSTANCE.getBinding_Comments(), createXMLInfo("comment"));
		xmlMap.add(FractalPackage.eINSTANCE.getBinding_Client(), createXMLInfoAttribute("client"));
		xmlMap.add(FractalPackage.eINSTANCE.getBinding_Server(), createXMLInfoAttribute("server"));
		xmlMap.add(FractalPackage.eINSTANCE.getBinding_ServerAST(), createXMLInfoNull());
		xmlMap.add(FractalPackage.eINSTANCE.getBinding_ClientAST(), createXMLInfoNull());
		xmlMap.add(FractalPackage.eINSTANCE.getBinding_ServerInterface(), createXMLInfoNull());
		xmlMap.add(FractalPackage.eINSTANCE.getBinding_ClientInterface(), createXMLInfoNull());
		
		
		// Content
		xmlMap.add(FractalPackage.eINSTANCE.getContent(), createXMLInfo("content"));
		xmlMap.add(FractalPackage.eINSTANCE.getContent_Comments(), createXMLInfo("comment"));
		xmlMap.add(FractalPackage.eINSTANCE.getContent_Class(), createXMLInfoAttribute("class"));
		xmlMap.add(FractalPackage.eINSTANCE.getContent_ClassAST(), createXMLInfoNull());
		
		// Membrane
		xmlMap.add(FractalPackage.eINSTANCE.getController(), createXMLInfo("controller"));
		xmlMap.add(FractalPackage.eINSTANCE.getController_Comments(), createXMLInfo("comment"));
		xmlMap.add(FractalPackage.eINSTANCE.getController_TypeAST(), createXMLInfoNull());
		xmlMap.add(FractalPackage.eINSTANCE.getController_Type(), createXMLInfoAttribute("desc"));
		
		// Template - Membrane
		xmlMap.add(FractalPackage.eINSTANCE.getTemplateController(), createXMLInfo("template-membrane"));
		xmlMap.add(FractalPackage.eINSTANCE.getTemplateController_Comments(), createXMLInfo("comment"));
		xmlMap.add(FractalPackage.eINSTANCE.getTemplateController_TypeAST(), createXMLInfoNull());
		xmlMap.add(FractalPackage.eINSTANCE.getTemplateController_Type(), createXMLInfoAttribute("desc"));
		
		// AttributeController
		xmlMap.add(FractalPackage.eINSTANCE.getAttributesController_Comments(), createXMLInfo("comment"));
		xmlMap.add(FractalPackage.eINSTANCE.getAttributesController_Attributes(), createXMLInfo("attribute"));
		xmlMap.add(FractalPackage.eINSTANCE.getAttributesController_Signature(),  createXMLInfoAttribute("signature"));
		xmlMap.add(FractalPackage.eINSTANCE.getAttributesController_SignatureAST(),  createXMLInfoNull());
		
		// Attribute
		xmlMap.add(FractalPackage.eINSTANCE.getAttribute_Comments(), createXMLInfo("comment"));
		xmlMap.add(FractalPackage.eINSTANCE.getAttribute_Name(),  createXMLInfoAttribute("name"));
		xmlMap.add(FractalPackage.eINSTANCE.getAttribute_Value(),  createXMLInfoAttribute("value"));
		xmlMap.add(FractalPackage.eINSTANCE.getAttribute_NameAST(),  createXMLInfoNull());
		xmlMap.add(FractalPackage.eINSTANCE.getAttribute_ValueAST(),  createXMLInfoNull());
		
		// Interface
		xmlMap.add(FractalPackage.eINSTANCE.getInterface_Comments(), createXMLInfo("comment"));
		xmlMap.add(FractalPackage.eINSTANCE.getInterface(),createXMLInfo("interface"));
		xmlMap.add(FractalPackage.eINSTANCE.getInterface_Name(), createXMLInfo("name"));
		xmlMap.add(FractalPackage.eINSTANCE.getInterface_Signature(), createXMLInfo("signature"));
		xmlMap.add(FractalPackage.eINSTANCE.getInterface_NameAST(), createXMLInfoNull());
		xmlMap.add(FractalPackage.eINSTANCE.getInterface_SignatureAST(), createXMLInfoNull());
		
		// Comment
		xmlMap.add(FractalPackage.eINSTANCE.getComment(), createXMLInfo("comment"));
		xmlMap.add(FractalPackage.eINSTANCE.getComment_Language(), createXMLInfo("language"));
		xmlMap.add(FractalPackage.eINSTANCE.getComment_LanguageAST(), createXMLInfoNull());
		xmlMap.add(FractalPackage.eINSTANCE.getComment_Text(), createXMLInfo("text"));
		xmlMap.add(FractalPackage.eINSTANCE.getComment_TextAST(), createXMLInfoNull());
		
		// Coordinates
		xmlMap.add(FractalPackage.eINSTANCE.getCoordinates(), createXMLInfo("coordinate"));
		xmlMap.add(FractalPackage.eINSTANCE.getCoordinates_ColorAST(), createXMLInfoNull());
		xmlMap.add(FractalPackage.eINSTANCE.getCoordinates_NameAST(), createXMLInfoNull());
		xmlMap.add(FractalPackage.eINSTANCE.getCoordinates_X0AST(), createXMLInfoNull());
		xmlMap.add(FractalPackage.eINSTANCE.getCoordinates_X1AST(), createXMLInfoNull());
		xmlMap.add(FractalPackage.eINSTANCE.getCoordinates_Y0AST(), createXMLInfoNull());
		xmlMap.add(FractalPackage.eINSTANCE.getCoordinates_Y1AST(), createXMLInfoNull());
		xmlMap.add(FractalPackage.eINSTANCE.getCoordinates_Coordinates(), createXMLInfo("coordinate"));
		xmlMap.add(FractalPackage.eINSTANCE.getCoordinates_Color(), createXMLInfo("color"));
		xmlMap.add(FractalPackage.eINSTANCE.getCoordinates_Name(), createXMLInfo("name"));
		xmlMap.add(FractalPackage.eINSTANCE.getCoordinates_X0(), createXMLInfo("x0"));
		xmlMap.add(FractalPackage.eINSTANCE.getCoordinates_X1(), createXMLInfo("x1"));
		xmlMap.add(FractalPackage.eINSTANCE.getCoordinates_Y0(), createXMLInfo("y0"));
		xmlMap.add(FractalPackage.eINSTANCE.getCoordinates_Y1(), createXMLInfo("y1"));
		
		return new FractalMetaData(xmlMap);
	}
	
	/**
	 * Instantiates a new fractal adl extended resource factory impl.
	 */
	public FractalResourceFactoryImpl() {
		super();
	}

	
	/* (non-Javadoc)
	 * @see org.eclipse.emf.ecore.resource.impl.ResourceFactoryImpl#createResource(org.eclipse.emf.common.util.URI)
	 */
	
	public Resource createResource(URI uri) {
		 
		FractalResourceImpl result = new FractalResourceImpl(uri);
		
		 if(uri.lastSegment().endsWith(".fractal")){
		   result.setEncoding("UTF-8");
		 
		    result.getDefaultLoadOptions().put(IFractalXMLResource.FRACTAL_SHOULD_LOAD_EXTENDS,true);
		    result.getDefaultLoadOptions().put(IFractalXMLResource.TRANSACTIONAL_SUPPORT_OPTION, true);
		    
		   	result.getDefaultLoadOptions().put(XMLResource.OPTION_RECORD_UNKNOWN_FEATURE, true);
		    result.getDefaultSaveOptions().put(XMLResource.OPTION_RECORD_UNKNOWN_FEATURE, true);
		    
		    result.getDefaultLoadOptions().put(XMLResource.OPTION_EXTENDED_META_DATA, extendedMetaData);
		    result.getDefaultSaveOptions().put(XMLResource.OPTION_EXTENDED_META_DATA, extendedMetaData);

		    result.getDefaultSaveOptions().put(XMLResource.OPTION_LINE_WIDTH, new Integer(80));
		    result.getDefaultSaveOptions().put(XMLResource.OPTION_SAVE_DOCTYPE, Boolean.TRUE);
		    result.getDefaultSaveOptions().put(XMIResource.OPTION_USE_XMI_TYPE, Boolean.TRUE);
		   
		    result.getDefaultLoadOptions().put(XMLResource.OPTION_USE_LEXICAL_HANDLER, Boolean.TRUE);
		   
		    HashMap parserFeatures = new HashMap(2);
		    
		    //TODO improve dtd support
		    parserFeatures.put("http://xml.org/sax/features/external-parameter-entities", Boolean.FALSE);
		    parserFeatures.put("http://apache.org/xml/features/nonvalidating/load-external-dtd", Boolean.FALSE);
		    parserFeatures.put("http://xml.org/sax/features/validation", Boolean.FALSE);
		    result.getDefaultLoadOptions().put(XMLResource.OPTION_PARSER_FEATURES, parserFeatures);
		    
		    // we don't want to serialize the xmi:type information
		    // ex : we don't want  <controller xsi:type="TemplateMembrane"/> but
		    // <template-controller />
		    // To do this we use our own XMLTypeInfo FractalXMLTypeInfo
		    result.getDefaultLoadOptions().put(XMLResource.OPTION_SAVE_TYPE_INFORMATION, Boolean.FALSE);
		    result.getDefaultSaveOptions().put(XMLResource.OPTION_SAVE_TYPE_INFORMATION, Boolean.FALSE);
		    
		    //  result.getDefaultLoadOptions().put(XMLResource.OPTION_USE_PARSER_POOL, new MyXMLParserPoolImpl());
		 }
		    return result;
		
	}
	
	/**
	 * Creates the xml info.
	 * 
	 * @param name the name
	 * 
	 * @return the xML resource. xml info
	 */
	static protected XMLResource.XMLInfo createXMLInfo(String name) {
		XMLResource.XMLInfo info = new XMLInfoImpl();
		info.setName(name);
		
		return info;
	}

	/**
	 * Creates the xml info attribute.
	 * 
	 * @param name the name
	 * 
	 * @return the xML resource. xml info
	 */
	static protected XMLResource.XMLInfo createXMLInfoAttribute(String name) {
		XMLResource.XMLInfo info = new XMLInfoImpl();
		info.setName(name);
		info.setXMLRepresentation(1);
		return info;
	}
	
	/**
	 * Creates the xml info null.
	 * 
	 * @return the xML resource. xml info
	 */
	static protected XMLResource.XMLInfo createXMLInfoNull() {
		XMLResource.XMLInfo info = new XMLInfoImpl();
		//info.setTargetNamespace("");
		info.setName("");
		info.setXMLRepresentation(XMLResource.XMLInfo.ELEMENT);
		return info;
	}
	
	/**
	 * Creates the xml info.
	 * 
	 * @return the xML resource. xml info
	 */
	static protected XMLResource.XMLInfo createXMLInfo() {
		XMLResource.XMLInfo info = new XMLInfoImpl();
		info.setXMLRepresentation(XMLResource.XMLInfo.ELEMENT); // We will use
																// an
																// xmi:Extension
																// element for
																// these
		return info;
	}
	
} // FractalResourceFactoryImpl
