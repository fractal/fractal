// $ANTLR 3.0.1 custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3 2009-06-11 16:09:15

  package org.ow2.fractal.f4e.fractal.parser;
  
import java.util.Stack;

import org.ow2.fractal.f4e.fractal.Constant;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.FormalParameter;
import org.ow2.fractal.f4e.fractal.FractalFactory;
import org.ow2.fractal.f4e.fractal.Value;
  


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;


import org.antlr.runtime.tree.*;

public class ArgumentsADLParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "ARGUMENT_STRING", "VALUE", "NEWLINE", "WS", "','", "'='"
    };
    public static final int WS=7;
    public static final int NEWLINE=6;
    public static final int VALUE=5;
    public static final int EOF=-1;
    public static final int ARGUMENT_STRING=4;

        public ArgumentsADLParser(TokenStream input) {
            super(input);
        }
        
    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    public TreeAdaptor getTreeAdaptor() {
        return adaptor;
    }

    public String[] getTokenNames() { return tokenNames; }
    public String getGrammarFileName() { return "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3"; }


    	Stack<Object> stack = new Stack<Object>();
    	
    	protected Definition definition = null;
    	
    	public ArgumentsADLParser(TokenStream input, Definition definition){
    		super(input);
    		this.definition = definition;
    	}
    	
    	private void popArguments(){
    			FormalParameter formalParameter = popArgument();
    			if(formalParameter != null){
    				popArguments();
    				definition.getArgumentsAST().add(formalParameter);
    			}
    	}
    	
    	private void pushArgument(){
    		FormalParameter parameter = FractalFactory.eINSTANCE.createFormalParameter();
    		stack.push(parameter);	
    	}	
    	
    	private FormalParameter popArgument(){
    		if(!stack.empty() && (stack.lastElement() instanceof FormalParameter)){
    			FormalParameter formalParameter = (FormalParameter)stack.pop();
    			Value value = popValue();
    			Value name = popValue();
    			if(name == null){
    				name = value;
    				value = null;
    			}
    			formalParameter.setName(name);
    			formalParameter.setValue(value);
    			return formalParameter;
    		}
    		return null;
    	}
    	
    	private void pushValue(String value){
    		Value valueValue = FractalFactory.eINSTANCE.createValue();
    		Constant constantValue = FractalFactory.eINSTANCE.createConstant();
    		constantValue.setConstant(value);
    		valueValue.getElements().add(constantValue);
    		valueValue.setContextDefinition(this.definition);
    		stack.push(valueValue);	
    	}
    	
    	private Value popValue(){
    		if(!stack.empty() && (stack.lastElement() instanceof Value)){
    			return	(Value)stack.pop();
    		}
    		return null;
    	}
    	


    public static class prog_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start prog
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:82:1: prog : ( argumentsADL )? EOF ;
    public final prog_return prog() throws RecognitionException {
        prog_return retval = new prog_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token EOF2=null;
        argumentsADL_return argumentsADL1 = null;


        CommonTree EOF2_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:83:5: ( ( argumentsADL )? EOF )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:83:7: ( argumentsADL )? EOF
            {
            root_0 = (CommonTree)adaptor.nil();

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:83:7: ( argumentsADL )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==ARGUMENT_STRING) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:83:9: argumentsADL
                    {
                    pushFollow(FOLLOW_argumentsADL_in_prog60);
                    argumentsADL1=argumentsADL();
                    _fsp--;

                    adaptor.addChild(root_0, argumentsADL1.getTree());
                    popArguments();

                    }
                    break;

            }

            EOF2=(Token)input.LT(1);
            match(input,EOF,FOLLOW_EOF_in_prog66); 
            EOF2_tree = (CommonTree)adaptor.create(EOF2);
            adaptor.addChild(root_0, EOF2_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end prog

    public static class argumentsADL_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start argumentsADL
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:85:1: argumentsADL : argumentADL ( ',' argumentADL )* ;
    public final argumentsADL_return argumentsADL() throws RecognitionException {
        argumentsADL_return retval = new argumentsADL_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token char_literal4=null;
        argumentADL_return argumentADL3 = null;

        argumentADL_return argumentADL5 = null;


        CommonTree char_literal4_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:86:2: ( argumentADL ( ',' argumentADL )* )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:86:4: argumentADL ( ',' argumentADL )*
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_argumentADL_in_argumentsADL75);
            argumentADL3=argumentADL();
            _fsp--;

            adaptor.addChild(root_0, argumentADL3.getTree());
            pushArgument();
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:86:34: ( ',' argumentADL )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==8) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:86:36: ',' argumentADL
            	    {
            	    char_literal4=(Token)input.LT(1);
            	    match(input,8,FOLLOW_8_in_argumentsADL81); 
            	    char_literal4_tree = (CommonTree)adaptor.create(char_literal4);
            	    adaptor.addChild(root_0, char_literal4_tree);

            	    pushFollow(FOLLOW_argumentADL_in_argumentsADL83);
            	    argumentADL5=argumentADL();
            	    _fsp--;

            	    adaptor.addChild(root_0, argumentADL5.getTree());
            	    pushArgument();

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end argumentsADL

    public static class argumentADL_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start argumentADL
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:89:1: argumentADL : argumentName ( '=' valueADL )? ;
    public final argumentADL_return argumentADL() throws RecognitionException {
        argumentADL_return retval = new argumentADL_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token char_literal7=null;
        argumentName_return argumentName6 = null;

        valueADL_return valueADL8 = null;


        CommonTree char_literal7_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:90:2: ( argumentName ( '=' valueADL )? )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:90:4: argumentName ( '=' valueADL )?
            {
            root_0 = (CommonTree)adaptor.nil();

            pushFollow(FOLLOW_argumentName_in_argumentADL99);
            argumentName6=argumentName();
            _fsp--;

            adaptor.addChild(root_0, argumentName6.getTree());
            pushValue(input.toString(argumentName6.start,argumentName6.stop));
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:90:50: ( '=' valueADL )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==9) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:90:52: '=' valueADL
                    {
                    char_literal7=(Token)input.LT(1);
                    match(input,9,FOLLOW_9_in_argumentADL105); 
                    char_literal7_tree = (CommonTree)adaptor.create(char_literal7);
                    adaptor.addChild(root_0, char_literal7_tree);

                    pushFollow(FOLLOW_valueADL_in_argumentADL107);
                    valueADL8=valueADL();
                    _fsp--;

                    adaptor.addChild(root_0, valueADL8.getTree());
                    pushValue(input.toString(valueADL8.start,valueADL8.stop));

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end argumentADL

    public static class argumentName_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start argumentName
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:93:1: argumentName : ARGUMENT_STRING ;
    public final argumentName_return argumentName() throws RecognitionException {
        argumentName_return retval = new argumentName_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token ARGUMENT_STRING9=null;

        CommonTree ARGUMENT_STRING9_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:94:2: ( ARGUMENT_STRING )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:94:4: ARGUMENT_STRING
            {
            root_0 = (CommonTree)adaptor.nil();

            ARGUMENT_STRING9=(Token)input.LT(1);
            match(input,ARGUMENT_STRING,FOLLOW_ARGUMENT_STRING_in_argumentName122); 
            ARGUMENT_STRING9_tree = (CommonTree)adaptor.create(ARGUMENT_STRING9);
            adaptor.addChild(root_0, ARGUMENT_STRING9_tree);


            }

            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end argumentName

    public static class valueADL_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start valueADL
    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:97:1: valueADL : ( VALUE | ( . )+ );
    public final valueADL_return valueADL() throws RecognitionException {
        valueADL_return retval = new valueADL_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token VALUE10=null;
        Token wildcard11=null;

        CommonTree VALUE10_tree=null;
        CommonTree wildcard11_tree=null;

        try {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:98:2: ( VALUE | ( . )+ )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==VALUE) ) {
                alt5=1;
            }
            else if ( (LA5_0==ARGUMENT_STRING||(LA5_0>=NEWLINE && LA5_0<=9)) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("97:1: valueADL : ( VALUE | ( . )+ );", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:98:4: VALUE
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    VALUE10=(Token)input.LT(1);
                    match(input,VALUE,FOLLOW_VALUE_in_valueADL133); 
                    VALUE10_tree = (CommonTree)adaptor.create(VALUE10);
                    adaptor.addChild(root_0, VALUE10_tree);


                    }
                    break;
                case 2 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:98:12: ( . )+
                    {
                    root_0 = (CommonTree)adaptor.nil();

                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:98:12: ( . )+
                    int cnt4=0;
                    loop4:
                    do {
                        int alt4=2;
                        switch ( input.LA(1) ) {
                        case 8:
                            {
                            alt4=2;
                            }
                            break;
                        case EOF:
                            {
                            alt4=2;
                            }
                            break;
                        case ARGUMENT_STRING:
                        case VALUE:
                        case NEWLINE:
                        case WS:
                        case 9:
                            {
                            alt4=1;
                            }
                            break;

                        }

                        switch (alt4) {
                    	case 1 :
                    	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3:98:13: .
                    	    {
                    	    wildcard11=(Token)input.LT(1);
                    	    matchAny(input); 
                    	    wildcard11_tree = (CommonTree)adaptor.create(wildcard11);
                    	    adaptor.addChild(root_0, wildcard11_tree);


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt4 >= 1 ) break loop4;
                                EarlyExitException eee =
                                    new EarlyExitException(4, input);
                                throw eee;
                        }
                        cnt4++;
                    } while (true);


                    }
                    break;

            }
            retval.stop = input.LT(-1);

                retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end valueADL


 

    public static final BitSet FOLLOW_argumentsADL_in_prog60 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_prog66 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_argumentADL_in_argumentsADL75 = new BitSet(new long[]{0x0000000000000102L});
    public static final BitSet FOLLOW_8_in_argumentsADL81 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_argumentADL_in_argumentsADL83 = new BitSet(new long[]{0x0000000000000102L});
    public static final BitSet FOLLOW_argumentName_in_argumentADL99 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_9_in_argumentADL105 = new BitSet(new long[]{0x00000000000003F0L});
    public static final BitSet FOLLOW_valueADL_in_argumentADL107 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ARGUMENT_STRING_in_argumentName122 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_VALUE_in_valueADL133 = new BitSet(new long[]{0x0000000000000002L});

}