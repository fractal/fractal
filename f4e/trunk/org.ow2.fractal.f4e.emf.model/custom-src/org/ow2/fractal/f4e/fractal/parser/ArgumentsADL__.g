lexer grammar ArgumentsADL;
@header {
  package org.ow2.fractal.f4e.fractal.parser;
  
 
}

T8 : ',' ;
T9 : '=' ;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3" 101
VALUE 
	: '\'' ( options {greedy=false;} : . )* '\''
	;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3" 105
ARGUMENT_STRING : (~('='|'\''|'\"'))+ ;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3" 107
NEWLINE : '\r'? '\n' ;

// $ANTLR src "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ArgumentsADL.g3" 109
WS : (' ' | '\t' | '\n' | '\r')+ {skip();} ;