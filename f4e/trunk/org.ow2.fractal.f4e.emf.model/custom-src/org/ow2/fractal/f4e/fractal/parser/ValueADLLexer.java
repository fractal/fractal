// $ANTLR 3.0.1 custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3 2009-06-11 16:09:17

  package org.ow2.fractal.f4e.fractal.parser;
  


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class ValueADLLexer extends Lexer {
    public static final int WS=9;
    public static final int NEWLINE=8;
    public static final int ESC=7;
    public static final int RESTRICTED_STRING=5;
    public static final int REFERENCE=6;
    public static final int Tokens=10;
    public static final int EOF=-1;
    public static final int STRING=4;
    public ValueADLLexer() {;} 
    public ValueADLLexer(CharStream input) {
        super(input);
    }
    public String getGrammarFileName() { return "custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3"; }

    // $ANTLR start REFERENCE
    public final void mREFERENCE() throws RecognitionException {
        try {
            int _type = REFERENCE;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:102:2: ( '${' ( options {greedy=false; } : 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '-' | '_' )+ '}' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:102:4: '${' ( options {greedy=false; } : 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '-' | '_' )+ '}'
            {
            match("${"); 

            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:102:9: ( options {greedy=false; } : 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '-' | '_' )+
            int cnt1=0;
            loop1:
            do {
                int alt1=6;
                switch ( input.LA(1) ) {
                case '}':
                    {
                    alt1=6;
                    }
                    break;
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                case 'g':
                case 'h':
                case 'i':
                case 'j':
                case 'k':
                case 'l':
                case 'm':
                case 'n':
                case 'o':
                case 'p':
                case 'q':
                case 'r':
                case 's':
                case 't':
                case 'u':
                case 'v':
                case 'w':
                case 'x':
                case 'y':
                case 'z':
                    {
                    alt1=1;
                    }
                    break;
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                case 'I':
                case 'J':
                case 'K':
                case 'L':
                case 'M':
                case 'N':
                case 'O':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                case 'V':
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                    {
                    alt1=2;
                    }
                    break;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    {
                    alt1=3;
                    }
                    break;
                case '-':
                    {
                    alt1=4;
                    }
                    break;
                case '_':
                    {
                    alt1=5;
                    }
                    break;

                }

                switch (alt1) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:102:37: 'a' .. 'z'
            	    {
            	    matchRange('a','z'); 

            	    }
            	    break;
            	case 2 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:102:48: 'A' .. 'Z'
            	    {
            	    matchRange('A','Z'); 

            	    }
            	    break;
            	case 3 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:102:59: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;
            	case 4 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:102:70: '-'
            	    {
            	    match('-'); 

            	    }
            	    break;
            	case 5 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:102:76: '_'
            	    {
            	    match('_'); 

            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);

            match('}'); 
            setText(getText().substring(2, getText().length()-1));

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end REFERENCE

    // $ANTLR start STRING
    public final void mSTRING() throws RecognitionException {
        try {
            int _type = STRING;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:106:2: ( '\\'' ( ESC | ~ ( '\\\\' | '\\'' ) )* '\\'' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:106:4: '\\'' ( ESC | ~ ( '\\\\' | '\\'' ) )* '\\''
            {
            match('\''); 
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:106:9: ( ESC | ~ ( '\\\\' | '\\'' ) )*
            loop2:
            do {
                int alt2=3;
                int LA2_0 = input.LA(1);

                if ( (LA2_0=='\\') ) {
                    alt2=1;
                }
                else if ( ((LA2_0>='\u0000' && LA2_0<='&')||(LA2_0>='(' && LA2_0<='[')||(LA2_0>=']' && LA2_0<='\uFFFE')) ) {
                    alt2=2;
                }


                switch (alt2) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:106:10: ESC
            	    {
            	    mESC(); 

            	    }
            	    break;
            	case 2 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:106:16: ~ ( '\\\\' | '\\'' )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFE') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            match('\''); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end STRING

    // $ANTLR start ESC
    public final void mESC() throws RecognitionException {
        try {
            int _type = ESC;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:111:2: ( '\\\\' . )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:111:4: '\\\\' .
            {
            match('\\'); 
            matchAny(); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end ESC

    // $ANTLR start RESTRICTED_STRING
    public final void mRESTRICTED_STRING() throws RecognitionException {
        try {
            int _type = RESTRICTED_STRING;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:114:19: ( (~ ( '$' | '{' | '}' ) )+ )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:114:21: (~ ( '$' | '{' | '}' ) )+
            {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:114:21: (~ ( '$' | '{' | '}' ) )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>='\u0000' && LA3_0<='#')||(LA3_0>='%' && LA3_0<='z')||LA3_0=='|'||(LA3_0>='~' && LA3_0<='\uFFFE')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:114:23: ~ ( '$' | '{' | '}' )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='#')||(input.LA(1)>='%' && input.LA(1)<='z')||input.LA(1)=='|'||(input.LA(1)>='~' && input.LA(1)<='\uFFFE') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end RESTRICTED_STRING

    // $ANTLR start NEWLINE
    public final void mNEWLINE() throws RecognitionException {
        try {
            int _type = NEWLINE;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:117:9: ( ( '\\r' )? '\\n' )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:117:11: ( '\\r' )? '\\n'
            {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:117:11: ( '\\r' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='\r') ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:117:11: '\\r'
                    {
                    match('\r'); 

                    }
                    break;

            }

            match('\n'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end NEWLINE

    // $ANTLR start WS
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:119:4: ( ( ' ' | '\\t' | '\\n' | '\\r' )+ )
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:119:6: ( ' ' | '\\t' | '\\n' | '\\r' )+
            {
            // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:119:6: ( ' ' | '\\t' | '\\n' | '\\r' )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>='\t' && LA5_0<='\n')||LA5_0=='\r'||LA5_0==' ') ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);

            skip();

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end WS

    public void mTokens() throws RecognitionException {
        // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:1:8: ( REFERENCE | STRING | ESC | RESTRICTED_STRING | NEWLINE | WS )
        int alt6=6;
        alt6 = dfa6.predict(input);
        switch (alt6) {
            case 1 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:1:10: REFERENCE
                {
                mREFERENCE(); 

                }
                break;
            case 2 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:1:20: STRING
                {
                mSTRING(); 

                }
                break;
            case 3 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:1:27: ESC
                {
                mESC(); 

                }
                break;
            case 4 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:1:31: RESTRICTED_STRING
                {
                mRESTRICTED_STRING(); 

                }
                break;
            case 5 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:1:49: NEWLINE
                {
                mNEWLINE(); 

                }
                break;
            case 6 :
                // custom-src/org/ow2/fractal/f4e/fractal/parser/g3/ValueADL.g3:1:57: WS
                {
                mWS(); 

                }
                break;

        }

    }


    protected DFA6 dfa6 = new DFA6(this);
    static final String DFA6_eotS =
        "\2\uffff\5\7\1\uffff\2\7\1\13\1\uffff\1\15\1\uffff\1\7";
    static final String DFA6_eofS =
        "\17\uffff";
    static final String DFA6_minS =
        "\1\0\1\uffff\2\0\3\11\1\uffff\3\0\1\uffff\1\0\1\uffff\1\0";
    static final String DFA6_maxS =
        "\1\ufffe\1\uffff\2\ufffe\3\40\1\uffff\3\ufffe\1\uffff\1\ufffe\1"+
        "\uffff\1\ufffe";
    static final String DFA6_acceptS =
        "\1\uffff\1\1\5\uffff\1\4\3\uffff\1\2\1\uffff\1\3\1\uffff";
    static final String DFA6_specialS =
        "\17\uffff}>";
    static final String[] DFA6_transitionS = {
            "\11\7\1\6\1\5\2\7\1\4\22\7\1\6\3\7\1\1\2\7\1\2\64\7\1\3\36\7"+
            "\1\uffff\1\7\1\uffff\uff81\7",
            "",
            "\44\11\1\13\2\11\1\12\64\11\1\10\36\11\1\13\1\11\1\13\uff81"+
            "\11",
            "\44\14\1\15\126\14\1\15\1\14\1\15\uff81\14",
            "\1\6\1\5\2\uffff\1\6\22\uffff\1\6",
            "\2\6\2\uffff\1\6\22\uffff\1\6",
            "\2\6\2\uffff\1\6\22\uffff\1\6",
            "",
            "\44\16\1\13\126\16\1\13\1\16\1\13\uff81\16",
            "\44\11\1\13\2\11\1\12\64\11\1\10\36\11\1\13\1\11\1\13\uff81"+
            "\11",
            "\44\7\1\uffff\126\7\1\uffff\1\7\1\uffff\uff81\7",
            "",
            "\44\7\1\uffff\126\7\1\uffff\1\7\1\uffff\uff81\7",
            "",
            "\44\11\1\13\2\11\1\12\64\11\1\10\36\11\1\13\1\11\1\13\uff81"+
            "\11"
    };

    static final short[] DFA6_eot = DFA.unpackEncodedString(DFA6_eotS);
    static final short[] DFA6_eof = DFA.unpackEncodedString(DFA6_eofS);
    static final char[] DFA6_min = DFA.unpackEncodedStringToUnsignedChars(DFA6_minS);
    static final char[] DFA6_max = DFA.unpackEncodedStringToUnsignedChars(DFA6_maxS);
    static final short[] DFA6_accept = DFA.unpackEncodedString(DFA6_acceptS);
    static final short[] DFA6_special = DFA.unpackEncodedString(DFA6_specialS);
    static final short[][] DFA6_transition;

    static {
        int numStates = DFA6_transitionS.length;
        DFA6_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA6_transition[i] = DFA.unpackEncodedString(DFA6_transitionS[i]);
        }
    }

    class DFA6 extends DFA {

        public DFA6(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 6;
            this.eot = DFA6_eot;
            this.eof = DFA6_eof;
            this.min = DFA6_min;
            this.max = DFA6_max;
            this.accept = DFA6_accept;
            this.special = DFA6_special;
            this.transition = DFA6_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( REFERENCE | STRING | ESC | RESTRICTED_STRING | NEWLINE | WS );";
        }
    }
 

}