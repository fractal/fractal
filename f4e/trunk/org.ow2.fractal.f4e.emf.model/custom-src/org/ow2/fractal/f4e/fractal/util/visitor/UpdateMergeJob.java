package org.ow2.fractal.f4e.fractal.util.visitor;

import java.util.Calendar;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.jface.action.Action;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.progress.IProgressConstants;
import org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl;
import org.ow2.fractal.f4e.fractal.plugin.FractalPlugin;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;

public class UpdateMergeJob extends Job{
	protected EObject eObject;
	private Action action;

	public UpdateMergeJob(EObject eObject){
		super("Merge");
		this.eObject = eObject;
		this.setProperty(IProgressConstants.ACTION_PROPERTY, new MergeAction());
		this.setProperty(IProgressConstants.KEEP_PROPERTY, true); 
		this.setProperty(IProgressConstants.NO_IMMEDIATE_ERROR_PROMPT_PROPERTY, true);
	}
	
	 protected IStatus run( final IProgressMonitor monitor) {
		String name = "Update Merge";
		final SubMonitor progress = SubMonitor.convert(monitor);
		
		String componentName = "";
		if(eObject instanceof AbstractComponentImpl){
			AbstractComponentImpl t = ((AbstractComponentImpl)eObject);
			componentName = t.getMergedName() != null?t.getMergedName():t.getName();
		}
		
		name += " : " + componentName;
		
		monitor.beginTask(name, IProgressMonitor.UNKNOWN);
		
		Calendar start = Calendar.getInstance();
		
		try{
			FractalTransactionalEditingDomain.getEditingDomain().getCommandStack().execute(new RecordingCommand(FractalTransactionalEditingDomain.getEditingDomain()) {
				protected void doExecute() {
					UpdateMergeVisitor visitor = new UpdateMergeVisitor(progress);
					visitor.visit(eObject);
				}
			});
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			monitor.done();
		}
		//mergeAction.setOk(true);
		Calendar end = Calendar.getInstance();
		
		
		//complete();
		return  new Status(IStatus.OK, FractalPlugin.PLUGIN_ID, "Merge " + componentName + " finished : " + (end.getTimeInMillis() - start.getTimeInMillis()) + "ms");
	 }
	 
	 protected void complete() {
	 }

	 
	 /**
	   * Asynchronous execution of an Action
	   * @param action
	   */
	  protected static void showResults(final Action action) {
	    Display.getDefault().asyncExec(new Runnable() {
	      public void run() {
	        action.run();
	      }
	    });
	  }

}
