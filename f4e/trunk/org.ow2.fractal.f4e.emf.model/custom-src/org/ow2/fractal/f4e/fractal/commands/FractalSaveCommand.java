package org.ow2.fractal.f4e.fractal.commands;

import java.util.Map;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.util.IExceptionHandler;

public class FractalSaveCommand extends RecordingCommand implements IFractalSaveCommand{

	private Resource resource;
	private Map<Object, Object> saveOptions;
	private IExceptionHandler handler;
	
	public FractalSaveCommand(Resource resource, final Map<Object, Object> saveOptions, IExceptionHandler handler){
		super(FractalTransactionalEditingDomain.getEditingDomain(),"Save Fractal Resource");
		this.resource = resource;
		this.saveOptions = saveOptions;
		this.handler = handler;
	}
	
	private FractalSaveCommand(TransactionalEditingDomain domain){
		super(domain);
	}
	
	private FractalSaveCommand(TransactionalEditingDomain domain, String label) {
		super(domain,label);
	}
	
	public void save(){
		FractalTransactionalEditingDomain.getEditingDomain().getCommandStack().execute(this);
	}
	
	protected void doExecute() {
		try {
			resource.save(saveOptions);
		}
		catch (Exception exception) {
			if(handler != null){
				handler.handleException(exception);
			}else{
				exception.printStackTrace();
			}
		}
	}
	
}
