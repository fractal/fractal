/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.RunnableWithResult;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.impl.AbstractTransactionalCommandStack;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.swt.internal.Library;
import org.eclipse.swt.widgets.Display;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.FormalParameter;
import org.ow2.fractal.f4e.fractal.FractalFactory;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IDefinitionHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelper;
import org.ow2.fractal.f4e.fractal.Interface;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.DefinitionImpl#getExtends <em>Extends</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.DefinitionImpl#getArguments <em>Arguments</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.DefinitionImpl#getArgumentsAST <em>Arguments AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.DefinitionImpl#getSystem <em>System</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DefinitionImpl extends AbstractComponentImpl implements Definition {
	/**
	 * The default value of the '{@link #getExtends() <em>Extends</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtends()
	 * @generated
	 * @ordered
	 */
	protected static final String EXTENDS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExtends() <em>Extends</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtends()
	 * @generated
	 * @ordered
	 */
	protected String extends_ = EXTENDS_EDEFAULT;

	/**
	 * The default value of the '{@link #getArguments() <em>Arguments</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArguments()
	 * @generated
	 * @ordered
	 */
	protected static final String ARGUMENTS_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getArguments() <em>Arguments</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArguments()
	 * @generated
	 * @ordered
	 */
	protected String arguments = ARGUMENTS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getArgumentsAST() <em>Arguments AST</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArgumentsAST()
	 * @generated
	 * @ordered
	 */
	protected EList<FormalParameter> argumentsAST;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	protected EClass eStaticClass() {
		return FractalPackage.Literals.DEFINITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExtends() {
		return extends_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExtends(String newExtends) {
		String oldExtends = extends_;
		extends_ = newExtends;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.DEFINITION__EXTENDS, oldExtends, extends_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getArguments() {
		return arguments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setArguments(String newArguments) {
		String oldArguments = arguments;
		arguments = newArguments;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.DEFINITION__ARGUMENTS, oldArguments, arguments));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalParameter> getArgumentsAST() {
		if (argumentsAST == null) {
			argumentsAST = new EObjectContainmentWithInverseEList<FormalParameter>(FormalParameter.class, this, FractalPackage.DEFINITION__ARGUMENTS_AST, FractalPackage.FORMAL_PARAMETER__DEFINITION);
		}
		return argumentsAST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.ow2.fractal.f4e.fractal.System getSystem() {
		if (eContainerFeatureID != FractalPackage.DEFINITION__SYSTEM) return null;
		return (org.ow2.fractal.f4e.fractal.System)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSystem(org.ow2.fractal.f4e.fractal.System newSystem, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newSystem, FractalPackage.DEFINITION__SYSTEM, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSystem(org.ow2.fractal.f4e.fractal.System newSystem) {
		if (newSystem != eInternalContainer() || (eContainerFeatureID != FractalPackage.DEFINITION__SYSTEM && newSystem != null)) {
			if (EcoreUtil.isAncestor(this, newSystem))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSystem != null)
				msgs = ((InternalEObject)newSystem).eInverseAdd(this, FractalPackage.SYSTEM__DEFINITIONS, org.ow2.fractal.f4e.fractal.System.class, msgs);
			msgs = basicSetSystem(newSystem, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.DEFINITION__SYSTEM, newSystem, newSystem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.DEFINITION__ARGUMENTS_AST:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getArgumentsAST()).basicAdd(otherEnd, msgs);
			case FractalPackage.DEFINITION__SYSTEM:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetSystem((org.ow2.fractal.f4e.fractal.System)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.DEFINITION__ARGUMENTS_AST:
				return ((InternalEList<?>)getArgumentsAST()).basicRemove(otherEnd, msgs);
			case FractalPackage.DEFINITION__SYSTEM:
				return basicSetSystem(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case FractalPackage.DEFINITION__SYSTEM:
				return eInternalContainer().eInverseRemove(this, FractalPackage.SYSTEM__DEFINITIONS, org.ow2.fractal.f4e.fractal.System.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FractalPackage.DEFINITION__EXTENDS:
				return getExtends();
			case FractalPackage.DEFINITION__ARGUMENTS:
				return getArguments();
			case FractalPackage.DEFINITION__ARGUMENTS_AST:
				return getArgumentsAST();
			case FractalPackage.DEFINITION__SYSTEM:
				return getSystem();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FractalPackage.DEFINITION__EXTENDS:
				setExtends((String)newValue);
				return;
			case FractalPackage.DEFINITION__ARGUMENTS:
				setArguments((String)newValue);
				return;
			case FractalPackage.DEFINITION__ARGUMENTS_AST:
				getArgumentsAST().clear();
				getArgumentsAST().addAll((Collection<? extends FormalParameter>)newValue);
				return;
			case FractalPackage.DEFINITION__SYSTEM:
				setSystem((org.ow2.fractal.f4e.fractal.System)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public void eUnset(int featureID) {
		switch (featureID) {
			case FractalPackage.DEFINITION__EXTENDS:
				setExtends(EXTENDS_EDEFAULT);
				return;
			case FractalPackage.DEFINITION__ARGUMENTS:
				setArguments(ARGUMENTS_EDEFAULT);
				return;
			case FractalPackage.DEFINITION__ARGUMENTS_AST:
				getArgumentsAST().clear();
				return;
			case FractalPackage.DEFINITION__SYSTEM:
				setSystem((org.ow2.fractal.f4e.fractal.System)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FractalPackage.DEFINITION__EXTENDS:
				return EXTENDS_EDEFAULT == null ? extends_ != null : !EXTENDS_EDEFAULT.equals(extends_);
			case FractalPackage.DEFINITION__ARGUMENTS:
				return ARGUMENTS_EDEFAULT == null ? arguments != null : !ARGUMENTS_EDEFAULT.equals(arguments);
			case FractalPackage.DEFINITION__ARGUMENTS_AST:
				return argumentsAST != null && !argumentsAST.isEmpty();
			case FractalPackage.DEFINITION__SYSTEM:
				return getSystem() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (extends: ");
		result.append(extends_);
		result.append(", arguments: ");
		result.append(arguments);
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	 
		
  	
	public Object getAdapter(Class adapter) {
		Object object = null;
		
		
		if(adapter == IHelper.class ||
			adapter == IDefinitionHelper.class ){
			object = HelperAdapterFactory.getInstance().adapt(this);
		}
		
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public IDefinitionHelper getHelper() {
		return (IDefinitionHelper)getAdapter(IDefinitionHelper.class);
	}

	/**
	 * 
	 * @param listInterfaces
	 * @param interface_
	 * @return
	 * @generated NOT
	 */
	private Interface getInterfaceWithSameName(List<Interface> listInterfaces,Interface interface_ ){
		Interface result = null;
		if(listInterfaces == null){
			return result;
		}
		
		Iterator<Interface> iterator = listInterfaces.iterator();
		
		while(iterator.hasNext()){
			Interface nextInterface = iterator.next();
			if(nextInterface.getName() != null && nextInterface.getName().equals(interface_.getName())){
				result = nextInterface;
				break;
			}
		}
		
		return result;
	}

	/**
	 * 
	 * @param definitionCall
	 * @param interface_
	 * @return
	 * @generated NOT
	 */
	//TODO Resolve potential arguments in 'name' and 'signature' fields.
	private Interface createMergedInterface(DefinitionCall definitionCall, Interface interface_){
		Interface result = FractalFactory.eINSTANCE.createInterface();
		result.setName(interface_.getName());
		if(interface_.eIsSet(FractalPackage.eINSTANCE.getInterface_Role())){
			result.setRole(interface_.getRole());
		}
		if(interface_.eIsSet(FractalPackage.eINSTANCE.getInterface_Cardinality())){
			result.setCardinality(interface_.getCardinality());
		}
		if(interface_.eIsSet(FractalPackage.eINSTANCE.getInterface_Contingency())){
			result.setContingency(interface_.getContingency());
		}
		if(interface_.eIsSet(FractalPackage.eINSTANCE.getInterface_Signature())){
			result.setSignature(interface_.getSignature());
		}
		return result;
	}
	
	/**
	 * 
	 * @param interface_
	 * @param interfaceToMerge
	 * @generated NOT
	 */
	private void mergedInterface(Interface interface_, Interface interfaceToMerge){
		if(!interface_.eIsSet(FractalPackage.eINSTANCE.getInterface_Role())){
			interface_.setRole(interfaceToMerge.getRole());
		}
		if(!interface_.eIsSet(FractalPackage.eINSTANCE.getInterface_Cardinality())){
			interface_.setCardinality(interfaceToMerge.getCardinality());
		}
		if(!interface_.eIsSet(FractalPackage.eINSTANCE.getInterface_Contingency())){
			interface_.setContingency(interfaceToMerge.getContingency());
		}
		if(!interface_.eIsSet(FractalPackage.eINSTANCE.getInterface_Signature())){
			interface_.setSignature(interfaceToMerge.getSignature());
		}
	}
	
	/**
	 * @generated NOT
	 */
	private List<Interface> createMergedInterfaces(){
		List<Interface> mergedInterfaces = new ArrayList<Interface>();
		EList<DefinitionCall> definitionCalls = this.getExtendsAST();
		
		for(int i=definitionCalls.size()-1;i>=0;i--){
			DefinitionCall definitionCall = definitionCalls.get(i);
			if(definitionCall.getDefinition() != null){
				Iterator<Interface> iteratorInterfaces = definitionCall.getDefinition().getInterfaces().iterator();
				// For each interface of a parent definition
				// we create a new merged interface if:
				// -c1 : the interface has not already be inherited by a other
				// parent definition
				// -c2 : the parent interface is not extended by the definition.
				//
				// if we are in the case c1 or c2 instead of created a new merged interface
				// we override the unset fields.
				//
				while(iteratorInterfaces.hasNext()){
					Interface interface_ = iteratorInterfaces.next();
					Interface extendedInterface = getInterfaceWithSameName(this.getInterfaces(),interface_);
					Interface parentExtendedInterface = getInterfaceWithSameName(mergedInterfaces,interface_);
					if(extendedInterface == null &&
					   parentExtendedInterface == null){
						mergedInterfaces.add(createMergedInterface(definitionCall, interface_));
					}else if(extendedInterface != null){
						//mergedInterface(extendedInterface, interface_);
					}else if(parentExtendedInterface != null){
						mergedInterface(parentExtendedInterface, interface_);
					}
				}
				
				//Now we have to do the same thing we parent merged interfaces
				List<Interface> parentMergedInterfaces = definitionCall.getDefinition().getMergedInterfaces();
		
				for(int j=0;j<parentMergedInterfaces.size();j++){
					Interface interface_ = parentMergedInterfaces.get(j);
					Interface extendedInterface = getInterfaceWithSameName(this.getInterfaces(),interface_);
					Interface parentExtendedInterface = getInterfaceWithSameName(mergedInterfaces,interface_);
					if(extendedInterface == null &&
					   parentExtendedInterface == null){
						mergedInterfaces.add(createMergedInterface(definitionCall, interface_));
					}else if(extendedInterface != null){
						mergedInterface(extendedInterface, interface_);
					}else if(parentExtendedInterface != null){
						mergedInterface(parentExtendedInterface, interface_);
					}
				}
			}
		}
		return mergedInterfaces;
	}
	
	/**
	 * Yann hand modifs.
	 * @author yann
	 *
	 * @param <E>
	 * @generated NOT
	 */
	private class MergedInterfaceEList<E> extends EObjectContainmentWithInverseEList<E> {
		public MergedInterfaceEList(Collection collection){
			super(Interface.class,DefinitionImpl.this,FractalPackage.ABSTRACT_COMPONENT__MERGED_INTERFACES,FractalPackage.INTERFACE__ABSTRACT_COMPONENT);
			size = collection.size();

			// Conditionally create the data.
			//
			if (size > 0)
			{ 
				// Allow for a bit-shift of growth.
				//
				data = newData(size + size / 8 + 1); 
				collection.toArray(data);
			}
		}
	}
} //DefinitionImpl
