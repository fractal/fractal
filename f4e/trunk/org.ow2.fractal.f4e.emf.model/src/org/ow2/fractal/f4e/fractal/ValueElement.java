/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.IValueElementHelper;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getValueElement()
 * @model
 * @generated
 */
public interface ValueElement extends EObject, IAdaptable, IHelperAdapter {

	IValueElementHelper getHelper();
} // ValueElement
