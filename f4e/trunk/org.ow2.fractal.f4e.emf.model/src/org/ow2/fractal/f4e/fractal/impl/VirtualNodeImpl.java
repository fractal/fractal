/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.VirtualNode;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IVirtualNodeHelper;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Virtual Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.VirtualNodeImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.VirtualNodeImpl#getNameAST <em>Name AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.VirtualNodeImpl#getMergedName <em>Merged Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.VirtualNodeImpl#getAny <em>Any</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.VirtualNodeImpl#getAnyAttributes <em>Any Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VirtualNodeImpl extends EObjectImpl implements VirtualNode {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getNameAST() <em>Name AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameAST()
	 * @generated
	 * @ordered
	 */
	protected Value nameAST;

	/**
	 * The default value of the '{@link #getMergedName() <em>Merged Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedName()
	 * @generated
	 * @ordered
	 */
	protected static final String MERGED_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMergedName() <em>Merged Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedName()
	 * @generated
	 * @ordered
	 */
	protected String mergedName = MERGED_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAny() <em>Any</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAny()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap any;

	/**
	 * The cached value of the '{@link #getAnyAttributes() <em>Any Attributes</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnyAttributes()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap anyAttributes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VirtualNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	protected EClass eStaticClass() {
		return FractalPackage.Literals.VIRTUAL_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.VIRTUAL_NODE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getNameAST() {
		return nameAST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNameAST(Value newNameAST, NotificationChain msgs) {
		Value oldNameAST = nameAST;
		nameAST = newNameAST;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.VIRTUAL_NODE__NAME_AST, oldNameAST, newNameAST);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNameAST(Value newNameAST) {
		if (newNameAST != nameAST) {
			NotificationChain msgs = null;
			if (nameAST != null)
				msgs = ((InternalEObject)nameAST).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.VIRTUAL_NODE__NAME_AST, null, msgs);
			if (newNameAST != null)
				msgs = ((InternalEObject)newNameAST).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.VIRTUAL_NODE__NAME_AST, null, msgs);
			msgs = basicSetNameAST(newNameAST, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.VIRTUAL_NODE__NAME_AST, newNameAST, newNameAST));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMergedName() {
		return mergedName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergedName(String newMergedName) {
		String oldMergedName = mergedName;
		mergedName = newMergedName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.VIRTUAL_NODE__MERGED_NAME, oldMergedName, mergedName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getAny() {
		if (any == null) {
			any = new BasicFeatureMap(this, FractalPackage.VIRTUAL_NODE__ANY);
		}
		return any;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getAnyAttributes() {
		if (anyAttributes == null) {
			anyAttributes = new BasicFeatureMap(this, FractalPackage.VIRTUAL_NODE__ANY_ATTRIBUTES);
		}
		return anyAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.VIRTUAL_NODE__NAME_AST:
				return basicSetNameAST(null, msgs);
			case FractalPackage.VIRTUAL_NODE__ANY:
				return ((InternalEList<?>)getAny()).basicRemove(otherEnd, msgs);
			case FractalPackage.VIRTUAL_NODE__ANY_ATTRIBUTES:
				return ((InternalEList<?>)getAnyAttributes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FractalPackage.VIRTUAL_NODE__NAME:
				return getName();
			case FractalPackage.VIRTUAL_NODE__NAME_AST:
				return getNameAST();
			case FractalPackage.VIRTUAL_NODE__MERGED_NAME:
				return getMergedName();
			case FractalPackage.VIRTUAL_NODE__ANY:
				if (coreType) return getAny();
				return ((FeatureMap.Internal)getAny()).getWrapper();
			case FractalPackage.VIRTUAL_NODE__ANY_ATTRIBUTES:
				if (coreType) return getAnyAttributes();
				return ((FeatureMap.Internal)getAnyAttributes()).getWrapper();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FractalPackage.VIRTUAL_NODE__NAME:
				setName((String)newValue);
				return;
			case FractalPackage.VIRTUAL_NODE__NAME_AST:
				setNameAST((Value)newValue);
				return;
			case FractalPackage.VIRTUAL_NODE__MERGED_NAME:
				setMergedName((String)newValue);
				return;
			case FractalPackage.VIRTUAL_NODE__ANY:
				((FeatureMap.Internal)getAny()).set(newValue);
				return;
			case FractalPackage.VIRTUAL_NODE__ANY_ATTRIBUTES:
				((FeatureMap.Internal)getAnyAttributes()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public void eUnset(int featureID) {
		switch (featureID) {
			case FractalPackage.VIRTUAL_NODE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case FractalPackage.VIRTUAL_NODE__NAME_AST:
				setNameAST((Value)null);
				return;
			case FractalPackage.VIRTUAL_NODE__MERGED_NAME:
				setMergedName(MERGED_NAME_EDEFAULT);
				return;
			case FractalPackage.VIRTUAL_NODE__ANY:
				getAny().clear();
				return;
			case FractalPackage.VIRTUAL_NODE__ANY_ATTRIBUTES:
				getAnyAttributes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FractalPackage.VIRTUAL_NODE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case FractalPackage.VIRTUAL_NODE__NAME_AST:
				return nameAST != null;
			case FractalPackage.VIRTUAL_NODE__MERGED_NAME:
				return MERGED_NAME_EDEFAULT == null ? mergedName != null : !MERGED_NAME_EDEFAULT.equals(mergedName);
			case FractalPackage.VIRTUAL_NODE__ANY:
				return any != null && !any.isEmpty();
			case FractalPackage.VIRTUAL_NODE__ANY_ATTRIBUTES:
				return anyAttributes != null && !anyAttributes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", mergedName: ");
		result.append(mergedName);
		result.append(", any: ");
		result.append(any);
		result.append(", anyAttributes: ");
		result.append(anyAttributes);
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	 
		
  	
	public Object getAdapter(Class adapter) {
		Object object = null;
		
		
		if(adapter == IHelper.class ||
			adapter == IVirtualNodeHelper.class ){
			object = HelperAdapterFactory.getInstance().adapt(this);
		}
		
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */

	
	public IVirtualNodeHelper getHelper() {
		return (IVirtualNodeHelper)getAdapter(IVirtualNodeHelper.class);
	}

} //VirtualNodeImpl
