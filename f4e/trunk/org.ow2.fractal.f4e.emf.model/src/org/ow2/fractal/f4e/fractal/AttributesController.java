/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.ow2.fractal.f4e.fractal.adapter.helper.IAttributesControllerHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attributes Controller</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AttributesController#getSignature <em>Signature</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AttributesController#getAttributes <em>Attributes</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AttributesController#getSignatureAST <em>Signature AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AttributesController#getAbstractComponent <em>Abstract Component</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AttributesController#getComments <em>Comments</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AttributesController#getMergedAttributes <em>Merged Attributes</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AttributesController#getMergedSignature <em>Merged Signature</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AttributesController#getAny <em>Any</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AttributesController#getAnyAttributes <em>Any Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttributesController()
 * @model
 * @generated
 */
public interface AttributesController extends EObject, IAdaptable, IHelperAdapter {
	/**
	 * Returns the value of the '<em><b>Signature</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signature</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signature</em>' attribute.
	 * @see #setSignature(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttributesController_Signature()
	 * @model default=""
	 * @generated
	 */
	String getSignature();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.AttributesController#getSignature <em>Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signature</em>' attribute.
	 * @see #getSignature()
	 * @generated
	 */
	void setSignature(String value);

	/**
	 * Returns the value of the '<em><b>Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.Attribute}.
	 * It is bidirectional and its opposite is '{@link org.ow2.fractal.f4e.fractal.Attribute#getAttributesController <em>Attributes Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attributes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttributesController_Attributes()
	 * @see org.ow2.fractal.f4e.fractal.Attribute#getAttributesController
	 * @model opposite="attributesController" containment="true"
	 * @generated
	 */
	EList<Attribute> getAttributes();

	/**
	 * Returns the value of the '<em><b>Signature AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signature AST</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signature AST</em>' containment reference.
	 * @see #setSignatureAST(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttributesController_SignatureAST()
	 * @model containment="true" required="true" transient="true"
	 * @generated
	 */
	Value getSignatureAST();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.AttributesController#getSignatureAST <em>Signature AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signature AST</em>' containment reference.
	 * @see #getSignatureAST()
	 * @generated
	 */
	void setSignatureAST(Value value);

	/**
	 * Returns the value of the '<em><b>Abstract Component</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getAttributesController <em>Attributes Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract Component</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract Component</em>' container reference.
	 * @see #setAbstractComponent(AbstractComponent)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttributesController_AbstractComponent()
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getAttributesController
	 * @model opposite="attributesController" transient="false"
	 * @generated
	 */
	AbstractComponent getAbstractComponent();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.AttributesController#getAbstractComponent <em>Abstract Component</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract Component</em>' container reference.
	 * @see #getAbstractComponent()
	 * @generated
	 */
	void setAbstractComponent(AbstractComponent value);

	/**
	 * Returns the value of the '<em><b>Comments</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.Comment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comments</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttributesController_Comments()
	 * @model containment="true"
	 * @generated
	 */
	EList<Comment> getComments();

	/**
	 * Returns the value of the '<em><b>Merged Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.Attribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Attributes</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Attributes</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttributesController_MergedAttributes()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	EList<Attribute> getMergedAttributes();

	/**
	 * Returns the value of the '<em><b>Merged Signature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Signature</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Signature</em>' attribute.
	 * @see #setMergedSignature(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttributesController_MergedSignature()
	 * @model transient="true"
	 * @generated
	 */
	String getMergedSignature();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.AttributesController#getMergedSignature <em>Merged Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merged Signature</em>' attribute.
	 * @see #getMergedSignature()
	 * @generated
	 */
	void setMergedSignature(String value);

	/**
	 * Returns the value of the '<em><b>Any</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any</em>' attribute list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttributesController_Any()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' wildcards='##any' name=':6' processing='lax'"
	 * @generated
	 */
	FeatureMap getAny();

	/**
	 * Returns the value of the '<em><b>Any Attributes</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any Attributes</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any Attributes</em>' attribute list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttributesController_AnyAttributes()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='attributeWildcard' wildcards='##any' name=':12' processing='lax'"
	 * @generated
	 */
	FeatureMap getAnyAttributes();

	IAttributesControllerHelper getHelper();

} // AttributesController
