/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal.impl;

import java.util.Collections;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Attribute;
import org.ow2.fractal.f4e.fractal.AttributesController;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.Cardinality;
import org.ow2.fractal.f4e.fractal.Comment;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.Constant;
import org.ow2.fractal.f4e.fractal.Content;
import org.ow2.fractal.f4e.fractal.Contingency;
import org.ow2.fractal.f4e.fractal.Controller;
import org.ow2.fractal.f4e.fractal.Coordinates;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.EffectiveParameter;
import org.ow2.fractal.f4e.fractal.FormalParameter;
import org.ow2.fractal.f4e.fractal.FractalFactory;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.Logger;
import org.ow2.fractal.f4e.fractal.Parameter;
import org.ow2.fractal.f4e.fractal.Reference;
import org.ow2.fractal.f4e.fractal.Role;
import org.ow2.fractal.f4e.fractal.TemplateController;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.ValueElement;
import org.ow2.fractal.f4e.fractal.VirtualNode;
import org.ow2.fractal.f4e.fractal.adapter.factory.EventAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.util.FractalResourceFactoryImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FractalFactoryImpl extends EFactoryImpl implements FractalFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FractalFactory init() {
		try {
			FractalFactory theFractalFactory = (FractalFactory)EPackage.Registry.INSTANCE.getEFactory("http://org.ow2.fractal/f4e/fractal.ecore/1.0.0"); 
			if (theFractalFactory != null) {
				return theFractalFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new FractalFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FractalFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case FractalPackage.SYSTEM: return createSystem();
			case FractalPackage.ABSTRACT_COMPONENT: return createAbstractComponent();
			case FractalPackage.PARAMETER: return createParameter();
			case FractalPackage.VALUE: return createValue();
			case FractalPackage.VALUE_ELEMENT: return createValueElement();
			case FractalPackage.CONSTANT: return createConstant();
			case FractalPackage.REFERENCE: return createReference();
			case FractalPackage.FORMAL_PARAMETER: return createFormalParameter();
			case FractalPackage.DEFINITION: return createDefinition();
			case FractalPackage.DEFINITION_CALL: return createDefinitionCall();
			case FractalPackage.EFFECTIVE_PARAMETER: return createEffectiveParameter();
			case FractalPackage.INTERFACE: return createInterface();
			case FractalPackage.COMPONENT: return createComponent();
			case FractalPackage.CONTENT: return createContent();
			case FractalPackage.ATTRIBUTES_CONTROLLER: return createAttributesController();
			case FractalPackage.ATTRIBUTE: return createAttribute();
			case FractalPackage.BINDING: return createBinding();
			case FractalPackage.CONTROLLER: return createController();
			case FractalPackage.TEMPLATE_CONTROLLER: return createTemplateController();
			case FractalPackage.COMMENT: return createComment();
			case FractalPackage.COORDINATES: return createCoordinates();
			case FractalPackage.LOGGER: return createLogger();
			case FractalPackage.VIRTUAL_NODE: return createVirtualNode();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case FractalPackage.CARDINALITY:
				return createCardinalityFromString(eDataType, initialValue);
			case FractalPackage.ROLE:
				return createRoleFromString(eDataType, initialValue);
			case FractalPackage.CONTINGENCY:
				return createContingencyFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case FractalPackage.CARDINALITY:
				return convertCardinalityToString(eDataType, instanceValue);
			case FractalPackage.ROLE:
				return convertRoleToString(eDataType, instanceValue);
			case FractalPackage.CONTINGENCY:
				return convertContingencyToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.ow2.fractal.f4e.fractal.System createSystem() {
		SystemImpl system = new SystemImpl();
		return system;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractComponent createAbstractComponent() {
		AbstractComponentImpl abstractComponent = new AbstractComponentImpl();
		return abstractComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Parameter createParameter() {
		ParameterImpl parameter = new ParameterImpl();
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value createValue() {
		ValueImpl value = new ValueImpl();
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueElement createValueElement() {
		ValueElementImpl valueElement = new ValueElementImpl();
		return valueElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constant createConstant() {
		ConstantImpl constant = new ConstantImpl();
		return constant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference createReference() {
		ReferenceImpl reference = new ReferenceImpl();
		return reference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalParameter createFormalParameter() {
		FormalParameterImpl formalParameter = new FormalParameterImpl();
		return formalParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Definition createDefinition() {
		DefinitionImpl definition = new DefinitionImpl();
		if(mustCreateAdapters()){
			HelperAdapterFactory.getInstance().createAdapter(definition);
			EventAdapterFactory.getInstance().createAdapter(definition);
		}
		return definition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefinitionCall createDefinitionCall() {
		DefinitionCallImpl definitionCall = new DefinitionCallImpl();
		return definitionCall;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EffectiveParameter createEffectiveParameter() {
		EffectiveParameterImpl effectiveParameter = new EffectiveParameterImpl();
		return effectiveParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Interface createInterface() {
		InterfaceImpl interface_ = new InterfaceImpl();
		if(mustCreateAdapters()){
			HelperAdapterFactory.getInstance().createAdapter(interface_);
			EventAdapterFactory.getInstance().createAdapter(interface_);
		}
		return interface_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Component createComponent() {
		ComponentImpl component = new ComponentImpl();
		if(mustCreateAdapters()){
			HelperAdapterFactory.getInstance().createAdapter(component);
			EventAdapterFactory.getInstance().createAdapter(component);
		}
		return component;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Content createContent() {
		ContentImpl content = new ContentImpl();
		if(mustCreateAdapters()){
			HelperAdapterFactory.getInstance().createAdapter(content);
			EventAdapterFactory.getInstance().createAdapter(content);
		}
		return content;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public AttributesController createAttributesController() {
		AttributesControllerImpl attributesController = new AttributesControllerImpl();
		if(mustCreateAdapters()){
			HelperAdapterFactory.getInstance().createAdapter(attributesController);
			EventAdapterFactory.getInstance().createAdapter(attributesController);
		}
		return attributesController;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Attribute createAttribute() {
		AttributeImpl attribute = new AttributeImpl();
		if(mustCreateAdapters()){
			HelperAdapterFactory.getInstance().createAdapter(attribute);
			EventAdapterFactory.getInstance().createAdapter(attribute);
		}
		return attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Binding createBinding() {
		BindingImpl binding = new BindingImpl();
		if(mustCreateAdapters()){
			HelperAdapterFactory.getInstance().createAdapter(binding);
			EventAdapterFactory.getInstance().createAdapter(binding);
		}
		return binding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Controller createController() {
		ControllerImpl controller = new ControllerImpl();
		if(mustCreateAdapters()){
			HelperAdapterFactory.getInstance().createAdapter(controller);
			EventAdapterFactory.getInstance().createAdapter(controller);
		}
		return controller;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public TemplateController createTemplateController() {
		TemplateControllerImpl templateController = new TemplateControllerImpl();
		if(mustCreateAdapters()){
			HelperAdapterFactory.getInstance().createAdapter(templateController);
			EventAdapterFactory.getInstance().createAdapter(templateController);
		}
		return templateController;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Comment createComment() {
		CommentImpl comment = new CommentImpl();
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Coordinates createCoordinates() {
		CoordinatesImpl coordinates = new CoordinatesImpl();
		return coordinates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Logger createLogger() {
		LoggerImpl logger = new LoggerImpl();
		return logger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VirtualNode createVirtualNode() {
		VirtualNodeImpl virtualNode = new VirtualNodeImpl();
		return virtualNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Cardinality createCardinalityFromString(EDataType eDataType, String initialValue) {
		Cardinality result = Cardinality.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCardinalityToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role createRoleFromString(EDataType eDataType, String initialValue) {
		Role result = Role.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRoleToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Contingency createContingencyFromString(EDataType eDataType, String initialValue) {
		Contingency result = Contingency.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertContingencyToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FractalPackage getFractalPackage() {
		return (FractalPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static FractalPackage getPackage() {
		return FractalPackage.eINSTANCE;
	}

	protected boolean mustCreateAdapters = true;
	public boolean mustCreateAdapters(){
		return mustCreateAdapters;
	}
	
	public void mustCreateAdapters(boolean mustCreateAdapters){
		this.mustCreateAdapters = mustCreateAdapters;
	}
} //FractalFactoryImpl
