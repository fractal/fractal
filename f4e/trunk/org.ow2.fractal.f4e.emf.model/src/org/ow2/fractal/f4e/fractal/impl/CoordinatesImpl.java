/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.ow2.fractal.f4e.fractal.Coordinates;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.ICoordinatesHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelper;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Coordinates</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.CoordinatesImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.CoordinatesImpl#getX0 <em>X0</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.CoordinatesImpl#getX1 <em>X1</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.CoordinatesImpl#getY0 <em>Y0</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.CoordinatesImpl#getY1 <em>Y1</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.CoordinatesImpl#getColor <em>Color</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.CoordinatesImpl#getNameAST <em>Name AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.CoordinatesImpl#getX0AST <em>X0AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.CoordinatesImpl#getX1AST <em>X1AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.CoordinatesImpl#getY0AST <em>Y0AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.CoordinatesImpl#getY1AST <em>Y1AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.CoordinatesImpl#getColorAST <em>Color AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.CoordinatesImpl#getCoordinates <em>Coordinates</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.CoordinatesImpl#getAny <em>Any</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.CoordinatesImpl#getAnyAttributes <em>Any Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CoordinatesImpl extends EObjectImpl implements Coordinates {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getX0() <em>X0</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX0()
	 * @generated
	 * @ordered
	 */
	protected static final String X0_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getX0() <em>X0</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX0()
	 * @generated
	 * @ordered
	 */
	protected String x0 = X0_EDEFAULT;

	/**
	 * The default value of the '{@link #getX1() <em>X1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX1()
	 * @generated
	 * @ordered
	 */
	protected static final String X1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getX1() <em>X1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX1()
	 * @generated
	 * @ordered
	 */
	protected String x1 = X1_EDEFAULT;

	/**
	 * The default value of the '{@link #getY0() <em>Y0</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getY0()
	 * @generated
	 * @ordered
	 */
	protected static final String Y0_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getY0() <em>Y0</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getY0()
	 * @generated
	 * @ordered
	 */
	protected String y0 = Y0_EDEFAULT;

	/**
	 * The default value of the '{@link #getY1() <em>Y1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getY1()
	 * @generated
	 * @ordered
	 */
	protected static final String Y1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getY1() <em>Y1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getY1()
	 * @generated
	 * @ordered
	 */
	protected String y1 = Y1_EDEFAULT;

	/**
	 * The default value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected static final String COLOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected String color = COLOR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getNameAST() <em>Name AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameAST()
	 * @generated
	 * @ordered
	 */
	protected Value nameAST;

	/**
	 * The cached value of the '{@link #getX0AST() <em>X0AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX0AST()
	 * @generated
	 * @ordered
	 */
	protected Value x0AST;

	/**
	 * The cached value of the '{@link #getX1AST() <em>X1AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX1AST()
	 * @generated
	 * @ordered
	 */
	protected Value x1AST;

	/**
	 * The cached value of the '{@link #getY0AST() <em>Y0AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getY0AST()
	 * @generated
	 * @ordered
	 */
	protected Value y0AST;

	/**
	 * The cached value of the '{@link #getY1AST() <em>Y1AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getY1AST()
	 * @generated
	 * @ordered
	 */
	protected Value y1AST;

	/**
	 * The cached value of the '{@link #getColorAST() <em>Color AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColorAST()
	 * @generated
	 * @ordered
	 */
	protected Value colorAST;

	/**
	 * The cached value of the '{@link #getCoordinates() <em>Coordinates</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoordinates()
	 * @generated
	 * @ordered
	 */
	protected EList<Coordinates> coordinates;

	/**
	 * The cached value of the '{@link #getAny() <em>Any</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAny()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap any;

	/**
	 * The cached value of the '{@link #getAnyAttributes() <em>Any Attributes</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnyAttributes()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap anyAttributes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CoordinatesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	protected EClass eStaticClass() {
		return FractalPackage.Literals.COORDINATES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.COORDINATES__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getX0() {
		return x0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setX0(String newX0) {
		String oldX0 = x0;
		x0 = newX0;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.COORDINATES__X0, oldX0, x0));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getX1() {
		return x1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setX1(String newX1) {
		String oldX1 = x1;
		x1 = newX1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.COORDINATES__X1, oldX1, x1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getY0() {
		return y0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setY0(String newY0) {
		String oldY0 = y0;
		y0 = newY0;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.COORDINATES__Y0, oldY0, y0));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getY1() {
		return y1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setY1(String newY1) {
		String oldY1 = y1;
		y1 = newY1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.COORDINATES__Y1, oldY1, y1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getColor() {
		return color;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setColor(String newColor) {
		String oldColor = color;
		color = newColor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.COORDINATES__COLOR, oldColor, color));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getNameAST() {
		return nameAST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNameAST(Value newNameAST, NotificationChain msgs) {
		Value oldNameAST = nameAST;
		nameAST = newNameAST;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.COORDINATES__NAME_AST, oldNameAST, newNameAST);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNameAST(Value newNameAST) {
		if (newNameAST != nameAST) {
			NotificationChain msgs = null;
			if (nameAST != null)
				msgs = ((InternalEObject)nameAST).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.COORDINATES__NAME_AST, null, msgs);
			if (newNameAST != null)
				msgs = ((InternalEObject)newNameAST).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.COORDINATES__NAME_AST, null, msgs);
			msgs = basicSetNameAST(newNameAST, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.COORDINATES__NAME_AST, newNameAST, newNameAST));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getX0AST() {
		return x0AST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetX0AST(Value newX0AST, NotificationChain msgs) {
		Value oldX0AST = x0AST;
		x0AST = newX0AST;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.COORDINATES__X0_AST, oldX0AST, newX0AST);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setX0AST(Value newX0AST) {
		if (newX0AST != x0AST) {
			NotificationChain msgs = null;
			if (x0AST != null)
				msgs = ((InternalEObject)x0AST).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.COORDINATES__X0_AST, null, msgs);
			if (newX0AST != null)
				msgs = ((InternalEObject)newX0AST).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.COORDINATES__X0_AST, null, msgs);
			msgs = basicSetX0AST(newX0AST, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.COORDINATES__X0_AST, newX0AST, newX0AST));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getX1AST() {
		return x1AST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetX1AST(Value newX1AST, NotificationChain msgs) {
		Value oldX1AST = x1AST;
		x1AST = newX1AST;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.COORDINATES__X1_AST, oldX1AST, newX1AST);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setX1AST(Value newX1AST) {
		if (newX1AST != x1AST) {
			NotificationChain msgs = null;
			if (x1AST != null)
				msgs = ((InternalEObject)x1AST).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.COORDINATES__X1_AST, null, msgs);
			if (newX1AST != null)
				msgs = ((InternalEObject)newX1AST).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.COORDINATES__X1_AST, null, msgs);
			msgs = basicSetX1AST(newX1AST, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.COORDINATES__X1_AST, newX1AST, newX1AST));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getY0AST() {
		return y0AST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetY0AST(Value newY0AST, NotificationChain msgs) {
		Value oldY0AST = y0AST;
		y0AST = newY0AST;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.COORDINATES__Y0_AST, oldY0AST, newY0AST);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setY0AST(Value newY0AST) {
		if (newY0AST != y0AST) {
			NotificationChain msgs = null;
			if (y0AST != null)
				msgs = ((InternalEObject)y0AST).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.COORDINATES__Y0_AST, null, msgs);
			if (newY0AST != null)
				msgs = ((InternalEObject)newY0AST).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.COORDINATES__Y0_AST, null, msgs);
			msgs = basicSetY0AST(newY0AST, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.COORDINATES__Y0_AST, newY0AST, newY0AST));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getY1AST() {
		return y1AST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetY1AST(Value newY1AST, NotificationChain msgs) {
		Value oldY1AST = y1AST;
		y1AST = newY1AST;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.COORDINATES__Y1_AST, oldY1AST, newY1AST);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setY1AST(Value newY1AST) {
		if (newY1AST != y1AST) {
			NotificationChain msgs = null;
			if (y1AST != null)
				msgs = ((InternalEObject)y1AST).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.COORDINATES__Y1_AST, null, msgs);
			if (newY1AST != null)
				msgs = ((InternalEObject)newY1AST).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.COORDINATES__Y1_AST, null, msgs);
			msgs = basicSetY1AST(newY1AST, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.COORDINATES__Y1_AST, newY1AST, newY1AST));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getColorAST() {
		return colorAST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetColorAST(Value newColorAST, NotificationChain msgs) {
		Value oldColorAST = colorAST;
		colorAST = newColorAST;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.COORDINATES__COLOR_AST, oldColorAST, newColorAST);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setColorAST(Value newColorAST) {
		if (newColorAST != colorAST) {
			NotificationChain msgs = null;
			if (colorAST != null)
				msgs = ((InternalEObject)colorAST).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.COORDINATES__COLOR_AST, null, msgs);
			if (newColorAST != null)
				msgs = ((InternalEObject)newColorAST).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.COORDINATES__COLOR_AST, null, msgs);
			msgs = basicSetColorAST(newColorAST, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.COORDINATES__COLOR_AST, newColorAST, newColorAST));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Coordinates> getCoordinates() {
		if (coordinates == null) {
			coordinates = new EObjectContainmentEList<Coordinates>(Coordinates.class, this, FractalPackage.COORDINATES__COORDINATES);
		}
		return coordinates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getAny() {
		if (any == null) {
			any = new BasicFeatureMap(this, FractalPackage.COORDINATES__ANY);
		}
		return any;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getAnyAttributes() {
		if (anyAttributes == null) {
			anyAttributes = new BasicFeatureMap(this, FractalPackage.COORDINATES__ANY_ATTRIBUTES);
		}
		return anyAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.COORDINATES__NAME_AST:
				return basicSetNameAST(null, msgs);
			case FractalPackage.COORDINATES__X0_AST:
				return basicSetX0AST(null, msgs);
			case FractalPackage.COORDINATES__X1_AST:
				return basicSetX1AST(null, msgs);
			case FractalPackage.COORDINATES__Y0_AST:
				return basicSetY0AST(null, msgs);
			case FractalPackage.COORDINATES__Y1_AST:
				return basicSetY1AST(null, msgs);
			case FractalPackage.COORDINATES__COLOR_AST:
				return basicSetColorAST(null, msgs);
			case FractalPackage.COORDINATES__COORDINATES:
				return ((InternalEList<?>)getCoordinates()).basicRemove(otherEnd, msgs);
			case FractalPackage.COORDINATES__ANY:
				return ((InternalEList<?>)getAny()).basicRemove(otherEnd, msgs);
			case FractalPackage.COORDINATES__ANY_ATTRIBUTES:
				return ((InternalEList<?>)getAnyAttributes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FractalPackage.COORDINATES__NAME:
				return getName();
			case FractalPackage.COORDINATES__X0:
				return getX0();
			case FractalPackage.COORDINATES__X1:
				return getX1();
			case FractalPackage.COORDINATES__Y0:
				return getY0();
			case FractalPackage.COORDINATES__Y1:
				return getY1();
			case FractalPackage.COORDINATES__COLOR:
				return getColor();
			case FractalPackage.COORDINATES__NAME_AST:
				return getNameAST();
			case FractalPackage.COORDINATES__X0_AST:
				return getX0AST();
			case FractalPackage.COORDINATES__X1_AST:
				return getX1AST();
			case FractalPackage.COORDINATES__Y0_AST:
				return getY0AST();
			case FractalPackage.COORDINATES__Y1_AST:
				return getY1AST();
			case FractalPackage.COORDINATES__COLOR_AST:
				return getColorAST();
			case FractalPackage.COORDINATES__COORDINATES:
				return getCoordinates();
			case FractalPackage.COORDINATES__ANY:
				if (coreType) return getAny();
				return ((FeatureMap.Internal)getAny()).getWrapper();
			case FractalPackage.COORDINATES__ANY_ATTRIBUTES:
				if (coreType) return getAnyAttributes();
				return ((FeatureMap.Internal)getAnyAttributes()).getWrapper();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FractalPackage.COORDINATES__NAME:
				setName((String)newValue);
				return;
			case FractalPackage.COORDINATES__X0:
				setX0((String)newValue);
				return;
			case FractalPackage.COORDINATES__X1:
				setX1((String)newValue);
				return;
			case FractalPackage.COORDINATES__Y0:
				setY0((String)newValue);
				return;
			case FractalPackage.COORDINATES__Y1:
				setY1((String)newValue);
				return;
			case FractalPackage.COORDINATES__COLOR:
				setColor((String)newValue);
				return;
			case FractalPackage.COORDINATES__NAME_AST:
				setNameAST((Value)newValue);
				return;
			case FractalPackage.COORDINATES__X0_AST:
				setX0AST((Value)newValue);
				return;
			case FractalPackage.COORDINATES__X1_AST:
				setX1AST((Value)newValue);
				return;
			case FractalPackage.COORDINATES__Y0_AST:
				setY0AST((Value)newValue);
				return;
			case FractalPackage.COORDINATES__Y1_AST:
				setY1AST((Value)newValue);
				return;
			case FractalPackage.COORDINATES__COLOR_AST:
				setColorAST((Value)newValue);
				return;
			case FractalPackage.COORDINATES__COORDINATES:
				getCoordinates().clear();
				getCoordinates().addAll((Collection<? extends Coordinates>)newValue);
				return;
			case FractalPackage.COORDINATES__ANY:
				((FeatureMap.Internal)getAny()).set(newValue);
				return;
			case FractalPackage.COORDINATES__ANY_ATTRIBUTES:
				((FeatureMap.Internal)getAnyAttributes()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public void eUnset(int featureID) {
		switch (featureID) {
			case FractalPackage.COORDINATES__NAME:
				setName(NAME_EDEFAULT);
				return;
			case FractalPackage.COORDINATES__X0:
				setX0(X0_EDEFAULT);
				return;
			case FractalPackage.COORDINATES__X1:
				setX1(X1_EDEFAULT);
				return;
			case FractalPackage.COORDINATES__Y0:
				setY0(Y0_EDEFAULT);
				return;
			case FractalPackage.COORDINATES__Y1:
				setY1(Y1_EDEFAULT);
				return;
			case FractalPackage.COORDINATES__COLOR:
				setColor(COLOR_EDEFAULT);
				return;
			case FractalPackage.COORDINATES__NAME_AST:
				setNameAST((Value)null);
				return;
			case FractalPackage.COORDINATES__X0_AST:
				setX0AST((Value)null);
				return;
			case FractalPackage.COORDINATES__X1_AST:
				setX1AST((Value)null);
				return;
			case FractalPackage.COORDINATES__Y0_AST:
				setY0AST((Value)null);
				return;
			case FractalPackage.COORDINATES__Y1_AST:
				setY1AST((Value)null);
				return;
			case FractalPackage.COORDINATES__COLOR_AST:
				setColorAST((Value)null);
				return;
			case FractalPackage.COORDINATES__COORDINATES:
				getCoordinates().clear();
				return;
			case FractalPackage.COORDINATES__ANY:
				getAny().clear();
				return;
			case FractalPackage.COORDINATES__ANY_ATTRIBUTES:
				getAnyAttributes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FractalPackage.COORDINATES__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case FractalPackage.COORDINATES__X0:
				return X0_EDEFAULT == null ? x0 != null : !X0_EDEFAULT.equals(x0);
			case FractalPackage.COORDINATES__X1:
				return X1_EDEFAULT == null ? x1 != null : !X1_EDEFAULT.equals(x1);
			case FractalPackage.COORDINATES__Y0:
				return Y0_EDEFAULT == null ? y0 != null : !Y0_EDEFAULT.equals(y0);
			case FractalPackage.COORDINATES__Y1:
				return Y1_EDEFAULT == null ? y1 != null : !Y1_EDEFAULT.equals(y1);
			case FractalPackage.COORDINATES__COLOR:
				return COLOR_EDEFAULT == null ? color != null : !COLOR_EDEFAULT.equals(color);
			case FractalPackage.COORDINATES__NAME_AST:
				return nameAST != null;
			case FractalPackage.COORDINATES__X0_AST:
				return x0AST != null;
			case FractalPackage.COORDINATES__X1_AST:
				return x1AST != null;
			case FractalPackage.COORDINATES__Y0_AST:
				return y0AST != null;
			case FractalPackage.COORDINATES__Y1_AST:
				return y1AST != null;
			case FractalPackage.COORDINATES__COLOR_AST:
				return colorAST != null;
			case FractalPackage.COORDINATES__COORDINATES:
				return coordinates != null && !coordinates.isEmpty();
			case FractalPackage.COORDINATES__ANY:
				return any != null && !any.isEmpty();
			case FractalPackage.COORDINATES__ANY_ATTRIBUTES:
				return anyAttributes != null && !anyAttributes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", x0: ");
		result.append(x0);
		result.append(", x1: ");
		result.append(x1);
		result.append(", y0: ");
		result.append(y0);
		result.append(", y1: ");
		result.append(y1);
		result.append(", color: ");
		result.append(color);
		result.append(", any: ");
		result.append(any);
		result.append(", anyAttributes: ");
		result.append(anyAttributes);
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	 
		
  	
	public Object getAdapter(Class adapter) {
		Object object = null;
		
		
		if(adapter == IHelper.class ||
			adapter == ICoordinatesHelper.class ){
			object = HelperAdapterFactory.getInstance().adapt(this);
		}
		
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public ICoordinatesHelper getHelper() {
		return (ICoordinatesHelper)getAdapter(ICoordinatesHelper.class);
	}

} //CoordinatesImpl
