/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.ITemplateControllerHelper;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Controller</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.TemplateController#getType <em>Type</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.TemplateController#getTypeAST <em>Type AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.TemplateController#getAbstractComponent <em>Abstract Component</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.TemplateController#getComments <em>Comments</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.TemplateController#getMergedType <em>Merged Type</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.TemplateController#getAny <em>Any</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.TemplateController#getAnyAttributes <em>Any Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getTemplateController()
 * @model
 * @generated
 */
public interface TemplateController extends EObject, IAdaptable, IHelperAdapter {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getTemplateController_Type()
	 * @model
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.TemplateController#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

	/**
	 * Returns the value of the '<em><b>Type AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type AST</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type AST</em>' containment reference.
	 * @see #setTypeAST(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getTemplateController_TypeAST()
	 * @model containment="true" required="true" transient="true"
	 * @generated
	 */
	Value getTypeAST();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.TemplateController#getTypeAST <em>Type AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type AST</em>' containment reference.
	 * @see #getTypeAST()
	 * @generated
	 */
	void setTypeAST(Value value);

	/**
	 * Returns the value of the '<em><b>Abstract Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract Component</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract Component</em>' reference.
	 * @see #setAbstractComponent(AbstractComponent)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getTemplateController_AbstractComponent()
	 * @model
	 * @generated
	 */
	AbstractComponent getAbstractComponent();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.TemplateController#getAbstractComponent <em>Abstract Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract Component</em>' reference.
	 * @see #getAbstractComponent()
	 * @generated
	 */
	void setAbstractComponent(AbstractComponent value);

	/**
	 * Returns the value of the '<em><b>Comments</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.Comment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comments</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getTemplateController_Comments()
	 * @model containment="true"
	 * @generated
	 */
	EList<Comment> getComments();

	/**
	 * Returns the value of the '<em><b>Merged Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Type</em>' attribute.
	 * @see #setMergedType(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getTemplateController_MergedType()
	 * @model transient="true"
	 * @generated
	 */
	String getMergedType();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.TemplateController#getMergedType <em>Merged Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merged Type</em>' attribute.
	 * @see #getMergedType()
	 * @generated
	 */
	void setMergedType(String value);

	/**
	 * Returns the value of the '<em><b>Any</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any</em>' attribute list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getTemplateController_Any()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' wildcards='##any' name=':6' processing='lax'"
	 * @generated
	 */
	FeatureMap getAny();

	/**
	 * Returns the value of the '<em><b>Any Attributes</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any Attributes</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any Attributes</em>' attribute list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getTemplateController_AnyAttributes()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='attributeWildcard' wildcards='##any' name=':12' processing='lax'"
	 * @generated
	 */
	FeatureMap getAnyAttributes();

	ITemplateControllerHelper getHelper();

} // TemplateController
