/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.ow2.fractal.f4e.fractal.adapter.helper.IBindingHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binding</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Binding#getClient <em>Client</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Binding#getServer <em>Server</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Binding#getClientInterface <em>Client Interface</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Binding#getServerInterface <em>Server Interface</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Binding#getClientAST <em>Client AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Binding#getServerAST <em>Server AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Binding#getAbstractComponent <em>Abstract Component</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Binding#getComments <em>Comments</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Binding#getMergedClient <em>Merged Client</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Binding#getMergedServer <em>Merged Server</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Binding#getAny <em>Any</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Binding#getAnyAttributes <em>Any Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getBinding()
 * @model
 * @generated
 */
public interface Binding extends EObject, IAdaptable, IHelperAdapter {
	/**
	 * Returns the value of the '<em><b>Client</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Client</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Client</em>' attribute.
	 * @see #setClient(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getBinding_Client()
	 * @model
	 * @generated
	 */
	String getClient();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Binding#getClient <em>Client</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Client</em>' attribute.
	 * @see #getClient()
	 * @generated
	 */
	void setClient(String value);

	/**
	 * Returns the value of the '<em><b>Server</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Server</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Server</em>' attribute.
	 * @see #setServer(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getBinding_Server()
	 * @model
	 * @generated
	 */
	String getServer();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Binding#getServer <em>Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Server</em>' attribute.
	 * @see #getServer()
	 * @generated
	 */
	void setServer(String value);

	/**
	 * Returns the value of the '<em><b>Client Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Client Interface</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Client Interface</em>' reference.
	 * @see #setClientInterface(Interface)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getBinding_ClientInterface()
	 * @model
	 * @generated
	 */
	Interface getClientInterface();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Binding#getClientInterface <em>Client Interface</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Client Interface</em>' reference.
	 * @see #getClientInterface()
	 * @generated
	 */
	void setClientInterface(Interface value);

	/**
	 * Returns the value of the '<em><b>Server Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Server Interface</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Server Interface</em>' reference.
	 * @see #setServerInterface(Interface)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getBinding_ServerInterface()
	 * @model
	 * @generated
	 */
	Interface getServerInterface();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Binding#getServerInterface <em>Server Interface</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Server Interface</em>' reference.
	 * @see #getServerInterface()
	 * @generated
	 */
	void setServerInterface(Interface value);

	/**
	 * Returns the value of the '<em><b>Client AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Client AST</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Client AST</em>' containment reference.
	 * @see #setClientAST(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getBinding_ClientAST()
	 * @model containment="true" required="true" transient="true"
	 * @generated
	 */
	Value getClientAST();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Binding#getClientAST <em>Client AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Client AST</em>' containment reference.
	 * @see #getClientAST()
	 * @generated
	 */
	void setClientAST(Value value);

	/**
	 * Returns the value of the '<em><b>Server AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Server AST</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Server AST</em>' containment reference.
	 * @see #setServerAST(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getBinding_ServerAST()
	 * @model containment="true" required="true" transient="true"
	 * @generated
	 */
	Value getServerAST();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Binding#getServerAST <em>Server AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Server AST</em>' containment reference.
	 * @see #getServerAST()
	 * @generated
	 */
	void setServerAST(Value value);

	/**
	 * Returns the value of the '<em><b>Abstract Component</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getBindings <em>Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract Component</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract Component</em>' container reference.
	 * @see #setAbstractComponent(AbstractComponent)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getBinding_AbstractComponent()
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getBindings
	 * @model opposite="bindings" transient="false"
	 * @generated
	 */
	AbstractComponent getAbstractComponent();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Binding#getAbstractComponent <em>Abstract Component</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract Component</em>' container reference.
	 * @see #getAbstractComponent()
	 * @generated
	 */
	void setAbstractComponent(AbstractComponent value);

	/**
	 * Returns the value of the '<em><b>Comments</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.Comment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comments</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getBinding_Comments()
	 * @model containment="true"
	 * @generated
	 */
	EList<Comment> getComments();

	/**
	 * Returns the value of the '<em><b>Merged Client</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Client</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Client</em>' attribute.
	 * @see #setMergedClient(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getBinding_MergedClient()
	 * @model transient="true"
	 * @generated
	 */
	String getMergedClient();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Binding#getMergedClient <em>Merged Client</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merged Client</em>' attribute.
	 * @see #getMergedClient()
	 * @generated
	 */
	void setMergedClient(String value);

	/**
	 * Returns the value of the '<em><b>Merged Server</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Server</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Server</em>' attribute.
	 * @see #setMergedServer(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getBinding_MergedServer()
	 * @model transient="true"
	 * @generated
	 */
	String getMergedServer();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Binding#getMergedServer <em>Merged Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merged Server</em>' attribute.
	 * @see #getMergedServer()
	 * @generated
	 */
	void setMergedServer(String value);

	/**
	 * Returns the value of the '<em><b>Any</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any</em>' attribute list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getBinding_Any()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' wildcards='##any' name=':6' processing='lax'"
	 * @generated
	 */
	FeatureMap getAny();

	/**
	 * Returns the value of the '<em><b>Any Attributes</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any Attributes</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any Attributes</em>' attribute list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getBinding_AnyAttributes()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='attributeWildcard' wildcards='##any' name=':12' processing='lax'"
	 * @generated
	 */
	FeatureMap getAnyAttributes();

	IBindingHelper getHelper();

} // Binding
