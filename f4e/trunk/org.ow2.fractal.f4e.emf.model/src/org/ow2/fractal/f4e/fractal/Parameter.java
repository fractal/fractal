/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.IParameterHelper;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Parameter#getName <em>Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Parameter#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getParameter()
 * @model
 * @generated
 */
public interface Parameter extends EObject, IAdaptable, IHelperAdapter {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' containment reference.
	 * @see #setName(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getParameter_Name()
	 * @model containment="true"
	 * @generated
	 */
	Value getName();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Parameter#getName <em>Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' containment reference.
	 * @see #getName()
	 * @generated
	 */
	void setName(Value value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getParameter_Value()
	 * @model containment="true"
	 * @generated
	 */
	Value getValue();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Parameter#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Value value);

	IParameterHelper getHelper();





} // Parameter
