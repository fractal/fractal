/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.IInterfaceHelper;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Interface#getName <em>Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Interface#getSignature <em>Signature</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Interface#getRole <em>Role</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Interface#getCardinality <em>Cardinality</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Interface#getContingency <em>Contingency</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Interface#getNameAST <em>Name AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Interface#getSignatureAST <em>Signature AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Interface#getAbstractComponent <em>Abstract Component</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Interface#getComments <em>Comments</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Interface#getMergedName <em>Merged Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Interface#getMergedSignature <em>Merged Signature</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Interface#getMergedRole <em>Merged Role</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Interface#getMergedCardinality <em>Merged Cardinality</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Interface#getMergedContingency <em>Merged Contingency</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Interface#getAny <em>Any</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Interface#getAnyAttributes <em>Any Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getInterface()
 * @model
 * @generated
 */
public interface Interface extends EObject, IAdaptable, IHelperAdapter {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getInterface_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Signature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signature</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signature</em>' attribute.
	 * @see #isSetSignature()
	 * @see #unsetSignature()
	 * @see #setSignature(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getInterface_Signature()
	 * @model unsettable="true"
	 * @generated
	 */
	String getSignature();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getSignature <em>Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signature</em>' attribute.
	 * @see #isSetSignature()
	 * @see #unsetSignature()
	 * @see #getSignature()
	 * @generated
	 */
	void setSignature(String value);

	/**
	 * Unsets the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getSignature <em>Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSignature()
	 * @see #getSignature()
	 * @see #setSignature(String)
	 * @generated
	 */
	void unsetSignature();

	/**
	 * Returns whether the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getSignature <em>Signature</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Signature</em>' attribute is set.
	 * @see #unsetSignature()
	 * @see #getSignature()
	 * @see #setSignature(String)
	 * @generated
	 */
	boolean isSetSignature();

	/**
	 * Returns the value of the '<em><b>Role</b></em>' attribute.
	 * The default value is <code>"client"</code>.
	 * The literals are from the enumeration {@link org.ow2.fractal.f4e.fractal.Role}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role</em>' attribute.
	 * @see org.ow2.fractal.f4e.fractal.Role
	 * @see #isSetRole()
	 * @see #unsetRole()
	 * @see #setRole(Role)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getInterface_Role()
	 * @model default="client" unsettable="true"
	 * @generated
	 */
	Role getRole();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getRole <em>Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role</em>' attribute.
	 * @see org.ow2.fractal.f4e.fractal.Role
	 * @see #isSetRole()
	 * @see #unsetRole()
	 * @see #getRole()
	 * @generated
	 */
	void setRole(Role value);

	/**
	 * Unsets the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getRole <em>Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetRole()
	 * @see #getRole()
	 * @see #setRole(Role)
	 * @generated
	 */
	void unsetRole();

	/**
	 * Returns whether the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getRole <em>Role</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Role</em>' attribute is set.
	 * @see #unsetRole()
	 * @see #getRole()
	 * @see #setRole(Role)
	 * @generated
	 */
	boolean isSetRole();

	/**
	 * Returns the value of the '<em><b>Cardinality</b></em>' attribute.
	 * The literals are from the enumeration {@link org.ow2.fractal.f4e.fractal.Cardinality}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cardinality</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cardinality</em>' attribute.
	 * @see org.ow2.fractal.f4e.fractal.Cardinality
	 * @see #isSetCardinality()
	 * @see #unsetCardinality()
	 * @see #setCardinality(Cardinality)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getInterface_Cardinality()
	 * @model unsettable="true"
	 * @generated
	 */
	Cardinality getCardinality();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getCardinality <em>Cardinality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cardinality</em>' attribute.
	 * @see org.ow2.fractal.f4e.fractal.Cardinality
	 * @see #isSetCardinality()
	 * @see #unsetCardinality()
	 * @see #getCardinality()
	 * @generated
	 */
	void setCardinality(Cardinality value);

	/**
	 * Unsets the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getCardinality <em>Cardinality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCardinality()
	 * @see #getCardinality()
	 * @see #setCardinality(Cardinality)
	 * @generated
	 */
	void unsetCardinality();

	/**
	 * Returns whether the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getCardinality <em>Cardinality</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Cardinality</em>' attribute is set.
	 * @see #unsetCardinality()
	 * @see #getCardinality()
	 * @see #setCardinality(Cardinality)
	 * @generated
	 */
	boolean isSetCardinality();

	/**
	 * Returns the value of the '<em><b>Contingency</b></em>' attribute.
	 * The literals are from the enumeration {@link org.ow2.fractal.f4e.fractal.Contingency}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contingency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contingency</em>' attribute.
	 * @see org.ow2.fractal.f4e.fractal.Contingency
	 * @see #isSetContingency()
	 * @see #unsetContingency()
	 * @see #setContingency(Contingency)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getInterface_Contingency()
	 * @model unsettable="true"
	 * @generated
	 */
	Contingency getContingency();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getContingency <em>Contingency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contingency</em>' attribute.
	 * @see org.ow2.fractal.f4e.fractal.Contingency
	 * @see #isSetContingency()
	 * @see #unsetContingency()
	 * @see #getContingency()
	 * @generated
	 */
	void setContingency(Contingency value);

	/**
	 * Unsets the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getContingency <em>Contingency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetContingency()
	 * @see #getContingency()
	 * @see #setContingency(Contingency)
	 * @generated
	 */
	void unsetContingency();

	/**
	 * Returns whether the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getContingency <em>Contingency</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Contingency</em>' attribute is set.
	 * @see #unsetContingency()
	 * @see #getContingency()
	 * @see #setContingency(Contingency)
	 * @generated
	 */
	boolean isSetContingency();

	/**
	 * Returns the value of the '<em><b>Name AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name AST</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name AST</em>' containment reference.
	 * @see #setNameAST(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getInterface_NameAST()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Value getNameAST();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getNameAST <em>Name AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name AST</em>' containment reference.
	 * @see #getNameAST()
	 * @generated
	 */
	void setNameAST(Value value);

	/**
	 * Returns the value of the '<em><b>Signature AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signature AST</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signature AST</em>' containment reference.
	 * @see #setSignatureAST(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getInterface_SignatureAST()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Value getSignatureAST();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getSignatureAST <em>Signature AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signature AST</em>' containment reference.
	 * @see #getSignatureAST()
	 * @generated
	 */
	void setSignatureAST(Value value);

	/**
	 * Returns the value of the '<em><b>Abstract Component</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getInterfaces <em>Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract Component</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract Component</em>' container reference.
	 * @see #setAbstractComponent(AbstractComponent)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getInterface_AbstractComponent()
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getInterfaces
	 * @model opposite="interfaces" transient="false"
	 * @generated
	 */
	AbstractComponent getAbstractComponent();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getAbstractComponent <em>Abstract Component</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract Component</em>' container reference.
	 * @see #getAbstractComponent()
	 * @generated
	 */
	void setAbstractComponent(AbstractComponent value);

	/**
	 * Returns the value of the '<em><b>Comments</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.Comment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comments</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getInterface_Comments()
	 * @model containment="true"
	 * @generated
	 */
	EList<Comment> getComments();

	/**
	 * Returns the value of the '<em><b>Merged Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Name</em>' attribute.
	 * @see #setMergedName(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getInterface_MergedName()
	 * @model transient="true"
	 * @generated
	 */
	String getMergedName();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getMergedName <em>Merged Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merged Name</em>' attribute.
	 * @see #getMergedName()
	 * @generated
	 */
	void setMergedName(String value);

	/**
	 * Returns the value of the '<em><b>Merged Signature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Signature</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Signature</em>' attribute.
	 * @see #setMergedSignature(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getInterface_MergedSignature()
	 * @model transient="true"
	 * @generated
	 */
	String getMergedSignature();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getMergedSignature <em>Merged Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merged Signature</em>' attribute.
	 * @see #getMergedSignature()
	 * @generated
	 */
	void setMergedSignature(String value);

	/**
	 * Returns the value of the '<em><b>Merged Role</b></em>' attribute.
	 * The literals are from the enumeration {@link org.ow2.fractal.f4e.fractal.Role}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Role</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Role</em>' attribute.
	 * @see org.ow2.fractal.f4e.fractal.Role
	 * @see #setMergedRole(Role)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getInterface_MergedRole()
	 * @model transient="true"
	 * @generated
	 */
	Role getMergedRole();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getMergedRole <em>Merged Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merged Role</em>' attribute.
	 * @see org.ow2.fractal.f4e.fractal.Role
	 * @see #getMergedRole()
	 * @generated
	 */
	void setMergedRole(Role value);

	/**
	 * Returns the value of the '<em><b>Merged Cardinality</b></em>' attribute.
	 * The literals are from the enumeration {@link org.ow2.fractal.f4e.fractal.Cardinality}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Cardinality</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Cardinality</em>' attribute.
	 * @see org.ow2.fractal.f4e.fractal.Cardinality
	 * @see #setMergedCardinality(Cardinality)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getInterface_MergedCardinality()
	 * @model transient="true"
	 * @generated
	 */
	Cardinality getMergedCardinality();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getMergedCardinality <em>Merged Cardinality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merged Cardinality</em>' attribute.
	 * @see org.ow2.fractal.f4e.fractal.Cardinality
	 * @see #getMergedCardinality()
	 * @generated
	 */
	void setMergedCardinality(Cardinality value);

	/**
	 * Returns the value of the '<em><b>Merged Contingency</b></em>' attribute.
	 * The literals are from the enumeration {@link org.ow2.fractal.f4e.fractal.Contingency}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Contingency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Contingency</em>' attribute.
	 * @see org.ow2.fractal.f4e.fractal.Contingency
	 * @see #setMergedContingency(Contingency)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getInterface_MergedContingency()
	 * @model transient="true"
	 * @generated
	 */
	Contingency getMergedContingency();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Interface#getMergedContingency <em>Merged Contingency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merged Contingency</em>' attribute.
	 * @see org.ow2.fractal.f4e.fractal.Contingency
	 * @see #getMergedContingency()
	 * @generated
	 */
	void setMergedContingency(Contingency value);

	/**
	 * Returns the value of the '<em><b>Any</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any</em>' attribute list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getInterface_Any()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' wildcards='##any' name=':6' processing='lax'"
	 * @generated
	 */
	FeatureMap getAny();

	/**
	 * Returns the value of the '<em><b>Any Attributes</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any Attributes</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any Attributes</em>' attribute list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getInterface_AnyAttributes()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='attributeWildcard' wildcards='##any' name=':12' processing='lax'"
	 * @generated
	 */
	FeatureMap getAnyAttributes();

	IInterfaceHelper getHelper();

	/**
	 * 
	 */
	boolean isMerged();
} // Interface
