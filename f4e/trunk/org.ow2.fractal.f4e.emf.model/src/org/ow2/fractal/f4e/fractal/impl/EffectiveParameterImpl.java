/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal.impl;

import org.eclipse.emf.ecore.EClass;

import org.ow2.fractal.f4e.fractal.EffectiveParameter;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IEffectiveParameterHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelper;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Effective Parameter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class EffectiveParameterImpl extends ParameterImpl implements EffectiveParameter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EffectiveParameterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	protected EClass eStaticClass() {
		return FractalPackage.Literals.EFFECTIVE_PARAMETER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	 
		
  	
	public Object getAdapter(Class adapter) {
		Object object = null;
		
		
		if(adapter == IHelper.class ||
			adapter == IEffectiveParameterHelper.class ){
			object = HelperAdapterFactory.getInstance().adapt(this);
		}
		
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public IEffectiveParameterHelper getHelper() {
		return (IEffectiveParameterHelper)getAdapter(IEffectiveParameterHelper.class);
	}

} //EffectiveParameterImpl
