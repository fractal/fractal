/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Attribute;
import org.ow2.fractal.f4e.fractal.AttributesController;
import org.ow2.fractal.f4e.fractal.Comment;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IAttributesControllerHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelper;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attributes Controller</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AttributesControllerImpl#getSignature <em>Signature</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AttributesControllerImpl#getAttributes <em>Attributes</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AttributesControllerImpl#getSignatureAST <em>Signature AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AttributesControllerImpl#getAbstractComponent <em>Abstract Component</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AttributesControllerImpl#getComments <em>Comments</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AttributesControllerImpl#getMergedAttributes <em>Merged Attributes</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AttributesControllerImpl#getMergedSignature <em>Merged Signature</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AttributesControllerImpl#getAny <em>Any</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AttributesControllerImpl#getAnyAttributes <em>Any Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AttributesControllerImpl extends EObjectImpl implements AttributesController {
	/**
	 * The default value of the '{@link #getSignature() <em>Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignature()
	 * @generated
	 * @ordered
	 */
	protected static final String SIGNATURE_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getSignature() <em>Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignature()
	 * @generated
	 * @ordered
	 */
	protected String signature = SIGNATURE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAttributes() <em>Attributes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributes()
	 * @generated
	 * @ordered
	 */
	protected EList<Attribute> attributes;

	/**
	 * The cached value of the '{@link #getSignatureAST() <em>Signature AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignatureAST()
	 * @generated
	 * @ordered
	 */
	protected Value signatureAST;

	/**
	 * The cached value of the '{@link #getComments() <em>Comments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComments()
	 * @generated
	 * @ordered
	 */
	protected EList<Comment> comments;

	/**
	 * The cached value of the '{@link #getMergedAttributes() <em>Merged Attributes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedAttributes()
	 * @generated
	 * @ordered
	 */
	protected EList<Attribute> mergedAttributes;

	/**
	 * The default value of the '{@link #getMergedSignature() <em>Merged Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedSignature()
	 * @generated
	 * @ordered
	 */
	protected static final String MERGED_SIGNATURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMergedSignature() <em>Merged Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedSignature()
	 * @generated
	 * @ordered
	 */
	protected String mergedSignature = MERGED_SIGNATURE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAny() <em>Any</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAny()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap any;

	/**
	 * The cached value of the '{@link #getAnyAttributes() <em>Any Attributes</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnyAttributes()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap anyAttributes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributesControllerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	protected EClass eStaticClass() {
		return FractalPackage.Literals.ATTRIBUTES_CONTROLLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSignature() {
		return signature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSignature(String newSignature) {
		String oldSignature = signature;
		signature = newSignature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ATTRIBUTES_CONTROLLER__SIGNATURE, oldSignature, signature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Attribute> getAttributes() {
		if (attributes == null) {
			attributes = new EObjectContainmentWithInverseEList<Attribute>(Attribute.class, this, FractalPackage.ATTRIBUTES_CONTROLLER__ATTRIBUTES, FractalPackage.ATTRIBUTE__ATTRIBUTES_CONTROLLER);
		}
		return attributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getSignatureAST() {
		return signatureAST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSignatureAST(Value newSignatureAST, NotificationChain msgs) {
		Value oldSignatureAST = signatureAST;
		signatureAST = newSignatureAST;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.ATTRIBUTES_CONTROLLER__SIGNATURE_AST, oldSignatureAST, newSignatureAST);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSignatureAST(Value newSignatureAST) {
		if (newSignatureAST != signatureAST) {
			NotificationChain msgs = null;
			if (signatureAST != null)
				msgs = ((InternalEObject)signatureAST).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ATTRIBUTES_CONTROLLER__SIGNATURE_AST, null, msgs);
			if (newSignatureAST != null)
				msgs = ((InternalEObject)newSignatureAST).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ATTRIBUTES_CONTROLLER__SIGNATURE_AST, null, msgs);
			msgs = basicSetSignatureAST(newSignatureAST, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ATTRIBUTES_CONTROLLER__SIGNATURE_AST, newSignatureAST, newSignatureAST));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractComponent getAbstractComponent() {
		if (eContainerFeatureID != FractalPackage.ATTRIBUTES_CONTROLLER__ABSTRACT_COMPONENT) return null;
		return (AbstractComponent)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAbstractComponent(AbstractComponent newAbstractComponent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newAbstractComponent, FractalPackage.ATTRIBUTES_CONTROLLER__ABSTRACT_COMPONENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstractComponent(AbstractComponent newAbstractComponent) {
		if (newAbstractComponent != eInternalContainer() || (eContainerFeatureID != FractalPackage.ATTRIBUTES_CONTROLLER__ABSTRACT_COMPONENT && newAbstractComponent != null)) {
			if (EcoreUtil.isAncestor(this, newAbstractComponent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newAbstractComponent != null)
				msgs = ((InternalEObject)newAbstractComponent).eInverseAdd(this, FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER, AbstractComponent.class, msgs);
			msgs = basicSetAbstractComponent(newAbstractComponent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ATTRIBUTES_CONTROLLER__ABSTRACT_COMPONENT, newAbstractComponent, newAbstractComponent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Comment> getComments() {
		if (comments == null) {
			comments = new EObjectContainmentEList<Comment>(Comment.class, this, FractalPackage.ATTRIBUTES_CONTROLLER__COMMENTS);
		}
		return comments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Attribute> getMergedAttributes() {
		if (mergedAttributes == null) {
			mergedAttributes = new EObjectContainmentEList<Attribute>(Attribute.class, this, FractalPackage.ATTRIBUTES_CONTROLLER__MERGED_ATTRIBUTES);
		}
		return mergedAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMergedSignature() {
		return mergedSignature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergedSignature(String newMergedSignature) {
		String oldMergedSignature = mergedSignature;
		mergedSignature = newMergedSignature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ATTRIBUTES_CONTROLLER__MERGED_SIGNATURE, oldMergedSignature, mergedSignature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getAny() {
		if (any == null) {
			any = new BasicFeatureMap(this, FractalPackage.ATTRIBUTES_CONTROLLER__ANY);
		}
		return any;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getAnyAttributes() {
		if (anyAttributes == null) {
			anyAttributes = new BasicFeatureMap(this, FractalPackage.ATTRIBUTES_CONTROLLER__ANY_ATTRIBUTES);
		}
		return anyAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.ATTRIBUTES_CONTROLLER__ATTRIBUTES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAttributes()).basicAdd(otherEnd, msgs);
			case FractalPackage.ATTRIBUTES_CONTROLLER__ABSTRACT_COMPONENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetAbstractComponent((AbstractComponent)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.ATTRIBUTES_CONTROLLER__ATTRIBUTES:
				return ((InternalEList<?>)getAttributes()).basicRemove(otherEnd, msgs);
			case FractalPackage.ATTRIBUTES_CONTROLLER__SIGNATURE_AST:
				return basicSetSignatureAST(null, msgs);
			case FractalPackage.ATTRIBUTES_CONTROLLER__ABSTRACT_COMPONENT:
				return basicSetAbstractComponent(null, msgs);
			case FractalPackage.ATTRIBUTES_CONTROLLER__COMMENTS:
				return ((InternalEList<?>)getComments()).basicRemove(otherEnd, msgs);
			case FractalPackage.ATTRIBUTES_CONTROLLER__MERGED_ATTRIBUTES:
				return ((InternalEList<?>)getMergedAttributes()).basicRemove(otherEnd, msgs);
			case FractalPackage.ATTRIBUTES_CONTROLLER__ANY:
				return ((InternalEList<?>)getAny()).basicRemove(otherEnd, msgs);
			case FractalPackage.ATTRIBUTES_CONTROLLER__ANY_ATTRIBUTES:
				return ((InternalEList<?>)getAnyAttributes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case FractalPackage.ATTRIBUTES_CONTROLLER__ABSTRACT_COMPONENT:
				return eInternalContainer().eInverseRemove(this, FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER, AbstractComponent.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FractalPackage.ATTRIBUTES_CONTROLLER__SIGNATURE:
				return getSignature();
			case FractalPackage.ATTRIBUTES_CONTROLLER__ATTRIBUTES:
				return getAttributes();
			case FractalPackage.ATTRIBUTES_CONTROLLER__SIGNATURE_AST:
				return getSignatureAST();
			case FractalPackage.ATTRIBUTES_CONTROLLER__ABSTRACT_COMPONENT:
				return getAbstractComponent();
			case FractalPackage.ATTRIBUTES_CONTROLLER__COMMENTS:
				return getComments();
			case FractalPackage.ATTRIBUTES_CONTROLLER__MERGED_ATTRIBUTES:
				return getMergedAttributes();
			case FractalPackage.ATTRIBUTES_CONTROLLER__MERGED_SIGNATURE:
				return getMergedSignature();
			case FractalPackage.ATTRIBUTES_CONTROLLER__ANY:
				if (coreType) return getAny();
				return ((FeatureMap.Internal)getAny()).getWrapper();
			case FractalPackage.ATTRIBUTES_CONTROLLER__ANY_ATTRIBUTES:
				if (coreType) return getAnyAttributes();
				return ((FeatureMap.Internal)getAnyAttributes()).getWrapper();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FractalPackage.ATTRIBUTES_CONTROLLER__SIGNATURE:
				setSignature((String)newValue);
				return;
			case FractalPackage.ATTRIBUTES_CONTROLLER__ATTRIBUTES:
				getAttributes().clear();
				getAttributes().addAll((Collection<? extends Attribute>)newValue);
				return;
			case FractalPackage.ATTRIBUTES_CONTROLLER__SIGNATURE_AST:
				setSignatureAST((Value)newValue);
				return;
			case FractalPackage.ATTRIBUTES_CONTROLLER__ABSTRACT_COMPONENT:
				setAbstractComponent((AbstractComponent)newValue);
				return;
			case FractalPackage.ATTRIBUTES_CONTROLLER__COMMENTS:
				getComments().clear();
				getComments().addAll((Collection<? extends Comment>)newValue);
				return;
			case FractalPackage.ATTRIBUTES_CONTROLLER__MERGED_ATTRIBUTES:
				getMergedAttributes().clear();
				getMergedAttributes().addAll((Collection<? extends Attribute>)newValue);
				return;
			case FractalPackage.ATTRIBUTES_CONTROLLER__MERGED_SIGNATURE:
				setMergedSignature((String)newValue);
				return;
			case FractalPackage.ATTRIBUTES_CONTROLLER__ANY:
				((FeatureMap.Internal)getAny()).set(newValue);
				return;
			case FractalPackage.ATTRIBUTES_CONTROLLER__ANY_ATTRIBUTES:
				((FeatureMap.Internal)getAnyAttributes()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public void eUnset(int featureID) {
		switch (featureID) {
			case FractalPackage.ATTRIBUTES_CONTROLLER__SIGNATURE:
				setSignature(SIGNATURE_EDEFAULT);
				return;
			case FractalPackage.ATTRIBUTES_CONTROLLER__ATTRIBUTES:
				getAttributes().clear();
				return;
			case FractalPackage.ATTRIBUTES_CONTROLLER__SIGNATURE_AST:
				setSignatureAST((Value)null);
				return;
			case FractalPackage.ATTRIBUTES_CONTROLLER__ABSTRACT_COMPONENT:
				setAbstractComponent((AbstractComponent)null);
				return;
			case FractalPackage.ATTRIBUTES_CONTROLLER__COMMENTS:
				getComments().clear();
				return;
			case FractalPackage.ATTRIBUTES_CONTROLLER__MERGED_ATTRIBUTES:
				getMergedAttributes().clear();
				return;
			case FractalPackage.ATTRIBUTES_CONTROLLER__MERGED_SIGNATURE:
				setMergedSignature(MERGED_SIGNATURE_EDEFAULT);
				return;
			case FractalPackage.ATTRIBUTES_CONTROLLER__ANY:
				getAny().clear();
				return;
			case FractalPackage.ATTRIBUTES_CONTROLLER__ANY_ATTRIBUTES:
				getAnyAttributes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FractalPackage.ATTRIBUTES_CONTROLLER__SIGNATURE:
				return SIGNATURE_EDEFAULT == null ? signature != null : !SIGNATURE_EDEFAULT.equals(signature);
			case FractalPackage.ATTRIBUTES_CONTROLLER__ATTRIBUTES:
				return attributes != null && !attributes.isEmpty();
			case FractalPackage.ATTRIBUTES_CONTROLLER__SIGNATURE_AST:
				return signatureAST != null;
			case FractalPackage.ATTRIBUTES_CONTROLLER__ABSTRACT_COMPONENT:
				return getAbstractComponent() != null;
			case FractalPackage.ATTRIBUTES_CONTROLLER__COMMENTS:
				return comments != null && !comments.isEmpty();
			case FractalPackage.ATTRIBUTES_CONTROLLER__MERGED_ATTRIBUTES:
				return mergedAttributes != null && !mergedAttributes.isEmpty();
			case FractalPackage.ATTRIBUTES_CONTROLLER__MERGED_SIGNATURE:
				return MERGED_SIGNATURE_EDEFAULT == null ? mergedSignature != null : !MERGED_SIGNATURE_EDEFAULT.equals(mergedSignature);
			case FractalPackage.ATTRIBUTES_CONTROLLER__ANY:
				return any != null && !any.isEmpty();
			case FractalPackage.ATTRIBUTES_CONTROLLER__ANY_ATTRIBUTES:
				return anyAttributes != null && !anyAttributes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (signature: ");
		result.append(signature);
		result.append(", mergedSignature: ");
		result.append(mergedSignature);
		result.append(", any: ");
		result.append(any);
		result.append(", anyAttributes: ");
		result.append(anyAttributes);
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	 
		
  	
	public Object getAdapter(Class adapter) {
		Object object = null;
		
		
		if(adapter == IHelper.class ||
			adapter == IAttributesControllerHelper.class ){
			object = HelperAdapterFactory.getInstance().adapt(this);
		}
		
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public IAttributesControllerHelper getHelper() {
		return (IAttributesControllerHelper)getAdapter(IAttributesControllerHelper.class);
	}

} //AttributesControllerImpl
