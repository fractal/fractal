/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.ow2.fractal.f4e.fractal.adapter.helper.IAbstractComponentHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getName <em>Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedName <em>Merged Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getNameAST <em>Name AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getExtendsAST <em>Extends AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getComments <em>Comments</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getInterfaces <em>Interfaces</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedInterfaces <em>Merged Interfaces</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getSubComponents <em>Sub Components</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedSubComponents <em>Merged Sub Components</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getContent <em>Content</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedContent <em>Merged Content</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getAttributesController <em>Attributes Controller</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedAttributesController <em>Merged Attributes Controller</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getBindings <em>Bindings</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedBindings <em>Merged Bindings</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getController <em>Controller</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedController <em>Merged Controller</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getTemplateController <em>Template Controller</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedTemplateController <em>Merged Template Controller</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getLogger <em>Logger</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedLogger <em>Merged Logger</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getVirtualNode <em>Virtual Node</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedVirtualNode <em>Merged Virtual Node</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getCoordinates <em>Coordinates</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getAny <em>Any</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getAnyAttributes <em>Any Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent()
 * @model
 * @generated
 */
public interface AbstractComponent extends EObject, IAdaptable, IHelperAdapter {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Merged Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Name</em>' attribute.
	 * @see #setMergedName(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_MergedName()
	 * @model transient="true"
	 * @generated
	 */
	String getMergedName();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedName <em>Merged Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merged Name</em>' attribute.
	 * @see #getMergedName()
	 * @generated
	 */
	void setMergedName(String value);

	/**
	 * Returns the value of the '<em><b>Name AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name AST</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name AST</em>' containment reference.
	 * @see #setNameAST(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_NameAST()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Value getNameAST();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getNameAST <em>Name AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name AST</em>' containment reference.
	 * @see #getNameAST()
	 * @generated
	 */
	void setNameAST(Value value);

	/**
	 * Returns the value of the '<em><b>Extends AST</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.DefinitionCall}.
	 * It is bidirectional and its opposite is '{@link org.ow2.fractal.f4e.fractal.DefinitionCall#getAbstractComponent <em>Abstract Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extends AST</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extends AST</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_ExtendsAST()
	 * @see org.ow2.fractal.f4e.fractal.DefinitionCall#getAbstractComponent
	 * @model opposite="abstractComponent" containment="true"
	 * @generated
	 */
	EList<DefinitionCall> getExtendsAST();

	/**
	 * Returns the value of the '<em><b>Comments</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.Comment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comments</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_Comments()
	 * @model containment="true"
	 * @generated
	 */
	EList<Comment> getComments();

	/**
	 * Returns the value of the '<em><b>Interfaces</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.Interface}.
	 * It is bidirectional and its opposite is '{@link org.ow2.fractal.f4e.fractal.Interface#getAbstractComponent <em>Abstract Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interfaces</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interfaces</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_Interfaces()
	 * @see org.ow2.fractal.f4e.fractal.Interface#getAbstractComponent
	 * @model opposite="abstractComponent" containment="true"
	 * @generated
	 */
	EList<Interface> getInterfaces();

	/**
	 * Returns the value of the '<em><b>Merged Interfaces</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.Interface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Interfaces</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Interfaces</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_MergedInterfaces()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	EList<Interface> getMergedInterfaces();

	/**
	 * Returns the value of the '<em><b>Sub Components</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.Component}.
	 * It is bidirectional and its opposite is '{@link org.ow2.fractal.f4e.fractal.Component#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Components</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Components</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_SubComponents()
	 * @see org.ow2.fractal.f4e.fractal.Component#getParent
	 * @model opposite="parent" containment="true"
	 * @generated
	 */
	EList<Component> getSubComponents();

	/**
	 * Returns the value of the '<em><b>Merged Sub Components</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.Component}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Sub Components</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Sub Components</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_MergedSubComponents()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	EList<Component> getMergedSubComponents();

	/**
	 * Returns the value of the '<em><b>Content</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link org.ow2.fractal.f4e.fractal.Content#getAbstractComponent <em>Abstract Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' containment reference.
	 * @see #setContent(Content)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_Content()
	 * @see org.ow2.fractal.f4e.fractal.Content#getAbstractComponent
	 * @model opposite="abstractComponent" containment="true"
	 * @generated
	 */
	Content getContent();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getContent <em>Content</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' containment reference.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(Content value);

	/**
	 * Returns the value of the '<em><b>Merged Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Content</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Content</em>' containment reference.
	 * @see #setMergedContent(Content)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_MergedContent()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	Content getMergedContent();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedContent <em>Merged Content</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merged Content</em>' containment reference.
	 * @see #getMergedContent()
	 * @generated
	 */
	void setMergedContent(Content value);

	/**
	 * Returns the value of the '<em><b>Attributes Controller</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link org.ow2.fractal.f4e.fractal.AttributesController#getAbstractComponent <em>Abstract Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attributes Controller</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes Controller</em>' containment reference.
	 * @see #setAttributesController(AttributesController)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_AttributesController()
	 * @see org.ow2.fractal.f4e.fractal.AttributesController#getAbstractComponent
	 * @model opposite="abstractComponent" containment="true"
	 * @generated
	 */
	AttributesController getAttributesController();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getAttributesController <em>Attributes Controller</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attributes Controller</em>' containment reference.
	 * @see #getAttributesController()
	 * @generated
	 */
	void setAttributesController(AttributesController value);

	/**
	 * Returns the value of the '<em><b>Merged Attributes Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Attributes Controller</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Attributes Controller</em>' containment reference.
	 * @see #setMergedAttributesController(AttributesController)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_MergedAttributesController()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	AttributesController getMergedAttributesController();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedAttributesController <em>Merged Attributes Controller</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merged Attributes Controller</em>' containment reference.
	 * @see #getMergedAttributesController()
	 * @generated
	 */
	void setMergedAttributesController(AttributesController value);

	/**
	 * Returns the value of the '<em><b>Bindings</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.Binding}.
	 * It is bidirectional and its opposite is '{@link org.ow2.fractal.f4e.fractal.Binding#getAbstractComponent <em>Abstract Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bindings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bindings</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_Bindings()
	 * @see org.ow2.fractal.f4e.fractal.Binding#getAbstractComponent
	 * @model opposite="abstractComponent" containment="true"
	 * @generated
	 */
	EList<Binding> getBindings();

	/**
	 * Returns the value of the '<em><b>Merged Bindings</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.Binding}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Bindings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Bindings</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_MergedBindings()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	EList<Binding> getMergedBindings();

	/**
	 * Returns the value of the '<em><b>Controller</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link org.ow2.fractal.f4e.fractal.Controller#getAbstractComponent <em>Abstract Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controller</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controller</em>' containment reference.
	 * @see #setController(Controller)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_Controller()
	 * @see org.ow2.fractal.f4e.fractal.Controller#getAbstractComponent
	 * @model opposite="abstractComponent" containment="true"
	 * @generated
	 */
	Controller getController();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getController <em>Controller</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Controller</em>' containment reference.
	 * @see #getController()
	 * @generated
	 */
	void setController(Controller value);

	/**
	 * Returns the value of the '<em><b>Merged Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Controller</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Controller</em>' containment reference.
	 * @see #setMergedController(Controller)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_MergedController()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	Controller getMergedController();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedController <em>Merged Controller</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merged Controller</em>' containment reference.
	 * @see #getMergedController()
	 * @generated
	 */
	void setMergedController(Controller value);

	/**
	 * Returns the value of the '<em><b>Template Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Template Controller</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Template Controller</em>' containment reference.
	 * @see #setTemplateController(TemplateController)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_TemplateController()
	 * @model containment="true"
	 * @generated
	 */
	TemplateController getTemplateController();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getTemplateController <em>Template Controller</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Template Controller</em>' containment reference.
	 * @see #getTemplateController()
	 * @generated
	 */
	void setTemplateController(TemplateController value);

	/**
	 * Returns the value of the '<em><b>Merged Template Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Template Controller</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Template Controller</em>' containment reference.
	 * @see #setMergedTemplateController(TemplateController)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_MergedTemplateController()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	TemplateController getMergedTemplateController();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedTemplateController <em>Merged Template Controller</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merged Template Controller</em>' containment reference.
	 * @see #getMergedTemplateController()
	 * @generated
	 */
	void setMergedTemplateController(TemplateController value);

	/**
	 * Returns the value of the '<em><b>Logger</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Logger</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Logger</em>' containment reference.
	 * @see #setLogger(Logger)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_Logger()
	 * @model containment="true"
	 * @generated
	 */
	Logger getLogger();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getLogger <em>Logger</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Logger</em>' containment reference.
	 * @see #getLogger()
	 * @generated
	 */
	void setLogger(Logger value);

	/**
	 * Returns the value of the '<em><b>Merged Logger</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Logger</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Logger</em>' containment reference.
	 * @see #setMergedLogger(Logger)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_MergedLogger()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	Logger getMergedLogger();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedLogger <em>Merged Logger</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merged Logger</em>' containment reference.
	 * @see #getMergedLogger()
	 * @generated
	 */
	void setMergedLogger(Logger value);

	/**
	 * Returns the value of the '<em><b>Virtual Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Virtual Node</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Virtual Node</em>' containment reference.
	 * @see #setVirtualNode(VirtualNode)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_VirtualNode()
	 * @model containment="true"
	 * @generated
	 */
	VirtualNode getVirtualNode();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getVirtualNode <em>Virtual Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Virtual Node</em>' containment reference.
	 * @see #getVirtualNode()
	 * @generated
	 */
	void setVirtualNode(VirtualNode value);

	/**
	 * Returns the value of the '<em><b>Merged Virtual Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Virtual Node</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Virtual Node</em>' containment reference.
	 * @see #setMergedVirtualNode(VirtualNode)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_MergedVirtualNode()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	VirtualNode getMergedVirtualNode();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedVirtualNode <em>Merged Virtual Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merged Virtual Node</em>' containment reference.
	 * @see #getMergedVirtualNode()
	 * @generated
	 */
	void setMergedVirtualNode(VirtualNode value);

	/**
	 * Returns the value of the '<em><b>Coordinates</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.Coordinates}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coordinates</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coordinates</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_Coordinates()
	 * @model containment="true"
	 * @generated
	 */
	EList<Coordinates> getCoordinates();

	/**
	 * Returns the value of the '<em><b>Any</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any</em>' attribute list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_Any()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' wildcards='##any' name=':6' processing='lax'"
	 * @generated
	 */
	FeatureMap getAny();

	
	
	/**
	 * Returns the value of the '<em><b>Any Attributes</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any Attributes</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any Attributes</em>' attribute list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAbstractComponent_AnyAttributes()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='attributeWildcard' wildcards='##any' name=':12' processing='lax'"
	 * @generated
	 */
	FeatureMap getAnyAttributes();

	IAbstractComponentHelper<?> getHelper();

} // AbstractComponent
