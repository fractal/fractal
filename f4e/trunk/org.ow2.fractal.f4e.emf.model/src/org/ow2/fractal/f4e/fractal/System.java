/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.ISystemHelper;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.System#getDefinitionCalls <em>Definition Calls</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.System#getPamameters <em>Pamameters</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.System#getDefinitions <em>Definitions</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getSystem()
 * @model
 * @generated
 */
public interface System extends EObject, IAdaptable, IHelperAdapter {
	/**
	 * Returns the value of the '<em><b>Definition Calls</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.DefinitionCall}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definition Calls</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition Calls</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getSystem_DefinitionCalls()
	 * @model containment="true"
	 * @generated
	 */
	EList<DefinitionCall> getDefinitionCalls();

	/**
	 * Returns the value of the '<em><b>Pamameters</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.Parameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pamameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pamameters</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getSystem_Pamameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<Parameter> getPamameters();

	/**
	 * Returns the value of the '<em><b>Definitions</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.Definition}.
	 * It is bidirectional and its opposite is '{@link org.ow2.fractal.f4e.fractal.Definition#getSystem <em>System</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definitions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definitions</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getSystem_Definitions()
	 * @see org.ow2.fractal.f4e.fractal.Definition#getSystem
	 * @model opposite="system" containment="true"
	 * @generated
	 */
	EList<Definition> getDefinitions();

	ISystemHelper getHelper();





} // System
