/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal;

import org.eclipse.core.runtime.IAdaptable;
import org.ow2.fractal.f4e.fractal.adapter.helper.IComponentHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Component#getDefinition <em>Definition</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Component#getShared <em>Shared</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Component#getParent <em>Parent</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getComponent()
 * @model
 * @generated
 */
public interface Component extends AbstractComponent, IAdaptable, IHelperAdapter {
	/**
	 * Returns the value of the '<em><b>Definition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition</em>' attribute.
	 * @see #setDefinition(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getComponent_Definition()
	 * @model
	 * @generated
	 */
	String getDefinition();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Component#getDefinition <em>Definition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Definition</em>' attribute.
	 * @see #getDefinition()
	 * @generated
	 */
	void setDefinition(String value);

	/**
	 * Returns the value of the '<em><b>Shared</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared</em>' reference.
	 * @see #setShared(Component)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getComponent_Shared()
	 * @model
	 * @generated
	 */
	Component getShared();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Component#getShared <em>Shared</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Shared</em>' reference.
	 * @see #getShared()
	 * @generated
	 */
	void setShared(Component value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getSubComponents <em>Sub Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(AbstractComponent)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getComponent_Parent()
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getSubComponents
	 * @model opposite="subComponents" transient="false"
	 * @generated
	 */
	AbstractComponent getParent();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Component#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(AbstractComponent value);

	IComponentHelper getHelper();

	boolean isMerged();
} // Component
