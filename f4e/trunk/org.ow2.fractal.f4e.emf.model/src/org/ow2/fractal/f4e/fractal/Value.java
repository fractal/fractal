/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.IValueHelper;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Value#getElements <em>Elements</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Value#getContextDefinition <em>Context Definition</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getValue()
 * @model
 * @generated
 */
public interface Value extends EObject, IAdaptable, IHelperAdapter {
	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.ValueElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getValue_Elements()
	 * @model containment="true"
	 * @generated
	 */
	EList<ValueElement> getElements();

	/**
	 * Returns the value of the '<em><b>Context Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context Definition</em>' reference.
	 * @see #setContextDefinition(Definition)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getValue_ContextDefinition()
	 * @model required="true"
	 * @generated
	 */
	Definition getContextDefinition();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Value#getContextDefinition <em>Context Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Context Definition</em>' reference.
	 * @see #getContextDefinition()
	 * @generated
	 */
	void setContextDefinition(Definition value);

	IValueHelper getHelper();





} // Value
