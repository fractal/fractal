/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.ow2.fractal.f4e.fractal.adapter.helper.IAttributeHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Attribute#getName <em>Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Attribute#getValue <em>Value</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Attribute#getNameAST <em>Name AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Attribute#getValueAST <em>Value AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Attribute#getAttributesController <em>Attributes Controller</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Attribute#getComments <em>Comments</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Attribute#getMergedName <em>Merged Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Attribute#getMergedValue <em>Merged Value</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Attribute#getAny <em>Any</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Attribute#getAnyAttributes <em>Any Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttribute()
 * @model
 * @generated
 */
public interface Attribute extends EObject, IAdaptable, IHelperAdapter {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttribute_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Attribute#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttribute_Value()
	 * @model default=""
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Attribute#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Name AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name AST</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name AST</em>' containment reference.
	 * @see #setNameAST(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttribute_NameAST()
	 * @model containment="true" required="true" transient="true"
	 * @generated
	 */
	Value getNameAST();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Attribute#getNameAST <em>Name AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name AST</em>' containment reference.
	 * @see #getNameAST()
	 * @generated
	 */
	void setNameAST(Value value);

	/**
	 * Returns the value of the '<em><b>Value AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value AST</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value AST</em>' containment reference.
	 * @see #setValueAST(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttribute_ValueAST()
	 * @model containment="true" required="true" transient="true"
	 * @generated
	 */
	Value getValueAST();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Attribute#getValueAST <em>Value AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value AST</em>' containment reference.
	 * @see #getValueAST()
	 * @generated
	 */
	void setValueAST(Value value);

	/**
	 * Returns the value of the '<em><b>Attributes Controller</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.ow2.fractal.f4e.fractal.AttributesController#getAttributes <em>Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attributes Controller</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes Controller</em>' container reference.
	 * @see #setAttributesController(AttributesController)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttribute_AttributesController()
	 * @see org.ow2.fractal.f4e.fractal.AttributesController#getAttributes
	 * @model opposite="attributes" transient="false"
	 * @generated
	 */
	AttributesController getAttributesController();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Attribute#getAttributesController <em>Attributes Controller</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attributes Controller</em>' container reference.
	 * @see #getAttributesController()
	 * @generated
	 */
	void setAttributesController(AttributesController value);

	/**
	 * Returns the value of the '<em><b>Comments</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.Comment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comments</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttribute_Comments()
	 * @model containment="true"
	 * @generated
	 */
	EList<Comment> getComments();

	/**
	 * Returns the value of the '<em><b>Merged Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Name</em>' attribute.
	 * @see #setMergedName(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttribute_MergedName()
	 * @model transient="true"
	 * @generated
	 */
	String getMergedName();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Attribute#getMergedName <em>Merged Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merged Name</em>' attribute.
	 * @see #getMergedName()
	 * @generated
	 */
	void setMergedName(String value);

	/**
	 * Returns the value of the '<em><b>Merged Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Value</em>' attribute.
	 * @see #setMergedValue(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttribute_MergedValue()
	 * @model transient="true"
	 * @generated
	 */
	String getMergedValue();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Attribute#getMergedValue <em>Merged Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merged Value</em>' attribute.
	 * @see #getMergedValue()
	 * @generated
	 */
	void setMergedValue(String value);

	/**
	 * Returns the value of the '<em><b>Any</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any</em>' attribute list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttribute_Any()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' wildcards='##any' name=':6' processing='lax'"
	 * @generated
	 */
	FeatureMap getAny();

	/**
	 * Returns the value of the '<em><b>Any Attributes</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any Attributes</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any Attributes</em>' attribute list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getAttribute_AnyAttributes()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='attributeWildcard' wildcards='##any' name=':12' processing='lax'"
	 * @generated
	 */
	FeatureMap getAnyAttributes();

	IAttributeHelper getHelper();

} // Attribute
