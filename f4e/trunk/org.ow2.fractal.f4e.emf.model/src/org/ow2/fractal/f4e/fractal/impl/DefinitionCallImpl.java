/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.EffectiveParameter;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IDefinitionCallHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelper;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Definition Call</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.DefinitionCallImpl#getDefinition <em>Definition</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.DefinitionCallImpl#getParameters <em>Parameters</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.DefinitionCallImpl#getDefinitionName <em>Definition Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.DefinitionCallImpl#getAbstractComponent <em>Abstract Component</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DefinitionCallImpl extends EObjectImpl implements DefinitionCall {
	/**
	 * The cached value of the '{@link #getDefinition() <em>Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefinition()
	 * @generated
	 * @ordered
	 */
	protected Definition definition;

	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<EffectiveParameter> parameters;

	/**
	 * The cached value of the '{@link #getDefinitionName() <em>Definition Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefinitionName()
	 * @generated
	 * @ordered
	 */
	protected Value definitionName;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DefinitionCallImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	protected EClass eStaticClass() {
		return FractalPackage.Literals.DEFINITION_CALL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Definition getDefinition() {
		if (definition != null && definition.eIsProxy()) {
			InternalEObject oldDefinition = (InternalEObject)definition;
			definition = (Definition)eResolveProxy(oldDefinition);
			if (definition != oldDefinition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FractalPackage.DEFINITION_CALL__DEFINITION, oldDefinition, definition));
			}
		}
		return definition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Definition basicGetDefinition() {
		return definition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefinition(Definition newDefinition) {
		Definition oldDefinition = definition;
		definition = newDefinition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.DEFINITION_CALL__DEFINITION, oldDefinition, definition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EffectiveParameter> getParameters() {
		if (parameters == null) {
			parameters = new EObjectContainmentEList<EffectiveParameter>(EffectiveParameter.class, this, FractalPackage.DEFINITION_CALL__PARAMETERS);
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getDefinitionName() {
		return definitionName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefinitionName(Value newDefinitionName, NotificationChain msgs) {
		Value oldDefinitionName = definitionName;
		definitionName = newDefinitionName;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.DEFINITION_CALL__DEFINITION_NAME, oldDefinitionName, newDefinitionName);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefinitionName(Value newDefinitionName) {
		if (newDefinitionName != definitionName) {
			NotificationChain msgs = null;
			if (definitionName != null)
				msgs = ((InternalEObject)definitionName).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.DEFINITION_CALL__DEFINITION_NAME, null, msgs);
			if (newDefinitionName != null)
				msgs = ((InternalEObject)newDefinitionName).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.DEFINITION_CALL__DEFINITION_NAME, null, msgs);
			msgs = basicSetDefinitionName(newDefinitionName, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.DEFINITION_CALL__DEFINITION_NAME, newDefinitionName, newDefinitionName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractComponent getAbstractComponent() {
		if (eContainerFeatureID != FractalPackage.DEFINITION_CALL__ABSTRACT_COMPONENT) return null;
		return (AbstractComponent)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAbstractComponent(AbstractComponent newAbstractComponent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newAbstractComponent, FractalPackage.DEFINITION_CALL__ABSTRACT_COMPONENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstractComponent(AbstractComponent newAbstractComponent) {
		if (newAbstractComponent != eInternalContainer() || (eContainerFeatureID != FractalPackage.DEFINITION_CALL__ABSTRACT_COMPONENT && newAbstractComponent != null)) {
			if (EcoreUtil.isAncestor(this, newAbstractComponent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newAbstractComponent != null)
				msgs = ((InternalEObject)newAbstractComponent).eInverseAdd(this, FractalPackage.ABSTRACT_COMPONENT__EXTENDS_AST, AbstractComponent.class, msgs);
			msgs = basicSetAbstractComponent(newAbstractComponent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.DEFINITION_CALL__ABSTRACT_COMPONENT, newAbstractComponent, newAbstractComponent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.DEFINITION_CALL__ABSTRACT_COMPONENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetAbstractComponent((AbstractComponent)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.DEFINITION_CALL__PARAMETERS:
				return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
			case FractalPackage.DEFINITION_CALL__DEFINITION_NAME:
				return basicSetDefinitionName(null, msgs);
			case FractalPackage.DEFINITION_CALL__ABSTRACT_COMPONENT:
				return basicSetAbstractComponent(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case FractalPackage.DEFINITION_CALL__ABSTRACT_COMPONENT:
				return eInternalContainer().eInverseRemove(this, FractalPackage.ABSTRACT_COMPONENT__EXTENDS_AST, AbstractComponent.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FractalPackage.DEFINITION_CALL__DEFINITION:
				if (resolve) return getDefinition();
				return basicGetDefinition();
			case FractalPackage.DEFINITION_CALL__PARAMETERS:
				return getParameters();
			case FractalPackage.DEFINITION_CALL__DEFINITION_NAME:
				return getDefinitionName();
			case FractalPackage.DEFINITION_CALL__ABSTRACT_COMPONENT:
				return getAbstractComponent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FractalPackage.DEFINITION_CALL__DEFINITION:
				setDefinition((Definition)newValue);
				return;
			case FractalPackage.DEFINITION_CALL__PARAMETERS:
				getParameters().clear();
				getParameters().addAll((Collection<? extends EffectiveParameter>)newValue);
				return;
			case FractalPackage.DEFINITION_CALL__DEFINITION_NAME:
				setDefinitionName((Value)newValue);
				return;
			case FractalPackage.DEFINITION_CALL__ABSTRACT_COMPONENT:
				setAbstractComponent((AbstractComponent)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public void eUnset(int featureID) {
		switch (featureID) {
			case FractalPackage.DEFINITION_CALL__DEFINITION:
				setDefinition((Definition)null);
				return;
			case FractalPackage.DEFINITION_CALL__PARAMETERS:
				getParameters().clear();
				return;
			case FractalPackage.DEFINITION_CALL__DEFINITION_NAME:
				setDefinitionName((Value)null);
				return;
			case FractalPackage.DEFINITION_CALL__ABSTRACT_COMPONENT:
				setAbstractComponent((AbstractComponent)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FractalPackage.DEFINITION_CALL__DEFINITION:
				return definition != null;
			case FractalPackage.DEFINITION_CALL__PARAMETERS:
				return parameters != null && !parameters.isEmpty();
			case FractalPackage.DEFINITION_CALL__DEFINITION_NAME:
				return definitionName != null;
			case FractalPackage.DEFINITION_CALL__ABSTRACT_COMPONENT:
				return getAbstractComponent() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	 
		
  	
	public Object getAdapter(Class adapter) {
		Object object = null;
		
		
		if(adapter == IHelper.class ||
			adapter == IDefinitionCallHelper.class ){
			object = HelperAdapterFactory.getInstance().adapt(this);
		}
		
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public IDefinitionCallHelper getHelper() {
		return (IDefinitionCallHelper)getAdapter(IDefinitionCallHelper.class);
	}

} //DefinitionCallImpl
