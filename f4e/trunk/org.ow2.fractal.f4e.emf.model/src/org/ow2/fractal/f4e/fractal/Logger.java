/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;
import org.ow2.fractal.f4e.fractal.adapter.helper.ILoggerHelper;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Logger</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Logger#getName <em>Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Logger#getNameAST <em>Name AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Logger#getMergedName <em>Merged Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Logger#getAny <em>Any</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Logger#getAnyAttributes <em>Any Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getLogger()
 * @model
 * @generated
 */
public interface Logger extends EObject, IAdaptable, IHelperAdapter {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getLogger_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Logger#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Name AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name AST</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name AST</em>' containment reference.
	 * @see #setNameAST(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getLogger_NameAST()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	Value getNameAST();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Logger#getNameAST <em>Name AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name AST</em>' containment reference.
	 * @see #getNameAST()
	 * @generated
	 */
	void setNameAST(Value value);

	/**
	 * Returns the value of the '<em><b>Merged Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merged Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merged Name</em>' attribute.
	 * @see #setMergedName(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getLogger_MergedName()
	 * @model transient="true"
	 * @generated
	 */
	String getMergedName();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Logger#getMergedName <em>Merged Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Merged Name</em>' attribute.
	 * @see #getMergedName()
	 * @generated
	 */
	void setMergedName(String value);

	/**
	 * Returns the value of the '<em><b>Any</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any</em>' attribute list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getLogger_Any()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' wildcards='##any' name=':6' processing='lax'"
	 * @generated
	 */
	FeatureMap getAny();

	/**
	 * Returns the value of the '<em><b>Any Attributes</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any Attributes</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any Attributes</em>' attribute list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getLogger_AnyAttributes()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='attributeWildcard' wildcards='##any' name=':12' processing='lax'"
	 * @generated
	 */
	FeatureMap getAnyAttributes();

	ILoggerHelper getHelper();

} // Logger
