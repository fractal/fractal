/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Cardinality;
import org.ow2.fractal.f4e.fractal.Comment;
import org.ow2.fractal.f4e.fractal.Contingency;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.Role;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IInterfaceHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.InterfaceHelperAdapter;
import org.ow2.fractal.f4e.fractal.notification.IFractalNotification;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interface</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.InterfaceImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.InterfaceImpl#getSignature <em>Signature</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.InterfaceImpl#getRole <em>Role</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.InterfaceImpl#getCardinality <em>Cardinality</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.InterfaceImpl#getContingency <em>Contingency</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.InterfaceImpl#getNameAST <em>Name AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.InterfaceImpl#getSignatureAST <em>Signature AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.InterfaceImpl#getAbstractComponent <em>Abstract Component</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.InterfaceImpl#getComments <em>Comments</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.InterfaceImpl#getMergedName <em>Merged Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.InterfaceImpl#getMergedSignature <em>Merged Signature</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.InterfaceImpl#getMergedRole <em>Merged Role</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.InterfaceImpl#getMergedCardinality <em>Merged Cardinality</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.InterfaceImpl#getMergedContingency <em>Merged Contingency</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.InterfaceImpl#getAny <em>Any</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.InterfaceImpl#getAnyAttributes <em>Any Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class InterfaceImpl extends EObjectImpl implements Interface {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSignature() <em>Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignature()
	 * @generated
	 * @ordered
	 */
	protected static final String SIGNATURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSignature() <em>Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignature()
	 * @generated
	 * @ordered
	 */
	protected String signature = SIGNATURE_EDEFAULT;

	/**
	 * This is true if the Signature attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean signatureESet;

	/**
	 * The default value of the '{@link #getRole() <em>Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole()
	 * @generated
	 * @ordered
	 */
	protected static final Role ROLE_EDEFAULT = Role.CLIENT;

	/**
	 * The cached value of the '{@link #getRole() <em>Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole()
	 * @generated
	 * @ordered
	 */
	protected Role role = ROLE_EDEFAULT;

	/**
	 * This is true if the Role attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean roleESet;

	/**
	 * The default value of the '{@link #getCardinality() <em>Cardinality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCardinality()
	 * @generated
	 * @ordered
	 */
	protected static final Cardinality CARDINALITY_EDEFAULT = Cardinality.SINGLETON;

	/**
	 * The cached value of the '{@link #getCardinality() <em>Cardinality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCardinality()
	 * @generated
	 * @ordered
	 */
	protected Cardinality cardinality = CARDINALITY_EDEFAULT;

	/**
	 * This is true if the Cardinality attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean cardinalityESet;

	/**
	 * The default value of the '{@link #getContingency() <em>Contingency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContingency()
	 * @generated
	 * @ordered
	 */
	protected static final Contingency CONTINGENCY_EDEFAULT = Contingency.MANDATORY;

	/**
	 * The cached value of the '{@link #getContingency() <em>Contingency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContingency()
	 * @generated
	 * @ordered
	 */
	protected Contingency contingency = CONTINGENCY_EDEFAULT;

	/**
	 * This is true if the Contingency attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean contingencyESet;

	/**
	 * The cached value of the '{@link #getNameAST() <em>Name AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameAST()
	 * @generated
	 * @ordered
	 */
	protected Value nameAST;

	/**
	 * The cached value of the '{@link #getSignatureAST() <em>Signature AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignatureAST()
	 * @generated
	 * @ordered
	 */
	protected Value signatureAST;

	/**
	 * The cached value of the '{@link #getComments() <em>Comments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComments()
	 * @generated
	 * @ordered
	 */
	protected EList<Comment> comments;

	/**
	 * The default value of the '{@link #getMergedName() <em>Merged Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedName()
	 * @generated
	 * @ordered
	 */
	protected static final String MERGED_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMergedName() <em>Merged Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedName()
	 * @generated
	 * @ordered
	 */
	protected String mergedName = MERGED_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getMergedSignature() <em>Merged Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedSignature()
	 * @generated
	 * @ordered
	 */
	protected static final String MERGED_SIGNATURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMergedSignature() <em>Merged Signature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedSignature()
	 * @generated
	 * @ordered
	 */
	protected String mergedSignature = MERGED_SIGNATURE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMergedRole() <em>Merged Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedRole()
	 * @generated
	 * @ordered
	 */
	protected static final Role MERGED_ROLE_EDEFAULT = Role.CLIENT;

	/**
	 * The cached value of the '{@link #getMergedRole() <em>Merged Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedRole()
	 * @generated
	 * @ordered
	 */
	protected Role mergedRole = MERGED_ROLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMergedCardinality() <em>Merged Cardinality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedCardinality()
	 * @generated
	 * @ordered
	 */
	protected static final Cardinality MERGED_CARDINALITY_EDEFAULT = Cardinality.SINGLETON;

	/**
	 * The cached value of the '{@link #getMergedCardinality() <em>Merged Cardinality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedCardinality()
	 * @generated
	 * @ordered
	 */
	protected Cardinality mergedCardinality = MERGED_CARDINALITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getMergedContingency() <em>Merged Contingency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedContingency()
	 * @generated
	 * @ordered
	 */
	protected static final Contingency MERGED_CONTINGENCY_EDEFAULT = Contingency.MANDATORY;

	/**
	 * The cached value of the '{@link #getMergedContingency() <em>Merged Contingency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedContingency()
	 * @generated
	 * @ordered
	 */
	protected Contingency mergedContingency = MERGED_CONTINGENCY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAny() <em>Any</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAny()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap any;

	/**
	 * The cached value of the '{@link #getAnyAttributes() <em>Any Attributes</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnyAttributes()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap anyAttributes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterfaceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	protected EClass eStaticClass() {
		return FractalPackage.Literals.INTERFACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.INTERFACE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getSignature() {
//		String result = signature;
//		if(!this.eIsSet(FractalPackage.INTERFACE__SIGNATURE)){
//			result = getMergedSignature();
//			if(result == null){
//				result = signature;
//			}
//		}
		return signature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSignature(String newSignature) {
		String oldSignature = signature;
		signature = newSignature;
		boolean oldSignatureESet = signatureESet;
		signatureESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.INTERFACE__SIGNATURE, oldSignature, signature, !oldSignatureESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSignature() {
		String oldSignature = signature;
		boolean oldSignatureESet = signatureESet;
		signature = SIGNATURE_EDEFAULT;
		signatureESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, FractalPackage.INTERFACE__SIGNATURE, oldSignature, SIGNATURE_EDEFAULT, oldSignatureESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSignature() {
		return signatureESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	private Role previousMergedRole = ROLE_EDEFAULT;
	public Role getRole() {
		Role result = role;
		if(!this.eIsSet(FractalPackage.INTERFACE__ROLE)){
			result = getMergedRole();
			
			if(result == null){
				result = role;
			}
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRole(Role newRole) {
		Role oldRole = role;
		role = newRole == null ? ROLE_EDEFAULT : newRole;
		boolean oldRoleESet = roleESet;
		roleESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.INTERFACE__ROLE, oldRole, role, !oldRoleESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetRole() {
		Role oldRole = role;
		boolean oldRoleESet = roleESet;
		role = ROLE_EDEFAULT;
		roleESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, FractalPackage.INTERFACE__ROLE, oldRole, ROLE_EDEFAULT, oldRoleESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetRole() {
		return roleESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Cardinality getCardinality() {
		Cardinality result = cardinality;
		if(!this.eIsSet(FractalPackage.INTERFACE__CARDINALITY)){
			result = getMergedCardinality();
			if(result == null){
				result = cardinality;
			}
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCardinality(Cardinality newCardinality) {
		Cardinality oldCardinality = cardinality;
		cardinality = newCardinality == null ? CARDINALITY_EDEFAULT : newCardinality;
		boolean oldCardinalityESet = cardinalityESet;
		cardinalityESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.INTERFACE__CARDINALITY, oldCardinality, cardinality, !oldCardinalityESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCardinality() {
		Cardinality oldCardinality = cardinality;
		boolean oldCardinalityESet = cardinalityESet;
		cardinality = CARDINALITY_EDEFAULT;
		cardinalityESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, FractalPackage.INTERFACE__CARDINALITY, oldCardinality, CARDINALITY_EDEFAULT, oldCardinalityESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCardinality() {
		return cardinalityESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Contingency getContingency() {
		Contingency result = contingency;
		if(!this.eIsSet(FractalPackage.INTERFACE__CONTINGENCY)){
			result = getMergedContingency();
			if(result == null){
				result = contingency;
			}
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContingency(Contingency newContingency) {
		Contingency oldContingency = contingency;
		contingency = newContingency == null ? CONTINGENCY_EDEFAULT : newContingency;
		boolean oldContingencyESet = contingencyESet;
		contingencyESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.INTERFACE__CONTINGENCY, oldContingency, contingency, !oldContingencyESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetContingency() {
		Contingency oldContingency = contingency;
		boolean oldContingencyESet = contingencyESet;
		contingency = CONTINGENCY_EDEFAULT;
		contingencyESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, FractalPackage.INTERFACE__CONTINGENCY, oldContingency, CONTINGENCY_EDEFAULT, oldContingencyESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetContingency() {
		return contingencyESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getNameAST() {
		return nameAST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNameAST(Value newNameAST, NotificationChain msgs) {
		Value oldNameAST = nameAST;
		nameAST = newNameAST;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.INTERFACE__NAME_AST, oldNameAST, newNameAST);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNameAST(Value newNameAST) {
		if (newNameAST != nameAST) {
			NotificationChain msgs = null;
			if (nameAST != null)
				msgs = ((InternalEObject)nameAST).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.INTERFACE__NAME_AST, null, msgs);
			if (newNameAST != null)
				msgs = ((InternalEObject)newNameAST).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.INTERFACE__NAME_AST, null, msgs);
			msgs = basicSetNameAST(newNameAST, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.INTERFACE__NAME_AST, newNameAST, newNameAST));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getSignatureAST() {
		return signatureAST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSignatureAST(Value newSignatureAST, NotificationChain msgs) {
		Value oldSignatureAST = signatureAST;
		signatureAST = newSignatureAST;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.INTERFACE__SIGNATURE_AST, oldSignatureAST, newSignatureAST);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSignatureAST(Value newSignatureAST) {
		if (newSignatureAST != signatureAST) {
			NotificationChain msgs = null;
			if (signatureAST != null)
				msgs = ((InternalEObject)signatureAST).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.INTERFACE__SIGNATURE_AST, null, msgs);
			if (newSignatureAST != null)
				msgs = ((InternalEObject)newSignatureAST).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.INTERFACE__SIGNATURE_AST, null, msgs);
			msgs = basicSetSignatureAST(newSignatureAST, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.INTERFACE__SIGNATURE_AST, newSignatureAST, newSignatureAST));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractComponent getAbstractComponent() {
		if (eContainerFeatureID != FractalPackage.INTERFACE__ABSTRACT_COMPONENT) return null;
		return (AbstractComponent)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAbstractComponent(AbstractComponent newAbstractComponent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newAbstractComponent, FractalPackage.INTERFACE__ABSTRACT_COMPONENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstractComponent(AbstractComponent newAbstractComponent) {
		if (newAbstractComponent != eInternalContainer() || (eContainerFeatureID != FractalPackage.INTERFACE__ABSTRACT_COMPONENT && newAbstractComponent != null)) {
			if (EcoreUtil.isAncestor(this, newAbstractComponent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newAbstractComponent != null)
				msgs = ((InternalEObject)newAbstractComponent).eInverseAdd(this, FractalPackage.ABSTRACT_COMPONENT__INTERFACES, AbstractComponent.class, msgs);
			msgs = basicSetAbstractComponent(newAbstractComponent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.INTERFACE__ABSTRACT_COMPONENT, newAbstractComponent, newAbstractComponent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Comment> getComments() {
		if (comments == null) {
			comments = new EObjectContainmentEList<Comment>(Comment.class, this, FractalPackage.INTERFACE__COMMENTS);
		}
		return comments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMergedName() {
		return mergedName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergedName(String newMergedName) {
		String oldMergedName = mergedName;
		mergedName = newMergedName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.INTERFACE__MERGED_NAME, oldMergedName, mergedName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMergedSignature() {
		return mergedSignature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergedSignature(String newMergedSignature) {
		String oldMergedSignature = mergedSignature;
		mergedSignature = newMergedSignature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.INTERFACE__MERGED_SIGNATURE, oldMergedSignature, mergedSignature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role getMergedRole() {
		return mergedRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergedRole(Role newMergedRole) {
		Role oldMergedRole = mergedRole;
		mergedRole = newMergedRole == null ? MERGED_ROLE_EDEFAULT : newMergedRole;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.INTERFACE__MERGED_ROLE, oldMergedRole, mergedRole));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Cardinality getMergedCardinality() {
		return mergedCardinality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergedCardinality(Cardinality newMergedCardinality) {
		Cardinality oldMergedCardinality = mergedCardinality;
		mergedCardinality = newMergedCardinality == null ? MERGED_CARDINALITY_EDEFAULT : newMergedCardinality;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.INTERFACE__MERGED_CARDINALITY, oldMergedCardinality, mergedCardinality));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Contingency getMergedContingency() {
		return mergedContingency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergedContingency(Contingency newMergedContingency) {
		Contingency oldMergedContingency = mergedContingency;
		mergedContingency = newMergedContingency == null ? MERGED_CONTINGENCY_EDEFAULT : newMergedContingency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.INTERFACE__MERGED_CONTINGENCY, oldMergedContingency, mergedContingency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getAny() {
		if (any == null) {
			any = new BasicFeatureMap(this, FractalPackage.INTERFACE__ANY);
		}
		return any;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getAnyAttributes() {
		if (anyAttributes == null) {
			anyAttributes = new BasicFeatureMap(this, FractalPackage.INTERFACE__ANY_ATTRIBUTES);
		}
		return anyAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.INTERFACE__ABSTRACT_COMPONENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetAbstractComponent((AbstractComponent)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.INTERFACE__NAME_AST:
				return basicSetNameAST(null, msgs);
			case FractalPackage.INTERFACE__SIGNATURE_AST:
				return basicSetSignatureAST(null, msgs);
			case FractalPackage.INTERFACE__ABSTRACT_COMPONENT:
				return basicSetAbstractComponent(null, msgs);
			case FractalPackage.INTERFACE__COMMENTS:
				return ((InternalEList<?>)getComments()).basicRemove(otherEnd, msgs);
			case FractalPackage.INTERFACE__ANY:
				return ((InternalEList<?>)getAny()).basicRemove(otherEnd, msgs);
			case FractalPackage.INTERFACE__ANY_ATTRIBUTES:
				return ((InternalEList<?>)getAnyAttributes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case FractalPackage.INTERFACE__ABSTRACT_COMPONENT:
				return eInternalContainer().eInverseRemove(this, FractalPackage.ABSTRACT_COMPONENT__INTERFACES, AbstractComponent.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FractalPackage.INTERFACE__NAME:
				return getName();
			case FractalPackage.INTERFACE__SIGNATURE:
				return getSignature();
			case FractalPackage.INTERFACE__ROLE:
				return getRole();
			case FractalPackage.INTERFACE__CARDINALITY:
				return getCardinality();
			case FractalPackage.INTERFACE__CONTINGENCY:
				return getContingency();
			case FractalPackage.INTERFACE__NAME_AST:
				return getNameAST();
			case FractalPackage.INTERFACE__SIGNATURE_AST:
				return getSignatureAST();
			case FractalPackage.INTERFACE__ABSTRACT_COMPONENT:
				return getAbstractComponent();
			case FractalPackage.INTERFACE__COMMENTS:
				return getComments();
			case FractalPackage.INTERFACE__MERGED_NAME:
				return getMergedName();
			case FractalPackage.INTERFACE__MERGED_SIGNATURE:
				return getMergedSignature();
			case FractalPackage.INTERFACE__MERGED_ROLE:
				return getMergedRole();
			case FractalPackage.INTERFACE__MERGED_CARDINALITY:
				return getMergedCardinality();
			case FractalPackage.INTERFACE__MERGED_CONTINGENCY:
				return getMergedContingency();
			case FractalPackage.INTERFACE__ANY:
				if (coreType) return getAny();
				return ((FeatureMap.Internal)getAny()).getWrapper();
			case FractalPackage.INTERFACE__ANY_ATTRIBUTES:
				if (coreType) return getAnyAttributes();
				return ((FeatureMap.Internal)getAnyAttributes()).getWrapper();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FractalPackage.INTERFACE__NAME:
				setName((String)newValue);
				return;
			case FractalPackage.INTERFACE__SIGNATURE:
				setSignature((String)newValue);
				return;
			case FractalPackage.INTERFACE__ROLE:
				setRole((Role)newValue);
				return;
			case FractalPackage.INTERFACE__CARDINALITY:
				setCardinality((Cardinality)newValue);
				return;
			case FractalPackage.INTERFACE__CONTINGENCY:
				setContingency((Contingency)newValue);
				return;
			case FractalPackage.INTERFACE__NAME_AST:
				setNameAST((Value)newValue);
				return;
			case FractalPackage.INTERFACE__SIGNATURE_AST:
				setSignatureAST((Value)newValue);
				return;
			case FractalPackage.INTERFACE__ABSTRACT_COMPONENT:
				setAbstractComponent((AbstractComponent)newValue);
				return;
			case FractalPackage.INTERFACE__COMMENTS:
				getComments().clear();
				getComments().addAll((Collection<? extends Comment>)newValue);
				return;
			case FractalPackage.INTERFACE__MERGED_NAME:
				setMergedName((String)newValue);
				return;
			case FractalPackage.INTERFACE__MERGED_SIGNATURE:
				setMergedSignature((String)newValue);
				return;
			case FractalPackage.INTERFACE__MERGED_ROLE:
				setMergedRole((Role)newValue);
				return;
			case FractalPackage.INTERFACE__MERGED_CARDINALITY:
				setMergedCardinality((Cardinality)newValue);
				return;
			case FractalPackage.INTERFACE__MERGED_CONTINGENCY:
				setMergedContingency((Contingency)newValue);
				return;
			case FractalPackage.INTERFACE__ANY:
				((FeatureMap.Internal)getAny()).set(newValue);
				return;
			case FractalPackage.INTERFACE__ANY_ATTRIBUTES:
				((FeatureMap.Internal)getAnyAttributes()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public void eUnset(int featureID) {
		switch (featureID) {
			case FractalPackage.INTERFACE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case FractalPackage.INTERFACE__SIGNATURE:
				unsetSignature();
				return;
			case FractalPackage.INTERFACE__ROLE:
				unsetRole();
				return;
			case FractalPackage.INTERFACE__CARDINALITY:
				unsetCardinality();
				return;
			case FractalPackage.INTERFACE__CONTINGENCY:
				unsetContingency();
				return;
			case FractalPackage.INTERFACE__NAME_AST:
				setNameAST((Value)null);
				return;
			case FractalPackage.INTERFACE__SIGNATURE_AST:
				setSignatureAST((Value)null);
				return;
			case FractalPackage.INTERFACE__ABSTRACT_COMPONENT:
				setAbstractComponent((AbstractComponent)null);
				return;
			case FractalPackage.INTERFACE__COMMENTS:
				getComments().clear();
				return;
			case FractalPackage.INTERFACE__MERGED_NAME:
				setMergedName(MERGED_NAME_EDEFAULT);
				return;
			case FractalPackage.INTERFACE__MERGED_SIGNATURE:
				setMergedSignature(MERGED_SIGNATURE_EDEFAULT);
				return;
			case FractalPackage.INTERFACE__MERGED_ROLE:
				setMergedRole(MERGED_ROLE_EDEFAULT);
				return;
			case FractalPackage.INTERFACE__MERGED_CARDINALITY:
				setMergedCardinality(MERGED_CARDINALITY_EDEFAULT);
				return;
			case FractalPackage.INTERFACE__MERGED_CONTINGENCY:
				setMergedContingency(MERGED_CONTINGENCY_EDEFAULT);
				return;
			case FractalPackage.INTERFACE__ANY:
				getAny().clear();
				return;
			case FractalPackage.INTERFACE__ANY_ATTRIBUTES:
				getAnyAttributes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FractalPackage.INTERFACE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case FractalPackage.INTERFACE__SIGNATURE:
				return isSetSignature();
			case FractalPackage.INTERFACE__ROLE:
				return isSetRole();
			case FractalPackage.INTERFACE__CARDINALITY:
				return isSetCardinality();
			case FractalPackage.INTERFACE__CONTINGENCY:
				return isSetContingency();
			case FractalPackage.INTERFACE__NAME_AST:
				return nameAST != null;
			case FractalPackage.INTERFACE__SIGNATURE_AST:
				return signatureAST != null;
			case FractalPackage.INTERFACE__ABSTRACT_COMPONENT:
				return getAbstractComponent() != null;
			case FractalPackage.INTERFACE__COMMENTS:
				return comments != null && !comments.isEmpty();
			case FractalPackage.INTERFACE__MERGED_NAME:
				return MERGED_NAME_EDEFAULT == null ? mergedName != null : !MERGED_NAME_EDEFAULT.equals(mergedName);
			case FractalPackage.INTERFACE__MERGED_SIGNATURE:
				return MERGED_SIGNATURE_EDEFAULT == null ? mergedSignature != null : !MERGED_SIGNATURE_EDEFAULT.equals(mergedSignature);
			case FractalPackage.INTERFACE__MERGED_ROLE:
				return mergedRole != MERGED_ROLE_EDEFAULT;
			case FractalPackage.INTERFACE__MERGED_CARDINALITY:
				return mergedCardinality != MERGED_CARDINALITY_EDEFAULT;
			case FractalPackage.INTERFACE__MERGED_CONTINGENCY:
				return mergedContingency != MERGED_CONTINGENCY_EDEFAULT;
			case FractalPackage.INTERFACE__ANY:
				return any != null && !any.isEmpty();
			case FractalPackage.INTERFACE__ANY_ATTRIBUTES:
				return anyAttributes != null && !anyAttributes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", signature: ");
		if (signatureESet) result.append(signature); else result.append("<unset>");
		result.append(", role: ");
		if (roleESet) result.append(role); else result.append("<unset>");
		result.append(", cardinality: ");
		if (cardinalityESet) result.append(cardinality); else result.append("<unset>");
		result.append(", contingency: ");
		if (contingencyESet) result.append(contingency); else result.append("<unset>");
		result.append(", mergedName: ");
		result.append(mergedName);
		result.append(", mergedSignature: ");
		result.append(mergedSignature);
		result.append(", mergedRole: ");
		result.append(mergedRole);
		result.append(", mergedCardinality: ");
		result.append(mergedCardinality);
		result.append(", mergedContingency: ");
		result.append(mergedContingency);
		result.append(", any: ");
		result.append(any);
		result.append(", anyAttributes: ");
		result.append(anyAttributes);
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	 
		
  	
	public Object getAdapter(Class adapter) {
		Object object = null;
		
		
		if(adapter == IHelper.class ||
			adapter == IInterfaceHelper.class ){
			object = HelperAdapterFactory.getInstance().adapt(this);
		}
		
		return object;
	}

	public boolean isMerged(){
		return this.eContainmentFeature() == FractalPackage.eINSTANCE.getAbstractComponent_MergedInterfaces();
	}
	
	public InterfaceHelperAdapter getHelper(){
		InterfaceHelperAdapter result = null;
		Adapter helper = HelperAdapterFactory.getInstance().adapt(this,InterfaceHelperAdapter.class);
		if(helper != null){
			result = (InterfaceHelperAdapter)helper;
		}
		return result;
	}
} //InterfaceImpl
