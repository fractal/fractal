/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.Comment;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IBindingHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelper;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Binding</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.BindingImpl#getClient <em>Client</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.BindingImpl#getServer <em>Server</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.BindingImpl#getClientInterface <em>Client Interface</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.BindingImpl#getServerInterface <em>Server Interface</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.BindingImpl#getClientAST <em>Client AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.BindingImpl#getServerAST <em>Server AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.BindingImpl#getAbstractComponent <em>Abstract Component</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.BindingImpl#getComments <em>Comments</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.BindingImpl#getMergedClient <em>Merged Client</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.BindingImpl#getMergedServer <em>Merged Server</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.BindingImpl#getAny <em>Any</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.BindingImpl#getAnyAttributes <em>Any Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BindingImpl extends EObjectImpl implements Binding {
	/**
	 * The default value of the '{@link #getClient() <em>Client</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClient()
	 * @generated
	 * @ordered
	 */
	protected static final String CLIENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getClient() <em>Client</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClient()
	 * @generated
	 * @ordered
	 */
	protected String client = CLIENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getServer() <em>Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServer()
	 * @generated
	 * @ordered
	 */
	protected static final String SERVER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getServer() <em>Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServer()
	 * @generated
	 * @ordered
	 */
	protected String server = SERVER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getClientInterface() <em>Client Interface</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClientInterface()
	 * @generated
	 * @ordered
	 */
	protected Interface clientInterface;

	/**
	 * The cached value of the '{@link #getServerInterface() <em>Server Interface</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServerInterface()
	 * @generated
	 * @ordered
	 */
	protected Interface serverInterface;

	/**
	 * The cached value of the '{@link #getClientAST() <em>Client AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClientAST()
	 * @generated
	 * @ordered
	 */
	protected Value clientAST;

	/**
	 * The cached value of the '{@link #getServerAST() <em>Server AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServerAST()
	 * @generated
	 * @ordered
	 */
	protected Value serverAST;

	/**
	 * The cached value of the '{@link #getComments() <em>Comments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComments()
	 * @generated
	 * @ordered
	 */
	protected EList<Comment> comments;

	/**
	 * The default value of the '{@link #getMergedClient() <em>Merged Client</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedClient()
	 * @generated
	 * @ordered
	 */
	protected static final String MERGED_CLIENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMergedClient() <em>Merged Client</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedClient()
	 * @generated
	 * @ordered
	 */
	protected String mergedClient = MERGED_CLIENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getMergedServer() <em>Merged Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedServer()
	 * @generated
	 * @ordered
	 */
	protected static final String MERGED_SERVER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMergedServer() <em>Merged Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedServer()
	 * @generated
	 * @ordered
	 */
	protected String mergedServer = MERGED_SERVER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAny() <em>Any</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAny()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap any;

	/**
	 * The cached value of the '{@link #getAnyAttributes() <em>Any Attributes</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnyAttributes()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap anyAttributes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BindingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	protected EClass eStaticClass() {
		return FractalPackage.Literals.BINDING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getClient() {
		return client;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClient(String newClient) {
		String oldClient = client;
		client = newClient;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.BINDING__CLIENT, oldClient, client));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getServer() {
		return server;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setServer(String newServer) {
		String oldServer = server;
		server = newServer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.BINDING__SERVER, oldServer, server));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interface getClientInterface() {
		if (clientInterface != null && clientInterface.eIsProxy()) {
			InternalEObject oldClientInterface = (InternalEObject)clientInterface;
			clientInterface = (Interface)eResolveProxy(oldClientInterface);
			if (clientInterface != oldClientInterface) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FractalPackage.BINDING__CLIENT_INTERFACE, oldClientInterface, clientInterface));
			}
		}
		return clientInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interface basicGetClientInterface() {
		return clientInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClientInterface(Interface newClientInterface) {
		Interface oldClientInterface = clientInterface;
		clientInterface = newClientInterface;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.BINDING__CLIENT_INTERFACE, oldClientInterface, clientInterface));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interface getServerInterface() {
		if (serverInterface != null && serverInterface.eIsProxy()) {
			InternalEObject oldServerInterface = (InternalEObject)serverInterface;
			serverInterface = (Interface)eResolveProxy(oldServerInterface);
			if (serverInterface != oldServerInterface) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FractalPackage.BINDING__SERVER_INTERFACE, oldServerInterface, serverInterface));
			}
		}
		return serverInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interface basicGetServerInterface() {
		return serverInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setServerInterface(Interface newServerInterface) {
		Interface oldServerInterface = serverInterface;
		serverInterface = newServerInterface;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.BINDING__SERVER_INTERFACE, oldServerInterface, serverInterface));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getClientAST() {
		return clientAST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClientAST(Value newClientAST, NotificationChain msgs) {
		Value oldClientAST = clientAST;
		clientAST = newClientAST;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.BINDING__CLIENT_AST, oldClientAST, newClientAST);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClientAST(Value newClientAST) {
		if (newClientAST != clientAST) {
			NotificationChain msgs = null;
			if (clientAST != null)
				msgs = ((InternalEObject)clientAST).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.BINDING__CLIENT_AST, null, msgs);
			if (newClientAST != null)
				msgs = ((InternalEObject)newClientAST).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.BINDING__CLIENT_AST, null, msgs);
			msgs = basicSetClientAST(newClientAST, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.BINDING__CLIENT_AST, newClientAST, newClientAST));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getServerAST() {
		return serverAST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetServerAST(Value newServerAST, NotificationChain msgs) {
		Value oldServerAST = serverAST;
		serverAST = newServerAST;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.BINDING__SERVER_AST, oldServerAST, newServerAST);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setServerAST(Value newServerAST) {
		if (newServerAST != serverAST) {
			NotificationChain msgs = null;
			if (serverAST != null)
				msgs = ((InternalEObject)serverAST).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.BINDING__SERVER_AST, null, msgs);
			if (newServerAST != null)
				msgs = ((InternalEObject)newServerAST).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.BINDING__SERVER_AST, null, msgs);
			msgs = basicSetServerAST(newServerAST, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.BINDING__SERVER_AST, newServerAST, newServerAST));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractComponent getAbstractComponent() {
		if (eContainerFeatureID != FractalPackage.BINDING__ABSTRACT_COMPONENT) return null;
		return (AbstractComponent)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAbstractComponent(AbstractComponent newAbstractComponent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newAbstractComponent, FractalPackage.BINDING__ABSTRACT_COMPONENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstractComponent(AbstractComponent newAbstractComponent) {
		if (newAbstractComponent != eInternalContainer() || (eContainerFeatureID != FractalPackage.BINDING__ABSTRACT_COMPONENT && newAbstractComponent != null)) {
			if (EcoreUtil.isAncestor(this, newAbstractComponent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newAbstractComponent != null)
				msgs = ((InternalEObject)newAbstractComponent).eInverseAdd(this, FractalPackage.ABSTRACT_COMPONENT__BINDINGS, AbstractComponent.class, msgs);
			msgs = basicSetAbstractComponent(newAbstractComponent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.BINDING__ABSTRACT_COMPONENT, newAbstractComponent, newAbstractComponent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Comment> getComments() {
		if (comments == null) {
			comments = new EObjectContainmentEList<Comment>(Comment.class, this, FractalPackage.BINDING__COMMENTS);
		}
		return comments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMergedClient() {
		return mergedClient;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergedClient(String newMergedClient) {
		String oldMergedClient = mergedClient;
		mergedClient = newMergedClient;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.BINDING__MERGED_CLIENT, oldMergedClient, mergedClient));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMergedServer() {
		return mergedServer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergedServer(String newMergedServer) {
		String oldMergedServer = mergedServer;
		mergedServer = newMergedServer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.BINDING__MERGED_SERVER, oldMergedServer, mergedServer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getAny() {
		if (any == null) {
			any = new BasicFeatureMap(this, FractalPackage.BINDING__ANY);
		}
		return any;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getAnyAttributes() {
		if (anyAttributes == null) {
			anyAttributes = new BasicFeatureMap(this, FractalPackage.BINDING__ANY_ATTRIBUTES);
		}
		return anyAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.BINDING__ABSTRACT_COMPONENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetAbstractComponent((AbstractComponent)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.BINDING__CLIENT_AST:
				return basicSetClientAST(null, msgs);
			case FractalPackage.BINDING__SERVER_AST:
				return basicSetServerAST(null, msgs);
			case FractalPackage.BINDING__ABSTRACT_COMPONENT:
				return basicSetAbstractComponent(null, msgs);
			case FractalPackage.BINDING__COMMENTS:
				return ((InternalEList<?>)getComments()).basicRemove(otherEnd, msgs);
			case FractalPackage.BINDING__ANY:
				return ((InternalEList<?>)getAny()).basicRemove(otherEnd, msgs);
			case FractalPackage.BINDING__ANY_ATTRIBUTES:
				return ((InternalEList<?>)getAnyAttributes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case FractalPackage.BINDING__ABSTRACT_COMPONENT:
				return eInternalContainer().eInverseRemove(this, FractalPackage.ABSTRACT_COMPONENT__BINDINGS, AbstractComponent.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FractalPackage.BINDING__CLIENT:
				return getClient();
			case FractalPackage.BINDING__SERVER:
				return getServer();
			case FractalPackage.BINDING__CLIENT_INTERFACE:
				if (resolve) return getClientInterface();
				return basicGetClientInterface();
			case FractalPackage.BINDING__SERVER_INTERFACE:
				if (resolve) return getServerInterface();
				return basicGetServerInterface();
			case FractalPackage.BINDING__CLIENT_AST:
				return getClientAST();
			case FractalPackage.BINDING__SERVER_AST:
				return getServerAST();
			case FractalPackage.BINDING__ABSTRACT_COMPONENT:
				return getAbstractComponent();
			case FractalPackage.BINDING__COMMENTS:
				return getComments();
			case FractalPackage.BINDING__MERGED_CLIENT:
				return getMergedClient();
			case FractalPackage.BINDING__MERGED_SERVER:
				return getMergedServer();
			case FractalPackage.BINDING__ANY:
				if (coreType) return getAny();
				return ((FeatureMap.Internal)getAny()).getWrapper();
			case FractalPackage.BINDING__ANY_ATTRIBUTES:
				if (coreType) return getAnyAttributes();
				return ((FeatureMap.Internal)getAnyAttributes()).getWrapper();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FractalPackage.BINDING__CLIENT:
				setClient((String)newValue);
				return;
			case FractalPackage.BINDING__SERVER:
				setServer((String)newValue);
				return;
			case FractalPackage.BINDING__CLIENT_INTERFACE:
				setClientInterface((Interface)newValue);
				return;
			case FractalPackage.BINDING__SERVER_INTERFACE:
				setServerInterface((Interface)newValue);
				return;
			case FractalPackage.BINDING__CLIENT_AST:
				setClientAST((Value)newValue);
				return;
			case FractalPackage.BINDING__SERVER_AST:
				setServerAST((Value)newValue);
				return;
			case FractalPackage.BINDING__ABSTRACT_COMPONENT:
				setAbstractComponent((AbstractComponent)newValue);
				return;
			case FractalPackage.BINDING__COMMENTS:
				getComments().clear();
				getComments().addAll((Collection<? extends Comment>)newValue);
				return;
			case FractalPackage.BINDING__MERGED_CLIENT:
				setMergedClient((String)newValue);
				return;
			case FractalPackage.BINDING__MERGED_SERVER:
				setMergedServer((String)newValue);
				return;
			case FractalPackage.BINDING__ANY:
				((FeatureMap.Internal)getAny()).set(newValue);
				return;
			case FractalPackage.BINDING__ANY_ATTRIBUTES:
				((FeatureMap.Internal)getAnyAttributes()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public void eUnset(int featureID) {
		switch (featureID) {
			case FractalPackage.BINDING__CLIENT:
				setClient(CLIENT_EDEFAULT);
				return;
			case FractalPackage.BINDING__SERVER:
				setServer(SERVER_EDEFAULT);
				return;
			case FractalPackage.BINDING__CLIENT_INTERFACE:
				setClientInterface((Interface)null);
				return;
			case FractalPackage.BINDING__SERVER_INTERFACE:
				setServerInterface((Interface)null);
				return;
			case FractalPackage.BINDING__CLIENT_AST:
				setClientAST((Value)null);
				return;
			case FractalPackage.BINDING__SERVER_AST:
				setServerAST((Value)null);
				return;
			case FractalPackage.BINDING__ABSTRACT_COMPONENT:
				setAbstractComponent((AbstractComponent)null);
				return;
			case FractalPackage.BINDING__COMMENTS:
				getComments().clear();
				return;
			case FractalPackage.BINDING__MERGED_CLIENT:
				setMergedClient(MERGED_CLIENT_EDEFAULT);
				return;
			case FractalPackage.BINDING__MERGED_SERVER:
				setMergedServer(MERGED_SERVER_EDEFAULT);
				return;
			case FractalPackage.BINDING__ANY:
				getAny().clear();
				return;
			case FractalPackage.BINDING__ANY_ATTRIBUTES:
				getAnyAttributes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FractalPackage.BINDING__CLIENT:
				return CLIENT_EDEFAULT == null ? client != null : !CLIENT_EDEFAULT.equals(client);
			case FractalPackage.BINDING__SERVER:
				return SERVER_EDEFAULT == null ? server != null : !SERVER_EDEFAULT.equals(server);
			case FractalPackage.BINDING__CLIENT_INTERFACE:
				return clientInterface != null;
			case FractalPackage.BINDING__SERVER_INTERFACE:
				return serverInterface != null;
			case FractalPackage.BINDING__CLIENT_AST:
				return clientAST != null;
			case FractalPackage.BINDING__SERVER_AST:
				return serverAST != null;
			case FractalPackage.BINDING__ABSTRACT_COMPONENT:
				return getAbstractComponent() != null;
			case FractalPackage.BINDING__COMMENTS:
				return comments != null && !comments.isEmpty();
			case FractalPackage.BINDING__MERGED_CLIENT:
				return MERGED_CLIENT_EDEFAULT == null ? mergedClient != null : !MERGED_CLIENT_EDEFAULT.equals(mergedClient);
			case FractalPackage.BINDING__MERGED_SERVER:
				return MERGED_SERVER_EDEFAULT == null ? mergedServer != null : !MERGED_SERVER_EDEFAULT.equals(mergedServer);
			case FractalPackage.BINDING__ANY:
				return any != null && !any.isEmpty();
			case FractalPackage.BINDING__ANY_ATTRIBUTES:
				return anyAttributes != null && !anyAttributes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (client: ");
		result.append(client);
		result.append(", server: ");
		result.append(server);
		result.append(", mergedClient: ");
		result.append(mergedClient);
		result.append(", mergedServer: ");
		result.append(mergedServer);
		result.append(", any: ");
		result.append(any);
		result.append(", anyAttributes: ");
		result.append(anyAttributes);
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	 
		
  	
	public Object getAdapter(Class adapter) {
		Object object = null;
		
		
		if(adapter == IHelper.class ||
			adapter == IBindingHelper.class ){
			object = HelperAdapterFactory.getInstance().adapt(this);
		}
		
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public IBindingHelper getHelper() {
		return (IBindingHelper)getAdapter(IBindingHelper.class);
	}

} //BindingImpl
