/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.ow2.fractal.f4e.fractal.adapter.helper.ICommentHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Comment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Comment#getLanguage <em>Language</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Comment#getText <em>Text</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Comment#getLanguageAST <em>Language AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Comment#getTextAST <em>Text AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Comment#getAny <em>Any</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Comment#getAnyAttributes <em>Any Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getComment()
 * @model
 * @generated
 */
public interface Comment extends EObject, IAdaptable, IHelperAdapter {
	/**
	 * Returns the value of the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Language</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Language</em>' attribute.
	 * @see #setLanguage(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getComment_Language()
	 * @model
	 * @generated
	 */
	String getLanguage();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Comment#getLanguage <em>Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Language</em>' attribute.
	 * @see #getLanguage()
	 * @generated
	 */
	void setLanguage(String value);

	/**
	 * Returns the value of the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Text</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Text</em>' attribute.
	 * @see #setText(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getComment_Text()
	 * @model
	 * @generated
	 */
	String getText();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Comment#getText <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Text</em>' attribute.
	 * @see #getText()
	 * @generated
	 */
	void setText(String value);

	/**
	 * Returns the value of the '<em><b>Language AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Language AST</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Language AST</em>' containment reference.
	 * @see #setLanguageAST(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getComment_LanguageAST()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	Value getLanguageAST();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Comment#getLanguageAST <em>Language AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Language AST</em>' containment reference.
	 * @see #getLanguageAST()
	 * @generated
	 */
	void setLanguageAST(Value value);

	/**
	 * Returns the value of the '<em><b>Text AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Text AST</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Text AST</em>' containment reference.
	 * @see #setTextAST(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getComment_TextAST()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	Value getTextAST();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Comment#getTextAST <em>Text AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Text AST</em>' containment reference.
	 * @see #getTextAST()
	 * @generated
	 */
	void setTextAST(Value value);

	/**
	 * Returns the value of the '<em><b>Any</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any</em>' attribute list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getComment_Any()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' wildcards='##any' name=':6' processing='lax'"
	 * @generated
	 */
	FeatureMap getAny();

	/**
	 * Returns the value of the '<em><b>Any Attributes</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any Attributes</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any Attributes</em>' attribute list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getComment_AnyAttributes()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='attributeWildcard' wildcards='##any' name=':12' processing='lax'"
	 * @generated
	 */
	FeatureMap getAnyAttributes();

	ICommentHelper getHelper();

} // Comment
