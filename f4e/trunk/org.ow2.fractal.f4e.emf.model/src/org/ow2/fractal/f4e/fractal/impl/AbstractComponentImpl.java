/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.AttributesController;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.Comment;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.Content;
import org.ow2.fractal.f4e.fractal.Controller;
import org.ow2.fractal.f4e.fractal.Coordinates;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.Logger;
import org.ow2.fractal.f4e.fractal.TemplateController;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.VirtualNode;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IAbstractComponentHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelper;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getMergedName <em>Merged Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getNameAST <em>Name AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getExtendsAST <em>Extends AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getComments <em>Comments</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getInterfaces <em>Interfaces</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getMergedInterfaces <em>Merged Interfaces</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getSubComponents <em>Sub Components</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getMergedSubComponents <em>Merged Sub Components</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getContent <em>Content</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getMergedContent <em>Merged Content</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getAttributesController <em>Attributes Controller</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getMergedAttributesController <em>Merged Attributes Controller</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getBindings <em>Bindings</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getMergedBindings <em>Merged Bindings</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getController <em>Controller</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getMergedController <em>Merged Controller</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getTemplateController <em>Template Controller</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getMergedTemplateController <em>Merged Template Controller</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getLogger <em>Logger</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getMergedLogger <em>Merged Logger</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getVirtualNode <em>Virtual Node</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getMergedVirtualNode <em>Merged Virtual Node</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getCoordinates <em>Coordinates</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getAny <em>Any</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl#getAnyAttributes <em>Any Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AbstractComponentImpl extends EObjectImpl implements AbstractComponent {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getMergedName() <em>Merged Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedName()
	 * @generated
	 * @ordered
	 */
	protected static final String MERGED_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMergedName() <em>Merged Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedName()
	 * @generated
	 * @ordered
	 */
	protected String mergedName = MERGED_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getNameAST() <em>Name AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameAST()
	 * @generated
	 * @ordered
	 */
	protected Value nameAST;

	/**
	 * The cached value of the '{@link #getExtendsAST() <em>Extends AST</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtendsAST()
	 * @generated
	 * @ordered
	 */
	protected EList<DefinitionCall> extendsAST;

	/**
	 * The cached value of the '{@link #getComments() <em>Comments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComments()
	 * @generated
	 * @ordered
	 */
	protected EList<Comment> comments;

	/**
	 * The cached value of the '{@link #getInterfaces() <em>Interfaces</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaces()
	 * @generated
	 * @ordered
	 */
	protected EList<Interface> interfaces;

	/**
	 * The cached value of the '{@link #getMergedInterfaces() <em>Merged Interfaces</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedInterfaces()
	 * @generated
	 * @ordered
	 */
	protected EList<Interface> mergedInterfaces;

	/**
	 * The cached value of the '{@link #getSubComponents() <em>Sub Components</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubComponents()
	 * @generated
	 * @ordered
	 */
	protected EList<Component> subComponents;

	/**
	 * The cached value of the '{@link #getMergedSubComponents() <em>Merged Sub Components</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedSubComponents()
	 * @generated
	 * @ordered
	 */
	protected EList<Component> mergedSubComponents;

	/**
	 * The cached value of the '{@link #getContent() <em>Content</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected Content content;

	/**
	 * The cached value of the '{@link #getMergedContent() <em>Merged Content</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedContent()
	 * @generated
	 * @ordered
	 */
	protected Content mergedContent;

	/**
	 * The cached value of the '{@link #getAttributesController() <em>Attributes Controller</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributesController()
	 * @generated
	 * @ordered
	 */
	protected AttributesController attributesController;

	/**
	 * The cached value of the '{@link #getMergedAttributesController() <em>Merged Attributes Controller</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedAttributesController()
	 * @generated
	 * @ordered
	 */
	protected AttributesController mergedAttributesController;

	/**
	 * The cached value of the '{@link #getBindings() <em>Bindings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBindings()
	 * @generated
	 * @ordered
	 */
	protected EList<Binding> bindings;

	/**
	 * The cached value of the '{@link #getMergedBindings() <em>Merged Bindings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedBindings()
	 * @generated
	 * @ordered
	 */
	protected EList<Binding> mergedBindings;

	/**
	 * The cached value of the '{@link #getController() <em>Controller</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getController()
	 * @generated
	 * @ordered
	 */
	protected Controller controller;

	/**
	 * The cached value of the '{@link #getMergedController() <em>Merged Controller</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedController()
	 * @generated
	 * @ordered
	 */
	protected Controller mergedController;

	/**
	 * The cached value of the '{@link #getTemplateController() <em>Template Controller</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemplateController()
	 * @generated
	 * @ordered
	 */
	protected TemplateController templateController;

	/**
	 * The cached value of the '{@link #getMergedTemplateController() <em>Merged Template Controller</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedTemplateController()
	 * @generated
	 * @ordered
	 */
	protected TemplateController mergedTemplateController;

	/**
	 * The cached value of the '{@link #getLogger() <em>Logger</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogger()
	 * @generated
	 * @ordered
	 */
	protected Logger logger;

	/**
	 * The cached value of the '{@link #getMergedLogger() <em>Merged Logger</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedLogger()
	 * @generated
	 * @ordered
	 */
	protected Logger mergedLogger;

	/**
	 * The cached value of the '{@link #getVirtualNode() <em>Virtual Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVirtualNode()
	 * @generated
	 * @ordered
	 */
	protected VirtualNode virtualNode;

	/**
	 * The cached value of the '{@link #getMergedVirtualNode() <em>Merged Virtual Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedVirtualNode()
	 * @generated
	 * @ordered
	 */
	protected VirtualNode mergedVirtualNode;

	/**
	 * The cached value of the '{@link #getCoordinates() <em>Coordinates</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoordinates()
	 * @generated
	 * @ordered
	 */
	protected EList<Coordinates> coordinates;

	/**
	 * The cached value of the '{@link #getAny() <em>Any</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAny()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap any;

	/**
	 * The cached value of the '{@link #getAnyAttributes() <em>Any Attributes</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnyAttributes()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap anyAttributes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return FractalPackage.Literals.ABSTRACT_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMergedName() {
		return mergedName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergedName(String newMergedName) {
		String oldMergedName = mergedName;
		mergedName = newMergedName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__MERGED_NAME, oldMergedName, mergedName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getNameAST() {
		return nameAST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNameAST(Value newNameAST, NotificationChain msgs) {
		Value oldNameAST = nameAST;
		nameAST = newNameAST;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__NAME_AST, oldNameAST, newNameAST);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNameAST(Value newNameAST) {
		if (newNameAST != nameAST) {
			NotificationChain msgs = null;
			if (nameAST != null)
				msgs = ((InternalEObject)nameAST).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__NAME_AST, null, msgs);
			if (newNameAST != null)
				msgs = ((InternalEObject)newNameAST).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__NAME_AST, null, msgs);
			msgs = basicSetNameAST(newNameAST, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__NAME_AST, newNameAST, newNameAST));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefinitionCall> getExtendsAST() {
		if (extendsAST == null) {
			extendsAST = new EObjectContainmentWithInverseEList<DefinitionCall>(DefinitionCall.class, this, FractalPackage.ABSTRACT_COMPONENT__EXTENDS_AST, FractalPackage.DEFINITION_CALL__ABSTRACT_COMPONENT);
		}
		return extendsAST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Comment> getComments() {
		if (comments == null) {
			comments = new EObjectContainmentEList<Comment>(Comment.class, this, FractalPackage.ABSTRACT_COMPONENT__COMMENTS);
		}
		return comments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Interface> getInterfaces() {
		if (interfaces == null) {
			interfaces = new EObjectContainmentWithInverseEList<Interface>(Interface.class, this, FractalPackage.ABSTRACT_COMPONENT__INTERFACES, FractalPackage.INTERFACE__ABSTRACT_COMPONENT);
		}
		return interfaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Interface> getMergedInterfaces() {
		if (mergedInterfaces == null) {
			mergedInterfaces = new EObjectContainmentEList<Interface>(Interface.class, this, FractalPackage.ABSTRACT_COMPONENT__MERGED_INTERFACES);
		}
		return mergedInterfaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Component> getSubComponents() {
		if (subComponents == null) {
			subComponents = new EObjectContainmentWithInverseEList<Component>(Component.class, this, FractalPackage.ABSTRACT_COMPONENT__SUB_COMPONENTS, FractalPackage.COMPONENT__PARENT);
		}
		return subComponents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Component> getMergedSubComponents() {
		if (mergedSubComponents == null) {
			mergedSubComponents = new EObjectContainmentEList<Component>(Component.class, this, FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS);
		}
		return mergedSubComponents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Content getContent() {
		return content;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContent(Content newContent, NotificationChain msgs) {
		Content oldContent = content;
		content = newContent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__CONTENT, oldContent, newContent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContent(Content newContent) {
		if (newContent != content) {
			NotificationChain msgs = null;
			if (content != null)
				msgs = ((InternalEObject)content).eInverseRemove(this, FractalPackage.CONTENT__ABSTRACT_COMPONENT, Content.class, msgs);
			if (newContent != null)
				msgs = ((InternalEObject)newContent).eInverseAdd(this, FractalPackage.CONTENT__ABSTRACT_COMPONENT, Content.class, msgs);
			msgs = basicSetContent(newContent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__CONTENT, newContent, newContent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Content getMergedContent() {
		return mergedContent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMergedContent(Content newMergedContent, NotificationChain msgs) {
		Content oldMergedContent = mergedContent;
		mergedContent = newMergedContent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTENT, oldMergedContent, newMergedContent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergedContent(Content newMergedContent) {
		if (newMergedContent != mergedContent) {
			NotificationChain msgs = null;
			if (mergedContent != null)
				msgs = ((InternalEObject)mergedContent).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTENT, null, msgs);
			if (newMergedContent != null)
				msgs = ((InternalEObject)newMergedContent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTENT, null, msgs);
			msgs = basicSetMergedContent(newMergedContent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTENT, newMergedContent, newMergedContent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributesController getAttributesController() {
		return attributesController;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttributesController(AttributesController newAttributesController, NotificationChain msgs) {
		AttributesController oldAttributesController = attributesController;
		attributesController = newAttributesController;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER, oldAttributesController, newAttributesController);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributesController(AttributesController newAttributesController) {
		if (newAttributesController != attributesController) {
			NotificationChain msgs = null;
			if (attributesController != null)
				msgs = ((InternalEObject)attributesController).eInverseRemove(this, FractalPackage.ATTRIBUTES_CONTROLLER__ABSTRACT_COMPONENT, AttributesController.class, msgs);
			if (newAttributesController != null)
				msgs = ((InternalEObject)newAttributesController).eInverseAdd(this, FractalPackage.ATTRIBUTES_CONTROLLER__ABSTRACT_COMPONENT, AttributesController.class, msgs);
			msgs = basicSetAttributesController(newAttributesController, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER, newAttributesController, newAttributesController));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributesController getMergedAttributesController() {
		return mergedAttributesController;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMergedAttributesController(AttributesController newMergedAttributesController, NotificationChain msgs) {
		AttributesController oldMergedAttributesController = mergedAttributesController;
		mergedAttributesController = newMergedAttributesController;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER, oldMergedAttributesController, newMergedAttributesController);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergedAttributesController(AttributesController newMergedAttributesController) {
		if (newMergedAttributesController != mergedAttributesController) {
			NotificationChain msgs = null;
			if (mergedAttributesController != null)
				msgs = ((InternalEObject)mergedAttributesController).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER, null, msgs);
			if (newMergedAttributesController != null)
				msgs = ((InternalEObject)newMergedAttributesController).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER, null, msgs);
			msgs = basicSetMergedAttributesController(newMergedAttributesController, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER, newMergedAttributesController, newMergedAttributesController));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Binding> getBindings() {
		if (bindings == null) {
			bindings = new EObjectContainmentWithInverseEList<Binding>(Binding.class, this, FractalPackage.ABSTRACT_COMPONENT__BINDINGS, FractalPackage.BINDING__ABSTRACT_COMPONENT);
		}
		return bindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Binding> getMergedBindings() {
		if (mergedBindings == null) {
			mergedBindings = new EObjectContainmentEList<Binding>(Binding.class, this, FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS);
		}
		return mergedBindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Controller getController() {
		return controller;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetController(Controller newController, NotificationChain msgs) {
		Controller oldController = controller;
		controller = newController;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__CONTROLLER, oldController, newController);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setController(Controller newController) {
		if (newController != controller) {
			NotificationChain msgs = null;
			if (controller != null)
				msgs = ((InternalEObject)controller).eInverseRemove(this, FractalPackage.CONTROLLER__ABSTRACT_COMPONENT, Controller.class, msgs);
			if (newController != null)
				msgs = ((InternalEObject)newController).eInverseAdd(this, FractalPackage.CONTROLLER__ABSTRACT_COMPONENT, Controller.class, msgs);
			msgs = basicSetController(newController, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__CONTROLLER, newController, newController));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Controller getMergedController() {
		return mergedController;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMergedController(Controller newMergedController, NotificationChain msgs) {
		Controller oldMergedController = mergedController;
		mergedController = newMergedController;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTROLLER, oldMergedController, newMergedController);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergedController(Controller newMergedController) {
		if (newMergedController != mergedController) {
			NotificationChain msgs = null;
			if (mergedController != null)
				msgs = ((InternalEObject)mergedController).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTROLLER, null, msgs);
			if (newMergedController != null)
				msgs = ((InternalEObject)newMergedController).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTROLLER, null, msgs);
			msgs = basicSetMergedController(newMergedController, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTROLLER, newMergedController, newMergedController));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplateController getTemplateController() {
		return templateController;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTemplateController(TemplateController newTemplateController, NotificationChain msgs) {
		TemplateController oldTemplateController = templateController;
		templateController = newTemplateController;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__TEMPLATE_CONTROLLER, oldTemplateController, newTemplateController);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTemplateController(TemplateController newTemplateController) {
		if (newTemplateController != templateController) {
			NotificationChain msgs = null;
			if (templateController != null)
				msgs = ((InternalEObject)templateController).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__TEMPLATE_CONTROLLER, null, msgs);
			if (newTemplateController != null)
				msgs = ((InternalEObject)newTemplateController).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__TEMPLATE_CONTROLLER, null, msgs);
			msgs = basicSetTemplateController(newTemplateController, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__TEMPLATE_CONTROLLER, newTemplateController, newTemplateController));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplateController getMergedTemplateController() {
		return mergedTemplateController;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMergedTemplateController(TemplateController newMergedTemplateController, NotificationChain msgs) {
		TemplateController oldMergedTemplateController = mergedTemplateController;
		mergedTemplateController = newMergedTemplateController;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__MERGED_TEMPLATE_CONTROLLER, oldMergedTemplateController, newMergedTemplateController);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergedTemplateController(TemplateController newMergedTemplateController) {
		if (newMergedTemplateController != mergedTemplateController) {
			NotificationChain msgs = null;
			if (mergedTemplateController != null)
				msgs = ((InternalEObject)mergedTemplateController).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__MERGED_TEMPLATE_CONTROLLER, null, msgs);
			if (newMergedTemplateController != null)
				msgs = ((InternalEObject)newMergedTemplateController).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__MERGED_TEMPLATE_CONTROLLER, null, msgs);
			msgs = basicSetMergedTemplateController(newMergedTemplateController, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__MERGED_TEMPLATE_CONTROLLER, newMergedTemplateController, newMergedTemplateController));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLogger(Logger newLogger, NotificationChain msgs) {
		Logger oldLogger = logger;
		logger = newLogger;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__LOGGER, oldLogger, newLogger);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLogger(Logger newLogger) {
		if (newLogger != logger) {
			NotificationChain msgs = null;
			if (logger != null)
				msgs = ((InternalEObject)logger).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__LOGGER, null, msgs);
			if (newLogger != null)
				msgs = ((InternalEObject)newLogger).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__LOGGER, null, msgs);
			msgs = basicSetLogger(newLogger, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__LOGGER, newLogger, newLogger));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Logger getMergedLogger() {
		return mergedLogger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMergedLogger(Logger newMergedLogger, NotificationChain msgs) {
		Logger oldMergedLogger = mergedLogger;
		mergedLogger = newMergedLogger;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__MERGED_LOGGER, oldMergedLogger, newMergedLogger);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergedLogger(Logger newMergedLogger) {
		if (newMergedLogger != mergedLogger) {
			NotificationChain msgs = null;
			if (mergedLogger != null)
				msgs = ((InternalEObject)mergedLogger).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__MERGED_LOGGER, null, msgs);
			if (newMergedLogger != null)
				msgs = ((InternalEObject)newMergedLogger).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__MERGED_LOGGER, null, msgs);
			msgs = basicSetMergedLogger(newMergedLogger, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__MERGED_LOGGER, newMergedLogger, newMergedLogger));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VirtualNode getVirtualNode() {
		return virtualNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVirtualNode(VirtualNode newVirtualNode, NotificationChain msgs) {
		VirtualNode oldVirtualNode = virtualNode;
		virtualNode = newVirtualNode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__VIRTUAL_NODE, oldVirtualNode, newVirtualNode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVirtualNode(VirtualNode newVirtualNode) {
		if (newVirtualNode != virtualNode) {
			NotificationChain msgs = null;
			if (virtualNode != null)
				msgs = ((InternalEObject)virtualNode).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__VIRTUAL_NODE, null, msgs);
			if (newVirtualNode != null)
				msgs = ((InternalEObject)newVirtualNode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__VIRTUAL_NODE, null, msgs);
			msgs = basicSetVirtualNode(newVirtualNode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__VIRTUAL_NODE, newVirtualNode, newVirtualNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VirtualNode getMergedVirtualNode() {
		return mergedVirtualNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMergedVirtualNode(VirtualNode newMergedVirtualNode, NotificationChain msgs) {
		VirtualNode oldMergedVirtualNode = mergedVirtualNode;
		mergedVirtualNode = newMergedVirtualNode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__MERGED_VIRTUAL_NODE, oldMergedVirtualNode, newMergedVirtualNode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergedVirtualNode(VirtualNode newMergedVirtualNode) {
		if (newMergedVirtualNode != mergedVirtualNode) {
			NotificationChain msgs = null;
			if (mergedVirtualNode != null)
				msgs = ((InternalEObject)mergedVirtualNode).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__MERGED_VIRTUAL_NODE, null, msgs);
			if (newMergedVirtualNode != null)
				msgs = ((InternalEObject)newMergedVirtualNode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__MERGED_VIRTUAL_NODE, null, msgs);
			msgs = basicSetMergedVirtualNode(newMergedVirtualNode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ABSTRACT_COMPONENT__MERGED_VIRTUAL_NODE, newMergedVirtualNode, newMergedVirtualNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Coordinates> getCoordinates() {
		if (coordinates == null) {
			coordinates = new EObjectContainmentEList<Coordinates>(Coordinates.class, this, FractalPackage.ABSTRACT_COMPONENT__COORDINATES);
		}
		return coordinates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getAny() {
		if (any == null) {
			any = new BasicFeatureMap(this, FractalPackage.ABSTRACT_COMPONENT__ANY);
		}
		return any;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getAnyAttributes() {
		if (anyAttributes == null) {
			anyAttributes = new BasicFeatureMap(this, FractalPackage.ABSTRACT_COMPONENT__ANY_ATTRIBUTES);
		}
		return anyAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.ABSTRACT_COMPONENT__EXTENDS_AST:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getExtendsAST()).basicAdd(otherEnd, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__INTERFACES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getInterfaces()).basicAdd(otherEnd, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__SUB_COMPONENTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSubComponents()).basicAdd(otherEnd, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__CONTENT:
				if (content != null)
					msgs = ((InternalEObject)content).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__CONTENT, null, msgs);
				return basicSetContent((Content)otherEnd, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER:
				if (attributesController != null)
					msgs = ((InternalEObject)attributesController).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER, null, msgs);
				return basicSetAttributesController((AttributesController)otherEnd, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__BINDINGS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getBindings()).basicAdd(otherEnd, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__CONTROLLER:
				if (controller != null)
					msgs = ((InternalEObject)controller).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ABSTRACT_COMPONENT__CONTROLLER, null, msgs);
				return basicSetController((Controller)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.ABSTRACT_COMPONENT__NAME_AST:
				return basicSetNameAST(null, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__EXTENDS_AST:
				return ((InternalEList<?>)getExtendsAST()).basicRemove(otherEnd, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__COMMENTS:
				return ((InternalEList<?>)getComments()).basicRemove(otherEnd, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__INTERFACES:
				return ((InternalEList<?>)getInterfaces()).basicRemove(otherEnd, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_INTERFACES:
				return ((InternalEList<?>)getMergedInterfaces()).basicRemove(otherEnd, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__SUB_COMPONENTS:
				return ((InternalEList<?>)getSubComponents()).basicRemove(otherEnd, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS:
				return ((InternalEList<?>)getMergedSubComponents()).basicRemove(otherEnd, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__CONTENT:
				return basicSetContent(null, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTENT:
				return basicSetMergedContent(null, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER:
				return basicSetAttributesController(null, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER:
				return basicSetMergedAttributesController(null, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__BINDINGS:
				return ((InternalEList<?>)getBindings()).basicRemove(otherEnd, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS:
				return ((InternalEList<?>)getMergedBindings()).basicRemove(otherEnd, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__CONTROLLER:
				return basicSetController(null, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTROLLER:
				return basicSetMergedController(null, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__TEMPLATE_CONTROLLER:
				return basicSetTemplateController(null, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_TEMPLATE_CONTROLLER:
				return basicSetMergedTemplateController(null, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__LOGGER:
				return basicSetLogger(null, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_LOGGER:
				return basicSetMergedLogger(null, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__VIRTUAL_NODE:
				return basicSetVirtualNode(null, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_VIRTUAL_NODE:
				return basicSetMergedVirtualNode(null, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__COORDINATES:
				return ((InternalEList<?>)getCoordinates()).basicRemove(otherEnd, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__ANY:
				return ((InternalEList<?>)getAny()).basicRemove(otherEnd, msgs);
			case FractalPackage.ABSTRACT_COMPONENT__ANY_ATTRIBUTES:
				return ((InternalEList<?>)getAnyAttributes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FractalPackage.ABSTRACT_COMPONENT__NAME:
				return getName();
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_NAME:
				return getMergedName();
			case FractalPackage.ABSTRACT_COMPONENT__NAME_AST:
				return getNameAST();
			case FractalPackage.ABSTRACT_COMPONENT__EXTENDS_AST:
				return getExtendsAST();
			case FractalPackage.ABSTRACT_COMPONENT__COMMENTS:
				return getComments();
			case FractalPackage.ABSTRACT_COMPONENT__INTERFACES:
				return getInterfaces();
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_INTERFACES:
				return getMergedInterfaces();
			case FractalPackage.ABSTRACT_COMPONENT__SUB_COMPONENTS:
				return getSubComponents();
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS:
				return getMergedSubComponents();
			case FractalPackage.ABSTRACT_COMPONENT__CONTENT:
				return getContent();
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTENT:
				return getMergedContent();
			case FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER:
				return getAttributesController();
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER:
				return getMergedAttributesController();
			case FractalPackage.ABSTRACT_COMPONENT__BINDINGS:
				return getBindings();
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS:
				return getMergedBindings();
			case FractalPackage.ABSTRACT_COMPONENT__CONTROLLER:
				return getController();
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTROLLER:
				return getMergedController();
			case FractalPackage.ABSTRACT_COMPONENT__TEMPLATE_CONTROLLER:
				return getTemplateController();
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_TEMPLATE_CONTROLLER:
				return getMergedTemplateController();
			case FractalPackage.ABSTRACT_COMPONENT__LOGGER:
				return getLogger();
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_LOGGER:
				return getMergedLogger();
			case FractalPackage.ABSTRACT_COMPONENT__VIRTUAL_NODE:
				return getVirtualNode();
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_VIRTUAL_NODE:
				return getMergedVirtualNode();
			case FractalPackage.ABSTRACT_COMPONENT__COORDINATES:
				return getCoordinates();
			case FractalPackage.ABSTRACT_COMPONENT__ANY:
				if (coreType) return getAny();
				return ((FeatureMap.Internal)getAny()).getWrapper();
			case FractalPackage.ABSTRACT_COMPONENT__ANY_ATTRIBUTES:
				if (coreType) return getAnyAttributes();
				return ((FeatureMap.Internal)getAnyAttributes()).getWrapper();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FractalPackage.ABSTRACT_COMPONENT__NAME:
				setName((String)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_NAME:
				setMergedName((String)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__NAME_AST:
				setNameAST((Value)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__EXTENDS_AST:
				getExtendsAST().clear();
				getExtendsAST().addAll((Collection<? extends DefinitionCall>)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__COMMENTS:
				getComments().clear();
				getComments().addAll((Collection<? extends Comment>)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__INTERFACES:
				getInterfaces().clear();
				getInterfaces().addAll((Collection<? extends Interface>)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_INTERFACES:
				getMergedInterfaces().clear();
				getMergedInterfaces().addAll((Collection<? extends Interface>)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__SUB_COMPONENTS:
				getSubComponents().clear();
				getSubComponents().addAll((Collection<? extends Component>)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS:
				getMergedSubComponents().clear();
				getMergedSubComponents().addAll((Collection<? extends Component>)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__CONTENT:
				setContent((Content)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTENT:
				setMergedContent((Content)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER:
				setAttributesController((AttributesController)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER:
				setMergedAttributesController((AttributesController)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__BINDINGS:
				getBindings().clear();
				getBindings().addAll((Collection<? extends Binding>)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS:
				getMergedBindings().clear();
				getMergedBindings().addAll((Collection<? extends Binding>)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__CONTROLLER:
				setController((Controller)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTROLLER:
				setMergedController((Controller)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__TEMPLATE_CONTROLLER:
				setTemplateController((TemplateController)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_TEMPLATE_CONTROLLER:
				setMergedTemplateController((TemplateController)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__LOGGER:
				setLogger((Logger)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_LOGGER:
				setMergedLogger((Logger)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__VIRTUAL_NODE:
				setVirtualNode((VirtualNode)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_VIRTUAL_NODE:
				setMergedVirtualNode((VirtualNode)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__COORDINATES:
				getCoordinates().clear();
				getCoordinates().addAll((Collection<? extends Coordinates>)newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__ANY:
				((FeatureMap.Internal)getAny()).set(newValue);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__ANY_ATTRIBUTES:
				((FeatureMap.Internal)getAnyAttributes()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public void eUnset(int featureID) {
		switch (featureID) {
			case FractalPackage.ABSTRACT_COMPONENT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_NAME:
				setMergedName(MERGED_NAME_EDEFAULT);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__NAME_AST:
				setNameAST((Value)null);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__EXTENDS_AST:
				getExtendsAST().clear();
				return;
			case FractalPackage.ABSTRACT_COMPONENT__COMMENTS:
				getComments().clear();
				return;
			case FractalPackage.ABSTRACT_COMPONENT__INTERFACES:
				getInterfaces().clear();
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_INTERFACES:
				getMergedInterfaces().clear();
				return;
			case FractalPackage.ABSTRACT_COMPONENT__SUB_COMPONENTS:
				getSubComponents().clear();
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS:
				getMergedSubComponents().clear();
				return;
			case FractalPackage.ABSTRACT_COMPONENT__CONTENT:
				setContent((Content)null);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTENT:
				setMergedContent((Content)null);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER:
				setAttributesController((AttributesController)null);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER:
				setMergedAttributesController((AttributesController)null);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__BINDINGS:
				getBindings().clear();
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS:
				getMergedBindings().clear();
				return;
			case FractalPackage.ABSTRACT_COMPONENT__CONTROLLER:
				setController((Controller)null);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTROLLER:
				setMergedController((Controller)null);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__TEMPLATE_CONTROLLER:
				setTemplateController((TemplateController)null);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_TEMPLATE_CONTROLLER:
				setMergedTemplateController((TemplateController)null);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__LOGGER:
				setLogger((Logger)null);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_LOGGER:
				setMergedLogger((Logger)null);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__VIRTUAL_NODE:
				setVirtualNode((VirtualNode)null);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_VIRTUAL_NODE:
				setMergedVirtualNode((VirtualNode)null);
				return;
			case FractalPackage.ABSTRACT_COMPONENT__COORDINATES:
				getCoordinates().clear();
				return;
			case FractalPackage.ABSTRACT_COMPONENT__ANY:
				getAny().clear();
				return;
			case FractalPackage.ABSTRACT_COMPONENT__ANY_ATTRIBUTES:
				getAnyAttributes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FractalPackage.ABSTRACT_COMPONENT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_NAME:
				return MERGED_NAME_EDEFAULT == null ? mergedName != null : !MERGED_NAME_EDEFAULT.equals(mergedName);
			case FractalPackage.ABSTRACT_COMPONENT__NAME_AST:
				return nameAST != null;
			case FractalPackage.ABSTRACT_COMPONENT__EXTENDS_AST:
				return extendsAST != null && !extendsAST.isEmpty();
			case FractalPackage.ABSTRACT_COMPONENT__COMMENTS:
				return comments != null && !comments.isEmpty();
			case FractalPackage.ABSTRACT_COMPONENT__INTERFACES:
				return interfaces != null && !interfaces.isEmpty();
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_INTERFACES:
				return mergedInterfaces != null && !mergedInterfaces.isEmpty();
			case FractalPackage.ABSTRACT_COMPONENT__SUB_COMPONENTS:
				return subComponents != null && !subComponents.isEmpty();
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS:
				return mergedSubComponents != null && !mergedSubComponents.isEmpty();
			case FractalPackage.ABSTRACT_COMPONENT__CONTENT:
				return content != null;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTENT:
				return mergedContent != null;
			case FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER:
				return attributesController != null;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER:
				return mergedAttributesController != null;
			case FractalPackage.ABSTRACT_COMPONENT__BINDINGS:
				return bindings != null && !bindings.isEmpty();
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS:
				return mergedBindings != null && !mergedBindings.isEmpty();
			case FractalPackage.ABSTRACT_COMPONENT__CONTROLLER:
				return controller != null;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTROLLER:
				return mergedController != null;
			case FractalPackage.ABSTRACT_COMPONENT__TEMPLATE_CONTROLLER:
				return templateController != null;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_TEMPLATE_CONTROLLER:
				return mergedTemplateController != null;
			case FractalPackage.ABSTRACT_COMPONENT__LOGGER:
				return logger != null;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_LOGGER:
				return mergedLogger != null;
			case FractalPackage.ABSTRACT_COMPONENT__VIRTUAL_NODE:
				return virtualNode != null;
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_VIRTUAL_NODE:
				return mergedVirtualNode != null;
			case FractalPackage.ABSTRACT_COMPONENT__COORDINATES:
				return coordinates != null && !coordinates.isEmpty();
			case FractalPackage.ABSTRACT_COMPONENT__ANY:
				return any != null && !any.isEmpty();
			case FractalPackage.ABSTRACT_COMPONENT__ANY_ATTRIBUTES:
				return anyAttributes != null && !anyAttributes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", mergedName: ");
		result.append(mergedName);
		result.append(", any: ");
		result.append(any);
		result.append(", anyAttributes: ");
		result.append(anyAttributes);
		result.append(')');
		return result.toString();
	}



	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	 
		
  	
	@SuppressWarnings("unchecked")
	public Object getAdapter(Class adapter) {
		Object object = null;
		
		
		if(adapter == IHelper.class ||
			adapter == IAbstractComponentHelper.class ){
			object = HelperAdapterFactory.getInstance().adapt(this);
		}
		
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IAbstractComponentHelper<?> getHelper() {
		return (IAbstractComponentHelper<?>)getAdapter(IAbstractComponentHelper.class);
	}

} //AbstractComponentImpl
