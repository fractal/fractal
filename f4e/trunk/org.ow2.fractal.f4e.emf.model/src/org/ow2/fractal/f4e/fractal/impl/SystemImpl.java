/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Parameter;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.ISystemHelper;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>System</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.SystemImpl#getDefinitionCalls <em>Definition Calls</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.SystemImpl#getPamameters <em>Pamameters</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.SystemImpl#getDefinitions <em>Definitions</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SystemImpl extends EObjectImpl implements org.ow2.fractal.f4e.fractal.System {
	/**
	 * The cached value of the '{@link #getDefinitionCalls() <em>Definition Calls</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefinitionCalls()
	 * @generated
	 * @ordered
	 */
	protected EList<DefinitionCall> definitionCalls;

	/**
	 * The cached value of the '{@link #getPamameters() <em>Pamameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPamameters()
	 * @generated
	 * @ordered
	 */
	protected EList<Parameter> pamameters;

	/**
	 * The cached value of the '{@link #getDefinitions() <em>Definitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefinitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Definition> definitions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	protected EClass eStaticClass() {
		return FractalPackage.Literals.SYSTEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefinitionCall> getDefinitionCalls() {
		if (definitionCalls == null) {
			definitionCalls = new EObjectContainmentEList<DefinitionCall>(DefinitionCall.class, this, FractalPackage.SYSTEM__DEFINITION_CALLS);
		}
		return definitionCalls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Parameter> getPamameters() {
		if (pamameters == null) {
			pamameters = new EObjectContainmentEList<Parameter>(Parameter.class, this, FractalPackage.SYSTEM__PAMAMETERS);
		}
		return pamameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Definition> getDefinitions() {
		if (definitions == null) {
			definitions = new EObjectContainmentWithInverseEList<Definition>(Definition.class, this, FractalPackage.SYSTEM__DEFINITIONS, FractalPackage.DEFINITION__SYSTEM);
		}
		return definitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.SYSTEM__DEFINITIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getDefinitions()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.SYSTEM__DEFINITION_CALLS:
				return ((InternalEList<?>)getDefinitionCalls()).basicRemove(otherEnd, msgs);
			case FractalPackage.SYSTEM__PAMAMETERS:
				return ((InternalEList<?>)getPamameters()).basicRemove(otherEnd, msgs);
			case FractalPackage.SYSTEM__DEFINITIONS:
				return ((InternalEList<?>)getDefinitions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FractalPackage.SYSTEM__DEFINITION_CALLS:
				return getDefinitionCalls();
			case FractalPackage.SYSTEM__PAMAMETERS:
				return getPamameters();
			case FractalPackage.SYSTEM__DEFINITIONS:
				return getDefinitions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FractalPackage.SYSTEM__DEFINITION_CALLS:
				getDefinitionCalls().clear();
				getDefinitionCalls().addAll((Collection<? extends DefinitionCall>)newValue);
				return;
			case FractalPackage.SYSTEM__PAMAMETERS:
				getPamameters().clear();
				getPamameters().addAll((Collection<? extends Parameter>)newValue);
				return;
			case FractalPackage.SYSTEM__DEFINITIONS:
				getDefinitions().clear();
				getDefinitions().addAll((Collection<? extends Definition>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public void eUnset(int featureID) {
		switch (featureID) {
			case FractalPackage.SYSTEM__DEFINITION_CALLS:
				getDefinitionCalls().clear();
				return;
			case FractalPackage.SYSTEM__PAMAMETERS:
				getPamameters().clear();
				return;
			case FractalPackage.SYSTEM__DEFINITIONS:
				getDefinitions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FractalPackage.SYSTEM__DEFINITION_CALLS:
				return definitionCalls != null && !definitionCalls.isEmpty();
			case FractalPackage.SYSTEM__PAMAMETERS:
				return pamameters != null && !pamameters.isEmpty();
			case FractalPackage.SYSTEM__DEFINITIONS:
				return definitions != null && !definitions.isEmpty();
		}
		return super.eIsSet(featureID);
	}



	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	 
		
  	
	public Object getAdapter(Class adapter) {
		Object object = null;
		
		
		if(adapter == IHelper.class ||
			adapter == ISystemHelper.class ){
			object = HelperAdapterFactory.getInstance().adapt(this);
		}
		
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISystemHelper getHelper() {
		return (ISystemHelper)getAdapter(ISystemHelper.class);
	}

} //SystemImpl
