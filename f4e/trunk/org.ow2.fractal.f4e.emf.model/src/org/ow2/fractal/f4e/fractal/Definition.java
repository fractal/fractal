/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.EList;
import org.ow2.fractal.f4e.fractal.adapter.helper.IDefinitionHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Definition#getExtends <em>Extends</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Definition#getArguments <em>Arguments</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Definition#getArgumentsAST <em>Arguments AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Definition#getSystem <em>System</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getDefinition()
 * @model
 * @generated
 */
public interface Definition extends AbstractComponent, IAdaptable, IHelperAdapter {
	/**
	 * Returns the value of the '<em><b>Extends</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extends</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extends</em>' attribute.
	 * @see #setExtends(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getDefinition_Extends()
	 * @model
	 * @generated
	 */
	String getExtends();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Definition#getExtends <em>Extends</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extends</em>' attribute.
	 * @see #getExtends()
	 * @generated
	 */
	void setExtends(String value);

	/**
	 * Returns the value of the '<em><b>Arguments</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arguments</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arguments</em>' attribute.
	 * @see #setArguments(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getDefinition_Arguments()
	 * @model default=""
	 * @generated
	 */
	String getArguments();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Definition#getArguments <em>Arguments</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Arguments</em>' attribute.
	 * @see #getArguments()
	 * @generated
	 */
	void setArguments(String value);

	/**
	 * Returns the value of the '<em><b>Arguments AST</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.FormalParameter}.
	 * It is bidirectional and its opposite is '{@link org.ow2.fractal.f4e.fractal.FormalParameter#getDefinition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arguments AST</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arguments AST</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getDefinition_ArgumentsAST()
	 * @see org.ow2.fractal.f4e.fractal.FormalParameter#getDefinition
	 * @model opposite="definition" containment="true"
	 * @generated
	 */
	EList<FormalParameter> getArgumentsAST();

	/**
	 * Returns the value of the '<em><b>System</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.ow2.fractal.f4e.fractal.System#getDefinitions <em>Definitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System</em>' container reference.
	 * @see #setSystem(org.ow2.fractal.f4e.fractal.System)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getDefinition_System()
	 * @see org.ow2.fractal.f4e.fractal.System#getDefinitions
	 * @model opposite="definitions" transient="false"
	 * @generated
	 */
	org.ow2.fractal.f4e.fractal.System getSystem();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Definition#getSystem <em>System</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System</em>' container reference.
	 * @see #getSystem()
	 * @generated
	 */
	void setSystem(org.ow2.fractal.f4e.fractal.System value);

	IDefinitionHelper getHelper();

} // Definition
