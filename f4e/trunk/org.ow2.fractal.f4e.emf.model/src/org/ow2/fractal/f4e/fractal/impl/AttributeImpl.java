/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.ow2.fractal.f4e.fractal.Attribute;
import org.ow2.fractal.f4e.fractal.AttributesController;
import org.ow2.fractal.f4e.fractal.Comment;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IAttributeHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelper;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AttributeImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AttributeImpl#getValue <em>Value</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AttributeImpl#getNameAST <em>Name AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AttributeImpl#getValueAST <em>Value AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AttributeImpl#getAttributesController <em>Attributes Controller</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AttributeImpl#getComments <em>Comments</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AttributeImpl#getMergedName <em>Merged Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AttributeImpl#getMergedValue <em>Merged Value</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AttributeImpl#getAny <em>Any</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.AttributeImpl#getAnyAttributes <em>Any Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AttributeImpl extends EObjectImpl implements Attribute {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getNameAST() <em>Name AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameAST()
	 * @generated
	 * @ordered
	 */
	protected Value nameAST;

	/**
	 * The cached value of the '{@link #getValueAST() <em>Value AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueAST()
	 * @generated
	 * @ordered
	 */
	protected Value valueAST;

	/**
	 * The cached value of the '{@link #getComments() <em>Comments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComments()
	 * @generated
	 * @ordered
	 */
	protected EList<Comment> comments;

	/**
	 * The default value of the '{@link #getMergedName() <em>Merged Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedName()
	 * @generated
	 * @ordered
	 */
	protected static final String MERGED_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMergedName() <em>Merged Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedName()
	 * @generated
	 * @ordered
	 */
	protected String mergedName = MERGED_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getMergedValue() <em>Merged Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedValue()
	 * @generated
	 * @ordered
	 */
	protected static final String MERGED_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMergedValue() <em>Merged Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedValue()
	 * @generated
	 * @ordered
	 */
	protected String mergedValue = MERGED_VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAny() <em>Any</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAny()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap any;

	/**
	 * The cached value of the '{@link #getAnyAttributes() <em>Any Attributes</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnyAttributes()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap anyAttributes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	protected EClass eStaticClass() {
		return FractalPackage.Literals.ATTRIBUTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ATTRIBUTE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ATTRIBUTE__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getNameAST() {
		return nameAST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNameAST(Value newNameAST, NotificationChain msgs) {
		Value oldNameAST = nameAST;
		nameAST = newNameAST;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.ATTRIBUTE__NAME_AST, oldNameAST, newNameAST);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNameAST(Value newNameAST) {
		if (newNameAST != nameAST) {
			NotificationChain msgs = null;
			if (nameAST != null)
				msgs = ((InternalEObject)nameAST).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ATTRIBUTE__NAME_AST, null, msgs);
			if (newNameAST != null)
				msgs = ((InternalEObject)newNameAST).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ATTRIBUTE__NAME_AST, null, msgs);
			msgs = basicSetNameAST(newNameAST, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ATTRIBUTE__NAME_AST, newNameAST, newNameAST));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getValueAST() {
		return valueAST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValueAST(Value newValueAST, NotificationChain msgs) {
		Value oldValueAST = valueAST;
		valueAST = newValueAST;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.ATTRIBUTE__VALUE_AST, oldValueAST, newValueAST);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueAST(Value newValueAST) {
		if (newValueAST != valueAST) {
			NotificationChain msgs = null;
			if (valueAST != null)
				msgs = ((InternalEObject)valueAST).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ATTRIBUTE__VALUE_AST, null, msgs);
			if (newValueAST != null)
				msgs = ((InternalEObject)newValueAST).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.ATTRIBUTE__VALUE_AST, null, msgs);
			msgs = basicSetValueAST(newValueAST, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ATTRIBUTE__VALUE_AST, newValueAST, newValueAST));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributesController getAttributesController() {
		if (eContainerFeatureID != FractalPackage.ATTRIBUTE__ATTRIBUTES_CONTROLLER) return null;
		return (AttributesController)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttributesController(AttributesController newAttributesController, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newAttributesController, FractalPackage.ATTRIBUTE__ATTRIBUTES_CONTROLLER, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributesController(AttributesController newAttributesController) {
		if (newAttributesController != eInternalContainer() || (eContainerFeatureID != FractalPackage.ATTRIBUTE__ATTRIBUTES_CONTROLLER && newAttributesController != null)) {
			if (EcoreUtil.isAncestor(this, newAttributesController))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newAttributesController != null)
				msgs = ((InternalEObject)newAttributesController).eInverseAdd(this, FractalPackage.ATTRIBUTES_CONTROLLER__ATTRIBUTES, AttributesController.class, msgs);
			msgs = basicSetAttributesController(newAttributesController, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ATTRIBUTE__ATTRIBUTES_CONTROLLER, newAttributesController, newAttributesController));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Comment> getComments() {
		if (comments == null) {
			comments = new EObjectContainmentEList<Comment>(Comment.class, this, FractalPackage.ATTRIBUTE__COMMENTS);
		}
		return comments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMergedName() {
		return mergedName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergedName(String newMergedName) {
		String oldMergedName = mergedName;
		mergedName = newMergedName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ATTRIBUTE__MERGED_NAME, oldMergedName, mergedName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMergedValue() {
		return mergedValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergedValue(String newMergedValue) {
		String oldMergedValue = mergedValue;
		mergedValue = newMergedValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.ATTRIBUTE__MERGED_VALUE, oldMergedValue, mergedValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getAny() {
		if (any == null) {
			any = new BasicFeatureMap(this, FractalPackage.ATTRIBUTE__ANY);
		}
		return any;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getAnyAttributes() {
		if (anyAttributes == null) {
			anyAttributes = new BasicFeatureMap(this, FractalPackage.ATTRIBUTE__ANY_ATTRIBUTES);
		}
		return anyAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.ATTRIBUTE__ATTRIBUTES_CONTROLLER:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetAttributesController((AttributesController)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.ATTRIBUTE__NAME_AST:
				return basicSetNameAST(null, msgs);
			case FractalPackage.ATTRIBUTE__VALUE_AST:
				return basicSetValueAST(null, msgs);
			case FractalPackage.ATTRIBUTE__ATTRIBUTES_CONTROLLER:
				return basicSetAttributesController(null, msgs);
			case FractalPackage.ATTRIBUTE__COMMENTS:
				return ((InternalEList<?>)getComments()).basicRemove(otherEnd, msgs);
			case FractalPackage.ATTRIBUTE__ANY:
				return ((InternalEList<?>)getAny()).basicRemove(otherEnd, msgs);
			case FractalPackage.ATTRIBUTE__ANY_ATTRIBUTES:
				return ((InternalEList<?>)getAnyAttributes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case FractalPackage.ATTRIBUTE__ATTRIBUTES_CONTROLLER:
				return eInternalContainer().eInverseRemove(this, FractalPackage.ATTRIBUTES_CONTROLLER__ATTRIBUTES, AttributesController.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FractalPackage.ATTRIBUTE__NAME:
				return getName();
			case FractalPackage.ATTRIBUTE__VALUE:
				return getValue();
			case FractalPackage.ATTRIBUTE__NAME_AST:
				return getNameAST();
			case FractalPackage.ATTRIBUTE__VALUE_AST:
				return getValueAST();
			case FractalPackage.ATTRIBUTE__ATTRIBUTES_CONTROLLER:
				return getAttributesController();
			case FractalPackage.ATTRIBUTE__COMMENTS:
				return getComments();
			case FractalPackage.ATTRIBUTE__MERGED_NAME:
				return getMergedName();
			case FractalPackage.ATTRIBUTE__MERGED_VALUE:
				return getMergedValue();
			case FractalPackage.ATTRIBUTE__ANY:
				if (coreType) return getAny();
				return ((FeatureMap.Internal)getAny()).getWrapper();
			case FractalPackage.ATTRIBUTE__ANY_ATTRIBUTES:
				if (coreType) return getAnyAttributes();
				return ((FeatureMap.Internal)getAnyAttributes()).getWrapper();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FractalPackage.ATTRIBUTE__NAME:
				setName((String)newValue);
				return;
			case FractalPackage.ATTRIBUTE__VALUE:
				setValue((String)newValue);
				return;
			case FractalPackage.ATTRIBUTE__NAME_AST:
				setNameAST((Value)newValue);
				return;
			case FractalPackage.ATTRIBUTE__VALUE_AST:
				setValueAST((Value)newValue);
				return;
			case FractalPackage.ATTRIBUTE__ATTRIBUTES_CONTROLLER:
				setAttributesController((AttributesController)newValue);
				return;
			case FractalPackage.ATTRIBUTE__COMMENTS:
				getComments().clear();
				getComments().addAll((Collection<? extends Comment>)newValue);
				return;
			case FractalPackage.ATTRIBUTE__MERGED_NAME:
				setMergedName((String)newValue);
				return;
			case FractalPackage.ATTRIBUTE__MERGED_VALUE:
				setMergedValue((String)newValue);
				return;
			case FractalPackage.ATTRIBUTE__ANY:
				((FeatureMap.Internal)getAny()).set(newValue);
				return;
			case FractalPackage.ATTRIBUTE__ANY_ATTRIBUTES:
				((FeatureMap.Internal)getAnyAttributes()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public void eUnset(int featureID) {
		switch (featureID) {
			case FractalPackage.ATTRIBUTE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case FractalPackage.ATTRIBUTE__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case FractalPackage.ATTRIBUTE__NAME_AST:
				setNameAST((Value)null);
				return;
			case FractalPackage.ATTRIBUTE__VALUE_AST:
				setValueAST((Value)null);
				return;
			case FractalPackage.ATTRIBUTE__ATTRIBUTES_CONTROLLER:
				setAttributesController((AttributesController)null);
				return;
			case FractalPackage.ATTRIBUTE__COMMENTS:
				getComments().clear();
				return;
			case FractalPackage.ATTRIBUTE__MERGED_NAME:
				setMergedName(MERGED_NAME_EDEFAULT);
				return;
			case FractalPackage.ATTRIBUTE__MERGED_VALUE:
				setMergedValue(MERGED_VALUE_EDEFAULT);
				return;
			case FractalPackage.ATTRIBUTE__ANY:
				getAny().clear();
				return;
			case FractalPackage.ATTRIBUTE__ANY_ATTRIBUTES:
				getAnyAttributes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FractalPackage.ATTRIBUTE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case FractalPackage.ATTRIBUTE__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case FractalPackage.ATTRIBUTE__NAME_AST:
				return nameAST != null;
			case FractalPackage.ATTRIBUTE__VALUE_AST:
				return valueAST != null;
			case FractalPackage.ATTRIBUTE__ATTRIBUTES_CONTROLLER:
				return getAttributesController() != null;
			case FractalPackage.ATTRIBUTE__COMMENTS:
				return comments != null && !comments.isEmpty();
			case FractalPackage.ATTRIBUTE__MERGED_NAME:
				return MERGED_NAME_EDEFAULT == null ? mergedName != null : !MERGED_NAME_EDEFAULT.equals(mergedName);
			case FractalPackage.ATTRIBUTE__MERGED_VALUE:
				return MERGED_VALUE_EDEFAULT == null ? mergedValue != null : !MERGED_VALUE_EDEFAULT.equals(mergedValue);
			case FractalPackage.ATTRIBUTE__ANY:
				return any != null && !any.isEmpty();
			case FractalPackage.ATTRIBUTE__ANY_ATTRIBUTES:
				return anyAttributes != null && !anyAttributes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", value: ");
		result.append(value);
		result.append(", mergedName: ");
		result.append(mergedName);
		result.append(", mergedValue: ");
		result.append(mergedValue);
		result.append(", any: ");
		result.append(any);
		result.append(", anyAttributes: ");
		result.append(anyAttributes);
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	 
		
  	
	public Object getAdapter(Class adapter) {
		Object object = null;
		
		
		if(adapter == IHelper.class ||
			adapter == IAttributeHelper.class ){
			object = HelperAdapterFactory.getInstance().adapt(this);
		}
		
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public IAttributeHelper getHelper() {
		return (IAttributeHelper)getAdapter(IAttributeHelper.class);
	}

} //AttributeImpl
