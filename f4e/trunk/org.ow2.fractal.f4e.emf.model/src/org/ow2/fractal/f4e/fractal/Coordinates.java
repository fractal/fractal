/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.ow2.fractal.f4e.fractal.adapter.helper.ICoordinatesHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Coordinates</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Coordinates#getName <em>Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Coordinates#getX0 <em>X0</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Coordinates#getX1 <em>X1</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Coordinates#getY0 <em>Y0</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Coordinates#getY1 <em>Y1</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Coordinates#getColor <em>Color</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Coordinates#getNameAST <em>Name AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Coordinates#getX0AST <em>X0AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Coordinates#getX1AST <em>X1AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Coordinates#getY0AST <em>Y0AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Coordinates#getY1AST <em>Y1AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Coordinates#getColorAST <em>Color AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Coordinates#getCoordinates <em>Coordinates</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Coordinates#getAny <em>Any</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.Coordinates#getAnyAttributes <em>Any Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getCoordinates()
 * @model
 * @generated
 */
public interface Coordinates extends EObject, IAdaptable, IHelperAdapter {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getCoordinates_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Coordinates#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>X0</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>X0</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>X0</em>' attribute.
	 * @see #setX0(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getCoordinates_X0()
	 * @model required="true"
	 * @generated
	 */
	String getX0();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Coordinates#getX0 <em>X0</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X0</em>' attribute.
	 * @see #getX0()
	 * @generated
	 */
	void setX0(String value);

	/**
	 * Returns the value of the '<em><b>X1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>X1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>X1</em>' attribute.
	 * @see #setX1(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getCoordinates_X1()
	 * @model required="true"
	 * @generated
	 */
	String getX1();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Coordinates#getX1 <em>X1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X1</em>' attribute.
	 * @see #getX1()
	 * @generated
	 */
	void setX1(String value);

	/**
	 * Returns the value of the '<em><b>Y0</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Y0</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Y0</em>' attribute.
	 * @see #setY0(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getCoordinates_Y0()
	 * @model required="true"
	 * @generated
	 */
	String getY0();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Coordinates#getY0 <em>Y0</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Y0</em>' attribute.
	 * @see #getY0()
	 * @generated
	 */
	void setY0(String value);

	/**
	 * Returns the value of the '<em><b>Y1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Y1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Y1</em>' attribute.
	 * @see #setY1(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getCoordinates_Y1()
	 * @model required="true"
	 * @generated
	 */
	String getY1();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Coordinates#getY1 <em>Y1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Y1</em>' attribute.
	 * @see #getY1()
	 * @generated
	 */
	void setY1(String value);

	/**
	 * Returns the value of the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Color</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Color</em>' attribute.
	 * @see #setColor(String)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getCoordinates_Color()
	 * @model
	 * @generated
	 */
	String getColor();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Coordinates#getColor <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Color</em>' attribute.
	 * @see #getColor()
	 * @generated
	 */
	void setColor(String value);

	/**
	 * Returns the value of the '<em><b>Name AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name AST</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name AST</em>' containment reference.
	 * @see #setNameAST(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getCoordinates_NameAST()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	Value getNameAST();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Coordinates#getNameAST <em>Name AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name AST</em>' containment reference.
	 * @see #getNameAST()
	 * @generated
	 */
	void setNameAST(Value value);

	/**
	 * Returns the value of the '<em><b>X0AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>X0AST</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>X0AST</em>' containment reference.
	 * @see #setX0AST(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getCoordinates_X0AST()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	Value getX0AST();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Coordinates#getX0AST <em>X0AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X0AST</em>' containment reference.
	 * @see #getX0AST()
	 * @generated
	 */
	void setX0AST(Value value);

	/**
	 * Returns the value of the '<em><b>X1AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>X1AST</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>X1AST</em>' containment reference.
	 * @see #setX1AST(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getCoordinates_X1AST()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	Value getX1AST();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Coordinates#getX1AST <em>X1AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X1AST</em>' containment reference.
	 * @see #getX1AST()
	 * @generated
	 */
	void setX1AST(Value value);

	/**
	 * Returns the value of the '<em><b>Y0AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Y0AST</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Y0AST</em>' containment reference.
	 * @see #setY0AST(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getCoordinates_Y0AST()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	Value getY0AST();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Coordinates#getY0AST <em>Y0AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Y0AST</em>' containment reference.
	 * @see #getY0AST()
	 * @generated
	 */
	void setY0AST(Value value);

	/**
	 * Returns the value of the '<em><b>Y1AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Y1AST</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Y1AST</em>' containment reference.
	 * @see #setY1AST(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getCoordinates_Y1AST()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	Value getY1AST();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Coordinates#getY1AST <em>Y1AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Y1AST</em>' containment reference.
	 * @see #getY1AST()
	 * @generated
	 */
	void setY1AST(Value value);

	/**
	 * Returns the value of the '<em><b>Color AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Color AST</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Color AST</em>' containment reference.
	 * @see #setColorAST(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getCoordinates_ColorAST()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	Value getColorAST();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.Coordinates#getColorAST <em>Color AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Color AST</em>' containment reference.
	 * @see #getColorAST()
	 * @generated
	 */
	void setColorAST(Value value);

	/**
	 * Returns the value of the '<em><b>Coordinates</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.Coordinates}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coordinates</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coordinates</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getCoordinates_Coordinates()
	 * @model containment="true"
	 * @generated
	 */
	EList<Coordinates> getCoordinates();

	/**
	 * Returns the value of the '<em><b>Any</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any</em>' attribute list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getCoordinates_Any()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' wildcards='##any' name=':6' processing='lax'"
	 * @generated
	 */
	FeatureMap getAny();

	/**
	 * Returns the value of the '<em><b>Any Attributes</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Any Attributes</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any Attributes</em>' attribute list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getCoordinates_AnyAttributes()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='attributeWildcard' wildcards='##any' name=':12' processing='lax'"
	 * @generated
	 */
	FeatureMap getAnyAttributes();

	ICoordinatesHelper getHelper();

} // Coordinates
