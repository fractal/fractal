/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal;

import org.eclipse.core.runtime.IAdaptable;
import org.ow2.fractal.f4e.fractal.adapter.helper.IFormalParameterHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Formal Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.FormalParameter#getDefinition <em>Definition</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getFormalParameter()
 * @model
 * @generated
 */
public interface FormalParameter extends Parameter, IAdaptable, IHelperAdapter {
	/**
	 * Returns the value of the '<em><b>Definition</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.ow2.fractal.f4e.fractal.Definition#getArgumentsAST <em>Arguments AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definition</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition</em>' container reference.
	 * @see #setDefinition(Definition)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getFormalParameter_Definition()
	 * @see org.ow2.fractal.f4e.fractal.Definition#getArgumentsAST
	 * @model opposite="argumentsAST" transient="false"
	 * @generated
	 */
	Definition getDefinition();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.FormalParameter#getDefinition <em>Definition</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Definition</em>' container reference.
	 * @see #getDefinition()
	 * @generated
	 */
	void setDefinition(Definition value);

	IFormalParameterHelper getHelper();

} // FormalParameter
