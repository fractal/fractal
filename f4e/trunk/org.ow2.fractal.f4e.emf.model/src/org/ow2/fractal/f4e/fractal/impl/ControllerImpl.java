/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Comment;
import org.ow2.fractal.f4e.fractal.Controller;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.adapter.factory.HelperAdapterFactory;
import org.ow2.fractal.f4e.fractal.adapter.helper.IControllerHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelper;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Controller</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.ControllerImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.ControllerImpl#getTypeAST <em>Type AST</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.ControllerImpl#getAbstractComponent <em>Abstract Component</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.ControllerImpl#getComments <em>Comments</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.ControllerImpl#getMergedType <em>Merged Type</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.ControllerImpl#getAny <em>Any</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.impl.ControllerImpl#getAnyAttributes <em>Any Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ControllerImpl extends EObjectImpl implements Controller {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTypeAST() <em>Type AST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeAST()
	 * @generated
	 * @ordered
	 */
	protected Value typeAST;

	/**
	 * The cached value of the '{@link #getComments() <em>Comments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComments()
	 * @generated
	 * @ordered
	 */
	protected EList<Comment> comments;

	/**
	 * The default value of the '{@link #getMergedType() <em>Merged Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedType()
	 * @generated
	 * @ordered
	 */
	protected static final String MERGED_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMergedType() <em>Merged Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMergedType()
	 * @generated
	 * @ordered
	 */
	protected String mergedType = MERGED_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAny() <em>Any</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAny()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap any;

	/**
	 * The cached value of the '{@link #getAnyAttributes() <em>Any Attributes</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnyAttributes()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap anyAttributes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ControllerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	protected EClass eStaticClass() {
		return FractalPackage.Literals.CONTROLLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.CONTROLLER__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getTypeAST() {
		return typeAST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTypeAST(Value newTypeAST, NotificationChain msgs) {
		Value oldTypeAST = typeAST;
		typeAST = newTypeAST;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FractalPackage.CONTROLLER__TYPE_AST, oldTypeAST, newTypeAST);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypeAST(Value newTypeAST) {
		if (newTypeAST != typeAST) {
			NotificationChain msgs = null;
			if (typeAST != null)
				msgs = ((InternalEObject)typeAST).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FractalPackage.CONTROLLER__TYPE_AST, null, msgs);
			if (newTypeAST != null)
				msgs = ((InternalEObject)newTypeAST).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FractalPackage.CONTROLLER__TYPE_AST, null, msgs);
			msgs = basicSetTypeAST(newTypeAST, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.CONTROLLER__TYPE_AST, newTypeAST, newTypeAST));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractComponent getAbstractComponent() {
		if (eContainerFeatureID != FractalPackage.CONTROLLER__ABSTRACT_COMPONENT) return null;
		return (AbstractComponent)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAbstractComponent(AbstractComponent newAbstractComponent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newAbstractComponent, FractalPackage.CONTROLLER__ABSTRACT_COMPONENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstractComponent(AbstractComponent newAbstractComponent) {
		if (newAbstractComponent != eInternalContainer() || (eContainerFeatureID != FractalPackage.CONTROLLER__ABSTRACT_COMPONENT && newAbstractComponent != null)) {
			if (EcoreUtil.isAncestor(this, newAbstractComponent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newAbstractComponent != null)
				msgs = ((InternalEObject)newAbstractComponent).eInverseAdd(this, FractalPackage.ABSTRACT_COMPONENT__CONTROLLER, AbstractComponent.class, msgs);
			msgs = basicSetAbstractComponent(newAbstractComponent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.CONTROLLER__ABSTRACT_COMPONENT, newAbstractComponent, newAbstractComponent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Comment> getComments() {
		if (comments == null) {
			comments = new EObjectContainmentEList<Comment>(Comment.class, this, FractalPackage.CONTROLLER__COMMENTS);
		}
		return comments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMergedType() {
		return mergedType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMergedType(String newMergedType) {
		String oldMergedType = mergedType;
		mergedType = newMergedType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FractalPackage.CONTROLLER__MERGED_TYPE, oldMergedType, mergedType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getAny() {
		if (any == null) {
			any = new BasicFeatureMap(this, FractalPackage.CONTROLLER__ANY);
		}
		return any;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getAnyAttributes() {
		if (anyAttributes == null) {
			anyAttributes = new BasicFeatureMap(this, FractalPackage.CONTROLLER__ANY_ATTRIBUTES);
		}
		return anyAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.CONTROLLER__ABSTRACT_COMPONENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetAbstractComponent((AbstractComponent)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FractalPackage.CONTROLLER__TYPE_AST:
				return basicSetTypeAST(null, msgs);
			case FractalPackage.CONTROLLER__ABSTRACT_COMPONENT:
				return basicSetAbstractComponent(null, msgs);
			case FractalPackage.CONTROLLER__COMMENTS:
				return ((InternalEList<?>)getComments()).basicRemove(otherEnd, msgs);
			case FractalPackage.CONTROLLER__ANY:
				return ((InternalEList<?>)getAny()).basicRemove(otherEnd, msgs);
			case FractalPackage.CONTROLLER__ANY_ATTRIBUTES:
				return ((InternalEList<?>)getAnyAttributes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID) {
			case FractalPackage.CONTROLLER__ABSTRACT_COMPONENT:
				return eInternalContainer().eInverseRemove(this, FractalPackage.ABSTRACT_COMPONENT__CONTROLLER, AbstractComponent.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FractalPackage.CONTROLLER__TYPE:
				return getType();
			case FractalPackage.CONTROLLER__TYPE_AST:
				return getTypeAST();
			case FractalPackage.CONTROLLER__ABSTRACT_COMPONENT:
				return getAbstractComponent();
			case FractalPackage.CONTROLLER__COMMENTS:
				return getComments();
			case FractalPackage.CONTROLLER__MERGED_TYPE:
				return getMergedType();
			case FractalPackage.CONTROLLER__ANY:
				if (coreType) return getAny();
				return ((FeatureMap.Internal)getAny()).getWrapper();
			case FractalPackage.CONTROLLER__ANY_ATTRIBUTES:
				if (coreType) return getAnyAttributes();
				return ((FeatureMap.Internal)getAnyAttributes()).getWrapper();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FractalPackage.CONTROLLER__TYPE:
				setType((String)newValue);
				return;
			case FractalPackage.CONTROLLER__TYPE_AST:
				setTypeAST((Value)newValue);
				return;
			case FractalPackage.CONTROLLER__ABSTRACT_COMPONENT:
				setAbstractComponent((AbstractComponent)newValue);
				return;
			case FractalPackage.CONTROLLER__COMMENTS:
				getComments().clear();
				getComments().addAll((Collection<? extends Comment>)newValue);
				return;
			case FractalPackage.CONTROLLER__MERGED_TYPE:
				setMergedType((String)newValue);
				return;
			case FractalPackage.CONTROLLER__ANY:
				((FeatureMap.Internal)getAny()).set(newValue);
				return;
			case FractalPackage.CONTROLLER__ANY_ATTRIBUTES:
				((FeatureMap.Internal)getAnyAttributes()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public void eUnset(int featureID) {
		switch (featureID) {
			case FractalPackage.CONTROLLER__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case FractalPackage.CONTROLLER__TYPE_AST:
				setTypeAST((Value)null);
				return;
			case FractalPackage.CONTROLLER__ABSTRACT_COMPONENT:
				setAbstractComponent((AbstractComponent)null);
				return;
			case FractalPackage.CONTROLLER__COMMENTS:
				getComments().clear();
				return;
			case FractalPackage.CONTROLLER__MERGED_TYPE:
				setMergedType(MERGED_TYPE_EDEFAULT);
				return;
			case FractalPackage.CONTROLLER__ANY:
				getAny().clear();
				return;
			case FractalPackage.CONTROLLER__ANY_ATTRIBUTES:
				getAnyAttributes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FractalPackage.CONTROLLER__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
			case FractalPackage.CONTROLLER__TYPE_AST:
				return typeAST != null;
			case FractalPackage.CONTROLLER__ABSTRACT_COMPONENT:
				return getAbstractComponent() != null;
			case FractalPackage.CONTROLLER__COMMENTS:
				return comments != null && !comments.isEmpty();
			case FractalPackage.CONTROLLER__MERGED_TYPE:
				return MERGED_TYPE_EDEFAULT == null ? mergedType != null : !MERGED_TYPE_EDEFAULT.equals(mergedType);
			case FractalPackage.CONTROLLER__ANY:
				return any != null && !any.isEmpty();
			case FractalPackage.CONTROLLER__ANY_ATTRIBUTES:
				return anyAttributes != null && !anyAttributes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(", mergedType: ");
		result.append(mergedType);
		result.append(", any: ");
		result.append(any);
		result.append(", anyAttributes: ");
		result.append(anyAttributes);
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	 
		
  	
	public Object getAdapter(Class adapter) {
		Object object = null;
		
		
		if(adapter == IHelper.class ||
			adapter == IControllerHelper.class ){
			object = HelperAdapterFactory.getInstance().adapt(this);
		}
		
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public IControllerHelper getHelper() {
		return (IControllerHelper)getAdapter(IControllerHelper.class);
	}

} //ControllerImpl
