/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal;

import org.eclipse.core.runtime.IAdaptable;
import org.ow2.fractal.f4e.fractal.adapter.helper.IEffectiveParameterHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Effective Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getEffectiveParameter()
 * @model
 * @generated
 */
public interface EffectiveParameter extends Parameter, IAdaptable, IHelperAdapter {

	IEffectiveParameterHelper getHelper();
} // EffectiveParameter
