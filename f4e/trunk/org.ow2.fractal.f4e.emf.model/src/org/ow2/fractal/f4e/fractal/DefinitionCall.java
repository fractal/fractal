/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;
import org.ow2.fractal.f4e.fractal.adapter.helper.IDefinitionCallHelper;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Definition Call</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.fractal.f4e.fractal.DefinitionCall#getDefinition <em>Definition</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.DefinitionCall#getParameters <em>Parameters</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.DefinitionCall#getDefinitionName <em>Definition Name</em>}</li>
 *   <li>{@link org.ow2.fractal.f4e.fractal.DefinitionCall#getAbstractComponent <em>Abstract Component</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getDefinitionCall()
 * @model
 * @generated
 */
public interface DefinitionCall extends EObject, IAdaptable, IHelperAdapter {
	/**
	 * Returns the value of the '<em><b>Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition</em>' reference.
	 * @see #setDefinition(Definition)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getDefinitionCall_Definition()
	 * @model required="true"
	 * @generated
	 */
	Definition getDefinition();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.DefinitionCall#getDefinition <em>Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Definition</em>' reference.
	 * @see #getDefinition()
	 * @generated
	 */
	void setDefinition(Definition value);

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link org.ow2.fractal.f4e.fractal.EffectiveParameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getDefinitionCall_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<EffectiveParameter> getParameters();

	/**
	 * Returns the value of the '<em><b>Definition Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definition Name</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition Name</em>' containment reference.
	 * @see #setDefinitionName(Value)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getDefinitionCall_DefinitionName()
	 * @model containment="true"
	 * @generated
	 */
	Value getDefinitionName();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.DefinitionCall#getDefinitionName <em>Definition Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Definition Name</em>' containment reference.
	 * @see #getDefinitionName()
	 * @generated
	 */
	void setDefinitionName(Value value);

	/**
	 * Returns the value of the '<em><b>Abstract Component</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getExtendsAST <em>Extends AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract Component</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract Component</em>' container reference.
	 * @see #setAbstractComponent(AbstractComponent)
	 * @see org.ow2.fractal.f4e.fractal.FractalPackage#getDefinitionCall_AbstractComponent()
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getExtendsAST
	 * @model opposite="extendsAST" transient="false"
	 * @generated
	 */
	AbstractComponent getAbstractComponent();

	/**
	 * Sets the value of the '{@link org.ow2.fractal.f4e.fractal.DefinitionCall#getAbstractComponent <em>Abstract Component</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract Component</em>' container reference.
	 * @see #getAbstractComponent()
	 * @generated
	 */
	void setAbstractComponent(AbstractComponent value);

	IDefinitionCallHelper getHelper();

} // DefinitionCall
