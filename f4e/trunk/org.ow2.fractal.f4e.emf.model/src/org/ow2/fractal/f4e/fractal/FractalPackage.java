/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.ow2.fractal.f4e.fractal.FractalFactory
 * @model kind="package"
 * @generated
 */
public interface FractalPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "fractal";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.ow2.fractal/f4e/fractal.ecore/1.0.0";

	/**
	 * The package namespace name.
	 * Yann hand modifs.
	 *
	 * @generated NOT
	 */
	String eNS_PREFIX = "fractal.ecore";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FractalPackage eINSTANCE = org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.SystemImpl <em>System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.SystemImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getSystem()
	 * @generated
	 */
	int SYSTEM = 0;

	/**
	 * The feature id for the '<em><b>Definition Calls</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM__DEFINITION_CALLS = 0;

	/**
	 * The feature id for the '<em><b>Pamameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM__PAMAMETERS = 1;

	/**
	 * The feature id for the '<em><b>Definitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM__DEFINITIONS = 2;

	/**
	 * The number of structural features of the '<em>System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl <em>Abstract Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getAbstractComponent()
	 * @generated
	 */
	int ABSTRACT_COMPONENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Merged Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__MERGED_NAME = 1;

	/**
	 * The feature id for the '<em><b>Name AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__NAME_AST = 2;

	/**
	 * The feature id for the '<em><b>Extends AST</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__EXTENDS_AST = 3;

	/**
	 * The feature id for the '<em><b>Comments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__COMMENTS = 4;

	/**
	 * The feature id for the '<em><b>Interfaces</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__INTERFACES = 5;

	/**
	 * The feature id for the '<em><b>Merged Interfaces</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__MERGED_INTERFACES = 6;

	/**
	 * The feature id for the '<em><b>Sub Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__SUB_COMPONENTS = 7;

	/**
	 * The feature id for the '<em><b>Merged Sub Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS = 8;

	/**
	 * The feature id for the '<em><b>Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__CONTENT = 9;

	/**
	 * The feature id for the '<em><b>Merged Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__MERGED_CONTENT = 10;

	/**
	 * The feature id for the '<em><b>Attributes Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER = 11;

	/**
	 * The feature id for the '<em><b>Merged Attributes Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER = 12;

	/**
	 * The feature id for the '<em><b>Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__BINDINGS = 13;

	/**
	 * The feature id for the '<em><b>Merged Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__MERGED_BINDINGS = 14;

	/**
	 * The feature id for the '<em><b>Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__CONTROLLER = 15;

	/**
	 * The feature id for the '<em><b>Merged Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__MERGED_CONTROLLER = 16;

	/**
	 * The feature id for the '<em><b>Template Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__TEMPLATE_CONTROLLER = 17;

	/**
	 * The feature id for the '<em><b>Merged Template Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__MERGED_TEMPLATE_CONTROLLER = 18;

	/**
	 * The feature id for the '<em><b>Logger</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__LOGGER = 19;

	/**
	 * The feature id for the '<em><b>Merged Logger</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__MERGED_LOGGER = 20;

	/**
	 * The feature id for the '<em><b>Virtual Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__VIRTUAL_NODE = 21;

	/**
	 * The feature id for the '<em><b>Merged Virtual Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__MERGED_VIRTUAL_NODE = 22;

	/**
	 * The feature id for the '<em><b>Coordinates</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__COORDINATES = 23;

	/**
	 * The feature id for the '<em><b>Any</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__ANY = 24;

	/**
	 * The feature id for the '<em><b>Any Attributes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT__ANY_ATTRIBUTES = 25;

	/**
	 * The number of structural features of the '<em>Abstract Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPONENT_FEATURE_COUNT = 26;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.ParameterImpl <em>Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.ParameterImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getParameter()
	 * @generated
	 */
	int PARAMETER = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__NAME = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.ValueImpl <em>Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.ValueImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getValue()
	 * @generated
	 */
	int VALUE = 3;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE__ELEMENTS = 0;

	/**
	 * The feature id for the '<em><b>Context Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE__CONTEXT_DEFINITION = 1;

	/**
	 * The number of structural features of the '<em>Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.ValueElementImpl <em>Value Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.ValueElementImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getValueElement()
	 * @generated
	 */
	int VALUE_ELEMENT = 4;

	/**
	 * The number of structural features of the '<em>Value Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_ELEMENT_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.ConstantImpl <em>Constant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.ConstantImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getConstant()
	 * @generated
	 */
	int CONSTANT = 5;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT__CONSTANT = VALUE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_FEATURE_COUNT = VALUE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.ReferenceImpl <em>Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.ReferenceImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getReference()
	 * @generated
	 */
	int REFERENCE = 6;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE__REFERENCE = VALUE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE__NAME = VALUE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_FEATURE_COUNT = VALUE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.FormalParameterImpl <em>Formal Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.FormalParameterImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getFormalParameter()
	 * @generated
	 */
	int FORMAL_PARAMETER = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMAL_PARAMETER__NAME = PARAMETER__NAME;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMAL_PARAMETER__VALUE = PARAMETER__VALUE;

	/**
	 * The feature id for the '<em><b>Definition</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMAL_PARAMETER__DEFINITION = PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Formal Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMAL_PARAMETER_FEATURE_COUNT = PARAMETER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.DefinitionImpl <em>Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.DefinitionImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getDefinition()
	 * @generated
	 */
	int DEFINITION = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__NAME = ABSTRACT_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Merged Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__MERGED_NAME = ABSTRACT_COMPONENT__MERGED_NAME;

	/**
	 * The feature id for the '<em><b>Name AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__NAME_AST = ABSTRACT_COMPONENT__NAME_AST;

	/**
	 * The feature id for the '<em><b>Extends AST</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__EXTENDS_AST = ABSTRACT_COMPONENT__EXTENDS_AST;

	/**
	 * The feature id for the '<em><b>Comments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__COMMENTS = ABSTRACT_COMPONENT__COMMENTS;

	/**
	 * The feature id for the '<em><b>Interfaces</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__INTERFACES = ABSTRACT_COMPONENT__INTERFACES;

	/**
	 * The feature id for the '<em><b>Merged Interfaces</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__MERGED_INTERFACES = ABSTRACT_COMPONENT__MERGED_INTERFACES;

	/**
	 * The feature id for the '<em><b>Sub Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__SUB_COMPONENTS = ABSTRACT_COMPONENT__SUB_COMPONENTS;

	/**
	 * The feature id for the '<em><b>Merged Sub Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__MERGED_SUB_COMPONENTS = ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS;

	/**
	 * The feature id for the '<em><b>Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__CONTENT = ABSTRACT_COMPONENT__CONTENT;

	/**
	 * The feature id for the '<em><b>Merged Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__MERGED_CONTENT = ABSTRACT_COMPONENT__MERGED_CONTENT;

	/**
	 * The feature id for the '<em><b>Attributes Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__ATTRIBUTES_CONTROLLER = ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER;

	/**
	 * The feature id for the '<em><b>Merged Attributes Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__MERGED_ATTRIBUTES_CONTROLLER = ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER;

	/**
	 * The feature id for the '<em><b>Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__BINDINGS = ABSTRACT_COMPONENT__BINDINGS;

	/**
	 * The feature id for the '<em><b>Merged Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__MERGED_BINDINGS = ABSTRACT_COMPONENT__MERGED_BINDINGS;

	/**
	 * The feature id for the '<em><b>Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__CONTROLLER = ABSTRACT_COMPONENT__CONTROLLER;

	/**
	 * The feature id for the '<em><b>Merged Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__MERGED_CONTROLLER = ABSTRACT_COMPONENT__MERGED_CONTROLLER;

	/**
	 * The feature id for the '<em><b>Template Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__TEMPLATE_CONTROLLER = ABSTRACT_COMPONENT__TEMPLATE_CONTROLLER;

	/**
	 * The feature id for the '<em><b>Merged Template Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__MERGED_TEMPLATE_CONTROLLER = ABSTRACT_COMPONENT__MERGED_TEMPLATE_CONTROLLER;

	/**
	 * The feature id for the '<em><b>Logger</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__LOGGER = ABSTRACT_COMPONENT__LOGGER;

	/**
	 * The feature id for the '<em><b>Merged Logger</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__MERGED_LOGGER = ABSTRACT_COMPONENT__MERGED_LOGGER;

	/**
	 * The feature id for the '<em><b>Virtual Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__VIRTUAL_NODE = ABSTRACT_COMPONENT__VIRTUAL_NODE;

	/**
	 * The feature id for the '<em><b>Merged Virtual Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__MERGED_VIRTUAL_NODE = ABSTRACT_COMPONENT__MERGED_VIRTUAL_NODE;

	/**
	 * The feature id for the '<em><b>Coordinates</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__COORDINATES = ABSTRACT_COMPONENT__COORDINATES;

	/**
	 * The feature id for the '<em><b>Any</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__ANY = ABSTRACT_COMPONENT__ANY;

	/**
	 * The feature id for the '<em><b>Any Attributes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__ANY_ATTRIBUTES = ABSTRACT_COMPONENT__ANY_ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Extends</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__EXTENDS = ABSTRACT_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Arguments</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__ARGUMENTS = ABSTRACT_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Arguments AST</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__ARGUMENTS_AST = ABSTRACT_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>System</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION__SYSTEM = ABSTRACT_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_FEATURE_COUNT = ABSTRACT_COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.DefinitionCallImpl <em>Definition Call</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.DefinitionCallImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getDefinitionCall()
	 * @generated
	 */
	int DEFINITION_CALL = 9;

	/**
	 * The feature id for the '<em><b>Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_CALL__DEFINITION = 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_CALL__PARAMETERS = 1;

	/**
	 * The feature id for the '<em><b>Definition Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_CALL__DEFINITION_NAME = 2;

	/**
	 * The feature id for the '<em><b>Abstract Component</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_CALL__ABSTRACT_COMPONENT = 3;

	/**
	 * The number of structural features of the '<em>Definition Call</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEFINITION_CALL_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.EffectiveParameterImpl <em>Effective Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.EffectiveParameterImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getEffectiveParameter()
	 * @generated
	 */
	int EFFECTIVE_PARAMETER = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFFECTIVE_PARAMETER__NAME = PARAMETER__NAME;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFFECTIVE_PARAMETER__VALUE = PARAMETER__VALUE;

	/**
	 * The number of structural features of the '<em>Effective Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFFECTIVE_PARAMETER_FEATURE_COUNT = PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.InterfaceImpl <em>Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.InterfaceImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getInterface()
	 * @generated
	 */
	int INTERFACE = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__SIGNATURE = 1;

	/**
	 * The feature id for the '<em><b>Role</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__ROLE = 2;

	/**
	 * The feature id for the '<em><b>Cardinality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__CARDINALITY = 3;

	/**
	 * The feature id for the '<em><b>Contingency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__CONTINGENCY = 4;

	/**
	 * The feature id for the '<em><b>Name AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__NAME_AST = 5;

	/**
	 * The feature id for the '<em><b>Signature AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__SIGNATURE_AST = 6;

	/**
	 * The feature id for the '<em><b>Abstract Component</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__ABSTRACT_COMPONENT = 7;

	/**
	 * The feature id for the '<em><b>Comments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__COMMENTS = 8;

	/**
	 * The feature id for the '<em><b>Merged Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__MERGED_NAME = 9;

	/**
	 * The feature id for the '<em><b>Merged Signature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__MERGED_SIGNATURE = 10;

	/**
	 * The feature id for the '<em><b>Merged Role</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__MERGED_ROLE = 11;

	/**
	 * The feature id for the '<em><b>Merged Cardinality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__MERGED_CARDINALITY = 12;

	/**
	 * The feature id for the '<em><b>Merged Contingency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__MERGED_CONTINGENCY = 13;

	/**
	 * The feature id for the '<em><b>Any</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__ANY = 14;

	/**
	 * The feature id for the '<em><b>Any Attributes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__ANY_ATTRIBUTES = 15;

	/**
	 * The number of structural features of the '<em>Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_FEATURE_COUNT = 16;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.ComponentImpl <em>Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.ComponentImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getComponent()
	 * @generated
	 */
	int COMPONENT = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__NAME = ABSTRACT_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Merged Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__MERGED_NAME = ABSTRACT_COMPONENT__MERGED_NAME;

	/**
	 * The feature id for the '<em><b>Name AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__NAME_AST = ABSTRACT_COMPONENT__NAME_AST;

	/**
	 * The feature id for the '<em><b>Extends AST</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__EXTENDS_AST = ABSTRACT_COMPONENT__EXTENDS_AST;

	/**
	 * The feature id for the '<em><b>Comments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__COMMENTS = ABSTRACT_COMPONENT__COMMENTS;

	/**
	 * The feature id for the '<em><b>Interfaces</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__INTERFACES = ABSTRACT_COMPONENT__INTERFACES;

	/**
	 * The feature id for the '<em><b>Merged Interfaces</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__MERGED_INTERFACES = ABSTRACT_COMPONENT__MERGED_INTERFACES;

	/**
	 * The feature id for the '<em><b>Sub Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__SUB_COMPONENTS = ABSTRACT_COMPONENT__SUB_COMPONENTS;

	/**
	 * The feature id for the '<em><b>Merged Sub Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__MERGED_SUB_COMPONENTS = ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS;

	/**
	 * The feature id for the '<em><b>Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__CONTENT = ABSTRACT_COMPONENT__CONTENT;

	/**
	 * The feature id for the '<em><b>Merged Content</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__MERGED_CONTENT = ABSTRACT_COMPONENT__MERGED_CONTENT;

	/**
	 * The feature id for the '<em><b>Attributes Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__ATTRIBUTES_CONTROLLER = ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER;

	/**
	 * The feature id for the '<em><b>Merged Attributes Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__MERGED_ATTRIBUTES_CONTROLLER = ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER;

	/**
	 * The feature id for the '<em><b>Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__BINDINGS = ABSTRACT_COMPONENT__BINDINGS;

	/**
	 * The feature id for the '<em><b>Merged Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__MERGED_BINDINGS = ABSTRACT_COMPONENT__MERGED_BINDINGS;

	/**
	 * The feature id for the '<em><b>Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__CONTROLLER = ABSTRACT_COMPONENT__CONTROLLER;

	/**
	 * The feature id for the '<em><b>Merged Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__MERGED_CONTROLLER = ABSTRACT_COMPONENT__MERGED_CONTROLLER;

	/**
	 * The feature id for the '<em><b>Template Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__TEMPLATE_CONTROLLER = ABSTRACT_COMPONENT__TEMPLATE_CONTROLLER;

	/**
	 * The feature id for the '<em><b>Merged Template Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__MERGED_TEMPLATE_CONTROLLER = ABSTRACT_COMPONENT__MERGED_TEMPLATE_CONTROLLER;

	/**
	 * The feature id for the '<em><b>Logger</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__LOGGER = ABSTRACT_COMPONENT__LOGGER;

	/**
	 * The feature id for the '<em><b>Merged Logger</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__MERGED_LOGGER = ABSTRACT_COMPONENT__MERGED_LOGGER;

	/**
	 * The feature id for the '<em><b>Virtual Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__VIRTUAL_NODE = ABSTRACT_COMPONENT__VIRTUAL_NODE;

	/**
	 * The feature id for the '<em><b>Merged Virtual Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__MERGED_VIRTUAL_NODE = ABSTRACT_COMPONENT__MERGED_VIRTUAL_NODE;

	/**
	 * The feature id for the '<em><b>Coordinates</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__COORDINATES = ABSTRACT_COMPONENT__COORDINATES;

	/**
	 * The feature id for the '<em><b>Any</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__ANY = ABSTRACT_COMPONENT__ANY;

	/**
	 * The feature id for the '<em><b>Any Attributes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__ANY_ATTRIBUTES = ABSTRACT_COMPONENT__ANY_ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Definition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__DEFINITION = ABSTRACT_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Shared</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__SHARED = ABSTRACT_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__PARENT = ABSTRACT_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_FEATURE_COUNT = ABSTRACT_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.ContentImpl <em>Content</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.ContentImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getContent()
	 * @generated
	 */
	int CONTENT = 13;

	/**
	 * The feature id for the '<em><b>Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTENT__CLASS = 0;

	/**
	 * The feature id for the '<em><b>Class AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTENT__CLASS_AST = 1;

	/**
	 * The feature id for the '<em><b>Abstract Component</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTENT__ABSTRACT_COMPONENT = 2;

	/**
	 * The feature id for the '<em><b>Comments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTENT__COMMENTS = 3;

	/**
	 * The feature id for the '<em><b>Merged Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTENT__MERGED_CLASS = 4;

	/**
	 * The feature id for the '<em><b>Any</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTENT__ANY = 5;

	/**
	 * The feature id for the '<em><b>Any Attributes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTENT__ANY_ATTRIBUTES = 6;

	/**
	 * The number of structural features of the '<em>Content</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTENT_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.AttributesControllerImpl <em>Attributes Controller</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.AttributesControllerImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getAttributesController()
	 * @generated
	 */
	int ATTRIBUTES_CONTROLLER = 14;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTES_CONTROLLER__SIGNATURE = 0;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTES_CONTROLLER__ATTRIBUTES = 1;

	/**
	 * The feature id for the '<em><b>Signature AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTES_CONTROLLER__SIGNATURE_AST = 2;

	/**
	 * The feature id for the '<em><b>Abstract Component</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTES_CONTROLLER__ABSTRACT_COMPONENT = 3;

	/**
	 * The feature id for the '<em><b>Comments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTES_CONTROLLER__COMMENTS = 4;

	/**
	 * The feature id for the '<em><b>Merged Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTES_CONTROLLER__MERGED_ATTRIBUTES = 5;

	/**
	 * The feature id for the '<em><b>Merged Signature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTES_CONTROLLER__MERGED_SIGNATURE = 6;

	/**
	 * The feature id for the '<em><b>Any</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTES_CONTROLLER__ANY = 7;

	/**
	 * The feature id for the '<em><b>Any Attributes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTES_CONTROLLER__ANY_ATTRIBUTES = 8;

	/**
	 * The number of structural features of the '<em>Attributes Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTES_CONTROLLER_FEATURE_COUNT = 9;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.AttributeImpl <em>Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.AttributeImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getAttribute()
	 * @generated
	 */
	int ATTRIBUTE = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__VALUE = 1;

	/**
	 * The feature id for the '<em><b>Name AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__NAME_AST = 2;

	/**
	 * The feature id for the '<em><b>Value AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__VALUE_AST = 3;

	/**
	 * The feature id for the '<em><b>Attributes Controller</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__ATTRIBUTES_CONTROLLER = 4;

	/**
	 * The feature id for the '<em><b>Comments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__COMMENTS = 5;

	/**
	 * The feature id for the '<em><b>Merged Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__MERGED_NAME = 6;

	/**
	 * The feature id for the '<em><b>Merged Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__MERGED_VALUE = 7;

	/**
	 * The feature id for the '<em><b>Any</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__ANY = 8;

	/**
	 * The feature id for the '<em><b>Any Attributes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__ANY_ATTRIBUTES = 9;

	/**
	 * The number of structural features of the '<em>Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_FEATURE_COUNT = 10;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.BindingImpl <em>Binding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.BindingImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getBinding()
	 * @generated
	 */
	int BINDING = 16;

	/**
	 * The feature id for the '<em><b>Client</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING__CLIENT = 0;

	/**
	 * The feature id for the '<em><b>Server</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING__SERVER = 1;

	/**
	 * The feature id for the '<em><b>Client Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING__CLIENT_INTERFACE = 2;

	/**
	 * The feature id for the '<em><b>Server Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING__SERVER_INTERFACE = 3;

	/**
	 * The feature id for the '<em><b>Client AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING__CLIENT_AST = 4;

	/**
	 * The feature id for the '<em><b>Server AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING__SERVER_AST = 5;

	/**
	 * The feature id for the '<em><b>Abstract Component</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING__ABSTRACT_COMPONENT = 6;

	/**
	 * The feature id for the '<em><b>Comments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING__COMMENTS = 7;

	/**
	 * The feature id for the '<em><b>Merged Client</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING__MERGED_CLIENT = 8;

	/**
	 * The feature id for the '<em><b>Merged Server</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING__MERGED_SERVER = 9;

	/**
	 * The feature id for the '<em><b>Any</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING__ANY = 10;

	/**
	 * The feature id for the '<em><b>Any Attributes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING__ANY_ATTRIBUTES = 11;

	/**
	 * The number of structural features of the '<em>Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING_FEATURE_COUNT = 12;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.ControllerImpl <em>Controller</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.ControllerImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getController()
	 * @generated
	 */
	int CONTROLLER = 17;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Type AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER__TYPE_AST = 1;

	/**
	 * The feature id for the '<em><b>Abstract Component</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER__ABSTRACT_COMPONENT = 2;

	/**
	 * The feature id for the '<em><b>Comments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER__COMMENTS = 3;

	/**
	 * The feature id for the '<em><b>Merged Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER__MERGED_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Any</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER__ANY = 5;

	/**
	 * The feature id for the '<em><b>Any Attributes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER__ANY_ATTRIBUTES = 6;

	/**
	 * The number of structural features of the '<em>Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROLLER_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.TemplateControllerImpl <em>Template Controller</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.TemplateControllerImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getTemplateController()
	 * @generated
	 */
	int TEMPLATE_CONTROLLER = 18;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_CONTROLLER__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Type AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_CONTROLLER__TYPE_AST = 1;

	/**
	 * The feature id for the '<em><b>Abstract Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_CONTROLLER__ABSTRACT_COMPONENT = 2;

	/**
	 * The feature id for the '<em><b>Comments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_CONTROLLER__COMMENTS = 3;

	/**
	 * The feature id for the '<em><b>Merged Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_CONTROLLER__MERGED_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Any</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_CONTROLLER__ANY = 5;

	/**
	 * The feature id for the '<em><b>Any Attributes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_CONTROLLER__ANY_ATTRIBUTES = 6;

	/**
	 * The number of structural features of the '<em>Template Controller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_CONTROLLER_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.CommentImpl <em>Comment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.CommentImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getComment()
	 * @generated
	 */
	int COMMENT = 19;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__LANGUAGE = 0;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__TEXT = 1;

	/**
	 * The feature id for the '<em><b>Language AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__LANGUAGE_AST = 2;

	/**
	 * The feature id for the '<em><b>Text AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__TEXT_AST = 3;

	/**
	 * The feature id for the '<em><b>Any</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__ANY = 4;

	/**
	 * The feature id for the '<em><b>Any Attributes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__ANY_ATTRIBUTES = 5;

	/**
	 * The number of structural features of the '<em>Comment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.CoordinatesImpl <em>Coordinates</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.CoordinatesImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getCoordinates()
	 * @generated
	 */
	int COORDINATES = 20;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATES__NAME = 0;

	/**
	 * The feature id for the '<em><b>X0</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATES__X0 = 1;

	/**
	 * The feature id for the '<em><b>X1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATES__X1 = 2;

	/**
	 * The feature id for the '<em><b>Y0</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATES__Y0 = 3;

	/**
	 * The feature id for the '<em><b>Y1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATES__Y1 = 4;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATES__COLOR = 5;

	/**
	 * The feature id for the '<em><b>Name AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATES__NAME_AST = 6;

	/**
	 * The feature id for the '<em><b>X0AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATES__X0_AST = 7;

	/**
	 * The feature id for the '<em><b>X1AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATES__X1_AST = 8;

	/**
	 * The feature id for the '<em><b>Y0AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATES__Y0_AST = 9;

	/**
	 * The feature id for the '<em><b>Y1AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATES__Y1_AST = 10;

	/**
	 * The feature id for the '<em><b>Color AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATES__COLOR_AST = 11;

	/**
	 * The feature id for the '<em><b>Coordinates</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATES__COORDINATES = 12;

	/**
	 * The feature id for the '<em><b>Any</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATES__ANY = 13;

	/**
	 * The feature id for the '<em><b>Any Attributes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATES__ANY_ATTRIBUTES = 14;

	/**
	 * The number of structural features of the '<em>Coordinates</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATES_FEATURE_COUNT = 15;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.LoggerImpl <em>Logger</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.LoggerImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getLogger()
	 * @generated
	 */
	int LOGGER = 21;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGGER__NAME = 0;

	/**
	 * The feature id for the '<em><b>Name AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGGER__NAME_AST = 1;

	/**
	 * The feature id for the '<em><b>Merged Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGGER__MERGED_NAME = 2;

	/**
	 * The feature id for the '<em><b>Any</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGGER__ANY = 3;

	/**
	 * The feature id for the '<em><b>Any Attributes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGGER__ANY_ATTRIBUTES = 4;

	/**
	 * The number of structural features of the '<em>Logger</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGGER_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.impl.VirtualNodeImpl <em>Virtual Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.impl.VirtualNodeImpl
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getVirtualNode()
	 * @generated
	 */
	int VIRTUAL_NODE = 22;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIRTUAL_NODE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Name AST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIRTUAL_NODE__NAME_AST = 1;

	/**
	 * The feature id for the '<em><b>Merged Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIRTUAL_NODE__MERGED_NAME = 2;

	/**
	 * The feature id for the '<em><b>Any</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIRTUAL_NODE__ANY = 3;

	/**
	 * The feature id for the '<em><b>Any Attributes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIRTUAL_NODE__ANY_ATTRIBUTES = 4;

	/**
	 * The number of structural features of the '<em>Virtual Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIRTUAL_NODE_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.Cardinality <em>Cardinality</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.Cardinality
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getCardinality()
	 * @generated
	 */
	int CARDINALITY = 23;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.Role <em>Role</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.Role
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getRole()
	 * @generated
	 */
	int ROLE = 24;

	/**
	 * The meta object id for the '{@link org.ow2.fractal.f4e.fractal.Contingency <em>Contingency</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.ow2.fractal.f4e.fractal.Contingency
	 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getContingency()
	 * @generated
	 */
	int CONTINGENCY = 25;


	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.System <em>System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System</em>'.
	 * @see org.ow2.fractal.f4e.fractal.System
	 * @generated
	 */
	EClass getSystem();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.System#getDefinitionCalls <em>Definition Calls</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Definition Calls</em>'.
	 * @see org.ow2.fractal.f4e.fractal.System#getDefinitionCalls()
	 * @see #getSystem()
	 * @generated
	 */
	EReference getSystem_DefinitionCalls();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.System#getPamameters <em>Pamameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Pamameters</em>'.
	 * @see org.ow2.fractal.f4e.fractal.System#getPamameters()
	 * @see #getSystem()
	 * @generated
	 */
	EReference getSystem_Pamameters();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.System#getDefinitions <em>Definitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Definitions</em>'.
	 * @see org.ow2.fractal.f4e.fractal.System#getDefinitions()
	 * @see #getSystem()
	 * @generated
	 */
	EReference getSystem_Definitions();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.AbstractComponent <em>Abstract Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Component</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent
	 * @generated
	 */
	EClass getAbstractComponent();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getName()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EAttribute getAbstractComponent_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedName <em>Merged Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Merged Name</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedName()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EAttribute getAbstractComponent_MergedName();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getNameAST <em>Name AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Name AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getNameAST()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_NameAST();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getExtendsAST <em>Extends AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Extends AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getExtendsAST()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_ExtendsAST();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getComments <em>Comments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Comments</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getComments()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_Comments();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getInterfaces <em>Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interfaces</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getInterfaces()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_Interfaces();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getSubComponents <em>Sub Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Components</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getSubComponents()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_SubComponents();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Content</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getContent()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_Content();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getAttributesController <em>Attributes Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Attributes Controller</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getAttributesController()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_AttributesController();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedAttributesController <em>Merged Attributes Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Merged Attributes Controller</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedAttributesController()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_MergedAttributesController();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getBindings <em>Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Bindings</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getBindings()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_Bindings();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedBindings <em>Merged Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Merged Bindings</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedBindings()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_MergedBindings();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getController <em>Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Controller</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getController()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_Controller();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getTemplateController <em>Template Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Template Controller</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getTemplateController()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_TemplateController();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedTemplateController <em>Merged Template Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Merged Template Controller</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedTemplateController()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_MergedTemplateController();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getLogger <em>Logger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Logger</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getLogger()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_Logger();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedLogger <em>Merged Logger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Merged Logger</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedLogger()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_MergedLogger();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getVirtualNode <em>Virtual Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Virtual Node</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getVirtualNode()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_VirtualNode();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedVirtualNode <em>Merged Virtual Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Merged Virtual Node</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedVirtualNode()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_MergedVirtualNode();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getCoordinates <em>Coordinates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Coordinates</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getCoordinates()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_Coordinates();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getAny <em>Any</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getAny()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EAttribute getAbstractComponent_Any();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getAnyAttributes <em>Any Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any Attributes</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getAnyAttributes()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EAttribute getAbstractComponent_AnyAttributes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedInterfaces <em>Merged Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Merged Interfaces</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedInterfaces()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_MergedInterfaces();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedSubComponents <em>Merged Sub Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Merged Sub Components</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedSubComponents()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_MergedSubComponents();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedContent <em>Merged Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Merged Content</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedContent()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_MergedContent();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedController <em>Merged Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Merged Controller</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AbstractComponent#getMergedController()
	 * @see #getAbstractComponent()
	 * @generated
	 */
	EReference getAbstractComponent_MergedController();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Parameter
	 * @generated
	 */
	EClass getParameter();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.Parameter#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Name</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Parameter#getName()
	 * @see #getParameter()
	 * @generated
	 */
	EReference getParameter_Name();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.Parameter#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Parameter#getValue()
	 * @see #getParameter()
	 * @generated
	 */
	EReference getParameter_Value();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.Value <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Value
	 * @generated
	 */
	EClass getValue();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.Value#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Value#getElements()
	 * @see #getValue()
	 * @generated
	 */
	EReference getValue_Elements();

	/**
	 * Returns the meta object for the reference '{@link org.ow2.fractal.f4e.fractal.Value#getContextDefinition <em>Context Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Context Definition</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Value#getContextDefinition()
	 * @see #getValue()
	 * @generated
	 */
	EReference getValue_ContextDefinition();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.ValueElement <em>Value Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Element</em>'.
	 * @see org.ow2.fractal.f4e.fractal.ValueElement
	 * @generated
	 */
	EClass getValueElement();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.Constant <em>Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constant</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Constant
	 * @generated
	 */
	EClass getConstant();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Constant#getConstant <em>Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constant</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Constant#getConstant()
	 * @see #getConstant()
	 * @generated
	 */
	EAttribute getConstant_Constant();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.Reference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Reference
	 * @generated
	 */
	EClass getReference();

	/**
	 * Returns the meta object for the reference '{@link org.ow2.fractal.f4e.fractal.Reference#getReference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reference</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Reference#getReference()
	 * @see #getReference()
	 * @generated
	 */
	EReference getReference_Reference();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Reference#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Reference#getName()
	 * @see #getReference()
	 * @generated
	 */
	EAttribute getReference_Name();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.FormalParameter <em>Formal Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Formal Parameter</em>'.
	 * @see org.ow2.fractal.f4e.fractal.FormalParameter
	 * @generated
	 */
	EClass getFormalParameter();

	/**
	 * Returns the meta object for the container reference '{@link org.ow2.fractal.f4e.fractal.FormalParameter#getDefinition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Definition</em>'.
	 * @see org.ow2.fractal.f4e.fractal.FormalParameter#getDefinition()
	 * @see #getFormalParameter()
	 * @generated
	 */
	EReference getFormalParameter_Definition();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.Definition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Definition</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Definition
	 * @generated
	 */
	EClass getDefinition();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Definition#getExtends <em>Extends</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Extends</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Definition#getExtends()
	 * @see #getDefinition()
	 * @generated
	 */
	EAttribute getDefinition_Extends();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Definition#getArguments <em>Arguments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Arguments</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Definition#getArguments()
	 * @see #getDefinition()
	 * @generated
	 */
	EAttribute getDefinition_Arguments();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.Definition#getArgumentsAST <em>Arguments AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Arguments AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Definition#getArgumentsAST()
	 * @see #getDefinition()
	 * @generated
	 */
	EReference getDefinition_ArgumentsAST();

	/**
	 * Returns the meta object for the container reference '{@link org.ow2.fractal.f4e.fractal.Definition#getSystem <em>System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>System</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Definition#getSystem()
	 * @see #getDefinition()
	 * @generated
	 */
	EReference getDefinition_System();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.DefinitionCall <em>Definition Call</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Definition Call</em>'.
	 * @see org.ow2.fractal.f4e.fractal.DefinitionCall
	 * @generated
	 */
	EClass getDefinitionCall();

	/**
	 * Returns the meta object for the reference '{@link org.ow2.fractal.f4e.fractal.DefinitionCall#getDefinition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Definition</em>'.
	 * @see org.ow2.fractal.f4e.fractal.DefinitionCall#getDefinition()
	 * @see #getDefinitionCall()
	 * @generated
	 */
	EReference getDefinitionCall_Definition();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.DefinitionCall#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see org.ow2.fractal.f4e.fractal.DefinitionCall#getParameters()
	 * @see #getDefinitionCall()
	 * @generated
	 */
	EReference getDefinitionCall_Parameters();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.DefinitionCall#getDefinitionName <em>Definition Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Definition Name</em>'.
	 * @see org.ow2.fractal.f4e.fractal.DefinitionCall#getDefinitionName()
	 * @see #getDefinitionCall()
	 * @generated
	 */
	EReference getDefinitionCall_DefinitionName();

	/**
	 * Returns the meta object for the container reference '{@link org.ow2.fractal.f4e.fractal.DefinitionCall#getAbstractComponent <em>Abstract Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Abstract Component</em>'.
	 * @see org.ow2.fractal.f4e.fractal.DefinitionCall#getAbstractComponent()
	 * @see #getDefinitionCall()
	 * @generated
	 */
	EReference getDefinitionCall_AbstractComponent();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.EffectiveParameter <em>Effective Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Effective Parameter</em>'.
	 * @see org.ow2.fractal.f4e.fractal.EffectiveParameter
	 * @generated
	 */
	EClass getEffectiveParameter();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.Interface <em>Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interface</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Interface
	 * @generated
	 */
	EClass getInterface();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Interface#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Interface#getName()
	 * @see #getInterface()
	 * @generated
	 */
	EAttribute getInterface_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Interface#getSignature <em>Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Signature</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Interface#getSignature()
	 * @see #getInterface()
	 * @generated
	 */
	EAttribute getInterface_Signature();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Interface#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Role</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Interface#getRole()
	 * @see #getInterface()
	 * @generated
	 */
	EAttribute getInterface_Role();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Interface#getCardinality <em>Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cardinality</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Interface#getCardinality()
	 * @see #getInterface()
	 * @generated
	 */
	EAttribute getInterface_Cardinality();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Interface#getContingency <em>Contingency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Contingency</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Interface#getContingency()
	 * @see #getInterface()
	 * @generated
	 */
	EAttribute getInterface_Contingency();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.Interface#getNameAST <em>Name AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Name AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Interface#getNameAST()
	 * @see #getInterface()
	 * @generated
	 */
	EReference getInterface_NameAST();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.Interface#getSignatureAST <em>Signature AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Signature AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Interface#getSignatureAST()
	 * @see #getInterface()
	 * @generated
	 */
	EReference getInterface_SignatureAST();

	/**
	 * Returns the meta object for the container reference '{@link org.ow2.fractal.f4e.fractal.Interface#getAbstractComponent <em>Abstract Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Abstract Component</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Interface#getAbstractComponent()
	 * @see #getInterface()
	 * @generated
	 */
	EReference getInterface_AbstractComponent();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.Interface#getComments <em>Comments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Comments</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Interface#getComments()
	 * @see #getInterface()
	 * @generated
	 */
	EReference getInterface_Comments();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Interface#getMergedName <em>Merged Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Merged Name</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Interface#getMergedName()
	 * @see #getInterface()
	 * @generated
	 */
	EAttribute getInterface_MergedName();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Interface#getMergedSignature <em>Merged Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Merged Signature</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Interface#getMergedSignature()
	 * @see #getInterface()
	 * @generated
	 */
	EAttribute getInterface_MergedSignature();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Interface#getMergedRole <em>Merged Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Merged Role</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Interface#getMergedRole()
	 * @see #getInterface()
	 * @generated
	 */
	EAttribute getInterface_MergedRole();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Interface#getMergedCardinality <em>Merged Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Merged Cardinality</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Interface#getMergedCardinality()
	 * @see #getInterface()
	 * @generated
	 */
	EAttribute getInterface_MergedCardinality();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Interface#getMergedContingency <em>Merged Contingency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Merged Contingency</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Interface#getMergedContingency()
	 * @see #getInterface()
	 * @generated
	 */
	EAttribute getInterface_MergedContingency();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.Interface#getAny <em>Any</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Interface#getAny()
	 * @see #getInterface()
	 * @generated
	 */
	EAttribute getInterface_Any();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.Interface#getAnyAttributes <em>Any Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any Attributes</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Interface#getAnyAttributes()
	 * @see #getInterface()
	 * @generated
	 */
	EAttribute getInterface_AnyAttributes();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.Component <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Component
	 * @generated
	 */
	EClass getComponent();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Component#getDefinition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Definition</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Component#getDefinition()
	 * @see #getComponent()
	 * @generated
	 */
	EAttribute getComponent_Definition();

	/**
	 * Returns the meta object for the reference '{@link org.ow2.fractal.f4e.fractal.Component#getShared <em>Shared</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Shared</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Component#getShared()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Shared();

	/**
	 * Returns the meta object for the container reference '{@link org.ow2.fractal.f4e.fractal.Component#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Component#getParent()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Parent();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.Content <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Content</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Content
	 * @generated
	 */
	EClass getContent();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Content#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Class</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Content#getClass_()
	 * @see #getContent()
	 * @generated
	 */
	EAttribute getContent_Class();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.Content#getClassAST <em>Class AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Content#getClassAST()
	 * @see #getContent()
	 * @generated
	 */
	EReference getContent_ClassAST();

	/**
	 * Returns the meta object for the container reference '{@link org.ow2.fractal.f4e.fractal.Content#getAbstractComponent <em>Abstract Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Abstract Component</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Content#getAbstractComponent()
	 * @see #getContent()
	 * @generated
	 */
	EReference getContent_AbstractComponent();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.Content#getComments <em>Comments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Comments</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Content#getComments()
	 * @see #getContent()
	 * @generated
	 */
	EReference getContent_Comments();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Content#getMergedClass <em>Merged Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Merged Class</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Content#getMergedClass()
	 * @see #getContent()
	 * @generated
	 */
	EAttribute getContent_MergedClass();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.Content#getAny <em>Any</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Content#getAny()
	 * @see #getContent()
	 * @generated
	 */
	EAttribute getContent_Any();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.Content#getAnyAttributes <em>Any Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any Attributes</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Content#getAnyAttributes()
	 * @see #getContent()
	 * @generated
	 */
	EAttribute getContent_AnyAttributes();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.AttributesController <em>Attributes Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attributes Controller</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AttributesController
	 * @generated
	 */
	EClass getAttributesController();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.AttributesController#getSignature <em>Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Signature</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AttributesController#getSignature()
	 * @see #getAttributesController()
	 * @generated
	 */
	EAttribute getAttributesController_Signature();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.AttributesController#getAttributes <em>Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attributes</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AttributesController#getAttributes()
	 * @see #getAttributesController()
	 * @generated
	 */
	EReference getAttributesController_Attributes();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.AttributesController#getSignatureAST <em>Signature AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Signature AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AttributesController#getSignatureAST()
	 * @see #getAttributesController()
	 * @generated
	 */
	EReference getAttributesController_SignatureAST();

	/**
	 * Returns the meta object for the container reference '{@link org.ow2.fractal.f4e.fractal.AttributesController#getAbstractComponent <em>Abstract Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Abstract Component</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AttributesController#getAbstractComponent()
	 * @see #getAttributesController()
	 * @generated
	 */
	EReference getAttributesController_AbstractComponent();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.AttributesController#getComments <em>Comments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Comments</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AttributesController#getComments()
	 * @see #getAttributesController()
	 * @generated
	 */
	EReference getAttributesController_Comments();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.AttributesController#getMergedAttributes <em>Merged Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Merged Attributes</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AttributesController#getMergedAttributes()
	 * @see #getAttributesController()
	 * @generated
	 */
	EReference getAttributesController_MergedAttributes();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.AttributesController#getMergedSignature <em>Merged Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Merged Signature</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AttributesController#getMergedSignature()
	 * @see #getAttributesController()
	 * @generated
	 */
	EAttribute getAttributesController_MergedSignature();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.AttributesController#getAny <em>Any</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AttributesController#getAny()
	 * @see #getAttributesController()
	 * @generated
	 */
	EAttribute getAttributesController_Any();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.AttributesController#getAnyAttributes <em>Any Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any Attributes</em>'.
	 * @see org.ow2.fractal.f4e.fractal.AttributesController#getAnyAttributes()
	 * @see #getAttributesController()
	 * @generated
	 */
	EAttribute getAttributesController_AnyAttributes();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.Attribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Attribute
	 * @generated
	 */
	EClass getAttribute();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Attribute#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Attribute#getName()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Attribute#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Attribute#getValue()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_Value();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.Attribute#getNameAST <em>Name AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Name AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Attribute#getNameAST()
	 * @see #getAttribute()
	 * @generated
	 */
	EReference getAttribute_NameAST();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.Attribute#getValueAST <em>Value AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Attribute#getValueAST()
	 * @see #getAttribute()
	 * @generated
	 */
	EReference getAttribute_ValueAST();

	/**
	 * Returns the meta object for the container reference '{@link org.ow2.fractal.f4e.fractal.Attribute#getAttributesController <em>Attributes Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Attributes Controller</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Attribute#getAttributesController()
	 * @see #getAttribute()
	 * @generated
	 */
	EReference getAttribute_AttributesController();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.Attribute#getComments <em>Comments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Comments</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Attribute#getComments()
	 * @see #getAttribute()
	 * @generated
	 */
	EReference getAttribute_Comments();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Attribute#getMergedName <em>Merged Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Merged Name</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Attribute#getMergedName()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_MergedName();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Attribute#getMergedValue <em>Merged Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Merged Value</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Attribute#getMergedValue()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_MergedValue();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.Attribute#getAny <em>Any</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Attribute#getAny()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_Any();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.Attribute#getAnyAttributes <em>Any Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any Attributes</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Attribute#getAnyAttributes()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_AnyAttributes();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.Binding <em>Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binding</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Binding
	 * @generated
	 */
	EClass getBinding();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Binding#getClient <em>Client</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Client</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Binding#getClient()
	 * @see #getBinding()
	 * @generated
	 */
	EAttribute getBinding_Client();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Binding#getServer <em>Server</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Server</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Binding#getServer()
	 * @see #getBinding()
	 * @generated
	 */
	EAttribute getBinding_Server();

	/**
	 * Returns the meta object for the reference '{@link org.ow2.fractal.f4e.fractal.Binding#getClientInterface <em>Client Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Client Interface</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Binding#getClientInterface()
	 * @see #getBinding()
	 * @generated
	 */
	EReference getBinding_ClientInterface();

	/**
	 * Returns the meta object for the reference '{@link org.ow2.fractal.f4e.fractal.Binding#getServerInterface <em>Server Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Server Interface</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Binding#getServerInterface()
	 * @see #getBinding()
	 * @generated
	 */
	EReference getBinding_ServerInterface();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.Binding#getClientAST <em>Client AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Client AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Binding#getClientAST()
	 * @see #getBinding()
	 * @generated
	 */
	EReference getBinding_ClientAST();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.Binding#getServerAST <em>Server AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Server AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Binding#getServerAST()
	 * @see #getBinding()
	 * @generated
	 */
	EReference getBinding_ServerAST();

	/**
	 * Returns the meta object for the container reference '{@link org.ow2.fractal.f4e.fractal.Binding#getAbstractComponent <em>Abstract Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Abstract Component</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Binding#getAbstractComponent()
	 * @see #getBinding()
	 * @generated
	 */
	EReference getBinding_AbstractComponent();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.Binding#getComments <em>Comments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Comments</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Binding#getComments()
	 * @see #getBinding()
	 * @generated
	 */
	EReference getBinding_Comments();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Binding#getMergedClient <em>Merged Client</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Merged Client</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Binding#getMergedClient()
	 * @see #getBinding()
	 * @generated
	 */
	EAttribute getBinding_MergedClient();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Binding#getMergedServer <em>Merged Server</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Merged Server</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Binding#getMergedServer()
	 * @see #getBinding()
	 * @generated
	 */
	EAttribute getBinding_MergedServer();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.Binding#getAny <em>Any</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Binding#getAny()
	 * @see #getBinding()
	 * @generated
	 */
	EAttribute getBinding_Any();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.Binding#getAnyAttributes <em>Any Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any Attributes</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Binding#getAnyAttributes()
	 * @see #getBinding()
	 * @generated
	 */
	EAttribute getBinding_AnyAttributes();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.Controller <em>Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Controller</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Controller
	 * @generated
	 */
	EClass getController();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Controller#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Controller#getType()
	 * @see #getController()
	 * @generated
	 */
	EAttribute getController_Type();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.Controller#getTypeAST <em>Type AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Controller#getTypeAST()
	 * @see #getController()
	 * @generated
	 */
	EReference getController_TypeAST();

	/**
	 * Returns the meta object for the container reference '{@link org.ow2.fractal.f4e.fractal.Controller#getAbstractComponent <em>Abstract Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Abstract Component</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Controller#getAbstractComponent()
	 * @see #getController()
	 * @generated
	 */
	EReference getController_AbstractComponent();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.Controller#getComments <em>Comments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Comments</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Controller#getComments()
	 * @see #getController()
	 * @generated
	 */
	EReference getController_Comments();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Controller#getMergedType <em>Merged Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Merged Type</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Controller#getMergedType()
	 * @see #getController()
	 * @generated
	 */
	EAttribute getController_MergedType();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.Controller#getAny <em>Any</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Controller#getAny()
	 * @see #getController()
	 * @generated
	 */
	EAttribute getController_Any();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.Controller#getAnyAttributes <em>Any Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any Attributes</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Controller#getAnyAttributes()
	 * @see #getController()
	 * @generated
	 */
	EAttribute getController_AnyAttributes();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.TemplateController <em>Template Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Template Controller</em>'.
	 * @see org.ow2.fractal.f4e.fractal.TemplateController
	 * @generated
	 */
	EClass getTemplateController();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.TemplateController#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.ow2.fractal.f4e.fractal.TemplateController#getType()
	 * @see #getTemplateController()
	 * @generated
	 */
	EAttribute getTemplateController_Type();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.TemplateController#getTypeAST <em>Type AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.TemplateController#getTypeAST()
	 * @see #getTemplateController()
	 * @generated
	 */
	EReference getTemplateController_TypeAST();

	/**
	 * Returns the meta object for the reference '{@link org.ow2.fractal.f4e.fractal.TemplateController#getAbstractComponent <em>Abstract Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Abstract Component</em>'.
	 * @see org.ow2.fractal.f4e.fractal.TemplateController#getAbstractComponent()
	 * @see #getTemplateController()
	 * @generated
	 */
	EReference getTemplateController_AbstractComponent();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.TemplateController#getComments <em>Comments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Comments</em>'.
	 * @see org.ow2.fractal.f4e.fractal.TemplateController#getComments()
	 * @see #getTemplateController()
	 * @generated
	 */
	EReference getTemplateController_Comments();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.TemplateController#getMergedType <em>Merged Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Merged Type</em>'.
	 * @see org.ow2.fractal.f4e.fractal.TemplateController#getMergedType()
	 * @see #getTemplateController()
	 * @generated
	 */
	EAttribute getTemplateController_MergedType();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.TemplateController#getAny <em>Any</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any</em>'.
	 * @see org.ow2.fractal.f4e.fractal.TemplateController#getAny()
	 * @see #getTemplateController()
	 * @generated
	 */
	EAttribute getTemplateController_Any();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.TemplateController#getAnyAttributes <em>Any Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any Attributes</em>'.
	 * @see org.ow2.fractal.f4e.fractal.TemplateController#getAnyAttributes()
	 * @see #getTemplateController()
	 * @generated
	 */
	EAttribute getTemplateController_AnyAttributes();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.Comment <em>Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Comment</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Comment
	 * @generated
	 */
	EClass getComment();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Comment#getLanguage <em>Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Language</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Comment#getLanguage()
	 * @see #getComment()
	 * @generated
	 */
	EAttribute getComment_Language();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Comment#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Comment#getText()
	 * @see #getComment()
	 * @generated
	 */
	EAttribute getComment_Text();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.Comment#getLanguageAST <em>Language AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Language AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Comment#getLanguageAST()
	 * @see #getComment()
	 * @generated
	 */
	EReference getComment_LanguageAST();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.Comment#getTextAST <em>Text AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Text AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Comment#getTextAST()
	 * @see #getComment()
	 * @generated
	 */
	EReference getComment_TextAST();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.Comment#getAny <em>Any</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Comment#getAny()
	 * @see #getComment()
	 * @generated
	 */
	EAttribute getComment_Any();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.Comment#getAnyAttributes <em>Any Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any Attributes</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Comment#getAnyAttributes()
	 * @see #getComment()
	 * @generated
	 */
	EAttribute getComment_AnyAttributes();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.Coordinates <em>Coordinates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Coordinates</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Coordinates
	 * @generated
	 */
	EClass getCoordinates();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Coordinates#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Coordinates#getName()
	 * @see #getCoordinates()
	 * @generated
	 */
	EAttribute getCoordinates_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Coordinates#getX0 <em>X0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X0</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Coordinates#getX0()
	 * @see #getCoordinates()
	 * @generated
	 */
	EAttribute getCoordinates_X0();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Coordinates#getX1 <em>X1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X1</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Coordinates#getX1()
	 * @see #getCoordinates()
	 * @generated
	 */
	EAttribute getCoordinates_X1();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Coordinates#getY0 <em>Y0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y0</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Coordinates#getY0()
	 * @see #getCoordinates()
	 * @generated
	 */
	EAttribute getCoordinates_Y0();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Coordinates#getY1 <em>Y1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y1</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Coordinates#getY1()
	 * @see #getCoordinates()
	 * @generated
	 */
	EAttribute getCoordinates_Y1();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Coordinates#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Color</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Coordinates#getColor()
	 * @see #getCoordinates()
	 * @generated
	 */
	EAttribute getCoordinates_Color();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.Coordinates#getNameAST <em>Name AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Name AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Coordinates#getNameAST()
	 * @see #getCoordinates()
	 * @generated
	 */
	EReference getCoordinates_NameAST();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.Coordinates#getX0AST <em>X0AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>X0AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Coordinates#getX0AST()
	 * @see #getCoordinates()
	 * @generated
	 */
	EReference getCoordinates_X0AST();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.Coordinates#getX1AST <em>X1AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>X1AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Coordinates#getX1AST()
	 * @see #getCoordinates()
	 * @generated
	 */
	EReference getCoordinates_X1AST();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.Coordinates#getY0AST <em>Y0AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Y0AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Coordinates#getY0AST()
	 * @see #getCoordinates()
	 * @generated
	 */
	EReference getCoordinates_Y0AST();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.Coordinates#getY1AST <em>Y1AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Y1AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Coordinates#getY1AST()
	 * @see #getCoordinates()
	 * @generated
	 */
	EReference getCoordinates_Y1AST();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.Coordinates#getColorAST <em>Color AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Color AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Coordinates#getColorAST()
	 * @see #getCoordinates()
	 * @generated
	 */
	EReference getCoordinates_ColorAST();

	/**
	 * Returns the meta object for the containment reference list '{@link org.ow2.fractal.f4e.fractal.Coordinates#getCoordinates <em>Coordinates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Coordinates</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Coordinates#getCoordinates()
	 * @see #getCoordinates()
	 * @generated
	 */
	EReference getCoordinates_Coordinates();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.Coordinates#getAny <em>Any</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Coordinates#getAny()
	 * @see #getCoordinates()
	 * @generated
	 */
	EAttribute getCoordinates_Any();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.Coordinates#getAnyAttributes <em>Any Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any Attributes</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Coordinates#getAnyAttributes()
	 * @see #getCoordinates()
	 * @generated
	 */
	EAttribute getCoordinates_AnyAttributes();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.Logger <em>Logger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Logger</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Logger
	 * @generated
	 */
	EClass getLogger();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Logger#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Logger#getName()
	 * @see #getLogger()
	 * @generated
	 */
	EAttribute getLogger_Name();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.Logger#getNameAST <em>Name AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Name AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Logger#getNameAST()
	 * @see #getLogger()
	 * @generated
	 */
	EReference getLogger_NameAST();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.Logger#getMergedName <em>Merged Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Merged Name</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Logger#getMergedName()
	 * @see #getLogger()
	 * @generated
	 */
	EAttribute getLogger_MergedName();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.Logger#getAny <em>Any</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Logger#getAny()
	 * @see #getLogger()
	 * @generated
	 */
	EAttribute getLogger_Any();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.Logger#getAnyAttributes <em>Any Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any Attributes</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Logger#getAnyAttributes()
	 * @see #getLogger()
	 * @generated
	 */
	EAttribute getLogger_AnyAttributes();

	/**
	 * Returns the meta object for class '{@link org.ow2.fractal.f4e.fractal.VirtualNode <em>Virtual Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Virtual Node</em>'.
	 * @see org.ow2.fractal.f4e.fractal.VirtualNode
	 * @generated
	 */
	EClass getVirtualNode();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.VirtualNode#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.ow2.fractal.f4e.fractal.VirtualNode#getName()
	 * @see #getVirtualNode()
	 * @generated
	 */
	EAttribute getVirtualNode_Name();

	/**
	 * Returns the meta object for the containment reference '{@link org.ow2.fractal.f4e.fractal.VirtualNode#getNameAST <em>Name AST</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Name AST</em>'.
	 * @see org.ow2.fractal.f4e.fractal.VirtualNode#getNameAST()
	 * @see #getVirtualNode()
	 * @generated
	 */
	EReference getVirtualNode_NameAST();

	/**
	 * Returns the meta object for the attribute '{@link org.ow2.fractal.f4e.fractal.VirtualNode#getMergedName <em>Merged Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Merged Name</em>'.
	 * @see org.ow2.fractal.f4e.fractal.VirtualNode#getMergedName()
	 * @see #getVirtualNode()
	 * @generated
	 */
	EAttribute getVirtualNode_MergedName();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.VirtualNode#getAny <em>Any</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any</em>'.
	 * @see org.ow2.fractal.f4e.fractal.VirtualNode#getAny()
	 * @see #getVirtualNode()
	 * @generated
	 */
	EAttribute getVirtualNode_Any();

	/**
	 * Returns the meta object for the attribute list '{@link org.ow2.fractal.f4e.fractal.VirtualNode#getAnyAttributes <em>Any Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any Attributes</em>'.
	 * @see org.ow2.fractal.f4e.fractal.VirtualNode#getAnyAttributes()
	 * @see #getVirtualNode()
	 * @generated
	 */
	EAttribute getVirtualNode_AnyAttributes();

	/**
	 * Returns the meta object for enum '{@link org.ow2.fractal.f4e.fractal.Cardinality <em>Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Cardinality</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Cardinality
	 * @generated
	 */
	EEnum getCardinality();

	/**
	 * Returns the meta object for enum '{@link org.ow2.fractal.f4e.fractal.Role <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Role</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Role
	 * @generated
	 */
	EEnum getRole();

	/**
	 * Returns the meta object for enum '{@link org.ow2.fractal.f4e.fractal.Contingency <em>Contingency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Contingency</em>'.
	 * @see org.ow2.fractal.f4e.fractal.Contingency
	 * @generated
	 */
	EEnum getContingency();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FractalFactory getFractalFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.SystemImpl <em>System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.SystemImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getSystem()
		 * @generated
		 */
		EClass SYSTEM = eINSTANCE.getSystem();

		/**
		 * The meta object literal for the '<em><b>Definition Calls</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM__DEFINITION_CALLS = eINSTANCE.getSystem_DefinitionCalls();

		/**
		 * The meta object literal for the '<em><b>Pamameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM__PAMAMETERS = eINSTANCE.getSystem_Pamameters();

		/**
		 * The meta object literal for the '<em><b>Definitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM__DEFINITIONS = eINSTANCE.getSystem_Definitions();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl <em>Abstract Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.AbstractComponentImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getAbstractComponent()
		 * @generated
		 */
		EClass ABSTRACT_COMPONENT = eINSTANCE.getAbstractComponent();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_COMPONENT__NAME = eINSTANCE.getAbstractComponent_Name();

		/**
		 * The meta object literal for the '<em><b>Merged Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_COMPONENT__MERGED_NAME = eINSTANCE.getAbstractComponent_MergedName();

		/**
		 * The meta object literal for the '<em><b>Name AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__NAME_AST = eINSTANCE.getAbstractComponent_NameAST();

		/**
		 * The meta object literal for the '<em><b>Extends AST</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__EXTENDS_AST = eINSTANCE.getAbstractComponent_ExtendsAST();

		/**
		 * The meta object literal for the '<em><b>Comments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__COMMENTS = eINSTANCE.getAbstractComponent_Comments();

		/**
		 * The meta object literal for the '<em><b>Interfaces</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__INTERFACES = eINSTANCE.getAbstractComponent_Interfaces();

		/**
		 * The meta object literal for the '<em><b>Sub Components</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__SUB_COMPONENTS = eINSTANCE.getAbstractComponent_SubComponents();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__CONTENT = eINSTANCE.getAbstractComponent_Content();

		/**
		 * The meta object literal for the '<em><b>Attributes Controller</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER = eINSTANCE.getAbstractComponent_AttributesController();

		/**
		 * The meta object literal for the '<em><b>Merged Attributes Controller</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER = eINSTANCE.getAbstractComponent_MergedAttributesController();

		/**
		 * The meta object literal for the '<em><b>Bindings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__BINDINGS = eINSTANCE.getAbstractComponent_Bindings();

		/**
		 * The meta object literal for the '<em><b>Merged Bindings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__MERGED_BINDINGS = eINSTANCE.getAbstractComponent_MergedBindings();

		/**
		 * The meta object literal for the '<em><b>Controller</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__CONTROLLER = eINSTANCE.getAbstractComponent_Controller();

		/**
		 * The meta object literal for the '<em><b>Template Controller</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__TEMPLATE_CONTROLLER = eINSTANCE.getAbstractComponent_TemplateController();

		/**
		 * The meta object literal for the '<em><b>Merged Template Controller</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__MERGED_TEMPLATE_CONTROLLER = eINSTANCE.getAbstractComponent_MergedTemplateController();

		/**
		 * The meta object literal for the '<em><b>Logger</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__LOGGER = eINSTANCE.getAbstractComponent_Logger();

		/**
		 * The meta object literal for the '<em><b>Merged Logger</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__MERGED_LOGGER = eINSTANCE.getAbstractComponent_MergedLogger();

		/**
		 * The meta object literal for the '<em><b>Virtual Node</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__VIRTUAL_NODE = eINSTANCE.getAbstractComponent_VirtualNode();

		/**
		 * The meta object literal for the '<em><b>Merged Virtual Node</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__MERGED_VIRTUAL_NODE = eINSTANCE.getAbstractComponent_MergedVirtualNode();

		/**
		 * The meta object literal for the '<em><b>Coordinates</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__COORDINATES = eINSTANCE.getAbstractComponent_Coordinates();

		/**
		 * The meta object literal for the '<em><b>Any</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_COMPONENT__ANY = eINSTANCE.getAbstractComponent_Any();

		/**
		 * The meta object literal for the '<em><b>Any Attributes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_COMPONENT__ANY_ATTRIBUTES = eINSTANCE.getAbstractComponent_AnyAttributes();

		/**
		 * The meta object literal for the '<em><b>Merged Interfaces</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__MERGED_INTERFACES = eINSTANCE.getAbstractComponent_MergedInterfaces();

		/**
		 * The meta object literal for the '<em><b>Merged Sub Components</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS = eINSTANCE.getAbstractComponent_MergedSubComponents();

		/**
		 * The meta object literal for the '<em><b>Merged Content</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__MERGED_CONTENT = eINSTANCE.getAbstractComponent_MergedContent();

		/**
		 * The meta object literal for the '<em><b>Merged Controller</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPONENT__MERGED_CONTROLLER = eINSTANCE.getAbstractComponent_MergedController();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.ParameterImpl <em>Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.ParameterImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getParameter()
		 * @generated
		 */
		EClass PARAMETER = eINSTANCE.getParameter();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER__NAME = eINSTANCE.getParameter_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER__VALUE = eINSTANCE.getParameter_Value();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.ValueImpl <em>Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.ValueImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getValue()
		 * @generated
		 */
		EClass VALUE = eINSTANCE.getValue();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUE__ELEMENTS = eINSTANCE.getValue_Elements();

		/**
		 * The meta object literal for the '<em><b>Context Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VALUE__CONTEXT_DEFINITION = eINSTANCE.getValue_ContextDefinition();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.ValueElementImpl <em>Value Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.ValueElementImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getValueElement()
		 * @generated
		 */
		EClass VALUE_ELEMENT = eINSTANCE.getValueElement();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.ConstantImpl <em>Constant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.ConstantImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getConstant()
		 * @generated
		 */
		EClass CONSTANT = eINSTANCE.getConstant();

		/**
		 * The meta object literal for the '<em><b>Constant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT__CONSTANT = eINSTANCE.getConstant_Constant();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.ReferenceImpl <em>Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.ReferenceImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getReference()
		 * @generated
		 */
		EClass REFERENCE = eINSTANCE.getReference();

		/**
		 * The meta object literal for the '<em><b>Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE__REFERENCE = eINSTANCE.getReference_Reference();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REFERENCE__NAME = eINSTANCE.getReference_Name();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.FormalParameterImpl <em>Formal Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.FormalParameterImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getFormalParameter()
		 * @generated
		 */
		EClass FORMAL_PARAMETER = eINSTANCE.getFormalParameter();

		/**
		 * The meta object literal for the '<em><b>Definition</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORMAL_PARAMETER__DEFINITION = eINSTANCE.getFormalParameter_Definition();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.DefinitionImpl <em>Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.DefinitionImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getDefinition()
		 * @generated
		 */
		EClass DEFINITION = eINSTANCE.getDefinition();

		/**
		 * The meta object literal for the '<em><b>Extends</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEFINITION__EXTENDS = eINSTANCE.getDefinition_Extends();

		/**
		 * The meta object literal for the '<em><b>Arguments</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEFINITION__ARGUMENTS = eINSTANCE.getDefinition_Arguments();

		/**
		 * The meta object literal for the '<em><b>Arguments AST</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFINITION__ARGUMENTS_AST = eINSTANCE.getDefinition_ArgumentsAST();

		/**
		 * The meta object literal for the '<em><b>System</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFINITION__SYSTEM = eINSTANCE.getDefinition_System();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.DefinitionCallImpl <em>Definition Call</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.DefinitionCallImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getDefinitionCall()
		 * @generated
		 */
		EClass DEFINITION_CALL = eINSTANCE.getDefinitionCall();

		/**
		 * The meta object literal for the '<em><b>Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFINITION_CALL__DEFINITION = eINSTANCE.getDefinitionCall_Definition();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFINITION_CALL__PARAMETERS = eINSTANCE.getDefinitionCall_Parameters();

		/**
		 * The meta object literal for the '<em><b>Definition Name</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFINITION_CALL__DEFINITION_NAME = eINSTANCE.getDefinitionCall_DefinitionName();

		/**
		 * The meta object literal for the '<em><b>Abstract Component</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEFINITION_CALL__ABSTRACT_COMPONENT = eINSTANCE.getDefinitionCall_AbstractComponent();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.EffectiveParameterImpl <em>Effective Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.EffectiveParameterImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getEffectiveParameter()
		 * @generated
		 */
		EClass EFFECTIVE_PARAMETER = eINSTANCE.getEffectiveParameter();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.InterfaceImpl <em>Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.InterfaceImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getInterface()
		 * @generated
		 */
		EClass INTERFACE = eINSTANCE.getInterface();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERFACE__NAME = eINSTANCE.getInterface_Name();

		/**
		 * The meta object literal for the '<em><b>Signature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERFACE__SIGNATURE = eINSTANCE.getInterface_Signature();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERFACE__ROLE = eINSTANCE.getInterface_Role();

		/**
		 * The meta object literal for the '<em><b>Cardinality</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERFACE__CARDINALITY = eINSTANCE.getInterface_Cardinality();

		/**
		 * The meta object literal for the '<em><b>Contingency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERFACE__CONTINGENCY = eINSTANCE.getInterface_Contingency();

		/**
		 * The meta object literal for the '<em><b>Name AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE__NAME_AST = eINSTANCE.getInterface_NameAST();

		/**
		 * The meta object literal for the '<em><b>Signature AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE__SIGNATURE_AST = eINSTANCE.getInterface_SignatureAST();

		/**
		 * The meta object literal for the '<em><b>Abstract Component</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE__ABSTRACT_COMPONENT = eINSTANCE.getInterface_AbstractComponent();

		/**
		 * The meta object literal for the '<em><b>Comments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE__COMMENTS = eINSTANCE.getInterface_Comments();

		/**
		 * The meta object literal for the '<em><b>Merged Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERFACE__MERGED_NAME = eINSTANCE.getInterface_MergedName();

		/**
		 * The meta object literal for the '<em><b>Merged Signature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERFACE__MERGED_SIGNATURE = eINSTANCE.getInterface_MergedSignature();

		/**
		 * The meta object literal for the '<em><b>Merged Role</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERFACE__MERGED_ROLE = eINSTANCE.getInterface_MergedRole();

		/**
		 * The meta object literal for the '<em><b>Merged Cardinality</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERFACE__MERGED_CARDINALITY = eINSTANCE.getInterface_MergedCardinality();

		/**
		 * The meta object literal for the '<em><b>Merged Contingency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERFACE__MERGED_CONTINGENCY = eINSTANCE.getInterface_MergedContingency();

		/**
		 * The meta object literal for the '<em><b>Any</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERFACE__ANY = eINSTANCE.getInterface_Any();

		/**
		 * The meta object literal for the '<em><b>Any Attributes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERFACE__ANY_ATTRIBUTES = eINSTANCE.getInterface_AnyAttributes();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.ComponentImpl <em>Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.ComponentImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getComponent()
		 * @generated
		 */
		EClass COMPONENT = eINSTANCE.getComponent();

		/**
		 * The meta object literal for the '<em><b>Definition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENT__DEFINITION = eINSTANCE.getComponent_Definition();

		/**
		 * The meta object literal for the '<em><b>Shared</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__SHARED = eINSTANCE.getComponent_Shared();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__PARENT = eINSTANCE.getComponent_Parent();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.ContentImpl <em>Content</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.ContentImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getContent()
		 * @generated
		 */
		EClass CONTENT = eINSTANCE.getContent();

		/**
		 * The meta object literal for the '<em><b>Class</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTENT__CLASS = eINSTANCE.getContent_Class();

		/**
		 * The meta object literal for the '<em><b>Class AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTENT__CLASS_AST = eINSTANCE.getContent_ClassAST();

		/**
		 * The meta object literal for the '<em><b>Abstract Component</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTENT__ABSTRACT_COMPONENT = eINSTANCE.getContent_AbstractComponent();

		/**
		 * The meta object literal for the '<em><b>Comments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTENT__COMMENTS = eINSTANCE.getContent_Comments();

		/**
		 * The meta object literal for the '<em><b>Merged Class</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTENT__MERGED_CLASS = eINSTANCE.getContent_MergedClass();

		/**
		 * The meta object literal for the '<em><b>Any</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTENT__ANY = eINSTANCE.getContent_Any();

		/**
		 * The meta object literal for the '<em><b>Any Attributes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTENT__ANY_ATTRIBUTES = eINSTANCE.getContent_AnyAttributes();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.AttributesControllerImpl <em>Attributes Controller</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.AttributesControllerImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getAttributesController()
		 * @generated
		 */
		EClass ATTRIBUTES_CONTROLLER = eINSTANCE.getAttributesController();

		/**
		 * The meta object literal for the '<em><b>Signature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTES_CONTROLLER__SIGNATURE = eINSTANCE.getAttributesController_Signature();

		/**
		 * The meta object literal for the '<em><b>Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTES_CONTROLLER__ATTRIBUTES = eINSTANCE.getAttributesController_Attributes();

		/**
		 * The meta object literal for the '<em><b>Signature AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTES_CONTROLLER__SIGNATURE_AST = eINSTANCE.getAttributesController_SignatureAST();

		/**
		 * The meta object literal for the '<em><b>Abstract Component</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTES_CONTROLLER__ABSTRACT_COMPONENT = eINSTANCE.getAttributesController_AbstractComponent();

		/**
		 * The meta object literal for the '<em><b>Comments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTES_CONTROLLER__COMMENTS = eINSTANCE.getAttributesController_Comments();

		/**
		 * The meta object literal for the '<em><b>Merged Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTES_CONTROLLER__MERGED_ATTRIBUTES = eINSTANCE.getAttributesController_MergedAttributes();

		/**
		 * The meta object literal for the '<em><b>Merged Signature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTES_CONTROLLER__MERGED_SIGNATURE = eINSTANCE.getAttributesController_MergedSignature();

		/**
		 * The meta object literal for the '<em><b>Any</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTES_CONTROLLER__ANY = eINSTANCE.getAttributesController_Any();

		/**
		 * The meta object literal for the '<em><b>Any Attributes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTES_CONTROLLER__ANY_ATTRIBUTES = eINSTANCE.getAttributesController_AnyAttributes();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.AttributeImpl <em>Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.AttributeImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getAttribute()
		 * @generated
		 */
		EClass ATTRIBUTE = eINSTANCE.getAttribute();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__NAME = eINSTANCE.getAttribute_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__VALUE = eINSTANCE.getAttribute_Value();

		/**
		 * The meta object literal for the '<em><b>Name AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE__NAME_AST = eINSTANCE.getAttribute_NameAST();

		/**
		 * The meta object literal for the '<em><b>Value AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE__VALUE_AST = eINSTANCE.getAttribute_ValueAST();

		/**
		 * The meta object literal for the '<em><b>Attributes Controller</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE__ATTRIBUTES_CONTROLLER = eINSTANCE.getAttribute_AttributesController();

		/**
		 * The meta object literal for the '<em><b>Comments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE__COMMENTS = eINSTANCE.getAttribute_Comments();

		/**
		 * The meta object literal for the '<em><b>Merged Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__MERGED_NAME = eINSTANCE.getAttribute_MergedName();

		/**
		 * The meta object literal for the '<em><b>Merged Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__MERGED_VALUE = eINSTANCE.getAttribute_MergedValue();

		/**
		 * The meta object literal for the '<em><b>Any</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__ANY = eINSTANCE.getAttribute_Any();

		/**
		 * The meta object literal for the '<em><b>Any Attributes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__ANY_ATTRIBUTES = eINSTANCE.getAttribute_AnyAttributes();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.BindingImpl <em>Binding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.BindingImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getBinding()
		 * @generated
		 */
		EClass BINDING = eINSTANCE.getBinding();

		/**
		 * The meta object literal for the '<em><b>Client</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINDING__CLIENT = eINSTANCE.getBinding_Client();

		/**
		 * The meta object literal for the '<em><b>Server</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINDING__SERVER = eINSTANCE.getBinding_Server();

		/**
		 * The meta object literal for the '<em><b>Client Interface</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINDING__CLIENT_INTERFACE = eINSTANCE.getBinding_ClientInterface();

		/**
		 * The meta object literal for the '<em><b>Server Interface</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINDING__SERVER_INTERFACE = eINSTANCE.getBinding_ServerInterface();

		/**
		 * The meta object literal for the '<em><b>Client AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINDING__CLIENT_AST = eINSTANCE.getBinding_ClientAST();

		/**
		 * The meta object literal for the '<em><b>Server AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINDING__SERVER_AST = eINSTANCE.getBinding_ServerAST();

		/**
		 * The meta object literal for the '<em><b>Abstract Component</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINDING__ABSTRACT_COMPONENT = eINSTANCE.getBinding_AbstractComponent();

		/**
		 * The meta object literal for the '<em><b>Comments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINDING__COMMENTS = eINSTANCE.getBinding_Comments();

		/**
		 * The meta object literal for the '<em><b>Merged Client</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINDING__MERGED_CLIENT = eINSTANCE.getBinding_MergedClient();

		/**
		 * The meta object literal for the '<em><b>Merged Server</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINDING__MERGED_SERVER = eINSTANCE.getBinding_MergedServer();

		/**
		 * The meta object literal for the '<em><b>Any</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINDING__ANY = eINSTANCE.getBinding_Any();

		/**
		 * The meta object literal for the '<em><b>Any Attributes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINDING__ANY_ATTRIBUTES = eINSTANCE.getBinding_AnyAttributes();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.ControllerImpl <em>Controller</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.ControllerImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getController()
		 * @generated
		 */
		EClass CONTROLLER = eINSTANCE.getController();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTROLLER__TYPE = eINSTANCE.getController_Type();

		/**
		 * The meta object literal for the '<em><b>Type AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROLLER__TYPE_AST = eINSTANCE.getController_TypeAST();

		/**
		 * The meta object literal for the '<em><b>Abstract Component</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROLLER__ABSTRACT_COMPONENT = eINSTANCE.getController_AbstractComponent();

		/**
		 * The meta object literal for the '<em><b>Comments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROLLER__COMMENTS = eINSTANCE.getController_Comments();

		/**
		 * The meta object literal for the '<em><b>Merged Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTROLLER__MERGED_TYPE = eINSTANCE.getController_MergedType();

		/**
		 * The meta object literal for the '<em><b>Any</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTROLLER__ANY = eINSTANCE.getController_Any();

		/**
		 * The meta object literal for the '<em><b>Any Attributes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTROLLER__ANY_ATTRIBUTES = eINSTANCE.getController_AnyAttributes();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.TemplateControllerImpl <em>Template Controller</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.TemplateControllerImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getTemplateController()
		 * @generated
		 */
		EClass TEMPLATE_CONTROLLER = eINSTANCE.getTemplateController();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPLATE_CONTROLLER__TYPE = eINSTANCE.getTemplateController_Type();

		/**
		 * The meta object literal for the '<em><b>Type AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE_CONTROLLER__TYPE_AST = eINSTANCE.getTemplateController_TypeAST();

		/**
		 * The meta object literal for the '<em><b>Abstract Component</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE_CONTROLLER__ABSTRACT_COMPONENT = eINSTANCE.getTemplateController_AbstractComponent();

		/**
		 * The meta object literal for the '<em><b>Comments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE_CONTROLLER__COMMENTS = eINSTANCE.getTemplateController_Comments();

		/**
		 * The meta object literal for the '<em><b>Merged Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPLATE_CONTROLLER__MERGED_TYPE = eINSTANCE.getTemplateController_MergedType();

		/**
		 * The meta object literal for the '<em><b>Any</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPLATE_CONTROLLER__ANY = eINSTANCE.getTemplateController_Any();

		/**
		 * The meta object literal for the '<em><b>Any Attributes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPLATE_CONTROLLER__ANY_ATTRIBUTES = eINSTANCE.getTemplateController_AnyAttributes();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.CommentImpl <em>Comment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.CommentImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getComment()
		 * @generated
		 */
		EClass COMMENT = eINSTANCE.getComment();

		/**
		 * The meta object literal for the '<em><b>Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMENT__LANGUAGE = eINSTANCE.getComment_Language();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMENT__TEXT = eINSTANCE.getComment_Text();

		/**
		 * The meta object literal for the '<em><b>Language AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMENT__LANGUAGE_AST = eINSTANCE.getComment_LanguageAST();

		/**
		 * The meta object literal for the '<em><b>Text AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMENT__TEXT_AST = eINSTANCE.getComment_TextAST();

		/**
		 * The meta object literal for the '<em><b>Any</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMENT__ANY = eINSTANCE.getComment_Any();

		/**
		 * The meta object literal for the '<em><b>Any Attributes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMENT__ANY_ATTRIBUTES = eINSTANCE.getComment_AnyAttributes();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.CoordinatesImpl <em>Coordinates</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.CoordinatesImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getCoordinates()
		 * @generated
		 */
		EClass COORDINATES = eINSTANCE.getCoordinates();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COORDINATES__NAME = eINSTANCE.getCoordinates_Name();

		/**
		 * The meta object literal for the '<em><b>X0</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COORDINATES__X0 = eINSTANCE.getCoordinates_X0();

		/**
		 * The meta object literal for the '<em><b>X1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COORDINATES__X1 = eINSTANCE.getCoordinates_X1();

		/**
		 * The meta object literal for the '<em><b>Y0</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COORDINATES__Y0 = eINSTANCE.getCoordinates_Y0();

		/**
		 * The meta object literal for the '<em><b>Y1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COORDINATES__Y1 = eINSTANCE.getCoordinates_Y1();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COORDINATES__COLOR = eINSTANCE.getCoordinates_Color();

		/**
		 * The meta object literal for the '<em><b>Name AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COORDINATES__NAME_AST = eINSTANCE.getCoordinates_NameAST();

		/**
		 * The meta object literal for the '<em><b>X0AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COORDINATES__X0_AST = eINSTANCE.getCoordinates_X0AST();

		/**
		 * The meta object literal for the '<em><b>X1AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COORDINATES__X1_AST = eINSTANCE.getCoordinates_X1AST();

		/**
		 * The meta object literal for the '<em><b>Y0AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COORDINATES__Y0_AST = eINSTANCE.getCoordinates_Y0AST();

		/**
		 * The meta object literal for the '<em><b>Y1AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COORDINATES__Y1_AST = eINSTANCE.getCoordinates_Y1AST();

		/**
		 * The meta object literal for the '<em><b>Color AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COORDINATES__COLOR_AST = eINSTANCE.getCoordinates_ColorAST();

		/**
		 * The meta object literal for the '<em><b>Coordinates</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COORDINATES__COORDINATES = eINSTANCE.getCoordinates_Coordinates();

		/**
		 * The meta object literal for the '<em><b>Any</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COORDINATES__ANY = eINSTANCE.getCoordinates_Any();

		/**
		 * The meta object literal for the '<em><b>Any Attributes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COORDINATES__ANY_ATTRIBUTES = eINSTANCE.getCoordinates_AnyAttributes();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.LoggerImpl <em>Logger</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.LoggerImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getLogger()
		 * @generated
		 */
		EClass LOGGER = eINSTANCE.getLogger();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOGGER__NAME = eINSTANCE.getLogger_Name();

		/**
		 * The meta object literal for the '<em><b>Name AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOGGER__NAME_AST = eINSTANCE.getLogger_NameAST();

		/**
		 * The meta object literal for the '<em><b>Merged Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOGGER__MERGED_NAME = eINSTANCE.getLogger_MergedName();

		/**
		 * The meta object literal for the '<em><b>Any</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOGGER__ANY = eINSTANCE.getLogger_Any();

		/**
		 * The meta object literal for the '<em><b>Any Attributes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOGGER__ANY_ATTRIBUTES = eINSTANCE.getLogger_AnyAttributes();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.impl.VirtualNodeImpl <em>Virtual Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.impl.VirtualNodeImpl
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getVirtualNode()
		 * @generated
		 */
		EClass VIRTUAL_NODE = eINSTANCE.getVirtualNode();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VIRTUAL_NODE__NAME = eINSTANCE.getVirtualNode_Name();

		/**
		 * The meta object literal for the '<em><b>Name AST</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VIRTUAL_NODE__NAME_AST = eINSTANCE.getVirtualNode_NameAST();

		/**
		 * The meta object literal for the '<em><b>Merged Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VIRTUAL_NODE__MERGED_NAME = eINSTANCE.getVirtualNode_MergedName();

		/**
		 * The meta object literal for the '<em><b>Any</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VIRTUAL_NODE__ANY = eINSTANCE.getVirtualNode_Any();

		/**
		 * The meta object literal for the '<em><b>Any Attributes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VIRTUAL_NODE__ANY_ATTRIBUTES = eINSTANCE.getVirtualNode_AnyAttributes();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.Cardinality <em>Cardinality</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.Cardinality
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getCardinality()
		 * @generated
		 */
		EEnum CARDINALITY = eINSTANCE.getCardinality();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.Role <em>Role</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.Role
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getRole()
		 * @generated
		 */
		EEnum ROLE = eINSTANCE.getRole();

		/**
		 * The meta object literal for the '{@link org.ow2.fractal.f4e.fractal.Contingency <em>Contingency</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.ow2.fractal.f4e.fractal.Contingency
		 * @see org.ow2.fractal.f4e.fractal.impl.FractalPackageImpl#getContingency()
		 * @generated
		 */
		EEnum CONTINGENCY = eINSTANCE.getContingency();

	}

} //FractalPackage
