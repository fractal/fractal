package org.ow2.fractal.f4e.fractal.emfdom;

import java.util.List;

import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.wst.common.internal.emf.resource.GenericTranslator;
import org.eclipse.wst.common.internal.emf.resource.RootTranslator;
import org.eclipse.wst.common.internal.emf.resource.Translator;
import org.eclipse.wst.common.internal.emf.resource.TranslatorResource;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.FractalFactory;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.System;
/**
 *  org.eclipse.wst.common.core,
 org.eclipse.wst.common.emf,
 org.eclipse.wst.sse.core
 * @author Yann Davin
 *
 */
public class FractalTranslator extends RootTranslator {

	@Override
	public EStructuralFeature getFeature() {
		// TODO Auto-generated method stub
		return super.getFeature();
	}

	public void setMOFValue(Notifier owner, Object value, int newIndex) {
		if(((Resource) owner).getContents().isEmpty()){
			((Resource) owner).getContents().add(FractalFactory.eINSTANCE.createSystem());
		}
		((System)((Resource) owner).getContents().get(0)).getDefinitions().add((Definition)value);
		//((Resource) owner).getContents().add(newIndex, (EObject)value);
	}
	
	@Override
	public List getMOFChildren(EObject mofObject) {
		// TODO Auto-generated method stub
		return super.getMOFChildren(mofObject);
	}

	@Override
	public String getDOMName(Object value) {
		// TODO Auto-generated method stub
		return super.getDOMName(value);
	}

	@Override
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return super.getNameSpace();
	}

	@Override
	public EObject createEMFObject(String nodeName, String readAheadName) {
		return super.createEMFObject(nodeName, readAheadName);
	}

	public FractalTranslator(){
		super("definition",FractalPackage.eINSTANCE.getDefinition());
	}
	
	public static FractalTranslator getInstance(){
		return SingletonHolder.instance;
	}
	
	/**
	 * The Class SingletonHolder.
	 */
	private static class SingletonHolder {
		/** The instance. */
		private static FractalTranslator instance = new FractalTranslator();
	}
	
	public Translator[] getChildren(Object target, int versionID) {
		GenericTranslator system = new GenericTranslator(
		        "",
		        FractalPackage.eINSTANCE.getSystem());
		
		
//		Translator system = new Translator("system",
//				FractalPackage.eINSTANCE.getSystem_Definitions(),Translator.EMPTY_TAG);
//		return new Translator[]{definition};
		
		//Binding
		GenericTranslator binding = new GenericTranslator(
		        "binding",
		        FractalPackage.eINSTANCE.getAbstractComponent_Bindings());
		
		Translator bindingClient = new Translator("client",
	            FractalPackage.eINSTANCE.getBinding_Client(),
	            Translator.DOM_ATTRIBUTE);
		
		Translator bindingServer = new Translator("client",
	            FractalPackage.eINSTANCE.getBinding_Client(),
	            Translator.DOM_ATTRIBUTE);
		
		binding.setChildren(new Translator[]{bindingClient,bindingServer});
		
		//interface
		GenericTranslator interface_ = new GenericTranslator(
		        "interface",
		        FractalPackage.eINSTANCE.getAbstractComponent_Interfaces());
		
		Translator interfaceName = new Translator("name",
	            FractalPackage.eINSTANCE.getInterface_Name(),
	            Translator.DOM_ATTRIBUTE);
		
		Translator interfaceRole = new Translator("role",
	            FractalPackage.eINSTANCE.getInterface_Role(),
	            Translator.DOM_ATTRIBUTE);
		
		Translator interfaceCardinality = new Translator("cardinality",
	            FractalPackage.eINSTANCE.getInterface_Cardinality(),
	            Translator.DOM_ATTRIBUTE);
		
		Translator interfaceContingency = new Translator("contingency",
	            FractalPackage.eINSTANCE.getInterface_Contingency(),
	            Translator.DOM_ATTRIBUTE);
		
		Translator interfaceSignature = new Translator("signature",
	            FractalPackage.eINSTANCE.getInterface_Signature(),
	            Translator.DOM_ATTRIBUTE);
		
		interface_.setChildren(new Translator[]{
				interfaceName,
				interfaceRole,
				interfaceCardinality,
				interfaceContingency,
				interfaceSignature
				});
		
		//Component
		GenericTranslator component = new GenericTranslator(
		        "component",
		        FractalPackage.eINSTANCE.getAbstractComponent_SubComponents());
		
		Translator componentName = new Translator("name",
		            FractalPackage.eINSTANCE.getAbstractComponent_Name(),
		            Translator.DOM_ATTRIBUTE);
		
		Translator componentDefinition = new Translator("definition",
	            FractalPackage.eINSTANCE.getComponent_Definition(),
	            Translator.DOM_ATTRIBUTE);
		
		component.setChildren(new Translator[] {
				componentName, 
				componentDefinition
		});
		
		
		//Definition
		GenericTranslator definition = new GenericTranslator(
		        "definition",
		        FractalPackage.eINSTANCE.getSystem_Definitions());
		
		Translator definitionName = new Translator("name",
	            FractalPackage.eINSTANCE.getAbstractComponent_Name(),
	            Translator.DOM_ATTRIBUTE);
		
		Translator definitionExtends = new Translator("extends",
	            FractalPackage.eINSTANCE.getDefinition_Extends(),
	            Translator.DOM_ATTRIBUTE);
		
		Translator definitionArguments = new Translator("arguments",
	            FractalPackage.eINSTANCE.getDefinition_Arguments(),
	            Translator.DOM_ATTRIBUTE);
		
		definition.setChildren(new Translator[] {
				definitionName, definitionExtends, definitionArguments,
				interface_,component,binding});
		
		
				
		if(target instanceof TranslatorResource){
			return new Translator[]{
					system
			};
		}else if(target == null){
			return new Translator[]{
					definition
			};
		}else if(target instanceof Definition){
			return definition.getChildren();
		}
		else{ return  new Translator[]{};

		}
//		return new Translator[]{
//				 new Translator("name",
//				            FractalPackage.eINSTANCE.getAbstractComponent_Name(),
//				            Translator.DOM_ATTRIBUTE)};
		
		
	}

}
