package org.ow2.fractal.f4e.fractal.emfdom;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.wst.common.internal.emf.resource.EMF2DOMAdapter;
import org.eclipse.wst.common.internal.emf.resource.EMF2DOMRenderer;
import org.eclipse.wst.common.internal.emf.utilities.DOMLoadOptions;
import org.eclipse.wst.common.internal.emf.utilities.DOMUtilities;
import org.eclipse.wst.xml.core.internal.emf2xml.EMF2DOMSSERenderer;

public class FractalEMF2DOMRenderer extends EMF2DOMSSERenderer {

	@Override
	protected void loadDocument(InputStream in, Map options) throws IOException {
		// TODO Auto-generated method stub
		try {
			DOMLoadOptions domOpts = new DOMLoadOptions();
			domOpts.setAllowJavaEncodings(true);
			domOpts.setExpandEntityRefererences(true);
			domOpts.setValidate(isValidating());
			document = DOMUtilities.loadDocument(in, domOpts, getResource().getEntityResolver());
			needsToCreateDOM = false;
		} catch (RuntimeException t_rex) {
			throw t_rex;
		} catch (IOException iox) {
			throw iox;
		} catch (Exception ex) {
			throw new WrappedException(ex);
		}
	}

	@Override
	protected EMF2DOMAdapter createRootDOMAdapter() {
		// TODO Auto-generated method stub
		return super.createRootDOMAdapter();
		//return  new EMF2DOMSSEAdapterEx(getResource(), document, this, getResource().getRootTranslator());
	}

	@Override
	public void doLoad(InputStream in, Map options) throws IOException {
		// TODO Auto-generated method stub
		super.doLoad(in, options);
	}

}
