package org.ow2.fractal.f4e.fractal.emfdom;

import org.eclipse.wst.common.internal.emf.resource.EMF2DOMRenderer;
import org.eclipse.wst.common.internal.emf.resource.EMF2DOMRendererFactory;
import org.eclipse.wst.common.internal.emf.resource.Renderer;
import org.eclipse.wst.common.internal.emf.resource.RendererFactory;

public class FractalEMF2DOMRendererFactory extends RendererFactory {

	public static final FractalEMF2DOMRendererFactory INSTANCE = new FractalEMF2DOMRendererFactory();

	public FractalEMF2DOMRendererFactory() {
		super();
	}

	/**
	 * @see com.ibm.etools.emf2xml.RendererFactory#createRenderer()
	 */
	public Renderer createRenderer() {
		FractalEMF2DOMRenderer renderer = new FractalEMF2DOMRenderer();
		return renderer;
	}
}