package org.ow2.fractal.f4e.fractal.emfdom;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.ow2.fractal.f4e.fractal.FractalPackage;

public class RootDefinitionAdapter extends AdapterImpl {
	  public boolean isAdapterForType(Object type) {
	        return super.isAdapterForType(type);
	    }

	    public void notifyChanged(Notification msg) {
	    	// ((FractalTranslatorResourceImpl) msg.getNotifier()).syncVersionOfRootObject();
	        if (msg.getFeatureID(null) == FractalPackage.DEFINITION
	                && msg.getEventType() == Notification.ADD) {
	           
	            ((Notifier) msg.getNotifier()).eAdapters().remove(this);
	        }
	    }

	public String getVersionString() {
	   return "";
	}
}
