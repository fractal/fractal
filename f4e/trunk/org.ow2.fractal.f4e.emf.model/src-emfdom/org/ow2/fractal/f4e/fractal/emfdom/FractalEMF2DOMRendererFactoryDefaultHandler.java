package org.ow2.fractal.f4e.fractal.emfdom;

import org.eclipse.wst.common.internal.emf.resource.EMF2DOMRendererFactory;
import org.eclipse.wst.common.internal.emf.resource.EMF2DOMRendererFactoryDefaultHandler;
import org.eclipse.wst.common.internal.emf.resource.RendererFactory;
import org.eclipse.wst.common.internal.emf.resource.RendererFactoryDefaultHandler;

public class FractalEMF2DOMRendererFactoryDefaultHandler implements RendererFactoryDefaultHandler {

	public static final FractalEMF2DOMRendererFactoryDefaultHandler INSTANCE = new FractalEMF2DOMRendererFactoryDefaultHandler();

	/**
	 *  
	 */
	protected FractalEMF2DOMRendererFactoryDefaultHandler() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.etools.emf2xml.RendererFactoryDefaultHandler#getDefaultRendererFactory()
	 */
	public RendererFactory getDefaultRendererFactory() {
		return FractalEMF2DOMRendererFactory.INSTANCE;
	}
	
	
}