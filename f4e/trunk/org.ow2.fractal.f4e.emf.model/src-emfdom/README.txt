Attempt to synchronize the emf editor with the xml editor
using the EMF2DOMSSE adapter. 
It is unstable, but can be tested.

add to the plugin-dependency :
org.eclipse.wst.xml.core
org.eclipse.wst.sse.core
org.eclipse.jem.util

Change in the FractalResourceSetImpl the factory for Fractal files:
getResourceFactoryRegistry().getExtensionToFactoryMap()
       .put("fractal", new FractalTranslatorResourceFactory());
       
and extends the class to be a ProjectResourceSetImpl.

