/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.test;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.util.FractalResourceSetImpl;


public class LoadFractalInXmi {
	 	Resource createdResource;
	 	Resource loadedResource;
	 	Resource copyResource;
	 	private static final String COPIED_FILE = "test/merging/test1/D1D0.fractal";
	 	private static final String FILE = "test/Screnario_gen.fractal";
	 	
        public static void main(String[] args) {
        	LoadFractalInXmi test = new LoadFractalInXmi();
                test.main();

        }
        
        /**
         * Main.
         */
        public void main() {
                EPackage.Registry.INSTANCE.put(FractalPackage.eINSTANCE.getNsURI(),
                                FractalPackage.eINSTANCE);
                 
                ResourceSet resourceSet = new FractalResourceSetImpl();
               
                //URI fileURI = URI.createFileURI(new File("test/testadlfractalcopy.xmi")
                //.getAbsolutePath());
                
                URI fileURI = URI.createFileURI(new File(FILE)
                .getAbsolutePath());
                Date t = new Date();
                System.out.println(t);
                loadedResource = resourceSet.getResource(fileURI, true); 
                t = new Date();
                System.out.println(t);
                
              /*  Resource copied = collectAll(loadedResource);
                
                try{
                copied.save(null);
                }catch(Exception e){
                	e.printStackTrace();
                }*/
        }
        
        public Resource collectAll(Resource resource){
        	//get the resourceset
        	ResourceSet resourceSet = resource.getResourceSet();
        		
        	//search for other resources containing referenced objects
        	List<Resource> resources = new ArrayList<Resource>();
        	resources.add(resource);
                for(int i = 0; i < resources.size(); i++){
                	Resource r = resources.get(i);
                	for(Iterator<EObject> j = r.getAllContents(); j.hasNext(); ){
                		for(Object object : j.next().eCrossReferences()){
                			EObject eObject = (EObject)object;
                			Resource otherResource = eObject.eResource();
                			if(otherResource != null && !resources.contains(otherResource)){
                				resources.add(otherResource);
                			}
                		}
                	}
                }
                
                //create resource to hold all objects
                File tempFile = new File(COPIED_FILE);
                Resource complete_resource = resourceSet.createResource(URI.createFileURI(tempFile.getAbsolutePath()));
                
                //move all objects into one resource
                Iterator iter = resources.iterator();
                Collection cl = new ArrayList();
                for(Resource resource2 : resources){
                	cl.addAll(resource2.getContents());
        	}
                complete_resource.getContents().addAll(EcoreUtil.copyAll(cl));
                
                return complete_resource;
        }
}
