/***
 * eFrac
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: yann.davin@gmail.com
 *
 * Author: Yann Davin
 */
package org.ow2.fractal.f4e.fractal.test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.FractalFactory;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.Role;
import org.ow2.fractal.f4e.fractal.System;
import org.ow2.fractal.f4e.fractal.ocl.adapter.ValidatorAdapter;
import org.ow2.fractal.f4e.fractal.ocl.util.OCLConstraintLoader;
import org.ow2.fractal.f4e.fractal.util.FractalResourceSetImpl;
import org.ow2.fractal.f4e.fractal.util.IFractalXMLResource;

/**
 * The Class CreateAndSaveInXml.
 * 
 */
public class CreateAndSaveInXml {
		static final String OCL_FILE = "/org/ow2/fractal/f4e/fractal/ocl/constraints.ocl";	
		
		static final String CREATED_FILE = "test/generated/Definition1.fractal";
		static final String LOADED_FILE = "test/DefinitionWithArgs.fractal";
		static final String COPIED_FILE = "test/generated/DefinitionWithArgs.xmi";
		
	 	/** The created resource. */
	 	Resource createdResource;
	 	
	 	/** The loaded resource. */
	 	Resource loadedResource;
	 	
	 	/** The copy resource. */
	 	Resource copyResource;
	 	
        /**
         * The main method.
         * 
         * @param args the arguments
         */
        public static void main(String[] args) {
        	CreateAndSaveInXml test = new CreateAndSaveInXml();
                test.main();

        }
        
        /**
         * Creates the system.
         * 
         * @return the org.ow2.fractal.adl.extended.fractaladlextended. system
         */
        private Definition createSystem(){
        	System system;
        	system = FractalFactory.eINSTANCE.createSystem();
            
            Definition definition1;
            Definition definition2;
            definition1 = FractalFactory.eINSTANCE.createDefinition();
            definition1.setExtends("test.Definition1(3,4)");
            definition2 = FractalFactory.eINSTANCE.createDefinition();
            
            Interface interface1;
            interface1 = FractalFactory.eINSTANCE.createInterface();
            interface1.setName("m");
            interface1.setSignature("HelloWorld");
            interface1.setRole(Role.SERVER);
           
            Component component1;
            component1 =  FractalFactory.eINSTANCE.createComponent();
            
            system.getDefinitions().add(definition1);
            definition1.setName("helloworld.test.Definition1");
            definition1.getInterfaces().add(interface1);
            definition1.getSubComponents().add(component1);
            
            
            definition2.setExtends("Definition1");
            system.getDefinitions().add(definition2);
            
            return definition1;
        }
        
        /**
         * Main.
         */
        public void main() {
        	
                EPackage.Registry.INSTANCE.put(FractalPackage.eINSTANCE.getNsURI(),
                		FractalPackage.eINSTANCE);
                
                // register the validator adapter.
                ValidatorAdapter validator = ValidatorAdapter.getInstance();
            	ValidatorAdapter.Registry.INSTANCE.put(
            			FractalPackage.eINSTANCE,
            				validator);

            	try{
            		// load the ocl constraints;
        			URL url = ClassLoader.getSystemClassLoader().getResource(OCL_FILE);
        			OCLConstraintLoader oclConstraintLoader = new OCLConstraintLoader();
        			//oclConstraintLoader.load(this.getClass().getResource(OCL_FILE));
        		}catch(Exception e){
        			java.lang.System.err.println(e.getMessage());
        		}
        		
                ResourceSet resourceSet = new FractalResourceSetImpl();
                resourceSet.getLoadOptions().put(IFractalXMLResource.TRANSACTIONAL_SUPPORT_OPTION, new Boolean(false));
                // Create a fractal filedefaultLoadOptions
                createdResource = resourceSet.createResource(URI
                                .createFileURI(CREATED_FILE));
                createdResource.getContents().add(createSystem());
                
               
                // Load a fractal file with its adl dependencies.
                // The loaded model has all informations about the relations
                // between the fractal adl elements. It means that
                // if for example a definition extends test.Definition1
                // the extends="test.Definition1" have been parsed
                // a Definition element is created in the model
                // and a reference is created.
                URI fileURI = URI.createFileURI(new File(LOADED_FILE)
                .getAbsolutePath());
                
                loadedResource = resourceSet.getResource(fileURI, true); 
        		
                // check the ocl constraints on the loaded model
               // ModelVisitor<ModelValidator> modelValidator = new ModelVisitor<ModelValidator>(ModelValidator.class);
                //modelValidator.doSwitch(loadedResource.getContents().get(0));
                
                
                
                // Save in xmi format the previous loaded model
                // with all its dependencies.
                // The model saved has all informations needed
                // to apply merge algorithms.
                // (Open the xmi with the text editor)
                copyResource = collectAll(loadedResource);
            
               
                
      
                try {
                       Map options = new HashMap();
                       options.put(XMLResource.OPTION_DECLARE_XML, Boolean.TRUE);
                       
                      createdResource.save(options);
                   
                      copyResource.save(options);
                      
                } catch (IOException e) {
                        java.lang.System.err.println(e.getMessage());
                }
        }
        
        // I retrieve an object and all of the objects it references
        // (regardless of the kind of reference) 
        // and save them as a single resource
        /**
         * Collect all.
         * 
         * @param resource the resource
         * 
         * @return the resource
         */
        public Resource collectAll(Resource resource){
        	//get the resourceset
        	ResourceSet resourceSet = resource.getResourceSet();
        		
        	//search for other resources containing referenced objects
        	List<Resource> resources = new ArrayList<Resource>();
        	resources.add(resource);
                for(int i = 0; i < resources.size(); i++){
                	Resource r = resources.get(i);
                	for(Iterator<EObject> j = r.getAllContents(); j.hasNext(); ){
                		for(Object object : j.next().eCrossReferences()){
                			EObject eObject = (EObject)object;
                			Resource otherResource = eObject.eResource();
                			if(otherResource != null && !resources.contains(otherResource)){
                				resources.add(otherResource);
                			}
                		}
                	}
                }
                
                //create resource to hold all objects
                File tempFile = new File(COPIED_FILE);
                Resource complete_resource = resourceSet.createResource(URI.createFileURI(tempFile.getAbsolutePath()));
                
                //move all objects into one resource
                Iterator iter = resources.iterator();
                Collection cl = new ArrayList();
                for(Resource resource2 : resources){
                	cl.addAll(resource2.getContents());
        	}
                complete_resource.getContents().addAll(EcoreUtil.copyAll(cl));
                
                return complete_resource;
        }
}
