package org.ow2.fractal.f4e.adl.launcher.configurations;

import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.jdt.debug.ui.launchConfigurations.JavaMainTab;

public class FractalJavaMainTab extends JavaMainTab{
	
	public FractalJavaMainTab(){
		super();
		//The main class set to  LauncherPlugin.FRACTAL_ADL_LAUNCHER_CLASS_NAME can't be changed
		//fMainText.setEditable(false);
	}
	
	/**
	 * We just need to overwrite setDefaults.
	 * The JavaMainTab  set defaults from a selected JavaElement.
	 * In our case the user select a FractalADL File, so the inherited setDefaults
	 * method not works.
	 */
	
	public void setDefaults(ILaunchConfigurationWorkingCopy config) {
	
	}
}
