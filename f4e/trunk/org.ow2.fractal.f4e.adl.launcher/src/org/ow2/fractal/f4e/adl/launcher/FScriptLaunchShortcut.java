
package org.ow2.fractal.f4e.adl.launcher;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.jdt.internal.core.JavaProject;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jface.viewers.ISelection;
import org.ow2.fractal.f4e.adl.launcher.configurations.FractalLauncherUtils;
import org.ow2.fractal.f4e.adl.launcher.messages.UIMessages;
import org.ow2.fractal.f4e.fractal.Definition;


/**
 * The launch shortcut to launch FScript
 * 
 */
/**
 * @author Yann Davin
 *
 */
public class FScriptLaunchShortcut extends FractalLaunchShortcut {
	
	
	public void launch(ISelection selection, String mode) {
		JavaProject javaProject = FractalLauncherUtils.getCurrentJavaProject(selection);
		
		if(javaProject != null){
			ILaunchConfigurationWorkingCopy wc = createConfiguration(javaProject,"");
			try {
				ILaunchConfiguration config = wc.doSave();
				DebugUITools.launch(config, mode);
			} catch (Exception e) {
				LauncherPlugin.getDefault().getLog().log(new Status(IStatus.ERROR,LauncherPlugin.PLUGIN_ID,UIMessages.ERROR_CAN_NOT_LAUNCH_FRACTAL_ADL_FILE,e));
			}
		}	
	}
	
	protected ILaunchConfigurationWorkingCopy createConfiguration(JavaProject javaProject, String definitionNameADL){
		ILaunchConfigurationWorkingCopy wc = super.createConfiguration(javaProject, definitionNameADL);
		wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME, LauncherPlugin.FRACTAL_FSCRIPT_CLASS_NAME);
		wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_PROGRAM_ARGUMENTS, "");
		return wc;
	}
}
