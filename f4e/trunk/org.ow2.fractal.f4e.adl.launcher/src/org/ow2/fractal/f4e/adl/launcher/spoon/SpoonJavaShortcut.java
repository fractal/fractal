/*******************************************************************************
 * Copyright (c) 2000, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Yann Davin initial copy from JUnit David Saff (saff@mit.edu)
 *******************************************************************************/
package org.ow2.fractal.f4e.adl.launcher.spoon;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.variables.VariablesPlugin;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.internal.core.JavaProject;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jdt.launching.IRuntimeClasspathEntry;
import org.eclipse.jdt.launching.JavaRuntime;
import org.ow2.fractal.eclipse.plugin.installer.FractalInstaller;
import org.ow2.fractal.f4e.adl.launcher.FractalADLLaunchDelegate;
import org.ow2.fractal.f4e.adl.launcher.LauncherPlugin;
import org.ow2.fractal.f4e.adl.launcher.messages.UIMessages;


/**
 * The launch shortcut to launch JUnit tests.
 * 
 * <p>
 * This class may be instantiated and subclassed.
 * </p>
 * @since 3.3
 */
public class SpoonJavaShortcut extends AbstractSpoonShortcut {
	
	public SpoonJavaShortcut(){
		super();
	}
	
	
	protected ILaunchConfigurationWorkingCopy createConfiguration(JavaProject javaProject, Object selection){
		try{
			FractalADLLaunchDelegate launcher = new FractalADLLaunchDelegate();
			
			ILaunchManager manager = launcher.getLaunchManager();
			ILaunchConfigurationType type1 = manager.getLaunchConfigurationType(LauncherPlugin.FRACTAL_ADL_CONFIGURATION_TYPE);
		  
			ILaunchConfigurationWorkingCopy wc = type1.newInstance(null, "Generate Fractal Java files");
			wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME, javaProject.getElementName());
			wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME, "spoon.Launcher");
		  
			String classpath = new String();
		
			// A List which will contain all the classpath entry of the project + the output directory.
			List<String> classpathList = new ArrayList<String>();
	
		   IClasspathEntry[] classpathEntries = javaProject.getExpandedClasspath();
		   for(int i=0; i< classpathEntries.length;i++){
			 IClasspathEntry entry = classpathEntries[i];
			 IRuntimeClasspathEntry cpEntry = JavaRuntime.newArchiveRuntimeClasspathEntry(entry.getResolvedEntry().getPath());
			 classpathList.add(cpEntry.getMemento());
		   }
		   
		   IRuntimeClasspathEntry cpEntry = JavaRuntime.newArchiveRuntimeClasspathEntry(javaProject.getOutputLocation());
		   classpathList.add(cpEntry.getMemento());
		   
		   appendFractalHome(wc);
		   
		   wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_DEFAULT_CLASSPATH,false);
		   wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_CLASSPATH, classpathList);
		  
		   wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_VM_ARGUMENTS,"");
		   String arguments="";
		   arguments +=" -v";
		   arguments +=" -c";
		   arguments +=" -b";
		   arguments +=Platform.getLocation() + javaProject.getOutputLocation().toOSString();
		   arguments += " -i";
		   arguments += Platform.getLocation()  + ((IResource)selection).getFullPath().toOSString();
		   arguments += " -s";
		   arguments += FractalInstaller.getDefault().getFractalSpoonletJarPath();
		  
		   wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_PROGRAM_ARGUMENTS,arguments);
		   
		   return wc;
		}catch(Exception e){
			LauncherPlugin.getDefault().getLog().log(new Status(IStatus.ERROR,LauncherPlugin.PLUGIN_ID,UIMessages.ERROR_LOADING_FRACTAL_EXPLORER_EXTENSION,e));
			return null;
		}
	}
}
