/*******************************************************************************
 * Copyright (c) 2000, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Yann Davin initial copy from JUnit David Saff (saff@mit.edu)
 *******************************************************************************/
package org.ow2.fractal.f4e.adl.launcher.spoon;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.internal.core.JavaProject;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jdt.launching.IRuntimeClasspathEntry;
import org.eclipse.jdt.launching.JavaRuntime;
import org.ow2.fractal.eclipse.plugin.installer.FractalInstaller;
import org.ow2.fractal.f4e.adl.launcher.FractalADLLaunchDelegate;
import org.ow2.fractal.f4e.adl.launcher.LauncherPlugin;
import org.ow2.fractal.f4e.adl.launcher.configurations.FractalLauncherUtils;


/**
 * The launch shortcut to launch JUnit tests.
 * 
 * <p>
 * This class may be instantiated and subclassed.
 * </p>
 * @since 3.3
 */
public class SpoonFractalADLShortcut extends AbstractSpoonShortcut {
	public static String MODE="run";
	
	public SpoonFractalADLShortcut(){
		super();
	}
	
	protected ILaunchConfigurationWorkingCopy createConfiguration(JavaProject javaProject, Object selection){
		try{
			FractalADLLaunchDelegate launcher = new FractalADLLaunchDelegate();
			
			ILaunchManager manager = launcher.getLaunchManager();
			ILaunchConfigurationType type1 = manager.getLaunchConfigurationType(LauncherPlugin.FRACTAL_ADL_CONFIGURATION_TYPE);
		  
			ILaunchConfigurationWorkingCopy wc = type1.newInstance(null, "Generate Fractal ADL Files");
			wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME, javaProject.getElementName());
			wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME, "spoon.Launcher");
			
			String classpath = new String();
		
			// A List which will contain all the classpath entry of the project + the output directory.
			List<String> classpathList = new ArrayList<String>();
			
			// We get the resolved classpath. By default the JDT version that comes with Galileo,
			// always try to resolve chained libraries, which raises some problems with the Fractal embedded libraries.
			// So we have duplicate the resolveClasspath in the FractalLauncherUtils in order to be able to 
			// set the resolveChainedLibraries to false.
			// The problem comes from the castor-0.9.9.jar which declare in its manifest :
			// Class-Path: xerces-J_1.4.0.jar
			// When resolveChainedLibraries is set to true, it search xerces-J_1.4.0.jar in the classpath and fails.
			FractalLauncherUtils.ResolvedClasspath resolvedClasspath = FractalLauncherUtils.resolveClasspath(javaProject, javaProject.getRawClasspath(), false,false);
			
			//	IClasspathEntry[] classpathEntries =  javaProject.getResolvedClasspath(true);;
			IClasspathEntry[] classpathEntries = resolvedClasspath.getResolvedClasspath();
		   
			for(int i=0; i< classpathEntries.length;i++){
			 IClasspathEntry entry = classpathEntries[i];
			 IRuntimeClasspathEntry cpEntry = JavaRuntime.newArchiveRuntimeClasspathEntry(entry.getResolvedEntry().getPath());
			 classpathList.add(cpEntry.getMemento());
		   }
		   
		   IRuntimeClasspathEntry cpEntry = JavaRuntime.newArchiveRuntimeClasspathEntry(javaProject.getOutputLocation());
		   classpathList.add(cpEntry.getMemento());
		  
		   wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_DEFAULT_CLASSPATH,false);
		   wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_CLASSPATH, classpathList);
		   
		   wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_VM_ARGUMENTS,"");
		   
		   // Add the FRACTAL_HOME variable to the launching environment
		   appendFractalHome(wc);
		   
		   String arguments="";
		   // Generated adl files are put in the default output java project location
		   arguments +=" -o";
		   arguments +=Platform.getLocation() + javaProject.getOutputLocation().toOSString();
		   // Verbose mode
		   arguments +=" -v";
		   // The spoon processors are applied to the selected file/folder
		   arguments += " -i";
		   arguments += getArguments(javaProject, selection);
		   // Pass the jar containing the spoonlet to generate ADL files
		   arguments += " -s ";
		   arguments += FractalInstaller.getDefault().getFractalADLSpoonletJarPath();
		  
		   wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_PROGRAM_ARGUMENTS,arguments);
		  
		   return wc;
		}catch(Exception e){
			LauncherPlugin.getDefault().getLog().log(new Status(IStatus.ERROR,LauncherPlugin.PLUGIN_ID,"",e));
			return null;
		}
	}
	
	protected String getRootFolder(JavaProject javaProject, IFile file){
		String result="spooned";
		IPath path = file.getFullPath();
		while(path.segmentCount()>0){
			try{
			if(javaProject.findElement(path) != null){
				result = path.toOSString();
			}else{
				path = path.removeFirstSegments(1);
			}
			}catch(Exception e){
				LauncherPlugin.getDefault().getLog().log(new Status(IStatus.ERROR,LauncherPlugin.PLUGIN_ID,"",e));
			}
		}
		return result;
	}
	
	protected String getArguments(JavaProject javaProject, Object selection){
		String result = ""; 
		if(selection instanceof IFile){
			result =  Platform.getLocation()  + ((IFile)selection).getFullPath().toOSString();
//			result += " -o";
//			result += Platform.getLocation()  + getRootFolder(javaProject, ((IFile)selection));
		}else if(selection instanceof IFolder){
//			result += " -o";
			result =  Platform.getLocation()  + ((IFolder)selection).getFullPath().toOSString();
		}
		return result;
	}
}
