package org.ow2.fractal.f4e.adl.launcher;

import java.net.URL;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

public class LauncherPlugin extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.ow2.fractal.f4e.adl.launcher";

	// Name of the ADL launcher class which is used to launch Fractal ADL files
	public static final String FRACTAL_ADL_LAUNCHER_CLASS_NAME="org.objectweb.fractal.adl.Launcher";
	
	public static final String FRACTAL_FSCRIPT_CLASS_NAME="org.objectweb.fractal.fscript.console.Main";
	
	// Fractal libraries plugin id
	public static final String FRACTAL_LIBRARIES_PLUGIN_ID="org.ow2.fractal.distribution.libraries";
	
	// JMV Args needed to launch Fractal ADL files
	public static final String FRACTAL_ADL_LAUNCHER_JVM_ARGS="-Dfractal.provider=\"org.objectweb.fractal.julia.Julia\"";
	
	public static final String FRACTAL_ADL_CONFIGURATION_TYPE="org.ow2.fractal.f4e.adl.launcher.fractalADL";
	
	public static final String FRACTAL_ADL_BACKEND="-fractal";
	
	public static final String FRACTAL_ADL_EXPLORER="org.objectweb.fractal.explorer.BasicFractalExplorer";
	
	public static final String FRACTAL_ADL_DEFINITION_NOT_FOUND="The Fractal ADL definition can't be found";
	
	public static final String FRACTAL_ADL_FILE_NOT_FOUND="The Fractal ADL file can't be found";
	
	public static final String FRACTAL_ADL_FILE_EXTENSION=".fractal";
	// The shared instance
	private static LauncherPlugin plugin;
	
	/**
	 * The constructor
	 */
	public LauncherPlugin() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static LauncherPlugin getDefault() {
		return plugin;
	}
}