package org.ow2.fractal.f4e.adl.launcher;

import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTabGroup;
import org.eclipse.debug.ui.CommonTab;
import org.eclipse.debug.ui.EnvironmentTab;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;
import org.eclipse.debug.ui.sourcelookup.SourceLookupTab;
import org.eclipse.jdt.debug.ui.launchConfigurations.JavaClasspathTab;
import org.eclipse.jdt.debug.ui.launchConfigurations.JavaJRETab;
import org.eclipse.jdt.internal.core.JavaProject;
import org.eclipse.jdt.internal.debug.ui.JDIDebugUIPlugin;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.ui.IWorkbenchPage;
import org.ow2.fractal.f4e.adl.launcher.configurations.FractalJavaArgumentsTab;
import org.ow2.fractal.f4e.adl.launcher.configurations.FractalJavaMainTab;
import org.ow2.fractal.f4e.adl.launcher.configurations.FractalLauncherUtils;
import org.ow2.fractal.f4e.fractal.Definition;

public class FractalADLConfigurationTabGroup extends AbstractLaunchConfigurationTabGroup {
	protected ILaunchConfigurationDialog dialog;
	
	public void createTabs(ILaunchConfigurationDialog dialog, String mode) {
		this.dialog = dialog;

		ILaunchConfigurationTab[] tabs = new ILaunchConfigurationTab[] {
			new FractalJavaMainTab(),
			new FractalJavaArgumentsTab(),
			new JavaJRETab(),
			new JavaClasspathTab(),
			new SourceLookupTab(),
			new EnvironmentTab(),
			new CommonTab()
		};
		setTabs(tabs);
	}

	private Definition getSelectedDefinition(){
		IWorkbenchPage page = JDIDebugUIPlugin.getActivePage();
		Definition returnDefinition = null;
		
		if (page != null) {
			returnDefinition = FractalLauncherUtils.getSelectedDefinition(page.getSelection());
		}
		return returnDefinition;
	}
	
	private JavaProject getCurrentJavaProject(){
		IWorkbenchPage page = JDIDebugUIPlugin.getActivePage();
		JavaProject jProject = null;
		
		if (page != null) {
			jProject = FractalLauncherUtils.getCurrentJavaProject(page.getSelection());
		}
		return jProject;
	}
	
	
	public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {
		Definition definition = getSelectedDefinition();
		JavaProject javaProject = getCurrentJavaProject();
		
		if(javaProject != null){
			configuration.setAttribute(IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME, javaProject.getElementName());
		}

		configuration.setAttribute(IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME, LauncherPlugin.FRACTAL_ADL_LAUNCHER_CLASS_NAME);
		configuration.setAttribute(IJavaLaunchConfigurationConstants.ATTR_VM_ARGUMENTS, LauncherPlugin.FRACTAL_ADL_LAUNCHER_JVM_ARGS);
		
		if(definition != null){
			configuration.setAttribute(IJavaLaunchConfigurationConstants.ATTR_PROGRAM_ARGUMENTS, definition.getName());
			configuration.rename(definition.getName());
		}
		
		super.setDefaults(configuration);
	}
	
	
}
