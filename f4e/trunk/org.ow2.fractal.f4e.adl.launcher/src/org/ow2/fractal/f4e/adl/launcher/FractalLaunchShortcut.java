/*******************************************************************************
 * Copyright (c) 2000, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Yann Davin initial copy from JUnit David Saff (saff@mit.edu)
 *******************************************************************************/
package org.ow2.fractal.f4e.adl.launcher;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.emf.codegen.util.CodeGenUtil;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.internal.core.JavaProject;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jdt.launching.IRuntimeClasspathEntry;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.ow2.fractal.f4e.adl.launcher.configurations.FractalLauncherUtils;
import org.ow2.fractal.f4e.adl.launcher.messages.UIMessages;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;


/**
 * The launch shortcut to launch JUnit tests.
 * 
 * <p>
 * This class may be instantiated and subclassed.
 * </p>
 * @since 3.3
 */
public class FractalLaunchShortcut implements ILaunchShortcut {
	
	public FractalLaunchShortcut(){
		super();
	}
	
	private void launch(IType type, String mode) {
		//ILaunchConfiguration config = findLaunchConfiguration(type, getConfigurationType());
		//if (config == null) {
		//	config = createConfiguration(type);
		//}
		
		//if (config != null) {
		//	DebugUITools.launch(config, mode);
		//}		
		try{
		ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
		   ILaunchConfigurationType type1 = manager.getLaunchConfigurationType(IJavaLaunchConfigurationConstants.ID_JAVA_APPLICATION);
		   ILaunchConfigurationWorkingCopy wc = type1.newInstance(null, "SampleConfig");
		   wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME, "TestFractal");
		   wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME, "org.objectweb.fractal.adl.Launcher");
		  
		   List<String> test = null;
		   test = CodeGenUtil.EclipseUtil.getClasspathPaths("org.ow2.fractal.distribution.libraries");
		   String classpath = new String();
		   Iterator<String> iterator = test.iterator();
		   while(iterator.hasNext()){
			   String path = iterator.next();
			   classpath += "," + path;
		   }
		   
		   wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_CLASSPATH, classpath);
		   wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_VM_ARGUMENTS,"-Dfractal.provider=\"org.objectweb.fractal.julia.Julia\"");
		 //  wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_PROGRAM_ARGUMENTS, "helloworld.ClientServerImpl");
		   ILaunchConfiguration config = wc.doSave();	
		   //config.launch(ILaunchManager.RUN_MODE, null);
		   DebugUITools.launch(config, mode);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.debug.ui.ILaunchShortcut#launch(org.eclipse.ui.IEditorPart, java.lang.String)
	 */
	public void launch(IEditorPart editor, String mode) {
		IEditorInput input = editor.getEditorInput();
		IFile file = (IFile) editor.getEditorInput().getAdapter(IFile.class) ;
		if(file != null && file.getName().endsWith(LauncherPlugin.FRACTAL_ADL_FILE_EXTENSION)){
			Definition definition = FractalTransactionalEditingDomain.getResourceSet().getDefinition(URI.createPlatformResourceURI(file.getFullPath().toOSString(),true));
			if(definition != null){
				String definitionName = definition.getName();
				IProject project = file.getProject();
				try{
				if(project.hasNature(JavaCore.NATURE_ID) == true){
					IJavaProject javaProject = (IJavaProject)project.getNature(JavaCore.NATURE_ID);
					
					ILaunchConfigurationWorkingCopy wc = createConfiguration((JavaProject)javaProject,
							definitionName);
					try {
						ILaunchConfiguration config = wc.doSave();
						DebugUITools.launch(config, mode);
					} catch (Exception e) {
						LauncherPlugin.getDefault().getLog().log(new Status(IStatus.ERROR,LauncherPlugin.PLUGIN_ID,UIMessages.ERROR_CAN_NOT_LAUNCH_FRACTAL_ADL_FILE,e));
					}
				}
				}catch(Exception e){
					LauncherPlugin.getDefault().getLog().log(new Status(IStatus.ERROR,LauncherPlugin.PLUGIN_ID,UIMessages.ERROR_CAN_NOT_LAUNCH_FRACTAL_ADL_FILE,e));
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.ui.ILaunchShortcut#launch(org.eclipse.jface.viewers.ISelection, java.lang.String)
	 */
	public void launch(ISelection selection, String mode) {
		String projectName = null;
		String definitionNameADL = null;

		Definition definition = FractalLauncherUtils.getSelectedDefinition(selection);
		if (definition != null) {
			definitionNameADL = definition.getName();
		}

		JavaProject javaProject = FractalLauncherUtils.getCurrentJavaProject(selection);
		if (javaProject != null) {
			projectName = javaProject.getElementName();
		}
		// TODO check that the ADL File has a right name. Check the name of the
		// current package, check the name of the definition
		// inside the ADL file.
		// TODO check that all arguments of the ADL definition have a value.

		ILaunchConfigurationWorkingCopy wc = createConfiguration(javaProject,
				definitionNameADL);
		try {
			ILaunchConfiguration config = wc.doSave();
			DebugUITools.launch(config, mode);
		} catch (Exception e) {
			LauncherPlugin.getDefault().getLog().log(new Status(IStatus.ERROR,LauncherPlugin.PLUGIN_ID,UIMessages.ERROR_CAN_NOT_LAUNCH_FRACTAL_ADL_FILE,e));
		}
				
	}
	
	protected ILaunchConfigurationWorkingCopy createConfiguration(JavaProject javaProject, String definitionNameADL){
		try{
			FractalADLLaunchDelegate launcher = new FractalADLLaunchDelegate();
			
			ILaunchManager manager = launcher.getLaunchManager();
			ILaunchConfigurationType type1 = manager.getLaunchConfigurationType(LauncherPlugin.FRACTAL_ADL_CONFIGURATION_TYPE);
		  
			ILaunchConfigurationWorkingCopy wc = type1.newInstance(null, definitionNameADL);
			wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME, javaProject.getElementName());
			wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME, LauncherPlugin.FRACTAL_ADL_LAUNCHER_CLASS_NAME);
		  
			String classpath = new String();
		
			// A List which will contain all the classpath entry of the project + the output directory.
			List<String> classpathList = new ArrayList<String>();
	
		   IClasspathEntry[] classpathEntries = javaProject.getExpandedClasspath();
		   for(int i=0; i< classpathEntries.length;i++){
			 IClasspathEntry entry = classpathEntries[i];
			 IRuntimeClasspathEntry cpEntry = JavaRuntime.newArchiveRuntimeClasspathEntry(entry.getResolvedEntry().getPath());
			 classpathList.add(cpEntry.getMemento());
		   }
		   
		   IRuntimeClasspathEntry cpEntry = JavaRuntime.newArchiveRuntimeClasspathEntry(javaProject.getOutputLocation());
		   classpathList.add(cpEntry.getMemento());
		   
		   wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_DEFAULT_CLASSPATH,false);
		   wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_CLASSPATH, classpathList);
		  
		   wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_VM_ARGUMENTS, LauncherPlugin.FRACTAL_ADL_LAUNCHER_JVM_ARGS);
		   wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_PROGRAM_ARGUMENTS, LauncherPlugin.FRACTAL_ADL_BACKEND + " " + definitionNameADL);
		   
		   return wc;
		}catch(Exception e){
			LauncherPlugin.getDefault().getLog().log(new Status(IStatus.ERROR,LauncherPlugin.PLUGIN_ID,UIMessages.ERROR_LOADING_FRACTAL_EXPLORER_EXTENSION,e));
			return null;
		}
	}
}
