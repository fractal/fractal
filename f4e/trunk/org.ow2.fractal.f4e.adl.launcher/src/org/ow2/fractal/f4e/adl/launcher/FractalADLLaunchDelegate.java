package org.ow2.fractal.f4e.adl.launcher;

import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.variables.VariablesPlugin;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.emf.codegen.util.CodeGenUtil;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.internal.core.SetVariablesOperation;
import org.eclipse.jdt.internal.launching.LaunchingMessages;
import org.eclipse.jdt.launching.AbstractJavaLaunchConfigurationDelegate;
import org.eclipse.jdt.launching.ExecutionArguments;
import org.eclipse.jdt.launching.IRuntimeClasspathEntry;
import org.eclipse.jdt.launching.IVMRunner;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jdt.launching.VMRunnerConfiguration;
import org.ow2.fractal.f4e.adl.launcher.configurations.FractalLauncherUtils;

public class FractalADLLaunchDelegate extends AbstractJavaLaunchConfigurationDelegate {
	
	
	
	
	
	public ILaunchManager getLaunchManager() {
		// TODO Auto-generated method stub
		return super.getLaunchManager();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.debug.core.model.ILaunchConfigurationDelegate#launch(org.eclipse.debug.core.ILaunchConfiguration, java.lang.String, org.eclipse.debug.core.ILaunch, org.eclipse.core.runtime.IProgressMonitor)
	 */
	public void launch(ILaunchConfiguration configuration, String mode, ILaunch launch, IProgressMonitor monitor) throws CoreException {
		
		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}
		
		monitor.beginTask(MessageFormat.format("{0}...", new String[]{configuration.getName()}), 3); //$NON-NLS-1$
		// check for cancellation
		if (monitor.isCanceled()) {
			return;
		}
		try {
			monitor.subTask(LaunchingMessages.JavaLocalApplicationLaunchConfigurationDelegate_Verifying_launch_attributes____1); 
							
			String mainTypeName = verifyMainTypeName(configuration);
			IVMRunner runner = getVMRunner(configuration, mode);
	
			File workingDir = verifyWorkingDirectory(configuration);
			String workingDirName = null;
			if (workingDir != null) {
				workingDirName = workingDir.getAbsolutePath();
			}
			
			// Environment variables
			List<String> envp= new ArrayList<String>();
			if(getEnvironment(configuration) != null){
				envp.addAll(Arrays.asList(getEnvironment(configuration)));
			}
			
			// Program & VM arguments
			String pgmArgs = getProgramArguments(configuration);
			String vmArgs = getVMArguments(configuration);
			ExecutionArguments execArgs = new ExecutionArguments(vmArgs, pgmArgs);
			
			// VM-specific attributes
			Map vmAttributesMap = getVMSpecificAttributesMap(configuration);
			
			// Classpath
			
			// Classpath
			String[] classpath = getClasspath(configuration);
			
			// Create a List which will contains all the classpath entry of the current configuration +
			// fractal libraries of the fractal libraries plugin. It allows us to lunch a Fractal ADL file
			// even if the libraries are not present in the classpath of the project
			
			List<String> classpathList = new ArrayList<String>();
			for(int i=0; i< classpath.length;i++){
				classpathList.add(classpath[i]);
			}
			
//			FractalLauncherUtils.ResolvedClasspath resolvedClasspath = FractalLauncherUtils.resolveClasspath(javaProject, javaProject.getRawClasspath(), false,false);
//			IClasspathEntry[] classpathEntries = resolvedClasspath.getResolvedClasspath();
//			   
//			//String[] classpath = getClasspath(configuration);
//			List<String> classpathList = new ArrayList<String>();
//			for(int i=0; i< classpathEntries.length;i++){
//				 IClasspathEntry entry = classpathEntries[i];
//				 IRuntimeClasspathEntry cpEntry = JavaRuntime.newArchiveRuntimeClasspathEntry(entry.getResolvedEntry().getPath());
//				 classpathList.add(cpEntry.getMemento());
//			   }
			
			// Create a List which will contains all the classpath entry of the current configuration +
			// fractal libraries of the fractal libraries plugin. It allows us to lunch a Fractal ADL file
			// even if the libraries are not present in the classpath of the project
			
			
//			for(int i=0; i< classpath.length;i++){
//				classpathList.add(classpath[i]);
//			}
			
			//Arrays.asList(classpath);
//			List<String> bundleClasspathList = new ArrayList<String>();
//			   try{
//				   bundleClasspathList = CodeGenUtil.EclipseUtil.getClasspathPaths(LauncherPlugin.FRACTAL_LIBRARIES_PLUGIN_ID);
//				   Iterator<String> iterator = bundleClasspathList.iterator();
//				   while(iterator.hasNext()){
//					 String entry = iterator.next();  
//					 classpathList.add(entry);
//				   }
//			   }catch(Exception e){
//				   e.printStackTrace();
//			}
			   
			// Create VM config
			VMRunnerConfiguration runConfig = new VMRunnerConfiguration(mainTypeName, classpathList.toArray(new String[0]));
			runConfig.setProgramArguments(execArgs.getProgramArgumentsArray());
			runConfig.setEnvironment(envp.toArray(new String[]{}));
			runConfig.setVMArguments(execArgs.getVMArgumentsArray());
			runConfig.setWorkingDirectory(workingDirName);
			runConfig.setVMSpecificAttributesMap(vmAttributesMap);
		
			// check for cancellation
			if (monitor.isCanceled()) {
				return;
			}		
			
			// stop in main
			prepareStopInMain(configuration);
			
			// done the verification phase
			monitor.worked(1);
			
			monitor.subTask(LaunchingMessages.JavaLocalApplicationLaunchConfigurationDelegate_Creating_source_locator____2); 
			// set the default source locator if required
			setDefaultSourceLocator(launch, configuration);
			monitor.worked(1);		
			
			// Launch the configuration - 1 unit of work
			runner.run(runConfig, launch, monitor);
			
			// check for cancellation
			if (monitor.isCanceled()) {
				return;
			}	
		}
		finally {
			monitor.done();
		}
	
	}

}

