package org.ow2.fractal.f4e.adl.launcher.configurations;


import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

import org.eclipse.core.internal.resources.File;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jdt.core.IClasspathContainer;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaModelStatus;
import org.eclipse.jdt.core.IJavaModelStatusConstants;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.internal.core.ClasspathEntry;
import org.eclipse.jdt.internal.core.ExternalFoldersManager;
import org.eclipse.jdt.internal.core.JavaModelManager;
import org.eclipse.jdt.internal.core.JavaModelStatus;
import org.eclipse.jdt.internal.core.JavaProject;
import org.eclipse.jdt.internal.core.util.Util;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.ow2.fractal.f4e.adl.launcher.LauncherPlugin;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.util.IFractalResource;

public class FractalLauncherUtils {
	public static Definition getSelectedDefinition(ISelection selection) {
		Definition returnDefinition = null;

		if (selection != null && selection instanceof TreeSelection) {
			TreeSelection tselection = (TreeSelection) selection;
			TreePath[] treePathArr = tselection.getPaths();
			Object object = ((IStructuredSelection) selection)
					.getFirstElement();

			if (object != null && object instanceof File) {
				File adlFile = (File) object;
				
				
					Resource resource = FractalTransactionalEditingDomain.getEditingDomain().getResourceSet().getResource(URI
						.createPlatformResourceURI(adlFile.getFullPath().toString(), true), true);
					
				if (resource instanceof IFractalResource && ((IFractalResource)resource).getRootDefinition() != null) {
					returnDefinition = ((IFractalResource)resource).getRootDefinition();
				}else{
					LauncherPlugin.getDefault().getLog().log(new Status(IStatus.ERROR,LauncherPlugin.PLUGIN_ID,LauncherPlugin.FRACTAL_ADL_DEFINITION_NOT_FOUND));
				}
			}else{
				LauncherPlugin.getDefault().getLog().log(new Status(IStatus.ERROR,LauncherPlugin.PLUGIN_ID,LauncherPlugin.FRACTAL_ADL_FILE_NOT_FOUND));
			}
		}

		return returnDefinition;
	}
	
	public static JavaProject getCurrentJavaProject(ISelection selection){
		JavaProject jProject = null;
		
			if(selection != null && selection instanceof TreeSelection){
				TreeSelection tselection = (TreeSelection) selection;
				TreePath[] treePathArr = tselection.getPaths();
				
				if (treePathArr != null && treePathArr.length>0) {
					if (treePathArr[0].getSegment(0) instanceof JavaProject) {
						jProject = (JavaProject) treePathArr[0].getSegment(0);	
					}
				}
		}
		return jProject;
	}
	
	
	
	/**
	 * The ResolvedClasspath is not visible in the JDT and we need
	 * it too use the resolveClasspath with resolveChainedLibraries set to false.
	 * 
	 * @author Yann Davin
	 *
	 */
	public static class ResolvedClasspath {
		IClasspathEntry[] resolvedClasspath;
		IJavaModelStatus unresolvedEntryStatus = JavaModelStatus.VERIFIED_OK;
		HashMap rawReverseMap = new HashMap();
		Map rootPathToResolvedEntries = new HashMap();
		
		public IClasspathEntry[] getResolvedClasspath(){
			return resolvedClasspath;
		}
	}
	
	/**
	 * Copy paste from the JDT
	 * 
	 * @param rawEntry
	 * @param resolvedEntry
	 * @param result
	 * @param resolvedEntries
	 * @param externalFoldersManager
	 */
	private static void addToResult(IClasspathEntry rawEntry, IClasspathEntry resolvedEntry, ResolvedClasspath result, LinkedHashSet resolvedEntries, ExternalFoldersManager externalFoldersManager) {
		IPath resolvedPath;
		if (result.rawReverseMap.get(resolvedPath = resolvedEntry.getPath()) == null) {
			result.rawReverseMap.put(resolvedPath, rawEntry);
			result.rootPathToResolvedEntries.put(resolvedPath, resolvedEntry);
		}
		resolvedEntries.add(resolvedEntry);
		if (resolvedEntry.getEntryKind() == IClasspathEntry.CPE_LIBRARY && ExternalFoldersManager.isExternalFolderPath(resolvedPath)) {
			externalFoldersManager.addFolder(resolvedPath); // no-op if not an external folder or if already registered
		}
	}

	/**
	 * Copy paste from the JDT, we can't use it directly because the 
	 * ResolvedClasspath is not visible
	 * 
	 * @param javaProject
	 * @param rawClasspath
	 * @param usePreviousSession
	 * @param resolveChainedLibraries
	 * @return
	 * @throws JavaModelException
	 */
	public static ResolvedClasspath resolveClasspath(JavaProject javaProject, IClasspathEntry[] rawClasspath, boolean usePreviousSession, boolean resolveChainedLibraries) throws JavaModelException {
		JavaModelManager manager = JavaModelManager.getJavaModelManager();
		ExternalFoldersManager externalFoldersManager = JavaModelManager.getExternalManager();
		ResolvedClasspath result = new ResolvedClasspath();

		LinkedHashSet resolvedEntries = new LinkedHashSet();
		int length = rawClasspath.length;
		for (int i = 0; i < length; i++) {

			IClasspathEntry rawEntry = rawClasspath[i];
			IClasspathEntry resolvedEntry = rawEntry;

			switch (rawEntry.getEntryKind()){

				case IClasspathEntry.CPE_VARIABLE :
					try {
						resolvedEntry = manager.resolveVariableEntry(rawEntry, usePreviousSession);
					} catch (ClasspathEntry.AssertionFailedException e) {
						// Catch the assertion failure and set status instead
						// see bug https://bugs.eclipse.org/bugs/show_bug.cgi?id=55992
						result.unresolvedEntryStatus = new JavaModelStatus(IJavaModelStatusConstants.INVALID_PATH, e.getMessage());
						break;
					}
					if (resolvedEntry == null) {
						result.unresolvedEntryStatus = new JavaModelStatus(IJavaModelStatusConstants.CP_VARIABLE_PATH_UNBOUND, javaProject, rawEntry.getPath());
					} else {
						if (resolveChainedLibraries && resolvedEntry.getEntryKind() == IClasspathEntry.CPE_LIBRARY) {
							// resolve Class-Path: in manifest
							ClasspathEntry[] extraEntries = ((ClasspathEntry) resolvedEntry).resolvedChainedLibraries();
							for (int j = 0, length2 = extraEntries.length; j < length2; j++) {
								addToResult(rawEntry, extraEntries[j], result, resolvedEntries, externalFoldersManager);
							}
						}
						addToResult(rawEntry, resolvedEntry, result, resolvedEntries, externalFoldersManager);
					}
					break;

				case IClasspathEntry.CPE_CONTAINER :
					IClasspathContainer container = usePreviousSession ? manager.getPreviousSessionContainer(rawEntry.getPath(), javaProject) : JavaCore.getClasspathContainer(rawEntry.getPath(), javaProject);
					if (container == null){
						result.unresolvedEntryStatus = new JavaModelStatus(IJavaModelStatusConstants.CP_CONTAINER_PATH_UNBOUND, javaProject, rawEntry.getPath());
						break;
					}

					IClasspathEntry[] containerEntries = container.getClasspathEntries();
					if (containerEntries == null) {
						if (JavaModelManager.CP_RESOLVE_VERBOSE || JavaModelManager.CP_RESOLVE_VERBOSE_FAILURE) {
							verbose_missbehaving_container_null_entries(javaProject, rawEntry.getPath());
						}
						break;
					}

					// container was bound
					for (int j = 0, containerLength = containerEntries.length; j < containerLength; j++){
						ClasspathEntry cEntry = (ClasspathEntry) containerEntries[j];
						if (cEntry == null) {
							if (JavaModelManager.CP_RESOLVE_VERBOSE || JavaModelManager.CP_RESOLVE_VERBOSE_FAILURE) {
								verbose_missbehaving_container(javaProject, rawEntry.getPath(), containerEntries);
							}
							break;
						}
						// if container is exported or restricted, then its nested entries must in turn be exported  (21749) and/or propagate restrictions
						cEntry = cEntry.combineWith((ClasspathEntry) rawEntry);
						
						if (cEntry.getEntryKind() == IClasspathEntry.CPE_LIBRARY) {
							// resolve ".." in library path
							cEntry = cEntry.resolvedDotDot();
							
							if (resolveChainedLibraries) {
								// resolve Class-Path: in manifest
								ClasspathEntry[] extraEntries = cEntry.resolvedChainedLibraries();
								for (int k = 0, length2 = extraEntries.length; k < length2; k++) {
									addToResult(rawEntry, extraEntries[k], result, resolvedEntries, externalFoldersManager);
								}
							}
						}
						addToResult(rawEntry, cEntry, result, resolvedEntries, externalFoldersManager);
					}
					break;

				case IClasspathEntry.CPE_LIBRARY:
					// resolve ".." in library path
					resolvedEntry = ((ClasspathEntry) rawEntry).resolvedDotDot();
					
					if (resolveChainedLibraries) {
						// resolve Class-Path: in manifest
						ClasspathEntry[] extraEntries = ((ClasspathEntry) resolvedEntry).resolvedChainedLibraries();
						for (int k = 0, length2 = extraEntries.length; k < length2; k++) {
							addToResult(rawEntry, extraEntries[k], result, resolvedEntries, externalFoldersManager);
						}
					}

					addToResult(rawEntry, resolvedEntry, result, resolvedEntries, externalFoldersManager);
					break;
				default :
					addToResult(rawEntry, resolvedEntry, result, resolvedEntries, externalFoldersManager);
					break;
			}
		}
		result.resolvedClasspath = new IClasspathEntry[resolvedEntries.size()];
		resolvedEntries.toArray(result.resolvedClasspath);
		return result;
	}
	
	/**
	 * Copy paste from the JDT in order to be able to use the
	 * resolveClasspath function
	 * 
	 * @param project
	 * @param containerPath
	 * @param classpathEntries
	 */
	static void verbose_missbehaving_container(IJavaProject project, IPath containerPath, IClasspathEntry[] classpathEntries) {
		Util.verbose(
			"CPContainer GET - missbehaving container (returning null classpath entry)\n" + //$NON-NLS-1$
			"	project: " + project.getElementName() + '\n' + //$NON-NLS-1$
			"	container path: " + containerPath + '\n' + //$NON-NLS-1$
			"	classpath entries: {\n" + //$NON-NLS-1$
			org.eclipse.jdt.internal.compiler.util.Util.toString(
				classpathEntries,
				new org.eclipse.jdt.internal.compiler.util.Util.Displayable(){
					public String displayString(Object o) {
						StringBuffer buffer = new StringBuffer("		"); //$NON-NLS-1$
						if (o == null) {
							buffer.append("<null>"); //$NON-NLS-1$
							return buffer.toString();
						}
						buffer.append(o);
						return buffer.toString();
					}
				}) +
			"\n	}" //$NON-NLS-1$
		);
	}
	
	/**
	 * Copy paste from the JDT in order to be able to use the
	 * resolveClasspath function
	 * 
	 */
	static void verbose_missbehaving_container_null_entries(IJavaProject project, IPath containerPath) {
		Util.verbose(
			"CPContainer GET - missbehaving container (returning null as classpath entries)\n" + //$NON-NLS-1$
			"	project: " + project.getElementName() + '\n' + //$NON-NLS-1$
			"	container path: " + containerPath + '\n' + //$NON-NLS-1$
			"	classpath entries: <null>" //$NON-NLS-1$
		);
	}
}
