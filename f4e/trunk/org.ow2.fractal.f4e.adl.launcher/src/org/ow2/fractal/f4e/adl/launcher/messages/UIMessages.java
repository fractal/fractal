package org.ow2.fractal.f4e.adl.launcher.messages;

import org.eclipse.osgi.util.NLS;

public class UIMessages {
	private static final String BUNDLE_NAME = "org.ow2.fractal.f4e.adl.launcher.messages.UIMessages";

	public static String ERROR_CAN_NOT_LAUNCH_FRACTAL_ADL_FILE;
	public static String ERROR_CAN_NOT_CONFIGURE_FRACTAL_ADL_LAUNCHER;
	public static String ERROR_LOADING_FRACTAL_EXPLORER_EXTENSION;
	
	static {
		NLS.initializeMessages(BUNDLE_NAME, UIMessages.class);
	}
}
