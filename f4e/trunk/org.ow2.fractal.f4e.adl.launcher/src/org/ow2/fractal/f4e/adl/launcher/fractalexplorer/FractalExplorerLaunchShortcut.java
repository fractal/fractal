package org.ow2.fractal.f4e.adl.launcher.fractalexplorer;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.jdt.internal.core.JavaProject;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jdt.launching.IRuntimeClasspathEntry;
import org.eclipse.jdt.launching.JavaRuntime;
import org.osgi.framework.Bundle;
import org.ow2.fractal.f4e.adl.launcher.FractalLaunchShortcut;
import org.ow2.fractal.f4e.adl.launcher.LauncherPlugin;
import org.ow2.fractal.f4e.adl.launcher.messages.UIMessages;


public class FractalExplorerLaunchShortcut extends FractalLaunchShortcut {
	//public static final String FRACTAL_ADL_EXPLORER="org.objectweb.fractal.explorer.GenericFractalExplorerImpl";
	public static String FRACTAL_EXPLORER_EXTENSION_FILE = "fractal-explorer-extension/properties.xml";
	// previous used Fractal ADL file "org.objectweb.fractal.explorer.GenericFractalExplorer"
	public static String FRACTAL_EXPLORER_ADL_DEFINITION="org.ow2.fractal.f4e.adl.launcher.fractalexplorerextension.FractalExplorer"; 
	public static String FRACTAL_EXTENSION_CLASSPATH="/fractal-explorer-extension/bin";
	
	/**
	 * Return the last segment of a Fractal ADL definition name.
	 * for example on: 'org.objectweb.fractal.definition1' it returns 'definition1'
	 * 
	 * This method is used to set a name to the root component when we launch Fractal Explorer.
	 * 
	 * @param definitionNameADL
	 * @return the last segment of the definition name.
	 */
	protected String getRootComponentName(String definitionNameADL){
		String[] splits = definitionNameADL.split("\\.");
		if(splits.length > 0){
			return splits[splits.length-1];
		}else{
			return definitionNameADL;
		}
	}
	
	
	protected String getFractalExplorerConfigurationFile(){
		Bundle bundle = Platform.getBundle(LauncherPlugin.PLUGIN_ID);
		
		try{
		URL url = FileLocator.toFileURL(bundle.getEntry(FRACTAL_EXPLORER_EXTENSION_FILE));
		String path = url.getPath();
		
		return path;
		}catch(Exception e){
			LauncherPlugin.getDefault().getLog().log(new Status(IStatus.ERROR,LauncherPlugin.PLUGIN_ID,"Fails to load the property file for Fractal explorer extension",e));
			return null;
		}
	}
	
	protected ILaunchConfigurationWorkingCopy createConfiguration(JavaProject javaProject, String definitionNameADL){
		ILaunchConfigurationWorkingCopy wc = super.createConfiguration(javaProject, definitionNameADL);
		wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_PROGRAM_ARGUMENTS, 
				" " + LauncherPlugin.FRACTAL_ADL_BACKEND +
				" -name=\"" + getRootComponentName(definitionNameADL) + "\""+ 
				" -definition=\"" + definitionNameADL + "\"" +
				" -runnableInterface=r" +  
				// By default we need that the user root component has a Runnable interface named 'r' as
				// expected by the Fractal ADL launcher.
				" -config=\"" + getFractalExplorerConfigurationFile() +"\""+
				" -fractal " + FRACTAL_EXPLORER_ADL_DEFINITION);
		
		// We now have to add the Fractal explorer extension classes to the classpath
		Bundle bundle = Platform.getBundle(LauncherPlugin.PLUGIN_ID);
		try{
			URL url = FileLocator.toFileURL(bundle.getEntry("/" + FRACTAL_EXTENSION_CLASSPATH));
			String path = url.getPath();
		
			File file = new File(path);
			if(!file.exists()){
				throw new FileNotFoundException("can't find the file : " + path);
				
			}
			IRuntimeClasspathEntry cpEntry = JavaRuntime.newArchiveRuntimeClasspathEntry(new Path(path));
			
			List<String> classpathList = new ArrayList<String>();
			classpathList = wc.getAttribute(IJavaLaunchConfigurationConstants.ATTR_CLASSPATH,new ArrayList());
			classpathList.add(cpEntry.getMemento());
			wc.setAttribute(IJavaLaunchConfigurationConstants.ATTR_CLASSPATH, classpathList);
		}catch(Exception e){
			LauncherPlugin.getDefault().getLog().log(new Status(IStatus.ERROR,LauncherPlugin.PLUGIN_ID,UIMessages.ERROR_LOADING_FRACTAL_EXPLORER_EXTENSION,e));
		}
		return wc;
	}
	
}
