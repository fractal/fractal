/*******************************************************************************
 * Copyright (c) 2000, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.ow2.fractal.f4e.adl.launcher;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.JavaCore;


/**
 * JUnitPropertyTester provides propertyTester(s) for IResource types
 * for use in XML Expression Language syntax.
 */
public class FractalADLPropertyTester extends PropertyTester {

	private static final String PROPERTY_CAN_LAUNCH_AS_FRACTAL_ADL_TEST= "canLaunchAsFractalADL"; //$NON-NLS-1$

	/* (non-Javadoc)
	 * @see org.eclipse.jdt.internal.corext.refactoring.participants.properties.IPropertyEvaluator#test(java.lang.Object, java.lang.String, java.lang.String)
	 */
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		return true;
		
// TODO check that the ADL file is instanciable
//		if (!(receiver instanceof IAdaptable)) {
//			throw new IllegalArgumentException("Element must be of type 'IAdaptable', is " + receiver == null ? "null" : receiver.getClass().getName()); //$NON-NLS-1$ //$NON-NLS-2$
//		}
//		
//		IJavaElement element;
//		if (receiver instanceof IJavaElement) {
//			element= (IJavaElement) receiver;
//		} else if (receiver instanceof IResource) {
//			element = JavaCore.create((IResource) receiver);
//			if (element == null) {
//				return false;
//			}
//		} else { // is IAdaptable
//			element= (IJavaElement) ((IAdaptable) receiver).getAdapter(IJavaElement.class);
//			if (element == null) {
//				IResource resource= (IResource) ((IAdaptable) receiver).getAdapter(IResource.class);
//				element = JavaCore.create(resource);
//				if (element == null) {
//					return false;
//				}
//				return false;
//			}
//		}
//		if (PROPERTY_CAN_LAUNCH_AS_FRACTAL_ADL_TEST.equals(property)) { 
//			//return isFractalADLFile(element);
//		}
//		throw new IllegalArgumentException("Unknown test property '" + property + "'"); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	private boolean launchable(IJavaElement element) {
		//TODO
		return true;
	}

}
