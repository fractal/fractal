package org.ow2.fractal.f4e.adl.launcher.spoon;

import java.util.Map;
import java.util.TreeMap;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.variables.VariablesPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.internal.core.JavaProject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.ow2.fractal.eclipse.plugin.installer.FractalInstaller;
import org.ow2.fractal.f4e.adl.launcher.LauncherPlugin;

public abstract class AbstractSpoonShortcut implements ILaunchShortcut {
	
	public void launch(IEditorPart editor, String mode) {
	}
	
	public void launch(ISelection selection, final String mode) {
		IFile file = null;
		JavaProject javaProject = null;
		if(!(((IStructuredSelection)selection).getFirstElement() instanceof IResource)) return;
		IResource resource = (IResource)((IStructuredSelection)selection).getFirstElement();
		
		try{
			javaProject = (JavaProject)resource.getProject().getNature(JavaCore.NATURE_ID);
		}catch(Exception e){
			LauncherPlugin.getDefault().getLog().log(new Status(IStatus.ERROR,LauncherPlugin.PLUGIN_ID,"The enclosing project is not a Java project",e));
		}
		
		ILaunchConfigurationWorkingCopy wc = null;
		
		if(	selection != null && (selection instanceof StructuredSelection)) {
			//Object object = ((StructuredSelection)selection).getFirstElement();
			wc = createConfiguration(javaProject,resource);
		}
		
		try {
			if(wc != null){
				ILaunchConfiguration config = wc.doSave();
				Job.getJobManager();
				ILaunch launch = DebugUITools.buildAndLaunch(config, mode, new NullProgressMonitor());
				
				class UpdateFolder extends Job {
					private  IResource resource;
					private  JavaProject javaProject;
					private ILaunch launch;
					
					public UpdateFolder(IResource resource, JavaProject javaProject, ILaunch launch){
						super("Update output folder");
						this.resource = resource;
						this.javaProject = javaProject;
						this.launch = launch;
					}
					
					protected IStatus run(IProgressMonitor monitor) {
						// TODO Auto-generated method stub
						try{
							// TODO: find a better way than the loop/sleep.
							// It could be great if we can find a way to be notified when
							// the process ends
							while(!launch.getProcesses()[0].isTerminated()){
								this.getThread().sleep(1000);
							}
							resource.getProject().getFolder(
									javaProject.getOutputLocation().removeFirstSegments(1).toOSString()).refreshLocal(IResource.DEPTH_INFINITE, new NullProgressMonitor());
							}catch(Exception e){
								e.printStackTrace();
							}
							return new Status(IStatus.OK,LauncherPlugin.PLUGIN_ID,"Ouput Folder updated");
					}

					
					public boolean shouldRun() {
						return true;
					}
					
				}
				Job job = new UpdateFolder(resource,javaProject,launch);
				job.schedule();
				
			}
		} catch (Exception e) {
			LauncherPlugin.getDefault().getLog().log(new Status(IStatus.ERROR,LauncherPlugin.PLUGIN_ID,"Can't generate Fractal ADL files",e));
		}
	}
	
	public void appendFractalHome(ILaunchConfigurationWorkingCopy wc){
		Map map = new TreeMap<String,String>();
		try{
			wc.getAttribute(ILaunchManager.ATTR_ENVIRONMENT_VARIABLES,map);
			map.put("FRACTAL_HOME", "${FRACTAL_HOME}");
		}catch(Exception e){
			LauncherPlugin.getDefault().getLog().log(new Status(IStatus.ERROR,LauncherPlugin.PLUGIN_ID,"",e));
		}
		wc.setAttribute(ILaunchManager.ATTR_ENVIRONMENT_VARIABLES, map);
	}
	
	abstract protected ILaunchConfigurationWorkingCopy createConfiguration(JavaProject javaProject, Object selection);
}
