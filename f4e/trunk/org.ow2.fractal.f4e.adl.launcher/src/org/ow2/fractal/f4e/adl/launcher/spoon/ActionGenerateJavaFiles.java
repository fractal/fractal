package org.ow2.fractal.f4e.adl.launcher.spoon;

import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchPart;

public class ActionGenerateJavaFiles implements org.eclipse.ui.IObjectActionDelegate{
	private ISelection selection;
	
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		
	}

	public void run(IAction action) {
		ILaunchShortcut launcher = new SpoonJavaShortcut();
		launcher.launch(selection, "run");		
	}

	public void selectionChanged(IAction action, ISelection selection) {
		this.selection = selection;
	}

}
