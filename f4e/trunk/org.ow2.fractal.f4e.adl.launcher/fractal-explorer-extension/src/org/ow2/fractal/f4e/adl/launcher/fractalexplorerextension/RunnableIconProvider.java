package org.ow2.fractal.f4e.adl.launcher.fractalexplorerextension;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.objectweb.util.explorer.api.IconProvider;

public class RunnableIconProvider implements IconProvider{
	public static String ICON_FOLDER = "org/ow2/fractal/f4e/adl/launcher/fractalexplorerextension/";
	public static String RUNNABLE_ICON_FILE = "runnable.png";
	public Object newIcon(Object arg0) {
		Icon icon = new ImageIcon(Thread.currentThread().getContextClassLoader().getResource(ICON_FOLDER + RUNNABLE_ICON_FILE));
		return icon;
	}

}
