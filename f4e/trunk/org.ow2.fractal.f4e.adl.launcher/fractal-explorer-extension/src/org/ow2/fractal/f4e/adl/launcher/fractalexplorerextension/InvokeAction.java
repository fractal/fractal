package org.ow2.fractal.f4e.adl.launcher.fractalexplorerextension;
import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.TreeView;


/**
 * This action allows to invoke the run method of a <code>java.util.Runnable</code> object.
 *
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * 
 * @version 0.1
 */
public class InvokeAction
    implements MenuItem 
{

    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#actionPerformed(org.objectweb.util.explorer.api.MenuItemTreeView)
     */
    public void actionPerformed(MenuItemTreeView e) {
        Runnable r = (Runnable)e.getSelectedObject();
        InvokeThread t = new InvokeThread(r);
        t.start();
    }

    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#getStatus(org.objectweb.util.explorer.api.TreeView)
     */
    public int getStatus(TreeView treeView) {
        return MenuItem.ENABLED_STATUS;
    }

    public class InvokeThread extends Thread {
         
        protected Runnable r_ = null;
         
        public InvokeThread(Runnable r) {
            r_ = r;
        }
 
        public void run() {
            r_.run();
        }
     }
 
}