package org.ow2.fractal.f4e.fractal.presentation;

import java.io.File;
import java.util.zip.ZipFile;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gmf.runtime.common.ui.util.FileUtil;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.internal.core.JarEntryFile;
import org.eclipse.jdt.internal.ui.javaeditor.JarEntryEditorInput;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.ResourceUtil;
import org.eclipse.ui.part.FileEditorInput;
import org.ow2.fractal.f4e.fractal.AttributesController;
import org.ow2.fractal.f4e.fractal.Content;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.presentation.util.DefinitionLocatorHelper;
import org.ow2.fractal.f4e.fractal.util.IDefinitionLocatorHelper;
import org.ow2.fractal.f4e.fractal.util.SDefinitionLoadLocator;

/**
 * Listener to listen double click event.
 * When it occurs on a DefinitionCall item, the referenced Fractal ADL definition is opened in a new window.
 * When it occurs on a Interface item, the referenced Java file by the 'signature' attribute is opened.
 * When it occurs on a Content item, the referenced Java file by the 'class' attribute is opened.
 * When it occurs on a AttributesController item, the referenced Java file by the 'signature' attribute is opened.
 * 
 * @author Yann Davin
 */
public class DoubleClickListener implements IDoubleClickListener{
	 public void doubleClick(DoubleClickEvent event){
		handleDefinitionCallDoubleClick(event);
	 }
	 
	 /**
	  * 
	  * @param event
	  */
	 protected void handleDefinitionCallDoubleClick(DoubleClickEvent event){
		TreeSelection selection = (TreeSelection)event.getSelection();
		
		Object elem = selection.getFirstElement();
		if(elem != null && elem instanceof DefinitionCall){
			Definition definition = ((DefinitionCall)elem).getDefinition();
			if(definition != null && definition.eResource() != null){
				openFractalFile(getJavaProject(((DefinitionCall)elem).eResource()),definition.eResource());
			}
		}
		
		// When we double click on a Content, we open the Java file used as content
		if(elem != null && elem instanceof Content){
			Content content = (Content)elem;
			if(content.getMergedClass() != null){
				openJavaFile(getJavaProject(((Content)elem).eResource()),content.getMergedClass());
			}else if(content.getClass() != null){
				openJavaFile(getJavaProject(((Content)elem).eResource()),content.getClass_());
			}
		}
		
		// When we double click on a Interface, we open the Java file used as signature
		if(elem != null && elem instanceof Interface){
			Interface interface_ = (Interface)elem;
			if(interface_.getMergedSignature() != null){
				openJavaFile(getJavaProject(interface_.eResource()),interface_.getMergedSignature());
			}else if(interface_.getSignature() != null){
				openJavaFile(getJavaProject(interface_.eResource()),interface_.getSignature());
			}
		}
		
		// When we double click on a AttributesController, we open the Java file used as signature
		if(elem != null && elem instanceof AttributesController){
			AttributesController attributesController = (AttributesController)elem;
			if(attributesController.getMergedSignature() != null){
				openJavaFile(getJavaProject(attributesController.eResource()),attributesController.getMergedSignature());
			}else if(attributesController.getSignature() != null){
				openJavaFile(getJavaProject(attributesController.eResource()),attributesController.getSignature());
			}
		}
	 }
	 
	 /**
	  * 
	  * @param javaProject the java project in which the java file will be searched.
	  * @param the java element name e.g "helloworld.MyClass"
	  */
	protected void openJavaFile(IJavaProject javaProject, final String javaElementName){
		try{
			final IJavaElement javaElement = javaProject.findElement(new Path(javaElementName.replace('.', '/') + ".java"));
			if(javaElement != null){
				JavaUI.openInEditor(javaElement);
			}

		}catch(Exception e){
			FractalEditorPlugin.getPlugin().log(e);
		}
	}
	
	 /**
	 * @param resource of the Fractal ADL file
	 * @return the Java project which holds the Fractal ADL file resource
	 */
	protected IJavaProject getJavaProject(Resource resource){
		IDefinitionLocatorHelper helper = SDefinitionLoadLocator.getInstance().getHelper(resource.getURI());
		if(helper instanceof DefinitionLocatorHelper){
			return ((DefinitionLocatorHelper)helper).getJavaProject();
		}
		return null;
	 }
	 
	 protected void openFractalFile(IJavaProject javaProject, final Resource resource){
	
				class OpenEditorCommand implements Runnable {
					IJavaProject javaProject;
					Resource resource;
					
					public OpenEditorCommand(IJavaProject javaProject, Resource resource){
						this.javaProject = javaProject;
						this.resource = resource;
					}
					
					public void run() {
						try {

							IWorkbenchPage page = getActivePage();

							if (page != null) {
								IEditorPart activeEditor = page
										.getActiveEditor();
								IEditorInput editor = null;
								
								if(resource.getURI().isArchive()){
									try{
										String trunkedPath = 	resource.getURI().authority().substring(0, resource.getURI().authority().length()-1).replace("file://", "");
										File file = new File(trunkedPath);
										String jarEntryFilePath = resource.getURI().path();
										ZipFile jarFile = new ZipFile(file);
										String fractalFile = resource.getURI().path();
										String jarFilePath = resource.getURI().authority().substring(0, resource.getURI().authority().length()-1).replace("file://", "");
										org.eclipse.core.runtime.Path path = new org.eclipse.core.runtime.Path(jarFilePath);
										IPackageFragmentRoot packageFragmentRoot = OpenEditorCommand.this.javaProject.findPackageFragmentRoot(path);
										
										if(packageFragmentRoot == null){
											//May be the jar has been registred in the classpath with its relative path
											String relativePath = FileUtil.getRelativePath(path.toString(), Platform.getLocation().toString());
											packageFragmentRoot = OpenEditorCommand.this.javaProject.findPackageFragmentRoot(new org.eclipse.core.runtime.Path("/" + relativePath));
										}
										
										JarEntryFile jarEntryFile = 
											new JarEntryFile(jarEntryFilePath);
										jarEntryFile.setParent(packageFragmentRoot);
										editor = new JarEntryEditorInput(jarEntryFile);
									}catch(Exception e){
										FractalEditorPlugin.INSTANCE.log(e);
									}
								}else{
									editor = new FileEditorInput(WorkspaceSynchronizer.getFile(resource));
								}
								
								page.openEditor(editor,
												"org.ow2.fractal.f4e.fractal.presentation.FractalEditorID",
											false);
								}
								// restore the previously active editor to
								// active
								// state
								// if (activeEditor != null) {
								// page.activate(activeEditor);
								// }
						} catch (PartInitException e) {
							FractalEditorPlugin.getPlugin().log(e.getStatus());
						}
					}
					
				};
				
				Display.getDefault().asyncExec(new OpenEditorCommand(javaProject,resource));
			}
		;
	 
	/** 
	 * Obtains the currently active workbench page.
	 * @return the active page, or <code>null</code> if none is active
	 */
	private IWorkbenchPage getActivePage() {
		IWorkbenchPage result = null;
		
		IWorkbench bench = PlatformUI.getWorkbench();
		if (bench != null) {
			IWorkbenchWindow window = bench.getActiveWorkbenchWindow();
			
			if (window != null) {
				result = window.getActivePage();
			}
		}
		
		return result;
	}
}
