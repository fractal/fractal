package org.ow2.fractal.f4e.fractal.presentation.action;

import java.io.File;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPart;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.util.IFractalResource;


/**
 * @author Yann Davin
 * Implementation of the Fractal reload user action available on the contextual pop-up menu raised
 * when the user click right on a Fractal ADL file.
 */
public class ReloadFractalFile implements org.eclipse.ui.IObjectActionDelegate{
private IStructuredSelection structuredSelection;
	
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub
		
	}

	public void run(IAction action) {
		// TODO Auto-generated method stub
		if(structuredSelection != null && structuredSelection.getFirstElement() instanceof IFile){
			IFile selectedFile = (IFile)(structuredSelection.getFirstElement());
			if(selectedFile.getRawLocation() != null && selectedFile.getFullPath() !=null){
				ResourceSet resourceSet = FractalTransactionalEditingDomain.getEditingDomain().getResourceSet();
		
				Resource resource = resourceSet.getResource(URI.createPlatformResourceURI(selectedFile.getFullPath().toString(), false),true);
				if(resource != null && resource instanceof IFractalResource){
					try{
						((IFractalResource)resource).reload();
						
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}	
		}
	}

	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
		structuredSelection = (IStructuredSelection)selection;
		
	}
	
}
