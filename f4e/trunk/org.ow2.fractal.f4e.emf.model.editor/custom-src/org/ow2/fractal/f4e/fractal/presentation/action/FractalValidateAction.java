package org.ow2.fractal.f4e.fractal.presentation.action;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.ui.dialogs.DiagnosticDialog;
import org.eclipse.emf.common.ui.viewer.IViewerProvider;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.ui.EMFEditUIPlugin;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ISetSelectionTarget;
import org.ow2.fractal.f4e.fractal.util.FractalResourcesUtil;
import org.ow2.fractal.f4e.fractal.util.IFractalResource;

public class FractalValidateAction extends org.eclipse.emf.edit.ui.action.ValidateAction{
	private static FractalResourcesUtil fractalResourcesUtil = new FractalResourcesUtil();
	
	protected void handleDiagnostic(Diagnostic diagnostic)
	  {
		int severity = diagnostic.getSeverity();
	    String title = null;
	    String message = null;

	    if(selectedObjects != null && selectedObjects.get(0) instanceof EObject &&
	    		((EObject)selectedObjects.get(0)).eResource() != null){
	    	eclipseResourcesUtil.deleteMarkers(((EObject)selectedObjects.get(0)).eResource());
        }
	    
	    if (severity == Diagnostic.ERROR || severity == Diagnostic.WARNING)
	    {
	      title = EMFEditUIPlugin.INSTANCE.getString("_UI_ValidationProblems_title");
	      message = EMFEditUIPlugin.INSTANCE.getString("_UI_ValidationProblems_message");
	    }
	    else
	    {
	      title = EMFEditUIPlugin.INSTANCE.getString("_UI_ValidationResults_title");
	      message = EMFEditUIPlugin.INSTANCE.getString(severity == Diagnostic.OK ? "_UI_ValidationOK_message" : "_UI_ValidationResults_message");
	    }

	    int result = 0;
	    if (diagnostic.getSeverity() == Diagnostic.OK)
	    {
	      MessageDialog.openInformation(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), title, message);
	      result = Window.CANCEL;
	    }
	    else
	    {
	      result = DiagnosticDialog.open
	        (PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), title, message, diagnostic);
	    }

	    if (eclipseResourcesUtil != null)
	    {
	     
	      if (result == Window.OK)
	      {
	        if (!diagnostic.getChildren().isEmpty())
	        {
	          List<?> data = (diagnostic.getChildren().get(0)).getData();
	          if (!data.isEmpty() && data.get(0) instanceof EObject)
	          {
	            Object part = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActivePart();
	            if (part instanceof ISetSelectionTarget)
	            {
	              ((ISetSelectionTarget)part).selectReveal(new StructuredSelection(data.get(0)));
	            }
	            else if (part instanceof IViewerProvider)
	            {
	              Viewer viewer = ((IViewerProvider)part).getViewer();
	              if (viewer != null)
	              {
	                viewer.setSelection(new StructuredSelection(data.get(0)), true);
	              }
	            }
	          }
	        }
	    
	          for (Diagnostic childDiagnostic : diagnostic.getChildren())
	          {
	        	  Iterator<?> iterator = childDiagnostic.getData().iterator();
	        	  while(iterator.hasNext()){
	        		  Object object = iterator.next();
	        		  if(object instanceof EObject){
	        			  Resource resource = ((EObject)object).eResource();
	        			  if(resource != null && resource instanceof IFractalResource){
	        				  eclipseResourcesUtil.createMarkers(resource, childDiagnostic);
	        			  }
	        		  }
	        	  }
//	        	  if((diagnostic.getChildren().get(0)) != 
//	        	  Resource resource = domain.getResourceSet().getResources().get(0);
//	    	      if (resource != null)
//	    	      {
//	    	        eclipseResourcesUtil.deleteMarkers(resource);
//	    	      }
	          }
	        }
	      }
	  }
}
