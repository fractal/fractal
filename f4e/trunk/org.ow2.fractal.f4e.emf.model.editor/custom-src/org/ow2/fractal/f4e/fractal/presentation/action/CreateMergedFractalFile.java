package org.ow2.fractal.f4e.fractal.presentation.action;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPart;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.FractalFactory;
import org.ow2.fractal.f4e.fractal.util.FractalResourceSetImpl;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.util.IFractalResource;
import org.ow2.fractal.f4e.fractal.util.visitor.MergeVisitor;


public class CreateMergedFractalFile implements org.eclipse.ui.IObjectActionDelegate{
	private IStructuredSelection structuredSelection;
	
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub
		
	}

	public void run(IAction action) {
		// TODO Auto-generated method stub
		if(structuredSelection != null && structuredSelection.getFirstElement() instanceof IFile){
			IFile selectedFile = (IFile)(structuredSelection.getFirstElement());
			if(selectedFile.getRawLocation() != null && selectedFile.getFullPath() !=null){
				ResourceSet resourceSet = FractalTransactionalEditingDomain.getEditingDomain().getResourceSet();
		
				Resource resource = resourceSet.getResource(URI.createPlatformResourceURI(selectedFile.getFullPath().toString(), false),true);
				if(resource != null){
					String filename = selectedFile.getRawLocation().removeFileExtension().lastSegment().concat("Merged");
					collectAll(resource,new File(selectedFile.getRawLocation().removeFileExtension().removeLastSegments(1).append(filename).addFileExtension("fractal").toOSString()));
				}
			}	
		}
	}

	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
		structuredSelection = (IStructuredSelection)selection;
		
	}
	
	
	private void collectAll(final Resource resource, File file){
    	//get the resourceset
    	ResourceSet resourceSet = new FractalResourceSetImpl();//resource.getResourceSet();
    	
    	 Resource complete_resource = resourceSet.createResource(URI.createFileURI(file.getAbsolutePath()));
    	
    	
    	if(resource instanceof IFractalResource){
    		MergeVisitor visitor = new MergeVisitor(complete_resource);
			final Definition definition = ((IFractalResource) resource).getRootDefinition();
			FractalFactory.eINSTANCE.mustCreateAdapters(false);
			final AbstractComponent copy = (AbstractComponent)EcoreUtil.copy(definition);
			FractalFactory.eINSTANCE.mustCreateAdapters(true);
			
			final Definition definitionCopy = (Definition)visitor.doSwitch(copy);
			
			Command cmd = new RecordingCommand(FractalTransactionalEditingDomain
					.getEditingDomain()) {
				protected void doExecute() {
					definitionCopy.setName(definitionCopy.getName().concat("Merged"));
				}
			};
		
			FractalTransactionalEditingDomain.getEditingDomain().getCommandStack().execute(cmd);
			
    	}

    	try {
    		Map options = new HashMap();
    		options.put(XMIResource.OPTION_DECLARE_XML, Boolean.TRUE);

    		complete_resource.save(options);

    	} catch (IOException e) {
    		e.printStackTrace();
    	}
         
    }
}
