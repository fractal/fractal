package org.ow2.fractal.f4e.fractal.presentation;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.Widget;
import org.ow2.fractal.f4e.fractal.plugin.FractalPlugin;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;

public class FractalTreeViewer extends TreeViewer{
	

	public FractalTreeViewer(Composite parent) {
		super(parent);
	}

	
	public FractalTreeViewer(Composite parent, int style) {
		super(parent,style);
	}

	public FractalTreeViewer(Tree tree) {
		super(tree);
		this.addDoubleClickListener(new DoubleClickListener());
	}

	
	protected void createTreeItem(Widget parent, Object element, int index) {
		super.createTreeItem(parent, element, index);
		//		Item item = newItem(parent, SWT.NULL, index);
//		updateItem(item, element);
//		updatePlus(item, element);
//		if(item instanceof TreeItem && element instanceof EObject){
//			ComposedAdapterFactory adapterFactory = (ComposedAdapterFactory)this.getContentProvider().getAdapterFactory();
//			
//				Object colorProvider = adapterFactory.getFactoryForType(INotEditableItemContentProvider.class).adapt(element, INotEditableItemContentProvider.class);
//				if(colorProvider != null){
//					Object color = ((IItemColorProvider)colorProvider).getBackground(element);
//					if(color != null && color instanceof Color){
//						((TreeItem)item).setBackground((Color)color);
//					}
//				}else{
//					((TreeItem)item).setBackground(new Color(null,255,255,255));
//				}
//		}
//		if(element instanceof Interface){
//			if(parent.getData() != null && parent.getData() instanceof Binding){
//				if(((Binding)parent.getData()).getClientInterface() == element){
//					item.setText(((Binding)parent.getData()).getClient());
//				}else if(((Binding)parent.getData()).getServerInterface() == element){
//					item.setText(((Binding)parent.getData()).getServer());
//				}
//			}
//		}
	}

	public void setContentProvider(AdapterFactoryContentProvider provider) {
		// TODO Auto-generated method stub
		super.setContentProvider(provider);
	}
	
	public AdapterFactoryContentProvider getContentProvider(){
		return (AdapterFactoryContentProvider)super.getContentProvider();
	}


	protected void doUpdateItem(final Item item, final Object element) {
		super.doUpdateItem(item, element);
	}
	

	public void refresh() {
		// TODO Auto-generated method stub
		super.refresh();
	}

}
