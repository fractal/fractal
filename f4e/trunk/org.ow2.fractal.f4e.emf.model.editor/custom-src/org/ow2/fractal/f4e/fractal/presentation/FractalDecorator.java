package org.ow2.fractal.f4e.fractal.presentation;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.edit.provider.ComposedImage;
import org.eclipse.gmf.runtime.common.ui.util.OverlayImageDescriptor;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ILabelDecorator;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.LabelProviderChangedEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.ow2.fractal.f4e.fractal.adapter.helper.IHelperAdapter;
import org.ow2.fractal.f4e.fractal.plugin.FractalPlugin;
import org.ow2.fractal.f4e.fractal.provider.FractalEditPlugin;

public class FractalDecorator extends LabelProvider implements ILabelDecorator{
	public static String DECORATOR_ID = "org.ow2.fractal.f4e.fractal.presentation.decorator";
	public static String IMG_ERROR = "icons/full/error.png";
	
	public FractalDecorator()
	{
		super();
		try{
			Image errorImage = new Image(null,FileLocator.openStream(FractalEditPlugin.INSTANCE.getPlugin().getBundle(),new org.eclipse.core.runtime.Path(IMG_ERROR), false));
			FractalEditorPlugin.getPlugin().getImageRegistry().put(IMG_ERROR, errorImage);
		}catch(Exception e){
			FractalEditPlugin.INSTANCE.getPlugin().getLog().log(new Status(IStatus.ERROR,FractalPlugin.PLUGIN_ID,"",e));
		}
	} 
	
	public static ImageRegistry getImageRegistry(){
		return FractalEditorPlugin.getPlugin().getImageRegistry();
	}
	
	public Image decorateImage(Image image, Object element) {
		Image result = null;
		if (element instanceof IHelperAdapter && ((IHelperAdapter)element).getHelper().getMarkerHelper().hasErrors())
		{
			OverlayImageDescriptor overlayIcon = new OverlayImageDescriptor(image, getImageRegistry().getDescriptor(IMG_ERROR));
			result = overlayIcon.createImage();
		}
		return result;
	}

	public String decorateText(String text, Object element) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public void refresh(List resourcesToBeUpdated)
	{
	  // resourcesToBeUpdated is a list of resources whose decorators
	  // need to be changed. The persistent property of the resources 
	  // has been changed and hence its decorators should change

	  // Check to see whether the custom decoration is enabled 
	  FractalDecorator demoDecorator = getDemoDecorator();
	  if (demoDecorator == null)
	  {
	    // Decorator is not enabled.. Don't decorate the resources.
	    return;
	  }

	  // Fire a label provider changed event to decorate the 
	  // resources whose image needs to be updated
	fireLabelEvent(new LabelProviderChangedEvent(demoDecorator,
	      resourcesToBeUpdated.toArray()));
	} 
	
	private void fireLabelEvent(final LabelProviderChangedEvent event)
	{ 
	  // Decorate using current UI thread
	Display.getDefault().asyncExec(new Runnable()
	  {
	    public void run()
	    {
	      // Fire a LabelProviderChangedEvent to notify eclipse views
	      // that label provider has been changed for the resources
	   fireLabelProviderChanged(event);
	    }
	  });
	}
	
	public static FractalDecorator getDemoDecorator()
	{
	  if(FractalEditorPlugin.getPlugin().getWorkbench().getDecoratorManager().getEnabled(DECORATOR_ID)){
		  return (FractalDecorator) FractalEditorPlugin.getPlugin().getWorkbench().getDecoratorManager().getLabelDecorator(DECORATOR_ID);
	  }
	  return null;
	}	
}
