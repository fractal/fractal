package org.ow2.fractal.f4e.fractal.presentation;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.ui.provider.PropertySource;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.ui.provider.TransactionalAdapterFactoryContentProvider;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.IPropertySource2;

public class FractalTransactionalAdapterFactoryContentProvider extends TransactionalAdapterFactoryContentProvider{
	public FractalTransactionalAdapterFactoryContentProvider(TransactionalEditingDomain domain, AdapterFactory adapterFactory) {
		super(domain,adapterFactory);
	}
	
	protected IPropertySource createPropertySource(Object object, IItemPropertySource itemPropertySource)
	  {
	    return new PropertySource2(object, itemPropertySource);
	  }
	
	public class PropertySource2 extends PropertySource implements IPropertySource2{
		 public PropertySource2(Object object, IItemPropertySource itemPropertySource){
			 super(object,itemPropertySource);
		 }
		 
		 public boolean isPropertyResettable(Object id){
			// return itemPropertySource.getPropertyDescriptor(object, id)
			 if(id instanceof String && object instanceof EObject){
				 return (((EObject)object).eClass().getEStructuralFeature((String)id)).isUnsettable();
			 }
			 return false;
		 }
	}
}
