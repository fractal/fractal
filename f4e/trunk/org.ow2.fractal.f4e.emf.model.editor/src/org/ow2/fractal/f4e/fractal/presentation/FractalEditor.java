package org.ow2.fractal.f4e.fractal.presentation;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.commands.operations.IOperationHistory;
import org.eclipse.core.commands.operations.IOperationHistoryListener;
import org.eclipse.core.commands.operations.IUndoContext;
import org.eclipse.core.commands.operations.IUndoableOperation;
import org.eclipse.core.commands.operations.ObjectUndoContext;
import org.eclipse.core.commands.operations.OperationHistoryEvent;
import org.eclipse.core.commands.operations.TriggeredOperations;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.command.CommandStackListener;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.ui.MarkerHelper;
import org.eclipse.emf.common.ui.ViewerPane;
import org.eclipse.emf.common.ui.editor.ProblemEditorPart;
import org.eclipse.emf.common.ui.viewer.IViewerProvider;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.emf.edit.provider.AdapterFactoryItemDelegator;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.resource.ResourceItemProviderAdapterFactory;
import org.eclipse.emf.edit.ui.action.EditingDomainActionBarContributor;
import org.eclipse.emf.edit.ui.celleditor.AdapterFactoryTreeEditor;
import org.eclipse.emf.edit.ui.dnd.EditingDomainViewerDropAdapter;
import org.eclipse.emf.edit.ui.dnd.LocalTransfer;
import org.eclipse.emf.edit.ui.dnd.ViewerDragAdapter;
import org.eclipse.emf.edit.ui.provider.UnwrappingSelectionProvider;
import org.eclipse.emf.edit.ui.util.EditUIMarkerHelper;
import org.eclipse.emf.edit.ui.util.EditUIUtil;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.EMFCommandOperation;
import org.eclipse.emf.workspace.IWorkspaceCommandStack;
import org.eclipse.emf.workspace.ResourceUndoContext;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gmf.runtime.diagram.ui.properties.views.PropertiesBrowserPage;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.internal.core.JarEntryFile;
import org.eclipse.jdt.internal.ui.javaeditor.JarEntryEditorInput;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.DecoratingLabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.dialogs.SaveAsDialog;
import org.eclipse.ui.ide.IGotoMarker;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.MultiPageEditorPart;
import org.eclipse.ui.part.MultiPageSelectionProvider;
import org.eclipse.ui.progress.IProgressConstants;
import org.eclipse.ui.views.contentoutline.ContentOutline;
import org.eclipse.ui.views.contentoutline.ContentOutlinePage;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;
import org.eclipse.ui.views.properties.IPropertySheetPage;
import org.eclipse.ui.views.properties.PropertySheet;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertySheetPageContributor;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.adapter.OperationHistoryListenerAdapter;
import org.ow2.fractal.f4e.fractal.adapter.factory.OperationHistoryListenerAdapterFactory;
import org.ow2.fractal.f4e.fractal.commands.FractalSaveCommand;
import org.ow2.fractal.f4e.fractal.commands.IFractalSaveCommand;
import org.ow2.fractal.f4e.fractal.notification.IFractalNotification;
import org.ow2.fractal.f4e.fractal.plugin.FractalPlugin;
import org.ow2.fractal.f4e.fractal.presentation.util.DefinitionLocatorHelper;
import org.ow2.fractal.f4e.fractal.provider.ExtendedFractalItemProviderAdapterFactory;
import org.ow2.fractal.f4e.fractal.provider.FractalEditPlugin;
import org.ow2.fractal.f4e.fractal.util.FractalTransactionalEditingDomain;
import org.ow2.fractal.f4e.fractal.util.IExceptionHandler;
import org.ow2.fractal.f4e.fractal.util.IFractalResource;
import org.ow2.fractal.f4e.fractal.util.SDefinitionLoadLocator;
import org.ow2.fractal.f4e.fractal.util.visitor.MergeAction;
import org.ow2.fractal.f4e.fractal.util.visitor.ModelUpdater;
import org.ow2.fractal.f4e.fractal.util.visitor.UpdateMergeJob;


/**
 * This is an example of a Fractal model editor.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated NOT
 */
public class FractalEditor
	extends MultiPageEditorPart
	implements IEditingDomainProvider, ISelectionProvider, IMenuListener, IViewerProvider, IGotoMarker, ITabbedPropertySheetPageContributor {
	
	/**
	 * This keeps track of the editing domain that is used to track all changes to the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected AdapterFactoryEditingDomain editingDomain;


	/**
	 * Yann hand modifs.
	 * The undo context for this editor's Undo and Redo menus
	 * @generated NOT
	 */
	protected IUndoContext undoContext;
	
	/**
	 * Yann hand modifs.
	 * The (one and only) resource that we are editing. 
	 * The EMF-generated editor edits any number of resources.
	 * @generated NOT
	 */
	protected Resource resource;
	
	/**
	 * This is the one adapter factory used for providing views of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComposedAdapterFactory adapterFactory;

	/**
	 * This is the content outline page.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IContentOutlinePage contentOutlinePage;

	/**
	 * This is a kludge...
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IStatusLineManager contentOutlineStatusLineManager;

	/**
	 * This is the content outline page's viewer.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TreeViewer contentOutlineViewer;

	/**
	 * This is the property sheet page.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PropertiesBrowserPage propertySheetPage;

	/**
	 * This is the viewer that shadows the selection in the content outline.
	 * The parent relation must be correctly defined for this to work.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TreeViewer selectionViewer;

	/**
	 * This keeps track of the active viewer pane, in the book.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ViewerPane currentViewerPane;

	/**
	 * This keeps track of the active content viewer, which may be either one of the viewers in the pages or the content outline viewer.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Viewer currentViewer;

	/**
	 * This listens to which ever viewer is active.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISelectionChangedListener selectionChangedListener;

	/**
	 * This keeps track of all the {@link org.eclipse.jface.viewers.ISelectionChangedListener}s that are listening to this editor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Collection<ISelectionChangedListener> selectionChangedListeners = new ArrayList<ISelectionChangedListener>();

	/**
	 * This keeps track of the selection of the editor as a whole.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISelection editorSelection = StructuredSelection.EMPTY;

	/**
	 * The MarkerHelper is responsible for creating workspace resource markers presented
	 * in Eclipse's Problems View.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MarkerHelper markerHelper = new EditUIMarkerHelper();

	/**
	 * This selection provider coordinates the selections of the various editor parts.
	 */
	protected MultiPageSelectionProvider selectionProvider;
	
	/**
	 * This listens for when the outline becomes active
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IPartListener partListener =
		new IPartListener() {
			public void partActivated(IWorkbenchPart p) {
				if (p instanceof ContentOutline) {
					if (((ContentOutline)p).getCurrentPage() == contentOutlinePage) {
						getActionBarContributor().setActiveEditor(FractalEditor.this);

						setCurrentViewer(contentOutlineViewer);
					}
				}
				else if (p instanceof PropertySheet) {
					if (((PropertySheet)p).getCurrentPage() == propertySheetPage) {
						getActionBarContributor().setActiveEditor(FractalEditor.this);
						handleActivate();
					}
				}
				else if (p == FractalEditor.this) {
					handleActivate();
				}
			}
			public void partBroughtToTop(IWorkbenchPart p) {
				// Ignore.
			}
			public void partClosed(IWorkbenchPart p) {
				// Ignore.
			}
			public void partDeactivated(IWorkbenchPart p) {
				// Ignore.
			}
			public void partOpened(IWorkbenchPart p) {
				// Ignore.
			}
		};

	/**
	 * Resources that have been removed since last activation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Collection<Resource> removedResources = new ArrayList<Resource>();

	/**
	 * Resources that have been changed since last activation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Collection<Resource> changedResources = new ArrayList<Resource>();

	/**
	 * Resources that have been moved since last activation.
	 */
	//.CUSTOM: Demonstrates the WorkspaceSynchronizer's handling of moves
	protected Map<Resource, URI> movedResources = new HashMap<Resource, URI>();
	
	/**
	 * Resources that have been saved.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Collection<Resource> savedResources = new ArrayList<Resource>();

	/**
	 * Map to store the diagnostic associated with a resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Map<Resource, Diagnostic> resourceToDiagnosticMap = new LinkedHashMap<Resource, Diagnostic>();

	/**
	 * Controls whether the problem indication should be updated.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected boolean updateProblemIndication = true;

	protected EContentAdapter validatorProblemAdapter = new EContentAdapter(){
		public void notifyChanged(Notification notification) {
			switch(notification.getEventType()){
			case IFractalNotification.UPDATE_VALIDATION_DECORATION:
				List list = new ArrayList();
				list.add(notification.getNotifier());
				FractalDecorator.getDemoDecorator().refresh(list);
				break;
				
			case IFractalNotification.UPDATE_TREE_EDITOR:
				if(notification.getNewValue() == FractalEditor.this.resource){
				selectionViewer.setInput(((IFractalResource)getResource()).getRootDefinition().eContainer());
				selectionViewer.expandAll();
				selectionViewer.refresh();
				
				}
				break;
			}
		}
	};
	/**
	 * Adapter used to update the problem indication when resources are demanded loaded.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EContentAdapter problemIndicationAdapter = 
		new EContentAdapter() {
			public void notifyChanged(Notification notification) {
				
				if (notification.getNotifier() instanceof Resource) {
					switch (notification.getFeatureID(Resource.class)) {
						case Resource.RESOURCE__IS_LOADED:
						case Resource.RESOURCE__ERRORS:
						case Resource.RESOURCE__WARNINGS: {
							Resource resource = (Resource)notification.getNotifier();
							Diagnostic diagnostic = analyzeResourceProblems(resource, null);
							if (diagnostic.getSeverity() != Diagnostic.OK) {
								resourceToDiagnosticMap.put(resource, diagnostic);
							}
							else {
								resourceToDiagnosticMap.remove(resource);
							}

							if (updateProblemIndication) {
								getSite().getShell().getDisplay().asyncExec
									(new Runnable() {
										 public void run() {
											 updateProblemIndication();
										 }
									 });
							}
							break;
						}
					}
				}
				else {
					super.notifyChanged(notification);
				}
			}

			protected void setTarget(Resource target) {
				basicSetTarget(target);
			}

			protected void unsetTarget(Resource target) {
				basicUnsetTarget(target);
			}
		};

		/**
		 * Yann hand modifs.
		 * We track dirty state by the last operation executed when saved
		 * @generated NOT
		 */
		private IUndoableOperation savedOperation;
			
		/**
		 * 
		 * @param command
		 * @return
		 * @generated NOT
		 */
		private boolean isResourceModified(Command command){
			Iterator iterator = command.getAffectedObjects().iterator();
			boolean modified = false;
			while(iterator.hasNext() && !modified){
				Object object = iterator.next();
				if(object instanceof EObject && ((EObject)object).eResource().equals(getResource())){
					modified = true;
				}
			}
			return modified;
		}
		
		/**
		 * Yann hand modifs.
		 * Applies this editor's undo context to operations that affect
		 * its resource.  Also sets selection to viewer on execution of
		 * operations that wrap EMF Commands.
		 * @generated NOT
		 */
		private final IOperationHistoryListener historyListener = new IOperationHistoryListener() {
			
			public void historyNotification(OperationHistoryEvent event) {
				// We filter only emf command that affect our Fractal resource
				switch(event.getEventType()) {
					case OperationHistoryEvent.DONE:
						Set<Resource> affectedResources = ResourceUndoContext.getAffectedResources(
								event.getOperation());
						if(event.getOperation() instanceof TriggeredOperations){
							affectedResources = ResourceUndoContext.getAffectedResources(
									((TriggeredOperations)event.getOperation()).getTriggeringOperation());
						}
						Resource resource = getResource();
						if (affectedResources.contains(getResource())) {
							final IUndoableOperation operation = event.getOperation();
							// remove the default undo context so that we can have
							//     independent undo/redo of independent resource changes
							operation.removeContext(((IWorkspaceCommandStack)
									getEditingDomain().getCommandStack()).getDefaultUndoContext());
							// add our undo context to populate our undo menu
							operation.addContext(getUndoContext());
							
							
							getSite().getShell().getDisplay().asyncExec(new Runnable() {
								public void run() {
									firePropertyChange(IEditorPart.PROP_DIRTY);

									// Try to select the affected objects.
									//
									if (operation instanceof EMFCommandOperation) {
										Command command = ((EMFCommandOperation) operation).getCommand();

										if (command != null) {
											setSelectionToViewer(command
													.getAffectedObjects());
										}
									}

									if (propertySheetPage != null && propertySheetPage.getCurrentTab() != null) {
											propertySheetPage.refresh();
									}
								}
							});
						}
						break;
					case OperationHistoryEvent.UNDONE:
					case OperationHistoryEvent.REDONE:
						getSite().getShell().getDisplay().asyncExec(new Runnable() {
							public void run() {
								
								firePropertyChange(IEditorPart.PROP_DIRTY);
							}});
						break;
					}
				}
		};
		
		/**
		 * Yann hand modifs.
		 * Synchronizes workspace changes with the editing domain.
		 * Replaces the resourceChangeListener field generated by EMF
		 * @generated NOT
		 */
		protected WorkspaceSynchronizer workspaceSynchronizer;
		
		/**
		 * Yann hand modifs.
		 * Handles activation of the editor or it's associated views.
		 * This editor edits only a single resource and uses a
		 * WorkspaceSynchronizer to detect external changes.
		 * @generated NOT
		 */
		protected void handleActivate() {
			setCurrentViewer(selectionViewer);
			
			// Recompute the read only state.
			//
			if (editingDomain.getResourceToReadOnlyMap() != null) {
			  editingDomain.getResourceToReadOnlyMap().clear();

			  // Refresh any actions that may become enabled or disabled.
			  //
			  //setSelection(getSelection());
			  selectionProvider.setSelection(selectionProvider.getSelection());
			}

			try {
				final Resource res = getResource();
				//FractalTransactionalEditingDomain.getResourceSetListener().handleResourcesChanged();
				
				if (removedResources.contains(res)) {
					if (handleDirtyConflict()) {
						getSite().getPage().closeEditor(FractalEditor.this, false);
						FractalEditor.this.dispose();
					}
				} else if (movedResources.containsKey(res)) {
					//.CUSTOM: Generated editor does not have move support
					if (savedResources.contains(res)) {
						getOperationHistory().dispose(undoContext, true, true, true);
						
						// change saved resource's URI and remove from map
						//res.setURI(movedResources.remove(res));
							
						// must change my editor input
						IEditorInput newInput = new FileEditorInput(
								WorkspaceSynchronizer.getFile(res));
						setInputWithNotify(newInput);
						setPartName(newInput.getName());
					} else {
						handleMovedResource();
						IEditorInput newInput = new FileEditorInput(
								WorkspaceSynchronizer.getFile(res));
						setInputWithNotify(newInput);
						setPartName(newInput.getName());
					}
				} else if (changedResources.contains(res)) {
					changedResources.removeAll(savedResources);
					handleChangedResource();
				}
			} finally {
				removedResources.clear();
				changedResources.clear();
				movedResources.clear();
				savedResources.clear();
			}
		}
		
		/**
		 * Yann hand modifs.
		 * Replaces EMF-generated IResourceChangeListener implementation.
		 * @return
		 * @generated NOT
		 */
		private WorkspaceSynchronizer.Delegate createSynchronizationDelegate() {
			return new WorkspaceSynchronizer.Delegate() {
				
				public boolean handleResourceDeleted(Resource resource) {
					if ((resource == getResource()) && !isDirty()) {
						// just close now without prompt
						getSite().getShell().getDisplay().asyncExec
							(new Runnable() {
								 public void run() {
									 getSite().getPage().closeEditor(FractalEditor.this, false);
									 FractalEditor.this.dispose();
								 }
							 });
					} else {
						removedResources.add(resource);
					}
					
					return true;
				}
				
				public boolean handleResourceChanged(Resource resource) {
	                // is this a resource that we just saved?  If so, then this is
	                //   notification of that save, so forget it
					if(resource != getResource()){
						return true;
					}
					
	                if (savedResources.contains(resource)) {
	                    savedResources.remove(resource);
	                } else {
	                    changedResources.add(resource);
	                }
					
	                class ApplyChange implements Runnable {
						public void run(){
							 if(getActiveEditor() != FractalEditor.this){
								
								//TODO
							}
						}
					}
	                
	              //  getSite().getShell().getDisplay().syncExec(new ApplyChange());
	                
					return true;
				}
				
				public boolean handleResourceMoved(Resource resource, URI newURI) {
					if ((resource == getResource())) {
						// just close now without prompt
//						getSite().getShell().getDisplay().asyncExec
//							(new Runnable() {
//								 public void run() {
//									 getSite().getPage().closeEditor(FractalEditor.this, false);
//									 FractalEditor.this.dispose();
//								 }
//							 });
						movedResources.put(resource, newURI);
					} 
					
					return true;
				}
				
				public void dispose() {
					removedResources.clear();
					changedResources.clear();
					movedResources.clear();
				}};
		}
		
		/**
		 * Yann hand modifs.
		 * Handles what to do with changed resource on activation.
		 * Replaces EMF-generated handleChangedResources() method.
		 * @generated NOT
		 */
		protected void handleChangedResource() {
			Resource res = getResource();
			IFile file = WorkspaceSynchronizer.getFile(getResource());
			
			if (getResourceOperationListenerAdapter() != null && getResourceOperationListenerAdapter().isExternalLastChange() && changedResources.contains(res) && (!isDirty() || handleDirtyConflict())) {
				changedResources.remove(res);
				
				getOperationHistory().dispose(undoContext, true, true, true);
				firePropertyChange(IEditorPart.PROP_DIRTY);
				
				
				ResourceLoadedListener listener = ResourceLoadedListener.getDefault();
				if(listener != null) {
					listener.ignore(res);
				}
		
					selectionViewer.setInput(getResource().getContents().get(0));
					selectionViewer.expandAll();
					selectionViewer.refresh();
					if(listener != null){
						listener.watch(res);
					}

					
				updateProblemIndication = true;
				updateProblemIndication();
			}
		}
		
		/**
		 * Yann hand modifs.
		 * Handles what to do with moved resource on activation.
		 * EMF-generated editor does not handle moves
		 * @generated NOT
		 */
		protected void handleMovedResource() {
//			if (!isDirty() || handleDirtyConflict()) {
//				Resource res = getResource();
//				URI newURI = movedResources.get(res);
//				
//				if (newURI != null) {
//					if (res.isLoaded()) {
//						// unload
//						res.unload();
//					}
//
//					// load the new URI in another editor
//					res.getResourceSet().getResource(newURI, true);
//				}
//					
				updateProblemIndication = true;
				updateProblemIndication();
				
//			}
		}
  
	/**
	 * Updates the problems indication with the information described in the specified diagnostic.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void updateProblemIndication() {
		if (updateProblemIndication) {
			BasicDiagnostic diagnostic =
				new BasicDiagnostic
					(Diagnostic.OK,
					 "org.ow2.fractal.f4e.fractal.editor",
					 0,
					 null,
					 new Object [] { editingDomain.getResourceSet() });
			for (Diagnostic childDiagnostic : resourceToDiagnosticMap.values()) {
				if (childDiagnostic.getSeverity() != Diagnostic.OK) {
					diagnostic.add(childDiagnostic);
				}
			}

			int lastEditorPage = getPageCount() - 1;
			if (lastEditorPage >= 0 && getEditor(lastEditorPage) instanceof ProblemEditorPart) {
				((ProblemEditorPart)getEditor(lastEditorPage)).setDiagnostic(diagnostic);
				if (diagnostic.getSeverity() != Diagnostic.OK) {
					setActivePage(lastEditorPage);
				}
			}
			else if (diagnostic.getSeverity() != Diagnostic.OK) {
				ProblemEditorPart problemEditorPart = new ProblemEditorPart();
				problemEditorPart.setDiagnostic(diagnostic);
				problemEditorPart.setMarkerHelper(markerHelper);
				try {
					addPage(++lastEditorPage, problemEditorPart, getEditorInput());
					setPageText(lastEditorPage, problemEditorPart.getPartName());
					setActivePage(lastEditorPage);
					showTabs();
				}
				catch (PartInitException exception) {
					FractalEditorPlugin.INSTANCE.log(exception);
				}
			}

			if (markerHelper.hasMarkers(editingDomain.getResourceSet())) {
				markerHelper.deleteMarkers(editingDomain.getResourceSet());
				if (diagnostic.getSeverity() != Diagnostic.OK) {
					try {
						markerHelper.createMarkers(diagnostic);
					}
					catch (CoreException exception) {
						FractalEditorPlugin.INSTANCE.log(exception);
					}
				}
			}
		}
	}

	/**
	 * Shows a dialog that asks if conflicting changes should be discarded.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected boolean handleDirtyConflict() {
		return
			MessageDialog.openQuestion
				(getSite().getShell(),
				 getString("_UI_FileConflict_label"),
				 getString("_WARN_FileConflict"));
	}

	/**
	 * This creates a model editor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FractalEditor() {
		super();
		initializeEditingDomain();
		selectionProvider = new MultiPageSelectionProvider(this);
		selectionProvider.addSelectionChangedListener(new ISelectionChangedListener(){
			public void selectionChanged(SelectionChangedEvent event) {
		        setStatusLineManager(event.getSelection());
		      }
		    });  
	}
	
	static int number = 0;
	
	CommandStackListener commandStackListener = null;
	
	/**
	 * This sets up the editing domain for the model editor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void initializeEditingDomain() {
		// Create an adapter factory that yields item providers.
		//
		adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

		adapterFactory.addAdapterFactory(new ResourceItemProviderAdapterFactory());
		FractalPackage.eINSTANCE.getDefinition();
		// Start yann hand modifs.
		// We replace the default item provider factory by our customized factory.
		adapterFactory.addAdapterFactory(new ExtendedFractalItemProviderAdapterFactory());
		// End yann hand modifs.
		adapterFactory.addAdapterFactory(new ReflectiveItemProviderAdapterFactory());
		
		TransactionalEditingDomain domain = FractalTransactionalEditingDomain.getEditingDomain();
		editingDomain = (AdapterFactoryEditingDomain) domain;
	
		// Add a listener to set the most recent command's affected objects to be the selection of the viewer with focus.
	    // 
		commandStackListener =  new CommandStackListener() {
	          public void commandStackChanged(final EventObject event) {
	        	if(getContainer().isDisposed())
	        		return;
	        	
	        	  getContainer().getDisplay().asyncExec
		              (new Runnable() {
		                public void run() {
		                  firePropertyChange(IEditorPart.PROP_DIRTY);
		                 
		                // Try to select the affected objects.
		                //
		                Command mostRecentCommand = ((CommandStack) event.getSource()).getMostRecentCommand();
		                if (mostRecentCommand != null) {
		                  setSelectionToViewer(mostRecentCommand.getAffectedObjects());
		                }
		                if (propertySheetPage != null && !propertySheetPage.getControl().isDisposed() && propertySheetPage.getCurrentTab() != null) {
		                		propertySheetPage.refresh();
		                }
		              }
		            });
		          }
	          };
	    domain.getCommandStack().addCommandStackListener(commandStackListener);	
		getOperationHistory().addOperationHistoryListener(historyListener);
			
		undoContext = new ObjectUndoContext(
				this,
				FractalEditorPlugin.getPlugin().getString("_UI_FractalEditor_label")); //$NON-NLS-1$
		
		//FractalTransactionalEditingDomain.getResourceSetListener().addHandler(this);
	}

	/**
	 * This is here for the listener to be able to call it.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
			
	protected void firePropertyChange(int action) {
		super.firePropertyChange(action);
	}

	/**
	 * This sets the selection into whichever viewer is active.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectionToViewer(Collection<?> collection) {
		final Collection<?> theSelection = collection;
		// Make sure it's okay.
		//
		if (theSelection != null && !theSelection.isEmpty()) {
			// I don't know if this should be run this deferred
			// because we might have to give the editor a chance to process the viewer update events
			// and hence to update the views first.
			//
			//
			Runnable runnable =
				new Runnable() {
					public void run() {
						// Try to select the items in the current content viewer of the editor.
						//
						if (currentViewer != null) {
							currentViewer.setSelection(new StructuredSelection(theSelection.toArray()), true);
						}
					}
				};
			runnable.run();
		}
	}

	/**
	 * This returns the editing domain as required by the {@link IEditingDomainProvider} interface.
	 * This is important for implementing the static methods of {@link AdapterFactoryEditingDomain}
	 * and for supporting {@link org.eclipse.emf.edit.ui.action.CommandAction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EditingDomain getEditingDomain() {
		return editingDomain;
	}

	/**
	 * Yann hand modifs.
	 * EMF-generated class extends AdapterFactoryContentProvider
	 * @generated NOT
	 */
	public class ReverseAdapterFactoryContentProvider extends FractalTransactionalAdapterFactoryContentProvider {
		
		/**
		 * Yann hand modifs.
		 * Superclass constructor requires the transactional editing domain
		 * @generated NOT
		 */
		public ReverseAdapterFactoryContentProvider(AdapterFactory adapterFactory) {
			super((TransactionalEditingDomain) getEditingDomain(), adapterFactory);
		}

		/**
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		public Object [] getElements(Object object) {
			Object parent = super.getParent(object);
			return (parent == null ? Collections.EMPTY_SET : Collections.singleton(parent)).toArray();
		}

		/**
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		public Object [] getChildren(Object object) {
			Object parent = super.getParent(object);
			return (parent == null ? Collections.EMPTY_SET : Collections.singleton(parent)).toArray();
		}

		/**
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		public boolean hasChildren(Object object) {
			Object parent = super.getParent(object);
			return parent != null;
		}

		/**
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		public Object getParent(Object object) {
			return null;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentViewerPane(ViewerPane viewerPane) {
		if (currentViewerPane != viewerPane) {
			if (currentViewerPane != null) {
				currentViewerPane.showFocus(false);
			}
			currentViewerPane = viewerPane;
		}
		setCurrentViewer(currentViewerPane.getViewer());
	}

	/**
	 * This makes sure that one content viewer, either for the current page or the outline view, if it has focus,
	 * is the current one.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentViewer(Viewer viewer) {
		// If it is changing...
		//
		if (currentViewer != viewer) {
			if (selectionChangedListener == null) {
				// Create the listener on demand.
				//
				selectionChangedListener =
					new ISelectionChangedListener() {
						// This just notifies those things that are affected by the section.
						//
						public void selectionChanged(SelectionChangedEvent selectionChangedEvent) {
							setSelection(selectionChangedEvent.getSelection());
						}
					};
			}

			// Stop listening to the old one.
			//
			if (currentViewer != null) {
				currentViewer.removeSelectionChangedListener(selectionChangedListener);
			}

			// Start listening to the new one.
			//
			if (viewer != null) {
				viewer.addSelectionChangedListener(selectionChangedListener);
			}

			// Remember it.
			//
			currentViewer = viewer;

			// Set the editors selection based on the current viewer's selection.
			//
			setSelection(currentViewer == null ? StructuredSelection.EMPTY : currentViewer.getSelection());
		}
	}

	/**
	 * This returns the viewer as required by the {@link IViewerProvider} interface.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Viewer getViewer() {
		return currentViewer;
	}

	/**
	 * This creates a context menu for the viewer and adds a listener as well registering the menu for extension.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createContextMenuFor(StructuredViewer viewer) {
		MenuManager contextMenu = new MenuManager("#PopUp");
		contextMenu.add(new Separator("additions"));
		contextMenu.setRemoveAllWhenShown(true);
		contextMenu.addMenuListener(this);
		Menu menu= contextMenu.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(contextMenu, new UnwrappingSelectionProvider(viewer));

		int dndOperations = DND.DROP_COPY | DND.DROP_MOVE | DND.DROP_LINK;
		Transfer[] transfers = new Transfer[] { LocalTransfer.getInstance() };
		viewer.addDragSupport(dndOperations, transfers, new ViewerDragAdapter(viewer));
		viewer.addDropSupport(dndOperations, transfers, new EditingDomainViewerDropAdapter(editingDomain, viewer));
	}

	
	/**
	 * This is the method called to load a resource into the editing domain's resource set based on the editor's input.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createModel() {
		URI resourceURI = EditUIUtil.getURI(getEditorInput());
		
		
		// If we select a fractal file located inside a jar file
		if(getEditorInput() instanceof JarEntryEditorInput){
			JarEntryEditorInput jarEditor = (JarEntryEditorInput)getEditorInput();
			if(jarEditor.getStorage() instanceof JarEntryFile){
				IPackageFragmentRoot packageFragmentRoot = ((JarEntryFile)((JarEntryEditorInput)getEditorInput()).getStorage()).getPackageFragmentRoot();
				if(packageFragmentRoot.isExternal()){
					resourceURI = URI.createURI("jar:file://" + packageFragmentRoot.getPath().toOSString() + "!" + ((JarEntryFile)jarEditor.getStorage()).getFullPath());
				}else{
					resourceURI = URI.createURI("jar:file://" + Platform.getLocation() +  packageFragmentRoot.getPath().toOSString() + "!" + ((JarEntryFile)jarEditor.getStorage()).getFullPath());
				}
			}
		}
		
		// the resourceURI is the uri of the current resource edited by the editor
		// the DefinitionLocatorHelper helps to find fractal definition search location, 
		// and to list all available fractal definitions in the scope of the current edited fractal ADL file.
		// To do this the definitionLocatorHelper take as input getEditorInput that allows to determinate the
		// current java project.
		SDefinitionLoadLocator.getInstance().addHelper(resourceURI, new DefinitionLocatorHelper(getEditorInput()));
		
		Exception exception = null;
		resource = null;
		try {
			// Load the resource through the editing domain.
			//
			resource = getEditingDomain().getResourceSet().getResource(resourceURI, true);
			resource.eAdapters().add(validatorProblemAdapter);
			//getEditingDomain().getResourceSet().getResources().add(resource);
			OperationHistoryListenerAdapter helper = OperationHistoryListenerAdapterFactory.getInstance().adapt(resource);
			
			if(helper != null){
				helper.addUndoContext(getUndoContext());
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			exception = e;
			if(resource != null){
				resource.unload();
			}
			//resource = editingDomain.getResourceSet().getResource(resourceURI, false);
		}

		Diagnostic diagnostic = analyzeResourceProblems(resource, exception);
		if (diagnostic.getSeverity() != Diagnostic.OK) {
			resourceToDiagnosticMap.put(resource,  analyzeResourceProblems(resource, exception));
		}
		
		editingDomain.getResourceSet().eAdapters().add(problemIndicationAdapter);
	}

	/**
	 * Returns a diagnostic describing the errors and warnings listed in the resource
	 * and the specified exception (if any).
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Diagnostic analyzeResourceProblems(Resource resource, Exception exception) {
		if (resource != null && (!resource.getErrors().isEmpty() || !resource.getWarnings().isEmpty())) {
			BasicDiagnostic basicDiagnostic =
				new BasicDiagnostic
					(Diagnostic.ERROR,
					 "org.ow2.fractal.f4e.fractal.editor",
					 0,
					 getString("_UI_CreateModelError_message", resource.getURI()),
					 new Object [] { exception == null ? (Object)resource : exception });
			basicDiagnostic.merge(EcoreUtil.computeDiagnostic(resource, true));
			return basicDiagnostic;
		}
		else if (exception != null) {
			return
				new BasicDiagnostic
					(Diagnostic.ERROR,
					 "org.ow2.fractal.f4e.fractal.editor",
					 0,
					 getString("_UI_CreateModelError_message", EditUIUtil.getURI(getEditorInput())),
					 new Object[] { exception });
		}
		else {
			return Diagnostic.OK_INSTANCE;
		}
	}

	/**
	 * Yann hand modifs.
	 * Obtains the single resource that I edit.
	 * This editor edits only one resource.
	 * @generated NOT
	 */
	protected Resource getResource() {
		return resource;
	}
	
	/**
	 * Yann hand modifs.
	 * Obtains my undo context for populating the Undo and Redo menus
	 * from the operation history.
	 * Operation-history-integrated editors have undo contexts.
	 * @generated NOT
	 */
	public IUndoContext getUndoContext() {
		return undoContext;
	}
	
	
	/**
	 * This is the method used by the framework to install your own controls.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void createPages() {
		// Creates the model from the editor input
		//
		createModel();

		// Only creates the other pages if there is something that can be edited
		//
		if (getResource() != null &&
			    !getResource().getContents().isEmpty()) {
			// Create a page for the selection tree view.
			//
			{
				ViewerPane viewerPane =
					new ViewerPane(getSite().getPage(), FractalEditor.this) {
						
						public Viewer createViewer(Composite composite) {
							Tree tree = new Tree(composite, SWT.MULTI);
							TreeViewer newTreeViewer = new FractalTreeViewer(tree);
							
							tree.addKeyListener(new KeyListener(){

								public void keyPressed(KeyEvent e) {
								}

								public void keyReleased(KeyEvent e) {
									if(e.keyCode == SWT.F5){
										
										Definition def = ((IFractalResource)FractalEditor.this.resource).getRootDefinition();
										if(def != null){
										//	ModelUpdater updator = new ModelUpdater();
										//	updator.visit(def);
											
											UpdateMergeJob job = new UpdateMergeJob(def);
											job.setProperty(IProgressConstants.ICON_PROPERTY, FractalEditPlugin.INSTANCE.getImage("full/obj16/Definition"));
											job.schedule();
//											
										}
									}
								}
							});
							
							return newTreeViewer;
						}
						
						public void requestActivation() {
							super.requestActivation();
							setCurrentViewerPane(this);
						}
						
						
					};
				
				viewerPane.createControl(getContainer());

				selectionViewer = (TreeViewer)viewerPane.getViewer();
				
				// Start yann hand modifs.
				// Use a transactional content provider
				selectionViewer.setContentProvider(new FractalTransactionalAdapterFactoryContentProvider((TransactionalEditingDomain) getEditingDomain(), adapterFactory));
				
				
				// Use a transactional label provider
				selectionViewer.setLabelProvider(new DecoratingLabelProvider(new ExtendedTransactionalAdapterFactoryLabelProvider((TransactionalEditingDomain) getEditingDomain(), adapterFactory),FractalEditorPlugin.getPlugin().getWorkbench().getDecoratorManager().getLabelDecorator()));
				
				// We edit only a single resource
				if(getResource() instanceof IFractalResource){
					// To display definition as tree root element, we set the model root system element as tree root element.
					selectionViewer.setInput(((IFractalResource)getResource()).getRootDefinition().eContainer());
				}
				selectionViewer.setSelection(new StructuredSelection(getResource()), true);
				viewerPane.setTitle(getResource());
				//End yann hand modifs.
				
				new AdapterFactoryTreeEditor(selectionViewer.getTree(), adapterFactory);

				createContextMenuFor(selectionViewer);
				int pageIndex = addPage(viewerPane.getControl());
				setPageText(pageIndex, getString("_UI_SelectionPage_label"));
			}
		}


		getSite().getShell().getDisplay().asyncExec
			(new Runnable() {
				 public void run() {
					 setActivePage(0);
				 }
			 });
		
		// Ensures that this editor will only display the page's tab
		// area if there are more than one page
		//
		getContainer().addControlListener
			(new ControlAdapter() {
				boolean guard = false;
				
				public void controlResized(ControlEvent event) {
					if (!guard) {
						guard = true;
						hideTabs();
						guard = false;
					}
				}
			 });

		getSite().getShell().getDisplay().asyncExec
			(new Runnable() {
				 public void run() {
					 updateProblemIndication();
				 }
			 });
	}

	/**
	 * If there is just one page in the multi-page editor part,
	 * this hides the single tab at the bottom.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void hideTabs() {
		if (getPageCount() <= 1) {
			setPageText(0, "");
			if (getContainer() instanceof CTabFolder) {
				((CTabFolder)getContainer()).setTabHeight(1);
				Point point = getContainer().getSize();
				getContainer().setSize(point.x, point.y + 6);
			}
		}
	}

	/**
	 * If there is more than one page in the multi-page editor part,
	 * this shows the tabs at the bottom.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void showTabs() {
		if (getPageCount() > 1) {
			setPageText(0, getString("_UI_SelectionPage_label"));
			if (getContainer() instanceof CTabFolder) {
				((CTabFolder)getContainer()).setTabHeight(SWT.DEFAULT);
				Point point = getContainer().getSize();
				getContainer().setSize(point.x, point.y - 6);
			}
		}
	}

	/**
	 * This is used to track the active viewer.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	protected void pageChange(int pageIndex) {
		super.pageChange(pageIndex);

		if (contentOutlinePage != null) {
			handleContentOutlineSelection(contentOutlinePage.getSelection());
		}
	}

	/**
	 * This is how the framework determines which interfaces we implement.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	
	public Object getAdapter(Class key) {
		if (key.equals(IContentOutlinePage.class)) {
			return showOutlineView() ? getContentOutlinePage() : null;
		}
		else if (key.equals(IPropertySheetPage.class)) {
			return getPropertySheetPage();
		}
		else if (key.equals(IGotoMarker.class)) {
			return this;
		}
		else if (key.equals(IUndoContext.class)) {
			// Yann hand modifs.
			// used by undo/redo actions to get their undo context
			return undoContext;
		}
		else {
			return super.getAdapter(key);
		}
	}

	/**
	 * This accesses a cached version of the content outliner.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public IContentOutlinePage getContentOutlinePage() {
		if (contentOutlinePage == null) {
			// The content outline is just a tree.
			//

			class MyContentOutlinePage extends ContentOutlinePage {
				
				public void createControl(Composite parent) {
					super.createControl(parent);
					contentOutlineViewer = getTreeViewer();
					contentOutlineViewer.addSelectionChangedListener(this);

					// Set up the tree viewer.
					// Start yann hand modifs.
					// Use transactional content provider
					contentOutlineViewer.setContentProvider(
							new FractalTransactionalAdapterFactoryContentProvider(
									(TransactionalEditingDomain) getEditingDomain(), adapterFactory));

					// Use transactional label provider
					contentOutlineViewer.setLabelProvider(
							new ExtendedTransactionalAdapterFactoryLabelProvider(
									(TransactionalEditingDomain) getEditingDomain(), adapterFactory));

					// We edit only a single resource, not a resource set
					contentOutlineViewer.setInput(getResource());
					// End yann hand modifs.

					// Make sure our popups work.
					//
					createContextMenuFor(contentOutlineViewer);

					if (!editingDomain.getResourceSet().getResources().isEmpty()) {
						// Select the root object in the view.

						// Start yann hand modifs.
						// We edit only a single resource.
						ArrayList<Object> selection = new ArrayList<Object>();
						selection.add(getResource());
						contentOutlineViewer.setSelection(new StructuredSelection(selection), true);
						// End yann hand modifs.
					}
				}

				
				public void makeContributions(IMenuManager menuManager, IToolBarManager toolBarManager, IStatusLineManager statusLineManager) {
					super.makeContributions(menuManager, toolBarManager, statusLineManager);
					contentOutlineStatusLineManager = statusLineManager;
				}

				
				public void setActionBars(IActionBars actionBars) {
					super.setActionBars(actionBars);
					getActionBarContributor().shareGlobalActions(this, actionBars);
				}
			}

			contentOutlinePage = new MyContentOutlinePage();

			// Listen to selection so that we can handle it is a special way.
			//
			contentOutlinePage.addSelectionChangedListener
			(new ISelectionChangedListener() {
				// This ensures that we handle selections correctly.
				//
				public void selectionChanged(SelectionChangedEvent event) {
					handleContentOutlineSelection(event.getSelection());
				}
			});
		}

		return contentOutlinePage;
	}

	/**
	 * This accesses a cached version of the property sheet.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public IPropertySheetPage getPropertySheetPage() {
		if (propertySheetPage == null || (propertySheetPage != null && propertySheetPage.getControl().isDisposed())) {
			
			propertySheetPage = new PropertiesBrowserPage(FractalEditor.this){
				public void setActionBars(IActionBars actionBars) {
					super.setActionBars(actionBars);
					getActionBarContributor().shareGlobalActions(this, actionBars);
				}
			
				
				public void selectionChanged(IWorkbenchPart part,
						ISelection selection) {
					// TODO Auto-generated method stub
					super.selectionChanged(part, selection);
				}
				
				
			};

		}


		//			propertySheetPage =
		//				new ExtendedPropertySheetPage(editingDomain) {
		//					
		//					public void setSelectionToViewer(List<?> selection) {
		//						FractalEditor.this.setSelectionToViewer(selection);
		//						FractalEditor.this.setFocus();
		//					}
		//
		//					
		//					public void setActionBars(IActionBars actionBars) {
		//						super.setActionBars(actionBars);
		//						getActionBarContributor().shareGlobalActions(this, actionBars);
		//					}
		//				};
		//				// Start yann hand modifs.
		//				// Use a transactional property-source provider
		//				propertySheetPage.setPropertySourceProvider(new FractalTransactionalAdapterFactoryContentProvider((TransactionalEditingDomain) getEditingDomain(), adapterFactory));
		//				// End yann hand modifs.
		//		}

		return propertySheetPage;
	}

	/**
	 * This deals with how we want selection in the outliner to affect the other views.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void handleContentOutlineSelection(ISelection selection) {	
		if (currentViewerPane != null && !selection.isEmpty() && selection instanceof IStructuredSelection) {
			Iterator<?> selectedElements = ((IStructuredSelection)selection).iterator();
			if (selectedElements.hasNext()) {
				// Get the first selected element.
				//
				Object selectedElement = selectedElements.next();

				// If it's the selection viewer, then we want it to select the same selection as this selection.
				//
				if (currentViewerPane.getViewer() == selectionViewer) {
					ArrayList<Object> selectionList = new ArrayList<Object>();
					selectionList.add(selectedElement);
					while (selectedElements.hasNext()) {
						selectionList.add(selectedElements.next());
					}

					// Set the selection to the widget.
					//
					selectionViewer.setSelection(new StructuredSelection(selectionList));
					selectionProvider.setSelection(new StructuredSelection(selectedElements));
				}
				else {
					// Set the input to the widget.
					//
					//					if (currentViewerPane.getViewer().getInput() != selectedElement) {
					//						currentViewerPane.getViewer().setInput(selectedElement);
					//						currentViewerPane.setTitle(selectedElement);
					//					}
				}
			}
		}
	}

	/**
	 * This is for implementing {@link IEditorPart} and simply tests the command stack.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	
	public boolean isDirty() {
		// Start yann hand modifs.
		// The editor is dirty is the associated Fractal resource is dirty
		// 
		// Replaced code:
		OperationHistoryListenerAdapter helper = getResourceOperationListenerAdapter();
		return helper!=null?helper.isDirty():false;

		//return getResource().isModified();
		// End yann hand modifs.
	}

	/**
	 * This is for implementing {@link IEditorPart} and simply saves the model file.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	
	public void doSave(IProgressMonitor progressMonitor) {
		// Save only resources that have actually changed.
		//
		final Map<Object, Object> saveOptions = new HashMap<Object, Object>();
		saveOptions.put(Resource.OPTION_SAVE_ONLY_IF_CHANGED, Resource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER);

		// Do the work within an operation because this is a long running activity that modifies the workbench.
		//
		WorkspaceModifyOperation operation =
			new WorkspaceModifyOperation() {
			// This is the method that gets invoked when the operation runs.
			//
			
			public void execute(IProgressMonitor monitor) {
				try {

					Resource savedResource = getResource();

					savedResources.add(savedResource);
					IFractalSaveCommand saveCommand = 
						new FractalSaveCommand(savedResource,saveOptions, new IExceptionHandler(){public Object handleException(Exception e){ resourceToDiagnosticMap.put(resource, analyzeResourceProblems(resource, e)); return null;}});
					saveCommand.save();

					//						//.CUSTOM: Save in read-write transaction as we need to remove the first System element.
					//						((TransactionalEditingDomain) getEditingDomain()).getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain)getEditingDomain()) {
					//							public void doExecute() {
					//								try {
					//									// Save the resource to the file system.
					//									//
					//									Resource savedResource = getResource();
					//									savedResources.add(savedResource);
					//									savedResource.save(saveOptions);
					//								}
					//								catch (Exception exception) {
					//									resourceToDiagnosticMap.put(resource, analyzeResourceProblems(resource, exception));
					//								}
					//							}});
				}
				catch (Exception exception) {
					FractalEditorPlugin.INSTANCE.log(exception);
				}
			}
		};

		updateProblemIndication = false;
		try {
			// This runs the options, and shows progress.
			//
			new ProgressMonitorDialog(getSite().getShell()).run(true, false, operation);

			firePropertyChange(IEditorPart.PROP_DIRTY);

		}
		catch (Exception exception) {
			// Something went wrong that shouldn't.
			//
			FractalEditorPlugin.INSTANCE.log(exception);
		}
		updateProblemIndication = true;
		updateProblemIndication();
	}

	/**
	 * This returns whether something has been persisted to the URI of the specified resource.
	 * The implementation uses the URI converter from the editor's resource set to try to open an input stream. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected boolean isPersisted(Resource resource) {
		boolean result = false;
		try {
			InputStream stream = editingDomain.getResourceSet().getURIConverter().createInputStream(resource.getURI());
			if (stream != null) {
				result = true;
				stream.close();
			}
		}
		catch (IOException e) {
			// Ignore
		}
		return result;
	}

	/**
	 * This always returns true because it is not currently supported.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public boolean isSaveAsAllowed() {
		return true;
	}

	/**
	 * This also changes the editor's input.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public void doSaveAs() {
		SaveAsDialog saveAsDialog = new SaveAsDialog(getSite().getShell());
		saveAsDialog.open();
		IPath path = saveAsDialog.getResult();
		if (path != null) {
			IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
			if (file != null) {
				doSaveAs(URI.createPlatformResourceURI(file.getFullPath().toString(), true), new FileEditorInput(file));
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void doSaveAs(final URI uri, final IEditorInput editorInput) {
		// changing the URI is, conceptually, a write operation.  However, it does
		//    not affect the abstract state of the model, so we only need exclusive
		//    (read) access
		try {
			//.CUSTOM: Save in a read-only transaction
			((TransactionalEditingDomain) getEditingDomain()).runExclusive(new Runnable() {
				public void run() {
					getResource().setURI(uri);
					setInputWithNotify(editorInput);
					setPartName(editorInput.getName());
				}});
		} catch (InterruptedException e) {
			// just log it
			FractalEditorPlugin.INSTANCE.log(e);
			
			// don't follow through with the save because we were interrupted while
			//    trying to start the transaction, so our URI is not actually changed
			return;
		}
		
		IProgressMonitor progressMonitor =
			getActionBars().getStatusLineManager() != null ?
				getActionBars().getStatusLineManager().getProgressMonitor() :
				new NullProgressMonitor();
		doSave(progressMonitor);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void gotoMarker(IMarker marker) {
		try {
			if (marker.getType().equals(EValidator.MARKER)) {
				final String uriAttribute = marker.getAttribute(EValidator.URI_ATTRIBUTE, null);
				if (uriAttribute != null) {
					//.CUSTOM: Use a read-only transaction to read the resource
					//         when navigating to an object
					try {
						((TransactionalEditingDomain) getEditingDomain()).runExclusive(new Runnable() {
							public void run() {
								URI uri = URI.createURI(uriAttribute);
								EObject eObject = editingDomain.getResourceSet().getEObject(uri, true);
								if (eObject != null) {
								  setSelectionToViewer(Collections.singleton(editingDomain.getWrapper(eObject)));
								}
							}});
					} catch (InterruptedException e) {
						// just log it
						FractalEditorPlugin.INSTANCE.log(e);
					}
				}
			}
		}
		catch (CoreException exception) {
			FractalEditorPlugin.INSTANCE.log(exception);
		}
	}

	/**
	 * This is called during startup.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	
	public void init(IEditorSite site, IEditorInput editorInput) {
		setSite(site);
		setInputWithNotify(editorInput);
		setPartName(editorInput.getName());
		site.setSelectionProvider(this);
		site.getPage().addPartListener(partListener);
	
		//.CUSTOM: Create a workspace synchronizer instead of a
		//         resource change listener
		workspaceSynchronizer = new WorkspaceSynchronizer(
				(TransactionalEditingDomain) editingDomain,
				createSynchronizationDelegate());
	}

	protected int getPageCount() {
		CTabFolder folder = (CTabFolder)getContainer();
		// May not have been created yet, or may have been disposed.
		if (folder != null && !folder.isDisposed()) {
			return folder.getItemCount();
		}
		return 0;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	
	public void setFocus() {
		
		if (currentViewerPane != null) {
			currentViewerPane.setFocus();
		}
		else {
			getControl(getActivePage()).setFocus();
		}
		//.CUSTOM: We only have the one viewer
		//selectionViewer.getControl().setFocus();
	}

	/**
	 * This implements {@link org.eclipse.jface.viewers.ISelectionProvider}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addSelectionChangedListener(ISelectionChangedListener listener) {
		selectionChangedListeners.add(listener);
	}

	/**
	 * This implements {@link org.eclipse.jface.viewers.ISelectionProvider}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeSelectionChangedListener(ISelectionChangedListener listener) {
		selectionChangedListeners.remove(listener);
	}

	/**
	 * This implements {@link org.eclipse.jface.viewers.ISelectionProvider} to return this editor's overall selection.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISelection getSelection() {
		return editorSelection;
	}

	/**
	 * This implements {@link org.eclipse.jface.viewers.ISelectionProvider} to set this editor's overall selection.
	 * Calling this result will notify the listeners.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelection(ISelection selection) {
		editorSelection = selection;

		for (ISelectionChangedListener listener : selectionChangedListeners) {
			listener.selectionChanged(new SelectionChangedEvent(this, selection));
		}
		setStatusLineManager(selection);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatusLineManager(ISelection selection) {
		IStatusLineManager statusLineManager = currentViewer != null && currentViewer == contentOutlineViewer ?
			contentOutlineStatusLineManager : getActionBars().getStatusLineManager();

		if (statusLineManager != null) {
			if (selection instanceof IStructuredSelection) {
				Collection<?> collection = ((IStructuredSelection)selection).toList();
				switch (collection.size()) {
					case 0: {
						statusLineManager.setMessage(getString("_UI_NoObjectSelected"));
						break;
					}
					case 1: {
						String text = new AdapterFactoryItemDelegator(adapterFactory).getText(collection.iterator().next());
						statusLineManager.setMessage(getString("_UI_SingleObjectSelected", text));
						break;
					}
					default: {
						statusLineManager.setMessage(getString("_UI_MultiObjectSelected", Integer.toString(collection.size())));
						break;
					}
				}
			}
			else {
				statusLineManager.setMessage("");
			}
		}
	}

	/**
	 * This looks up a string in the plugin's plugin.properties file.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static String getString(String key) {
		return FractalEditorPlugin.INSTANCE.getString(key);
	}

	/**
	 * This looks up a string in plugin.properties, making a substitution.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static String getString(String key, Object s1) {
		return FractalEditorPlugin.INSTANCE.getString(key, new Object [] { s1 });
	}

	/**
	 * This implements {@link org.eclipse.jface.action.IMenuListener} to help fill the context menus with contributions from the Edit menu.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void menuAboutToShow(IMenuManager menuManager) {
		((IMenuListener)getEditorSite().getActionBarContributor()).menuAboutToShow(menuManager);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EditingDomainActionBarContributor getActionBarContributor() {
		return (EditingDomainActionBarContributor)getEditorSite().getActionBarContributor();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IActionBars getActionBars() {
		return getActionBarContributor().getActionBars();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdapterFactory getAdapterFactory() {
		return adapterFactory;
	}

	/**
	 * Yann hand modifs.
	 * We have a command stack that delegates
	 * to the operation history
	 * @return
	 * @generated NOT
	 */     
	private IOperationHistory getOperationHistory() {
		return ((IWorkspaceCommandStack) editingDomain.getCommandStack()).getOperationHistory();
	}
	
	/**
	 * 
	 * @return
	 * @generated NOT
	 */
	private OperationHistoryListenerAdapter getResourceOperationListenerAdapter(){
		OperationHistoryListenerAdapter helper = null;
		if(getResource() != null){
			 helper = OperationHistoryListenerAdapterFactory.getInstance().adapt(getResource());
		}
		return helper;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	
	public void dispose() {
		updateProblemIndication = false;

		//.CUSTOM: We use a workspace synchronizer instead of a
		//         resource change listener
		workspaceSynchronizer.dispose();
		
		//.CUSTOM: We have operation history stuff to clean up
		getOperationHistory().removeOperationHistoryListener(historyListener);
		getResourceOperationListenerAdapter().removeUndoContext(getUndoContext());
		getOperationHistory().dispose(getUndoContext(), true, true, true);
		
		//.CUSTOM: We have only one resource to edit, but it is in
		//         a shared resource set (not our own private set).
		//         So, we must unload it explicitly.  Also remove our problem
		//         indication adapter
		//getResource().unload();
		//editingDomain.getResourceSet().getResources().remove(getResource());
		if(this.resource != null){
			this.resource.eAdapters().remove(validatorProblemAdapter);
		}
		editingDomain.getResourceSet().eAdapters().remove(problemIndicationAdapter);
		editingDomain.getCommandStack().removeCommandStackListener(commandStackListener);
		//DefinitionManager.getInstance().remove(getResource().getURI());
	
		getSite().getPage().removePartListener(partListener);

		adapterFactory.dispose();

		if (getActionBarContributor().getActiveEditor() == this) {
			getActionBarContributor().setActiveEditor(null);
		}

		if (propertySheetPage != null) {
			propertySheetPage.dispose();
		}

		if (contentOutlinePage != null) {
			contentOutlinePage.dispose();
		}

		super.dispose();
	}

	/**
	 * Returns whether the outline view should be presented to the user.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected boolean showOutlineView() {
		return true;
	}

	public String getContributorId() {
		return "org.ow2.fractal.f4e.emf.diagram";
	}
	
	
}