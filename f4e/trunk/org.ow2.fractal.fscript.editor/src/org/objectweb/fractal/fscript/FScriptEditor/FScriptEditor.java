package org.objectweb.fractal.fscript.FScriptEditor;

import org.eclipse.ui.editors.text.TextEditor;

/**
 * @author Mahmoud Ben Hassine
 */
public class FScriptEditor extends TextEditor {

	public FScriptEditor() {
		setSourceViewerConfiguration(new FScriptEditorConfiguration());
	}
}