package org.objectweb.fractal.fscript.FScriptEditor;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IWordDetector;
import org.eclipse.jface.text.rules.NumberRule;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;


/**
 * @author Mahmoud Ben Hassine
 */
public class FScriptEditorScanner extends RuleBasedScanner {
	
	private static final Color COLOR_KEYWORD;
	private static final Color COLOR_AXE;
	private static final Color COLOR_NUMBER;
	private static final Color COLOR_COMMENT;
	
	
	private static final String [] KEYWORDS = {
		"action", "function", "add","remove", "start",
	     "stop", "bind", "unbind", "single","set",
	     "server", "client", "mandatory", "optional", 
	     "external", "internal", "started", "stopped", "bound", "rename",
	     "set","for","if","not","return","union","difference","size",
	     "name","collection","signature", "compatible","state","value",
	     "matches", "starts","with", "ends","intersection","adl","new"
	};
	
	private static final String [] AXES = {
		"interface", "binding", "component","parent", "child",
	     "descendant", "or","self", "ancestor", "sibling","attribute",
	     };
	
	static {
		Display d = PlatformUI.getWorkbench().getDisplay();
		COLOR_KEYWORD = new Color(d, 0, 0, 128);
		COLOR_AXE = new Color(d, 255, 0, 0);
		COLOR_NUMBER = new Color(d, 0, 128, 128);
		COLOR_COMMENT = new Color(d, 0, 128, 0);
		
	}
	

	public FScriptEditorScanner() {
		//Tokens
		Token keyword = new Token(new TextAttribute(COLOR_KEYWORD, null, SWT.BOLD));
		Token axe = new Token(new TextAttribute(COLOR_AXE, null, SWT.BOLD));
		Token number = new Token(new TextAttribute(COLOR_NUMBER, null, SWT.BOLD));
		Token comment = new Token(new TextAttribute(COLOR_COMMENT, null, SWT.ITALIC));
		
		
		//Rules
		WordRule keywordrule = new WordRule(new IWordDetector() {
			@Override
			public boolean isWordPart(char c) {
				return Character.isJavaIdentifierPart(c);
			}

			@Override
			public boolean isWordStart(char c) {
				return Character.isJavaIdentifierStart(c);
			}			
		});
		
		for (String k : KEYWORDS) {
			keywordrule.addWord(k, keyword);
		}
		
		WordRule axerule = new WordRule(new IWordDetector() {
			@Override
			public boolean isWordPart(char c) {
				return Character.isJavaIdentifierPart(c);
			}

			@Override
			public boolean isWordStart(char c) {
				return Character.isJavaIdentifierStart(c);
			}			
		});
		
		for (String k : AXES) {
			axerule.addWord(k, axe);
		}
		
		IRule[] rules = new IRule[4];
		
		//comment
		rules[0] = new SingleLineRule("--", null, comment, (char) 0, true);
		
		//number
		rules[1] = new NumberRule(number);
		
		//axe
		rules[2] = axerule;
		
		//keyword
		rules[3] = keywordrule;

		setRules(rules);
	
	}
}