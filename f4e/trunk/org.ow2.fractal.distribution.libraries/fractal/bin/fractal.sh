#!/bin/sh

#A Little script to launch java class or adl file without ant or maven directly by calling java.
#run.sh -h for help
 

#-----------------------------------------------------------
# get the real path of the script
DIR=$(pwd)

case $0 in
    /*)
        SCRIPT="$0"
        ;;
    *)
        PWD=`pwd`
        SCRIPT="$PWD/$0"
        ;;
esac

# Change spaces to ":" so the tokens can be parsed.
SCRIPT=`echo $SCRIPT | sed -e 's; ;:;g'`
# Get the real path to this script, resolving any symbolic links
TOKENS=`echo $SCRIPT | sed -e 's;/; ;g'`
REALPATH=
for C in $TOKENS; do
    REALPATH="$REALPATH/$C"
    while [ -h "$REALPATH" ] ; do
        LS="`ls -ld "$REALPATH"`"
        LINK="`expr "$LS" : '.*-> \(.*\)$'`"
        if expr "$LINK" : '/.*' > /dev/null; then
            REALPATH="$LINK"
        else
            REALPATH="`dirname "$REALPATH"`""/$LINK"
        fi
    done
done
# Change ":" chars back to spaces.
REALPATH=`echo $REALPATH | sed -e 's;:; ;g'`

REALDIR=`dirname "$REALPATH"`

#-----------------------------------------------------------
# Add libraries to the CP variable

LIBDIR=$REALDIR/../lib

if [ ! -d $LIBDIR ]
then
	LIBDIR=$FRACTAL_HOME/lib
fi

if [ ! -d $LIBDIR ]
then
echo "Can not found Fractal libraries. 
Please install the Fractal Libraries, and set the FRACTAL_HOME variable."
fi

CP=
for i in $LIBDIR/*.jar
do
  CP="${CP}:${i}"
done
#-----------------------------------------------------------



#-----------------------------------------------------------
# check that the getopt version has the long option support
if `getopt -T >/dev/null 2>&1` ; [ $? = 4 ] ; then
  echo "Fractal Script Launcher."
else
  echo "Sorry you have a old getopt(1)"
  exit 1
fi


#-----------------------------------------------------------
# funtion to display the help
function help {
echo "fractal.sh 
    -cp  <class search path of directories and zip/jar files>
          A : separated list of directories, JAR archives,
          and ZIP archives to search for class files.
    -jvmargs \"[-D<name>=<value>]*\" arguments passed to the JVM
    -fscript a option to start the Fscript console
    -groovy a option to start the groovy console
    -adl <adlfile.fractal> option to start a Fractal ADL file
    -itf <interface name> option to specify which fractal interface must be launch
    -class <class name> option to start a java class
    -args extra arguments
    
    examples :
		To use the script you sould create a jar with your Java classes and Fractal files add 
		it to the classpath with the -cp option.
		By default, all jars contained in the lib folder of the FRACTAL_HOME are added to the classpath.
	
		fractal.sh  -cp dist/helloworld-julia-adl.jar -fscript
		Fscript is started in console mode with a custom jar added to the classpath.
 	
		fractal.sh  -cp dist/helloworld-julia-adl.jar -groovy
		Groovy is started in console mode with a custom jar added to the classpath.
 	
		fractal.sh  -cp dist/helloworld-julia-adl.jar -adl helloworld.ClientServerImpl
		Fractal ADL Launcher is called with the helloworld.ClientServerImpl Fractal definition 
		as argument. If the Fractal definition has a Fractal interface named 'r' with a 
		java.lang.Runnable as signature, the interface will be launched.

		fractal.sh  -cp dist/helloworld-julia-adl.jar -adl helloworld.ClientServerImpl -fe
		Launch Fractal Explorer on the helloworld.ClientServerImpl Fractal definition. 
		The name associated to the Fractal definition, needed by Fractal Explorer is set to "RootComposite"		
 
		fractal.sh  -cp dist/helloworld-julia-adl.jar -class helloworld.HelloWorld
		Launch the helloworld.HelloWorld java class.
    "
}        

# initialization of options variables
opt_jvmargs=''
opt_fexplorer="False"
opt_fscript="False"
opt_groovy="False"
opt_cp=''
opt_adl=''
opt_itf=''
opt_class=''
opt_args=''

#arguments parsing
TEMP=`getopt -o : -a --long help,jvmargs:,groovy,fscript,fe,cp:,adl:,itf:,class:,args: \
     -n 'run.sh' -- "$@"`

if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi

# Note the quotes around `$TEMP': they are essential!
eval set -- "$TEMP"

while true ; do	
	case "$1" in
		--help)  	help; shift;;
		--jvmargs)    	opt_jvmargs="$2"; shift 2;;
		--groovy)    	opt_groovy="True"; shift;;
		--fscript)    	opt_fscript="True"; shift;;
		--fe)    	opt_fexplorer="True"; shift;;
		--cp)    	opt_cp=$2; shift 2;;
		--adl)   	opt_adl="$2"; shift 2 ;;
		--itf)   	opt_itf="$2"; shift 2;;
		--class) 	opt_class="$2"; shift 2;;
		--args)  	opt_args="$2"; shift 2;;
		--) shift ; break ;;
		*) echo "Internal error!" ; exit 1 ;;
	esac
done


#check that options are compatible
if ( ( test "$opt_adl" != "" ) || ( test "$opt_itf" != "" ) ) &&  ( test "$opt_class" != "" ) 
then
 echo "The -class option can't be set with either -adl or -itf option";
 exit;
fi

#check that -itf is not alone
if ( test "$opt_class" != "" ) &&  ( test "$opt_itf" != "" )
then
 echo "The -itf option, can't be specified with the -class option."
fi

#check that there is a least -adl, -class or -fscript options activated
if ( test "$opt_adl" == "" ) && ( test "$opt_class" == "" ) && ( test "$opt_fscript" == "" )
then
 echo "Extra options are required."
fi

start_java="False"
start_fexplorer="False";
start_fscript="False";
start_groovy="False";
start_adl="False";


if ( test "$opt_class" != "" ) 
then
   start_java="True"
elif ( test "$opt_groovy" == "True" )
then
   start_groovy="True"
elif ( test "$opt_fscript" == "True" )
then
   start_fscript="True"
elif ( test "$opt_adl" != "" ) && ( test "$opt_fexplorer" == "False" ) && ( test "$opt_fscript" == "False" )
then
   start_adl="True"
elif ( test "$opt_adl" != "" ) && ( test "$opt_fexplorer" == "True" ) && ( test "$opt_fscript" == "False" )
then
   start_fexplorer="True"
fi


if ( test "$start_java" == "True" )
then
 java $opt_jvmargs -Dfractal.provider="org.objectweb.fractal.julia.Julia" -classpath "$CP:$opt_cp" "$opt_class" "$opt_args"

elif ( test "$start_adl" == "True" )
then
 echo "Start ADL Launcher..."
	#ADL Launcher
 java $opt_jvmargs -Dfractal.provider="org.objectweb.fractal.julia.Julia" -classpath "$CP:$opt_cp" 	org.objectweb.fractal.adl.Launcher -fractal "$opt_adl"

elif ( test "$start_fexplorer" == "True" )
then
 java $opt_jvmargs -Dfractal.provider="org.objectweb.fractal.julia.Julia" -classpath "$CP:$opt_cp" org.objectweb.fractal.adl.Launcher -name="RootComposite" -definition="$opt_adl" -fractal org.objectweb.fractal.explorer.BasicFractalExplorer

elif ( test "$start_fscript" == "True" )
then
 java $opt_jvmargs -Dfractal.provider="org.objectweb.fractal.julia.Julia" -classpath "$CP:$opt_cp" org.objectweb.fractal.fscript.console.Main

elif ( test "$start_groovy" == "True" )
then
 java $opt_jvmargs -Dfractal.provider="org.objectweb.fractal.julia.Julia" -classpath "$CP:$opt_cp" groovy.ui.Console
fi


