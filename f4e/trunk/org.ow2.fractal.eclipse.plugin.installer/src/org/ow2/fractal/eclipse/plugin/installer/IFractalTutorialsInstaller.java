package org.ow2.fractal.eclipse.plugin.installer;

import org.eclipse.core.runtime.IPath;

public interface IFractalTutorialsInstaller {
	public void installTutorial(IPath tutorialArchivePath, IPath outputPath);
}
