package org.ow2.fractal.eclipse.plugin.installer;

import java.io.File;
import java.net.URL;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.variables.IValueVariable;
import org.eclipse.core.variables.IValueVariableInitializer;

public class FractalHomeVariableInitializer implements IValueVariableInitializer {
	public void initialize(IValueVariable variable) {
		String fractalHome = "";
		try{
			URL url = Platform.getBundle(FractalInstaller.FRACTAL_LIBRARIES_PLUGIN_ID).getEntry(
					File.separator + FractalInstaller.FRACTAL_FOLDER + File.separator);
			fractalHome = Platform.resolve(url).toURI().getPath();
			if(fractalHome == null || fractalHome.equals("")){
				FractalInstaller.getDefault().getLog().log(new Status(IStatus.WARNING,FractalInstaller.PLUGIN_ID,"The FRACTAL_HOME variable has been initialized with a empty value"));
			}else{
				variable.setValue(fractalHome);
			}
		}catch(Exception e){
			FractalInstaller.getDefault().getLog().log(new Status(IStatus.ERROR,FractalInstaller.PLUGIN_ID,"Error durring the FRACTAL_HOME variable initialisation",e));
		}
	}

}
