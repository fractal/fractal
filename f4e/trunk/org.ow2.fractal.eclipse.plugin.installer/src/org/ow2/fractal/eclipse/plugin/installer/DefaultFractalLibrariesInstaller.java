package org.ow2.fractal.eclipse.plugin.installer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Enumeration;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

public class DefaultFractalLibrariesInstaller implements IFractalLibrariesInstaller{
	public static final String FRACTAL_LIBRARIES_FILTER="*.jar";
	
	public DefaultFractalLibrariesInstaller(){
		
	}
	
	
	public void installFractalLibraries(){
		
	}
	
	
	private String getFractalLibrariesPath(){
		return null;
	}
	
	private void copy(InputStream in, OutputStream out){
		try{
			int available = in.available();
			
			byte[] bytes = new byte[available];
			in.read(bytes);
			
			out.write(bytes);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Copy the Fractal libraries from the lib directory of the fractal libraries plugin to
	 * the location/lib directory.
	 *
	 * @param location is the location where the lib/fractal directory will be created. 
	 * 		The Fractal libraries will be copied in it.
	 */
	public void installFractalLibraries(IPath location){
		Bundle bundle = Platform.getBundle(FractalInstaller.FRACTAL_LIBRARIES_PLUGIN_ID);
		if(bundle != null){
			Enumeration<URL> librariesURLs = bundle.findEntries(FractalInstaller.FRACTAL_LIBRARIES_FOLDER, FRACTAL_LIBRARIES_FILTER, false);
			
			File libFolder = new File(location.toOSString() + File.separator + FractalInstaller.LIBRARIES_FOLDER);
			if(!libFolder.exists()){
				try{
					libFolder.mkdirs();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			while(librariesURLs.hasMoreElements()){
				URL url = librariesURLs.nextElement();
				
				try{
					File file = new File(location.toOSString() + File.separator + new File(url.getFile()).getName());
					FileOutputStream output = new FileOutputStream(file);
					copy(url.openStream(),output);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}
	
}
