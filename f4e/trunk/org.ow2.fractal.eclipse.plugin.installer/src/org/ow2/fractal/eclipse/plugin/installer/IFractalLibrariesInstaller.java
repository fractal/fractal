package org.ow2.fractal.eclipse.plugin.installer;

import java.io.File;

import org.eclipse.core.runtime.IPath;

public interface IFractalLibrariesInstaller {
	public void installFractalLibraries(IPath location);
}
