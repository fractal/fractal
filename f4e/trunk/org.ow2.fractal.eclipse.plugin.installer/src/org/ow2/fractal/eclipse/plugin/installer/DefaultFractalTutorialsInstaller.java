package org.ow2.fractal.eclipse.plugin.installer;


import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

import de.schlichtherle.io.ArchiveDetector;
import de.schlichtherle.io.DefaultArchiveDetector;
import de.schlichtherle.io.File;

public class DefaultFractalTutorialsInstaller implements IFractalTutorialsInstaller{
	public static final String FRACTAL_TUTORIALS_PLUGIN_ID = "org.ow2.fractal.distribution.tutorials";
	
	
	public DefaultFractalTutorialsInstaller(){
		
	}
	
	public void installTutorial(IPath tutorialArchivePath, IPath outputPath){
		Bundle bundle = Platform.getBundle(FRACTAL_TUTORIALS_PLUGIN_ID);
		if(bundle != null){
			URL tutorialArchiveURL = bundle.getEntry(tutorialArchivePath.toString());
			try{
				ArchiveDetector detector = new DefaultArchiveDetector("tar.bz2|tar.gz|zip");
				
				de.schlichtherle.io.File tutorialArchiveFile = new de.schlichtherle.io.File(FileLocator.resolve(tutorialArchiveURL).getFile(),detector);
			if(tutorialArchiveFile.exists()){
				try{
					//We unpack the first root directory which is in the archive
					tutorialArchiveFile.listFiles(detector)[0].archiveCopyAllTo(new java.io.File(outputPath.toOSString()));
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
}
