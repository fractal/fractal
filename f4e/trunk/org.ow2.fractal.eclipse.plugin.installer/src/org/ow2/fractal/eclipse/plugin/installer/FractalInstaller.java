package org.ow2.fractal.eclipse.plugin.installer;

import java.io.File;
import java.io.FilenameFilter;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.variables.IValueVariable;
import org.eclipse.core.variables.VariablesPlugin;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
/**
 * @author yann
 *
 */
public class FractalInstaller extends AbstractUIPlugin implements org.eclipse.ui.IStartup {
	public static final String LIBRARIES_FOLDER="lib";
	public static final String FRACTAL_FOLDER="fractal";
	public static final String FRACTAL_LIBRARIES_FOLDER=FRACTAL_FOLDER + File.separator + "lib";
	
	// The plug-in ID
	public static final String PLUGIN_ID = "org.ow2.fractal.eclipse.plugin.installer";

	// The plug-in ID of the fractal libraries plug-in
	public static final String FRACTAL_LIBRARIES_PLUGIN_ID="org.ow2.fractal.distribution.libraries";
	
	// The shared instance
	private static FractalInstaller plugin;

	/**
	 * The constructor
	 */
	public FractalInstaller() {
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
		
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static FractalInstaller getDefault() {
		return plugin;
	}

	public void earlyStartup() {
		IValueVariable fractalHome = VariablesPlugin.getDefault().getStringVariableManager().getValueVariable("FRACTAL_HOME");
		if(fractalHome != null){
		FractalHomeVariableInitializer fhi = new FractalHomeVariableInitializer();
		fhi.initialize(fractalHome);
		}else{
			this.getLog().log(new Status(IStatus.ERROR,PLUGIN_ID,"The FRACTAL_HOME variable initialization fails"));
		}
	}
	
	/**
	 * @param a prefix of the library name 
	 * @return the first jar library filename that match the given partial name
	 */
	public String getFractalLibraryJarPath(final String name){
		String resultJarFilename=null;
		IValueVariable variable = VariablesPlugin.getDefault().getStringVariableManager().getValueVariable("FRACTAL_HOME");
		if(variable != null){
			String fractalHome = variable.getValue();
			File folder = new File(fractalHome + File.separator + LIBRARIES_FOLDER);
			if(folder.isDirectory()){
				File[] files = folder.listFiles(new FilenameFilter(){
					public boolean accept(File dir, String filename) {
						return filename.startsWith(name) && filename.endsWith(".jar");
					}
				});
				if(files.length>0){
					resultJarFilename = files[0].getAbsolutePath();
				}else{
					FractalInstaller.getDefault().getLog().log(new Status(IStatus.WARNING,FractalInstaller.PLUGIN_ID,"The " + name + " library can't be found in the " + folder.getAbsolutePath() + " folder"));
				}
			}
		}
		if(resultJarFilename == null){
			FractalInstaller.getDefault().getLog().log(new Status(IStatus.WARNING,FractalInstaller.PLUGIN_ID,"The library " + name + " can' be found because the FRACTAL_HOME variable is null"));
		}
		return resultJarFilename;
	}
	
	/**
	 * @return the absolute path of the first fractal-spoonlet jar found in the FRACTAL_HOME folder
	 */
	public String getFractalSpoonletJarPath(){
		return getFractalLibraryJarPath("fractal-spoonlet");
	}
	
	/**
	 * @return the absolute path of the first fractal-spoonlet jar found in the FRACTAL_HOME folder
	 */
	public String getFractalADLSpoonletJarPath(){
		return getFractalLibraryJarPath("fractaladl-spoonlet");
	}
	
	public String getFractalHome(){
		IValueVariable variable = VariablesPlugin.getDefault().getStringVariableManager().getValueVariable("FRACTAL_HOME");
		if(variable != null){
			return variable.getValue();
		}else{
			return null;
		}
	}
}
