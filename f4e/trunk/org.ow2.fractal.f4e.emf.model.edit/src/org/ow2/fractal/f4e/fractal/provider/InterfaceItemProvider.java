/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal.provider;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.ui.EclipseUIPlugin;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.ow2.fractal.f4e.fractal.FractalFactory;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.Role;
import org.ow2.fractal.f4e.fractal.notification.IFractalNotification;

/**
 * This is the item provider adapter for a {@link org.ow2.fractal.f4e.fractal.Interface} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class InterfaceItemProvider
	extends ExtendableItemProviderAdapter
	implements
		IEditingDomainItemProvider, IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
			addSignaturePropertyDescriptor(object);
			addRolePropertyDescriptor(object);
			addCardinalityPropertyDescriptor(object);
			addContingencyPropertyDescriptor(object);
//			addMergedNamePropertyDescriptor(object);
//			addMergedSignaturePropertyDescriptor(object);
//			addMergedRolePropertyDescriptor(object);
//			addMergedCardinalityPropertyDescriptor(object);
//			addMergedContingencyPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Interface_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Interface_name_feature", "_UI_Interface_type"),
				 FractalPackage.Literals.INTERFACE__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	 protected ItemPropertyDescriptor createItemPropertyDescriptor(
			    AdapterFactory adapterFactory,
			    ResourceLocator resourceLocator,
			    String displayName,
			    String description,
			    EStructuralFeature feature,
			    boolean isSettable,
			    boolean multiLine,
			    boolean sortChoices,
			    Object staticImage,
			    String category,
			    String[] filterFlags)
			  {
			    return createMergedItemPropertyDescriptor(
			      adapterFactory,
			      resourceLocator,
			      displayName,
			      description,
			      feature,
			      isSettable,
			      multiLine,
			      sortChoices,
			      staticImage,
			      category,
			      filterFlags);
			  }
	 
	/**
	 * This adds a property descriptor for the Signature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSignaturePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Interface_signature_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Interface_signature_feature", "_UI_Interface_type"),
				 FractalPackage.Literals.INTERFACE__SIGNATURE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Role feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRolePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Interface_role_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Interface_role_feature", "_UI_Interface_type"),
				 FractalPackage.Literals.INTERFACE__ROLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Cardinality feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCardinalityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Interface_cardinality_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Interface_cardinality_feature", "_UI_Interface_type"),
				 FractalPackage.Literals.INTERFACE__CARDINALITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Contingency feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContingencyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Interface_contingency_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Interface_contingency_feature", "_UI_Interface_type"),
				 FractalPackage.Literals.INTERFACE__CONTINGENCY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Merged Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addMergedNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Interface_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Interface_mergedName_feature", "_UI_Interface_type"),
				 FractalPackage.Literals.INTERFACE__MERGED_NAME,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Merged Signature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addMergedSignaturePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Interface_signature_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Interface_mergedSignature_feature", "_UI_Interface_type"),
				 FractalPackage.Literals.INTERFACE__MERGED_SIGNATURE,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Merged Role feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addMergedRolePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Interface_role_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Interface_mergedRole_feature", "_UI_Interface_type"),
				 FractalPackage.Literals.INTERFACE__MERGED_ROLE,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Merged Cardinality feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addMergedCardinalityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Interface_cardinality_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Interface_mergedCardinality_feature", "_UI_Interface_type"),
				 FractalPackage.Literals.INTERFACE__MERGED_CARDINALITY,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Merged Contingency feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addMergedContingencyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Interface_contingency_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Interface_mergedContingency_feature", "_UI_Interface_type"),
				 FractalPackage.Literals.INTERFACE__MERGED_CONTINGENCY,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(FractalPackage.Literals.INTERFACE__COMMENTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Interface.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object) {
		Interface interface_ = (Interface)object;
		String image = "full/obj16/Interface";
		if(interface_.eContainmentFeature()==FractalPackage.eINSTANCE.getAbstractComponent_MergedInterfaces()){
			if(interface_.getRole() == Role.CLIENT){
				image = "full/obj16/Interface-in-non-editable";
			}else{
				image = "full/obj16/Interface-out-non-editable";
			}
		}else{
			if(interface_.getRole() == Role.CLIENT){
				image = "full/obj16/Interface-in";
			}else{
				image = "full/obj16/Interface-out";
			}
		}
		return overlayImage(object, getResourceLocator().getImage(image));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		if(object instanceof InterfaceDescriptor){
			return ((InterfaceDescriptor)object).getName();
		}else{
			String label = ((Interface)object).getName();
			String mergedInterfaceType = ((Interface)object).getMergedSignature();
			String interfaceType = ((Interface)object).getSignature();
			
			if(interfaceType == null && mergedInterfaceType != null){
				interfaceType = mergedInterfaceType;
			}else if(interfaceType == null && mergedInterfaceType == null){
				interfaceType="null";
			}
			
			return label == null || label.length() == 0 ?
					getString("_UI_Interface_type") :
						getString("_UI_Interface_type") + " " + label + " : " + interfaceType;
		}
	}

	
	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		
		switch (notification.getEventType())
        {
			// When the merged role is changed we have to update the interface icon.
          case IFractalNotification.SET_MERGED_INTERFACE_ROLE:
        	  fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
        	  return;
          case IFractalNotification.UPDATE_VALIDATION_DECORATION:
        	  List list = new ArrayList();
        	  return;
        	  
        }
		
		switch (notification.getFeatureID(Interface.class)) {
			case FractalPackage.INTERFACE__NAME:
			case FractalPackage.INTERFACE__SIGNATURE:
			case FractalPackage.INTERFACE__ROLE:
			case FractalPackage.INTERFACE__CARDINALITY:
			case FractalPackage.INTERFACE__CONTINGENCY:
			case FractalPackage.INTERFACE__NAME_AST:
			case FractalPackage.INTERFACE__SIGNATURE_AST:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case FractalPackage.INTERFACE__COMMENTS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(FractalPackage.Literals.INTERFACE__COMMENTS,
				 FractalFactory.eINSTANCE.createComment()));
		
		newChildDescriptors.addAll(createAnyChilds(object));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return FractalEditPlugin.INSTANCE;
	}
}
