/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.fractal.f4e.fractal.provider;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.eclipse.emf.ecore.xml.type.AnyType;
import org.eclipse.emf.ecore.xml.type.XMLTypeFactory;
import org.eclipse.emf.edit.EMFEditPlugin;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.ow2.fractal.f4e.extension.FractalADLExtensionHelper;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.FractalFactory;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.adapter.helper.HelperAdapter;
import org.ow2.fractal.f4e.fractal.util.FractalResourceFactoryImpl;

/**
 * This is the item provider adapter for a {@link org.ow2.fractal.f4e.fractal.AbstractComponent} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AbstractComponentItemProvider
	extends ExtendableItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractComponentItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
//			addMergedNamePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractComponent_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractComponent_name_feature", "_UI_AbstractComponent_type"),
				 FractalPackage.Literals.ABSTRACT_COMPONENT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Merged Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addMergedNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractComponent_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractComponent_mergedName_feature", "_UI_AbstractComponent_type"),
				 FractalPackage.Literals.ABSTRACT_COMPONENT__MERGED_NAME,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__EXTENDS_AST);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__COMMENTS);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__INTERFACES);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__MERGED_INTERFACES);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__SUB_COMPONENTS);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__CONTENT);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__MERGED_CONTENT);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__BINDINGS);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__MERGED_BINDINGS);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__CONTROLLER);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__MERGED_CONTROLLER);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__TEMPLATE_CONTROLLER);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__MERGED_TEMPLATE_CONTROLLER);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__LOGGER);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__MERGED_LOGGER);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__VIRTUAL_NODE);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__MERGED_VIRTUAL_NODE);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__COORDINATES);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__ANY);
			childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__ANY_ATTRIBUTES);
		}
		
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.
		if(child instanceof EObject && ((EObject)child).eContainingFeature() != null){
			return ((EObject)child).eContainingFeature();
		}
		return super.getChildFeature(object, child);
	}

	/**
	 * This returns AbstractComponent.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/AbstractComponent"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((AbstractComponent)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_AbstractComponent_type") :
			getString("_UI_AbstractComponent_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AbstractComponent.class)) {
			case FractalPackage.ABSTRACT_COMPONENT__NAME:
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_NAME:
			case FractalPackage.ABSTRACT_COMPONENT__NAME_AST:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case FractalPackage.ABSTRACT_COMPONENT__EXTENDS_AST:
			case FractalPackage.ABSTRACT_COMPONENT__COMMENTS:
			case FractalPackage.ABSTRACT_COMPONENT__INTERFACES:
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_INTERFACES:
			case FractalPackage.ABSTRACT_COMPONENT__SUB_COMPONENTS:
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_SUB_COMPONENTS:
			case FractalPackage.ABSTRACT_COMPONENT__CONTENT:
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTENT:
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER:
			case FractalPackage.ABSTRACT_COMPONENT__BINDINGS:
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_BINDINGS:
			case FractalPackage.ABSTRACT_COMPONENT__CONTROLLER:
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_CONTROLLER:
			case FractalPackage.ABSTRACT_COMPONENT__TEMPLATE_CONTROLLER:
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_TEMPLATE_CONTROLLER:
			case FractalPackage.ABSTRACT_COMPONENT__LOGGER:
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_LOGGER:
			case FractalPackage.ABSTRACT_COMPONENT__VIRTUAL_NODE:
			case FractalPackage.ABSTRACT_COMPONENT__MERGED_VIRTUAL_NODE:
			case FractalPackage.ABSTRACT_COMPONENT__COORDINATES:
			case FractalPackage.ABSTRACT_COMPONENT__ANY:
			case FractalPackage.ABSTRACT_COMPONENT__ANY_ATTRIBUTES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
			case FractalPackage.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER:
				switch(notification.getEventType()){
					case Notification.SET:
						if(notification.getNewValue() != null){
							
							//childrenFeatures.remove(FractalPackage.Literals.ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER);
						}else{
							//childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER);
						}
						break;
					case Notification.UNSET:
						childrenFeatures.add(FractalPackage.Literals.ABSTRACT_COMPONENT__MERGED_ATTRIBUTES_CONTROLLER);
						break;
				}
				fireNotifyChanged(new ViewerNotification(notification, ((EObject)notification.getNotifier()).eContainer(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(FractalPackage.Literals.ABSTRACT_COMPONENT__COMMENTS,
				 FractalFactory.eINSTANCE.createComment()));

		newChildDescriptors.add
			(createChildParameter
				(FractalPackage.Literals.ABSTRACT_COMPONENT__INTERFACES,
				 FractalFactory.eINSTANCE.createInterface()));

		newChildDescriptors.add
			(createChildParameter
				(FractalPackage.Literals.ABSTRACT_COMPONENT__SUB_COMPONENTS,
				 FractalFactory.eINSTANCE.createComponent()));

		newChildDescriptors.add
			(createChildParameter
				(FractalPackage.Literals.ABSTRACT_COMPONENT__CONTENT,
				 FractalFactory.eINSTANCE.createContent()));

		newChildDescriptors.add
			(createChildParameter
				(FractalPackage.Literals.ABSTRACT_COMPONENT__ATTRIBUTES_CONTROLLER,
				 FractalFactory.eINSTANCE.createAttributesController()));

		newChildDescriptors.add
			(createChildParameter
				(FractalPackage.Literals.ABSTRACT_COMPONENT__BINDINGS,
				 FractalFactory.eINSTANCE.createBinding()));

		newChildDescriptors.add
			(createChildParameter
				(FractalPackage.Literals.ABSTRACT_COMPONENT__CONTROLLER,
				 FractalFactory.eINSTANCE.createController()));

		newChildDescriptors.add
			(createChildParameter
				(FractalPackage.Literals.ABSTRACT_COMPONENT__TEMPLATE_CONTROLLER,
				 FractalFactory.eINSTANCE.createTemplateController()));

		newChildDescriptors.add
			(createChildParameter
				(FractalPackage.Literals.ABSTRACT_COMPONENT__LOGGER,
				 FractalFactory.eINSTANCE.createLogger()));

		newChildDescriptors.add
			(createChildParameter
				(FractalPackage.Literals.ABSTRACT_COMPONENT__VIRTUAL_NODE,
				 FractalFactory.eINSTANCE.createVirtualNode()));

		newChildDescriptors.add
			(createChildParameter
				(FractalPackage.Literals.ABSTRACT_COMPONENT__COORDINATES,
				 FractalFactory.eINSTANCE.createCoordinates()));
		
		//newChildDescriptors.addAll(createAnyChilds(object));
		
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return FractalEditPlugin.INSTANCE;
	}
	
	
	// Start yann hand modifs
	protected Object getFeatureValue(EObject object, EStructuralFeature feature){
		if(feature != null){
			if(HelperAdapter.isMergedFeature(object, feature.getFeatureID())){
					return new ArrayList();		
			}
		}
		return super.getFeatureValue(object, feature);
	}

	public Collection<?> getChildren(Object object) {
		// TODO Auto-generated method stub
		AbstractComponent abstractComponent = (AbstractComponent)object; 
		Collection<?> result =  super.getChildren(abstractComponent);
		EStructuralFeature[] uniqueFeatures = abstractComponent.getHelper().getUniqueFeatures();
		
		for(int i = 0; i< uniqueFeatures.length; i++){
			EStructuralFeature feature = uniqueFeatures[i];
			EStructuralFeature mergedFeature = HelperAdapter.getMergedFeature((EObject)object, feature);
			Object child = ((EObject)object).eGet(feature);
			Object mergedChild =  ((EObject)object).eGet(mergedFeature);
			ChildrenStore store = getChildrenStore(object);
			if(child != null){
				child = wrap((EObject)object, feature, child, CommandParameter.NO_INDEX);
				store.setValue(feature, child);
				store.setValue(mergedFeature, null);
			}else{
				mergedChild = wrap((EObject)object, mergedFeature, mergedChild, CommandParameter.NO_INDEX);
				store.setValue(mergedFeature, mergedChild);
			}
		}
		return result;
	}	
	// End yann hand modifs
}
