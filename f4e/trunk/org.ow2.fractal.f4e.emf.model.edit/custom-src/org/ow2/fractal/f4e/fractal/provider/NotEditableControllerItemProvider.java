package org.ow2.fractal.f4e.fractal.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.ow2.fractal.f4e.fractal.Controller;
import org.ow2.fractal.f4e.fractal.FractalPackage;

public class NotEditableControllerItemProvider extends ControllerItemProvider  implements INotEditableItemContentProvider, IItemColorProvider {
	
	public NotEditableControllerItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}
	
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			 itemPropertyDescriptors = new ArrayList<IItemPropertyDescriptor>();
			 addMergedTypePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}
	
	public String getText(Object object) {
		String label = ((Controller)object).getMergedType();
		return label == null || label.length() == 0 ?
			getString("_UI_Controller_type") :
			getString("_UI_Controller_type") + " " + label;
	}
	
	public Object getBackground(Object object) {
		// TODO Auto-generated method stub
		return GRAY;
	}
	
	protected Object getFeatureValue(EObject object, EStructuralFeature feature){
		return new ArrayList();		
	}

	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		
	}
}
