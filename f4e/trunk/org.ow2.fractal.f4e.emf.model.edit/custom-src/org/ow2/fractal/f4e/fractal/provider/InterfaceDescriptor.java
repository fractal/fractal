package org.ow2.fractal.f4e.fractal.provider;

import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.Interface;

/**
 * A class to provide the name of a interface associated to a binding.
 * The interface name will be prefixed by its component/definition container name.
 * 
 * @author Yann Davin
 */
public class InterfaceDescriptor {
	Interface interface_ = null;
	Binding binding = null;
	public InterfaceDescriptor(Object binding, Object interface_){
		if(interface_ instanceof Interface && binding instanceof Binding){
			this.interface_ = (Interface)interface_;
			this.binding = (Binding)binding;
		}
	}
	
	public String getName(){
		String interfaceName = null;
		AbstractComponent bindingContainer = null;
		AbstractComponent interfaceContainer = null;
		if(binding.eContainer() != null && binding.eContainer() instanceof AbstractComponent){
			 bindingContainer = (AbstractComponent)binding.eContainer();
		}
		if(interface_ != null && interface_.eContainer() != null && interface_.eContainer() instanceof AbstractComponent){
			 interfaceContainer = (AbstractComponent)interface_.eContainer();
		}
		if(interfaceContainer != null && bindingContainer != null){
			if(interfaceContainer == bindingContainer){
				interfaceName = "this" + "." + interface_.getName();
			}else{
				// We don't check if the interface is in a correct sub components,
				// It should be checked before.
				interfaceName = interfaceContainer.getName() + "." + interface_.getName();
			}
		}
		
		return interfaceName;
	}
}
