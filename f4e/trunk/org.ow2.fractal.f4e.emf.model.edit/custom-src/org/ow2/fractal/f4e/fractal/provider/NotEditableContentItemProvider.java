package org.ow2.fractal.f4e.fractal.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.ow2.fractal.f4e.fractal.Content;

public class NotEditableContentItemProvider extends ContentItemProvider  implements INotEditableItemContentProvider, IItemColorProvider {
	
	public NotEditableContentItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}
	
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) 
	  {
	    if (itemPropertyDescriptors == null)
	    {
	      itemPropertyDescriptors = new ArrayList<IItemPropertyDescriptor>();
	      addMergedClassPropertyDescriptor(object);
	    }
	    return itemPropertyDescriptors;
	  }
	
	public String getText(Object object) {
		String label = ((Content)object).getMergedClass();
		return label == null || label.length() == 0 ?
			getString("_UI_Content_type") :
			getString("_UI_Content_type") + " " + label;
	}
	
	public Object getBackground(Object object) {
		// TODO Auto-generated method stub
		return GRAY;
	}
	
	protected Object getFeatureValue(EObject object, EStructuralFeature feature){
		return new ArrayList();		
	}
	
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		
	}
}
