package org.ow2.fractal.f4e.fractal.provider;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.Attribute;
import org.ow2.fractal.f4e.fractal.AttributesController;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.Comment;
import org.ow2.fractal.f4e.fractal.Component;
import org.ow2.fractal.f4e.fractal.Constant;
import org.ow2.fractal.f4e.fractal.Content;
import org.ow2.fractal.f4e.fractal.Controller;
import org.ow2.fractal.f4e.fractal.Coordinates;
import org.ow2.fractal.f4e.fractal.Definition;
import org.ow2.fractal.f4e.fractal.DefinitionCall;
import org.ow2.fractal.f4e.fractal.EffectiveParameter;
import org.ow2.fractal.f4e.fractal.FormalParameter;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;
import org.ow2.fractal.f4e.fractal.Logger;
import org.ow2.fractal.f4e.fractal.Parameter;
import org.ow2.fractal.f4e.fractal.Reference;
import org.ow2.fractal.f4e.fractal.TemplateController;
import org.ow2.fractal.f4e.fractal.Value;
import org.ow2.fractal.f4e.fractal.ValueElement;
import org.ow2.fractal.f4e.fractal.VirtualNode;
import org.ow2.fractal.f4e.fractal.util.FractalSwitch;

/**
 * Extends the NotEditableVirtualNodeItemProvider.
 * Factory that provide custom not editable item provider if model element is a merged element 
 * else it provides the standard generated item provider.
 * 
 * @author Yann Davin
 *
 */
public class ExtendedFractalItemProviderAdapterFactory extends FractalItemProviderAdapterFactory {
	
	public ExtendedFractalItemProviderAdapterFactory(){
		super();
		
		supportedTypes.add(IItemColorProvider.class);
		supportedTypes.add(INotEditableItemContentProvider.class);
		supportedTypes.add(ExtendedFractalItemProviderAdapterFactory.class);
	}
	
	protected NotEditableInterfaceItemProvider nonEditableInterfaceItemProvider = null;
	protected NotEditableComponentItemProvider nonEditableComponentItemProvider = null;
	protected NotEditableAttributesControllerItemProvider nonEditableAttributesControllerItemProvider = null;
	protected NotEditableBindingItemProvider nonEditableBindingItemProvider = null;
	protected NotEditableAttributeItemProvider nonEditableAttributeItemProvider = null;
	protected NotEditableContentItemProvider nonEditableContentItemProvider = null;
	protected NotEditableControllerItemProvider nonEditableControllerItemProvider = null;
	
	
	public Adapter createNonEditableInterfaceAdapter() {
		if (nonEditableInterfaceItemProvider == null) {
			nonEditableInterfaceItemProvider = new NotEditableInterfaceItemProvider(this);
		}

		return nonEditableInterfaceItemProvider;
	}

	public Adapter createNonEditableComponentAdapter() {
		if (nonEditableComponentItemProvider == null) {
			nonEditableComponentItemProvider = new NotEditableComponentItemProvider(this);
		}
		
		return nonEditableComponentItemProvider;
	}
	

	public Adapter createNonEditableBindingAdapter() {
		if (nonEditableBindingItemProvider == null) {
			nonEditableBindingItemProvider = new NotEditableBindingItemProvider(this);
		}
		
		return nonEditableBindingItemProvider;
	}
	
	public Adapter createNonEditableAttributesControllerAdapter() {
		if (nonEditableAttributesControllerItemProvider == null) {
			nonEditableAttributesControllerItemProvider = new NotEditableAttributesControllerItemProvider(this);
		}
		
		return nonEditableAttributesControllerItemProvider;
	}
	
	public Adapter createNonEditableAttributeAdapter() {
		if (nonEditableAttributeItemProvider == null) {
			nonEditableAttributeItemProvider = new NotEditableAttributeItemProvider(this);
		}
		
		return nonEditableAttributeItemProvider;
	}
	
	public Adapter createNonEditableContentAdapter() {
		if (nonEditableContentItemProvider == null) {
			nonEditableContentItemProvider = new NotEditableContentItemProvider(this);
		}
		
		return nonEditableContentItemProvider;
	}
	
	public Adapter createNonEditableControllerAdapter() {
		if (nonEditableControllerItemProvider == null) {
			nonEditableControllerItemProvider = new NotEditableControllerItemProvider(this);
		}
		
		return nonEditableControllerItemProvider;
	}
	
	protected NotEditableLoggerItemProvider nonEditableLoggerItemProvider = null;
	public Adapter createNonEditableLoggerAdapter() {
		if (nonEditableLoggerItemProvider == null) {
			nonEditableLoggerItemProvider = new NotEditableLoggerItemProvider(this);
		}
		
		return nonEditableLoggerItemProvider;
	}
	
	protected NotEditableVirtualNodeItemProvider nonEditableVirualNodeItemProvider = null;
	public Adapter createNonEditableVirtualNodeAdapter() {
		if (nonEditableVirualNodeItemProvider == null) {
			nonEditableVirualNodeItemProvider = new NotEditableVirtualNodeItemProvider(this);
		}
		
		return nonEditableVirualNodeItemProvider;
	}
	
	protected FractalSwitch<Adapter> modelSwitch =
		new FractalSwitch<Adapter>() {
			@Override
			public Adapter caseSystem(org.ow2.fractal.f4e.fractal.System object) {
				return createSystemAdapter();
			}
			@Override
			public Adapter caseAbstractComponent(AbstractComponent object) {
				return createAbstractComponentAdapter();
			}
			@Override
			public Adapter caseParameter(Parameter object) {
				return createParameterAdapter();
			}
			@Override
			public Adapter caseValue(Value object) {
				return createValueAdapter();
			}
			@Override
			public Adapter caseValueElement(ValueElement object) {
				return createValueElementAdapter();
			}
			@Override
			public Adapter caseConstant(Constant object) {
				return createConstantAdapter();
			}
			@Override
			public Adapter caseReference(Reference object) {
				return createReferenceAdapter();
			}
			@Override
			public Adapter caseFormalParameter(FormalParameter object) {
				return createFormalParameterAdapter();
			}
			@Override
			public Adapter caseDefinition(Definition object) {
				return createDefinitionAdapter();
			}
			@Override
			public Adapter caseDefinitionCall(DefinitionCall object) {
				return createDefinitionCallAdapter();
			}
			@Override
			public Adapter caseEffectiveParameter(EffectiveParameter object) {
				return createEffectiveParameterAdapter();
			}
			@Override
			public Adapter caseInterface(Interface object) {
				if(souldUseNotEditableItemProvider(object) == null){
					return createInterfaceAdapter();
				}else if(souldUseNotEditableItemProvider(object) == true){
					return createNonEditableInterfaceAdapter();
				}else{
					return createInterfaceAdapter();
				}
			}
			@Override
			public Adapter caseComponent(Component object) {
				if(souldUseNotEditableItemProvider(object) == null){
					return createComponentAdapter();
				}else if(souldUseNotEditableItemProvider(object) == true){
					return createNonEditableComponentAdapter();
				}else{
					return createComponentAdapter();
				}
			}
			@Override
			public Adapter caseContent(Content object) {
				if(souldUseNotEditableItemProvider(object) == null){
					return createContentAdapter();
				}else if(souldUseNotEditableItemProvider(object) == true){
					return createNonEditableContentAdapter();
				}else{
					return createContentAdapter();
				}
			}
			@Override
			public Adapter caseAttributesController(AttributesController object) {
				if(souldUseNotEditableItemProvider(object) == null){
					return createAttributesControllerAdapter();
				}else if(souldUseNotEditableItemProvider(object) == true){
					return createNonEditableAttributesControllerAdapter();
				}else{
					return createAttributesControllerAdapter();
				}
			}
			@Override
			public Adapter caseAttribute(Attribute object) {
				if(souldUseNotEditableItemProvider(object) == null){
					return createAttributeAdapter();
				}else if(souldUseNotEditableItemProvider(object) == true){
					return createNonEditableAttributeAdapter();
				}else{
					return createAttributeAdapter();
				}
			}
			@Override
			public Adapter caseBinding(Binding object) {
				if(souldUseNotEditableItemProvider(object) == null){
					return createBindingAdapter();
				}else if(souldUseNotEditableItemProvider(object) == true){
					return createNonEditableBindingAdapter();
				}else{
					return createBindingAdapter();
				}
			}
			@Override
			public Adapter caseController(Controller object) {
				if(souldUseNotEditableItemProvider(object) == null){
					return createControllerAdapter();
				}else if(souldUseNotEditableItemProvider(object) == true){
					return createNonEditableControllerAdapter();
				}else{
					return createControllerAdapter();
				}
			}
			@Override
			public Adapter caseTemplateController(TemplateController object) {
				return createTemplateControllerAdapter();
			}
			@Override
			public Adapter caseComment(Comment object) {
				return createCommentAdapter();
			}
			@Override
			public Adapter caseCoordinates(Coordinates object) {
				return createCoordinatesAdapter();
			}
			@Override
			public Adapter caseLogger(Logger object) {
				if(souldUseNotEditableItemProvider(object) == null){
					return createLoggerAdapter();
				}else if(souldUseNotEditableItemProvider(object) == true){
					return createNonEditableLoggerAdapter();
				}else{
					return createLoggerAdapter();
				}
			}
			@Override
			public Adapter caseVirtualNode(VirtualNode object) {
				if(souldUseNotEditableItemProvider(object) == null){
					return createVirtualNodeAdapter();
				}else if(souldUseNotEditableItemProvider(object) == true){
					return createNonEditableVirtualNodeAdapter();
				}else{
					return createVirtualNodeAdapter();
				}
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};
	 
	protected Boolean souldUseNotEditableItemProvider(EObject object){
		EStructuralFeature features = object.eContainingFeature();
		features = object.eContainingFeature();
		
		if((object instanceof Interface && features == FractalPackage.eINSTANCE.getAbstractComponent_MergedInterfaces()) ||
			(object instanceof Component && features == FractalPackage.eINSTANCE.getAbstractComponent_MergedSubComponents()) ||
			(object instanceof Binding && features == FractalPackage.eINSTANCE.getAbstractComponent_MergedBindings()) ||
			(object instanceof AttributesController && features == FractalPackage.eINSTANCE.getAbstractComponent_MergedAttributesController()) ||
			(object instanceof Attribute && features == FractalPackage.eINSTANCE.getAttributesController_MergedAttributes()) ||
			(object instanceof Controller && features == FractalPackage.eINSTANCE.getAbstractComponent_MergedController()) ||
			(object instanceof Content && features == FractalPackage.eINSTANCE.getAbstractComponent_MergedContent())||
			(object instanceof Logger && features == FractalPackage.eINSTANCE.getAbstractComponent_MergedLogger()) ||
			(object instanceof TemplateController && features == FractalPackage.eINSTANCE.getAbstractComponent_MergedTemplateController())||
			(object instanceof VirtualNode && features == FractalPackage.eINSTANCE.getAbstractComponent_MergedVirtualNode())	
		){
			return true;
		}else if(features == null){
			return null;
		}else{
			return false;
		}
	}
	
	public Adapter createAdapter(Notifier target) {
		return this.modelSwitch.doSwitch((EObject)target);
	}
}
