package org.ow2.fractal.f4e.fractal.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.Interface;


public class NotEditableInterfaceItemProvider  extends InterfaceItemProvider implements INotEditableItemContentProvider, IItemColorProvider {
	
	public NotEditableInterfaceItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	public String getText(Object object) {
		if(object instanceof InterfaceDescriptor){
			return ((InterfaceDescriptor)object).getName();
		}else{
			String mergedInterfaceType = ((Interface)object).getMergedSignature();
		if(mergedInterfaceType == null){
			mergedInterfaceType="null";
		}
			String label = ((Interface)object).getMergedName();
			return label == null || label.length() == 0 ?
					getString("_UI_Interface_type") :
						getString("_UI_Interface_type") + " " + label + " : " + mergedInterfaceType;
		}
	}
	
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			 itemPropertyDescriptors = new ArrayList<IItemPropertyDescriptor>();
			 
			addMergedNamePropertyDescriptor(object);
			addMergedSignaturePropertyDescriptor(object);
			addMergedRolePropertyDescriptor(object);
			addMergedCardinalityPropertyDescriptor(object);
			addMergedContingencyPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}
	
	public Object getBackground(Object object) {
		// TODO Auto-generated method stub
		return GRAY;
	}

	protected Object getFeatureValue(EObject object, EStructuralFeature feature){
		return new ArrayList();		
	}
	
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		
	}
}