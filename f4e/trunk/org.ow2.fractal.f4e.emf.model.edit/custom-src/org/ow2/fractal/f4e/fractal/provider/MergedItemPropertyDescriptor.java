package org.ow2.fractal.f4e.fractal.provider;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.ow2.fractal.f4e.fractal.adapter.helper.HelperAdapter;

/**
 * Modified ItemPropertyDescriptor in order to display a attribute with specials requirements.
 * If the attribute is not set, the related merged attribute must be displayed.
 * 
 * @author Yann Davin
 *
 */
public class MergedItemPropertyDescriptor extends ItemPropertyDescriptor{
	
	public MergedItemPropertyDescriptor
     (AdapterFactory adapterFactory,
      ResourceLocator resourceLocator,
      String displayName,
      String description,
      EStructuralFeature feature, 
      boolean isSettable,
      boolean multiLine,
      boolean sortChoices,
      Object staticImage,
      String category,
      String [] filterFlags)
  {
	super(	adapterFactory,
			resourceLocator,
			displayName,
			description,
			feature, 
			isSettable, 
			multiLine, 
			sortChoices,
			staticImage,
			category,
			filterFlags );
  }
	
	protected Object getValue(EObject object, EStructuralFeature feature) {
		try
	    {
			if(object.eIsSet(feature) == false && HelperAdapter.hasMergedFeature(object, feature.getFeatureID())){
				return object.eGet(object.eClass().getEStructuralFeature(HelperAdapter.getMergedFeatureID(object, feature.getFeatureID())));
			}
	      return object.eGet(feature);
	    }
	    catch (Throwable exception)
	    {
	      return null;
	    }
	}
}
