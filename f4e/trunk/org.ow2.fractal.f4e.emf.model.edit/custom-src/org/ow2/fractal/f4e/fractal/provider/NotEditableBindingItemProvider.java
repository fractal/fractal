package org.ow2.fractal.f4e.fractal.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemColorProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.ow2.fractal.f4e.fractal.Binding;
import org.ow2.fractal.f4e.fractal.FractalPackage;


public class NotEditableBindingItemProvider  extends BindingItemProvider implements INotEditableItemContentProvider, IItemColorProvider {
	
	public NotEditableBindingItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	 public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) 
	  {
	    if (itemPropertyDescriptors == null)
	    {
	      itemPropertyDescriptors = new ArrayList<IItemPropertyDescriptor>();
	      addMergedClientPropertyDescriptor(object);
	      addMergedServerPropertyDescriptor(object);
	      addClientInterfacePropertyDescriptor(object);
		  addServerInterfacePropertyDescriptor(object);
	    }
	    return itemPropertyDescriptors;
	  }
	 
	 public String getText(Object object) {
			String labelClient = ((Binding)object).getMergedClient();
			String labelServer = ((Binding)object).getMergedServer();
			return (labelClient == null &&  labelServer == null) || (labelClient.length() == 0  && labelServer.length() == 0) ?
				getString("_UI_Binding_type") :
				getString("_UI_Binding_type") + " " + labelClient + "-|---|-" + labelServer;
		}
	 
	public Object getBackground(Object object) {
		// TODO Auto-generated method stub
		return GRAY;
	}
	
	protected Object getFeatureValue(EObject object, EStructuralFeature feature){
		return new ArrayList();		
	}
	
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		
	}
}