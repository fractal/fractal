package org.ow2.fractal.f4e.fractal.provider;

import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.swt.graphics.Color;

public interface INotEditableItemContentProvider extends IStructuredItemContentProvider{
	static final Color GRAY = new org.eclipse.swt.graphics.Color(null,230,230,230);

}
