package org.ow2.fractal.f4e.fractal.provider;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.DelegatingWrapperItemProvider;
import org.eclipse.emf.edit.provider.IChangeNotifier;

public class NotEditableDelegatingWrapperItemProvider extends DelegatingWrapperItemProvider{
	public NotEditableDelegatingWrapperItemProvider(Object value, Object owner, EStructuralFeature feature, int index, AdapterFactory adapterFactory){
		 super(value, owner, feature, index, adapterFactory);
		    if (value == null)
		    {
		      throw new IllegalArgumentException("value=null");
		    }

		    Object delegateValue = getDelegateValue();
		    if (delegateValue != null && delegateValue instanceof Notifier)
		    {
		      delegateItemProvider = getRootAdapterFactory().adaptNew((Notifier)delegateValue, INotEditableItemContentProvider.class);
		      if (delegateItemProvider instanceof IChangeNotifier)
		      {
		        ((IChangeNotifier)delegateItemProvider).addListener(this);
		      }
		    }
	}
}
