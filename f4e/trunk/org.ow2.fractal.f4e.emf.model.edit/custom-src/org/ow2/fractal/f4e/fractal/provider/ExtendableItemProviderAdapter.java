package org.ow2.fractal.f4e.fractal.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.eclipse.emf.edit.EMFEditPlugin;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.ow2.fractal.f4e.extension.FractalADLExtensionHelper;
import org.ow2.fractal.f4e.fractal.AbstractComponent;
import org.ow2.fractal.f4e.fractal.FractalPackage;
import org.ow2.fractal.f4e.fractal.util.FractalMetaData;
import org.ow2.fractal.f4e.fractal.util.FractalResourceFactoryImpl;

/**
 * ItemProviderAdapter for elements that can be extended via
 * the extension mecanism.
 * 
 * It looks up in registred extension to find new features
 * and adds child descriptors for them
 * @author Yann Davin
 *
 */
public class ExtendableItemProviderAdapter extends ItemProviderAdapter{
	
	
	public ExtendableItemProviderAdapter(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	protected List<CommandParameter> createAnyChilds(Object object)
	 {
		 List<CommandParameter> resultList = new ArrayList<CommandParameter>();
		 
		 EObject ac = (EObject)object;
		 if(ac.eGet(ac.eClass().getEStructuralFeature("any")) != null && object instanceof EObject){		
			 List<EStructuralFeature> extendedFeatures = new FractalADLExtensionHelper().getNotExistingEStructuralFeature(((EObject)object).eClass());
			 for(EStructuralFeature feature : extendedFeatures){
				 FeatureMap.Entry entry = FeatureMapUtil.createEntry(feature,EcoreUtil.create((EClass)feature.getEType()));
				 resultList.add(new CommandParameter(null, FractalPackage.Literals.ABSTRACT_COMPONENT__ANY,entry));
			 }
		 }
		 return resultList;
	 }
	 
	protected String getFeatureText(Object feature) {
			// Fractal namespace is null
			if(feature instanceof EStructuralFeature && 
					FractalResourceFactoryImpl.getExtendedMetaData().getNamespace((EStructuralFeature)feature) != null){
				return ((EStructuralFeature)feature).getName();
			}
			return super.getFeatureText(feature);
	}

	public Object getCreateChildImage(Object owner, Object feature,
			Object child, Collection<?> selection) {
		try{
			return super.getCreateChildImage(owner, feature, child, selection);
		}catch(Exception e){
			 return EMFEditPlugin.INSTANCE.getImage("full/ctool16/CreateChild");
		}
	}

	public String getCreateChildText(Object owner, Object feature,
			Object child, Collection<?> selection) {
		return super.getCreateChildText(owner, feature, child, selection);
	}	
	
	protected ItemPropertyDescriptor createMergedItemPropertyDescriptor(
		    AdapterFactory adapterFactory,
		    ResourceLocator resourceLocator,
		    String displayName,
		    String description,
		    EStructuralFeature feature,
		    boolean isSettable,
		    boolean multiLine,
		    boolean sortChoices,
		    Object staticImage,
		    String category,
		    String[] filterFlags)
		  {
		    return new MergedItemPropertyDescriptor(
		      adapterFactory,
		      resourceLocator,
		      displayName,
		      description,
		      feature,
		      isSettable,
		      multiLine,
		      sortChoices,
		      staticImage,
		      category,
		      filterFlags);
		  }
}
