package org.ow2.fractal.eclipse.plugin.ui.wizards.tutorials;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IJavaProject;
import org.ow2.fractal.eclipse.plugin.installer.DefaultFractalTutorialsInstaller;
import org.ow2.fractal.eclipse.plugin.installer.IFractalTutorialsInstaller;
import org.ow2.fractal.eclipse.plugin.ui.wizards.FractalProjectWizard;
import org.ow2.fractal.eclipse.plugin.ui.wizards.NewFractalProjectWizardPageTwo;

public class NewFractalTutorialWizardPageTwo extends NewFractalProjectWizardPageTwo{
	protected IFractalTutorialsInstaller fractalTutorialInstaller;
	
	public NewFractalTutorialWizardPageTwo(FractalProjectWizard fractalWizard){
		super(fractalWizard);
		fractalTutorialInstaller = new DefaultFractalTutorialsInstaller();
	}
	
	public void initializeBuildPath(IJavaProject javaProject,
			IProgressMonitor monitor) throws CoreException {
		
		super.initializeBuildPath(javaProject, monitor);
		fractalTutorialInstaller.installTutorial(((FractalTutorialWizard)fractalWizard).getTutorialArchivePath(), javaProject.getProject().getLocation());
		javaProject.getResource().refreshLocal(IResource.DEPTH_INFINITE, monitor);
	}

	
}
