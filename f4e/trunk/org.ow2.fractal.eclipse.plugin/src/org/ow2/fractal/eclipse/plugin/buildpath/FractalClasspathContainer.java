package org.ow2.fractal.eclipse.plugin.buildpath;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.variables.IValueVariable;
import org.eclipse.core.variables.VariablesPlugin;
import org.eclipse.emf.codegen.jet.JETException;
import org.eclipse.jdt.core.IClasspathContainer;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.osgi.util.ManifestElement;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;
import org.ow2.fractal.eclipse.plugin.installer.FractalInstaller;
import org.ow2.fractal.eclipse.plugin.messages.Messages;


/** *
 * This classpath container add archive files from a configured project directory to the
 * classpath as CPE_LIBRARY entries, and it attaches -src archives as source attachments.
 *
 * @author Yann Davin, inspired by Aaron J Tarter
 */
public class FractalClasspathContainer implements IClasspathContainer {
    public final static Path ID = new Path("org.ow2.fractal.eclipse.plugin.buildpath.FRACTAL_CONTAINER");
    // use this string to represent the root project directory
    public final static String ROOT_DIR = "-";

    
    public final static String FRACTAL_FOLDER = "fractal";
    
    // the plugin relative path for embedded lib is /fractal/lib
    public final static String LIBRARY_FOLDER = "lib"; 
    
    // the plugin relative path for embedded lib sources is /fractal/sources
    public final static String LIBRARY_SOURCES_FOLDER = "sources"; 
    
    // user-fiendly name for the container that shows on the UI
    private String _desc;
    // path string that uniquiely identifies this container instance
    private IPath _path;
    // directory that will hold files for inclusion in this container
    private File _dir;
    // Filename extensions to include in container
    private HashSet<String> _exts;

    protected IJavaProject currentJavaProject;
    /**
     * This filename filter will be used to determine which files
     * will be included in the container
     */
    private FilenameFilter _dirFilter = new FilenameFilter() {

        /**
         * This File filter is used to filter files that are not in the configured
         * extension set.  Also, filters out files that have the correct extension
         * but end in -src, since filenames with this pattern will be attached as
         * source.
         *
         * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
         */
        public boolean accept(File dir, String name) {
            // lets avoid including filnames that end with -src since
            // we will use this as the convention for attaching source
            
            if(name.endsWith("-src.jar")) {
                return false;
            }
            if(name.endsWith(".jar")) {
                return true;
            }
            return false;
        }
    };

    /**
     * This constructor uses the provided IPath and IJavaProject arguments to assign the
     * instance variables that are used for determining the classpath entries included
     * in this container.  The provided IPath comes from the classpath entry element in
     * project's .classpath file.  It is a three segment path with the following
     * segments:
     *   [0] - Unique container ID
     *   [1] - project relative directory that this container will collect files from
     *   [2] - comma separated list of extensions to include in this container
     *         (extensions do not include the preceding ".")
     * @param path unique path for this container instance, including directory
     *             and extensions a segments
     * @param project the Java project that is referencing this container
     */
    public FractalClasspathContainer(IPath path, IJavaProject project) {
        currentJavaProject = project;
        
    	_path = path;
        
        // extract the extension types for this container from the path
        String extString = path.lastSegment();
        _exts = new HashSet<String>();
        String[] extArray = extString.split(",");
        for(String ext: extArray) {
            _exts.add(ext.toLowerCase());
        }
        // extract the directory string from the PATH and create the directory relative
        // to the project
        path = path.removeLastSegments(1).removeFirstSegments(1);
        File rootProj = project.getProject().getLocation().makeAbsolute().toFile();
        
        if(path.segmentCount()==1 && path.segment(0).equals(ROOT_DIR)) {
            _dir = rootProj;
            path = path.removeFirstSegments(1);
        } else {
            _dir = new File(rootProj, path.toString());
        }
        
    }

    /**
     * This method is used to determine if the directory specified
     * in the container path is valid, i.e. it exists relative to
     * the project and it is a directory.
     *
     * @return true if the configured directory is valid
     */
    public boolean isValid() {
        if(_dir.exists() && _dir.isDirectory()) {
            return true;
        }
        return false;
    }

    private IPath directory;
    
    public void setDirectory(IPath path){
    	directory = path;
    }
    
    /**
     * 
     * @return
     */
    protected String getFractalHome(){
    	 IValueVariable variable = VariablesPlugin.getDefault().getStringVariableManager().getValueVariable("FRACTAL_HOME");
    	 return variable.getValue();
    }
   
    /**
     * Returns a set of CPE_LIBRARY entries from the configured project directory
     * that conform to the configured set of file extensions and attaches a source
     * archive to the libraries entries if a file with same name ending with
     * -src is found in the directory.
     *
     * @see org.eclipse.jdt.core.IClasspathContainer#getClasspathEntries()
     */
    public IClasspathEntry[] getClasspathEntries() {
        ArrayList<IClasspathEntry> entryList = new ArrayList<IClasspathEntry>();
        // fetch the names of all files that match our filter
        File folder = _dir;
        
        // The /fractal/lib folder of the current project
        //folder = new File(folder + File.separator + FRACTAL_FOLDER + File.separator + LIBRARY_FOLDER);
        
        folder = new File(getFractalHome() + File.separator + LIBRARY_FOLDER);
        
        if(folder.exists()){
        	entryList.addAll(getClasspathEntries(folder));
        }
//        else{
//        	URL url = Platform.getBundle(FractalInstaller.FRACTAL_LIBRARIES_PLUGIN_ID).getEntry( "/" + FRACTAL_FOLDER + "/" + LIBRARY_FOLDER);
//    		try{
//    			String fractalHomeLib = Platform.resolve(url).toURI().getPath();
//    			entryList.addAll(getClasspathEntries(new File(fractalHomeLib)));
//    		}catch(Exception e){
//    			e.printStackTrace();
//    		}
//        }
       
        // convert the list to an array and return it
        IClasspathEntry[] entryArray = new IClasspathEntry[entryList.size()];
        return (IClasspathEntry[])entryList.toArray(entryArray);
    }

    /**
     * 
     * @param folder
     * @return a list a class path entries which contains all .jar present in the folder.
     */
    protected ArrayList<IClasspathEntry> getClasspathEntries(File folder){
    	 File sourcesFolder = _dir; 
         // The /fractal/sources folder of the current project
    	// sourcesFolder = new File(sourcesFolder + File.separator + FRACTAL_FOLDER + File.separator + LIBRARY_SOURCES_FOLDER);
    	 sourcesFolder = new File(getFractalHome() + File.separator + File.separator + LIBRARY_SOURCES_FOLDER);
    	ArrayList<IClasspathEntry> entryList = new ArrayList<IClasspathEntry>();
    	if(folder.exists()){
            File[] libs = folder.listFiles(_dirFilter);
            for( File lib: libs ) {
                // now see if this archive has an associated src jar
            	String sourcesJarFilename = sourcesFolder +  
            								File.separator + 
            								lib.getName().replace(".jar", "-sources.jar");
                File srcArc = new File(sourcesJarFilename);
                Path srcPath = null;
                // if the source archive exists then get the path to attach it
                if( srcArc.exists()) {
                    srcPath = new Path(srcArc.getAbsolutePath());
                }
                
                // create a new CPE_LIBRARY type of cp entry with an attached source
                // archive if it exists
                entryList.add( JavaCore.newLibraryEntry(
                        new Path(lib.getAbsolutePath()) , srcPath, null));
            }
    	}
    	return entryList;
    }
    
    public static List<String> getClasspathPaths(String pluginID) throws JETException
    {
      List<String> result = new ArrayList<String>();
      try
      {
        Bundle bundle = Platform.getBundle(pluginID);
        String requires = (String)bundle.getHeaders().get(Constants.BUNDLE_CLASSPATH);
        if (requires == null)
        {
          requires = ".";
        }
        ManifestElement[] elements = ManifestElement.parseHeader(Constants.BUNDLE_CLASSPATH, requires);
        if (elements != null)
        {
          for (int i = 0; i < elements.length; ++i)
          {
            ManifestElement element = elements[i];
            String value = element.getValue();
            if (".".equals(value))
            {
              value = "/";
            }
            try
            {
              URL url = bundle.getEntry(value);
              if (url != null)
              {
                URL resolvedURL = FileLocator.resolve(url);
                String resolvedURLString = resolvedURL.toString();
                if (resolvedURLString.endsWith("!/"))
                {
                  resolvedURLString = resolvedURL.getFile();
                  resolvedURLString = resolvedURLString.substring(0,resolvedURLString.length() - "!/".length());
                }
                if (resolvedURLString.startsWith("file:"))
                {
                  result.add(resolvedURLString.substring("file:".length()));
                }
                else
                {
                  result.add(FileLocator.toFileURL(url).getFile());
                }
              }
            }
            catch (IOException exception)
            {
              throw new JETException(exception);
            }
          }
        }
      }
      catch (BundleException exception)
      {
        throw new JETException(exception);
      }
      return result;
    }
    /* (non-Javadoc)
     * @see org.eclipse.jdt.core.IClasspathContainer#getDescription()
     */
    public String getDescription() {
    	return Messages.ClassPathFractalContainerName;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.core.IClasspathContainer#getKind()
     */
    public int getKind() {
        return IClasspathContainer.K_APPLICATION;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.core.IClasspathContainer#getPath()
     */
    public IPath getPath() {
        return _path;
    }

    /*
     * @return configured directory for this container
     */
    public File getDir() {
        return _dir;
    }

    /*
     * @return whether or not this container would include the file
     */
    public boolean isContained(File file) {
        if(file.getParentFile().equals(_dir)) {
            // peel off file extension
            String fExt = file.toString().substring(file.toString().lastIndexOf('.') + 1);
            // check is it is in the set of configured extensions
            if(_exts.contains(fExt.toLowerCase())) {
                return true;
            }
        }
        return false;
    }
    
}
