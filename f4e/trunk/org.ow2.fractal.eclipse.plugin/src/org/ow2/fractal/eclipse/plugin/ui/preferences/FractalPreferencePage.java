package org.ow2.fractal.eclipse.plugin.ui.preferences;

import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class FractalPreferencePage extends PreferencePage implements
IWorkbenchPreferencePage {
	protected  StringFieldEditor fractalHome;
	
	public FractalPreferencePage(){
		//setPreferenceStore(null);
		super();
	}
	
	public void init(IWorkbench workbench) {
		// TODO Auto-generated method stub
	}

	protected Control createContents(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);          
       
        return composite;
	}

	public boolean performOk() {
        return true;
    }
}
