package org.ow2.fractal.eclipse.plugin.ui.wizards.tutorials;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.ow2.fractal.eclipse.plugin.ui.wizards.FractalProjectWizard;
import org.ow2.fractal.eclipse.plugin.ui.wizards.NewFractalProjectWizardPageOne;
import org.ow2.fractal.eclipse.plugin.ui.wizards.NewFractalProjectWizardPageTwo;

// TODO: Auto-generated Javadoc
/**
 * The Class FractalProjectWizard. Copy from the JavaProjectWizard.
 */
public class FractalTutorialWizard extends FractalProjectWizard {
	protected String fTutorialArchivePath;
	protected String fProjectName;
	
	public IPath getTutorialArchivePath(){
		return new Path("/" + fTutorialArchivePath);
	}
	
	
	public void addPages() {
		if (fFirstPage == null)
			fFirstPage= new NewFractalTutorialWizardPageOne(this);
		addPage(fFirstPage);

		if (fSecondPage == null)
			fSecondPage= new NewFractalTutorialWizardPageTwo(this);
		addPage(fSecondPage);
		
		fFirstPage.setProjectName(fProjectName);
		fFirstPage.init(getSelection(), getActivePart());
	}		
	
	
	public void setInitializationData(IConfigurationElement cfig, String propertyName, Object data) {
			fConfigElement = cfig;
		
			String pageName = fConfigElement.getAttribute("name");
			String pageTitle = fConfigElement.getAttribute("projectPageTitle"); 
			String pageDescription = fConfigElement.getAttribute("projectPageDescription"); 
			String pageProjectName = fConfigElement.getAttribute("projectName"); 

			List nameFormatsL = new ArrayList();
			List zipURLs = new ArrayList();
			
			IConfigurationElement[] projectElements = fConfigElement.getChildren("tutorial"); 
			if(projectElements.length == 1) {
				fTutorialArchivePath = projectElements[0].getAttribute("archivePath"); 
				fProjectName = projectElements[0].getAttribute("projectName");
			}
			
	}
}