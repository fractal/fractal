package org.ow2.fractal.eclipse.plugin.messages;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	//classpath of the messages.properties
    private static final String BUNDLE_NAME = "org.ow2.fractal.eclipse.plugin.messages.messages"; //$NON-NLS-1$

    
    public static String ClassPathBrowse;

    public static String ClassPathDirErr;

    public static String ClassPathDirLabel;

    public static String ClassPathDirSelect;

    public static String ClassPathExtErr;

    public static String ClassPathExtLabel;

    public static String ClassPathInvalidContainer;

    public static String ClassPathPageDesc;

    public static String ClassPathPageName;

    public static String ClassPathPageTitle;
    
    public static String ClassPathFractalContainerName;
    
    public static String FractalProjectWizardName;
    
    public static String FractalProjectWizard_title;
    
    public static String FractalProjectWizardPageTwo_operation_copy_libraries;
    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }
}