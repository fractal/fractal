package org.ow2.fractal.eclipse.plugin.ui.wizards;

import org.eclipse.jdt.ui.wizards.NewJavaProjectWizardPageOne;
import org.ow2.fractal.eclipse.plugin.messages.Messages;

public class NewFractalProjectWizardPageOne extends NewJavaProjectWizardPageOne{
	protected FractalProjectWizard fractalWizard;
	
	public NewFractalProjectWizardPageOne(FractalProjectWizard fractalWizard){
		super();
		this.fractalWizard = fractalWizard;
		setTitle(Messages.FractalProjectWizard_title);
	}
}
