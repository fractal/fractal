package org.ow2.fractal.eclipse.plugin.ui.wizards;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.internal.core.JavaProject;
import org.eclipse.jdt.internal.ui.util.CoreUtility;
import org.eclipse.jdt.internal.ui.wizards.ClassPathDetector;
import org.eclipse.jdt.internal.ui.wizards.NewWizardMessages;
import org.eclipse.jdt.ui.wizards.NewJavaProjectWizardPageTwo;
import org.ow2.fractal.eclipse.plugin.buildpath.FractalClasspathContainer;
import org.ow2.fractal.eclipse.plugin.installer.DefaultFractalLibrariesInstaller;
import org.ow2.fractal.eclipse.plugin.installer.IFractalLibrariesInstaller;

public class NewFractalProjectWizardPageTwo extends NewJavaProjectWizardPageTwo{
	protected NewFractalProjectWizardPageOne pageOne;
	protected static final String FILENAME_CLASSPATH= ".classpath"; //$NON-NLS-1$
	protected static final String DEFAULT_OUTPUT_FRACTAL_LIB_FOLDER=File.separator + "lib" + File.separator + "fractal";
	protected IFractalLibrariesInstaller installer;
	protected FractalProjectWizard fractalWizard;
	
	public NewFractalProjectWizardPageTwo( FractalProjectWizard fractalWizard) {
		super(fractalWizard.fFirstPage);
		pageOne = fractalWizard.fFirstPage;
		this.fractalWizard = fractalWizard;
		installer = new DefaultFractalLibrariesInstaller();
	}

	
	public void performFinish(IProgressMonitor monitor) throws CoreException,
			InterruptedException {
		super.performFinish(monitor);
	}

	private boolean hasExistingContent(URI realLocation) throws CoreException {
		IFileStore file= EFS.getStore(realLocation);
		return file.fetchInfo().exists();
	}
	
	
	public void initializeBuildPath(IJavaProject javaProject, IProgressMonitor monitor) throws CoreException {
	//	super.initializeBuildPath(javaProject, monitor);
		
		Boolean fKeepContent = false;
		
		String projectName= pageOne.getProjectName();

		IProject currentProject= javaProject.getProject();
		
		monitor.beginTask(NewWizardMessages.NewJavaProjectWizardPageTwo_monitor_init_build_path, 2);
		
		try {
			IClasspathEntry[] entries= null;
			IPath outputLocation= null;
			
//			URI currProjectLocation =  new URI(
//					ResourcesPlugin.getWorkspace().getRoot().getLocationURI().toString() + 
//					javaProject.getProject().getFullPath());
			
			IFolder projectFolder = ResourcesPlugin.getWorkspace().getRoot().getFolder(javaProject.getProject().getLocation());
			
			// Install fractal libraries
			if(installer != null){
				//installer.installFractalLibraries(new Path(currProjectLocation.getRawPath() + DEFAULT_OUTPUT_FRACTAL_LIB_FOLDER ));
				projectFolder.refreshLocal(IResource.DEPTH_INFINITE, new SubProgressMonitor(monitor, 2));
			}
			
			if (monitor == null) {
				monitor= new NullProgressMonitor();
			}
				
			if (fKeepContent) {
				if (!currentProject.getFile(FILENAME_CLASSPATH).exists()) { 
					final ClassPathDetector detector= new ClassPathDetector(currentProject, new SubProgressMonitor(monitor, 2));
					entries= detector.getClasspath();
					outputLocation= detector.getOutputLocation();
					
					List<IClasspathEntry> cpEntries= new ArrayList<IClasspathEntry>();
					cpEntries.add(JavaCore.newContainerEntry(FractalClasspathContainer.ID));
					entries= (IClasspathEntry[]) cpEntries.toArray(new IClasspathEntry[cpEntries.size()]);
					
					if (entries.length == 0){
						entries= null;
					}
				} else {
					monitor.worked(2);
				}
			} else {
				List cpEntries= new ArrayList();
				IWorkspaceRoot root= currentProject.getWorkspace().getRoot();

				IClasspathEntry[] sourceClasspathEntries= pageOne.getSourceClasspathEntries();
				for (int i= 0; i < sourceClasspathEntries.length; i++) {
					IPath path= sourceClasspathEntries[i].getPath();
					if (path.segmentCount() > 1) {
						IFolder folder= root.getFolder(path);
						CoreUtility.createFolder(folder, true, true, new SubProgressMonitor(monitor, 1));
					}
					cpEntries.add(sourceClasspathEntries[i]);
				}

				cpEntries.addAll(Arrays.asList(pageOne.getDefaultClasspathEntries()));
				cpEntries.add(JavaCore.newContainerEntry(FractalClasspathContainer.ID));
				
				entries= (IClasspathEntry[]) cpEntries.toArray(new IClasspathEntry[cpEntries.size()]);
				
				outputLocation= pageOne.getOutputLocation();
				if (outputLocation.segmentCount() > 1) {
					IFolder folder= root.getFolder(outputLocation);
					CoreUtility.createDerivedFolder(folder, true, true, new SubProgressMonitor(monitor, 1));
				}
			}
			if (monitor.isCanceled()) {
				throw new OperationCanceledException();
			}
			init(javaProject, outputLocation, entries, false);
		}catch(Exception e){
			e.printStackTrace();
		}
		finally {
			monitor.done();
		}
	}
	
}

