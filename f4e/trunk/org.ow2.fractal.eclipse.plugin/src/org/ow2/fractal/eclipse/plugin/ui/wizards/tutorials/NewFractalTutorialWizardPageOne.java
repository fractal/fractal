package org.ow2.fractal.eclipse.plugin.ui.wizards.tutorials;

import org.ow2.fractal.eclipse.plugin.ui.wizards.FractalProjectWizard;
import org.ow2.fractal.eclipse.plugin.ui.wizards.NewFractalProjectWizardPageOne;

public class NewFractalTutorialWizardPageOne extends NewFractalProjectWizardPageOne{
	public NewFractalTutorialWizardPageOne(FractalProjectWizard fractalWizard){
		super(fractalWizard);
	}
}
