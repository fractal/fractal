package org.ow2.fractal.eclipse.plugin.ui;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.internal.ui.JavaPluginImages;
import org.eclipse.jface.resource.ImageDescriptor;
import org.ow2.fractal.eclipse.plugin.FractalPlugin;

public class FractalPluginImages extends JavaPluginImages {
	private static final String T_WIZBAN= "wizban";
	
	public static final ImageDescriptor DESC_WIZBAN_NEWJPRJ= createUnManaged(T_WIZBAN, "newjprj_wiz.png"); 
	
	private static ImageDescriptor createUnManaged(String prefix, String name) {
		return create(prefix, name, true);
	}
	
	private static ImageDescriptor create(String prefix, String name,  boolean useMissingImageDescriptor) {
		IPath path= ICONS_PATH.append(prefix).append(name);
		return createImageDescriptor(FractalPlugin.getDefault().getBundle(), path, useMissingImageDescriptor);
	}
}
