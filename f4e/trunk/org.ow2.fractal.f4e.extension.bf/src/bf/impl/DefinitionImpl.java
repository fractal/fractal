/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package bf.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import bf.BfPackage;
import bf.Binder;
import bf.Definition;
import bf.Exporter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link bf.impl.DefinitionImpl#getExporters <em>Exporters</em>}</li>
 *   <li>{@link bf.impl.DefinitionImpl#getBinders <em>Binders</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DefinitionImpl extends EObjectImpl implements Definition {
	/**
	 * The cached value of the '{@link #getExporters() <em>Exporters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExporters()
	 * @generated
	 * @ordered
	 */
	protected EList<Exporter> exporters;

	/**
	 * The cached value of the '{@link #getBinders() <em>Binders</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBinders()
	 * @generated
	 * @ordered
	 */
	protected EList<Binder> binders;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BfPackage.Literals.DEFINITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Exporter> getExporters() {
		if (exporters == null) {
			exporters = new EObjectContainmentEList<Exporter>(Exporter.class, this, BfPackage.DEFINITION__EXPORTERS);
		}
		return exporters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Binder> getBinders() {
		if (binders == null) {
			binders = new EObjectContainmentEList<Binder>(Binder.class, this, BfPackage.DEFINITION__BINDERS);
		}
		return binders;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BfPackage.DEFINITION__EXPORTERS:
				return ((InternalEList<?>)getExporters()).basicRemove(otherEnd, msgs);
			case BfPackage.DEFINITION__BINDERS:
				return ((InternalEList<?>)getBinders()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BfPackage.DEFINITION__EXPORTERS:
				return getExporters();
			case BfPackage.DEFINITION__BINDERS:
				return getBinders();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BfPackage.DEFINITION__EXPORTERS:
				getExporters().clear();
				getExporters().addAll((Collection<? extends Exporter>)newValue);
				return;
			case BfPackage.DEFINITION__BINDERS:
				getBinders().clear();
				getBinders().addAll((Collection<? extends Binder>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BfPackage.DEFINITION__EXPORTERS:
				getExporters().clear();
				return;
			case BfPackage.DEFINITION__BINDERS:
				getBinders().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BfPackage.DEFINITION__EXPORTERS:
				return exporters != null && !exporters.isEmpty();
			case BfPackage.DEFINITION__BINDERS:
				return binders != null && !binders.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DefinitionImpl
