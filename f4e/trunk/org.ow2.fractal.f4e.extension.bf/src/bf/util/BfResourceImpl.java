/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package bf.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see bf.util.BfResourceFactoryImpl
 * @generated
 */
public class BfResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public BfResourceImpl(URI uri) {
		super(uri);
	}

} //BfResourceImpl
