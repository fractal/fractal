/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package bf;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link bf.Definition#getExporters <em>Exporters</em>}</li>
 *   <li>{@link bf.Definition#getBinders <em>Binders</em>}</li>
 * </ul>
 * </p>
 *
 * @see bf.BfPackage#getDefinition()
 * @model
 * @generated
 */
public interface Definition extends EObject {
	/**
	 * Returns the value of the '<em><b>Exporters</b></em>' containment reference list.
	 * The list contents are of type {@link bf.Exporter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exporters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exporters</em>' containment reference list.
	 * @see bf.BfPackage#getDefinition_Exporters()
	 * @model containment="true"
	 * @generated
	 */
	EList<Exporter> getExporters();

	/**
	 * Returns the value of the '<em><b>Binders</b></em>' containment reference list.
	 * The list contents are of type {@link bf.Binder}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binders</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binders</em>' containment reference list.
	 * @see bf.BfPackage#getDefinition_Binders()
	 * @model containment="true"
	 * @generated
	 */
	EList<Binder> getBinders();

} // Definition
