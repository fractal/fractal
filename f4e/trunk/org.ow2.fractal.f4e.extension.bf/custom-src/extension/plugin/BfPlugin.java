package extension.plugin;

import org.eclipse.core.runtime.Plugin;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.ui.IStartup;
import org.osgi.framework.BundleContext;

import bf.BfPackage;

/**
 * The activator class controls the plug-in life cycle
 */
public class BfPlugin extends Plugin implements IStartup {

	
	public void earlyStartup() {
		// TODO Auto-generated method stub
		
	}

	// The shared instance
	private static BfPlugin plugin;
	

	/**
	 * The constructor
	 */
	public BfPlugin() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		
		EPackage.Registry.INSTANCE.put(BfPackage.eINSTANCE.getNsURI(),
				BfPackage.eINSTANCE);
 
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static BfPlugin getDefault() {
		return plugin;
	}
}
