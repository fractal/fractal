/**
 * Copyright (C) 2013 Inria
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: gwenael.cattez@inria.fr
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */
package org.ow2.fractal.upnp.client;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.logging.Logger;

import org.ow2.fractal.upnp.exception.UPnPActionArgumentNotFound;
import org.ow2.fractal.upnp.exception.UPnPActionNotFoundException;
import org.ow2.fractal.upnp.exception.UPnPDeviceNotFoundException;
import org.ow2.fractal.upnp.exception.UPnPException;
import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.model.action.ActionArgumentValue;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.meta.Action;
import org.teleal.cling.model.meta.ActionArgument;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.LocalDevice;
import org.teleal.cling.model.meta.RemoteDevice;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.model.types.Datatype;
import org.teleal.cling.model.types.UDN;
import org.teleal.cling.registry.DefaultRegistryListener;
import org.teleal.cling.registry.Registry;

public abstract class UPnPGeneratedServiceClient extends DefaultRegistryListener
{
    protected final static Logger logger = Logger.getLogger(UPnPGeneratedServiceClient.class.getName());
    
    /**UPnP Device id, string representation of UDN*/
    private String upnpDeviceId;
    
    /**UPnP Device type*/
    private String upnpDeviceType;
    
    /**UPnP Service Id*/
    private String upnpServiceId;
    
    /**UPnP Service type*/
    private String upnpServiceType;
    
    /**UPnP Cling remote service*/
    @SuppressWarnings("rawtypes")
    private Service upnpService;

    private boolean isValid;
    
    public UPnPGeneratedServiceClient()
    {
        this.isValid=false;
    }
    
    @SuppressWarnings("rawtypes")
    public void validate() throws UPnPException
    {
        if(isValid)
        {
            return;
        }
        
        Collection<Device> devices = CommonClientUPnPService.getInstance().getUPnPDevices(this.upnpServiceType);
        for(Device device : devices)
        {
            this.upnpService=this.getValidService(device);
            if(this.upnpService!=null)
            {
                break;
            }
        }
        
        if (this.upnpService == null)
        {
            throw new UPnPDeviceNotFoundException(this.upnpDeviceId, this.upnpDeviceType, this.upnpServiceId, this.upnpServiceType);
        }

        logger.fine(this.getClass().getSimpleName()+" is valid");
        this.isValid=true;
    }
    
    @SuppressWarnings("rawtypes")
    private Service getValidService(Device upnpDevice)
    {
        logger.info("getValidService "+upnpDevice.toString());
        
        //if upnpDeviceId is defined and not equals to upnpDevicd UDN
        if(this.upnpDeviceId!=null && !this.upnpDeviceId.equals(""))
        {
          UDN deviceUDN;
          try
          {
              UUID uuid = UUID.fromString(this.upnpDeviceId);
              deviceUDN=new UDN(uuid);
          }
          catch(Exception e)
          {
              deviceUDN = UDN.valueOf(this.upnpDeviceId);
          }
          
          /**UDN comparaison is case sentive so we need to compare their String representation and ignoring case*/
          if(!upnpDevice.getIdentity().getUdn().toString().equalsIgnoreCase(deviceUDN.toString()))
          {
              return null;
          }
        }
        
        //if upnpDeviceType is defined and not equals to upnpDevicd type
        if(this.upnpDeviceType!=null && !this.upnpDeviceType.equals("") && !this.upnpDeviceType.equals(upnpDevice.getType().getType()))
        {
            return null;
        }
        
        boolean isUPnPServiceIDDefined = this.upnpServiceId!=null && !this.upnpServiceId.equals("");
        boolean isUPnPServiceTypeDefined = this.upnpServiceType!=null && !this.upnpServiceType.equals("");
        for(Service upnpDeviceService : upnpDevice.getServices())
        {
            if(isUPnPServiceTypeDefined && !this.upnpServiceType.equals(upnpDeviceService.getServiceType().getType()))
            {
                continue;
            }
            if(isUPnPServiceIDDefined && !this.upnpServiceId.equals(upnpDeviceService.getServiceId().getId()))
            {
                continue;
            }
            
            return upnpDeviceService;
        }
        return null;
    }
    

    
    /**UPnP action invocation methods*/
    protected ActionArgumentValue<?>[] doAction(String actionName, Map<String, Object> args)
    {
        logger.fine("doAction, actionName "+actionName);
        for(Entry<String, Object> argsSet : args.entrySet())
        {
            logger.fine("\t"+argsSet.getKey()+" : "+argsSet.getValue());
        }

        Action<?> action = this.upnpService.getAction(actionName);
        if(action==null)
        {
            throw new UPnPActionNotFoundException(this.upnpServiceType, actionName);
        }
        
        @SuppressWarnings({ "rawtypes", "unchecked" })
        ActionInvocation<?> actionInvocation = new ActionInvocation(action);
        Object argValue;
        for (Entry<String, Object> arg : args.entrySet())
        {
            argValue = this.getArgValue(action, arg.getKey(), arg.getValue());
            actionInvocation.setInput(arg.getKey(), argValue);
        }
        
        ActionCallback setTargetCallback = new ActionCallback.Default(actionInvocation, CommonClientUPnPService.getInstance().getControlPoint());
        setTargetCallback.run();
        if(setTargetCallback.getActionInvocation()==null)
        {
                logger.severe("Action failed : "+setTargetCallback.getActionInvocation().getFailure().toString());
        }
        return setTargetCallback.getActionInvocation().getOutput();
    }

    private Object getArgValue(Action<?> action, String argName, Object value)
    {
        logger.fine(UPnPGeneratedServiceClient.class.getName() + ".getArgValue(" + action + ", " + argName + ", " + value + ")");
        ActionArgument<?> actionArgument = action.getInputArgument(argName);
        if(actionArgument==null)
        {
            throw new UPnPActionArgumentNotFound(action, argName);
        }
        
        Datatype<?> datatype = action.getService().getDatatype(actionArgument);
        if (datatype == null)
        {
            return null;
        }
        return datatype.valueOf(value.toString());
    }
    
    protected String getActionArgumentStringValue(String argumentValueName, ActionArgumentValue<?>[] actionArgumentValues)
    {
        for(ActionArgumentValue<?> actionArgumentValue : actionArgumentValues)
        {
                if(actionArgumentValue.getArgument().getName().toLowerCase().equals(argumentValueName.toLowerCase()))
                {
                        if(actionArgumentValue.getValue()!=null)
                        {
                                return actionArgumentValue.getValue().toString();
                        }
                }
        }
        return null;
    }

    
    /**RegistryListener methods*/
    public void remoteDeviceRemoved(Registry registry, RemoteDevice device)
    {
        logger.fine("Remote device removed: " + device.getDisplayString());
        if(this.isValid() && this.upnpService.getDevice().equals(device))
        {
            logger.severe("Client is no longer available, upnp device "+device.getDisplayString()+" has been stopped");
            isValid=false;
        }
    }

    public void localDeviceRemoved(Registry registry, LocalDevice device)
    {
        if(this.isValid() && this.upnpService.getDevice().equals(device))
        {
            isValid=false;
           logger.severe("Client is no longer available, upnp device "+device.getDisplayString()+" has been stopped");
        }
    }

    /**Getters and setters*/
    public String getUpnpDeviceId()
    {
        return upnpDeviceId;
    }

    public void setUpnpDeviceId(String upnpDeviceId)
    {
        this.upnpDeviceId = upnpDeviceId;
    }

    public String getUpnpDeviceType()
    {
        return upnpDeviceType;
    }

    public void setUpnpDeviceType(String upnpDeviceType)
    {
        this.upnpDeviceType = upnpDeviceType;
    }

    public String getUpnpServiceId()
    {
        return upnpServiceId;
    }

    public void setUpnpServiceId(String upnpServiceId)
    {
        this.upnpServiceId = upnpServiceId;
    }

    public String getUpnpServiceType()
    {
        return upnpServiceType;
    }

    public void setUpnpServiceType(String upnpServiceType)
    {
        this.upnpServiceType = upnpServiceType;
    }

    public boolean isValid()
    {
        return isValid;
    }
    
    @Override
    public String toString()
    {
        return "deviceId "+this.upnpDeviceId+" deviceType "+this.upnpDeviceType+" upnpServiceId "+this.upnpServiceId+" upnpServiceType "+this.upnpServiceType+" isValid "+this.isValid;
    }
}
