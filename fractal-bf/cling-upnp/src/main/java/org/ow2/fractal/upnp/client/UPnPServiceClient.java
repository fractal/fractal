/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.fractal.upnp.client;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.ow2.fractal.upnp.annotation.UPnPAction;
import org.ow2.fractal.upnp.annotation.UPnPVariable;
import org.ow2.fractal.upnp.exception.UPnPException;
import org.ow2.fractal.upnp.service.AbstractUPnPService;
import org.ow2.fractal.upnp.util.JavaCompilerUtil;
import org.ow2.fractal.upnp.util.ReflectionUtil;
import org.teleal.cling.model.action.ActionArgumentValue;



/**
 *
 */
public class UPnPServiceClient extends AbstractUPnPService
{
    /**
     * UPnP Device id, string representation of UDN
     */
    private String upnpDeviceId;
    
    /**
     * UPnP Device type
     */
    private String upnpDeviceType;
    
    @SuppressWarnings("unchecked")
    public<T> T generateUPnPClient(Class<T> serviceClass) throws UPnPException
    {
        this.upnpServiceClass=serviceClass;
        Object upnpServiceClient = this.generateServiceInstance();
        
        UPnPGeneratedServiceClient upnpGeneratedServiceClient= (UPnPGeneratedServiceClient) upnpServiceClient;
        upnpGeneratedServiceClient.setUpnpDeviceId(this.upnpDeviceId);
        upnpGeneratedServiceClient.setUpnpDeviceType(this.upnpDeviceType);
        upnpGeneratedServiceClient.setUpnpServiceId(this.upnpServiceId);
        upnpGeneratedServiceClient.setUpnpServiceType(this.upnpServiceType);
        upnpGeneratedServiceClient.validate();
        return (T) upnpGeneratedServiceClient;
    }
    
    /**
     * @param generatedUPnPServiceClassSimpleName
     * @return
     * @throws UPnPException 
     */
    protected String generateSource(String generatedUPnPServiceClassSimpleName) throws UPnPException
    {
        StringBuilder upnpClientSourceBuilder = new StringBuilder();
        // Package and class declaration
        upnpClientSourceBuilder.append("package " + JavaCompilerUtil.generationPackage + ";\n");
        upnpClientSourceBuilder.append("public class " + generatedUPnPServiceClassSimpleName + "\n");
        upnpClientSourceBuilder.append(" extends " + UPnPGeneratedServiceClient.class.getName());
        upnpClientSourceBuilder.append(" implements ");
        upnpClientSourceBuilder.append(this.upnpServiceClass.getName() + "\n");
        upnpClientSourceBuilder.append("{\n");

        String methodDeclaration, upnpStateVariableName, upnpActionName, returnTypeFieldSetter;
        Annotation[][] paramsAnnotations;
        Class<?> consistentReturnType, consistentFieldType;
        for (Method method : this.upnpServiceClass.getMethods())
        {
            methodDeclaration = ReflectionUtil.generateMethodDeclaration(method);
            upnpClientSourceBuilder.append("\t"+ methodDeclaration +"\n");
            
            upnpActionName = this.getUPnPActionName(method);
            
            upnpClientSourceBuilder.append("\t{\n");
            upnpClientSourceBuilder.append("\t\t" + Map.class.getName() + "<" + String.class.getName() + ", " + Object.class.getName() + ">");
            upnpClientSourceBuilder.append("args = new "+ HashMap.class.getName() + "<" + String.class.getName() + ", " + Object.class.getName() + ">();\n");
            
            paramsAnnotations = method.getParameterAnnotations();
            for (int parameterIndex = 0; parameterIndex < paramsAnnotations.length; parameterIndex++)
            {
                upnpStateVariableName = this.getUPnPStateVariableName(method, parameterIndex);
                upnpClientSourceBuilder.append("\t\targs.put(\"" + upnpStateVariableName + "\", arg" + parameterIndex + ");\n");
            }

            upnpClientSourceBuilder.append("\t\t" + ActionArgumentValue.class.getName() + "[] ");
            upnpClientSourceBuilder.append("output = doAction(\""+ upnpActionName+ "\", args);\n");

            consistentReturnType = ReflectionUtil.getConsistentClass(method.getReturnType());
            if(consistentReturnType.equals(Void.class))
            {
                //nothing to do
            }
            else if(consistentReturnType.equals(String.class))
            {
                upnpClientSourceBuilder.append("\t\treturn output[0].getValue().toString();\n");
            }
            else
            {
                try
                {
                    //if return type have a valueOF(String) method use it to create the return Object
                    if (consistentReturnType.getMethod("valueOf", String.class) != null)
                    {
                        upnpClientSourceBuilder.append("\t\treturn " + consistentReturnType.getName() + ".valueOf(output[0].getValue().toString());\n");
                    }
                } catch (NoSuchMethodException e)
                {
                    try
                    {
                        //check that return type class have a default empty constructor
                        consistentReturnType.getConstructor();
                        upnpClientSourceBuilder.append("\t\t"+consistentReturnType.getName() + " toReturn = new " + consistentReturnType.getName() + "();\n");
                    }
                    catch (Exception exception)
                    {
                        throw new UPnPException("No empty constructor available for return type : "+consistentReturnType.getName());
                    }
                    
                    for (Field returnTypeField : ReflectionUtil.getAllFields(consistentReturnType))
                    {
                        consistentFieldType = ReflectionUtil.getConsistentClass(returnTypeField.getType());
                        upnpClientSourceBuilder.append("\t\tString " + returnTypeField.getName() + "String = this.getActionArgumentStringValue(\"" + returnTypeField.getName() + "\", output );\n");
                        upnpClientSourceBuilder.append("\t\tif (" + returnTypeField.getName() + "String !=null)\n");
                        upnpClientSourceBuilder.append("\t\t{\n");
                        returnTypeFieldSetter="set" + returnTypeField.getName().replaceFirst(".", (returnTypeField.getName().charAt(0) + "").toUpperCase());
                        //check stter exist for the current field
                        try
                        {
                            consistentReturnType.getMethod(returnTypeFieldSetter, returnTypeField.getType());
                        }
                        catch (Exception exception)
                        {
                            throw new UPnPException("No setter found for field "+returnTypeField.getName()+"  on return type : "+consistentReturnType.getName());
                        }
                        upnpClientSourceBuilder.append("\t\t\ttoReturn."+ returnTypeFieldSetter + "("+ consistentFieldType.getName() + ".valueOf(" + returnTypeField.getName() + "String));\n");
                        upnpClientSourceBuilder.append("\t\t}\n\n");
                    }
                    upnpClientSourceBuilder.append("\t\treturn toReturn;\n");
                }
            }
            upnpClientSourceBuilder.append("\t}\n\n");
        }
        upnpClientSourceBuilder.append("}\n");

        return upnpClientSourceBuilder.toString();
    }
    
    /**Getters and setters*/
    public String getUpnpDeviceId()
    {
        return upnpDeviceId;
    }

    public void setUpnpDeviceId(String upnpDeviceId)
    {
        this.upnpDeviceId = upnpDeviceId;
    }

    public String getUpnpDeviceType()
    {
        return upnpDeviceType;
    }

    public void setUpnpDeviceType(String upnpDeviceType)
    {
        this.upnpDeviceType = upnpDeviceType;
    }

}