/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.fractal.upnp.server;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.ow2.fractal.upnp.util.ApacheUpnpServiceConfiguration;
import org.teleal.cling.UpnpService;
import org.teleal.cling.UpnpServiceImpl;
import org.teleal.cling.model.meta.LocalDevice;
import org.teleal.cling.model.types.UDN;
import org.teleal.cling.registry.Registry;

/**
 *
 */
public class UPnPServer implements Runnable
{
    protected final static Logger logger = Logger.getLogger(UPnPServer.class.getName());
    
    protected List<LocalDevice> upnpDevices;
    
    protected UpnpService upnpService;
    
    private boolean started;
    
    public UPnPServer()
    {
        logger.fine("Create UPnPServer object");
        this.upnpDevices=new ArrayList<LocalDevice>();
        this.started=false;
    }

    public boolean addUPnPDevice(LocalDevice upnpDevice)
    {
        logger.fine("addUPnPDevice");
        if(upnpDevice==null)
        {
            logger.severe("The UPnP device is null");
            return false;
        }
        
        this.upnpDevices.add(upnpDevice);
        if(!this.isStarted())
        {
            return true;
        }
        else
        {
            logger.info("UPnP server started, register new UPnP device, id: "+upnpDevice.getIdentity().getUdn().getIdentifierString());
            try
            {
                this.upnpService.getRegistry().addDevice(upnpDevice);
                return true;
            }
            catch (RuntimeException runtimeException)
            {
                runtimeException.printStackTrace();
                return false;
            }
        }
    }
    
    public boolean removeUPnPDevice(UDN upnpDeviceUDN)
    {
        logger.fine("removeUPnPDevice UDN : "+upnpDeviceUDN.getIdentifierString());
        LocalDevice upnpDevice = this.upnpService.getRegistry().getLocalDevice(upnpDeviceUDN, true);
        if(upnpDevice==null)
        {
            logger.fine(upnpDeviceUDN+" device not found");
            return false;
        }
        
        if(this.upnpDevices.remove(upnpDevice))
        {
            upnpService.getRegistry().removeDevice(upnpDevice);
            logger.info(upnpDeviceUDN+" device is now stopped");
            return true;
        }
        else
        {
            logger.fine(upnpDeviceUDN+" device have not been registered on this server");
            return false;
        }
    }
    
    public boolean removeUPnPDevice(String upnpDeviceId)
    {
        logger.fine("removeUPnPDevice id : "+upnpDeviceId);
        UDN upnpDeviceUDN = UDN.valueOf(upnpDeviceId);
        return this.removeUPnPDevice(upnpDeviceUDN);
    }
    
    /**
     * @throws InterruptedException 
     * @see org.ow2.fractal.upnp.factory.server.UPnPServerItf#start()
     */
    public boolean start()
    {
        if(isStarted())
        {
            return false;
        }
        
        Thread serverThread=new Thread(this);
        serverThread.start();
        
        while(!isStarted())
        {
            try
            {
                Thread.sleep(100);
            } catch (InterruptedException e)
            {
                //should not happen
                e.printStackTrace();
            }
        }
        
        return true;
    }

    /**
     * @throws InterruptedException 
     * @see org.ow2.fractal.upnp.factory.server.UPnPServerItf#stop()
     */
    public boolean stop()
    {
        if(!isStarted())
        {
            return false;
        }
        this.upnpService.shutdown();
        started=false;
        logger.info("UPnP server is now stopped");
        return true;
    }

    /**
     * @see org.ow2.fractal.upnp.factory.server.UPnPServerItf#isStarted()
     */
    public boolean isStarted()
    {
        return started;
    }

    /**
     * @see java.lang.Runnable#run()
     */
    public void run()
    {
        this.upnpService=new UpnpServiceImpl(new ApacheUpnpServiceConfiguration());
        Registry upnpRegistry = this.upnpService.getRegistry();
        logger.info("UPnP server is now started");
        for(LocalDevice upnpLocalDevice : this.upnpDevices)
        {
            upnpRegistry.addDevice(upnpLocalDevice);
            logger.info("Register UPnP device : "+upnpLocalDevice.getIdentity().getUdn().getIdentifierString());
        }
        started=true;
    }
}
