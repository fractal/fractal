/**
 * Copyright (C) 2013 Inria
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: gwenael.cattez@inria.fr
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */
package org.ow2.fractal.upnp.device;

import org.ow2.fractal.upnp.exception.UPnPException;
import org.teleal.cling.model.meta.LocalService;

/**
 *
 */
public abstract class UPnPGeneratedDeviceService<T>
{
    
    /**
     * Java Objet that executes business method for this UPnP service
     */
    protected T serviceProvider;
    
    @SuppressWarnings("unchecked")
    public void setServiceProvider(Object serviceProvider) throws UPnPException
    {
        try
        {
            this.serviceProvider=(T) serviceProvider;
        }
        catch (ClassCastException classCastException)
        {
            throw new UPnPException("Invalid UPnP service provider, class "+serviceProvider.getClass().getName());
        }
    }
    
    @SuppressWarnings("rawtypes")
    public abstract LocalService getLocalService(); 
}
