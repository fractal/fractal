/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.fractal.upnp.service;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import org.ow2.fractal.upnp.annotation.UPnPAction;
import org.ow2.fractal.upnp.annotation.UPnPService;
import org.ow2.fractal.upnp.annotation.UPnPVariable;
import org.ow2.fractal.upnp.exception.UPnPException;
import org.ow2.fractal.upnp.util.JavaCompilerUtil;
import org.ow2.fractal.upnp.util.ReflectionUtil;

/**
 *
 */
public abstract class AbstractUPnPService
{
    /**
     * UPnP Service Id
     */
    protected String upnpServiceId;
    
    /**
     * UPnP Service type
     */
    protected String upnpServiceType;

    /**
     * UPnP Service class
     */
    protected Class<?> upnpServiceClass;

    
    protected void validateServiceParams() throws UPnPException
    {
        /** On both client and server side service type must be defined */
        /**Priority to UPnPService annotation*/
        UPnPService upnpServiceAnnotation=this.upnpServiceClass.getAnnotation(UPnPService.class);
        if(upnpServiceAnnotation!=null)
        {
            /**if id is defined set it*/
            if(!upnpServiceAnnotation.id().equals(""))
            {
                this.upnpServiceId=upnpServiceAnnotation.id();
            }
            /**if type is defined set it*/
            if(!upnpServiceAnnotation.type().equals(""))
            {
                this.upnpServiceType=upnpServiceAnnotation.type();
            }
        }
        
        /**At this point no UPnPService annotation has been found and no upnpServiceType is defined
         * Use the upnpServiclass qualfiedName as UPnP type by replacing dot by - 
         * */
        if(this.upnpServiceType==null || this.upnpServiceType.equals(""))
        {
            this.upnpServiceType=this.upnpServiceClass.getName().replace(".", "-");
        }
    }
    
    public Object generateServiceInstance() throws UPnPException
    {
        this.validateServiceParams();
        
        try
        {
            String generatedUPnPServiceClassSimpleName = JavaCompilerUtil.generateClassName(this.upnpServiceClass);
            String serviceSource = this.generateSource(generatedUPnPServiceClassSimpleName);
            String generatedUPnPServiceClassQualifiedName = JavaCompilerUtil.generationPackage + "." + generatedUPnPServiceClassSimpleName;
            JavaCompilerUtil.compile(generatedUPnPServiceClassQualifiedName, serviceSource);
            Class<?> generatedUPnPServiceClass = Class.forName(generatedUPnPServiceClassQualifiedName, true, JavaCompilerUtil.getCurrentClassLoader());
            return generatedUPnPServiceClass.newInstance();
        }
        catch (Exception e)
        {
            // No Exception should happen
            e.printStackTrace();
            throw new UPnPException("UPnP DeviceService code generation failed");
        }
    }
    
    protected abstract String generateSource(String generatedUPnPServiceClassSimpleName) throws UPnPException;
    
    /**UPnP name generation methods */
    /**By using those methods client and server have the same naming rules*/
    
    /**
     * Get UPnP action name for a Java method
     * 
     * If the method is annotated with UPnPAction annotation use it, otherwise use Java method name
     * UPnP action name always start with an upperCase letter
     * 
     * @param method The Java method
     * @return name of the related UPnP action
     */
    protected String getUPnPActionName(Method method)
    {
        String upnpActionName;
        if(method.getAnnotation(UPnPAction.class)!=null)
        {
            upnpActionName =  method.getAnnotation(UPnPAction.class).value();
        }
        else
        {
            upnpActionName = method.getName();
        }
        return ReflectionUtil.firstLetterToUpperCase(upnpActionName);
    }
    
    /**
     * Get UPnP state variable name for a Java method and a parameter index
     * 
     * If the method parameter is annotated with UPnPVariable annotation use it, otherwise use upnp action name of Java method and parameter index to create a name
     * UPnP state variable name always start with an upperCase letter
     * 
     * @param method
     * @param parameterIndex
     * @return
     */
    protected String getUPnPStateVariableName(Method method, int parameterIndex)
    {
        String upnpStateVariableName="";
        if(method.getParameterAnnotations()[parameterIndex].length!=0)
        {
            for(Annotation paramAnnotation : method.getParameterAnnotations()[parameterIndex])
            {
                if(paramAnnotation instanceof UPnPVariable)
                {
                    upnpStateVariableName=  ((UPnPVariable)paramAnnotation).value();
                    break;
                }
            }
        }
        if(upnpStateVariableName.equals(""))
        {
            upnpStateVariableName=  this.getUPnPActionName(method)+"Param"+parameterIndex;
        }
        return ReflectionUtil.firstLetterToUpperCase(upnpStateVariableName);
    }
    
    /**Getters and setters*/
    
    protected boolean isServiceDefined()
    {
        return (this.upnpServiceId!=null && !this.upnpServiceId.equals("")) || (this.upnpServiceType != null && !this.upnpServiceType.equals(""));
    }
    
    public String getUpnpServiceId()
    {
        return upnpServiceId;
    }

    public void setUpnpServiceId(String upnpServiceId)
    {
        this.upnpServiceId = upnpServiceId;
    }

    public String getUpnpServiceType()
    {
        return upnpServiceType;
    }

    public void setUpnpServiceType(String upnpServiceType)
    {
        this.upnpServiceType = upnpServiceType;
    }
}
