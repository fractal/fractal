/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.fractal.upnp.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;
import java.util.logging.Logger;

import org.ow2.fractal.upnp.util.ApacheUpnpServiceConfiguration;
import org.teleal.cling.UpnpServiceImpl;
import org.teleal.cling.model.message.header.STAllHeader;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.model.types.ServiceType;
import org.teleal.cling.model.types.UDAServiceType;
import org.teleal.cling.model.types.UDN;

/**
 * Helper class to find UPnP device on the network
 * To save resources this class is a Singleton shared by UPnP clients
 */
/**
 *
 */
public class CommonClientUPnPService extends UpnpServiceImpl
{
    private final static Logger logger = Logger.getLogger(CommonClientUPnPService.class.getName());
    
    private static final long SEARCH_TIMEOUT = 60000L;
    private static final long SEARCH_SLEEPTIME = 1000L;
    
    private static CommonClientUPnPService instance;
    
    private CommonClientUPnPService()
    {
        super(new ApacheUpnpServiceConfiguration());
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                shutdown();
            }
        });
    }
    
    public static CommonClientUPnPService getInstance()
    {
        if(instance==null)
        {
            instance = new CommonClientUPnPService();
        }
        return instance;
    }
    
    
    /**
     * It's seems that this.getRegistry().getDevices(new UDAServiceType(serviceTypeString))
     * doesn't work each time
     * TODO fix it
     * 
     * @param serviceType
     * @return
     */
    @SuppressWarnings("rawtypes")
    private Collection<Device> getDevicesFromRegistry(String serviceTypeString)
    {
        logger.fine("getDevicesFromRegistry serviceTypeString "+serviceTypeString);
        Collection<Device> devices = new ArrayList<Device>();
        for(Device device : this.getRegistry().getDevices())
        {
            for(Service service : device.getServices())
            {
                if(serviceTypeString.equals(service.getServiceType().getType()))
                {
                    devices.add(device);
                    break;
                }
            }
        }
        logger.fine("getDevicesFromRegistry "+devices.size()+" device(s) found");
        return devices;
    }
    
    @SuppressWarnings("rawtypes")
    public Collection<Device> getUPnPDevices(String serviceTypeString)
    {
        long startTime = System.currentTimeMillis();
        Collection<Device> devices = this.getDevicesFromRegistry(serviceTypeString);
        if(devices.size()!=0)
        {
            logger.info("UPnP "+devices.size()+" "+serviceTypeString+" services,  "+(System.currentTimeMillis()-startTime)+" ms to be found");
            return devices;
        }
        
        this.getControlPoint().search(new STAllHeader());
        Long startScanningTime= System.currentTimeMillis();
        while(devices.size() == 0 && (System.currentTimeMillis()-startScanningTime)<SEARCH_TIMEOUT)
          {
              try
              {
                  Thread.sleep(SEARCH_SLEEPTIME);
              }catch (InterruptedException e)
              {
                  // should not append
                  e.printStackTrace();
              }
              devices = this.getDevicesFromRegistry(serviceTypeString);
          }
        logger.info("UPnP "+devices.size()+" "+serviceTypeString+" services,  "+(System.currentTimeMillis()-startTime)+" ms to be found");
         return devices;
    }
    
    @SuppressWarnings("rawtypes")
    public Service getUPnPService(String upnpDeviceId, String upnpDeviceType, String upnpServiceId, String upnpServiceType)
    {
        if (upnpServiceType == null || upnpServiceType.equals(""))
        {
            logger.severe("CommonClientUPnPService.getUPnPService service type must de defined");
            return null;
        }

        ServiceType serviceType = new UDAServiceType(upnpServiceType);
        Collection<Device> devices = this.getRegistry().getDevices(serviceType);
        Service service = this.getService(devices, upnpDeviceId, upnpDeviceType, upnpServiceId, upnpServiceType);
        // If no available service found launch a new search
        if (service == null)
        {
            this.getControlPoint().search(new STAllHeader());
            Long startScanningTime = System.currentTimeMillis();
            while (devices.size() == 0 && (System.currentTimeMillis() - startScanningTime) < SEARCH_TIMEOUT)
            {
                try
                {
                    Thread.sleep(SEARCH_SLEEPTIME);
                } catch (InterruptedException e)
                {
                    // should not append
                    e.printStackTrace();
                }
                devices = this.getRegistry().getDevices(serviceType);
            }
            return this.getService(devices, upnpDeviceId, upnpDeviceType, upnpServiceId, upnpServiceType);
        }
        return null;
    }
    
    @SuppressWarnings("rawtypes")
    private Service getService(Collection<Device> upnpDevices, String upnpDeviceId, String upnpDeviceType, String upnpServiceId, String upnpServiceType)
    {
        Service service = null;
        for(Device device : upnpDevices)
        {
            service = this.getService(device, upnpDeviceId, upnpDeviceType, upnpServiceId, upnpServiceType);
            if(service!=null)
            {
                return service;
            }
        }
        return null;
    }
    
    @SuppressWarnings("rawtypes")
    private Service getService(Device upnpDevice, String upnpDeviceId, String upnpDeviceType, String upnpServiceId, String upnpServiceType)
    {
        boolean isUPnPDeviceIDDefined = upnpDeviceId!=null && !upnpDeviceId.equals("");
        boolean isUPnPDeviceTypeDefined = upnpDeviceType!=null && !upnpDeviceType.equals("");
        boolean isUPnPServiceIDDefined = upnpServiceId!=null && !upnpServiceId.equals("");
        boolean isUPnPServiceTypeDefined = upnpServiceType!=null && !upnpServiceType.equals("");
        
        if(isUPnPDeviceIDDefined)
        {
            UDN deviceUDN;
            try
            {
                UUID uuid = UUID.fromString(upnpDeviceId);
                deviceUDN=new UDN(uuid);
            }
            catch(Exception e)
            {
                deviceUDN = UDN.valueOf(upnpDeviceId);
            }
            if(!upnpDevice.getIdentity().getUdn().equals(deviceUDN))
            {
                return null;
            }
        }
        
        //if upnpDeviceType is defined and not equals to upnpDevicd type
        if(isUPnPDeviceTypeDefined && !upnpDeviceType.equals(upnpDevice.getType().getType()))
        {
            return null;
        }
        
        
        for(Service upnpDeviceService : upnpDevice.getServices())
        {
            if(isUPnPServiceTypeDefined && !upnpServiceType.equals(upnpDeviceService.getServiceType().getType()))
            {
                continue;
            }
            if(isUPnPServiceIDDefined && !upnpServiceId.equals(upnpDeviceService.getServiceId().getId()))
            {
                continue;
            }
            
            return upnpDeviceService;
        }
        return null;
    }
}
