/**
 * Copyright (C) 2013 Inria
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: gwenael.cattez@inria.fr
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */
package org.ow2.fractal.upnp.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Reflection functions.
 * 
 * @author Gwenael Cattez
 * @since 1.6
 */
public class ReflectionUtil
{
    /**
     * Get all fields declared in a given class and all its super classes.
     * 
     * @param clazz The given class.
     * @return All fields declared in the given class and all its super classes.
     */
    public static List<Field> getAllFields(Class<?> clazz)
    {
        List<Field> fields = new ArrayList<Field>();
        for (Class<?> c = clazz; c != null; c = c.getSuperclass())
        {
            fields.addAll(Arrays.asList(c.getDeclaredFields()));
        }
        return fields;
    }
       
    /**
     * Invoke static method valueOf on clazz to convert value
     * 
     * @param clazz the class 
     * @param value the value to convert
     * @return value is value.class= clazz, clazz.valueOf(value) if method found, null otherwise
     */
    public static Object valueOf(Class<?> clazz, Object value)
    {
        if (clazz == null || value == null)
        {
            return null;
        }

        if (clazz.isInstance(value))
        {
            return value;
        }

        try
        {
            Method valueOfMethod = clazz.getDeclaredMethod("valueOf", value.getClass());
            return valueOfMethod.invoke(null, value);
        } catch (Exception exception)
        {
            return null;
        }
    }
    
    public static String generateMethodDeclaration(Method method, int modifier)
    {
        StringBuilder methodDeclarationBuilder=new StringBuilder();
        methodDeclarationBuilder.append(Modifier.toString(modifier));
        methodDeclarationBuilder.append(" ");
        methodDeclarationBuilder.append(method.getReturnType().getName());
        methodDeclarationBuilder.append(" ");
        methodDeclarationBuilder.append(method.getName());
        methodDeclarationBuilder.append("(");
        
        Class<?>[] parameterTypes=method.getParameterTypes();
        for(int i=0; i<parameterTypes.length;i++)
        {
            if(i!=0)
            {
                methodDeclarationBuilder.append(", ");
            }
            methodDeclarationBuilder.append(parameterTypes[i].getName()+" arg"+i);
        }
        methodDeclarationBuilder.append(")");
        
        Class<?>[] methodExceptions = method.getExceptionTypes();
        if (methodExceptions.length != 0)
        {
            methodDeclarationBuilder.append(" throws " + methodExceptions[0].getName());
            for (int i = 1; i < methodExceptions.length; i++)
            {
                methodDeclarationBuilder.append(methodExceptions[i].getName());
            }
        }
        
        return methodDeclarationBuilder.toString();
    }
    
    public static String generateMethodDeclaration(Method method)
    {
        return ReflectionUtil.generateMethodDeclaration(method, Modifier.PUBLIC);
    }

    private static final Map<String, Class<?>> primitiveTypesMap = new HashMap<String, Class<?>>();
    static
    {
        primitiveTypesMap.put("byte", Byte.class );
        primitiveTypesMap.put("short", Short.class );
        primitiveTypesMap.put("int", Integer.class );
        primitiveTypesMap.put("long", Long.class );
        primitiveTypesMap.put("float", Float.class );
        primitiveTypesMap.put("double", Double.class );
        primitiveTypesMap.put("boolean", Boolean.class );
        primitiveTypesMap.put("char", Character.class );
        primitiveTypesMap.put("void", Void.class );
    }

    /**
     * Get the consistent class for a given class
     * ie if the class is a primitive class return its Object class
     * 
     */
    public static Class<?> getConsistentClass(Class<?> clazz)
    {
        if(clazz.isPrimitive())
        {
            return primitiveTypesMap.get(clazz.getName());
        }
        return clazz;
    }
    
    public static String firstLetterToUpperCase(String s)
    {
        return s.replaceFirst(".", String.valueOf(Character.toUpperCase(s.charAt(0))));
    }
}
