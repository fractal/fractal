/**
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: gwenael.cattez@inria.fr
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.fractal.upnp.util;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import javax.tools.JavaCompiler;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.SimpleJavaFileObject;
import javax.tools.ToolProvider;

/**
 *
 */
public class JavaCompilerUtil
{
    public static String generationPackage="org.ow2.upnp.generated";
    
    /** where shall the compiled class be saved to (should exist already) */
    private static File classOutputDirectory;
    static
    {
        classOutputDirectory=new File("target/generated-sources");
        classOutputDirectory.mkdirs();
    }
    
    public static ClassLoader getCurrentClassLoader()
    {
        ClassLoader classloader;
        try
        {
            URL classOutputDirectoryUrl = classOutputDirectory.toURI().toURL();
            classloader=URLClassLoader.newInstance(new URL[]{classOutputDirectoryUrl}, Thread.currentThread().getContextClassLoader());
        }
        catch (MalformedURLException e)
        {
           classloader=Thread.currentThread().getContextClassLoader();
        }
        return classloader;
    }
    
    @SuppressWarnings("unchecked")
    public static <T> T getInstance(Class<T> instanceClass, String classSource) throws ClassNotFoundException, InstantiationException, IllegalAccessException
    {
      String generatedInstanceClassSimpleName = JavaCompilerUtil.generateClassName(instanceClass);
      String generatedInstanceClassQualifiedName = JavaCompilerUtil.generationPackage + "." + generatedInstanceClassSimpleName;
      JavaCompilerUtil.compile(generatedInstanceClassQualifiedName, classSource);
      Class<?> upnpClientClass = Class.forName(generatedInstanceClassQualifiedName, true, JavaCompilerUtil.getCurrentClassLoader());
      return (T)upnpClientClass.newInstance();
    }
    
    @SuppressWarnings("restriction")
    public static boolean compile(String qualifiedClassName, String source)
    {
        JavaCompiler javaCompiler = ToolProvider.getSystemJavaCompiler();
        JavaFileManager javaFileManager = javaCompiler.getStandardFileManager(null, null, null);
        JavaFileObject upnpClientFileObject = new DynamicJavaSourceCodeObject(qualifiedClassName, source);
        List<JavaFileObject> compilationUnits = Arrays.asList(upnpClientFileObject);

        ArrayList<String> options=new ArrayList<String>();
        options.add("-d");
        options.add(classOutputDirectory.getPath());
        options.add("-classpath");
        options.add(JavaCompilerUtil.getCurrentThreadClassPath());
        
        JavaCompiler.CompilationTask compilationTask = javaCompiler.getTask(null, javaFileManager, null, options, null, compilationUnits);
        return compilationTask.call();
    }
    
    private static String getCurrentThreadClassPath()
    {
        String pathSeparator=System.getProperty("path.separator");
        StringBuilder classPathStringBuilder=new StringBuilder();
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        while(classLoader != null) {
          if(classLoader instanceof URLClassLoader) {
            URLClassLoader urlClassLoader = (URLClassLoader) classLoader;
            URL[] urls=urlClassLoader.getURLs();
            for(int i=0;i<urls.length; i++) {
              classPathStringBuilder.append(urls[i].getFile().replaceAll("%20", " "));
              classPathStringBuilder.append(pathSeparator);
            }
          }
          classLoader = classLoader.getParent();
        }

        Logger.getAnonymousLogger().fine("\n\ngetCurrentThreadClassPath = " + classPathStringBuilder.toString() + "\n\n");
        return classPathStringBuilder.toString();
    }
    
    /** Internal methods */
    public static String generateClassName(Class<?> baseClassName)
    {
        String upnpDeviceClassName=baseClassName.getSimpleName();
        String noWhiteSpaceString=upnpDeviceClassName.replaceAll("\\s", "");
        Random random = new Random();
        return ReflectionUtil.firstLetterToUpperCase(noWhiteSpaceString) + random.nextInt(Integer.MAX_VALUE);
    }
}
    
@SuppressWarnings("restriction")
class DynamicJavaSourceCodeObject extends SimpleJavaFileObject
{
    private String qualifiedName;
    private String sourceCode;

    
    protected DynamicJavaSourceCodeObject(String name, String code)
    {
        super(URI.create("string:///" + name.replaceAll("\\.", "/") + JavaFileObject.Kind.SOURCE.extension), JavaFileObject.Kind.SOURCE);
        this.qualifiedName = name;
        this.sourceCode = code;
    }

    @Override
    public CharSequence getCharContent(boolean ignoreEncodingErrors) throws IOException
    {
        return sourceCode;
    }
}