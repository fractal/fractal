/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.fractal.upnp.device;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Logger;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.ow2.fractal.upnp.exception.UPnPException;
import org.teleal.cling.model.ValidationException;
import org.teleal.cling.model.meta.DeviceDetails;
import org.teleal.cling.model.meta.DeviceIdentity;
import org.teleal.cling.model.meta.Icon;
import org.teleal.cling.model.meta.LocalDevice;
import org.teleal.cling.model.meta.LocalService;
import org.teleal.cling.model.meta.ManufacturerDetails;
import org.teleal.cling.model.meta.ModelDetails;
import org.teleal.cling.model.types.DeviceType;
import org.teleal.cling.model.types.UDADeviceType;
import org.teleal.cling.model.types.UDN;

/**
 *
 */
public class UPnPDeviceFactory
{
    private final static Logger logger = Logger.getLogger(UPnPDeviceFactory.class.getName());
    
    /**Default Icon*/
    private final static String DEFAULT_ICON_FILENAME="default_upnp_device_icon.png";
    
    /**UPnP Device id, string representation of UDN*/
    private String upnpDeviceId;
    
    /**UPnP Device type*/
    private String upnpDeviceType = "Fractal-UPnP-server";
    
    /**UPnP Device version*/
    private int upnpDeviceVersion = 1;
    
    /**UPnP Device icon*/
    private Icon upnpDeviceIcon = this.getIcon(DEFAULT_ICON_FILENAME);
    
    /**UPnP Device details*/
    /**UPnP Device friendly name*/
    private String upnpDeviceFriendlyName = "Fractal UPnP server";
    
    /**UPnP Device manufacturer details*/
    private String upnpDeviceManufacturerDetails = "Fractal";
    
    /**UPnP Device model details*/
    private String upnpDeviceModelDetails = "Fractal-UPnP-server";
    
    private List<UPnPDeviceService> upnpServices = new ArrayList<UPnPDeviceService>();
    
    
    /**UPnP cling device*/
    private LocalDevice upnpLocalDevice;
    
    /**Business methods*/
    public void addService(UPnPDeviceService upnpDeviceService)
    {
        this.upnpServices.add(upnpDeviceService);
    }
    
    public boolean removeService(String serviceId)
    {
        //TODO
        return this.upnpServices.remove(serviceId);
    }
    
    @SuppressWarnings("rawtypes")
    public LocalDevice generateUPnPDevice() throws UPnPException
    {
        if(this.upnpLocalDevice!=null)
        {
            return this.upnpLocalDevice;
        }
        
        if(this.upnpServices.isEmpty())
        {
            //Can not create a device with no service
            return null;
        }
        
        LocalService<?>[] upnpLocalServices=new LocalService<?>[this.upnpServices.size()];
        UPnPDeviceService upnpDeviceService;
        UPnPGeneratedDeviceService upnpGeneratedService;
        for(int i=0; i<this.upnpServices.size(); i++)
        {
            upnpDeviceService=this.upnpServices.get(i);
            upnpGeneratedService = upnpDeviceService.generateUPnPDeviceService();
            upnpLocalServices[i]=upnpGeneratedService.getLocalService();
        }
        
        UDN upnpLocalDeviceUDN;
        if(this.getUpnpDeviceId()==null || this.getUpnpDeviceId().equals(""))
        {
            //If no device id is defined generate a random one
            UUID randomUUID= UUID.randomUUID();
            upnpLocalDeviceUDN = new UDN(randomUUID);
        }
        else
        {
            upnpLocalDeviceUDN = UDN.valueOf(this.getUpnpDeviceId());
        }

        DeviceIdentity upnpDeviceIdentity = new DeviceIdentity(upnpLocalDeviceUDN);
        DeviceType upnpLocalDeviceType = new UDADeviceType(this.getUpnpDeviceType(), this.getUpnpDeviceVersion());
        DeviceDetails upnpLocalDeviceDetails = new DeviceDetails( this.getUpnpDeviceFriendlyName(), new ManufacturerDetails(this.getUpnpDeviceManufacturerDetails()), new ModelDetails(this.getUpnpDeviceModelDetails()));
        try
        {
            this.upnpLocalDevice = new LocalDevice(upnpDeviceIdentity, upnpLocalDeviceType, upnpLocalDeviceDetails, this.getUpnpDeviceIcon(), upnpLocalServices);
            return this.upnpLocalDevice;
        }
        catch (ValidationException validationException)
        {
            throw new UPnPException(validationException);
        }
    }
    
    /**Getters and setters*/
    
    public String getUpnpDeviceType()
    {
        return upnpDeviceType;
    }

    public String getUpnpDeviceId()
    {
        return upnpDeviceId;
    }

    public void setUpnpDeviceId(String upnpDeviceId)
    {
        this.upnpDeviceId = upnpDeviceId;
    }

    public void setUpnpDeviceType(String upnpDeviceType) throws UPnPException
    {
        if(upnpDeviceType.contains(" ") || upnpDeviceType.contains("."))
        {
            throw new UPnPException("UPnP device type can not contains space or dot");
        }
        this.upnpDeviceType = upnpDeviceType;
    }

    public int getUpnpDeviceVersion()
    {
        return upnpDeviceVersion;
    }

    public void setUpnpDeviceVersion(int upnpDeviceVersion)
    {
        this.upnpDeviceVersion = upnpDeviceVersion;
    }

    public Icon getUpnpDeviceIcon()
    {
        return upnpDeviceIcon;
    }

    public void setUpnpDeviceIcon(String upnpDeviceIconFileName)
    {
        this.upnpDeviceIcon = this.getIcon(upnpDeviceIconFileName);
    }

    public String getUpnpDeviceFriendlyName()
    {
        return upnpDeviceFriendlyName;
    }

    public void setUpnpDeviceFriendlyName(String upnpDeviceFriendlyName)
    {
        this.upnpDeviceFriendlyName = upnpDeviceFriendlyName;
    }

    public String getUpnpDeviceManufacturerDetails()
    {
        return upnpDeviceManufacturerDetails;
    }

    public void setUpnpDeviceManufacturerDetails(String upnpDeviceManufacturerDetails)  throws UPnPException
    {
        if(upnpDeviceManufacturerDetails.contains(" ") || upnpDeviceManufacturerDetails.contains("."))
        {
            throw new UPnPException("UPnP device manufacturer details can not contains space or dot");
        }
        this.upnpDeviceManufacturerDetails = upnpDeviceManufacturerDetails;
    }

    public String getUpnpDeviceModelDetails()
    {
        return upnpDeviceModelDetails;
    }

    public void setUpnpDeviceModelDetails(String upnpDeviceModelDetails)  throws UPnPException
    {
        if(upnpDeviceModelDetails.contains(" ") || upnpDeviceModelDetails.contains("."))
        {
            throw new UPnPException("UPnP device model details can not contains space or dot");
        }
        this.upnpDeviceModelDetails = upnpDeviceModelDetails;
    }
    
    /** Internal methods */
    
    /**
     * @return the File of the default UPnP device icon
     */
    private Icon getIcon(String iconFileName)
    {
        try
        {
            String tempDirPath = System.getProperty("java.io.tmpdir");
            File tempDir = new File(tempDirPath);
            File iconFile = new File(tempDir, iconFileName);
            if (!iconFile.exists())
            {
                InputStream defaultIconInputStream = UPnPDeviceFactory.class.getClassLoader().getResourceAsStream(iconFileName);
                FileOutputStream defaultIconFileOutputStream = new FileOutputStream(iconFile);
                IOUtils.copy(defaultIconInputStream, defaultIconFileOutputStream);
                IOUtils.closeQuietly(defaultIconInputStream);
                IOUtils.closeQuietly(defaultIconInputStream);
            }
            String fileExtension = FilenameUtils.getExtension(iconFileName);
            String mimeType = "image/" + fileExtension;
            return new Icon(mimeType, 48, 48, 8, iconFile);
        } catch (Exception e)
        {
            e.printStackTrace();
            logger.severe("Can't create Icon from "+iconFileName+" file, use the default one");
            return this.getIcon(DEFAULT_ICON_FILENAME);
        }
    }
}


