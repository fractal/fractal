/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.fractal.upnp.device;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.ow2.fractal.upnp.exception.UPnPException;
import org.ow2.fractal.upnp.service.AbstractUPnPService;
import org.ow2.fractal.upnp.util.JavaCompilerUtil;
import org.ow2.fractal.upnp.util.ReflectionUtil;
import org.teleal.cling.binding.annotations.AnnotationLocalServiceBinder;
import org.teleal.cling.binding.annotations.UpnpAction;
import org.teleal.cling.binding.annotations.UpnpInputArgument;
import org.teleal.cling.binding.annotations.UpnpOutputArgument;
import org.teleal.cling.binding.annotations.UpnpService;
import org.teleal.cling.binding.annotations.UpnpServiceId;
import org.teleal.cling.binding.annotations.UpnpServiceType;
import org.teleal.cling.binding.annotations.UpnpStateVariable;
import org.teleal.cling.model.DefaultServiceManager;
import org.teleal.cling.model.meta.LocalService;

/**
 *
 */
public class UPnPDeviceService extends AbstractUPnPService
{
    /**
     * UPnP Service provider
     */
    private Object upnpServiceProvider;

    @SuppressWarnings("rawtypes")
    private UPnPGeneratedDeviceService upnpGeneratedDeviceService;
    
    @SuppressWarnings("rawtypes")
    public UPnPGeneratedDeviceService generateUPnPDeviceService() throws UPnPException
    {

        this.upnpGeneratedDeviceService = (UPnPGeneratedDeviceService) this.generateServiceInstance();
        this.upnpGeneratedDeviceService.setServiceProvider(upnpServiceProvider);
        return this.upnpGeneratedDeviceService;
    }
    
    /**
     * @throws UPnPException 
     * @see org.ow2.fractal.upnp.service.AbstractUPnPService#validateServiceParams()
     */
    @Override
    protected void validateServiceParams() throws UPnPException
    {
        if(this.upnpServiceClass==null)
        {
            throw new UPnPException(this.getClass().getName()+" validation failed : upnpServiceClass must be defined");
        }
        
        if(this.upnpServiceProvider==null)
        {
            throw new UPnPException(this.getClass().getName()+" validation failed : upnpServiceProvider must be defined");
        }
        
        if(!upnpServiceClass.isInstance(upnpServiceProvider))
        {
            throw new UPnPException(this.getClass().getName()+" validation failed : upnpServiceProvider class "+upnpServiceProvider.getClass().getName()+" does not implements upnpServiceClass "+this.upnpServiceClass.getName());
        }
        
        /**validateServiceParams from AbstractUPnPService validate upnpServiceType field*/
        super.validateServiceParams();

        /**if upnpServiceId is undefined use upnpServiceClass name as Type*/
        if(this.upnpServiceId==null ||this.upnpServiceId.equals(""))
        {
            this.upnpServiceId=this.upnpServiceClass.getSimpleName();
        }
            
    }
    
    private void addFieldDeclaration(String fieldName,String fieldType, Map<String,String> fields)
    {
        if(!fields.containsKey(fieldName))
        {
            fields.put(fieldName, fieldType);
        }
        else
        {
            if(!fields.get(fieldName).equals(fieldType))
            {
                throw new RuntimeException("Multiple field declaration of different types found");
            }
        }
    }
    
    /** Code generation methods */

    /**
     * @see org.ow2.fractal.upnp.service.AbstractUPnPService#generateSource(java.lang.String)
     */
    @Override
    protected String generateSource(String generatedUPnPServiceClassSimpleName)
    {
        StringBuilder upnpServerSourceBuilder = new StringBuilder();
        
        /** Package and class declaration */
        upnpServerSourceBuilder.append("package " + JavaCompilerUtil.generationPackage + ";\n");
        upnpServerSourceBuilder.append("@" + UpnpService.class.getName() + "(");
        upnpServerSourceBuilder.append("serviceId = @" + UpnpServiceId.class.getName() + "(\"" + this.upnpServiceId + "\")");
        upnpServerSourceBuilder.append(", serviceType = @" + UpnpServiceType.class.getName() + "(value = \"" + this.upnpServiceType + "\")");
        upnpServerSourceBuilder.append(")\n");
        upnpServerSourceBuilder.append("public class " + generatedUPnPServiceClassSimpleName + "\n");
        upnpServerSourceBuilder.append("extends "+UPnPGeneratedDeviceService.class.getName()+"<"+this.upnpServiceClass.getName()+"> implements ");
        upnpServerSourceBuilder.append(this.upnpServiceClass.getName() + "\n");
        upnpServerSourceBuilder.append("{\n");
        
        Map<String,String> fields = new HashMap<String, String>();
        StringBuilder methodsBuilder= new StringBuilder();
        for (Method method : this.upnpServiceClass.getMethods())
        {
            this.proceedMethodReturnType(method, methodsBuilder, fields);
            this.proceedMehtodDeclaration(method, methodsBuilder, fields);
            methodsBuilder.append("\t{\n\t");
            this.proceedMethodContent(method, methodsBuilder);    
            methodsBuilder.append("\t}\n\n");
        }

        for(Entry<String,String> field : fields.entrySet())
        {
            upnpServerSourceBuilder.append("\t@"+UpnpStateVariable.class.getName()+"(defaultValue = \"0\")\n");
            upnpServerSourceBuilder.append("\tprivate " + field.getValue() + " " + field.getKey() + ";\n\n");
        }
        
        upnpServerSourceBuilder.append("\n");
        upnpServerSourceBuilder.append(methodsBuilder.toString());
        
        /**generate getLocalService*/
        upnpServerSourceBuilder.append("\t@SuppressWarnings({ \"rawtypes\",\"unchecked\" })\n");
        upnpServerSourceBuilder.append("\tpublic "+LocalService.class.getName()+"<"+generatedUPnPServiceClassSimpleName+"> getLocalService()\n\t{\n");
        upnpServerSourceBuilder.append("\t\t"+LocalService.class.getName()+"<"+generatedUPnPServiceClassSimpleName+"> localService");
        upnpServerSourceBuilder.append("= new "+AnnotationLocalServiceBinder.class.getName()+"().read("+generatedUPnPServiceClassSimpleName+".class);\n");
        upnpServerSourceBuilder.append("\t\tlocalService.setManager(new "+DefaultServiceManager.class.getName()+"(localService, "+generatedUPnPServiceClassSimpleName+".class));\n");
        upnpServerSourceBuilder.append("\t\ttry{\n\t\t\tlocalService.getManager().getImplementation().setServiceProvider(this.serviceProvider);\n");
        upnpServerSourceBuilder.append("\t\t}catch("+UPnPException.class.getName()+" upnpException)\n\t\t{\n");
        upnpServerSourceBuilder.append("\t\t\t//this should not happen\n\t\t}\n");
        upnpServerSourceBuilder.append("\t\treturn localService;\n");
        upnpServerSourceBuilder.append("\t}\n\n");
        
        upnpServerSourceBuilder.append("}\n");
        
        return upnpServerSourceBuilder.toString();
    }
    
    private  void proceedMethodReturnType(Method method, StringBuilder methodBuilder, Map<String,String> fields)
    {
        Class<?> returnTypeClass = method.getReturnType();
        String upnpActionName=this.getUPnPActionName(method);
        methodBuilder.append("\t@"+UpnpAction.class.getName());
        methodBuilder.append("(name=\""+upnpActionName+"\"");
        
        if (returnTypeClass.equals(void.class) || returnTypeClass.equals(Void.class))
        {
            //Nothing to do
        }
        else if(returnTypeClass.isPrimitive() || returnTypeClass.equals(String.class))
        {
            String upnpActionReturnStateVariableName=ReflectionUtil.firstLetterToUpperCase(upnpActionName)+"Return";
            methodBuilder.append(",out = @"+UpnpOutputArgument.class.getName()+"(name=\""+upnpActionReturnStateVariableName+"\")");
            this.addFieldDeclaration(upnpActionReturnStateVariableName, returnTypeClass.getName(), fields);
        }
        else
        {
            methodBuilder.append(",out = \n\t{\n");
            List<Field> returnTypefields = ReflectionUtil.getAllFields(returnTypeClass);
            Class<?> returnTypeConsistentFieldClass;
            Field returnTypeField;
            String upnpStateVariableName, returnTypeFieldName;
            for (int i=0; i<returnTypefields.size(); i++)
            {
                returnTypeField=returnTypefields.get(i);
                returnTypeFieldName = ReflectionUtil.firstLetterToUpperCase(returnTypeField.getName());
                upnpStateVariableName = ReflectionUtil.firstLetterToUpperCase(method.getName())+ReflectionUtil.firstLetterToUpperCase(returnTypeField.getName());
                /**UPnP state variable name can't be more than 32 characters*/
                if(upnpStateVariableName.length()>32)
                {
                    /**we keep the last 32 characters because all stateVariable start with the same method name*/
                    upnpStateVariableName=upnpStateVariableName.substring(upnpStateVariableName.length()-31, upnpStateVariableName.length());
                    /**UPnP state variable must start with upper case character*/
                    upnpStateVariableName=ReflectionUtil.firstLetterToUpperCase(upnpStateVariableName);
                }
                returnTypeConsistentFieldClass=ReflectionUtil.getConsistentClass(returnTypeField.getType());
                methodBuilder.append("\t\t");
                if(i!=0)
                {
                    methodBuilder.append(",");
                }
                methodBuilder.append("@"+UpnpOutputArgument.class.getName()+"(name=\"" +upnpStateVariableName+ "\", getterName=\"get" + returnTypeFieldName + "\")\n");
                this.addFieldDeclaration(upnpStateVariableName, returnTypeConsistentFieldClass.getName(), fields);
            }
            methodBuilder.append("\t}");
        }
        methodBuilder.append(")\n");
    }
    
    private  void proceedMehtodDeclaration(Method method, StringBuilder methodBuilder, Map<String,String> fields)
    {
        methodBuilder.append("\tpublic " + method.getReturnType().getName() + " " + method.getName() + "(");
        
        Class<?>[] parametersType = method.getParameterTypes();
        String upnpStateVariableName;
        for (int parameterIndex = 0; parameterIndex < parametersType.length; parameterIndex++)
        {
            if (parameterIndex != 0)
            {
                methodBuilder.append(", ");
            }
            
            upnpStateVariableName = this.getUPnPStateVariableName(method, parameterIndex);
            methodBuilder.append("@"+UpnpInputArgument.class.getName()+"(name=\""+upnpStateVariableName+"\") "+parametersType[parameterIndex].getName() + " "+ upnpStateVariableName);
            this.addFieldDeclaration(upnpStateVariableName, parametersType[parameterIndex].getName(), fields);
        }
        methodBuilder.append(")");
        
        Class<?>[] exceptions = method.getExceptionTypes();
        if(exceptions.length!=0)
        {
            methodBuilder.append(" throws "+exceptions[0].getName());
            for (int i = 1; i < exceptions.length; i++)
            {
                methodBuilder.append(", "+ exceptions[i].getName());
            }
        }
        methodBuilder.append("\n");
    }
    
    private  void proceedMethodContent(Method method, StringBuilder methodBuilder)
    {
        Class<?> returnTypeClass = method.getReturnType();
        methodBuilder.append("\t\t");
        if (!returnTypeClass.equals(void.class) && !returnTypeClass.equals(Void.class))
        {
            methodBuilder.append("return ");
        }
        methodBuilder.append("this.serviceProvider."+method.getName()+"(");
        Class<?>[] parametersType = method.getParameterTypes();
        String upnpStateVariableName;
        for (int parameterIndex = 0; parameterIndex < parametersType.length; parameterIndex++)
        {
            if (parameterIndex != 0)
            {
                methodBuilder.append(", ");
            }
            upnpStateVariableName = this.getUPnPStateVariableName(method, parameterIndex);
            methodBuilder.append(upnpStateVariableName);
        }
        methodBuilder.append(");\n");
    }
    
    /**Getters and setters*/
    public Class<?> getUpnpServiceClass()
    {
        return upnpServiceClass;
    }

    public void setUpnpServiceClass(Class<?> upnpServiceClass)
    {
        this.upnpServiceClass = upnpServiceClass;
    }

    public Object getUpnpServiceProvider()
    {
        return upnpServiceProvider;
    }

    public void setUpnpServiceProvider(Object upnpServiceProvider)
    {
        this.upnpServiceProvider = upnpServiceProvider;
    }
}
