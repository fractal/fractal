package org.ow2.fractal.upnp.test.example.philipsNP3500;


public class PhilipsNP3500Server implements UPnPDeviceRenderingControlItf, UPnPDeviceConnectionManagerItf, UPnPDeviceAVTransportItf
{
    private boolean mute;
    
    private int volume;
    
    public PhilipsNP3500Server()
    {
        this.mute=false;
        this.volume = 0;
    }
    

    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceRenderingControlItf#listPresets(java.lang.String)
     */
    public ListPresets listPresets(String InstanceID)
    {
        return new ListPresets();
    }

    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceRenderingControlItf#setMute(java.lang.String, java.lang.String, java.lang.String)
     */
    public void setMute(String InstanceID, String Channel, String DesiredMute)
    {
        this.mute = Boolean.valueOf(DesiredMute);
        
    }

    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceRenderingControlItf#setVolume(java.lang.String, java.lang.String, java.lang.String)
     */
    public void setVolume(String InstanceID, String Channel, String DesiredVolume)
    {
        this.volume = Integer.valueOf(DesiredVolume);
    }

    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceRenderingControlItf#getVolume(java.lang.String, java.lang.String)
     */
    public int getVolume(String InstanceID, String Channel)
    {
       return this.volume;
    }

    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceRenderingControlItf#getMute(java.lang.String, java.lang.String)
     */
    public boolean getMute(String InstanceID, String Channel)
    {
       return this.mute;
    }

    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceRenderingControlItf#selectPresets(java.lang.String, java.lang.String)
     */
    public void selectPresets(String InstanceID, String PresetName)
    {
        System.out.println("selectPresets "+PresetName);
    }


    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceAVTransportItf#seek(java.lang.String, java.lang.String, java.lang.String)
     */
    public void seek(String InstanceID, String Unit, String Target)
    {
        System.out.println("seek InstanceID : "+InstanceID+", Unit : "+Unit+", Target : "+Target);
    }


    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceAVTransportItf#next(java.lang.String)
     */
    public void next(String InstanceID)
    {
        System.out.println("next InstanceID: "+InstanceID+" ");
    }


    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceAVTransportItf#play(java.lang.String, java.lang.String)
     */
    public void play(String InstanceID, String Speed)
    {
        System.out.println("play InstanceID : "+InstanceID+", Speed : "+Speed);
    }


    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceAVTransportItf#getTransportInfo(java.lang.String)
     */
    public GetTransportInfo getTransportInfo(String InstanceID)
    {
        System.out.println("getTransportInfo InstanceID : "+InstanceID);
        return new GetTransportInfo();
    }


    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceAVTransportItf#previous(java.lang.String)
     */
    public void previous(String InstanceID)
    {
        System.out.println("previous : "+InstanceID+" ");
    }


    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceAVTransportItf#getMediaInfo(java.lang.String)
     */
    public GetMediaInfo getMediaInfo(String InstanceID)
    {
        System.out.println("getMediaInfo : "+InstanceID+" ");
        return new GetMediaInfo();
    }


    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceAVTransportItf#getDeviceCapabilities(java.lang.String)
     */
    public GetDeviceCapabilities getDeviceCapabilities(String InstanceID)
    {
        System.out.println("getDeviceCapabilities : "+InstanceID+" ");
        return new GetDeviceCapabilities();
    }


    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceAVTransportItf#pause(java.lang.String)
     */
    public void pause(String InstanceID)
    {
        System.out.println("pause  InstanceID: "+InstanceID+" ");
    }


    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceAVTransportItf#getCurrentTransportActions(java.lang.String)
     */
    public String getCurrentTransportActions(String InstanceID)
    {
        System.out.println("pause  InstanceID: "+InstanceID);
        return "CurrentTransportActions";
    }


    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceAVTransportItf#setAVTransportURI(java.lang.String, java.lang.String, java.lang.String)
     */
    public void setAVTransportURI(String InstanceID, String CurrentURI, String CurrentURIMetaData)
    {
        System.out.println("getPositionInfo InstanceID : "+InstanceID+", CurrentURI : "+CurrentURI+", CurrentURIMetaData : "+CurrentURIMetaData);
    }


    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceAVTransportItf#getPositionInfo(java.lang.String)
     */
    public GetPositionInfo getPositionInfo(String InstanceID)
    {
        System.out.println("getPositionInfo InstanceID : "+InstanceID);
        return new GetPositionInfo();
    }


    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceAVTransportItf#getTransportSettings(java.lang.String)
     */
    public GetTransportSettings getTransportSettings(String InstanceID)
    {
        System.out.println("getTransportSettings InstanceID : "+InstanceID);
        return new GetTransportSettings();
    }


    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceAVTransportItf#stop(java.lang.String)
     */
    public int stop(String InstanceID)
    {
        System.out.println("stop InstanceID : "+InstanceID);
        return 0;
    }


    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceConnectionManagerItf#getCurrentConnectionInfo(java.lang.String)
     */
    public GetCurrentConnectionInfo getCurrentConnectionInfo(String ConnectionID)
    {
        System.out.println("getCurrentConnectionInfo ConnectionID : "+ConnectionID);
        return new GetCurrentConnectionInfo();
    }


    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceConnectionManagerItf#getProtocolInfo()
     */
    public GetProtocolInfo getProtocolInfo()
    {
       System.out.println("getProtocolInfo");
       return new GetProtocolInfo();
    }


    /* (non-Javadoc)
     * @see org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceConnectionManagerItf#getCurrentConnectionIDs()
     */
    public String getCurrentConnectionIDs()
    {
        System.out.println("getCurrentConnectionIDs");
       return "CurrentConnectionIDs";
    }
	
}
