package org.ow2.fractal.upnp.test.example.philipsNP3500;

public class GetTransportInfo
{
	protected String currentTransportState;
	protected String currentTransportStatus;
	protected String currentSpeed;
	
	public GetTransportInfo()
	{
		this.currentTransportState="Constructor GetTransportInfo";
		this.currentTransportStatus="Constructor GetTransportInfo";
		this.currentSpeed="Constructor GetTransportInfo";
	}
	
	public String getCurrentTransportState()
	{
		return currentTransportState;
	}

	public String getCurrentTransportStatus()
	{
		return currentTransportStatus;
	}

	public String getCurrentSpeed()
	{
		return currentSpeed;
	}
	
	public void setCurrentTransportState(String currentTransportState)
	{
		this.currentTransportState = currentTransportState;
	}

	public void setCurrentTransportStatus(String currentTransportStatus)
	{
		this.currentTransportStatus = currentTransportStatus;
	}

	public void setCurrentSpeed(String currentSpeed)
	{
		this.currentSpeed = currentSpeed;
	}

	@Override
	public boolean equals(Object otherObject)
	{
	    return otherObject != null && GetTransportInfo.class.isInstance(otherObject) && ((GetTransportInfo)otherObject).getCurrentTransportState().equals(this.getCurrentTransportState()) && ((GetTransportInfo)otherObject).getCurrentTransportStatus().equals(this.getCurrentTransportStatus()) && ((GetTransportInfo)otherObject).getCurrentSpeed().equals(this.getCurrentSpeed());
	}
}
