/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.fractal.upnp.test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.fractal.upnp.client.UPnPServiceClient;
import org.ow2.fractal.upnp.device.UPnPDeviceFactory;
import org.ow2.fractal.upnp.device.UPnPDeviceService;
import org.ow2.fractal.upnp.exception.UPnPException;
import org.ow2.fractal.upnp.server.UPnPServer;
import org.ow2.fractal.upnp.test.example.PrintServer;
import org.ow2.fractal.upnp.test.example.PrintService;
import org.ow2.fractal.upnp.test.example.TInterface;
import org.ow2.fractal.upnp.test.example.TInterface2;
import org.ow2.fractal.upnp.test.example.TServer;
import org.ow2.fractal.upnp.test.example.philipsNP3500.ListPresets;
import org.ow2.fractal.upnp.test.example.philipsNP3500.PhilipsNP3500Server;
import org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceAVTransportItf;
import org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceConnectionManagerItf;
import org.ow2.fractal.upnp.test.example.philipsNP3500.UPnPDeviceRenderingControlItf;
import org.teleal.cling.model.meta.LocalDevice;

/**
 *
 */
public class UPnPDeviceTestCase
{
    private static UPnPServer upnpServer;
    
    @BeforeClass
    public static void startTest()
    {
        upnpServer = new UPnPServer();
        upnpServer.start();
    }
    
    @AfterClass
    public static void finishTest()
    {
        upnpServer.stop();
    }
    
    @Test
    public void basicTest() throws Exception
    {
      TServer upnpServiceProvider = new TServer();
      UPnPDeviceFactory deviceFactory = new UPnPDeviceFactory();
    
      UPnPDeviceService upnpDeviceService = new UPnPDeviceService();
      upnpDeviceService.setUpnpServiceClass(TInterface.class);
      upnpDeviceService.setUpnpServiceProvider(upnpServiceProvider);
      deviceFactory.addService(upnpDeviceService);
      
      upnpDeviceService = new UPnPDeviceService();
      upnpDeviceService.setUpnpServiceClass(TInterface2.class);
      upnpDeviceService.setUpnpServiceProvider(upnpServiceProvider);
      deviceFactory.addService(upnpDeviceService);

      LocalDevice localDevice = deviceFactory.generateUPnPDevice();
      upnpServer.addUPnPDevice(localDevice);
      
      UPnPServiceClient uPnPClientFactory=new UPnPServiceClient();
      TInterface upnpClient=uPnPClientFactory.generateUPnPClient(TInterface.class);

      upnpClient.meth1(82);
      assertEquals(upnpServiceProvider.meth2(7, "+", 8), upnpClient.meth2(7, "+", 8));

      uPnPClientFactory=new UPnPServiceClient();
      TInterface2 upnpClient2=uPnPClientFactory.generateUPnPClient(TInterface2.class);
      assertEquals(upnpServiceProvider.meth3(), upnpClient2.meth3());
      upnpClient2.meth4();
    }
    
    @Test
    public void printServerTest() throws UPnPException
    {
       PrintServer printServer = new PrintServer();
       UPnPDeviceService upnpDeviceService = new UPnPDeviceService();
       upnpDeviceService.setUpnpServiceClass(PrintService.class);
       upnpDeviceService.setUpnpServiceProvider(printServer);
       
       UPnPDeviceFactory deviceFactory = new UPnPDeviceFactory();
       deviceFactory.addService(upnpDeviceService);
       LocalDevice printServerDevice = deviceFactory.generateUPnPDevice();
       upnpServer.addUPnPDevice(printServerDevice);
       
       UPnPServiceClient uPnPClientFactory=new UPnPServiceClient();
       PrintService printServerClient=uPnPClientFactory.generateUPnPClient(PrintService.class);
       printServerClient.print("Hello UPnP world");
    }
    
    @Test
    public void philipsNP3500ServerTest() throws UPnPException, InterruptedException
    {
      UPnPDeviceFactory upnpDeviceFactory = new UPnPDeviceFactory();
      upnpDeviceFactory.setUpnpDeviceId("Fractal");
      upnpDeviceFactory.setUpnpDeviceType("MediaRenderer");
      upnpDeviceFactory.setUpnpDeviceVersion(1);
      upnpDeviceFactory.setUpnpDeviceFriendlyName("Friendly UPnP device name");
      upnpDeviceFactory.setUpnpDeviceManufacturerDetails("Fractal");
      upnpDeviceFactory.setUpnpDeviceModelDetails("Test");
        
      PhilipsNP3500Server philipsNP3500Server = new PhilipsNP3500Server();
      UPnPDeviceService upnpRenderingControlService = new UPnPDeviceService();
      upnpRenderingControlService.setUpnpServiceClass(UPnPDeviceRenderingControlItf.class);
      upnpRenderingControlService.setUpnpServiceProvider(philipsNP3500Server);
      upnpDeviceFactory.addService(upnpRenderingControlService);

      UPnPDeviceService upnpConnectionManagerService = new UPnPDeviceService();
      upnpConnectionManagerService.setUpnpServiceClass(UPnPDeviceConnectionManagerItf.class);
      upnpConnectionManagerService.setUpnpServiceProvider(philipsNP3500Server);
      upnpDeviceFactory.addService(upnpConnectionManagerService);
      
      UPnPDeviceService upnpAVTTransportService = new UPnPDeviceService();
      upnpAVTTransportService.setUpnpServiceClass(UPnPDeviceAVTransportItf.class);
      upnpAVTTransportService.setUpnpServiceProvider(philipsNP3500Server);
      upnpDeviceFactory.addService(upnpAVTTransportService);
      
      LocalDevice philipsDevice = upnpDeviceFactory.generateUPnPDevice(); 
      upnpServer.addUPnPDevice(philipsDevice);
      
      /**Clients for the different service exposed by Philips NP3500*/
      /**Device rendering client*/
      UPnPServiceClient uPnPClientFactory=new UPnPServiceClient();
      uPnPClientFactory.setUpnpDeviceType("MediaRenderer");
      UPnPDeviceRenderingControlItf upnpDeviceRenderingControlClient=uPnPClientFactory.generateUPnPClient(UPnPDeviceRenderingControlItf.class);
      assertNotNull(upnpDeviceRenderingControlClient);
      
      upnpDeviceRenderingControlClient.setMute("0", "Master", "true");
      assertTrue(upnpDeviceRenderingControlClient.getMute("0", "Master"));
      upnpDeviceRenderingControlClient.setVolume("0", "Master", "10");
      assertEquals(10, upnpDeviceRenderingControlClient.getVolume("0", "Master"));
      ListPresets listPresets = upnpDeviceRenderingControlClient.listPresets("0");
      assertNotNull(listPresets);
      assertEquals("FactoryDefaults", listPresets.getCurrentPresetNameList());
      
      /**Device connection manager client*/
      uPnPClientFactory=new UPnPServiceClient();
      uPnPClientFactory.setUpnpDeviceType("MediaRenderer");
      UPnPDeviceConnectionManagerItf upnpDeviceConnectionManagerClient=uPnPClientFactory.generateUPnPClient(UPnPDeviceConnectionManagerItf.class);
      assertNotNull(upnpDeviceConnectionManagerClient);
      
      assertEquals(philipsNP3500Server.getCurrentConnectionIDs(),upnpDeviceConnectionManagerClient.getCurrentConnectionIDs());
      assertEquals(philipsNP3500Server.getCurrentConnectionInfo("0"),upnpDeviceConnectionManagerClient.getCurrentConnectionInfo("0"));
      assertEquals(philipsNP3500Server.getProtocolInfo(),upnpDeviceConnectionManagerClient.getProtocolInfo());
      
      /**Device AVT transport client*/
      uPnPClientFactory=new UPnPServiceClient();
      uPnPClientFactory.setUpnpDeviceType("MediaRenderer");
      UPnPDeviceAVTransportItf upnpDeviceAVTransportClient=uPnPClientFactory.generateUPnPClient(UPnPDeviceAVTransportItf.class);
      assertNotNull(upnpDeviceAVTransportClient);
      
      assertEquals(philipsNP3500Server.getCurrentTransportActions("0"),upnpDeviceAVTransportClient.getCurrentTransportActions("0"));
      assertEquals(philipsNP3500Server.getDeviceCapabilities("0"),upnpDeviceAVTransportClient.getDeviceCapabilities("0"));
      assertEquals(philipsNP3500Server.getMediaInfo("0"),upnpDeviceAVTransportClient.getMediaInfo("0"));
      assertEquals(philipsNP3500Server.getPositionInfo("0"),upnpDeviceAVTransportClient.getPositionInfo("0"));
      assertEquals(philipsNP3500Server.getTransportInfo("0"),upnpDeviceAVTransportClient.getTransportInfo("0"));
      assertEquals(philipsNP3500Server.getTransportSettings("0"),upnpDeviceAVTransportClient.getTransportSettings("0"));
      assertEquals(philipsNP3500Server.stop("0"),upnpDeviceAVTransportClient.stop("0"));
    }
}
