package org.ow2.fractal.upnp.test.example.philipsNP3500;

public class GetCurrentConnectionInfo
{
	protected String rcsID;
	protected String aVTransportID;
	protected String protocolInfo;
	protected String peerConnectionManager;
	protected String peerConnectionID;
	protected String direction;
	protected String status;

	public GetCurrentConnectionInfo()
	{
		this.rcsID="Constructor GetCurrentConnectionInfo";
		this.aVTransportID="Constructor GetCurrentConnectionInfo";
		this.protocolInfo="Constructor GetCurrentConnectionInfo";
		this.peerConnectionManager="Constructor GetCurrentConnectionInfo";
		this.peerConnectionID="Constructor GetCurrentConnectionInfo";
		this.direction="Constructor GetCurrentConnectionInfo";
		this.status="Constructor GetCurrentConnectionInfo";
	}
	
	public String getRcsID()
	{
		return rcsID;
	}

	public void setRcsID(String rcsID)
	{
		this.rcsID = rcsID;
	}

	public String getAVTransportID()
	{
		return aVTransportID;
	}

	public void setAVTransportID(String aVTransportID)
	{
		this.aVTransportID = aVTransportID;
	}

	public String getProtocolInfo()
	{
		return protocolInfo;
	}

	public void setProtocolInfo(String protocolInfo)
	{
		this.protocolInfo = protocolInfo;
	}

	public String getPeerConnectionManager()
	{
		return peerConnectionManager;
	}

	public void setPeerConnectionManager(String peerConnectionManager)
	{
		this.peerConnectionManager = peerConnectionManager;
	}

	public String getPeerConnectionID()
	{
		return peerConnectionID;
	}

	public void setPeerConnectionID(String peerConnectionID)
	{
		this.peerConnectionID = peerConnectionID;
	}

	public String getDirection()
	{
		return direction;
	}

	public void setDirection(String direction)
	{
		this.direction = direction;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	@Override
	public boolean equals(Object otherObject)
	{
	    return otherObject != null && GetCurrentConnectionInfo.class.isInstance(otherObject)
	    		&&((GetCurrentConnectionInfo)otherObject).getRcsID().equals(this.getRcsID())
	    		&&((GetCurrentConnectionInfo)otherObject).getAVTransportID().equals(this.getAVTransportID())
	    		&&((GetCurrentConnectionInfo)otherObject).getProtocolInfo().equals(this.getProtocolInfo())
	    		&&((GetCurrentConnectionInfo)otherObject).getPeerConnectionManager().equals(this.getPeerConnectionManager())
	    		&&((GetCurrentConnectionInfo)otherObject).getPeerConnectionID().equals(this.getPeerConnectionID())
	    		&&((GetCurrentConnectionInfo)otherObject).getDirection().equals(this.getDirection())
	    		&&((GetCurrentConnectionInfo)otherObject).getStatus().equals(this.getStatus())
	    		;
	}
}
