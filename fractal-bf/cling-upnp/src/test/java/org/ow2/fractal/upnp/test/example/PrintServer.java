/**
 * OW2 FraSCAti Examples: HelloWorld UPnP
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: R�mi M�lisson
 *         
 * Contributor(s): 
 * 
 */
package org.ow2.fractal.upnp.test.example;


/**
 * The print service implementation.
 */
public class PrintServer
  implements PrintService
{
    //@Property
    private String header = "->";

    private int count = 1;

    /**
     * Default constructor.
     */
    public PrintServer()
    {
        System.out.println("SERVER created.");
    }

    /**
     * PrintService implementation.
     */
    public final void print(final String msg)
    {
        System.out.println("SERVER: begin printing...");
        for (int i = 0; i < count; ++i) {
            System.out.println(header + msg);
        }
        System.out.println("SERVER: print done.");
    }

}
