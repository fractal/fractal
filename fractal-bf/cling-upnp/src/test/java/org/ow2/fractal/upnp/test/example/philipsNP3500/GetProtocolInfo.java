package org.ow2.fractal.upnp.test.example.philipsNP3500;

public class GetProtocolInfo
{
	protected String source;
	protected String sink;

	public GetProtocolInfo()
	{
		this.source="Constructor GetProtocolInfo";
		this.sink="Constructor GetProtocolInfo";
	}
		
	public String getSource()
	{
		return source;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public String getSink()
	{
		return sink;
	}

	public void setSink(String sink)
	{
		this.sink = sink;
	}

	@Override
	public boolean equals(Object otherObject)
	{
	    return otherObject != null && GetProtocolInfo.class.isInstance(otherObject)
	    		&&((GetProtocolInfo)otherObject).getSource().equals(this.getSource())
	    		&&((GetProtocolInfo)otherObject).getSink().equals(this.getSink())
	    		;
	}
}
