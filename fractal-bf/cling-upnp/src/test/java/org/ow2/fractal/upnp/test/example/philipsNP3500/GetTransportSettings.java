package org.ow2.fractal.upnp.test.example.philipsNP3500;

public class GetTransportSettings
{
	protected String playMode;
	protected String recQualityMode;

	public GetTransportSettings()
	{
		this.playMode="Constructor GetTransportSettings";
		this.recQualityMode="Constructor GetDeviceCapabilities";
	}

	public String getPlayMode()
	{
		return playMode;
	}

	public String getRecQualityMode()
	{
		return recQualityMode;
	}
	
	
	public void setPlayMode(String playMode)
	{
		this.playMode = playMode;
	}

	public void setRecQualityMode(String recQualityMode)
	{
		this.recQualityMode = recQualityMode;
	}

	@Override
	public boolean equals(Object otherObject)
	{
	    return otherObject != null && GetTransportSettings.class.isInstance(otherObject)
	    		&&((GetTransportSettings)otherObject).getPlayMode().equals(this.getPlayMode())
	    		&&((GetTransportSettings)otherObject).getRecQualityMode().equals(this.getRecQualityMode())
	    		;
	}
}
