package org.ow2.fractal.upnp.test.example.philipsNP3500;

public class ListPresets
{
	protected String currentPresetNameList;
	
	public ListPresets()
	{
		this.currentPresetNameList="FactoryDefaults";
	}

	public String getCurrentPresetNameList()
	{
		return currentPresetNameList;
	}

	public void setCurrentPresetNameList(String currentPresetNameList)
	{
		this.currentPresetNameList = currentPresetNameList;
	}
	
	@Override
	public boolean equals(Object otherObject)
	{
	    return otherObject != null && ListPresets.class.isInstance(otherObject) && ((ListPresets)otherObject).getCurrentPresetNameList().equals(this.currentPresetNameList);
	}
}
