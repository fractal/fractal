package org.ow2.fractal.upnp.test.example.philipsNP3500;

public class GetMediaInfo
{
	protected String nrTracks;
	protected String mediaDuration;
	protected String currentURI;
	protected String currentURIMetaData;
	protected String nextURI;
	protected String nextURIMetaData;
	protected String playMedium;
	protected String recordMedium;
	protected String writeStatus;

	
	public GetMediaInfo()
	{
		this.nrTracks="Constructor GetMediaInfo";
		this.mediaDuration="Constructor GetMediaInfo";
		this.currentURI="Constructor GetMediaInfo";
		this.currentURIMetaData="Constructor GetMediaInfo";
		this.nextURI="Constructor GetMediaInfo";
		this.nextURIMetaData="Constructor GetMediaInfo";
		this.playMedium="Constructor GetMediaInfo";
		this.recordMedium="Constructor GetMediaInfo";
		this.writeStatus="Constructor GetMediaInfo";
	}	

	public String getNrTracks()
	{
		return nrTracks;
	}

	public String getMediaDuration()
	{
		return mediaDuration;
	}

	public String getCurrentURI()
	{
		return currentURI;
	}

	public String getCurrentURIMetaData()
	{
		return currentURIMetaData;
	}

	public String getNextURI()
	{
		return nextURI;
	}

	public String getNextURIMetaData()
	{
		return nextURIMetaData;
	}

	public String getPlayMedium()
	{
		return playMedium;
	}

	public String getRecordMedium()
	{
		return recordMedium;
	}

	public String getWriteStatus()
	{
		return writeStatus;
	}
	
	public void setNrTracks(String nrTracks)
	{
		this.nrTracks = nrTracks;
	}

	public void setMediaDuration(String mediaDuration)
	{
		this.mediaDuration = mediaDuration;
	}

	public void setCurrentURI(String currentURI)
	{
		this.currentURI = currentURI;
	}

	public void setCurrentURIMetaData(String currentURIMetaData)
	{
		this.currentURIMetaData = currentURIMetaData;
	}

	public void setNextURI(String nextURI)
	{
		this.nextURI = nextURI;
	}

	public void setNextURIMetaData(String nextURIMetaData)
	{
		this.nextURIMetaData = nextURIMetaData;
	}

	public void setPlayMedium(String playMedium)
	{
		this.playMedium = playMedium;
	}

	public void setRecordMedium(String recordMedium)
	{
		this.recordMedium = recordMedium;
	}

	public void setWriteStatus(String writeStatus)
	{
		this.writeStatus = writeStatus;
	}

	@Override
	public boolean equals(Object otherObject)
	{
	    return otherObject != null && GetMediaInfo.class.isInstance(otherObject)
	    		&&((GetMediaInfo)otherObject).getNrTracks().equals(this.getNrTracks())
	    		&&((GetMediaInfo)otherObject).getMediaDuration().equals(this.getMediaDuration())
	    		&&((GetMediaInfo)otherObject).getCurrentURI().equals(this.getCurrentURI())
	    		&&((GetMediaInfo)otherObject).getCurrentURIMetaData().equals(this.getCurrentURIMetaData())
	    		&&((GetMediaInfo)otherObject).getNextURI().equals(this.getNextURI())
	    		&&((GetMediaInfo)otherObject).getNextURIMetaData().equals(this.getNextURIMetaData())
	    		&&((GetMediaInfo)otherObject).getPlayMedium().equals(this.getPlayMedium())
	    		&&((GetMediaInfo)otherObject).getRecordMedium().equals(this.getRecordMedium())
	    		&&((GetMediaInfo)otherObject).getWriteStatus().equals(this.getWriteStatus())
	    		;
	}
}
