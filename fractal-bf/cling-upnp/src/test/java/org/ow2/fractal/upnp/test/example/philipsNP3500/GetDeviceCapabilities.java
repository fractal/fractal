package org.ow2.fractal.upnp.test.example.philipsNP3500;

public class GetDeviceCapabilities
{
	protected String playMedia;
	protected String recMedia;
	protected String recQualityModes;

	public GetDeviceCapabilities()
	{
		this.playMedia="Constructor GetDeviceCapabilities";
		this.recMedia="Constructor GetDeviceCapabilities";
		this.recQualityModes="Constructor GetDeviceCapabilities";
	}

	public String getPlayMedia()
	{
		return playMedia;
	}

	public String getRecMedia()
	{
		return recMedia;
	}

	public String getRecQualityModes()
	{
		return recQualityModes;
	}
		
	public void setPlayMedia(String playMedia)
	{
		this.playMedia = playMedia;
	}

	public void setRecMedia(String recMedia)
	{
		this.recMedia = recMedia;
	}

	public void setRecQualityModes(String recQualityModes)
	{
		this.recQualityModes = recQualityModes;
	}

	@Override
	public boolean equals(Object otherObject)
	{
	    return otherObject != null && GetDeviceCapabilities.class.isInstance(otherObject)
	    		&&((GetDeviceCapabilities)otherObject).getPlayMedia().equals(this.getPlayMedia())
	    		&&((GetDeviceCapabilities)otherObject).getRecMedia().equals(this.getRecMedia())
	    		&&((GetDeviceCapabilities)otherObject).getRecQualityModes().equals(this.getRecQualityModes())
	    		;
	}
}
