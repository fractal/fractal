package org.ow2.fractal.upnp.test.example.philipsNP3500;
import org.ow2.fractal.upnp.annotation.UPnPAction;
import org.ow2.fractal.upnp.annotation.UPnPService;
import org.ow2.fractal.upnp.annotation.UPnPVariable;


/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Mathieu Schepens
 *
 * Contributor(s): 
 *
 */


/**
 *
 */
@UPnPService(type="RenderingControl")
public interface UPnPDeviceRenderingControlItf
{
    @UPnPAction("ListPresets")
    public ListPresets listPresets(@UPnPVariable("InstanceID") String InstanceID);
	
    @UPnPAction("SetMute")
    public void setMute(@UPnPVariable("InstanceID") String InstanceID, @UPnPVariable("Channel") String Channel, @UPnPVariable("DesiredMute") String DesiredMute);
    
    @UPnPAction("SetVolume")
    public void setVolume(@UPnPVariable("InstanceID") String InstanceID, @UPnPVariable("Channel") String Channel, @UPnPVariable("DesiredVolume") String DesiredVolume);
    
    @UPnPAction("GetVolume")
    public int getVolume(@UPnPVariable("InstanceID") String InstanceID, @UPnPVariable("Channel") String Channel);
    
    @UPnPAction("GetMute")
    public boolean getMute(@UPnPVariable("InstanceID") String InstanceID, @UPnPVariable("Channel") String Channel);
    
    @UPnPAction("SelectPresets")
    public void selectPresets(@UPnPVariable("InstanceID") String InstanceID, @UPnPVariable("PresetName") String PresetName);
}
