/**
 * Copyright (C) 2013 Inria
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: gwenael.cattez@inria.fr
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.fractal.upnp.test.example;

import org.ow2.fractal.upnp.annotation.UPnPAction;
import org.ow2.fractal.upnp.annotation.UPnPService;
import org.ow2.fractal.upnp.annotation.UPnPVariable;


@UPnPService(type="Fractal-UPnP-Test-TInterface")
public interface TInterface
{
    @UPnPAction("tInterfaceMethod1")
    public void meth1(int i);

    @UPnPAction("tInterfaceMethod2")
    public String meth2(@UPnPVariable("intI") int i,@UPnPVariable("stringS") String s,@UPnPVariable("intJ") int j);
}
