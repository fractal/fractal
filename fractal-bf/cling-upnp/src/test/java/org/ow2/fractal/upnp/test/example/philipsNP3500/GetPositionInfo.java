package org.ow2.fractal.upnp.test.example.philipsNP3500;

public class GetPositionInfo
{
	protected String track;
	protected String trackDuration;
	protected String trackMetaData;
	protected String trackURI;
	protected String relTime;
	protected String absTime;
	protected String relCount;
	protected String absCount;

	public GetPositionInfo()
	{
		this.track="Constructor GetPositionInfo";
		this.trackDuration="Constructor GetPositionInfo";
		this.trackMetaData="Constructor GetPositionInfo";
		this.trackURI="Constructor GetPositionInfo";
		this.relTime="Constructor GetPositionInfo";
		this.absTime="Constructor GetPositionInfo";
		this.relCount="Constructor GetPositionInfo";
		this.absCount="Constructor GetPositionInfo";
	}
		
	public String getTrack()
	{
		return track;
	}

	public String getTrackDuration()
	{
		return trackDuration;
	}

	public String getTrackMetaData()
	{
		return trackMetaData;
	}

	public String getTrackURI()
	{
		return trackURI;
	}

	public String getRelTime()
	{
		return relTime;
	}

	public String getAbsTime()
	{
		return absTime;
	}

	public String getRelCount()
	{
		return relCount;
	}

	public String getAbsCount()
	{
		return absCount;
	}
	
	public void setTrack(String track)
	{
		this.track = track;
	}

	public void setTrackDuration(String trackDuration)
	{
		this.trackDuration = trackDuration;
	}

	public void setTrackMetaData(String trackMetaData)
	{
		this.trackMetaData = trackMetaData;
	}

	public void setTrackURI(String trackURI)
	{
		this.trackURI = trackURI;
	}

	public void setRelTime(String relTime)
	{
		this.relTime = relTime;
	}

	public void setAbsTime(String absTime)
	{
		this.absTime = absTime;
	}

	public void setRelCount(String relCount)
	{
		this.relCount = relCount;
	}

	public void setAbsCount(String absCount)
	{
		this.absCount = absCount;
	}

	@Override
	public boolean equals(Object otherObject)
	{
	    return otherObject != null && GetPositionInfo.class.isInstance(otherObject)
	    		&&((GetPositionInfo)otherObject).getTrack().equals(this.getTrack())
	    		&&((GetPositionInfo)otherObject).getTrackDuration().equals(this.getTrackDuration())
	    		&&((GetPositionInfo)otherObject).getTrackURI().equals(this.getTrackURI())
	    		&&((GetPositionInfo)otherObject).getRelTime().equals(this.getRelTime())
	    		&&((GetPositionInfo)otherObject).getAbsTime().equals(this.getAbsTime())
	    		&&((GetPositionInfo)otherObject).getRelCount().equals(this.getRelCount())
	    		&&((GetPositionInfo)otherObject).getAbsCount().equals(this.getAbsCount())
	    		;
	}
}
