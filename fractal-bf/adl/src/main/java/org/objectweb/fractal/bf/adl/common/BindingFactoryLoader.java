/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.adl.common;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;

/**
 * 
 */
public abstract class BindingFactoryLoader extends AbstractLoader {

	/**
	 * @see org.objectweb.fractal.adl.Loader#load(java.lang.String,
	 *      java.util.Map)
	 */
	public Definition load(String name, Map<Object, Object> context)
			throws ADLException {
		Definition d = clientLoader.load(name, context);
		checkNode(d);
		return d;
	}

	/**
	 * @param node
	 * @throws ADLException
	 *             if the node doesn't provide values for the type and the
	 *             interface attributes
	 */
	private void checkNode(Object node) throws ADLException {
		performSpecificChecksOn(node);
		if (node instanceof ComponentContainer) {
			ComponentContainer componentContainer = (ComponentContainer) node;

			Component[] comps = componentContainer.getComponents();
			if (comps != null) {
				for (int i = 0; i < comps.length; i++) {
					checkNode(comps[i]);

				}
			}
		}
	}

	/**
	 * @param node
	 */
	protected abstract void performSpecificChecksOn(Object node)
			throws ADLException;

}
