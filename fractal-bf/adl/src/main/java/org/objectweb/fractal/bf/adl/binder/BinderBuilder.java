/**
 * Fractal Binding Factory.
 * Copyright (Cimport java.util.List;

import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.adl.common.Parameter;
ser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.adl.binder;

import java.util.List;

import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.adl.common.Parameter;

/**
 * A builder for binders
 */
public interface BinderBuilder {
	public void bind(String itfName, Object component,
			List<Parameter> parameters, String type, Object context)
			throws BindingFactoryException;
}
