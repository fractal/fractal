/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2010 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.adl.binder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.components.ComponentPair;
import org.objectweb.fractal.adl.components.PrimitiveCompiler;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.bf.adl.common.Parameter;
import org.objectweb.fractal.bf.adl.common.ParameterContainer;
import org.objectweb.fractal.bf.adl.exporter.ExporterBuilder;
import org.objectweb.fractal.task.core.Task;
import org.objectweb.fractal.task.core.TaskMap;
import org.objectweb.fractal.task.deployment.api.InstanceProviderTask;
import org.objectweb.fractal.task.deployment.lib.AbstractRequireInstanceProviderTask;

public class BinderCompiler implements PrimitiveCompiler, BindingController {

	Logger log = Logger.getLogger(BinderCompiler.class.getCanonicalName());

	/**
	 * Name of the mandatory interface bound to the {@link ExporterBuilder} used
	 * by this compiler.
	 */

	public final static String BUILDER_BINDING = "builder";

	private BinderBuilder builder;

	/**
	 * @see org.objectweb.fractal.adl.components.PrimitiveCompiler#compile(java.util.List,
	 *      org.objectweb.fractal.adl.components.ComponentContainer,
	 *      org.objectweb.fractal.task.core.TaskMap, java.util.Map)
	 */
	public void compile(List<ComponentContainer> path,
			ComponentContainer container, TaskMap tasks,
			Map<Object, Object> context) throws ADLException {

		log.finest("Entering Binder Compiler");

		// this code is here purely to obtain a reference to serverComp via the
		// subComponents map
		Map<String, Object> subComponents = new HashMap<String, Object>();
		subComponents.put("this", container);
		Component[] comps = container.getComponents();
		for (int i = 0; i < comps.length; i++) {
			subComponents.put(comps[i].getName(), comps[i]);
		}

		if (container instanceof BinderContainer) {

			Binder[] binders = ((BinderContainer) container).getBinders();

			for (Binder binder : binders) {

				Parameter[] parameters = null;

				if (binder instanceof ParameterContainer) {

					ParameterContainer pc = (ParameterContainer) binder;
					parameters = pc.getParameters();
				}

				final String componentDotInterface = binder.getInterface();
				// compName.itfName
				int dot = componentDotInterface.lastIndexOf(".");
				final String componentName = componentDotInterface.substring(0,
						dot);
				final String itfName = componentDotInterface.substring(dot + 1);

				Object serverComp = subComponents.get(componentName);
				try {
					// the task may already exist, in case of a shared component
					tasks.getTask("binder" + componentDotInterface, serverComp);
				} catch (NoSuchElementException e) {

					// createServerTask TaskHole
					TaskMap.TaskHole createServerTaskHole = tasks.getTaskHole(
							"create", serverComp);

					BinderTask exportTask = new BinderTask(builder, binder
							.getType(), itfName, parameters);

					exportTask
							.setServerInstanceProviderTask(createServerTaskHole);

					tasks.addTask("binder" + componentDotInterface, serverComp,
							exportTask);

					if (serverComp != container) {

						TaskMap.TaskHole addTaskHole = tasks.getTaskHole("add",
								new ComponentPair(container,
										(Component) serverComp));
						exportTask.addDependency(addTaskHole,
								Task.PREVIOUS_TASK_ROLE, context);
					}

				} // end catch block

			}

		}

		log.finest("Exiting Binder Compiler");
	}

	static class BinderTask extends AbstractRequireInstanceProviderTask {

		private TaskMap.TaskHole serverInstanceProviderTask;

		Logger log = Logger.getLogger(BinderTask.class.getCanonicalName());

		private final BinderBuilder builder;

		String type;

		String itfName;

		Parameter[] parameters;

		/**
		 * @param builder
		 * @param type
		 * @param itfName
		 * @param parameters
		 */
		public BinderTask(BinderBuilder builder, String type, String itfName,
				Parameter[] parameters) {
			this.builder = builder;
			this.type = type;
			this.itfName = itfName;
			this.parameters = parameters;
		}

		/**
		 * @see org.objectweb.fractal.task.core.Task#execute(java.util.Map)
		 */
		public void execute(Map<Object, Object> context) throws Exception {

			List<Parameter> prms = new ArrayList<Parameter>();

			if (parameters != null) {
				for (Parameter p : parameters)
					prms.add(p);
			} else {
				log
						.warning("Couldn't find any parameters to configure the binder");
			}

			Object server = getServerInstanceProviderTask().getInstance();

			builder.bind(itfName, server, prms, type, context);

		}

		/*
		 * the following 2 methods are copied from BindingCompiler
		 */
		public InstanceProviderTask getServerInstanceProviderTask() {
			return (serverInstanceProviderTask) == null ? null
					: (InstanceProviderTask) serverInstanceProviderTask
							.getTask();
		}

		public void setServerInstanceProviderTask(final TaskMap.TaskHole task) {
			if (serverInstanceProviderTask != null) {
				removePreviousTask(serverInstanceProviderTask);
			}
			serverInstanceProviderTask = task;
			if (serverInstanceProviderTask != null) {
				addPreviousTask(serverInstanceProviderTask);
			}
		}

		/**
		 * @see org.objectweb.fractal.task.core.Task#getResult()
		 */
		public Object getResult() {
			return null;
		}

		@Override
		public String toString() {
			return "T" + System.identityHashCode(this) + "[BinderTask()]";
		}

	}

	// ~ BindingController methods
	public void bindFc(String itf, Object value)
			throws NoSuchInterfaceException, IllegalBindingException,
			IllegalLifeCycleException {

		if (itf.equals(BUILDER_BINDING)) {
			builder = (BinderBuilder) value;
			return;
		}
		throw new NoSuchInterfaceException("Cannot find interface " + itf);
	}

	public String[] listFc() {
		return new String[] { BUILDER_BINDING };
	}

	public Object lookupFc(String itf) throws NoSuchInterfaceException {

		if (itf.equals(BUILDER_BINDING)) {
			return builder;
		}
		throw new NoSuchInterfaceException("Cannot find interface " + itf);
	}

	public void unbindFc(String itf) throws NoSuchInterfaceException,
			IllegalBindingException, IllegalLifeCycleException {

		if (itf.equals(BUILDER_BINDING)) {
			builder = null;
			return;
		}
		throw new NoSuchInterfaceException("Cannot find interface " + itf);
	}

}
