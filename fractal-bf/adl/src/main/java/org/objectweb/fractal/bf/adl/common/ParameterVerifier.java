/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.adl.common;

import java.util.logging.Logger;

import org.objectweb.fractal.adl.ADLException;

public class ParameterVerifier {

	static Logger log = Logger.getLogger(ParameterVerifier.class
			.getCanonicalName());

	private ParameterVerifier() {
	}

	/**
	 * @param pc
	 * @throws ADLException
	 */
	public static void verify(ParameterContainer pc) throws ADLException {
		if (pc.getParameters().length > 0) {
			for (Parameter p : pc.getParameters()) {
				log.config("ADL Parameter to be verified: [" + p.getName()
						+ ":" + p.getValue() + "] ");
				if ((p.getName() == null) || (p.getName().equalsIgnoreCase(""))) {

					throw new ADLException(
							"The name of a parameter can't be empty");

				} else if (p.getValue() == null
						|| p.getValue().equalsIgnoreCase("")) {
					throw new ADLException("The value for parameter '"
							+ p.getName() + "' can't be empty");
				}
			}
		}
	}
}
