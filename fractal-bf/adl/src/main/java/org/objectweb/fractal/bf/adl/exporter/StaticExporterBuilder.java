/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.adl.exporter;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.adl.common.Parameter;

/**
 * Exporter builder meant to be used with the static backed of the Fractal-BF
 * ADL.
 */
public class StaticExporterBuilder implements ExporterBuilder {
	Logger log = Logger.getLogger(StaticExporterBuilder.class
			.getCanonicalName());

	/**
	 * @see org.objectweb.fractal.bf.adl.exporter.ExporterBuilder#export(java.lang.String,
	 *      java.lang.Object, java.util.List, java.lang.String,
	 *      java.lang.Object)
	 */
	public void export(String itfName, Object component,
			List<Parameter> parameters, String type, Object context)
			throws BindingFactoryException {
		PrintWriter pw = (PrintWriter) ((Map<?, ?>) context).get("printwriter");
		log.finest("PrintWriter for static exporter builder: " + pw);

		pw.write(Map.class.getCanonicalName()
				+ "<String,Object> exportHints = new "
				+ HashMap.class.getCanonicalName() + "<String,Object>();\n");
		for (Parameter p : parameters) {
			pw.write("exportHints.put(\"" + p.getName() + "\",\""
					+ p.getValue() + "\");\n");
		}
		pw.write("org.objectweb.fractal.bf.BindingFactory bf = org.objectweb.fractal.bf.BindingFactoryHelper.getBindingFactory();\n");
		pw.write("Component component = (Component)" + component + ";\n");
		pw.write("exportHints.put(org.objectweb.fractal.bf.BindingFactoryImpl.PLUGIN_ID,"+ type + "\");\n");
		pw.write("bf.export(component, \"" + itfName + "\", exportHints);\n");
	}
}
