/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.adl.exporter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.bf.BindingFactory;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.BindingFactoryHelper;
import org.objectweb.fractal.bf.BindingFactoryImpl;
import org.objectweb.fractal.bf.adl.common.Parameter;

/**
 * An exporter builder meant to be used with the dynamic backend of Fractal-BF
 * ADL.
 */
public class DynamicExporterBuilder implements ExporterBuilder {

	Logger log = Logger.getLogger(DynamicExporterBuilder.class
			.getCanonicalName());

	/**
	 * @throws BindingFactoryException
	 * @see org.objectweb.fractal.bf.adl.exporter.ExporterBuilder#export(java.lang.String,
	 *      java.lang.Object, java.util.List, String, Object)
	 */
	public void export(String itfName, Object c, List<Parameter> parameters,
			String type, Object context) throws BindingFactoryException {

		Map<String, Object> exportHints = new HashMap<String, Object>();
		/* fill the export hints map */
		for (Parameter p : parameters)
			exportHints.put(p.getName(), p.getValue());

		exportHints.put(BindingFactoryImpl.PLUGIN_ID, type);

		log
				.config("BindingFactory will export with the following parameters: [itf,"
						+ itfName + "],[params:" + exportHints.toString() + "]");

		BindingFactory bf = BindingFactoryHelper.getBindingFactory();
		Component component = (Component) c;
		bf.export(component, itfName, exportHints);
	}

}
