/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.adl.binder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.bf.BindingFactory;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.BindingFactoryHelper;
import org.objectweb.fractal.bf.BindingFactoryImpl;
import org.objectweb.fractal.bf.adl.common.Parameter;

/**
 * A binder builder that uses Fractal API to build the binder.
 */
public class FractalBinderBuilder implements BinderBuilder {

	Logger log = Logger
			.getLogger(FractalBinderBuilder.class.getCanonicalName());

	/**
	 * @see org.objectweb.fractal.bf.adl.binder.BinderBuilder#bind(java.lang.String,
	 *      java.lang.Object, java.util.List, java.lang.String, Object)
	 */
	public void bind(String itfName, Object c, List<Parameter> parameters,
			String type, Object context) throws BindingFactoryException {
		Map<String, Object> bindHints = new HashMap<String, Object>();
		/* fill the export hints map */
		for (Parameter p : parameters)
			bindHints.put(p.getName(), p.getValue());

		bindHints.put(BindingFactoryImpl.PLUGIN_ID, type);

		log.finest("Remote binding to be established: [itf," + itfName
				+ "],[params:" + bindHints.toString() + "]");

		BindingFactory bf = BindingFactoryHelper.getBindingFactory();
		Component component = (Component) c;
		bf.bind(component, itfName, bindHints);

	}

}
