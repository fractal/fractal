/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.adl.binder;

import java.util.logging.Logger;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.bf.adl.common.BindingFactoryLoader;
import org.objectweb.fractal.bf.adl.common.ParameterContainer;
import org.objectweb.fractal.bf.adl.common.ParameterVerifier;

/**
 * A loader for <binder> nodes.
 */
public class BinderLoader extends BindingFactoryLoader {

	Logger log = Logger.getLogger(BinderLoader.class.getCanonicalName());

	/**
	 * @throws ADLException
	 * @see org.objectweb.fractal.bf.adl.common.BindingFactoryLoader#performSpecificChecksOn(java.lang.Object)
	 */
	@Override
	protected void performSpecificChecksOn(Object node) throws ADLException {
		if (node instanceof BinderContainer) {
			BinderContainer bindingContaier = (BinderContainer) node;
			final Binder[] binders = bindingContaier.getBinders();
			if (binders.length > 0) {
				for (Binder binder : binders) {
					if (binder.getType() == null
							|| binder.getType().equalsIgnoreCase("")) {
						throw new ADLException(
								"The type of the binder to use can't be empty");
					}
					if (binder.getInterface() == null
							| binder.getInterface().equalsIgnoreCase("")) {
						throw new ADLException(
								"Interface to be used by the binder missing");
					} else {
						String itf = binder.getInterface();
						int dot = itf.lastIndexOf(".");
						String componentName = itf.substring(0, dot);
						String itfName = itf.substring(dot + 1);
						log.info("Binder will bind interface '" + itfName
								+ "' of component '" + componentName + "'");
					}
					if (binder instanceof ParameterContainer) {

						ParameterContainer pc = (ParameterContainer) binder;
						ParameterVerifier.verify(pc);

					}

				}
			}
		}

	}

}
