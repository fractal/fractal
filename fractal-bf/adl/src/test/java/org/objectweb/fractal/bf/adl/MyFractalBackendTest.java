package org.objectweb.fractal.bf.adl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.adl.types.TypeBuilder;
import org.objectweb.fractal.adl.util.ContentControllerHelper;
import org.objectweb.fractal.adl.util.NoSuchComponentException;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.util.Fractal;

public class MyFractalBackendTest {

	private Factory myBackend;

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Before
	public void setUp() throws Exception {
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
	}

	/**
	 * Load my backend using my backend
	 * 
	 * @throws NoSuchComponentException
	 * @throws NoSuchInterfaceException
	 */
	@Test
	public void testLoadMyFractalBackendADL() throws NoSuchInterfaceException,
			NoSuchComponentException {
		try {
			Factory fac = FactoryFactory
					.getFactory(FactoryFactory.FRACTAL_BACKEND);
			Component myBackendComp = (Component) fac.newComponent(
					"org.objectweb.fractal.bf.adl.MyFractalBackend",
					new HashMap<Object, Object>());
			assertNotNull(myBackendComp);

			Component typeCompiler = ContentControllerHelper
					.getSubComponentByName(myBackendComp, "type-compiler");
			final TypeBuilder typeBuilder = (TypeBuilder) Fractal
					.getBindingController(typeCompiler).lookupFc("builder");
			assertNotNull(typeBuilder);
		} catch (ADLException e) {
			e.printStackTrace();
			fail("Cannot load MyFractalBackend");
		}
	}

	/**
	 * Trying to load adl definitions without <exporter> or <binder>
	 * definitions. don't need to check exception here
	 * 
	 * @throws ADLException
	 */
	@Test
	public void testLoadSimpleADLWithMyBackend() throws ADLException {
		myBackend = loadDynamicBackend();
		try {
			final Component simpleComponent = (Component) myBackend
					.newComponent(
							"org.objectweb.fractal.bf.connectors.Service",
							new HashMap<Object, Object>());
			assertNotNull(simpleComponent);
		} catch (NullPointerException npe) {
			npe.printStackTrace();
		}
	}

	/**
	 * @throws ADLException
	 */
	private Factory loadDynamicBackend() throws ADLException {
		final Factory factory = FactoryFactory.getFactory(
				"org.objectweb.fractal.bf.adl.MyBasicFactory",
				"org.objectweb.fractal.bf.adl.MyFractalBackend",
				new HashMap<Object, Object>());

		return factory;
	}
}
