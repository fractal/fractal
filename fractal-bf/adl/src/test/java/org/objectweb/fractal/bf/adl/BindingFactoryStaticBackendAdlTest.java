package org.objectweb.fractal.bf.adl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;

public class BindingFactoryStaticBackendAdlTest {
	protected Factory adlFactory;
	private Map<Object, Object> context;

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Before
	public void setUp() throws Exception {
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");

		final PrintWriter printWriter = new PrintWriter(System.err, true);
		context = new HashMap<Object, Object>();
		context.put("printwriter", printWriter);

		adlFactory = FactoryFactory.getFactory(
				"org.objectweb.fractal.bf.adl.MyBasicFactory",
				"org.objectweb.fractal.bf.adl.MyStaticFractalBackend", context);

		assertNotNull(adlFactory);
	}

	@Test
	public void testLoadSimpleADL() {
		try {
			final String simpleComponentAsString = (String) adlFactory
					.newComponent(
							"org.objectweb.fractal.bf.connectors.Service",
							context);

			assertNotNull(simpleComponentAsString);

		} catch (ADLException e) {
			e.printStackTrace();
			fail(e.getLocalizedMessage());
		}
	}

}
