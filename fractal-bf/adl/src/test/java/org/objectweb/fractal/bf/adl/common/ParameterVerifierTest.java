package org.objectweb.fractal.bf.adl.common;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;

public class ParameterVerifierTest {

	protected Factory adlFactory;

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Before
	public void setUp() throws Exception {
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
		adlFactory = FactoryFactory.getFactory(
				"org.objectweb.fractal.bf.adl.MyBasicFactory",
				"org.objectweb.fractal.bf.adl.MyFractalBackend",
				new HashMap<Object, Object>());

		assertNotNull(adlFactory);
	}

	String EMPTY_PARAMETER_NAME = "org.objectweb.fractal.bf.adl.EmptyParameterName";
	String EMPTY_PARAMETER_VALUE = "org.objectweb.fractal.bf.adl.EmptyParameterValue";

	@Test(expected = ADLException.class)
	public void testEmptyParameterNameThrowsADLException() throws ADLException {
		this.adlFactory.newComponent(EMPTY_PARAMETER_NAME, null);
	}

	@Test(expected = ADLException.class)
	public void testEmptyParameterValueThrowsADLException() throws ADLException {
		this.adlFactory.newComponent(EMPTY_PARAMETER_VALUE, null);
	}
}
