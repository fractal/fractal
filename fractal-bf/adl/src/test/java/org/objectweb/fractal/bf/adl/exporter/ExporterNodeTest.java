
package org.objectweb.fractal.bf.adl.exporter;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.xml.XMLNode;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;
import org.objectweb.fractal.bf.adl.common.ParameterContainer;
import org.xml.sax.SAXException;

/**
 * Unit tests for exporter AST elements.
 */
public class ExporterNodeTest
{

  XMLNodeFactory factory;

  /**
   * @see junit.framework.TestCase#setUp()
   */
  @Before
  public void setUp() throws Exception
  {
    this.factory = new XMLNodeFactoryImpl();
  }

  @Test
  public void testExporterNode() throws SAXException
  {

    final XMLNode newXMLNode = factory.newXMLNode(
        "classpath://org/objectweb/fractal/bf/adl/xml/standard-bf.dtd",
        "exporter");

    Exporter exporterNode = (Exporter) newXMLNode.astNewInstance();

    assertNotNull(exporterNode);

    assertTrue("Exporter must support parameter sub-elements",
        exporterNode instanceof ParameterContainer);

  }

}
