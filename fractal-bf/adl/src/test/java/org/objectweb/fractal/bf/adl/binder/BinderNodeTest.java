
package org.objectweb.fractal.bf.adl.binder;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.xml.XMLNode;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;
import org.objectweb.fractal.bf.adl.common.ParameterContainer;
import org.xml.sax.SAXException;

/**
 * Unit tests for binder AST elements.
 */
public class BinderNodeTest
{

  XMLNodeFactory factory;

  /**
   * @see junit.framework.TestCase#setUp()
   */
  @Before
  public void setUp() throws Exception
  {
    this.factory = new XMLNodeFactoryImpl();
    assertNotNull(this.factory);
  }

  @Test
  public void testBinderNode() throws SAXException
  {
    final XMLNode newXMLNode = factory.newXMLNode(
        "classpath://org/objectweb/fractal/bf/adl/xml/standard-bf.dtd",
        "binder");

    assertNotNull(newXMLNode);

    Binder binderNode = (Binder) newXMLNode.astNewInstance();

    assertNotNull(binderNode);

    assertTrue("Binder must support parameter sub-elements",
        binderNode instanceof ParameterContainer);
  }
}
