============================================================================
Fractal Binding Factory.
Copyright (C) 2007-2009 INRIA, SARDES

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Contact: Valerio.Schiavoni@gmail.com

Author: Valerio Schiavoni

============================================================================

Fractal Binding Factory 0.6
---------------------------

The Fractal Binding Factory (BF, from now on) provides support for establishing remote 
bindings between Fractal components using diverse protocols. The BF can expose a Fractal
server interface as a web service or can register it into a RMI registry, or can make it
available for group-RPC, and so on, each one via the same APIs. Also, it can transparently
bind Fractal client interfaces to remotely available web services, or to services available
into an RMI registry, and so on.

Table of content
----------------
  1. Introduction
  2. Compiling and installing the BF artifacts
  3. Running the sample applications
  4. Using the BF with FractalADL
  5. Using the BF with its Java APIs
  6. How to develop a new plugin
  7. References
  
1. Introduction
---------------
The BF is a general purpose framework aimed at establishing remote bindings between Fractal components.
Its design is built upon the "export-bind" pattern (see: http://sardes.inrialpes.fr/papers/files/05-Krakowiak-CFSE-eng.pdf
for further details). 
Two main entities are made available by the BF:
 - exporters: these entities are able to export a given interface so that it's available through
  some distributed communication protocol;
 - binders: these entities can establish a so-called "distributed binding" between a local interface and a previously
  exported one, using some distributed communication protocol;

These two key entities are available at different levels throughout the BF: when using FractalADL (see Sec.4), the user
can declare <exporter> and <binder> elements; when using the Java APIs (see Sec.5), two operations (export/bind) are 
made available to the developer.

The specific semantic of the export and bind operations is protocol specific: exporting an interface using a 
SOAP-like protocol means to let it accepts remote calls through web-service clients; conversely, exporting it
using JavaRMI causes it to be registered into an RMI registry, and so on.

The result of an export operation is the generation of a skeleton; the result of a bind
operation is a stub. Stubs and skeletons are themselves Fractal components: this brings all the advantages
of the Fractal component model (ie., dynamically reconfigure distributed bindings).
Currently, most of the generation of the code for these components is done at runtime, leveraging JDK's dynamic proxies
or other bytecode generation mechanisms. 

It would be technically possible to pre-generate the source-code of stub and skeleton components, but such feature 
is not currently available.


2. Compiling and installing BF artifacts
------------------------------------------------------
The BF sources is organized into several Maven modules, among which:
 
 - fractal-bf-core: it's the main module, it contains the default implementation
   of the BF APIs allowing; it also contains the APIs to be implemented to provide 
   new plugins;
    
 - fractal-bf-adl: an extension to the Fractal ADL to use
   the BF;
   
 - connectors: this module stores the currently available  implementations of  connectors, to be used
   by the core module; third parties connectors can be used as well;

 - examples: a set of examples (the inevitable "helloworld", and many others), showcasing how to use the
   BF APIs, the ADL extension, and how to configure your projects accordingly;
 
To compile and install the latest SNAPSHOT versions of all BF modules in your
local Maven repository:
 
 svn co svn+ssh://svn.forge.objectweb.org/svnroot/fractal/trunk/fractal-bf fractal-bf-parent
 cd fractal-bf-parent
 mvn install
 
 2.1 Importing BF artifacts into your Maven-based project
 ------------------------------------------------------
If you don't want to compile the BF sources, and your project is based on the Maven build tool management, it is
very easy to import the required libraries into your project.

Add the OW2 repositories in your pom.xml file:
 <!--OW2 repositories -->
	<repositories>
		<repository>
			<id>objectweb-release</id>
			<name>ObjectWeb Maven Repository</name>
			<url>http://maven.objectweb.org/maven2</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>objectweb-snapshot</id>
			<name>ObjectWeb Maven Repository</name>
			<url>http://maven.objectweb.org/maven2-snapshot</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>daily</updatePolicy>
			</snapshots>
		</repository>
	</repositories>
	
 
 Add the following dependencies in your pom.xml file:
 <properties>
 <bf.version>0.6</bf.version>
 </properties>
 <dependency>
	 <groupId>org.objectweb.fractal.bf</groupId>
	 <artifactId>fractal-bf-core</artifactId>
	 <version>${bf.version}</version>
</dependency>
<dependency>
	<groupId> org.objectweb.fractal.bf </groupId>
	<artifactId>fractal-bf-adl</artifactId>
	 <version>${bf.version}</version>
</dependency>
<!-- the following is only if you need WebService bindings. Change it 
accordingly to your needs -->
<dependency>
	<groupId>org.objectweb.fractal.bf.connectors</groupId>
	<artifactId>fractal-bf-connectors-soap-cxf</artifactId>
	 <version>${bf.version}</version>
</dependency>

3. Running the sample applications
----------------------------------
Samples applications are available in the examples/
directory. Read the README.txt file into each of the directories for
detailed explanations. 


4. Using the BF with FractalADL
--------------------------------------------------------
The fractal-bf-adl module provides an extension to the Fractal ADL toolchain. It is the preferred way to 
use the BF with your Fractal application.

Consider the following ADL example:

01 <!DOCTYPE definition PUBLIC "-//objectweb.org//DTD Fractal ADL 2.0//EN" "classpath://org/objectweb/fractal/bf/adl/xml/standard-bf.dtd">
02 <definition name="org.objectweb.fractal.bf.Services">
03 <interface name="print" role="server" signature="org.objectweb.fractal.bf.Print" />
04	<component name="service">
05		<interface name="print" role="server"
06			signature="org.objectweb.fractal.bf.Print" />
07		<content class="org.objectweb.fractal.bf.PrintImpl" />
08	</component>
09	<binding client="this.print" server="service.print" />
10  <exporter type="ws" interface="service.print">
11		<parameter name="address" value="http://localhost:8080/Print" />
12	</exporter>
13 </definition>

Line 01 declares the DTD used in this ADL file (standard-bf.dtd): it is required
to use BF extensions.Tthis DTD is an extension to the 
default FractalADL standard.dtd: it adds two more elements (namely, exporter and binder elements). 
This dtd file becomes available only after having imported the fractal-bf-adl module.
 
Line 02 defines a composite component named 'org.objectweb.fractal.bf.Services'.

Line 03 defines an external 'print' server interface, whose java type is 'org.objectweb.fractal.bf.Print'.

Lines 04-08 define a 'service' component with a server interface 'print'. 

Line 09 defines an internal binding between the external 'print' server interface and the 'print' server
interface of the 'service' component.

Lines 10-12 define an 'exporter' element: two required attributes must be provided:
 -type: a unique string identifier, used by the BF plugin resolver to load the appropriate plugin;
 -interface: a '{component}.{interfaceName}' identifier for the interface to export; in this case, the 'service.print' identifies
  the 'print' interface declared by the 'service' component

Line 11 define am optional 'parameter' element: plugins might declare any number of <parameter> elements. Each parameter
element requires two attributes:
 - name : the name of attribute
 - value: the value for the attribute
 Parameter names are plugin-specifics. Using the Java APIs it's possible to retrieve the list of required attributes
 declared by a plugin (see following section for further details).  
 
== Maven users ==
To use the fractal-bf-module, add the following dependency to your Maven-based pom.xml :

<dependency>
	<groupId> org.objectweb.fractal.bf </groupId>
	<artifactId>fractal-bf-adl</artifactId>
	<version>0.5-SNAPSHOT</version>
</dependency>

5. Using the BF via the Java APIs
----------------------------------------------------------------

The following code shows a possible use of the Java APIs offered by the BF to export a server interface
through the web-service plugin. The example assumes that the following ADL file is available in the classpath:

01 <!DOCTYPE definition PUBLIC "-//objectweb.org//DTD Fractal ADL 2.0//EN" "classpath://org/objectweb/fractal/bf/adl/xml/standard.dtd">
02 <definition name="org.objectweb.fractal.bf.Services">
03 <interface name="print" role="server" signature="org.objectweb.fractal.bf.Print" />
04	<component name="printer">
05		<interface name="print" role="server"
06			signature="org.objectweb.fractal.bf.Print" />
07		<content class="org.objectweb.fractal.bf.PrintImpl" />
08	</component>
09	<binding client="this.print" server="printer.print" />
10 </definition>

The following java code is functionally equivalent to the ADL version shown before:
it will load the above ADL file using the FractalADL factory, retrieve the BF and
export its print interface as a web service. The main difference is that the ADL file is not using
the dtd provided by the fractal-bf-adl module,i.e. this is a classical ADL file.

public class BFDemo {
  public static void main(String[] args) throws Exception{
    Factory adlFactory = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
    Component services = (Component) adlFactory.newComponent("org.objectweb.fractal.bf.Services",new HashMap());
    Component printer = ContentControllerHelper.getSubComponentByName(services,"printer");
    Map<String, Object> exportHints = new HashMap<String, Object>();
    exportHints.put(BindingFactoryImpl.PLUGIN_ID, "ws");
    exportHints.put("address", "http://localhost:8080/Service");
    BindingFactory bindingFactory = BindingFactoryHelper.getBindingFactory();
    bindingFactory.export(printer,"print",exportHints);
  }
}

It is nevertheless possible to avoid using the ADL factory, and completely rely on the 
Fractal Java APIs to instantiate the BindingFactoryComp component and use it the same way as
illustrated above.

6. Developing a new connector
----------------------------------------------------------------

This section intends to provide guidelines and a simple walk-through to develop
a new connector. Because of a plugin-based architecture of the core module of 
the BF, connectors are considered as plugins: thus, the terms connector and plugin
will be used interchangeably.

** Please note that the following guide is based on version 0.5 of the BF APIs. **

a) Define a unique ID. 
 Example: "rss"

b) Define the feed type
 Example: version=atom1/atom2/etc

c) Create a class modeling the export hints and provide getter/setter methods:
 Example:
public class RssExportHints implements ExportHints {
   String version;
   String feedType;
   public String getVersion(){return this.version};
   public void setVersion(String v){this.version=v};
   ...
}

d) Create a class modeling the import hints and provide getter/setter methods:
 Example:
public class RssBindHints implements BindHints {
 String version;
 String feedType;
   public String getVersion(){return this.version};
   public void setVersion(String v){this.version=v};
   ...
}

e) Extend the AbstractBindingFactoryPlugin class:
 Example:
public class RssConnector 
    extends AbstractBindingFactoryPlugin<RssExportHints,RssBindHints>{
  
  public RssConnector{} //required by Fractal
  
  public String getPluginIdentifier() {
		return "rss";
	}
  public RssExportHints getExportHints(Map<String,String> hints) {
		RssExportHints rssExportHints = new RssExportHints();
		rssExportHints.setVersion(hints.get("version"));
		return rssExportHints;
	}
  public RssBindHints getBindHints(Map<String,String> hints) {
		RssBindHints rssBindHints = new RssBindHints();
		rssBindHints.setVersion(hints.get("version"));
		return rssBindHints;
	}
	
  public Component createSkel(String interfaceName, Object serverItf,
			Component owner, RssExportHints rssExportHints)
			throws SkeletonGenerationException {
            ....
  }
}

7. References
-------------
 - Maven      : http://maven.apache.org
 - Fractal    : http://fractal.objectweb.org
 - FractalADL : http://fractal.objectweb.org/fractaladl/index.html


For any question concerning the Fractal Binding Factory, please contact
valerio.schiavoni@gmail.com

Questions concerning Fractal in general can be asked to the official mailing-list:
http://mail.ow2.org/wws/info/fractal

Last update of this file: March 22, 2009.
Date of creation of this file: June 23, 2008.
