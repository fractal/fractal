This module provides a plugin for BindingFactory to support RESTful bindings
between components.

This plugin exploits Apache CXF http://cxf.apache.org/ to expose component 
interfaces as RESTful services.
