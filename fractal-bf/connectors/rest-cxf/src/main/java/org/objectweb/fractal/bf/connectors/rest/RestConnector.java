/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 INRIA, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Philippe.Merle@inria.fr
 * 
 * Author: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.rest;

import java.util.Map;
import org.objectweb.fractal.bf.connectors.common.AbstractConnector;

/**
 * A RESTful-Connector can export and bind Fractal interfaces via RESTful web services.
 *
 * @author Philippe Merle
 */
public class RestConnector extends AbstractConnector<RestExportHints, RestBindHints> {

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getPluginIdentifier()
	 */
	public String getPluginIdentifier() {
		return "RESTful";
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getExportHints(Map)
	 */
	public RestExportHints getExportHints(Map<String, Object> initialHints) {
		RestExportHints restExportHints = new RestExportHints();
		restExportHints.setUri((String) initialHints
				.get(RestConnectorConstants.URI));
		log.fine("RestExportHints: " + restExportHints);
		return restExportHints;
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getBindHints(Map)
	 */
	public RestBindHints getBindHints(Map<String, Object> initialHints) {
		log.fine("Raw hints: " + initialHints);
		RestBindHints restBindHints = new RestBindHints();
		restBindHints.setUri((String) initialHints.get(RestConnectorConstants.URI));
		log.fine("RestBindHints: " + restBindHints);
		return restBindHints;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#getSkeletonAdl()
	 */
	protected String getSkeletonAdl() {
        return "org.objectweb.fractal.bf.connectors.rest.RestSkeleton";
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#initSkeletonAdlContext(EH, Map)
	 */
	protected void initSkeletonAdlContext(RestExportHints exportHints, Map<String, Object> context) {
		String uri = exportHints.getUri();
		if (uri == null) {
			uri = RestConnectorConstants.DEFAULT_URI;
			log.warning("Export hints didn't specify an uri, going to use "
					+ uri);
		}
		context.put(RestConnectorConstants.URI, uri);
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#getStubAdl()
	 */
	protected String getStubAdl() {
        return "org.objectweb.fractal.bf.connectors.rest.RestStub";
    }

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#initStubAdlContext(BH, Map)
	 */
	protected void initStubAdlContext(RestBindHints bindHints, Map<String, Object> context) {
		generateStubContentClass(RestStubContent.class, context);
		String uri = bindHints.getUri();
		if (uri == null) {
			uri = RestConnectorConstants.DEFAULT_URI;
			log.warning("Bind hints didn't specify an uri, going to use "
					+ uri);
		}
		context.put(RestConnectorConstants.URI, uri);
	}
}
