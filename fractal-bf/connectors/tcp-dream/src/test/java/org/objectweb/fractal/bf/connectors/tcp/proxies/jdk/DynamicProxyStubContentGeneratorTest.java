package org.objectweb.fractal.bf.connectors.tcp.proxies.jdk;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Proxy;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.dream.Push;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.bf.connectors.Service;
import org.objectweb.fractal.bf.connectors.tcp.proxies.TcpDreamInvocationHandler;
import org.objectweb.fractal.bf.proxies.GeneratorException;

/**
 * Tests for
 * {@link org.objectweb.fractal.bf.connectors.tcp.proxies.jdk.DreamStubContentGenerator}
 * classes.
 */
public class DynamicProxyStubContentGeneratorTest {
	private DreamStubContentGenerator cipg;

	/**
	 * Setup method.
	 */
	@Before
	public void setUp() throws Exception {

		this.cipg = new DreamStubContentGenerator();
	}

	protected Class<?> createProxyForServiceInterface() {
		Class<?> clientProxy = null;
		DreamStubContentGenerator gen = new DreamStubContentGenerator();
		try {
			clientProxy = gen.getStubComponentContentClass(Service.class);
		} catch (GeneratorException e) {
			fail("some error occured while generating the proxy class for Service interface");
		}
		return clientProxy;
	}

	@Test
	public void testGetProxyInstance() throws GeneratorException {

		Object proxyInstance = cipg.getStubComponentContentInstance(
				Service.class, (String) null);

		assertNotNull(proxyInstance);

	}

	@Test
	public void testCanUseBindingControllerInterfaceOnProxyInstance()
			throws GeneratorException {
		Object proxyInstance = cipg.getStubComponentContentInstance(
				Service.class, (String) null);

		assertTrue(BindingController.class.isAssignableFrom(proxyInstance
				.getClass()));

		BindingController bc = (BindingController) proxyInstance;
		String[] clientItfs = bc.listFc();
		assertNotNull("The clientItfs array should not be null", clientItfs);
		assertEquals("The client interface array has not the correct size", 2,
				clientItfs.length);

	}

	@Test
	public void testMultipleInstancesSameInterfaces() {

		Object proxy1 = Proxy.newProxyInstance(Service.class.getClassLoader(),
				new Class[] { Service.class, BindingController.class,
						Push.class }, new TcpDreamInvocationHandler());

		Object proxy2 = Proxy.newProxyInstance(Service.class.getClassLoader(),
				new Class[] { Service.class, BindingController.class,
						Push.class }, new TcpDreamInvocationHandler());

		assertNotSame(proxy1, proxy2);
	}

}
