package org.objectweb.fractal.bf.connectors.tcp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.HashMap;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.dream.channel.AddIPExportIdChunkAttributeController;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.adl.util.ContentControllerHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.util.Fractal;

/**
 * test TransportAgent dream component
 */
public class TransportAgentTest {

	String ORG_OBJECTWEB_FRACTAL_BF_TRANSPORT_AGENT = "org.objectweb.fractal.bf.connectors.tcp.TransportAgent";
	static Factory adlFactory;
	Component transportAgent;

	@BeforeClass
	public static void init() throws ADLException {
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");

		System.setProperty("julia.config",
				"julia-dream-bundled.cfg,julia-dream-activities-bundled.cfg");
		adlFactory = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
	}

	@AfterClass
	public static void destroy() {
		adlFactory = null;
	}

	/**
	 * Test adl definition
	 */
	@Test
	public void testLoadTransportAgent() {
		try {
			final HashMap<String, String> ctx = new HashMap<String, String>();
			ctx
					.put(
							TcpConnectorConstants.LISTEN_PORT,
							TcpConnectorConstants.LISTEN_PORT_DEFAULT_VALUE);
			ctx
					.put(
							"destinationPort",
							TcpConnectorConstants.TRANSPORT_AGENT_CLIENT_SIDE_CHAN_IN_LISTEN_PORT);
			ctx.put("hostname", "localhost");
			transportAgent = (Component) adlFactory.newComponent(
					ORG_OBJECTWEB_FRACTAL_BF_TRANSPORT_AGENT, ctx);
			assertNotNull(transportAgent);

			// find AddDestination component
			Component addDest = ContentControllerHelper.getSubComponentByName(
					transportAgent, "AddDestination");
			assertNotNull(addDest);
			// verify port value
			AddIPExportIdChunkAttributeController ctrl = (AddIPExportIdChunkAttributeController) Fractal
					.getAttributeController(addDest);
			assertEquals(
					TcpConnectorConstants.TRANSPORT_AGENT_CLIENT_SIDE_CHAN_IN_LISTEN_PORT,
					String.valueOf(ctrl.getPort()));

		} catch (Exception e) {
			e.printStackTrace();
			fail("some exception " + e.getMessage());
		}
	}

}
