package org.objectweb.fractal.bf.connectors.tcp;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.util.Fractal;

public class TcpConnectorTest {

	private static Factory adlFactory;

	private Component client;

	protected TcpConnector tcpConnector;

	private Component theStub;

	@BeforeClass
	public static void init() throws ADLException {
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");

		System.setProperty("julia.config",
				"julia-dream-bundled.cfg,julia-dream-activities-bundled.cfg");
		adlFactory = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
	}

	/**
	 * This is done to avoid weird behaviours when executing the suite of tests:
	 * apparently the adl factory is not loaded systematically by the getFactory
	 * method: from experimental tests.
	 */
	@AfterClass
	public static void destroy() {
		adlFactory = null;
	}

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Before
	public void setUp() throws Exception {

		client = (Component) adlFactory.newComponent(
				"org.objectweb.fractal.bf.connectors.Client", null);
		assertNotNull(client);

		tcpConnector = new TcpConnector();
	}

	/**
	 * Test method for
	 * {@link org.objectweb.fractal.bf.connectors.tcp.TcpConnector#createStub(org.objectweb.fractal.api.Component, java.lang.String, java.util.Map)}.
	 * 
	 * @throws BindingFactoryException
	 * @throws NoSuchInterfaceException
	 */
	@Test
	public void testCreateStub() throws BindingFactoryException,
			NoSuchInterfaceException {
		Map<String, String> hints = new HashMap<String, String>();
		hints.put(TcpConnectorConstants.REMOTE_INTERFACE_NAME, "service");
		theStub = tcpConnector.createStub(client, "service", hints);
		assertNotNull(theStub);
	}

	@After
	public void cleanUp() throws IllegalLifeCycleException,
			NoSuchInterfaceException {
		tcpConnector = null;
		Fractal.getLifeCycleController(theStub).stopFc();
		Fractal.getLifeCycleController(client).stopFc();
	}

}
