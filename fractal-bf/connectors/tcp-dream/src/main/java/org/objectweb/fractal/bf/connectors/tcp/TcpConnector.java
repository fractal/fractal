package org.objectweb.fractal.bf.connectors.tcp;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.objectweb.dream.adl.FactoryFactory;
import org.objectweb.dream.channel.AddIPExportIdChunkAttributeController;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.util.ContentControllerHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.bf.AbstractBindingFactoryPlugin;
import org.objectweb.fractal.bf.BindHints;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.BindingFactoryExportException;
import org.objectweb.fractal.bf.BindingFactoryPlugin;
import org.objectweb.fractal.bf.ExportHints;
import org.objectweb.fractal.bf.SkeletonGenerationException;

import org.objectweb.fractal.bf.connectors.tcp.proxies.TcpConnectorStubComponentFactoryImpl;
import org.objectweb.fractal.bf.connectors.tcp.proxies.jdk.DreamStubContentGenerator;
import org.objectweb.fractal.bf.proxies.StubComponentFactory;

import org.objectweb.fractal.util.Fractal;

/**
 * Support tcp-based connectors using Dream.
 * 
 * @author Valerio Schiavoni <valerio.schiavoni@gmail.com>
 */
public class TcpConnector extends
		AbstractBindingFactoryPlugin<TcpExportHints, TcpBindHints> {

	final Logger log = Logger.getLogger(TcpConnector.class.getName());

	public TcpConnector(Map<String, String> hints) {
		this.initialHints = hints;
		try {
			adlFactory = FactoryFactory.getFactory();
		} catch (ADLException e) {
			e.printStackTrace();

		}
	}

	/**
	 * The DreamADL factory
	 */
	Factory adlFactory;

	/**
	 * Load the TransportAgent component. This component is used by the
	 * generated stub: in particular the stub is bound to the message-manager
	 * server interface of the TransportAgent.
	 * 
	 * @param bindHints
	 *            a map of hints to load the transport agent
	 * @return the Transport Agent
	 * @throws ADLException
	 *             if some error occurs while loading the ADL definition for the
	 *             TransportAgent component
	 */
	private Component loadClientSideTransportAgent(Map<String, String> bindHints)
			throws ADLException {

		// the port the server is listening to
		String serverPort = null;
		// the port the server should send replies to
		String clientPort = null;
		// the host name
		String hostName = null;

		if (adlFactory == null) {
			adlFactory = FactoryFactory.getFactory();
		}
		if (bindHints != null) {
			serverPort = bindHints.get(TcpConnectorConstants.LISTEN_PORT);
			clientPort = bindHints.get(TcpConnectorConstants.REPLY_TO);
			hostName = bindHints.get(TcpConnectorConstants.HOST_NAME);
		}

		if (serverPort == null) {
			// fall back to dream default
			serverPort = TcpConnectorConstants.LISTEN_PORT_DEFAULT_VALUE;
		}
		if (clientPort == null) {
			// fall back to dream default
			clientPort = TcpConnectorConstants.TRANSPORT_AGENT_CLIENT_SIDE_CHAN_IN_LISTEN_PORT;
		}
		if (hostName == null) {
			hostName = TcpConnectorConstants.HOST_NAME_DEFAULT_VALUE;
		}

		Map<String, String> ctx = new HashMap<String, String>();
		// the argument 'destinationPort' is used in the TransportAgent.fractal
		ctx.put(TcpConnectorConstants.DESTINATION_PORT, serverPort);
		ctx.put("hostname", "localhost");
		// the destination port for the client is the listen-port for the server
		ctx.put(TcpConnectorConstants.LISTEN_PORT, clientPort);

		return (Component) adlFactory.newComponent(
				TcpConnectorConstants.TRANSPORT_AGENT_ADL, ctx);

	}

	/**
	 * This method returns the a stub component.
	 * 
	 * @param clientComponent
	 *            owner of the interface to stub
	 * @param itfName
	 *            the name of the interface to stub
	 * @param hints
	 *            a map of hints that can be used to create the stub
	 * @throws BindingFactoryException
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#createExportId(org.objectweb.fractal.api.Component)
	 */
	@Override
	public Component createStub(Component clientComponent, String itfName,
			Map<String, String> hints) throws BindingFactoryException {

		String remoteItfName = hints
				.get(TcpConnectorConstants.REMOTE_INTERFACE_NAME);
		log
				.info("Building tcp/dream stub targeting remote interface with name: "
						+ remoteItfName);

		Component stub;
		InterfaceType interfaceType;
		try {
			ComponentType componentType = (ComponentType) (clientComponent)
					.getFcType();
			log.finest("Fractal ComponentType for stub: "
					+ componentType.toString());
			interfaceType = componentType.getFcInterfaceType(itfName);

			StubComponentFactory proxyComponentFactory = new TcpConnectorStubComponentFactoryImpl(
					new DreamStubContentGenerator());

			stub = proxyComponentFactory.createStubComponent(interfaceType,
					remoteItfName);

			log.finest("Dream stub generated: " + stub.toString());

		} catch (Exception e) {
			e.printStackTrace();
			throw new BindingFactoryException(
					"Some error occurred while creating a stub with TCP/Dream plugin",
					e);
		}

		Component compositeWrappingRealStub = wrap(stub, interfaceType, itfName);

		/*
		 * the stub component is added to a composite component, whose type is
		 * the same as the stub component
		 */

		try {
			Fractal.getNameController(compositeWrappingRealStub).setFcName(
					"BFTcpStubConnector");
		} catch (NoSuchInterfaceException e) {
			e.printStackTrace();
			throw new BindingFactoryException(
					"Impossible to set a name to the tcp stub using the NameController",
					e);
		}

		return compositeWrappingRealStub;
	}

	/**
	 * @param stub
	 * @param interfaceType
	 *            TODO
	 * @param itfName
	 *            required to setup the export binding between the subcomponent
	 *            and the enclosing composite
	 * @return
	 */
	private Component wrap(Component stub, InterfaceType interfaceType,
			String itfName) {

		Component toReturn;

		try {
			Component bootstrap = org.objectweb.fractal.api.Fractal
					.getBootstrapComponent();
			TypeFactory tf = Fractal.getTypeFactory(bootstrap);
			GenericFactory gf = Fractal.getGenericFactory(bootstrap);

			toReturn = gf.newFcInstance(tf
					.createFcType(new InterfaceType[] { Util
							.symmetricInterfaceType(interfaceType) }),
					"dreamComposite", null);
			// add the stub in the wrapper component
			Fractal.getContentController(toReturn).addFcSubComponent(stub);
			// export binding
			Fractal.getBindingController(toReturn).bindFc(itfName,
					stub.getFcInterface(itfName));

		} catch (Exception e) {
			// if some error occurs, return the stub directly
			toReturn = stub;
		}

		return toReturn;
	}

	@Override
	public String toString() {
		return "TCP/Dream Connector Plugin";
	}

	/**
	 * XXX TO UPDATE <p/> The stub finalization procedure for the TcpConnector
	 * consists in:
	 * <ol>
	 * <li> load a TransportAgent component
	 * <li> adding the transport agent into the same component containing the st
	 * of the component: the membrane is also the parent component of the stub
	 * <li> bind the message-manager client interface of the TcpConnector stub
	 * to the message-manager server interface of the MessageManager
	 * <li> bind the out-push client interface of the TcpConnector stub to the
	 * in-push server interface of the TransportAgent
	 * <li> start the stub and the transport agent component
	 * </ol>
	 * 
	 * @param stub
	 *            the stub component, previously generated by
	 *            {@link TcpConnector#createStub(Component, String, Map)} method
	 * @param finalizationHints
	 *            a map of hints usable while finalizing the stub
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#finalizeStub(org.objectweb.fractal.api.Component,
	 *      Map)
	 */
	@Override
	public void finalizeStub(Component stub,
			Map<String, String> finalizationHints)
			throws BindingFactoryException {
		try {

			Component transportAgent = loadClientSideTransportAgent(finalizationHints);
			// get this component before adding the TA into the composite
			// component
			Component concreteDreamStub = Fractal.getContentController(stub)
					.getFcSubComponents()[0];
			// add the TA in the same composite
			Fractal.getContentController(stub)
					.addFcSubComponent(transportAgent);

			String address = "";
			String port = finalizationHints
					.get(TcpConnectorConstants.LISTEN_PORT);

			log.fine("Stub will direct messages to: " + address + ":" + port);

			// configure the transport agent
			Component addDestination = ContentControllerHelper
					.getSubComponentByName(transportAgent, "AddDestination");

			AddIPExportIdChunkAttributeController addDestinationAC = (AddIPExportIdChunkAttributeController) Fractal
					.getAttributeController(addDestination);
			addDestinationAC.setChunkName("destination");
			addDestinationAC.setHostname(address);
			addDestinationAC.setPort(Integer.valueOf(port));
			Interface messageManager = (Interface) transportAgent
					.getFcInterface("message-manager");

			BindingController stubBC = Fractal
					.getBindingController(concreteDreamStub);

			stubBC.bindFc("message-manager", messageManager);
			stubBC.bindFc("out-push", transportAgent.getFcInterface("in-push"));

			Fractal.getBindingController(transportAgent).bindFc("out-push",
					concreteDreamStub.getFcInterface("in-push"));

			Fractal.getLifeCycleController(stub).startFc();
		} catch (Exception e) {
			e.printStackTrace();
			throw new BindingFactoryException(e);
		}
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#finalizeSkeleton(org.objectweb.fractal.api.Component,
	 *      java.util.Map)
	 */
	public void finalizeSkeleton(Component skel,
			Map<String, String> finalizationHints)
			throws BindingFactoryException {
		// do nothing

	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getStubComponentFactory()
	 */
	public StubComponentFactory getStubComponentFactory() {
		return new TcpConnectorStubComponentFactoryImpl(
				new DreamStubContentGenerator());
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#createSkel(java.lang.String,
	 *      java.lang.Object, org.objectweb.fractal.api.Component,
	 *      org.objectweb.fractal.bf.ExportHints)
	 */
	public Component createSkel(String interfaceName, Object serverItf,
			Component owner, TcpExportHints exportHints)
			throws SkeletonGenerationException {
		log.info("TcpConnector - Creating skeleton.");

		Component tcpDreamSkel;
		try {
			Map<String, String> ctx = new HashMap<String, String>();
			ctx.put(TcpConnectorConstants.LISTEN_PORT, exportHints
					.getListenPort());
			ctx.put(TcpConnectorConstants.HOST_NAME, exportHints.getHostname());
			tcpDreamSkel = (Component) adlFactory.newComponent(
					TcpConnectorConstants.TCP_BINDING_SERVER_ADL, ctx);

		} catch (ADLException e) {
			throw new SkeletonGenerationException(
					"Cannot load skeleton ADL definition for TCP/Dream plugin",
					e);
		}

		return tcpDreamSkel;
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getBindHints()
	 */
	public BindHints getBindHints() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("todo");
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getExportHints()
	 */
	public ExportHints getExportHints() {
		TcpExportHints teh = new TcpExportHints();
		teh.setListenPort(computeListenPort());
		teh.setHostname(computeHostname());
		return teh;
	}

	/**
	 * @return the hostname to use by the transport agent as host identifier.
	 */
	private String computeHostname() {
		String hostname = initialHints.get(TcpConnectorConstants.HOST_NAME);
		if (hostname == null || hostname.equalsIgnoreCase("")) {
			hostname = TcpConnectorConstants.HOST_NAME_DEFAULT_VALUE;
		}
		return hostname;
	}

	/**
	 * @return the listen port to use.
	 */
	private String computeListenPort() {
		String listenPort = initialHints.get(TcpConnectorConstants.LISTEN_PORT);
		if (listenPort == null || listenPort.equalsIgnoreCase("")) {
			listenPort = TcpConnectorConstants.LISTEN_PORT_DEFAULT_VALUE;
		}
		return listenPort;
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getPluginIdentifier()
	 */
	public String getPluginIdentifier() {
		return "tcp";
	}

}