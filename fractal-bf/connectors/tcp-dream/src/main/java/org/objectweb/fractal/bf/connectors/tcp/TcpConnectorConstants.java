package org.objectweb.fractal.bf.connectors.tcp;

/**
 * Defines constants used in the BindingFactory.
 * 
 * @author valerio.schiavoni@gmail.com
 */
public interface TcpConnectorConstants {

	String REPLY_TO = "replyTo";
	String LISTEN_PORT = "listenPort";
	String DESTINATION_PORT = "destinationPort";

	/**
	 * The defualt listen is 1600.
	 */
	String LISTEN_PORT_DEFAULT_VALUE = "1600";
	/**
	 * Defines the port used by the client side Transport Agent to receive
	 * messages from the server. This port must be the same one as defined by
	 * the {@link LISTEN_PORT_DEFAULT_VALUE} constant.
	 */
	String TRANSPORT_AGENT_CLIENT_SIDE_CHAN_IN_LISTEN_PORT = "1601";

	/**
	 * The ADL definition file defining the Transport Agent.
	 */
	String TRANSPORT_AGENT_ADL = "org.objectweb.fractal.bf.connectors.tcp.TransportAgent";
	/**
	 * The ADL definition file defining the TCP Binding Server.
	 */
	String TCP_BINDING_SERVER_ADL = "org.objectweb.fractal.bf.connectors.tcp.TCPBindingServer";

	String REMOTE_INTERFACE_NAME = "remoteInterfaceName";
	String HOST_NAME = "hostName";
	String HOST_NAME_DEFAULT_VALUE = "localhost";

}
