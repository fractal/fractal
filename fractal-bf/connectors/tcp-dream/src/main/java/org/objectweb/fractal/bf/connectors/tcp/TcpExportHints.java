/**
 * Author: Valerio Schiavoni <valerio.schiavoni@gmail.com>
 */
package org.objectweb.fractal.bf.connectors.tcp;

import org.objectweb.fractal.bf.ExportHints;

/**
 * The TCP export hints.
 */
public class TcpExportHints implements ExportHints {
	private String listenPort;
	private String hostname;

	public final String getHostname() {
		return hostname;
	}

	public final void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public final String getListenPort() {
		return listenPort;
	}

	public final void setListenPort(String listenPort) {
		this.listenPort = listenPort;
	}
}
