package org.objectweb.fractal.bf.connectors.tcp.proxies;

import org.objectweb.dream.PushException;
import org.objectweb.dream.message.Message;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.connectors.tcp.bindings.ClientBridge;
import org.objectweb.fractal.bf.proxies.AbstractDefaultRemoteCallInvocationHandler;

import java.lang.reflect.Method;
import java.util.logging.Logger;

public class TcpDreamInvocationHandler extends
		AbstractDefaultRemoteCallInvocationHandler<ClientBridge> {

	final Logger log = Logger.getLogger(TcpDreamInvocationHandler.class
			.getCanonicalName());

	/**
	 * The name of the (remote) interface this handler will direct messages to.
	 */
	private String itfName;

	/**
	 * Use ClientBridge.
	 */
	public TcpDreamInvocationHandler() {
		super(new ClientBridge());
	}

	/**
	 * Use ClientBridge.
	 */
	public TcpDreamInvocationHandler(String itfName) {
		super(new ClientBridge());
		this.itfName = itfName;
	}

	/**
	 * Initialize the client bridge.
	 */
	public TcpDreamInvocationHandler(ClientBridge bridge) {
		super(bridge);
	}

	@Override
	public Object handleMethod(Method method, Object[] args, String methodName) {

		Object result = null;

		if (methodName.equals("push")) // call made by the transport-agent when
										// the result comes back
		{
			log.fine("Argument length: " + args.length);
			try {
				clientBridge.push((Message) args[0]);
			} catch (PushException e) {
				e.printStackTrace();
			}
		} else {
			try {
				result = clientBridge.send(this.itfName, method, args);
			} catch (BindingFactoryException e) {
				e.printStackTrace();
			}
		}

		if (result != null) {
			log.finer("Method invoked: " + method.toString()
					+ " Result object: " + result + " (class: "
					+ result.getClass().getCanonicalName() + ")");
			return result;
		} else {
			log.warning("Result is NULL");
			return null;
		}
	}

}
