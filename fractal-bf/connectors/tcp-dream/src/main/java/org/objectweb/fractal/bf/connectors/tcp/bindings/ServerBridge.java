package org.objectweb.fractal.bf.connectors.tcp.bindings;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Logger;

import org.objectweb.dream.Push;
import org.objectweb.dream.PushException;
import org.objectweb.dream.message.ChunkFactoryReference;
import org.objectweb.dream.message.Message;
import org.objectweb.dream.message.MessageManagerType;
import org.objectweb.dream.protocol.ExportIdentifier;
import org.objectweb.dream.protocol.ExportIdentifierChunk;
import org.objectweb.dream.protocol.IPExportIdentifier;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.bf.connectors.tcp.chunks.InvokeMethodChunk;
import org.objectweb.fractal.bf.connectors.tcp.chunks.MethodInvocationDescriptor;
import org.objectweb.fractal.bf.connectors.tcp.chunks.ResultChunk;
import org.objectweb.fractal.fraclet.annotation.annotations.FractalComponent;
import org.objectweb.fractal.fraclet.annotation.annotations.Interface;
import org.objectweb.fractal.fraclet.annotation.annotations.Provides;
import org.objectweb.fractal.fraclet.annotation.annotations.Requires;
import org.objectweb.fractal.fraclet.annotation.annotations.Service;
import org.objectweb.fractal.fraclet.annotation.annotations.type.Contingency;

/**
 * A server bridge can receives Dream {@link Message}s and send them back via the underlying protocols.
 *
 * @author valerio.schiavoni@gmail.com
 * @see Message
 */
@FractalComponent(controllerDesc = "primitive")
@Provides(interfaces = {@Interface(name = "in-push", signature = Push.class)})
public class ServerBridge implements Push {

    final Logger log = Logger.getLogger(ServerBridge.class.getCanonicalName());
    // ------------------------------------------------------------------------
    // Client Interfaces
    // ------------------------------------------------------------------------
    
    /**
     * Component reference
     */
    @Service(name = "component")
    protected Component weaveableC;

    /**
     * Component content
     */
    @Requires(name = "delegate", contingency = Contingency.OPTIONAL)
    Component delegate;

    /**
     * Interface to push result of the call
     */
    @Requires(name = "out-push")
    Push outPushItf;

    @Requires(name = "message-manager")
    MessageManagerType messageManagerItf;

    private ChunkFactoryReference<ResultChunk> resultChunkFactory = null;

    protected ChunkFactoryReference<ExportIdentifierChunk> exportIdChunkFactory = null;


    // ------------------------------------------------------------------------
    // Push Interface Implementation
    // ------------------------------------------------------------------------
    /**
     * Receive a message: messages carry a method invocation description, and other informations to be used to
     * properly send back the response.
     *
     * @param message a Dream message
     * @see Message
     */
    public void push(Message message) throws PushException {

        MethodInvocationDescriptor mid = extractMidFrom(message);

        try {

            //get the functional interface
            String itfName = mid.getItfName();
            Object serverItf = delegate.getFcInterface(itfName);

            Method method = serverItf.getClass().getMethod(mid.getName(),
                    mid.getParameterTypes());
            log.fine("Going to invoke method: " + method + " on the exported server interface");

            // dispatch method call to business delegate
            Object result = method.invoke(serverItf, mid.getArgs());

            Class<?> returnType = method.getReturnType();
            // if method doesn't return void..
            if (!returnType.equals(Void.TYPE)) {

                if (resultChunkFactory == null) {
                    resultChunkFactory = messageManagerItf
                            .getChunkFactory(ResultChunk.class);
                }

                ResultChunk resultChunk = messageManagerItf
                        .createChunk(resultChunkFactory);

                resultChunk.setResult(result);

                messageManagerItf.addChunk(message, ResultChunk.DEFAULT_NAME,
                        resultChunk);

                //add a "destination" chunk, to properly direct messages back to service consumers

                ExportIdentifier exportIdentifier = new IPExportIdentifier("localhost", 8080); //XXX hard-coding
                log.fine("Creating exportIdentifier to correctly send the result message to the client: " + exportIdentifier);
                log.warning("The exportIdentifier contains hard-coded values (localhost:8080). TO BE FIXED ");

                if (exportIdChunkFactory == null) {
                    exportIdChunkFactory = messageManagerItf
                            .getChunkFactory(ExportIdentifierChunk.class);
                }
                ExportIdentifierChunk exportIdentifierChunk = messageManagerItf
                        .createChunk(exportIdChunkFactory);
                exportIdentifierChunk.setExportIdentifier(exportIdentifier);
                messageManagerItf.addChunk(message, "destination",
                        exportIdentifierChunk);

                log.fine("Going to push the message carrying out the result of the RPC pushed back into the tcp stack");

                outPushItf.push(message);


            }

            // TODO recycle ?

        }
        catch (SecurityException e) {

            e.printStackTrace();
        }
        catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        catch (NoSuchInterfaceException e) {

            e.printStackTrace();
        }

    }


    /**
     * Extract a {@link MethodInvocationDescriptor} from the message.
     * 
     * @param message the message
     * @return the 
     * @throws PushException
     */
    protected MethodInvocationDescriptor extractMidFrom(Message message)
        throws PushException
    {
      // 1. Transform the message -> call
      InvokeMethodChunk chunk = (InvokeMethodChunk) messageManagerItf.getChunk(
              message, InvokeMethodChunk.DEFAULT_NAME);
      if (chunk == null) {
          throw new PushException("Unable to find InvokeMethod chunk");
      }
      // 2. Make the call : contentItf.<method_to_call>

      MethodInvocationDescriptor mid = chunk.getMethodInvocationDescriptor();
      return mid;
    }

}
