package org.objectweb.fractal.bf.connectors.tcp.chunks;

import org.objectweb.dream.message.AbstractChunk;
import org.objectweb.dream.util.Util;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.OptionalDataException;

/**
 * A concrete {@link AbstractChunk} to model method invocation. This method
 * transports remotely a method invocation, carrying informations like:
 * <ol>
 * <li> the name of the method
 * <li> the parameters
 * </ol>
 *
 * @author valerio.schiavoni@gmail.com
 */
public class InvokeMethodChunk extends AbstractChunk<InvokeMethodChunk> {

    private static final long serialVersionUID = -4078053737179499607L;
    /**
     * The name of the chunk
     */
    public static final String DEFAULT_NAME = "invoke-method-chunk";

    private MethodInvocationDescriptor methodInvocationDescriptor;

    @Override
    protected InvokeMethodChunk newChunk() {

        return new InvokeMethodChunk();
    }

    @Override
    protected void transfertStateTo(InvokeMethodChunk arg0) {

        arg0.methodInvocationDescriptor = methodInvocationDescriptor;

    }

    /**
     *
     */
    public void readExternal(ObjectInput in) throws IOException,
            ClassNotFoundException {
        try {
            Object o = Util.readObject(in);
            this.methodInvocationDescriptor = (MethodInvocationDescriptor) o;
          
        }
        catch (OptionalDataException e) {
            e.printStackTrace();
            throw new IOException(
                    "Some error occurred while reading the ObjectInput for the InvokeMethodChunk");
        }
    }

    /**
     *
     */
    public void writeExternal(ObjectOutput out) throws IOException {
        Util.writeObject(out, methodInvocationDescriptor);
    }

    /**
     *
     */
    public void recycle() {
        this.methodInvocationDescriptor = null;

    }

    /**
     * @param methodInvocationDescriptor
     */
    public void setMethodInvocationDescriptor(
            MethodInvocationDescriptor methodInvocationDescriptor) {
        this.methodInvocationDescriptor = methodInvocationDescriptor;
    }

    /**
     * @return
     */
    public MethodInvocationDescriptor getMethodInvocationDescriptor() {
        return methodInvocationDescriptor;
    }

}
