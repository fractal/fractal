package org.objectweb.fractal.bf.connectors.tcp.chunks;

import org.objectweb.dream.message.AbstractChunk;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * A concrete {@link AbstractChunk} used to transport the result of a method invocation 
 * over the network.
 *
 * @author valerio.schiavoni@gmail.com
 */
public class ResultChunk extends AbstractChunk<ResultChunk> {

    private static final long serialVersionUID = 6783808998824911748L;
    /**
     * The default name for this chunk.
     */
    public static final String DEFAULT_NAME = "result-chunk";

    private Object result;

    @Override
    protected ResultChunk newChunk() {
        return new ResultChunk();
    }

    @Override
    protected void transfertStateTo(ResultChunk arg0) {
        arg0.setResult(this.result);

    }

    /**
     *
     */
    public void readExternal(ObjectInput arg0) throws IOException,
            ClassNotFoundException {

        result = arg0.readObject();

    }

    /**
     *
     */
    public void writeExternal(ObjectOutput arg0) throws IOException {

        arg0.writeObject(this.result);

    }

    /**
     *
     */
    public void recycle() {
        result = null;
    }

    /**
     * @return
     */
    public Object getResult() {
        return result;
    }

    /**
     * @param result
     */
    public void setResult(Object result) {
        this.result = result;
    }

}
