package org.objectweb.fractal.bf.connectors.tcp.bindings;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.objectweb.dream.Push;
import org.objectweb.dream.PushException;
import org.objectweb.dream.message.ChunkFactoryReference;
import org.objectweb.dream.message.Message;
import org.objectweb.dream.message.MessageManagerType;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.connectors.tcp.chunks.InvokeMethodChunk;
import org.objectweb.fractal.bf.connectors.tcp.chunks.MethodInvocationDescriptor;
import org.objectweb.fractal.bf.connectors.tcp.chunks.ResultChunk;
import org.objectweb.fractal.bf.proxies.Bridge;

public class ClientBridge implements BindingController, Bridge {

	final Logger log = Logger.getLogger(ClientBridge.class.getCanonicalName());

	/**
	 * client interface to push dream messages out
	 */
	private Push outPushItf;

	// name = "message-manager"
	private MessageManagerType messageManagerItf;

	private ChunkFactoryReference<org.objectweb.fractal.bf.connectors.tcp.chunks.InvokeMethodChunk> invokeMethodChunkFactory = null;

	final Map<MethodInvocationDescriptor, ResultReceived> mid2result = new HashMap<MethodInvocationDescriptor, ResultReceived>();

	/**
	 * Transform a method invocation to an message, to be sent over a
	 * communication channel.
	 * 
	 * @param itfName
	 *            the name of the interface to be used to invoke this method
	 * @param m
	 *            the method to invoke
	 * @param args
	 *            the arguments for the method to invoke
	 */
	public Object send(String itfName, Method m, Object[] args)
			throws BindingFactoryException {

		log.fine("Method to be incapsulated into Dream message:"
				+ m.toGenericString());
		log.finer("Arguments for the method: " + Arrays.deepToString(args));

		MethodInvocationDescriptor mid = new MethodInvocationDescriptor();

		mid.setItfName(itfName);

		mid.setName(m.getName());
		mid.setParameterTypes(m.getParameterTypes());
		if (args == null) {
			log
					.finer("Method arguments were null (or zero-parameter arguments), building up an empty object array");
			mid.setArgs(new Object[] {});
		} else {

			mid.setArgs(args);
		}
		// register this mid
		try {
			register(mid, m.getReturnType());
			log.finer("MID object " + mid + " has been registered");
		} catch (InstantiationException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		}
		// 1. Transform the call -> message
		Message message = messageManagerItf.createMessage();

		if (invokeMethodChunkFactory == null) {
			invokeMethodChunkFactory = messageManagerItf
					.getChunkFactory(InvokeMethodChunk.class);
		}
		InvokeMethodChunk resultChunk = messageManagerItf
				.createChunk(invokeMethodChunkFactory);

		// create a chunk to transport the method invocation
		resultChunk.setMethodInvocationDescriptor(mid);

		messageManagerItf.addChunk(message, InvokeMethodChunk.DEFAULT_NAME,
				resultChunk);

		// TODO add a destination chunk, so that the server can send back the
		// message to the appropriate client port

		try {
			outPushItf.push(message); // push message down the stack
		} catch (PushException e) {
			throw new BindingFactoryException(
					"An error occurred in sending the message over the channel.",
					e);
		}

		Object res = null;

		if (!m.getReturnType().equals(Void.TYPE)) {

			ResultReceived resultTransporter = mid2result.get(mid);
			// blocking call, waiting for the producer to produce the result...
			res = resultTransporter.getResult();
			synchronized (res) { // acquire lock
				while (!resultTransporter.isReceived()) {
					try {
						res.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

			res = resultTransporter.getResult(); // refreshing ?

			log.fine("Result : " + res);
		}

		// result received, let's return it
		return res;
	}

	/**
	 * Register a ResultReceived instance object.
	 * 
	 * @param mid
	 *            the {@link MethodInvocationDescriptor} instance this result is
	 *            for
	 * @param returntype
	 *            the type of the instance object
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	private void register(MethodInvocationDescriptor mid, Class returntype)
			throws InstantiationException, IllegalAccessException {
		if (!returntype.equals(Void.TYPE)) {

			Object resultPlaceHolder = new Object();

			mid2result.put(mid, new ResultReceived(false, resultPlaceHolder)); // returntype.newInstance()
		}
	}

	/**
	 * This method is invoked when a message-chunk arrives carrying out the
	 * result of a method invocation.
	 */
	public void push(Message msg) throws PushException {

		log.fine("ClientBridge received a message..." + msg);

		ResultChunk chunk = (ResultChunk) messageManagerItf.getChunk(msg,
				ResultChunk.DEFAULT_NAME);
		InvokeMethodChunk invokeChunk = (InvokeMethodChunk) messageManagerItf
				.getChunk(msg, InvokeMethodChunk.DEFAULT_NAME);

		MethodInvocationDescriptor mid = invokeChunk
				.getMethodInvocationDescriptor();

		// get the object that stores the result of this invocation
		ResultReceived result = mid2result.get(mid);

		if (result == null) {
			throw new RuntimeException(
					"the shared object variable has not been set previously");
		}
		Object lock = result.getResult();
		synchronized (lock) {
			// assign the result of the invocation carried by the chunk
			final Object resultInChunk = chunk.getResult();

			result.setResult(resultInChunk);
			result.setReceived(true);

			lock.notifyAll();

		}

	}

	/**
	 * private class modeling the reception of a remote method invocation
	 * result.
	 * 
	 * @author Valerio Schiavoni <valerio.schiavoni@gmail.com>
	 */
	private class ResultReceived {
		boolean received = false;
		Object result;
		Class<?> resultClass;

		/**
		 * @return the received
		 */
		public boolean isReceived() {
			return received;
		}

		/**
		 * @param received
		 *            the received to set
		 */
		public void setReceived(boolean received) {
			this.received = received;
		}

		/**
		 * @return the result
		 */
		public Object getResult() {
			return result;
		}

		/**
		 * @param result
		 *            the result to set
		 */
		public void setResult(Object result) {
			this.result = result;
		}

		/**
		 * @param received
		 * @param result
		 */
		public ResultReceived(boolean received, Object result) {
			super();
			this.received = received;
			this.result = result;
			this.resultClass = result.getClass();
		}

		public Class<?> getResultClass() {
			return resultClass;
		}

		public void setResultClass(Class<?> resultClass) {
			this.resultClass = resultClass;
		}
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.objectweb.fractal.api.control.BindingController#bindFc(java.lang.String,java.lang.Object)
	 */
	public void bindFc(String clientItfName, Object serverItf)
			throws NoSuchInterfaceException, IllegalBindingException,
			IllegalLifeCycleException {
		if (clientItfName.equals("out-push")) {
			if (!(Push.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(
						((("server interfaces connected to " + clientItfName) + " must be instances of ") + (Push.class
								.getName())));
			}
			outPushItf = ((Push) (serverItf));
			return;
		}
		if (clientItfName.equals("message-manager")) {
			if (!(MessageManagerType.class.isAssignableFrom(serverItf
					.getClass()))) {
				throw new IllegalBindingException(
						((("server interfaces connected to " + clientItfName) + " must be instances of ") + (MessageManagerType.class
								.getName())));
			}
			messageManagerItf = ((MessageManagerType) (serverItf));
			return;
		}
		throw new NoSuchInterfaceException(
				(("Client interface \'" + clientItfName) + "\' is undefined."));

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.objectweb.fractal.api.control.BindingController#listFc()
	 */
	public String[] listFc() {
		return new String[] { "out-push", "message-manager" };
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.objectweb.fractal.api.control.BindingController#lookupFc(java.lang.String)
	 */
	public Object lookupFc(String clientItfName)
			throws NoSuchInterfaceException {
		if (clientItfName.equals("out-push")) {
			return outPushItf;
		}
		if (clientItfName.equals("message-manager")) {
			return messageManagerItf;
		}
		throw new NoSuchInterfaceException(
				(("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.objectweb.fractal.api.control.BindingController#unbindFc(java.lang.String)
	 */
	public void unbindFc(String clientItfName) throws NoSuchInterfaceException,
			IllegalBindingException, IllegalLifeCycleException {
		if (clientItfName.equals("out-push")) {
			outPushItf = null;
			return;
		}
		if (clientItfName.equals("message-manager")) {
			messageManagerItf = null;
			return;
		}
		throw new NoSuchInterfaceException(
				(("Client interface \'" + clientItfName) + "\' is undefined."));

	}

}
