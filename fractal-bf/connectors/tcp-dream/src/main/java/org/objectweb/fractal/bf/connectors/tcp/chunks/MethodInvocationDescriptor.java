package org.objectweb.fractal.bf.connectors.tcp.chunks;

import org.objectweb.dream.util.Util;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Arrays;
import java.util.Collection;

/**
 * An object (plain old java object) modeling a method invocation. Instances of this class are designed to
 * be transported through {@link InvokeMethodChunk} from a service consumer to a service provider. For this
 * reason, this object implements {@link Externalizable}.
 *
 */
public class MethodInvocationDescriptor implements Externalizable {

    private static final long serialVersionUID = 1L;

    String itfName;             //the name of the interface owning the method to call
    String name;                 // method name
    Class<?>[] parameterTypes;       // parameter types
    Object[] args;                 // arguments


    /**
     * @return
     */
    public Object[] getArgs() {
        return args;
    }

    /**
     * @param args
     */
    public void setArgs(Object[] args) {
        this.args = args;
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return
     */
    public Class<?>[] getParameterTypes() {
        return parameterTypes;
    }

    /**
     * @param parameterTypes
     */
    public void setParameterTypes(Class<?>[] parameterTypes) {
        this.parameterTypes = parameterTypes;
    }

    /**
     *
     */
    public void readExternal(ObjectInput in) throws IOException,
            ClassNotFoundException {

        this.itfName = in.readUTF();
        this.name = in.readUTF();

        Collection<Class> cc = Util.readExternalObjectArray(in, Class.class);
        this.parameterTypes = cc.toArray(new Class[]{});

        this.args = Util.readExternalObjectArray(in);
    }

    /**
     *
     */
    public void writeExternal(ObjectOutput out) throws IOException {

        out.writeUTF(this.itfName);
        out.writeUTF(this.name);
        Util.writeExternalObjectArray(out, this.parameterTypes);

        Util.writeExternalObjectArray(out, this.args);

    }

    public String toString() {
        return "Interface Name: " + itfName + "\n Method name: " + name + "\n ParameterTypes: "
                + parameterTypes.toString();
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(args);
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + Arrays.hashCode(parameterTypes);
        return result;
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final MethodInvocationDescriptor other = (MethodInvocationDescriptor) obj;
        if (!Arrays.equals(args, other.args))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return Arrays.equals(parameterTypes, other.parameterTypes);
    }

    /**
     * @return the itfName
     */
    public String getItfName() {
        return itfName;
    }

    /**
     * @param itfName the itfName to set
     */
    public void setItfName(String itfName) {
        this.itfName = itfName;
    }

}
