package org.objectweb.fractal.bf.connectors.tcp.proxies.jdk;

import org.objectweb.dream.Push;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.bf.connectors.tcp.proxies.TcpDreamInvocationHandler;
import org.objectweb.fractal.bf.proxies.AbstractDynamicProxyStubContentGenerator;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.logging.Logger;

/**
 * This implementation of the ProxyGenerator uses JDK {@link Proxy}.
 *
 * @author valerio.schiavoni@gmail.com
 */
public class DreamStubContentGenerator extends AbstractDynamicProxyStubContentGenerator {

    Logger log = Logger.getLogger(DreamStubContentGenerator.class.getName());


    /**
     * @param itfName the name of the client interface
     * @return an instance of TcpDreamInvocationHandler
     */
    protected InvocationHandler createInvocationHandler(String itfName, Class<?> clazz) {
        return new TcpDreamInvocationHandler(itfName);
    }

    /**
     * The TCP/Dream based proxy implements the following interfaces:
     * <ol>
     * <li> {@link Interface}
     * <li> {@link BindingController}
     * <li> {@link Push}
     * </ol>
     *
     * @see org.objectweb.fractal.bf.proxies.AbstractDynamicProxyStubContentGenerator#proxyInterfaces(java.lang.Class)
     */
    @Override
    protected Class<?>[] proxyInterfaces(Class<?> clazz) {
        return new Class[]{clazz, Interface.class, BindingController.class,
                Push.class};
    }

    /**
     * Assume that the first element of the params argument is the name of the interface
     * @see org.objectweb.fractal.bf.proxies.AbstractDynamicProxyStubContentGenerator#createInvocationHandler(java.lang.String[], java.lang.Class)
     */
    @Override
    protected InvocationHandler createInvocationHandler(String[] params,
        Class<?> clazz)
    {
      return createInvocationHandler(params[0], clazz);
    }
}
