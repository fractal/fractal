
package org.objectweb.fractal.bf.connectors.tcp.proxies;

import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.bf.proxies.AbstractStubComponentFactory;
import org.objectweb.fractal.bf.proxies.StubContentGenerator;

/**
 *A stub component factory for TCP/Dream connectors.
 */
public class TcpConnectorStubComponentFactoryImpl
    extends
      AbstractStubComponentFactory
{

  /**
   * @param pg the stub content generator to use by this component factory
   */
  public TcpConnectorStubComponentFactoryImpl(StubContentGenerator pg)
  {

    super();

    this.stubContentGenerator = pg;
  }

  /**
   * @param proxyInterfaceType
   * @return
   * @throws InstantiationException
   */
  @Override
  protected ComponentType createProxyType(InterfaceType proxyInterfaceType)
      throws InstantiationException
  {
    ComponentType proxyType = tf.createFcType(new InterfaceType[]{
        proxyInterfaceType,
        tf.createFcItfType("message-manager",
            "org.objectweb.dream.message.MessageManagerType", true, false,
            false),
        tf.createFcItfType("out-push", "org.objectweb.dream.Push", true, false,
            false),
        tf.createFcItfType("in-push", "org.objectweb.dream.Push", false, false,
            false)});
    return proxyType;
  }

  /**
   * @see org.objectweb.fractal.bf.proxies.AbstractStubComponentFactory#getStubContentGenerator()
   */
  @Override
  protected StubContentGenerator getStubContentGenerator()
  {
    return this.stubContentGenerator;
  }

}
