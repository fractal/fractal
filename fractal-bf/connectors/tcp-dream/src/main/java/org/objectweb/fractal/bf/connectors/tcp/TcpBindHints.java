/**
 * Author: Valerio Schiavoni <valerio.schiavoni@gmail.com>
 */
package org.objectweb.fractal.bf.connectors.tcp;

import org.objectweb.fractal.bf.BindHints;

/**
 * The TCP/Dream bind hints.
 */
public class TcpBindHints implements BindHints {

}
