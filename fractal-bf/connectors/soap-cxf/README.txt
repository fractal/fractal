This module provides a plugin for BindingFactory to support SOAP-based bindings between
components. 

This plugin exploits Apache CXF http://cxf.apache.org/ to expose component interfaces as web services, 
and to call web-services via Fractal's client interfaces.

