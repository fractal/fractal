/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Valerio.Schiavoni@gmail.com
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.ws;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.SkeletonGenerationException;
import org.objectweb.fractal.bf.StubGenerationException;
import org.objectweb.fractal.util.Fractal;

/**
 * @author Valerio Schiavoni <valerio.schiavoni@gmail.com>
 */
public class WsConnectorTest {
	Component service;
	Component client;

	protected WsConnector connector;
	private static Factory adlFactory;

	@BeforeClass
	public static void initializeSystemProperties() throws ADLException {
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
		adlFactory = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
	}

	@Before
	public void setUp() throws Exception {

		service = (Component) adlFactory.newComponent(
				"org.objectweb.fractal.bf.connectors.Service", null);
		assertNotNull(service);
		client = (Component) adlFactory.newComponent(
				"org.objectweb.fractal.bf.connectors.Client", null);
		assertNotNull(client);

		BindingFactoryClassLoader cl = new BindingFactoryClassLoader(Thread
				.currentThread().getContextClassLoader());

		connector = new WsConnector();
		connector.setBindingFactoryClassLoader(cl);
		assertNotNull(connector);
	}

	@Test
	public void testCreateSkeletonAndCheckState() throws Exception {

		Component skel = connector.createSkel("service", service
				.getFcInterface("service"), service, createExportHints());
		assertNotNull("Skeleton component should not be null", skel);
		assertEquals(LifeCycleController.STOPPED, Fractal
				.getLifeCycleController(skel).getFcState());

	}

	@Test(expected = StubGenerationException.class)
	public void testCreateStubComponentThrowsBindingFactoryExceptionForNullBindingHints()
			throws BindingFactoryException {
		connector.createStub(client, "service", null);

	}

	@Test
	public void testCreateStubComponentDefaultWSUri()
			throws BindingFactoryException {

		Component stub = connector.createStub(client, "service",
				new WsBindHints());
		assertNotNull("should export using default ws uri", stub);
	}

	/*
	 * the architecture of the client component must be changed: the signature
	 * of the client interface must be derived by the wsdl of the exposed
	 * web-service. TODO
	 */
	public void _testCreateStubComponentWithTargetWSUri()
			throws BindingFactoryException, NoSuchInterfaceException,
			IllegalLifeCycleException, SkeletonGenerationException {

		final WsBindHints hints = createBindHints();
		Component stub = connector.createStub(client, "service", hints);

		assertNotNull("The stub component shouldn't be null", stub);

		Fractal.getLifeCycleController(stub).startFc();
		assertEquals(LifeCycleController.STARTED, Fractal
				.getLifeCycleController(stub).getFcState());

		WsStubContentAttributes wsacc = (WsStubContentAttributes) Fractal
				.getAttributeController(stub);
		System.err.println(wsacc.getUri());

	}

	/**
	 * Utility method
	 * 
	 * @return
	 */
	private WsExportHints createExportHints() {
		WsExportHints hints = new WsExportHints();
		hints.setUri("http://localhost:8080/Service");
		return hints;
	}

	private WsBindHints createBindHints() {
		WsBindHints hints = new WsBindHints();
		hints.setUri("http://localhost:8080/Service");

		return hints;
	}

}
