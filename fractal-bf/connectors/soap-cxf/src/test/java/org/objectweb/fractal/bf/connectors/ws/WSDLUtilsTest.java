/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.ws;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.objectweb.fractal.bf.connectors.ws.WSDLUtils.WsdlElements;

public class WSDLUtilsTest {

	@Test
	public void testParseWsdlPort() {
		String wsdlElement = "http://localhost:8080/proactive#wsdl.port(HelloWorldServer_Service/HelloWorldServer_ServiceSoap12Binding)";
		WsdlElements elements = WSDLUtils.parse(wsdlElement);
		assertEquals("HelloWorldServer_Service", elements.serviceName);
		assertEquals("HelloWorldServer_ServiceSoap12Binding", elements.portName);
	}

	@Test
	public void testParseWsdlEndpoint() {
		String wsdlElement = "http://auchandanslapoche.norsys.fr/back-office/ws/client#wsdl.endpoint(ClientWebService/ClientWebServiceImplPort)";
		WsdlElements elements = WSDLUtils.parse(wsdlElement);
		assertEquals("ClientWebService", elements.serviceName);
		assertEquals("ClientWebServiceImplPort", elements.portName);
	}

	@Test
	public void testParseWsdlPort_2() {
		String wsdlElement = "http://webservices.amazon.com/AWSECommerceService/2009-02-01#wsdl.port(AWSECommerceService/AWSECommerceServicePort)";
		WsdlElements elements = WSDLUtils.parse(wsdlElement);
		assertEquals("AWSECommerceService", elements.serviceName);
		assertEquals("AWSECommerceServicePort", elements.portName);
	}
}
