/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Valerio.Schiavoni@gmail.com
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.ws;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;
import org.objectweb.fractal.bf.connectors.Service;
import org.objectweb.fractal.bf.proxies.ASMStubGenerator;

/**
 * Test the {@link ASMStubGenerator} to generate stubs for the Web Service
 * bindings.
 */
public class WsASMStubGeneratorTest {

	private BindingFactoryClassLoader cl;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.cl = new BindingFactoryClassLoader(Thread.currentThread()
				.getContextClassLoader());
	}

	@Test
	public void testGenerateNewInstance() {

		try {
			Object impl = ASMStubGenerator.generateNewInstance(Service.class,
					WsStubContent.class, cl);
			assertNotNull("Impl can' be null", impl);
			assertTrue("Cannot assign impl to "
					+ Service.class.getCanonicalName(), Service.class
					.isAssignableFrom(impl.getClass()));
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

}
