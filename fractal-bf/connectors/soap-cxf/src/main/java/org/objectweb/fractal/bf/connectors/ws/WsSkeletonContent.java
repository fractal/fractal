/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2014 Inria, SARDES, ADAM, Spirals

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 * Contributor: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.ws;

import javax.jws.WebService;
import javax.xml.namespace.QName;

import org.apache.cxf.aegis.AegisContext;
import org.apache.cxf.aegis.databinding.AegisDatabinding;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.frontend.ServerFactoryBean;
import org.apache.cxf.jaxws.JaxWsServerFactoryBean;
import org.apache.cxf.service.invoker.BeanInvoker;
import org.objectweb.fractal.bf.connectors.common.uri.UriSkeletonContent;
import org.objectweb.fractal.bf.connectors.ws.WSDLUtils.WsdlElements;

/**
 * The content implementation of Web Service skeleton components.
 */
public class WsSkeletonContent extends UriSkeletonContent implements
		WsSkeletonContentAttributes {

	/**
	 * The wsdlElement to configure the binding.
	 */
	private String wsdlElement;

	/**
	 * The wsdlLocation to configure the binding.
	 */
	private String wsdlLocation;

	/**
	 * The server factory bean.
	 */
	private final ServerFactoryBean wsServerFactoryBean;

	/**
	 * A reference to the server bean. This reference is initialized during the
	 * {@link WsSkeletonContent#startFc()} method.
	 */
	private Server server;

	public WsSkeletonContent() {
		wsServerFactoryBean = new ServerFactoryBean();
		/*
		 * the databinding used by this plugin. Should be this configurable,
		 * something must be done in the attribute controller and in this code.
		 */
		AegisDatabinding aDB = new AegisDatabinding();
		final AegisContext aegisContext = new AegisContext();
		aegisContext.setWriteXsiTypes(true);
		aDB.setAegisContext(aegisContext);

		wsServerFactoryBean.getServiceFactory().setDataBinding(aDB);
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.ws.WsStubContentAttributes#getWsdlElement()
	 */
	public String getWsdlElement() {
		return this.wsdlElement;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.ws.WsStubContentAttributes#setWsdlElement(java.lang.String)
	 */
	public void setWsdlElement(String wsdlElement) {
		this.wsdlElement = wsdlElement;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.ws.WsStubContentAttributes#getWsdlLocation()
	 */
	public String getWsdlLocation() {
		return wsdlLocation;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.ws.WsStubContentAttributes#setWsdlLocation(java.lang.String)
	 */
	public void setWsdlLocation(String uri) {
		this.wsdlLocation = uri;
	}

	/**
	 * @see org.objectweb.fractal.api.control.LifeCycleController#startFc()
	 */
	@Override
	public void startFc() {
        String uri = getUri();
		QName serviceName = null;
		QName portName = null;
		String we = WsConnectorConstants.NO_WSDL_ELEMENT.equals(wsdlElement) ? null : wsdlElement;
		if(we != null) {
  	      final WsdlElements wsdlElements = WSDLUtils.parse(we);
  	      serviceName = new QName(wsdlElements.getNamespace(), wsdlElements.getServiceName());
  	      portName = new QName(wsdlElements.getNamespace(), wsdlElements.getPortName());
		}

		if(getServiceClass().getAnnotation(WebService.class) == null) {
  	      log.info("WsSkeletonContent:startFc: will expose a simple Java interface at URI: " + uri);
  	      wsServerFactoryBean.setAddress(uri);
	      wsServerFactoryBean.setServiceClass(getServiceClass());
	      BeanInvoker invoker = new BeanInvoker(getServant());
	      wsServerFactoryBean.setInvoker(invoker);
	      if(serviceName != null) {
	        wsServerFactoryBean.setServiceName(serviceName);
	      }
	      if(portName != null) {
	        wsServerFactoryBean.setEndpointName(portName);
		  }
	      if(!WsConnectorConstants.NO_WSDL_LOCATION.equals(this.wsdlLocation)) {
	    	  wsServerFactoryBean.setWsdlLocation(this.wsdlLocation);
	      }
	      this.server = wsServerFactoryBean.create();
        } else {
          log.info("WsSkeletonContent:startFc: will expose a JAX-WS interface at URI: " + uri);
   	      JaxWsServerFactoryBean sf = new JaxWsServerFactoryBean();
          sf.setAddress(uri);
	      sf.setServiceClass(getServiceClass());
          sf.setServiceBean(getServant());
	      if(serviceName != null) {
	        sf.setServiceName(serviceName);
          }
          if(portName != null) {
            sf.setEndpointName(portName);
          }
	      if(!WsConnectorConstants.NO_WSDL_LOCATION.equals(this.wsdlLocation)) {
	    	  wsServerFactoryBean.setWsdlLocation(this.wsdlLocation);
	      }
          this.server = sf.create();        
        }
	    log.info("WSDL available at the address: " + uri + "?wsdl");
	    applyIntentHandlers(this.server);
		super.startFc();
	}

	/**
	 * @see org.objectweb.fractal.api.control.LifeCycleController#stopFc()
	 */
	@Override
	public void stopFc() {
		this.server.stop();
		log.fine("WebService skeleton stopped");
		super.stopFc();
	}
}
