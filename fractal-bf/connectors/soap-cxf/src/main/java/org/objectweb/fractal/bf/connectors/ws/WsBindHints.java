/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Valerio.Schiavoni@gmail.com
 * 
 * Author: Valerio Schiavoni
 * 
 * Contributor: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.ws;

import org.objectweb.fractal.bf.connectors.common.uri.UriBindHints;

/**
 * The hints supported by the web-service binding.
 */
public class WsBindHints extends UriBindHints {
	/**
	 * The wsdl element that describes the reference endpoint.
	 */
	private String wsdlElement;

	/**
	 * The (optional) attribute to specify the location of the WSDL file to use.
	 */
	private String wsdlLocation;

	/**
	 * @return the wsdlElement
	 */
	public final String getWsdlElement() {
		return wsdlElement;
	}

	/**
	 * @param wsdlElement
	 *            the wsdlElement to set
	 */
	public final void setWsdlElement(String wsdlElement) {
		this.wsdlElement = wsdlElement;
	}

	/**
	 * @return the wsdlLocation
	 */
	public final String getWsdlLocation() {
		return wsdlLocation;
	}

	/**
	 * @param wsdlLocation
	 *            the wsdlLocation to set
	 */
	public final void setWsdlLocation(String wsdlLocation) {
		this.wsdlLocation = wsdlLocation;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("[");
		sb.append(WsConnectorConstants.URI + ":");
		if (getUri() != null) {
			sb.append(getUri());
		} else {
			sb.append("NOT-SET");
		}
		sb.append(";");
		sb.append(WsConnectorConstants.WSDL_ELEMENT + ":");
		if (wsdlElement != null) {
			sb.append(wsdlElement);
		} else {
			sb.append("NOT-SET");
		}
		sb.append(";");
		sb.append(WsConnectorConstants.WSDL_LOCATION + ":");
		if (wsdlLocation != null) {
			sb.append(wsdlLocation);
		} else {
			sb.append("NOT-SET");
		}
		sb.append("]");

		return sb.toString();
	}
}
