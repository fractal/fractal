/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.ws;

import java.util.logging.Logger;

/**
 * Parse wsdl binding descriptions.
 * 
 * @since 0.6
 */
public class WSDLUtils {

	static Logger log = Logger.getLogger(WSDLUtils.class.getCanonicalName());

	/*
	 * Private constructor to avoid instantiation.
	 */
	private WSDLUtils() {
	}

	/**
	 * @param input
	 *            parse a wsdlElement and get the ServiceName
	 * @return the name of the service, or null if not found.
	 */
	public static WsdlElements parse(String input) {

		WsdlElements wsdlElements = new WsdlElements();

		String serviceName = "";
		int i = input.indexOf("#");
		if (i == -1) {
			return null;
		} else {
			wsdlElements.namespace = input.substring(0, i);

			String localName = input.substring(i + 1);

			if (localName.startsWith("wsdl.port")) {
				localName = localName.substring("wsdl.port(".length(),
						localName.length() - 1);
				log.info("wsdl.port value: " + localName);
			} else if (localName.startsWith("wsdl.endpoint")) {
				localName = localName.substring("wsdl.endpoint(".length(),
						localName.length() - 1);
				log.info("wsdl.endpoint value: " + localName);
			}
			int s = localName.indexOf('/');
			serviceName = localName.substring(0, s);
			wsdlElements.serviceName = serviceName;

			/*
			 * get the port name
			 */

			String port = localName.substring(s + 1, localName.length());

			wsdlElements.portName = port;

		}
		return wsdlElements;
	}

	protected static class WsdlElements {
		String namespace;
		String serviceName;
		String portName;

		/**
		 * @return the namespace
		 */
		public final String getNamespace() {
			return namespace;
		}

		/**
		 * @param namespace
		 *            the namespace to set
		 */
		public final void setNamespace(String namespace) {
			this.namespace = namespace;
		}

		/**
		 * @return the serviceName
		 */
		public final String getServiceName() {
			return serviceName;
		}

		/**
		 * @param serviceName
		 *            the serviceName to set
		 */
		public final void setServiceName(String serviceName) {
			this.serviceName = serviceName;
		}

		/**
		 * @return the portName
		 */
		public final String getPortName() {
			return portName;
		}

		/**
		 * @param portName
		 *            the portName to set
		 */
		public final void setPortName(String portName) {
			this.portName = portName;
		}
	}
}
