/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES
 * Copyright (C) 2013 Inria, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 * 
 * Contact: Valerio.Schiavoni@gmail.com
 * 
 * Author: Valerio Schiavoni
 *
 * Contributor: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.ws;

import java.util.Map;

import javax.jws.WebService;
import javax.xml.ws.BindingProvider;
import org.apache.cxf.message.Message;
import org.objectweb.fractal.bf.connectors.common.uri.UriStubContent;

/**
 * A super class for WS stubs. Concrete classes are generated on the fly.
 */
public abstract class WsStubContent extends UriStubContent
	implements WsStubContentAttributes {

	/**
	 * The wsdlElement to configure the binding.
	 */
	private String wsdlElement;

	/**
	 * The wsdlLocation to configure the binding.
	 */
	private String wsdlLocation;

	/**
	 * The CXF client recovered by the objects instantiated by pre-generated
	 * stubs via wsdl2java. This client is initialized in
	 * {@link AbstractWsStubContent#resolveReference()} .
	 */
	private org.apache.cxf.endpoint.Client client;

	/**
	 * @see org.objectweb.fractal.bf.connectors.ws.WsStubContentAttributes#getWsdlElement()
	 */
	public String getWsdlElement() {
		return this.wsdlElement;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.ws.WsStubContentAttributes#setWsdlElement(java.lang.String)
	 */
	public void setWsdlElement(String wsdlElement) {
		this.wsdlElement = wsdlElement;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.ws.WsStubContentAttributes#getWsdlLocation()
	 */
	public String getWsdlLocation() {
		return wsdlLocation;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.ws.WsStubContentAttributes#setWsdlLocation(java.lang.String)
	 */
	public void setWsdlLocation(String uri) {
		this.wsdlLocation = uri;
	}

	/**
	 * Build a CXF client using the {@link DefaultResolutionStrategy}.
	 * 
	 * @return the object instantiated by the
	 *         {@link DefaultResolutionStrategy#resolve()} method.
	 */
	protected Object resolveReference() {

		String uri = WsConnectorConstants.NO_URI.equals(getUri()) ? null : getUri();
		String we = WsConnectorConstants.NO_WSDL_ELEMENT.equals(wsdlElement) ? null : wsdlElement;
		String wl = WsConnectorConstants.NO_WSDL_LOCATION.equals(wsdlLocation) ? null : wsdlLocation;

		WsStubResolutionStrategy strategy;
		Object ref;
		if(getServiceClass().getAnnotation(WebService.class) == null) {
	  	  log.info("WsStubContent:resolveReference: will create a WS stub from a simple Java interface for URI: " + uri);
	  	  strategy = new DynamicClientResolutionStrategy(uri, we, getServiceClass());
		} else {
           log.info("WsStubContent:resolveReference: will create a WS stub from a JAX-WS Java interface for URI: " + uri);
		   strategy = new DefaultResolutionStrategy(uri,
				we, wl, getServiceClass(), getClass().getClassLoader());
		}
        ref = strategy.resolve();
		client = org.apache.cxf.frontend.ClientProxy.getClient(ref);

		/*
		 * to make the client request parameters thread-local
		 */
		client.setThreadLocalRequestContext(true);

		/*
		 * If the uri has been changed because of a reconfiguraiton, orbecuase
		 * the uri given in the wsdl must be modified,cahnge the corresponding
		 * attributes in the request.
		 */
		if (uri != null) {
			final Map<String, Object> requestContext = client
					.getRequestContext();
			// XXX is this taken into account?
			requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
					uri);
			/*
			 * set the URI to which messages are sent: this is CXF specific, but
			 * internally this key is mapped to JaxWs properties.
			 */
			requestContext.put(Message.ENDPOINT_ADDRESS, uri);

			log.info("URI: " + uri);
		}

		return ref;
	}
}
