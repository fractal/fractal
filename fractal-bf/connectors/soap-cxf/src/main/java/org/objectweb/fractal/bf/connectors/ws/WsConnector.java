/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2014 Inria, SARDES, ADAM, Spirals

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 * Contributor: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.ws;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.bf.connectors.common.AbstractConnector;

/**
 * A WS-Connector can export and bind Fractal interfaces via web services.
 * 
 */
public class WsConnector extends AbstractConnector<WsExportHints, WsBindHints> {

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getPluginIdentifier()
	 */
	public String getPluginIdentifier() {
		return "ws";
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getExportHints(Map)
	 */
	public WsExportHints getExportHints(Map<String, Object> initialHints) {
		WsExportHints wsExportHints = new WsExportHints();
		String uri = (String) initialHints.get(WsConnectorConstants.URI);
		if (uri == null) {
			uri = WsConnectorConstants.DEFAULT_URI;
			log.warning("Export hints didn't specify an uri, going to use "
					+ uri);
		}
		wsExportHints.setUri(uri);
		wsExportHints.setWsdlElement((String) initialHints
				.get(WsConnectorConstants.WSDL_ELEMENT));

		if (initialHints.containsKey(WsConnectorConstants.WSDL_LOCATION)) {

			Object wsdlLocation = initialHints
					.get(WsConnectorConstants.WSDL_LOCATION);
			/*
			 * the SCA model represents location value as a list of 1 element..
			 */
			if (wsdlLocation instanceof List) {
				List<String> wsdLocationXml = (List<String>) wsdlLocation;
				if (wsdLocationXml != null && wsdLocationXml.size() > 0)
					wsExportHints.setWsdlLocation(wsdLocationXml.get(0));
			} else if (wsdlLocation instanceof String) { // for FractalADL
				wsExportHints.setWsdlLocation((String) wsdlLocation);
			}
		}

		log.fine("WsExportHints: " + wsExportHints);
		return wsExportHints;
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getBindHints(Map)
	 */
	public WsBindHints getBindHints(Map<String, Object> initialHints) {
		log.fine("Raw hints: " + initialHints);
		WsBindHints wsBindHints = new WsBindHints();
		wsBindHints.setUri((String) initialHints.get(WsConnectorConstants.URI));
		wsBindHints.setWsdlElement((String) initialHints
				.get(WsConnectorConstants.WSDL_ELEMENT));
		if (initialHints.containsKey(WsConnectorConstants.WSDL_LOCATION)) {

			Object wsdlLocation = initialHints
					.get(WsConnectorConstants.WSDL_LOCATION);
			/*
			 * the SCA model represents location value as a list of 1 element..
			 */
			if (wsdlLocation instanceof List) {
				List<String> wsdLocationXml = (List<String>) wsdlLocation;
				if (wsdLocationXml != null && wsdLocationXml.size() > 0)
					wsBindHints.setWsdlLocation(wsdLocationXml.get(0));
			} else if (wsdlLocation instanceof String) { // for FractalADL
				wsBindHints.setWsdlLocation((String) wsdlLocation);
			}
		}

		log.fine("WsBindHints: " + wsBindHints);
		return wsBindHints;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#getSkeletonAdl()
	 */
	@Override
	protected String getSkeletonAdl() {
		return "org.objectweb.fractal.bf.connectors.ws.WsSkeleton";
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#initSkeletonAdlContext(EH,
	 *      Map)
	 */
	@Override
	protected void initSkeletonAdlContext(WsExportHints exportHints,
			Map<String, Object> context) {
		String uri = exportHints.getUri();
		if (uri == null) {
			uri = WsConnectorConstants.DEFAULT_URI;
			log.warning("Export hints didn't specify an uri, going to use "
					+ uri);
		}
		context.put(WsConnectorConstants.URI, uri);
		String wsdlElement = exportHints.getWsdlElement();
		if (wsdlElement == null) {
			wsdlElement = WsConnectorConstants.NO_WSDL_ELEMENT;
		}
		context.put(WsConnectorConstants.WSDL_ELEMENT, wsdlElement);
		String wsdlLocation = exportHints.getWsdlLocation();
		if (wsdlLocation == null) {
			wsdlLocation = WsConnectorConstants.NO_WSDL_LOCATION;
		}
		context.put("wsdlLocation", wsdlLocation);
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#getStubAdl()
	 */
	@Override
	protected String getStubAdl() {
		return "org.objectweb.fractal.bf.connectors.ws.WsStub";
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#initStubAdlContext(BH,
	 *      Map)
	 */
	@Override
	protected void initStubAdlContext(WsBindHints bindHints,
			Map<String, Object> context) {
		generateStubContentClass(WsStubContent.class, context);
		String uri = bindHints.getUri();
		if (uri == null) {
			uri = WsConnectorConstants.NO_URI;
		}
		context.put(WsConnectorConstants.URI, uri);
		String wsdlElement = bindHints.getWsdlElement();
		if (wsdlElement == null) {
			wsdlElement = WsConnectorConstants.NO_WSDL_ELEMENT;
		}
		context.put(WsConnectorConstants.WSDL_ELEMENT, wsdlElement);
		String wsdlLocation = bindHints.getWsdlLocation();
		if (wsdlLocation == null) {
			wsdlLocation = WsConnectorConstants.NO_WSDL_LOCATION;
		}
		context.put("wsdlLocation", wsdlLocation);
	}
}
