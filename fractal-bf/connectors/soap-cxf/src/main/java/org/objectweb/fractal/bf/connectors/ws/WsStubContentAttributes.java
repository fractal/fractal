/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Valerio.Schiavoni@gmail.com
 * 
 * Author: Valerio Schiavoni
 * 
 * Contributor: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.ws;

import org.objectweb.fractal.bf.connectors.common.uri.UriStubContentAttributes;

/**
 * Attribute controller for web service stubs.
 */
public interface WsStubContentAttributes extends UriStubContentAttributes {
	/**
	 * Get the wsdlElement used to configure the stub.
	 * 
	 * @return the wsdlElement used to configure the stub.
	 */
	public String getWsdlElement();

	/**
	 * Set the wsdlElement used to configure the stub.
	 * 
	 * @param wsdlElement
	 */
	public void setWsdlElement(String wsdlElement);

	/**
	 * Get the uri of the wsdl to use.
	 * 
	 * @return
	 */
	public String getWsdlLocation();

	/**
	 * Set the wsdlLocation.
	 * 
	 * @param uri
	 *            the uri of the wsdl to use
	 */
	public void setWsdlLocation(String uri);
}
