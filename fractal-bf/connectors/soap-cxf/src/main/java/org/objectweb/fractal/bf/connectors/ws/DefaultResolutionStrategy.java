/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Valerio.Schiavoni@gmail.com
 * 
 * Author: Valerio Schiavoni
 * 
 * Contributor: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.ws;

import com.sun.xml.bind.api.impl.NameConverter;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.logging.Logger;

import org.objectweb.fractal.bf.connectors.common.ConnectorException;
import org.objectweb.fractal.bf.connectors.ws.WSDLUtils.WsdlElements;

/**
 * This class uses pre-generated class by wsdl2java as mean to create the
 * appropriate object to be assigned to the
 * {@link AbstractWsStubContent#remoteRef}. The logic behind the parsing of
 * wsdlElement is defined by the SCA_WebServiceBinding specifications 1.0 (lines
 * 35-68).
 */
public class DefaultResolutionStrategy implements WsStubResolutionStrategy {

	Logger log = Logger.getLogger(DefaultResolutionStrategy.class
			.getCanonicalName());

	private final String uri;

	private final Class<?> buzinezClass;

	private final String wsdlElement;

	private final ClassLoader classloader;

	private final String wsdlLocation;

	public DefaultResolutionStrategy(String uri, String wsdlElement,
			String wsdlLocation, Class<?> buzinezClazz, ClassLoader classLoader) {
		this.uri = uri;
		this.wsdlElement = wsdlElement;
		this.wsdlLocation = wsdlLocation;
		this.buzinezClass = buzinezClazz;
		this.classloader = classLoader;
	}

	/**
	 * Can parse wsdlELement elements, discovering service and port name values.
	 * The value of the uri is set to configure the default service name. In
	 * this case, the port name to use is deriver
	 * 
	 * @see org.objectweb.fractal.bf.connectors.ws.WsStubResolutionStrategy#resolve()
	 */
	public Object resolve() {

		String packageName = null;
		String serviceName = null;
		String port = null;

		if (uri == null && wsdlElement == null) {
			throw new IllegalStateException(
					"Impossible to configure web-service binding without uri or wsdlElement.");
		}
		/*
		 * uri gets the precedence over the values set in the wsdlElement.
		 */
		if (uri != null) {
			// use the url to get the service name
			int i = uri.lastIndexOf("/");
			serviceName = uri.substring(i + 1, uri.length());
			log.fine("Service name from uri: " + serviceName);
			// default is the clazz simple name
			port = buzinezClass.getSimpleName();
			log.fine("Port name from uri: " + serviceName);
		}
		/*
		 * wsdl element can overwrite previous values
		 */
		if (wsdlElement != null) {
			// overwrite values derived by default
			final WsdlElements wsdlElements = WSDLUtils.parse(this.wsdlElement);

			// Convert the namespace into a Java package name.
			packageName = NameConverter.standard.toPackageName(wsdlElements.getNamespace());

			String serviceNameWsdl = wsdlElements.getServiceName();

			if ((serviceNameWsdl != null && !"".equals(serviceNameWsdl))
					&& serviceNameWsdl != serviceName) {
				log.finest("Overwriting default value '" + serviceName
						+ "' with service name from wsdlElement '"
						+ serviceNameWsdl + "'");
				serviceName = serviceNameWsdl;
			}

			log.fine("Service name from wsdlElement: " + serviceName);
			String portWsdl = wsdlElements.getPortName();
			if ((portWsdl != null && !"".equals(portWsdl)) && portWsdl != port) {
				log.finest("Overwriting default port name '" + port
						+ "' with port name from wsdlElement '"
						+ serviceNameWsdl + "'");
				port = portWsdl;
			}
			log.fine("Port name from wsdlElement: " + portWsdl);

		}

		if (serviceName == null || "".equals(serviceName)) {
			throw new IllegalStateException(
					"An error occurred while parsing the service name. uri: "
							+ uri + ",wsdlElement:" + wsdlElement);
		}

		if(packageName == null) {
			packageName = buzinezClass.getPackage().getName();
		}
		log.finest("Java package :" + packageName);

		// instantiate factory class
		Class<?> factoryClass = null;
		try {
			/*
			 * use the bf cl to load the factory class.
			 */
			factoryClass = classloader.loadClass(packageName + "." + serviceName);

			log.finest("Factory class:" + factoryClass.getCanonicalName());
			Object factoryInstance = null;
			if (wsdlLocation != null) {

				/*
				 * use the constructor of the factory class that takes the URI
				 * of the wsdl to use
				 */
				// First, check if the the WSDL location is present into the class loader.
				URL wsdlLocationUri = this.classloader.getResource(wsdlLocation);
				if(wsdlLocationUri == null) {
					// If not then create an url.
					wsdlLocationUri = new URL(wsdlLocation);
				}

				log.fine("wsdlLocation : " + wsdlLocationUri.toString());
				try {
					Constructor<?> constr = factoryClass
							.getConstructor(new Class[] { URL.class });
					factoryInstance = constr
							.newInstance(new Object[] { wsdlLocationUri });
				} catch (NoSuchMethodException nsme) {
					log.warning("Using factory default constructor");
					factoryInstance = factoryClass.newInstance();
				}

			} else {
				factoryInstance = factoryClass.newInstance();
			}

			/*
			 * the technique to derive the name of the method on the factory is
			 * defined by JAX-WS specs http://jcp.org/aboutJava/communityprocess
			 * /pfd/jsr224/index.html
			 */
			Method factoryMethod = factoryInstance.getClass().getMethod(
					"get" + port, new Class[] {});
			return factoryMethod.invoke(factoryInstance, new Object[] {});
		} catch (Exception e) {
			e.printStackTrace();
			throw new ConnectorException("Could not initialize stub reference",
					e);
		}

	}
}
