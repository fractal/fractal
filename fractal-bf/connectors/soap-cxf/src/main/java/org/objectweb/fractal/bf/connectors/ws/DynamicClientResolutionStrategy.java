/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.ws;

import java.util.logging.Logger;

import org.apache.cxf.aegis.AegisContext;
import org.apache.cxf.aegis.databinding.AegisDatabinding;
import org.apache.cxf.frontend.ClientProxyFactoryBean;

/**
 * Use {@link ClientProxyFactoryBean} to instantiate the object to be assiged to
 * the {@link AbstractWsStubContent#remoteRef} field.
 */
public class DynamicClientResolutionStrategy implements
		WsStubResolutionStrategy {

	Logger log = Logger.getLogger(DynamicClientResolutionStrategy.class
			.getCanonicalName());

	private final String uri;

	private final Class<?> buzinezClass;

	private final String wsdlElement; // XXX not used?

	public DynamicClientResolutionStrategy(String uri,
			String wsdlElement, Class<?> buzinezClazz) {
		this.uri = uri;
		this.wsdlElement = wsdlElement;
		this.buzinezClass = buzinezClazz;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.ws.WsStubResolutionStrategy#resolve()
	 */
	public Object resolve() {
		log.fine("Initializing CXF Simple Frontend");
		final ClientProxyFactoryBean clientProxyfactoryBean = new ClientProxyFactoryBean();
		clientProxyfactoryBean.setServiceClass(this.buzinezClass);
		clientProxyfactoryBean.setAddress(this.uri);
		log.fine("ClientProxyFactory using URI: " + this.uri);
		final AegisDatabinding aegisDatabinding = new AegisDatabinding();
		AegisContext aegisCtx = new AegisContext();
		aegisCtx.setReadXsiTypes(true);
		aegisDatabinding.setAegisContext(aegisCtx);
		clientProxyfactoryBean.setDataBinding(aegisDatabinding);
		return clientProxyfactoryBean.create();
	}

}
