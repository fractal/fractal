/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Valerio.Schiavoni@gmail.com
 * 
 * Author: Valerio Schiavoni
 * 
 * Contributor: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.ws;

import org.objectweb.fractal.bf.connectors.common.uri.UriConnectorConstants;

/**
 * Constants to configure WS bindings.
 */
public interface WsConnectorConstants extends UriConnectorConstants {
	public static final String DEFAULT_URI = UriConnectorConstants.DEFAULT_URI + "/Service";
	public static final String NO_URI = "no_uri";
	public static final String WSDL_ELEMENT = "wsdlElement";
	public static final String NO_WSDL_ELEMENT = "no_wsdl_element";
	public static final String WSDL_PORT = "port";
	public static final String WSDL_ENDPOINT = "endPoint";
	public static final String WSDL_BINDING = "binding";
	public static final String WSDL_LOCATION = "wsdli:wsdlLocation";
	public static final String NO_WSDL_LOCATION = "no_wsdl_location";
}
