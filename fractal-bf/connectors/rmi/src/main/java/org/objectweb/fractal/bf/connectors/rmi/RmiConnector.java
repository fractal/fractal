/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.rmi;

import java.util.Arrays;
import java.util.Map;
import java.util.logging.Logger;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.bf.AbstractSkeleton;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.BindingFactoryPlugin;
import org.objectweb.fractal.bf.SkeletonGenerationException;
import org.objectweb.fractal.bf.StubGenerationException;
import org.objectweb.fractal.bf.StubGenerationUtil;
import org.objectweb.fractal.bf.proxies.ASMStubGenerator;
import org.objectweb.fractal.util.Fractal;

/**
 * A plugin to bind and export fractal interfaces through the Java RMI protocol.
 * 
 * @since 0.3
 */
public class RmiConnector implements
		BindingFactoryPlugin<RmiExportHints, RmiBindHints> {

	Logger log = Logger.getLogger(RmiConnector.class.getCanonicalName());

	private TypeFactory typeFactory;
	private GenericFactory genericFactory;

	private BindingFactoryClassLoader cl;

	/**
	 * here to please Fractal..
	 */
	public RmiConnector() {
		try {
			Component bootstrap = Fractal.getBootstrapComponent();
			typeFactory = Fractal.getTypeFactory(bootstrap);
			genericFactory = Fractal.getGenericFactory(bootstrap);
		} catch (Exception e) {
			throw new RmiConnectorException(e);
		}

	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#createSkel(java.lang.String,
	 *      java.lang.Object, org.objectweb.fractal.api.Component,
	 *      org.objectweb.fractal.bf.ExportHints)
	 */
	public Component createSkel(String interfaceName, Object serverItf,
			Component owner, RmiExportHints exportHints)
			throws SkeletonGenerationException {
		log.info("RmiConnector - Creating skeleton.");
		log.finest("ServerItf ref: " + serverItf + " Owner: " + owner);

		String name = exportHints.getServiceName();
		String host = exportHints.getHost();
		String port = exportHints.getPort();

		Component rmiSkeleton = null;
		Component rmiRegistry = null;
		Component skeletonComposite = null;
		try {
			rmiRegistry = createRmiRegistryComponent(host, port);

			/*
			 * create the skeleton component
			 */
			ComponentType rmiSkeletonType = typeFactory
					.createFcType(new InterfaceType[] {
					// type for attribute-controller
							typeFactory.createFcItfType("attribute-controller",
									RmiSkeletonAttributes.class
											.getCanonicalName(), false, false,
									false),
							// type for delegate interface
							typeFactory.createFcItfType("delegate",
									"org.objectweb.fractal.api.Component",
									true, true, false),
							// type for the rmi registry
							typeFactory.createFcItfType("rmiRegistry",
									"java.rmi.registry.Registry", true, false,
									false) });
			rmiSkeleton = genericFactory.newFcInstance(rmiSkeletonType,
					"primitive", RmiSkeletonContent.class.getCanonicalName());

			Fractal.getNameController(rmiSkeleton).setFcName(
					RmiSkeletonContent.class.getCanonicalName());

			/*
			 * Set the required attributes
			 */
			RmiSkeletonAttributes rmiSkelAC = (RmiSkeletonAttributes) Fractal
					.getAttributeController(rmiSkeleton);
			rmiSkelAC.setServiceName(name);
			rmiSkelAC.setServiceObject(serverItf);
			rmiSkelAC.setBindingFactoryClassLoader(cl);

			ComponentType compositeType = typeFactory
					.createFcType(new InterfaceType[] { typeFactory
							.createFcItfType("delegate",
									"org.objectweb.fractal.api.Component",
									true, true, false) });

			skeletonComposite = genericFactory.newFcInstance(compositeType,
					"composite", null);

			/*
			 * put the skeleton and the registry inside a composite, so that
			 * their binding is local (avoid "Not a local binding" error)
			 */
			final ContentController ccComp = Fractal
					.getContentController(skeletonComposite);
			ccComp.addFcSubComponent(rmiRegistry);
			ccComp.addFcSubComponent(rmiSkeleton);

			/*
			 * set the binding between the skeleton and the rmi registry
			 */
			Fractal.getBindingController(rmiSkeleton).bindFc(
					RmiSkeletonContent.RMI_REGISTRY_CLIENT_ITF_NAME,
					rmiRegistry.getFcInterface("rmiRegistry"));

			/*
			 * set the internal binding between the skeleton and the composite
			 */
			Fractal
					.getBindingController(rmiSkeleton)
					.bindFc(
							AbstractSkeleton.DELEGATE_CLIENT_ITF_NAME,
							ccComp
									.getFcInternalInterface(AbstractSkeleton.DELEGATE_CLIENT_ITF_NAME));

		} catch (Exception e) {

			throw new SkeletonGenerationException(
					"Some error occurred while exporting using the JavaRMI plugin ",
					e);
		}
		log.info("RmiConnector - skeleton created.");
		return skeletonComposite;
	}

	/**
	 * Instantiate the RmiRegistry component.
	 * 
	 * @param host
	 *            the host the rmiRegistry is located to
	 * @param port
	 *            the port the rmiRegistry is localted to
	 * @return the rmiRegistry
	 * @throws InstantiationException
	 * @throws NoSuchInterfaceException
	 */
	protected Component createRmiRegistryComponent(String host, String port)
			throws InstantiationException, NoSuchInterfaceException {
		Component rmiRegistry;
		/*
		 * create the component for the Rmi Registry
		 */
		ComponentType rmiRegistryType = typeFactory
				.createFcType(new InterfaceType[] {
						typeFactory.createFcItfType("attribute-controller",
								RmiRegistryAttributes.class.getCanonicalName(),
								false, false, false),
						typeFactory.createFcItfType("rmiRegistry",
								"java.rmi.registry.Registry", false, false,
								false)

				});

		rmiRegistry = genericFactory.newFcInstance(rmiRegistryType,
				"primitive",
				"org.objectweb.fractal.bf.connectors.rmi.RmiRegistry");
		Fractal.getNameController(rmiRegistry).setFcName(
				"org.objectweb.fractal.bf.connectors.rmi.RmiRegistry");
		RmiRegistryAttributes rmiRegistryAttributes = (RmiRegistryAttributes) Fractal
				.getAttributeController(rmiRegistry);
		rmiRegistryAttributes.setHost(host);
		rmiRegistryAttributes.setPort(Integer.valueOf(port).intValue());
		log.fine("RmiRegistry component created");
		return rmiRegistry;
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getExportHints(Map)
	 */
	public RmiExportHints getExportHints(Map<String, Object> initialHints) {
		RmiExportHints rmiExportHints = new RmiExportHints();

		String registryHost = computeRegistryHost(initialHints);
		rmiExportHints.setHost(registryHost);

		String registryPort = computeRegistryPort(initialHints);
		rmiExportHints.setPort(registryPort);

		String serviceName = computeServiceName(initialHints);
		rmiExportHints.setServiceName(serviceName);

		return rmiExportHints;
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getBindHints(Map)
	 */
	public RmiBindHints getBindHints(Map<String, Object> initialHints) {
		RmiBindHints rmiBindHints = new RmiBindHints();
		String registryHost = computeRegistryHost(initialHints);
		rmiBindHints.setHost(registryHost);

		String registryPort = computeRegistryPort(initialHints);
		rmiBindHints.setPort(registryPort);

		String serviceName = computeServiceName(initialHints);
		rmiBindHints.setServiceName(serviceName);

		return rmiBindHints;
	}

	/**
	 * Look in the hints for a key {@link RmiConnectorConstants#SERVICE_NAME}
	 * 
	 * @param initialHints
	 *            the initial hints
	 * @return the name of the service to export/locate in the registry
	 */
	private String computeServiceName(Map<String, Object> initialHints) {
		String serviceName = (String) initialHints
				.get(RmiConnectorConstants.SERVICE_NAME);

		if (serviceName == null || serviceName.equalsIgnoreCase("")) {
			throw new IllegalArgumentException(
					"Hints do not specify a value for hint:"
							+ RmiConnectorConstants.SERVICE_NAME);
		}

		log.finer("Service name: " + serviceName);
		return serviceName;
	}

	/**
	 * @param initialHints
	 *            the initial hints
	 * @return the value for key
	 *         {@link RmiConnectorConstants#JAVA_RMI_REGISTRY_PORT}, or "1099"
	 *         if not present.
	 */
	private String computeRegistryPort(Map<String, Object> initialHints) {
		String registryPort = (String) initialHints
				.get(RmiConnectorConstants.JAVA_RMI_REGISTRY_PORT);
		if (registryPort == null) {
			log.warning("RMI registry port not specified, default to 1099");
			registryPort = "1099";
		}
		return registryPort;
	}

	/**
	 * @param initialHints
	 *            the initial hints
	 * @return the value for key
	 *         {@link RmiConnectorConstants#JAVA_RMI_REGISTRY_HOST_ADDRESS}, or
	 *         {@link RmiConnectorConstants#JAVA_RMI_REGISTRY_HOST_ADDRESS_DEFAULT}
	 *         if not present.
	 */
	private String computeRegistryHost(Map<String, Object> initialHints) {
		String registryHost = (String) initialHints
				.get(RmiConnectorConstants.JAVA_RMI_REGISTRY_HOST_ADDRESS);
		if (registryHost == null) {
			log
					.warning("You didn't specify "
							+ RmiConnectorConstants.JAVA_RMI_REGISTRY_HOST_ADDRESS
							+ ", will use '"
							+ RmiConnectorConstants.JAVA_RMI_REGISTRY_HOST_ADDRESS_DEFAULT
							+ "' ");
			registryHost = RmiConnectorConstants.JAVA_RMI_REGISTRY_HOST_ADDRESS_DEFAULT;
		}
		return registryHost;
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getPluginIdentifier()
	 */
	public String getPluginIdentifier() {
		return "rmi";
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#createStub(org.objectweb.fractal.api.Component,
	 *      java.lang.String, org.objectweb.fractal.bf.BindHints)
	 */
	public Component createStub(Component client, String itfname,
			RmiBindHints bindHints) throws StubGenerationException {

		log.info("RmiConnector - Creating stub.");

		if (client == null) {
			throw new IllegalArgumentException(
					"The component owning the client interface to bind can't be null");
		} else if (itfname == null || itfname.equalsIgnoreCase("")) {
			throw new IllegalArgumentException(
					"Can't find client interface with name:" + itfname);
		} else if (bindHints == null) {
			throw new IllegalArgumentException(
					"You didn't specify a map of hints to create the stub.");
		}
		/*
		 * this composite is the component that is returned
		 */
		Component stubComposite = null;
		Component stubComponent = null;
		InterfaceType businessInterfaceItfType = null;

		Component rmiRegistry = null;
		String host = bindHints.getHost();
		String port = bindHints.getPort();
		try {
			/*
			 * create the rmiRegistry component
			 */
			rmiRegistry = createRmiRegistryComponent(host, port);

			ComponentType itf = (ComponentType) (client).getFcType();
			businessInterfaceItfType = itf.getFcInterfaceType(itfname);
			Class<?> rmiStubClass = null;

			rmiStubClass = StubGenerationUtil
					.instantiateClassFromInterfaceType(
							businessInterfaceItfType, this.cl);

			final Object stubImpl = ASMStubGenerator.generateNewInstance(
					rmiStubClass, AbstractRmiStubContent.class, this.cl);

			log.finer("Stub impl created: " + stubImpl.toString());

			InterfaceType stubReversedInterfaceType = StubGenerationUtil
					.symmetricInterfaceType(businessInterfaceItfType);
			log.finer("Interface type for stub component created: "
					+ stubReversedInterfaceType);
			/*
			 * the AC type for rmi stub
			 */
			InterfaceType acIType = typeFactory.createFcItfType(
					"attribute-controller", RmiStubAttributes.class
							.getCanonicalName(), false, false, false);

			ComponentType stubComponentType = typeFactory
					.createFcType(new InterfaceType[] {
							acIType,
							stubReversedInterfaceType,
							typeFactory.createFcItfType("rmiRegistry",
									"java.rmi.registry.Registry", true, false,
									false) });

			log.finer("Component type for stub component created: "
					+ stubComponentType);
			Object[] ctrlDesc = new Object[] { this.cl, "primitive" };
			log.finest("Controller descriptor for stub component: "
					+ Arrays.deepToString(ctrlDesc));
			stubComponent = genericFactory.newFcInstance(stubComponentType,
					ctrlDesc, stubImpl);

			Fractal.getNameController(stubComponent).setFcName(
					"rmi-stub-primitive");

			/*
			 * put the stub and the registry inside a composite, so that their
			 * binding is local (and avoid "Not a local binding" error)
			 */

			ComponentType stubCompositeType = typeFactory
					.createFcType(new InterfaceType[] { stubReversedInterfaceType });

			stubComposite = genericFactory.newFcInstance(stubCompositeType,
			    new Object[] { this.cl, "composite" }, null);
			/*
			 * add both inside
			 */
			final ContentController ccComp = Fractal
					.getContentController(stubComposite);
			ccComp.addFcSubComponent(rmiRegistry);
			ccComp.addFcSubComponent(stubComponent);

			// bind the stub to the registry
			final Object serverItf = rmiRegistry.getFcInterface("rmiRegistry");
			Fractal.getBindingController(stubComponent).bindFc("rmiRegistry",
					serverItf);

			// this is required to bind the internal interface of the composite
			// to the external server itf
			final BindingController compositeBC = Fractal
					.getBindingController(stubComposite);
			compositeBC.bindFc(itfname, stubComponent.getFcInterface(itfname));

			// set the attributes for the stub
			RmiStubAttributes rmiStubAC = (RmiStubAttributes) Fractal
					.getAttributeController(stubComponent);
			rmiStubAC.setServiceName(bindHints.getServiceName());

		} catch (Exception e) {
			e.printStackTrace();
			throw new StubGenerationException(
					"Some error occurred while creating the RMI stub component",
					e);
		}

		return stubComposite;
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#finalizeSkeleton(org.objectweb.fractal.api.Component,
	 *      java.util.Map)
	 */
	public void finalizeSkeleton(Component skel,
			Map<String, String> finalizationHints)
			throws BindingFactoryException {
		// do nothing

	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#finalizeStub(org.objectweb.fractal.api.Component,
	 *      java.util.Map)
	 */
	public void finalizeStub(Component stub, Map<String, Object> hints)
			throws BindingFactoryException {
		// do nothing

	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getClassLoader()
	 */
	public BindingFactoryClassLoader getBindingFactoryClassLoader() {
		return this.cl;
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#setBindingFactoryClassLoader(BindingFactoryClassLoader)
	 */
	public void setBindingFactoryClassLoader(BindingFactoryClassLoader cl) {
		this.cl = cl;
	}

}
