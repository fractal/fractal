/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Philippe Merle
 *
 * Contributor:
 *
 * $Id$
 */

package org.objectweb.fractal.bf.connectors.rmi;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.PrintStream;
import java.io.Reader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import org.objectweb.fractal.bf.BindingFactoryClassLoader;

import sun.net.util.IPAddressUtil;

/**
 * This class is a HTTP server to download classes.
 */

public class HttpServer extends Thread {

	Logger log = Logger.getLogger(HttpServer.class.getCanonicalName());

	private final ServerSocket serverSocket;

	private final String serverUrl;

	private final Set<BindingFactoryClassLoader> classLoader = new HashSet<BindingFactoryClassLoader>();

	public HttpServer(BindingFactoryClassLoader cl) throws Exception {

		if (cl != null) {
			classLoader.add(cl);
		}

		log.finer("ClassLoader used by HttpServer: " + classLoader);

		// Create the socket where accepting HTTP requests.
		serverSocket = new ServerSocket(0);

		// Compute the URL of the HTTP server.
		serverUrl = "http://" + getLocalHostAddress() + ":"
				+ serverSocket.getLocalPort() + "/";

		// Set the Java RMI codebase to this HTTP server.
		System.getProperties().setProperty("java.rmi.server.codebase",
				serverUrl);

		// Start the HTTP server.
		super.start();
	}

	/**
	 * @return
	 * @throws UnknownHostException
	 */
	protected String getLocalHostAddress() throws UnknownHostException {
		final InetAddress localHost = InetAddress.getLocalHost();
		String str = localHost.getHostAddress();
		if (IPAddressUtil.isIPv4LiteralAddress(str)) {
			return str;
		} else {
			return "127.0.0.1";
		}
	}

	public void addClassLoader(BindingFactoryClassLoader cl) {
		if (cl != null) {
			classLoader.add(cl);
		}
	}

	@Override
	public void run() {

		while (true) {
			try {
				Socket socket = serverSocket.accept();
				Reader in = new InputStreamReader(socket.getInputStream());
				String rq = new LineNumberReader(in).readLine();
				PrintStream out = new PrintStream(socket.getOutputStream());

				if (rq.startsWith("GET ")) {
					String url = rq.substring(5, rq.indexOf(' ', 4));
					InputStream is = null;
					for (BindingFactoryClassLoader cl : classLoader) {
						is = cl.getResourceAsStream(url);
						if (is != null) {
							break;
						}
					}
					if (is != null) {
						log.info("GET " + url + " " + is.available());
						byte[] data = new byte[is.available()];
						is.read(data);
						is.close();
						out.print("HTTP/1.0 200 OK\n\n");
						out.write(data);
					} else {
						log.fine("ClassLoader " + this.classLoader
								+ " could not return specified class " + url);
						
						String classes = "";		
						for (BindingFactoryClassLoader cl : classLoader) {
                            classes+=cl.getDefinedClasses().toString()+"\n";
                        }
                        
						log.finer("ClassLoader " + this.classLoader
						+ " defined the following classes:\n"
						+ classes);
						log
								.warning("JavaRMI HttpServer Could Not find resource "
										+ url);
						out.print("HTTP/1.0 404 Not Found\n\n");
						out.print("<html>Document not found.</html>");
					}
					out.close();
					socket.close();
				}
			} catch (IOException exc) {
				throw new RuntimeException("Can't run", exc);
			}
		}
	}

	public void shutdown() {
		try {
			log.info("shutdown");
			serverSocket.close();
		} catch (IOException exc) {
			throw new RuntimeException("Can't shutdown", exc);
		}
	}
}
