/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Valerio.Schiavoni@gmail.com
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.rmi;

import java.lang.reflect.Constructor;
import java.rmi.Remote;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Logger;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.bf.AbstractSkeleton;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;
import org.objectweb.fractal.bf.connectors.rmi.asm.RemoteInterfaceGenerator;
import org.objectweb.fractal.bf.connectors.rmi.asm.RemoteSubclassGenerator;

/**
 * An RMI skeleton to translate calls received from the underlying transport
 * layer to the business object.
 */
public class RmiSkeletonContent extends AbstractSkeleton implements
		RmiSkeletonAttributes, BindingController, LifeCycleController {
	/**
	 * The state of this Fractal component.
	 */
	private String fcState;

	/**
	 * The name of the client interface referencing the Fractal business
	 * interface.
	 */
	public static final String SERVICE_CLIENT_ITF_NAME = "service";
	/**
	 * The name of the client interface referencing the rmi registry.
	 */
	public static final String RMI_REGISTRY_CLIENT_ITF_NAME = "rmiRegistry";
	/**
	 * The logger of this skeleton.
	 */
	private final Logger log = Logger.getLogger(RmiSkeletonContent.class
			.getCanonicalName());

	/**
	 * The business object. This is a client interface, in the sense of fractal.
	 */
	private Object service;

	/**
	 * The service name to register the business object
	 */
	private String name;

	/**
	 * The http server used to dynamically download stubs/skeletons. There's 1
	 * instance of this http server for every instance of
	 * {@link RmiSkeletonContent}s. XXX A better design could be to extract this
	 * object into a component. This refactoring is delayed until it shows to be
	 * useful to someone.
	 */
	private static HttpServer httpServer;
	/**
	 * The {@link BindingFactoryClassLoader} instance used to load
	 * stub/skeletons.
	 */
	private BindingFactoryClassLoader cl;
	/**
	 * The RMI registry for this skeleton.
	 */
	private Registry registry;
	/**
	 * A reference to the object bound in the RMi registry. This reference is
	 * kept to avoid premature garbage collection or distributed garbage
	 * collection.
	 */
	private Remote rmiRef;

	/**
	 * Start the http server, optionally generate RMI stub/skeletons, register
	 * them in the RMI registry.<br/>
	 * TODO split this big method...
	 */
	public void startRmi() {
		/**
		 * Only create 1 httpServer
		 */
		if (httpServer == null) {
			try {

				httpServer = new HttpServer(cl);
				String codebase = System
						.getProperty("java.rmi.server.codebase");
				log.info("System property: java.rmi.server.codebase ->"
						+ codebase);

			} catch (Exception e1) {
				throw new RmiConnectorException(
						"Could not start the HttpServer used for dynamic code downloading",
						e1);
			}
		}else{
			httpServer.addClassLoader(cl);
		}

		try {

			/*
			 * If the service reference is not Remotable
			 */
			if (isNotRemotable(service)) {

				/*
				 * 1) Instantiate the transformer
				 */
				RemoteInterfaceGenerator transformer = new RemoteInterfaceGenerator();

				Class<?>[] interfaces = service.getClass().getInterfaces();
				/*
				 * 2) Use the first interface from the above array to generate a
				 * corresponding RMI-friendly equivalente interface.
				 */
				Class<?> toTransform = interfaces[0];
				log.finest("Canonical name for java interface to transform:"
						+ toTransform);
				/*
				 * 3) Create a corresponding java class that extends
				 * java.rmi.Remote and add a throws RemoteException to every
				 * declared method
				 */

				final Class<?> remotedJavaInterface = transformer.transform(
						toTransform, cl);

				/*
				 * 4) Create a generator that is capable of generating java
				 * classes that implements a given remote interface and a given
				 * non remote interface.
				 */
				RemoteSubclassGenerator remoteSubclassGenerator = new RemoteSubclassGenerator();

				Class<?> remotableClassForSkeletonContent = remoteSubclassGenerator
						.generate(toTransform, remotedJavaInterface, cl);
				log
						.finest("Subclass implementing java.rmi.Remote and business interface: "
								+ toTransform
								+ "correctly generated: "
								+ remotableClassForSkeletonContent);
				Constructor<?> constructor;

				try {
					/*
					 * 5) find the constructor required to istantiate the
					 * content class: it's the one with one parameter and whose
					 * type the same as the java interface originally to export.
					 */
					constructor = remotableClassForSkeletonContent
							.getConstructor(new Class[] { toTransform });
					log.finest("Constructor found: " + constructor);
					final Object newInstance = constructor
							.newInstance(new Object[] { service });

					log.finest("RMI Skeleton content class instantiated: "
							+ newInstance);
					// this should work here
					rmiRef = (Remote) newInstance;

				} catch (Exception e) {
					throw new RmiConnectorException(
							"Some error occurred while generating RMI classes for interface: "
									+ service, e);
				}
			} else {
				rmiRef = (Remote) service;

			}
			Remote remote = UnicastRemoteObject.exportObject(rmiRef, 0);

			registry.bind(name, remote);

		} catch (Exception e) {
			throw new RmiConnectorException(e);

		}

	}

	/**
	 * True if the interface is not assignable to {@link Remote}, false
	 * otherwise.
	 * 
	 * @param interfaceToExport
	 *            a java interface
	 * @return
	 * @See {@link Class#isAssignableFrom(Class)}
	 */
	private boolean isNotRemotable(Object interfaceToExport) {

		return !Remote.class.isAssignableFrom(interfaceToExport.getClass());
	}

	// attribute controller implementation
	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.RmiSkeletonAttributes#setServiceName(java.lang.String)
	 */
	public void setServiceName(String name) {
		this.name = name;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.RmiSkeletonAttributes#getServiceName()
	 */
	public String getServiceName() {
		return this.name;
	}

	public void setServiceObject(Object service) {
		this.service = service;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.RmiSkeletonAttributes#getServiceObject()
	 */
	public Object getServiceObject() {
		return this.service;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.RmiSkeletonAttributes#setBindingFactoryClassLoader(BindingFactoryClassLoader)
	 */
	public void setBindingFactoryClassLoader(BindingFactoryClassLoader cl) {
		this.cl = cl;

	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.RmiSkeletonAttributes#getBindingFactoryClassLoader()
	 */
	public BindingFactoryClassLoader getBindingFactoryClassLoader() {
		return this.cl;
	}

	// binding-controller implementation
	@Override
	public void bindFc(String clientItfName, Object serverItf)
			throws NoSuchInterfaceException, IllegalBindingException,
			IllegalLifeCycleException {
		if (clientItfName.equalsIgnoreCase(SERVICE_CLIENT_ITF_NAME)) {
			this.service = serverItf;
		} else if (clientItfName.equalsIgnoreCase(RMI_REGISTRY_CLIENT_ITF_NAME)) {
			this.registry = (Registry) serverItf;
		} else
			super.bindFc(clientItfName, serverItf);
	}

	@Override
	public String[] listFc() {
		String[] superItfs = super.listFc();
		String[] itfs = new String[superItfs.length + 2];
		int i = 0;
		while (i < superItfs.length) {
			itfs[i] = superItfs[i];
			i++;
		}
		itfs[i++] = SERVICE_CLIENT_ITF_NAME;
		itfs[i] = RMI_REGISTRY_CLIENT_ITF_NAME;

		return itfs;
	}

	@Override
	public Object lookupFc(String clientItfName)
			throws NoSuchInterfaceException {
		if (clientItfName.equalsIgnoreCase(SERVICE_CLIENT_ITF_NAME)) {
			return this.service;
		} else if (clientItfName.equalsIgnoreCase(RMI_REGISTRY_CLIENT_ITF_NAME)) {
			return this.registry;
		} else
			return super.lookupFc(clientItfName);
	}

	@Override
	public void unbindFc(String clientItfName) throws NoSuchInterfaceException,
			IllegalBindingException, IllegalLifeCycleException {
		if (clientItfName.equalsIgnoreCase(SERVICE_CLIENT_ITF_NAME)) {
			this.service = null;
		} else if (clientItfName.equalsIgnoreCase(RMI_REGISTRY_CLIENT_ITF_NAME)) {
			this.registry = null;
		} else
			super.unbindFc(clientItfName);
	}

	/**
	 * @see org.objectweb.fractal.api.control.LifeCycleController#getFcState()
	 */
	public String getFcState() {
		return this.fcState;
	}

	/**
	 * @see org.objectweb.fractal.api.control.LifeCycleController#startFc()
	 */
	public void startFc() throws IllegalLifeCycleException {
		startRmi();
		this.fcState = LifeCycleController.STARTED;
	}

	/**
	 * @see org.objectweb.fractal.api.control.LifeCycleController#stopFc()
	 */
	public void stopFc() throws IllegalLifeCycleException {
		try {
			/*
			 * unbind this service from the registry
			 */
			this.registry.unbind(name);
			/*
			 * set this skeleton as stopped
			 */
			this.fcState = LifeCycleController.STOPPED;
		} catch (Exception e) {
			throw new RmiConnectorException(
					"Cannot unbind the service from the RMI registry", e);
		}

	}

}
