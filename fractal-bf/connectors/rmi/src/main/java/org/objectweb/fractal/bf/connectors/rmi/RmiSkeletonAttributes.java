/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.rmi;

import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;

/**
 *To configure {@link RmiSkeletonContent} components.
 * 
 * @since 0.6
 */
public interface RmiSkeletonAttributes extends AttributeController {

	String RMI_CONNECTOR_ATTRIBUTES = "rmiAttributes";

	void setServiceObject(Object service);

	public Object getServiceObject();

	void setBindingFactoryClassLoader(BindingFactoryClassLoader cl);

	public BindingFactoryClassLoader getBindingFactoryClassLoader();

	void setServiceName(String name);

	public String getServiceName();

}
