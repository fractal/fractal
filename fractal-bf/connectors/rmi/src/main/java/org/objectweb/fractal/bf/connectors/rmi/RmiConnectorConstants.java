/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.rmi;

/**
 * Rmi Connector constants.
 */
public interface RmiConnectorConstants {
	/**
	 * The name of the parameter accepted by this connector to set the name of
	 * the exported service, or to retrive the name of a service from the RMI
	 * registry.
	 */
	static final String SERVICE_NAME = "serviceName";
	/**
	 * The port used to create the java rmi registry.
	 */
	static final String JAVA_RMI_REGISTRY_DEFAULT_PORT = "1099";
	/**
	 * The name of the parameter accepted by this connector to set the port of
	 * the registry.
	 */
	static final String JAVA_RMI_REGISTRY_PORT = "port";
	/**
	 * The name of the parameter accepted by this connector to set the host of
	 * the registry.
	 */
	static final String JAVA_RMI_REGISTRY_HOST_ADDRESS = "hostAddress";
	/**
	 * The default host address used by this connector when the given
	 * hostAddress value refers to a registry that can't be used.
	 */
	static final String JAVA_RMI_REGISTRY_HOST_ADDRESS_DEFAULT = "localhost";
}
