/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Valerio.Schiavoni@gmail.com
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.rmi;

import java.rmi.registry.Registry;
import java.util.logging.Logger;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.bf.proxies.ASMStubGenerator;

/**
 * An abstract RMI stub content. Concrete implementations are generated on the
 * fly in the {@link RmiConnector} class by using the {@link ASMStubGenerator}
 * utility class.
 * 
 * @since 0.6
 */
public abstract class AbstractRmiStubContent implements RmiStubAttributes,
		BindingController, LifeCycleController {

	Logger log = Logger.getLogger(AbstractRmiStubContent.class
			.getCanonicalName());
	private Registry registry;
	private String serviceName;
	private String fcState;
	/**
	 * The name of the client interface referencing the rmi registry.
	 */
	public static final String RMI_REGISTRY_CLIENT_ITF_NAME = "rmiRegistry";
	/**
	 * Name of the remoteRef field
	 */
	public static String REMOTE_REF_FIELD_NAME = "remoteRef";

	/**
	 * A ref to a stub object recovered from the registry.
	 */
	protected Object remoteRef;

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.RmiStubAttributes#getServiceName()
	 */
	public String getServiceName() {
		return this.serviceName;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.RmiStubAttributes#setServiceName(java.lang.String)
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	// lifecycle-controller implementation
	/**
	 * @see org.objectweb.fractal.api.control.LifeCycleController#getFcState()
	 */
	public String getFcState() {
		return this.fcState;
	}

	/**
	 * @see org.objectweb.fractal.api.control.LifeCycleController#startFc()
	 */
	public void startFc() throws IllegalLifeCycleException {
		if (registry == null) {
			throw new IllegalStateException("Stub not bound to rmiRegistry");
		}
		try {
			// Make sure that the class loader used by registry.lookup() is
			// the one used to generate this RMI stub component class.
			Thread currentThread = Thread.currentThread();
			ClassLoader currentThreadContextClassLoader = currentThread.getContextClassLoader();
			currentThread.setContextClassLoader(getClass().getClassLoader());
			try {
				this.remoteRef = registry.lookup(serviceName);
			} finally {
				currentThread.setContextClassLoader(currentThreadContextClassLoader);
			}
		} catch (Exception e) {
			IllegalLifeCycleException ilce = new IllegalLifeCycleException(
					"Could not start RMI stub");
			ilce.initCause(e);
			throw ilce;
		}
	}

	/**
	 * @see org.objectweb.fractal.api.control.LifeCycleController#stopFc()
	 */
	public void stopFc() throws IllegalLifeCycleException {
		throw new UnsupportedOperationException("not done yet");
	}

	// binding-controller implementation

	public void bindFc(String clientItf, Object serverItf)
			throws NoSuchInterfaceException, IllegalBindingException,
			IllegalLifeCycleException {
		if (clientItf.equalsIgnoreCase(RMI_REGISTRY_CLIENT_ITF_NAME)) {
			this.registry = (Registry) serverItf;
			return;
		} else if (clientItf.equalsIgnoreCase(REMOTE_REF_FIELD_NAME)) {
			log.warning("Set remoteRef via bindFc, this is not expected");
			this.remoteRef = serverItf;
			return;
		}
		throw new NoSuchInterfaceException("Cannot find client interface "
				+ clientItf);
	}

	public String[] listFc() {
		return new String[] { RMI_REGISTRY_CLIENT_ITF_NAME };
	}

	public Object lookupFc(String clientItfName)
			throws NoSuchInterfaceException {
		Object res = null;
		if (clientItfName.equalsIgnoreCase(RMI_REGISTRY_CLIENT_ITF_NAME)) {
			res = this.registry;
		}
		return res;
	}

	public void unbindFc(String clientItfName) throws NoSuchInterfaceException,
			IllegalBindingException, IllegalLifeCycleException {
		if (clientItfName.equalsIgnoreCase(RMI_REGISTRY_CLIENT_ITF_NAME)) {
			this.registry = null;
			return;
		}
		throw new NoSuchInterfaceException("Cannot find client interface "
				+ clientItfName);
	}
}
