/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.rmi;

import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Logger;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;

/**
 * Fractalization of the Java RMI registry.
 */
public class RmiRegistry implements Registry, RmiRegistryAttributes,
		LifeCycleController {

	Logger log = Logger.getLogger(RmiRegistry.class.getCanonicalName());
	/**
	 * The port of the registry. If not set, the {@link Registry#REGISTRY_PORT}
	 * is used.
	 */
	private int port;
	/**
	 * The host into which search the RMI registry. If not set, localhost is
	 * used.
	 */
	private String host;
	/**
	 * The RMI registry being wrapped by this Fractal component.
	 */
	private Registry theRegistry;
	/**
	 * The state of the Fractal component.
	 */
	private String fcState;

	/**
	 * @see java.rmi.registry.Registry#bind(java.lang.String, java.rmi.Remote)
	 */
	public void bind(String name, Remote obj) throws RemoteException,
			AlreadyBoundException, AccessException {
		theRegistry.bind(name, obj);
	}

	/**
	 * @see java.rmi.registry.Registry#list()
	 */
	public String[] list() throws RemoteException, AccessException {
		return theRegistry.list();
	}

	/**
	 * @see java.rmi.registry.Registry#lookup(java.lang.String)
	 */
	public Remote lookup(String name) throws RemoteException,
			NotBoundException, AccessException {
		return theRegistry.lookup(name);
	}

	/**
	 * @see java.rmi.registry.Registry#rebind(java.lang.String, java.rmi.Remote)
	 */
	public void rebind(String name, Remote obj) throws RemoteException,
			AccessException {
		theRegistry.rebind(name, obj);
	}

	/**
	 * @see java.rmi.registry.Registry#unbind(java.lang.String)
	 */
	public void unbind(String name) throws RemoteException, NotBoundException,
			AccessException {
		theRegistry.unbind(name);
	}

	/**
	 * @return the port
	 */
	public final int getPort() {
		return port;
	}

	/**
	 * @param port
	 *            the port to set
	 */
	public final void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return the host
	 */
	public final String getHost() {
		return host;
	}

	/**
	 * @param host
	 *            the host to set
	 */
	public final void setHost(String host) {
		this.host = host;
		/*
		 * how to notify skeletons that this values was modified ?
		 */
	}

	/**
	 * @see org.objectweb.fractal.api.control.LifeCycleController#getFcState()
	 */
	public String getFcState() {
		return this.fcState;
	}

	/**
	 * @see org.objectweb.fractal.api.control.LifeCycleController#startFc()
	 */
	public void startFc() throws IllegalLifeCycleException {

		if (port <= 0) {
			log.warning("No port set for RMI registry, will use default:"
					+ RmiConnectorConstants.JAVA_RMI_REGISTRY_DEFAULT_PORT);
			port = Registry.REGISTRY_PORT;
		}
		try {

			theRegistry = LocateRegistry.getRegistry(host, port);
			log.fine("JavaRMI RMI registry recovered :"
					+ theRegistry.toString());
			/*
			 * ping the registry, to see if it's really accessible
			 */
			theRegistry.list();

			this.fcState = LifeCycleController.STARTED;
			log.info("JavaRMI registry started at " + host + ":" + port);

		} catch (Exception e) {
			// create it locally

			try {
				theRegistry = LocateRegistry.createRegistry(port);
				this.fcState = LifeCycleController.STARTED;

			} catch (RemoteException re) {
				throw new RmiRegistryCreationException(
						"Cannot create a JavaRMI registry on localhost:" + port,
						re);
			}
		} finally {
			/*
			 * how to notify skeletons that this values was modified ?
			 */
		}

	}

	/**
	 * @see org.objectweb.fractal.api.control.LifeCycleController#stopFc()
	 */
	public void stopFc() throws IllegalLifeCycleException {
		theRegistry = null;
		this.fcState = LifeCycleController.STOPPED;

	}

}
