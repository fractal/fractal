package org.objectweb.fractal.bf.connectors.rmi.fixtures;

import java.rmi.RemoteException;

/**
 * to drive asm-generation of subclasses.
 */
public class SubClass extends ItfPrimitiveTypesImpl {
	public SubClass() {

	}

	void i() {
		inheritedField.i(0);
	}

	void p() {
		System.err.println("Entering method p");
		ItfPrimitiveTypes casted = (ItfPrimitiveTypes) super.remoteRef;
		casted.i(23213);

	}

	void a() throws RemoteException {
		try {
			p();
		} catch (Exception e) {
			RemoteException re = new RemoteException();
			re.initCause(e);
			throw re;
		}
	}
}
