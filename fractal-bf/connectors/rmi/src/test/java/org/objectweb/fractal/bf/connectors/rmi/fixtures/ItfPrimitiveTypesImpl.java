/**
 * Author: Valerio Schiavoni <valerio.schiavoni@gmail.com>
 */
package org.objectweb.fractal.bf.connectors.rmi.fixtures;

/**
 * 
 */
public class ItfPrimitiveTypesImpl implements ItfPrimitiveTypes {
	ItfPrimitiveTypes s;

	protected ItfPrimitiveTypes inheritedField;

	protected Object remoteRef;

	public ItfPrimitiveTypesImpl() {

	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes#byteParamType(byte)
	 */
	public void byteParamType(byte b) {
		s.byteParamType(b);

	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes#byteParamType(java.lang.Byte)
	 */
	public void byteParamType(Byte bytes, Object o) {
		s.byteParamType(bytes);

	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes#byteParamType(java.lang.Byte)
	 */
	public void byteParamType(Byte b, byte b1) {

	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes#byteParamType(java.lang.Byte)
	 */
	public void byteParamType(Byte b) {
		// TODO Auto-generated method stub

	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes#booleanParamType(boolean)
	 */
	public void booleanParamType(boolean b) {
		// TODO Auto-generated method stub

	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes#charParamType(char)
	 */
	public void charParamType(char c) {
		// TODO Auto-generated method stub

	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes#doubleParamType(double)
	 */
	public void doubleParamType(double d) {
		// TODO Auto-generated method stub

	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes#floatParamType(float)
	 */
	public void floatParamType(float f) {
		// TODO Auto-generated method stub

	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes#intParamType(int)
	 */
	public void intParamType(int i) {
		// TODO Auto-generated method stub

	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes#longParamType(long)
	 */
	public void longParamType(long l) {
		// TODO Auto-generated method stub

	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes#shortParamType(short)
	 */
	public void shortParamType(short s) {
		// TODO Auto-generated method stub

	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes#b(byte)
	 */
	public byte b(byte b) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes#b(boolean)
	 */
	public boolean b(boolean b) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes#c(char)
	 */
	public char c(char c) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes#d(double)
	 */
	public double d(double d) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes#f(float)
	 */
	public float f(float f) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes#i(int)
	 */
	public int i(int i) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes#l(long)
	 */
	public long l(long l) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes#s(short)
	 */
	public short s(short s) {
		// TODO Auto-generated method stub
		return 0;
	}

}
