/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.rmi.asm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;
import org.objectweb.fractal.bf.connectors.BasicServiceImpl;
import org.objectweb.fractal.bf.connectors.Service;
import org.objectweb.fractal.bf.connectors.rmi.fixtures.SerializableService;

/**
 * Test the {@link RemoteInterfaceGenerator}.
 */
public class RemoteInterfaceGeneratorTest {

	RemoteInterfaceGenerator rt;

	Service s;

	private final BindingFactoryClassLoader cl = new BindingFactoryClassLoader(
			Thread.currentThread().getContextClassLoader());

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		rt = new RemoteInterfaceGenerator();
		s = new BasicServiceImpl();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testTansformClassThrowsException() {
		rt.transform(BasicServiceImpl.class, cl);
	}

	@Test
	public void testCreateRemoteForJuliaGeneratedInterface()
			throws ADLException, NoSuchInterfaceException {
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
		Factory factory = FactoryFactory
				.getFactory(FactoryFactory.FRACTAL_BACKEND);
		Component service = (Component) factory.newComponent(
				"org.objectweb.fractal.bf.connectors.Service", null);
		Object juliaItf = service.getFcInterface("service");

		Class<?> clazz = juliaItf.getClass().getInterfaces()[0];
		assertNotNull(clazz);

		Class<?> remote = rt.transform(clazz, cl);
		checkClass(remote);
	}

	@Test
	public void transform() {
		assertFalse(Remote.class.isAssignableFrom(Service.class));
		Class<?> remote = rt.transform(Service.class, cl);
		assertTrue(
				"The generated interface must be compatible with the original type",
				Service.class.isAssignableFrom(remote));

		checkClass(remote);

	}

	@Test
	public void testComputeInterfacesfrom_1() {
		final String[] itfs = rt.computeInterfacesfrom(Service.class);
		assertEquals(3, itfs.length);
		assertEquals("java/rmi/Remote", itfs[0]);
		assertEquals("java/io/Serializable", itfs[2]);
	}

	@Test
	public void testComputeInterfacesfrom_2() {

		final String[] itfs = rt
				.computeInterfacesfrom(SerializableService.class);
		assertEquals(2, itfs.length);
		assertEquals("java/rmi/Remote", itfs[0]);
	}

	private void checkClass(Class<?> remotifiedInterface) {
		assertNotNull(remotifiedInterface);
		assertTrue("The resulting class must implements java.rmi.Remote",
				Remote.class.isAssignableFrom(remotifiedInterface));
		assertTrue("The resulting class must implements java.rmi.Serializable",
				Serializable.class.isAssignableFrom(remotifiedInterface));

		for (Method m : remotifiedInterface.getMethods()) {
			Class<?>[] exceptionTypes = m.getExceptionTypes();
			assertTrue(Arrays.asList(exceptionTypes).contains(
					RemoteException.class));
		}
	}
}
