/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.rmi.fixtures;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.xml.datatype.XMLGregorianCalendar;

import org.objectweb.fractal.bf.connectors.HelloWorldException;
import org.objectweb.fractal.bf.connectors.Pojo;
import org.objectweb.fractal.bf.connectors.RemotableService;
import org.objectweb.fractal.bf.connectors.Service;

/**
 * This class is here to drive ASM programming.
 */
public class MyUnicastRemoteObject implements RemotableService, Serializable {

	private static final long serialVersionUID = 3385393185946916267L;
	public Service s;
	public Object obj;

	protected MyUnicastRemoteObject(Service s) throws RemoteException {
		this.s = s;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.RemotableService#getCurrentDate()
	 */
	public XMLGregorianCalendar getCurrentDate() throws RemoteException {
		return s.getCurrentDate();
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.RemotableService#getPojo()
	 */
	public Pojo getPojo() throws RemoteException {
		return s.getPojo();
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.RemotableService#print()
	 */
	public void print() throws RemoteException {
		s.print();

	}

	public Pojo child(Pojo p) {
		return this.s.child(p);
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.rmi.fixtures.RemotableService#printAndAnswer()
	 */
	public String printAndAnswer() throws RemoteException {
		return s.printAndAnswer();

	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.RemotableService#badMethod()
	 */
	public void badMethod() throws HelloWorldException, RemoteException {
		s.badMethod();
	}

	public void printWithObj() {
		System.err.println(obj);
		((Service) obj).print();
	}

}
