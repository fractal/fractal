/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Valerio.Schiavoni@gmail.com
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.rmi;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.rmi.AccessException;
import java.rmi.NoSuchObjectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.bf.AbstractSkeleton;

/**
 * Test the {@link RmiSkeletonContent} class.
 */
public class RmiSkeletonContentTest {

	private static final String TEST_SERVICE = "testService";
	protected RmiSkeletonContent skeleton;

	private Registry rmiRegistry;
	private static final int RMI_PORT_INT = 9915;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		rmiRegistry = LocateRegistry.createRegistry(RMI_PORT_INT);
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
		Factory factory = FactoryFactory
				.getFactory(FactoryFactory.FRACTAL_BACKEND);
		Component remotableService = (Component) factory.newComponent(
				"org.objectweb.fractal.bf.connectors.RemotableService", null);
		skeleton = new RmiSkeletonContent();
		// fake fc binding
		skeleton.bindFc("rmiRegistry", rmiRegistry);

		skeleton.setServiceName(TEST_SERVICE);
		skeleton
				.bindFc(
						RmiSkeletonContent.SERVICE_CLIENT_ITF_NAME,
						remotableService
								.getFcInterface(RmiSkeletonContent.SERVICE_CLIENT_ITF_NAME));
	}

	@After
	public void tearDown() throws NoSuchObjectException {
		UnicastRemoteObject.unexportObject(rmiRegistry, true);
	}

	/**
	 * Test method for
	 * {@link org.objectweb.fractal.bf.connectors.rmi.RmiSkeletonContent#startRmi()}
	 * .
	 * 
	 * @throws RemoteException
	 * @throws AccessException
	 * @throws IllegalLifeCycleException
	 * @throws NotBoundException
	 */
	@Test
	public void testStartFc() throws AccessException, RemoteException,
			IllegalLifeCycleException, NotBoundException {
		skeleton.startFc();
		final int registeredServices = rmiRegistry.list().length;
		assertEquals(registeredServices, 1);

		rmiRegistry.unbind(TEST_SERVICE);
		assertEquals(rmiRegistry.list().length, 0);
	}

	@Test
	public void testStopFc() throws IllegalLifeCycleException, AccessException,
			RemoteException {
		// start it
		skeleton.startFc();
		Integer registeredServices = new Integer(rmiRegistry.list().length);
		assertThat(registeredServices, is(new Integer(1)));
		assertThat(skeleton.getFcState(), is(LifeCycleController.STARTED));
		// now stop it
		skeleton.stopFc();
		registeredServices = new Integer(rmiRegistry.list().length);
		assertThat(registeredServices, is(new Integer(0)));
		assertThat(skeleton.getFcState(), is(LifeCycleController.STOPPED));

	}

	@Test
	public void testListFc() {
		String[] itfs = skeleton.listFc();
		assertEquals(4, itfs.length);
		assertEquals(AbstractSkeleton.DELEGATE_CLIENT_ITF_NAME, itfs[0]);
        assertEquals(RmiSkeletonContent.SERVANT_CLIENT_ITF_NAME, itfs[1]);
		assertEquals(RmiSkeletonContent.SERVICE_CLIENT_ITF_NAME, itfs[2]);
		assertEquals(RmiSkeletonContent.RMI_REGISTRY_CLIENT_ITF_NAME, itfs[3]);

	}
}
