/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.rmi;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.UnknownHostException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;

import sun.net.util.IPAddressUtil;

public class HttpServerTest {

	HttpServer server;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		server = new HttpServer(new BindingFactoryClassLoader(Thread
				.currentThread().getContextClassLoader()));
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		server.shutdown();
	}

	/**
	 * Test method for
	 * {@link org.objectweb.fractal.bf.connectors.rmi.HttpServer#getLocalHostAddress()}
	 * .
	 */
	@Test
	public void testGetLocalHostAddress() {
		try {
			final String localHostAddress = server.getLocalHostAddress();
			assertTrue("The localHostAddress '" + localHostAddress
					+ "' doesn't seem to be a valid ipv4 address.",
					IPAddressUtil.isIPv4LiteralAddress(localHostAddress));

		} catch (UnknownHostException e) {
			fail("An host can always be found");
		}
	}

}
