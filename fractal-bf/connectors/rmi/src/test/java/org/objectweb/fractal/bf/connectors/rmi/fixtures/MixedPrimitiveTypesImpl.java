/**
 * Author: Valerio Schiavoni <valerio.schiavoni@gmail.com>
 */
package org.objectweb.fractal.bf.connectors.rmi.fixtures;

public class MixedPrimitiveTypesImpl implements MixedPrimitiveTypesItf {
	MixedPrimitiveTypesItf s;

	public byte a(short s, long bytes, double d, short s1, long l2, long l3) {
		return this.s.a(s, bytes, d, s1, l2, l3);
	}

}
