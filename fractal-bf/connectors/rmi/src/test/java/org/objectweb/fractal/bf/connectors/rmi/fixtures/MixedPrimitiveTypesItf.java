/**
 * Author: Valerio Schiavoni <valerio.schiavoni@gmail.com>
 */
package org.objectweb.fractal.bf.connectors.rmi.fixtures;

/**
 * 
 */
public interface MixedPrimitiveTypesItf {
	// mixing primtive types
	public byte a(short s, long l, double d, short s1, long l2, long l3);
}
