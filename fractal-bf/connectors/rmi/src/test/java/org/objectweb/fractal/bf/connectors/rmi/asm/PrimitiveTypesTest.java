/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.rmi.asm;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.junit.Test;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;
import org.objectweb.fractal.bf.connectors.rmi.fixtures.ItfPrimitiveTypes;

public class PrimitiveTypesTest {

	private final RemoteSubclassGenerator generator = new RemoteSubclassGenerator();
	private final RemoteInterfaceGenerator rt = new RemoteInterfaceGenerator();
	private final BindingFactoryClassLoader cl = new BindingFactoryClassLoader(
			Thread.currentThread().getContextClassLoader());

	@Test
	public void testOnInterfacesUsingPrimitiveTypes() {

		Class<?> remote = rt.transform(ItfPrimitiveTypes.class, cl);
		Class<?> generated = generator.generate(ItfPrimitiveTypes.class,
				remote, cl);
		Constructor<?> oneArgConstructor;
		try {
			oneArgConstructor = generated
					.getConstructor(new Class[] { ItfPrimitiveTypes.class });
			assertNotNull("Cannot find the constructor to be used",
					oneArgConstructor);
		} catch (SecurityException e) {

			e.printStackTrace();
			fail();
		} catch (NoSuchMethodException e) {

			e.printStackTrace();
			fail();
		}

		// final Object newInstance = oneArgConstructor
		// .newInstance(new Object[] { this.serviceImpl });
		//
		// assertNotNull(newInstance);
	}

	@Test
	public void discoverPrimitiveTypes() throws SecurityException,
			NoSuchMethodException {
		Method byteParamPrimitive = ItfPrimitiveTypes.class.getMethod(
				"byteParamType", new Class[] { byte.class });
		Class<?>[] paramTypes = byteParamPrimitive.getParameterTypes();
		// final TypeVariable<Method>[] typeParameters = byteParamPrimitive
		// .getTypeParameters();

		assertTrue(paramTypes[0].isPrimitive());

	}

}
