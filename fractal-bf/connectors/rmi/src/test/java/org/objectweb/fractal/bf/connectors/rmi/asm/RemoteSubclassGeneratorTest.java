/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.rmi.asm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;
import org.objectweb.fractal.bf.connectors.BasicServiceImpl;
import org.objectweb.fractal.bf.connectors.Pojo;
import org.objectweb.fractal.bf.connectors.RemotableService;
import org.objectweb.fractal.bf.connectors.Service;

/**
 * Test the {@link RemoteSubclassGenerator} class.
 */
public class RemoteSubclassGeneratorTest {

	Logger log = Logger.getLogger(RemoteSubclassGeneratorTest.class
			.getCanonicalName());
	private BindingFactoryClassLoader cl;
	private RemoteSubclassGenerator generator;
	private BasicServiceImpl serviceImpl;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.generator = new RemoteSubclassGenerator();
		this.serviceImpl = new BasicServiceImpl();
		cl = new BindingFactoryClassLoader(Thread.currentThread()
				.getContextClassLoader());
	}

	@Test
	public void generateSubclassRemote() {
		assertFalse(UnicastRemoteObject.class
				.isAssignableFrom(RemotableService.class));
		Class<?> generated = generator.generate(Service.class,
				RemotableService.class, cl);

		assertNotNull("The generated class should not be null", generated);
		assertTrue("The generated class should implement java.io.Serializable",
				Serializable.class.isAssignableFrom(generated));
		assertTrue(RemotableService.class.isAssignableFrom(generated));

		try {
			Field s = generated.getField("s");
			assertNotNull(s);

			assertTrue("The type of the 's' field is not correct", s.getType()
					.isAssignableFrom(Service.class));
			ClassLoader clField = s.getDeclaringClass().getClassLoader();
			ClassLoader clService = Service.class.getClassLoader();
			assertEquals(clField.getParent(), clService);

		} catch (SecurityException e1) {
			e1.printStackTrace();
			fail("Generated field 's' was not accessible");
		} catch (NoSuchFieldException e1) {
			e1.printStackTrace();
			fail("Can't find field 's' in generated class");
		}

		Constructor<?>[] construtors = generated.getConstructors();
		assertEquals(1, construtors.length);

		try {
			Constructor<?> oneArgConstructor = generated
					.getConstructor(new Class[] { Service.class });

			assertNotNull("Cannot find the constructor to be used",
					oneArgConstructor);

			final Object newInstance = oneArgConstructor
					.newInstance(new Object[] { this.serviceImpl });

			assertNotNull(newInstance);

			RemotableService generatedRemotableService = (RemotableService) newInstance;
			generatedRemotableService.print();
			String answer = generatedRemotableService.printAndAnswer();
			assertEquals(answer, BasicServiceImpl.SERVICE_REPLY);
			Pojo pojoFromRMI = generatedRemotableService.getPojo();
			assertNotNull(
					"the pojo fom the remotable service should not be null",
					pojoFromRMI);

		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
}
