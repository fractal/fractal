package org.objectweb.fractal.bf.connectors.rmi.fixtures;

import java.io.Serializable;

import org.objectweb.fractal.bf.connectors.Service;

public interface SerializableService extends Service, Serializable {

}
