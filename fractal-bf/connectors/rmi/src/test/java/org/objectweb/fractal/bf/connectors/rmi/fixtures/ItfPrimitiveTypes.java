package org.objectweb.fractal.bf.connectors.rmi.fixtures;

/**
 * An interface whose methods use only primitive types.
 */
public interface ItfPrimitiveTypes {
	/*
	 * methods without return values
	 */
	public void byteParamType(byte b);

	public void byteParamType(Byte b);

	public void shortParamType(short s);

	public void doubleParamType(double d);

	public void intParamType(int i);

	public void longParamType(long l);

	public void floatParamType(float f);

	public void booleanParamType(boolean b);

	public void charParamType(char c);

	/* methods with return values */
	public byte b(byte b);

	public double d(double d);

	public short s(short s);

	public int i(int i);

	public long l(long l);

	public float f(float f);

	public boolean b(boolean b);

	public char c(char c);

}