/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.rmi;

import static org.junit.Assert.assertNotNull;

import java.rmi.NoSuchObjectException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;
import org.objectweb.fractal.bf.connectors.BasicRemotableServiceImpl;
import org.objectweb.fractal.bf.connectors.RemotableService;

public class RmiConnectorBindTest {

	private static final String RMI_SERVICE_NAME = "service";

	private RmiConnector rmiConnector;

	private Component client;

	private Registry rmiRegistry;

	private static final int RMI_PORT_INT = 9902;
	private static final String RMI_PORT = "9902";

	@BeforeClass
	public static void initFractal() {
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");

	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

		rmiRegistry = LocateRegistry.createRegistry(RMI_PORT_INT);

		this.rmiConnector = new RmiConnector();
		Factory factory = FactoryFactory
				.getFactory(FactoryFactory.FRACTAL_BACKEND);
		client = (Component) factory.newComponent(
				"org.objectweb.fractal.bf.connectors.RemotableClient", null);

		RemotableService remotable = new BasicRemotableServiceImpl();
		rmiRegistry.bind(RMI_SERVICE_NAME, remotable);

		BindingFactoryClassLoader cl = new BindingFactoryClassLoader(Thread
				.currentThread().getContextClassLoader());
		rmiConnector.setBindingFactoryClassLoader(cl);

	}

	@Test
	public void testCreateStub() throws Exception {
		final Component stub = rmiConnector.createStub(client,
				RMI_SERVICE_NAME, createBindHints());

		assertNotNull(stub);

	}

	private RmiBindHints createBindHints() {
		RmiBindHints hints = new RmiBindHints();
		hints.setServiceName(RMI_SERVICE_NAME);
		hints.setHost("localhost");
		hints.setPort(RMI_PORT);

		return hints;
	}

	@After
	public void tearDown() throws NoSuchObjectException {
		UnicastRemoteObject.unexportObject(rmiRegistry, true);
	}
}
