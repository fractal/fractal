/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.rmi;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.lang.reflect.Method;
import java.rmi.Remote;
import java.rmi.registry.Registry;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;
import org.objectweb.fractal.bf.connectors.BasicServiceImpl;
import org.objectweb.fractal.bf.connectors.BasicServiceImplFCprimitiveFCa34ec92d;
import org.objectweb.fractal.bf.connectors.tinfi.BasicServiceImplFCscaPrimitiveFCe78ce1d5;
import org.objectweb.fractal.bf.connectors.tinfi.Service;
import org.objectweb.fractal.util.ContentControllerHelper;
import org.objectweb.fractal.util.Fractal;

/**
 * Test the Rmi connector in isolation.
 */
public class RmiConnectorTest {

	private static final String RMI_SERVICE_NAME = "service";

	private RmiConnector rmiConnector;

	private Component remotableService;

	private Component nonRemotableService;

	private Component juliaComposite;

	private Component rmiRegistry;

	private static final String RMI_PORT = "9910";

	private Remote remoteReference;

	public static BindingFactoryClassLoader cl;

	@After
	public void tearDown() {
		try {

			final Registry registry = (Registry) rmiRegistry
					.getFcInterface("rmiRegistry");
			registry.unbind(RMI_SERVICE_NAME);
			assertEquals(0, registry.list().length);

			this.rmiConnector = null;

		} catch (Exception e) {

		}
	}

	@BeforeClass
	public static void init() {
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
		// allow everything
		System.setProperty("java.security.policy", "myrmi.policy");
		cl = new BindingFactoryClassLoader(Thread.currentThread()
				.getContextClassLoader());
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

		// the connector to test
		this.rmiConnector = new RmiConnector();

		this.rmiConnector.setBindingFactoryClassLoader(cl);

		Factory factory = FactoryFactory
				.getFactory(FactoryFactory.FRACTAL_BACKEND);
		remotableService = (Component) factory.newComponent(
				"org.objectweb.fractal.bf.connectors.RemotableService", null);

		Fractal.getLifeCycleController(remotableService).startFc();

		Interface it = (Interface) remotableService
				.getFcInterface(RMI_SERVICE_NAME);
		assertEquals(it.getFcItfOwner(), remotableService);

		nonRemotableService = (Component) factory.newComponent(
				"org.objectweb.fractal.bf.connectors.Service", null);

		Fractal.getLifeCycleController(nonRemotableService).startFc();

		Component compositeContainer = (Component) factory
				.newComponent(
						"org.objectweb.fractal.bf.connectors.EnclosingEnclosingService",
						null);

		this.juliaComposite = ContentControllerHelper.getSubComponentByName(
				compositeContainer, "service");
		assertNotNull("Could not find julia composite", this.juliaComposite);
		Fractal.getLifeCycleController(juliaComposite).startFc();

		if (System.getSecurityManager() == null)
			System.setSecurityManager(new java.rmi.RMISecurityManager());

	}

	@Test
	public void testCreateSkeletonFromComponentWithRemoteFcInterface()
			throws Exception {

		exportAndCheck(remotableService);

	}

	@Test
	public void testCreateSkeletonFromComponentWithoutRemotableFcInterface()
			throws Exception {
		exportAndCheck(nonRemotableService);
	}

	@Test
	public void testCreateSkeletonFromJuliacGeneratedComponent()
			throws Exception {

		Component juliacComponent = null;
		try {

			juliacComponent = ((org.objectweb.fractal.juliac.runtime.Factory) Class
					.forName(
							BasicServiceImplFCprimitiveFCa34ec92d.class
									.getCanonicalName(), true, cl)
					.newInstance()).newFcInstance();

			Fractal.getLifeCycleController(juliacComponent).startFc();
			exportAndCheck(juliacComponent);
		} catch (InstantiationException e1) {
			e1.printStackTrace();
			fail("cannot instantiate Juliac component");
		}
	}

	@Test
	public void testCreateSkeletonFromTinfiGeneratedComponent()
			throws Exception {

		Component tinfiComponent = null;
		try {
			tinfiComponent = ((org.objectweb.fractal.juliac.runtime.Factory) Class
					.forName(
							BasicServiceImplFCscaPrimitiveFCe78ce1d5.class
									.getCanonicalName(), true, cl)
					.newInstance()).newFcInstance();
			Fractal.getLifeCycleController(tinfiComponent).startFc();
			Service tinfiService = (Service) tinfiComponent
					.getFcInterface(RMI_SERVICE_NAME);
			tinfiService.print();
			exportAndCheck(tinfiComponent);
		} catch (InstantiationException e1) {
			e1.printStackTrace();
			fail("cannot instantiate Tinfi component");
		}
	}

	@Test
	public void testCreateSkeletonFromJuliaComposite() throws Exception {
		exportAndCheck(juliaComposite);
	}

	protected void exportAndCheck(Component comp) throws Exception {

		assertNotNull(comp);

		RmiExportHints hints = createRmiExportHints();

		final Object fcInterface = comp.getFcInterface(RMI_SERVICE_NAME);
		final Interface it = (Interface) fcInterface;
		final Component skeleton = this.rmiConnector.createSkel(
				RMI_SERVICE_NAME, it, comp, hints);

		Fractal.getLifeCycleController(skeleton).startFc();

		rmiRegistry = ContentControllerHelper.getSubComponentByName(skeleton,
				"org.objectweb.fractal.bf.connectors.rmi.RmiRegistry");

		Registry registryItf = (Registry) rmiRegistry
				.getFcInterface("rmiRegistry");

		String[] namesOfRegisteredServices = registryItf.list();
		assertEquals(1, namesOfRegisteredServices.length);

		remoteReference = registryItf.lookup(RMI_SERVICE_NAME);
		assertNotNull(remoteReference);

		Method print = remoteReference.getClass().getMethod("print",
				new Class[] {});

		assertNotNull(print);

		print.invoke(remoteReference, new Object[] {});

		Method printAndAswer = remoteReference.getClass().getMethod(
				"printAndAnswer", new Class[] {});
		String answer = (String) printAndAswer.invoke(remoteReference,
				new Object[] {});
		assertEquals(BasicServiceImpl.SERVICE_REPLY, answer);

		assertEquals(1, registryItf.list().length);

		// clean-up the registry
		// UnicastRemoteObject.unexportObject(remoteReference, true);

	}

	@Test
	public void testCreateRmiRegistryComponent() throws Exception {
		Component rmiReg = rmiConnector.createRmiRegistryComponent("localhost",
				"9999");
		assertNotNull(rmiReg);
		assertEquals(LifeCycleController.STOPPED, Fractal
				.getLifeCycleController(rmiReg).getFcState());
		RmiRegistryAttributes ac = (RmiRegistryAttributes) Fractal
				.getAttributeController(rmiReg);
		assertEquals("localhost", ac.getHost());
		assertEquals(Integer.valueOf("9999").intValue(), ac.getPort());

		assertNotNull(rmiReg.getFcInterface("rmiRegistry"));

	}

	private RmiExportHints createRmiExportHints() {
		RmiExportHints hints = new RmiExportHints();
		hints.setServiceName(RMI_SERVICE_NAME);
		hints.setHost("localhost");

		hints.setPort(RMI_PORT);
		return hints;
	}

}
