/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.rmi.asm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.InvocationTargetException;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;
import org.objectweb.fractal.bf.connectors.Service;
import org.objectweb.fractal.bf.connectors.rmi.AbstractRmiStubContent;
import org.objectweb.fractal.bf.proxies.ASMStubGenerator;

public class ASMRmiStubContentGeneratorTest {

	ASMStubGenerator cut;
	BindingFactoryClassLoader cl;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		cl = new BindingFactoryClassLoader(Thread.currentThread()
				.getContextClassLoader());
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		cl = null;
	}

	/**
	 * Test method for
	 * {@link org.objectweb.fractal.bf.connectors.rmi.asm.ASMRmiStubContentGenerator#generate(java.lang.Class, org.objectweb.fractal.bf.connectors.rmi.asm.RMIServiceClassLoader)}
	 * .
	 * 
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws IllegalArgumentException
	 * @throws ClassNotFoundException
	 */
	@Test
	public void testGenerate() throws SecurityException, NoSuchMethodException,
			IllegalArgumentException, InstantiationException,
			IllegalAccessException, InvocationTargetException,
			ClassNotFoundException {

		final Class<?> stubClazz = ASMStubGenerator.generate(Service.class,
				AbstractRmiStubContent.class, cl);

		assertNotNull(stubClazz);
		// test for business interface
		assertTrue(Service.class.isAssignableFrom(stubClazz));
		// test for Fractal controller
		assertTrue(BindingController.class.isAssignableFrom(stubClazz));
		assertTrue(LifeCycleController.class.isAssignableFrom(stubClazz));
		/*
		 * more tests on instance
		 */

		Object instance = ASMStubGenerator.generateNewInstance(Service.class,
				AbstractRmiStubContent.class, cl);

		AbstractRmiStubContent abstractRmiStub = (AbstractRmiStubContent) instance;

		Service mockService = EasyMock.createMock(Service.class);

		try {
			abstractRmiStub.bindFc(
					AbstractRmiStubContent.REMOTE_REF_FIELD_NAME, mockService);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getLocalizedMessage());
		}

		assertEquals(1, abstractRmiStub.listFc().length);

		Service s = (Service) instance;

		mockService.badMethod();
		EasyMock.replay(mockService);
		s.badMethod();
		EasyMock.verify(mockService);
	}
}
