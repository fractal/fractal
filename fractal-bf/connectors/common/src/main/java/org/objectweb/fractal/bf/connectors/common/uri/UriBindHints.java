/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 INRIA, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Philippe.Merle@inria.fr
 * 
 * Author: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.common.uri;

import org.objectweb.fractal.bf.BindHints;

/**
 * Bind hints for URI-based binding.
 *
 * @author Philippe Merle
 */
public class UriBindHints implements BindHints {
	/**
	 * The uri of the binding.
	 */
	private String uri;

	/**
	 * Get the uri of the bind hints.
	 * 
	 * @return the uri of the bind hints.
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * Set the uri of the bind hints.
	 * 
	 * @param uri the uri to use.
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("[");
		sb.append(UriConnectorConstants.URI + ":");
		if (uri != null) {
			sb.append(uri);
		} else {
			sb.append("NOT-SET");
		}
		sb.append("]");
		return sb.toString();
	}
}
