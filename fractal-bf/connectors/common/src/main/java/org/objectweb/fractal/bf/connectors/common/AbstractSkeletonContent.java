/**
 * Fractal Binding Factory.
 * Copyright (C) 2009-2012 Inria, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fracatl@ow2.org
 * 
 * Author: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.common;

import java.util.logging.Logger;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.bf.AbstractSkeleton;

/**
 * The content implementation of common skeleton components.
 *
 * @author Philippe Merle
 */
public abstract class AbstractSkeletonContent extends AbstractSkeleton
implements SkeletonContentAttributes, LifeCycleController {

	protected Logger log = Logger.getLogger(this.getClass().getCanonicalName());

	/**
	 * the fcState of this component.
	 */
	private String fcState;

	/**
	 * The service class used to create the skeleton.
	 */
	private Class<?> serviceClass;

	/**
	 * List of binding intent handlers.
	 */
	private java.util.List<BindingIntentHandler> intentHandlers;

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.SkeletonContentAttributes#getServiceClass()
	 */
	public Class<?> getServiceClass() {
		return this.serviceClass;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.SkeletonContentAttributes#setServiceClass(java.lang.Class)
	 */
	public void setServiceClass(Class<?> serviceClass) {
		this.serviceClass = serviceClass;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.StubContentAttributes#getIntentHandlers()
	 */
	public java.util.List<BindingIntentHandler> getIntentHandlers()
	{
	  if(intentHandlers == null) {
	    intentHandlers = new java.util.ArrayList<BindingIntentHandler>();
	  }
	  return intentHandlers;
	}

	/**
	 * Apply all registered binding intent handlers on the given proxy.
	 *
	 * @param proxy a given proxy.
	 */
    protected void applyIntentHandlers(Object proxy)
    {
  	  if(intentHandlers != null) {
        for(BindingIntentHandler bih : this.intentHandlers) {
          bih.apply(proxy);
        }
  	  }
    }

    /**
	 * @see org.objectweb.fractal.api.control.LifeCycleController#getFcState()
	 */
	public String getFcState() {
		return this.fcState;
	}

	/**
	 * @see org.objectweb.fractal.api.control.LifeCycleController#startFc()
	 */
	public void startFc() {
		this.fcState = LifeCycleController.STARTED;
		log.fine("Skeleton started");
	}

	/**
	 * @see org.objectweb.fractal.api.control.LifeCycleController#stopFc()
	 */
	public void stopFc() {
		this.fcState = LifeCycleController.STOPPED;
		log.fine("Skeleton stopped");
	}
}
