/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 INRIA, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Philippe.Merle@inria.fr
 * 
 * Author: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.common.uri;

import org.objectweb.fractal.bf.connectors.common.AbstractSkeletonContent;

/**
 * The content implementation of URI-based skeleton components.
 *
 * @author Philippe Merle
 */
public abstract class UriSkeletonContent extends AbstractSkeletonContent
	implements UriSkeletonContentAttributes {

	/**
	 * The URI of the skeleton.
	 */
	private String uri;

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.uri.UriSkeletonContentAttributes#getUri()
	 */
	public String getUri() {
		return this.uri;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.uri.UriSkeletonContentAttributes#setUri(java.lang.String)
	 */
	public void setUri(String uri) {
		this.uri = uri;
		log.info("Skeleton configured to use URI: " + uri);
	}
}
