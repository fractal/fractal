/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 INRIA, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Philippe.Merle@inria.fr
 * 
 * Author: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.common.uri;

import org.objectweb.fractal.bf.connectors.common.AbstractStubContent;

/**
 * A super class for URI-based stubs. Concrete classes are generated on the fly.
 *
 * @author Philippe Merle
 */
public abstract class UriStubContent extends AbstractStubContent
	implements UriStubContentAttributes {

	/**
	 * The uri used to bind the remoteRef.
	 */
	private String uri;

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.uri.UriStubContentAttributes#getUri()
	 */
	public String getUri() {
		return this.uri;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.uri.UriStubContentAttributes#setUri(java.lang.String)
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}
}
