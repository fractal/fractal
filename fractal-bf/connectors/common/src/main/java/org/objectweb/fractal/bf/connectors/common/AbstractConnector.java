/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 INRIA, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Philippe.Merle@inria.fr
 * 
 * Author: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.common;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.bf.BindHints;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.BindingFactoryPlugin;
import org.objectweb.fractal.bf.ExportHints;
import org.objectweb.fractal.bf.SkeletonGenerationException;
import org.objectweb.fractal.bf.StubGenerationException;
import org.objectweb.fractal.bf.proxies.ASMStubGenerator;
import org.objectweb.fractal.util.Fractal;

/**
 * An abstract connector can export and bind Fractal interfaces.
 * 
 * @author Philippe Merle
 */
public abstract class AbstractConnector<EH extends ExportHints,
                                        BH extends BindHints>
	implements BindingFactoryPlugin<EH, BH> {

	protected Logger log = Logger.getLogger(this.getClass().getCanonicalName());

	/**
	 * The Fractal ADL {@link Factory} shared by all connectors.
	 *
	 * This field is static and shared between all connectors in order to improve performance
	 * and memory footprint, i.e., create only one Fractal ADL Factory instead of one by connector.
	 */
	private static Factory adlFactory;

	private BindingFactoryClassLoader cl;

	/**
	 * Default 0-args constructor.
	 */
	public AbstractConnector() {
	    if(adlFactory == null) {
	    	try {
	    		adlFactory = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
	    	} catch (ADLException e) {
	    		throw new ConnectorException(
	    				"Could not initialize Fractal bootstrap components.", e);
	    	}
		}
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getBindingFactoryClassLoader()
	 */
	public BindingFactoryClassLoader getBindingFactoryClassLoader() {
		return this.cl;
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#setBindingFactoryClassLoader(java.lang.ClassLoader)
	 */
	public void setBindingFactoryClassLoader(BindingFactoryClassLoader cl) {
		this.cl = cl;
	}

	/**
	 * Return the Fractal ADL definition name for instantiating skeleton components.
	 *
	 * @return the Fractal ADL definition name for instantiating skeleton components.
	 */
	protected abstract String getSkeletonAdl();

	/**
	 * Initialize the Fractal ADL context for instantiating skeleton components.
	 *
	 * @param exportHints export hints.
	 * @param context the Fractal ADL context.
	 */
	protected abstract void initSkeletonAdlContext(EH exportHints, Map<String, Object> context);

	/**
	 * Return the Fractal ADL definition name for instantiating stub components.
	 *
	 * @return the Fractal ADL definition name for instantiating stub components.
	 */
	protected abstract String getStubAdl();

	/**
	 * Initialize the Fractal ADL context for instantiating stub components.
	 *
	 * @param bindHints bind hints.
	 * @param context the Fractal ADL context.
	 */
	protected abstract void initStubAdlContext(BH bindHints, Map<String, Object> context);

	protected static final String SERVANT_INTERFACE_SIGNATURE = "SERVANT_INTERFACE_SIGNATURE";
	protected static final String STUB_INTERFACE_NAME = "STUB_INTERFACE_NAME";
	protected static final String STUB_INTERFACE_SIGNATURE = "STUB_INTERFACE_SIGNATURE";
	protected static final String STUB_CONTENT_CLASS = "STUB_CONTENT_CLASS";

	/**
	 * Generate a stub content class inheriting from a given abstract class.
	 * The name of the generated class is stored into the given Fractal ADL context.
	 *
	 * @param abstractStubClass the abstract stub class. 
	 * @param context the Fractal ADL context.
	 */
	protected void generateStubContentClass(Class<?> abstractStubClass, Map<String, Object> context) {
		try {
			final Class<?> stubClass = ASMStubGenerator.generate(
				this.cl.loadClass((String)context.get(STUB_INTERFACE_SIGNATURE)),
				abstractStubClass,
				this.cl);
			context.put(STUB_CONTENT_CLASS, stubClass.getCanonicalName());
		} catch(ClassNotFoundException cnfe) {
			throw new ConnectorException(cnfe);
		}
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#createSkel(java.lang.String,
	 *      java.lang.Object, org.objectweb.fractal.api.Component,
	 *      org.objectweb.fractal.bf.ExportHints)
	 */
	public Component createSkel(String interfaceName, Object serverItf,
			Component owner, EH exportHints)
			throws SkeletonGenerationException {
		log.info(getPluginIdentifier() + "Connector - Creating skeleton...");

		if (this.cl == null) {
			throw new IllegalStateException(
					"The BindingFactoryClassLoader was not set");
		}
		
		try {
			String skeletonAdl = getSkeletonAdl();
			Map<String, Object> context = new HashMap<String, Object>();
			initSkeletonAdlContext(exportHints, context);
			String servantInterfaceSignature =
				((InterfaceType) ((Interface) serverItf).getFcItfType()).getFcItfSignature();
			context.put(SERVANT_INTERFACE_SIGNATURE, servantInterfaceSignature);
			context.put("classloader", getBindingFactoryClassLoader());
			log.info(getPluginIdentifier() + "Connector - Instantiating " + skeletonAdl + '(' + context + ")...");
			Component skeleton = (Component)adlFactory.newComponent(skeletonAdl, context);

			SkeletonContentAttributes ska = (SkeletonContentAttributes) Fractal
					.getAttributeController(skeleton);
			Class<?> serviceClass = this.cl.loadClass(servantInterfaceSignature);
			ska.setServiceClass(serviceClass);

			log.info(getPluginIdentifier() + "Connector - Skeleton created");
			return skeleton;

		} catch (Exception e) {
			throw new SkeletonGenerationException(
					"Some error occurred while exporting using the " + getPluginIdentifier() + " plugin ",
					e);
		}
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#createStub(org.objectweb.fractal.api.Component,
	 *      java.lang.String, org.objectweb.fractal.bf.BindHints)
	 */
	public Component createStub(Component client, String itfname,
			BH bindHints) throws StubGenerationException
	{
		log.info(getPluginIdentifier() + "Connector - Creating stub...");

		if (bindHints == null) {
			throw new StubGenerationException(
					"BindHints to create stub are null");
		}

		if (getBindingFactoryClassLoader() == null) {
			throw new IllegalStateException(
					"The BindingFactoryClassLoader was not set");
		}

		try {
			String stubAdl = getStubAdl();
			Map<String, Object> context = new HashMap<String, Object>();
		    String stubInterfaceSignature = ((ComponentType) (client).getFcType())
		    		.getFcInterfaceType(itfname).getFcItfSignature();
			context.put(STUB_INTERFACE_SIGNATURE, stubInterfaceSignature);
			initStubAdlContext(bindHints, context);
			context.put(STUB_INTERFACE_NAME, itfname);
			context.put("classloader", getBindingFactoryClassLoader());
			log.info(getPluginIdentifier() + "Connector - Instantiating " + stubAdl + '(' + context + ")...");
			Component stub = (Component)adlFactory.newComponent(stubAdl, context);

			StubContentAttributes ssa = (StubContentAttributes) Fractal
					.getAttributeController(stub);
			ssa.setServiceClass(this.cl.loadClass(stubInterfaceSignature));

			log.info(getPluginIdentifier() + "Connector - Stub created");
			return stub;

		} catch (Exception e) {
			throw new StubGenerationException(
					"Some error occurred while creating the " + getPluginIdentifier() + " stub component",
					e);
		}
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#finalizeSkeleton(org.objectweb.fractal.api.Component,
	 *      java.util.Map)
	 */
	public void finalizeSkeleton(Component skel,
			Map<String, String> finalizationHints)
			throws BindingFactoryException {
		// Nothing to do.
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#finalizeStub(org.objectweb.fractal.api.Component,
	 *      java.util.Map)
	 */
	public void finalizeStub(Component stub, Map<String, Object> hints)
			throws BindingFactoryException {
		// Nothing to do.
	}
}
