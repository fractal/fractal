/**
 * Fractal Binding Factory.
 * Copyright (C) 2009-2012 Inria, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.common;

import java.util.logging.Logger;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;

/**
 * A super class for stubs. Concrete classes are generated on the fly.
 *
 * @author Philippe Merle
 */
public abstract class AbstractStubContent implements StubContentAttributes, LifeCycleController {

	protected Logger log = Logger.getLogger(this.getClass().getCanonicalName());

	/**
	 * The lifecycle state.
	 */
	private String fcState;

	/**
	 * A ref to a stub object. This field is initialized when this component is
	 * started.
	 */
	protected Object remoteRef;

	private Class<?> serviceClass;

	/**
	 * List of binding intent handlers.
	 */
	private java.util.List<BindingIntentHandler> intentHandlers;

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.StubContentAttributes#getServiceClass()
	 */
	public Class<?> getServiceClass() {
		return this.serviceClass;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.StubContentAttributes#setServiceClass(java.lang.Class)
	 */
	public void setServiceClass(Class<?> clazz) {
		this.serviceClass = clazz;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.StubContentAttributes#getIntentHandlers()
	 */
	public java.util.List<BindingIntentHandler> getIntentHandlers()
	{
	  if(intentHandlers == null) {
	    intentHandlers = new java.util.ArrayList<BindingIntentHandler>();
	  }
	  return intentHandlers;
	}

	/**
	 * @see org.objectweb.fractal.api.control.LifeCycleController#getFcState()
	 */
	public String getFcState() {
		return this.fcState;
	}

	/**
	 * @see org.objectweb.fractal.api.control.LifeCycleController#startFc()
	 */
	public void startFc() throws IllegalLifeCycleException {
		try {
			this.remoteRef = resolveReference();

			if(intentHandlers != null) {
				for(BindingIntentHandler ih : intentHandlers) {
					ih.apply(this.remoteRef);
				}
			}
			
		} catch(Exception e) {
			throw new ConnectorException(
					"Could not initialize stub reference", e);
		}
		this.fcState = LifeCycleController.STARTED;
		log.fine("Stub started");
	}

	/**
	 * @see org.objectweb.fractal.api.control.LifeCycleController#stopFc()
	 */
	public void stopFc() {
		this.remoteRef = null;
		this.fcState = LifeCycleController.STOPPED;
		log.fine("Stub stopped");
	}

	/**
	 * Resolve the concrete stub.
	 * 
	 * @return the object instantiated.
	 */
	protected abstract Object resolveReference() throws Exception;
}
