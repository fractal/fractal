/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 INRIA, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Philippe.Merle@inria.fr
 * 
 * Author: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.common;

/**
 * A general purpose exception to be thrown in case of problems during the usage
 * of the {@link AbstractConnector}.
 *
 * @author Philippe Merle
 */
public class ConnectorException extends RuntimeException {

	// TODO: Update the next magic number.
	private static final long serialVersionUID = 1699011988358598477L;

	/**
	 * Use the other constructors to provide better feedbacks.
	 */
	public ConnectorException() {
	}

	/**
	 * @param message
	 */
	public ConnectorException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ConnectorException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ConnectorException(String message, Throwable cause) {
		super(message, cause);
	}
}
