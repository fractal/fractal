/**
 * Fractal Binding Factory.
 * Copyright (C) 2009-2012 Inria, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.common;

import org.objectweb.fractal.api.control.AttributeController;

/**
 * Attribute controller to configure the {@link AbstractSkeletonContent}.
 * 
 * @see AbstractSkeletonContent
 *
 * @author Philippe Merle
 */
public interface SkeletonContentAttributes extends AttributeController, BindingIntentController {
	/**
	 * Get the class to generate the proxy.
	 * 
	 * @return the class used to generate the proxy.
	 */
	public Class<?> getServiceClass();

	/**
	 * Set the class used to generate the proxy.
	 * 
	 * @param serviceClass the class to generate the proxy.
	 */
	public void setServiceClass(Class<?> serviceClass);
}
