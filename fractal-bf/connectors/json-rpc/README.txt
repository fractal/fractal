This module provides a plugin for BindingFactory to support JSON-RPC bindings.

This plugin exploits jabsorb http://jabsorb.org/ successor of JSON-RPC-Java 
http://oss.metaparadigm.com/jsonrpc/ to expose Fractal server interfaces as 
JSON-RPC services, and bind Fractal client interfaces.

As jabsorb is not currently available in a Maven repository, type the following 
to install jabsorb in your local Maven repository:

mvn install:install-file -DgroupId=org.jabsorb -DartifactId=jabsorb -Dversion=1.3.1 -Dpackaging=jar -Dfile=lib/jabsorb-1.3.1.jar
