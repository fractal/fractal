/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 INRIA, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Philippe.Merle@inria.fr
 * 
 * Author: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.jsonrpc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.SkeletonGenerationException;
import org.objectweb.fractal.bf.StubGenerationException;
import org.objectweb.fractal.util.Fractal;

/**
 * @author Philippe Merle
 */
public class JsonRpcConnectorTest {
	Component service;
	Component client;

	protected JsonRpcConnector connector;
	private static Factory adlFactory;

	@BeforeClass
	public static void initializeSystemProperties() throws ADLException {
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
		adlFactory = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
	}

	@Before
	public void setUp() throws Exception {

		service = (Component) adlFactory.newComponent(
				"org.objectweb.fractal.bf.connectors.Service", null);
		assertNotNull(service);
		client = (Component) adlFactory.newComponent(
				"org.objectweb.fractal.bf.connectors.Client", null);
		assertNotNull(client);

		BindingFactoryClassLoader cl = new BindingFactoryClassLoader(Thread
				.currentThread().getContextClassLoader());

		connector = new JsonRpcConnector();
		connector.setBindingFactoryClassLoader(cl);
		assertNotNull(connector);
	}

	@Test
	public void testCreateSkeletonAndCheckState() throws Exception {

		Component skel = connector.createSkel("service", service
				.getFcInterface("service"), service, createExportHints());
		assertNotNull("Skeleton component should not be null", skel);
		assertEquals(LifeCycleController.STOPPED, Fractal
				.getLifeCycleController(skel).getFcState());

	}

	/**
	 * Utility method
	 * 
	 * @return
	 */
	private JsonRpcExportHints createExportHints() {
		JsonRpcExportHints hints = new JsonRpcExportHints();
		hints.setUri("http://localhost:8080/JSON-RPC/service");
		return hints;
	}

}
