/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 INRIA, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Philippe.Merle@inria.fr
 * 
 * Author: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.jsonrpc;

import java.util.Map;
import org.objectweb.fractal.bf.connectors.common.AbstractConnector;

/**
 * A JSON-RPC Connector can export and bind Fractal interfaces via the JSON-RPC protocol.
 *
 * @author Philippe Merle
 */
public class JsonRpcConnector
	extends AbstractConnector<JsonRpcExportHints, JsonRpcBindHints> {

	/**
	 * The global JSON-RPC manager used by JSON-RPC stub and skeleton components.
	 */
	public static JSONRPCManager TheGlobalManager = new JSONRPCManagerImpl();

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getPluginIdentifier()
	 */
	public String getPluginIdentifier() {
		return "JSON-RPC";
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getExportHints(Map)
	 */
	public JsonRpcExportHints getExportHints(Map<String, Object> initialHints) {
		JsonRpcExportHints exportHints = new JsonRpcExportHints();
		exportHints.setUri((String) initialHints
				.get(JsonRpcConnectorConstants.URI));
		log.fine("JsonRpcExportHints: " + exportHints);
		return exportHints;
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getBindHints(Map)
	 */
	public JsonRpcBindHints getBindHints(Map<String, Object> initialHints) {
		JsonRpcBindHints bindHints = new JsonRpcBindHints();
		bindHints.setUri((String) initialHints
				.get(JsonRpcConnectorConstants.URI));
		log.fine("JsonRpcBindHints: " + bindHints);
		return bindHints;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#getSkeletonAdl()
	 */
	protected String getSkeletonAdl() {
        return "org.objectweb.fractal.bf.connectors.jsonrpc.JsonRpcSkeleton";
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#initSkeletonAdlContext(EH, Map)
	 */
	protected void initSkeletonAdlContext(JsonRpcExportHints exportHints, Map<String, Object> context) {
		String uri = exportHints.getUri();
		if (uri == null) {
			uri = JsonRpcConnectorConstants.DEFAULT_URI;
			log.warning("Export hints didn't specify an uri, going to use "
					+ uri);
		}
		context.put(JsonRpcConnectorConstants.URI, uri);
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#getStubAdl()
	 */
	protected String getStubAdl() {
        return "org.objectweb.fractal.bf.connectors.jsonrpc.JsonRpcStub";
    }

	/**
	 * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#initStubAdlContext(BH, Map)
	 */
	protected void initStubAdlContext(JsonRpcBindHints bindHints, Map<String, Object> context) {
		generateStubContentClass(JsonRpcStubContent.class, context);
		String uri = bindHints.getUri();
		if (uri == null) {
			uri = JsonRpcConnectorConstants.DEFAULT_URI;
			log.warning("Bind hints didn't specify an uri, going to use "
					+ uri);
		}
		context.put(JsonRpcConnectorConstants.URI, uri);
	}
}
