/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 INRIA, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Philippe.Merle@inria.fr
 * 
 * Author: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.jsonrpc;

/**
 * Interface for managing JSON-RPC.
 *
 * @author Philippe Merle
 */
public interface JSONRPCManager {

	/**
	 * Register/export a given object at a given uri.
	 *
	 * @param uri the given uri.
	 * @param object the given object to register/export.
	 */
	void registerObject(String uri, Object object);
	
	/**
	 * Unregister/unexport the object exported at a given uri.
	 *
	 * @param uri the given uri.
	 */
	void unregisterObject(String uri);

	/**
	 * Obtain a proxy from a given uri and interface.
	 *
	 * @param uri the uri of the exported object.
	 * @param interfaze the interface of the proxy.
	 *
	 * @return the proxy.
	 */
	Object openProxy(String uri, Class<?> interfaze);

	/**
	 * Close/release the proxy at a given uri.
	 *
	 * @param uri the uri of the proxy.
	 */
	void closeProxy(String uri);
}
