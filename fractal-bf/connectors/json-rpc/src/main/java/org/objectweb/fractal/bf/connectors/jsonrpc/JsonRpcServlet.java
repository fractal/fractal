/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 INRIA, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Philippe.Merle@inria.fr
 * 
 * Author: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.jsonrpc;

import javax.servlet.http.HttpServletRequest;
import org.jabsorb.JSONRPCBridge;
import org.jabsorb.JSONRPCServlet;

/**
 * Specialized {@link JSONRPCServlet} class.
 *
 * @author Philippe Merle
 */
public class JsonRpcServlet extends JSONRPCServlet {

	/**
	 * The bridge associated to this servlet.
	 */
    private JSONRPCBridge bridge;

    /**
     * Default constructor.
     */
    public JsonRpcServlet() {
	    bridge = new JSONRPCBridge();
	    try {
// TODO: The mechanism of CallableReference does not work currently.
// This must be improved.
            bridge.registerCallableReference(org.objectweb.fractal.api.Component.class);
            bridge.registerCallableReference(org.objectweb.fractal.api.Interface.class);
            bridge.registerCallableReference(org.objectweb.fractal.api.Type.class);
//            bridge.registerCallableReference(org.objectweb.fractal.api.type.InterfaceType.class);
//            bridge.registerCallableReference(org.objectweb.fractal.api.type.ComponentType.class);
	    } catch(Exception e) {
            throw new RuntimeException(e);
	    }
    }

    /**
     * @see JSONRPCServlet#findBridge(HttpServletRequest)
     */
    protected JSONRPCBridge findBridge(HttpServletRequest request) {
        request.getSession(true).setAttribute("JSONRPCBridge", bridge);
        return bridge;
    }

    /**
     * Return the bridge associated to this servlet.
     *
     * @return the bridge associated to this servlet.
     */
    public JSONRPCBridge getBridge() {
        return bridge;
    }
}
