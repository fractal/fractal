/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 INRIA, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Philippe.Merle@inria.fr
 * 
 * Author: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.jsonrpc;

import org.objectweb.fractal.bf.connectors.common.uri.UriSkeletonContent;

/**
 * The content implementation of JSON-RPC skeleton components.
 *
 * @author Philippe Merle
 */
public class JsonRpcSkeletonContent extends UriSkeletonContent
	implements JsonRpcSkeletonContentAttributes {

	/**
	 * @see org.objectweb.fractal.api.control.LifeCycleController#startFc()
	 */
	@Override
	public void startFc() {
		log.fine("Skeleton will be started...");
		log.info("Register the servant at " + getUri());
		JsonRpcConnector.TheGlobalManager.registerObject(getUri(), getServant());
		super.startFc();
	}

	/**
	 * @see org.objectweb.fractal.api.control.LifeCycleController#stopFc()
	 */
	@Override
	public void stopFc() {
		log.fine("Skeleton will be stopped...");
		log.info("Unregister the servant at " + getUri());
		JsonRpcConnector.TheGlobalManager.unregisterObject(getUri());
        super.stopFc();
	}
}
