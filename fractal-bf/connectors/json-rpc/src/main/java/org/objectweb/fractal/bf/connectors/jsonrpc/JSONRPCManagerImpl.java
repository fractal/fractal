/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 INRIA, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Philippe.Merle@inria.fr
 * 
 * Author: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf.connectors.jsonrpc;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import org.jabsorb.JSONRPCBridge;
import org.jabsorb.client.Client;
import org.jabsorb.client.Session;
import org.jabsorb.client.TransportRegistry;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.servlet.Context;
import org.mortbay.jetty.servlet.ServletHolder;

/**
 * Implementation of the {@link JSONRPCManager} interface.
 *
 * TODO: Release created JSON-RPC instances when no more used or too many opened,
 * e.g., Jetty Servers, JsonRpcServlets, and Client instances.
 *
 * @author Philippe Merle
 */
public class JSONRPCManagerImpl implements JSONRPCManager {

	/**
	 * Logger
	 */
	private Logger log = Logger.getLogger(this.getClass().getCanonicalName());

	/**
	 * Cache of already created Jetty servers and JSON-RPC servlets.
	 */
	private Map<Integer, Server> servers = new HashMap<Integer, Server>();
	private Map<Integer, Context> contexts = new HashMap<Integer, Context>();
	private Map<String, JsonRpcServlet> servlets = new HashMap<String, JsonRpcServlet>();

   /**
    * Cache of already created JSON-RPC Client instances.
    */
	private Map<String, Client> clients = new HashMap<String, Client>();

	/**
	 * Split a uri into a path and name.
	 *
	 * @param uri the given uri.
	 * @return an array containing the path and the name.
	 */
	private String[] splitUri(String uri) {
		String[] result = new String[2];
		int index = uri.lastIndexOf('/');
		result[0] = uri.substring(0, index);
		result[1] = uri.substring(index+1);
		return result;
	}

	/**
	 * @see JSONRPCBridgeManager#registerObject(String,Object)
	 */
	public synchronized void registerObject(String key, Object object) {
		// Create a URI instance from the 'key' string.
		URI uri;
		try {
			uri = new URI(key);
		} catch (URISyntaxException use) {
			throw new RuntimeException(use);
		}
		String[] pathAndName = splitUri(uri.getPath());
		String path = pathAndName[0];

		// Find the Jetty Context instance associated to the uri's port.
		int port = uri.getPort();
		Context context = contexts.get(port);
		if(context == null) {
			log.info("Creating a Jetty server on port " + port + "...");
			Server server = new Server(port);
			servers.put(port, server);
			context = new Context(server, "/", Context.SESSIONS);
			contexts.put(port, context);
			try {
				log.info("Starting the Jetty server on port " + port + "...");
				server.start();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		// Find the Servlet managing the given uri.
		JsonRpcServlet servlet = servlets.get(port + path);
		if(servlet == null) {
			log.info("Adding a JSON-RPC Servlet with path '" + path +
					"' on the Jetty server on port " + port + " ...");
			servlet = new JsonRpcServlet();
			servlets.put(port + path, servlet);
			context.addServlet(new ServletHolder(servlet), path);
		}
		// Register the object to the JSON-RPC bridge of the servlet.
		servlet.getBridge().registerObject(pathAndName[1], object);
	}
	
	/**
	 * @see JSONRPCBridgeManager#unregisterObject(String)
	 */
	public void unregisterObject(String key) {
		// Create a URI instance from the 'key' string.
		URI uri;
		try {
			uri = new URI(key);
		} catch (URISyntaxException use) {
			throw new RuntimeException(use);
		}
		String[] pathAndName = splitUri(uri.getPath());

		// Find the JSON-RPC servlet.
		JsonRpcServlet servlet = servlets.get(uri.getPort() + pathAndName[0]);
		if(servlet == null) {
			throw new RuntimeException("No Servlet instance for '" + pathAndName[0] + "'");			
		}
		// Unregister the object from the JSON-RPC bridge of the servlet.
		servlet.getBridge().unregisterObject(pathAndName[1]);
	}

	/**
	 * @see JSONRPCBridgeManager#openProxy(String,Class)
	 */
	public synchronized Object openProxy(String uri, Class<?> interfaze) {
		String[] pathAndName = splitUri(uri);
		String path = pathAndName[0];

		// Obtain the JSON-RPC Client for the given uri's path.
		Client client = clients.get(path);
		if(client == null) {
			log.info("Create a JSON-RPC Client for " + path);
			Session session = TransportRegistry.i().createSession(path);
			client = new Client(session);
			clients.put(path, client);
		}
		// Open a proxy for the given name.
        return client.openProxy(pathAndName[1], interfaze);		
	}

	/**
	 * @see JSONRPCBridgeManager#closeProxy(String)
	 */
	public void closeProxy(String uri) {
		String[] pathAndName = splitUri(uri);

		// Obtain the JSON-RPC instance for the given uri's path.
		Client client = clients.get(pathAndName[0]);
		if(client == null) {
			throw new RuntimeException("No Client instance for '" + pathAndName[0] + "'");
		}
		// Close the proxy for the given name.
        client.closeProxy(pathAndName[1]);
	}
}
