/**
 * Fractal Binding Factory.
 * Copyright (C) 2013 INRIA, University of Lille 1

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: gwenael.cattez@inria.fr
 * 
 * Author: Gwenael Cattez
 * 
 */

package org.objectweb.fractal.bf.connectors.upnp;

import java.util.Map;
import java.util.Map.Entry;

import org.objectweb.fractal.bf.connectors.common.AbstractConnector;

public class UPnPConnector extends AbstractConnector<UPnPExportHints, UPnPBindHints>
{

    public String getPluginIdentifier()
    {
        return "upnp";
    }

    private void getUPnPHints(UPnPHints upnpHints, Map<String, Object> initialHints)
    {
        upnpHints.setServiceId((String) initialHints.get(UPnPConnectorConstants.SERVICE_ID));
        upnpHints.setServiceType((String) initialHints.get(UPnPConnectorConstants.SERVICE_TYPE));
        upnpHints.setDeviceId((String) initialHints.get(UPnPConnectorConstants.DEVICE_ID));
        upnpHints.setDeviceType((String) initialHints.get(UPnPConnectorConstants.DEVICE_TYPE));
    }
    
    public UPnPBindHints getBindHints(Map<String, Object> initialHints)
    {
        UPnPBindHints upnpHints = new UPnPBindHints();
        this.getUPnPHints(upnpHints, initialHints);
        log.fine("UPnPExportHints: " + upnpHints);
        return upnpHints;
    }

    public UPnPExportHints getExportHints(Map<String, Object> initialHints)
    {
        UPnPExportHints upnpHints = new UPnPExportHints();
        this.getUPnPHints(upnpHints, initialHints);
        upnpHints.setDeviceVersion((String) initialHints.get(UPnPConnectorConstants.DEVICE_VERSION));
        upnpHints.setDeviceIcon((String) initialHints.get(UPnPConnectorConstants.DEVICE_ICON));
        log.fine("UPnPExportHints: " + upnpHints);
        return upnpHints;
    }
    
    private void initUPnPADLContext(UPnPHints upnpHints, Map<String, Object> context)
    {
        context.put(UPnPConnectorConstants.SERVICE_ID, upnpHints.getServiceId());
        context.put(UPnPConnectorConstants.SERVICE_TYPE, upnpHints.getServiceType());
        context.put(UPnPConnectorConstants.DEVICE_ID, upnpHints.getDeviceId());
        context.put(UPnPConnectorConstants.DEVICE_TYPE, upnpHints.getDeviceType());
    }
    
    @Override
    protected String getSkeletonAdl()
    {
        return "org.objectweb.fractal.bf.connectors.upnp.UPnPSkeleton";
    }
    
    @Override
    protected void initSkeletonAdlContext(UPnPExportHints exportHints, Map<String, Object> context)
    {
       this.initUPnPADLContext(exportHints, context);
        context.put(UPnPConnectorConstants.DEVICE_VERSION, exportHints.getDeviceVersion());
        context.put(UPnPConnectorConstants.DEVICE_ICON, exportHints.getDeviceIcon());
    }
    
    @Override
    protected String getStubAdl()
    {
        return "org.objectweb.fractal.bf.connectors.upnp.UPnPStub";
    }

    @Override
    protected void initStubAdlContext(UPnPBindHints bindHints, Map<String, Object> context)
    {
        generateStubContentClass(UPnPStubContent.class, context);
        this.initUPnPADLContext(bindHints, context);
    }

}
