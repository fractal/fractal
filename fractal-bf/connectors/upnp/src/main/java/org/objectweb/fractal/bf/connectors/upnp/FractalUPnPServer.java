/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.objectweb.fractal.bf.connectors.upnp;

import org.ow2.fractal.upnp.server.UPnPServer;
import org.teleal.cling.model.meta.LocalDevice;

/**
 *
 */
public class FractalUPnPServer extends UPnPServer
{
    private static FractalUPnPServer instance;
    
    private FractalUPnPServer()
    {
        super();
    }
    
    public static FractalUPnPServer getInstance()
    {
        if(instance==null)
        {
            instance=new FractalUPnPServer();
        }
        return instance;
    }
    
    @Override
    public boolean addUPnPDevice(LocalDevice upnpDevice)
    {
        boolean isDeviceAdded = super.addUPnPDevice(upnpDevice);
        if(!isDeviceAdded)
        {
            return false;
        }

        /**If the added UPnP device is the first we start the server*/
        if(this.upnpDevices.size()==1)
        {
            this.start();
        }
        return true;
    }
    
    @Override
    public boolean removeUPnPDevice(String upnpDeviceId)
    {
        boolean isDeviceRemoved = super.removeUPnPDevice(upnpDeviceId);
        if(!isDeviceRemoved)
        {
            return false;
        }

        /**If the removed UPnP device was the last stop the server*/
        if(this.upnpDevices.size()==0)
        {
            this.stop();
        }
        return true;
    }
}
