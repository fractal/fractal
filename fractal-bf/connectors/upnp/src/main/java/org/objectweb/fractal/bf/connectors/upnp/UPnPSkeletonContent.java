/**
 * Fractal Binding Factory.
 * Copyright (C) 2013 INRIA, University of Lille 1

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: gwenael.cattez@inria.fr
 * 
 * Author: Gwenael Cattez
 * 
 */
package org.objectweb.fractal.bf.connectors.upnp;

import org.objectweb.fractal.bf.connectors.common.AbstractSkeletonContent;
import org.ow2.fractal.upnp.device.UPnPDeviceFactory;
import org.ow2.fractal.upnp.device.UPnPDeviceService;
import org.ow2.fractal.upnp.exception.UPnPException;
import org.teleal.cling.model.meta.LocalDevice;
import org.teleal.cling.model.types.UDN;

/**
 *
 */
public class UPnPSkeletonContent extends AbstractSkeletonContent implements UPnPSkeletonContentAttributes
{
    private FractalUPnPServer upnpServer = FractalUPnPServer.getInstance();
    
    private String serviceId;

    private String serviceType;

    private String deviceId;

    private String deviceType;

    private String deviceVersion;

    private String deviceIcon;

    private UDN deviceUDN;
    
    /**
     * @throws UPnPException 
     * @see org.objectweb.fractal.bf.connectors.common.AbstractSkeletonContent#startFc()
     */
    @Override
    public void startFc()
    {
        log.fine("Skeleton will be started...");
        try
        {
            UPnPDeviceFactory upnpDeviceFactory = new UPnPDeviceFactory();
            upnpDeviceFactory.setUpnpDeviceId(this.getDeviceId());
            
            /**Set UPnP device type if defined, otherwise use the default value*/
            String upnpDeviceType;
            if(this.getDeviceType().equals("") || this.getDeviceType().equals(""))
            {
                upnpDeviceType = UPnPSkeletonContentAttributes.DEFAULT_DEVICE_TYPE;
            }
            else
            {
                upnpDeviceType = this.getDeviceType();
            }
            upnpDeviceFactory.setUpnpDeviceType(upnpDeviceType);
            
            /**Convert version String to int*/
            int upnpDeviceVersionInt;
            try
            {
                upnpDeviceVersionInt =  Integer.valueOf(this.getDeviceVersion());
            }
            catch (NumberFormatException numberFormatException)
            {
                upnpDeviceVersionInt = 0;
            }
            /**Set device version if defined and not equals to 0 , otherwise use the default value*/
            if(upnpDeviceVersionInt == 0)
            {
                upnpDeviceFactory.setUpnpDeviceVersion(UPnPSkeletonContentAttributes.DEFAULT_DEVICE_VERSION);
            }
            else
            {
                upnpDeviceFactory.setUpnpDeviceVersion(upnpDeviceVersionInt);
            }
            
            upnpDeviceFactory.setUpnpDeviceFriendlyName(upnpDeviceType);
            /**Always default values for those attributes*/
            upnpDeviceFactory.setUpnpDeviceManufacturerDetails(UPnPSkeletonContentAttributes.DEFAULT_MANUFACTURER_DETAILS);
            upnpDeviceFactory.setUpnpDeviceModelDetails(UPnPSkeletonContentAttributes.DEFAULT_MODEL_DETAILS);
            
            UPnPDeviceService upnpDeviceService = new UPnPDeviceService();
            upnpDeviceService.setUpnpServiceId(this.getServiceId());
            upnpDeviceService.setUpnpServiceType(this.getServiceType());
            upnpDeviceService.setUpnpServiceClass(this.getServiceClass());
            upnpDeviceService.setUpnpServiceProvider(this.getServant());
            upnpDeviceFactory.addService(upnpDeviceService);
            LocalDevice localDevice = upnpDeviceFactory.generateUPnPDevice();
            this.deviceUDN = localDevice.getIdentity().getUdn();
            upnpServer.addUPnPDevice(localDevice);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
 
        this.applyIntentHandlers(upnpServer);
        super.startFc();
    }

    /**
     * @see org.objectweb.fractal.api.control.LifeCycleController#stopFc()
     */
    @Override
    public void stopFc()
    {
        log.fine("Skeleton will be stopped...");
        if(this.deviceUDN==null)
        {
            log.warning("Try to stop a not started UPnP skelton");
        }
        else
        {
            this.upnpServer.removeUPnPDevice(this.deviceUDN);
        }
        super.stopFc();
    }
    
    /** Getters and setters */
    public String getDeviceVersion()
    {
        return deviceVersion;
    }

    public void setDeviceVersion(String deviceVersion)
    {
        this.deviceVersion = deviceVersion;
    }

    public String getDeviceIcon()
    {
        return deviceIcon;
    }

    public void setDeviceIcon(String deviceIcon)
    {
        this.deviceIcon = deviceIcon;
    }

    public String getServiceId()
    {
        return serviceId;
    }

    public void setServiceId(String serviceId)
    {
        this.serviceId = serviceId;
    }

    public String getServiceType()
    {
        return serviceType;
    }

    public void setServiceType(String serviceType)
    {
        this.serviceType = serviceType;
    }

    public String getDeviceId()
    {
        return deviceId;
    }

    public void setDeviceId(String deviceId)
    {
        this.deviceId = deviceId;
    }

    public String getDeviceType()
    {
        return deviceType;
    }

    public void setDeviceType(String deviceType)
    {
        this.deviceType = deviceType;
    }
}
