/**
 * Fractal Binding Factory.
 * Copyright (C) 2013 INRIA, University of Lille 1

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: gwenael.cattez@inria.fr
 * 
 * Author: Gwenael Cattez
 * 
 */
package org.objectweb.fractal.bf.connectors.upnp;

import org.objectweb.fractal.bf.connectors.common.SkeletonContentAttributes;

/**
 *
 */
public interface UPnPSkeletonContentAttributes extends SkeletonContentAttributes, UPnPContentAttributes
{
    public final static String DEFAULT_DEVICE_TYPE="FRACTAL-SKELTON";
    public final static int DEFAULT_DEVICE_VERSION=1;
    public final static String DEFAULT_MANUFACTURER_DETAILS="FRACTAL";
    public final static String DEFAULT_MODEL_DETAILS="FRACTAL-CLING-UPnP";
    
    public String getDeviceVersion();
    public void setDeviceVersion(String deviceVersion);

    public String getDeviceIcon();
    public void setDeviceIcon(String deviceIcon);
}
