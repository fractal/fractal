/**
 * Fractal Binding Factory.
 * Copyright (C) 2013 INRIA, University of Lille 1

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: gwenael.cattez@inria.fr
 * 
 * Author: Gwenael Cattez
 * 
 */

package org.objectweb.fractal.bf.connectors.upnp;

import org.objectweb.fractal.bf.BindHints;
import org.objectweb.fractal.bf.ExportHints;

/**
 *
 */
public class UPnPHints implements ExportHints, BindHints
{
    private String serviceId;

    private String serviceType;

    private String deviceId;

    private String deviceType;

    public UPnPHints()
    {
        this.serviceId=new String();
        this.serviceType=new String();
        this.deviceId=new String();
        this.deviceType=new String();
    }
    
    public String getServiceId()
    {
        return serviceId;
    }

    public void setServiceId(String serviceId)
    {
        this.serviceId = serviceId;
    }

    public String getServiceType()
    {
        return serviceType;
    }

    public void setServiceType(String serviceType)
    {
        this.serviceType = serviceType;
    }

    public String getDeviceId()
    {
        return deviceId;
    }

    public void setDeviceId(String deviceId)
    {
        this.deviceId = deviceId;
    }

    public String getDeviceType()
    {
        return deviceType;
    }

    public void setDeviceType(String deviceType)
    {
        this.deviceType = deviceType;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer("[");
        sb.append(UPnPConnectorConstants.SERVICE_ID + ":");
        sb.append(this.serviceId);
        sb.append(UPnPConnectorConstants.SERVICE_TYPE + ":");
        sb.append(this.serviceType);
        sb.append(UPnPConnectorConstants.DEVICE_ID + ":");
        sb.append(this.deviceId);
        sb.append(UPnPConnectorConstants.DEVICE_TYPE + ":");
        sb.append(this.deviceType);
        sb.append("]");
        return sb.toString();
    }
}
