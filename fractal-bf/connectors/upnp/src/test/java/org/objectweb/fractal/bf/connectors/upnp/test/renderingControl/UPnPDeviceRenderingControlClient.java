/**
 * Fractal Binding Factory.
 * Copyright (C) 2013 INRIA, University of Lille 1

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: gwenael.cattez@inria.fr
 * 
 * Author: Gwenael Cattez
 * 
 */

package org.objectweb.fractal.bf.connectors.upnp.test.renderingControl;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.ow2.fractal.upnp.annotation.UPnPAction;
import org.ow2.fractal.upnp.annotation.UPnPVariable;


public class UPnPDeviceRenderingControlClient implements UPnPDeviceRenderingControlItf , BindingController
{
        UPnPDeviceRenderingControlItf server;
    
	@UPnPAction("ListPresets")
	public ListPresets listPresets(@UPnPVariable("InstanceID") String InstanceID)
	{
	    return server.listPresets(InstanceID);
	}

	@UPnPAction("SetMute")
	public void setMute(@UPnPVariable("InstanceID") String InstanceID, @UPnPVariable("Channel") String Channel, @UPnPVariable("DesiredMute") String DesiredMute)
	{
		server.setMute(InstanceID, Channel, DesiredMute);
	}

	@UPnPAction("SetVolume")
	public void setVolume(@UPnPVariable("InstanceID") String InstanceID, @UPnPVariable("Channel") String Channel, @UPnPVariable("DesiredVolume") String DesiredVolume)
	{
		server.setVolume(InstanceID, Channel, DesiredVolume);
	}

	@UPnPAction("GetVolume")
	public int getVolume(@UPnPVariable("InstanceID") String InstanceID, @UPnPVariable("Channel") String Channel) 
	{
		return server.getVolume(InstanceID, Channel);
	}

	@UPnPAction("GetMute")
	public boolean getMute(@UPnPVariable("InstanceID") String InstanceID, @UPnPVariable("Channel") String Channel)
	{
	    return server.getMute(InstanceID, Channel);
	}

	@UPnPAction("SelectPresets")
	public void selectPresets(@UPnPVariable("InstanceID") String InstanceID, @UPnPVariable("PresetName") String PresetName)
	{
	    server.selectPresets(InstanceID, PresetName);
	}
	
	/**
         * @see org.objectweb.fractal.api.control.BindingController#bindFc(java.lang.String,
         *      java.lang.Object)
         */
        public void bindFc(String arg0, Object arg1)
                        throws NoSuchInterfaceException, IllegalBindingException,
                        IllegalLifeCycleException {
                if (arg0.equals("service")) {
                        this.server = (UPnPDeviceRenderingControlItf) arg1;
                }
        }

        /**
         * @see org.objectweb.fractal.api.control.BindingController#listFc()
         */
        public String[] listFc() {
                return new String[] { "service" };
        }

        /**
         * @see org.objectweb.fractal.api.control.BindingController#lookupFc(java.lang.String)
         */
        public Object lookupFc(String arg0) throws NoSuchInterfaceException {

                if (arg0.equals("service")) {
                        return this.server;
                }

                return null;
        }

        /**
         * @see org.objectweb.fractal.api.control.BindingController#unbindFc(java.lang.String)
         */
        public void unbindFc(String arg0) throws NoSuchInterfaceException,
                        IllegalBindingException, IllegalLifeCycleException {
                // TODO Auto-generated method stub

        }


}
