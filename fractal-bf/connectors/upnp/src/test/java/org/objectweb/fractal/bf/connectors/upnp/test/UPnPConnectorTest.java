/**
 * Fractal Binding Factory.
 * Copyright (C) 2013 INRIA, University of Lille 1

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: gwenael.cattez@inria.fr
 * 
 * Author: Gwenael Cattez
 * 
 */

package org.objectweb.fractal.bf.connectors.upnp.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;
import org.objectweb.fractal.bf.connectors.upnp.UPnPBindHints;
import org.objectweb.fractal.bf.connectors.upnp.UPnPConnector;
import org.objectweb.fractal.bf.connectors.upnp.UPnPExportHints;
import org.objectweb.fractal.bf.connectors.upnp.test.renderingControl.UPnPDeviceRenderingControlImpl;
import org.objectweb.fractal.bf.connectors.upnp.test.renderingControl.UPnPDeviceRenderingControlItf;
import org.objectweb.fractal.util.Fractal;
import org.ow2.fractal.upnp.device.UPnPDeviceFactory;
import org.ow2.fractal.upnp.device.UPnPDeviceService;
import org.ow2.fractal.upnp.server.UPnPServer;
import org.teleal.cling.model.meta.LocalDevice;


public class UPnPConnectorTest
{
    Component service;
    Component client;

    Component upnpDeviceRenderingControlService;
    Component upnpDeviceRenderingControlClient;
    
    private static Factory adlFactory;
    private UPnPConnector connector;

    @BeforeClass
    public static void initializeSystemProperties() throws ADLException
    {
        System.setProperty("fractal.provider", "org.objectweb.fractal.julia.Julia");
        adlFactory = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
    }

    @Before
    public void setUp() throws Exception
    {
        service = (Component) adlFactory.newComponent("org.objectweb.fractal.bf.connectors.upnp.test.example.Service", null);
        Assert.assertNotNull(service);
        client = (Component) adlFactory.newComponent("org.objectweb.fractal.bf.connectors.upnp.test.example.Client", null);
        Assert.assertNotNull(client);

        upnpDeviceRenderingControlClient = (Component) adlFactory.newComponent("org.objectweb.fractal.bf.connectors.upnp.test.example.UPnPDeviceRenderingControlClient", null);
        Assert.assertNotNull(upnpDeviceRenderingControlClient);
        upnpDeviceRenderingControlService = (Component) adlFactory.newComponent("org.objectweb.fractal.bf.connectors.upnp.test.example.UPnPDeviceRenderingControlService", null);
        Assert.assertNotNull(upnpDeviceRenderingControlService);
        
        BindingFactoryClassLoader cl = new BindingFactoryClassLoader(Thread.currentThread().getContextClassLoader());
        connector = new UPnPConnector();
        connector.setBindingFactoryClassLoader(cl);
        Assert.assertNotNull(connector);
    }

    @Test
    public void testCreateStubComponent() throws Exception
    {
        UPnPDeviceFactory serverFactory = new UPnPDeviceFactory();
        serverFactory.setUpnpDeviceId("Fractal");
        serverFactory.setUpnpDeviceType("Fractal-UPnP-test");
        serverFactory.setUpnpDeviceVersion(1);
        serverFactory.setUpnpDeviceFriendlyName("Friendly UPnP device name");
        serverFactory.setUpnpDeviceManufacturerDetails("Fractal");
        serverFactory.setUpnpDeviceModelDetails("Test");
        
        TServer upnpServiceProvider = new TServer();
        UPnPDeviceService upnpDeviceService = new UPnPDeviceService();
        upnpDeviceService.setUpnpServiceClass(TInterface.class);
        upnpDeviceService.setUpnpServiceProvider(upnpServiceProvider);
        serverFactory.addService(upnpDeviceService);

        LocalDevice localDevice = serverFactory.generateUPnPDevice();
        UPnPServer upnpServer = new UPnPServer();
        upnpServer.addUPnPDevice(localDevice);
        upnpServer.start();
        
        UPnPBindHints bindingHints = new UPnPBindHints();
        bindingHints.setServiceType("org-objectweb-fractal-bf-connectors-upnp-test-TInterface");
        Component stub = connector.createStub(client, "service", bindingHints);
        assertNotNull(stub);

        try
        {
            Fractal.getLifeCycleController(stub).startFc();
            assertEquals(LifeCycleController.STARTED, Fractal.getLifeCycleController(stub).getFcState());

            TInterface upnpClient = (TInterface) stub.getFcInterface("service");
            assertNotNull(upnpClient);
            
            assertEquals(upnpClient.meth3(), upnpServiceProvider.meth3());
            //TODO continue test by comparing return values
            
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            fail();
        }
        finally
        {
            upnpServer.stop();
        }
    }
    
    @Test
    public void testUPnPDeviceRenderingControl() throws Exception
    {
        //run upnp server
        UPnPDeviceFactory serverFactory = new UPnPDeviceFactory();
        serverFactory.setUpnpDeviceId("Fractal");
        serverFactory.setUpnpDeviceType("Fractal-UPnP-test");
        serverFactory.setUpnpDeviceVersion(1);
        serverFactory.setUpnpDeviceFriendlyName("Friendly UPnP device name");
        serverFactory.setUpnpDeviceManufacturerDetails("Fractal");
        serverFactory.setUpnpDeviceModelDetails("Test");
        
        UPnPDeviceRenderingControlImpl upnpServiceProvider = new UPnPDeviceRenderingControlImpl();
        UPnPDeviceService upnpDeviceService = new UPnPDeviceService();
        upnpDeviceService.setUpnpServiceClass(UPnPDeviceRenderingControlItf.class);
        upnpDeviceService.setUpnpServiceProvider(upnpServiceProvider);
        serverFactory.addService(upnpDeviceService);

        LocalDevice localDevice = serverFactory.generateUPnPDevice();
        UPnPServer upnpServer = new UPnPServer();
        upnpServer.addUPnPDevice(localDevice);
        upnpServer.start();
        
        UPnPBindHints bindingHints = new UPnPBindHints();
        bindingHints.setServiceType("RenderingControl");
        Component stub = connector.createStub(upnpDeviceRenderingControlClient, "service", bindingHints);
        assertNotNull(stub);

        try
        {
            Fractal.getLifeCycleController(stub).startFc();
            assertEquals(LifeCycleController.STARTED, Fractal.getLifeCycleController(stub).getFcState());

            UPnPDeviceRenderingControlItf upnpClient = (UPnPDeviceRenderingControlItf) stub.getFcInterface("service");
            assertNotNull(upnpClient);
            
            assertEquals(upnpClient.listPresets("0"), upnpServiceProvider.listPresets("0"));
            assertEquals(upnpClient.getMute("0", "0"),upnpServiceProvider.getMute("0", "0"));
            upnpClient.setMute("0", "0", "true");
            assertEquals(true,upnpClient.getMute("0", "0"));
            assertEquals(upnpClient.getVolume("0", "0"),upnpServiceProvider.getVolume("0", "0"));
            upnpClient.setVolume("0", "0", "42");
            assertEquals(42,upnpClient.getVolume("0", "0"));
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testCreateSkeletonAndCheckState() throws Exception
    {
        UPnPExportHints upnpBindHints=new UPnPExportHints();
        Component skel = connector.createSkel("service", service.getFcInterface("service"), service, upnpBindHints);
        assertNotNull("Skeleton component should not be null", skel);
        assertEquals(LifeCycleController.STOPPED, Fractal.getLifeCycleController(skel).getFcState());
    }
}
