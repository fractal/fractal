/**
 * Fractal Binding Factory.
 * Copyright (C) 2013 INRIA, University of Lille 1

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: gwenael.cattez@inria.fr
 * 
 * Author: Gwenael Cattez
 * 
 */

package org.objectweb.fractal.bf.connectors.upnp.test.renderingControl;

import java.util.logging.Logger;

import org.ow2.fractal.upnp.annotation.UPnPAction;
import org.ow2.fractal.upnp.annotation.UPnPVariable;

public class UPnPDeviceRenderingControlImpl implements UPnPDeviceRenderingControlItf
{
    private Logger log = Logger.getLogger(UPnPDeviceRenderingControlImpl.class.getName());

    private boolean mute;

    private int volume;

    @UPnPAction("ListPresets")
    public ListPresets listPresets(@UPnPVariable("InstanceID") String InstanceID)
    {
        return new ListPresets();
    }

    @UPnPAction("SetMute")
    public void setMute(@UPnPVariable("InstanceID") String InstanceID, @UPnPVariable("Channel") String Channel, @UPnPVariable("DesiredMute") String DesiredMute)
    {
        try
        {
            this.mute = Boolean.valueOf(DesiredMute);
        } catch (Exception e)
        {
            this.mute = false;
        }
    }

    @UPnPAction("SetVolume")
    public void setVolume(@UPnPVariable("InstanceID") String InstanceID, @UPnPVariable("Channel") String Channel,
            @UPnPVariable("DesiredVolume") String DesiredVolume)
    {
        log.info("setVolume");
        try
        {
            this.volume = Integer.valueOf(DesiredVolume);
        } catch (Exception e)
        {
            this.volume = 0;
        }
    }

    @UPnPAction("GetVolume")
    public int getVolume(@UPnPVariable("InstanceID") String InstanceID, @UPnPVariable("Channel") String Channel)
    {
        log.info("getVolume");
        return volume;
    }

    @UPnPAction("GetMute")
    public boolean getMute(@UPnPVariable("InstanceID") String InstanceID, @UPnPVariable("Channel") String Channel)
    {
        log.info("getMute");
        return mute;
    }

    @UPnPAction("SelectPresets")
    public void selectPresets(@UPnPVariable("InstanceID") String InstanceID, @UPnPVariable("PresetName") String PresetName)
    {
        log.info("selectPresets");
    }

}
