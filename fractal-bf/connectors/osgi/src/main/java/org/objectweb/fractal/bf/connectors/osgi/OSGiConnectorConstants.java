/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 Bull S.A.S.
 * Copyright (C) 2009 INRIA, SARDES
 * Contact: fractal@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.fractal.bf.connectors.osgi;

/**
 * OSGi connector constants.
 *
 * @author Loris Bouzonnet
 */
public interface OSGiConnectorConstants {

    /**
     * A search filter.
     * A filter string is based upon the string representation of LDAP search filters.
     */
    String OSGI_SERVICE_FILTER = "filter";

    /**
     * Some service properties.
     * A properties string is based upon the syntax "name=value(,name=value)*".
     */
    String OSGI_SERVICE_PROPERTIES = "properties";

	String OBJECTCLASS = "objectClass";
}
