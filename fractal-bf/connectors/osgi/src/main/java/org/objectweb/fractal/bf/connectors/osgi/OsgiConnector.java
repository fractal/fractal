/**
 * Fractal Binding Factory.
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Copyright (C) 2008-2009 INRIA, SARDES
 * Contact: fractal@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.objectweb.fractal.bf.connectors.osgi;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.net.URL;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.BindingFactoryPlugin;
import org.objectweb.fractal.bf.SkeletonGenerationException;
import org.objectweb.fractal.bf.StubGenerationException;
import org.objectweb.fractal.bf.StubGenerationUtil;
import org.objectweb.fractal.util.Fractal;
import org.osgi.framework.BundleContext;
import org.ow2.fractal.julia.osgi.api.control.OSGiContextController;

/**
 * A plugin to import and export fractal interfaces through the OSGi(tm) services.
 * @author Loris Bouzonnet
 */
public class OsgiConnector implements BindingFactoryPlugin<OsgiExportHints, OsgiBindHints>, OSGiContextController {
	
    /**
     * Logger.
     */
    private static Logger log = Logger.getLogger(OsgiConnector.class.getCanonicalName());


    private BindingFactoryClassLoader classLoader;

    /**
     * Bundle context.
     */
    private BundleContext bundleContext;


    public Component createSkel(final String interfaceName, final Object interfaceToExport,
    		final Component owner, final OsgiExportHints exportHints)
            throws SkeletonGenerationException {
            log.info("OsgiConnector - Creating skeleton.");
            log.finest("ServerItf ref: " + interfaceToExport + " Owner: " + owner);

            String sdict = exportHints.getDictionary();
            Dictionary<Object, Object> dictionary = null;
            if(sdict != null) {
                dictionary = new Hashtable<Object, Object>();

                String[] entries = sdict.split(",");
                for(String entry : entries) {
                    String[] keyValue = entry.split("=");
                    dictionary.put(keyValue[0], keyValue[1]);
                }
            }

            String objectClass = exportHints.getObjectClass();
            Set<String> classnames = new HashSet<String>();
            if (objectClass != null) {
            	String[] entries = objectClass.split(",");
            	for(String entry : entries) {
            		classnames.add(entry);
            	}
            }

            // A fractal component whose content is a service.
            Component osgiServiceBridge = null;

            try {
                Factory factory = (Factory) bundleContext.getBundle().loadClass(
                        "org.objectweb.fractal.bf.connectors.osgi.org.objectweb.fractal.bf.connectors.osgi.OsgiSkeleton").newInstance();

                osgiServiceBridge = factory.newFcInstance();

                OSGiContextController occ =
                    (OSGiContextController) osgiServiceBridge.getFcInterface(
                            "osgi-context-controller");

                BundleContext ctxt = findBundleContext(owner);
                occ.setBundleContext(ctxt);

                InterfaceType it =
                    (InterfaceType) ((Interface) interfaceToExport).getFcItfType();
                classnames.add(it.getFcItfSignature());

                OsgiSkeletonAttributes connectorAttributes =
                    (OsgiSkeletonAttributes) Fractal.getAttributeController(osgiServiceBridge);
                if(dictionary != null) {
                    connectorAttributes.setDictionary(dictionary);
                }
                connectorAttributes.setServiceObject(interfaceToExport);
                
                connectorAttributes.setServiceClassnames(classnames.toArray(new String[classnames.size()]));
            } catch (Exception e) {
                log.log(Level.SEVERE, "Unable to start the OSGi service bridge", e);
                throw new OsgiConnectorException("Unable to start the OSGi service bridge", e);
            }
            log.info("OsgiConnector - skeleton created.");
            return osgiServiceBridge;
    }

    public OsgiBindHints getBindHints(final Map<String, Object> initialHints) {
        OsgiBindHints bindHints = new OsgiBindHints();
        String filter =
            (String) initialHints.get(OSGiConnectorConstants.OSGI_SERVICE_FILTER);
        bindHints.setFilter(filter);
        return bindHints;
    }

    public OsgiExportHints getExportHints(final Map<String, Object> initialHints) {
        OsgiExportHints exportHints = new OsgiExportHints();
        String props =
            (String) initialHints.get(OSGiConnectorConstants.OSGI_SERVICE_PROPERTIES);
        exportHints.setDictionary(props);
        String objectClass = (String) initialHints.get(OSGiConnectorConstants.OBJECTCLASS);
        exportHints.setObjectClass(objectClass);
        return exportHints;
    }

    public String getPluginIdentifier() {
        return "osgi";
    }

    public BundleContext getBundleContext() {
        return bundleContext;
    }

    public void setBundleContext(final BundleContext context) {
        this.bundleContext = context;

    }

    public Component createStub(final Component client, final String itfname,
            final OsgiBindHints bindHints) throws StubGenerationException {

        log.info("OsgiConnector - Creating stub.");

        if (client == null) {
            throw new IllegalArgumentException(
                    "The component owning the client interface to bind can't be null");
        } else if (itfname == null || itfname.equalsIgnoreCase("")) {
            throw new IllegalArgumentException(
                    "Can't find client interface with name:" + itfname);
        } else if (bindHints == null) {
            throw new IllegalArgumentException(
                    "You didn't specify a map of hints to create the stub.");
        }

        /*
         * this composite is the component that is returned
         */
        Component stubComponent = null;
        InterfaceType businessInterfaceItfType = null;

        ComponentType itf = (ComponentType) (client).getFcType();

        try {
            businessInterfaceItfType = itf.getFcInterfaceType(itfname);
        } catch (NoSuchInterfaceException e) {
            log.log(Level.SEVERE, "Cannot get the interface type: " + itfname, e);
            throw new StubGenerationException(
                    "Cannot get the interface type: " + itfname, e);
        }

        Class<?> itfClass = null;
        try {
            itfClass = Thread.currentThread().getContextClassLoader().loadClass(
                    businessInterfaceItfType.getFcItfSignature());
        } catch (ClassNotFoundException e) {
            log.log(Level.SEVERE, "Cannot find class for interface type: "
                            + businessInterfaceItfType.getFcItfSignature(), e);
            throw new StubGenerationException(
                    "Cannot find class for interface type: "
                            + businessInterfaceItfType.getFcItfSignature(), e);
        }

        // Check if the stub has been generated with Juliac
        try {
            Class<?> clazz = Thread.currentThread().getContextClassLoader().loadClass(
                    businessInterfaceItfType.getFcItfSignature() + "_OSGiStub");
            Factory factory = (Factory) clazz.newInstance();

            stubComponent = factory.newFcInstance();


        } catch (ClassNotFoundException e) {

            log.log(Level.FINE, "Unable to load the OSGi stub", e);

            // Otherwise dynamically generate it with Julia
            stubComponent = createStubWithJulia(itfClass, businessInterfaceItfType, bindHints);
        } catch (Exception e) {
            log.log(Level.SEVERE, "Unable to instanciate the OSGi stub", e);
            throw new StubGenerationException("Unable to instanciate the OSGi stub");
        }

        OSGiContextController occ;
        try {
            occ = (OSGiContextController) stubComponent.getFcInterface(
                    "osgi-context-controller");
        } catch (NoSuchInterfaceException e) {
            log.log(Level.SEVERE, "Unable to retrieve the OSGi context controller", e);
            throw new StubGenerationException(
                    "Unable to retrieve the OSGi context controller", e);
        }

        BundleContext ctxt = findBundleContext(client);
        occ.setBundleContext(ctxt);

        OsgiStubAttributes attributes;
        try {
            attributes = (OsgiStubAttributes)
                Fractal.getAttributeController(stubComponent);
        } catch (NoSuchInterfaceException e) {
            log.log(Level.SEVERE, "Unable to retrieve the attribute controller", e);
            throw new StubGenerationException(
                    "Unable to retrieve the attribute controller", e);
        }
        attributes.setServiceName(itfClass.getCanonicalName());
        attributes.setFilter(bindHints.getFilter());

        return stubComponent;
    }

    private Component createStubWithJulia(final Class<?> itfClass, final InterfaceType businessInterfaceItfType,
            final OsgiBindHints bindHints) throws StubGenerationException {

        Object proxyComponentContentInstance;
        try {
            InvocationHandler ih = new OsgiServiceCaller();
            proxyComponentContentInstance = Proxy.newProxyInstance(
            		new ClassLoader(Thread.currentThread().getContextClassLoader()) {
            	
            	protected Class<?> findClass(final String name) throws ClassNotFoundException {
            		return bundleContext.getBundle().loadClass(name);
            	}
            	
            	protected URL findResource(final String name) {
            		return bundleContext.getBundle().getResource(name);
            	}
			}
                    ,new Class[] {itfClass, LifeCycleController.class,
                        OsgiStubAttributes.class, OSGiContextController.class}, ih);
        } catch (Exception e) {
            log.log(Level.SEVERE,
                    "Unable to build the OSGi service caller for the class with name "
                    + itfClass.getName(), e);
            throw new StubGenerationException(
                    "Unable to build the OSGi service caller for the class with name "
                    + itfClass.getName(), e);
        }

        InterfaceType stubInterfaceType =
            StubGenerationUtil.symmetricInterfaceType(businessInterfaceItfType);

        log.finer("Interface type for stub component created: "
                + stubInterfaceType);

        Map<String, String> hints = new HashMap<String, String>();
        hints.put("fractal.provider", "org.objectweb.fractal.julia.Julia");
        hints.put("julia.config", "julia-osgi-context.cfg");

        TypeFactory tf;
        GenericFactory gf;
        try {
            Component boot = Fractal.getBootstrapComponent(hints);
            tf = Fractal.getTypeFactory(boot);
            gf = Fractal.getGenericFactory(boot);
        } catch (Exception e) {
            log.log(Level.SEVERE, "Unable to retrieve the Fractal bootstrap components", e);
            throw new StubGenerationException(
                    "Unable to retrieve the Fractal bootstrap components", e);
        }

        ComponentType stubComponentType;
        try {
            stubComponentType = tf.createFcType(
                    new InterfaceType[] {stubInterfaceType,
                            tf.createFcItfType("attribute-controller",
                                    "org.objectweb.fractal.bf.connectors.osgi.OsgiStubAttributes", false, false, false)});
        } catch (InstantiationException e) {
            log.log(Level.SEVERE, "Unable to create a component type for stub component", e);
            throw new StubGenerationException(
                    "Unable to create a component type for stub component", e);
        }
        log.finer("Component type for stub component created: " + stubComponentType);

        Object[] ctrlDesc = new Object[] {
                proxyComponentContentInstance.getClass().getClassLoader(),
                "primitiveBundled" };
        log.finest("Controller descriptor for stub component: " + ctrlDesc);

        Component stubComponent;
        try {
            stubComponent = gf.newFcInstance(stubComponentType, ctrlDesc,
                    proxyComponentContentInstance);
        } catch (InstantiationException e) {
            log.log(Level.SEVERE, "Unable to create a stub component", e);
            throw new StubGenerationException(
                    "Unable to create a stub component", e);
        }
        log.finer("Stub component created: " + stubComponent);

        return stubComponent;
    }

    public void finalizeSkeleton(final Component skel,
            final Map<String, String> finalizationHints)
            throws BindingFactoryException {
        // do nothing

    }

    public void finalizeStub(final Component stub, final Map<String, Object> hints)
            throws BindingFactoryException {
        // do nothing

    }

    public BindingFactoryClassLoader getBindingFactoryClassLoader() {
        return this.classLoader;
    }

    public void setBindingFactoryClassLoader(final BindingFactoryClassLoader classLoader) {
        this.classLoader = classLoader;
    }
    
    private BundleContext findBundleContext(Component owner) {

        BundleContext ctxt = null;
        try {
            OSGiContextController occOwner = (OSGiContextController) owner.getFcInterface("osgi-context-controller");
            ctxt = occOwner.getBundleContext();
        } catch (NoSuchInterfaceException e) {
            log.log(Level.FINE, "OSGi context controller not found", e);
        }

        if (ctxt == null) {
        	Component parentComposite;
			try {
				parentComposite = Fractal.getSuperController(owner).getFcSuperComponents()[0];
	        	try {
	                OSGiContextController occOwner =
	                	(OSGiContextController) parentComposite.getFcInterface("osgi-context-controller");
	                ctxt = occOwner.getBundleContext();
	            } catch (NoSuchInterfaceException e) {
	                log.log(Level.FINE, "OSGi context controller not found in the parent composite", e);
	            }
			} catch (NoSuchInterfaceException e) {
				log.log(Level.FINE, "Super controller not found", e);
			}
        }

        if (ctxt == null) {
            log.log(Level.WARNING, "Using the bundle context of the OSGi connector");
            ctxt = bundleContext;
        }
        return ctxt;
    }

}
