/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 Bull S.A.S.
 * Copyright (C) 2009 INRIA, SARDES
 * Contact: fractal@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.objectweb.fractal.bf.connectors.osgi;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.Filter;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.util.tracker.ServiceTracker;
import org.ow2.fractal.julia.osgi.api.control.OSGiContextController;

/**
 * @author Loris Bouzonnet
 */
public abstract class AbstractOsgiStubContent
    implements LifeCycleController, OSGiContextController, OsgiStubAttributes {

    /**
     * Logger.
     */
    private static Logger log = Logger.getLogger(AbstractOsgiStubContent.class.getCanonicalName());

    private ServiceTracker tracker;

    private BundleContext context;

    private String filter;

    private String fcState;

    private String serviceName;


    public String getFcState() {
        return fcState;
    }

    public void startFc() throws IllegalLifeCycleException {
        Filter osgiFilter;
        if (filter != null) {
            if (!filter.contains(Constants.OBJECTCLASS)) {
                filter = "(&(" + Constants.OBJECTCLASS
                + "=" + serviceName + ")" + filter + ")";
            }
        } else {
            filter = "(" + Constants.OBJECTCLASS + "=" + serviceName + ")";
        }
        try {
            osgiFilter = context.createFilter(filter);
        } catch (InvalidSyntaxException e) {
            log.log(Level.SEVERE, "Invalid filter: " + filter, e);
            IllegalLifeCycleException ilce =
                new IllegalLifeCycleException("Invalid filter: " + filter);
            ilce.initCause(e);
            throw ilce;
        }
        tracker = new ServiceTracker(context, osgiFilter, null);
        tracker.open();
    }

    public void stopFc() throws IllegalLifeCycleException {
        if (tracker != null) {
            tracker.close();
        }
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(final String filter) {
        this.filter = filter;
    }

    public BundleContext getBundleContext() {
        return context;
    }

    public void setBundleContext(final BundleContext context) {
    	if (this.context == null) {
    		this.context = context;
    	}
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(final String serviceName) {
        this.serviceName = serviceName;
    }

    protected Object getService() {
        return tracker.getService();
    }

}
