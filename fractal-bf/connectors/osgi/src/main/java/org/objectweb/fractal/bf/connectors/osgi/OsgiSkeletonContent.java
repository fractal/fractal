/**
 * Fractal Binding Factory.
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Copyright (C) 2008-2009 INRIA, SARDES
 * Contact: fractal@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.objectweb.fractal.bf.connectors.osgi;

import java.util.Dictionary;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.bf.AbstractSkeleton;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.ow2.fractal.julia.osgi.api.control.OSGiContextController;

/**
 * A bridge to publish the interface of business object as an OSGi(tm) service.
 * @author Loris Bouzonnet
 */
public class OsgiSkeletonContent extends AbstractSkeleton implements
        OsgiSkeletonAttributes, BindingController, LifeCycleController, OSGiContextController {


    /**
     * The name of the client interface referencing the Fractal business
     * interface.
     */
    public static final String SERVICE_CLIENT_ITF_NAME = "service";

    private String state = LifeCycleController.STOPPED;

    private Dictionary<?, ?> dictionary;

    private String[] serviceNames;

    private BundleContext bundleContext;

    private ServiceRegistration serviceRegistration;

    /**
     * The business object. This is a client interface, in the sense of fractal.
     */
    private Object service;

    public String getFcState() {
        return this.state;
    }

    public void startFc() throws IllegalLifeCycleException {
    	if (state.equals(LifeCycleController.STARTED)) {
            throw new IllegalLifeCycleException(
            		"The OSGi skeleton component is already started!");
        }

        if (bundleContext == null) {
            throw new IllegalStateException("Bundle context missing!!!");
        }
        registerService();
        this.state = LifeCycleController.STARTED;
    }

    public void stopFc() throws IllegalLifeCycleException {
    	if (state.equals(LifeCycleController.STOPPED)) {
            throw new IllegalLifeCycleException(
            		"The OSGi skeleton component is already stopped!");
        }
        unregisterService();
        this.state = LifeCycleController.STOPPED;
    }

    private void registerService() {
        serviceRegistration = bundleContext.registerService(serviceNames, service, dictionary);
    }

    private void unregisterService() {
        if(serviceRegistration != null) {
            serviceRegistration.unregister();
        }
    }

    public void setDictionary(final Dictionary<?, ?> dictionary) {
        this.dictionary = dictionary;
    }

    public void setServiceObject(Object service) {
        this.service = service;
    }

    // binding-controller implementation
    @Override
    public void bindFc(final String clientItfName, final Object serverItf)
            throws NoSuchInterfaceException, IllegalBindingException,
            IllegalLifeCycleException {
        if (clientItfName.equalsIgnoreCase(SERVICE_CLIENT_ITF_NAME)) {
            this.service = serverItf;
        } else
            super.bindFc(clientItfName, serverItf);
    }

    @Override
    public String[] listFc() {
        String[] superItfs = super.listFc();
        String[] itfs = new String[superItfs.length + 1];
        int i = 0;
        while (i < superItfs.length) {
            itfs[i] = superItfs[i];
            i++;
        }
        itfs[i] = SERVICE_CLIENT_ITF_NAME;
        return itfs;
    }

    @Override
    public Object lookupFc(final String clientItfName)
            throws NoSuchInterfaceException {
        if (clientItfName.equalsIgnoreCase(SERVICE_CLIENT_ITF_NAME)) {
            return this.service;
        } else
            return super.lookupFc(clientItfName);
    }

    @Override
    public void unbindFc(final String clientItfName) throws NoSuchInterfaceException,
            IllegalBindingException, IllegalLifeCycleException {
        if (clientItfName.equalsIgnoreCase(SERVICE_CLIENT_ITF_NAME)) {
            this.service = null;
        } else
            super.unbindFc(clientItfName);
    }

    public void setBundleContext(final BundleContext bundleContext) {
    	if (this.bundleContext == null) {
    		this.bundleContext = bundleContext;
    	}
    }

    public BundleContext getBundleContext() {
        return bundleContext;
    }

    public void setServiceClassnames(final String[] names) {
        this.serviceNames = names;
    }

}
