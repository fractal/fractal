/**
 * Fractal Binding Factory.
 * Copyright (C) 2008-2010 Bull S.A.S.
 * Copyright (C) 2008-2010 INRIA, SARDES
 * Contact: fractal@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.objectweb.fractal.bf.connectors.osgi;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.osgi.framework.BundleContext;

/**
 * Dispatch calls to the OSGi(tm) services.
 * @author Loris Bouzonnet
 */
public class OsgiServiceCaller extends AbstractOsgiStubContent implements InvocationHandler {

    /**
     * Logger.
     */
    private static Logger log = Logger.getLogger(OsgiServiceCaller.class.getCanonicalName());


    public OsgiServiceCaller() {
    }

    public Object invoke(final Object proxy, final Method method, final Object[] args)
            throws Throwable {

        String methodName = method.getName();
        if (methodName.equals("startFc")) {
            startFc();
            return null;
        } else if (methodName.equals("stopFc")) {
            stopFc();
            return null;
        } else if (methodName.equals("getFcState")) {
            return getFcState();
        } else if (methodName.equals("getBundleContext")) {
            return getBundleContext();
        } else if (methodName.equals("setBundleContext")) {
            setBundleContext((BundleContext) args[0]);
            return null;
        } else if (methodName.equals("getServiceName")) {
            return getServiceName();
        } else if (methodName.equals("setServiceName")) {
            setServiceName((String) args[0]);
            return null;
        } else if (methodName.equals("getFilter")) {
            return getFilter();
        } else if (methodName.equals("setFilter")) {
            setFilter((String) args[0]);
            return null;
        }

        Object service = getService();
        if (service == null) {
            log.log(Level.SEVERE,
                    "Unable to invoke " + methodName + ": no service available");
            throw new OsgiConnectorException(
                    "Unable to invoke " + methodName + ": no service available");
        }
        try {
        	return method.invoke(service, args);
        } catch (InvocationTargetException e) {
        	throw e.getCause();
        }
    }

}
