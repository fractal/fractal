/**
 * Fractal Binding Factory.
 * Copyright (C) 2008-2009 Bull S.A.S.
 * Copyright (C) 2008-2009 INRIA, SARDES
 * Contact: fractal@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.objectweb.fractal.bf.connectors.osgi;

import org.objectweb.fractal.bf.ExportHints;

/**
 * The OSGi(tm) export hints.
 * @author Loris Bouzonnet
 */
public final class OsgiExportHints implements ExportHints {

    /**
     * Service properties.
     */
    private String dictionary;
    
    private String objectClass;

    /**
     * @return service properties
     */
    public String getDictionary() {
        return dictionary;
    }

    /**
     * Set service properties.
     * @param dictionary service properties
     */
    public void setDictionary(final String dictionary) {
        this.dictionary = dictionary;
    }

	public String getObjectClass() {
		return objectClass;
	}

	public void setObjectClass(final String objectClass) {
		this.objectClass = objectClass;
	}

}
