
This module stores the connectors currently under development for the binding factory framework.
Five of them are currently functional:
- soap-cxf : to export Fractal server interfaces as Web Services,  and to bind Fractal client interfaces to
 	previously exported web services;
- rest-cxf : to export Fractal server interfaces as RESTful Web Services, and to bind Fractal client interfaces to
 	previously exported RESTful web services;
- json-rpc : to export Fractal server interfaces via JSON-RPC;
- rmi: a JavaRMI based connector;
- osgi: an OSGi based connector.
 	
Three are currently considered 'experimental': 	
- tcp-dream: a 1-to-1 RPC-oriented connector, to carry on remote procedure calls between remote components; this
	 connector might be considered a simple alternative to fractal-rmi, where remote administration
	 of components is not a requirement;
- groupcomm-jgroups: a group oriented connector; it allows group-communication; examples show how
	to use it as a mean to send method invocations to all members in a specified group;
- jbi-petals