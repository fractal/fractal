
package org.objectweb.fractal.bf.connectors.group;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

import org.jgroups.Channel;
import org.jgroups.ChannelClosedException;
import org.jgroups.ChannelException;
import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.bf.AbstractSkeleton;
import org.objectweb.fractal.bf.BindingFactoryException;

public class GroupServerBridge extends AbstractSkeleton
    implements
      ChannelController,
      GroupCommunicationAttributesController
{

  /**
   * The receiver adapter to attach to the channel.
   */
  ReceiverAdapter receiverAdapter;

  /**
   * The channel used by this skeleton to receive messages
   */
  Channel         channel;

  /**
   * A logger
   */
  Logger          log = Logger.getLogger(GroupServerBridge.class
                          .getCanonicalName());

  /**
   * The name of the group to use
   */
  private String  groupName;
  /**
   * The properties used to configure jgroups stack
   */
  private String  props;

  /**
   * The URL of the properties to be used to configure the JChannel. See
   * {@link JChannel#JChannel(java.net.URL)}
   */
  private String  url;

  public GroupServerBridge()
  {

  }

  /**
   * @see org.objectweb.fractal.bf.connectors.group.GroupCommunicationAttributesController#getName()
   */
  public String getName()
  {
    return this.groupName;
  }

  /**
   * @see org.objectweb.fractal.bf.connectors.group.GroupCommunicationAttributesController#getProps()
   */
  public String getProps()
  {
    return this.props;
  }

  /**
   * @see org.objectweb.fractal.bf.connectors.group.GroupCommunicationAttributesController#setName(java.lang.String)
   */
  public void setName(String name)
  {
    this.groupName = name;
  }

  /**
   * @see org.objectweb.fractal.bf.connectors.group.GroupCommunicationAttributesController#setProps(java.lang.String)
   */
  public void setProps(String props)
  {
    this.props = props;
  }

  /**
   * @throws ChannelException
   * @throws ChannelClosedException
   * @see org.objectweb.fractal.bf.connectors.group.ChannelController#connect()
   */
  public void connect() throws ChannelClosedException, ChannelException
  {
    channel.connect(this.groupName);
  }

  /**
   * @throws ChannelException if some error occurs while attempting to connect
   *             to the {@link JChannel}
   * @throws MalformedURLException if the url is not a valid URL
   * @throws BindingFactoryException
   * @see org.objectweb.fractal.bf.connectors.group.ChannelController#finalizeConfig(java.lang.String)
   */
  public void finalizeConfig(String itfName) throws ChannelException,
      MalformedURLException, BindingFactoryException
  {

    Object businessDelegate = null;
    if (delegate != null)
    {
      try
      {
        businessDelegate = this.delegate.getFcInterface(itfName);
      }
      catch (NoSuchInterfaceException e)
      {
        throw new RuntimeException("Cannot find Fractal interface '" + itfName
            + "' on delegate component", e);
      }
    }
    else
    {
      throw new BindingFactoryException(
          "Cannot finalize this bridge, as the delegate object client interface has not been set yet");
    }

    if (this.url != null)
    {
      this.channel = new JChannel(new URL(this.url));
    }
    else
    {
      this.channel = new JChannel(this.props);
    }

    this.receiverAdapter = new MyReceiverAdapter(businessDelegate, channel);

    this.channel.setReceiver(this.receiverAdapter);

  }

  private class MyReceiverAdapter extends ReceiverAdapter
  {

    final Logger    log = Logger.getLogger(MyReceiverAdapter.class
                            .getCanonicalName());

    /**
     * The object this receiver will delegate method calls to.
     */
    private Object  delegate;
    /**
     * The channel this receiver is attached on. 
     */
    private Channel channel;

    /**
     * @param delegate
     */
    public MyReceiverAdapter(Object delegate, Channel channel)
    {
      this.delegate = delegate;
      log.info("business delegate passed to receiver adapter:" + this.delegate);
      this.channel = channel;
    }

    @Override
    public void receive(Message msg)
    {

      RPCHeader rpcHeader = (RPCHeader) msg.getHeader("RPC");

      log.finer("Delegate object:" + delegate);

      log.finer(rpcHeader.toString());

      log.fine("Message received: <src," + msg.getSrc() + "> <obj,"
          + msg.getObject() + ">");

      try
      {
        delegate.getClass().getMethod(rpcHeader.getMethodName(), new Class[]{})
            .invoke(delegate, new Object[]{});
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }

    }

  }

  /**
   * @see org.objectweb.fractal.bf.connectors.group.GroupCommunicationAttributesController#setUrl(java.lang.String)
   */
  public void setUrl(String urlProps)
  {
    this.url = urlProps;

  }

  /**
   * @see org.objectweb.fractal.bf.connectors.group.GroupCommunicationAttributesController#getUrl()
   */
  public String getUrl()
  {
    return this.url;
  }

}
