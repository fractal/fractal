package org.objectweb.fractal.bf.connectors.group;

import org.jgroups.*;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.bf.proxies.Bridge;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

/**
 * A simple JGroupClient
 */
public class JGroupClientBridge implements Bridge {
    
    Logger log = Logger.getLogger(JGroupClientBridge.class.getCanonicalName());
    
    JChannel channel;

    public JGroupClientBridge(String groupName, String propsUrl)  {
        
      if (propsUrl == null) {
        propsUrl = GroupConnectorConstants.TCP_URL;
        log.warning("JGroup client will use protocol stacks defined at URL: "+propsUrl);
      }
        
      log.info("JGroup client will connect to group: '"+groupName+"'");
      
        try
        {
          channel = new JChannel(new URL(propsUrl));
          channel.connect(groupName);
        }
        catch (MalformedURLException e)
        {
          e.printStackTrace();
        }
        catch (ChannelException e)
        {
          e.printStackTrace();
        }
       
    }

    public void send(String methodName) throws ChannelNotConnectedException, ChannelClosedException {
        Message msg = new Message(null, null, methodName);
        msg.putHeader("RPC", new RPCHeader(methodName));
        log.info("Sending RPC-oriented message over the channel");
        channel.send(msg);
        log.info("Message sent.");
        
    }

    public void disconnect() {
        channel.disconnect();
    }

    /**
     * @see org.objectweb.fractal.api.control.BindingController#bindFc(java.lang.String, java.lang.Object)
     */
    public void bindFc(String clientItfName, Object serverItf)
        throws NoSuchInterfaceException, IllegalBindingException,
        IllegalLifeCycleException
    {
      // TODO Auto-generated method stub
      
    }

    /**
     * @see org.objectweb.fractal.api.control.BindingController#listFc()
     */
    public String[] listFc()
    {
      // TODO Auto-generated method stub
      return null;
    }

    /**
     * @see org.objectweb.fractal.api.control.BindingController#lookupFc(java.lang.String)
     */
    public Object lookupFc(String clientItfName)
        throws NoSuchInterfaceException
    {
      // TODO Auto-generated method stub
      return null;
    }

    /**
     * @see org.objectweb.fractal.api.control.BindingController#unbindFc(java.lang.String)
     */
    public void unbindFc(String clientItfName) throws NoSuchInterfaceException,
        IllegalBindingException, IllegalLifeCycleException
    {
      // TODO Auto-generated method stub
      
    }
}