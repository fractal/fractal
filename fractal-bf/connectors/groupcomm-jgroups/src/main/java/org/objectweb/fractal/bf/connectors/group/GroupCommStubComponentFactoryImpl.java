
package org.objectweb.fractal.bf.connectors.group;

import org.objectweb.fractal.bf.proxies.AbstractStubComponentFactory;
import org.objectweb.fractal.bf.proxies.StubContentGenerator;

/**
 * @author Valerio Schiavoni <valerio.schiavoni@gmail.com>
 *
 */
public class GroupCommStubComponentFactoryImpl
    extends
      AbstractStubComponentFactory
{

  /**
   * @see org.objectweb.fractal.bf.proxies.AbstractStubComponentFactory#getStubContentGenerator()
   */
  @Override
  protected StubContentGenerator getStubContentGenerator()
  {
   return new GroupCommContentGenerator();
  }

}
