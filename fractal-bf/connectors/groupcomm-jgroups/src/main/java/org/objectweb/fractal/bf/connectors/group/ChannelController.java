package org.objectweb.fractal.bf.connectors.group;

import org.jgroups.ChannelClosedException;
import org.jgroups.ChannelException;
import org.jgroups.JChannel;
import org.objectweb.fractal.bf.BindingFactoryException;

import java.net.MalformedURLException;

public interface ChannelController {
    /**
     * Finalize the skeleton.
     *
     * @param itfName the name of the business interface
     * @throws ChannelException
     * @throws MalformedURLException if the {@link GroupServerBridge#getUrl()} is not a valid URL
     */
    public void finalizeConfig(String itfName) throws BindingFactoryException, ChannelException, MalformedURLException;

    /**
     * Trigger the connection to the {@link JChannel}
     *
     * @throws ChannelClosedException
     * @throws ChannelException
     */
    public void connect() throws ChannelClosedException, ChannelException;
}
