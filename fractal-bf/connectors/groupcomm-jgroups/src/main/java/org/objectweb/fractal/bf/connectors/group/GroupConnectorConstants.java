
package org.objectweb.fractal.bf.connectors.group;

/**
 * Set of constants used for the group-jgroups plugin.
 */
public interface GroupConnectorConstants
{

  public static final String PROPS = "props";
  public static final String GROUP_NAME = "groupName";
  public static final String PROPS_URL = "url";
  public static final String TCP_URL = "http://www.jgroups.org/javagroupsnew/perfnew/tcp.xml";

}
