
package org.objectweb.fractal.bf.connectors.group;

import org.objectweb.fractal.api.control.AttributeController;

/**
 * An {@link AttributeController} interface to configure JGroups components. It 
 * provides support to:
 * <ol>
 * <li> set the name of the group to join
 * <li> set the url of the XML file providing a JGroups protocol stack
 * <li> set a string representation of a JGroups protocol stacl
 * </ol> 
 * 
 * @author Valerio Schiavoni <valerio.schiavoni@gmail.com>
 *
 */
public interface GroupCommunicationAttributesController
    extends
      AttributeController
{

  public void setName(String name);

  public String getName();

  public void setProps(String props);

  public String getProps();
  
  public void setUrl(String urlProps);
  
  public String getUrl();

}
