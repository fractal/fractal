
package org.objectweb.fractal.bf.connectors.group;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import org.jgroups.Header;

public class RPCHeader extends Header implements Externalizable
{
  
  String methodName;
  Class<?>[] parameterTypes;       // parameter types
  Object[] args;                 // arguments
  
  /**
   * Default public constructor, required for de/serialization.
   */
  public RPCHeader() {
    
  }
  /**
   * Default public constructor, required for de/serialization.
   * @param methodName
   */
  public RPCHeader(String methodName)
  {
    this.methodName = methodName;
  }

  /**
   * @see java.io.Externalizable#readExternal(java.io.ObjectInput)
   */
  public void readExternal(ObjectInput in) throws IOException,
      ClassNotFoundException
  {
    this.methodName = in.readUTF();
  }

  /**
   * @see java.io.Externalizable#writeExternal(java.io.ObjectOutput)
   */
  public void writeExternal(ObjectOutput out) throws IOException
  {
    out.writeUTF(this.methodName);

  }
  public String getMethodName()
  {
    return methodName;
  }

}