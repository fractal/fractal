
package org.objectweb.fractal.bf.connectors.group;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.objectweb.fractal.bf.AbstractExportId;

/**
 * ExportId to join a group, or to connect to a JChannel using appropriate
 * configuration values.
 *
 */
public class GroupCommExportId extends AbstractExportId
{
  
  String props;
  
  String groupName;
  
  String url;
  
  /**
   * @param itfName
   * @param componentOwnerName
   * @param groupName the name of the group
   * @param props jgroups properties
   */
  public GroupCommExportId(String itfName, String componentOwnerName,
      String groupName, String props)
  {
    super(itfName, componentOwnerName);

    this.groupName = groupName;

    this.props = props;
  }

  public GroupCommExportId(String itfName, String componentOwnerName,
      String groupName, String props, String url)
  {
    this(itfName, componentOwnerName, groupName, props);
    this.url = url;
  }

  /**
   * 
   */
  private static final long serialVersionUID = -2798171540815869546L;

  /**
   * @see org.objectweb.fractal.bf.ExportId#getExportMode()
   */
  public String getExportMode()
  {
    return "group";
  }

  @Override
  public String toString()
  {

    return new ToStringBuilder(this).append("group name", groupName).append(
        "props", this.props).append("url", url).toString();
  }
  
  

}
