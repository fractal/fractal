
package org.objectweb.fractal.bf.connectors.group;

import java.net.MalformedURLException;
import java.util.Map;
import java.util.logging.Logger;

import org.jgroups.ChannelException;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.bf.AbstractBindingFactoryPlugin;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.BindingFactoryExportException;
import org.objectweb.fractal.bf.CannotCreateSkeletonException;
import org.objectweb.fractal.bf.proxies.StubComponentFactory;
import org.objectweb.fractal.util.Fractal;

/**
 * A group communication plug-in for the binding-factory
 */
public class GroupConnector extends AbstractBindingFactoryPlugin<GroupCommExportId>
{

  Logger  log = Logger.getLogger(GroupConnector.class.getName());

  Factory adlFactory;

  /**
   * @see org.objectweb.fractal.bf.BindingFactoryPlugin#createExportId(java.lang.String,
   *      org.objectweb.fractal.api.Component, java.util.Map)
   */
  public GroupCommExportId createExportId(String itfName, Component owner,
      Map<String, String> hints) throws BindingFactoryException
  {
    String nameOwner = null;
    try
    {
      nameOwner = Fractal.getNameController(owner).getFcName();
    }
    catch (NoSuchInterfaceException e)
    {
      throw new BindingFactoryExportException(
          "Some error occurred while creating a groupcomm/jgroups export id", e);
    }

    GroupCommExportId exportId = null;

    final String groupName = hints.get(GroupConnectorConstants.GROUP_NAME);
    final String props = hints.get(GroupConnectorConstants.PROPS);
    final String url = hints.get(GroupConnectorConstants.PROPS_URL);

    if (groupName == null)
    {
      throw new BindingFactoryException("Missing "
          + GroupConnectorConstants.GROUP_NAME + " export hint");
    }
    else
    {
      if (url != null && !url.equalsIgnoreCase(""))
      {

        exportId = new GroupCommExportId(itfName, nameOwner, groupName, props,
            url);

      }
      else if (props != null && !props.equalsIgnoreCase(""))
      {
        exportId = new GroupCommExportId(itfName, nameOwner, groupName, props);
      }

    }

    log.info("GroupComm ExportId Created: " + exportId);

    return exportId;
  }

  /**
   * @see org.objectweb.fractal.bf.BindingFactoryPlugin#createSkel(java.lang.String,
   *      java.lang.Object, org.objectweb.fractal.api.Component, java.util.Map)
   */
  public Component createSkel(String itfName, Object serverItf, Component c,
      Map<String, String> exportHints) throws CannotCreateSkeletonException
  {

    Component jgroupsSkel = null;

    try
    {

      if (adlFactory == null)
      {
        adlFactory = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
      }

      jgroupsSkel = (Component) adlFactory.newComponent(GroupServerBridge.class
          .getCanonicalName(), exportHints);

      Fractal.getLifeCycleController(jgroupsSkel).startFc();
    }
    catch (ADLException e)
    {
      throw new CannotCreateSkeletonException(
          "Cannot load skeleton ADL definition for GroupComm/JGroups plugin", e);
    }
    catch (IllegalLifeCycleException e)
    {
      throw new CannotCreateSkeletonException(
          "Cannot start the skeleton using GroupComm/JGroups plugin", e);
    }
    catch (NoSuchInterfaceException e)
    {
      log.severe("The generated skeleton has not a LC interface");
      throw new CannotCreateSkeletonException(
          "Cannot find LC interface for skeleton using GroupComm/Jgroups plugin",
          e);
    }

    return jgroupsSkel;
  }

  /**
   * @see org.objectweb.fractal.bf.BindingFactoryPlugin#finalizeSkeleton(org.objectweb.fractal.api.Component,
   *      java.util.Map)
   */
  public void finalizeSkeleton(Component skel,
      Map<String, String> finalizationHints) throws BindingFactoryException
  {

    try
    {
      final ChannelController channelController = ((ChannelController) skel
          .getFcInterface("channelCtrl"));
      channelController.finalizeConfig(finalizationHints.get("itfName"));
      channelController.connect();
    }
    catch (NoSuchInterfaceException e)
    {
      throw new BindingFactoryException(
          "Cannot find 'channelCtrl' fcInterface on the skeleton component", e);
    }
    catch (ChannelException e)
    {
      throw new BindingFactoryException(
          "Cannot finalize GroupComm/Jgroups skeleton ", e);
    }
    catch (MalformedURLException e)
    {
      throw new BindingFactoryException("Cannot connect to the JChannel", e);
    }

    log.info("GroupComm skeleton property finalized");
  }

  /**
   * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getStubComponentFactory()
   */
  public StubComponentFactory getStubComponentFactory()
  {
    return new GroupCommStubComponentFactoryImpl();
  }

  /**
   * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getStubGenerationParameters(java.util.Map)
   */
  public String[] getStubGenerationParameters(Map<String, String> bindHints)
  {
    String groupName = bindHints.get(GroupConnectorConstants.GROUP_NAME);
    String propsUrl = bindHints.get(GroupConnectorConstants.PROPS_URL);
    String[] params = new String[] {groupName,propsUrl};
    return params;
  }

}
