package org.objectweb.fractal.bf.connectors.group;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.util.logging.Logger;

import org.jgroups.ChannelException;
import org.objectweb.fractal.bf.proxies.AbstractDefaultRemoteCallInvocationHandler;

/**
 * An invocation handler..
 */
public class GroupCommInvocationHandler extends AbstractDefaultRemoteCallInvocationHandler<JGroupClientBridge> {

    Logger log = Logger.getLogger(GroupCommInvocationHandler.class.getCanonicalName());
    
    /**
     * The remote interface name
     */
    String itfName;

    /**
     * @param groupName
     * @throws ChannelException 
     * @throws MalformedURLException 
     */
    public GroupCommInvocationHandler(String groupName)
  {
   
    super(new JGroupClientBridge(groupName, null));
   
    this.itfName = groupName;

  }

    /**
     * @param groupName
     * @param props
     */
    public GroupCommInvocationHandler(String groupName, String props)
    {
      super(new JGroupClientBridge(groupName, props));
      this.itfName = groupName;
    }

    /**
     * @see java.lang.reflect.InvocationHandler#invoke(java.lang.Object,
     *      java.lang.reflect.Method, java.lang.Object[])
     */
  public Object invoke(Object proxy, Method method, Object[] args)
      throws Throwable
  {

    log
        .fine("JGroupClientBridge will send a group-rpc request to invoke method: "
            + method.getName());
    this.clientBridge.send(method.getName());// XXX method.getName()

    return null;
  }

    /**
     * @see org.objectweb.fractal.bf.proxies.AbstractDefaultRemoteCallInvocationHandler#handleMethod(java.lang.reflect.Method,
     *      java.lang.Object[], java.lang.String)
     */
    @Override
    public Object handleMethod(Method method, Object[] args, String methodName)
    {
      // TODO Auto-generated method stub
      return null;
    }

}
