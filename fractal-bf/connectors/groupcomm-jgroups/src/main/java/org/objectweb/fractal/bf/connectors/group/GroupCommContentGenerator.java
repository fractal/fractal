package org.objectweb.fractal.bf.connectors.group;

import org.objectweb.fractal.bf.proxies.AbstractDynamicProxyStubContentGenerator;

import java.lang.reflect.InvocationHandler;

/**
 *
 *
 */
public class GroupCommContentGenerator extends AbstractDynamicProxyStubContentGenerator {

    /**
     * @see org.objectweb.fractal.bf.proxies.AbstractDynamicProxyStubContentGenerator#createInvocationHandler(java.lang.String, java.lang.Class)
     */
    @Override
    protected InvocationHandler createInvocationHandler(String groupName,
                                                        Class<?> clazz) {
        return new GroupCommInvocationHandler(groupName);
    }

    /**
     * @see org.objectweb.fractal.bf.proxies.AbstractDynamicProxyStubContentGenerator#createInvocationHandler(java.lang.String[], java.lang.Class)
     */
    @Override
    protected InvocationHandler createInvocationHandler(String[] params,
        Class<?> clazz)
    {
      
      String groupName = params[0];
      String props = params[1];
      
      return new GroupCommInvocationHandler(groupName, props);
    }

}
