package org.objectweb.fractal.bf.connectors.group;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

/**
 * Test ADL definition for {@link GroupServerBridge}
 */
public class GroupServerBridgeTest extends TestCase {

	private static final String groupServerBridgeADLDef = GroupServerBridge.class
			.getCanonicalName();
	private Factory adlFactory;

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
		adlFactory = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
	}

	public void testLoadAdlDefWithMissingParameters() {

		try {
			adlFactory.newComponent(groupServerBridgeADLDef,
					new HashMap<String, String>());
			fail("Should complains about missing parameters");
		} catch (ADLException e) {
			// OK
		}

	}

	public void testLoadAdlDefWithProperParameters()
			throws NoSuchInterfaceException, IllegalLifeCycleException {

		try {
			final Map<String, String> ctx = new HashMap<String, String>();
			ctx.put(GroupConnectorConstants.GROUP_NAME, "foo");
			ctx.put(GroupConnectorConstants.PROPS_URL,
					GroupConnectorConstants.TCP_URL);
			Component groupServerBridge = (Component) adlFactory.newComponent(
					groupServerBridgeADLDef, ctx);
			assertNotNull(groupServerBridge);
		} catch (ADLException e) {
			fail(e.getMessage());
		}

	}

}
