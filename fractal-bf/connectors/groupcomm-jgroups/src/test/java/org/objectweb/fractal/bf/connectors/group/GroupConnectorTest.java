package org.objectweb.fractal.bf.connectors.group;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.CannotCreateSkeletonException;
import org.objectweb.fractal.bf.ExportId;
import org.objectweb.fractal.bf.connectors.Service;
import org.objectweb.fractal.util.Fractal;

/**
 * Test group connector.
 */
public class GroupConnectorTest extends TestCase {

	private static final String TEST_GROUP_NAME = "testGroup";

	private Component service;

	private Component client;

	private GroupConnector connector;

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
		Factory factory = FactoryFactory
				.getFactory(FactoryFactory.FRACTAL_BACKEND);
		service = (Component) factory.newComponent(
				"org.objectweb.fractal.bf.connectors.Service", null);
		assertNotNull(service);

		client = (Component) factory.newComponent(
				"org.objectweb.fractal.bf.connectors.Client", null);
		assertNotNull(client);
		connector = new GroupConnector();

	}

	/**
	 * Test method for
	 * {@link org.objectweb.fractal.bf.connectors.group.GroupConnector#createExportId(java.lang.String, org.objectweb.fractal.api.Component, java.util.Map)}.
	 */
	public void testCreateExportId() {
		Map<String, String> hints = createJGroupsExportHints();
		try {
			final ExportId exportId = connector.createExportId("service",
					service, hints);
			assertNotNull(exportId);

			GroupCommExportId gcei = (GroupCommExportId) exportId;
			assertEquals(gcei.getExportMode(), "group");
			assertEquals(gcei.groupName, TEST_GROUP_NAME);
		} catch (BindingFactoryException e) {
			fail("some error occurred while creating a jgroup exportid"
					+ e.getMessage());
		}
	}

	/**
	 * @return
	 */
	private Map<String, String> createJGroupsExportHints() {
		Map<String, String> hints = new HashMap<String, String>();
		hints.put(GroupConnectorConstants.GROUP_NAME, TEST_GROUP_NAME);
		/*
		 * props copied from
		 * http://www.jgroups.org/javagroupsnew/docs/blocks.html
		 */
		hints.put(GroupConnectorConstants.PROPS_URL,
				GroupConnectorConstants.TCP_URL);
		return hints;
	}

	/**
	 * Test method for
	 * {@link org.objectweb.fractal.bf.connectors.group.GroupConnector#createSkel(java.lang.String, java.lang.Object, org.objectweb.fractal.api.Component, java.util.Map)}.
	 */
	public void testCreateSkel() throws NoSuchInterfaceException {
		Map<String, String> hints = createJGroupsExportHints();

		try {
			final Component skel = connector.createSkel("service", service
					.getFcInterface("service"), service, hints);
			assertNotNull(skel);
			assertNotNull("Skeleton component should not be null", skel);
			assertEquals(LifeCycleController.STARTED, Fractal
					.getLifeCycleController(skel).getFcState());

			assertEquals(TEST_GROUP_NAME,
					((GroupCommunicationAttributesController) Fractal
							.getAttributeController(skel)).getName());
			assertEquals(GroupConnectorConstants.TCP_URL,
					((GroupCommunicationAttributesController) Fractal
							.getAttributeController(skel)).getUrl());
		}

		catch (CannotCreateSkeletonException e) {
			e.printStackTrace();
			fail("some error occurred while creating a a skeleton with the jgroups connector "
					+ e.getMessage());
		}

	}

	public void testCreateStub() throws IllegalLifeCycleException {
		Map<String, String> hints = createJGroupsExportHints();

		try {
			final Component stub = connector.createStub(client, "service",
					hints);

			assertNotNull(stub);

			/**
			 * BF will start the stub
			 */
			Fractal.getLifeCycleController(stub).startFc();

			Service stubServiceItf = (Service) stub.getFcInterface("service");

			stubServiceItf.print();

		} catch (BindingFactoryException e) {
			e.printStackTrace(); // To change body of catch statement use
									// File | Settings | File Templates.
			fail("Some error occured creating a groupcomm stub");
		} catch (NoSuchInterfaceException e) {
			e.printStackTrace(); // To change body of catch statement use
									// File | Settings | File Templates.
			fail("Generated stub doesn't provide appropriate Fractal interface ");
		}
	}

}
