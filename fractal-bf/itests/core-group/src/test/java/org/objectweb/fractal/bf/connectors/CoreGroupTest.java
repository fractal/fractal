
package org.objectweb.fractal.bf.connectors;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.ExportMode;
import org.objectweb.fractal.bf.connectors.group.GroupConnectorConstants;
import org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest;
import org.objectweb.fractal.util.Fractal;


public class CoreGroupTest extends AbstractBindingFactoryIntegrationTest
{

  /**
   * @see org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest#setUp()
   */
  @Override
  @Before
  public void setUp() throws Exception
  {
    super.setUp();
  }

  /**
   * @see org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest#createBindHints()
   */
  @Override
  protected Map<String, String> createBindHints()
  {
    //reuse export hints
    return createExportHints();
  }

  /**
   * @see org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest#createExportHints()
   */
  @Override
  protected Map<String, String> createExportHints()
  {
    Map<String,String> hints = new HashMap<String,String>();
    hints.put(ExportMode.EXPORT_MODE, "group");
    hints.put(GroupConnectorConstants.GROUP_NAME, "test-group");
    /*props copied from http://www.jgroups.org/javagroupsnew/docs/blocks.html */
    hints.put(GroupConnectorConstants.PROPS_URL, GroupConnectorConstants.TCP_URL);
    return hints;
  }
  
  @Test
  public void testExportAndBind() throws IllegalLifeCycleException,
      NoSuchInterfaceException, BindingFactoryException
  {
    Fractal.getLifeCycleController(enclosingService).startFc();
    bindingFactory.export(service, SERVICE, createExportHints());
    bindingFactory.bind(client, SERVICE, createBindHints());
    Fractal.getLifeCycleController(enclosingClient).startFc();
    Service groupRpcClient = (Service) client.getFcInterface("service");
    groupRpcClient.print();
  }


}
