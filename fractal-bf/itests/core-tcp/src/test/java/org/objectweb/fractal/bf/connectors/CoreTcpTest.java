package org.objectweb.fractal.bf.connectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.BindingFactoryPlugin;
import org.objectweb.fractal.bf.ExportMode;
import org.objectweb.fractal.bf.PluginNotFoundException;
import org.objectweb.fractal.bf.PluginResolver;
import org.objectweb.fractal.bf.connectors.tcp.TcpConnectorConstants;
import org.objectweb.fractal.bf.connectors.tcp.TcpExportHints;
import org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest;
import org.objectweb.fractal.util.Fractal;

/**
 * Integration tests for core and tcp module. These tests are meant to test
 * integration between the core module and the tcp-dream module.
 * 
 * @author Valerio Schiavoni <valerio.schiavoni@gmail.com>
 * 
 */
public class CoreTcpTest extends AbstractBindingFactoryIntegrationTest {

	private HashMap<String, String> exportHints;
	private TcpExportHints tcpExportHints;

	private PluginResolver pluginResolverItf;
	private final String listenPort = "1600";

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	@Before
	public void setUp() throws Exception {

		super.setUp();

		pluginResolverItf = (PluginResolver) pluginResolverComp
				.getFcInterface(PluginResolver.INTERFACE_NAME);

		tcpExportHints = new TcpExportHints();
		tcpExportHints.setListenPort(listenPort);

	}

	/**
	 * @see org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest#initializeFractalSystemProperties()
	 */
	@Override
	protected void initializeFractalSystemProperties() {
		System.setProperty("julia.config",
				"julia-dream-bundled.cfg,julia-dream-activities-bundled.cfg");
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
	}

	@Test
	public void testCreateSkeletonsForTcpConnectors() throws Exception {

		try {
			final BindingFactoryPlugin<?, ?> tcpConnectorsPlugin = pluginResolverItf
					.resolvePlugin("tcp");
			assertNotNull(tcpConnectorsPlugin);

			exportHints = new HashMap<String, String>();
			final Object fcInterface = service.getFcInterface(SERVICE);
			final Component skel = tcpConnectorsPlugin.createSkel(SERVICE,
					fcInterface, service, exportHints);

			assertNotNull(skel);

		} catch (PluginNotFoundException e) {
			e.printStackTrace();
			fail("Some error occurred while loading plugin for tcp connectors");
		}

	}

	/**
	 * This test should fails as long as client and service component are not
	 * added to the same super-component. In particular, it should be possible
	 * to export the service component, but the bind should fail with a "not a
	 * local binding" error message.
	 * 
	 * 
	 * @throws BindingFactoryException
	 * @throws NoSuchInterfaceException
	 *             if service0 is not found
	 * @throws IllegalLifeCycleException
	 */
	@Test
	public void testExportAndBind() throws BindingFactoryException,
			NoSuchInterfaceException, IllegalLifeCycleException {

		// EXPORT
		Map<String, String> exportHints = new HashMap<String, String>();
		exportHints.put(ExportMode.EXPORT_MODE, "tcp");
		exportHints.put(TcpConnectorConstants.LISTEN_PORT, "8081");
		bindingFactory.export(service, SERVICE, exportHints);
		// BIND

		Map<String, String> bindHints = new HashMap<String, String>(); // could
		// reuse
		// exportHints
		// here
		bindHints.put(ExportMode.EXPORT_MODE, "tcp");
		bindHints.put(TcpConnectorConstants.REMOTE_INTERFACE_NAME, SERVICE);
		bindHints.put(TcpConnectorConstants.LISTEN_PORT, "8081");
		bindHints.put(TcpConnectorConstants.REPLY_TO, "8080");

		bindingFactory.bind(client, SERVICE, bindHints);

		Fractal.getLifeCycleController(client).startFc();
		Fractal.getLifeCycleController(service).startFc();

		Service service0 = (Service) client.getFcInterface("service0");
		assertNotNull("Server interface on client comp should not be null",
				service0);
		String result = service0.printAndAnswer();
		result = service0.printAndAnswer();
		assertEquals(String.class.getCanonicalName(), result.getClass()
				.getCanonicalName());
		assertTrue("Result should be a string", result instanceof String);
		assertNotNull("String returned by printAndAnswer should not be null",
				result);
	}

	/**
	 * These map of hints is used in the
	 * {@link AbstractBindingFactoryIntegrationTest#testExport()} method.
	 * 
	 * @see org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest#initializeExportHints()
	 */
	@Override
	protected Map<String, String> createExportHints() {
		Map<String, String> exportHints = new HashMap<String, String>();
		exportHints.put(ExportMode.EXPORT_MODE, "tcp");
		exportHints.put(TcpConnectorConstants.LISTEN_PORT, "8081");
		return exportHints;
	}

	/**
	 * @see org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest#createBindHints()
	 */
	@Override
	protected Map<String, String> createBindHints() {
		Map<String, String> bindHints = new HashMap<String, String>(); // could
		// reuse
		// exportHints
		// here
		bindHints.put(ExportMode.EXPORT_MODE, "tcp");
		bindHints.put(TcpConnectorConstants.REMOTE_INTERFACE_NAME, SERVICE);
		bindHints.put(TcpConnectorConstants.LISTEN_PORT, "8081");
		bindHints.put(TcpConnectorConstants.REPLY_TO, "8080");
		return bindHints;
	}

}
