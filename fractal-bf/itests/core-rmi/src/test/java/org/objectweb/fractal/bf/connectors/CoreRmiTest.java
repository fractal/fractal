/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.BindingFactoryImpl;
import org.objectweb.fractal.bf.connectors.rmi.RmiConnectorConstants;
import org.objectweb.fractal.bf.connectors.rmi.RmiRegistryAttributes;
import org.objectweb.fractal.bf.connectors.rmi.RmiStubAttributes;
import org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest;
import org.objectweb.fractal.util.ContentControllerHelper;
import org.objectweb.fractal.util.Fractal;

/**
 * Integration tests for core and rmi module. These tests are meant to test a
 * correct integration between the core module and the JavaRMI module.
 */
public class CoreRmiTest extends AbstractBindingFactoryIntegrationTest {

	private Registry rmiRegistry;

	@Override
	protected String getEnclosingServiceAdlDefinition() {
		return "org.objectweb.fractal.bf.connectors.EnclosingRemotableService";
	}

	@Override
	protected String getEnclosingClientAdlDefinition() {
		return "org.objectweb.fractal.bf.connectors.EnclosingRemotableClient";
	}

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
		System.setProperty("java.security.policy",
				"src/test/resources/rmi.policy");
		// Assign security manager
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new RMISecurityManager());
		}
		rmiRegistry = LocateRegistry.createRegistry(9999);
	}

	@Override
	@After
	public void tearDown() {
		// try to clean the registry
		// clean the registry
		try {
			rmiRegistry.unbind("service");
			UnicastRemoteObject.unexportObject(rmiRegistry, true);
		} catch (Exception e) {
			// ok
		}
	}

	@Test
	public void testExportAndBindUserDefinedTypes() {
		try {
			Fractal.getLifeCycleController(enclosingService).startFc();
			bindingFactory.export(service, SERVICE, createExportHints());
			bindingFactory.bind(client, SERVICE, createBindHints());
			Fractal.getLifeCycleController(enclosingClient).startFc();
			RemotableService rmiService = (RemotableService) client
					.getFcInterface("service");

			rmiService.print();
			assertEquals(BasicServiceImpl.SERVICE_REPLY, rmiService
					.printAndAnswer());

			final Pojo pojo = rmiService.getPojo();
			assertNotNull(pojo);
			final int id = pojo.getId();
			assertEquals(2, id);
			assertThat(pojo.getName(), is("pojo-2"));
			assertThat(pojo.getParent(), is(not(nullValue())));

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause().printStackTrace();
			e.printStackTrace();
			fail();
		}

	}

	// @Test
	public void testExportSameRegistryDifferentName()
			throws IllegalLifeCycleException, NoSuchInterfaceException {
		Fractal.getLifeCycleController(enclosingService).startFc();
		try {
			final Map<String, Object> hints = createExportHints();
			bindingFactory.export(service, SERVICE, hints);

			printRegisteredServices();

			final Map<String, Object> hints2 = new HashMap<String, Object>(
					hints);

			hints2.put(RmiConnectorConstants.SERVICE_NAME, "service2");

			bindingFactory.export(service, SERVICE, hints2);
		} catch (BindingFactoryException e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void testExportDifferentComponentsSameRegistryDifferentNames()
			throws ADLException, NoSuchInterfaceException {
		// create another component
		Component enclosingService = (Component) adlFactory.newComponent(
				getEnclosingServiceAdlDefinition(), null);
		Component service2 = ContentControllerHelper.getSubComponentByName(
				enclosingService, "service");

		Fractal.getNameController(service).setFcName("s1");
		Fractal.getNameController(service2).setFcName("s2");

		final Map<String, Object> hints = createExportHints();
		try {
			bindingFactory.export(service, SERVICE, hints);
			final Map<String, Object> hints2 = new HashMap<String, Object>(
					hints);

			hints2.put(RmiConnectorConstants.SERVICE_NAME, "service2");
			bindingFactory.export(service2, SERVICE, hints2);

		} catch (BindingFactoryException e) {
			e.printStackTrace();
			fail();
		}

	}

	private void printRegisteredServices() {
		try {
			String[] ss = rmiRegistry.list();
			for (String s : ss)
				System.err.println(s);
		} catch (Exception e) {
		}
	}

	/**
	 * Related to : <a href="http://www.scorware.org/projects/SCOrWare/bugtracker/AF_WS_bindings_Handling_exceptions_"
	 * >this bug</a>
	 * 
	 * @throws RemoteException
	 * @throws NotBoundException
	 */
	@Test
	public void testUserDefinedExceptionPassesThroughBindings()
			throws IllegalLifeCycleException, NoSuchInterfaceException,
			BindingFactoryException, RemoteException, NotBoundException {
		Fractal.getLifeCycleController(enclosingService).startFc();
		bindingFactory.export(service, SERVICE, createExportHints());
		bindingFactory.bind(client, SERVICE, createBindHints());
		Fractal.getLifeCycleController(enclosingClient).startFc();
		RemotableService rmiService = (RemotableService) client
				.getFcInterface("service");
		try {
			rmiService.badMethod();
			fail();
		} catch (HelloWorldException hew) {
			// ok

		}

	}

	@Test
	public void testValuesOnAttributeControllerOfSkeleton()
			throws IllegalLifeCycleException, NoSuchInterfaceException,
			BindingFactoryException, AccessException, RemoteException,
			NotBoundException {
		Fractal.getLifeCycleController(enclosingService).startFc();
		bindingFactory.export(service, SERVICE, createExportHints());

		Component skeleton = ContentControllerHelper.getSubComponentByName(
				enclosingService, "service-rmi-skeleton");
		assertNotNull("Could not find  service-rmi-skeleton", skeleton);

		Component rmiRegistry = ContentControllerHelper
				.getSubComponentByName(skeleton,
						"org.objectweb.fractal.bf.connectors.rmi.RmiRegistry");

		AttributeController ac = Fractal.getAttributeController(rmiRegistry);
		RmiRegistryAttributes rmiac = (RmiRegistryAttributes) ac;
		assertEquals(rmiac.getHost(),
				RmiConnectorConstants.JAVA_RMI_REGISTRY_HOST_ADDRESS_DEFAULT);
		assertEquals(rmiac.getPort(), 9999);

	}

	/**
	 * @see org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest#initializeExportHints()
	 */
	@Override
	protected Map<String, Object> createExportHints() {
		Map<String, Object> exportHints = new HashMap<String, Object>();
		exportHints.put(BindingFactoryImpl.PLUGIN_ID, "rmi");
		exportHints.put(RmiConnectorConstants.SERVICE_NAME, "service");
		exportHints.put(RmiConnectorConstants.JAVA_RMI_REGISTRY_HOST_ADDRESS,
				RmiConnectorConstants.JAVA_RMI_REGISTRY_HOST_ADDRESS_DEFAULT);
		exportHints.put(RmiConnectorConstants.JAVA_RMI_REGISTRY_PORT, "9999");
		exportHints.put(BindingFactoryImpl.CLASS_LOADER, Thread.currentThread()
				.getContextClassLoader());
		return exportHints;
	}

	/**
	 * test values on attribute controller over stub
	 * 
	 * @throws NoSuchInterfaceException
	 * @throws IllegalLifeCycleException
	 * @throws BindingFactoryException
	 */
	@Test
	public void testStubAC() throws IllegalLifeCycleException,
			NoSuchInterfaceException, BindingFactoryException {
		Fractal.getLifeCycleController(enclosingService).startFc();
		bindingFactory.export(service, SERVICE, createExportHints());
		final Map<String, Object> bindHints = createBindHints();
		bindingFactory.bind(client, SERVICE, bindHints);
		Fractal.getLifeCycleController(enclosingClient).startFc();

		Component stub = ContentControllerHelper.getSubComponentByName(
				enclosingClient, "client-rmi-stub");
		assertNotNull(stub);

		Component primitive = ContentControllerHelper.getSubComponentByName(
				stub, "rmi-stub-primitive");

		RmiStubAttributes stubAC = (RmiStubAttributes) Fractal
				.getAttributeController(primitive);

		assertEquals(bindHints.get("serviceName"), stubAC.getServiceName());

	}

	/**
	 * @see org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest#createBindHints()
	 */
	@Override
	protected Map<String, Object> createBindHints() {
		Map<String, Object> bindHints = new HashMap<String, Object>();
		bindHints.put(BindingFactoryImpl.PLUGIN_ID, "rmi");
		bindHints.put(RmiConnectorConstants.SERVICE_NAME, "service");
		bindHints.put(RmiConnectorConstants.JAVA_RMI_REGISTRY_HOST_ADDRESS,
				RmiConnectorConstants.JAVA_RMI_REGISTRY_HOST_ADDRESS_DEFAULT);
		bindHints.put(RmiConnectorConstants.JAVA_RMI_REGISTRY_PORT, "9999");
		bindHints.put(BindingFactoryImpl.CLASS_LOADER, Thread.currentThread()
				.getContextClassLoader());
		return bindHints;
	}

	@Override
	public void testBind() {
		try {
			rmiRegistry.bind("service", new BasicRemotableServiceImpl());
			super.testBind();
		} catch (Exception e) {
			fail();
		}
	}

}
