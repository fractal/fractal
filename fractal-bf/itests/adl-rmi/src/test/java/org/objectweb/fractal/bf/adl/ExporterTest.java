package org.objectweb.fractal.bf.adl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;

public class ExporterTest {

	String RMI_EXPORTER = "org.objectweb.fractal.bf.adl.ServiceWithRmiExporter";

	protected Factory adlFactory;

	private static Registry rmiRegistry;
	static {
		try {
			rmiRegistry = LocateRegistry.createRegistry(9919);
		} catch (RemoteException e) {
			e.printStackTrace();
			fail();
		}
	}

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Before
	public void setUp() throws Exception {
		System.setProperty("java.security.policy",
				"src/test/resources/rmi.policy");
		// Assign security manager
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new RMISecurityManager());
		}
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
		adlFactory = FactoryFactory.getFactory(
				"org.objectweb.fractal.bf.adl.MyBasicFactory",
				"org.objectweb.fractal.bf.adl.MyFractalBackend",
				new HashMap<Object, Object>());

		assertNotNull(adlFactory);
	}

	@Test
	public void testRmiExporter() {
		try {
			Component c = loadComponent(RMI_EXPORTER, null);
			assertNotNull(c);

		} catch (ADLException e) {
			e.printStackTrace();
			fail("Some error occurred while loading definition at "
					+ RMI_EXPORTER);
		}

	}

	/**
	 * @param adl
	 *            the adl definition
	 * @param ctx
	 *            context map to load adl definitions
	 * @return
	 * @throws ADLException
	 */
	private Component loadComponent(String adl, Map<String, String> ctx)
			throws ADLException {
		Component c = null;
		try {
			c = (Component) adlFactory.newComponent(adl, ctx);
		} catch (NullPointerException npe) {
			npe.printStackTrace();
		}
		return c;
	}
}