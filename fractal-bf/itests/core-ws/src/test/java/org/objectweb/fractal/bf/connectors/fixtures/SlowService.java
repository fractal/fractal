/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.fixtures;

import org.objectweb.fractal.bf.connectors.BasicServiceImpl;

/**
 * A fixture class to test reconfiguration of stubs.
 */
public class SlowService extends BasicServiceImpl {

	private static final long serialVersionUID = 7754576364948833405L;

	/**
	 * @see org.objectweb.fractal.bf.connectors.BasicServiceImpl#printAndAnswer()
	 */
	@Override
	public String printAndAnswer() {
		return "This is a very SLOW web service :-(";
	}
}
