/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.cxf.aegis.databinding.AegisDatabinding;
import org.apache.cxf.frontend.ClientProxyFactoryBean;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.BindingFactoryImpl;
import org.objectweb.fractal.bf.connectors.ws.WsConnectorConstants;
import org.objectweb.fractal.bf.connectors.ws.WsSkeletonAttributes;
import org.objectweb.fractal.bf.connectors.ws.WsStubAttributes;
import org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest;
import org.objectweb.fractal.util.ContentControllerHelper;
import org.objectweb.fractal.util.Fractal;

/**
 * Integration tests for core and ws module. These tests are meant to test a
 * correct integration between the core module and the WS module.
 */
public class CoreWsTest extends AbstractBindingFactoryIntegrationTest {

	Logger log = Logger.getLogger(CoreWsTest.class.getCanonicalName());

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@Test
	public void testExportAndInvokeWithExternalClient()
			throws IllegalLifeCycleException, NoSuchInterfaceException,
			BindingFactoryException {
		Fractal.getLifeCycleController(enclosingService).startFc();
		Fractal.getLifeCycleController(service).startFc();

		bindingFactory.export(service, SERVICE, createExportHints());

		ClientProxyFactoryBean factory = new ClientProxyFactoryBean();
		AegisDatabinding aDB = new AegisDatabinding();
		factory.getServiceFactory().setDataBinding(aDB);
		factory.setServiceClass(Service.class);
		factory.setAddress(WsConnectorConstants.DEFAULT_URI);
		Service client = (Service) factory.create();
		String result = client.printAndAnswer();
		checkResult(result);
		Pojo p = client.getPojo();
		checkPojo(p);

		Pojo childOfP = client.child(p);

		log.fine("Child pojo of " + p.toString() + " returned by WS: "
				+ childOfP.toString());
	}

	/**
	 * @param result
	 *            the result sent back from the service
	 */
	private void checkResult(String result) {
		assertEquals(BasicServiceImpl.SERVICE_REPLY, result);
	}

	@Test
	public void testExportAndBind() throws IllegalLifeCycleException,
			NoSuchInterfaceException, BindingFactoryException {
		Fractal.getLifeCycleController(enclosingService).startFc();
		bindingFactory.export(service, SERVICE, createExportHints());
		bindingFactory.bind(client, SERVICE, createBindHints());
		Fractal.getLifeCycleController(enclosingClient).startFc();
		Service ws = (Service) client.getFcInterface("service");
		String fromWebService = ws.printAndAnswer();
		checkResult(fromWebService);

	}

	@Test
	public void testExportAndBindNonPrimitiveTypes()
			throws IllegalLifeCycleException, NoSuchInterfaceException,
			BindingFactoryException {
		Fractal.getLifeCycleController(enclosingService).startFc();
		bindingFactory.export(service, SERVICE, createExportHints());
		bindingFactory.bind(client, SERVICE, createBindHints());
		Fractal.getLifeCycleController(enclosingClient).startFc();
		Service ws = (Service) client.getFcInterface("service");
		final XMLGregorianCalendar currentDate = ws.getCurrentDate();
		assertNotNull(currentDate);
	}

	@Test
	public void testExportAndBindUserDefinedTypes()
			throws IllegalLifeCycleException, NoSuchInterfaceException,
			BindingFactoryException {
		Fractal.getLifeCycleController(enclosingService).startFc();
		bindingFactory.export(service, SERVICE, createExportHints());
		bindingFactory.bind(client, SERVICE, createBindHints());
		Fractal.getLifeCycleController(enclosingClient).startFc();
		Service ws = (Service) client.getFcInterface("service");
		final Pojo pojo = ws.getPojo();
		checkPojo(pojo);
		Pojo child = ws.child(pojo);
		assertNotNull(
				"Child pojo returned by ws.child(pojo) should not be null",
				child);
		log.fine("Child pojo of " + pojo.toString() + " returned by WS: "
				+ child.toString());
		assertNotNull("The parent pojo of: " + pojo
				+ " returned by web-service was null", child);

	}

	@Test
	public void testExportAndBindWithPropertiesAndRawBytes()
			throws IllegalLifeCycleException, NoSuchInterfaceException,
			BindingFactoryException {
		Fractal.getLifeCycleController(enclosingService).startFc();
		bindingFactory.export(service, SERVICE, createExportHints());
		bindingFactory.bind(client, SERVICE, createBindHints());
		Fractal.getLifeCycleController(enclosingClient).startFc();
		Service ws = (Service) client.getFcInterface("service");
		Properties someProos = new Properties();
		someProos.put("key-1", "value-1");
		someProos.put("key-2", "value-2");

		final byte[] elaboratedBytes = ws.elaborateBytes(someProos,
				CoreWsTest.class.getCanonicalName().getBytes());
		assertNotNull(elaboratedBytes);
	}

	/**
	 * Related to : <a href="http://www.scorware.org/projects/SCOrWare/bugtracker/AF_WS_bindings_Handling_exceptions_"
	 * >this bug</a>
	 */
	@Test
	public void testUserDefinedExceptionPassesThroughBindings()
			throws IllegalLifeCycleException, NoSuchInterfaceException,
			BindingFactoryException {
		Fractal.getLifeCycleController(enclosingService).startFc();
		bindingFactory.export(service, SERVICE, createExportHints());
		bindingFactory.bind(client, SERVICE, createBindHints());
		Fractal.getLifeCycleController(enclosingClient).startFc();
		Service ws = (Service) client.getFcInterface("service");
		try {
			ws.badMethod();
			fail();
		} catch (HelloWorldException hew) {
			// ok
			hew.printStackTrace();
		}
	}

	@Test
	public void testSkeletonName() throws IllegalLifeCycleException,
			NoSuchInterfaceException, BindingFactoryException {
		Fractal.getLifeCycleController(enclosingService).startFc();
		bindingFactory.export(service, SERVICE, createExportHints());

		Component skeleton = ContentControllerHelper.getSubComponentByName(
				enclosingService, "service-ws-skeleton");
		assertNotNull("Could not find  service-ws-skeleton", skeleton);
	}

	@Test
	public void testStubName() throws IllegalLifeCycleException,
			NoSuchInterfaceException, BindingFactoryException {
		bindingFactory.bind(client, SERVICE, createBindHints());
		Fractal.getLifeCycleController(enclosingClient).startFc();

		Component stub = ContentControllerHelper.getSubComponentByName(
				enclosingClient, "client-ws-stub");
		assertNotNull("Could not find  'client-ws-stub' component", stub);
	}

	@Test
	public void testACStub() throws IllegalLifeCycleException,
			NoSuchInterfaceException, BindingFactoryException {
		bindingFactory.bind(client, SERVICE, createBindHints());
		Fractal.getLifeCycleController(enclosingClient).startFc();

		Component stub = ContentControllerHelper.getSubComponentByName(
				enclosingClient, "client-ws-stub");

		AttributeController ac = Fractal.getAttributeController(stub);
		WsStubAttributes wsac = (WsStubAttributes) ac;

		assertEquals("The ws uri on the stub's is not the expected one",
				WsConnectorConstants.DEFAULT_URI, wsac.getUri());
	}

	@Test
	public void testACSkeleton() throws IllegalLifeCycleException,
			NoSuchInterfaceException, BindingFactoryException {
		Fractal.getLifeCycleController(enclosingService).startFc();
		bindingFactory.export(service, SERVICE, createExportHints());

		Component skeleton = ContentControllerHelper.getSubComponentByName(
				enclosingService, "service-ws-skeleton");
		assertNotNull("Could not find  service-ws-skeleton", skeleton);

		AttributeController ac = Fractal.getAttributeController(skeleton);
		WsSkeletonAttributes wsac = (WsSkeletonAttributes) ac;
		assertEquals(
				"The ws uri on the AC of skeleton is not the expected one",
				WsConnectorConstants.DEFAULT_URI, wsac.getUri());
	}

	@Override
	@After
	public void tearDown() {
		try {
			Fractal.getLifeCycleController(enclosingService).stopFc();
		} catch (IllegalLifeCycleException e) {
			fail();
		} catch (NoSuchInterfaceException e) {
			fail();
		}
	}

	/**
	 * @param pojo
	 */
	private void checkPojo(final Pojo pojo) {
		assertNotNull(pojo);
		assertThat(Integer.valueOf(pojo.getId()), is(not(nullValue())));
		assertThat(Integer.valueOf(pojo.getId()), is(Integer.valueOf(2)));
		assertThat(pojo.getName(), is("pojo-2"));
		assertThat(pojo.getParent(), is(not(nullValue())));
	}

	/**
	 * @see org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest#initializeExportHints()
	 */
	@Override
	protected Map<String, Object> createExportHints() {
		Map<String, Object> exportHints = new HashMap<String, Object>();
		exportHints.put(BindingFactoryImpl.PLUGIN_ID, "ws");
		exportHints.put(WsConnectorConstants.URI,
				WsConnectorConstants.DEFAULT_URI);

		return exportHints;
	}

	/**
	 * @see org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest#createBindHints()
	 */
	@Override
	protected Map<String, Object> createBindHints() {
		Map<String, Object> bindhints = new HashMap<String, Object>();
		bindhints.put(BindingFactoryImpl.PLUGIN_ID, "ws");
		bindhints.put(WsConnectorConstants.URI,
				WsConnectorConstants.DEFAULT_URI);

		return bindhints;
	}
}
