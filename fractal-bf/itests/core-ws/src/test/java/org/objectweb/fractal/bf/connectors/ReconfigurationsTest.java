/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.apache.cxf.aegis.AegisContext;
import org.apache.cxf.aegis.databinding.AegisDatabinding;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.frontend.ClientProxyFactoryBean;
import org.apache.cxf.frontend.ServerFactoryBean;
import org.apache.cxf.service.invoker.BeanInvoker;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.BindingFactoryImpl;
import org.objectweb.fractal.bf.connectors.fixtures.FastService;
import org.objectweb.fractal.bf.connectors.fixtures.SlowService;
import org.objectweb.fractal.bf.connectors.ws.WsConnectorConstants;
import org.objectweb.fractal.bf.connectors.ws.WsSkeletonAttributes;
import org.objectweb.fractal.bf.connectors.ws.WsStubAttributes;
import org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest;
import org.objectweb.fractal.util.ContentControllerHelper;
import org.objectweb.fractal.util.Fractal;

/**
 * Test skeleton and stub reconfigurations.
 */
public class ReconfigurationsTest extends AbstractBindingFactoryIntegrationTest {

	private static final String FAST_SERVICE_URI = "http://localhost:8080/FastService";

	private static final String SLOW_SERVICE_URI = "http://localhost:8080/SlowService";

	BindingFactoryClassLoader cl;

	/**
	 * @throws java.lang.Exception
	 */
	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
		cl = new BindingFactoryClassLoader(Thread.currentThread()
				.getContextClassLoader());

		Thread.currentThread().setContextClassLoader(cl);
		cl.loadClass(Service.class.getCanonicalName());
		cl.loadClass(SlowService.class.getCanonicalName());
		cl.loadClass(FastService.class.getCanonicalName());
	}

	@Test
	public void testReconfigureSkeletonURI() throws IllegalLifeCycleException,
			NoSuchInterfaceException, BindingFactoryException {
		Fractal.getLifeCycleController(enclosingService).startFc();
		bindingFactory.export(service, SERVICE, createExportHints());
		Fractal.getLifeCycleController(enclosingService).startFc();

		/*
		 * invoke the service before reconfiguration
		 */

		ClientProxyFactoryBean factory = new ClientProxyFactoryBean();
		AegisDatabinding aDB = new AegisDatabinding();
		factory.getServiceFactory().setDataBinding(aDB);
		factory.setServiceClass(Service.class);
		factory.setAddress(WsConnectorConstants.DEFAULT_URI);
		Service client = (Service) factory.create();
		// test the service before reconfiguration
		String result = client.printAndAnswer();

		assertNotNull(result);

		Component skeleton = ContentControllerHelper.getSubComponentByName(
				enclosingService, "service-ws-skeleton");

		final LifeCycleController skelLC = Fractal
				.getLifeCycleController(skeleton);
		skelLC.stopFc();

		assertEquals(LifeCycleController.STOPPED, skelLC.getFcState());
		WsSkeletonAttributes wsac = (WsSkeletonAttributes) Fractal
				.getAttributeController(skeleton);
		wsac.setUri(WsConnectorConstants.DEFAULT_URI + "Reconfigured");

		skelLC.startFc();

		/*
		 * reinvoke it after the reconfiguration, should fails, a new client
		 * must be done
		 */
		try {
			client.printAndAnswer();
			fail("The old client can't work, if it does the old service has not been shut down");
		} catch (Exception e) {
			// ok, need to be reconfigured
			System.err.println("Old client could not contact service at URI "
					+ WsConnectorConstants.DEFAULT_URI
					+ ", need to be reconfigured");
		}

		ClientProxyFactoryBean factoryForNewAddress = new ClientProxyFactoryBean();

		factoryForNewAddress.getServiceFactory().setDataBinding(aDB);
		factoryForNewAddress.setServiceClass(Service.class);
		factoryForNewAddress.setAddress(WsConnectorConstants.DEFAULT_URI
				+ "Reconfigured");
		Service clientForReconfigured = (Service) factoryForNewAddress.create();
		String resultFromReconfiguredClient = clientForReconfigured
				.printAndAnswer();
		assertNotNull(resultFromReconfiguredClient);

	}

	@Test
	public void testReconfigureStub() throws BindingFactoryException,
			IllegalLifeCycleException, NoSuchInterfaceException {
		// these act as external web services
		Server fast = startFastService();

		Server slow = startSlowService();

		bindingFactory.bind(client, SERVICE, createBindHintsForSlowService());
		Fractal.getLifeCycleController(enclosingClient).startFc();
		Service ws = (Service) client.getFcInterface("service");
		String fromWebService = ws.printAndAnswer();
		System.err.println("Client received: " + fromWebService);

		Component stub = ContentControllerHelper.getSubComponentByName(
				enclosingClient, "client-ws-stub");
		assertNotNull("Could not find stub", stub);

		/*
		 * the stopFc impl does nothing for the moment..
		 */
		Fractal.getLifeCycleController(stub).stopFc();
		/*
		 * Let's reconfigure the stub to use the much faster service...
		 */
		WsStubAttributes wsacc = (WsStubAttributes) Fractal
				.getAttributeController(stub);

		wsacc.setUri(FAST_SERVICE_URI);
		/*
		 * now lest restart it and check
		 */
		Fractal.getLifeCycleController(stub).startFc();

		String fromWebServiceAfterReconf = ws.printAndAnswer();
		System.err.println("After reconfiguration, client received: "
				+ fromWebServiceAfterReconf);

		// sto them to avoid conflicts with other tests
		fast.stop();
		slow.stop();
	}

	protected Map<String, Object> createBindHintsForSlowService() {
		Map<String, Object> bindHints = new HashMap<String, Object>();
		bindHints.put(BindingFactoryImpl.PLUGIN_ID, "ws");
		bindHints.put(WsConnectorConstants.URI, SLOW_SERVICE_URI);
		bindHints.put(BindingFactoryImpl.CLASS_LOADER, cl);
		return bindHints;
	}

	private Server startFastService() {
		ServerFactoryBean wsServerFactoryBean = new ServerFactoryBean();
		AegisDatabinding aDB = new AegisDatabinding();
		final AegisContext aegisContext = new AegisContext();
		aegisContext.setWriteXsiTypes(true);
		aDB.setAegisContext(aegisContext);
		wsServerFactoryBean.getServiceFactory().setDataBinding(aDB);
		wsServerFactoryBean.setAddress(FAST_SERVICE_URI);
		wsServerFactoryBean.setServiceClass(Service.class);
		BeanInvoker invoker = new BeanInvoker(new FastService());
		wsServerFactoryBean.setInvoker(invoker);
		return wsServerFactoryBean.create();
	}

	private Server startSlowService() {
		ServerFactoryBean wsServerFactoryBean = new ServerFactoryBean();
		AegisDatabinding aDB = new AegisDatabinding();
		final AegisContext aegisContext = new AegisContext();
		aegisContext.setWriteXsiTypes(true);
		aDB.setAegisContext(aegisContext);
		wsServerFactoryBean.getServiceFactory().setDataBinding(aDB);
		wsServerFactoryBean.setAddress(SLOW_SERVICE_URI);
		wsServerFactoryBean.setServiceClass(Service.class);
		final SlowService slowService = new SlowService();
		BeanInvoker invoker = new BeanInvoker(slowService);
		wsServerFactoryBean.setInvoker(invoker);
		return wsServerFactoryBean.create();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Override
	@After
	public void tearDown() throws Exception {
		Fractal.getLifeCycleController(service).stopFc();
		Fractal.getLifeCycleController(enclosingService).stopFc();

	}

	/**
	 * @see org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest#initializeExportHints()
	 */
	@Override
	protected Map<String, Object> createExportHints() {
		Map<String, Object> exportHints = new HashMap<String, Object>();
		exportHints.put(BindingFactoryImpl.PLUGIN_ID, "ws");
		exportHints.put(WsConnectorConstants.URI,
				WsConnectorConstants.DEFAULT_URI);

		exportHints.put(BindingFactoryImpl.CLASS_LOADER, cl);

		return exportHints;
	}

	/**
	 * @see org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest#createBindHints()
	 */
	@Override
	protected Map<String, Object> createBindHints() {
		Map<String, Object> bindHints = new HashMap<String, Object>();
		bindHints.put(BindingFactoryImpl.PLUGIN_ID, "ws");
		bindHints.put(WsConnectorConstants.URI,
				WsConnectorConstants.DEFAULT_URI);
		bindHints.put(BindingFactoryImpl.CLASS_LOADER, cl);
		return bindHints;
	}

}
