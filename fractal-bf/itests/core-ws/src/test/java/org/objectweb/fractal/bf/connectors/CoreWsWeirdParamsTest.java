/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors;

import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.BindingFactoryImpl;
import org.objectweb.fractal.bf.connectors.weird.ServiceWithWeirdParams;
import org.objectweb.fractal.bf.connectors.ws.WsConnectorConstants;
import org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest;
import org.objectweb.fractal.util.Fractal;

/**
 * Integration tests for core and ws module. These tests are meant to test a
 * correct integration between the core module and the WS module.
 */
public class CoreWsWeirdParamsTest extends
		AbstractBindingFactoryIntegrationTest {

	/**
	 * The uri used in these tests.
	 */
	private static final String WS_SERVICE_URL = "http://localhost:8080/Service";

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@Test
	public void testExportAndBindUsingBigMaps()
			throws IllegalLifeCycleException, NoSuchInterfaceException,
			BindingFactoryException {
		Fractal.getLifeCycleController(enclosingService).startFc();

		bindingFactory.export(service, "weirdService", createExportHints());

		bindingFactory.bind(client, "weirdService", createBindHints());
		Fractal.getLifeCycleController(enclosingClient).startFc();
		ServiceWithWeirdParams ws = (ServiceWithWeirdParams) client
				.getFcInterface("weirdService");

		// friends
		String n1 = "valerio";
		String n2 = "puccio";
		Map<String, String> friends = new HashMap<String, String>();
		friends.put(n1, "123");
		friends.put(n2, "345");
		// parents
		String n3 = "mom";
		String n4 = "dad";
		Map<String, String> parents = new HashMap<String, String>();

		parents.put(n3, "4353");
		parents.put(n4, "546456");

		Map<String, Map<String, String>> bigParam = new HashMap<String, Map<String, String>>();
		bigParam.put("friends", friends);
		bigParam.put("parents", parents);

		// send the big map
		ws.mapOfMap(bigParam);

		Pojo p1 = new Pojo();
		p1.setName("i am a pojo");
		p1.setId(666);

		Map<String, Map<String, Pojo>> mapWithMapOfStringAndPojo = new HashMap<String, Map<String, Pojo>>();
		Map<String, Pojo> nameAndPojo = new HashMap<String, Pojo>();
		nameAndPojo.put("firstPojo", p1);
		mapWithMapOfStringAndPojo.put("keyForFirstPojo", nameAndPojo);
		ws.mapOfMapWithStringAndPojo(mapWithMapOfStringAndPojo);
	}

	/**
	 * @see org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest#initializeExportHints()
	 */
	@Override
	protected Map<String, Object> createExportHints() {
		Map<String, Object> exportHints = new HashMap<String, Object>();
		exportHints.put(BindingFactoryImpl.PLUGIN_ID, "ws");
		exportHints.put(WsConnectorConstants.URI, WS_SERVICE_URL);

		return exportHints;
	}

	/**
	 * @see org.objectweb.fractal.bf.testing.AbstractBindingFactoryIntegrationTest#createBindHints()
	 */
	@Override
	protected Map<String, Object> createBindHints() {
		Map<String, Object> bindHints = new HashMap<String, Object>();
		bindHints.put(BindingFactoryImpl.PLUGIN_ID, "ws");
		bindHints.put(WsConnectorConstants.URI, WS_SERVICE_URL);

		return bindHints;
	}

	@Override
	@After
	public void tearDown() {
		try {
			Fractal.getLifeCycleController(enclosingService).stopFc();
		} catch (IllegalLifeCycleException e) {
			fail();
		} catch (NoSuchInterfaceException e) {
			fail();
		}
	}

}
