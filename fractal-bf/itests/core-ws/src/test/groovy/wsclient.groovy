import groovy.net.soap.SoapClient

def proxy = new SoapClient("http://localhost:8080/Service?wsdl")

result = proxy.printAndAnswer()
println(result)

date = proxy.getCurrentDate()
println(date)
