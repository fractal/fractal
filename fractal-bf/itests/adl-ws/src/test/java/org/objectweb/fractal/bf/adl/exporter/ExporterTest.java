
package org.objectweb.fractal.bf.adl.exporter;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;

import static org.junit.Assert.*;

public class ExporterTest
{

  String SOAP_EXPORTER  = "org.objectweb.fractal.bf.adl.ServiceWithSoapExporter";
  protected Factory adlFactory;

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Before
	public void setUp() throws Exception {
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
		adlFactory = FactoryFactory.getFactory(
				"org.objectweb.fractal.bf.adl.MyBasicFactory",
				"org.objectweb.fractal.bf.adl.MyFractalBackend",
				new HashMap<Object, Object>());

		assertNotNull(adlFactory);
	}


  @Test
  public void testSoapExporter()
  {
    try
    {
      Component c = loadComponent(SOAP_EXPORTER, null);
      assertNotNull(c);
    }

    catch (ADLException e)
    {
      e.printStackTrace();
      fail("Some error occurred while loading definition at " + SOAP_EXPORTER);
    }

  }

  

  /**
   * @param adl the adl definition
   * @param ctx context map to load adl definitions
   * @return
   * @throws ADLException
   */
  private Component loadComponent(String adl, Map<String, String> ctx)
      throws ADLException
  {
    Component c = null;
    try
    {
      c = (Component) adlFactory.newComponent(adl, ctx);
    }
    catch (NullPointerException npe)
    {
      npe.printStackTrace();
    }
    return c;
  }
}