
package org.objectweb.fractal.bf.adl.binder;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.adl.util.ContentControllerHelper;
import org.objectweb.fractal.adl.util.NoSuchComponentException;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.util.Fractal;

import static org.junit.Assert.*;

public class BinderTest
{

  String BINDER = "org.objectweb.fractal.bf.adl.ClientWithSoapBinder";
  protected Factory adlFactory;
	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Before
	public void setUp() throws Exception {
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
		adlFactory = FactoryFactory.getFactory(
				"org.objectweb.fractal.bf.adl.MyBasicFactory",
				"org.objectweb.fractal.bf.adl.MyFractalBackend",
				new HashMap<Object, Object>());

		assertNotNull(adlFactory);
	}
  
  @Test
  public void testBinder() throws NoSuchInterfaceException
  {
    try
    {
      Component c = (Component) adlFactory.newComponent(BINDER, null);
      assertNotNull(c);

      Component client = ContentControllerHelper.getSubComponentByName(c, "client");

      Object serverItf = Fractal.getBindingController(client).lookupFc(
          "service");
      assertNotNull("Client interface not bound", serverItf);
    }
    catch (ADLException e)
    {
      e.printStackTrace();
      fail("Some error occurred while loading definition at " + BINDER);
    }
    catch (NoSuchComponentException e)
    {
      fail("Can't find client subcomponent");
    }
  }

}