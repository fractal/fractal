package org.objectweb.fractal.bf.adl.exporter;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;

public class ExporterTest  {


	String GROUP_EXPORTER = "org.objectweb.fractal.bf.adl.ServiceWithGroupExporter";

	protected Factory adlFactory;

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Before
	public void setUp() throws Exception {
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
		adlFactory = FactoryFactory.getFactory(
				"org.objectweb.fractal.bf.adl.MyBasicFactory",
				"org.objectweb.fractal.bf.adl.MyFractalBackend",
				new HashMap<Object, Object>());

		assertNotNull(adlFactory);
	}

	@Test
	public void testGroupExporter() {
		try {
			Component c = loadComponent(GROUP_EXPORTER, null);
			assertNotNull(c);

		} catch (ADLException e) {
			e.printStackTrace();
			fail("Some error occurred while loading definition at "
					+ GROUP_EXPORTER);
		}

	}

	/**
	 * @param adl
	 *            the adl definition
	 * @param ctx
	 *            context map to load adl definitions
	 * @return
	 * @throws ADLException
	 */
	private Component loadComponent(String adl, Map<String, String> ctx)
			throws ADLException {
		Component c = null;
		try {
			c = (Component) adlFactory.newComponent(adl, ctx);
		} catch (NullPointerException npe) {
			npe.printStackTrace();
		}
		return c;
	}
}