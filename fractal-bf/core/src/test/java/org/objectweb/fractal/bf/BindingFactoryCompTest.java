/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf;

import java.util.HashMap;

import junit.framework.TestCase;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.util.Fractal;

/**
 * Test the ADL configuration of the BindingFactoryComp component.
 * 
 */
public class BindingFactoryCompTest extends TestCase {

	private static final String BINDING_FACTORY_COMP = "org.objectweb.fractal.bf.BindingFactoryComp";

	BindingFactory bindingFactory;

	Component toExport;

	protected Component bindingFactoryComp;

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");

		Factory adlFactory = FactoryFactory
				.getFactory(FactoryFactory.FRACTAL_BACKEND);

		bindingFactoryComp = (Component) adlFactory.newComponent(
				BINDING_FACTORY_COMP, new HashMap<Object, Object>());
		Fractal.getLifeCycleController(bindingFactoryComp).startFc();

		assertNotNull(bindingFactoryComp);

	}

	public void testGetBindingFactoryInterface() {

		try {
			this.bindingFactory = (BindingFactory) bindingFactoryComp
					.getFcInterface("binding-factory");
			assertNotNull(bindingFactoryComp);
		} catch (NoSuchInterfaceException e) {
			fail("Could not find interface named 'binding-factory'");
		}

	}

}
