/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.api.NoSuchInterfaceException;

/**
 * Test plugin resolver ability to dynamically add plugins as late as the first
 * user requests it.
 */
public class PluginResolverTest {

	private PluginResolverImpl pluginResolver;

	@BeforeClass
	public static void setupSystemProperties() {
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() {
		this.pluginResolver = new PluginResolverImpl();
	}

	@Test(expected = NoSuchInterfaceException.class)
	public void lookUpFc() throws NoSuchInterfaceException {
		pluginResolver.lookupFc("mock");
	}

	@Test
	public void resolveNonExistantPlugin() {
		try {
			this.pluginResolver
					.resolvePlugin(PluginResolverImpl.PLUGINS_BINDING
							+ "impossibleToFindPlugin");
			fail("should throws exception");

		} catch (PluginNotFoundException e) {
			// e.printStackTrace();
			// ok
		}
	}

	@Test
	public void dynamicallyLoadPlugin() {
		try {
			this.pluginResolver.lookupFc("donothing");
			fail("should throw an exception");
		} catch (NoSuchInterfaceException e) {
			// ok, plugin still not present
		}
		try {
			final BindingFactoryPlugin<?, ?> donothingPlugin = pluginResolver
					.resolvePlugin(PluginResolverImpl.PLUGINS_BINDING
							+ "donothing");
			assertThat(donothingPlugin, is(not(nullValue())));
		} catch (PluginNotFoundException pnfr) {
			fail("The plugin should have been loaded dynamically");
		}
	}

	@Test
	public void testCapitalize() {
		String fooCapilitazed = this.pluginResolver.capitalize("foo");
		assertEquals("Foo", fooCapilitazed);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() {
		this.pluginResolver = null;
	}

}
