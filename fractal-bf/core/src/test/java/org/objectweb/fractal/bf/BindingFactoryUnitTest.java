/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf;

import junit.framework.TestCase;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.BindingController;

/**
 * Test the BindingFactory content class
 */
public class BindingFactoryUnitTest extends TestCase {

	protected BindingFactory bindingFactory;

	Component toExport;

	/**
	 * Load BindingFactory component
	 */
	@Override
	public void setUp() throws Exception {

		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");

		Factory adlFactory = FactoryFactory
				.getFactory(FactoryFactory.FRACTAL_BACKEND);

		this.bindingFactory = new BindingFactoryImpl();

		Component pluginResolver = (Component) adlFactory.newComponent(
				"org.objectweb.fractal.bf.PluginResolver", null);
		assertNotNull(pluginResolver);

		((BindingController) bindingFactory).bindFc(
				PluginResolver.INTERFACE_NAME, new PluginResolverImpl());

		assertNotNull(this.bindingFactory);

		toExport = (Component) adlFactory.newComponent(
				"org.objectweb.fractal.bf.connectors.Service", null);

		assertNotNull(toExport);
	}

	public void testThrowsBindingFactoryExceptionIfComponentToExportIsNull() {
		try {
			bindingFactory.export(null, "fake", null);
			fail("BindingFactoryException should have been thrown, as the component to export is null");
		} catch (BindingFactoryException e) {
			// ok
		}
	}

	public void testThrowsBindingFactoryExceptionIfNameOfInterfaceToExportIsNotFound() {

		try {
			bindingFactory.export(toExport, "interface-not-present", null);
			fail("Should throw BindingFactoryException when the interface to export cannot be found on the component to export");
		} catch (BindingFactoryException e) {
			// ok
		}

	}
}
