/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.connectors.donothing;

import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.BindingFactoryPlugin;
import org.objectweb.fractal.bf.SkeletonGenerationException;
import org.objectweb.fractal.bf.StubGenerationException;

/**
 * a do-nothing connector for testing purpose
 */
public class DonothingConnector implements
		BindingFactoryPlugin<DonothingExportHints, DonothingBindHints> {

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getStubGenerationParameters(java.util.Map)
	 */
	public String[] getStubGenerationParameters(Map<String, Object> bindHints)
			throws BindingFactoryException {
		return null;
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getExportHints(Map)
	 */
	public DonothingExportHints getExportHints(Map<String, Object> initialHints) {
		return null;
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getPluginIdentifier()
	 */
	public String getPluginIdentifier() {
		return "do-nothing";
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#createSkel(java.lang.String,
	 *      java.lang.Object, org.objectweb.fractal.api.Component,
	 *      org.objectweb.fractal.bf.ExportHints)
	 */
	public Component createSkel(String interfaceName, Object serverItf,
			Component owner, DonothingExportHints exportHints)
			throws SkeletonGenerationException {
		return null;
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getBindHints(Map)
	 */
	public DonothingBindHints getBindHints(Map<String, Object> initialHints) {
		return null;
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#createStub(org.objectweb.fractal.api.Component,
	 *      java.lang.String, org.objectweb.fractal.bf.BindHints)
	 */
	public Component createStub(Component client, String itfname,
			DonothingBindHints hints) throws StubGenerationException {
		return null;
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#finalizeSkeleton(org.objectweb.fractal.api.Component,
	 *      java.util.Map)
	 */
	public void finalizeSkeleton(Component skel,
			Map<String, String> finalizationHints)
			throws BindingFactoryException {

	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#finalizeStub(org.objectweb.fractal.api.Component,
	 *      java.util.Map)
	 */
	public void finalizeStub(Component stub, Map<String, Object> hints)
			throws BindingFactoryException {
		// TODO Auto-generated method stub

	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#setBindingFactoryClassLoader(org.objectweb.fractal.bf.BindingFactoryClassLoader)
	 */
	public void setBindingFactoryClassLoader(BindingFactoryClassLoader cl) {

	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getBindingFactoryClassLoader()
	 */
	public BindingFactoryClassLoader getBindingFactoryClassLoader() {

		return null;
	}

}
