/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf;

import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.julia.type.BasicInterfaceType;

/**
 * Utilities methods used to generate stub components.
 * 
 * @since 0.6
 */
public class StubGenerationUtil {
	/**
	 * Instantiate the class object for the given interface type using the given
	 * class loader.
	 * 
	 * @param it
	 *            the interfaceType from which to derive
	 * @param cl
	 *            the classLoader to use
	 * @return
	 * @throws InstantiationException
	 */
	public static final Class<?> instantiateClassFromInterfaceType(
			InterfaceType it, ClassLoader cl) throws InstantiationException {
		Class<?> clazz = null;

		try {

			clazz = cl.loadClass(it.getFcItfSignature());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();

			InstantiationException ie = new InstantiationException(
					"Cannot find class for interface type: "
							+ it.getFcItfSignature());
			ie.initCause(e);

			throw ie;
		}

		return clazz;
	}

	/**
	 * Return a copy of the given interface type where the direction of the
	 * interface has been inverted: if the direction of the specified interface
	 * type is client (resp. server), the direction of the returned interface
	 * type is server (resp. client).
	 */
	public static InterfaceType symmetricInterfaceType(InterfaceType it) {

		return new BasicInterfaceType(it.getFcItfName(),
				it.getFcItfSignature(), !it.isFcClientItf(), // client <->
				// server
				it.isFcOptionalItf(), it.isFcCollectionItf());
	}

	/**
	 * @param tf
	 *            the TypeFactory to use to create the stub component type
	 * @param stubInterfaceType
	 *            the interface type for the concrete proxy
	 * @return the component type of the proxy component
	 * @throws InstantiationException
	 *             if the type factory tf can't create the component type
	 * @see InstantiationException
	 */
	public static ComponentType createStubComponentType(TypeFactory tf,
			InterfaceType stubInterfaceType) throws InstantiationException {
		ComponentType stubComponentType = tf
				.createFcType(new InterfaceType[] { stubInterfaceType });
		return stubComponentType;
	}

}
