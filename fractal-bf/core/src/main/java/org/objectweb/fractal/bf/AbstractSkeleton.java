/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 *
 * Contributor: Philippe Merle
 *
 */
package org.objectweb.fractal.bf;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

/**
 * Abstract implementation for skeletons. Concrete plugins can extend this
 * class.
 */
public class AbstractSkeleton implements BindingController {

	/**
	 * The name of the delegate client interface.
	 */
	public static final String DELEGATE_CLIENT_ITF_NAME = "delegate";

	/**
	 * The name of the servant client interface.
	 */
	public static final String SERVANT_CLIENT_ITF_NAME = "servant";

	/**
	 * Component being bridged. Calls received by the abstract skeleton are
	 * forwarded to the functional interface of this delegate.
	 */
	/*
	 * TODO this field should be generated depending on the type of the server
	 * interface to be bound to. A generic tool to generate AbstractSkeleton
	 * subclasses should be in charge of doing this.
	 */
	protected Component delegate;

	/**
	 * Interface being bridged. Calls received by the abstract skeleton are
	 * forwarded to this servant.
	 */
	private Object servant;

	/**
	 * Returns the servant to invoke by the skeleton.
	 */
	protected Object getServant() {
		return servant;
	}

	/**
	 * @see org.objectweb.fractal.api.control.BindingController#bindFc(java.lang.String,
	 *      java.lang.Object)
	 */
	public void bindFc(String clientItfName, Object serverItf)
			throws NoSuchInterfaceException, IllegalBindingException,
			IllegalLifeCycleException {
		if (clientItfName.equals(DELEGATE_CLIENT_ITF_NAME)) {
			if (!(Component.class.isAssignableFrom(serverItf.getClass()))) {
				throw new IllegalBindingException(
						((("server interfaces connected to " + clientItfName) + " must be instances of ") + (Component.class
								.getName())));
			}
			delegate = ((Component) (serverItf));
			return;
		}
		if (clientItfName.equals(SERVANT_CLIENT_ITF_NAME)) {
			servant = serverItf;
			return;
		}
		throw new NoSuchInterfaceException(
				(("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	/**
	 * @see org.objectweb.fractal.api.control.BindingController#listFc()
	 */
	public String[] listFc() {
		return new String[] { DELEGATE_CLIENT_ITF_NAME, SERVANT_CLIENT_ITF_NAME };
	}

	/**
	 * @see org.objectweb.fractal.api.control.BindingController#lookupFc(java.lang.String)
	 */
	public Object lookupFc(String clientItfName)
			throws NoSuchInterfaceException {
		if (clientItfName.equals(DELEGATE_CLIENT_ITF_NAME)) {
			return delegate;
		}
		if (clientItfName.equals(SERVANT_CLIENT_ITF_NAME)) {
			return servant;
		}
		throw new NoSuchInterfaceException(
				(("Client interface \'" + clientItfName) + "\' is undefined."));
	}

	/**
	 * @see org.objectweb.fractal.api.control.BindingController#unbindFc(java.lang.String)
	 */
	public void unbindFc(String clientItfName) throws NoSuchInterfaceException,
			IllegalBindingException, IllegalLifeCycleException {
		if (clientItfName.equals(DELEGATE_CLIENT_ITF_NAME)) {
			delegate = null;
			return;
		}
		if (clientItfName.equals(SERVANT_CLIENT_ITF_NAME)) {
			servant = null;
			return;
		}
		throw new NoSuchInterfaceException(
				(("Client interface \'" + clientItfName) + "\' is undefined."));
	}

}
