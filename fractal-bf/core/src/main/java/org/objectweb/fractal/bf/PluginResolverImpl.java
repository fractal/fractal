/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES
 * Copyright (C) 2011 INRIA, University of Lille 1

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 * Author: Philippe Merle
 *
 */
package org.objectweb.fractal.bf;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.util.Fractal;

/**
 * A simple implementation of the {@link PluginResolver} interface.
 */
public class PluginResolverImpl implements PluginResolver, BindingController {

	Logger log = Logger.getLogger(PluginResolverImpl.class.getCanonicalName());
	/**
	 * The name of the collection interface to which e
	 */
	public final static String PLUGINS_BINDING = "plugins-";

	/**
	 * The plugins loaded.
	 */
	private final Map<String, BindingFactoryPlugin<?, ?>> plugins;

	/**
	 * Fractal ADL Factory used to load plugins.
	 */
	private Factory fractalAdlFactory;

	public PluginResolverImpl() {
		this.plugins = new HashMap<String, BindingFactoryPlugin<?, ?>>();
	}

	/**
	 * This implementation is based on a name-convention approach. Given the
	 * specified exportMode, the PluginResolver scans its "plugins" collection
	 * interface, looking for a referenced interface named
	 * "plugins-${exportMode}". If such interface is found, a reference to its
	 * {@link BindingFactoryPlugin} is returned; otherwise, an
	 * {@link PluginNotFoundException} exception is thrown.
	 * 
	 * @param pluginId
	 *            a string identifying the plugin able to identify the exporter.
	 * @throws PluginNotFoundException
	 *             if the plugin specified by the specified exportMode can't be
	 *             loaded
	 * @see org.objectweb.fractal.bf.PluginResolver#resolvePlugin(java.lang.String)
	 */
	public BindingFactoryPlugin<?, ?> resolvePlugin(String pluginId)
			throws PluginNotFoundException {
		log.info("Resolving plugin with identifier:'" + pluginId + "'");
		BindingFactoryPlugin<?, ?> plugin = null;
		try {
			log.fine("Looking into the collection interface for the plugin");
			plugin = (BindingFactoryPlugin<?, ?>) lookupFc(PLUGINS_BINDING
					+ pluginId);
		} catch (NoSuchInterfaceException e) {
			throw new PluginNotFoundException(
					"Could not find plugin for plugin.id : '" + pluginId + "'",
					e);
		}

		if (plugin == null) {
			// let's try to look for it in the classpath

			String pluginName = null;

			if (pluginId.lastIndexOf("-") != -1) {
				pluginName = pluginId.split("-")[1];
			} else {
				pluginName = pluginId;
			}

			final String pluginNameCapitalized = capitalize(pluginName);
			// XXX hard-coding package for adl definition of connectors..
			final String def = "org.objectweb.fractal.bf.connectors."
					+ pluginName + "." + pluginNameCapitalized + "Connector";
			log.info("Loading plugin from ADL file: " + def);

			try {

				HashMap<Object, Object> hints = new HashMap<Object, Object>();
				//
				// Be sure that Fractal ADL will use the class loader of the
				// binding exporter/binder caller instead of the class loader
				// where Fractal ADL was loaded initially.
				// This allows us to load a plugin present in the class loader
				// of the caller but not present in the class loader of Fractal ADL.
				// This changes nothing for most of simple applications as both
				// class loaders are the same one, but is required for OW2 FraSCAti.
				//
				hints.put("classloader", Thread.currentThread().getContextClassLoader());

				// Create the Fractal ADL Factory at the first time a plugin is loaded
				// instead of each time a plugin is loaded.
				if(fractalAdlFactory == null) {
					fractalAdlFactory = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
				}
				final Component lazyLoadedPlugin = (Component) fractalAdlFactory.newComponent(def, hints);

				BindingFactoryPlugin<?, ?> pluginForPluginIdentifier = (BindingFactoryPlugin<?, ?>) lazyLoadedPlugin
						.getFcInterface("plugin");

				// add this component to the list of loaded plugins...
				bindFc(PLUGINS_BINDING + pluginId, pluginForPluginIdentifier);

				Fractal.getLifeCycleController(lazyLoadedPlugin).startFc();

				plugin = pluginForPluginIdentifier;
			} catch (ADLException adlexp) {
				throw new PluginNotFoundException(
						"Cannot load plugin for plugin.id : " + pluginId,
						adlexp);
			} catch (NoSuchInterfaceException nsie) {
				throw new PluginNotFoundException(
						"Cannot find 'plugin' server interface for ADL : "
								+ def, nsie);
			} catch (IllegalLifeCycleException ilce) {
				throw new PluginNotFoundException(
						"An error occurred while starting the plugin loaded from ADL : "
								+ def, ilce);
			}
		}

		if (plugin == null) {
			throw new PluginNotFoundException(
					"Cannot load plugin for export mode: " + pluginId);
		}

		return plugin;
	}

	/**
	 * @param pluginName
	 * @return
	 */
	protected String capitalize(String pluginName) {
		final char firstChar = pluginName.charAt(0);
		return String.valueOf(firstChar).toUpperCase()
				+ pluginName.substring(1, pluginName.length());
	}

	// -------------------------------------------------------------------------
	// Implementation of the BindingController interface
	// -------------------------------------------------------------------------

	public String[] listFc() {
		return plugins.keySet().toArray(new String[plugins.size()]);
	}

	public Object lookupFc(final String clientItfName)
			throws NoSuchInterfaceException {
		if (clientItfName.startsWith(PLUGINS_BINDING)) {
			return plugins.get(clientItfName);
		}
		throw new NoSuchInterfaceException("Cannot find client interface "
				+ clientItfName);
	}

	public void bindFc(final String clientItfName, final Object serverItf)
			throws NoSuchInterfaceException {
		if (clientItfName.startsWith(PLUGINS_BINDING)
				&& (serverItf instanceof BindingFactoryPlugin<?, ?>)) {
			plugins.put(clientItfName, (BindingFactoryPlugin<?, ?>) serverItf);
			return;
		}
		throw new NoSuchInterfaceException("Cannot find client interface "
				+ clientItfName);
	}

	public void unbindFc(final String clientItfName)
			throws NoSuchInterfaceException {
		if (clientItfName.startsWith(PLUGINS_BINDING)) {
			plugins.remove(clientItfName);
			return;
		}
		throw new NoSuchInterfaceException("Cannot find client interface "
				+ clientItfName);
	}

}
