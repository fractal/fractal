/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.util.Fractal;

/**
 * A BindingFactoryHelper provides access to the singleton instance of the local
 * binding factory.
 */
public class BindingFactoryHelper {

	public static final Logger log = Logger
			.getLogger(BindingFactoryHelper.class.getCanonicalName());
	/**
	 * A map of binding-factory instances, each one with a different
	 * configuration.
	 */
	private static Map<String, BindingFactory> BINDING_FACTORY_CONFIGURATION = new ConcurrentHashMap<String, BindingFactory>();

	/**
	 * The singleton instance of BindingFactory
	 */
	private static BindingFactory SINGLETON;
	/**
	 * The ADL factory used to load the BindingFactory definition
	 */
	private static Factory adlFactory;

	/**
	 * Return the the default configuration of the BindingFactory.
	 * 
	 * @return
	 * @throws BindingFactoryException
	 */
	public static synchronized BindingFactory getBindingFactory()
			throws BindingFactoryException {
		try {
			if (SINGLETON == null) {
				if (adlFactory == null) {
					adlFactory = FactoryFactory
							.getFactory(FactoryFactory.FRACTAL_BACKEND);
				}
				Component bf = (Component) adlFactory.newComponent(
						"org.objectweb.fractal.bf.BindingFactoryComp",
						new HashMap<String, String>());

				Fractal.getLifeCycleController(bf).startFc();

				SINGLETON = (BindingFactory) bf
						.getFcInterface("binding-factory");

			}
		} catch (Exception e) {
			throw new BindingFactoryException(
					"Some error occurred while retrieving the BindingFactory",
					e);
		}
		return SINGLETON;
	}

	/**
	 * Load the binding factory with the given configuration.
	 * 
	 * @param configuration
	 *            the required configuratin for the binding factory.
	 * @return
	 * @throws BindingFactoryException
	 */
	public static synchronized BindingFactory getBindingFactory(
			String configuration) throws BindingFactoryException {

		return getBindingFactory(configuration, new HashMap<String, String>());
	}

	public static synchronized BindingFactory getBindingFactory(
			String configuration, Map<String, String> contextMap)
			throws BindingFactoryException {
		BindingFactory toReturn = null;

		if (BINDING_FACTORY_CONFIGURATION.containsKey(configuration)) {
			toReturn = BINDING_FACTORY_CONFIGURATION.get(configuration);
		} else {

			try {
				if (adlFactory == null) {
					adlFactory = FactoryFactory
							.getFactory(FactoryFactory.FRACTAL_BACKEND);
				}

				log.fine("BFHelper, loading BF for definition in file: "
						+ configuration);

				Component bf = (Component) adlFactory.newComponent(
						configuration, contextMap);
				Fractal.getLifeCycleController(bf).startFc();

				toReturn = (BindingFactory) bf
						.getFcInterface("binding-factory");
				BINDING_FACTORY_CONFIGURATION.put(configuration, toReturn);
			} catch (ADLException e) {

				throw new BindingFactoryException(
						"Some error occurred while trying to load BindingFactory with configuration in file: "
								+ configuration, e);
			} catch (Exception e) {
				throw new BindingFactoryException(
						"Some error occurred while  loading the BindingFactory",
						e);
			}

		}

		return toReturn;
	}
}
