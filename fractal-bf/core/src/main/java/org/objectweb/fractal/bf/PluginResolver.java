/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf;

/**
 * A plug-in resolver can resolve the correct plug-in given a specified
 * {@link ExportMode} value.
 * 
 */
public interface PluginResolver {

	public static final String INTERFACE_NAME = "plugin-resolver";

	/**
	 * Resolve the plug-in for the specified export mode.
	 * 
	 * @param exportMode
	 *            see {@link ExportMode}
	 * @return the plug-in that can handle the specified exportMode
	 * @throws PluginNotFoundException
	 */
	BindingFactoryPlugin<?, ?> resolvePlugin(String exportMode)
			throws PluginNotFoundException;

}
