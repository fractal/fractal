/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf;

import java.util.Map;

import org.objectweb.fractal.api.Component;

/**
 * <p>
 * An interface to model a binding factory plugin. It exposes low-level actions
 * used by the default implementation of the {@link BindingFactory} to create
 * stub and skeleton components.
 * </p>
 * During the export phase (see
 * {@link BindingFactory#export(Component, String, Map)}, a skeleton is created,
 * then it is finalized, and then it is started.<br>
 * The same actions are performed for stub components.
 * 
 * @see BindingFactory
 * @See {@link BindingFactoryImpl} the default implementation
 */
public interface BindingFactoryPlugin<E extends ExportHints, B extends BindHints> {

	public static final String IDENTIFIER = "identifier";

	/**
	 * Creates plugin-specific skeletons.
	 * 
	 * @param interfaceName
	 *            the name of the interface to export
	 * @param interfaceToExport
	 *            a reference to the the interface to export
	 * @param owner
	 *            the component owning the interface to export
	 * @param exportHints
	 *            hints used to create the skeleton
	 * @return the skeleton component
	 * @throws SkeletonGenerationException
	 *             if some error occurs while generating the skeleton
	 */
	public Component createSkel(String interfaceName, Object interfaceToExport,
			Component owner, E exportHints) throws SkeletonGenerationException;

	/**
	 * Creates plugin specific stubs.
	 * 
	 * @param client
	 *            the component owning the interface to export
	 * @param itfname
	 *            the name of the client interface to bound to the stub
	 * @param hints
	 *            hints to create the stub
	 * @throws StubGenerationException
	 *             if some error occurs while generating the stub
	 */
	public Component createStub(Component client, String itfname, B hints)
			throws StubGenerationException;

	/**
	 * Finalize the stub component, setting bindings or performing operations
	 * that couldn't be done before.
	 * 
	 * @param stub
	 *            the stub component to finalize
	 * @param hints
	 *            hints to be used during the finalization process
	 * @throws BindingFactoryException
	 */
	void finalizeStub(Component stub, Map<String, Object> hints)
			throws BindingFactoryException;

	/**
	 * Finalize the skel component, setting bindings or performing operations
	 * that couldn't be done before.
	 * 
	 * @param skel
	 *            the skeleton component to finalize
	 * @param finalizationHints
	 *            hints to be used during the finalization process
	 * @throws BindingFactoryException
	 */
	void finalizeSkeleton(Component skel, Map<String, String> finalizationHints)
			throws BindingFactoryException;

	/**
	 * @param initialHints
	 *            the hints passed to the binding factory
	 * @return the {@link ExportHints} specific for this connector.
	 */
	public E getExportHints(Map<String, Object> initialHints);

	/**
	 * 
	 * @param initialHints
	 *            the hints passed to the binding factory
	 * 
	 * @return the {@link BindHints} specific for this connector.
	 */
	public B getBindHints(Map<String, Object> initialHints);

	/**
	 * Set the classloader to use by this plugin.
	 * 
	 * @param cl
	 */
	public void setBindingFactoryClassLoader(BindingFactoryClassLoader cl);

	/**
	 * Get the classloader used by this plugin.
	 * 
	 * @return the classloader used by this plugin.
	 */
	public BindingFactoryClassLoader getBindingFactoryClassLoader();

	/**
	 * The unique identifier for this plugin.
	 * 
	 * @return a string identifier for this plugin
	 */
	String getPluginIdentifier();

}
