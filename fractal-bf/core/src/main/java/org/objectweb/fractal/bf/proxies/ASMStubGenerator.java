/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.proxies;

import java.lang.reflect.Method;
import java.util.logging.Logger;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;

/**
 * A generic stub generator. This class can generate concrete implementations of
 * a class. It is used to generate stub implementation classes to be used as
 * 'impl' elements of stub components.
 * 
 * @since 0.6
 */
public class ASMStubGenerator implements org.objectweb.asm.Opcodes {

	static Logger log = Logger.getLogger(ASMStubGenerator.class
			.getCanonicalName());

	/**
	 * Generate a class that implements the given interface and that extends the
	 * given super class. The generated class provides a default constructor
	 * without arguments which invokes the super class's default constructor.
	 * 
	 * @param interfaze
	 *            the java interface that the generated class will implement
	 * @param superClazz
	 *            the superclass of the generated class
	 * @param cl
	 *            the classLoader that will define the generated class
	 * @return the generated class.
	 * @See {@link BindingFactoryClassLoader#defineClass(byte[], String)}
	 */
	public static Class<?> generate(Class<?> interfaze, Class<?> superClazz,
			BindingFactoryClassLoader cl) {

		/*
		 * Using COMPUTE_MAXS flag avoids to handle manually stack indices to be
		 * specified in the various computeMaxs methods.
		 */
		ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);

		MethodVisitor mv;

		String className = createStubClassName(superClazz);
		String superClassName = Type.getInternalName(superClazz);

		String itfInternalName = Type.getInternalName(interfaze);
		cw.visit(V1_5, ACC_PUBLIC + ACC_SUPER, className, null, superClassName,
				new String[] { itfInternalName, });

		/*
		 * generate default constructor of the derived class, which invoke the
		 * default constructor of the super class.
		 */
		{
			mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
			mv.visitCode();
			Label l0 = new Label();
			mv.visitLabel(l0);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitMethodInsn(INVOKESPECIAL, superClassName, "<init>", "()V");
			Label l1 = new Label();
			mv.visitLabel(l1);
			mv.visitInsn(RETURN);
			Label l2 = new Label();
			mv.visitLabel(l2);
			mv.visitLocalVariable("this", "L" + className + ";", null, l0, l2,
					0);
			mv.visitMaxs(1, 1);
			mv.visitEnd();
		}

		/*
		 * for every method declared by the interfaze, generate a method in the
		 * subclass being generated with the same signature, and that delegates
		 * the calls to the remoteRef reference inherited by the superclass
		 */
		for (Method method : interfaze.getMethods()) {
			log.finest("Generating method body for " + method.getName());

			Class<?>[] paramTypes = method.getParameterTypes();
			int args = paramTypes.length;

			String desc = Type.getMethodDescriptor(method);
			mv = cw.visitMethod(ACC_PUBLIC, method.getName(), desc, null, null);

			mv.visitCode();

			Label l0 = new Label();
			mv.visitLabel(l0);
			/*
			 * push this on the stack
			 */
			mv.visitVarInsn(ALOAD, 0);
			/*
			 * recover a reference to the remoteRef field for 'this' object: the
			 * field remoteRef must be inherited by the super class of this
			 * generated class.
			 */
			mv.visitFieldInsn(GETFIELD, className, "remoteRef",
					"Ljava/lang/Object;");
			/*
			 * cast the remoteRef object to the type of interface
			 */
			mv.visitTypeInsn(CHECKCAST, itfInternalName);
			Label l1 = new Label();
			mv.visitLabel(l1);
			mv.visitLocalVariable("this", "L" + className + ";", null, l0, l1,
					0);
			/*
			 * for every parameter of this method, push the actual arguments on
			 * the stack. Depending on the type of the variable, a corresponding
			 * java-type dependant LOAD instruction must be executed.
			 */
			log.finest("There are " + args + " arguments to push on stack");
			int i = 1;
			for (Class<?> c : paramTypes) {
				Type paramType = Type.getType(c);
				int opcode = paramType.getOpcode(ILOAD);
				final int size = paramType.getSize();
				log.finest("Next index used by visitVarIns: " + i);
				/* declares local variables contextually to the pushing */
				/*
				 * The name is not important as it is used only for debug infos.
				 * Also, there's no way to recover the original name of the
				 * variable as it was used in the original code no support in
				 * the reflection APIs. A deeper discussion might be found
				 * following links here:
				 * http://www.jroller.com/vschiavoni/entry/how_to_get_the_name
				 */
				mv.visitLocalVariable("a", paramType.toString(), null, l0, l1,
						i);
				mv.visitVarInsn(opcode, i);
				i = i + size;
			}

			mv.visitMethodInsn(INVOKEINTERFACE, itfInternalName, method
					.getName(), desc);

			mv.visitInsn(Type.getReturnType(method).getOpcode(IRETURN));

			/* this should not be required because of the COMPUTE_MAX */
			mv.visitMaxs(0, 0);
			mv.visitEnd();

			log.finer("Method " + method.toGenericString() + " generated");
		}

		cw.visitEnd();
		byte[] bytecode = cw.toByteArray();
		BytecodeVerifier.verify(bytecode);

		final Class<?> definedClazz = cl.defineClass(bytecode, className
				.replace("/", "."));

		return definedClazz;
	}

	/**
	 * @return return the internal name for the class to generate.
	 */
	protected static String createStubClassName(Class<?> superClass) {
		String superClazz = Type.getInternalName(superClass);
		return superClazz + "Impl";

	}

	/**
	 * @param stubItf
	 *            the interface that the stub class will implement
	 * @param superClazz
	 *            the super class of the stub class
	 * @param theClassLoaderToUse
	 *            the class loader to use
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public static Object generateNewInstance(Class<?> stubItf,
			Class<?> superClazz, BindingFactoryClassLoader theClassLoaderToUse)
			throws InstantiationException, IllegalAccessException {

		Class<?> instanceClazz = generate(stubItf, superClazz,
				theClassLoaderToUse);
		return instanceClazz.newInstance();

	}

}
