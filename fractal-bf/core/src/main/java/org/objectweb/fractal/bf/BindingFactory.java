/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2012 Inria, SARDES, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 *
 * Contributor: Philippe Merle
 */
package org.objectweb.fractal.bf;

import java.util.Map;

import org.objectweb.fractal.api.Component;

/**
 * A simple binding factory interface. The specific semantic of the operations
 * is protocol-specific.
 * 
 * @see BindingFactoryImpl
 * @see BindingFactoryException
 */
public interface BindingFactory {
	/**
	 * Export an interface using the specified hints.
	 * 
	 * @param c
	 *            the Fractal component owner of the interface to export
	 * @param itfName
	 *            the name of the server interface to export
	 * @param hints
	 *            hints used to export the interface
	 *
	 * @return the new skeleton component.
	 *
	 * @throws BindingFactoryException
	 *             if some error occurs while exporting the interface
	 */
	Component export(Component c, String itfName, Map<String, Object> hints)
			throws BindingFactoryException;

	/**
	 * Unexport an interface.
	 * 
     * @param serverComponent
     *            the component owning the interface to export
     * @param skeleton
     *            the BF skeleton component used to export the interface.
	 * @param hints
	 *            hints to be used to unexport the interface
	 * @throws BindingFactoryException
	 *             if some error occurs while unexporting the interface
	 */
	void unexport(Component serverComponent, Component skeleton, Map<String, Object> hints)
			throws BindingFactoryException;

	/**
	 * Bind the specified client interface using the specified hints.
	 * 
	 * @param component
	 *            the Fractal component owner of the interface to bind
	 * @param itfName
	 *            the name of client interface to bind
	 * @param hints
	 *            a map of hints to reconstruct the export id
	 * @return the new stub component.
	 *
	 * @throws BindingFactoryException
	 *             if some error occurs while binding the interface
	 */
	Component bind(Component component, String itfName, Map<String, Object> hints)
			throws BindingFactoryException;

	/**
	 * Unbind the specified client interface.
	 * 
     * @param clientComponent
     *            the component of the interface to unbind
     * @param clientItf
     *            the name of the client interface to unbind.
     * @param stub
     *            the BF stub component used to bind the interface.
	 * @throws BindingFactoryException
	 *             if some error occurs while unbinding.
	 */
	void unbind(Component clientComponent, String clientItf, Component stub)
			throws BindingFactoryException;
}
