/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf.proxies;

/**
 * An exception thrown when some error occurs generating bytecode.
 * 
 */
public class BytecodeGenerationException extends RuntimeException {

	/**
	 * @param string
	 */
	public BytecodeGenerationException(String message) {
		super(message);
	}

	/**
	 * @param string
	 * @param cause
	 */
	public BytecodeGenerationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = 1L;

}
