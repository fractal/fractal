/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Valerio.Schiavoni@gmail.com
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

/**
 * The class loader used by the Binding Factory. Among the other things, it
 * implements a caching system to avoid reloading classes or to delegate to the
 * super classloaders.
 */
public class BindingFactoryClassLoader extends ClassLoader {

	Logger log = Logger.getLogger(BindingFactoryClassLoader.class
			.getCanonicalName());

	/**
	 * Stores the binary names of the classes defined by this class loader and
	 * the corresponding Class and byte objects.
	 */
	private final Map<String, LoadedClass> cache;

	/**
	 * 
	 * @param parentClassLoader
	 *            the parent class loader for this class loader.
	 */
	public BindingFactoryClassLoader(ClassLoader parentClassLoader) {

		super(parentClassLoader);
		log.finest("Parent classloader for BindingFactoryClassLoader: "
				+ parentClassLoader);
		this.cache = new HashMap<String, LoadedClass>();
	}

	public Class<?> defineClass(byte[] byteArray, String binaryName) {

		log.finest("Binary name for class to define : " + binaryName);
		Class<?> result = null;
		if (!cache.containsKey(binaryName)) {
			result = super.defineClass(binaryName, byteArray, 0,
					byteArray.length);

			this.cache.put(binaryName, new LoadedClass(result, byteArray));

		} else {
			result = this.cache.get(binaryName).clazz;
		}
		log.finest("Class returned by defineClass: " + result);
		return result;
	}

	public Class<?> getClass(String binaryName) {
		return this.cache.get(canonicalNameFrom(binaryName)).clazz;
	}

	@Override
	public InputStream getResourceAsStream(String name) {
		log.finest("Name of resource: " + name + " (classes defined: "
				+ this.cache + ")");

		InputStream is = null;

		String nameForDefinedClass = canonicalNameFrom(name);

		LoadedClass found = this.cache.get(nameForDefinedClass);
		if (found != null) {
			is = found.toInputStream();
		} else {
			is = super.getResourceAsStream(name);
		}

		return is;
	}

	/**
	 * Infer the canonical name from the binary name
	 * 
	 * @param binaryName
	 *            the binary name of the class
	 * @return the canonical name
	 */
	private String canonicalNameFrom(String binaryName) {
		String canonical = binaryName.substring(0, binaryName.lastIndexOf("."))
				.replace("/", ".");
		return canonical;
	}

	/**
	 * Return a view of the binary names for the classes defined by this class
	 * loader. Modifications to this set are not backed by modifications to the
	 * set of classes defined by this classloader.
	 * 
	 * @returna copy of set of defined classes by this class loader.
	 */
	public Set<String> getDefinedClasses() {
		return new HashSet<String>(cache.keySet());
	}

	/**
	 * A class to keep track of classes (and their bytes) defined by this
	 * classloader.
	 */
	class LoadedClass {
		private final Class<?> clazz;
		private final byte[] raw;

		LoadedClass(Class<?> c, byte[] raw) {
			this.clazz = c;
			this.raw = raw;
		}

		InputStream toInputStream() {
			return new BufferedInputStream(new ByteArrayInputStream(raw));
		}
	}

}
