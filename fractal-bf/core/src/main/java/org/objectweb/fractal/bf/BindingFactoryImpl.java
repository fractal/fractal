/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2012 Inria, SARDES, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 *
 * Contributors: Philippe Merle
 *               Christophe Demarey
 */
package org.objectweb.fractal.bf;

import java.util.Map;
import java.util.logging.Logger;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.control.SuperController;
import org.objectweb.fractal.util.Fractal;

/**
 * The default BindingFactory implementation. This implementation can export and
 * bind components (creating skeleton and stub components binding them to the
 * server and the client, respectively).
 * 
 * The binding factory use a map of hints to load the plugin to use during the
 * generation of the binding components.
 * 
 * @since 0.1
 */
public class BindingFactoryImpl implements BindingFactory, BindingController {

	/**
	 * The key to use in the map of hints passed as arguments to the export/bind
	 * methods, to define which plugin to load.
	 */
	public static final String PLUGIN_ID = "plugin.id";
	/**
	 * The key to use to specify which classloader to use when binding/exporting
	 * interfaces.
	 */
	public static final String CLASS_LOADER = "classloader";

	/**
	 * The key to use in the map of hints passed as arguments to the export/bind
	 * methods, to not start stub/skeleton components created by the Fractal Binding Factory.
	 * These components will be started later by the calling application, e.g., OW2 FraSCAti.
	 *
	 * @since 0.9
	 */
	public static final String DONT_START_COMPONENT = "don't start component";

	/**
	 * The logger.
	 */
	private final Logger log = Logger.getLogger(BindingFactoryImpl.class
			.getCanonicalName());

	/**
	 * The pluginResolver that load required plugins.
	 * 
	 */
	private PluginResolver pluginResolver;

	/**
	 * Export an interface using the specified hints. The export process is
	 * peformed by executing the following steps:
	 * <ol>
	 * <li>input parameters are verified;
	 * <li>a lookup of the functional interface whose name is 'itfName' is done
	 * on the input serverComponent passed as parameter;
	 * <li>using the hints, the appropriate plug-in is loaded
	 * <li>the loaded plugin is used to generate a skeleton component
	 * <li>the skeleton is added to the enclosing componento of the
	 * serverComponent passed as parameter
	 * <li>the client inteface named 'delegate' owned by the skeleton is bound
	 * to the functional interface
	 * </ol>
	 * 
	 * @param serverComponent
	 *            the component owning the interface to export
	 * @param itfName
	 *            the name of the interface to export
	 * @param hints
	 *            hits used to export the interface.
	 * @throws BindingFactoryException
	 *             if the input parameters are invalid. This exception should
	 *             wrap the exception causing the error.
	 * @throws NoSuchInterfaceException
	 *             if the serverComponent doesn't provide an external interface
	 *             whose name is <code>itfName</code>
	 */
	@SuppressWarnings("unchecked")
	public Component export(Component serverComponent, String itfName,
			Map<String, Object> hints) throws BindingFactoryException {

		// 0) verify parameters
		if (serverComponent == null) {
			IllegalArgumentException iea = new IllegalArgumentException(
					"The reference to the owner component of the interface to be exported was null");
			throw new BindingFactoryException(iea);

		} else if (itfName == null || itfName.equalsIgnoreCase("")) {
			IllegalArgumentException iea = new IllegalArgumentException(
					"The name of the server interface to export can't be null or empty (was:'"
							+ itfName + "')");
			throw new BindingFactoryException(iea);

		} else if (hints == null || hints.keySet().size() == 0) {
			IllegalArgumentException iea = new IllegalArgumentException(
					"Export hints map is null or empty. At least it must provide a plugin.id key/value mapping.");
			throw new BindingFactoryException(iea);
		}

		// 1) retrieve the Fractal interface for the specified 'itfName'
		Object interfaceToExport = null;

		try {
			interfaceToExport = serverComponent.getFcInterface(itfName);
		} catch (NoSuchInterfaceException e) {
			throw new BindingFactoryException("Cannot find interface '"
					+ itfName
					+ "' on the component owning the interface to export", e);
		}

		String pluginId = getPluginIdFromHints(hints);

		// 2) load the the plug-in to handle the given export mode
		BindingFactoryPlugin plugin = loadPlugin(pluginId);

		if (plugin == null) {
			throw new BindingFactoryException(
					"Couldn't load a plugin from hints: " + hints.toString());
		}

		configurePluginClassLoader(hints, plugin);

		ExportHints exportHints = plugin.getExportHints(hints);
		log.info("ExportHints :" + exportHints);
		// 3) use the plugin to generate the skeleton
		Component skel = null;
		try {
			skel = plugin.createSkel(itfName, interfaceToExport,
					serverComponent, exportHints);
		} catch (SkeletonGenerationException e) {
			throw new BindingFactoryException(
					"Some error occured while creating the skeleton using the "
							+ pluginId + " plugin ", e);
		}

		try	{
			/*
			 * 4) get the the composite component containing the server
			 * component being exported
			 */
			Component parentComposite = getParentComponent(serverComponent);

			if (parentComposite == null) {
				throw new BindingFactoryException(
						"Cannot export a Fractal interface of a primitive component. Add the primitive component into a composite before exporting it.");
			}

			/*
			 * 5) add the skeleton into the same composite of the server
			 * component
			 */
			Fractal.getContentController(parentComposite).addFcSubComponent(
					skel);

			/*
			 * 6) bind the skeleton 'delegate' client interface to the
			 * 'component' interface of the serverComponent
			 */
			/*
			 * TODO in next release, the 'delegate' fieldw be typed according to
			 * the type of the server interface being exported. The bindFc
			 * serverItf won't be anymore the Component reference to the owner
			 * component but directly the business interface to export.
			 */
			Fractal.getBindingController(skel).bindFc(
					AbstractSkeleton.DELEGATE_CLIENT_ITF_NAME, serverComponent);

			try {
				// TODO: Following implements the previous TODO.
				Fractal.getBindingController(skel).bindFc(
					AbstractSkeleton.SERVANT_CLIENT_ITF_NAME, interfaceToExport);
			} catch(Exception e) {
				// TODO: But currently not all plugins create skeletons with a 'servant' client interface.
			}

			plugin.finalizeSkeleton(skel, hints);

			setComponentName(serverComponent, skel, plugin
					.getPluginIdentifier()
					+ "-skeleton");

			try {
				String compName = Fractal.getNameController(serverComponent)
						.getFcName();
				log.info("BindingFactory successfully exported interface "
						+ itfName + " owned by component [" + compName
						+ "] with the following parameters: " + hints);
			} catch (NoSuchInterfaceException e) {
				log
						.info("BindingFactory successfully exported interface   "
								+ itfName
								+ " owned by [unamed] component with the following parameters: "
								+ hints);

			}

			// start the skeleton only if hints does not contain the DONT_START_COMPONENT key.
			if(!hints.containsKey(DONT_START_COMPONENT)) {
				Fractal.getLifeCycleController(skel).startFc();
				log.info("Skeleton component started");
			}

		} catch (NoSuchInterfaceException e) {
			throw new BindingFactoryException(
					"Some error occurred while accessing the membrane of the component",
					e);
		} catch (IllegalBindingException e) {
			throw new BindingFactoryException(
					"Some error occurred while binding the skeleton to the business component",
					e);
		} catch (IllegalLifeCycleException e) {
			throw new BindingFactoryException(
					"Some error occurred while binding the skeleton to the business component interface",
					e);
		} catch (IllegalContentException e) {
			throw new BindingFactoryException(
					"Some error occurred while adding the skeleton into the composite containing the component owning the interface to export",
					e);
		}

		return skel;
	}

	/**
	 * Set the classloader to be used by the plugin. If the hints do not provide
	 * an entry for the key {@link BindingFactoryImpl#CLASS_LOADER}, the current
	 * thread's context class loader is used as parent class loader.
	 * 
	 * @param hints
	 *            the hints which provide the classloader
	 * @param plugin
	 *            the plugin receiving the class loader
	 */
	@SuppressWarnings("unchecked")
	private void configurePluginClassLoader(Map<String, Object> hints,
			BindingFactoryPlugin plugin) {
		if (hints.containsKey(CLASS_LOADER)) {
			ClassLoader parent = (ClassLoader) hints.get(CLASS_LOADER);
			BindingFactoryClassLoader bfcl = new BindingFactoryClassLoader(
					parent);
			plugin.setBindingFactoryClassLoader(bfcl);
		} else {
			log.warning("Plugin " + plugin.getPluginIdentifier()
					+ " will use context class loader");
			BindingFactoryClassLoader bfcl = new BindingFactoryClassLoader(
					Thread.currentThread().getContextClassLoader());
			plugin.setBindingFactoryClassLoader(bfcl);
		}
	}

	/**
	 * @param hints
	 *            the initial hints
	 * @return the value for key {@link BindingFactoryImpl#PLUGIN_ID}
	 */
	protected String getPluginIdFromHints(Map<String, Object> hints)
			throws IllegalArgumentException {
		if (!hints.containsKey(PLUGIN_ID)) {
			throw new IllegalArgumentException(
					"The hints do not provide an entry for key " + PLUGIN_ID
							+ " (maybe you're still using export.mode)?");
		}
		String pluginId = (String) hints.get(PLUGIN_ID);

		return pluginId;
	}

	/**
	 * Return the parent component of the specified component. If the component
	 * has more than one parent component, the first one is returned.
	 * 
	 * @param comp
	 * @return the first parent component of the given component.
	 * @throws NoSuchInterfaceException
	 *             if the comp does not provide the {@link SuperController}
	 *             interface.
	 */
	protected Component getParentComponent(Component comp)
			throws NoSuchInterfaceException {
		Component parentComposite = Fractal.getSuperController(comp)
				.getFcSuperComponents()[0];
		return parentComposite;
	}

	/**
	 * Load a {@link BindingFactoryPlugin}. The map used to load the plugin must
	 * contain a value for key {@link BindingFactoryImpl#PLUGIN_ID}. If such key
	 * is not found, the plugin to establish local bindings is used instead.
	 * 
	 * @param hints
	 *            contains hints to load the plugin
	 * @return the plugin
	 * @throws BindingFactoryException
	 *             if the pluginResolver client interface is not bound, or if
	 *             some problem occurred while loading the specified plugin
	 *             using a specific export mode
	 */
	protected BindingFactoryPlugin<?, ?> loadPlugin(String pluginId)
			throws BindingFactoryException {

		// load appropriate plugin for the specified export mode
		BindingFactoryPlugin<?, ?> plugin;
		try {
			if (pluginResolver == null) {
				throw new BindingFactoryException(
						"The "
								+ PluginResolver.INTERFACE_NAME
								+ " client interface of the BindingFactoryCore component was not properly bound.");

			}
			plugin = pluginResolver.resolvePlugin(pluginId);
		} catch (PluginNotFoundException e) {
			throw new BindingFactoryException(
					"Cannot resolve any plugin for plugin.id: " + pluginId, e);
		}

		return plugin;
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactory#bind(org.objectweb.fractal.api.Component,
	 *      java.lang.String, java.util.Map)
	 */
	@SuppressWarnings("unchecked")
	public Component bind(Component client, String clientItf,
			Map<String, Object> hints) throws BindingFactoryException {

		// 0) verify parameters
		if (client == null) {
			IllegalArgumentException iea = new IllegalArgumentException(
					"The reference to the owner component of the interface to be exported was null");
			throw new BindingFactoryException(iea);

		} else if (clientItf == null || clientItf.equalsIgnoreCase("")) {
			IllegalArgumentException iea = new IllegalArgumentException(
					"The name of the server interface to export can't be null or empty (was:'"
							+ clientItf + "')");
			throw new BindingFactoryException(iea);

		} else if (hints == null || hints.keySet().size() == 0) {
			IllegalArgumentException iea = new IllegalArgumentException(
					"Bind hints map is null or empty. At least it must provide a plugin.id key/value mapping.");
			throw new BindingFactoryException(iea);
		}

		String pluginId = getPluginIdFromHints(hints);

		BindingFactoryPlugin plugin = loadPlugin(pluginId);

		configurePluginClassLoader(hints, plugin);

		BindHints bindHints = plugin.getBindHints(hints);
		log.info("BindHints :" + bindHints);
		// create an appropriate stub
		final Component stub = plugin.createStub(client, clientItf, bindHints);

		finalizeBind(client, clientItf, stub, plugin, hints);

		setComponentName(client, stub, plugin.getPluginIdentifier() + "-stub");

		// start the stub
		try {
			log.finest("State of stub component: "
					+ Fractal.getLifeCycleController(stub).getFcState());
			// start the stub only if hints does not contain the DONT_START_COMPONENT key.
			if(!hints.containsKey(DONT_START_COMPONENT)) {
				Fractal.getLifeCycleController(stub).startFc();
				log.info("Stub component started");
			}
		} catch (NoSuchInterfaceException e) {
			throw new BindingFactoryException("Cannot start stub component", e);
		} catch (IllegalLifeCycleException e) {
			throw new BindingFactoryException("Cannot start stub component", e);
		}

		return stub;
	}

	/**
	 * Used internally to set human-readable names to stubs and skeletons.
	 */
	private void setComponentName(final Component base,
			final Component bindingComponent, final String suffix) {
		String bindingComponentName = suffix;
		NameController nc;
		try {
			nc = Fractal.getNameController(base);

		} catch (NoSuchInterfaceException e) {
			log.warning("Could not retrieve NamingController on component "
					+ base + ", not setting any name.");
			return;
		}
		if (nc != null) {
			String ownerName = nc.getFcName();
			bindingComponentName = ownerName + "-" + bindingComponentName;
		}

		try {
			Fractal.getNameController(bindingComponent).setFcName(
					bindingComponentName);
		} catch (NoSuchInterfaceException e) {
			log.warning("Could not retrieve NamingController on component "
					+ bindingComponent + ", not setting any name.");
			return;
		}
	}

	/**
	 * Add the stub component into the the same composite component of the
	 * client component, and bind the client component interface to the
	 * appropriate interface of the stub. Also, set appropriate name for the
	 * stub.
	 * 
	 * @param clientComponent
	 *            the component owning the client interface
	 * @param clientItf
	 *            the name of the client interface the stub is for
	 * @param stub
	 *            a stub for the client interface
	 * @param plugin
	 *            the plug-in to use to finalize the binding
	 * @param hints
	 *            the map of hints to be used during the finalization phase
	 * @throws BindingFactoryException
	 *             if some exception occurs
	 */
	private void finalizeBind(Component clientComponent, String clientItf,
			final Component stub, BindingFactoryPlugin<?, ?> plugin,
			Map<String, Object> hints) throws BindingFactoryException {
		try {

			// get the parent of the client component
			Component parentOfClient = getParentComponent(clientComponent);

			// insert the stub into same composite of the client
			Fractal.getContentController(parentOfClient)
					.addFcSubComponent(stub);

			// let the plugin finalize the stub
			plugin.finalizeStub(stub, hints);

			/*
			 * use the name of the component to set the name of the skeleton, by
			 * appending -(PLUGIN_ID)-skeleton to its name. If the server
			 * component doesn't define a NamingController, simply use not
			 * found, use simply (PLUGIN_ID)-skeleton.
			 */
			String stubName = plugin.getPluginIdentifier() + "-stub";
			NameController nc = Fractal.getNameController(clientComponent);
			if (nc != null) {
				String ownerName = nc.getFcName();
				stubName = ownerName + "-" + stubName;
			}

			Fractal.getNameController(stub).setFcName(stubName);

			// bind the client interface of the client to the server interface
			// of the stub
			Fractal.getBindingController(clientComponent).bindFc(clientItf,
					stub.getFcInterface(clientItf));
			log.fine("Client interface: " + clientItf + " owned by  "
					+ Fractal.getNameController(clientComponent).getFcName()
					+ " is now bound to the generated stub component:" + stub
					+ " throught its server interface:" + clientItf);

		} catch (Exception e) {
			throw new BindingFactoryException(
					"Some error occurred while finalizing the binding", e);
		}
	}

	/**
	 * Unexport an interface, removing the skeleton from the membrane. If there
	 * are established bindings with the interface to be unexorted, the
	 * operation is interrupted. More complicate policies can be established in
	 * future.
	 * 
     * @param serverComponent
     *            the component owning the interface to export
     * @param skeleton
     *            the BF skeleton component used to export the interface.
	 * @param hints
	 *            hints to be used to unexport the interface
	 * @throws BindingFactoryException
	 *             if some error occurs while unexporting the interface
	 */
	public void unexport(Component serverComponent, Component skeleton, Map<String, Object> hints)
			throws BindingFactoryException {
        try {
            // 1) Stop the skeleton
            Fractal.getLifeCycleController(skeleton).stopFc();
            log.info("Skeleton component stopped");

            /*
             * 2) Unbind the skeleton 'delegate' client interface to the
             * 'component' interface of the serverComponent
             */
            /*
             * TODO in next release, the 'delegate' fieldw be typed according to
             * the type of the server interface being exported. The bindFc
             * serverItf won't be anymore the Component reference to the owner
             * component but directly the business interface to export.
             */
            Fractal.getBindingController(skeleton).unbindFc(
                    AbstractSkeleton.DELEGATE_CLIENT_ITF_NAME);

            try {
                // TODO: Following implements the previous TODO.
                Fractal.getBindingController(skeleton).unbindFc(
                    AbstractSkeleton.SERVANT_CLIENT_ITF_NAME);
            } catch(Exception e) {
                // TODO: But currently not all plugins create skeletons with a 'servant' client interface.
            }

            /*
             * 3) get the the composite component containing the server
             * component being exported
             */
            Component parentComposite = getParentComponent(serverComponent);

            if (parentComposite != null) {
                /*
                 * 4) remove the skeleton from the same composite of the server
                 * component
                 */
                Fractal.getLifeCycleController(parentComposite).stopFc();
                Fractal.getContentController(parentComposite).removeFcSubComponent(skeleton);
                Fractal.getLifeCycleController(parentComposite).startFc();
            }
        } catch (NoSuchInterfaceException e) {
            throw new BindingFactoryException(
                    "Some error occurred while accessing the membrane of the component", e);
        } catch (IllegalBindingException e) {
            throw new BindingFactoryException(
                    "Some error occurred while unbinding the skeleton from the business component", e);
        } catch (IllegalLifeCycleException e) {
            throw new BindingFactoryException(
                    "Some error occurred while unbinding the skeleton from the business component interface", e);
        } catch (IllegalContentException e) {
            throw new BindingFactoryException(
                    "Some error occurred while removing the skeleton from the composite containing the component owning the interface to unexport", e);
        }
	}

	/**
	 * Unbind a client interface.
	 * 
     * @param clientComponent
     *            the component of the interface to unbind
	 * @param clientItf
	 *            the name of the client interface to unbind.
     * @param stub
     *            the BF stub component used to bind the interface.
	 */
    public void unbind(Component clientComponent, String clientItf, Component stub)
			throws BindingFactoryException {
        try {
            // 1) Stop the stub
            Fractal.getLifeCycleController(stub).stopFc();
            log.info("Stub component stopped");

            /*
             * 2) Unbind the client interface of the client from the server interface
             *    of the stub
             */
            Fractal.getLifeCycleController(clientComponent).stopFc();
            Fractal.getBindingController(clientComponent).unbindFc(clientItf);
            Fractal.getLifeCycleController(clientComponent).startFc();

            /*
             * 3) get the the composite component containing the server
             * component being exported
             */
            Component parentComposite = getParentComponent(clientComponent);

            if (parentComposite != null) {
                /*
                 * 4) remove the skeleton from the same composite of the server
                 * component
                 */
                Fractal.getLifeCycleController(parentComposite).stopFc();
                Fractal.getContentController(parentComposite).removeFcSubComponent(stub);
                Fractal.getLifeCycleController(parentComposite).startFc();
            }
        } catch (NoSuchInterfaceException e) {
            throw new BindingFactoryException(
                    "Some error occurred while accessing the membrane of the component", e);
        } catch (IllegalBindingException e) {
            throw new BindingFactoryException(
                    "Some error occurred while unbinding the stub from the business component", e);
        } catch (IllegalLifeCycleException e) {
            throw new BindingFactoryException(
                    "Some error occurred while unbinding the stub from the business component interface", e);
        } catch (IllegalContentException e) {
            throw new BindingFactoryException(
                    "Some error occurred while removing the stub from the composite containing the component owning the interface to unbind", e);
        }
	}

	// ///////////////////
	// BINDING-CONTROLLER
	// ///////////////////
	/**
	 * @see org.objectweb.fractal.api.control.BindingController#bindFc(java.lang.String,
	 *      java.lang.Object)
	 */
	public void bindFc(String clientItf, Object serverItf)
			throws NoSuchInterfaceException, IllegalBindingException,
			IllegalLifeCycleException {
		if (clientItf.equalsIgnoreCase(PluginResolver.INTERFACE_NAME)) {
			this.pluginResolver = (PluginResolver) serverItf;
			return;
		}

		throw new NoSuchInterfaceException("Cannot find client interface "
				+ clientItf);
	}

	/**
	 * @see org.objectweb.fractal.api.control.BindingController#listFc()
	 */
	public String[] listFc() {
		return new String[] { PluginResolver.INTERFACE_NAME };
	}

	/**
	 * @see org.objectweb.fractal.api.control.BindingController#lookupFc(java.lang.String)
	 */
	public Object lookupFc(String clientItf) throws NoSuchInterfaceException {
		if (clientItf.equalsIgnoreCase(PluginResolver.INTERFACE_NAME)) {
			return this.pluginResolver;
		}
		throw new NoSuchInterfaceException("Cannot find client interface "
				+ clientItf);
	}

	/**
	 * @see org.objectweb.fractal.api.control.BindingController#unbindFc(java.lang.String)
	 */
	public void unbindFc(String clientItf) throws NoSuchInterfaceException,
			IllegalBindingException, IllegalLifeCycleException {
		if (clientItf.equalsIgnoreCase(PluginResolver.INTERFACE_NAME)) {
			this.pluginResolver = null;
			return;
		}
		throw new NoSuchInterfaceException("Cannot find client interface "
				+ clientItf);

	}

}
