package org.objectweb.fractal.bf;

/**
 * Allow registration of listeners
 * 
 * @since 0.7
 */
public interface Register {
	public void addListener(String name, ContextualRequestInformation cri);

	public ContextualRequestInformation removeListener(String name);
}
