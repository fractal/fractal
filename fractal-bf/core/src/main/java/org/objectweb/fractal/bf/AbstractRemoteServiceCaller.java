/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * An abstract service caller. Concrete implementations are used as the content
 * part of stub components, and must take care of protocol-specific details.
 */
public abstract class AbstractRemoteServiceCaller implements InvocationHandler {

	/**
	 * A protocol-dependant proxy, usually implemented as an
	 * {@link InvocationHandler}.
	 */
	protected Object reference;

	/**
	 * @see java.lang.reflect.InvocationHandler#invoke(java.lang.Object,
	 *      java.lang.reflect.Method, java.lang.Object[])
	 */
	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {

		if (this.reference == null) {
			throw new IllegalStateException("Reference field for "
					+ this.getClass() + " is null");
		}

		String methodName = method.getName();

		Object result = null;

		final Method remoteMethod = reference.getClass().getMethod(methodName,
				method.getParameterTypes());

		beforeInvokeHook();

		try {

			result = remoteMethod.invoke(reference, args);

		} catch (InvocationTargetException ite) {
			// rethrows locally the cause of the exception
			Throwable cause = ite.getCause();
			throw cause;
		}

		return result;
	}

	/**
	 * A template method invoked before invoking the method on the reference
	 * field.
	 * 
	 * @param reference
	 */
	protected void beforeInvokeHook() {
		// default impl is empty

	}

}
