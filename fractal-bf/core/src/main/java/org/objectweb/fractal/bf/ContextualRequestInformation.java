package org.objectweb.fractal.bf;

import java.io.Serializable;

/*
 *Temporarely here, should be part of Tinfi 
 */
public interface ContextualRequestInformation {
	Serializable get();

	void set(Serializable v);
}
