package org.objectweb.fractal.bf.testing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.adl.util.ContentControllerHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.bf.BindingFactory;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.BindingFactoryHelper;
import org.objectweb.fractal.bf.PluginResolver;
import org.objectweb.fractal.util.Fractal;

/**
 * Abstract test case that initializes fixture components (that is, components
 * meant to be used as test fixtures).
 * 
 * @author Valerio Schiavoni <valerio.schiavoni@gmail.com>
 * 
 */
public abstract class AbstractBindingFactoryIntegrationTest {

	/**
	 * The name of the client (and server) interface used to access the service.
	 */
	protected static final String SERVICE = "service";

	protected Component pluginResolverComp;

	/**
	 * A composite component enclosing the service component
	 */
	protected Component enclosingService;

	/**
	 * A primitive component exposing a service interface
	 */
	protected Component service;

	/**
	 * A composite component enclosing the client component
	 */
	protected Component enclosingClient;
	/**
	 * A primitive component requesting a service
	 */
	protected Component client;

	protected BindingFactory bindingFactory;

	/**
	 * A map of exportHints
	 */
	protected Map<String, Object> exportHints;

	protected PluginResolver pluginResolverItf;

	protected Factory adlFactory;

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Before
	public void setUp() throws Exception {
		initializeFractalSystemProperties();

		adlFactory = FactoryFactory
				.getFactory(FactoryFactory.FRACTAL_BACKEND);

		pluginResolverComp = (Component) adlFactory.newComponent(
				getPluginResolverAdl(), null);
		assertNotNull(pluginResolverComp);

		Fractal.getLifeCycleController(pluginResolverComp).startFc();

		enclosingService = (Component) adlFactory.newComponent(
				getEnclosingServiceAdlDefinition(), null);
		assertNotNull("Enclosing service should not be null", enclosingService);

		service = ContentControllerHelper.getSubComponentByName(
				enclosingService, "service");
		assertNotNull(
				"The component owning the interface to export should not be null",
				service);

		pluginResolverItf = (PluginResolver) pluginResolverComp
				.getFcInterface(PluginResolver.INTERFACE_NAME);

		bindingFactory = BindingFactoryHelper
				.getBindingFactory(getBindingFactoryConf());
		assertNotNull(bindingFactory);

		enclosingClient = (Component) adlFactory.newComponent(
				getEnclosingClientAdlDefinition(), null);
		assertNotNull(enclosingClient);

		client = ContentControllerHelper.getSubComponentByName(enclosingClient,
				"client");
		assertNotNull(
				"Cannot find 'client' component into the 'enclosingClient' component",
				client);
		exportHints = createExportHints();

	}

	/**
	 * @return
	 */
	protected String getEnclosingClientAdlDefinition() {
		return "org.objectweb.fractal.bf.connectors.EnclosingClient";
	}

	/**
	 * @return
	 */
	protected String getEnclosingServiceAdlDefinition() {
		return "org.objectweb.fractal.bf.connectors.EnclosingService";
	}

	/**
	 * Initialize the map of export hints: concrete test classes will initialize
	 * the map with appropriate values.
	 * 
	 * @return
	 */
	protected abstract Map<String, Object> createExportHints();

	protected abstract Map<String, Object> createBindHints();

	/**
	 * Initialize Fractal required system properties. Default values are:
	 * <ul>
	 * <li>"fractal.provider", "org.objectweb.fractal.koch.Koch"
	 * </ul>
	 */
	protected void initializeFractalSystemProperties() {
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
	}

	protected String getPluginResolverAdl() {
		return "org.objectweb.fractal.bf.PluginResolver";
	}

	protected String getBindingFactoryConf() {
		return "org.objectweb.fractal.bf.BindingFactoryComp";
	}

	/**
	 * Testing the export.
	 * 
	 * @throws NoSuchInterfaceException
	 */
	@Test
	public void testExport() throws NoSuchInterfaceException,
			BindingFactoryException {
		// pre-conditions
		final int nSubComps = Fractal.getContentController(enclosingService)
				.getFcSubComponents().length;
		assertEquals(
				"Before the export, there should be only 1 subcomponent in the enclosing component containing the service component",
				new Integer(1), new Integer(nSubComps));

		bindingFactory.export(service, SERVICE, createExportHints());
		// post-conditions
		final int nSubComsAfterExport = Fractal.getContentController(
				enclosingService).getFcSubComponents().length;
		assertEquals(
				"After the export, there should be 2 subcomponents in the enclosing component containing the service component",
				new Integer(2), new Integer(nSubComsAfterExport));

	}

	@Test
	public void testBind() throws NoSuchInterfaceException {
		// pre-conditions
		final int clientSubComps = Fractal
				.getContentController(enclosingClient).getFcSubComponents().length;
		assertEquals(
				"Before the bind, there should be only 1 subcomponent in the enclosing component containing the service component",
				new Integer(1), new Integer(clientSubComps));

		try {
			bindingFactory.bind(client, SERVICE, createBindHints());
		} catch (BindingFactoryException e) {
			e.printStackTrace();
			fail("Some error occured while binding");
		}

		// post-conditions
		final int clientSubCompsAfterBind = Fractal.getContentController(
				enclosingClient).getFcSubComponents().length;

		assertEquals(
				"After the bind, there should be 2 subcomponents in the enclosing component containing the service component",
				new Integer(2), new Integer(clientSubCompsAfterBind));
	}

	@After
	public void tearDown() throws Exception {

		service = null;
		enclosingService = null;
		client = null;
		enclosingClient = null;

	}

}
