/**
 * Author: Valerio Schiavoni <valerio.schiavoni@gmail.com>
 */
package org.objectweb.fractal.bf.connectors;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This mimics the simpler {@link Service} interface, but it extends it with
 * declared {@link RemoteException} on each method and by extending the
 * {@link Remote} java.rmi interface.
 * 
 */
public interface RemotableService extends Remote {
	void print() throws RemoteException;

	String printAndAnswer() throws RemoteException;;

	javax.xml.datatype.XMLGregorianCalendar getCurrentDate()
			throws RemoteException;

	Pojo getPojo() throws RemoteException;

	void badMethod() throws HelloWorldException, RemoteException;

}
