/**
 * Author: Valerio Schiavoni <valerio.schiavoni@gmail.com>
 */
package org.objectweb.fractal.bf.connectors;

import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import javax.xml.datatype.XMLGregorianCalendar;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.bf.connectors.weird.ServiceWithWeirdParams;

public class BasicClientImpl implements BindingController, Service,
		ServiceWithWeirdParams {

	Logger log = Logger.getLogger(BasicClientImpl.class.getCanonicalName());

	Service service;
	ServiceWithWeirdParams weirdService;

	/**
	 * @see org.objectweb.fractal.api.control.BindingController#bindFc(java.lang.String,
	 *      java.lang.Object)
	 */
	public void bindFc(String arg0, Object arg1)
			throws NoSuchInterfaceException, IllegalBindingException,
			IllegalLifeCycleException {
		if (arg0.equals("service")) {
			this.service = (Service) arg1;
		}
	}

	/**
	 * @see org.objectweb.fractal.api.control.BindingController#listFc()
	 */
	public String[] listFc() {
		return new String[] { "service" };
	}

	/**
	 * @see org.objectweb.fractal.api.control.BindingController#lookupFc(java.lang.String)
	 */
	public Object lookupFc(String arg0) throws NoSuchInterfaceException {

		if (arg0.equals("service")) {
			return this.service;
		}

		return null;
	}

	/**
	 * @see org.objectweb.fractal.api.control.BindingController#unbindFc(java.lang.String)
	 */
	public void unbindFc(String arg0) throws NoSuchInterfaceException,
			IllegalBindingException, IllegalLifeCycleException {
		// TODO Auto-generated method stub

	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.Service#print()
	 */
	public void print() {
		log.info("Client invoking print() on service reference");
		this.service.print();
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.Service#printAndAnswer()
	 */
	public String printAndAnswer() {
		log.info("Client invoking printAndAnswer() on service reference");

		return this.service.printAndAnswer();
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.Service#getCurrentDate()
	 */
	public XMLGregorianCalendar getCurrentDate() {
		log.info("Client going to invoke getCurrentDate on remote service");
		return this.service.getCurrentDate();
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.Service#getPojo()
	 */
	public Pojo getPojo() {
		return this.service.getPojo();
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.Service#child(org.objectweb.fractal.bf.connectors.Pojo)
	 */
	public Pojo child(Pojo p) {
		log.info("Client going to invoke childOf on remote service");
		return this.service.child(p);
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.Service#elaborateBytes(java.util.Properties,
	 *      byte[])
	 */
	public byte[] elaborateBytes(Properties p, byte[] raw) {
		log.info("Client going to invoke elaborateBytes on remote service");
		return this.service.elaborateBytes(p, raw);
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.Service#badMethod()
	 */
	public void badMethod() throws HelloWorldException {
		this.service.badMethod();
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.Service#mapOfMap(java.util.Map)
	 */
	public void mapOfMap(Map<String, Map<String, String>> bigParam) {
		// big param
		this.weirdService.mapOfMap(bigParam);
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.weird.ServiceWithWeirdParams#mapOfMapWithStringAndPojo(java.util.Map)
	 */
	public void mapOfMapWithStringAndPojo(
			Map<String, Map<String, Pojo>> bigParam) {
		this.weirdService.mapOfMapWithStringAndPojo(bigParam);
	}

}
