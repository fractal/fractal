package org.objectweb.fractal.bf.connectors;

import java.io.Serializable;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Properties;

import javax.xml.datatype.XMLGregorianCalendar;

import org.objectweb.fractal.bf.connectors.weird.ServiceWithWeirdParams;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;

public class BasicServiceImpl implements Service, ServiceWithWeirdParams,
		Serializable {

	private static final long serialVersionUID = 5986870616764101057L;
	public static final String SERVICE_REPLY = "Congratulations, Binding Factory Hello World up&running!";

	public void print() {

		System.out.println("Server will print: " + SERVICE_REPLY);

	}

	public String printAndAnswer() {
		System.out.println("Server will print and return " + SERVICE_REPLY);
		return SERVICE_REPLY;
	}

	/**
	 * return the current date
	 * 
	 * @see org.objectweb.fractal.bf.connectors.Service#getCurrentDate()
	 */
	public XMLGregorianCalendar getCurrentDate() {
		final XMLGregorianCalendarImpl result = new XMLGregorianCalendarImpl(
				new GregorianCalendar());
		System.out
				.println("The date sent back to the client by the server is: "
						+ result);
		return result;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.Service#getPojo()
	 */
	public Pojo getPojo() {
		Pojo p1 = new Pojo();
		p1.setId(1);
		p1.setDate(getCurrentDate());
		p1.setName("pojo-1");

		Pojo p2 = new Pojo();
		p2.setId(2);
		p2.setDate(getCurrentDate());
		p2.setName("pojo-2");

		p2.setParent(p1);

		return p2;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.Service#child(org.objectweb.fractal.bf.connectors.Pojo)
	 */
	public Pojo child(Pojo p) {
		System.out.println("Requesting child of pojo obj: " + p);
		Pojo parent = new Pojo();
		parent.setParent(p);
		parent.setId(p.getId() + 1);
		parent.setName("child-of-" + p.getName());
		int parentYear = p.getDate().getYear() - 1;
		XMLGregorianCalendar parentDate = p.getDate();
		parentDate.setYear(parentYear);
		parent.setDate(parentDate);
		System.out.println("Returning parent pojo: " + parent);
		return parent;
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.Service#elaborateBytes(java.util.Properties,
	 *      byte[])
	 */
	public byte[] elaborateBytes(Properties p, byte[] raw) {
		System.out.println("Server received the following properties: " + p);
		System.out.println("will return this class canononical name raw bytes");
		return BasicServiceImpl.class.getCanonicalName().getBytes();
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.Service#badMethod()
	 */
	public void badMethod() throws HelloWorldException {
		throw new HelloWorldException(
				"Service.badMethod() is spitting out this exception");
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.Service#mapOfMap(java.util.Map)
	 */
	public void mapOfMap(Map<String, Map<String, String>> bigParam) {

		System.out.println("***** Server received map of map: " + bigParam);

	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.weird.ServiceWithWeirdParams#mapOfMapWithStringAndPojo(java.util.Map)
	 */
	public void mapOfMapWithStringAndPojo(
			Map<String, Map<String, Pojo>> bigParam) {

		System.out.println("***** Server received map of map (with pojo): "
				+ bigParam);

	}

}
