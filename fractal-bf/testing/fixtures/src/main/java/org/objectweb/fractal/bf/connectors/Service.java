package org.objectweb.fractal.bf.connectors;

import java.util.Properties;

public interface Service {
	void print();

	String printAndAnswer();

	javax.xml.datatype.XMLGregorianCalendar getCurrentDate();

	Pojo getPojo();

	Pojo child(Pojo p);

	byte[] elaborateBytes(Properties p, byte[] raw);

	void badMethod() throws HelloWorldException;

}
