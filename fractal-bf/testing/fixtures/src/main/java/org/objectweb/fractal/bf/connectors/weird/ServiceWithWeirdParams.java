/**
 * Author: Valerio Schiavoni <valerio.schiavoni@gmail.com>
 */
package org.objectweb.fractal.bf.connectors.weird;

import java.util.Map;

import org.objectweb.fractal.bf.connectors.Pojo;

public interface ServiceWithWeirdParams {

	void mapOfMap(Map<String, Map<String, String>> bigParam);

	void mapOfMapWithStringAndPojo(Map<String, Map<String, Pojo>> bigParam);

}
