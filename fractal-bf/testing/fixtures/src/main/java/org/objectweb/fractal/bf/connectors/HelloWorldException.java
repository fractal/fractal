/**
 * Author: Valerio Schiavoni <valerio.schiavoni@gmail.com>
 */
package org.objectweb.fractal.bf.connectors;

/**
 * 
 */
public class HelloWorldException extends RuntimeException {

	private static final long serialVersionUID = 1639813386956271470L;

	/**
	 * 
	 */
	public HelloWorldException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public HelloWorldException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public HelloWorldException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public HelloWorldException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
