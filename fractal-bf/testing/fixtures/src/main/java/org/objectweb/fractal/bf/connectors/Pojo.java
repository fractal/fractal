/**
 * Author: Valerio Schiavoni <valerio.schiavoni@gmail.com>
 */
package org.objectweb.fractal.bf.connectors;

import java.io.Serializable;

import javax.xml.datatype.XMLGregorianCalendar;

/**
 * a simple pojo.
 */
public class Pojo implements Serializable {

	private static final long serialVersionUID = 1481418882118674796L;

	int id;

	String name;

	Pojo parent;

	XMLGregorianCalendar date;

	public final XMLGregorianCalendar getDate() {
		return date;
	}

	public final void setDate(XMLGregorianCalendar date) {
		this.date = date;
	}

	public final int getId() {
		return id;
	}

	public final void setId(int id) {
		this.id = id;
	}

	public final String getName() {
		return name;
	}

	public final void setName(String name) {
		this.name = name;
	}

	public final Pojo getParent() {
		return parent;
	}

	public final void setParent(Pojo parent) {
		this.parent = parent;
	}

	@Override
	public String toString() {
		return "Pojo[id:" + id + ",name:" + name + ",date:" + date + "]";
	}
}
