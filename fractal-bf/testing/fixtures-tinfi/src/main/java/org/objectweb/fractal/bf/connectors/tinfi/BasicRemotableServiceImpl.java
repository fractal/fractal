/**
 * Author: Valerio Schiavoni <valerio.schiavoni@gmail.com>
 */
package org.objectweb.fractal.bf.connectors.tinfi;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.xml.datatype.XMLGregorianCalendar;

/**
 * 
 */
public class BasicRemotableServiceImpl implements RemotableService,
		Serializable {

	private final BasicServiceImpl service;

	public BasicRemotableServiceImpl() {
		this.service = new BasicServiceImpl();
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.RemotableService#getCurrentDate()
	 */
	public XMLGregorianCalendar getCurrentDate() throws RemoteException {
		return this.service.getCurrentDate();
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.RemotableService#getPojo()
	 */
	public Pojo getPojo() throws RemoteException {
		return this.service.getPojo();
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.RemotableService#print()
	 */
	public void print() throws RemoteException {
		this.service.print();
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.RemotableService#printAndAnswer()
	 */
	public String printAndAnswer() throws RemoteException {
		return this.service.printAndAnswer();
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.RemotableService#badMethod()
	 */
	public void badMethod() throws HelloWorldException {
		throw new HelloWorldException();

	}

}
