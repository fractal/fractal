/**
 * Author: Valerio Schiavoni <valerio.schiavoni@gmail.com>
 */
package org.objectweb.fractal.bf.connectors.tinfi;

import java.rmi.RemoteException;

import javax.xml.datatype.XMLGregorianCalendar;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

/**
 * 
 */
public class BasicRemotableClientImpl implements RemotableService,
		BindingController {

	RemotableService service;

	/**
	 * @see org.objectweb.fractal.bf.connectors.RemotableService#getCurrentDate()
	 */
	public XMLGregorianCalendar getCurrentDate() throws RemoteException {
		return this.service.getCurrentDate();
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.RemotableService#getPojo()
	 */
	public Pojo getPojo() throws RemoteException {
		return this.service.getPojo();
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.RemotableService#print()
	 */
	public void print() throws RemoteException {
		this.service.print();
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.RemotableService#printAndAnswer()
	 */
	public String printAndAnswer() throws RemoteException {
		return this.service.printAndAnswer();
	}

	/**
	 * @see org.objectweb.fractal.bf.connectors.RemotableService#badMethod()
	 */
	public void badMethod() throws HelloWorldException, RemoteException {
		this.service.badMethod();
	}

	/**
	 * @see org.objectweb.fractal.api.control.BindingController#bindFc(java.lang.String,
	 *      java.lang.Object)
	 */
	public void bindFc(String arg0, Object arg1)
			throws NoSuchInterfaceException, IllegalBindingException,
			IllegalLifeCycleException {
		if (arg0.equals("service")) {
			this.service = (RemotableService) arg1;
		}
	}

	/**
	 * @see org.objectweb.fractal.api.control.BindingController#listFc()
	 */
	public String[] listFc() {
		return new String[] { "service" };
	}

	/**
	 * @see org.objectweb.fractal.api.control.BindingController#lookupFc(java.lang.String)
	 */
	public Object lookupFc(String arg0) throws NoSuchInterfaceException {

		if (arg0.equals("service")) {
			return this.service;
		}

		return null;
	}

	/**
	 * @see org.objectweb.fractal.api.control.BindingController#unbindFc(java.lang.String)
	 */
	public void unbindFc(String arg0) throws NoSuchInterfaceException,
			IllegalBindingException, IllegalLifeCycleException {
		// TODO Auto-generated method stub

	}

}
