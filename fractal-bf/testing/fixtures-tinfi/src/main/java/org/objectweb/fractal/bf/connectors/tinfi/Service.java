package org.objectweb.fractal.bf.connectors.tinfi;

import java.io.Serializable;
import java.util.Properties;

public interface Service extends Serializable {
	void print();

	String printAndAnswer();

	javax.xml.datatype.XMLGregorianCalendar getCurrentDate();

	Pojo getPojo();

	Pojo child(Pojo p);

	byte[] elaborateBytes(Properties p, byte[] raw);

	void badMethod() throws HelloWorldException;

}
