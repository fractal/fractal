============================================================================
Fractal Binding Factory.
Copyright (C) 2007-2009 INRIA, SARDES

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Contact: fractal@ow2.org

Author: Valerio Schiavoni

============================================================================

Fractal Binding Factory Developer Notes
---------------------------
This document aims at describing how to perform releases for stable and unstable
versions of the Fractal-BF project.

- How to perform a stable release

A stable release should be performed when a sufficient number of new features
and bug fixes have been contributed to the codebase. 
The Fractal-BF uses the maven-release-plugin to perform releases. To perform 
a release, a two-steps process is involved:

1) mvn release:prepare
	* this step checks for unstable dependencies currently being used by the 
	project modules which need to be solved, it checks for uncommitted changes, it asks 
	informations about the version to release, the svn tag to use on the remote repository,
	it tags the SVN repository with the user-provided label,
	and it upgrade the pom.xml files of any module of the 
	project to the next version (to be specified during the process);
	
2) mvn release:perform
	* this steps checkout from the svn the previously tagged version (the one tagged during
	the prepare phase), and it performs a deploy of the resulting artifacts in the
	specified Maven repository. In the case of the Fractal-BF, the official OW2 Maven repositories
	for snapshot and stable releases are used.

For more informations about the release plugin, please see: 
	http://maven.apache.org/plugins/maven-release-plugin/


- How to perform a unstable release

It is possible to release of unstable/snapshot artifacts. This involves a one-step process:

1) mvn deploy
 	* this step executes all tests, and if successful, it deploys produced artifacts in the 
	remote OW2 repository for snapshot artifacts.


