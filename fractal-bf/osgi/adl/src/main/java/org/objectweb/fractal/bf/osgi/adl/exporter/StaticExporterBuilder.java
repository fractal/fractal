/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 Bull S.A.S.
 * Copyright (C) 2009 INRIA, SARDES
 * Contact: fractal@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.objectweb.fractal.bf.osgi.adl.exporter;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.adl.common.Parameter;
import org.objectweb.fractal.bf.adl.exporter.ExporterBuilder;

/**
 * Exporter builder meant to be used with the static backed of the Fractal-BF ADL on OSGi.
 * @author Loris Bouzonnet
 */
public class StaticExporterBuilder implements ExporterBuilder {

    Logger log = Logger.getLogger(StaticExporterBuilder.class
            .getCanonicalName());

    /**
     * @see org.objectweb.fractal.bf.adl.exporter.ExporterBuilder#export(java.lang.String,
     *      java.lang.Object, java.util.List, java.lang.String,
     *      java.lang.Object)
     */
    public void export(String itfName, Object component,
            List<Parameter> parameters, String type, Object context)
            throws BindingFactoryException {
        PrintWriter pw = (PrintWriter) ((Map<?, ?>) context).get("printwriter");
        log.finest("PrintWriter for static exporter builder: " + pw);

        String hintsName = Integer.toString(Math.abs(component.hashCode())) + Integer.toString(Math.abs(itfName.hashCode()));
        
        pw.write(Map.class.getCanonicalName()
                + "<String,Object> exportHints" + hintsName + " = new "
                + HashMap.class.getCanonicalName() + "<String,Object>();\n");
        for (Parameter p : parameters) {;
            pw.write("exportHints" + hintsName + ".put(\"" + p.getName() + "\",\""
                    + p.getValue() + "\");\n");
        }
        pw.write("exportHints" + hintsName + ".put(org.objectweb.fractal.bf.BindingFactoryImpl.PLUGIN_ID, \"" + type + "\");\n");
        pw.write("org.objectweb.fractal.bf.osgi.BindingFactoryHelper.getBindingFactory().export("
        		+ component + ", \"" + itfName + "\", exportHints" + hintsName + ");\n");
    }

}
