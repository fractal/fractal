/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 Bull S.A.S.
 * Copyright (C) 2009 INRIA, SARDES
 * Contact: fractal@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.objectweb.fractal.bf.osgi;

import org.objectweb.fractal.bf.BindingFactory;
import org.osgi.framework.ServiceReference;

/**
 * Helper to retrieve the {@link BindingFactory} as an OSGi service.
 * @author Loris Bouzonnet
 */
public final class BindingFactoryHelper {

    /**
     * Helper class: no public constructor.
     */
    private BindingFactoryHelper() {
        // Do nothing
    }

    /**
     * Retrieve the binding factory service through the bundle context.
     * @return the binding factory service.
     */
    public static BindingFactory getBindingFactory() {
        if (Activator.bundleContext == null) {
            throw new IllegalStateException("The Binding Factory is not started!");
        }
        ServiceReference ref =
            Activator.bundleContext.getServiceReference(BindingFactory.class.getName());
        return (BindingFactory) Activator.bundleContext.getService(ref);
    }

}
