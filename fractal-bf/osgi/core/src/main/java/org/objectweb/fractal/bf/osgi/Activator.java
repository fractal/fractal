/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 Bull S.A.S.
 * Copyright (C) 2009 INRIA, SARDES
 * Contact: fractal@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.objectweb.fractal.bf.osgi;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Activator to statically retrieve the {@link BundleContext}.
 * @author Loris Bouzonnet
 */
public final class Activator implements BundleActivator {

    /**
     * Share the bundle context to the {@link BindingFactoryHelper}.
     */
    static BundleContext bundleContext;

    public void start(final BundleContext bundleContext) throws Exception {
        Activator.bundleContext = bundleContext;
    }

    public void stop(final BundleContext bundleContext) throws Exception {
        Activator.bundleContext = null;
    }

}
