/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 Bull S.A.S.
 * Copyright (C) 2009 INRIA, SARDES
 * Contact: fractal@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.objectweb.fractal.bf.osgi;

import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.bf.BindHints;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.BindingFactoryPlugin;
import org.objectweb.fractal.bf.ExportHints;
import org.objectweb.fractal.bf.SkeletonGenerationException;
import org.objectweb.fractal.bf.StubGenerationException;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;

/**
 * @author Loris Bouzonnet
 */
public class BindingFactoryPluginListener<E extends ExportHints, B extends BindHints> implements
		BindingFactoryPlugin<E, B>, ServiceListener {
	
	private BindingFactoryPlugin<E, B> bfPlugin;

    private ServiceReference serviceReference;

    private final Lock readLock;

    private final Lock writeLock;

    private final BundleContext context;
	
    @SuppressWarnings("unchecked")
	public BindingFactoryPluginListener(final BundleContext context,
            final ServiceReference serviceReference) {
    	this.serviceReference = serviceReference;
        this.context = context;

        if (serviceReference != null) {
        	bfPlugin = (BindingFactoryPlugin<E, B>) context.getService(serviceReference);
        }
        ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
        readLock = readWriteLock.readLock();
        writeLock = readWriteLock.writeLock();
	}
    
	@SuppressWarnings("unchecked")
	public void serviceChanged(ServiceEvent event) {
		switch (event.getType()) {
        case ServiceEvent.REGISTERED:
            writeLock.lock();
            try {
                if (bfPlugin == null) {
                    serviceReference = event.getServiceReference();
                    bfPlugin =
                            (BindingFactoryPlugin<E, B>) context.getService(serviceReference);
                }
            } finally {
                writeLock.unlock();
            }
            break;
        case ServiceEvent.UNREGISTERING:
            writeLock.lock();
            bfPlugin = null;
            writeLock.unlock();
            serviceReference = null;
            break;
        }
	}

	public Component createSkel(String interfaceName, Object interfaceToExport,
			Component owner, E exportHints) throws SkeletonGenerationException {
		try {
            readLock.lock();
            if (bfPlugin == null) {
                throw new IllegalStateException(
                        "None binding factory plugin available");
            }
            return bfPlugin.createSkel(interfaceName, interfaceToExport, owner, exportHints);
        } finally {
            readLock.unlock();
        }
	}

	public Component createStub(Component client, String itfname, B hints)
			throws StubGenerationException {
		try {
            readLock.lock();
            if (bfPlugin == null) {
                throw new IllegalStateException(
                        "None binding factory plugin available");
            }
            return bfPlugin.createStub(client, itfname, hints);
        } finally {
            readLock.unlock();
        }
	}

	public void finalizeSkeleton(Component skel,
			Map<String, String> finalizationHints)
			throws BindingFactoryException {
		try {
            readLock.lock();
            if (bfPlugin == null) {
                throw new IllegalStateException(
                        "None binding factory plugin available");
            }
            bfPlugin.finalizeSkeleton(skel, finalizationHints);
        } finally {
            readLock.unlock();
        }
	}

	public void finalizeStub(Component stub, Map<String, Object> hints)
			throws BindingFactoryException {
		try {
            readLock.lock();
            if (bfPlugin == null) {
                throw new IllegalStateException(
                        "None binding factory plugin available");
            }
            bfPlugin.finalizeStub(stub, hints);
        } finally {
            readLock.unlock();
        }
	}

	public B getBindHints(Map<String, Object> initialHints) {
		try {
            readLock.lock();
            if (bfPlugin == null) {
                throw new IllegalStateException(
                        "None binding factory plugin available");
            }
            return bfPlugin.getBindHints(initialHints);
        } finally {
            readLock.unlock();
        }
	}

	public BindingFactoryClassLoader getBindingFactoryClassLoader() {
		try {
            readLock.lock();
            if (bfPlugin == null) {
                throw new IllegalStateException(
                        "None binding factory plugin available");
            }
            return bfPlugin.getBindingFactoryClassLoader();
        } finally {
            readLock.unlock();
        }
	}

	public E getExportHints(Map<String, Object> initialHints) {
		try {
            readLock.lock();
            if (bfPlugin == null) {
                throw new IllegalStateException(
                        "None binding factory plugin available");
            }
            return bfPlugin.getExportHints(initialHints);
        } finally {
            readLock.unlock();
        }
	}

	public String getPluginIdentifier() {
		try {
            readLock.lock();
            if (bfPlugin == null) {
                throw new IllegalStateException(
                        "None binding factory plugin available");
            }
            return bfPlugin.getPluginIdentifier();
        } finally {
            readLock.unlock();
        }
	}

	public void setBindingFactoryClassLoader(BindingFactoryClassLoader cl) {
		try {
            readLock.lock();
            if (bfPlugin == null) {
                throw new IllegalStateException(
                        "None binding factory plugin available");
            }
            bfPlugin.setBindingFactoryClassLoader(cl);
        } finally {
            readLock.unlock();
        }
	}

}
