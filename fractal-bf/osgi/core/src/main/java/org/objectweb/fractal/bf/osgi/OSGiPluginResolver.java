/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 Bull S.A.S.
 * Copyright (C) 2009 INRIA, SARDES
 * Contact: fractal@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.objectweb.fractal.bf.osgi;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.bf.BindHints;
import org.objectweb.fractal.bf.BindingFactoryPlugin;
import org.objectweb.fractal.bf.ExportHints;
import org.objectweb.fractal.bf.PluginNotFoundException;
import org.objectweb.fractal.bf.PluginResolver;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.ow2.fractal.julia.osgi.api.control.OSGiContextController;

/**
 * In OSGi, BF plugin are accessed through services.
 * @author Loris Bouzonnet
 */
public class OSGiPluginResolver implements PluginResolver, OSGiContextController {

    /**
     * Logger.
     */
    private static Logger log = Logger.getLogger(OSGiPluginResolver.class.getName());

    /**
     * Bundle context.
     * Set at bundle startup by the Julia OSGi Launcher.
     */
    private BundleContext context;

    /**
	 * The plugins loaded.
	 */
	private final Map<String, BindingFactoryPlugin<ExportHints, BindHints>> plugins;
	
	public OSGiPluginResolver() {
		this.plugins = new HashMap<String, BindingFactoryPlugin<ExportHints, BindHints>>();
	}
    
    public BindingFactoryPlugin<ExportHints, BindHints> resolvePlugin(final String pluginId)
            throws PluginNotFoundException {

        log.fine("Resolving plugin with identifier:'" + pluginId + "'");
        
        BindingFactoryPlugin<ExportHints, BindHints> plugin = plugins.get(pluginId);
		if (plugin != null) {
			return plugin;
		}

        if (context == null) {
            log.log(Level.SEVERE, "Bundle context missing");
            throw new PluginNotFoundException("Bundle context missing");
        }

        String filter = "(plugin=" + pluginId + ")";

        // TODO return a proxy
        ServiceReference[] refs;
        try {
            refs = context.getServiceReferences("org.objectweb.fractal.bf.BindingFactoryPlugin", filter);
        } catch (InvalidSyntaxException e) {
            log.log(Level.SEVERE, "Bad filters: " + filter, e);
            throw new PluginNotFoundException("Bad filters: " + filter, e);
        }
        ServiceReference ref = null;
        if (refs == null) {
            log.log(Level.WARNING, "No plugin for " + filter);
        } else {
        	ref = refs[0];
        	if (refs.length > 1) {
        		log.warning("Many plugins for " + filter + " are available: using the first one of " + Arrays.toString(refs));
        	}
        }
        plugin = new BindingFactoryPluginListener<ExportHints, BindHints>(context, ref);
        plugins.put(pluginId, plugin);
        
        return plugin;
    }

    public BundleContext getBundleContext() {
        return context;
    }

    public void setBundleContext(final BundleContext context) {
        this.context = context;
    }

}
