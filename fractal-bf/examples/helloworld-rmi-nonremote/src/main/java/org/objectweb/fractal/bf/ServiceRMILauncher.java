/**
 * Author: Valerio Schiavoni <valerio.schiavoni@gmail.com>
 */
package org.objectweb.fractal.bf;

import java.util.HashMap;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.util.Fractal;

/**
 * Start the server, whose 'service' interface is being exported via the RMI
 * plugin. For some reason, starting the same example using the FractalADL
 * launcher class doesn't seem to work.
 */
public class ServiceRMILauncher {

	public static void main(String[] args) throws ADLException,
			IllegalLifeCycleException, NoSuchInterfaceException {

		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
		final HashMap<Object, Object> ctx = new HashMap<Object, Object>();
		Factory adlFactory = FactoryFactory.getFactory(
				"org.objectweb.fractal.bf.adl.MyBasicFactory",
				"org.objectweb.fractal.bf.adl.MyFractalBackend", ctx);

		Component serviceAndRmiExporter = (Component) adlFactory.newComponent(
				"org.objectweb.fractal.bf.ServiceRMI", ctx);
		Fractal.getLifeCycleController(serviceAndRmiExporter).startFc();
	}
}
