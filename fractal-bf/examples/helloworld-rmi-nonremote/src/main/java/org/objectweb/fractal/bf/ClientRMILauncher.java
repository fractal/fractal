/**
 * Author: Valerio Schiavoni <valerio.schiavoni@gmail.com>
 */
package org.objectweb.fractal.bf;

import java.util.HashMap;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.util.Fractal;

public class ClientRMILauncher {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
		final HashMap<Object, Object> ctx = new HashMap<Object, Object>();
		Factory adlFactory = FactoryFactory.getFactory(
				"org.objectweb.fractal.bf.adl.MyBasicFactory",
				"org.objectweb.fractal.bf.adl.MyFractalBackend", ctx);

		Component client = (Component) adlFactory.newComponent(
				"org.objectweb.fractal.bf.ClientRMI", ctx);
		Fractal.getLifeCycleController(client).startFc();

		((Runnable) client.getFcInterface("r")).run();

	}
}
