package org.objectweb.fractal.bf;

import java.util.Properties;

/**
 * A simple printing-on-demand service. It extends Service to be reused with the
 * different plugins.
 * 
 * @author valerio.schiavoni@gmail.com
 * 
 */
public interface Service {
	/**
	 * Print some message
	 * 
	 */
	void print();

	/**
	 * Return a message to the printing-service user.
	 * 
	 * @return a string to be print.
	 */
	String printAndAnswer();

	byte[] getBytes(Properties p, byte[] b);

	void badMethod() throws HelloWorldException;

}
