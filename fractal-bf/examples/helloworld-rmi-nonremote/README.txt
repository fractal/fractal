=== Fractal-BF HelloWorld-RMI without remote interfaces ===

This version of HelloWorld uses the fractal-bf-connectors-rmi plugin to expose
the designated interface by the JavaRMI protocol. 
The peculiarity of this example is that the interfaces are not subclasses of java.rmi.Remote.

== HOW TO BUILD ==

This example builds two executable jar files, one for the client and one for the server.

Execute the following to build the executable jar file for the server:

   mvn package -Ppack.server

Execute the following to build the executable jar file for the client:

   mvn package -Ppack.client


== HOW TO LAUNCH ==
 
These are the required steps to execute this example:

0) Open a console and issue the following:
   rmiregistry 9901 

where 9901 is the port number the registry will wait for connections at. If you
need to change this value, you'll need to modify the .fractal files accordingly.
If you want to see more debug from the RMI registry:
   rmiregistry 9901  -J-Dsun.rmi.loader.logLevel=VERBOSE

1)  This example assumes that you've previously executed the steps explained in the section "How to build". Type
the following to run the server:
  java -jar target/fractal-bf-helloworld-rmi-server-jar-with-dependencies.jar

If everything went fine, you should see something like the following:

INFO: Exporter will export interface 'service' of component 'service'
INFO: Resolving plugin with identifier:'rmi'
INFO: Loading plugin from ADL file: org.objectweb.fractal.bf.connectors.rmi.RmiConnector
INFO: RmiConnector - Creating skeleton.
INFO: Export hints being used to create the skeleton: 
 serviceName: print
 host:127.0.0.1
 port:9901
INFO: RmiConnector - skeleton created.
AVVERTENZA: RMI FinalizationHints map currently ignored
HTTP Server(http://192.168.0.1:60562/): Ready
INFO: System property: java.rmi.server.codebase ->http://192.168.0.1:60562/
HTTP Server(http://192.168.0.1:60562/): GET org/objectweb/fractal/bf/ServiceRemotedImpl.class 932
HTTP Server(http://192.168.0.1:60562/): GET org/objectweb/fractal/bf/ServiceRemoted.class 377
HTTP Server(http://192.168.0.1:60562/): GET org/objectweb/fractal/bf/Service.class 333
HTTP Server(http://192.168.0.1:60562/): GET org/objectweb/fractal/bf/ServiceImpl.class 1344
HTTP Server(http://192.168.0.1:60562/): GET org/objectweb/fractal/bf/HelloWorldException.class 866
INFO: BindingFactory successfully exported interface service owned by component [service] with the following parameters: {itfName=service, export.mode=rmi, hostAddress=127.0.0.1, serviceName=print, port=9901}
   
If you need to modify parameters like the service name, the port or the host address for the registry, you can edit the file: 
   src/main/resources/org/objectweb/fractal/bf/ServiceRMI.fractal 

2) Type the following to run the client:
  java -Djava.security.manager -Djava.security.policy=my.policy -jar target/fractal-bf-helloworld-rmi-client-jar-with-dependencies.jar 
  
If everything went fine, you should see something like the following:
INFO: Binder will bind interface 'service' of component 'client'
Warning: Mandatory client interface 'client.service is not bound (org/objectweb/fractal/bf/ClientRMI.fractal:5)
INFO: Resolving plugin with identifier:'rmi'
INFO: Loading plugin from ADL file: org.objectweb.fractal.bf.connectors.rmi.RmiConnector
INFO: Creating stub for component org.objectweb.fractal.julia.generated.C5929b02d_0@745fd9e1,interface:service,bindHints:{export.mode=rmi, hostAddress=127.0.0.1, serviceName=print, port=9901}
AVVERTENZA: The stub doesn't need to be finalized.
Hello BindingFactory
Server said: Message received from the Service component
ServiceImpl received properties: {k=v} and bytes:[B@a16e60
bytes received from service: [B@a16e60
Client received a HelloWorldException,now printing stacktrace
org.objectweb.fractal.bf.HelloWorldException
	at org.objectweb.fractal.bf.ServiceImpl.badMethod(ServiceImpl.java:44)
	at org.objectweb.fractal.julia.generated.C6df6466a_0.badMethod(INTERCEPTOR[Service])
	at org.objectweb.fractal.julia.generated.Cf4faeeef_0.badMethod(INTERFACE[Service])
	at org.objectweb.fractal.bf.Service$$BindingFactoryGeneratedImpl.badMethod(Unknown Source)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
	at java.lang.reflect.Method.invoke(Method.java:585)
	at sun.rmi.server.UnicastServerRef.dispatch(UnicastServerRef.java:294)
	at sun.rmi.transport.Transport$1.run(Transport.java:153)
	at java.security.AccessController.doPrivileged(Native Method)
	at sun.rmi.transport.Transport.serviceCall(Transport.java:149)
	at sun.rmi.transport.tcp.TCPTransport.handleMessages(TCPTransport.java:466)
	at sun.rmi.transport.tcp.TCPTransport$ConnectionHandler.run(TCPTransport.java:707)
	at java.lang.Thread.run(Thread.java:613)
	at sun.rmi.transport.StreamRemoteCall.exceptionReceivedFromServer(StreamRemoteCall.java:247)
	at sun.rmi.transport.StreamRemoteCall.executeCall(StreamRemoteCall.java:223)
	at sun.rmi.server.UnicastRef.invoke(UnicastRef.java:126)
	at java.rmi.server.RemoteObjectInvocationHandler.invokeRemoteMethod(RemoteObjectInvocationHandler.java:179)
	at java.rmi.server.RemoteObjectInvocationHandler.invoke(RemoteObjectInvocationHandler.java:132)
	at $Proxy0.badMethod(Unknown Source)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
	at java.lang.reflect.Method.invoke(Method.java:585)
	at org.objectweb.fractal.bf.AbstractRemoteServiceCaller.invoke(AbstractRemoteServiceCaller.java:43)
	at $Proxy1.badMethod(Unknown Source)
	at org.objectweb.fractal.julia.generated.C6df6466a_0.badMethod(INTERCEPTOR[Service])
	at org.objectweb.fractal.julia.generated.Cf4faeeef_0.badMethod(INTERFACE[Service])
	at org.objectweb.fractal.bf.Client.run(Client.java:31)
	at org.objectweb.fractal.julia.generated.Cdb531190_0.run(INTERCEPTOR[Runnable])
	at org.objectweb.fractal.julia.generated.Ca8e8ed5_0.run(INTERFACE[Runnable])
	at org.objectweb.fractal.bf.ClientRMILauncher.main(ClientRMILauncher.java:35)


Please note that the exception at end is intended to show how custom exceptions are properly propagated through the bindings.