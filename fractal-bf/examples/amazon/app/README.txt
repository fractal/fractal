This example shows how to use AWS Affiliate web services with Fractal, FractalADL, and
the Binding Factory.

The code is *greatly* inspired by an analougous code by Glen Mazza, which can be found at:
http://www.jroller.com/gmazza/entry/creating_soap_clients_for_the

You must obtain an AWS affiliate key to run this example. You can find more informations
about this subject at:
https://aws-portal.amazon.com/gp/aws/developer/registration/index.html

To run this example:
1) Open file pom.xml, and check line 98
2) Execute:
mvn -Prun.client 

To tune logging, edit the logging.properties file in this directory.

If everything worked as expected, you should see a list of books with relevant informations.