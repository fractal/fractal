/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf;

import java.util.List;

import org.objectweb.fractal.api.control.BindingController;

import com.amazon.webservices.awsecommerceservice._2009_02_01.AWSECommerceServicePortType;
import com.amazon.webservices.awsecommerceservice._2009_02_01.Item;
import com.amazon.webservices.awsecommerceservice._2009_02_01.ItemAttributes;
import com.amazon.webservices.awsecommerceservice._2009_02_01.ItemSearch;
import com.amazon.webservices.awsecommerceservice._2009_02_01.ItemSearchRequest;
import com.amazon.webservices.awsecommerceservice._2009_02_01.ItemSearchResponse;
import com.amazon.webservices.awsecommerceservice._2009_02_01.Items;
import com.amazon.webservices.awsecommerceservice._2009_02_01.Offer;
import com.amazon.webservices.awsecommerceservice._2009_02_01.Offers;
import com.amazon.webservices.awsecommerceservice._2009_02_01.Price;

/**
 * Simple client for Amazon web service whose WSDL is
 * http://webservices.amazon.com/AWSECommerceService/AWSECommerceService.wsdl
 */
public class ClientAmazon implements Runnable, BindingController,
		ClientAmazonAttributes {

	/**
	 * Client interface: implementation is bound by the Binding Factory
	 */
	AWSECommerceServicePortType amazon;
	/**
	 * AWS key to access Amazon web services.
	 */
	String awskey;

	/**
	 * See {@link BindingController#listFc()}
	 */
	public String[] listFc() {
		return new String[] { "amazon" };
	}

	/**
	 * See {@link BindingController#lookupFc(String)}
	 */
	public Object lookupFc(final String cItf) {
		if (cItf.equals("amazon")) {
			return amazon;
		}
		return null;
	}

	/**
	 * See {@link BindingController#bindFc(String, Object)}
	 */
	public void bindFc(final String cItf, final Object sItf) {
		if (cItf.equals("amazon")) {
			amazon = (AWSECommerceServicePortType) sItf;
			return;
		}
	}

	/**
	 * See {@link BindingController#unbindFc(String)}
	 */
	public void unbindFc(final String cItf) {
		if (cItf.equals("amazon")) {
			amazon = null;
		}
	}

	/**
	 * Same business code of Glen Mazza's blog entry:
	 * http://www.jroller.com/gmazza/entry/creating_soap_clients_for_the
	 */
	public void run() {

		ItemSearch itemSearch = new ItemSearch();
		String key = getAWSAccessKeyId();
		if (key == null
				|| key.equalsIgnoreCase("")
				|| key
						.equalsIgnoreCase("**CHANGE_THIS_VALUE_TO_SOMETHING_ELSE**")) {
			System.err
					.println("You didn't specify a valid AWS key. Check the pom.xml, line 98.");
			System.exit(-1);
		}
		System.err.println("AWS key: " + key);
		itemSearch.setAWSAccessKeyId(key);

		ItemSearchRequest iSReq = new ItemSearchRequest();
		iSReq.setKeywords("Macintosh Mac OSX");
		iSReq.setSearchIndex("Books");

		iSReq.getResponseGroup().add("ItemAttributes");
		iSReq.getResponseGroup().add("Offers");
		iSReq.getResponseGroup().add("OfferFull");

		List list = itemSearch.getRequest();
		list.add(iSReq);
		ItemSearchResponse itemSearchResponse = amazon.itemSearch(itemSearch);

		List itemsList = itemSearchResponse.getItems();
		Items myItems = (Items) itemsList.get(0);
		List<Item> itemList = myItems.getItem();
		for (Item singleItem : itemList) {
			ItemAttributes ia = singleItem.getItemAttributes();
			System.out.println("\n\nASIN:  " + singleItem.getASIN());
			System.out.println("Title:  " + ia.getTitle());
			System.out.println("Author:  " + ia.getAuthor());
			System.out.println("Manufacturer:  " + ia.getManufacturer());
			Price lp = ia.getListPrice();
			if (lp != null) {
				System.out.println("List Price: " + lp.getFormattedPrice());
			}

			Offers offers = singleItem.getOffers();
			List<Offer> offerList = offers.getOffer();
			for (Offer ofr : offerList) {
				System.out.println("Merchant ID: "
						+ ofr.getMerchant().getName());
				System.out.println("Regular Price: "
						+ ofr.getOfferListing().get(0).getPrice()
								.getFormattedPrice());
				Price salePrice = ofr.getOfferListing().get(0).getSalePrice();
				if (salePrice != null) {
					System.out.println("Sale Price: "
							+ salePrice.getFormattedPrice());
				}
			}
		}

	}

	/**
	 * @see org.objectweb.fractal.bf.ClientAmazonAttributes#getAWSAccessKeyId()
	 */
	public String getAWSAccessKeyId() {
		return this.awskey;
	}

	/**
	 * @see org.objectweb.fractal.bf.ClientAmazonAttributes#setAWSAccessKeyId(java.lang.String)
	 */
	public void setAWSAccessKeyId(String key) {
		this.awskey = key;

	}
}
