package org.objectweb.fractal.bf;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Properties;

/**
 * A simple implementation of the {@link Service} interface.
 * 
 * @author valerio.schiavoni@gmail.com
 * 
 */
public class ServiceImpl implements Service, Serializable {
	/**
	 * Print a nice message.
	 */
	public void print() throws RemoteException {

		System.out.println("Hello BindingFactory");

	}

	/**
	 * Return a nice message.
	 */
	public String printAndAnswer() throws RemoteException {

		return "Message received from the Service component";
	}

	/**
	 * @see org.objectweb.fractal.bf.Service#getBytes(java.util.Properties,
	 *      byte[])
	 */
	public byte[] getBytes(Properties p, byte[] b) throws RemoteException {
		System.out.println("ServiceImpl received properties: " + p
				+ " and bytes:" + b);
		return b;
	}

	/**
	 * @see org.objectweb.fractal.bf.Service#badMethod()
	 */
	public void badMethod() throws HelloWorldException {
		throw new HelloWorldException();
	}

}
