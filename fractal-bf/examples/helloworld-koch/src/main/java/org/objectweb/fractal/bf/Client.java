package org.objectweb.fractal.bf;

import java.rmi.RemoteException;
import java.util.Properties;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;

/**
 * A simple implementation of {@link Main} and {@link BindingController}
 * interfaces.
 * 
 * @author valerio.schiavoni@gmail.com
 * 
 */
public class Client implements Runnable, BindingController {

	private Service service;

	/**
	 * @see java.lang.Runnable#run()
	 */
	public void run() {

		try {
			service.print();
			System.out.println("Server said: " + service.printAndAnswer());
			final Properties p = new Properties();
			p.put("k", "v");
			byte[] b = service.getBytes(p, "hello".getBytes());
			System.out.println("bytes received from service: " + b);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		try {
			try {
				service.badMethod();
			} catch (RemoteException e) {
				// this should not happen
			}
		} catch (HelloWorldException e) {
			System.out
					.println("Client received a HelloWorldException,now printing stacktrace");
			e.printStackTrace();
		}
	}

	// ** BINDING-CONTROLLER IMPL

	/**
	 * See {@link BindingController#listFc()}
	 */
	public String[] listFc() {
		return new String[] { "service" };
	}

	/**
	 * See {@link BindingController#lookupFc(String)}
	 * 
	 * @throws NoSuchInterfaceException
	 */
	public Object lookupFc(final String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("service")) {
			return service;
		}
		throw new NoSuchInterfaceException(cItf);
	}

	/**
	 * See {@link BindingController#bindFc(String, Object)}
	 * 
	 * @throws NoSuchInterfaceException
	 */
	public void bindFc(final String cItf, final Object sItf)
			throws NoSuchInterfaceException {
		if (cItf.equals("service")) {
			service = (Service) sItf;
			return;
		}
		throw new NoSuchInterfaceException(cItf);
	}

	/**
	 * See {@link BindingController#unbindFc(String)}
	 * 
	 * @throws NoSuchInterfaceException
	 */
	public void unbindFc(final String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("service")) {
			service = null;
			return;
		}
		throw new NoSuchInterfaceException(cItf);
	}

}
