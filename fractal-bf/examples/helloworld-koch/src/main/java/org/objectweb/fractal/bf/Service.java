package org.objectweb.fractal.bf;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Properties;

/**
 * A simple printing-on-demand service. It extends Service to be reused with the
 * different plugins.
 * 
 * @author valerio.schiavoni@gmail.com
 * 
 */
public interface Service extends Remote {
	/**
	 * Print some message
	 * 
	 */
	void print() throws RemoteException;

	/**
	 * Return a message to the printing-service user.
	 * 
	 * @return a string to be print.
	 */
	String printAndAnswer() throws RemoteException;

	byte[] getBytes(Properties p, byte[] b) throws RemoteException;

	void badMethod() throws HelloWorldException, RemoteException;

}
