=== Fractal-BF HelloWorld - KOCH ===

This is the HelloWorld example for the Fractal-BindingFactory project that uses 
the Koch fractal provider instead of Julia.

== Soap-CXF Plugin ==

This version of HelloWorld uses the fractal-bf-connectors-soap-cxf plugin to expose the server interface of the service component
as a Web Service accessible via external clients (in this case, the client component).

To run the server, issue the following:

1)  mvn -Prun.serverws

This will instantiate and run the server component, exporting its server interface 'service' as a Web Service. T
If everything went fine, the WSDL for the web service should be accessible at this address :
  
   http://localhost:8080/Service?wsdl. 
   
If you need to modify this address, you can edit the file: 
   src/main/resources/org/objectweb/fractal/bf/ServiceWS.fractal 

2) mvn -Prun.clientws

will instantiate and run the client component: a distant binding between the
client and the service will be instantiated by the Binding Factory.
The client will then invoke the remote service.