/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 INRIA, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf;

/**
 * A simple implementation of the {@link HelloWorld} interface.
 * 
 * @author Philippe Merle
 */
public class HelloWorldImpl implements HelloWorld {

	/**
	 * Sleep and return a nice message.
	 */
	public String sayHello(String who, int seconds) {
		System.out.println("HelloWorldImpl - Sleeping " + seconds + " for " + who + "...");
		try { Thread.sleep(seconds * 1000); } catch(Exception e) {}
		final String result = "Hello " + who + " after " + seconds + " seconds";
		System.out.println("HelloWorldImpl - Return '" + result + "'");
		return result;
	}
}
