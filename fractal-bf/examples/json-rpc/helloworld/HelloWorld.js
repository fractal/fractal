function onLoad()
{
  try {
    jsonrpc = new JSONRpcClient("http://localhost:8080/JSON-RPC");
  } catch(e) {
    alert(e);
  }
}

function clickHello()
{
  try {
    var whoNode = document.getElementById("who");
    var secondsNode = document.getElementById("seconds");
    var result = jsonrpc.helloworld.sayHello(whoNode.value, secondsNode.value);
    alert("The server replied: " + result);
  } catch(e) {
    alert(e);
  }
}

var request_id = 0

function clickAsynchronousHello()
{
  try {
    var whoNode = document.getElementById("who");
    var secondsNode = document.getElementById("seconds");
    jsonrpc.helloworld.sayHello(callbackSayHello, whoNode.value + " #" + request_id, secondsNode.value);
    request_id++;
  } catch(e) {
    alert(e);
  }
}

function callbackSayHello(result, exception) {
    if(exception) {
        alert(exception.message);
    } else {
        alert("The server replied: " + result);
    }
}

function clickFractal()
{
// TODO: This part must be extended to do more Fractal introspection.
try {
  var h = jsonrpc.helloworld;
  var t = h.getFcItfType();
  var c = h.getFcItfOwner();
  alert("helloworld=" + h + " getFcItfName=" + h.getFcItfName() + " isFcInternalInterface=" + h.isFcInternalItf() + " getFcItfType=" + t + " getFcOwner()=" + c);

//  var i = c.getFcInterfaces();
//  alert(c + " " + i);

} catch(e) { alert(e); }
}
