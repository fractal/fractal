=== Fractal-BF JSON-RPC HelloWorld Example ===

This is an example of the usage of the JSON-RPC plugin for the Fractal-BindingFactory project. 
It exposes a Fractal server interface via the JSON-RPC protocol, and provides an HTML page to 
invoke the exposed service.

To run the server, issue the following:

1)  mvn -Prun.server

This will instantiate and run the Fractal server component, exporting its server interface
'helloworld' via JSON-RPC accessible at this address:
  
   http://localhost:8080/JSON-RPC/helloworld

 It is possible to customize the address through which the JSON-RPC service is exposed, 
 by editing the .fractal file:
   src/main/resources/org/objectweb/fractal/bf/ServerJSONRPC.fractal

To run the clients, issue the following:

2) mvn -Prun.client

This will instantiate, run the client component, and establish a JSON-RPC binding between
the client interface 'helloworld' of the client component to the JSON-RPC service.
The client will then invoke the remote service.

3) Open your web browser and load the page HelloWorld.html

The first button invokes sayHello synchronously and the the second button invokes
sayHello asynchronously. The last button introspects the exposed Fractal server interface.

Warning: This page works on Safari and Eclipse web browsers but not on Firefox and Opera
due to security constraints.
