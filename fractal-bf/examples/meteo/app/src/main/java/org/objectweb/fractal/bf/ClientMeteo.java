/**
 * Author: Valerio Schiavoni <valerio.schiavoni@gmail.com>
 */
package org.objectweb.fractal.bf;

import net.webservicex.GlobalWeatherSoap;

import org.objectweb.fractal.api.control.BindingController;

/**
 * A simple web service client.
 */
public class ClientMeteo implements Runnable, BindingController {

	private GlobalWeatherSoap meteo;

	/**
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		System.out.println("Entering: ClientMeteo.run()");

		String meteoGrenoble = meteo.getWeather("grenoble", "france");
		System.out.println("Meteo in Grenoble: \n" + meteoGrenoble);

		String meteoRomaUrbe = meteo.getWeather("urbe", "italy");
		System.err.println("Meteo in Rome: \n" + meteoRomaUrbe);

		Object citiesInFrance = meteo.getCitiesByCountry("france");
		System.err.println("Cities in france: \n" + citiesInFrance);

	}

	// ** BINDING-CONTROLLER IMPL

	/**
	 * See {@link BindingController#listFc()}
	 */
	public String[] listFc() {
		return new String[] { "meteo" };
	}

	/**
	 * See {@link BindingController#lookupFc(String)}
	 */
	public Object lookupFc(final String cItf) {
		if (cItf.equals("meteo")) {
			return meteo;
		}
		return null;
	}

	/**
	 * See {@link BindingController#bindFc(String, Object)}
	 */
	public void bindFc(final String cItf, final Object sItf) {
		if (cItf.equals("meteo")) {
			meteo = (GlobalWeatherSoap) sItf;
			return;
		}
	}

	/**
	 * See {@link BindingController#unbindFc(String)}
	 */
	public void unbindFc(final String cItf) {
		if (cItf.equals("meteo")) {
			meteo = null;
		}
	}

}
