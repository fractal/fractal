/**
 * Dream
 * Copyright (C) 2003-2008 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Pierre Garcia, pierre2101@gmail.com
 * Contributor(s): 
 */

import org.objectweb.fractal.fraclet.annotation.annotations.Interface;

/**
 * Interface used to export the server
 * 
 * @author pgarcia
 */
@Interface(name = "export")
public interface ExportServer
{
  public void export();
}
