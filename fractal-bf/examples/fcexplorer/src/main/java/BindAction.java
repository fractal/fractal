/**
 * Dream
 * Copyright (C) 2003-2008 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Pierre Garcia, pierre2101@gmail.com
 * Contributor(s): 
 */

import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.swing.gui.lib.LabelBox;

/**
 * Menu Item to bind the client component
 * 
 * @author pgarcia
 */
public class BindAction implements MenuItem
{

  protected LabelBox id_ = null;

  /*
   * (non-Javadoc)
   * 
   * @see org.objectweb.util.explorer.api.MenuItem#actionPerformed(org.objectweb.util.explorer.api.MenuItemTreeView)
   */
  public void actionPerformed(MenuItemTreeView e)
  {
    BindClient s = (BindClient) e.getSelectedObject();
    InvokeThread t = new InvokeThread(s);
    t.start();
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.objectweb.util.explorer.api.MenuItem#getStatus(org.objectweb.util.explorer.api.TreeView)
   */
  public int getStatus(TreeView treeView)
  {
    return MenuItem.ENABLED_STATUS;
  }

  public class InvokeThread extends Thread
  {

    protected BindClient s_ = null;

    /**
     * @param s
     */
    public InvokeThread(BindClient s)
    {
      s_ = s;
    }

    public void run()
    {
      s_.bindClient();
    }
  }
}
