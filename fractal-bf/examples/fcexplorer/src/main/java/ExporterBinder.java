/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.adl.util.ContentControllerHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.bf.BindingFactory;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.BindingFactoryHelper;
import org.objectweb.fractal.bf.BindingFactoryImpl;
import org.objectweb.fractal.bf.connectors.ws.WsConnectorConstants;
import org.objectweb.fractal.fraclet.annotation.annotations.FractalComponent;
import org.objectweb.fractal.fraclet.annotation.annotations.Service;
import org.objectweb.fractal.util.Fractal;

/**
 * Component used to export/bind the server/client component
 * 
 * @author pgarcia
 */
@FractalComponent
public class ExporterBinder implements BindClient, ExportServer {

	@Service
	Component ref;

	BindingFactory bindingFactory;

	public ExporterBinder() throws BindingFactoryException {
		bindingFactory = BindingFactoryHelper.getBindingFactory();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see BindClient#bindClient()
	 */
	public void bindClient() {
		try {
			Map<String, Object> bindHints = new HashMap<String, Object>();

			bindHints.put(BindingFactoryImpl.PLUGIN_ID, "ws");
			bindHints.put(WsConnectorConstants.ADDRESS,
					WsConnectorConstants.DEFAULT_ADDRESS);

			bindingFactory.bind(getClientComponent(), "service", bindHints);
		} catch (Exception e) {
			System.err.println("An error occured while trying to bind");
			e.printStackTrace();
		}
	}

	private Component getClientComponent() throws Exception {
		Component root = Fractal.getSuperController(ref).getFcSuperComponents()[0];
		return ContentControllerHelper.getSubComponentByName(root, "client");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ExportServer#export()
	 */
	public void export() {
		try {
			Map<String, Object> exportHints = new HashMap<String, Object>();
			exportHints.put(BindingFactoryImpl.PLUGIN_ID, "ws");
			exportHints.put(WsConnectorConstants.ADDRESS,
					WsConnectorConstants.DEFAULT_ADDRESS);
			bindingFactory.export(getServerComponent(), "s", exportHints);
		} catch (Exception e) {
			System.err.println("An error occured while trying to export");
			e.printStackTrace();
		}
	}

	private Component getServerComponent() throws Exception {
		Component root = Fractal.getSuperController(ref).getFcSuperComponents()[0];
		return ContentControllerHelper.getSubComponentByName(root, "server");
	}
}
