package example.foo.hw.server.osgi;

import java.util.Properties;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import example.foo.hw.api.Service;


public class ServiceImpl implements Service, BundleActivator {

  private String header = "";

  private int count = 0;

  private ServiceRegistration reg;


  public ServiceImpl () {
    System.err.println("SERVER created");
  }

  public void print (final String msg) {
    new Exception() {

        private static final long serialVersionUID = -866466196928888569L;

    public String toString () {
        return "Server: print method called";
      }
    }.printStackTrace();
    System.err.println("Server: begin printing...");
    for (int i = 0; i < count; ++i) {
      System.err.println(header + msg);
    }
    System.err.println("Server: print done.");
  }

  public String getHeader () {
    return header;
  }

  public void setHeader (final String header) {
    this.header = header;
  }

  public int getCount () {
    return count;
  }

  public void setCount (final int count) {
    this.count = count;
  }

  public void start(final BundleContext context) throws Exception {
      Properties dict = new Properties();
      dict.put("provider", "osgi");
      reg = context.registerService(Service.class.getName(), this, dict);
  }

  public void stop(final BundleContext context) throws Exception {
      if (reg != null) {
          reg.unregister();
      }
  }
}
