package example.foo.hw.client.fractal;

import org.objectweb.fractal.api.control.BindingController;

import example.foo.hw.api.Service;

public class ClientImpl implements Runnable, BindingController {

  private Service service;

  public ClientImpl () {
    System.err.println("CLIENT created");
  }

  public void run () {
    service.print("hello world");
  }

  public String[] listFc () {
    return new String[] { "service" };
  }

  public Object lookupFc (final String cItf) {
    if (cItf.equals("service")) {
      return service;
    }
    return null;
  }

  public void bindFc (final String cItf, final Object sItf) {
    if (cItf.equals("service")) {
      service = (Service) sItf;
    }
  }

  public void unbindFc (final String cItf) {
    if (cItf.equals("service")) {
      service = null;
    }
  }
}
