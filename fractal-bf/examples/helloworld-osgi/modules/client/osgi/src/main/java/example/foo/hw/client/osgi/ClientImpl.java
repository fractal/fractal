package example.foo.hw.client.osgi;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import example.foo.hw.api.Service;

public class ClientImpl implements BundleActivator {

    private Service service;

    private ServiceReference ref;

    public ClientImpl () {
        System.err.println("CLIENT created");
    }

    public void start(final BundleContext context) throws Exception {
        ref = context.getServiceReference(Service.class.getName());
        if (ref == null) {
            System.err.println("Server not found!");
        } else {
            service = (Service) context.getService(ref);
            service.print("hello world");
        }
    }

    public void stop(final BundleContext context) throws Exception {
        if (ref != null) {
            context.ungetService(ref);
        }
    }

}
