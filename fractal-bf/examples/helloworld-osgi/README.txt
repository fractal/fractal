=== Fractal-BF HelloWorld-OSGi(tm) ===

This example illustrates the use of Fractal On OSGi by using the Fractal OSGi Binding Factory and Juliac.

Typing 'mvn install' in this directory builds five OSGi bundles:

* The service API
* A Fractal On OSGi client
* A no Fractal based client
* A Fractal on OSGi server
* A no Fractal based server

The purpose of this example is to demonstrate benefits of using OSGi to create Fractal components.
Of course, testing together the no Fractal based client and server has no sense in this context ;)

For now, the stub is only generated at runtime with Julia.
