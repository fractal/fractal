/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf;

import java.rmi.RemoteException;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;

/**
 * A simple implementation of {@link Main} and {@link BindingController}
 * interfaces.
 * 
 */
public class ClientRMI implements Runnable, BindingController {

	private FcService service;

	/**
	 * @see java.lang.Runnable#run()
	 */
	public void run() {

		try {
			service.print();
			System.out.println("Server said: " + service.printAndAnswer());
		} catch (RemoteException e1) {

			e1.printStackTrace();
		}

		try {
			service.badMethod();
		} catch (Exception e) {
			System.out
					.println("Client received a HelloWorldException,now printing stacktrace");
			((Throwable) e).printStackTrace();
		}
	}

	// BINDING-CONTROLLER IMPL

	public String[] listFc() {
		return new String[] { "service" };
	}

	/**
	 * See {@link BindingController#lookupFc(String)}
	 * 
	 * @throws NoSuchInterfaceException
	 */
	public Object lookupFc(final String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("service")) {
			return service;
		}
		throw new NoSuchInterfaceException(cItf);
	}

	/**
	 * See {@link BindingController#bindFc(String, Object)}
	 * 
	 * @throws NoSuchInterfaceException
	 */
	public void bindFc(final String cItf, final Object sItf)
			throws NoSuchInterfaceException {
		if (cItf.equals("service")) {
			service = (FcService) sItf;
			return;
		}
		throw new NoSuchInterfaceException(cItf);
	}

	/**
	 * See {@link BindingController#unbindFc(String)}
	 * 
	 * @throws NoSuchInterfaceException
	 */
	public void unbindFc(final String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("service")) {
			service = null;
			return;
		}
		throw new NoSuchInterfaceException(cItf);
	}

}