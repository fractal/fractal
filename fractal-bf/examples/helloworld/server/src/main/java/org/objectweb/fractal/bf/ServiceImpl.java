/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Properties;

/**
 * A simple implementation of the {@link Service} interface.
 * 
 * 
 */
public class ServiceImpl implements FcService, Serializable {

	private static final long serialVersionUID = -3284911549814825473L;

	/**
	 * Print a nice message.
	 */
	public void print() throws RemoteException {

		System.out.println("Hello BindingFactory");

	}

	/**
	 * Return a nice message.
	 */
	public String printAndAnswer() throws RemoteException {

		return "Message received from the Service component";
	}

	/**
	 * @see org.objectweb.fractal.bf.Service#getBytes(java.util.Properties,
	 *      byte[])
	 */
	public byte[] getBytes(Properties p, byte[] b) throws RemoteException {
		System.out.println("ServiceImpl received properties: " + p
				+ " and bytes:" + b);
		return b;
	}

	/**
	 * @see org.objectweb.fractal.bf.Service#badMethod()
	 */
	public void badMethod() throws HelloWorldException {
		throw new HelloWorldException();
	}

}
