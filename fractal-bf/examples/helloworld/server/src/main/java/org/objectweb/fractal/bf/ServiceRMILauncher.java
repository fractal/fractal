/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf;

import java.util.HashMap;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.util.Fractal;

/**
 * Start the server, whose 'service' interface is being exported via the RMI
 * plugin. For some reason, starting the same example using the FractalADL
 * launcher class doesn't seem to work.
 */
public class ServiceRMILauncher {

	public static void main(String[] args) throws ADLException,
			IllegalLifeCycleException, NoSuchInterfaceException {
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
		final HashMap<Object, Object> ctx = new HashMap<Object, Object>();
		Factory adlFactory = FactoryFactory.getFactory(
				"org.objectweb.fractal.bf.adl.MyBasicFactory",
				"org.objectweb.fractal.bf.adl.MyFractalBackend", ctx);

		Component serviceAndRmiExporter = (Component) adlFactory.newComponent(
				"org.objectweb.fractal.bf.ServiceRMI", ctx);
		Fractal.getLifeCycleController(serviceAndRmiExporter).startFc();
	}
}
