/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Properties;

/**
 * A simple printing-on-demand service.
 * 
 */
public interface FcService extends Remote {
	/**
	 * Print some message
	 * 
	 */
	void print() throws RemoteException;

	/**
	 * Return a message to the printing-service user.
	 * 
	 * @return a string to be print.
	 */
	String printAndAnswer() throws RemoteException;

	byte[] getBytes(Properties p, byte[] b) throws RemoteException;

	void badMethod() throws HelloWorldException, RemoteException;

}
