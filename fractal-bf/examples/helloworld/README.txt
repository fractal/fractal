TO BE UPDATED: this document doesn't reflect anymore the structure of
the helloworld example. guess it by your self for the moment...

=== Fractal-BF HelloWorld ===

This is the HelloWorld example for the Fractal-BindingFactory project. 
It provides a basic hello-world application, and a set of different customizations, to demonstrate the usage of various available plugins.

== WS-CXF Plugin ==

This version of HelloWorld uses the fractal-bf-connectors-soap-cxf plugin to expose the server interface of a service component as web service.
A client component will access this web service.

To run the server, issue the following:

1)  mvn -Prun.serverws

The BF will instantiate and run the server component, exporting its server interface 'service' as webservice. 
If everything went fine, the WSDL for the web service should be accessible at this address:
  
   http://localhost:8080/Service?wsdl. 

(to see it in detail, you can open your web browser to the above URL)
   
It is possible to customize the address through which the WSDL is exposed, by editing the .fractal file:
   src/main/resources/org/objectweb/fractal/bf/ServiceWS.fractal 

2) mvn -Prun.clientws

The BF will instantiate and run the client component, it will establish a web-service binding between
the client interface 'service' of the client component to the web-service.

The client will then invoke the remote service.

== JavaRMI Plugin ==

This version of HelloWorld uses the fractal-bf-connectors-rmi plugin to expose
 the designated interface by the JavaRMI protocol.
These are the required steps to execute this example:

1)  mvn -Prun.serverrmi

The server component will register its 's' interface into a locally created RMI registry.   
If you need to modify parameters like the service name, the port or the host address for the registry, you can edit the file: 
   src/main/resources/org/objectweb/fractal/bf/ServiceRMI.fractal 

2) mvn -Prun.clientrmi

will instantiate and run the client component: a distant binding between the client and the 
service will be initiated by the Fractal Binding Factory. Finally, the run() method of
The client interface the client will then invoke the remote service through two methods on the exported interface.


== JGroups/GroupRPC Plugin ==
To run the example with JGroups exporters:
1) mvn -Prun.servergroup

will instantiate and run the server component, joining a group named 'helloworld', and using a TCP stack defined at http://www.jgroups.org/javagroupsnew/perfnew/tcp.xml.
To change the name of the group of the jgroup stack to use, look in src/main/resources/org/objectweb/fractal/bf/ServiceGroup.fractal and change that value as desired.
Please note that you can run as many servers as you like: this will result in an increasing number of nodes in the group. 
1.1) mvn -Prun.servergroup (in a different shell)

will let a new server join the group

2) mvn -Prun.clientgroup
will instantiate and run the client component: a distant binding between the client and the 
service will be instantiated by the Binding Factory; the client will then perform a group-rpc, dispatching a method request to the group: every member of the group
will respond.



=== JULIAC Support ===

To compile this example with Juliac:
mvn clean compile
mvn -Pjuliac.compile
