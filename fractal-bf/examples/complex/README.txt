This example shows how to export services exposing non-primitive and/or user-defined datatypes via
the WebService binding, and how to write a corresponding client bound to the previously exported interface.

---

1) Open a console and type:

mvn -Prun.server

2) Open another console and type:

mvn -Prun.client
