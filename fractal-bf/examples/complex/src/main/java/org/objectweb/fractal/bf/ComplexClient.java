/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf;

import java.util.logging.Logger;

import org.objectweb.fractal.api.control.BindingController;

/**
 * A client for the complex service.
 */
public class ComplexClient implements Runnable, BindingController {

	Logger log = Logger.getLogger(ComplexClient.class.getCanonicalName());
	private ComplexService service;

	/**
	 * @see java.lang.Runnable#run()
	 */
	public void run() {

		final Product product = service.getProduct();
		System.out.println("getProduct: " + product);
		System.out.println("by id (0): " + service.getProductById(0));
		System.out.println("companion: "
				+ service.getCompanionProductOf(product));
		System.out.println("is available: " + service.isAvailable(product));
		System.out.println("the maker is: " + service.makerOf(product));

	}

	// ** BINDING-CONTROLLER IMPL

	/**
	 * See {@link BindingController#listFc()}
	 */
	public String[] listFc() {
		return new String[] { "service" };
	}

	/**
	 * See {@link BindingController#lookupFc(String)}
	 */
	public Object lookupFc(final String cItf) {
		if (cItf.equals("service")) {
			return service;
		}
		return null;
	}

	/**
	 * See {@link BindingController#bindFc(String, Object)}
	 */
	public void bindFc(final String cItf, final Object sItf) {
		if (cItf.equals("service")) {
			service = (ComplexService) sItf;
			return;
		}
	}

	/**
	 * See {@link BindingController#unbindFc(String)}
	 */
	public void unbindFc(final String cItf) {
		if (cItf.equals("service")) {
			service = null;
		}
	}

}
