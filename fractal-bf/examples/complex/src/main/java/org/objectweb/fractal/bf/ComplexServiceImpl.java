/**
 * Fractal Binding Factory.
 * Copyright (C) 2007-2009 INRIA, SARDES

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 * 
 * Author: Valerio Schiavoni
 * 
 */
package org.objectweb.fractal.bf;

import java.util.logging.Logger;

/**
 * Extend the basic service with more complex operations, using non primitive
 * types.
 */
public class ComplexServiceImpl implements ComplexService, Runnable {
	Logger log = Logger.getLogger(ComplexServiceImpl.class.getCanonicalName());

	/**
	 * @see org.objectweb.fractal.bf.ComplexService#getProduct()
	 */
	public Product getProduct() {
		Product p = makeProduct();
		log.info("Returning " + p);

		return p;
	}

	/**
	 * @return
	 */
	private Product makeProduct() {
		Product p = new Product();
		p.setId(666);
		p.setName("base-product");

		return p;
	}

	/**
	 * @see org.objectweb.fractal.bf.ComplexService#getCompanionProductOf(org.objectweb.fractal.bf.Product)
	 */
	public Product getCompanionProductOf(Product p) {
		Product comp = makeProduct();
		comp.setName("companion-of-" + p.getName());
		comp.setId(p.getId() + 1);
		log.info("Returning " + p);

		return comp;
	}

	/**
	 * @see org.objectweb.fractal.bf.ComplexService#getProductById(int)
	 */
	public Product getProductById(int i) {
		log.fine("Call to getProductId received on server. Id: " + i);
		Product p = makeProduct();
		p.setId(i);
		log.info("Returning " + p);
		return p;
	}

	/**
	 * @see org.objectweb.fractal.bf.ComplexService#isAvailable(org.objectweb.fractal.bf.Product)
	 */
	public boolean isAvailable(Product p) {
		log.info("Returning true");
		return true;
	}

	/**
	 * @see org.objectweb.fractal.bf.ComplexService#makerOf(org.objectweb.fractal.bf.Product)
	 */
	public ProductMaker makerOf(Product p) {

		final ProductMaker productMaker = new ProductMaker();
		log.info("Returning: " + productMaker);
		return productMaker;
	}

	/**
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		log.info("server is running");

	}

}
