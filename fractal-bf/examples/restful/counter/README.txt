=== Fractal-BF RESTful CounterService Example ===

This is an example of the usage of the RESTful plugin for the Fractal-BindingFactory project. 
It exposes a Fractal component as a RESTful service, and provides two client applications
(a Fractal application and an HTML page) to invoke the exposed RESTful service.

To run the server, issue the following:

1)  mvn -Prun.server

This will instantiate and run the Fractal server component, exporting its server interface
'counter-service' as a RESTful service accessible at this address:
  
   http://localhost:8080/counter-service

 It is possible to customize the address through which the RESTful service is exposed, 
 by editing the .fractal file:
   src/main/resources/org/objectweb/fractal/bf/ServerREST.fractal

Note: The time of the second invocation of the service is extremely long.
Perhaps a problem into Apache CXF.

To run the clients, issue the following:

2) mvn -Prun.client

This will instantiate, run the client component, and establish a RESTful binding between
the client interface 'counter-service' of the client component to the RESTful service.
The client will then invoke the remote service.

Note: Currently, a patched version of Apache CXF 2.2.2 is required to run the client.

3) Open your web browser and load CounterService.html

Warning: This page works on Safari and Eclipse web browsers but not on Firefox and Opera
due to security constraints.
