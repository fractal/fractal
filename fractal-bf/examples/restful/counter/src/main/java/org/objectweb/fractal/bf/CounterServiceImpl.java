/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 INRIA, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Philippe.Merle@inria.fr
 * 
 * Author: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf;

import org.objectweb.fractal.fraclet.annotations.Component;

/**
 * A simple implementation of the {@link CounterService} interface.
 *
 * @author Philippe Merle
 */
@Component
public class CounterServiceImpl implements CounterService {

	/** Internal state of the counter. */
	protected int value ;

	/**
	 * Return the counter value.
	 * 
	 * @return the counter value.
	 */
	public int getValue() {
		System.out.println("CounterServiceImpl getValue(" + value + ')');
		return value;
	}

	/**
	 * Increment the counter value.
	 */
	public void increment(int v) {
		System.out.println("CounterServiceImpl increment(" + v + ')');
		value = value + v;
	}

	/**
	 * Decrement the counter value.
	 */
	public void decrement(int v) {
		System.out.println("CounterServiceImpl decrement(" + v + ')');
		value = value - v;
	}

	/**
	 * Reset the counter value.
	 */
	public void resetIt() {
		System.out.println("CounterServiceImpl resetIt.");
		value = 0;
	}
}
