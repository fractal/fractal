/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 INRIA, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Philippe.Merle@inria.fr
 * 
 * Author: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf;

import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Interface;
import org.objectweb.fractal.fraclet.annotations.Requires;

/**
 * A simple client for Twitter.
 * 
 * @author Philippe Merle
 */
@Component(provides = @Interface(name = "r", signature = Runnable.class))
public class Client implements Runnable {

	/** Reference to the Twitter services. */
	private @Requires Twitter twitter;

	/**
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		final String userName = "vschiavoni";
		getUser(userName);
		getFriends(userName);
	}
	
    protected void getUser(String userName) {
		System.out.println("getUserInXml(" + userName + "): " + twitter.getUserInXml(userName));
		System.out.println("getUserInJSON(" + userName + "): " + twitter.getUserInJSON(userName));
		User user = twitter.getUser(userName);
		System.out.println(userName + ".id=" + user.id);
		System.out.println(userName + ".name=" + user.name);
		System.out.println(userName + ".screen_name=" + user.screen_name);
		System.out.println(userName + ".location=" + user.location);
		System.out.println(userName + ".description=" + user.description);
		System.out.println(userName + ".profile_image_url=" + user.profile_image_url);
		System.out.println(userName + ".url=" + user.url);
		System.out.println(userName + ".protected=" + user.isProtected);
		System.out.println(userName + ".followers_count=" + user.followers_count);
		System.out.println(userName + ".profile_background_color=" + user.profile_background_color);
		System.out.println(userName + ".profile_text_color=" + user.profile_text_color);
		System.out.println(userName + ".profile_link_color=" + user.profile_link_color);
		System.out.println(userName + ".profile_sidebar_fill_color=" + user.profile_sidebar_fill_color);
		System.out.println(userName + ".profile_sidebar_border_color=" + user.profile_sidebar_border_color);
		System.out.println(userName + ".friends_count=" + user.friends_count);
		System.out.println(userName + ".created_at=" + user.created_at);
		System.out.println(userName + ".favourites_count=" + user.favourites_count);
		System.out.println(userName + ".utc_offset=" + user.utc_offset);
		System.out.println(userName + ".time_zone=" + user.time_zone);
		System.out.println(userName + ".profile_background_image_url=" + user.profile_background_image_url);
		System.out.println(userName + ".profile_background_tile=" + user.profile_background_tile);
		System.out.println(userName + ".statuses_count=" + user.statuses_count);
		System.out.println(userName + ".verified=" + user.verified);
		System.out.println(userName + ".status.created_at=" + user.status.created_at);
		System.out.println(userName + ".status.id=" + user.status.id);
		System.out.println(userName + ".status.text=" + user.status.text);
		System.out.println(userName + ".status.source=" + user.status.source);
		System.out.println(userName + ".status.truncated=" + user.status.truncated);
		System.out.println(userName + ".status.in_reply_to_status_id=" + user.status.in_reply_to_status_id);
		System.out.println(userName + ".status.in_reply_to_user_id=" + user.status.in_reply_to_user_id);
		System.out.println(userName + ".status.favorited=" + user.status.favorited);
		System.out.println(userName + ".status.in_reply_to_screen_name=" + user.status.in_reply_to_screen_name);
    }

    protected void getFriends(String userName) {
    	System.out.println("getFriendsInXml(" + userName + "): " + twitter.getFriendsInXml(userName));
    	System.out.println("getFriendsInJSON(" + userName + "): " + twitter.getFriendsInJSON(userName));
    	Ids friends = twitter.getFriends(userName);
    	System.out.println("getFriends(" + userName + "): " + friends.getId());
/* Removed as running this implies too many trafic.
    	for(String id : friends.getId()) {
    		getUser(id);
    	}
*/
    }
}
