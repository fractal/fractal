/**
 * Fractal Binding Factory.
 * Copyright (C) 2009 INRIA, ADAM

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: Philippe.Merle@inria.fr
 * 
 * Author: Philippe Merle
 * 
 */
package org.objectweb.fractal.bf;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author Philippe Merle
 */
@XmlRootElement
public class User {
    public String id;
    public String name;
    public String screen_name;
    public String location;
    public String description;
    public String profile_image_url; // TODO: get as a URL also.
    public String url; // TODO: get as a URL also.
    @XmlElement(name="protected")
    public boolean isProtected;
    public int followers_count;
    public String profile_background_color; // COLOR
    public String profile_text_color; // COLOR
    public String profile_link_color; // COLOR
    public String profile_sidebar_fill_color; // COLOR
    public String profile_sidebar_border_color; // COLOR
    public int friends_count;
    public String created_at; // TODO: get as a DATE also.
    public int favourites_count;
    public int utc_offset;
    public String time_zone;
    public String profile_background_image_url; // TODO: get as a URL also.
    public boolean profile_background_tile;
    public int statuses_count;
// TO BE MAPPED   <notifications></notifications>
    public boolean verified;
// TO BE MAPPED    <following></following>
    public Status status;
}
