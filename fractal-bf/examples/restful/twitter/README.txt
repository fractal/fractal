=== Fractal-BF RESTful Twitter Example ===

This example illustrates how the Twitter RESTful services can be used from a Fractal application.

To run the client, issue the following:

1) mvn -Prun.client

This will instantiate, run the client component, and establish a RESTful binding between
the client interface 'twitter' of the client component to the Twitter RESTful services.
The client will then invoke Twitter.
