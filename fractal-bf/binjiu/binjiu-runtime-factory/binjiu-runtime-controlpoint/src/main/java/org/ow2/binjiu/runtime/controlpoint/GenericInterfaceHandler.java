package org.ow2.binjiu.runtime.controlpoint;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;

import org.ow2.binjiu.description.exception.UnpleasantTypeException;
import org.ow2.binjiu.runtime.controlpoint.exception.UPnPDeviceUnreachableException;

public class GenericInterfaceHandler implements InvocationHandler {
	
	
	private GenericCyberlinkControlPointWrapper wrapper;
	
	public GenericInterfaceHandler(GenericCyberlinkControlPointWrapper wrapper){
		this.wrapper = wrapper;
	}	
	
	public Object invoke(Object proxy, Method method, Object[] args) throws UPnPDeviceUnreachableException, UnpleasantTypeException {
		
		String actionName = method.getName();
		HashMap<String, Object> argList = new HashMap<String, Object>();
		
		if (args != null) {
			int argCount = 0;
			String argName;
			for (Object arg : args){
				argName = "arg" + argCount++;
				argList.put(argName, arg);
			}
		}
		
		return wrapper.performCall(actionName, argList);
		
	}

}
