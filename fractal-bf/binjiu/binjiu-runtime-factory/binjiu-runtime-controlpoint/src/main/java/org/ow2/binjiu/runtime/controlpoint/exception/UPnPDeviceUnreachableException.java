package org.ow2.binjiu.runtime.controlpoint.exception;

public class UPnPDeviceUnreachableException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public UPnPDeviceUnreachableException(){
		super();
	}
	
	public UPnPDeviceUnreachableException(String message){
		super(message);
	}
}
