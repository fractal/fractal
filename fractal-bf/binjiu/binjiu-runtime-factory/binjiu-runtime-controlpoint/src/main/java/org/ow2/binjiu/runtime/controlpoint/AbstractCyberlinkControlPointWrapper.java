package org.ow2.binjiu.runtime.controlpoint;

import org.cybergarage.upnp.ControlPoint;
import org.cybergarage.upnp.device.NotifyListener;
import org.cybergarage.upnp.device.SearchResponseListener;
import org.cybergarage.upnp.event.EventListener;
import org.cybergarage.upnp.ssdp.SSDPPacket;

public abstract class AbstractCyberlinkControlPointWrapper  extends ControlPoint implements NotifyListener, EventListener, SearchResponseListener{
	
	public AbstractCyberlinkControlPointWrapper() {
		super();
		addNotifyListener(this);
		addSearchResponseListener(this);
		addEventListener(this);
		//addDeviceChangeListener(this);
		
		search();
	}
	
	public void deviceNotifyReceived(SSDPPacket ssdpPacket) {
		// TODO Auto-generated method stub
		
	}

	public void eventNotifyReceived(String uuid, long seq, String varName,
			String value) {
		// TODO Auto-generated method stub
		
	}
	
	public void deviceSearchResponseReceived(SSDPPacket ssdpPacket) {
		// TODO Auto-generated method stub
		
	}

}
