package org.ow2.binjiu.runtime.controlpoint;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class APIControlPoint {
	
	/**
	 * Create a control point connected to a device by name
	 * 
	 * @param deviceName name of the device we want to connect
	 * @return the control point
	 */
	public static GenericCyberlinkControlPointWrapper createUPnPControlPoint(String deviceName) {
		
		GenericCyberlinkControlPointWrapper c = new GenericCyberlinkControlPointWrapper(deviceName);
		return c;

	}
	
	
	public static Object createGenericStub(Class<?> itf, GenericCyberlinkControlPointWrapper wrapper){
		
		InvocationHandler handler = new GenericInterfaceHandler(wrapper);
				
		Object stub = Proxy.newProxyInstance(itf.getClassLoader(),
                new Class[] { itf },
                handler);
		
		return stub;
	}
	
}