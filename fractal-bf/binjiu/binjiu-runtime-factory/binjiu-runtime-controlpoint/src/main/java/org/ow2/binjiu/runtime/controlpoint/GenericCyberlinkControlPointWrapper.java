package org.ow2.binjiu.runtime.controlpoint;

import java.util.HashMap;
import java.util.Map.Entry;

import org.cybergarage.upnp.Action;
import org.cybergarage.upnp.Argument;
import org.cybergarage.upnp.ArgumentList;
import org.cybergarage.upnp.Device;
import org.ow2.binjiu.runtime.controlpoint.exception.UPnPDeviceUnreachableException;
import org.ow2.binjiu.description.exception.UnpleasantTypeException;
import org.ow2.binjiu.description.model.Component;

public class GenericCyberlinkControlPointWrapper extends
		AbstractCyberlinkControlPointWrapper {

	private String deviceType;
	//private Component component;

	public String getDeviceType() {
		return this.deviceType;
	}
	/*
	public Component getComponent(){
		return this.component;
	}*/

	public GenericCyberlinkControlPointWrapper(String deviceName) {
		super();

		this.deviceType = getTypeFromName(deviceName);	
	}
	
	public boolean isConnected(){
		Device d = this.getDevice(this.deviceType);
		
		return ( d != null ) ;
	}

	//FIXME Manage exceptions better
	public Object performCall(String actionName, HashMap<String, Object> arguments)
			throws UPnPDeviceUnreachableException, UnpleasantTypeException {

		Device device = null;

		device = getDevice(this.deviceType);

		// in case of a first try, device may not be yet accessible
		// so we sleep for 4sec. before another try
		// in order to let the control point find the device
		if (device == null) { 
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// I believe FIXME --> could be a problem with hard constrained hardware.				 
				e.printStackTrace();
			}
			
			device = getDevice(this.deviceType);			
		}
		
		// if device still unreachable, we throw exception 
		if (device == null)
			throw new UPnPDeviceUnreachableException(this.deviceType);
		
		Action action = device.getAction(actionName);

		try {
			action = this.fillInputArgument(action, arguments);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		if (action.postControlAction())
			return this.getOutputArgument(action);
		else
			throw new UPnPDeviceUnreachableException("your correspondant seems " +
					"unreachable, try to call later plz.");
	}

	private Action fillInputArgument(Action action, HashMap<String, Object> argList) throws UnpleasantTypeException {
			
		if (argList == null) return action;
		
		for (Entry<String, Object> e : argList.entrySet()){
			// FIXME beurk
			if (e.getValue().getClass().equals(int.class) || e.getValue().getClass().equals(Integer.class)){
				action.setArgumentValue(e.getKey(), (Integer) e.getValue()); 
			} else if (e.getValue().getClass().equals(String.class)){
				action.setArgumentValue(e.getKey(), (String) e.getValue());
			} else 
				throw new UnpleasantTypeException(e.getValue().getClass().getName());
		}
		
		return action;
	}

	private Object getOutputArgument(Action action) throws UnpleasantTypeException {
		
		// retrieve output argument list
		ArgumentList outputArgs = action.getOutputArgumentList();

		// case of void return statement
		if (outputArgs.size() == 0)
			return null;
		
		// FIXME waiting for type management refactoring
		// Only one return argument for java interfaces
		Argument a = outputArgs.getArgument(0);
			
		String dataType = a.getRelatedStateVariable().getDataType();

		Class<?> c = null;
		if ((dataType.equals("integer") || dataType.equals("ui4") || dataType.equals("i"))) {
			c = int.class; 
		} else if (dataType.equals("string")){
			c = String.class;
		}
			
		if (c==null) throw new UnpleasantTypeException(dataType);
		
		// FIXME beurk
		if (c.equals(int.class)){
			return a.getIntegerValue();
		} else if (c.equals(String.class)){
			return a.getValue();
		} else {
			//FIXME 
			throw new UnpleasantTypeException(c.getName());
		}
	}
	
	public boolean start(){
		return super.start();
	}
	
	private String getTypeFromName(String deviceName){
		return "urn:schemas-upnp-org" + ":device:" + deviceName +":1";
	}
	
}
