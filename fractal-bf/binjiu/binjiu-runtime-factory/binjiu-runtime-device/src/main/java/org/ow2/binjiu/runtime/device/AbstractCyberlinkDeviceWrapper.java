package org.ow2.binjiu.runtime.device;

import org.cybergarage.upnp.Device;
import org.cybergarage.upnp.control.ActionListener;
import org.cybergarage.upnp.control.QueryListener;
import org.cybergarage.upnp.device.InvalidDescriptionException;


public abstract class AbstractCyberlinkDeviceWrapper extends Device implements ActionListener, QueryListener {
	
	public AbstractCyberlinkDeviceWrapper(String descriptionFile) throws InvalidDescriptionException {
		super(descriptionFile);
	}

}
