package org.ow2.binjiu.runtime.device;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.cybergarage.upnp.Argument;
import org.cybergarage.upnp.ServiceList;
import org.cybergarage.upnp.StateVariable;
import org.cybergarage.upnp.device.InvalidDescriptionException;
import org.ow2.binjiu.description.exception.UnknownElementException;
import org.ow2.binjiu.description.exception.UnpleasantTypeException;
import org.ow2.binjiu.description.model.Action;
import org.ow2.binjiu.description.model.Component;
import org.ow2.binjiu.description.model.Service;
import org.ow2.binjiu.description.model.Variable;
import org.ow2.binjiu.description.model2java.M2JProcessor;


public class GenericCyberlinkDeviceWrapper extends
		AbstractCyberlinkDeviceWrapper {

	// interface class, instantiated implementation
	private HashMap<Class<?>, Object> services = new HashMap<Class<?>, Object>();
	// action name, associated interface class
	private HashMap<Action, Class<?>> actions = new HashMap<Action, Class<?>>();

	private Component component;

	public GenericCyberlinkDeviceWrapper(Component component,
			String deviceDescriptionFile,
			HashMap<Class<?>, Class<?>> uPnPSeekers)
			throws InvalidDescriptionException {

		super(deviceDescriptionFile);

		this.component = component;

		// we choose to listen to all of the services.
		// restrictions could happen for actions
		ServiceList serviceList = getServiceList();
		org.cybergarage.upnp.Service cyberlinkService;
		for (int i = 0; i < serviceList.size(); i++) {
			cyberlinkService = serviceList.getService(i);
			cyberlinkService.setQueryListener(this);
		}

		// Construction: we instantiate objects as services according to their
		// interfaces
		for (Entry<Class<?>, Class<?>> entry : uPnPSeekers.entrySet()) {
			this.instanciate(entry.getValue(), entry.getKey());
		}
	}

	public GenericCyberlinkDeviceWrapper(Component component,
			String deviceDescriptionFile, 
			Class<?> interfaceClass,
			Object instance) 
		throws InvalidDescriptionException {

		super(deviceDescriptionFile);

		this.component = component;

		// we choose to listen to all of the services.
		// restrictions could happen for actions
		ServiceList serviceList = getServiceList();
		org.cybergarage.upnp.Service cyberlinkService;
		for (int i = 0; i < serviceList.size(); i++) {
			cyberlinkService = serviceList.getService(i);
			cyberlinkService.setQueryListener(this);
		}
	
		try {
			this.registerService(interfaceClass, instance);
		} catch (UnknownElementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void instanciate(Class<?> implementationClass,
			Class<?> interfaceClass) {

		// we ensure that the class is well implementing the specified interface
		boolean implement = false;
		Class<?>[] itfs = implementationClass.getInterfaces();
		for (Class<?> c : itfs) {
			if (c.getName().equals(interfaceClass.getName())) {
				implement = true;
				break;
			}
		}
		// TODO exception
		if (!implement)
			return;
		
		try {
			// we instantiate the implementation FIXME constructor should not
			// use parameters
			Object adaptee = ((Constructor<?>) implementationClass
					.getConstructor(null)).newInstance(null);
			Class<? extends Object> c = adaptee.getClass();

			this.registerService(interfaceClass, adaptee);

		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownElementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void registerService(Class<?> interfaceClass, Object adaptee) throws UnknownElementException  {
		// we save the reference
		services.put(interfaceClass, adaptee);

		// and the mapping action/interface
		Service service = component.getService(interfaceClass.getSimpleName());
		
		org.cybergarage.upnp.Action cyberlinkAction;
		for (Action action : service.getActions()) {
			actions.put(action, interfaceClass);
			// we also have to listen to it :

			cyberlinkAction = getAction(action.getName());
			cyberlinkAction.setActionListener(this);
		}
	}
	
	public boolean actionControlReceived(org.cybergarage.upnp.Action upnpAction) {

		Action modelAction = getActionFromName(upnpAction.getName());

		if (modelAction == null)
			return false;

		// we get the adapted object
		Class<?> interfaceClass = actions.get(modelAction);
		Object adaptee = services.get(interfaceClass);

		// and we try to call the associated method on the implementation
		try {
			// get parameter types
			ArrayList<Variable> arguments = modelAction.getArgumentSet().getVariables();
			Class<?>[] argumentTypes = new Class<?>[arguments.size()];
			
			int i = 0;
			for (Variable v : arguments){				
				Class<?> type = M2JProcessor.getAssociatedType(v.getType());
				argumentTypes[i++] = type;
			}
			
			// get method
			Method m = adaptee.getClass().getMethod(modelAction.getName(), argumentTypes);

			// extract parameter (aka input arguments) values
			Object[] inputValues = this.getInputValues(upnpAction, modelAction);

			// we try to invoke it
			Object ret = m.invoke(adaptee, inputValues);

			// then we set return value

			
			try {
				ArrayList<Variable> returns = modelAction.getReturnSet().getVariables();
				
				// 3 case : 1) void, 2) one value -> cast, 3) Multiple return values, encapsulated into an object  

				if (ret == null && returns.isEmpty())
					return true;
				
				if (returns.size() == 1) {
					// FIXME waiting for type refactoring
					Argument upnpReturnArgument = ((Argument) upnpAction.getOutputArgumentList().get(0));
					
					if (M2JProcessor.getAssociatedType(returns.get(0).getType()).equals(int.class)){
						upnpReturnArgument.setValue((Integer) ret);
					} else if (M2JProcessor.getAssociatedType(returns.get(0).getType()).equals(String.class)){
						upnpReturnArgument.setValue((String) ret);
					}	
					
					return true;
				}
				
				//TODO Manage multiple return values
				for (Variable returnVariable : returns){
					// ...
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return true;

		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnpleasantTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	private Action getActionFromName(String actionName) {
		for (Action action : actions.keySet()) {
			if (action.getName().equals(actionName))
				return action;
		}
		// TODO exception
		return null;
	}

	public boolean queryControlReceived(StateVariable stateVar) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean start() {
		return super.start();
	}
	
	public boolean stop(){
		return super.stop();
	}

	public Object[] getInputValues(org.cybergarage.upnp.Action upnpAction, Action modelAction) throws UnpleasantTypeException{
		
		ArrayList<Variable> arguments = modelAction.getArgumentSet().getVariables();
		
		Object[] values = new Object[arguments.size()];
		int i = 0;
		for (Variable argument : arguments){
			Argument upnpArgument = upnpAction.getArgument(argument.getName());
			
			//FIXME waiting for type management refactoring
			if (M2JProcessor.getAssociatedType(argument.getType()).equals(int.class)){
				values[i++] = upnpArgument.getIntegerValue();
			} else if (M2JProcessor.getAssociatedType(argument.getType()).equals(String.class)){
				values[i++] = upnpArgument.getValue();
			}	
		}
		
		return values;
	}
	
}
