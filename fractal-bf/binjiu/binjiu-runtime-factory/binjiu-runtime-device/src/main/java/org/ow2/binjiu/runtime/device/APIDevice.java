package org.ow2.binjiu.runtime.device;

import org.cybergarage.upnp.device.InvalidDescriptionException;
import org.ow2.binjiu.description.model.Component;

public class APIDevice {
		
	public static GenericCyberlinkDeviceWrapper createDevice(Component component, Class<?> interfaceClass, Object instance, String deviceDescriptionFile) throws InvalidDescriptionException {	
		return new GenericCyberlinkDeviceWrapper(component, deviceDescriptionFile, interfaceClass, instance);
	}
}