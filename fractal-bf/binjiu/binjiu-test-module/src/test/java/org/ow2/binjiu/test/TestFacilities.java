package org.ow2.binjiu.test;

import java.io.IOException;

import org.cybergarage.upnp.device.InvalidDescriptionException;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.binjiu.description.exception.UnpleasantTypeException;
import org.ow2.binjiu.description.java2upnp.J2UProcessor;
import org.ow2.binjiu.description.model2upnp.UPnPDeviceDescription;
import org.ow2.binjiu.runtime.controlpoint.APIControlPoint;
import org.ow2.binjiu.runtime.controlpoint.GenericCyberlinkControlPointWrapper;
import org.ow2.binjiu.runtime.device.APIDevice;
import org.ow2.binjiu.runtime.device.GenericCyberlinkDeviceWrapper;


public class TestFacilities {

	public void runDevice() {

		try {
			UPnPDeviceDescription desc = J2UProcessor.process(Calculator.class, "CalculatorTest", "/tmp/");
						
			GenericCyberlinkDeviceWrapper device = APIDevice.createDevice(					
					desc.getComponent(), 
					Calculator.class, 
					new CalculatorImpl(),
					desc.getDeviceDescriptionFile());

			device.start();

		} catch (UnpleasantTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidDescriptionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Calculator runControlPoint() {

		GenericCyberlinkControlPointWrapper cpw = APIControlPoint
				.createUPnPControlPoint("CalculatorTest");
		Calculator stub = (Calculator) APIControlPoint.createGenericStub(
				Calculator.class, cpw);

		cpw.start();

		return stub;

	}

	@Test
	public void tryConnect() {
		runDevice();
		
		Calculator stub = runControlPoint();
		
		Assert.assertEquals(stub.addition(2, 5), 7);
	}
	/*
	@Test
	public void createContentDirectoryInterface(){
		String path = "/tmp/upnp/PMS.xml";
		
		String ret;
		try {
			ret = U2JProcessor.process(path);
			System.out.println(ret);
		} catch (InvalidDescriptionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail();
		} catch (UnpleasantTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail();
		}
	}*/

}
