package org.ow2.binjiu.description.model2java;

import org.ow2.binjiu.description.exception.UnpleasantTypeException;
import org.ow2.binjiu.description.model.Service;
import org.ow2.binjiu.description.model.Type;

public class M2JProcessor {

	public static String process(Service service) throws UnpleasantTypeException{
		JavaSerializer serializer = new JavaSerializer(service);
		return serializer.process();
	}
	
	public static Class<?> getAssociatedType(Type type) throws UnpleasantTypeException{
		//FIXME deport to another place
		if (type.equals(new Type("integer"))){
			return int.class;
		} else if (type.equals(new Type("string"))){
			return String.class;
		}
		throw new UnpleasantTypeException(type.getName());
	}
}
