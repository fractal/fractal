package org.ow2.binjiu.description.upnp2java;

import org.cybergarage.upnp.device.InvalidDescriptionException;
import org.ow2.binjiu.description.exception.UnpleasantTypeException;
import org.ow2.binjiu.description.model.Component;
import org.ow2.binjiu.description.model.Service;
import org.ow2.binjiu.description.model2java.M2JProcessor;
import org.ow2.binjiu.description.upnp2model.U2MProcessor;

public class U2JProcessor {

	public static String process(String descriptionPath) throws InvalidDescriptionException, UnpleasantTypeException{
		String ret = "";
		
		Component component = U2MProcessor.process(descriptionPath);
		
		for (Service service : component.getServices()){
			ret += M2JProcessor.process(service);
		}
		
		return ret;
	}
}
