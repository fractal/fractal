package org.ow2.binjiu.description.upnp2model;

import java.io.File;

import org.cybergarage.upnp.device.InvalidDescriptionException;
import org.ow2.binjiu.description.exception.UnpleasantTypeException;
import org.ow2.binjiu.description.model.Component;
import org.ow2.binjiu.description.model.Type;

public class U2MProcessor {

	public static Component process(String description)
			throws InvalidDescriptionException, UnpleasantTypeException {
		UPnPParser parser = new UPnPParser(new File(description));

		return parser.parse();
	}

	public static Type getAssociatedType(String upnpDatatype)
			throws UnpleasantTypeException {
		// FIXME deport to another place
		if (upnpDatatype.equals("integer")  || upnpDatatype.equals("ui4") || upnpDatatype.equals("i4")) {
			return new Type("integer");
		} else if (upnpDatatype.equals("string")) {
			return new Type("string");
		}
		throw new UnpleasantTypeException(upnpDatatype);

	}
}
