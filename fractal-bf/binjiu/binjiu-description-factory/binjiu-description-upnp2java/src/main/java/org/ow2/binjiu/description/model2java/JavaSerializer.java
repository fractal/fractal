package org.ow2.binjiu.description.model2java;

import org.ow2.binjiu.description.exception.UnpleasantTypeException;
import org.ow2.binjiu.description.model.Action;
import org.ow2.binjiu.description.model.ReturnSet;
import org.ow2.binjiu.description.model.Service;
import org.ow2.binjiu.description.model.Variable;

public class JavaSerializer {
	private Service service;

	private String pack;
	private String imports = "";
	private String body;
	private String packageName = "upnp";
	
	public JavaSerializer(Service service, String packageName){
		this.service = service;
		this.packageName = packageName;
	}
	
	public JavaSerializer(Service service){
		this.service = service;
	}
	
	public String process() throws UnpleasantTypeException{
		
		this.pack = "package " + this.packageName + ";";
		
		this.body = "public interface " + this.service.getName() + " {\n";
			
		String method;
		
		for (Action action : service.getActions() ){
			method = "\tpublic ";
			
			if (action.getReturnSet().getList().isEmpty()) {
				method += "void ";
			} else if (action.getReturnSet().getList().size() == 1) {
				method += M2JProcessor.getAssociatedType(action.getReturnSet().getVariables().get(0).getType()).getSimpleName() + " ";;
			} else {
				method += " HashMap<Class <?>, Object> ";
			}
			
			method += action.getName() + "(";
			
			for (Variable arg : action.getArgumentSet().getList()){
				method += M2JProcessor.getAssociatedType(arg.getType()).getSimpleName() + " " + arg.getName() + ", ";				
			}
			if (method.endsWith(", "))
				method = method.substring(0, method.length()-2);
			
			method += ");\n";
			
			body += method; 
		}
		
		this.body += "}";
		
		return pack + "\n" + imports + "\n" + body;
	}
	
	private String multipleReturnsJavadoc(ReturnSet returns){
				
		String ret = "/";
		
		return ret;
	}
}
