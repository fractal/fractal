package org.ow2.binjiu.description.java2model;

import java.lang.reflect.Method;

import org.ow2.binjiu.description.model.Action;
import org.ow2.binjiu.description.model.Service;
import org.ow2.binjiu.description.model.Type;
import org.ow2.binjiu.description.model.Variable;
import org.ow2.binjiu.description.exception.UnpleasantTypeException;

public class J2MProcessor {

	private static final String argName = "arg";
	private static final String retName = "ret";
	
	/**
	 * Java 2 Model :
	 * Transform a java interface into a generic representation.
	 * 
	 * @param javaInterface (.class) 
	 * @return java interface representation into generic model
	 * @throws UnpleasantTypeException if the interface contains unrecognized type
	 */
	public static Service process(Class<?> javaInterface) throws UnpleasantTypeException {
		
		Service service = new Service(javaInterface.getSimpleName());
		
		for (Method method : javaInterface.getMethods()){
			Action action = new Action(method.getName());
				
			int i = 0;
			for (Class<?> c : method.getParameterTypes()){				
				action.addArgument(new Variable(argName + i++, getAssociatedType(c)));				
			}
			
			if (! method.getReturnType().equals(void.class)) {				
				Type retType = getAssociatedType(method.getReturnType());
				action.addReturn(new Variable(retName, retType));
			}
			
			service.addAction(action);
		}
		
		return service;
	}
	
	private static Type getAssociatedType(Class<?> javaType) throws UnpleasantTypeException{
		if (javaType.equals(int.class)){
			return new Type("integer");
		} else if (javaType.equals(java.lang.String.class)){
			return new Type("string");
		}
		throw new UnpleasantTypeException(javaType.getName());
	}
}
