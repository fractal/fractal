package org.ow2.binjiu.description.model2upnp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.ow2.binjiu.description.exception.UnpleasantTypeException;
import org.ow2.binjiu.description.model.Component;
import org.ow2.binjiu.description.model.Service;
import org.ow2.binjiu.description.resource.Properties;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;

public class UPnPDeviceDescription {

	private Document deviceDescription;
	private HashMap<String, Document> serviceDescriptions;
	private String deviceDescriptionFile;	
	private UPnPSerializer serializer;
	// Model representation
	private Component component;
	
	public Component getComponent(){
		return this.component;
	}	
		
	public UPnPDeviceDescription(Component component){
		this.component = component;
		serviceDescriptions = new HashMap<String, Document>();
		this.serializer = new UPnPSerializer();
	}
	
	public String getDeviceDescriptionFile(){
		return this.deviceDescriptionFile;
	}
	
	public Document getDeviceDescription(){
		return this.deviceDescription;
	}
	
	public HashMap<String, Document> getServiceDescriptions(){
		return this.serviceDescriptions;
	}

	public void processSerialization() throws DOMException, UnpleasantTypeException {
					
		this.deviceDescription = serializer.createDevice(component, createUPnPDeviceProperties());
		
		//Serializer.addServicesToRoot(deviceDescription, component.getServices());
		
		for (Service service : component.getServices()){

			Document serviceDocument = serializer.createService(service, createUPnPServiceProperties(service.getName()));
			
			serviceDescriptions.put(service.getName(), serviceDocument);			
		}
	}
	
	private HashMap<String, String> createUPnPServiceProperties(String serviceName) {
		HashMap<String, String> props = new LinkedHashMap<String, String>();
		
		props.put("serviceType" , "urn:schemas-upnp-org" +  ":service:" + serviceName + ":1");
		props.put("serviceId", "urn:schemas-upnp-org" + ":serviceId:" + serviceName + ":1");
		props.put("SCPDURL", getServicePath(serviceName) + Properties.descriptionFileName);		
		props.put("controlURL", getServicePath(serviceName) + "control");
		props.put("eventSubURL", getServicePath(serviceName) + "eventSub");
		
		return props;
	}
	
	private HashMap<String, String> createUPnPDeviceProperties() {
		
		HashMap<String, String> props = new LinkedHashMap<String, String>();
		
		props.put("deviceType", Properties.schemas_urn + ":device:" +  this.component.getName() +":1");
		props.put("friendlyName", this.component.getName());
		props.put("manufacturer", "ADAM");
		props.put("manufacturerURL", "http://adam.lille.inria.fr/");
		props.put("modelDescription", "");
		props.put("modelName", "");
		props.put("modelNumber", "1");
		props.put("modelURL", "http://adam.lille.inria.fr/");
		props.put("serialNumber", "1234567890");
		props.put("UDN", "");
		props.put("UPC", "123456789012");
		
		return props;
	}
	
	private String getServicePath(String serviceName) {
			return Properties.descriptionServiceDirectory + File.separator + serviceName + File.separator;
	}

	
	/**
	 * Write device and services description
	 * on disk into the target directory 
	 * 
	 * @param targetDirectory
	 * @throws IOException 
	 */
	public void write(String targetDirectory) throws IOException {
		
		// Prepare folders
		
		File deviceDescriptionRoot = new File(targetDirectory, component.getName());
		deviceDescriptionRoot.mkdir();
		
		// save UPnP device description 
		
		File deviceDescriptionFile = new File(deviceDescriptionRoot, Properties.descriptionFileName); 
		
		OutputFormat format = new OutputFormat(this.deviceDescription);
		format.setIndenting(true);

		XMLSerializer serializer = new XMLSerializer(
				new FileOutputStream(deviceDescriptionFile), format);

		serializer.serialize(this.deviceDescription);
		
		// save UPnP service descriptions	
		
		// prepare folders		
		File serviceDescriptionRootDirectory = new File(deviceDescriptionRoot, Properties.descriptionServiceDirectory);
		serviceDescriptionRootDirectory.mkdir();
			
		File serviceDescriptionDirectory, serviceDescriptionFile;
		for (Entry<String, Document> serviceXMLDocumentEntry : serviceDescriptions.entrySet()){
			
			serviceDescriptionDirectory = new File(serviceDescriptionRootDirectory, serviceXMLDocumentEntry.getKey());
			serviceDescriptionDirectory.mkdir();			
			
			serviceDescriptionFile = new File(serviceDescriptionDirectory, Properties.descriptionFileName);
			
			format = new OutputFormat(serviceXMLDocumentEntry.getValue());
			format.setIndenting(true);

			try {
				serializer = new XMLSerializer(
						new FileOutputStream(serviceDescriptionFile), format);
				serializer.serialize(serviceXMLDocumentEntry.getValue());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
		}
		
		this.deviceDescriptionFile =  deviceDescriptionFile.getAbsolutePath();
	}
}
