package org.ow2.binjiu.description.model2upnp;

import org.ow2.binjiu.description.model.Component;
import org.ow2.binjiu.description.model.Type;
import org.ow2.binjiu.description.exception.UnpleasantTypeException;
import org.w3c.dom.DOMException;

public class M2UProcessor {

	public static UPnPDeviceDescription process(Component component) throws DOMException, UnpleasantTypeException {

		UPnPDeviceDescription deviceDesc = new UPnPDeviceDescription(component);
		
		deviceDesc.processSerialization();
		
		return deviceDesc;
		
	}
	
	public static String getAssociatedType(Type type) throws UnpleasantTypeException{
		//FIXME deport to another place
		if (type.equals(new Type("integer"))){
			return "integer";
		} else if (type.equals(new Type("string"))){
			return "string";
		}
		throw new UnpleasantTypeException(type.getName());
	}
}
