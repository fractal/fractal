package org.ow2.binjiu.description.java2upnp;

import java.io.IOException;

import org.ow2.binjiu.description.exception.UnpleasantTypeException;
import org.ow2.binjiu.description.java2model.J2MProcessor;
import org.ow2.binjiu.description.model.Component;
import org.ow2.binjiu.description.model.Service;
import org.ow2.binjiu.description.model2upnp.M2UProcessor;
import org.ow2.binjiu.description.model2upnp.UPnPDeviceDescription;

public class J2UProcessor {

	/** 
	 * Create XML UPnP description from a java interface 
	 * 
	 * @param javaInterface interface you want to convert
	 * @param deviceName which will appear on the network
	 * @param descriptionRootPath where the XML description files will be save
	 * @return The upnp description object representation where you can find the model (aka component) and the device description file path
	 * @throws UnpleasantTypeException if one of the java types used into interface is not allowed
	 * @throws IOException if there is problem when it try to save description files
	 */
	public static UPnPDeviceDescription process(Class<?> javaInterface, String deviceName, String descriptionRootPath) throws UnpleasantTypeException, IOException {
		
		Component component = new Component(deviceName);
		Service service = J2MProcessor.process(javaInterface);
		component.addService(service);

		UPnPDeviceDescription desc = M2UProcessor.process(component);
		desc.write(descriptionRootPath);
		
		return desc;
		
	}
}
