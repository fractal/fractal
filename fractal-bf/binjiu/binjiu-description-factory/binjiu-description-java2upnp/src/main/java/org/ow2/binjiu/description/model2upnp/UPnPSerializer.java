package org.ow2.binjiu.description.model2upnp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.xerces.jaxp.DocumentBuilderFactoryImpl;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.ow2.binjiu.description.exception.UnpleasantTypeException;
import org.ow2.binjiu.description.model.Action;
import org.ow2.binjiu.description.model.Component;
import org.ow2.binjiu.description.model.Service;
import org.ow2.binjiu.description.model.Variable;
import org.ow2.binjiu.description.resource.Properties;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class UPnPSerializer {

	private Document dom;
	private Element servicesNode;
	private DocumentBuilder db;
		
	public UPnPSerializer() {

		// Using Xerces parser
		DocumentBuilderFactory dbf = DocumentBuilderFactoryImpl.newInstance();
		DocumentBuilder db;

		try {
			this.db = dbf.newDocumentBuilder();
			this.dom = this.db.newDocument();

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Document createDevice(Component component,
			HashMap<String, String> UPnPProperties) {

		// Create root
		Element root = dom.createElement("root");
		root.setAttribute("xmlns", Properties.schemas_urn  + Properties.device );
		dom.appendChild(root);

		// Manage state version
		appendStateVersion(dom, root, "1", "0");

		// Create device node
		Element deviceNode = dom.createElement("device");
		root.appendChild(deviceNode);
		// append device specifications
		appendProperties(UPnPProperties, deviceNode);

		// we create the service node and keep its reference
		this.servicesNode = dom.createElement("serviceList");
		deviceNode.appendChild(servicesNode);

		return dom;
	}

	private void appendProperties(HashMap<String, String> UPnPProperties,
			Element deviceNode) {

		Element element;

		for (Entry<String, String> e : UPnPProperties.entrySet()) {
			element = this.dom.createElement(e.getKey());
			element.appendChild(this.dom.createTextNode(e.getValue()));
			deviceNode.appendChild(element);
		}

	}

	private void appendStateVersion(Document doc, Element container,
			String majorV, String minorV) {
		// Create specVersion
		Element spec = doc.createElement("specVersion");
		Element major = doc.createElement("major");
		major.appendChild(doc.createTextNode(majorV));
		Element minor = doc.createElement("minor");
		minor.appendChild(doc.createTextNode(minorV));
		spec.appendChild(major);
		spec.appendChild(minor);
		container.appendChild(spec);
	}

	public static void printDeviceOnlyToConsole(Document doc) {

		OutputFormat format = new OutputFormat(doc);
		format.setIndenting(true);

		try {
			XMLSerializer serializer = new XMLSerializer(System.out, format);
			serializer.serialize(doc);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Document createService(Service service,
			HashMap<String, String> properties) throws DOMException, UnpleasantTypeException {
		
		Element serviceNode = dom.createElement("service");
		servicesNode.appendChild(serviceNode);

		appendProperties(properties, serviceNode);

		// then we create the service description document.
		Document doc = createServiceDocument(service);
		return doc;
	}

	public Document createServiceDocument(Service service) throws DOMException, UnpleasantTypeException {
		
		Document serviceDocument = db.newDocument();
		
		// Create root
		Element scpd = serviceDocument.createElement("scpd");
		scpd.setAttribute("xmlns", Properties.schemas_urn + Properties.service);
		serviceDocument.appendChild(scpd);
		
		// Manage state version
		appendStateVersion(serviceDocument, scpd, "1", "0");
		
		Element actionList = serviceDocument.createElement("actionList");
		scpd.appendChild(actionList);
		
		ArrayList<Element> stateVariables = new ArrayList<Element>();
		
		for (Action action : service.getActions()){
			ArrayList<Element> a = this.appendAction(action, actionList, serviceDocument);
			stateVariables.addAll(a);
		}
		
		Element stateTable = serviceDocument.createElement("serviceStateTable");
		scpd.appendChild(stateTable);
		for (Element stateVariable : stateVariables){
			stateTable.appendChild(stateVariable);
		}

		return serviceDocument;
	}
	
	public ArrayList<Element> appendAction(Action action, Element actionList, Document serviceDocument) throws DOMException, UnpleasantTypeException{
		
		// action node
		Element actionNode = serviceDocument.createElement("action");
		actionList.appendChild(actionNode);
		
		// action name
		Element actionName = serviceDocument.createElement("name");
		actionNode.appendChild(actionName);
		actionName.appendChild(serviceDocument.createTextNode(action.getName()));
		
		ArrayList<Element> stateVariables = new ArrayList<Element>();
				
		Element argumentList = serviceDocument.createElement("argumentList");
		actionNode.appendChild(argumentList);
		
		// input argument
		for (Variable argument : action.getArgumentSet().getVariables()){
			// argument node
			Element argumentNode = serviceDocument.createElement("argument");
			argumentList.appendChild(argumentNode);
			
			// argument name
			Element argumentName = serviceDocument.createElement("name");
			argumentName.appendChild(serviceDocument.createTextNode(argument.getName()));
			argumentNode.appendChild(argumentName);
			
			String nameSV = action.getName() + argument.getName() + "_TYPE";  
			
			// we create relative state variable
			Element stateVariableNode = serviceDocument.createElement("stateVariable");
			stateVariableNode.setAttribute("sendEvent", "no");
			
			// SV name
			Element stateVariableName = serviceDocument.createElement("name");
			stateVariableName.appendChild(serviceDocument.createTextNode(nameSV));
			stateVariableNode.appendChild(stateVariableName);
						
			// SV datatype
			Element stateVariableDatatype = serviceDocument.createElement("dataType");
			stateVariableDatatype.appendChild(serviceDocument.createTextNode(M2UProcessor.getAssociatedType(argument.getType())));
			stateVariableNode.appendChild(stateVariableDatatype);
			
			stateVariables.add(stateVariableNode);
			
			 
			Element argumentSV = serviceDocument.createElement("relatedStateVariable");
			argumentSV.appendChild(serviceDocument.createTextNode(nameSV));
			argumentNode.appendChild(argumentSV);				
		
			// argument direction
			Element argumentDirection = serviceDocument.createElement("direction");
			argumentDirection.appendChild(serviceDocument.createTextNode("IN"));
			argumentNode.appendChild(argumentDirection);
		}
		
		// output variables
		for (Variable returnVariable : action.getReturnSet().getVariables()){
			// argument node
			Element argumentNode = serviceDocument.createElement("argument");
			argumentList.appendChild(argumentNode);
			
			// argument name
			Element argumentName = serviceDocument.createElement("name");
			argumentName.appendChild(serviceDocument.createTextNode(returnVariable.getName()));
			argumentNode.appendChild(argumentName);
			
			String nameSV = action.getName() + returnVariable.getName() + "_TYPE";  
			
			// we create relative state variable
			Element stateVariableNode = serviceDocument.createElement("stateVariable");
			stateVariableNode.setAttribute("sendEvent", "no");
			
			// SV name
			Element stateVariableName = serviceDocument.createElement("name");
			stateVariableName.appendChild(serviceDocument.createTextNode(nameSV));
			stateVariableNode.appendChild(stateVariableName);
						
			// SV datatype
			Element stateVariableDatatype = serviceDocument.createElement("dataType");
			stateVariableDatatype.appendChild(serviceDocument.createTextNode(M2UProcessor.getAssociatedType(returnVariable.getType())));
			stateVariableNode.appendChild(stateVariableDatatype);
			
			stateVariables.add(stateVariableNode);
			
			 
			Element argumentSV = serviceDocument.createElement("relatedStateVariable");
			argumentSV.appendChild(serviceDocument.createTextNode(nameSV));
			argumentNode.appendChild(argumentSV);				
		
			// argument direction
			Element argumentDirection = serviceDocument.createElement("direction");
			argumentDirection.appendChild(serviceDocument.createTextNode("OUT"));
			argumentNode.appendChild(argumentDirection);
		}
		
		return stateVariables;
	}
	
}
