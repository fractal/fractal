package org.ow2.binjiu.description.model;

import java.util.ArrayList;

import org.ow2.binjiu.description.model.generic.AbstractContainer;

public class ReturnSet extends AbstractContainer<Variable>{

	public void addVariable(Variable v){
		super.addElement(v);
	}
	
	public ArrayList<Variable> getVariables(){
		return super.getList();
	}
}
