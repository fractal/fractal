package org.ow2.binjiu.description.model.generic;

import java.util.ArrayList;

public interface Container<T> {
	
	public ArrayList<T> getList();
	
	public void addElement(T element);
	
}
