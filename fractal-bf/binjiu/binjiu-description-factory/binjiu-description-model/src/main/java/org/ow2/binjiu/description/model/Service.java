package org.ow2.binjiu.description.model;

import java.util.ArrayList;

import org.ow2.binjiu.description.model.generic.NamedContainer;

public class Service extends NamedContainer<Action>{

	public Service(String name) {
		super(name);
	}

	public ArrayList<Action> getActions(){
		return super.getList();
	}
	
	public void addAction(Action a){
		super.addElement(a);		
	}
}
