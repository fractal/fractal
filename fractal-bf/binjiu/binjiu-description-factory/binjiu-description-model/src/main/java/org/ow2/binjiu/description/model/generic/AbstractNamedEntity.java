package org.ow2.binjiu.description.model.generic;

public class AbstractNamedEntity implements NamedEntity {

	private String name;
	
	public String getName(){
		return this.name;
	}
	
	public AbstractNamedEntity(String name){
		this.name = name;
	}
}
