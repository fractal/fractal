package org.ow2.binjiu.description.exception;

public class UnknownElementException extends Exception {

	public UnknownElementException(String containerName, String serviceName) {
		super("Element "+  serviceName + "cannot be found into " + containerName);
	}

	private static final long serialVersionUID = 1L;
	
}
