package org.ow2.binjiu.description.exception;

public class UnpleasantTypeException extends Exception {

	private static final long serialVersionUID = 1L;

	public UnpleasantTypeException(String msg){
		super( "Unknown type : " +  msg);
	}
	
}
