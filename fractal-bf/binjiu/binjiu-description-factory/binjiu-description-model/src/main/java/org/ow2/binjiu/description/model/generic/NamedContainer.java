package org.ow2.binjiu.description.model.generic;

import java.util.ArrayList;

public abstract class NamedContainer<T extends NamedEntity> implements NamedEntity, Container<T> {
	
	private AbstractNamedEntity namedEntity;
	private AbstractContainer<T> container; 
	
	public NamedContainer(String name){
		this.container = new DefaultContainer<T>();
		this.namedEntity = new DefaultNamedEntity(name);
	}
	
	public String getName() {
		return namedEntity.getName();
	}
	
	public ArrayList<T> getList() {
		return this.container.getList();
	}
	
	public void addElement(T element) {
		this.container.addElement(element);
	};
}
