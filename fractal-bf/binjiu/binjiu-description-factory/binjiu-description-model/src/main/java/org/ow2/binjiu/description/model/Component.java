package org.ow2.binjiu.description.model;

import java.util.ArrayList;

import org.ow2.binjiu.description.exception.UnknownElementException;
import org.ow2.binjiu.description.model.generic.NamedContainer;

public class Component extends NamedContainer<Service>{

	public Component(String name) {
		super(name);
	}

	public void addService(Service s){
		super.addElement(s);
	}
	
	public ArrayList<Service> getServices(){
		return super.getList();
	}

	public Service getService(String serviceName) throws UnknownElementException {
		for (Service service : getServices()){
			if (service.getName().equals(serviceName))
				return service;
		}
		throw new UnknownElementException(super.getName(), serviceName);
	}
	
	/*
	public boolean equals(Component component){
		
		if (this.getName().equals(component.getName()))
			if ( this.getServices().size() == component.getServices().size() )
				for (Service s : this.getServices())
					if (component.getServices())
				return true;
				
		return false;
	}*/
}
