package org.ow2.binjiu.description.resource;

public class Properties {

	//public static String schemas = "upnp:schemas-upnp-org";
	public static String schemas_urn = "urn:schemas-upnp-org";
	public static String device = ":device-1-0";
	public static String service = ":service-1-0";
	public static String descriptionFileName = "description.xml";
	public static String descriptionServiceDirectory = "service";
	public static String serviceType = ":serviceType";

	public static String serviceNamePrefix = schemas_urn + ":service:";
}
