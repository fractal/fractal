package org.ow2.binjiu.description.model;

import java.util.ArrayList;

import org.ow2.binjiu.description.model.generic.AbstractContainer;

public class ArgumentSet extends AbstractContainer<Variable>{
	
	public ArrayList<Variable> getVariables(){
		return super.getList();
	}
	
	public void addVariable(Variable v){
		super.addElement(v);
	}
}
