package org.ow2.binjiu.description.model.generic;

public class DefaultNamedEntity extends AbstractNamedEntity {
	
	public DefaultNamedEntity(String name) {
		super(name);
	}
}
