package org.ow2.binjiu.description.model;

import org.ow2.binjiu.description.model.generic.AbstractNamedEntity;

public class Type extends AbstractNamedEntity{

	public Type(String name) {
		super(name);
	}
	
	
	public boolean equals(Type type) {
		return super.getName().equals(type.getName());
	}
}
