package org.ow2.binjiu.description.model.generic;

import java.util.ArrayList;

public abstract class AbstractContainer<T extends NamedEntity> implements Container<T> {

	private ArrayList<T> list;
	
	public AbstractContainer(){
		this.list = new ArrayList<T>();
	}
	
	public ArrayList<T> getList() {
		return this.list;
	}

	public void addElement(T element){
		this.list.add(element);
	}

}
