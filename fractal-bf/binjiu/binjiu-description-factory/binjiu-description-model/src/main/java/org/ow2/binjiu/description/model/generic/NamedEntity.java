package org.ow2.binjiu.description.model.generic;

public interface NamedEntity {
	
	public String getName();
	
}
