package org.ow2.binjiu.description.model;

import org.ow2.binjiu.description.model.generic.AbstractNamedEntity;


public class Variable extends AbstractNamedEntity {

	private Type type;
	
	public Variable(String name, Type type) {
		super(name);
		this.type = type;
	}
	
	public Type getType(){
		return this.type;
	}

}
