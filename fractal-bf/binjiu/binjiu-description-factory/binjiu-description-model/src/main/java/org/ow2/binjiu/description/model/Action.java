package org.ow2.binjiu.description.model;

import java.util.ArrayList;

import org.ow2.binjiu.description.model.generic.AbstractNamedEntity;

public class Action extends AbstractNamedEntity{

	private ReturnSet returnSet;
	private ArgumentSet argumentSet;
	
	public Action(String name) {
		super(name);
		this.returnSet = new ReturnSet();
		this.argumentSet  = new ArgumentSet();		
	}
	
	public void addArgument(Variable v){
		argumentSet.addVariable(v);
	}
	
	public void addReturn(Variable v){
		returnSet.addVariable(v);
	}
	
	public ReturnSet getReturnSet(){
		return this.returnSet;
	}

	public ArgumentSet getArgumentSet(){
		return this.argumentSet;
	}
}
