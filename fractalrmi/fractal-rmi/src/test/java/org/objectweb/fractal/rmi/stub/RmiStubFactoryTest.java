/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2003-2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 */
package org.objectweb.fractal.rmi.stub;

import java.io.File;
import java.util.Properties;

import junit.framework.TestCase;
/**
 * A test case for the {@link RmiStubFactory}. It shows how to use it to generate stub/skeletons
 * via its main method.
 */
public class RmiStubFactoryTest extends TestCase {

	private File skel;
	private File stub;


	public void testMain() {
		String[] itfs = new String[] { "org.objectweb.fractal.rmi.stub.RmiStubFactoryTest$Service" };
		try {
			RmiStubFactory.main(itfs);
			skel = new File("org/objectweb/fractal/rmi/stub/RmiStubFactoryTest$Service_Skel.class");
			
			assertTrue("Skeleton file not created",skel.exists());
			skel.delete(); 
			
			stub = new File("org/objectweb/fractal/rmi/stub/RmiStubFactoryTest$Service_Stub.class");
			
			assertTrue("Skeleton file not created",stub.exists());
			stub.delete(); 
			
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	

	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		//delete destination dir created for stubs
		File parent = new File("org");
		assertTrue("Cannot delete destination dir for stubs",deleteDir(parent));
	}
	
	 public static boolean deleteDir(File dir) {
	        if (dir.isDirectory()) {
	            String[] children = dir.list();
	            for (int i=0; i<children.length; i++) {
	                boolean success = deleteDir(new File(dir, children[i]));
	                if (!success) {
	                    return false;
	                }
	            }
	        }
	    
	        // The directory is now empty so delete it
	        return dir.delete();
	    }

	public interface Service {
		void print();

		String printAndAnswer();

		byte[] elaborateBytes(Properties p, byte[] raw);

		void badMethod();

	}

}
