/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.presentation;

import org.objectweb.jonathan.apis.resources.Chunk;
import org.objectweb.jonathan.apis.resources.ChunkProvider;

/** 
 * a marshaller factory is a factory for marshallers and unmarshallers (...)
 */
public interface MarshallerFactory {

   /** 
    * Returns a new marshaller.
    *
    * @return a new marshaller.
    */
   Marshaller newMarshaller();

   /** 
    * Returns a new unmarshaller, using the provided chunk provider as a data 
    * source.
    *
    * @param provider a chunk provider.
    * @return a new unmarshaller.
    */
   UnMarshaller newUnMarshaller(ChunkProvider provider);


   /** 
    * Returns a new unmarshaller, using the provided chunk(s) as a data source.
    * <p>
    * The <code>read</code> parameters is used to initialise the number of bytes
    * read from the message.
    *
    * @param chunk a (chain of) chunk(s)
    * @param read the number of bytes already read from the message.
    * @return an unmarshaller.
    */
   UnMarshaller newUnMarshaller(Chunk chunk, int read);
}

