/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0 
 * 
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.protocols.ip;

import org.objectweb.jonathan.apis.kernel.JonathanException;

/** 
 * This interface defines a type for Tcp/Ip connection managers.
 */
public interface TcpIpConnectionMgr {

   /** 
    * Returns a new client connection. 
    * <p>
    * This method is called by a protocol.
    * The protocol provides a session (i.e. an object
    * representing an abstract  communication channel) and expects a connection
    * (i.e. a communication resource). The returned connection must have been
    * built using the provided session, or be a connection associated with a
    * session having the same destination as the provided session.
    *
    * @param host the host name of the distant server;
    * @param port the port number of a server socket on that host;
    * @param session a TcpIp session
    * @return a connection for that session.
    * @exception JonathanException if an error occurs.
    */
   IpConnection newCltConnection(String host, int port,IpSession session)
      throws JonathanException;

   /** 
    * Returns a new server connection factory encapsulating a server socket on the
    * provided port. If port = 0, an anonymous server socket is opened.
    * 
    * @param port the expected port of the server socket;
    * @return a server connection factory.
    * @exception JonathanException if an error occurs.
    */
   TcpIpSrvConnectionFactory newSrvConnectionFactory(int port)
      throws JonathanException;

   /** 
    * Returns the canonical host name of the provided host.
    *
    * The purpose of this method is to help checking the equality of two host
    * names. Two names corresponding to the same host should have the same 
    * (the "sameness" being defined by the equals method on type String)
    * canonical names.
    * 
    * @param hostname a host name
    * @return the corresponding canonical host name.
    */
   String getCanonicalHostName(String hostname);

}


