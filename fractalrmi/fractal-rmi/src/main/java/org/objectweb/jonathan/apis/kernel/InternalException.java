/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * Copyright (C) 2001 Kelua SA
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.kernel;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * InternalExceptions are unexpected exceptions that may occur e.g., when some
 * entities (kernel, binders, stub factories, ...), are incompatible or badly used.
 */
public class InternalException extends RuntimeException {
   /** @serial */
   Throwable actual;

   /**
    * Constructs a new InternalException with no detail message. 
    */
   public InternalException() {}
   /**
    * Constructs a new InternalException with a detail message.
    * 
    * @param s a detail message.
    */
   public InternalException(String s) { super(s); }

   /**
    * Builds an InternalException that wraps another exception.
    * 
    * @param throwable a wrapped exception.
    */
   public InternalException(Throwable throwable) {
      actual = (throwable instanceof InternalException) ?
         ((InternalException) throwable).represents() :
         ((throwable instanceof JonathanException) ?
          ((JonathanException) throwable).represents() :
          throwable);
   }
   
   /**
    * Returns the detail message of this InternalException.
    * <p>
    * If this exception represents another exception, the returned message is
    * that of the represented exception.
    *
    * @return  the detail message of this 'InternalException',
    *          or 'null' if this 'InternalException' does not
    *          have a detail message.
    */
   public String getMessage() {
      return (actual != null) ? actual.getMessage() : super.getMessage();
   }

   /**
    * Returns a short description of this InternalException.
    * <p>
    * If this exception represents another exception, the returned description
    * is that of the represented exception.
    *
    * @return  a string representation of this 'InternalException'.
    */
   public String toString() {
      return (actual != null) ? actual.toString() : super.toString();
   }

   /**
    * Prints this InternalException and its backtrace to the 
    * standard error stream. 
    * <p>
    * If this exception represents another exception, the printed description
    * and backtrace are that of the represented exception.
    */  
   public void printStackTrace() {
      if (actual != null) actual.printStackTrace(); else super.printStackTrace();
   }

   /**
    * Prints this InternalException and its backtrace to the 
    * specified print stream.
    * <p>
    * If this exception represents another exception, the printed description
    * and backtrace are that of the represented exception.
    *
    * @param  s  the print stream.
    */
   public void printStackTrace(PrintStream s) {
      if (actual != null) actual.printStackTrace(s); else super.printStackTrace(s);
   }
   
   /**
    * Prints this 'Throwable' and its backtrace to the specified
    * print writer.
    * <p>
    * If this exception represents another exception, the printed description
    * and backtrace are that of the represented exception.
    *
    * @param  s  the print writer.
    */
   public void printStackTrace(PrintWriter s) {
      if (actual != null) actual.printStackTrace(s); else super.printStackTrace(s);
   }
   /**
    * Returns the exception wrapped by this InternalException.
    * <p>
    * If this exception doesn't wrap any other exception, it returns itself.
    *
    * @return the represented exception.
    */  
   public Throwable represents() {
      return (actual != null) ? actual : this;
   }   
}


