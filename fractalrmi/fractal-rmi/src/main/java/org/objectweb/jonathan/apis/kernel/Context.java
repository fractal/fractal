/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 2000 France Telecom R&D
 * Copyright (C) 2001 Kelua SA
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: jonathan@objectweb.org
 *
 * Author: Fabien Delpiano, Bruno Dumant
 * 
 */

package org.objectweb.jonathan.apis.kernel;

import java.util.Enumeration;
import java.io.Serializable;

/** 
 * A context represents a set of typed objects, each identified by a name.
 */
public interface Context extends Component {

   /** 
    * Adds an element in the target context.
    *
    * @param name the name of the object to be added;
    * @param type the type of the object to be added;
    * @param value the object to be added.
    * @return the previous element, if any; null otherwise.
    * @exception JonathanException if something goes wrong (usually, a type 
    *            exception).
    */
   Element addElement(Name name, Class type, Object value) 
      throws JonathanException;

   /** 
    * Adds an element to the target context.
    *
    * In this case, the value's type must be promotable to
    * an integer. <code>Class</code> is one of int.class, short.class, char.class,
    * or byte.class.
    *
    * @param name  the name of the element to be added;
    * @param type the type of the element to be added;
    * @param value the integer value of the element to be added;
    * @return the previous element, if any, null otherwise.
    * @exception JonathanException if something goes wrong (usually, a type 
    *            exception).
    */
   Element addElement(Name name, Class type, int value) throws JonathanException;

   /** 
    * Adds an element in the target context. If the component is already attached to some element,
    * this attachment will be lost in this operation.
    *
    * @param name the name of the element to be added;
    * @param component the component to be added;
    * @return the previous element, if any; null otherwise.
    * @exception JonathanException if something goes wrong (usually, a type 
    *            exception).
    */
   Element addElement(Name name, Component component) 
      throws JonathanException;

   /** 
    * Adds an element to the target context. 
    *
    * @param element the element to be added.
    * @return the previous element, if any, null otherwise. 
    * @exception JonathanException if something goes wrong (usually, a type 
    *            exception).
    */
   //Element addElement(Element element)  throws JonathanException;


   /** 
    * Adds an element to the target context.
    * 
    * This method is equivalent to {@link #addElement(Name,Class,Object) 
    * <code>addElement(Name,Class,Object)</code>}, except that the name to use
    * is specified by a string and a separator separating the different elements of
    * the name.
    *
    * If <code>separator == 0</code>, the name corresponds to an object 
    * local to the target context.
    * If the name begins with the separator, the name is an absolute name (this 
    * only makes sense if the context has a tree structure); otherwise, the name is
    * relative to the target context.
    *
    * @param name a string representing the name of the seeked element;
    * @param type the type of the object to be added;
    * @param value the object to be added;
    * @param separator the separator character used to parse <code>name</code>;
    * @return the previous element, if any; null otherwise.
    * @exception JonathanException if something goes wrong (usually, a type 
    *            exception).
    */
   Element addElement(String name,Class type,Object value,char separator) 
      throws JonathanException;
   
   /** 
    * Adds an element to the target context.
    * 
    * This method is equivalent to {@link #addElement(Name,Component) 
    * <code>addElement(Name,Component)</code>}, except that the name to use
    * is specified by a string and a separator separating the different elements of
    * the name.
    *
    * If <code>separator == 0</code>, the name corresponds to an object 
    * local to the target context.
    * If the name begins with the separator, the name is an absolute name (this 
    * only makes sense if the context has a tree structure); otherwise, the name is
    * relative to the target context.
    *
    * @param name a string representing the name of the added element;
    * @param component the component to be added;
    * @param separator the separator character used to parse <code>name</code>;
    * @return the previous element, if any; null otherwise.
    * @exception JonathanException if something goes wrong (usually, a type 
    *            exception).
    */
   Element addElement(String name,Component component,char separator) 
      throws JonathanException;
   

   /** 
    * Adds an element to the target context.
    * 
    * This method is equivalent to {@link #addElement(Name,Class,int) 
    * <code>addElement(Name,Class,int)</code>}, except that the name to use
    * is specified by a string and a separator separating the different elements of
    * the name.
    *
    * If <code>separator == 0</code>, the name corresponds to an object 
    * local to the target context.
    * If the name begins with the separator, the name is an absolute name (this 
    * only makes sense if the context has a tree structure); otherwise, the name is
    * relative to the target context.
    *
    * @param name a string representing the name of the seeked element;
    * @param type the type of the seeked element;
    * @param value the integer value of the element to be added;
    * @param separator the separator character used to parse <code>name</code>;
    * @return the previous element, if any; null otherwise.
    * @exception JonathanException if something goes wrong (usually, a type 
    *            exception).
    */
   Element addElement(String name,Class type,int value,char separator) 
      throws JonathanException ;


   public Context addOrGetContext(String _name,char _separator) throws JonathanException;
   
   public Context addOrGetContext(Name _name) throws JonathanException;

   /** 
    * Returns the element in the target context identified by <code>name</code>, 
    * null if no element is registered under name <code>name</code>.
    *
    * This method is equivalent to {@link #getElement(Name) 
    * <code>getElement(Name)</code>}, except that the name to use
    * is specified by a string and a separator separating the different elements of
    * the name.
    *
    * If <code>separator == 0</code>, the name corresponds to an object 
    * local to the target context.
    * If the name begins with the separator, the name is an absolute name (this 
    * only makes sense if the context has a tree structure); otherwise, the name is
    * relative to the target context.
    *
    * @param name a string representing the name of the seeked element;
    * @param separator the separator character used to parse <code>name</code>;
    * @return the corresponding element, if any; null otherwise.
    */
   Element getElement(String name,char separator);

   /** 
    * Returns the component contained in the element registered in the target context under 
    * the name <code>name</code>.
    *
    * This method is equivalent to {@link #getComponent(Name) 
    * <code>getComponent(Name)</code>}, except that the name to use
    * is specified by a string and a separator separating the different elements of
    * the name.
    *
    * If <code>separator == 0</code>, the name corresponds to an object 
    * local to the target context.
    * If the name begins with the separator, the name is an absolute name (this 
    * only makes sense if the context has a tree structure); otherwise, the name is
    * relative to the target context.
    *
    * @param name the name of the element whose component is seeked; 
    * @param separator the separator character used to parse <code>name</code>;
    * @return the corresponding component, or null.
    */
   Component getComponent(String name,char separator);

   /** 
    * Returns the value of the element registered in the target context under 
    * the name <code>name</code>.
    *
    * This method is equivalent to {@link #getValue(Name) 
    * <code>getValue(Name)</code>}, except that the name to use
    * is specified by a string and a separator separating the different elements of
    * the name.
    *
    * If <code>separator == 0</code>, the name corresponds to an object 
    * local to the target context.
    * If the name begins with the separator, the name is an absolute name (this 
    * only makes sense if the context has a tree structure); otherwise, the name is
    * relative to the target context.
    *
    * @param name the name of the element whose value is seeked; 
    * @param separator the separator character used to parse <code>name</code>;
    * @return the corresponding value, or {@link #NO_VALUE <code>NO_VALUE</code>}.
    */
   Object getValue(String name,char separator);

   /** 
    * Returns the value of the element registered in the target context under 
    * the name <code>name</code>. 
    * <p>
    * A call to <code>getIntValue(name)</code> is equivalent to
    * <code>{@link #getElement(Name) getElement(name)}.{@link Element#getIntValue()
    * getIntValue()}</code> if the provided name actually corresponds to an 
    * element. If it is not the case, 
    * <code>Integer.MAX_VALUE</code> is returned.
    *
    * @param name the name of the element whose value is seeked; 
    * @return the corresponding value, or <code>Integer.MAX_VALUE</code>
    */
   int getIntValue(Name name);


   /** 
    * Returns the value of the element registered in the target context under 
    * the name <code>name</code>.
    *
    * This method is equivalent to {@link #getIntValue(Name) 
    * <code>getIntValue(Name)</code>}, except that the name to use
    * is specified by a string and a separator separating the different elements of
    * the name.
    *
    * If <code>separator == 0</code>, the name corresponds to an object 
    * local to the target context.
    * If the name begins with the separator, the name is an absolute name (this 
    * only makes sense if the context has a tree structure); otherwise, the name is
    * relative to the target context.
    *
    * @param name the name of the element whose value is seeked; 
    * @param separator the separator character used to parse <code>name</code>;
    * @return the corresponding value, or <code>Integer.MAX_VALUE</code>
    */
   int getIntValue(String name,char separator);

   /** 
    * Returns an enumeration of the elements found in the target context.
    * @return an enumeration of the elements found in the target context.
    */
   Enumeration getElements();

   /** 
    * Releases the target context, releasing all the resources it uses, if no one still
    * uses it.
    * The context should no longer
    * be used once it has been released.
    */
   void release();


   /** 
    * Acquires the target context.
    */
   void acquire();
   

   /** 
    * Resets the target context, removing all its current elements.
    */
   void reset();


   /** 
    * Sets the scope of the target context;
    * @param scope the new scope of the target context; 
    * @return the previous scope of the target context;
    */
   Context setScope(Context scope);

   /** 
    * Returns the scope of the target context;
    * @return the scope of the target context;
    */
   Context getScope();
   
   /** 
    * The NO_VALUE object is returned by {@link #getValue(String,char) getValue} 
    *  if no value corresponds to the provided name. 
    */
   Object NO_VALUE = new NO_VALUE();

   final class NO_VALUE implements Serializable {
      
      private NO_VALUE(){}
      
      public String toString() {
         return "NO VALUE";
      }
   }
}
