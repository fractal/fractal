package org.objectweb.jonathan.libs.resources;

import org.objectweb.jonathan.apis.kernel.Context;
import org.objectweb.jonathan.libs.helpers.LogConfiguration;
import org.objectweb.util.monolog.api.Logger;

class LoggerProvider {
   static Logger logger = null;
	static Logger bind_logger = null;
   static Logger export_logger = null;
   static Logger send_logger = null;
   static Logger receive_logger = null;

   static {
      try {
         Context trace_context = LogConfiguration.newConfiguration(LoggerProvider.class);
         String package_name = "org.objectweb.jonathan.libs.resources";
         Object component = trace_context.getValue("packages." + package_name + ".generic logger",'.');
         if (component instanceof Logger) logger = (Logger) component;
         component = trace_context.getValue("packages." + package_name + ".bind logger",'.');
         if (component instanceof Logger) bind_logger = (Logger) component;
         component = trace_context.getValue("packages." + package_name + ".export logger",'.');
         if (component instanceof Logger) export_logger = (Logger) component;
         component = trace_context.getValue("packages." + package_name + ".send logger",'.');
         if (component instanceof Logger) send_logger = (Logger) component;
         component = trace_context.getValue("packages." + package_name + ".receive logger",'.');
         if (component instanceof Logger) receive_logger = (Logger) component;
      } catch (Exception e) {}
   }
}
