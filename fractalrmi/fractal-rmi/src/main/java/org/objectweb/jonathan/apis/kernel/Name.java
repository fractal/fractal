/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 2000 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 3.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Fabien Delpiano, Bruno Dumant
 * 
 */

package org.objectweb.jonathan.apis.kernel;


/** 
 * Names are non-mutable structures representing names used by contexts.
 * A Name is a pair <id,subname>, where id is an identifier (represented as 
 * a String, and subname a (possibly null) Name.
 *
 * The "" identifier is reserved in contexts representing trees 
 * to represent the root of the tree.
 */
public interface Name {

   /** 
    * Returns the id of the target name.
    * @return the id of the target name.
    */
   String getId();

   /** 
    * Returns the subname of the target name.
    * @return the subname of the target name.
    */
   Name getSubName();
}
