/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.protocols;

import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.presentation.UnMarshaller;
import org.objectweb.jonathan.apis.binding.ForwardException;

/**
 * A ReplyInterface holds the reply to a request. Reply interfaces are created 
 * two-ways invocations are performed.
 */
public interface ReplyInterface {
   /** 
    * Returns a message containing the reply to the request.
    * <p>
    * This method blocks until the reply is available. It is the responsibility of 
    * the caller to make sure that the returned message will be properly
    * {@ UnMarshaller#close() closed}.
    * 
    * @return  a message containing the reply to the request.
    * @exception ServerException if the server has thrown an application level 
    *                            exception;
    * @exception ForwardException if the server has moved;
    * @exception JonathanException if another exception has been raised.
    */
   public UnMarshaller listen() 
      throws ServerException, ForwardException, JonathanException;

   
   /** 
    * Returns true if a (possibly exceptional) reply has arrived.
    * <p>
    * If <code>available</code> returns true, a call to 
    * {@link #listen() listen} is non-blocking.
    *
    * @return true if a (possibly exceptional) reply has arrived.
    */
   public boolean available();   
}




  
