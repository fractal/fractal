/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.resources;

import org.objectweb.jonathan.apis.kernel.Context;

/** 
 *  A Job is the abstraction of an activity in the system. Jobs are created and
 * managed by {@link Scheduler schedulers}. The role of a job is to
 * {@link #run execute} code.
 */
public interface Job {
   
   /** 
    * Tells the target job to execute the code manifested by the <tt>message</tt>
    * argument.
    * @param message code to be executed.
    */
   public void run(Runnable message);

   /** 
    * Returns a context associated with the target Job.
    * @return a context associated with the target Job.
    */
   public Context getContext();
}


