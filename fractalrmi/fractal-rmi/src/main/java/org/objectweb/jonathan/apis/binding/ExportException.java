/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.binding;

import org.objectweb.jonathan.apis.kernel.JonathanException;
/**
 * ExportException is a checked exception that an interface could not be
 * exported for some reason.
 */
public class ExportException extends JonathanException {
   /**
    * Constructs a new ExportException with no detail message. 
    */
   public ExportException() { super(); }
   /**
    * Builds an ExportException with a detail message.
    * 
    * @param s a detail message.
    */
   public ExportException(String s) { super(s); }
   /**
    * Builds a ExportException that wraps another exception.
    * 
    * @param exception a wrapped exception.
    */
   public ExportException(Exception exception) { super(exception); }
}


