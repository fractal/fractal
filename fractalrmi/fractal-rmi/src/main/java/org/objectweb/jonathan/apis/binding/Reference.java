/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.binding;

/** 
 * A <tt>Reference</tt> simply holds a set of {@link Identifier identifiers}.
 */
public interface Reference {
   
   /** 
    * Returns the set of identifiers associated with the target reference.
    * @return the set of identifiers associated with the target reference.
    */
   Identifier[] getIdentifiers();

   
   /** 
    * Sets the set of identifiers associated with the target reference.
    * @param ids the set of identifiers to be associated with the target reference.
    */
   void setIdentifiers(Identifier[] ids);
}


