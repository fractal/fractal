/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.protocols;

import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.presentation.UnMarshaller;

/**
 * <code>Session_Low</code> is the type of sessions used to forward messages
 * coming from the network to their actual recipient; <code>Session_Low</code> is
 * also the type of interfaces represented by {@link SessionIdentifier session
 * identifiers}.
 * <p>
 * Sessions represent handles on particular communication channels: A session
 * object is dynamically created by a protocol, and lets messages be sent and
 * received through the communication channel it represents, using that
 * protocol. Sessions have {@link Session_High higher} and {@link Session_Low lower}
 * interfaces, respectively used to send messages down and up a protocol stack.
 * <p>
 * <code>Session_Low</code> is also the type of interfaces
 * {@link ProtocolGraph#export(Session_Low) exported} to protocols, and
 * designated by {@link SessionIdentifier session identifiers}.
 */
public interface Session_Low {
   /**
    * Sends a message up a protocol stack.
    * <p>
    * This method is used to send an incoming message to its recipient. The
    * provided <code>session</code> parameter represents the sender, and may
    * possibly be used to send a reply if necessary. This <code>session</code>
    * parameter may be null.
    * <p> 
    * When this method is used, the target object must make sure that the provided
    * message will be {@link UnMarshaller#close() closed}, as well as and the
    * {@link Session_High#close() session} (if not null). 
    *
    * @param message the message to send up;
    * @param session the sending session.
    * @exception JonathanException if the sending fails.
    */
   public void send(UnMarshaller message,Session_High session)
      throws JonathanException;

   /**
    * Sends an exception up a protocol stack.
    * <p>
    * This method is used to warn the target session that an exception
    * has occurred on an incoming message. The
    * provided <code>session</code> parameter represents the sender, and may
    * possibly be used to send a reply if necessary. This <code>session</code>
    * parameter may be null.
    * <p> 
    * When this method is used, the target object must make sure that the provided
    * session will be {@link Session_High#close() closed}(if not null). 
    *
    * @param exception the exception that occurred;
    * @param session the sending session.
    */
   public void send(JonathanException exception,Session_High session);
}


