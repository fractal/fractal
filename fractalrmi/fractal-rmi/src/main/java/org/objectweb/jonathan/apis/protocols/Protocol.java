/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.protocols;

import java.util.Properties;

import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.kernel.Context;
import org.objectweb.jonathan.apis.protocols.ProtocolGraph;

/**
 * The protocol abstraction represents protocols like TCP, IP or
 * GIOP. A protocol is a
 * {@link org.objectweb.jonathan.model.naming_context naming context}
 * that only deals with a specific family of interfaces -- called
 * {@link Session_Low sessions} --, and
 * manages names -- called {@link SessionIdentifier session identifiers} -- to 
 * designate these interfaces. A protocol is actually a  
 * {@link org.objectweb.jonathan.model.binder binder},
 * since it may give access to the interfaces it identifies.
 * <p>
 * The {@link Protocol Protocol},  and {@link ProtocolGraph ProtocolGraph} 
 * abstractions have been designed in Jonathan to allow the creation of 
 * arbitrary protocol stacks, or even graphs.
 * <p>
 * The first step in this process is the creation of a protocol graph that will
 * represent the expected protocol structure. Methods to create such graphs
 * must be provided by protocols. A protocol graph is a directed (acyclic)
 * graph composed of protocol-specific nodes, and describing a path to be
 * followed by messages when they are sent over the net, or received. Each node  
 * has a protocol-specific arity.
 * <p>
 * Imagine for instance that you want to build a stack with
 * the GIOP protocol on top of TCP/IP. You need first to create a graph reduced
 * to one TCP/IP node (by calling the appropriate method on the TCP/IP
 * protocol), and then to extend it with a GIOP node pointing to the TCP/IP node 
 * you have just obtained: The GIOP layer
 * is not designed to issue requests over a network, and thus expects to issue its
 * requests to a lower layer. The GIOP nodes are built using a protocol graph
 * representing that lower layer. Note that nothing would forbid a different
 * architecture, with possibly two graphs representing two different lower
 * layers, one used to issue one-way requests, and the other for two way
 * requests.
 * <p>
 * When graphs are created, it is also possible to specify additional
 * information: for instance, the TCP/IP protocol lets you specify a preferred
 * port number. That's why there is no generic <tt>newProtocolGraph</tt> 
 * operation: the structure of a node and the information to store in it are 
 * very dependent on the specific protocol used.
 * <p>
 * Once you have obtained a specific protocol graph, you
 * can use it to {@link ProtocolGraph#export(Session_Low) export} an interface of a
 * specific type: {@link Session_Low Session_Low}. This provides you with a
 * session identifier, which is a 
 * {@link org.objectweb.jonathan.model.name name} for the exported
 * interface. You only need to call the <tt>export</tt> method on the root of
 * the protocol graph: The appropriate calls will be recursively issued on its 
 * sub-graphs.
 * <p>
 * Information contained in this session identifier may be tranmitted over the 
 * network, and a copy of the session identifier re-constructed on a different 
 * machine (using the appropriate methods provided by protocols). To be able to
 * access the exported interface, you will have to call the
 * {@link SessionIdentifier#bind(Session_Low) bind} operation to obtain a
 * session of type {@link Session_High Session_High}, that will let you send
 * messages to the exported session.
 */
public interface Protocol {
   /** 
    * Returns true if the target protocol is an invocation protocol.
    * <p>
    * An invocation protocol is a protocol able to handle <i>invocations</i>, i.e.,
    * requests expecting a reply. In practice, this means that calls to the
    * {@link Session_High#prepareInvocation(Marshaller) prepareInvocation} method
    * on sessions obtained from the target protocol will not raise an
    * {@link org.objectweb.jonathan.apis.kernel.InternalException 
    * InternalException}, but perform the appropriate work.
    *
    * @return true if the target protocol is an invocation protocol.
    */
    public boolean isAnInvocationProtocol();

    /**
     * Creates a new protocol graph with a number of given sub
     * protocol graphs.
     * @param subgraphs the lower-level graphs
     * @param hints the information req'd to build the graph
     * @return a new ProtocolGraph
     * @exception JonathanException if the hints or the subgraphs are
     * invalid for this protocol
     */
     ProtocolGraph createProtocolGraph(ProtocolGraph[] subgraphs, Context hints) throws JonathanException;

    /**
     * Creates a new session identifier with the provided info.
     * 
     * @param info information to create the session identifier.
     * @param next session identifiers of the lower sessions?
     * @return the created session identifier.
     * @throws JonathanException if something goes wrong.
     */
    SessionIdentifier createSessionIdentifier(Properties info, SessionIdentifier[] next) throws JonathanException;
}
