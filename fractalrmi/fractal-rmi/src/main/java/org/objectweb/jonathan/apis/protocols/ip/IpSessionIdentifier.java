/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0 
 * 
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 */


package org.objectweb.jonathan.apis.protocols.ip;

import org.objectweb.jonathan.apis.protocols.SessionIdentifier;
import org.objectweb.jonathan.apis.protocols.ProtocolIdentifiers;


/**
 * IpSessionIdentifier is a specific
 * {@link org.objectweb.jonathan.apis.protocols.SessionIdentifier
 * SessionIdentifier} type for IP protocols. An IP session identifier contains
 * a host name, and a port 
 * number.
 */
abstract public class IpSessionIdentifier implements SessionIdentifier, ProtocolIdentifiers {

    /** Hostname of this session identifier. */
    public String hostname;

    /** Port number of this session identifier. */
    public int port;

    /**
     * Constructs a new IpSessionIdentifier with the specified host name and port
     * number.
     *
     * @param hostname a host name;
     * @param port a port number;
     * @return an IP session identifier.
     */
    public IpSessionIdentifier(String hostname, int port) {
        this.hostname = hostname;
        this.port = port;
    }

    /**
     * Constructs a new empty IpSessionIdentifier.
     */
    public IpSessionIdentifier() {
        this.port = 0;
    }

    /**
     * Returns a hash code value for this IpSessionIdentifier.
     * 
     * @return a hash code value for this object.
     */
    public int hashCode() {
        if (hostname == null) {
            return port;
        } else {
            return hostname.hashCode() + port;
        }
    }
   
    /**
     * Compares this object to the specified object.
     * <p>
     * The result is true if and only if the argument is not null and is a
     * IpSessionIdentifier object that has the same host name and port number.
     *
     * @param o the object to compare with.
     * @return true if the objects are the same; false otherwise.
     */
    public boolean equals(Object o) {
        if (o != null && o instanceof IpSessionIdentifier) {
            IpSessionIdentifier k = (IpSessionIdentifier)o;
            return port == k.port && hostname.equals(k.hostname);
        } else {
            return false;
        }
    }

    /** 
     * Returns a string representation of this session identifier.
     *
     * @return a string representation of this session identifier.
     */
    public String toString() {
        return "IpSessionIdentifier["+hostname+","+port+"]";
    }

    /**
     * Return an empty array: this is a leaf protocol in the protocol
     * tree.
     * 
     * @return the session identifiers corresponding to the lower level
     * protocol layers, if any.
     */
    public SessionIdentifier[] next() {
	return new SessionIdentifier[0];
    }

    public final int getProtocolId() {
	return ProtocolIdentifiers.TCPIP;
    }

}
