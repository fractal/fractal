/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.resources;

import org.objectweb.jonathan.apis.kernel.JonathanException;


/**
 * A chunk represents a part of an array of bytes. Chunks are linked to form
 * messages that may be sent from an adress space to another. Their use avoids
 * unnecessarily copying arrays of bytes, and helps recovering these arrays
 * without  resorting to garbage collection (thanks to
 * {@link ChunkFactory chunk factories}).
 * <p>
 * Chunks should not be used concurrently.
 */
public class Chunk {
   /** The associated array of bytes. */
   public byte[] data;
   
   /** Index of the last valid (written) byte in this chunk + 1. */
   public int top;
   
   /** The index of the first valid (written) byte in this chunk. */
   public int offset; 

   /** The next chunk in the chain */
   public Chunk next;
   
   /**
    * Constructs a new chunk with the specified data and offsets.
    * 
    * @param data the byte array containing the data.
    * @param offset the offset of the first valid byte in the chunk.
    * @param top offset + the number of valid bytes in the chunk.
    */
   public Chunk (byte[] data,int offset,int top) {
      this.data = data;
      this.offset = offset;
      this.top = top;
   }

   /**
    * Duplicates the whole chunk.
    * <p>
    * The default implementation copies the buffer,
    * and creates a new chunk with it.
    *
    * @return a copy of this chunk.
    * @exception JonathanException if an IO error occurs.
    */
   public Chunk duplicate() throws JonathanException {
      int len = top - offset;
      byte[] ndata = new byte[len];
      System.arraycopy(data,offset,ndata,0,len);
      return new Chunk(ndata,0,len);
   }

   /**
    * Partially duplicates this chunk. 'offset' must be greater than
    * the target chunk's offset, 'top' must be less or equal than the target's top.
    * <p>
    * The default implementation copies the appropriate portion of the buffer,
    * and creates a new chunk with it.
    *
    * @param offset the offset of the chunk copy.
    * @param top the top of the chunk copy.
    * @return a chunk containing the specified part of the target chunk.
    * @exception JonathanException if an error occurs.
    */
   public Chunk duplicate(int offset,int top) throws JonathanException {
      int len = top - offset;
      byte[] ndata = new byte[len];
      System.arraycopy(data,offset,ndata,0,len);
      return new Chunk(ndata,0,len);
   }
   

   /**
    * Releases the chunk. The data of a chunk may be obtained from managed
    * buffers. It may thus be necessary to tell when the data encapsulated by a
    * chunk may be reused.
    * <p>
    * The default implementation resets offset and top to 0.
    */
   public void release() {
      top = 0;
      offset = 0;
      next = null;
   }
   
   /**
    * Returns a string representation of the target chunk.
    *
    * @return a string representation of the target chunk.
    */
   public String toString() {
      return "Chunk[data: " + data + " offset: " + offset + " top: " + top + "]";
   }
}

