/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 3.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * With contributions from:
 *  H. Piccione & S. Thiebaud
 * 
 */


package org.objectweb.jonathan.apis.protocols;

import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.presentation.Marshaller;

/**
 * A reply session is used by a server to send a reply back to a client.
 */
public interface ReplySession {

   /** 
    * Lets the target session write its own headers into a newly created message 
    * and returns it (standard reply case)
    * <p>
    * An entity wishing to send a message as a reply to an invocation must not 
    * create a marshaller on its own and directly 
    * {@link #send(Marshaller) send} it down the protocol stack. Instead, it must 
    * ask the session for a marshaller, into which the session will usually 
    * already have written its specific headers. 
    * <p>
    * The <code>prepareException</code> method must only be used to prepare 
    * messages corresponding to a non-exceptional reply from the server. If the 
    * reply corresponds to an exception raised by the server, use 
    * {@link #prepareExceptionReply() prepareExceptionReply} instead.
    * 
    * @return a marshaller to write the reply into;
    * @exception JonathanException if something goes wrong.
    */
   public Marshaller prepareReply() throws JonathanException;

   /** 
    * Lets the target session write its own headers into a newly created message 
    * and returns it (exception case)
    * <p>
    * An entity wishing to send a message as a reply to an invocation must not 
    * create a marshaller on its own and directly 
    * {@link #send(Marshaller) send} it down the protocol stack. Instead, it must 
    * ask the session for a marshaller, into which the session will usually 
    * already have written its specific headers. 
    * <p>
    * The <code>prepareExceptionReply</code> method must only be used to prepare
    * messages 
    * corresponding to an exception raised by the server.
    * 
    * @return a marshaller to write the reply into;
    * @exception JonathanException if something goes wrong.
    */
   public Marshaller prepareExceptionReply() throws JonathanException;



	/** 
    * Lets the target session write its own headers into a newly created message 
    * and returns it (system exception case)
    * <p>
    * An entity wishing to send a message as a reply to an invocation must not 
    * create a marshaller on its own and directly 
    * {@link #send(Marshaller) send} it down the protocol stack. Instead, it must 
    * ask the session for a marshaller, into which the session will usually 
    * already have written its specific headers. 
    * <p>
    * The <code>prepareSystemExceptionReply</code> method must only be used to prepare
    * messages 
    * corresponding to an system exception raised by the server.
    * 
    * @return a marshaller to write the reply into;
    * @exception JonathanException if something goes wrong.
    */
   public Marshaller prepareSystemExceptionReply() throws JonathanException;



	/** 
    * Lets the target session write its own headers into a newly created message 
    * and returns it (location forward case)
    * <p>
    * An entity wishing to send a message as a reply to an invocation must not 
    * create a marshaller on its own and directly 
    * {@link #send(Marshaller) send} it down the protocol stack. Instead, it must 
    * ask the session for a marshaller, into which the session will usually 
    * already have written its specific headers. 
    * <p>
    * The <code>prepareLocationForwardReply</code> method must only be used to prepare
    * messages corresponding to an location forward message raised by the server.
    * 
    * @return a marshaller to write the reply into;
    * @exception JonathanException if something goes wrong.
    */
   public Marshaller prepareLocationForwardReply() throws JonathanException;



   /** 
    * Sends the reply down the protocol stack.
    * <p>
    * The sent message must have been obtained by calling the 
    * {@link #prepareReply() prepareReply} or
    * {@link #prepareExceptionReply() prepareExceptionReply} method.
    * <p>
    * It is the responsibility of the recipient of the message to 
    * {@link Marshaller#close() close} it.
    *
    * @param m the message to send;
    * @exception JonathanException if something goes wrong.
    */
   public void send(Marshaller m) throws JonathanException;
   
   /**
    * Closes the session, letting the associated resources be released or
    * reused.
    * <p>
    * Sessions may have an exclusive access to a communication resource. It is 
    * thus very important to ensure that they are properly closed if they are no 
    * longer in use.
    */
   public void close(); 
}




  
