/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0 
 * 
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.protocols.ip;

import org.objectweb.jonathan.apis.kernel.JonathanException;

/**
 * TcpIpSrvConnectionFactory encapsulates a server socket.
 */
public interface TcpIpSrvConnectionFactory {

   /** 
    * Returns a new connection for the provided session, obtained from the
    * encapuslated server socket.
    *
    * @param session an IpSession.
    * @return a new IpConnection.
    * @exception JonathanException if an error occurs.
    */
   IpConnection newSrvConnection(IpSession session)
      throws JonathanException;


   /** 
    * Returns the local port of the encapsulated socket.
    * @return the local port of the encapsulated socket.
    */
   int getPort();


   /** 
    * Returns the host name of the encapsulated socket.
    * @return the host name of the encapsulated socket. 
    */
   String getHostName(); 

   
   /** 
    * Tells the factory to release the server socket.
    */
   void close();
}


