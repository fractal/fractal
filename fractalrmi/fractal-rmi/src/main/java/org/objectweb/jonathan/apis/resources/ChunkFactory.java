/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.resources;


/** 
 * A chunk factory builds chunks, possibly implementing a specific chunk
 * management policy.
 */
public interface ChunkFactory {
   
   /** 
    * Returns a chunk. The offset and top of this chunk are 0.
    * @return a chunk.
    */
   public Chunk newChunk();

   
   /** 
    * Returns a chunk whose associated array of bytes is (at least) of size
    * <tt>size</tt>. Its offset and top are always 0.
    * @param  size the size of the expected chunk.
    * @return a chunk.
    */
   public Chunk newChunk(int size);
}

