/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.protocols;

import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.presentation.Marshaller;

/**
 * <code>Session_High</code> is the type of sessions used to send messages down
 * to the network; A <code>Session_High</code> is usually a surrogate for
 * a session of type {@link Session_Low Session_Low} exported to a protocol and
 * represented by an {@link SessionIdentifier session identifier}.
 * <p>
 * Sessions represent handles on particular communication channels: A session
 * object is dynamically created by a protocol, and lets messages be sent and
 * received through the communication channel it represents, using that
 * protocol. Sessions have {@link Session_High higher} and {@link Session_Low lower}
 * interfaces, respectively used to send messages down and up a protocol stack.
 * <p>
 * Usually, a <code>Session_High</code> instance is obtained using the
 * {@link SessionIdentifier#bind(Session_Low) bind} operation on a session 
 * identifier representing a {@link Session_Low Session_Low} interface: it is 
 * thus a surrogate, or a proxy, for that interface.
 */
public interface Session_High {

   /** 
    * Lets the target session write its own headers into the provided message 
    * (one-way case).
    * <p>
    * Protocols usually need to add headers in the front of messages before 
    * sending them down the net. It may be much more efficient to add these headers 
    * <i>before</i> the actual message is written. The <code>prepare</code> (and 
    * {@link #prepareInvocation(Marshaller) prepareInvocation} methods are
    * provided for this purpose. Therefore, an entity wishing to send a message 
    * down a protocol stack must first prepare that message by invoking the 
    * appropriate method (unless a call to {@link #direct() direct} returns true),
    * and then write its own data to the message.
    * <p>
    * The <code>prepare</code> method must only be used to prepare messages with a 
    * one-way semantics (no reply expected); If a reply is expected, use 
    * {@link #prepareInvocation(Marshaller) prepareInvocation} instead.
    *
    * @param m   a marshaller representing the message
    * @exception JonathanException if something goes wrong.
    */
   public void prepare(Marshaller m) throws JonathanException;

   /** 
    * Return false if the {@link #prepare(Marshaller) prepare} or
    * {@link #prepareInvocation(Marshaller) prepareInvocation} must be used, true
    * otherwise.
    * <p>
    * A true return means that the target protocols headers are added when the
    * message is sent, and not before. Invoking one of the <code>prepare</code> 
    * methods would in that case return without having changed anything to the
    * provided marshaler.
    *
    * @return true if the <code>prepare</code> methods need not be used.
    */
   public boolean direct();
   
   /*
    * Lets the target session write its own headers into the provided message 
    * (two-way case).
    * <p>
    * Protocols usually need to add headers in the front of messages before 
    * sending them down the net. It may be much more efficient to add these headers 
    * <i>before</i> the actual message is written. The
    * <code>prepareInvocation</code>
    * (and {@link #prepare(Marshaller) prepare} methods are
    * provided for this purpose. Therefore, an entity wishing to send a message 
    * down a protocol stack must first prepare that message by invoking the 
    * appropriate method (unless a call to {@link #direct() direct} returns true),
    * and then write its own data to the message.
    * <p>
    * The <code>prepareInvocation</code> method must only be used to prepare
    * messages with a two-ways semantics (a reply is expected); If no reply is 
    * expected, use {@link #prepare(Marshaller) prepare} instead. 
    * This method returns
    * a  {@link ReplyInterface ReplyInterface} that must be used to decode the
    * reply.
    *
    * @param m   a marshaller representing the message;
    * @return a reply interface.
    * @exception JonathanException if something goes wrong.
    */
   public ReplyInterface prepareInvocation(Marshaller m)
      throws JonathanException;
   

   /** 
    * Sends the message down the protocol stack.
    * <p>
    * The sent message must have been prepared first, unless a call to 
    * {@link #direct() direct} returns true.
    * <p>
    * It is the responsibility of the recipient of the message to 
    * {@link Marshaller#close() close} it.
    *
    * @param m the message to send;
    * @exception JonathanException if something goes wrong.
    * @see #prepareInvocation(Marshaller)
    * @see #prepare(Marshaller)
    */
   public void send(Marshaller m)
      throws JonathanException;
   
   /**
    * Closes the session, letting the associated resources be released or
    * reused.
    * <p>
    * Sessions may have an exclusive access to a communication resource. It is 
    * thus very important to ensure that they are properly closed if they are no 
    * longer in use.
    */
   public void close(); 
}
