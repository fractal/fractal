/***
 * Jonathan: an Open Distributed Processing Environment
 * Copyright (C) 1999-2000 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Release: 3.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 *
 */


package org.objectweb.jonathan.libs.resources;

import org.objectweb.jonathan.apis.resources.Chunk;
import org.objectweb.jonathan.apis.resources.ChunkFactory;

import org.objectweb.util.monolog.api.BasicLevel;

/**
 * A Chunk Factory implementation.
 * <p>
 * A chunk factory creates and manages chunks. This chunk factory manages two
 * pools of chunks of different sizes, so that a released chunk may be reused
 * in a different context, saving the time for allocating and collecting the
 * associated memory. The sizes of the chunks used, and of the preallocated
 * pools may be changed.
 */
public class JChunkFactory implements ChunkFactory {

   /** a pool of chunks. */
   BigPoolChunk[] big_pool;

   /** Number of chunks in the big pool. */
   int big_pool_size;

   /** index of first free chunk */
   int big_free;

   /** Size of chunks in the big pool. */
   int big_size;

   /** a pool of chunks. */
   SmallPoolChunk[] small_pool;

   /** Number of chunks in the small pool. */
   int small_pool_size;

   /** index of first free small chunk */
   int small_free;

   /** Size of chunks in the small pool. */
   int small_size;

   /**
    * Indicates whether warning message should be printed to stderr.
    * <p>
    * In particular, if verbose is true, and if all the chunks in a pool are used,
    * a warning will be emitted. This may be useful to tune the pool sizes.
    * <p>
    * This value is defined under the name "/jonathan/JChunkFactory/verbose"
    * in the {@link org.objectweb.jonathan.apis.kernel.Kernel#newConfiguration(Class)
    * bootstrap context}.

    */
   public boolean verbose;

   /**
    * Returns a new chunk factory
    */
   public JChunkFactory() {
      this(5,32768,10,8192,false);
   }

   /**
    * Returns a new chunk factory
    * @param big_pool_size size of the big chunks pool
    * @param big_size size of the big chunks
    * @param small_pool_size size of the small chunks pool
    * @param small_size size of the small chunks
    * @param verbose indicates whether warning messages should be printed to
    *                stderr.
    * @see verbose
    *
    */
   public JChunkFactory(int big_pool_size,int big_size,
                        int small_pool_size, int small_size,
                        boolean verbose) {
      this.big_pool_size = big_pool_size;
      this.big_size = big_size;
      this.small_pool_size = small_pool_size;
      this.small_size = small_size;
      this.verbose = verbose;

      big_free = big_pool_size;
      big_pool = new BigPoolChunk[big_free];
      for (int i = 0; i < big_free; i++) {
         big_pool[i] = new BigPoolChunk();
      }
      small_free = small_pool_size;
      small_pool = new SmallPoolChunk[small_free];
      for (int i = 0; i < small_free; i++) {
         small_pool[i] = new SmallPoolChunk();
      }
   }

   /**
    * Returns a chunk of a default (small) size.
    *
    * @return a chunk
    */
   public Chunk newChunk() {
      synchronized(small_pool) {
         PoolChunk chunk = null;
         if (small_free > 0) {
            chunk = small_pool[--small_free];
            small_pool[small_free] = null;
         } else {
            // begin log
            if ((LoggerProvider.logger != null)
                &&(LoggerProvider.logger.isLoggable(BasicLevel.INFO))) {
               LoggerProvider.logger.log(BasicLevel.INFO,"Allocating chunk outside small pool");
            }
            // end log
            chunk = new SmallPoolChunk();
         }
         return chunk;
      }
   }

   /**
    * Returns a chunk of (at least) the specified size.
    * <p>
    * This method may return a larger chunk than expected.
    *
    * @param size the expected size of the chunk.
    * @return a chunk
    */
   public Chunk newChunk(int size) {
      if (size <= small_size) {
         return newSmallChunk();
      } else if (size <= big_size) {
         return newBigChunk();
      } else {
         // begin log
         if ((LoggerProvider.logger != null)
             &&(LoggerProvider.logger.isLoggable(BasicLevel.INFO))) {
            LoggerProvider.logger.log(BasicLevel.INFO,"Chunk of size " + size + " requested.");
         }
         // end log
         Chunk c = new Chunk(new byte[size],0,0);
         //System.out.println("Non pool Chunk " + c + " returned.");
         return c;
      }
   }


   /**
    * @return a big chunk
    */
   Chunk newBigChunk() {
      synchronized(big_pool) {
         BigPoolChunk chunk = null;
         if (big_free > 0) {
            chunk = big_pool[--big_free];
            big_pool[big_free] = null;
         } else {
            // begin log
            if ((LoggerProvider.logger != null)
                &&(LoggerProvider.logger.isLoggable(BasicLevel.INFO))) {
               LoggerProvider.logger.log(BasicLevel.INFO,"Allocating chunk outside big pool");
            }
            // end log
            //            if (verbose) {
            //               System.err.println("Allocating chunk outside big pool");
            //            }
            chunk = new BigPoolChunk();
         }
         return chunk;
      }
   }

   /**
    * @return a small chunk
    */
   Chunk newSmallChunk() {
      synchronized(small_pool) {
         SmallPoolChunk chunk = null;
         if (small_free > 0) {
            chunk = small_pool[--small_free];
            small_pool[small_free] = null;
         } else {
            //            if (verbose) {
            //               System.err.println("Allocating chunk outside small pool");
            //            }
            // begin log
            if ((LoggerProvider.logger != null)
                &&(LoggerProvider.logger.isLoggable(BasicLevel.INFO))) {
               LoggerProvider.logger.log(BasicLevel.INFO,"Allocating chunk outside small pool");
            }
            // end log
            chunk = new SmallPoolChunk();
         }
         return chunk;
      }
   }


   /** 
    * Adds a chunk to the big pool.
    * 
    * @param resource the chunk that must be added.
    */
   void addToBigPool(BigPoolChunk resource) {
      synchronized(big_pool) {
         if (big_free < big_pool.length) {
            big_pool[big_free++] = resource;
         }
      }
   }

   /** 
    * Adds a chunk to the small pool.
    *
    * @param resource the chunk that must be added.
    */
   void addToSmallPool(SmallPoolChunk resource) {
      synchronized(small_pool) {
         if (small_free < small_pool.length) {
            small_pool[small_free++] = resource;
         }
      }
   }

   /** Extension of Chunk as a controlled resource. */
   static class PoolChunk extends Chunk {
      /** count of acquired references to the associated  chunk. */
      protected int acquired;

      PoolChunk(int size) {
         super(new byte[size],0,0);
         acquired = 1;
      }

      public Chunk duplicate() {
         return new Duplicate(this,offset,top);
      }

      public Chunk duplicate(int offset, int top) {
         return new Duplicate(this,offset,top);
      }

      synchronized void acquire() {
         acquired++;
      }
   }

   final class SmallPoolChunk extends PoolChunk {

      SmallPoolChunk () {
         super(JChunkFactory.this.small_size);
      }

      public synchronized void release() {
         if (acquired == 1) {
            next = null;
            super.release();
            addToSmallPool(this);
         } else {
            acquired--;
         }
      }
   }

   final class BigPoolChunk extends PoolChunk {

      BigPoolChunk () {
         super(JChunkFactory.this.big_size);
      }

      public synchronized void release() {
         if (acquired == 1) {
            next = null;
            super.release();
            addToBigPool(this);
         } else {
            acquired--;
         }
      }
   }

   static final class Duplicate extends Chunk {
      PoolChunk target;

      Duplicate(PoolChunk chunk,int offset, int top) {
         super(chunk.data,offset,top);
         this.target = chunk;
         chunk.acquire();
      }

      public Chunk duplicate() {
         return new Duplicate(target,offset,top);
      }

      public Chunk duplicate(int offset, int top) {
         return new Duplicate(target,offset,top);
      }

      public void release() {
         next = null;
         target.release();
         target = null;
      }
   }
}
