/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.resources;

import org.objectweb.jonathan.apis.kernel.JonathanException;

/**
 * Chunk providers are used to wrap input streams (like, e.g., socket input streams)
 * in unmarshallers.
 *
 * @see org.objectweb.jonathan.apis.presentation.MarshallerFactory
 */
public interface ChunkProvider {
   
   /** 
    * Returns a chunk to read data from.
    * <p>
    * When done with the chunk, its user must update its
    * {@link Chunk#offset offset} and {@link Chunk#top top} members and
    * {@link Chunk#release() release} it.
    * <p>
    * ChunkProviders should not be used concurrently.
    *
    * @return a chunk;
    * @exception JonathanException if no chunk can be provided.
    */
   public Chunk prepare() throws JonathanException;

   
   /** 
    * Closes the chunk provider. This method must be called if the target provider
    * is no longer used.
    */
   public void close();
}


