/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.protocols;

import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.presentation.UnMarshaller;

/**
 * Request sessions represent the recipient of an invocation for an invocation
 * protocol.
 */
public interface RequestSession {
   
   /** 
    * Sends a message (a request) to its recipient.
    * <p>
    * The unmarshaller representing the invocation is sent together with a 
    * reply session, to be used by the recipient to send the reply. The reply
    * session may be null if no response is expected.
    * <p>
    * It is the responsibility of the recipient to make sure that the unmarshaller 
    * will properly be {@ UnMarshaller#close() closed}. 
    *
    * @param message the unmarshaller representing the request;
    * @param session the session to send the reply;
    * @exception JonathanException if something goes wrong.
    */
   void send(UnMarshaller message,ReplySession session)
      throws JonathanException;

   /** 
    * Returns the target object represented by this request session.
    * @return the target object represented by this request session.
    */
   Object getTarget();
}


