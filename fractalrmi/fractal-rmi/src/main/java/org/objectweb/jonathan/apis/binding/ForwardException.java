/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.binding;

/**
 * ForwardException is a checked exception raised by
 * {@link Identifier#bind(Identifier[],org.objectweb.jonathan.apis.kernel.Context) bind} or 
 * {@link org.objectweb.jonathan.apis.protocols.ReplyInterface#listen() listen} 
 * operations or to indicate that
 * another reference should be used to designate the targeted object interface.
 */
public class ForwardException extends BindException {
   
   /**
    * Another reference for the targeted object interface. Depending on the
    * context, this object may be of type  {@link Identifier Identifier},
    * {@link Reference Reference}, or of any other type, provided that it really
    * designates the right interface, and that its type is clear from the context. 
    * @serial
    */
   public Object reference; 

   /** 
    * Constructs a new ForwardException with the given reference. 
    * @param reference an object identifying the targeted object interface.
    */
   public ForwardException(Object reference) {
      this.reference = reference;
   }
}


