/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 */


package org.objectweb.jonathan.apis.protocols;

import org.objectweb.jonathan.apis.kernel.Context;
import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.binding.BindException;

/**
 * A session identifier represents an exported session. In other words, it is a
 * {@link org.objectweb.jonathan.model.name name}, managed by a specific 
 * {@link org.objectweb.jonathan.model.naming_context naming context} of type
 * {@link Protocol Protocol}, and representing a {@link Session_Low session}.
 * <p>
 * The internal structure of a session identifier
 * is protocol specific: each protocol may define its own SessionIdentifier type
 * so that it can contain protocol specific information: host name, port... 
 * <p>
 * Session identifiers are created when "server" objects are exported, and
 * can typically be encoded in
 * {@link org.objectweb.jonathan.apis.binding.Identifier identifiers}. On the 
 * "client" side, they can be decoded and used to establish communication channels.
 *
 * @see Protocol
 */
public interface SessionIdentifier {   

    /** 
     * Returns the protocol that created this session identifier.
     * 
     * @return the protocol that created this session identifier.
     */
    Protocol getProtocol();

    /** 
     * Unexporting a session identifier means that the target session identifier
     * no longer designates the session it was created for.  A session
     * identifier must no longer be used after it has been "unexported".
     */
    void unexport();

    /** 
     * Returns a local session representing the session designated by the provided
     * lower session interface. 
     * <p>
     * This operation is used by a protocol to establish a communication channel
     * to the session specified by the target session identifier. The provided lower
     * session interface is used to receive messages using the opened channel.
     * 
     * @param hls a session to receive messages on the opened communication channel;
     * @return    a session to send messages to the remote session.
     * @exception BindException if the bind process fails.
     * @exception JonathanException if something else goes wrong.
     * @see org.objectweb.jonathan.model.binder#bind(org.objectweb.jonathan.model.name)
     */
    Session_High bind(Session_Low hls)
	throws BindException, JonathanException;


    /**
     * Return the session identifiers corresponding to the lower level
     * protocol layers, if any.
     * 
     * @return the session identifiers corresponding to the lower level
     * protocol layers, if any.
     * @throws JonathanException if something goes wrong.
     */
    SessionIdentifier[] next() throws JonathanException;


    /**
     * What kind of protocol does this session identifier relate to?
     * 
     * @return the kind of protocol this session identifier relates to.
     */
    int getProtocolId();

    /**
     * Yield info about this session, as a Context.
     * 
     * @return info about this session, as a Context.
     * @throws JonathanException if something goes wrong.
     */
    Context getInfo() throws JonathanException;

    /**
     * Return true if the target identifier corresponds to a local
     * connection.
     * 
     * @return true if the target identifier corresponds to a local
     * connection.
     */
    boolean isLocal();
}
