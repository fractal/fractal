package org.objectweb.jonathan.apis.protocols;

/**
 * Simply, a list of the protocol identifiers used in the exotic
 * protocol graph thingy.
 */
public interface ProtocolIdentifiers {
    static final int GIOP = 0;
    static final int TCPIP = 1;
    static final int HTTP = 2;
    static final int RTP = 3;
    static final int MULTICASTIP = 4;
    static final int MULTIPLEX = 5;
}
