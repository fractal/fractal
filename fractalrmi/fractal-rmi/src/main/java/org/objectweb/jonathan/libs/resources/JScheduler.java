/***
 * Jonathan: an Open Distributed Processing Environment
 * Copyright (C) 1999-2000 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Release: 3.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 *
 */


package org.objectweb.jonathan.libs.resources;

import org.objectweb.jonathan.apis.kernel.Context;
import org.objectweb.jonathan.apis.kernel.ContextFactory;
import org.objectweb.jonathan.apis.kernel.InternalException;
import org.objectweb.jonathan.apis.resources.Scheduler;
import org.objectweb.jonathan.apis.resources.Job;

import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Default {@link org.objectweb.jonathan.apis.resources.Scheduler scheduler}
 * implementation.
 * <p>
 * This implementation has exactly the same behaviour as the default Java
 * scheduling.
 * <p>
 * This scheduler manages a pool of re-usable threads. The size of this pool may
 * be customized in the
 * {@link org.objectweb.jonathan.apis.kernel.Kernel#newConfiguration(Class)
 * bootstrap context}.
 */
public class JScheduler implements Scheduler {

   static int sched_nb = 0;

   String sched_name;
   int id = 0;

   /** Max number of idle threads in the thread pool. */
   int max_waiting;

   /**
    * Indicates whether warning message should be printed to stderr.
    * <p>
    * This value is defined under the name "/jonathan/JScheduler/verbose"
    * in the {@link org.objectweb.jonathan.apis.kernel.Kernel#newConfiguration(Class)
    * bootstrap context}.
    * <p>
    * This variable is not used in the current implementation.
    */
   public boolean verbose;

   /** the pool of waiting jobs */
   JJob[] pool;

   /** number of threads waiting for a task. */
   int waiting;

   protected ContextFactory context_factory;
   
   /** 
    * Returns a new scheduler.
    * @param context_factory a {@link ContextFactory context factory}.
    */
   public JScheduler(ContextFactory context_factory) {
      this(5,false,context_factory);
   }

   /** 
    * Returns a new scheduler.
    * @param max_waiting maximum number of idle threads
    * @param verbose indicates whether warning messsages should be output on 
    stderr. 
    * @param context_factory a {@link ContextFactory context factory}.
    */
   public JScheduler(int max_waiting,boolean verbose, 
                     ContextFactory context_factory) {
      this.max_waiting = max_waiting;
      this.verbose = verbose;
      this.context_factory = context_factory;
      sched_name = "p" + (sched_nb++) + ".";
      pool = new JJob[max_waiting];
      waiting = 0;
   }

   /**
    * Returns a new job created by the scheduler.
    * @return a new job created by the scheduler.
    */
   public synchronized Job newJob() {
      JJob job = null;
      if (waiting == 0) {
         // add a means to know whether there are more threads running than the
         // pool size
         job = new JJob(sched_name + (id++));
      } else {
         job = pool[--waiting];
      }
      return job;
   }

   /**
    * Returns the currently executing job (the job performing the call).
    * @return the currently executing job.
    */
   public Job getCurrent() {
      if (Thread.currentThread() instanceof Job)
         return (Job) Thread.currentThread();
      else
         return new KJob();
   }

   /**
    * Calling this method gives the opportunity to the scheduler to re-schedule
    * the currently executing jobs.
    * <p>
    * This implementation calls the static <code>yield()</code> method
    * on the <code>Thread</code> class.
    */
   public void yield() {
      Thread.yield();
   }

   /**
    * Blocks the calling job until the {@link #notify(Object) <tt>notify</tt>} or
    * {@link #notifyAll(Object) <tt>notifyAll</tt>} method is called providing the
    * same lock identifier.
    * <p>
    * This implementation calls the standard <code>wait()</code> method
    * on the provided <code>lock</code>.
    *
    * @param lock the lock identifier.
    * @exception InterruptedException
    */
   public void wait(Object lock) throws InterruptedException {
      lock.wait();
   }

   /**
    * Blocks the calling job until the {@link #notify(Object) <tt>notify</tt>} or
    * {@link #notifyAll(Object) <tt>notifyAll</tt>} method is called providing the
    * same lock identifier.
    * <p>
    * This implementation calls the standard <code>wait()</code> method
    * on the provided <code>lock</code>.
    *
    * @param lock the lock identifier.
    * @param millis a time out in milliseconds.
    * @exception InterruptedException
    */
   public void wait(Object lock,long millis) throws InterruptedException {
      lock.wait(millis);
   }

   /**
    * Unblocks a job {@link #wait(Object) waiting} on the lock.
    * <p>
    * This implementation calls the standard <code>notify()</code> method
    * on the provided <code>lock</code>.
    *
    * @param lock the lock identifier.
    */
   public void notify(Object lock) {
      lock.notify();
   }

   /**
    * Unblocks all jobs {@link #wait(Object) waiting} on the lock.
    * <p>
    * This implementation calls the standard <code>notifyAll()</code> method
    * on the provided <code>lock</code>.
    *
    * @param lock the lock identifier.
    */
   public void notifyAll(Object lock) {
      lock.notifyAll();
   }

   /**
    * Causes the calling job to be removed from the set of jobs managed by the
    * target scheduler. It is necessary to call this method before every possibly
    * blocking method so that the scheduler can give a chance to run to another job.
    * <p>
    * This implementation has nothing to do.
    */
   public void escape() {}

   /**
    * Causes a job {@link #escape() "escaped"} from the scheduler to be re-admitted
    * in the set of jobs managed by the target scheduler.
    * <p>
    * This implementation has nothing to do.
    */
   public void enter() {}

   boolean register(JJob job) {
      if (waiting < pool.length) {
         pool[waiting++] = job;
         return true;
      } else {
         return false;
      }
   }

   class JJob extends Thread implements Job {
      Runnable task;
      Context context;

      JJob(String name) {
         super(name);
         setDaemon(true);
         task = null;
         context = null;
         super.start();
      }

      /** Runs the task. */
      public final void run() {
         try {
            synchronized (this) {
               while (task == null) {
                  this.wait();
               }
            }
            while (true) {
               //System.err.println(this + " running");
               task.run();
               if (context != null) {
                  context.release();
                  context = null;
               }
               //System.err.println(this + " done");
               synchronized(JScheduler.this) {
                  if (register(this)) {
                     task = null;
                  } else {
                     return;
                  }
               }
               if (task == null) {
                  synchronized (this) {
                     while (task == null) {
                        this.wait();
                     }
                  }
               }
            }
         } catch (InterruptedException e) {}
      }

      public synchronized void run(Runnable message) {
         if (task != null) {
            throw new InternalException("Can't use this job more than once");
         } else {
            task = message;
            this.notify();
         }
      }

      public Context getContext() {
         if (context == null) {
            context = context_factory.newContext();
         }
         return context;
      }

      public String toString() {
         return getName();
      }
   }

   private static ThreadLocal threadLocalContext = new ThreadLocal();

   /**
    * SylvainHack alert!
    *
    * In the case of servlet-driven communication the current thread
    * is not a Job managed by this scheduler. In this case trying to
    * get the current job context necessarily fails.
    *
    * This simply encapsulates an existing Thread into a Job. Thus,
    * trying to access methods such as getCurrent() in JScheduler
    * will simply create this Job...
    *
    * Note: set the context to be ThreadLocal or something...
    */
   class KJob implements Job {

      public KJob() {
      }

      /*
       * This is invalid. By definition a KJob encapsulates an
       * already running thread, and should not be reused.
       */
      public void run(Runnable m) {
         //	    System.err.println("KJob being asked to run a new task... Alert!");
         // begin log
         if ((LoggerProvider.logger != null)
             &&(LoggerProvider.logger.isLoggable(BasicLevel.WARN))) {
            LoggerProvider.logger.log(BasicLevel.WARN,"KJob being asked to run a new task... Alert!");
         }
         // end log
         // sadly we can't throw an exception without changing the
         // api...
      }

      public Context getContext() {
         if (threadLocalContext.get() == null) {
            threadLocalContext.set(context_factory.newContext());
         }

         return (Context)threadLocalContext.get();
      }
   }



}


