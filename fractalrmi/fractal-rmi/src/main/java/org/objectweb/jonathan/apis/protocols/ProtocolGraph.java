/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.protocols;

import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.binding.ExportException;

/**
 * @see Protocol
 */
public interface ProtocolGraph {   

   /**
    * Exports a session using the graph represented by the target object.
    * <p>
    * This method informs the protocol that the provided session 
    * is willing to accept messages. The returned session identifier is a
    * specific name
    * for the exported session. For a more general explanation, see
    * {@link Protocol here}.
    * 
    * @param hls a session to export;
    * @return    an identifier for the exported session.
    * @exception ExportException if the export process fails;
    * @exception JonathanException if another error occurs.
    * @see org.objectweb.jonathan.model.naming_context#export(org.objectweb.jonathan.model.name)
    */
   public SessionIdentifier export(Session_Low hls) 
      throws ExportException, JonathanException;
}


