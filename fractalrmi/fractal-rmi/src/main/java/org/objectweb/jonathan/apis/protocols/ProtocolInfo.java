package org.objectweb.jonathan.apis.protocols;

import org.objectweb.jonathan.apis.kernel.Context;

/**
 * Tree-like structure to hold the protocol stack info at
 * configuration time. This will be used in the Binder to instanciate
 * server side protocol graphs.
 */
public class ProtocolInfo {
    
    Protocol current;
    ProtocolInfo[] lower;
    Context hints = null; // optional!

    /**
     * Create a leaf ProtocolInfo node.
     * 
     * @param current the protocol id of this node.
     */
    public ProtocolInfo(Protocol current) {
	this.current = current;
	lower = new ProtocolInfo[0];
    }

    /**
     * Create a non-leaf ProtocolInfo node.
     * 
     * @param current protocol id of this node.
     * @param lower the lower nodes.
     */
    public ProtocolInfo(Protocol current, ProtocolInfo[] lower) {
	this.current = current;
	this.lower = lower;
    }

    /**
     * Set the hints that may help things to happen smoothly
     * (e.g. configure the port in TCP/IP, ...).
     * 
     * @param hints the new hints.
     */
    public void setHints(Context hints) {
	this.hints = hints;
    }

    /**
     * Get the lower nodes.
     * 
     * @return the lower nodes.
     */
    public ProtocolInfo[] getLower() {
	return lower;
    }

    /**
     * Get the protocol id of this node.
     * 
     * @return the protocol id of this node.
     */
    public Protocol getProtocol() {
	return current;
    }

    public Context getHints() {
	return hints;
    }

    public String toString() {
	return "ProtocolInfo [ " + current.toString() + ", [ " +lower + " ]]";
    }
}
