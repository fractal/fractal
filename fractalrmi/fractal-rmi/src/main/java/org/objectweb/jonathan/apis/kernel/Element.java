/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 2000 France Telecom R&D
 * Copyright (C) 2001 Kelua SA
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 3.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Fabien Delpiano
 * 
 */

package org.objectweb.jonathan.apis.kernel;

/** 
 * The Element interface represents an element in a context.
 */
public interface Element {

   /** 
    * Returns the (local) name of the element, in the context it belongs to.
    * @return the (local) name of the element, in the context it belongs to.
    */
   String getName();

   /** 
    * Returns the component contained in the target element.
    * @return the component contained in the target element.
    */
   Component  getComponent();

   /** 
    * Returns the type of the target element, as a Class.
    * @return the type of the target element, as a Class.
    */
   Class  getType();

   /** 
    * Returns the value of the target element, if its type is an object reference
    * type.
    * <p>
    * If the target element is of an integral type, 
    * {@link Context#NO_VALUE NO_VALUE}} is returned.
    *
    * @return the value of the target element.
    */
   Object getValue();

   /** 
    * Returns the value of the target element, if its class is an integer class.
    * <p>
    * If the target element has an object reference type, Integer.MAX_VALUE is 
    * returned.
    *
    * @return the value of the target element.
    */
   int getIntValue();

   /** 
    * Returns the context containing the target element, if any, null otherwise.
    * @return the context containing the target element, if any, null otherwise.
    */
   Component getContainer();
}
