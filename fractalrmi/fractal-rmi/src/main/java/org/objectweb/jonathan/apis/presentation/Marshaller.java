/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.presentation;

import java.io.OutputStream;

import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.kernel.Context;
import org.objectweb.jonathan.apis.resources.Chunk;

/**
 * Marshaller is the type of basic types marshallers. They should be used
 * by protocols.
 */
public interface Marshaller {

   /** 
    * Writes a chunk in the message. 
    * <p> 
    * The target becomes the "owner" of the provided chunk, and therefore is not 
    * supposed to duplicate it. If the entity invoking this operation wants to keep
    * a reference to the chunk, it must be duplicated.
    *
    * @param chunk the chunk to be written.
    */
   void write(Chunk chunk);


   /**
    * Writes a byte.
    * 
    * @param b a byte;
    * @exception JonathanException if a marshal error occurred.
    */
   void writeByte(byte b)
      throws JonathanException;
   
   /**
    * Writes a boolean.   
    *
    * @param b a boolean;
    * @exception JonathanException if a marshal error occurred.
    */
   void writeBoolean(boolean b)
      throws JonathanException;

   /**
    * Writes an 8 bits char.
    * <p>
    * The method used to translate the provided <code>char</code> into an 8 bits 
    * entity is not specified.
    *
    * @param i a char;
    * @exception JonathanException if a marshal error occurred.
    */
   void writeChar8(char i)
      throws JonathanException;   

   /**
    * Writes a 16 bits char.
    *
    * @param i a char;
    * @exception JonathanException if a marshal error occurred.
    */
   void writeChar16(char i)
      throws JonathanException;   

   /**
    * Writes a short.
    *
    * @param i a short;
    * @exception JonathanException if a marshal error occurred.
    */
   void writeShort(short i)
      throws JonathanException;

   /**
    * Writes an int.
    *
    * @param i an int;
    * @exception JonathanException if a marshal error occurred.
    */
   void writeInt(int i)
      throws JonathanException;

   /**
    * Writes a long.
    *
    * @param i a long;
    * @exception JonathanException if a marshal error occurred.
    */
   void writeLong(long i)
      throws JonathanException;

   /**
    * Writes a float.
    *
    * @param f a float;
    * @exception JonathanException if a marshal error occurred.
    */
   void writeFloat(float f)
      throws JonathanException;

   /**
    * Writes a double.
    *
    * @param d a double;
    * @exception JonathanException if a marshal error occurred.
    */
   void writeDouble(double d)
      throws JonathanException;

   /**
    * Writes a string of 8 bits chars.
    *
    * @param s a string;
    * @exception JonathanException if a marshal error occurred.
    */
   void writeString8(String s)
      throws JonathanException;
   
   /**
    * Writes a string of 16  bits chars.
    *
    * @param s a string;
    * @exception JonathanException if a marshal error occurred.
    */
   void writeString16(String s)
      throws JonathanException;

   /** 
    * Writes an object reference in the marshaller.
    *
    * @param obj an object reference
    * @exception JonathanException if a marshal error occurred.
    */
   void writeReference(Object obj) throws JonathanException;

   /** 
    * Writes a value in the marshaller.
    *
    * @param obj an object
    * @exception JonathanException if a marshal error occurred.
    * @since 3.0 a2
    */
   void writeValue(Object obj) throws JonathanException;

   /**
    * Writes an array of bytes.
    *
    * @param array an array of bytes;
    * @param offset index of the first byte in array that must be written.
    * @param length number of bytes of array that must be written.
    * @exception JonathanException if a marshal error occurred.
    */
   void writeByteArray(byte[] array, int offset, int length)
      throws JonathanException;

   
   /** 
    * Returns an output stream to write into the message. Closing the output stream
    * has the same effect as closing the marshaller itself.
    *
    * @return an output stream to write into the message.
    */
   OutputStream outputStream();


   /** 
    * Checks if the target marshaller and the provided one have the same contents, 
    * i.e., they contain the same bits.
    * 
    * @param marshaller a marshaller;
    * @return true if the target marshaller and the provided one have the same 
    *              contents, false otherwise.
    */
   boolean sameContents(Marshaller marshaller);   


   /**
    * Returns true if this marshaller is little-endian, false otherwise.
    *
    * @return true if this marshaller is little-endian, false otherwise.
    */
   boolean isLittleEndian();

   /** 
    * Returns the state of the message as a (chain of) chunk(s). 
    * <p>
    * The returned chunk(s) are NOT duplicated. If the caller keeps a
    * reference to them, it must {@link #reset() reset} the message, and not 
    * continue to use it.
    * 
    * @return the state of the message as a (chain of) chunk(s).
    */
   Chunk getState();

   /** 
    * Returns the current offset in the message, i.e., the position in the message 
    * at which the next byte will be written.
    * 
    * @return the current offset in the message.
    */
   int getOffset();


   /** 
    * Sets the offset in the message.
    * <p>
    * This method may be used to override data already written into the message.
    * 
    * @param offset the new offset.
    */
   void setOffset(int offset);

   /** 
    * Returns a {@link Context Context} associated with this marshaller.
    * @return a {@link Context Context} associated with this marshaller.
    */
   Context getContext();

   /** 
    * This method causes the message to lose all its references to the underlying 
    * chunks, without
    * {@link org.objectweb.jonathan.apis.resources.Chunk#release() releasing}
    * them. This method must not be used if no reference to chunks present in the
    * message is held by an entity in charge of their release.   It also releases 
    * the context associated with the target marshaller.
    */
   void reset();


   /** 
    * This method causes the message to lose all its references to the underlying 
    * chunks, and
    * {@link org.objectweb.jonathan.apis.resources.Chunk#release() release}  each
    * of them. It also released the context.
    */
   void close();

}


