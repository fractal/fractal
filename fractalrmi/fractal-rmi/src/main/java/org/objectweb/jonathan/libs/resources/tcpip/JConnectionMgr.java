/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999-2000 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 3.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */

package org.objectweb.jonathan.libs.resources.tcpip;

import java.io.IOException;

import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.protocols.ip.IpConnection;
import org.objectweb.jonathan.apis.protocols.ip.IpSession;
import org.objectweb.jonathan.apis.protocols.ip.TcpIpConnectionMgr;
import org.objectweb.jonathan.apis.protocols.ip.TcpIpSrvConnectionFactory;
import org.objectweb.jonathan.apis.resources.Chunk;

/** 
 * Default implementation of a connection manager and factory.
 */
public class JConnectionMgr implements TcpIpConnectionMgr {
   
   Connection[] connections = new Connection[101];
   int connects;
   SrvConnectionFactory server_connection_factories;

   Connection first_idle;
   Connection last_idle;
   int idle;
   
   protected TcpIpConnectionMgr factory;

   /**
    * Maximum number of idle connections kept by this manager.
    * This constant is defined under the name <code>/jonathan/tcpip/max_idle</code>
    * in the bootstrap context.
    */
   public int max_idle;

   /** 
    * Returns a new connection manager
    * @param max_idle maximum number of idle connections
    * @param connection_factory a factory to actually create connections
    */
   public JConnectionMgr(int max_idle,TcpIpConnectionMgr connection_factory) {
      connects = 0;
      this.max_idle = max_idle;
      this.factory = connection_factory;
   }

   /** 
    * Returns the canonical host name of the provided host.
    * @param hostname a host name
    * @return the corresponding canonical host name.
    */
   public String getCanonicalHostName(String hostname) {
      return factory.getCanonicalHostName(hostname);
   }
   
   /** 
    * Returns a new client connection. 
    * <p>
    * This method is called by a protocol.
    * The protocol provides a session (i.e. an object
    * representing an abstract  communication channel) and expects a connection
    * (i.e. a communication resource). The returned connection must have been
    * built using the provided session, or be a connection associated with a
    * session having the same destination as the provided session.
    * @param host the host name of the distant server;
    * @param port the port number of a server socket on that host;
    * @param session a TcpIp session
    * @return a connection for that session.
    * @exception JonathanException if an error occurs.
    */
   public synchronized IpConnection newCltConnection(String host, int port, 
                                                     IpSession session)
      throws JonathanException {
      host = getCanonicalHostName(host);
      int hash = host.hashCode() + port; 
      int len = connections.length;
      int index = (hash & 0x7FFFFFFF) % len;
      Connection connection = connections[index];
      while (connection != null) {
         if (connection.getPort() == port &&
             connection.getHostName().equals(host)) {
            if (connection.getSession() == null) {
               connection.acquire();
               connection.setSession(session);
               return connection;
            } else if (connection.getSession().equals(session)) {
               connection.acquire();   
               return connection;
            }
         }
         connection = connection.next;
      }
      connection =
         newCltConnection(factory.newCltConnection(host,port,session));
      Connection first = connections[index];
      connections[index] = connection;
      connection.next = first;
      connects++;
      if (connects > len / 2) {
         rehash(len);
      }
      return connection;
   }

   /** 
    * Returns a new server connection factory encapsulating a server socket on the
    * provided port. If port = 0, an anonymous server socket is opened.
    * @param port the expected port of the server socket;
    * @return a server connection factory.
    * @exception JonathanException if an error occurs.
    */
   public TcpIpSrvConnectionFactory newSrvConnectionFactory(int port) 
      throws JonathanException {
      SrvConnectionFactory fac = server_connection_factories;
      while (fac != null) {
         if (fac.getPort() == port) {
            break;
         }
         fac = fac.next;
      }
      if (fac == null) {
         fac = server_connection_factories;
         server_connection_factories = 
            new SrvConnectionFactory(factory.newSrvConnectionFactory(port));
         server_connection_factories.next = fac;
         return server_connection_factories;
      } else {
         return fac;
      }
   }
      
   /** 
    * Builds a new client-side connection encapsulating the provided connection.
    *
    * @param connection a tcpip connection. 
    * @return a new client-side connection.
    * @exception JonathanException JonathanException if something goes wrong.
    */
   protected Connection newCltConnection(IpConnection connection) 
      throws JonathanException {
      return new Connection(connection);
   }
   
   /** 
    * Removes the connection from the set of managed connections. 
    * @param connection the connection to remove.
    */   
   void remove(Connection connection) {
      int hash = connection.hashCode();
      int len = connections.length;
      int index = (hash & 0x7FFFFFFF) % len;
      Connection current = connections[index];
      Connection prev = null;
      while (current != null) {
         if (connection == current) {
            if (prev == null) {
               connections[index] = current.next;
            } else {
               prev.next = current.next;
            }
            return;
         } else {
            current = current.next;
         }
      }
   }

   void add(Connection connection) {
      int hash = connection.hashCode();
      int len = connections.length;
      int index = (hash & 0x7FFFFFFF) % len;
      Connection first = connections[index];
      connections[index] = connection;
      connection.next = first;
      connects++;
      if (connects > len / 2) {
         rehash(len);
      }
   }

   void removeConnectionFactory(SrvConnectionFactory factory) {
      SrvConnectionFactory connection_factory = 
         server_connection_factories, prev = null;
      while (connection_factory != null) {
         if (connection_factory == factory) {
            if (prev != null) {
               prev.next = connection_factory.next;
            } else {
               server_connection_factories = connection_factory.next;
            }
            return;
         }
         prev = connection_factory;
         connection_factory = connection_factory.next;
      }
   }

   
   // rehashes the hash table, if necessary.
   void rehash(int len) {
      Connection entry, next_entry, first_entry;
      int index;
      int new_len = 2 * len + 1;         
      Connection[] new_table = new Connection[new_len];
      
      for (int i = 0; i < len; i++) {
         entry = connections[i];
         while (entry != null) {
            next_entry = entry.next;
            // rehashing
            index = (entry.hashCode() & 0x7FFFFFFF) % new_len;
            first_entry = new_table[index];
            new_table[index] = entry;
            entry.next = first_entry;
            //
            entry = next_entry;
         } 
      } 
      connections = new_table;  
   }

   /** 
    * Implementation of TcpIpConnection.
    */
   public class Connection implements IpConnection {   
      /**  Next connection in the idle list (null if connection is active) */
      Connection next_idle;
      /**  Previous connection in the idle list (null if connection is active) */
      Connection prev_idle;
      /**  Number of objects using that connection. */
      int acquired;
      /** The next connection in a collision list */
      Connection next;
      
      IpConnection delegate; 

      /** 
       * Builds a new connection.
       * 
       * @param delegate next connection in the idle list (null if connection 
       * is active).
       */
      protected Connection(IpConnection delegate) {
         this.delegate = delegate;
         acquired = 1;
      }      

      public int available() throws IOException {
         return delegate.available();
      }
      
      public void receive(Chunk chunk,int to_read) throws IOException {
         delegate.receive(chunk,to_read);
      }
      
      public void emit(Chunk chunk) throws IOException {
         delegate.emit(chunk);
      }

      /** 
       * Returns the port number of the underlying socket.
       * @return the port number of the underlying socket.
       */
      public int getPort() {
         return delegate.getPort();
      }      

      /** 
       * Returns the host name of the underlying socket.
       * @return the host name of the underlying socket.
       */
      public String getHostName() {
         return delegate.getHostName();
      }   
      
      /** 
       * Returns the session attached to this connection.
       * @return the session attached to this connection.
       */
      public IpSession getSession() { 
         return delegate.getSession();
      }
      

      /** 
       * Attaches a new session to this connection.
       * @param session the session to be attached to the target connection.
       */
      public void setSession(IpSession session) { 
         delegate.setSession(session);
      }

      /** 
       * Deletes this connection, removing it from the connection manager, and
       * closing the socket. This method should not be used a a socket user unless 
       * a problem occurs on the connection, like an exception when trying to read 
       * or to write data.
       */
      public void delete() {
         synchronized(JConnectionMgr.this) {
            delegate.delete();
            withdraw();
         }
      }
   
      /** 
       * Returns when the socket is acquired. This information is taken into
       * account by the connection manager to avoid closing connections still in 
       * use.
       */
      public void acquire() { 
         synchronized(JConnectionMgr.this) {
            acquired++;
            if (acquired == 1) {
               if (prev_idle != null) {
                  prev_idle.next_idle = next_idle;               
               } else {
                  first_idle = next_idle;
               }
               if (next_idle != null) {
                  next_idle.prev_idle = prev_idle;
               } else {
                  last_idle = prev_idle;
               }
               prev_idle = next_idle = null;
               idle--;
            }
         }
      }
   
      /** 
       * Releases this connection. This is to indicate to the connection manager 
       * that the target connection is no longer used.  
       */
      public void release() {
         synchronized(JConnectionMgr.this) {      
            acquired--;
            if (acquired == 0) {
               if (last_idle != null) {
                  last_idle.next_idle = this;
                  last_idle = this;
               } else {
                  last_idle = first_idle = this;
               }
               if (idle >= max_idle) {
                  first_idle.delegate.release();
                  first_idle.withdraw();
               }
               idle++;
            }
         }
      }

      void withdraw() {
         remove(this);
         if (next_idle != null) {
            if (prev_idle != null) {
               prev_idle.next_idle = next_idle;
               next_idle.prev_idle = prev_idle;
            } else {
               first_idle = next_idle;
               next_idle.prev_idle = null;
            }
            idle--;
         } else if (prev_idle != null) {
            last_idle = prev_idle;
            prev_idle.next_idle = null;
            idle--;
         } else if (last_idle == this) {
            // may occur if max_idle <= 0
            last_idle = null;
            prev_idle = null;
            idle--;
         }
      }

      public String toString() {
         return "JConnectionMgr.Connection" + System.identityHashCode(this) 
            + "[" + delegate + "]";
      }

      public int hashCode() {
         int hashcode = delegate.getPort() + delegate.getHostName().hashCode();
         return hashcode;
      }
   }

   class SrvConnectionFactory implements TcpIpSrvConnectionFactory {

      TcpIpSrvConnectionFactory delegate;
      SrvConnectionFactory next;
   
      SrvConnectionFactory(TcpIpSrvConnectionFactory delegate)
         throws JonathanException {
         this.delegate = delegate;
      }

      public IpConnection newSrvConnection(IpSession session) 
         throws JonathanException {
         return delegate.newSrvConnection(session);
      }
   
      public int getPort() { 
         return delegate.getPort();
      }
      
      public String getHostName() {
         return delegate.getHostName();
      }
      
      public void close() {
         synchronized(JConnectionMgr.this) {
            delegate.close();
            removeConnectionFactory(this);
         }
      }
   }
}





