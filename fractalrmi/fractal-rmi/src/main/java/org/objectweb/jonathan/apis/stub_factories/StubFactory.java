/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.stub_factories;

import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.kernel.Context;
import org.objectweb.jonathan.apis.binding.Identifier;
import org.objectweb.jonathan.apis.protocols.SessionIdentifier;

/** 
 * A Stub factory is a factory for stubs...
 */
public interface StubFactory {


   /** 
    * Creates a new stub.
    * <p>
    * A stub plays two roles:
    * <ul>
    * <li> It is the local representative of a (remote) object, and thus should 
    *      bear a set of identifiers for that remote object;
    * <li> It is part of the binding between the client and the server, and is 
    *      thus related to a given protocol. The session identifier provided as an
    *      argument is the manifestation of this relation. It can be used to 
    *      obtain a session allowing the stub to send data to the remote object.
    * </ul>
    *
    * @param session_id  a session identifier, to be used to send marshalled
    *                    data to the object represented by the stub;
    * @param ids the set of identifiers of the stub;
    * @param hints other data possibly used to create the stub.
    * @return a stub
    * @exception JonathanException if something goes wrong.
    */
   Object newStub(SessionIdentifier session_id, Identifier[] ids,Context hints)
      throws JonathanException;
}
