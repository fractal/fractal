/***
 * Jonathan: an Open Distributed Processing Environment
 * Copyright (C) 1999-2006 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Release: 3.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * Contributor: Bruno Dillenseger
 */


package org.objectweb.jonathan.libs.resources.tcpip;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.StringTokenizer;
import org.objectweb.jonathan.apis.binding.ExportException;
import org.objectweb.jonathan.apis.kernel.InternalException;
import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.protocols.ip.IpConnection;
import org.objectweb.jonathan.apis.protocols.ip.IpSession;
import org.objectweb.jonathan.apis.protocols.ip.TcpIpConnectionMgr;
import org.objectweb.jonathan.apis.protocols.ip.TcpIpSrvConnectionFactory;
import org.objectweb.jonathan.apis.resources.Chunk;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 * Implementation of an IP V4 connection factory.
 * When several IPv4 addresses are vailable on current host, you may set
 * system property jonathan.connectionfactory.host to the one address
 * you want to be used, or alternatively to a subnet number in the form
 * d.d.d.d/n (subnet number/number of significant bits) 
 */
public class IPv4ConnectionFactory implements TcpIpConnectionMgr {
   /**
    * this system property may specify a local host address to use when creating
    * server sockets - very useful when working with several subnets...
    */
   static private final String DEFAULT_HOST_PROP = "jonathan.connectionfactory.host";
   static String default_localhost_name;
   static String default_localhost_address;
   static InetAddress default_localhost = null;

   static {
		try {
			// the default host address to use may be set through a system property...
			String default_host_prop = System.getProperty(DEFAULT_HOST_PROP);
			if (default_host_prop != null && default_host_prop.indexOf('/') == -1) {
				default_localhost = Inet4Address.getByName(default_host_prop);
			}
			else {
				// ... otherwise, look for an IP address among local network interfaces,
				// ignoring IPv6 addresses
				Enumeration interfaces = NetworkInterface.getNetworkInterfaces();
				while (
					interfaces.hasMoreElements()
					&& (default_localhost == null || default_localhost.isLoopbackAddress()))
				{
					Enumeration addresses = ((NetworkInterface)
						interfaces.nextElement()).getInetAddresses();
					while (
						addresses.hasMoreElements()
						&& (default_localhost == null
							|| default_localhost.isLoopbackAddress()))
					{
						InetAddress address = (InetAddress) addresses.nextElement();
						// keep this address if it is a IPv4 address
						// from the specified subnetwork if any
						if (
							!(address instanceof Inet6Address)
							&& (default_host_prop == null
								|| belongsToSubnet((Inet4Address) address, default_host_prop)))
						{
							default_localhost = address;
						}
					}
				}
				// if no valid address can be found, use a loopback address
				if (default_localhost == null) {
					default_localhost = InetAddress.getByName(null);
				}
			}
		}
		catch (Exception ignored) {
			if ((LoggerProvider.logger != null)
				&& (LoggerProvider.logger.isLoggable(BasicLevel.WARN)))
			{
				LoggerProvider.logger.log(BasicLevel.WARN,
						"Could not determine local host address.");
			}
		}
		if (default_localhost != null) {
			default_localhost_name = default_localhost.getHostName();
			default_localhost_address = default_localhost.getHostAddress();
		}
   }


   /**
    * Checks if an IPv4 address belongs to a IPv4 subnetwork.
    * @param address an IPv4 address
    * @param subnet an IPv4 subnet number in the form d.d.d.d/n
    * (e.g 192.168.1.0/24 24 being the number of significant bits, in other words
    * bits that would be set to 1 in the netmask)
    * @return true if the given address belongs to the given subnet, false otherwise.
    */
   static private boolean belongsToSubnet(Inet4Address address, String subnet) {	   
		byte[] bytes = address.getAddress();
		long addressHash = 255 & bytes[0];
		int subnetBits = 32 - Integer.parseInt(subnet.substring(subnet.indexOf('/') + 1));
		StringTokenizer parser = new StringTokenizer(subnet, "./");
		long subnetHash = Integer.parseInt(parser.nextToken());
		for (int i=1 ; i<4 ; ++i)
		{
			addressHash = (addressHash << 8) + (255 & bytes[i]);
			subnetHash = (subnetHash << 8) + Integer.parseInt(parser.nextToken());
		}
		addressHash >>= subnetBits;
		subnetHash >>= subnetBits;
		return addressHash == subnetHash;
	}


   InetAddress localhost;
   public String localhost_name;

   /**
	 * Indicates whether warning messages should be printed to stderr.
	 */
   public boolean verbose;

   /**
	 * Indicates whether the names used to identify endpoints should be host
	 * names or host addresses.
	 */
   public boolean use_address_as_name;

   /**
    * If >= 0, enables the SO_LINGER option with the specified linger time.
    */
   public int SO_LINGER;

   /**
    * If >= 0, enables the SO_TIMEOUT option with the specified timeout (in
    * ms). 0 means infinity (no timeout).
    */
   public int SO_TIMEOUT;

   /**
    * If true, enables the TCP_NODELAY option.
    */
   public boolean TCP_NODELAY;

   /**
    * Maximum length of the queue on incoming connect requests.
    */
   public int backlog;

   int max_retries;


   
   /** 
    * Returns a new IPv4ConnectionFactory with default parameters.
    * 
    * @exception JonathanException if something goes wrong.
    */
   public IPv4ConnectionFactory() throws JonathanException {
      this(null,false,100,0,true,50,10,true);
   }
   

   /** 
    * Returns a new IPv4 connection factory
    * @param localhost_value string representing the local machine (may be null)
    * @param verbose indicates whether warning messages should be output on stderr
    * @param SO_LINGER SO_LINGER parameter used to create sockets
    * @param SO_TIMEOUT SO_TIMEOUT parameter used to create sockets
    * @param TCP_NODELAY TCP_NODELAY parameter used to create sockets
    * @param backlog backlog parameter used to create sockets
    * @param max_retries maximum number of retries when creating a socket
    * @param use_address_as_name use a.b.c.d adresses instead of DNS names 
    * @exception JonathanException if something goes wrong
    */
   public IPv4ConnectionFactory(String localhost_value,
                                boolean verbose, 
                                int SO_LINGER, int SO_TIMEOUT,
                                boolean TCP_NODELAY, int backlog,
                                int max_retries, boolean use_address_as_name) 
      throws JonathanException {
      
      this.verbose = verbose;
      this.SO_LINGER = SO_LINGER;
      this.SO_TIMEOUT = SO_TIMEOUT;
      this.TCP_NODELAY = TCP_NODELAY;
      this.backlog = backlog;
      this.max_retries = max_retries;
      if (max_retries <= 0) {
         this.max_retries = 1;
      }
      this.use_address_as_name = use_address_as_name;
      
      try{
         if (localhost_value != null &&
             (localhost_value.equals("localhost") || 
              localhost_value.equals("127.0.0.1"))) {
            localhost_value = null;
         }
         if (localhost_value != null) {
            localhost = InetAddress.getByName(localhost_value);
            if (use_address_as_name) {
               localhost_name = localhost.getHostAddress();
            } else {
               localhost_name = localhost.getHostName();
            }
         } else {
            localhost = default_localhost;
            if (use_address_as_name) {
               localhost_name = default_localhost_address;
            } else {
               localhost_name = default_localhost_name;
            }
         }
      } catch(UnknownHostException e){
         throw new JonathanException(e);
      }
   }

   public void setVerbose(boolean verbose) {
      this.verbose = verbose;
   }

   /**
    * Builds a new client-side connection with the provided parameters.
    *
    * @param host    a host name;
    * @param port    a port number;
    * @param session a TCP/IP session;
    * @return        a new client-side connection.
    * @exception JonathanException if something goes wrong.
    */
   public IpConnection newCltConnection(String host,int port,
                                        IpSession session)
      throws JonathanException {
      try {
         if (host == null || host.equals("localhost") || host.equals("127.0.0.1")) {
            host = default_localhost_name;
         }
//         if (verbose) {
//            System.err.println("Trying to connect to " + host +
//                               " on port " + (port & 0xFFFF) + ".");
//         }
         // begin log
         if ((LoggerProvider.bind_logger != null)
            &&(LoggerProvider.bind_logger.isLoggable(BasicLevel.INFO))) {
            LoggerProvider.bind_logger.log(BasicLevel.INFO,"Trying to connect to " + host +
                               " on port " + (port & 0xFFFF) + ".");
         }
         // end log
         Socket socket = null;
         for (int i = 0; i < max_retries; i++)  {
            try {
               socket = new Socket(host,port & 0xFFFF);
               break;
            } catch (ConnectException e) {
               if (i == (max_retries - 1)) {
                  throw new JonathanException(e);
//               } else if (verbose) {
//                  System.err.println("Re-trying to connect to " + host +
//                                     " on port " + (port & 0xFFFF) + ".");
//               }
               // begin log
            } else if ((LoggerProvider.bind_logger != null)
                  &&(LoggerProvider.bind_logger.isLoggable(BasicLevel.INFO))) {
                  LoggerProvider.bind_logger.log(BasicLevel.INFO,"Re-trying to connect to " + host +
                                     " on port " + (port & 0xFFFF) + ".");
               }
               // end log
            }
         }
         if (SO_LINGER >= 0) {
            socket.setSoLinger(true,SO_LINGER);
         }
         if (SO_TIMEOUT >= 0) {
            socket.setSoTimeout(SO_TIMEOUT);
         }
         socket.setTcpNoDelay(TCP_NODELAY);
//          port = (short) socket.getPort();
          port =  socket.getPort() & 0xFFFF ;

//         if (verbose) {
//            System.err.println("Connected to " + socket);
//         }
         // begin log
         if ((LoggerProvider.bind_logger != null)
            &&(LoggerProvider.bind_logger.isLoggable(BasicLevel.INFO))) {
            LoggerProvider.bind_logger.log(BasicLevel.INFO,"Connected to " + socket);
         }
         // end log
         return new Connection(socket,session,host,port);
      } catch (IOException e) {
         throw new JonathanException(e);
      }
   }

   /**
    * Returns a new server connection factory encapsulating a server socket on the
    * provided port. If port = 0, an anonymous server socket is opened.
    * @param port the expected port of the server socket;
    * @return a server connection factory.
    * @exception JonathanException if an error occurs.
    */
   public TcpIpSrvConnectionFactory newSrvConnectionFactory(int port)
      throws JonathanException {
      return new SrvConnectionFactory(localhost,port,backlog);
   }

   /**
    * Returns the canonical host name corresponding to hostname.
    *
    * If use_address_as_name is true, the canonical host name is the
    * string corresponding to the IP address, else it is the machine name.
    *
    * @param hostname a host name.
    * @return the canonical host name corresponding to hostname.
    */
   public String getCanonicalHostName(String hostname) {
      if (hostname != null && hostname.length() > 0) {
         if (use_address_as_name) {
            if (! Character.isDigit(hostname.charAt(0))) {
               if (hostname.equals("localhost")) {
                  return default_localhost_address;
               } else {
                  try {
                     return InetAddress.getByName(hostname).getHostAddress();
                  } catch (Exception ignored) {
                  }
               }
            } else if (hostname.equals("127.0.0.1")) {
               return default_localhost_address;
            }
         } else {
            if (Character.isDigit(hostname.charAt(0))) {
               if (hostname.equals("127.0.0.1")) {
                  return default_localhost_name;
               } else {
                  try {
                     return InetAddress.getByName(hostname).getHostName();
                  } catch (Exception ignored) {
                  }
               }
            } else if (hostname.equals("localhost")) {
               return default_localhost_name;
            }
         }
      }
      return hostname;
   }

   /**
    * Builds a new server-side connection with the provided parameters.
    *
    * @param socket  a TCP/IP socket;
    * @param session a TCP/IP session;
    * @param host    a host name;
    * @param port    a port number;
    * @return        a new server-side connection.
    * @exception JonathanException if something goes wrong.
    */
   protected Connection newSrvConnection(Socket socket,IpSession session,
                                         String host, int port)

      throws JonathanException {
      return new Connection(socket,session,host,port);
   }

   /**
    * Implementation of TcpIpConnection.
    */
   class Connection implements IpConnection {

      /**  The encapsulated socket. */
      Socket socket;
      String host;
      int port;
      InputStream is;
      OutputStream os;

      /**  The related session. */
      IpSession session;

      /**
       * Builds a new connection.
       *
       * @param socket   a socket;
       * @param session  a session;
       * @param host a host name;
       * @param port a tcp port;
       * @exception JonathanException if something goes wrong.
       */
      protected Connection(Socket socket, IpSession session,
                           String host, int port)

         throws JonathanException {
         if (socket != null) {
            try {
               is = socket.getInputStream();
               os = socket.getOutputStream();
            } catch (IOException e) {
               try {
                  socket.close();
               } catch (IOException ignored) {
               }
               throw new JonathanException(e);
            }
         } else {
            throw new InternalException("Null socket to create connection");
         }
         this.socket = socket;
         this.session = session;
         this.host = host;
         this.port = port;
      }

      public int available() throws IOException {
         return is.available();
      }

      public void emit(Chunk c) throws IOException {
         synchronized (os) {
//              byte[] data = c.data;
//              int len = c.top - c.offset;
//              String str = "OUT: ";
//              for (int i = c.offset; i < len; i++) {
//                 str = str + data[i] + " ";
//              }
//              System.out.println(str);
            // begin log
            if ((LoggerProvider.send_logger != null)
               &&(LoggerProvider.send_logger.isLoggable(BasicLevel.DEBUG))) {
               byte[] data = c.data;
               int len = c.top - c.offset;
               String str = "OUT: ";
               for (int i = c.offset; i < len; i++) {
                  str = str + data[i] + " ";
               }
               LoggerProvider.send_logger.log(BasicLevel.DEBUG,str);
            }
            // end log
            os.write(c.data,c.offset,c.top);
         }
      }

      public void receive(Chunk c,int sz) throws IOException {
         byte[] data = c.data;
         int top = c.top;
//           System.err.println(Thread.currentThread() + 
//                              " trying to read " + sz + " bytes from " +
//                              socket.getPort());
         while (sz > 0) {
            int a = is.read(data,top,sz);
//              String str = "IN: ";
//              for (int i = top; i < top + sz; i++) {
//                 str = str + data[i] + " ";
//              }
//              System.out.println(str);
            // begin log
            if ((LoggerProvider.receive_logger != null)
               &&(LoggerProvider.receive_logger.isLoggable(BasicLevel.DEBUG))) {
               String str = "IN: ";
               for (int i = top; i < top + sz; i++) {
                  str = str + data[i] + " ";
               }
               LoggerProvider.receive_logger.log(BasicLevel.DEBUG,str);
            }
            // end log
            if (a > 0) {
               top += a;
               sz -= a;
            } else {
               throw new EOFException();
            }
         }
//           System.err.println(Thread.currentThread() + 
//                              " read " + (top - c.top) + " bytes from " +
//                              socket.getPort());

         c.top = top;
      }


      /**
       * Returns the port number of the underlying socket.
       * @return the port number of the underlying socket.
       */
      public int getPort() {
         return port;
      }

      /**
       * Returns the host name of the underlying socket.
       * @return the host name of the underlying socket.
       */
      public String getHostName() {
         return host;
      }

      /**
       * Returns the session attached to this connection.
       * @return the session attached to this connection.
       */
      public IpSession getSession() {
         return session;
      }

      /**
       * Attaches a new session to this connection.
       * @param session the session to be attached to the target connection.
       */
      public void setSession(IpSession session) {
         this.session = session;
      }

      /**
       * Deletes this connection, removing it from the connection manager, and
       * closing the socket. This method should not be used by a socket user unless
       * a problem occurs on the connection, like an exception when trying to read
       * or to write data.
       */
      public synchronized void delete() {
         if (socket != null) {
            try {
//               if (verbose) {
//                  System.err.println("Connection with host " + host +
//                                     " on port " +
//                                     (port & 0xFFFF) + " closed.");
//               }
               // begin log
               if ((LoggerProvider.logger != null)
                  &&(LoggerProvider.logger.isLoggable(BasicLevel.INFO))) {
                  LoggerProvider.logger.log(BasicLevel.INFO,"Connection with host " + host +
                                     " on port " +
                                     (port & 0xFFFF) + " closed.");
               }
               // end log
               socket.close();
            } catch (IOException ignored) {}
            socket = null;
            session = null;
         }
      }

      /**
       * Releases this connection. This is to indicate to the connection manager
       * that the target connection is no longer used.
       */
      public void release() {
         delete();
      }

      public String toString() {
         return "IPv4Connection[" + socket + "]";
      }

   }

   class SrvConnectionFactory implements TcpIpSrvConnectionFactory {
      int port;
      String hostname;
      ServerSocket server_socket;

      SrvConnectionFactory(InetAddress localhost,int port,int backlog)
         throws JonathanException {
         if (use_address_as_name) {
            this.hostname = localhost.getHostAddress();
         } else {
            this.hostname = localhost.getHostName();
         }
         try {
            server_socket = new ServerSocket(port & 0xFFFF,backlog,localhost);
            if (server_socket != null) {
               this.port = server_socket.getLocalPort() & 0xFFFF;
//               if (verbose) {
//                  System.err.println("Opened a server connection on host " +
//                                     hostname +
//                                     "\n                           on port " +
//                                     (this.port & 0xFFFF));
//               }
               // begin log
               if ((LoggerProvider.export_logger != null)
                  &&(LoggerProvider.export_logger.isLoggable(BasicLevel.INFO))) {
                  LoggerProvider.export_logger.log(BasicLevel.INFO,"Opened a server connection on host " +
                                     hostname + " on port " +
                                     (this.port & 0xFFFF));
               }
               // end log
            } else {
               throw new ExportException("Can't create server socket.");
            }
         } catch (IOException e) {
            throw new ExportException(e);
         }
      }

      public IpConnection newSrvConnection(IpSession session)
         throws JonathanException {
         try {
            ServerSocket current = null;
            synchronized (this) {
               if (server_socket != null) {
                  current = server_socket;
               } else {
                  throw new ExportException("Server Connection Factory has been closed");
               }
            }

            Socket socket = current.accept();
            if (SO_LINGER >= 0) {
               socket.setSoLinger(true,SO_LINGER);
            }
            if (SO_TIMEOUT >= 0) {
               socket.setSoTimeout(SO_TIMEOUT);
            }
            socket.setTcpNoDelay(TCP_NODELAY);
            int port = socket.getPort() & 0xFFFF;
            String hostname = socket.getInetAddress().getHostName();
//            if (verbose) {
//               System.err.println("Accepted connection with host: " +
//                                  hostname +
//                                  "\n                     on port: " +
//                                  port);
//            }
            // begin log
            if ((LoggerProvider.bind_logger != null)
               &&(LoggerProvider.bind_logger.isLoggable(BasicLevel.INFO))) {
               LoggerProvider.bind_logger.log(BasicLevel.INFO,"Accepted connection with host: " +
                                  hostname + " on port: " + port);
            }
            // end log
            return
               IPv4ConnectionFactory.this.newSrvConnection(socket,session,hostname,
                                                           port);
         } catch (IOException e) {
            throw new ExportException(e);
         }
      }

      public int getPort() {
         return port;
      }

      public String getHostName() {
         return hostname;
      }

      public synchronized void close() {
         if (server_socket != null) {
            try {
               server_socket.close();
            } catch (IOException ignored) {}
            server_socket = null;
         }
      }
   }
}
