/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.resources;

/**
 * Schedulers are used to create and schedule Jobs.
 * <p>
 * To allow application programmers to change the default scheduling used by Java,
 * and to implement some resource management policy on activity resources
 * (threads), Jonathan introduces abstractions to represent activities
 * (interface {@link Job Job}) and schedulers
 * (interface {@link Scheduler Scheduler}).
 * <p>
 * A Job is the abstraction of an activity in the system. When this activity
 * is sequential
 * (the usual case), it may be thought of as a thread. The role of a scheduler is
 * to create Jobs (a scheduler is a job factory), and to manage them according to
 * their properties (a priority, a deadline, ...). A scheduler has thus the
 * responsibility to associate a Job with some processing resource (which may be
 * a processor, or more simply a Java thread) when it is entitled to run. 
 */
public interface Scheduler {

   
   /** 
    * Returns a new job created by the scheduler.
    * @return a new job created by the scheduler.
    */
   public Job newJob();

   
   /** 
    * Returns the currently executing job (the job performing the call).
    * @return the currently executing job.
    */
   public Job getCurrent();

   
   /** 
    * Calling this method gives the opportunity to the scheduler to re-schedule
    * the currently executing jobs.
    */
   public void yield();   
   
   /** 
    * Blocks the calling job until the {@link #notify(Object) <tt>notify</tt>} or
    * {@link #notifyAll(Object) <tt>notifyAll</tt>} method is called providing the
    * same lock identifier.
    * @param lock the lock identifier.
    * @exception InterruptedException 
    */
   public void wait(Object lock) throws InterruptedException;

   /** 
    * Blocks the calling job until the {@link #notify(Object) <tt>notify</tt>} or
    * {@link #notifyAll(Object) <tt>notifyAll</tt>} method is called providing the
    * same lock identifier.
    * @param lock the lock identifier.
    * @param millis a time out in milliseconds.
    * @exception InterruptedException 
    */
   public void wait(Object lock,long millis) throws InterruptedException;

   /** 
    * Unblocks a job {@link #wait(Object) waiting} on the lock.  
    * @param lock the lock identifier.
    */
   public void notify(Object lock);

   
   /** 
    * Unblocks all jobs {@link #wait(Object) waiting} on the lock.  
    * @param lock the lock identifier.
    */
   public void notifyAll(Object lock);

   
   /** 
    * Causes the calling job to be removed from the set of jobs managed by the
    * target scheduler. It is necessary to call this method before every possibly
    * blocking method so that the scheduler can give a chance to run to another job.
    */
   public void escape();

   /** 
    * Causes a job {@link #escape() "escaped"} from the scheduler to be re-admitted
    * in the set of jobs managed by the target scheduler.
    */
   public void enter();
}


