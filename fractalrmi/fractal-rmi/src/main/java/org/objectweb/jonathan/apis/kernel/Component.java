/***
v * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 2001 Kelua SA
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 *
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 * 
 */

package org.objectweb.jonathan.apis.kernel;

/** 
 * The Component interface represents a component, with a value and a type. 
 */
public interface Component {
   
   /** 
    * Returns the element in the target component identified by <code>name</code>, 
    * null if no element exists under name <code>name</code>.
    *
    * @param name the name of the element to be retrieved;
    * @return the corresponding element, if any; null otherwise.
    */
   Element getElement(Name name);   

   /** 
    * Returns the component contained in the element registered in the target 
    * component under the name <code>name</code>.
    *
    * A call to <code>getComponent(name)</code> is equivalent to
    * <code>{@link #getElement(Name) getElement(name)}.{@link Element#getComponent()
    * getComponent()}</code> if the provided name actually 
    * corresponds to an element. If it is not the case, null is returned.
    * 
    * @param name the name of the element whose component is seeked; 
    * @return the corresponding component, or null.
    */
   Component getComponent(Name name);

   /** 
    * Returns the value of the element registered in the target context under 
    * the name <code>name</code>.
    *
    * A call to <code>getValue(name)</code> is equivalent to
    * <code>{@link #getElement(Name) getElement(name)}.{@link Element#getValue()
    * getValue()}</code> if the provided name actually 
    * corresponds to an element. If it is not the case, 
    * {@link Context#NO_VALUE <code>NO_VALUE</code>} is returned.
    * 
    * @param name the name of the element whose value is seeked; 
    * @return the corresponding value, or {@link Context#NO_VALUE <code>NO_VALUE</code>}.
    */
   Object getValue(Name name);

   /** 
    * Returns a distinguished element referencing the target component, if any, 
    * null otherwise. 
    * Implementations are not required to keep information about a given containing 
    * element. It is always legal to return null.
    * <p>
    * @return an element containing the target component, if any, null otherwise.
    */
   Element getReference();

   /** 
    * Sets an element containing the target component. The provided element should
    * already be initialized as a container of the component. The returned element
    * is the former container, if any. This method MUST be called when a component
    * is referred to by an element.
    *
    * @param element the new element referencing the target component.
    * @return the former element referencing the target component, if any, null 
    *         otherwise.
    */
   Element setReference(Element element);

   /** 
    * Returns the type of the target component, as a Class.
    * @return the type of the target component, as a Class.
    */
   Class  getType();

   /** 
    * Returns the value of the target component, if its type is an object reference
    * type.
    * <p>
    * If the target component is of an integral type, 
    * {@link Context#NO_VALUE NO_VALUE}} is returned.
    *
    * @return the value of the target component.
    */
   Object getValue();

   /** 
    * Returns the value of the target component, if its class is an integer class.
    * <p>
    * If the target component has an object reference type, Integer.MAX_VALUE is 
    * returned.
    *
    * @return the value of the target component.
    */
   int getIntValue();

   /*
    * Returns the value of a wrapper of the target component, if the component
    * has factory alternatives.
    * @return the value of factory.
    * @throws JonathanException if the component has not factory alternatives.
    */
   Object getFactoryValue() throws JonathanException;


   /** 
    * "Forks" a copy of the target component. The returned component is a
    * copy of the target component, sharing the same reference, but *NOT*
    * referred to by this reference. 
    * <p>
    * For basic components, this operation simply duplicates the component
    * structure, but the value is still shared.
    * <p> 
    * In the configuration framework, components contain a specification of
    * their value (in the form of an assemblage, atom, alias... description). 
    * The fork() method is used to duplicate the specification, but not the
    * value. It may thus be used to obtain several instances of a component, 
    * using getValue() on the returned component.
    * <p> 
    * When the target component is a context, the returned component is
    * a new context, roughly containing the forked versions of the target
    * context's components. 
    * <p>
    * If the target context contains an alias, and if the target T of this alias
    * is in the scope of the context, then the target of the forked alias is a
    * forked instance of T. But if the target is outside the scope of the
    * context, then the target of the forked alias is T itself. The context
    * thus defines a set of components that will be actually duplicated, all
    * links outside the context being preserved.
    * <p>
    * The same rule applies for assemblages, and other "container"
    * components. For instance, consider an assemblage 'A' made of factory 'F' and
    * configuration 'C', 'C' containing an atom 'At', an alias 'In' to At, and
    * an alias 'Out' to a component 'K' outside 'A'. Then the forked version of 'A'
    * is a new assemblage made of a copy of 'F' and a new configuration
    * containing a copy 'At2' of 'At', an alias 'In2' to 'At2', and an alias
    * 'Out2' to 'K'. 
    * <p>
    * Aliases are treated specifically. Calling the fork() method on an alias
    * is like calling the same method on its target.
    * <p>
    * @return a forked instance of the target component.
    */
   Component fork();

}
