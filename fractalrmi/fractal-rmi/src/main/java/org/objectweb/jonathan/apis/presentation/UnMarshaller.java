/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.presentation;

import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.kernel.Context;
import java.io.InputStream;

/**
 * UnMarshaller is the type of basic types unmarshallers. It should be used
 * by protocols.
 */
public interface UnMarshaller {

   /**
    * Reads a byte.
    *
    * @return a byte.
    * @exception JonathanException if the format of the data is incompatible
    *                               with the request.
    */   
   byte readByte()
      throws JonathanException;

   /**
    * Reads a boolean.
    *
    * @return a boolean.
    * @exception JonathanException if the format of the data is incompatible
    *                               with the request.
    */   
   boolean readBoolean()
      throws JonathanException;

   /**
    * Reads a 8 bits char.
    *
    * @return a char.
    * @exception JonathanException if the format of the data is incompatible
    *                               with the request.
    */   
   char readChar8()
      throws JonathanException;

   /**
    * Reads a 16 bits char.
    *
    * @return a char.
    * @exception JonathanException if the format of the data is incompatible
    *                               with the request.
    */   
   char readChar16()
      throws JonathanException;

   /**
    * Reads a short.
    *
    * @return a short.
    * @exception JonathanException if the format of the data is incompatible
    *                               with the request.
    */   
   short readShort()
      throws JonathanException;

   /**
    * Reads an int.
    *
    * @return an int.
    * @exception JonathanException if the format of the data is incompatible
    *                               with the request.
    */   
   int readInt()
      throws JonathanException;

   /**
    * Reads a long.
    *
    * @return a long.
    * @exception JonathanException if the format of the data is incompatible
    *                               with the request.
    */   
   long readLong()
      throws JonathanException;

   /**
    * Reads a float.
    *
    * @return a float.
    * @exception JonathanException if the format of the data is incompatible
    *                               with the request.
    */   
   float readFloat()
      throws JonathanException;

   /**
    * Reads a double.
    *
    * @return a double.
    * @exception JonathanException if the format of the data is incompatible
    *                               with the request.
    */   
   double readDouble()
      throws JonathanException;

   /**
    * Reads a string composed of 8 bits chars.
    *
    * @return a string.
    * @exception JonathanException if the format of the data is incompatible
    *                               with the request.
    */   
   String readString8()
      throws JonathanException;
   
   /**
    * Reads a string composed of 16 bits chars.
    *
    * @return a string.
    * @exception JonathanException if the format of the data is incompatible
    *                               with the request.
    */   
   String readString16()
      throws JonathanException;


   /** 
    * Reads a reference to an object.
    *
    * @return a reference to an object. 
    * @exception JonathanException if the format of the data is incompatible
    *                               with the request.
    */
   Object readReference() throws JonathanException;
   
   /** 
    * Reads a value
    *
    * @return an object representing the read value
    * @exception JonathanException if the format of the data is incompatible
    *                               with the request.
    */
   Object readValue() throws JonathanException;
   
   /** 
    * Reads an array of bytes.
    *
    * @param array  a byte array (of size >= offset + len)
    * @param offset the position (in array) of the first byte to write 
    * @param len    the total number of bytes to read;
    * @exception JonathanException if the format of the data is incompatible
    *                              with the request.
    */
   void readByteArray(byte[] array, int offset, int len)
      throws JonathanException;

   /** 
    * Returns an input stream to read data from the unmarshaller.
    *
    * Closing the returned input stream has the same effect as closing the actual 
    * unmarshaller.
    *
    * @return an input stream to read from the unmarshaller.
    */
   InputStream inputStream();


   /**
    * Returns true if this unmarshaller is little-endian, false otherwise.
    *
    * @return true if this unmarshaller is little-endian, false otherwise.
    */
   boolean isLittleEndian();


   /** 
    * Sets the byte order (returned by {@link #isLittleEndian() isLittleEndian}) of 
    * the target unmarshaller
    *
    * @param little_endian the new byte order.
    */
   void setByteOrder(boolean little_endian);


   /** 
    * Returns the number of bytes read since the beginning.
    *
    * @return the number of bytes read since the beginning.
    */
   int bytesRead();

   /** 
    * Sets the number of bytes readable from the unmarshaller.
    * <p>
    * Once this method has been called, it won't be possible to read more than the 
    * <code>size</code> specified bytes from this unmarshaller. Knowing the exact 
    * number of readable bytes lets the unmarshaller free the resources (such as 
    * a chunk provider) that won't be used. This method may block until the 
    * expected number of bytes is readable.
    *
    * @param size the expected number of readable bytes.
    * @exception JonathanException if something goes wrong.
    */
   void setSize(int size) throws JonathanException;

   /** 
    * Closes the target unmarshaller, releasing all underlying resources
    * (including a possible chunk provider).
    */
   void close();

   /** 
    * Returns a {@link Context Context} associated with this unmarshaller.
    * @return a {@link Context Context} associated with this unmarshaller.
    */
   Context getContext();
}



