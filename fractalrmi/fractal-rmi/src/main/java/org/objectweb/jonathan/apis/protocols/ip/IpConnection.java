/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0 
 * 
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 */


package org.objectweb.jonathan.apis.protocols.ip;

import org.objectweb.jonathan.apis.resources.Chunk;
import java.io.IOException;

/** 
 * A IpConnection represents a communication resource. It encapsulates a socket. 
 * A connection may always be related to a given session, i.e., a communication 
 * channel.
 */
public interface IpConnection {
   
   /** 
    * Returns the session attached to this connection.
    * @return the session attached to this connection.
    */
   IpSession getSession();

   /** 
    * Attaches a new session to this connection.
    * @param session the session to be attached to the target connection.
    */
   void setSession(IpSession session);

   void receive(Chunk chunk,int sz) throws IOException;

   int available() throws IOException;
      
   void emit(Chunk chunk) throws IOException;

   /** 
    * Returns the port number of the underlying socket.
    * @return the port number of the underlying socket.
    */
   int getPort();

   /** 
    * Returns the host name of the underlying socket.
    * @return the host name of the underlying socket.
    */
   String getHostName();

   /** 
    * Releases this connection. This is to indicate to the connection manager that 
    * this connection is no longer used.  
    */
   void release();

   /** 
    * Deletes this connection, removing it from the connection manager, and
    * closing the socket. This method should not be used a a socket user unless 
    * a problem occurs on the connection, like an exception when trying to read 
    * or to write data.
    */
   void delete();
}
