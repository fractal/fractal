/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.binding;

import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.kernel.Context;
import org.objectweb.jonathan.apis.presentation.Marshaller;

/** 
 * An identifier uniquely identifies an applicative object interface in a given
 * {@link NamingContext naming context}. It is a specific kind of
 * {@link org.objectweb.jonathan.model.name name}.
 */
public interface Identifier {
   /** 
    * Returns the naming context associated with the target identifier.
    *
    * An identifier is always associated the naming context that created it.
    *
    * @return the naming context associated with the target identifier.
    */
   public NamingContext getContext();   

   /** 
    * The bind operation returns an object giving access to the object interface 
    * referenced by the target identifier.
    * <p>
    * The returned object may be a direct reference to the object interface that
    * was used to create the identifier (at export-time), or it may be a surrogate
    * (a proxy) for that object interface.
    * <p>
    * If this operation is implemented, it means that the related naming context
    * is not only a
    * {@link org.objectweb.jonathan.model.naming_context naming context} but
    * also a  {@link org.objectweb.jonathan.model.binder binder}.
    * <p>
    * This operation may in particular raise two types of exceptions:
    * <ul>
    * <li> {@link ForwardException ForwardExceptions} that indicate that the
    *      "real" target object interface should be bound using the
    *      {@link ForwardException#reference reference} 
    *      contained in the exception. Note that in this case, calling the
    *      {@link #resolve() resolve} method should return the same reference.
    *      This facility may be used to handle mobility.
    * <li> {@link BindException BindExceptions} that indicate that an error
    *      occurred in the binding process;
    * </ul>
    * <p>
    * All parameters are optional (they may be null).
    * <ul>
    * <li> The <tt>ref</tt> parameter
    *      is an array of identifiers, to which the target identifier should belong:
    *      Object interfaces may be identified by several identifiers, and it
    *      is useful to maintain this list of identifiers, even if only one
    *      identifier is used to actually give access to the designated object
    *      interface. This parameter has been added as a convenience to achieve
    *      this, but it may not be meaningfull in all cases.
    * <li> The <tt>hints</tt> parameter contains information that may be required
    *      by naming contexts to build the returned object. Here again, it may not
    *      be meaningfull in all cases.
    * </ul>
    *
    * @param ref   a set of identifiers of the seeked object interface;
    * @param hints a context containing extra information that may be useful;
    * @return      a reference to the seeked interface.
    * @exception ForwardException if the target has moved;
    * @exception BindException if an error occurs in the binding process;
    * @exception JonathanException if something else goes wrong.
    * @see org.objectweb.jonathan.model.binder#bind(org.objectweb.jonathan.model.name)
    */
   public Object bind(Identifier[] ref,Context hints)
      throws ForwardException, BindException, JonathanException;

   
   /** 
    * Unexporting an identifier means that the target identifier no longer
    * designates the object interface it was created for (by some <tt>export</tt>
    * method). An identifier must no longer be used after it has been "unexported".
    */
   public void unexport();
   
   /** 
    * Tests the validity of the target identifier.
    * A <tt>false</tt> return means that a call to either
    * {@link #bind(Identifier[],Context) bind} or {@link #resolve() resolve} will
    * fail. If so, the target identifier must no longer be used.
    *
    * @return false if calls to either {@link #bind(Identifier[],Context) bind} or 
    * {@link #resolve() resolve} certainly fail.
    */
   public boolean isValid();
   
   /**
    * Returns the next name in the referencing chain.
    * <p>
    * If the naming context that has built the target identifier is an intermediate
    * naming context, this method should return the next "name" in the referencing
    * chain, or null if no such name can be found or created.
    * <p>
    * Such a name may be of type  {@link Identifier Identifier},
    * {@link Reference Reference}, or of any other type, provided that it really
    * designates the right interface, and that its type is clear from the context.
    * 
    * @return the next name in the referencing chain.
    * @see org.objectweb.jonathan.model.naming_context#resolve(org.objectweb.jonathan.model.name)
    */
   public Object resolve();

   /**
    * Encodes the target identifier in an array of bytes.
    * <p>
    * Since identifiers are likely to be transmitted on the net, they may have to be
    * encoded and decoded. The corresponding
    * {@link NamingContext#decode(byte[],int,int) decoding} method is borne
    * by the {@link NamingContext NamingContext} interface.
    *
    * @return an array of bytes encoding the target identifier. 
    * @exception JonathanException if something goes wrong.
    */
   public byte[] encode() throws JonathanException;   


   /**
    * Encodes the target identifier in a marshaller.
    *
    * @param m the marshaller to be used for encoding.
    * @exception JonathanException if something goes wrong.
    */
   public void encode(Marshaller m) throws JonathanException;   

}


