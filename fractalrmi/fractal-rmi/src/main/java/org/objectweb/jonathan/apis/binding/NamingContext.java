/***
 * Jonathan: an Open Distributed Processing Environment 
 * Copyright (C) 1999 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Release: 2.0
 *
 * Contact: jonathan@objectweb.org
 *
 * Author: Bruno Dumant
 * 
 */


package org.objectweb.jonathan.apis.binding;

import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.kernel.Context;
import org.objectweb.jonathan.apis.presentation.UnMarshaller;

/**
 * <code>NamingContext</code> is a specific sort of
 * {@link org.objectweb.jonathan.model.naming_context naming context} used to 
 * create and manage names of type {@link Identifier Identifier}.
 * <p>
 * There is no generic
 * {@link org.objectweb.jonathan.model.naming_context#export(org.objectweb.jonathan.model.name) export}
 * method defined on <code>NamingContext</code>, and the
 * {@link org.objectweb.jonathan.model.naming_context#resolve(org.objectweb.jonathan.model.name) resolve}
 * method is defined on {@link Identifier Identifier}.
 */
public interface NamingContext {
   
   /** 
    * Creates a new identifier for the object interface designated by the
    * <tt>itf</tt> parameter. Note that calling the
    * {@link Identifier#resolve() resolve} method on the returned identifier should
    * return tt>id</tt>.
    *
    * @param itf   an interface reference
    * @param hints additional information;
    * @return      an identifier managed by the target naming context.
    * @exception ExportException if an error occurs in the export process;
    * @exception JonathanException if something else goes wrong.
    * @see org.objectweb.jonathan.model.naming_context#export(org.objectweb.jonathan.model.name)
    */
   Identifier export(Object itf,Context hints) 
      throws ExportException, JonathanException ;

   /** 
    * Decodes an identifier from a buffer portion.
    * <p>
    * Since identifiers are likely to be transmitted on the net, they may have to be
    * encoded and decoded. The {@link Identifier#encode() encoding} method is borne
    * by the {@link Identifier Identifier} interface, but the decoding methods must
    * be borne by each naming context. This method creates an identifier (associated
    * with the target naming context), from the <code>length</code> bytes of
    * <code>data</code> starting at offset <code>offset</code>.
    *
    * @param data   the byte array to read the encoded identifier from;
    * @param offset offset of the first byte of the encoding;
    * @param length length of the encoding;
    * @return a decoded identifier;
    * @exception JonathanException if something goes wrong.
    */
   Identifier decode(byte[] data,int offset,int length)
      throws JonathanException;

   /** 
    * Decodes an identifier from the provided unmarshaller.
    * 
    * @param u an unmarhaller;
    * @return  an identifier managed by the target naming context;
    * @exception JonathanException if something goes wrong. 
    */
   Identifier decode(UnMarshaller u) throws JonathanException;
}



