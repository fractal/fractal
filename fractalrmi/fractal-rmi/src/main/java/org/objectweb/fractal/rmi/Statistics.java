/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Philippe Merle
 *
 * Contributor:
 *
 * $Id$
 */

package org.objectweb.fractal.rmi;

import java.io.PrintStream;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.Map;

/**
 * This class maintains Fractal RMI statistics:
 * <ul>
 * <li> the number of created stub and skeleton instances, and
 * <li> the number of sent and received remote method calls.
 * </ul>
 *
 * By default, statistics is activated when the "fractal.rmi.statistics" system property is equals to "true".
 *
 * @author <a href="Philippe.Merle@inria.fr">Philippe Merle</a>
 */

public class Statistics
{
  // --------------------------------------------------------------------------
  // Internal class state.
  // --------------------------------------------------------------------------

  /**
   * Flag for statistics activation.
   */

  private static boolean isActivated = Boolean.getBoolean("fractal.rmi.statistics");

  /**
   * Statistics of Fractal RMI stubs and skeletons for an interface name.
   */

  private static Map instances = new TreeMap();

  /**
   * This class maintains statistics of created stub and skeletons instances for a given interface.
   */

  private static class Instance
  {
    /**
     * The interface name.
     */

    protected String interfaceName;

    /**
     * The number of created stub instances.
     */

    protected int stubs;

    /**
     * The number of created skeleton instances.
     */

    protected int skeletons;

    /**
     * Constructs a new {@link Instance}.
     *
     * @param interfaceName The interface name.
     */

    protected Instance (final String interfaceName) {
      this.interfaceName = interfaceName;
      this.stubs = 0;
      this.skeletons = 0;
    }
  }

  /**
   * Statistics of Fractal RMI remote method calls.
   */

  private static Map calls = new TreeMap();

  /**
   * This class maintains statistics of sent and received remote method calls.
   */
  private static class Call
  {
    /**
     * The Fractal RMI method.
     */

    protected String method;

    /**
     * The number of sent calls to the Fractal RMI method.
     */

    protected int sent;

    /**
     * The number of received calls to the Fractal RMI method.
     */

    protected int received;

    /**
     * Constructs a new {@link Call}.
     *
     * @param method The Fractal RMI method.
     */

    protected Call (final String method) {
      this.method = method;
      this.sent = 0;
      this.received = 0;
    }
  }

  // --------------------------------------------------------------------------
  // Internal class methods.
  // --------------------------------------------------------------------------

  /**
   * Get instance statistics for a given interface.
   *
   * @param interfaceName The interface name.
   */

  private static Instance getInstance (final String interfaceName)
  {
    Instance stat = null;
    synchronized(instances) {
      stat = (Instance)instances.get(interfaceName);
      // If no statistics for the given interface name then create it.
      if(stat == null) {
        stat = new Instance(interfaceName);
        instances.put(interfaceName, stat);
      }
    }
    return stat;
  }

  /**
   * Get call statistics for a given Fractal RMI method.
   *
   * @param method The Fractal RMI method.
   */

  private static Call getCall (final String method)
  {
    Call stat = null;
    synchronized(calls) {
      stat = (Call)calls.get(method);
      // If no statistics for the given method then create it.
      if(stat == null) {
        stat = new Call(method);
        calls.put(method, stat);
      }
    }
    return stat;
  }

  private static String format (final int value)
  {
    StringBuffer sb = new StringBuffer();
    sb.append(value);
    for(int i=0, m=8-sb.length(); i<m; i++) {
      sb.insert(0, ' ');
    }
    return sb.toString();
  }

  private static String format (final String value, int len)
  {
    StringBuffer sb = new StringBuffer();
    sb.append(value);
    for(int i=0, m=len-value.length(); i<m; i++) {
      sb.append(' ');
    }
    return sb.toString();
  }

  // --------------------------------------------------------------------------
  // Public class methods.
  // --------------------------------------------------------------------------

  /**
   * Is Fractal RMI remote call statistics activated?
   *
   * @return True is activated else false.
   */
 
  public static boolean isActivated ()
  {
    return isActivated;
  }

  /**
   * Set Fractal RMI remote call statistics activation.
   *
   * @param activated If true then statistics is activated.
   */

  public static void setActivated (final boolean activated)
  {
    isActivated = activated;
  }

  /**
   * Reinitialize Fractal RMI statistics.
   */

  public static void reinitialize ()
  {
    instances = new TreeMap();
    calls = new TreeMap();
  }

  /**
   * Log a new Fractal RMI stub.
   *
   * @param interfaceName The interface name of the new stub.
   */

  public static void newStub (final String interfaceName)
  {
    if(isActivated) {
      Instance stat = getInstance(interfaceName);
      synchronized(stat) {
        stat.stubs++;
      }
    }
  }

  /**
   * Log a new Fractal RMI skeleton.
   *
   * @param interfaceName The interface name of the new skeleton.
   */

  public static void newSkeleton (final String interfaceName)
  {
    if(isActivated) {
      Instance stat = getInstance(interfaceName);
      synchronized(stat) {
        stat.skeletons++;
      }
    }
  }

  /**
   * Log a sent Fractal RMI method call.
   *
   * @param method The Fractal RMI method to log.
   */

  public static void sendCall (final String method)
  {
    if(isActivated) {
      Call stat = getCall(method);
      synchronized(stat) {
        stat.sent++;
      }
    }
  }

  /**
   * Log a received Fractal RMI method call.
   *
  * @param method The Fractal RMI method to log.
   */

  public static void receiveCall (final String method)
  {
    if(isActivated) {
      Call stat = getCall(method);
      synchronized(stat) {
        stat.received++;
      }
    }
  }

  /**
   * Generate a statistics report.
   *
   * @param out The {@link PrintStream} instance where the report is generated.
   */

  public static void report (final PrintStream out)
  {
    if(!isActivated) {
      return;
    }

    out.println("+----------------------------------------------------------------------------------+---------------------+");
    out.println("|                                                                                  |     Fractal RMI     |");    
    out.println("| Java Interfaces                                                                  |  Stubs   |  Skels   |");
    out.println("+----------------------------------------------------------------------------------+----------+----------+");
    int totalStubs = 0;
    int totalSkeletons = 0;
    for(Iterator iter=instances.values().iterator(); iter.hasNext(); ) {
      Instance stat = (Instance)iter.next();
      totalStubs += stat.stubs;
      totalSkeletons += stat.skeletons;
      out.println("| " + format(stat.interfaceName, 80) + " | " + format(stat.stubs) + " | " + format(stat.skeletons) + " |");
    }
    out.println("+----------------------------------------------------------------------------------+----------+----------+");
    out.println("| " + format("Total", 80) + " | " + format(totalStubs) + " | " + format(totalSkeletons) + " |");
    out.println("+----------------------------------------------------------------------------------+----------+----------+");

    out.println("+----------------------------------------------------------------------------------+---------------------+");
    out.println("|                                                                                  |  Fractal RMI calls  |");
    out.println("| Java Class and Method Names                                                      |   Sent   | Received |");
    out.println("+----------------------------------------------------------------------------------+----------+----------+");
    int totalSent = 0;
    int totalReceived = 0;
    for(Iterator iter=calls.values().iterator(); iter.hasNext(); ) {
      Call stat = (Call)iter.next();
      totalSent += stat.sent;
      totalReceived += stat.received;
      out.println("| " + format(stat.method, 80) + " | " + format(stat.sent) + " | " + format(stat.received) + " |");
    }
    out.println("+----------------------------------------------------------------------------------+----------+----------+");
    out.println("| " + format("Total", 80) + " | " + format(totalSent) + " | " + format(totalReceived) + " |");
    out.println("+----------------------------------------------------------------------------------+----------+----------+");
  }
}
