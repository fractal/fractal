/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Philippe Merle
 *
 * Contributor: Jeremy Dubus
 *
 * $Id$
 */

package org.objectweb.fractal.rmi;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.adl.Launcher;
import org.objectweb.fractal.rmi.registry.Registry;

/**
 * Main class for launching Fractal ADL definitions distributed via Fractal RMI.
 *
 * Usage: java ClientLauncher <definition> [virtual-node-name=URI]*
 *
 * Where <definition> is the Fractal ADL definition to launch.
 *
 * The rest of the command line arguments allows to set the mapping
 * between virtual node names and concrete Fractal RMI servers.
 * Concrete servers are identified by URI with the following syntax
 * '/[host]:[port]/server_name'. By default, host is the local one,
 * and port is the default one.
 *
 * @author Philippe Merle
 */

public class ClientLauncher {

  public static void main (final String[] args)
    throws Exception
  {
    String definition = args[0];
	System.out.println("ClientLauncher " + definition);

    // Creates a HTTP server to download classes.
    HttpServer httpServer = new HttpServer(true);

	// init default values
	Map context = new HashMap();
	context.put(Launcher.FACTORY_PROPERTY_NAME, FactoryFactory.DEFAULT_FACTORY);
	context.put(FactoryFactory.BACKEND_PROPERTY_NAME, FactoryFactory.FRACTAL_BACKEND);
	context.put(Launcher.RUNNABLE_INTERFACE_PROPERTY_NAME, Launcher.RUNNABLE_INTERFACE_DEFAULT_VALUE);
	context.put(Launcher.DAEMON_PROPERTY_NAME, Launcher.DAEMON_PROPERTY_DEFAULT_VALUE);

	for(int i=1; i<args.length; i++) {
	  String[] mapping = args[i].split("=");
      try {
		Object server = Registry.lookup(mapping[1]);
		if(server == null) {
	      System.err.println("Warning: Virtual-node '" + mapping[0] + "' is mapped to the local JVM as node '" + mapping[1] + "' was not found!");
		} else {
 	      System.out.println("Info: Virtual-node '" + mapping[0] + "' is mapped to node '" + mapping[1] + "'.");
          context.put(mapping[0], server);
		}
	  } catch(Exception exc) {
		System.err.println("Warning: Virtual-node '" + mapping[0] + "' is mapped to the local JVM as Registry.lookup(" + mapping[1] + ") failed due to " + exc);
	  }
	}

    Launcher launcher = new Launcher(context);
    launcher.compile(definition);

    Statistics.report(System.out);
  }
}
