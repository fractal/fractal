/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 *
 * adapted from Jonathan:
 *   org.objectweb.jonathan.libs.binding.moa.MinimalAdapter (author: B. Dumant)
 * with some comments copied from:
 *   org.objectweb.jonathan.apis.binding.NamingContext (author: B. Dumant)
 */

package org.objectweb.fractal.rmi;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.rmi.stub.SkeletonFactory;

import org.objectweb.jonathan.apis.binding.Identifier;
import org.objectweb.jonathan.apis.binding.NamingContext;
import org.objectweb.jonathan.apis.kernel.Context;
import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.presentation.Marshaller;
import org.objectweb.jonathan.apis.presentation.UnMarshaller;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.LoggerFactory;

import java.util.HashMap;

/**
 * Provides a very simple object adapter component. This naming context exports
 * an object in the following way:
 * <ul>
 * <li>if the object has already been exported, the corresponding {@link
 * Identifier} is returned directly.</li>
 * <li>otherwise a skeleton is created to give access to the interface
 * implemented by the object (the object must implement at least one interface.
 * If it implements more than one, the first one is selected).</li>
 * <li>a new identifier is created for this skeleton. This identifer is just
 * an integer value.</li>
 * <li>the skeleton is associated to this new identifier in an internal hash
 * table.</li>
 * <li>the identifier is returned to the caller.</li>
 * </ul>
 */

public class RmiAdapter
  implements NamingContext, Runnable, BindingController
{

  /**
   * The skeleton factory used to create skeletons for the exported objects.
   */

  protected SkeletonFactory skeletonFactory;

  /**
   * The optional logger factory used to get a logger for this component.
   */

  protected LoggerFactory loggerFactory;

  /**
   * The logger used to log messages. May be <tt>null</tt>.
   */

  protected Logger logger;

  /**
   * A map associating skeletons to Integer objects, and Id to already exported
   * objects.
   */

  private HashMap map;

  /**
   * A counter used to generate unique object identifiers.
   */

  private int counter;

  /**
   * The daemon thread used to keep the JVM process alive.
   */

  private Thread waiter;

  /**
   * Constructs a new {@link RmiAdapter}.
   */

  public RmiAdapter () {
    map = new HashMap();
  }

  // --------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // --------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] { "skeleton-factory", "logger-factory" };
  }

  public Object lookupFc (final String clientItfName) {
    if (clientItfName.equals("skeleton-factory")) {
      return skeletonFactory;
    } else if (clientItfName.equals("logger-factory")) {
      return loggerFactory;
    }
    return null;
  }

  public void bindFc (final String clientItfName, final Object serverItf) {
    if (clientItfName.equals("skeleton-factory")) {
      skeletonFactory = (SkeletonFactory)serverItf;
    } else if (clientItfName.equals("logger-factory")) {
      loggerFactory = (LoggerFactory)serverItf;
      logger = loggerFactory.getLogger(getClass().getName());
    }
  }

  public void unbindFc (final String clientItfName) {
    if (clientItfName.equals("skeleton-factory")) {
      skeletonFactory = null;
    } else if (clientItfName.equals("logger-factory")) {
      loggerFactory = null;
      logger = null;
    }
  }

  // --------------------------------------------------------------------------
  // Implementation of the NamingContext interface
  // --------------------------------------------------------------------------

  /**
   * Creates a new identifier for the object interface designated by the
   * <tt>obj</tt> parameter. Note that calling the {@link Identifier#resolve()
   * resolve} method on the returned identifier should return <tt>id</tt>.
   *
   * @param obj an object.
   * @param hints additional information.
   * @return an identifier managed by the target naming context.
   * @throws JonathanException if something goes wrong.
   */

  public Identifier export (final Object obj, final Context hints)
    throws JonathanException
  {
    synchronized (this) {
      // checks if the object has already been exported
      Id id = (Id)map.get(obj);
      if (id == null) {
        // if not, exports it
        if (waiter == null) {
          waiter = new Thread(this);
          waiter.start();
        }
        Integer key = null;
        if (hints != null) {
          key = (Integer)hints.getValue("key", (char)0);
          if (key != null && key.intValue() >= 0) {
            throw new JonathanException(
              "Cannot export objects with positive user specified keys");
          }
          if (key != null && map.get(key) != null) {
            throw new JonathanException("Key " + key + " already used");
          }
        }
        if (key == null) {
          key = new Integer(counter++);
        }
        id = new Id(key);
        Object skel = skeletonFactory.newSkeleton(obj);
        map.put(key, skel);
        map.put(obj, id);
        if (logger != null && logger.isLoggable(BasicLevel.INFO)) {
          logger.log(
            BasicLevel.INFO, "Object " + obj + " exported with key " + key);
        }
      }
      return id;
    }
  }

  /**
   * Decodes an identifier from a buffer portion. Since identifiers are likely
   * to be transmitted on the net, they may have to be encoded and decoded. The
   * {@link Identifier#encode() encoding} method is borne by the {@link
   * Identifier} interface, but the decoding methods must be borne by
   * each naming context. This method creates an identifier (associated
   * with the target naming context), from the <code>length</code> bytes of
   * <code>data</code> starting at offset <code>offset</code>.
   *
   * @param data the byte array to read the encoded identifier from.
   * @param offset offset of the first byte of the encoding.
   * @param length length of the encoding.
   * @return a decoded identifier.
   * @throws JonathanException if something goes wrong.
   */

  public Identifier decode (
    final byte[] data,
    final int offset,
    final int length) throws JonathanException
  {
    int key =
      ((data[offset] & 0xFF) << 24) +
      ((data[offset + 1] & 0xFF) << 16) +
      ((data[offset + 2] & 0xFF) << 8) +
      (data[offset + 3] & 0xFF);
    return new Id(new Integer(key));
  }

  /**
   * Decodes an identifier from the provided unmarshaller.
   *
   * @param u an unmarhaller;
   * @return an identifier managed by the target naming context;
   * @throws JonathanException if something goes wrong.
   */

  public Identifier decode (final UnMarshaller u) throws JonathanException {
    return new Id(new Integer(u.readInt()));
  }

  // --------------------------------------------------------------------------
  // Implementation of the Runnable interface
  // --------------------------------------------------------------------------

  /**
   * Blocks the caller thread. This method is used to create a "daemon" thread
   * when the {@link #export export} method is called for the time, in order to
   * keep the JVM process alive, waiting for incoming remote method calls.
   */

  public void run () {
    Object o = new Object();
    synchronized (o) {
      try {
        o.wait();
      } catch (Exception e) {
      }
    }
  }

  // --------------------------------------------------------------------------
  // Utility class
  // --------------------------------------------------------------------------

  class Id implements Identifier {

    Integer key;

    public Id (final Integer key) {
      this.key = key;
    }

    public NamingContext getContext () {
      return RmiAdapter.this;
    }

    public Object bind (final Identifier[] ref, final Context hints) {
      return map.get(key);
    }

    public void unexport () {
      map.remove(key);
    }

    public boolean isValid () {
      return map.containsKey(key);
    }

    public Object resolve () {
      Object o = map.get(key);
      if (logger != null && logger.isLoggable(BasicLevel.INFO)) {
        logger.log(BasicLevel.INFO, "Key " + key + " resolved to " + o);
      }
      return o;
    }

    public byte[] encode () {
      int key = this.key.intValue();
      return new byte[] {
        (byte)((key >>> 24) & 0xFF),
        (byte)((key >>> 16) & 0xFF),
        (byte)((key >>> 8) & 0xFF),
        (byte)(key & 0xFF)
      };
    }

    public void encode (final Marshaller m) throws JonathanException {
      m.writeInt(key.intValue());
    }

    public boolean equals (final Object o) {
      if (o instanceof Id) {
        return key.intValue() == ((Id)o).key.intValue();
      }
      return false;
    }

    public int hashCode () {
      return key.intValue();
    }

    public String toString () {
      return "Id[" + key.intValue() + "]";
    }
  }
}
