/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2003-2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributors: Bruno Dillenseger, Philippe Merle
 *
 * with some comments copied from Jonathan:
 *   org.objectweb.jonathan.apis.stub_factories.StubFactory (author: B. Dumant)
 *
 * Bruno Dillenseger added main method to generate stubs and skeletons (like a compiler).
 *
 * Philippe Merle added generation of stubs/skeletons for interfaces provided by
 * standard Java runtime librairies (e.g., java.lang.Runnable).
 *
 * Valerio Schiavoni migrated to ASM 3.0
 *
 * Philippe Merle added statistics.
 * 
 * Valerio Schiavoni improved the generation of stub/skeletons by checking the existence
 * of destination directories
 *
 * $Id$
 */

package org.objectweb.fractal.rmi.stub;

import java.io.File;
import java.lang.reflect.Method;
import java.util.HashMap;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.rmi.Statistics;
import org.objectweb.jonathan.apis.binding.ExportException;
import org.objectweb.jonathan.apis.binding.Identifier;
import org.objectweb.jonathan.apis.kernel.Context;
import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.presentation.MarshallerFactory;
import org.objectweb.jonathan.apis.protocols.RequestSession;
import org.objectweb.jonathan.apis.protocols.SessionIdentifier;
import org.objectweb.jonathan.apis.stub_factories.StubFactory;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.LoggerFactory;

/**
 * A stub and skeleton factory based on <a
 * href="http://www.objectweb.org/asm">ASM</a>. This factory generates the stub
 * and skeleton classes dynamically, when they are needed. It is therefore not
 * necessary for the user to statically generate these classes with a tool such
 * as <tt>rmic</tt> or <tt>idlc</tt>.
 * <br>
 * The stubs and skeletons created by this factory marshall and unmarshall the
 * method invocations by using the following format: a method invocation message
 * contains a four bytes method index, which uniquely and unambiguously
 * identifies a method among the methods provided by a Java interface, followed
 * by the method's arguments, marshalled in the order of their declaration in
 * the method's signature.
 * <p>
 * In order to be able to manually generate stubs and skeletons before execution
 * and avoid their generation at runtime (typically for performance reasons,
 * although no real testing has been performed), this class can be used as a
 * kind of a stub compiler through its main method, giving as arguments the Java
 * interfaces you want to compile stubs for. Note that this generation is
 * performed at byte code level only, i.e. it takes compiled interfaces as input
 * and generates compiled stubs and skeletons classes. In other words, the
 * interfaces (fully-qualified) names you specify must be compiled and reachable
 * in the classpath, and the result is a corresponding set of stub and skeleton
 * compiled classes (taking current directory as base directory).    
 */

public class RmiStubFactory
  implements Opcodes, StubFactory, SkeletonFactory, BindingController
{
  /**
   * This flag is set to true if generated stub classes must be written to files.
   */
  static private boolean writeStubs = false;

  /**
   * This flag is set to true if generated skeleton classes must be written to files.
   */
  static private boolean writeSkels = false;

  /**
   * FractalRMI stub and skeleton generator.
   * @param interfaces a list of interface fully qualified names. These interfaces
   * must be compiled already and reachable through the classpath.
   * Stubs and skeletons are generated as compiled classes taking current directory
   * as base directory.
   */
  static public void main(String[] interfaces)
    throws Exception
  {
    writeStubs = true;
    writeSkels = true;
    int argc = interfaces.length;
    Loader loader = new RmiStubFactory()
                    .getLoader(Thread.currentThread().getContextClassLoader());
    while (argc-- > 0) {
      loader.findClass(getSkelClassName(interfaces[argc]));
      loader.findClass(getStubClassName(interfaces[argc]));
    }
  }

  /**
   * A map associating {@link Loader loaders} to their parent class loader.
   */

  private final static HashMap LOADERS = new HashMap();

  /**
   * The marshaller factory to be used by the stubs created by this factory.
   */

  protected MarshallerFactory marshallerFactory;

  /**
   * The optional logger factory used to get a logger for this component.
   */

  protected LoggerFactory loggerFactory;

  /**
   * The logger used to log messages. May be <tt>null</tt>.
   */

  protected Logger logger;

  /**
   * Constructs a new {@link RmiStubFactory}.
   */

  public RmiStubFactory () {
  }

  // --------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // --------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] { "marshaller-factory", "logger-factory" };
  }

  public Object lookupFc (final String clientItfName) {
    if (clientItfName.equals("marshaller-factory")) {
      return marshallerFactory;
    } else if (clientItfName.equals("logger-factory")) {
      return loggerFactory;
    }
    return null;
  }

  public void bindFc (final String clientItfName, final Object serverItf) {
    if (clientItfName.equals("marshaller-factory")) {
      marshallerFactory = (MarshallerFactory)serverItf;
    } else if (clientItfName.equals("logger-factory")) {
      loggerFactory = (LoggerFactory)serverItf;
      logger = loggerFactory.getLogger(getClass().getName());
    }
  }

  public void unbindFc (final String clientItfName) {
    if (clientItfName.equals("marshaller-factory")) {
      marshallerFactory = null;
    } else if (clientItfName.equals("logger-factory")) {
      loggerFactory = null;
      logger = null;
    }
  }

  // --------------------------------------------------------------------------
  // Implementation of the StubFactory interface
  // --------------------------------------------------------------------------

  /**
   * Creates a new stub. A stub plays two roles:
   * <ul>
   * <li>It is the local representative of a (remote) object, and thus should
   * bear a set of identifiers for that remote object;</li>
   * <li>It is part of the binding between the client and the server, and is
   * thus related to a given protocol. The session identifier provided as an
   * argument is the manifestation of this relation. It can be used to
   * obtain a session allowing the stub to send data to the remote object.</li>
   * </ul>
   *
   * @param sessionId a session identifier, to be used to send marshalled
   *      data to the object represented by the stub;
   * @param ids the set of identifiers of the stub;
   * @param hints other data possibly used to create the stub. This method
   *      requires the fully qualified name of the Java interface to which the
   *      stub will gives access. This name must be associated to the
   *      "interface_type" key.
   * @return an instance of a sub class of the {@link Stub} class.
   * @throws JonathanException if something goes wrong.
   */

  public Object newStub (
    final SessionIdentifier sessionId,
    final Identifier[] ids,
    final Context hints) throws JonathanException
  {
    try {
      String implClassName = (String)hints.getValue("interface_type", (char)0);
      Class implClass =
        Thread.currentThread().getContextClassLoader().loadClass(implClassName);
      Class stubClass =
        getLoader(Thread.currentThread().getContextClassLoader()).loadClass(getStubClassName(implClass.getName()));
      if (ids.length != 1) {
        throw new JonathanException();
      }
      Stub stub = (Stub)stubClass.newInstance();
      stub.id = ids[0];
      stub.sessionId = sessionId;
      stub.marshallerFactory = marshallerFactory;
      if (logger != null && logger.isLoggable(BasicLevel.INFO)) {
        logger.log(BasicLevel.INFO, "Stub created for id " + ids[0]);
      }
      if(Statistics.isActivated()) {
        Statistics.newStub(implClassName);
      }
      return stub;
    } catch (Exception e) {
      throw new JonathanException(e);
    }
  }

  // --------------------------------------------------------------------------
  // Implementation of the StubFactory interface
  // --------------------------------------------------------------------------

  public RequestSession newSkeleton (final Object target)
    throws JonathanException
  {
    try {
      if (!(target instanceof Interface)) {
        throw new Exception("target object must implement Interface");
      }
      Class implClass = target.getClass();
      Class[] itfs = implClass.getInterfaces();
      if (itfs.length == 0) {
        throw new Exception("target object must implement an interface");
      }
      Class skelClass =
        getLoader(implClass.getClassLoader()).loadClass(getSkelClassName(itfs[0].getName()));
      Skeleton skel = (Skeleton)skelClass.newInstance();
      skel.target = target;
      if (logger != null && logger.isLoggable(BasicLevel.INFO)) {
        logger.log(BasicLevel.INFO, "Skeleton created for target " + target);
      }
      if(Statistics.isActivated()) {
        Statistics.newSkeleton(itfs[0].getName());
      }
      return skel;
    } catch (Exception e) {
      throw new ExportException(e);
    }
  }

  // --------------------------------------------------------------------------
  // Class loading
  // --------------------------------------------------------------------------

  /**
   * Returns the {@link Loader} whose parent class loader is equal to the
   * given one.
   *
   * @param parent the parent class loader.
   * @return the {@link Loader} whose parent class loader is equal to the
   *      given one.
   */

  private Loader getLoader (final ClassLoader parent) {
    Loader instance = (Loader)LOADERS.get(parent);
    if (instance == null) {
      instance = new Loader(parent);
      LOADERS.put(parent, instance);
    }
    return instance;
  }

  /**
   * A class loader that can generate stub and skeleton classes on the fly.
   */

  class Loader extends ClassLoader {

    /**
     * Constructs a {@link Loader} with the given parent class loader.
     *
     * @param parent the parent class loader.
     */

    public Loader (final ClassLoader parent) {
      super(parent);
    }

    /**
     * Finds the class whose name is given. If the given name is of the form
     * <i>prefix</i><tt>_Stub</tt> then this method calls the {@link
     * #generateStubClass generateStubClass} method with the <i>prefix</i> class
     * as parameter. If the given name is of the form
     * <i>prefix</i><tt>_Skel</tt> then this method calls the
     * {@link #generateSkeletonClass generateSkeletonClass} method with the
     * <i>prefix</i> class as parameter. In all other cases this method throws
     * an exception.
     *
     * @param name the name of the class to be found.
     * @return the specified class.
     * @throws ClassNotFoundException if the class cannot be found.
     */

    protected Class findClass (final String name)
      throws ClassNotFoundException
    {
      if (isStubClassName(name)) {
        Class itf = loadClass(resolveStubClassName(name));
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
        generateStubClass(cw, itf);
        byte[] b = cw.toByteArray();
        if (logger != null && logger.isLoggable(BasicLevel.INFO)) {
          logger.log(BasicLevel.INFO, "Stub class generated: " + name);
        }
        if (writeStubs) {
          try {
        	  java.io.OutputStream os;
        	  final String canonicalName = name.replace('.', '/');
        	  int i = canonicalName.lastIndexOf("/");
        	  File packageDir = new File(canonicalName.substring(0, i));
        	  /*
        	   * if the directory to store the stubs does not exist, try to create it
        	   */
        	  if(!packageDir.exists()) {
        		  if(!packageDir.mkdirs()) {
        			  if (logger != null && logger.isLoggable(BasicLevel.INFO)) {
        				  logger.log(BasicLevel.INFO, "Cannot create dirs "+packageDir.getAbsolutePath()+" to save stub class files");
        			  }
        		  }
        	  }
  			File clazzFile = new File(canonicalName + ".class");
              os = new java.io.FileOutputStream(clazzFile);
              os.write(b);
              os.close();
          } catch (Exception e) {
        	  e.printStackTrace();
          }
        }
        return defineClass(name, b, 0, b.length);
      } else if (isSkelClassName(name)) {
        Class itf = loadClass(resolveSkelClassName(name));
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
        generateSkeletonClass(cw, itf);
        byte[] b = cw.toByteArray();
        if (logger != null && logger.isLoggable(BasicLevel.INFO)) {
          logger.log(BasicLevel.INFO, "Skeleton class generated: " + name);
        }
        if (writeSkels) {
          try {
        	  java.io.OutputStream os;
        	  final String canonicalName = name.replace('.', '/');
        	  int i = canonicalName.lastIndexOf("/");
        	  File packageDir = new File(canonicalName.substring(0, i));
        	  /*
        	   * if the directory to store the skeletons does not exist, try to create it
        	   */
        	  if(!packageDir.exists()) {
        		  if(!packageDir.mkdirs()) {
        			  if (logger != null && logger.isLoggable(BasicLevel.INFO)) {
        				  logger.log(BasicLevel.INFO, "Cannot create dirs "+packageDir.getAbsolutePath()+" to save skeleton class files");
        			  }
        		  }
        	  }
        	  File clazzFile = new File(canonicalName + ".class");
        	  os = new java.io.FileOutputStream(clazzFile);
        	  os.write(b);
        	  os.close();
          } catch (Exception e) {
        	  e.printStackTrace();
          }
        }
        return defineClass(name, b, 0, b.length);
      } else {
        throw new ClassNotFoundException(name);
      }
    }
  }

  // --------------------------------------------------------------------------
  // Utility methods: class generation
  // --------------------------------------------------------------------------

  /**
   * Generates a stub class for the given Java interface. This method generates
   * a sub class of the {@link Stub} class that implements the given
   * interface, by using the given class visitor.
   *
   * @param cv the class visitor to be used to generate the stub class.
   * @param itf the Java interface that the stub class must implement.
   */

  protected void generateStubClass (final ClassVisitor cv, final Class itf) {
    int access = ACC_PUBLIC;
    String name = getStubClassName(itf.getName()).replace('.', '/');
    String superClass = Statistics.isActivated()
                      ? "org/objectweb/fractal/rmi/stub/StubStatistics"
                      : "org/objectweb/fractal/rmi/stub/Stub";
    String[] itfs = new String[] { Type.getInternalName(itf) };
    cv.visit(V1_1, access, name, null, superClass, itfs);
    generateConstructor(cv, superClass);
    Method[] meths = itf.getMethods();
    sort(meths);
    for (int i = 0; i < meths.length; ++i) {
      Method meth = meths[i];
      generateStubMethod(cv, name, meth, i);
    }
  }

  /**
   * Generates a skeleton class for the given Java interface. This method
   * generates a sub class of the {@link Skeleton} class whose {@link
   * Skeleton#target target} is an object implementing the given interface,
   * by using the given class visitor.
   *
   * @param cv the class visitor to be used to generate the stub class.
   * @param itf the Java interface implemented by the skeleton's target.
   */

  protected void generateSkeletonClass (final ClassVisitor cv, final Class itf) {
    int access = ACC_PUBLIC;
    String name = getSkelClassName(itf.getName()).replace('.', '/');
    String superClass = Statistics.isActivated()
                      ? "org/objectweb/fractal/rmi/stub/SkeletonStatistics"
                      : "org/objectweb/fractal/rmi/stub/Skeleton";
    cv.visit(V1_1, access, name, null, superClass, null);
    generateConstructor(cv, superClass);
    generateSkeletonMethod(cv, name, itf);
    cv.visitEnd();
  }

  /**
   * Generates an empty constructor for the given class.
   *
   * @param cv the class visitor to be used to generate the constructor.
   * @param superClass the internal name of the super class of the generated
   *      class. This name is used to generate a call to the super constructor.
   */

  protected void generateConstructor (
    final ClassVisitor cv,
    final String superClass)
  {
    MethodVisitor mv = cv.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
    // generates the bytecode corresponding to 'super(...);'
    mv.visitVarInsn(ALOAD, 0);
    mv.visitMethodInsn(INVOKESPECIAL, superClass, "<init>", "()V");
    mv.visitInsn(RETURN);
    mv.visitMaxs(1, 1);
    mv.visitEnd();
  }

  /**
   * Generates a stub method. A stub method is of the following form:
   * <p>
   * <pre>
   * public <i>T</i> m (<i>T0 arg0</i>, ...) throws <i>E0</i>, ... {
   *   try {
   *     Marshaller marshaller = request();
   *     ReplyInterface reply = prepareInvocation(marshaller);
   *     marshaller.writeInt(<i>methodIndex</i>);
   *     marshaller.write<i>XXX</i>(<i>arg0</i>);
   *     ...
   *     invoke(marshaller);
   *     UnMarshaller unmarshaller = reply.listen();
   *     <i>T</i> result = (<i>T</i>)unmarshaller.read<i>XXX</i>();
   *     unmarshaller.close();
   *     return result;
   *   } catch (Exception e) {
   *     e = handleException(e);
   *     if (e instanceof <i>E0</i>) throw (<i>E0</i>)e;
   *     ...
   *     if (e instanceof RuntimeException) throw (RuntimeException)e;
   *     throw new RemoteException("server side exception", e);
   *   }
   * }
   * </pre>
   *
   * @param cv the class visitor to be used to generate the stub method.
   * @param name the internal name of the generated stub class.
   * @param m the signature of the method that must be generated.
   * @param index the index of this method. This index will be marshalled by the
   *      generated stub method in each invocation message, to specify the
   *      method that must be called on the server side. This index is the index
   *      of the method in the {@link #sort sorted} array of all the
   *      methods implemented by the stub.
   */

  protected void generateStubMethod (
    final ClassVisitor cv,
    final String name,
    final Method m,
    final int index)
  {
    // generate the method header
    String mName = m.getName();
    String mDesc = Type.getMethodDescriptor(m);
    Class[] params = m.getParameterTypes();
    int locals = 1;
    for (int i = 0; i < params.length; ++i) {
      locals += getSize(params[i]);
    }
    Class result = m.getReturnType();
    Class[] exceptions = m.getExceptionTypes();
    String[] excepts = new String[exceptions.length];
    for (int i = 0; i < exceptions.length; ++i) {
      excepts[i] = Type.getInternalName(exceptions[i]);
    }
    MethodVisitor mv = cv.visitMethod(ACC_PUBLIC, mName, mDesc, null, excepts);

    // generate Statistics.sendCall(class.methodname)
    if(Statistics.isActivated()) {
      mv.visitLdcInsn(m.getDeclaringClass().getName() + '.' + mName);
      mv.visitMethodInsn(INVOKESTATIC, "org/objectweb/fractal/rmi/Statistics", "sendCall", "(Ljava/lang/String;)V");
    }

    Label beginL = new Label();
    Label endL = new Label();

    mv.visitLabel(beginL);
    // generate "Marshaller marshaller = request();"
    mv.visitVarInsn(ALOAD, 0);
    mv.visitMethodInsn(
      INVOKEVIRTUAL,
      name,
      "request",
      "()Lorg/objectweb/jonathan/apis/presentation/Marshaller;");
    mv.visitVarInsn(ASTORE, locals);

    // generate "ReplyInterface reply = prepareInvocation(marshaller);"
    mv.visitVarInsn(ALOAD, 0);
    mv.visitVarInsn(ALOAD, locals);
    mv.visitMethodInsn(
      INVOKEVIRTUAL,
      name,
      "prepareInvocation",
      "(Lorg/objectweb/jonathan/apis/presentation/Marshaller;)Lorg/objectweb/jonathan/apis/protocols/ReplyInterface;");
    mv.visitVarInsn(ASTORE, locals + 1);

    // generate "marshaller.writeInt(<index>);"
    mv.visitVarInsn(ALOAD, locals);
    mv.visitIntInsn(SIPUSH, index);
    mv.visitMethodInsn(
      INVOKEINTERFACE,
      "org/objectweb/jonathan/apis/presentation/Marshaller",
      "writeInt",
      "(I)V");

    // parameter marshalling
    int offset = 1;
    for (int i = 0; i < params.length; ++i) {
      mv.visitVarInsn(ALOAD, locals);
      if (params[i].isPrimitive()) {
        mv.visitVarInsn(ILOAD + getOpcodeOffset(params[i]), offset);
        mv.visitMethodInsn(
          INVOKEINTERFACE,
          "org/objectweb/jonathan/apis/presentation/Marshaller",
          "write" + getMarshallerMethod(params[i]),
          "(" + Type.getDescriptor(params[i]) + ")V");
      } else {
        if (isClassParameter(m, i)) {
          mv.visitVarInsn(ALOAD, 0);
          mv.visitVarInsn(ILOAD + getOpcodeOffset(params[i]), offset);
          mv.visitMethodInsn(
            INVOKEVIRTUAL,
            "org/objectweb/fractal/rmi/stub/Stub", 
            "replaceClassName", 
            "(Ljava/lang/Object;)Ljava/lang/Object;");
        } else {
          mv.visitVarInsn(ILOAD + getOpcodeOffset(params[i]), offset);
        }
        mv.visitMethodInsn(
          INVOKEINTERFACE,
          "org/objectweb/jonathan/apis/presentation/Marshaller",
          "writeValue",
          "(Ljava/lang/Object;)V");
      }
      offset += getSize(params[i]);
    }

    // generate "invoke(marshaller);"
    mv.visitVarInsn(ALOAD, 0);
    mv.visitVarInsn(ALOAD, locals);
    mv.visitMethodInsn(
      INVOKEVIRTUAL,
      name,
      "invoke",
      "(Lorg/objectweb/jonathan/apis/presentation/Marshaller;)V");

    // generate "UnMarshaller unmarshaller = reply.listen();"
    mv.visitVarInsn(ALOAD, locals + 1);
    mv.visitMethodInsn(
      INVOKEINTERFACE,
      "org/objectweb/jonathan/apis/protocols/ReplyInterface",
      "listen",
      "()Lorg/objectweb/jonathan/apis/presentation/UnMarshaller;");
    mv.visitVarInsn(ASTORE, locals + 2);

    // return value unmarshalling
    if (result != Void.TYPE) {
      if (result.isPrimitive()) {
        mv.visitVarInsn(ALOAD, locals + 2);
        mv.visitMethodInsn(
          INVOKEINTERFACE,
          "org/objectweb/jonathan/apis/presentation/UnMarshaller",
          "read" + getMarshallerMethod(result),
          "()" + Type.getDescriptor(result));
      } else {
        if (isClassParameter(m, -1)) {
          mv.visitVarInsn(ALOAD, 0);
          mv.visitVarInsn(ALOAD, locals + 2);
          mv.visitMethodInsn(
            INVOKEINTERFACE,
            "org/objectweb/jonathan/apis/presentation/UnMarshaller",
            "readValue",
            "()Ljava/lang/Object;");
          mv.visitMethodInsn(
            INVOKEVIRTUAL,
            "org/objectweb/fractal/rmi/stub/Stub", 
            "replaceClassValue", 
            "(Ljava/lang/Object;)Ljava/lang/Object;");
        } else {
          mv.visitVarInsn(ALOAD, locals + 2);
          mv.visitMethodInsn(
            INVOKEINTERFACE,
            "org/objectweb/jonathan/apis/presentation/UnMarshaller",
            "readValue",
            "()Ljava/lang/Object;");
        }
        mv.visitTypeInsn(CHECKCAST, Type.getInternalName(result));
      }
      mv.visitVarInsn(ISTORE + getOpcodeOffset(result), locals + 3);
    }

    // generate "unmarshaller.close();"
    mv.visitVarInsn(ALOAD, locals + 2);
    mv.visitMethodInsn(
      INVOKEINTERFACE,
      "org/objectweb/jonathan/apis/presentation/UnMarshaller",
      "close",
      "()V");

    // generate "return result";
    if (result != Void.TYPE) {
      mv.visitVarInsn(ILOAD + getOpcodeOffset(result), locals + 3);
      mv.visitInsn(IRETURN + getOpcodeOffset(result));
    } else {
      mv.visitInsn(RETURN);
    }
    mv.visitLabel(endL);

    mv.visitVarInsn(ASTORE, locals);

    // generate "e = handleException(e);"
    mv.visitVarInsn(ALOAD, 0);
    mv.visitVarInsn(ALOAD, locals);
    mv.visitMethodInsn(
      INVOKEVIRTUAL,
      name,
      "handleException",
      "(Ljava/lang/Exception;)Ljava/lang/Exception;");
    mv.visitVarInsn(ASTORE, locals);

    // generate "if (e instanceof <type>) throw (<type>)e;"
    for (int i = 0; i < exceptions.length; ++i) {
      String type = Type.getInternalName(exceptions[i]);
      mv.visitVarInsn(ALOAD, locals);
      mv.visitTypeInsn(INSTANCEOF, type);
      Label next = new Label();
      mv.visitJumpInsn(IFEQ, next);
      mv.visitVarInsn(ALOAD, locals);
      mv.visitTypeInsn(CHECKCAST, type);
      mv.visitInsn(ATHROW);
      mv.visitLabel(next);
    }

    // generate "if (e instanceof RuntimeException) throw (RuntimeException)e;"
    mv.visitVarInsn(ALOAD, locals);
    mv.visitTypeInsn(INSTANCEOF, "java/lang/RuntimeException");
    Label next = new Label();
    mv.visitJumpInsn(IFEQ, next);
    mv.visitVarInsn(ALOAD, locals);
    mv.visitTypeInsn(CHECKCAST, "java/lang/RuntimeException");
    mv.visitInsn(ATHROW);
    mv.visitLabel(next);

    // generate "throw new RemoteException("server side exception", e);"
    mv.visitTypeInsn(NEW, "org/objectweb/fractal/rmi/RemoteException");
    mv.visitInsn(DUP);
    mv.visitLdcInsn("server side exception");
    mv.visitVarInsn(ALOAD, locals);
    mv.visitMethodInsn(
      INVOKESPECIAL,
      "org/objectweb/fractal/rmi/RemoteException",
      "<init>",
      "(Ljava/lang/String;Ljava/lang/Exception;)V");
    mv.visitInsn(ATHROW);

    // end method body
    mv.visitTryCatchBlock(beginL, endL, endL, "java/lang/Exception");
    mv.visitMaxs(0, 0);
    mv.visitEnd();
  }

  /**
   * Generates a skeleton's {@link Skeleton#send send} method. The generated
   * method is of the following form:
   * <p>
   * <pre>
   * public void send (UnMarshaller unmarshaller, ReplySession session)
   *   throws JonathanException
   * {
   *   int methodIndex = unmarshaller.readInt();
   *   try {
   *     switch (methodIndex) {
   *       case 0:
   *         <i>T0 arg0</i> = unmarshaller.read<i>XXX</i>();
   *         ...
   *         unmarshaller.close();
   *         <i>T</i> result = ((<i>I</i>)target).<i>m0</i>(<i>arg0</i>, ... );
   *         Marshaller marshaller = session.prepareReply();
   *         marshaller.write<i>XXX</i>(result);
   *         session.send(marshaller);
   *         session.close();
   *         return;
   *       ...
   *       default:
   *         handleInterfaceMethods(unmarshaller, session, methodIndex);
   *     }
   *   } catch (Exception e) {
   *     handleException(e, session);
   *   }
   * }
   * </pre>
   *
   * @param cv the class visitor to be used to generate the stub method.
   * @param name the internal name of the generated stub class.
   * @param itf the target object's interface that must be exported by the
   *      skeleton.
   */

  protected void generateSkeletonMethod (
    final ClassVisitor cv,
    final String name,
    final Class itf)
  {
    MethodVisitor mv = cv.visitMethod(
      ACC_PUBLIC,
      "send",
      "(Lorg/objectweb/jonathan/apis/presentation/UnMarshaller;Lorg/objectweb/jonathan/apis/protocols/ReplySession;)V",
      null,
      new String[] { "org/objectweb/jonathan/apis/kernel/JonathanException" });

    Label beginL = new Label();
    Label endL = new Label();
    Label defaultL = new Label();

    // generate "int methodIndex = unmarshaller.readInt();"
    mv.visitLabel(beginL);
    mv.visitVarInsn(ALOAD, 1);
    mv.visitMethodInsn(
      INVOKEINTERFACE,
      "org/objectweb/jonathan/apis/presentation/UnMarshaller",
      "readInt",
      "()I");
    mv.visitVarInsn(ISTORE, 3);

    // generate the "switch (methodIndex)" statement
    Method[] meths = itf.getMethods();
    sort(meths);
    Label[] cases = new Label[meths.length];
    for (int i = 0; i < cases.length; ++i) {
      cases[i] = new Label();
    }
    if (meths.length > 0) {
      mv.visitVarInsn(ILOAD, 3);
      mv.visitTableSwitchInsn(0, meths.length - 1, defaultL, cases);
    }

    // generate the "case" blocks
    for (int i = 0; i < meths.length; ++i) {
      Method m = meths[i];
      String mName = m.getName();
      String mDesc = Type.getMethodDescriptor(m);
      Class[] params = m.getParameterTypes();
      Class result = m.getReturnType();

      mv.visitLabel(cases[i]);

      // generate "<type> obj = (<type>)target;"
      mv.visitVarInsn(ALOAD, 0);
      mv.visitFieldInsn(GETFIELD, name, "target", "Ljava/lang/Object;");
      mv.visitTypeInsn(CHECKCAST, Type.getInternalName(itf));

      // parameter unmarshalling
      for (int j = 0; j < params.length; ++j) {
        if (params[j].isPrimitive()) {
          mv.visitVarInsn(ALOAD, 1);
          mv.visitMethodInsn(
            INVOKEINTERFACE,
            "org/objectweb/jonathan/apis/presentation/UnMarshaller",
            "read" + getMarshallerMethod(params[j]),
            "()" + Type.getDescriptor(params[j]));
        } else {
          if (isClassParameter(m, j)) {
            mv.visitVarInsn(ALOAD, 0);
            mv.visitVarInsn(ALOAD, 1);
            mv.visitMethodInsn(
              INVOKEINTERFACE,
              "org/objectweb/jonathan/apis/presentation/UnMarshaller",
              "readValue",
              "()Ljava/lang/Object;");
            mv.visitMethodInsn(
              INVOKEVIRTUAL,
              "org/objectweb/fractal/rmi/stub/Skeleton", 
              "replaceClassValue", 
              "(Ljava/lang/Object;)Ljava/lang/Object;");            
          } else {
            mv.visitVarInsn(ALOAD, 1);
            mv.visitMethodInsn(
              INVOKEINTERFACE,
              "org/objectweb/jonathan/apis/presentation/UnMarshaller",
              "readValue",
              "()Ljava/lang/Object;");
          }
          mv.visitTypeInsn(CHECKCAST, Type.getInternalName(params[j]));
        }
      }

      // generate "unmarshaller.close();"
      mv.visitVarInsn(ALOAD, 1);
      mv.visitMethodInsn(
        INVOKEINTERFACE,
        "org/objectweb/jonathan/apis/presentation/UnMarshaller",
        "close",
        "()V");

      // generate Statistics.receiveCall(class.methodname)
      if(Statistics.isActivated()) {
        mv.visitLdcInsn(m.getDeclaringClass().getName() + '.' + mName);
        mv.visitMethodInsn(INVOKESTATIC, "org/objectweb/fractal/rmi/Statistics", "receiveCall", "(Ljava/lang/String;)V");
      }

      // generate code to call target method and store result
      mv.visitMethodInsn(INVOKEINTERFACE, Type.getInternalName(itf), mName, mDesc);
      if (result != Void.TYPE) {
        mv.visitVarInsn(ISTORE + getOpcodeOffset(result), 5);
      }

      // generate "Marshaller marshaller = session.prepareReply();"
      mv.visitVarInsn(ALOAD, 2);
      mv.visitMethodInsn(
        INVOKEINTERFACE,
        "org/objectweb/jonathan/apis/protocols/ReplySession",
        "prepareReply",
        "()Lorg/objectweb/jonathan/apis/presentation/Marshaller;");
      mv.visitVarInsn(ASTORE, 4);

      // generate code to marshall result value
      if (result != Void.TYPE) {
        mv.visitVarInsn(ALOAD, 4);
        if (result.isPrimitive()) {
          mv.visitVarInsn(ILOAD + getOpcodeOffset(result), 5);
          mv.visitMethodInsn(
            INVOKEINTERFACE,
            "org/objectweb/jonathan/apis/presentation/Marshaller",
            "write" + getMarshallerMethod(result),
            "(" + Type.getDescriptor(result)+ ")V");
        } else {
          if (isClassParameter(m, -1)) {
            mv.visitVarInsn(ALOAD, 0);
            mv.visitVarInsn(ILOAD + getOpcodeOffset(result), 5);
            mv.visitMethodInsn(
              INVOKEVIRTUAL,
              "org/objectweb/fractal/rmi/stub/Skeleton", 
              "replaceClassName", 
              "(Ljava/lang/Object;)Ljava/lang/Object;");
          } else {
            mv.visitVarInsn(ILOAD + getOpcodeOffset(result), 5);
          }
          mv.visitMethodInsn(
            INVOKEINTERFACE,
            "org/objectweb/jonathan/apis/presentation/Marshaller",
            "writeValue",
            "(Ljava/lang/Object;)V");
        }
      }

      // generate "session.send(marshaller);"
      mv.visitVarInsn(ALOAD, 2);
      mv.visitVarInsn(ALOAD, 4);
      mv.visitMethodInsn(
        INVOKEINTERFACE,
        "org/objectweb/jonathan/apis/protocols/ReplySession",
        "send",
        "(Lorg/objectweb/jonathan/apis/presentation/Marshaller;)V");

      // generate "session.close();"
      mv.visitVarInsn(ALOAD, 2);
      mv.visitMethodInsn(
        INVOKEINTERFACE,
        "org/objectweb/jonathan/apis/protocols/ReplySession",
        "close",
        "()V");

      // generate "return;"
      mv.visitInsn(RETURN);
    }

    // generate default block:
    //   "handleInterfaceMethods(unmarshaller, session, methodIndex);"
    //   "return;"
    mv.visitLabel(defaultL);
    mv.visitVarInsn(ALOAD, 0);
    mv.visitVarInsn(ALOAD, 1);
    mv.visitVarInsn(ALOAD, 2);
    mv.visitVarInsn(ILOAD, 3);
    mv.visitMethodInsn(
      INVOKEVIRTUAL,
      name,
      "handleInterfaceMethods",
      "(Lorg/objectweb/jonathan/apis/presentation/UnMarshaller;Lorg/objectweb/jonathan/apis/protocols/ReplySession;I)V");
    mv.visitInsn(RETURN);

    // generate "catch (Exception e) { handleException(e, session); }"
    mv.visitLabel(endL);
    mv.visitVarInsn(ASTORE, 3);
    mv.visitVarInsn(ALOAD, 0);
    mv.visitVarInsn(ALOAD, 3);
    mv.visitVarInsn(ALOAD, 2);
    mv.visitMethodInsn(
      INVOKEVIRTUAL,
      name,
      "handleException",
      "(Ljava/lang/Exception;Lorg/objectweb/jonathan/apis/protocols/ReplySession;)V");
    mv.visitInsn(RETURN);

    // end method code
    mv.visitTryCatchBlock(beginL, endL, endL, "java/lang/Exception");
    mv.visitMaxs(0, 0);
    mv.visitEnd();
  }

  /**
   * Returns <tt>true</tt> if the specified parameter contains the name of a
   * class. This method is used to marshall a <tt>Class</tt> object, instead of
   * a <tt>String</tt>, for parameters that contains class names, in order to
   * enable these classes to be downloaded from the network if needed. The
   * default implementation of this method returns <tt>true</tt> for:
   * <ul>
   * <li>the return value of <tt>Factory.getFcContentDesc</tt>.</li>
   * <li>the second parameter of <tt>GenericFactory.newFcInstance</tt>.</li>
   * <li>the return value of <tt>InterfaceType.getFcItfSignature</tt>.</li>
   * <li>the second parameter of <tt>TypeFactory.createFcItfType</tt>.</li>
   * </ul>
   *
   * @param m a method.
   * @param p index of a parameter of this method, or <tt>-1</tt> to designate
   *      the return value.
   * @return <tt>true</tt> if the specified parameter contains the name of a
   *      class.
   */

  protected boolean isClassParameter (final Method m, final int p) {
    String name = m.getName();
    if (name.equals("getFcContentDesc")) {
      return p == -1;
    } else if (name.equals("newFcInstance")) {
      return p == 2;
    } else if (name.equals("getFcItfSignature")) {
      return p == -1;
    } else if (name.equals("createFcItfType")) {
      return p == 1;
    }
    return false;
  }

  /**
   * Sorts the given methods. This method is used to assign an unambiguous
   * index to each method of an interface (the index of a method in
   * the array returned by {@link Class#getMethods() getMethods} cannot directly
   * be used for this purpose, since this method returns the methods in any
   * order).
   *
   * @param methods the method array to be sorted.
   */

  protected static void sort (final Method[] methods) {
    String[] descs = new String[methods.length];
    for (int i = 0; i < descs.length; ++i) {
      descs[i] = methods[i].getName() + Type.getMethodDescriptor(methods[i]);
    }
    for (int i = 0; i < descs.length; ++i) {
      for (int j = i + 1; j < descs.length; ++j) {
        if (descs[j].compareTo(descs[i]) < 0) {
          String s = descs[i];
          descs[i] = descs[j];
          descs[j] = s;
          Method m = methods[i];
          methods[i] = methods[j];
          methods[j] = m;
        }
      }
    }
  }

  /**
   * Returns the suffix of the (un)marshaller method corresponding to the given
   * primitive type.
   *
   * @param type a primitive type.
   * @return the suffix of the (un)marshaller method corresponding to the given
   *      primitive type (such as "Byte" for byte, or "Char16" for char).
   */

  private static String getMarshallerMethod (final Class type) {
    if (type == Byte.TYPE) {
      return "Byte";
    } else if (type == Integer.TYPE) {
      return "Int";
    } else if (type == Boolean.TYPE) {
      return "Boolean";
    } else if (type == Double.TYPE) {
      return "Double";
    } else if (type == Float.TYPE) {
      return "Float";
    } else if (type == Long.TYPE) {
      return "Long";
    } else if (type == Character.TYPE) {
      return "Char16";
    } else /*if (result == Short.TYPE)*/ {
      return "Short";
    }
  }

  /**
   * Returns the offset which must be added to some opcodes to get an opcode of
   * the given type. More precisely, returns the offset which must be added to
   * an opc_iXXXX opcode to get the opc_YXXXX opcode corresponding to the given
   * type. For example, if the given type is double the result is 3, which
   * means that opc_dload, opc_dstore, opc_dreturn... opcodes are equal to
   * opc_iload+3, opc_istore+3, opc_ireturn+3...
   *
   * @param type a Java class representing a Java type (primitive or not).
   * @return the opcode offset of the corresponding to the given type.
   */

  private static int getOpcodeOffset (final Class type) {
    if (type == Double.TYPE) {
      return 3;
    } else if (type == Float.TYPE) {
      return 2;
    } else if (type == Long.TYPE) {
      return 1;
    } else if (type.isPrimitive()) {
      return 0;
    }
    return 4;
  }

  /**
   * Returns the size of the given type. This size is 2 for the double and long
   * types, and 1 for the other types.
   *
   * @param type a Java class representing a Java type (primitive or not).
   * @return the size of the given type.
   */

  private static int getSize (final Class type) {
    return (type == Double.TYPE || type == Long.TYPE ? 2 : 1);
  }

  /**
   * Returns the name of the stub class for the given class.
   *
   * @param className a fully qualified class name.
   * @return the name of the stub class for the given class.
   */

  private static String getStubClassName (final String className) {
    if (className.startsWith("java.")) {
      return "stub." + className + "_JavaStub";
    } else {
      return className + "_Stub";
    }
  }

  /**
   * Returns <tt>true</tt> if the given name if the name of a stub class.
   *
   * @param className a fully qualified class name.
   * @return <tt>true</tt> if the given name if the name of a stub class.
   */

  public static boolean isStubClassName (final String className) {
    return className.endsWith("_JavaStub") || className.endsWith("_Stub");
  }

  /**
   * Returns the class name corresponding to the given stub class name.
   *
   * @param stubClassName a fully qualified stub class name.
   * @return the class name corresponding to the given stub class name.
   */

  private static String resolveStubClassName (final String stubClassName) {
    if (stubClassName.endsWith("_JavaStub")) {
      return stubClassName.substring(5, stubClassName.length() - 9);
    } else {
      return stubClassName.substring(0, stubClassName.length() - 5);
    }
  }

  /**
   * Returns the name of the skeleton class for the given class.
   *
   * @param className a fully qualified class name.
   * @return the name of the skeleton class for the given class.
   */

  private static String getSkelClassName (final String className) {
    if (className.startsWith("java.")) {
      return "skel." + className + "_JavaSkel";
    } else {
      return className + "_Skel";
    }
  }

  /**
   * Returns <tt>true</tt> if the given name if the name of a skeleton class.
   *
   * @param className a fully qualified class name.
   * @return <tt>true</tt> if the given name if the name of a skeleton class.
   */

  public static boolean isSkelClassName (final String className) {
    return className.endsWith("_JavaSkel") || className.endsWith("_Skel");
  }

  /**
   * Returns the class name corresponding to the given skeleton class name.
   *
   * @param skelClassName a fully qualified skeleton class name.
   * @return the class name corresponding to the given skeleton class name.
   */

  private static String resolveSkelClassName (final String skelClassName) {
    if (skelClassName.endsWith("_JavaSkel")) {
      return skelClassName.substring(5, skelClassName.length() - 9);
    } else {
      return skelClassName.substring(0, skelClassName.length() - 5);
    }
  }
}
