/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.jonathan;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.jonathan.apis.protocols.ip.TcpIpConnectionMgr;

/**
 * A Fractal compliant {@link
 * org.objectweb.jonathan.libs.resources.tcpip.JConnectionMgr} subclass.
 */

public class JConnectionMgr
  extends org.objectweb.jonathan.libs.resources.tcpip.JConnectionMgr
  implements BindingController
{

  public JConnectionMgr () {
    super(5, null);
  }

  public String[] listFc () {
    return new String[] { "delegate-factory" };
  }

  public Object lookupFc (final String clientItfName) {
    if (clientItfName.equals("delegate-factory")) {
      return factory;
    }
    return null;
  }

  public void bindFc (final String clientItfName, final Object serverItf) {
    if (clientItfName.equals("delegate-factory")) {
      factory = (TcpIpConnectionMgr)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (clientItfName.equals("delegate-factory")) {
      factory = null;
    }
  }
}
