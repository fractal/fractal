/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 *
 * adapted from Jonathan:
 *   org.objectweb.jeremie.libs.presentation.StdMarshallerFactory
 *     (authors: B. Dumant, K. Milsted)
 * with some comments copied from:
 *   org.objectweb.jonathan.apis.presentation.UnMarshaller
 *     (author: B. Dumant)
 */

package org.objectweb.fractal.rmi.io;

import org.objectweb.jonathan.apis.binding.NamingContext;
import org.objectweb.jonathan.apis.kernel.Context;
import org.objectweb.jonathan.apis.kernel.ContextFactory;
import org.objectweb.jonathan.apis.kernel.InternalException;
import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.presentation.EndOfMessageException;
import org.objectweb.jonathan.apis.presentation.UnMarshaller;
import org.objectweb.jonathan.apis.resources.Chunk;
import org.objectweb.jonathan.apis.resources.ChunkProvider;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;

/**
 * An {@link UnMarshaller} that uses an {@link RmiObjectInputStream} to
 * unmarshall object values.
 */

public class RmiUnMarshaller extends InputStream implements UnMarshaller {

  private final static Chunk EMPTY_CHUNK = new Chunk(new byte[0], 0, 0);

  /**
   * The domain used to instantiate {@link #is is}.
   */

  protected NamingContext domain;

  /**
   * The context factory used to create the context associated with this
   * marshaller. See {@link #getContext getContext}.
   */

  protected ContextFactory contextFactory;

  /**
   * The object input stream used to unmarshall object values. This stream is
   * lazily instantiated, i.e., it is instantiated at the first call to {@link
   * #readValue readValue} or to {@link #readReference readReference}.
   */

  protected ObjectInputStream is;

  private ChunkProvider provider;

  private Chunk first;

  private int offset;

  private int lastOffset;

  private Context context;

  // --------------------------------------------------------------------------
  // Constructors
  // --------------------------------------------------------------------------

  /**
   * Constructs a new {@link RmiUnMarshaller}.
   *
   * @param domain the domain to be used to instantiate {@link #is is}.
   * @param contextFactory the context factory to be used to create the context
   *      associated with this marshaller.
   * @param chunk a (chain of) chunk(s).
   * @param read the number of bytes already read from the message.
   */

  public RmiUnMarshaller (
    NamingContext domain,
    ContextFactory contextFactory,
    Chunk chunk,
    int read)
  {
    super();
    this.domain = domain;
    this.contextFactory = contextFactory;
    this.first = chunk;
    offset = chunk.offset;
    lastOffset = read - offset;
    context = null;
  }

  /**
   * Constructs a new {@link RmiUnMarshaller}.
   *
   * @param domain the domain to be used to instantiate {@link #is is}.
   * @param contextFactory the context factory to be used to create the context
   *      associated with this marshaller.
   * @param provider a chunk provider.
   */

  public RmiUnMarshaller (
    final NamingContext domain,
    final ContextFactory contextFactory,
    final ChunkProvider provider)
  {
    this(domain, contextFactory, EMPTY_CHUNK, 0);
    this.provider = provider;
  }

  // --------------------------------------------------------------------------
  // Implementation of the UnMarshaller interface
  // --------------------------------------------------------------------------

  /**
   * Returns true if this unmarshaller is little-endian, false otherwise.
   *
   * @return true if this unmarshaller is little-endian, false otherwise.
   */

  public boolean isLittleEndian () {
    return false;
  }

  /**
   * Sets the byte order  of this unmarshaller.
   *
   * @param littleEndian the new byte order.
   */

  public void setByteOrder (final boolean littleEndian) {
    throw new RuntimeException("Not implemented.");
  }

  /**
   * Returns a {@link Context} associated with this unmarshaller.
   *
   * @return a {@link Context} associated with this unmarshaller.
   */

  public final Context getContext () {
    if (context == null) {
      try {
        context = contextFactory.newContext();
      } catch (NullPointerException e) {
        e.printStackTrace(System.err);
        throw new InternalException("Context factory required.");
      }
    }
    return context;
  }

  /**
   * Returns the number of bytes read since the beginning.
   *
   * @return the number of bytes read since the beginning.
   */

  public int bytesRead () {
    return lastOffset + offset;
  }

  /**
   * Sets the number of bytes readable from the unmarshaller. Once this method
   * has been called, it won't be possible to read more than the
   * <code>size</code> specified bytes from this unmarshaller. Knowing the exact
   * number of readable bytes lets the unmarshaller free the resources (such as
   * a chunk provider) that won't be used. This method may block until the
   * expected number of bytes is readable.
   *
   * @param size the expected number of readable bytes.
   * @throws JonathanException if something goes wrong.
   */

  public void setSize (int size) throws JonathanException {
    boolean sz0 = (size == 0);
    Chunk result = null, current = null;
    int lastOff = lastOffset + offset;
    int available = first.top - offset;
    first.offset = offset;
    while (size > available) {
      if (available > 0) {
        if (current == null) {
          result = first.duplicate();
          current = result;
        } else {
          current.next = first.duplicate();
          current = current.next;
        }
        size -= available;
        offset = first.top;
      }
      prepare();
      available = first.top - offset;
    }
    if (size > 0) {
      if (current == null) {
        result = first.duplicate(offset,offset + size);
      } else {
        current.next = first.duplicate(offset,offset + size);
      }
      offset += size;
    }
    Context thisContext = context;
    if (context != null) {
      context.acquire();
    }
    close();
    context = thisContext;
    if (sz0) {
      first = EMPTY_CHUNK;
    } else {
      first = result;
    }
    offset = first.offset;
    lastOffset = lastOff - offset;
  }

  /**
   * Returns an input stream to read data from the unmarshaller. Closing the
   * returned input stream has the same effect as closing the actual
   * unmarshaller.
   *
   * @return an input stream to read from the unmarshaller.
   */

  public InputStream inputStream () {
    return this;
  }

  /**
   * Reads a byte.
   *
   * @return a byte.
   * @throws JonathanException if the format of the data is incompatible with
   *      the request.
   */

  public byte readByte () throws JonathanException {
    if (first.top == offset) {
      prepare();
    }
    return first.data[offset++];
  }

  /**
   * Reads a boolean.
   *
   * @return a boolean.
   * @throws JonathanException if the format of the data is incompatible with
   *      the request.
   */

  public boolean readBoolean () throws JonathanException {
    if (first.top == offset) {
      prepare();
    }
    return first.data[offset++] != 0;
  }

  /**
   * Reads a 8 bits char.
   *
   * @return a char.
   * @throws JonathanException if the format of the data is incompatible with
   *      the request.
   */

  public char readChar8 () throws JonathanException {
    if (first.top == offset) {
      prepare();
    }
    return (char) (first.data[offset++] & 0xFF);
  }

  /**
   * Reads a 16 bits char.
   *
   * @return a char.
   * @throws JonathanException if the format of the data is incompatible with
   *      the request.
   */

  public char readChar16 () throws JonathanException {
    byte[] data = first.data;
    int val = 0;
    if (first.top - offset >= 2) {
      val = ((data[offset] & 0xFF) << 8) + (data[offset + 1] & 0xFF);
      offset += 2;
    } else {
      int m = 1, p;
      while (m >= 0) {
        p = Math.min(m + 1, first.top-offset);
        if (p == 0) {
          prepare();
          data = first.data;
        } else {
          while (--p >= 0) {
            val += (data[offset++] & 0xFF) << (m-- * 8);
          }
        }
      }
    }
    return (char) val;
  }

  /**
   * Reads a short.
   *
   * @return a short.
   * @throws JonathanException if the format of the data is incompatible with
   *      the request.
   */

  public short readShort () throws JonathanException {
    byte[] data = first.data;
    int val = 0;
    if (first.top - offset >= 2) {
      val = ((data[offset] & 0xFF) << 8) + (data[offset + 1] & 0xFF);
      offset += 2;
    } else {
      int m = 1, p;
      while (m >= 0) {
        p = Math.min(m + 1, first.top-offset);
        if (p == 0) {
          prepare();
          data = first.data;
        } else {
          while (--p >= 0) {
            val += (data[offset++] & 0xFF) << (m-- * 8);
          }
        }
      }
    }
    return (short) val;
  }
  /**
   * Reads an int.
   *
   * @return an int.
   * @throws JonathanException if the format of the data is incompatible with
   *      the request.
   */

  public int readInt () throws JonathanException {
    byte[] data = first.data;
    int val = 0;
    if (first.top - offset >= 4) {
      val =
        ((data[offset] & 0xFF) << 24) +
        ((data[offset + 1] & 0xFF) << 16) +
        ((data[offset + 2] & 0xFF) << 8) +
        (data[offset + 3] & 0xFF);
      offset += 4;
    } else {
      int m = 3, p;
      while (m >= 0) {
        p = Math.min(m + 1, first.top-offset);
        if (p == 0) {
          prepare();
          data = first.data;
        } else {
          while (--p >= 0) {
            val += (data[offset++] & 0xFF) << (m-- * 8);
          }
        }
      }
    }
    return val;
  }

  /**
   * Reads a float.
   *
   * @return a float.
   * @throws JonathanException if the format of the data is incompatible with
   *      the request.
   */

  public float readFloat () throws JonathanException {
    byte[] data = first.data;
    int val = 0;
    if (first.top - offset >= 4) {
      val =
        ((data[offset] & 0xFF) << 24) +
        ((data[offset + 1] & 0xFF) << 16) +
        ((data[offset + 2] & 0xFF) << 8) +
        (data[offset + 3] & 0xFF);
      offset += 4;
    } else {
      int m = 3, p;
      while (m >= 0) {
        p = Math.min(m + 1, first.top-offset);
        if (p == 0) {
          prepare();
          data = first.data;
        } else {
          while (--p >= 0) {
            val += (data[offset++] & 0xFF) << (m-- * 8);
          }
        }
      }
    }
    return Float.intBitsToFloat(val);
  }

  /**
   * Reads a long.
   *
   * @return a long.
   * @throws JonathanException if the format of the data is incompatible with
   *      the request.
   */

  public long readLong () throws JonathanException {
    byte[] data = first.data;
    long val = 0;
    if (first.top - offset >= 8) {
      val =
        (((long)data[offset] & 0xFF) << 56) +
        (((long)data[offset + 1] & 0xFF) << 48) +
        (((long)data[offset + 2] & 0xFF) << 40) +
        (((long)data[offset + 3] & 0xFF) << 32) +
        (((long)data[offset + 4] & 0xFF) << 24) +
        (((long)data[offset + 5] & 0xFF) << 16) +
        (((long)data[offset + 6] & 0xFF) << 8) +
        ((long)data[offset + 7] & 0xFF);
      offset += 8;
    } else {
      int m = 7, p;
      while (m >= 0) {
        p = Math.min(m + 1,first.top-offset);
        if (p == 0) {
          prepare();
          data = first.data;
        } else {
          while (--p >= 0) {
            val += (((long) data[offset++]) & 0xFF) << (m-- * 8);
          }

        }
      }
    }
    return val;
  }

  /**
   * Reads a double.
   *
   * @return a double.
   * @throws JonathanException if the format of the data is incompatible with
   *      the request.
   */

  public double readDouble () throws JonathanException {
    byte[] data = first.data;
    long val = 0;
    if (first.top - offset >= 8) {
      val =
        (((long)data[offset] & 0xFF) << 56) +
        (((long)data[offset + 1] & 0xFF) << 48) +
        (((long)data[offset + 2] & 0xFF) << 40) +
        (((long)data[offset + 3] & 0xFF) << 32) +
        (((long)data[offset + 4] & 0xFF) << 24) +
        (((long)data[offset + 5] & 0xFF) << 16) +
        (((long)data[offset + 6] & 0xFF) << 8) +
        ((long)data[offset + 7] & 0xFF);
      offset += 8;
      return Double.longBitsToDouble(val);
    } else {
      int m = 7, p;
      while (m >= 0) {
        p = Math.min(m + 1,first.top-offset);
        if (p == 0) {
          prepare();
          data = first.data;
        } else {
          while (--p >= 0) {
            val += (((long) data[offset++]) & 0xFF) << (m-- * 8);
          }
        }
      }
      return Double.longBitsToDouble(val);
    }
  }

  /**
   * Reads a string composed of 8 bits chars.
   *
   * @return a string.
   * @throws JonathanException if the format of the data is incompatible with
   *      the request.
   */

  public String readString8 () throws JonathanException {
    int len = readInt() - 1;
    char[] tab = new char[len];
    byte[] data = first.data;
    int off = 0;
    int max = first.top - offset;
    // read the characters
    while (max < len) {
      while (off < max) {
        tab[off++] = (char) (data[offset++] & 0xFF);
      }
      prepare();
      data = first.data;
      max = first.top - offset + off;
    }
    while (off < len) {
      tab[off++] = (char) (data[offset++] & 0xFF);
    }
    if (first.top == offset) {
      prepare();
      offset++;
    } else {
      offset++;
    }
    return new String(tab);
  }

  /**
   * Reads a string composed of 16 bits chars.
   *
   * @return a string.
   * @throws JonathanException if the format of the data is incompatible with
   *      the request.
   */

  public String readString16 () throws JonathanException {
    throw new InternalException("Not implemented.");
  }

  /**
   * Reads an array of bytes.
   *
   * @param array a byte array (of size >= offset + len).
   * @param off the position (in array) of the first byte to write.
   * @param len the total number of bytes to read.
   * @throws JonathanException if the format of the data is incompatible with
   *      the request.
   */

  public void readByteArray (final byte[] array, int off, int len)
    throws JonathanException
  {
    byte[] data = first.data;
    int max = first.top - offset;
    while (max < len) {
      System.arraycopy(data,offset,array,off,max);
      offset += max;
      off += max;
      len -= max;
      prepare();
      data = first.data;
      max = first.top - offset;
    }
    System.arraycopy(data,offset,array,off,len);
    offset += len;
  }

  /**
   * Reads a reference to an object.
   *
   * @return a reference to an object.
   * @throws JonathanException if the format of the data is incompatible with
   *      the request.
   */

  public Object readReference () throws JonathanException {
    try {
      initObjectStream();
      return is.readObject();
    } catch (Exception e) {
      System.err.println(Thread.currentThread() + " Exception " + e);
      throw new JonathanException(e);
    }
  }

  /**
   * Reads a value.
   *
   * @return an object representing the read value.
   * @throws JonathanException if the format of the data is incompatible with
   *      the request.
   */

  public Object readValue () throws JonathanException {
    try {
      initObjectStream();
      return is.readObject();
    } catch (Exception e) {
      System.err.println(Thread.currentThread() + " Exception " + e);
      throw new JonathanException(e);
    }
  }

  // --------------------------------------------------------------------------
  // Overriden methods
  // --------------------------------------------------------------------------

  public int available () throws IOException {
    Chunk chunk = first;
    int size = chunk.top - offset;
    chunk = chunk.next;
    while (chunk != null) {
      size += chunk.top - chunk.offset;
      chunk = chunk.next;
    }
    return size;
  }

  public int read () throws IOException {
    try {
      if (first.top == offset) {
        prepare();
      }
      return first.data[offset++] & 0xFF;
    } catch (JonathanException e) {
      Throwable f = e.represents();
      if (f instanceof EndOfMessageException) {
        return -1;
      } else if (f instanceof IOException) {
        throw (IOException) f;
      } else {
        throw new IOException(f.getMessage());
      }
    }
  }

  public int read (final byte[] array, int off, int length) throws IOException {
    try {
      if (array == null) {
        return (int) skip(length);
      } else {
        int len = length, toCopy, available;
        while(len > 0) {
          available = first.top - offset;
          if (available == 0) {
            prepare();
            available = first.top - offset;
          }
          toCopy = Math.min(available,len);
          System.arraycopy(first.data,offset,array,off,toCopy);
          offset += toCopy;
          off += toCopy;
          len -= toCopy;
        }
        return length;
      }
    } catch (JonathanException e) {
      Throwable f = e.represents();
      if (f instanceof EndOfMessageException) {
        return -1;
      } else if (f instanceof IOException) {
        throw (IOException) f;
      } else {
        throw new IOException(f.getMessage());
      }
    }
  }

  public long skip (final long n) throws IOException {
    try {
      long len = n, toSkip;
      int available;
      while(len > 0) {
        available = first.top - offset;
        if (available == 0) {
          prepare();
          available = first.top - offset;
        }
        toSkip = Math.min(available,len);
        offset += toSkip;
        len -= toSkip;
      }
      return n;
    } catch (JonathanException e) {
      Throwable f = e.represents();
      if (f instanceof EndOfMessageException) {
        return -1;
      } else if (f instanceof IOException) {
        throw (IOException) f;
      } else {
        throw new IOException(f.getMessage());
      }
    }
  }

  /**
   * Closes this input stream and releases any system resources associated
   * with the stream.
   */

  public void close () {
    if (first != null) {
      first.offset = offset;
    }
    Chunk next;
    while (first != null) {
      next = first.next;
      first.release();
      first = next;
    }
    if (provider != null) {
      provider.close();
      provider = null;
    }
    if (context != null) {
      context.release();
      context = null;
    }
  }

  // --------------------------------------------------------------------------
  // Utility methods
  // --------------------------------------------------------------------------

  /**
   * Instantiates the delegate object input stream {@link #is is}. This
   * method creates {@link RmiObjectInputStream} stream.
   *
   * @throws IOException if the delegate object input stream cannot be created.
   */

  protected void initObjectStream () throws IOException {
    if (is == null) {
      is = new RmiObjectInputStream(this, domain, contextFactory);
    }
  }

  private void prepare () throws JonathanException {
    lastOffset += offset;
    first.offset = offset;
    Chunk prev;
    while (first != null && first.top == first.offset) {
      prev = first;
      first = first.next;
      prev.release();
    }
    if (first != null) {
      offset = first.offset;
      lastOffset -= offset;
    } else if (provider != null) {
      first = provider.prepare();
      offset = first.offset;
      lastOffset -= offset;
    } else {
      throw new EndOfMessageException();
    }
  }
}
