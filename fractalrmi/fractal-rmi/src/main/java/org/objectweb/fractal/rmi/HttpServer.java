/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Philippe Merle
 *
 * Contributor:
 *
 * $Id$
 */

package org.objectweb.fractal.rmi;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintStream;
import java.io.Reader;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * This class is a HTTP server to download classes.
 */

public class HttpServer extends Thread
{
  private boolean logMode;

  private ServerSocket serverSocket;

  private String serverUrl;

  public HttpServer (final boolean logMode)
  throws Exception
  {
    this.logMode = logMode;

    // Create the socket where accepting HTTP requests.
    serverSocket = new ServerSocket(0);

    // Compute the URL of the HTTP server.
    serverUrl = "http://" + InetAddress.getLocalHost().getHostName()
                + ":" + serverSocket.getLocalPort() + "/";

    // Set the Fractal RMI codebase to this HTTP server.
    System.getProperties().setProperty("fractal.rmi.codebase", serverUrl);

    // Start the HTTP server.
    super.start();
  }

  protected void log (final String message) {
    if(logMode) {
      System.out.println("HTTP Server(" + serverUrl + "): " + message);
    }
  }

  public void run() {
    log("Ready");
    while (true) {
      try {
        Socket socket = serverSocket.accept();
        Reader in = new InputStreamReader(socket.getInputStream());
        String rq = new LineNumberReader(in).readLine();
        PrintStream out = new PrintStream(socket.getOutputStream());

        if (rq.startsWith("GET ")) {
          String url = rq.substring(5, rq.indexOf(' ', 4));
          InputStream is = this.getContextClassLoader().getResourceAsStream(url);
          if(is != null) {
            log("GET " + url + " " + is.available());
            byte[] data = new byte[is.available()];
            is.read(data);
            is.close();
            out.print("HTTP/1.0 200 OK\n\n");
            out.write(data);
          } else {
            log("Not found " + url);
            out.print("HTTP/1.0 404 Not Found\n\n");
            out.print("<html>Document not found.</html>");
          }
          out.close();
          socket.close();
        }
      } catch (IOException exc) {
        throw new RuntimeException("Can't run", exc);
      }
    }
  }

  public void shutdown () {
    try {
      log("shutdown");
      serverSocket.close();
    } catch (IOException exc) {
      throw new RuntimeException("Can't shutdown", exc);
    }
  }
}
