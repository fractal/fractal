/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2006-2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Philippe Merle
 *
 * Contributor: Jeremy Dubus
 *
 * $Id$
 */

package org.objectweb.fractal.rmi;

import org.objectweb.fractal.rmi.registry.NamingService;
import org.objectweb.fractal.rmi.registry.Registry;

import org.objectweb.fractal.util.Fractal;

import java.net.URI;

/**
 * Main class for launching Fractal RMI servers.
 *
 * Usage: java ServerLauncher [-h|--help] [-r|--registry host[:port]] <server_name>
 *   -h|--help     Displays usage.
 *   -r|--registry Sets the address of the Fractal RMI Registry to register the server.
 *                 By default, the local one is used.
 *   <server_name> Sets the server name used to register the server.
 *
 * @author Philippe Merle
 */

public class ServerLauncher {

  public static void main (final String[] args)
    throws Exception
  {
    //
    // Parses command line arguments.
    //

    String serverName = null;
    String registryHostPortAddress = null;

    for(int i=0; i<args.length; i++) {
      String arg = args[i];
      if(arg.equals("-h") || arg.equals("--help")) {
        usage(0);
      }
      else if(arg.equals("-r") || arg.equals("--registry")) {
        if(registryHostPortAddress != null) {
          System.err.println("Error: The Fractal RMI Registry address is already set!");
          usage(-1);
        }
        i++;
        registryHostPortAddress = args[i];
      }
      else if(!arg.startsWith("-")) {
        if(serverName != null) {
          System.err.println("Error: The server name is already set!");
          usage(-1);
        }
        serverName = arg;
      }
      else {
        System.err.println("Error: Invalid argument " + arg);
        usage(-1);
      }
    } 

    // Checks that the server name is provided.
    if(serverName == null) {
      System.err.println("Error: The server name is not set!");
      usage(-1);
    }

    //
    // Gets the Fractal RMI Registry.
    //

    URI registryUri = Registry.newURI(registryHostPortAddress);
    NamingService ns = Registry.getRegistry(registryUri);

    //
    // Rebinds the server into the Fractal RMI Registry.
    //

    ns.rebind(serverName, Fractal.getBootstrapComponent());

    System.out.println("Server ready and available at " + registryUri + "/" + serverName);
  }

  public static void usage (final int exitStatus) {
    System.out.println("usage: java ServerLauncher [-h|--help] [-r|--registry host[:port]] <server_name>");
    System.out.println("  -h|--help     Displays usage.");
    System.out.println("  -r|--registry Sets the address of the Fractal RMI Registry to register the server.");
    System.out.println("                By default, the local one is used.");
    System.out.println("  <server_name> Sets the server name used to register the server.");
    System.exit(exitStatus);
  }
}
