/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Philippe Merle
 *
 * Contributor:
 *
 * $Id$
 */

package org.objectweb.fractal.rmi.stub;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.rmi.Statistics;

/**
 * Super class of the stub classes generated by the {@link RmiStubFactory}.
 *
 * @author <a href="Philippe.Merle@inria.fr">Philippe Merle</a>
 */

public class StubStatistics extends Stub {

  /**
   * Constructs a new {@link StubStatistics}.
   */

  public StubStatistics () {
  }

  // --------------------------------------------------------------------------
  // Implementation of the Interface interface
  // --------------------------------------------------------------------------

  public Component getFcItfOwner () {
	Statistics.sendCall("org.objectweb.fractal.api.Interface.getFcItfOwner");
    return super.getFcItfOwner();
  }

  public String getFcItfName () {
	Statistics.sendCall("org.objectweb.fractal.api.Interface.getFcItfName");
    return super.getFcItfName();
  }

  public Type getFcItfType () {
	Statistics.sendCall("org.objectweb.fractal.api.Interface.getFcItfType");
    return super.getFcItfType();
  }

  public boolean isFcInternalItf () {
	Statistics.sendCall("org.objectweb.fractal.api.Interface.isFcInternalItf");
    return super.isFcInternalItf();
  }
}
