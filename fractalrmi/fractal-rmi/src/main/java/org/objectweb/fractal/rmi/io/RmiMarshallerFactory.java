/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 *
 * adapted from Jonathan:
 *   org.objectweb.jeremie.libs.presentation.StdMarshallerFactory
 *     (authors: B. Dumant, K. Milsted)
 * with some comments copied from:
 *   org.objectweb.jonathan.apis.presentation.MarshallerFactory
 *     (author: B. Dumant)
 */

package org.objectweb.fractal.rmi.io;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.jonathan.apis.binding.NamingContext;
import org.objectweb.jonathan.apis.kernel.ContextFactory;
import org.objectweb.jonathan.apis.presentation.Marshaller;
import org.objectweb.jonathan.apis.presentation.MarshallerFactory;
import org.objectweb.jonathan.apis.presentation.UnMarshaller;
import org.objectweb.jonathan.apis.resources.Chunk;
import org.objectweb.jonathan.apis.resources.ChunkFactory;
import org.objectweb.jonathan.apis.resources.ChunkProvider;

/**
 * Provides a factory for creating marshallers and unmarshallers.
 */

public class RmiMarshallerFactory
  implements MarshallerFactory, BindingController
{

  /**
   * The domain to be used by the (un)marshallers created by this factory.
   */

  protected NamingContext domain;

  /**
   * The chunk factory to be used by the (un)marshallers created by this
   * factory.
   */

  protected ChunkFactory chunkFactory;

  /**
   * The context factory to be used by the (un)marshallers created by this
   * factory.
   */

  protected ContextFactory contextFactory;

  /**
   * Constructs a new {@link RmiMarshallerFactory}.
   */

  public RmiMarshallerFactory () {
  }

  // --------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // --------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] {
      "domain",
      "chunk-factory",
      "context-factory"
    };
  }

  public Object lookupFc (final String clientItfName) {
    if (clientItfName.equals("domain")) {
      return domain;
    } else if (clientItfName.equals("chunk-factory")) {
      return chunkFactory;
    } else if (clientItfName.equals("context-factory")) {
      return contextFactory;
    }
    return null;
  }

  public void bindFc (final String clientItfName, final Object serverItf) {
    if (clientItfName.equals("domain")) {
      domain = (NamingContext)serverItf;
    } else if (clientItfName.equals("chunk-factory")) {
      chunkFactory = (ChunkFactory)serverItf;
    } else if (clientItfName.equals("context-factory")) {
      contextFactory = (ContextFactory)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (clientItfName.equals("domain")) {
      domain = null;
    } else if (clientItfName.equals("chunk-factory")) {
      chunkFactory = null;
    } else if (clientItfName.equals("context-factory")) {
      contextFactory = null;
    }
  }

  // --------------------------------------------------------------------------
  // Implementation of the MarshallerFactory interface
  // --------------------------------------------------------------------------

  /**
   * Returns a new marshaller.
   *
   * @return a new marshaller.
   */

  public Marshaller newMarshaller() {
    return new RmiMarshaller(domain, chunkFactory, contextFactory);
  }

  /**
   * Returns a new unmarshaller, using the provided chunk provider as a data
   * source.
   *
   * @param message a chunk provider.
   * @return a new unmarshaller.
   */

  public UnMarshaller newUnMarshaller (final ChunkProvider message) {
    return new RmiUnMarshaller(domain, contextFactory, message);
  }

  /**
   * Returns a new unmarshaller, using the provided chunk(s) as a data source.
   * The <tt>read</tt> parameter is used to initialize the number of bytes
   * read from the message.
   *
   * @param chunk a (chain of) chunk(s).
   * @param read the number of bytes already read from the message.
   * @return an unmarshaller.
   */

  public UnMarshaller newUnMarshaller (final Chunk chunk, final int read) {
    return new RmiUnMarshaller(domain, contextFactory, chunk, read);
  }
}

