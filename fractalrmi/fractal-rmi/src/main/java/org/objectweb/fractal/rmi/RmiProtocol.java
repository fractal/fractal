/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 *
 * adapted from Jonathan:
 *   org.objectweb.david.libs.protocols.giop.GIOPProtocol
 *     (authors: B. Dumant, S. Chambon, H. Piccone, S. Thiebaud)
 * with some comments copied from:
 *   org.objectweb.jonathan.apis.protocols.Protocol
 *     (author: B. Dumant)
 */

package org.objectweb.fractal.rmi;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.jonathan.apis.binding.BindException;
import org.objectweb.jonathan.apis.binding.ExportException;
import org.objectweb.jonathan.apis.binding.Identifier;
import org.objectweb.jonathan.apis.binding.NamingContext;
import org.objectweb.jonathan.apis.kernel.Context;
import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.presentation.Marshaller;
import org.objectweb.jonathan.apis.presentation.MarshallerFactory;
import org.objectweb.jonathan.apis.presentation.UnMarshaller;
import org.objectweb.jonathan.apis.protocols.Protocol;
import org.objectweb.jonathan.apis.protocols.ProtocolGraph;
import org.objectweb.jonathan.apis.protocols.ReplyInterface;
import org.objectweb.jonathan.apis.protocols.ReplySession;
import org.objectweb.jonathan.apis.protocols.RequestSession;
import org.objectweb.jonathan.apis.protocols.ServerException;
import org.objectweb.jonathan.apis.protocols.SessionIdentifier;
import org.objectweb.jonathan.apis.protocols.Session_High;
import org.objectweb.jonathan.apis.protocols.Session_Low;
import org.objectweb.jonathan.apis.resources.Chunk;
import org.objectweb.jonathan.apis.resources.Scheduler;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.LoggerFactory;

import java.util.Properties;

/**
 * Provides a very simple invocation protocol component. This invocation
 * protocol uses only two kinds of messages:
 * <ul>
 * <li>request messages:
 * <table border>
 *   <tr>
 *     <td colspan="3"><center>HEADER</center></td>
 *     <td><center>PAYLOAD</center></td>
 *   </tr>
 *   <tr>
 *      <td><center>&nbsp;request identifier (4 bytes)&nbsp;</center></td>
 *      <td><center>&nbsp;object key length (4 bytes)&nbsp;</center></td>
 *      <td><center>&nbsp;object key data (variable length)&nbsp;</center></td>
 *      <td><center>&nbsp;payload (variable length)&nbsp;</center></td>
 *   </tr>
 * </table>
 * </li>
 * <li>reply messages:
 * <table border>
 *   <tr>
 *     <td colspan="2"><center>HEADER</center></td>
 *     <td><center>PAYLOAD</center></td>
 *   </tr>
 *   <tr>
 *     <td><center>&nbsp;request identifier (4 bytes)&nbsp;</center></td>
 *     <td><center>&nbsp;exception flag (1 byte)&nbsp;</center></td>
 *     <td><center>&nbsp;payload (variable length)&nbsp;</center></td>
 *   </tr>
 * </table>
 * </li>
 * </ul>
 * The request identifier is used to associate request and reply messages
 * correctly. The object key identifies the remote object on which the
 * invocation must be performed. The exception flag is used to indicate if a
 * reply contains a normal result or an exception.
 */

public class RmiProtocol implements Protocol, BindingController {

  /**
   * The naming context used to decode the object key contained in request
   * messages.
   */

  protected NamingContext adapter;

  /**
   * The marshaller factory used to create request or reply messages.
   */

  protected MarshallerFactory marshallerFactory;

  /**
   * The scheduler used to synchronized threads for waiting reply messages.
   */

  protected Scheduler scheduler;

  /**
   * The optional logger factory used to get a logger for this component.
   */

  protected LoggerFactory loggerFactory;

  /**
   * The logger used to log messages. May be <tt>null</tt>.
   */

  protected Logger logger;

  private ClientSession_Low clientSessionLow;

  private ServerSession_Low serverSessionLow;

  private ReplyHolder[] table;

  private ReplyHolder reusable;

  private int size;

  private int id;

  /**
   * Constructs a new {@link RmiProtocol}.
   */

  public RmiProtocol () {
    clientSessionLow = new ClientSession_Low();
    serverSessionLow = new ServerSession_Low();
    table = new ReplyHolder[17];
  }

  // --------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // --------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] {
      "adapter",
      "marshaller-factory",
      "scheduler",
      "logger-factory"
    };
  }

  public Object lookupFc (final String clientItfName) {
    if (clientItfName.equals("adapter")) {
      return adapter;
    } else if (clientItfName.equals("marshaller-factory")) {
      return marshallerFactory;
    } else if (clientItfName.equals("scheduler")) {
      return scheduler;
    } else if (clientItfName.equals("logger-factory")) {
      return loggerFactory;
    }
    return null;
  }

  public void bindFc (final String clientItfName, final Object serverItf) {
    if (clientItfName.equals("adapter")) {
      adapter = (NamingContext)serverItf;
    } else if (clientItfName.equals("marshaller-factory")) {
      marshallerFactory = (MarshallerFactory)serverItf;
    } else if (clientItfName.equals("scheduler")) {
      scheduler = (Scheduler)serverItf;
    } else if (clientItfName.equals("logger-factory")) {
      loggerFactory = (LoggerFactory)serverItf;
      logger = loggerFactory.getLogger(getClass().getName());
    }
  }

  public void unbindFc (final String clientItfName) {
    if (clientItfName.equals("adapter")) {
      adapter = null;
    } else if (clientItfName.equals("marshaller-factory")) {
      marshallerFactory = null;
    } else if (clientItfName.equals("scheduler")) {
      scheduler = null;
    } else if (clientItfName.equals("logger-factory")) {
      loggerFactory = null;
      logger = null;
    }
  }

  // --------------------------------------------------------------------------
  // Implementation of the Protocol interface
  // --------------------------------------------------------------------------

  /**
   * Returns true if the target protocol is an invocation protocol. An
   * invocation protocol is a protocol able to handle <i>invocations</i>, i.e.,
   * requests expecting a reply. In practice, this means that calls to the
   * {@link Session_High#prepareInvocation(Marshaller) prepareInvocation} method
   * on sessions obtained from the target protocol will not raise an
   * {@link org.objectweb.jonathan.apis.kernel.InternalException}, but perform
   * the appropriate work.
   *
   * @return true if the target protocol is an invocation protocol.
   */

  public boolean isAnInvocationProtocol () {
    return true;
  }

  /**
   * Creates a new protocol graph with a number of given sub protocol graphs.
   *
   * @param subgraphs the lower-level graphs.
   * @param hints the information required to build the graph.
   * @return a new ProtocolGraph.
   * @throws JonathanException if the hints or the subgraphs are invalid for
   *      this protocol.
   */

  public ProtocolGraph createProtocolGraph (
    final ProtocolGraph[] subgraphs,
    final Context hints) throws JonathanException
  {
    if (subgraphs.length != 1) {
      throw new JonathanException("Lower layers badly specified in RMIP");
    }
    return new Graph(subgraphs[0]);
  }

  /**
   * Creates a new session identifier with the provided info.
   * 
   * @param info the information to create the session identifier.
   * @param next lower session identifiers, if any.
   * @return the created session identifer.
   * @throws JonathanException if something goes wrong.
   */

  public SessionIdentifier createSessionIdentifier (
    final Properties info,
    final SessionIdentifier[] next) throws JonathanException
  {
    if (next.length != 1) {
      throw new JonathanException("Lower layers badly specified in RMIP");
    }
    byte[] key = (byte[])(info.get("object_key"));
    return new CltSessionId(key, next[0]);
  }

  // --------------------------------------------------------------------------
  // ProtocolGraph and Session identifiers
  // --------------------------------------------------------------------------

  class Graph implements ProtocolGraph {

    ProtocolGraph next;

    public Graph (final ProtocolGraph next) {
      this.next = next;
    }

    /*
    public boolean equals (final Object o) {
      if (o instanceof Graph) {
        Graph pgraph = (Graph) o;
        boolean one = false, two = false;
        if (next != null) {
          one = next.equals(pgraph.next);
        } else {
          one = pgraph.next == null;
        }
        return one;
      }
      return false;
    }

    public int hashCode () {
      if (next != null) {
        return  next.hashCode();
      } else {
        return 0;
      }
    }
    */

    public SessionIdentifier export (final Session_Low ignored)
      throws JonathanException
    {
      if (next == null) {
        throw new ExportException("Badly specified participants");
      }
      return new SrvSessionId(next.export(serverSessionLow));
    }
  }

  class SrvSessionId implements SessionIdentifier {

    SessionIdentifier next;

    public SrvSessionId (final SessionIdentifier next) {
      this.next = next;
    }

    public Session_High bind (final Session_Low ignored)
      throws JonathanException
    {
      throw new BindException("Bad session identifier type");
    }

    public void unexport () {
      next.unexport();
    }

    public Protocol getProtocol () {
      return RmiProtocol.this;
    }

    public SessionIdentifier[] next () {
      return new SessionIdentifier[] {next};
    }

    /*
    public boolean equals (final Object o) {
      if (o instanceof SrvSessionId) {
        SrvSessionId sessionId = (SrvSessionId)o;
        boolean one = false, two = false;
        if (next != null) {
          one = next.equals(sessionId.next);
        } else {
          one = sessionId.next == null;
        }
        return one;

      }
      return false;
    }

    public int hashCode () {
      if (next != null) {
        return  next.hashCode();
      } else {
        return 0;
      }
    }
    */

    public int getProtocolId () {
      return 0;
    }

    public Context getInfo () throws JonathanException {
      throw new JonathanException("Not implemented");
      //return contextFactory.newContext();
    }

    public boolean isLocal () {
      return false; // meaningless in this case
    }
  }

  class CltSessionId extends SrvSessionId {

    byte[] key;

    public CltSessionId (
      final byte[] key,
      final SessionIdentifier next)
    {
      super(next);
      this.key = key;
    }

    public Session_High bind (final Session_Low ignored)
      throws JonathanException
    {
      if (next == null) {
        throw new BindException("Badly specified participants");
      }
      return new ClientSession_High(key, next.bind(clientSessionLow));
    }

    public void unexport () {}

    /*
    public boolean equals (final Object o) {
      if (o instanceof CltSessionId) {
        CltSessionId sessionId = (CltSessionId)o;
        int len = key.length;
        byte[] otherKey = sessionId.key;
        if (otherKey.length == len) {
          for (int i = 0; i < len; i++) {
            if (otherKey[i] != key[i]) {
              return false;
            }
          }
          return super.equals(sessionId);
        }
      }
      return false;
    }

    public int hashCode () {
      int hash = 0;
      int len = key.length;
      for (int i = 0; i < len; i++) {
        hash += (key[i] << (i % 32));
      }
      return  hash + super.hashCode();
    }

    public String toString () {
      String str = "CltSessionId[key[";
      if (key.length > 0) {
        str = str + key[0];
      }
      for (int i = 1; i < key.length; i++) {
        str = str + "," + key[i];
      }
      return str;
    }
    */

    public Context getInfo () throws JonathanException {
      throw new JonathanException("Meaningless");
    }

    public boolean isLocal() {
      return next.isLocal();
    }
  }

  // --------------------------------------------------------------------------
  // SessionLow and SessionHigh interfaces
  // --------------------------------------------------------------------------

  class ClientSession_Low implements Session_Low {

    public void send (
      final UnMarshaller unmarshaller,
      final Session_High sender) throws JonathanException
    {
      try {
        int rqId = unmarshaller.readInt();
        ReplyHolder reply = getHolder(rqId);
        if (reply == null) {
          // we can't find the request: return silently
          unmarshaller.close();
          if (logger != null && logger.isLoggable(BasicLevel.INFO)) {
            logger.log(
              BasicLevel.DEBUG, "RMIP Request #" + rqId + " not found");
          }
        } else {
          reply.sendReply(unmarshaller);
        }
      } catch (JonathanException e) {
        unmarshaller.close();
        send(e, sender);
      }
    }

    public void send (final JonathanException e, final Session_High sender) {
      forwardException(e, sender);
    }
  }

  class ServerSession_Low implements Session_Low {

    public void send (
      final UnMarshaller unmarshaller,
      final Session_High sender) throws JonathanException
    {
      boolean unmarshallerOpened = true;
      int rqId = unmarshaller.readInt();
      RequestSession requestSession = null;
      try {
        int len = unmarshaller.readInt();
        byte[] key = new byte[len];
        unmarshaller.readByteArray(key,0,len);
        Identifier id = adapter.decode(key, 0, len);
        requestSession = (RequestSession)id.bind(null, null);
        if (requestSession != null) {
          ServerSession_High replySession =
            new ServerSession_High(sender, rqId);
          requestSession.send(unmarshaller, replySession);
          return;
        }
        unmarshallerOpened = false;
        unmarshaller.close();
      } catch (Exception e) {
        if (logger != null && logger.isLoggable(BasicLevel.INFO)) {
          logger.log(BasicLevel.DEBUG, "Exception caught in RMIP", e);
        }
        if (unmarshallerOpened) {
          unmarshaller.close();
        }
        sendException(e, rqId, sender);
      }
    }

    public void send (final JonathanException e, final Session_High sender) {
      if (logger != null && logger.isLoggable(BasicLevel.INFO)) {
        logger.log(
          BasicLevel.DEBUG, "Exception caught in RMIP related to " + sender, e);
      }
      sender.close();
    }

    void sendException (
      final Exception e,
      final int rqId,
      final Session_High session)
    {
      try {
        Marshaller marshaller = prepareReplyMessage(rqId, true);
        marshaller.writeValue(e);
        sendMessage(marshaller, session);
      } catch (Exception ignored) {
      } finally {
        session.close();
      }
    }
  }

  class RMIPSession_High {

    Session_High lower;

    public RMIPSession_High (final Session_High lower) {
      this.lower = lower;
    }

    public boolean direct () {
      return false;
    }

    public void send (final Marshaller marshaller) throws JonathanException {
      sendMessage(marshaller, lower);
    }

    public void close() {
      lower.close();
    }
  }

  class ClientSession_High extends RMIPSession_High implements Session_High {

    byte[] key;

    /**
     * @param key the target object key.
     * @param lower      the lower protocol session.
     */

    public ClientSession_High (final byte[] key, final Session_High lower) {
      super(lower);
      this.key = key;
    }

    public ReplyInterface prepareInvocation (final Marshaller marshaller)
      throws JonathanException
    {
      ReplyHolder reply = registerHolder(lower);
      marshaller.writeInt(reply.id);
      int len = key.length;
      marshaller.writeInt(len);
      marshaller.writeByteArray(key, 0, len);
      return reply;
    }

    public void prepare (final Marshaller marshaller) throws JonathanException {
      marshaller.writeInt(0);
      int len = key.length;
      marshaller.writeInt(len);
      marshaller.writeByteArray(key, 0, len);
    }
  }

  class ServerSession_High extends RMIPSession_High implements ReplySession {

    int rqId;

    public ServerSession_High (final Session_High lower, final int rqId) {
      super(lower);
      this.rqId = rqId;
    }

    public Marshaller prepareReply () throws JonathanException {
      return prepareReplyMessage(rqId, false);
    }

    public Marshaller prepareExceptionReply () throws JonathanException {
      return prepareReplyMessage(rqId, true);
    }

    public Marshaller prepareSystemExceptionReply () throws JonathanException {
      return prepareReplyMessage(rqId, true);
    }

    public Marshaller prepareLocationForwardReply () throws JonathanException {
      return prepareReplyMessage(rqId, true);
    }
  }

  // utility methods ----------------------------------------------------------

  void sendMessage (final Marshaller marshaller, final Session_High lower)
    throws JonathanException
  {
    Chunk first = marshaller.getState();
    Chunk c = first;
    int size = 0;
    while (c != null) {
      size += c.top - c.offset;
      c = c.next;
    }
    if (lower.direct()) {
      lower.send(marshaller);
    } else {
      Marshaller m = marshallerFactory.newMarshaller();
      lower.prepare(m);
      m.write(marshaller.getState());
      marshaller.reset();
      lower.send(m);
    }
  }

  Marshaller prepareReplyMessage (final int rqId, final boolean isException)
    throws JonathanException
  {
    Marshaller marshaller = marshallerFactory.newMarshaller();
    marshaller.writeInt(rqId);
    marshaller.writeBoolean(isException);
    return marshaller;
  }

  synchronized void forwardException (
    final JonathanException e,
    final Session_High lower)
  {
    int len = table.length;
    ReplyHolder holder;
    for (int i = 0; i < len; i++) {
      holder = table[i];
      while (holder != null) {
        if (holder.lower == lower) {
          holder.sendReply(e);
        }
        holder = holder.next;
      }
    }
  }

  // --------------------------------------------------------------------------
  // Reply holders management
  // --------------------------------------------------------------------------

  /**
   * Returns the holder identified by id, or null if none exists.
   * @param id a holder identifier.
   * @return the corresponding holder.
   */

  synchronized ReplyHolder getHolder (final int id) {
    int index = (id & 0x7FFFFFFF) % table.length;
    ReplyHolder holder = table[index];
    while (! (holder == null || holder.id == id)) {
      holder = holder.next;
    }
    return holder;
  }

  synchronized ReplyHolder registerHolder (final Session_High lower) {
    ReplyHolder holder;
    if (reusable == null) {
      holder = new ReplyHolder(lower);
      id++;
      holder.id = id;
    } else {
      holder = reusable;
      holder.lower = lower;
      reusable = reusable.next;
    }
    int len = table.length;
    int index = (holder.id & 0x7FFFFFFF) % len;
    holder.next = table[index];
    table[index] = holder;
    size++;
    if (size > len / 2) {
      rehash(len);
    }
    return holder;
  }

  /**
   * Removes the holder identified by the provided id from the table.
   * @param id the holder identifier.
   */

  synchronized void removeHolder (final int id) {
    int index = (id & 0x7FFFFFFF) % table.length;
    ReplyHolder first = table[index];
    ReplyHolder holder = first;
    ReplyHolder prev = null;
    while (holder.id != id) {
      prev = holder;
      holder = holder.next;
    }
    if (holder != null) {
      size--;
      if (prev != null) {
        prev.next = holder.next;
      } else {
        table[index] = holder.next;
      }
      holder.next = reusable;
      holder.lower = null;
      reusable = holder;
    }
  }

  void rehash (final int len) {
    int newLen = 2 * len + 1;
    int index;
    ReplyHolder holder, next_holder;
    ReplyHolder[] newTable = new ReplyHolder[newLen];
    for (int i = 0; i < len; i++) {
      holder = table[i];
      while (holder != null) {
        next_holder = holder.next;
        // rehashing
        index = (holder.id & 0x7FFFFFFF) % newLen;
        holder.next = newTable[index];
        newTable[index] = holder;

        holder = next_holder;
      }
    }
    table = newTable;
  }

  class ReplyHolder implements ReplyInterface {

    Object reply;
    int id;
    ReplyHolder next;
    Session_High lower;

    public ReplyHolder (final Session_High lower) {
      super();
      this.lower = lower;
    }

    public synchronized UnMarshaller listen () throws JonathanException {
      try {
        while (reply == null) {
          scheduler.wait(this);
        }
        UnMarshaller message = (UnMarshaller)reply;
        boolean isException = message.readBoolean();
        if (isException) {
          throw new ServerException(message);
        } else {
          return message;
        }
      } catch (InterruptedException e) {
        throw new JonathanException(e);
      } catch (ClassCastException e) {
        throw (JonathanException)reply;
      } finally {
        reply = null;
        removeHolder(id);
      }
    }

    public synchronized boolean available () {
      return reply != null;
    }

    final synchronized void sendReply (final Object reply) {
      this.reply = reply;
      scheduler.notify(this);
    }
  }
}
