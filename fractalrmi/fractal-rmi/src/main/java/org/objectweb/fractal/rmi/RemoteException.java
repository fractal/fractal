/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.rmi;

/**
 * Thrown when a remote method invocation fails for a "non functional" reason.
 * This exception is a runtime exception, so that users do not need to declare
 * it in every method of every Fractal interface that can be accessed remotely
 * (such as the interfaces of the Fractal API).
 */

public class RemoteException extends RuntimeException {

  /**
   * The exception that caused the remote method invocation failure.
   */

  public final Exception exception;

  /**
   * Constructs a new {@link RemoteException}.
   *
   * @param msg a detail message.
   * @param exception the exception that caused the method invocation failure.
   */

  public RemoteException (final String msg, final Exception exception) {
    super(msg + ": " + exception);
    this.exception = exception;
  }
}
