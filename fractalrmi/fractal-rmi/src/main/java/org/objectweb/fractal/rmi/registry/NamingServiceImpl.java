/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Bruno Dillenseger
 */

package org.objectweb.fractal.rmi.registry;

import org.objectweb.fractal.api.Component;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Vanilla implementation of a naming service component.
 * Persistence is not implemented.
 */

public class NamingServiceImpl
  implements NamingService, NamingServiceAttributes
{

  static final String defaultPersistenceFile = "namingServiceState.bytes";

  private boolean isPersistent = false;

  private String persistenceFileName = null;

  private Map bindings = new HashMap();

  // --------------------------------------------------------------------------
  // Implementation of the NamingService interface
  // --------------------------------------------------------------------------

  public synchronized String[] list () {
    return (String[])bindings.keySet().toArray(new String[bindings.size()]);
  }

  public synchronized Component lookup (final String name) {
    return (Component)bindings.get(name);
  }

  public synchronized boolean bind (final String name, final Component comp) {
    if (! bindings.containsKey(name)) {
      bindings.put(name, comp);
      return true;
    } else {
      return false;
    }
  }

  public synchronized Component rebind (final String name, final Component comp) {
    Component old = (Component)bindings.remove(name);
    bindings.put(name, comp);
    return old;
  }

  public synchronized Component unbind (final String name) {
    return (Component)bindings.remove(name);
  }

  // --------------------------------------------------------------------------
  // Implementation of the NamingServiceAttributes interface
  // --------------------------------------------------------------------------

  public boolean getPersistenceMode () {
    return isPersistent;
  }

  public synchronized void setPersistenceMode (final boolean on) {
    if (on) {
      if (!isPersistent) {
        isPersistent = true;
        if (persistenceFileName == null) {
          persistenceFileName = defaultPersistenceFile;
        }
      }
    }
    else if (isPersistent) {
      isPersistent = false;
      new File(persistenceFileName).delete();
    }
  }

  public String getPersistenceFileName () {
    //System.out.println("getPersistenceFile");
    return persistenceFileName;
  }

  public synchronized void setPersistenceFileName (final String fileName) {
    //System.out.println("setPersistenceFile");
    if (isPersistent && !fileName.equals(persistenceFileName)) {
      new File(persistenceFileName).renameTo(new File(fileName));
    }
    persistenceFileName = fileName;
  }
}
