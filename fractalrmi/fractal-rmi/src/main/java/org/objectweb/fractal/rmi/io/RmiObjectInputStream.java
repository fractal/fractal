/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2003-2007 France Telecom R&D
 * Copyright (c) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributor: Philippe Merle
 *
 * adapted from Jonathan:
 *   org.objectweb.jeremie.libs.presentation.StdMarshallerFactory
 *     (authors: B. Dumant, K. Milsted)
 *
 * Philippe Merle removed the dependency to Java RMI ClassLoader.
 *
 * $Id$
 */

package org.objectweb.fractal.rmi.io;

import org.objectweb.jonathan.apis.binding.Identifier;
import org.objectweb.jonathan.apis.binding.NamingContext;
import org.objectweb.jonathan.apis.kernel.Context;
import org.objectweb.jonathan.apis.kernel.ContextFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;

/**
 * An {@link ObjectInputStream} that replaces {@link Ref} objects with stubs.
 */

public class RmiObjectInputStream extends ObjectInputStream {

  /**
   * The naming context used to decode the identifiers contained in {@link Ref}
   * objects.
   */

  protected NamingContext domain;

  /**
   * The context factory used to create hints for the {@link Identifier#bind
   * bind} method.
   */

  protected ContextFactory contextFactory;

  /**
   * The code base to be used to load classes.
   */
  
  protected String codeBase;
  
  /**
   * Constructs a new {@link RmiObjectInputStream}.
   *
   * @param is the underlying input stream.
   * @param domain the naming context to be used to decode the identifiers
   *      contained in {@link Ref} objects.
   * @param contextFactory the context factory to be used to create hints for
   *      the {@link Identifier#bind bind} method.
   * @throws IOException if the super constructor throws an exception.
   */
  
  public RmiObjectInputStream (
    final InputStream is,
    final NamingContext domain,
    final ContextFactory contextFactory) throws IOException
  {
    super(is);
    enableResolveObject(true);
    this.domain = domain;
    this.contextFactory = contextFactory;
    this.codeBase = readUTF();
    
    ClassLoader l;
    if (codeBase.length() == 0) {
      l = getClass().getClassLoader();
    } else {
      l = RmiClassLoader.getClassLoader(codeBase);
    }
    if (l != null) {
      Thread.currentThread().setContextClassLoader(l);
    }
  }

  /**
   * Replaces {@link Ref} objects with corresponding stubs. This method uses
   * the {@link #domain domain} to decode the identifier encoded in a {@link
   * Ref} object, and then uses the {@link Identifier#bind bind} method of the
   * decoded identifier to get a stub corresponding to this identifier. The
   * type stored in the {@link Ref} object is passed as an hint to the
   * {@link Identifier#bind bind} method.
   *
   * @param obj an object.
   * @return a stub if obj is a {@link Ref} object, or <tt>obj</tt>
   *      otherwise.
   * @throws IOException if a {@link Ref} object cannot be replaced with a
   *      stub.
   */

  protected Object resolveObject (final Object obj) throws IOException {
    if (obj instanceof Ref) {
      try {        
        Ref ref = (Ref)obj;
        Identifier id = domain.decode(ref.id, 0, ref.id.length);
        Context hints = contextFactory.newContext();
        hints.addElement("interface_type", String.class, ref.type, (char)0);
        Object newObj = id.bind(new Identifier[] {id}, hints);
        hints.release();
        return newObj;
      } catch (Exception e) {
        throw new IOException("cannot bind to object: " + e);
      }
    } else {
      return obj;
    }
  }

  protected Class resolveClass (ObjectStreamClass desc)
    throws IOException, ClassNotFoundException
  {
    try {
      try {
        return RmiClassLoader.loadClass((String)codeBase, desc.getName());
      } catch (ClassNotFoundException e) {
        return getClass().getClassLoader().loadClass(desc.getName());
      }
    } catch (ClassNotFoundException e) {
      System.err.println("WARNING: " + e.toString());
      throw e;
    } catch (IOException e) {
      System.err.println("WARNING: " + e.toString());
      throw e;
    }
  }
}
