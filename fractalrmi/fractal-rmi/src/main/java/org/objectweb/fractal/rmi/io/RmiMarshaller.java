/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 *
 * adapted from Jonathan:
 *   org.objectweb.jeremie.libs.presentation.StdMarshallerFactory
 *     (authors: B. Dumant, K. Milsted)
 * with some comments copied from:
 *   org.objectweb.jonathan.apis.presentation.Marshaller
 *     (author: B. Dumant)
 */

package org.objectweb.fractal.rmi.io;

import org.objectweb.jonathan.apis.binding.NamingContext;
import org.objectweb.jonathan.apis.kernel.Context;
import org.objectweb.jonathan.apis.kernel.ContextFactory;
import org.objectweb.jonathan.apis.kernel.InternalException;
import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.presentation.Marshaller;
import org.objectweb.jonathan.apis.resources.Chunk;
import org.objectweb.jonathan.apis.resources.ChunkFactory;

import java.io.IOException;
import java.io.OutputStream;

/**
 * A {@link Marshaller} that uses an {@link RmiObjectOutputStream} to
 * marshall object values.
 */

public class RmiMarshaller extends OutputStream implements Marshaller {

  /**
   * The domain used to instantiate {@link #os os}.
   */

  protected NamingContext domain;

  /**
   * The chunk factory used to create new chunks to marshall data.
   */

  protected ChunkFactory chunkFactory;

  /**
   * The context factory used to create the context associated with this
   * marshaller. See {@link #getContext getContext}.
   */

  protected ContextFactory contextFactory;

  /**
   * The object output stream used to marshall object values. This stream is
   * lazily instantiated, i.e., it is instantiated at the first call to {@link
   * #writeValue writeValue} or to {@link #writeReference writeReference}.
   */

  protected RmiObjectOutputStream os;

  private Chunk first;

  private Chunk current;

  private int offset;

  private int top;

  private Context context;

  // --------------------------------------------------------------------------
  // Constructors
  // --------------------------------------------------------------------------

  /**
   * Constructs a new {@link RmiMarshaller}.
   *
   * @param domain the domain to be used to instantiate {@link #os os}.
   * @param chunkFactory the chunk factory to be used to create new chunks to
   *      marshall data.
   * @param contextFactory the context factory to be used to create the context
   *      associated with this marshaller.
   */

  public RmiMarshaller (
    final NamingContext domain,
    final ChunkFactory chunkFactory,
    final ContextFactory contextFactory)
  {
    super();
    this.domain = domain;
    this.chunkFactory = chunkFactory;
    this.contextFactory = contextFactory;
    first = chunkFactory.newChunk();
    current = first;
    top = current.data.length;
  }

  // --------------------------------------------------------------------------
  // Implementation of the Marshaller interface
  // --------------------------------------------------------------------------

  /**
   * Returns true if this marshaller is little-endian, false otherwise.
   *
   * @return true if this marshaller is little-endian, false otherwise.
   */

  public boolean isLittleEndian () {
    return false;
  }

  /**
   * Returns a {@link Context} associated with this marshaller.
   *
   * @return a {@link Context} associated with this marshaller.
   */

  public Context getContext () {
    if (context == null) {
      try {
        context = contextFactory.newContext();
      } catch (NullPointerException e) {
        e.printStackTrace(System.err);
        throw new InternalException("Context factory required.");
      }
    }
    return context;
  }

  /**
   * Returns the state of the message as a (chain of) chunk(s). The returned
   * chunk(s) are NOT duplicated. If the caller keeps a reference to them, it
   * must {@link #reset() reset} the message, and not continue to use it.
   *
   * @return the state of the message as a (chain of) chunk(s).
   */

  public Chunk getState () {
    if (current.next == null) {
      current.top = offset;
    }
    return first;
  }

  /**
   * Returns the position in the message at which the next byte will be written.
   *
   * @return the current offset in the message.
   * @see #setOffset
   */

  public int getOffset () {
    Chunk c = first;
    int size = 0;
    while (c != current) {
      size += c.top - c.offset;
      c = c.next;
    }
    return size + offset - current.offset;
  }

  /**
   * Sets the offset in the message. This method may be used to override data
   * already written into the message.
   *
   * @param off the new offset.
   * @see #getOffset
   */

  public void setOffset (int off) {
    if (current.next == null && offset > current.top) {
      current.top = offset;
    }
    Chunk c = first;
    int sz = c.top - c.offset;
    while (off > sz) {
      off -= sz;
      c = c.next;
      sz = c.top - c.offset;
    }
    offset = c.offset + off;
    current = c;
    if (current.next == null) {
      top = current.data.length;
    } else {
      top = current.top;
    }
  }

  /**
   * Checks if the given marshaller has the same content as this one.
   *
   * @param other a marshaller.
   * @return true if the this marshaller and the given one have the same
   *      contents, i.e., contain the same bits, false otherwise.
   */

  public boolean sameContents (final Marshaller other) {
    if (other == null) {
      return false;
    } else {
      Chunk mine = getState(), yours = other.getState();
      if (mine == yours) {
        return true;
      }

      byte[] my_data = null, your_data = null;
      int my_top = 0, my_offset = 0, your_top = 0, your_offset = 0;
      if (mine != null) {
        my_data = mine.data;
        my_top = mine.top;
        my_offset = mine.offset;
      }
      if (yours != null) {
        your_data = yours.data;
        your_top = yours.top;
        your_offset = yours.offset;
      }
      while (mine != null && yours != null) {
        if (your_top - your_offset > my_top - my_offset) {
          while (my_offset < my_top &&
            my_data[my_offset] == your_data[your_offset++]) {
            my_offset++;
          }
          if (my_offset < my_top) {
            return false;
          } else {
            mine = mine.next;
            if (mine != null) {
              my_data = mine.data;
              my_top = mine.top;
              my_offset = mine.offset;
            }
          }
        } else {
          while (your_offset < your_top &&
            my_data[my_offset++] == your_data[your_offset]) {
            your_offset++;
          }
          if (your_offset < your_top) {
            return false;
          } else {
            yours = yours.next;
            if (yours != null) {
              your_data = yours.data;
              your_top = yours.top;
              your_offset = yours.offset;
            }
          }
        }
      }
      if (yours == null) {
        if (mine != null && mine.top == my_offset) {
          mine = mine.next;
        }
        while (mine != null && mine.top == mine.offset) {
          mine = mine.next;
        }
        return mine == null;
      } else {
        if (yours.top == your_offset) {
          yours = yours.next;
        }
        while (yours != null && yours.top == yours.offset) {
          yours = yours.next;
        }
        return yours == null;
      }
    }
  }

  /**
   * Resets this marshaller. This method causes the message to lose all its
   * references to the underlying chunks, without {@link Chunk#release()
   * releasing} them. This method must not be used if no reference to chunks
   * present in the message is held by an entity in charge of their release.
   * It also releases the context associated with the target marshaller.
   */

  public void reset () {
    first = null;
    current = null;
    offset = 0;
    top = 0;
    context = null;
  }

  /**
   * Returns an output stream to write into the message. Closing the output
   * stream has the same effect as closing the marshaller itself.
   *
   * @return an output stream to write into the message.
   */

  public OutputStream outputStream () {
    return this;
  }

  /**
   * Writes a byte.
   *
   * @param v a byte.
   * @throws JonathanException if a marshal error occurred.
   */

  public void writeByte (final byte v) throws JonathanException {
    if (offset >= top) {
      prepare();
    }
    current.data[offset++] = v;
  }

  /**
   * Writes a boolean.
   *
   * @param v a boolean.
   * @throws JonathanException if a marshal error occurred.
   */

  public void writeBoolean (final boolean v) throws JonathanException {
    if (offset >= top) {
      prepare();
    }
    current.data[offset++] = (byte)(v ? 1 : 0);
  }

  /**
   * Writes an 8 bits char. The method used to translate the provided
   * <tt>char</tt> into an 8 bits entity is not specified.
   *
   * @param v a char.
   * @throws JonathanException if a marshal error occurred.
   */

  public void writeChar8 (final char v) throws JonathanException {
    if (offset >= top) {
      prepare();
    }
    current.data[offset++] = (byte)v;
  }

  /**
   * Writes a 16 bits char.
   *
   * @param v a char.
   * @throws JonathanException if a marshal error occurred.
   */

  public void writeChar16 (final char v) throws JonathanException {
    if (top - offset < 2) {
      prepare();
    }
    byte[] data = current.data;
    data[offset++] = (byte)(v >>> 8);
    data[offset++] = (byte)v;
  }

  /**
   * Writes a short.
   *
   * @param v a short.
   * @throws JonathanException if a marshal error occurred.
   */

  public void writeShort (final short v) throws JonathanException {
    if (top - offset < 2) {
      prepare();
    }
    byte[] data = current.data;
    data[offset++] = (byte)(v >>> 8);
    data[offset++] = (byte)v;
  }

  /**
   * Writes an int.
   *
   * @param v an int;
   * @throws JonathanException if a marshal error occurred.
   */

  public void writeInt (final int v) throws JonathanException {
    if (top - offset < 4) {
      prepare();
    }
    byte[] data = current.data;
    data[offset++] = (byte)(v >>> 24);
    data[offset++] = (byte)(v >>> 16);
    data[offset++] = (byte)(v >>> 8);
    data[offset++] = (byte)v;
  }

  /**
   * Writes a float.
   *
   * @param v a float.
   * @throws JonathanException if a marshal error occurred.
   */

  public void writeFloat (final float v) throws JonathanException {
    int i = Float.floatToIntBits(v);
    if (top - offset < 4) {
      prepare();
    }
    byte[] data = current.data;
    data[offset++] = (byte)(i >>> 24);
    data[offset++] = (byte)(i >>> 16);
    data[offset++] = (byte)(i >>> 8);
    data[offset++] = (byte)i;
  }

  /**
   * Writes a long.
   *
   * @param v a long.
   * @throws JonathanException if a marshal error occurred.
   */

  public void writeLong (final long v) throws JonathanException {
    if (top - offset < 8) {
      prepare();
    }
    byte[] data = current.data;
    data[offset++] = (byte)(v >>> 56);
    data[offset++] = (byte)(v >>> 48);
    data[offset++] = (byte)(v >>> 40);
    data[offset++] = (byte)(v >>> 32);
    data[offset++] = (byte)(v >>> 24);
    data[offset++] = (byte)(v >>> 16);
    data[offset++] = (byte)(v >>> 8);
    data[offset++] = (byte)v;
  }

  /**
   * Writes a double.
   *
   * @param v a double.
   * @throws JonathanException if a marshal error occurred.
   */

  public void writeDouble (final double v) throws JonathanException {
    long l = Double.doubleToLongBits(v);
    if (top - offset < 8) {
      prepare();
    }
    byte[] data = current.data;
    data[offset++] = (byte)(l >>> 56);
    data[offset++] = (byte)(l >>> 48);
    data[offset++] = (byte)(l >>> 40);
    data[offset++] = (byte)(l >>> 32);
    data[offset++] = (byte)(l >>> 24);
    data[offset++] = (byte)(l >>> 16);
    data[offset++] = (byte)(l >>> 8);
    data[offset++] = (byte)l;
  }

  /**
   * Writes a string of 8 bits chars.
   *
   * @param v a string.
   * @throws JonathanException if a marshal error occurred.
   */

  public void writeString8 (final String v) throws JonathanException {
    int len = v.length();
    writeInt(len + 1);
    byte[] data = current.data;
    int off = 0;
    int max = top - offset;
    while (max < len) {
      while (off < max) {
        data[offset++] = (byte)(v.charAt(off++));
      }
      prepare();
      data = current.data;
      max = top - offset + off;
    }
    while (off < len) {
      data[offset++] = (byte)(v.charAt(off++));
    }
    if (offset >= top) {
      prepare();
    }
    current.data[offset++] = (byte)0;
  }

  /**
   * Writes a string of 16  bits chars.
   *
   * @param v a string.
   * @throws JonathanException if a marshal error occurred.
   */

  public void writeString16 (final String v) throws JonathanException {
    throw new InternalException("Not implemented");
  }

  /**
   * Writes a chunk in the message.
   * The target becomes the "owner" of the provided chunk, and therefore is not
   * supposed to duplicate it. If the entity invoking this operation wants to
   * keep a reference to the chunk, it must be duplicated.
   *
   * @param chunk the chunk to be written.
   */

  public void write (final Chunk chunk) {
    if (current.next != null) {
      throw new InternalException("Illicit operation");
    }
    current.top = offset;
    current.next = chunk;
    Chunk c = chunk;
    while (c != null) {
      current = c;
      c = c.next;
    }
    offset = current.top;
    top = current.data.length;
  }

  /**
   * Writes an array of bytes.
   *
   * @param array an array of bytes.
   * @param off index of the first byte of the array that must be written.
   * @param len number of bytes of the array that must be written.
   * @throws JonathanException if a marshal error occurred.
   */

  public void writeByteArray (final byte[] array, int off, int len)
    throws JonathanException
  {
    int max = top - offset;
    while (max < len) {
      if (max > 0) {
        System.arraycopy(array, off, current.data, offset, max);
        off += max;
        len -= max;
        offset = top;
      }
      prepare();
      max = top - offset;
    }
    System.arraycopy(array,off,current.data,offset,len);
    offset += len;
  }

  /**
   * Writes an object reference in the marshaller.
   *
   * @param v an object reference.
   * @throws JonathanException if a marshal error occurred.
   */

  public void writeReference (final Object v) throws JonathanException {
    try {
      initObjectStream();
      os.writeObject(v);
      os.drain();
    } catch (IOException e) {
      throw new JonathanException(e);
    }
  }

  /**
   * Writes a value in the marshaller.
   *
   * @param v an object.
   * @throws JonathanException if a marshal error occurred.
   */

  public void writeValue (final Object v) throws JonathanException {
    try {
      initObjectStream();
      os.writeObject(v);
      os.drain();
    } catch (IOException e) {
      throw new JonathanException(e);
    }
  }

  // --------------------------------------------------------------------------
  // Overriden methods
  // --------------------------------------------------------------------------

  public void write (final int b) throws IOException {
    try {
      if (top <= offset) {
        prepare();
      }
      current.data[offset++] = (byte)b;
    } catch (JonathanException e) {
      throw new IOException(e.getMessage());
    }
  }

  public void write (final byte[] b, final int off, final int len)
    throws IOException
  {
    try {
      writeByteArray(b, off, len);
    } catch (JonathanException e) {
      throw new IOException(e.getMessage());
    }
  }

  public void close () {
    Chunk cur = first;
    Chunk next;
    while (cur != null) {
      next = cur.next;
      cur.release();
      cur = next;
    }
    first = null;
    current = null;
    offset = 0;
    top  = 0;
    if (context != null) {
      context.release();
      context = null;
    }
  }

  // --------------------------------------------------------------------------
  // Utility methods
  // --------------------------------------------------------------------------

  /**
   * Instantiates the delegate object output stream {@link #os os}. This
   * method creates {@link RmiObjectOutputStream} stream.
   *
   * @throws IOException if the delegate object output stream cannot be created.
   */

  protected void initObjectStream () throws IOException {
    if (os == null) {
      os = new RmiObjectOutputStream(this, domain);
    }
  }

  private void prepare () throws JonathanException {
    if (current == null) {
      Chunk chunk = chunkFactory.newChunk();
      first = chunk;
      current = chunk;
      offset = 0;
      top = current.data.length;
    } else if (current.next == null) {
      current.top = offset;
      Chunk chunk = chunkFactory.newChunk();
      current.next = chunk;
      current = chunk;
      offset = 0;
      top = current.data.length;
    } else {
      current = current.next;
      offset = current.offset;
      if (current.next != null) {
        top = current.top;
      } else {
        top = current.data.length;
      }
    }
  }
}
