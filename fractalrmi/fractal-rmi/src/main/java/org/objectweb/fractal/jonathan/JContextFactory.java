/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.jonathan;

import org.objectweb.jonathan.apis.kernel.Component;
import org.objectweb.jonathan.apis.kernel.Context;
import org.objectweb.jonathan.apis.kernel.ContextFactory;
import org.objectweb.jonathan.apis.kernel.Element;
import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.kernel.Name;

import java.util.Enumeration;
import java.util.HashMap;

/**
 * A minimal and Fractal compliant {@link ContextFactory} implementation.
 */

public class JContextFactory implements ContextFactory {

  public Context newContext () {
    return new JContext();
  }

  public Context newContext (Context context) {
    return new JContext();
  }

  /**
   * A minimal implementation of the {@link Context} interface.
   */

  static class JContext extends HashMap implements Context {

    public void acquire () {
    }

    public Element addElement (Name name, Class aClass, int i) {
      throw new RuntimeException("Not implemented");
    }

    public Element addElement (Name name, Class aClass, Object o) {
      throw new RuntimeException("Not implemented");
    }

    public Element addElement (Name name, Component component) {
      throw new RuntimeException("Not implemented");
    }

    public Element addElement (String s, Class aClass, int i, char c) {
      put(s, new Integer(i));
      return null;
    }

    public Element addElement (String s, Class aClass, Object o, char c) {
      put(s, o);
      return null;
    }

    public Element addElement (String s, Component component, char c) {
      throw new RuntimeException("Not implemented");
    }

    public Context addOrGetContext (Name name) {
      throw new RuntimeException("Not implemented");
    }

    public Context addOrGetContext (String s, char c) {
      throw new RuntimeException("Not implemented");
    }

    public Component fork () {
      throw new RuntimeException("Not implemented");
    }

    public Component getComponent (Name name) {
      throw new RuntimeException("Not implemented");
    }

    public Component getComponent (String s, char c) {
      throw new RuntimeException("Not implemented");
    }

    public Element getElement (Name name) {
      throw new RuntimeException("Not implemented");
    }

    public Element getElement (String s, char c) {
      throw new RuntimeException("Not implemented");
    }

    public Enumeration getElements () {
      throw new RuntimeException("Not implemented");
    }

    public Object getFactoryValue () throws JonathanException {
      throw new RuntimeException("Not implemented");
    }

    public int getIntValue () {
      throw new RuntimeException("Not implemented");
    }

    public int getIntValue (Name name) {
      throw new RuntimeException("Not implemented");
    }

    public int getIntValue (String s, char c) {
      return ((Integer)get(s)).intValue();
    }

    public Element getReference () {
      throw new RuntimeException("Not implemented");
    }

    public Context getScope () {
      throw new RuntimeException("Not implemented");
    }

    public Class getType () {
      throw new RuntimeException("Not implemented");
    }

    public Object getValue () {
      throw new RuntimeException("Not implemented");
    }

    public Object getValue (Name name) {
      throw new RuntimeException("Not implemented");
    }

    public Object getValue (String s, char c) {
      return get(s);
    }

    public void release () {
    }

    public void reset () {
      throw new RuntimeException("Not implemented");
    }

    public Element setReference (Element element) {
      throw new RuntimeException("Not implemented");
    }

    public Context setScope (Context context) {
      throw new RuntimeException("Not implemented");
    }
  }
}
