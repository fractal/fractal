/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2003 France Telecom R&D
 * Copyright (C) 2006-2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Bruno Dillenseger, Eric Bruneton
 *
 * Contributor: Philippe Merle
 *
 * $Id$
 */

package org.objectweb.fractal.rmi.registry;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;

import org.objectweb.fractal.util.Fractal;

import org.objectweb.fractal.jonathan.JContextFactory;
import org.objectweb.jonathan.apis.binding.Identifier;
import org.objectweb.jonathan.apis.binding.NamingContext;
import org.objectweb.jonathan.apis.kernel.Context;
import org.objectweb.jonathan.apis.kernel.JonathanException;

import java.net.InetAddress;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides static methods to launch and get access to a Fractal registry.
 */

public class Registry {

  /**
   * The default port used to export the naming service interface.
   */

  public final static int DEFAULT_PORT = 1234;

  /**
   * The URI scheme for identifying Fractal RMI registries.
   */

  public final static String URI_SCHEME = "frmi";

  /**
   * Private constructor (uninstantiable class).
   */

  private Registry () {
  }

  /**
   * Launches a Fractal registry on the local host.
   *
   * @param args a single argument setting the port number where to pick the
   *      naming service interface.
   * @throws Exception if something goes wrong.
   */

  public static void main (final String[] args)
    throws Exception
  {
    // port number to export the registry interface
    int port = DEFAULT_PORT;
    if (args.length == 1) {
      try {
        port = Integer.parseInt(args[0]);
      } catch (NumberFormatException nfe) {
      }
    }
    createRegistry(port);
  }

  /**
   * Creates {@link NamingService} component and exports its server interface.
   *
   * @param port the port number to be used to export the naming service
   *      interface.
   * @throws Exception if something goes wrong.
   */

  public static void createRegistry (final int port)
    throws Exception
  {
  	createRegistry(port, null);
  }

  /**
   * Creates {@link NamingService} component, using the provided context/hints,
   * and exports its server interface.
   *
   * @param port the port number to be used to export the naming service
   *      interface.
   * @param hints context/hints used when getting the factory, creating the
   * ORB component and getting the bootstrap component
   * @see FactoryFactory#getFactory(java.lang.String, java.util.Map)
   * @see Factory#newComponent(java.lang.String, java.util.Map)
   * @see Fractal#getBootstrapComponent(java.util.Map)
   * @throws Exception if something goes wrong.
   */

  public static void createRegistry (final int port, Map hints)
    throws Exception
  {
    // classloader hints set-up
    if (hints == null)
    {
      hints = new HashMap();
      hints.put("classloader",
        Thread.currentThread().getContextClassLoader() == null ?
          Registry.class.getClassLoader() : Thread.currentThread().getContextClassLoader());
    }

    // constructs the Fractal RMI binder
    Factory f = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND, hints);
    Component comp = 
      (Component)f.newComponent("org.objectweb.fractal.rmi.ORB", hints);
    Fractal.getLifeCycleController(comp).startFc();
    NamingContext binder = (NamingContext)comp.getFcInterface("context");

    // creates the name service component
    Component boot = Fractal.getBootstrapComponent(hints);
    TypeFactory tf = Fractal.getTypeFactory(boot);
    ComponentType nsType = tf.createFcType(
      new InterfaceType[] {
        tf.createFcItfType(
          NamingService.NAMING_SERVICE,
          "org.objectweb.fractal.rmi.registry.NamingService",
          TypeFactory.SERVER,
          TypeFactory.MANDATORY,
          TypeFactory.SINGLE),
        tf.createFcItfType(
          "attribute-controller",
          "org.objectweb.fractal.rmi.registry.NamingServiceAttributes",
          TypeFactory.SERVER,
          TypeFactory.MANDATORY,
          TypeFactory.SINGLE)});
    GenericFactory cf = Fractal.getGenericFactory(boot);
    Component nsId = cf.newFcInstance(
      nsType,
      "primitive",
      "org.objectweb.fractal.rmi.registry.NamingServiceImpl");
    Fractal.getLifeCycleController(nsId).startFc();

    // exports the NamingService interface of the naming service component
    Context ctxt = new JContextFactory().newContext();
    ctxt.addElement("port", Integer.class, new Integer(port), (char)0);
    ctxt.addElement("key", Integer.class, new Integer(-1), (char)0);
    binder.export(nsId.getFcInterface(NamingService.NAMING_SERVICE), ctxt);

    System.out.println("Fractal registry is ready.");
  }

  /**
   * Returns a reference to the {@link NamingService} on the local host, for the
   * default {@link #DEFAULT_PORT port}.
   *
   * @return a reference to the {@link NamingService} on the local host, for the
   *      default {@link #DEFAULT_PORT port}.
   * @throws Exception if something goes wrong.
   */

  public static NamingService getRegistry ()
    throws Exception
  {
    String host = InetAddress.getLocalHost().getHostName();
    return getRegistry(host);
  }

  /**
   * Returns a reference to the {@link NamingService} on the given host, for the
   * default {@link #DEFAULT_PORT port}.
   *
   * @param host the host where the naming service is located.
   * @return a reference to the {@link NamingService} on the given host, for the
   *      default {@link #DEFAULT_PORT port}.
   * @throws Exception if something goes wrong.
   */

  public static NamingService getRegistry (final String host)
    throws Exception
  {
    return getRegistry(host, DEFAULT_PORT);
  }

  /**
   * Returns a reference to the {@link NamingService} on the given host and
   * port.
   *
   * @param host the host where the naming service is located.
   * @param port the port that was used to export the naming service on the
   *      given host.
   * @return a reference to the {@link NamingService} on the given host and
   *      port.
   * @throws Exception if something goes wrong.
   */

  public static NamingService getRegistry (
    final String host,
    final int port) throws Exception
  {
    // constructs the Fractal RMI binder
    Factory f = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
    Component comp = 
      (Component)f.newComponent("org.objectweb.fractal.rmi.ORB", null);
    Fractal.getLifeCycleController(comp).startFc();
    NamingContext binder = (NamingContext)comp.getFcInterface("context");
    return getRegistry(host, port, binder);
  }

  /**
   * Returns a reference to the {@link NamingService} on the given host, for the
   * default {@link #DEFAULT_PORT port}.
   *
   * @param host the host where the naming service is located.
   * @param binder the binder to be used to create the binding to the naming
   *      service interface.
   * @return a reference to the {@link NamingService} on the given host, for the
   *      default {@link #DEFAULT_PORT port}.
   * @throws JonathanException if something goes wrong.
   */

  public static NamingService getRegistry (
    final String host,
    final NamingContext binder) throws JonathanException
  {
    return getRegistry(host, DEFAULT_PORT, binder);
  }

  /**
   * Returns a reference to the {@link NamingService} on the given host and
   * port.
   *
   * @param host the host where the naming service is located.
   * @param port the port that was used to export the naming service on the
   *      given host.
   * @param binder the binder to be used to create the binding to the naming
   *      service interface.
   * @return a reference to the {@link NamingService} on the given host and
   *      port.
   * @throws JonathanException if something goes wrong.
   */

  public static NamingService getRegistry (
    final String host,
    final int port,
    final NamingContext binder) throws JonathanException
  {
    short len = (short)host.length();
    byte[] b = new byte[len + 10];
    // port
    b[0] = (byte)((port >>> 24) & 0xFF);
    b[1] = (byte)((port >>> 16) & 0xFF);
    b[2] = (byte)((port >>> 8) & 0xFF);
    b[3] = (byte)(port & 0xFF);
    // host
    b[4] = (byte)((len >>> 8) & 0xFF);
    b[5] = (byte)(len & 0xFF);
    for (int i = 0 ; i < len; ++i) {
      b[i + 6] = (byte)host.charAt(i);
    }
    // key
    int key = -1;
    b[len + 6] = (byte)((key >>> 24) & 0xFF);
    b[len + 7] = (byte)((key >>> 16) & 0xFF);
    b[len + 8] = (byte)((key >>> 8) & 0xFF);
    b[len + 9] = (byte)(key & 0xFF);

    Identifier id = binder.decode(b, 0, b.length);

    Context hints = new JContextFactory().newContext();
    hints.addElement(
      "interface_type",
      String.class,
      "org.objectweb.fractal.rmi.registry.NamingService",
      (char)0);

    return (NamingService)id.bind(new Identifier[] {id}, hints);
  }

  /**
   * Creates a Fractal RMI {@link URI} instance for a given address.
   * The address has the following format "host[:port][/path]".
   *
   * @param address the given address.
   * @return a new Fractal RMI {@link URI} instance for the given address.
   * @throws Exception if something goes wrong.
   */

  public static URI newURI (String address)
    throws Exception
  {
      if(address == null) {
        address = InetAddress.getLocalHost().getHostName();
      }
      return new URI(URI_SCHEME + "://" + address);
  }

  /**
   * Returns a reference to the {@link NamingService}
   * identified by a given Fractal RMI {@link URI} instance.
   *
   * @param uri a Fractal RMI {@link URI} instance 
   *            with the following syntax 'frmi://host[:port]'.
   * @return a reference to the {@link NamingService} identified by
   *         the given Fractal RMI {@link URI} instance.
   * @throws Exception if something goes wrong.
   */

  public static NamingService getRegistry (final URI uri)
    throws Exception
  {
    //
    // Checks that the URI is correct.
    //

    // Checks that the URI starts with URI_SCHEME
    if(!URI_SCHEME.equals(uri.getScheme())) {
      throw new Exception("Invalid URI: " + uri + " instead of " +
                          URI_SCHEME + ":" + uri.getSchemeSpecificPart());
    }

    // Checks that the URI has no path
    String path = uri.getPath();
    if((path != null) && !path.equals("")) {
      throw new Exception("Invalid URI: " + uri + " instead of " +
                          URI_SCHEME + "://" + uri.getAuthority());
    }

    //
    // Returns the NamingService according to the given host and port.
    //

    String host = uri.getHost();
    // When no host specified then uses the local host name
    if((host == null) || (host.equals(""))) {
      host = InetAddress.getLocalHost().getHostName();
    }

    int port = uri.getPort();
    // When no port specified then uses DEFAULT_PORT
    if(port == -1)
      port = DEFAULT_PORT;

     return getRegistry(host, port);
  }

  /**
   * Lookups a {@link Component} reference for a given Fractal RMI {@link URI} instance.
   *
   * @param uri a Fractal RMI {@link URI} instance
   *            with the following syntax 'frmi://host[:port]/name'.
   * @return the {@link Component} reference for the given Fractal RMI {@link URI} instance.
   * @throws Exception if something goes wrong.
   */

  public static Component lookup (final URI uri)
    throws Exception
  {
	  return getRegistry(
			     newURI(uri.getHost() + ':' + uri.getPort())
			 ).lookup(uri.getPath().substring(1));
  }

  /**
   * Lookups a {@link Component} reference for a given address.
   *
   * @param address the address with the following syntax host[:port]/name
   * @return the {@link Component} reference for the given address.
   * @throws Exception if something goes wrong.
   */

  public static Component lookup (final String address)
    throws Exception
  {
	  return lookup(newURI(address));
  }
}
