/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 *
 * adapted from Jonathan:
 *   org.objectweb.david.libs.binding.iiop.IIOPBinder (author: B. Dumant)
 * with some comments copied from:
 *   org.objectweb.jonathan.apis.binding.NamingContext (author: B. Dumant)
 */

package org.objectweb.fractal.rmi;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.jonathan.apis.binding.Identifier;
import org.objectweb.jonathan.apis.binding.NamingContext;
import org.objectweb.jonathan.apis.kernel.Context;
import org.objectweb.jonathan.apis.kernel.ContextFactory;
import org.objectweb.jonathan.apis.kernel.JonathanException;
import org.objectweb.jonathan.apis.presentation.Marshaller;
import org.objectweb.jonathan.apis.presentation.UnMarshaller;
import org.objectweb.jonathan.apis.protocols.Protocol;
import org.objectweb.jonathan.apis.protocols.ProtocolGraph;
import org.objectweb.jonathan.apis.protocols.RequestSession;
import org.objectweb.jonathan.apis.protocols.SessionIdentifier;
import org.objectweb.jonathan.apis.stub_factories.StubFactory;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.LoggerFactory;

import java.util.Properties;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides a very simple binder component to create distributed bindings.
 * <p>
 * To export an object, this naming context first exports it through an object
 * adapter, whose role is to assign an object key to this object, and to return
 * it in an {@link Identifier}. This identifier is then encapsulated, together
 * with a host name and a port number, in a new {@link Identifier}, which is
 * returned to the caller.
 * <p>
 * To create a binding to an object identified by such an identifier, this
 * naming context creates a session identifier with the host name, the port
 * number and the object key, by using the transport and invocation protocols.
 * A stub is then created with the session and the object identifiers, by using
 * the stub factory. Finally this stub is returned to the caller.
 */

public class RmiBinder implements NamingContext, BindingController {

  /**
   * The object adapter used to export objects before exporting them with this
   * naming context.
   */

  protected NamingContext adapter;

  /**
   * The factory used to create bindings to the objects identified by the names
   * managed by this context.
   */

  protected StubFactory stubFactory;

  /**
   * The context factory used to create hints for various methods.
   */

  protected ContextFactory contextFactory;

  /**
   * The invocation protocol used to send invocation messages.
   */

  protected Protocol rmi;

  /**
   * The transport protocol used by the invocation protocol to send its
   * messages.
   */

  protected Protocol tcp;

  /**
   * The optional logger factory used to get a logger for this component.
   */

  protected LoggerFactory loggerFactory;

  /**
   * The logger used to log messages. May be <tt>null</tt>.
   */

  protected Logger logger;

  /**
   * A map associating the identifiers of the available sessions to their TCP
   * port number.
   */

  private Map sessionIdentifiers;

  /**
   * Constructs a new {@link RmiBinder}.
   */

  public RmiBinder () {
    sessionIdentifiers = new HashMap();
  }

  // --------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // --------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] {
      "adapter",
      "stub-factory",
      "context-factory",
      "rmi-protocol",
      "tcp-protocol",
      "logger-factory"
    };
  }

  public Object lookupFc (final String clientItfName) {
    if (clientItfName.equals("adapter")) {
      return adapter;
    } else if (clientItfName.equals("stub-factory")) {
      return stubFactory;
    } else if (clientItfName.equals("context-factory")) {
      return contextFactory;
    } else if (clientItfName.equals("rmi-protocol")) {
      return rmi;
    } else if (clientItfName.equals("tcp-protocol")) {
      return tcp;
    } else if (clientItfName.equals("logger-factory")) {
      return loggerFactory;
    }
    return null;
  }

  public void bindFc (final String clientItfName, final Object serverItf) {
    if (clientItfName.equals("adapter")) {
      adapter = (NamingContext)serverItf;
    } else if (clientItfName.equals("stub-factory")) {
      stubFactory = (StubFactory)serverItf;
    } else if (clientItfName.equals("context-factory")) {
      contextFactory = (ContextFactory)serverItf;
    } else if (clientItfName.equals("rmi-protocol")) {
      rmi = (Protocol)serverItf;
    } else if (clientItfName.equals("tcp-protocol")) {
      tcp = (Protocol)serverItf;
    } else if (clientItfName.equals("logger-factory")) {
      loggerFactory = (LoggerFactory)serverItf;
      logger = loggerFactory.getLogger(getClass().getName());
    }
  }

  public void unbindFc (final String clientItfName) {
    if (clientItfName.equals("adapter")) {
      adapter = null;
    } else if (clientItfName.equals("stub-factory")) {
      stubFactory = null;
    } else if (clientItfName.equals("context-factory")) {
      contextFactory = null;
    } else if (clientItfName.equals("rmi-protocol")) {
      rmi = null;
    } else if (clientItfName.equals("tcp-protocol")) {
      tcp = null;
    } else if (clientItfName.equals("logger-factory")) {
      loggerFactory = null;
      logger = null;
    }
  }

  // --------------------------------------------------------------------------
  // Implementation of the NamingContext interface
  // --------------------------------------------------------------------------

  /**
   * Creates a new identifier for the object interface designated by the
   * <tt>obj</tt> parameter. Note that calling the {@link Identifier#resolve()
   * resolve} method on the returned identifier should return <tt>id</tt>.
   *
   * @param obj an object.
   * @param hints additional information.
   * @return an identifier managed by the target naming context.
   * @throws JonathanException if something goes wrong.
   */

  public Identifier export (
    final Object obj,
    final Context hints) throws JonathanException
  {
    int port = 0;
    if (hints != null) {
      Integer i = (Integer)hints.getValue("port", (char)0);
      if (i != null) {
        port = i.intValue();
      }
    }

    SessionIdentifier sid = null;

    if (port == 0) {
      // no port specified -> use the first available session id, if any
      if (sessionIdentifiers.size() > 0) {
        sid = (SessionIdentifier)sessionIdentifiers.values().iterator().next();
      }
    } else {
      // finds the session id corresponding to the specified port
      sid = (SessionIdentifier)sessionIdentifiers.get(new Integer(port));
    }

    if (sid == null) {
      Context phints = contextFactory.newContext();
      phints.addElement("port", Integer.class, new Integer(port), (char)0);
      ProtocolGraph pgraph = rmi.createProtocolGraph(
        new ProtocolGraph[] {
          tcp.createProtocolGraph(new ProtocolGraph[0], phints)
        },
        phints);
      phints.release();
      sid = pgraph.export(null);
      Context ctxt = sid.next()[0].getInfo();
      port = ((Integer)ctxt.getValue("port", (char)0)).intValue();
      sessionIdentifiers.put(new Integer(port), sid);
    }

    Context ctxt = sid.next()[0].getInfo();
    String host = (String)ctxt.getValue("hostname", (char)0);
    port = ((Integer)ctxt.getValue("port", (char)0)).intValue();

    Identifier id = new Id(host, port, adapter.export(obj, hints));
    if (logger != null && logger.isLoggable(BasicLevel.INFO)) {
      logger.log(BasicLevel.INFO, "Object " + obj + " exported with id " + id);
    }
    return id;
  }

  /**
   * Decodes an identifier from a buffer portion. Since identifiers are likely
   * to be transmitted on the net, they may have to be encoded and decoded. The
   * {@link Identifier#encode() encoding} method is borne by the {@link
   * Identifier} interface, but the decoding methods must be borne by
   * each naming context. This method creates an identifier (associated
   * with the target naming context), from the <code>length</code> bytes of
   * <code>data</code> starting at offset <code>offset</code>.
   *
   * @param data the byte array to read the encoded identifier from.
   * @param offset offset of the first byte of the encoding.
   * @param length length of the encoding.
   * @return a decoded identifier.
   * @throws JonathanException if something goes wrong.
   */

  public Identifier decode (
    final byte[] data,
    final int offset,
    final int length) throws JonathanException
  {
    int port = ((data[offset] & 0xFF) << 24) +
               ((data[offset + 1] & 0xFF) << 16) +
               ((data[offset + 2] & 0xFF) << 8) +
               (data[offset + 3] & 0xFF);
    int len =  ((data[offset + 4] & 0xFF) << 8) +
               (data[offset + 5] & 0xFF);
    char[] host = new char[len];
    for (int i = 0; i < len; ++i) {
      host[i] = (char)(data[offset + 6 + i] & 0xFF);
    }
    Identifier next = adapter.decode(data, offset + 6 + len, length - 6 - len);
    return new Id(new String(host), port, next);
  }

  /**
   * Decodes an identifier from the provided unmarshaller.
   *
   * @param u an unmarhaller;
   * @return an identifier managed by the target naming context;
   * @throws JonathanException if something goes wrong.
   */

  public Identifier decode (final UnMarshaller u) throws JonathanException {
    int port = u.readInt();
    int len = u.readShort();
    char[] host = new char[len];
    for (int i = 0; i < len; ++i) {
      host[i] = (char)(u.readByte() & 0xFF);
    }
    Identifier next = adapter.decode(u);
    return new Id(new String(host), port, next);
  }

  // --------------------------------------------------------------------------
  // Utility class
  // --------------------------------------------------------------------------

  class Id implements Identifier {

    public String host;

    public int port;

    public Identifier next;

    public Id (
      final String host,
      final int port,
      final Identifier next)
    {
      this.host = host;
      this.port = port;
      this.next = next;
    }

    public NamingContext getContext () {
      return RmiBinder.this;
    }

    public Object bind (final Identifier[] ref, final Context hints)
      throws JonathanException
    {
      Properties p = new Properties();
      p.put("object_key", next.encode());
      p.put("hostname", host);
      p.put("port", new Integer(port));
      SessionIdentifier sessionId =
        rmi.createSessionIdentifier(p, new SessionIdentifier[] {
          tcp.createSessionIdentifier(p, new SessionIdentifier[] {})
        });
      // local optimization
      if (sessionId.isLocal()) {
        RequestSession session = (RequestSession)next.bind(null, hints);
        Object impl = session.getTarget();
        if (impl != null) {
          return impl;
        }
      }
      Object s = stubFactory.newStub(sessionId, new Identifier[] {this}, hints);
      if (logger != null && logger.isLoggable(BasicLevel.INFO)) {
        logger.log(BasicLevel.INFO, "Stub " + s + " bound to id " + ref[0]);
      }
      return s;
    }

    public void unexport () {
    }

    public boolean isValid () {
      return true;
    }

    public Object resolve () {
      return null;
    }

    public byte[] encode () throws JonathanException {
      short len = (short)host.length();
      byte[] n = next.encode();
      byte[] b = new byte[n.length + 6 + len];
      // port
      b[0] = (byte)((port >>> 24) & 0xFF);
      b[1] = (byte)((port >>> 16) & 0xFF);
      b[2] = (byte)((port >>> 8) & 0xFF);
      b[3] = (byte)(port & 0xFF);
      // host
      b[4] = (byte)((len >>> 8) & 0xFF);
      b[5] = (byte)(len & 0xFF);
      for (int i = 0 ; i < len; ++i) {
        b[i + 6] = (byte)host.charAt(i);
      }
      // next id
      System.arraycopy(n, 0, b, 6 + len, n.length);
      return b;
    }

    public void encode (final Marshaller m) throws JonathanException {
      short len = (short)host.length();
      m.writeInt(port);
      m.writeShort(len);
      for (int i = 0; i < len; ++i) {
        m.writeByte((byte)host.charAt(i));
      }
      next.encode(m);
    }

    public boolean equals (final Object o) {
      if (o instanceof Id) {
        Id id = (Id)o;
        if (host.equals(id.host) && port == id.port) {
          return next.equals(id.next);
        }
      }
      return false;
    }

    public int hashCode () {
      return host.hashCode()*port*(next.hashCode() + 17);
    }

    public String toString () {
      return "Id[" + host + "," + port + "," + next + "]";
    }
  }
}
