/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.jonathan;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.jonathan.apis.kernel.ContextFactory;
import org.objectweb.jonathan.apis.presentation.MarshallerFactory;
import org.objectweb.jonathan.apis.protocols.ip.TcpIpConnectionMgr;
import org.objectweb.jonathan.apis.resources.ChunkFactory;
import org.objectweb.jonathan.apis.resources.Scheduler;

/**
 * A Fractal compliant {@link
 * org.objectweb.jonathan.libs.protocols.tcpip.TcpIpProtocol} subclass.
 */

public class TcpIpProtocol
  extends org.objectweb.jonathan.libs.protocols.tcpip.TcpIpProtocol
  implements BindingController
{

  public TcpIpProtocol () {
    super(null, null, null, null, null);
  }

  public String[] listFc () {
    return new String[] {
      "connection-factory",
      "marshaller-factory",
      "chunk-factory",
      "scheduler",
      "context-factory"
    };
  }

  public Object lookupFc (final String clientItfName) {
    if (clientItfName.equals("connection-factory")) {
      return connection_mgr;
    } else if (clientItfName.equals("marshaller-factory")) {
      return marshaller_factory;
    } else if (clientItfName.equals("chunk-factory")) {
      return chunk_factory;
    } else if (clientItfName.equals("scheduler")) {
      return scheduler;
    } else if (clientItfName.equals("context-factory")) {
      return context_factory;
    }
    return null;
  }

  public void bindFc (final String clientItfName, final Object serverItf) {
    if (clientItfName.equals("connection-factory")) {
      connection_mgr = (TcpIpConnectionMgr)serverItf;
    } else if (clientItfName.equals("marshaller-factory")) {
      marshaller_factory = (MarshallerFactory)serverItf;
    } else if (clientItfName.equals("chunk-factory")) {
      chunk_factory = (ChunkFactory)serverItf;
    } else if (clientItfName.equals("scheduler")) {
      scheduler = (Scheduler)serverItf;
    } else if (clientItfName.equals("context-factory")) {
      context_factory = (ContextFactory)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (clientItfName.equals("connection-factory")) {
      connection_mgr = null;
    } else if (clientItfName.equals("marshaller-factory")) {
      marshaller_factory = null;
    } else if (clientItfName.equals("chunk-factory")) {
      chunk_factory = null;
    } else if (clientItfName.equals("scheduler")) {
      scheduler = null;
    } else if (clientItfName.equals("context-factory")) {
      context_factory = null;
    }
  }
}
