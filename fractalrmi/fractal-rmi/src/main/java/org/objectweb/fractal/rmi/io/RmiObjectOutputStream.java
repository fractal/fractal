/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2003-2007 France Telecom R&D
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributor: Philippe Merle
 *
 * adapted from Jonathan:
 *   org.objectweb.jeremie.libs.presentation.StdMarshallerFactory
 *     (authors: B. Dumant, K. Milsted)
 *
 * Philippe Merle removed the dependency to Java RMI ClassLoader.
 *
 * $Id$
 */

package org.objectweb.fractal.rmi.io;

import org.objectweb.fractal.api.Interface;

import org.objectweb.jonathan.apis.binding.NamingContext;
import org.objectweb.jonathan.apis.binding.Reference;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

/**
 * An {@link ObjectOutputStream} that replaces interface references with {@link
 * Ref} objects.
 */

public class RmiObjectOutputStream extends ObjectOutputStream {

  /**
   * The naming context used to export local interfaces, to make them
   * remotely accessible.
   */

  protected NamingContext domain;

  /**
   * Constructs a new {@link RmiObjectOutputStream}.
   *
   * @param os the underlying output stream.
   * @param domain the naming context to be used to export local interface
   *      references, to make them remotely accessible.
   * @throws IOException if the super constructor throws an exception.
   */

  public RmiObjectOutputStream (
    final OutputStream os,
    final NamingContext domain) throws IOException
  {
    super(os);
    enableReplaceObject(true);
    this.domain = domain;
    String codeBase = System.getProperty("fractal.rmi.codebase");
    writeUTF(codeBase == null ? "" : codeBase);
  }

  /**
   * Replaces component interfaces with {@link Ref} objects. If the given
   * object is an {@link Interface}, two cases are possible. If the object is
   * also a {@link Reference}, then it is replaced with a {@link Ref} object
   * containing an encoded form of the identifier held by the {@link Reference}.
   * If the object is not a {@link Reference}, then it is exported with {@link
   * #domain domain}, and replaced with a {@link Ref} object containing an
   * encoded form of the {@link org.objectweb.jonathan.apis.binding.Identifier}
   * returned by {@link NamingContext#export export}.
   *
   * @param obj an object.
   * @return a {@link Ref} object if <tt>obj</tt> is a component interface,
   *      or <tt>obj</tt> otherwise.
   * @throws IOException if a component interface cannot be replaced with a
   *      {@link Ref} object.
   */

  protected Object replaceObject (final Object obj) throws IOException {
    if (obj instanceof Interface) {
      try {
        Ref ref = new Ref();
        ref.type = obj.getClass().getInterfaces()[0].getName();
        if (obj instanceof Reference) {
          ref.id = ((Reference)obj).getIdentifiers()[0].encode();
        } else {
          ref.id = domain.export(obj, null).encode();
        }
        return ref;
      } catch (Exception e) {
        throw new IOException("Cannot export object: " + e);
      }
    }
    return obj;
  }

  /**
   * Drain any buffered data in this stream.
   * 
   * @throws IOException if an IO exception occurs.
   */

  protected void drain () throws IOException {
    super.drain();
  }
}
