/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.rmi.io;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * A typed encoded {@link org.objectweb.jonathan.apis.binding.Identifier} that
 * can be sent over a network.
 */

public class Ref implements Externalizable {

  /**
   * The fully qualified name of the interface designated by {@link #id id}.
   */

  public String type;

  /**
   * The encoded form of an {@link
   * org.objectweb.jonathan.apis.binding.Identifier} designating a remote
   * interface.
   */

  public byte[] id;

  /**
   * Constructs an uninitialized {@link Ref} object.
   */

  public Ref () {
  }

  public void writeExternal (final ObjectOutput out) throws IOException {
    out.writeUTF(type);
    out.writeInt(id.length);
    out.write(id);
  }

  public void readExternal (final ObjectInput in)
    throws IOException, ClassNotFoundException
  {
    type = in.readUTF();
    id = new byte[in.readInt()];
    in.readFully(id);
  }
}
