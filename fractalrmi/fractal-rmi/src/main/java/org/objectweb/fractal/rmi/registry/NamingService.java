/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Bruno Dillenseger
 */

package org.objectweb.fractal.rmi.registry;

import org.objectweb.fractal.api.Component;

/**
 * Typical naming service interface, inspired from Java RMI Registry's
 * interface. Note: be careful with term "bind", which is not used here as
 * Fractal's concept about linking interfaces, but as the basic naming service
 * concept of associating a name with a server reference.
 */

public interface NamingService {

  /**
   * Interface name for naming service.
   */

  static final public String NAMING_SERVICE = "naming service";

  /**
   * Gets the list of names currently present in the naming service.
   *
   * @return an array of names.
   */

  String[] list ();

  /**
   * Looks for a component.
   *
   * @param name the name to look for.
   * @return the component associated to the given name.
   */

  Component lookup (String name);

  /**
   * Associates the given name with the given component, if and only
   * if the given name is not already in use in the naming service.
   *
   * @param name name to be associated to the component.
   * @param comp component to be associated to the given name.
   * @return <tt>true</tt> if the name has been added to the naming service,
   *      <tt>false</tt> otherwise.
   */

  boolean bind (String name, Component comp);

  /**
   * Associates the given name with the given component, replacing
   * any existing association using the same name.
   *
   * @param name name to be associated to the component.
   * @param comp component to be associated to the given name.
   * @return the component formerly associated to the given name, or
   *      <tt>null</tt> if the given name is new to the naming service.
   */

  Component rebind (String name, Component comp);

  /**
   * Removes a name from the naming service.
   *
   * @param name to remove.
   * @return the component that was associated to the name, or
   *      <tt>null</tt> if the name was not known by the naming service.
   */

  Component unbind (String name);
}
