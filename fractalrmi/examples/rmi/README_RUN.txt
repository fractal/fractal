The example is composed by 3 processes:
1) a Registry
2) a Server
3) a Client

which must be launched in that order. 
To do so, open 3 terminals, and invoke in i-th terminal the i-th command below:

1) mvn -Prun.registry
2) mvn -Prun.server
3) mvn -Prun.client

After it, you can kill manually each of the 3 processes.

