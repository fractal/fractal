/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2001-2007 France Telecom R&D
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Valerio Schiavoni
 *
 * Contributor: Philippe Merle
 *
 * $Id$
 */

package rmi.srv;
import org.objectweb.fractal.rmi.registry.NamingService;
import org.objectweb.fractal.rmi.registry.Registry;

import org.objectweb.fractal.util.Fractal;

/**
 * This class mimics Fractal RMI HelloWorld ServerLauncher original behaviour. The main
 * difference is the key used to register the bootstrap component in the
 * naming service. The key has changed from '<em>server-host</em>' to '<em>fractal-bootstrap</em>'
 * to better reflect the content of that entry in the naming registry.<p>
 * 
 * @author valerio.schiavoni@gmail.com
 *
 */
public class ServerLauncher {

  public static void main (final String[] args) throws Exception {

    NamingService ns = Registry.getRegistry();
    ns.bind("fractal-bootstrap", Fractal.getBootstrapComponent());
    
    System.err.println("Server ready.");
  }
}
