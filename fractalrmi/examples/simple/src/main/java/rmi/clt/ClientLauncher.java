/***
 * Fractal RMI: a binder for remote method calls between Fractal components.
 * Copyright (C) 2001-2007 France Telecom R&D
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@objectweb.org
 *
 * Author: Valerio Schiavoni
 *
 * Contributor: Philippe Merle
 *
 * $Id$
 */

package rmi.clt;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.rmi.registry.NamingService;
import org.objectweb.fractal.rmi.registry.Registry;
import org.objectweb.fractal.util.Fractal;

/**
 * This class mimics Fractal RMI HelloWorld ClientLauncher original behaviour.
 * Main difference:
 * <ol>
 * <li> the key used to register the bootstrap component in the naming service.
 * The key has changed from '<em>server-host</em>' to '<em>fractal-bootstrap</em>'
 * to better reflect the content of that entry in the naming registry;
 * <li> the package name for classes implementing contents of components is now
 * rmi.clt instead of the default package;
 * <li> this launcher doesn't rely on FractalADL;
 * <li> the attribute-controller ServiceAttributes has been removed to further simplify the example:
 * please refer to default Julia HelloWorld example if you're interested in this case;
 * </ol>
 * 
 * @author valerio.schiavoni@gmail.com
 */
public class ClientLauncher {

  public static void main (final String[] args) throws Exception {

    NamingService ns = Registry.getRegistry();

    Component bootstrap = ns.lookup("fractal-bootstrap");
    
    TypeFactory tf = Fractal.getTypeFactory(bootstrap);
    // type of root component
    ComponentType rType = tf.createFcType(new InterfaceType[] {
      tf.createFcItfType("m", "rmi.clt.Main", false, false, false)
    });
    // type of client component
    ComponentType cType = tf.createFcType(new InterfaceType[] {
      tf.createFcItfType("m", "rmi.clt.Main", false, false, false),
      tf.createFcItfType("s", "rmi.clt.Service", true, false, false)
    });
    // type of server component
    ComponentType sType = tf.createFcType(new InterfaceType[] {
      tf.createFcItfType("s", "rmi.clt.Service", false, false, false)
    });

    GenericFactory cf = Fractal.getGenericFactory(bootstrap);

    
    // create root component
    Component rComp = cf.newFcInstance(rType, "composite", null);
    // create client component
    Component cComp = cf.newFcInstance(cType, "primitive", "rmi.clt.ClientImpl");
    // create server component
    Component sComp = cf.newFcInstance(sType, "primitive", "rmi.clt.ServerImpl");

   
    // component assembly
    Fractal.getContentController(rComp).addFcSubComponent(cComp);
    Fractal.getContentController(rComp).addFcSubComponent(sComp);
    Fractal.getBindingController(rComp).bindFc("m", cComp.getFcInterface("m"));
    Fractal.getBindingController(cComp).bindFc("s", sComp.getFcInterface("s"));

    //  start root component
    Fractal.getLifeCycleController(rComp).startFc();

    // call main method
    ((Main)rComp.getFcInterface("m")).main(null);
    
  }
}
