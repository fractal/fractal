This example shows how the Fractal RMI binder can be used to deploy
the Fractal ADL HelloWorld example in a distributed way.

The example is composed by 3 processes:
1) a Registry
2) a Server
3) a Client

which must be launched in that order. 
To do so, open 3 terminals, and invoke in i-th terminal the i-th command below:

1) mvn -Prun.registry
2) mvn -Prun.server
3) mvn -Prun.client       # Launch the HelloWorldRMI definition.
or mvn -Prun.client.stat  # Ditto but report Fractal RMI statistics.
or mvn -Prun.wrapped      # Launch the WrappedHelloWorldRMI definition.
or mvn -Prun.wrapped.stat # Ditto but report Fractal RMI statistics.

After it, you can kill manually each of the 3 processes.
