/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.graph.model.Tools;
import org.objectweb.fractal.gui.graph.view.ComponentPart;
import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.Interface;
import org.objectweb.fractal.gui.model.ServerInterface;

import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;

/**
 * A controller component to create or remove bindings. This controller
 * component modifies a configuration model, in reaction to events emited by a
 * graph view.
 */

public class BindTool extends EmptyGraphViewListener
  implements BindingController
{

  /**
   * A mandatory client interface bound to a {@link Tools tools} model. This
   * model is used to know if this tool is the currently selected tool or not.
   */

  public final static String TOOLS_BINDING = "tools";

  /**
   * The tools client interface.
   */

  private Tools tools;

  /**
   * Indicates if the last mouve event was a mouse dragged event.
   */

  private boolean mouseDragged;

  /**
   * Constructs a new {@link BindTool} component.
   */

  public BindTool () {
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] { TOOLS_BINDING };
  }

  public Object lookupFc (final String clientItfName) {
    if (TOOLS_BINDING.equals(clientItfName)) {
      return tools;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (TOOLS_BINDING.equals(clientItfName)) {
      tools = (Tools)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (TOOLS_BINDING.equals(clientItfName)) {
      tools = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the GraphViewListener interface
  // -------------------------------------------------------------------------

  public void viewChanged () {
    if (tools.getTool() == Tools.BIND) {
      tools.setTool(Tools.SELECT);
      mouseDragged = false;
    }
  }

  public void mousePressed (final MouseEvent e, final ComponentPart p) {
    if (tools.getTool() == Tools.BIND) {
      if (!e.isConsumed()) {
        tools.setTool(Tools.SELECT);
      }
      mouseDragged = false;
    }
  }

  public void mouseReleased (final MouseEvent e, final ComponentPart p) {
    if (tools.getTool() == Tools.BIND) {
      tools.setTool(Tools.SELECT);
      if (mouseDragged) {
        ClientInterface citf = (ClientInterface)tools.getBindInterface();
        Interface sitf = p.getInterface();
        //try {
          if (sitf != null) {
            // bind citf
            if (sitf instanceof ServerInterface) {
              if (citf.getBinding() == null) {
                citf.getOwner().bind(citf, null, (ServerInterface)sitf);
              } else {
                citf.getOwner().rebind(citf, (ServerInterface)sitf);
              }
            } else {
              JOptionPane.showMessageDialog(
                null,
                "Cannot bind a client interface to another client interface",
                "Error",
                JOptionPane.ERROR_MESSAGE);
            }
          } else {
            // unding citf
            if (citf.getBinding() != null) {
              citf.getOwner().unbind(citf);
            }
          }
        /*} catch (IllegalOperationException ioe) {
          JOptionPane.showMessageDialog(
            null, ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }*/
      }
    }
  }

  public void mouseClicked (final MouseEvent e, final ComponentPart p) {
    if (tools.getTool() == Tools.BIND) {
      tools.setTool(Tools.SELECT);
      mouseDragged = false;
    }
  }

  public void mouseDragged (final MouseEvent e) {
    if (tools.getTool() == Tools.BIND) {
      mouseDragged = true;
    }
  }

  public void mouseMoved (final MouseEvent e, final ComponentPart p) {
    if (tools.getTool() == Tools.BIND) {
      // tools.setTool(Tools.SELECT); // TODO bug JDK1.4? Windows?
      // mouseDragged = false;
    }
  }
}
