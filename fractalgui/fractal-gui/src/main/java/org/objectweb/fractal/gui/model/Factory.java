/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.model;

/**
 * A factory to create components and interfaces.
 */

public interface Factory {

  /**
   * Creates a new component.
   *
   * @return a newly created component.
   */

  Component createComponent ();

  /**
   * Creates a slave component.
   *
   * @param component a master component.
   * @return a newly created slave component of the given master component.
   */

  Component createComponent (Component component);

  /**
   * Creates a new external client interface.
   *
   * @param owner a component.
   * @return a newly created external client interface for the given component.
   */

  ClientInterface createClientInterface (Component owner);

  /**
   * Creates a new external server interface.
   *
   * @param owner a component.
   * @return a newly created external server interface for the given component.
   */

  ServerInterface createServerInterface (Component owner);
}
