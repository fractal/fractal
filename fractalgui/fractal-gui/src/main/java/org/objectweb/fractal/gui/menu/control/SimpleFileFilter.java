/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.menu.control;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 * Simple {@link FileFilter} implementation.
 */

public class SimpleFileFilter extends FileFilter {

  /**
   * The extension that files must have in order to be accepted by this filter.
   */

  private String extension;

  /**
   * Short description ot this file filter.
   */

  private String description;

  /**
   * Constructs a new {@link SimpleFileFilter} object.
   *
   * @param extension the extension that files must have in order to be accepted
   *      by this filter.
   * @param description a short description ot this file filter.
   */

	public SimpleFileFilter (final String extension, final String description) {
    this.extension = extension;
    this.description = description;
  }

  public boolean accept (final File f) {
    if (f.isDirectory()) {
      return true;
    } else {
      String name = f.getName();
      int i = name.lastIndexOf('.');
      if (i > 0 && i < name.length() - 1) {
        return name.substring(i + 1).toLowerCase().equals(extension);
      } else {
        return false;
      }
    }
	}

	public String getDescription () {
    return description;
	}
}
