/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.undo.model;

/**
 * A model for undo and redo.
 */

public interface UndoManager {

  /**
   * Returns <tt>true</tt> if there is something that can be undone.
   *
   * @return <tt>true</tt> if there is something that can be undone.
   */

  boolean canUndo ();

  /**
   * Undo the last change that was made.
   */

  void undo ();

  /**
   * Returns <tt>true</tt> if there is something that can be redone.
   *
   * @return <tt>true</tt> if there is something that can be redone.
   */

  boolean canRedo ();

  /**
   * Redo the last change that was undone.
   */

  void redo ();

  /**
   * Clears the state of this model. After a call to this method, {@link
   * #canUndo canUndo} and {@link #canRedo canRedo} return <tt>false</tt>.
   */

  void clear();
}
