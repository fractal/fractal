/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.dialog.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.dialog.model.DialogModel;
import org.objectweb.fractal.gui.dialog.view.DialogViewListener;
import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Configuration;
import org.objectweb.fractal.gui.model.Factory;
import org.objectweb.fractal.gui.model.ServerInterface;
import org.objectweb.fractal.gui.model.IllegalOperationException;
import org.objectweb.fractal.gui.selection.model.Selection;

import javax.swing.JOptionPane;

/**
 * A controller component for dialog view components. This component modifies
 * a dialog model, in reaction to events emitted by a dialog view.
 */

public class BasicDialogController implements
  DialogViewListener,
  BindingController
{

  /**
   * A mandatory client interface bound to a {@link Configuration configuration}
   * model. This model is used to get the component to which interfaces or
   * attributes must be added or removed by this controller (the component used
   * is the root component of the configuration).
   */

  public final static String CONFIGURATION_BINDING = "configuration";

  /**
   * A mandatory client interface bound to a {@link Selection selection}
   * model. This model is used to get the interface that must be removed when
   * a "remove interface" button is clicked.
   */

  public final static String SELECTION_BINDING = "selection";

  /**
   * A mandatory client interface bound to a {@link DialogModel dialog} model.
   * This model is used to get selection model of the attribute table, which is
   * itself used to find the attribute to be removed when the "remove attribute"
   * button is clicked.
   */

  public final static String DIALOG_MODEL_BINDING = "dialog-model";

  /**
   * A mandatory client interface bound to a {@link Factory factory}. This
   * factory is used to create interfaces when a "add interface" button is
   * clicked.
   */

  public final static String FACTORY_BINDING = "configuration-factory";

  /**
   * The configuration client interface.
   */

  protected Configuration configuration;

  /**
   * The selection client interface.
   */

  private Selection selection;

  /**
   * The dialog model client interface.
   */

  protected DialogModel model;

  /**
   * The factory client interface.
   */

  private Factory factory;

  /**
   * Constructs a new {@link BasicDialogController} component.
   */

  public BasicDialogController () {
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] {
      CONFIGURATION_BINDING,
      SELECTION_BINDING,
      DIALOG_MODEL_BINDING,
      FACTORY_BINDING
    };
  }

  public Object lookupFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      return configuration;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      return selection;
    } else if (DIALOG_MODEL_BINDING.equals(clientItfName)) {
      return model;
    } else if (FACTORY_BINDING.equals(clientItfName)) {
      return factory;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = (Configuration) serverItf;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = (Selection)serverItf;
    } else if (DIALOG_MODEL_BINDING.equals(clientItfName)) {
      model = (DialogModel) serverItf;
    } else if (FACTORY_BINDING.equals(clientItfName)) {
      factory = (Factory) serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = null;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = null;
    } else if (DIALOG_MODEL_BINDING.equals(clientItfName)) {
      model = null;
    } else if (FACTORY_BINDING.equals(clientItfName)) {
      factory = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the DialogViewListener interface
  // -------------------------------------------------------------------------

  public void addClientInterfaceButtonClicked () {
    Component component = configuration.getRootComponent();
    ClientInterface i = factory.createClientInterface(component);
    component.addClientInterface(i);
  }

  public void removeClientInterfaceButtonClicked () {
    try {
      ClientInterface i = (ClientInterface)selection.getSelection();
      i.getOwner().removeClientInterface(i);
      selection.clearSelection();
    } catch (IllegalOperationException ioe) {
      JOptionPane.showMessageDialog(
        null, ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }
  }

  public void addServerInterfaceButtonClicked () {
    Component component = configuration.getRootComponent();
    ServerInterface i = factory.createServerInterface(component);
    component.addServerInterface(i);
  }

  public void removeServerInterfaceButtonClicked () {
    try {
      ServerInterface i = (ServerInterface)selection.getSelection();
      i.getOwner().removeServerInterface(i);
      selection.clearSelection();
    } catch (IllegalOperationException ioe) {
      JOptionPane.showMessageDialog(
        null, ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }
  }

  public void addAttributeButtonClicked () {
    Component component = configuration.getRootComponent();
    int index = component.getAttributeNames().size();
    component.setAttribute("attribute " + index, "empty");
  }

  public void removeAttributeButtonClicked () {
    Component component = configuration.getRootComponent();
    int index = model.getAttributesTableSelectionModel().getMinSelectionIndex();
    String name = (String)component.getAttributeNames().get(index);
    component.setAttribute(name, null);
  }
}
