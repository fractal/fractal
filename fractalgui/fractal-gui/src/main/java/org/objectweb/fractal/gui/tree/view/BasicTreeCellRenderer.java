/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.tree.view;

import org.objectweb.fractal.gui.Constants;
import org.objectweb.fractal.gui.model.Component;

import java.awt.Color;
import java.awt.Font;
import java.net.URL;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.ImageIcon;

/**
 * A {@link javax.swing.tree.TreeCellRenderer} to draw component names with a
 * color indicating their type and status. More precisely, the name of a slave
 * component is drawn in italic, the name of invalid component is drawn in
 * {@link Constants#ERROR_COLOR ERROR_COLOR}, and missing components names are
 * drawn as "&lt;missing&gt".
 */

public class BasicTreeCellRenderer extends DefaultTreeCellRenderer {

  /**
   * The font used by the super class to tree cells.
   */

  private Font normalFont;

  public java.awt.Component getTreeCellRendererComponent (
    final JTree tree,
    final Object value,
    final boolean sel,
    final boolean expanded,
    final boolean leaf,
    final int row,
    final boolean hasFocus)
  {
    super.getTreeCellRendererComponent(
      tree, value, sel, expanded, leaf, row, hasFocus);
    if (!(value instanceof Component)) {
      return this;
    }
    if (normalFont == null) {
      normalFont = getFont();
    }
    Component c = (Component)value;
    long status = c.getStatus();
    setForeground (status == 0 ? Color.black : Constants.ERROR_COLOR);
    if ((status & Component.NAME_MISSING) != 0) {
      setText("<missing>");
    } else {
      setText(c.getName());
    }

    Color col = new Color (255, 255, 204);
    tree.setBackground (col);
    setBackgroundNonSelectionColor(col);
    setBackgroundSelectionColor(new Color(255, 204, 102));

    URL url_item = getClass().getResource
      ("/org/objectweb/fractal/gui/resources/tr_item.gif");
		this.setLeafIcon (new ImageIcon(url_item));
    URL url_open = getClass().getResource
      ("/org/objectweb/fractal/gui/resources/tr_open.gif");
    this.setOpenIcon (new ImageIcon (url_open));
    URL url_closed = getClass().getResource
      ("/org/objectweb/fractal/gui/resources/tr_closed.gif");
    this.setClosedIcon (new ImageIcon (url_closed));

    if (c.getMasterComponent() != null) {
      setFont(normalFont.deriveFont(Font.ITALIC));
    } else {
      setFont(normalFont);
    }
    return this;
  }
}
