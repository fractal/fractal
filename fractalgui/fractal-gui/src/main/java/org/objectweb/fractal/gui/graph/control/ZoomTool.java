/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.graph.model.Display;
import org.objectweb.fractal.gui.graph.model.Rect;
import org.objectweb.fractal.gui.graph.model.Tools;
import org.objectweb.fractal.gui.graph.view.ComponentPart;

import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.Cursor;
import javax.swing.JComponent;

/**
 * A controller component to zoom in or out a display. This controller component
 * modifies a display model, in reaction to events emited by a graph view.
 */

public class ZoomTool extends EmptyGraphViewListener
  implements BindingController
{

  /**
   * A mandatory client interface bound to a {@link Tools tools} model. This
   * model is used to know if this tool is the currently selected tool or not.
   */

  public final static String TOOLS_BINDING = "tools";

  /**
   * A mandatory client interface bound to a {@link Display display} model. This
   * model is modified by this controller component when the mouse is clicked.
   */

  public final static String DISPLAY_BINDING = "display";

  /**
   * The tools client interface.
   */

  private Tools tools;

  /**
   * The display client interface.
   */

  private Display display;

  /**
   * Constructs a new {@link ZoomTool} component.
   */

  public ZoomTool () {
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] { DISPLAY_BINDING, TOOLS_BINDING };
  }

  public Object lookupFc (final String clientItfName) {
    if (DISPLAY_BINDING.equals(clientItfName)) {
      return display;
    } else if (TOOLS_BINDING.equals(clientItfName)) {
      return tools;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (DISPLAY_BINDING.equals(clientItfName)) {
      display = (Display)serverItf;
    } else if (TOOLS_BINDING.equals(clientItfName)) {
      tools = (Tools)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (DISPLAY_BINDING.equals(clientItfName)) {
      display = null;
    } else if (TOOLS_BINDING.equals(clientItfName)) {
      tools = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the GraphViewListener interface
  // -------------------------------------------------------------------------

  public void mouseClicked (final MouseEvent e, final ComponentPart p) {
    int tool = tools.getTool();
    if (tool == Tools.ZOOM_IN || tool == Tools.ZOOM_OUT) {
      Rect position = display.getDisplayedArea();
      Rectangle r = display.getScreenSize();
      double X0 = position.x0;
      double Y0 = position.y0;
      double X1 = position.x1;
      double Y1 = position.y1;
      X0 += ((double)(r.width/2 - e.getX()))/r.width;
      Y0 += ((double)(r.height/2 - e.getY()))/r.height;
      X1 += ((double)(r.width/2 - e.getX()))/r.width;
      Y1 += ((double)(r.height/2 - e.getY()))/r.height;
      if (tool == Tools.ZOOM_IN) {
        display.setDisplayedArea(new Rect(
          0.5 + (X0 - 0.5)*1.2,
          0.5 + (Y0 - 0.5)*1.2,
          0.5 + (X1 - 0.5)*1.2,
          0.5 + (Y1 - 0.5)*1.2
        ));
      } else {
        display.setDisplayedArea(new Rect(
          0.5 + (X0 - 0.5)/1.2,
          0.5 + (Y0 - 0.5)/1.2,
          0.5 + (X1 - 0.5)/1.2,
          0.5 + (Y1 - 0.5)/1.2
        ));
      }
    }
  }

  public void mouseMoved (final MouseEvent e, final ComponentPart p) {
    int tool = tools.getTool();
    if (tool == Tools.ZOOM_IN) {
      // TODO zoom in cursor
      ((JComponent)e.getSource()).setCursor(
        Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR));
    } else if (tool == Tools.ZOOM_OUT) {
      // TODO zoom out cursor
      ((JComponent)e.getSource()).setCursor(
        Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));
    }
  }
}
