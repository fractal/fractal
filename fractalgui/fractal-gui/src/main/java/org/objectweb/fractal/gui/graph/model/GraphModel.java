/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.model;

import org.objectweb.fractal.gui.model.Component;

import java.awt.Color;

/**
 * A model associating coordinates to components.
 */

public interface GraphModel {

  /**
   * TODO javadoc.
   * 
   * @param component
   * @return
   */

  Color getComponentColor (Component component);

  /**
   * TODO javadoc.
   *
   * @param component
   * @param color
   */
  void setComponentColor (Component component, Color color);

  /**
   * Returns the coordinates of the given component.
   *
   * @param component the component whose coordinates must be returned.
   * @return the coordinates of the given component. These coordinates are
   *      <i>relative</i> to the parent component of the given component: (0,0)
   *      is the top left corner of the inner area of the parent component, and
   *      (1,1) is the botton right corner of this area.
   * @see #setComponentPosition
   */

  Rect getComponentPosition (Component component);

  /**
   * Sets the coordinates of the given component.
   *
   * @param component the component whose coordinates must be set.
   * @param position the new coordinates of the given component.
   * @see #getComponentPosition
   */

  void setComponentPosition (Component component, Rect position);
}
