/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * An implementation of the {@link ServerInterface} interface for slave
 * components.
 */

public class SharedServerInterface extends SharedInterface
  implements ServerInterface
{

  /**
   * The bindings that refer to this server interface. This list is a list of
   * {@link Binding} objects.
   */

  private List bindings;

  /**
   * Constructs a new server interface for a slave component.
   *
   * @param owner a slave component.
   * @param masterInterface the interface of the master component to which the
   *      constructed interface will correspond.
   */

  SharedServerInterface (
    final SharedComponent owner,
    final Interface masterInterface)
  {
    super(owner, masterInterface);
    bindings = new ArrayList();
  }

  public List getBindings () {
    return Collections.unmodifiableList(bindings);
  }

  /**
   * Adds the given binding to the list of bindings that refer to this
   * interface.
   *
   * @param binding a new binding that refer to this interface.
   */

  void addBinding (final Binding binding) {
    for (int i = 0; i < bindings.size(); ++i) {
      Binding b = (Binding)bindings.get(i);
      if (b.getClientInterface() == binding.getClientInterface() && b.getServerInterface() == binding.getServerInterface()) {
        return;
      }
    }
    bindings.add(binding);
  }

  /**
   * Removes the given binding from the list of bindings that refer to this
   * interface.
   *
   * @param binding a binding that refer to this interface.
   */

  void removeBinding (final Binding binding) {
    bindings.remove(binding);
  }
}
