/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.menu.control;

import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.model.Configuration;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.graph.model.GraphModel;
import org.objectweb.fractal.gui.repository.api.Repository;
import org.objectweb.fractal.gui.repository.api.Storage;
import org.objectweb.fractal.gui.selection.model.Selection;
import org.objectweb.fractal.gui.UserData;
import org.objectweb.fractal.swing.WaitGlassPane;
import org.objectweb.fractal.swing.AbstractAction;

import java.net.URL;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.KeyStroke;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.Action;
import javax.swing.JRootPane;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;

/**
 * An action to open a configuration stored in a repository.
 */

public class OpenAction extends AbstractAction implements
  BindingController, LifeCycleController
{

  /**
   * A mandatory client interface bound to a {@link Configuration configuration}
   * model. This is the configuration into which the opened configurations are
   * loaded.
   */

  public final static String CONFIGURATION_BINDING = "configuration";

  /**
   * An optional client interface bound to a {@link GraphModel graph} model.
   * This is the model into which the opened configuration graphical
   * information are loaded.
   */

  public final static String GRAPH_BINDING = "graph";

  /**
   * A mandatory client interface bound to a {@link Selection selection} model.
   * This model is reinitialized after a configuration has been opened.
   */

  public final static String SELECTION_BINDING = "selection";

  /**
   * A mandatory client interface bound to a {@link Repository repository}. This
   * repository is used to load the configurations stored in the storage.
   */

  public final static String REPOSITORY_BINDING = "repository";

  /**
   * A mandatory client interface bound to a {@link Storage storage}. This is
   * the storage from which the opened configurations are read.
   */

  public final static String STORAGE_BINDING = "storage";

  /**
   * An optional client interface bound to a {@link UserData userdata}. This is
   * the storage into/from which some personal data peculiar to each user are
   * written/read.
   */

  public final static String USER_DATA_BINDING = "user-data";

  /**
   * An optional client interface bound to a save {@link Action action}. This
   * action is used to save the current configuration, before opening a new
   * one.
   */

  public final static String SAVE_ACTION_BINDING = "save-action";

  /**
   * The configuration client interface.
   */

  protected Configuration configuration;

  /**
   * The graph client interface.
   */

  protected GraphModel graph;

  /**
   * The selection client interface.
   */

  protected Selection selection;

  /**
   * The repository client interface.
   */

  protected Repository repository;

  /**
   * The storage client interface.
   */

  protected Storage storage;

  /**
   * The user data client interface.
   */
  protected UserData userData;

  /**
   * The save action client interface.
   */

  protected Action save;

  /**
   * Constructs a new {@link OpenAction} component.
   */

  public OpenAction () {
    putValue(NAME, "Open");
    putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("control O"));
    putValue(SHORT_DESCRIPTION, "Open");
    URL url = getClass().getResource(
      "/org/objectweb/fractal/gui/resources/fileopen.gif");
    putValue(SMALL_ICON, new ImageIcon(url));
  }

  // -------------------------------------------------------------------------
  // Implementation of the UserBindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] {
      CONFIGURATION_BINDING,
      GRAPH_BINDING,
      SELECTION_BINDING,
      REPOSITORY_BINDING,
      STORAGE_BINDING,
      USER_DATA_BINDING,
      SAVE_ACTION_BINDING
    };
  }

  public Object lookupFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      return configuration;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      return graph;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      return selection;
    } else if (REPOSITORY_BINDING.equals(clientItfName)) {
      return repository;
    } else if (STORAGE_BINDING.equals(clientItfName)) {
      return storage;
    } else if (USER_DATA_BINDING.equals(clientItfName)) {
      return userData;
    } else if (SAVE_ACTION_BINDING.equals(clientItfName)) {
      return save;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = (Configuration)serverItf;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      graph = (GraphModel)serverItf;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = (Selection)serverItf;
    } else if (REPOSITORY_BINDING.equals(clientItfName)) {
      repository = (Repository)serverItf;
    } else if (STORAGE_BINDING.equals(clientItfName)) {
      storage = (Storage)serverItf;
    } else if (USER_DATA_BINDING.equals(clientItfName)) {
      userData = (UserData)serverItf;
    } else if (SAVE_ACTION_BINDING.equals(clientItfName)) {
      save = (Action)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = null;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      graph = null;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = null;
    } else if (REPOSITORY_BINDING.equals(clientItfName)) {
      repository = null;
    } else if (STORAGE_BINDING.equals(clientItfName)) {
      storage = null;
    } else if (USER_DATA_BINDING.equals(clientItfName)) {
      userData = null;
    } else if (SAVE_ACTION_BINDING.equals(clientItfName)) {
      save = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the LifeCycleController interface
  // -------------------------------------------------------------------------

  public String getFcState () {
    return null;
  }

  public void startFc () {
    // TODO open last opened file 
    /*String fileName = null;
    try {
      fileName = userData.getStringData(UserData.LAST_OPEN_FILE);
      // Pre-opening of the LAST_OPEN_FILE file stored in order to test its
      // availability and to prevent any dysfunction with storage.
      // (the file can be corrupted consequently to an external event).
      FileInputStream fin = new FileInputStream (fileName);
      fin.close();
    } catch (Exception e) {
      //userData.clean();
      return;
    }
    if (fileName == null) {
      return;
    }
    try {
      storage.open(fileName, false);
      try {
        Component c = repository.loadComponent(
          header.getAttribute("root", null), graph);
        c.setName (header.getAttribute("rootName", "root"));
        userData.setStringData(UserData.LAST_OPEN_CONF, c.getName());
        userData.save();
        configuration.setRootComponent(c);
        selection.selectComponent(c);
      } finally {
        storage.close();
      }
    } catch (Exception ignored) {
      try {
        //userData.clean();
      } catch (RuntimeException ex) {
        //ex.printStackTrace();
      }
    }*/
  }

  public void stopFc () {
  }

  // -------------------------------------------------------------------------
  // Implementation of the ActionListener interface
  // -------------------------------------------------------------------------

  public void actionPerformed (final ActionEvent e) {
    try {
      File storage = null;
      if (configuration.getStorage() != null) {
        storage = new File(configuration.getStorage());
        if (!storage.exists() || !storage.isDirectory()) {
          storage = null;
        }
      }
      if (storage == null) {
        JOptionPane.showMessageDialog(
          null,
          "A storage directory must be selected before files can be opened", 
          "Error",
          JOptionPane.ERROR_MESSAGE);
        return;
      }
      
      if (configuration.getChangeCount() > 0) {
        Object[] options = {"Yes", "No", "Cancel" };
        int n = JOptionPane.showOptionDialog(
          null,
          "Do you want to save the current configuration " +
          "before opening a new one ?",
          "Warning",
          JOptionPane.YES_NO_CANCEL_OPTION,
          JOptionPane.QUESTION_MESSAGE,
          null,
          options,
          options[0]);
        if (n == 0) {
          save.actionPerformed(e);
        }
        else if (n == 2) return;
      }
      JFileChooser fileChooser = new JFileChooser();
      String dir = null;
      if (userData != null) {
        dir = userData.getStringData(UserData.LAST_OPEN_DIR);
      }
      fileChooser.setCurrentDirectory(dir == null ? storage : new File(dir));
      String file = userData.getStringData(UserData.LAST_OPEN_FILE);
      if (file != null) {
        fileChooser.setSelectedFile(new File(file));
      }
      fileChooser.addChoosableFileFilter(
        new SimpleFileFilter("fractal", "Fractal ADL files"));
      if (fileChooser.showOpenDialog(null) != JFileChooser.APPROVE_OPTION) {
        return;
      }
      File f = fileChooser.getSelectedFile();

      File p = f;
      String name = f.getName().substring(0, f.getName().indexOf('.'));
      while (p.getParentFile() != null && !p.getParentFile().equals(storage)) {
        name = p.getParentFile().getName() + "." + name;
        p = p.getParentFile();
      }
      if (!storage.equals(p.getParentFile())) {
        JOptionPane.showMessageDialog(
          null,
          "Cannot open a file which is not in the storage directory. " +
          "Change the storage directory first.", 
          "Error",
          JOptionPane.ERROR_MESSAGE);
        return;
      }
      
      if (userData != null) {
        userData.setStringData(UserData.LAST_OPEN_FILE, f.getAbsolutePath());
        userData.setStringData(UserData.LAST_OPEN_DIR, f.getParent());
        userData.setStringData(UserData.LAST_OPEN_CONF, name);
        userData.save();
      }

      new Thread(new Open(e, storage, name)).start();
    } catch (Exception ignored) {
      ignored.printStackTrace();
    }
  }

  /**
   * A runnable action to open a configuration. This action must be performed
   * in a thread separated from the Swing event handler thread, otherwise the
   * wait cursor is not displayed during the action.
   */

  protected class Open implements Runnable {

    /**
     * The root pane of the frame into which the wait cursor must be displayed.
     */

      protected JRootPane rootPane;

    /**
     * The storage that must opened.
     */

      protected File storage;
    
    /**
     * The definition that must be opened. 
     */

    protected String name;

    /**
     * Constructs a new {@link OpenAction} object.
     *
     * @param e the event that triggered this action.
     */

    public Open (final ActionEvent e, final File storage, final String name) {
      JComponent src = (JComponent)e.getSource();
      if (src instanceof JMenuItem) {
        src = (JComponent)src.getParent();
      }
      if (src instanceof JPopupMenu) {
        src = (JComponent)((JPopupMenu)src).getInvoker();
      }
      this.rootPane = src.getRootPane();
      this.storage = storage;
      this.name = name;
    }

    public void run () {
      java.awt.Component glassPane = rootPane.getGlassPane();
      rootPane.setGlassPane(new WaitGlassPane());
      rootPane.getGlassPane().setVisible(true);

      try {
        OpenAction.this.storage.open(storage.getAbsolutePath());
        try {
          Component c = repository.loadComponent(name, graph);
          configuration.setRootComponent(c);
          selection.selectComponent(c);
        } finally {
          OpenAction.this.storage.close();
        }
      } catch (Exception ignored) {
        ignored.printStackTrace();
      }

      rootPane.getGlassPane().setVisible(false);
      rootPane.setGlassPane(glassPane);
    }
  }
}
