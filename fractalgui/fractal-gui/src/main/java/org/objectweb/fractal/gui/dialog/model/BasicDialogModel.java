/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.dialog.model;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Configuration;
import org.objectweb.fractal.gui.model.ConfigurationListener;
import org.objectweb.fractal.gui.model.Interface;
import org.objectweb.fractal.gui.model.ServerInterface;
import org.objectweb.fractal.gui.selection.model.Selection;
import org.objectweb.fractal.gui.selection.model.SelectionListener;

import javax.swing.DefaultListSelectionModel;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableModel;
import javax.swing.text.Document;

/**
 * Basic implementation of the {@link DialogModel} interface. This mode listens
 * to the configuration and selection models in order to update itself when the
 * configuration or the selection changes.
 */

public class BasicDialogModel implements
  DialogModel,
  ConfigurationListener,
  SelectionListener,
  BindingController
{

  /**
   * A mandatory client interface bound to a {@link Configuration configuration}
   * model. This configuration is used to get the component on which this dialog
   * model is based (this component is the root component of the configuration).
   */

  public final static String CONFIGURATION_BINDING = "configuration";

  /**
   * A mandatory client interface bound to a {@link Selection selection} model.
   * This is the model on which the Swing list selection models of this model
   * are based.
   */

  public final static String SELECTION_BINDING = "selection";

  /**
   * The configuration client interface.
   */
  protected Configuration configuration;

  /**
   * The selection client interface.
   */

  private Selection selection;

  /**
   * The table models representing the external interfaces.
   */

  private InterfaceTableModel[] itfTableModel;

  /**
   * The table selection models representing the external interfaces.
   */

  private InterfaceTableSelectionModel[] itfTableSelectionModel;

  /**
   * The table model representing the attributes.
   */

  private AttributeTableModel attrTableModel;

  /**
   * The table selection model representing the attributes.
   */

  private ListSelectionModel attrTableSelectionModel;

  /**
   * The text field model representing the component's name.
   */

  private TextFieldModel nameFieldModel;

  /**
   * The text field model representing the component's type.
   */

  private TextFieldModel typeFieldModel;

  /**
   * The text field model representing the component's implementation.
   */

  private TextFieldModel implementationFieldModel;

  /**
   * The text field model representing the component's attribute controller.
   */

  private TextFieldModel attrControllerFieldModel;

  /**
   * The text field model representing the component's template controller
   * descriptor.
   */

  private TextFieldModel tmplControllerDescFieldModel;

  /**
   * The text field model representing the component's controller descriptor.
   */

  private TextFieldModel compControllerDescFieldModel;

  /**
   * Constructs a new {@link BasicDialogModel} component.
   */

  public BasicDialogModel () {
    itfTableModel = new InterfaceTableModel[2];
    itfTableSelectionModel = new InterfaceTableSelectionModel[2];
    for (int i = 0; i < 2; ++i) {
      itfTableModel[i] = new InterfaceTableModel(i == 0);
      itfTableSelectionModel[i] = new InterfaceTableSelectionModel(i == 0);
    }

    attrTableModel = new AttributeTableModel();
    attrTableSelectionModel = new DefaultListSelectionModel();
    attrTableSelectionModel.setSelectionMode(
      ListSelectionModel.SINGLE_SELECTION);

    nameFieldModel = new TextFieldModel(TextFieldModel.NAME);
    typeFieldModel = new TextFieldModel(TextFieldModel.TYPE);
    implementationFieldModel =
      new TextFieldModel(TextFieldModel.IMPLEMENTATION);
    attrControllerFieldModel =
      new TextFieldModel(TextFieldModel.ATTRIBUTE_CONTROLLER);
    tmplControllerDescFieldModel =
      new TextFieldModel(TextFieldModel.TEMPLATE_CONTROLLER_DESC);
    compControllerDescFieldModel =
      new TextFieldModel(TextFieldModel.COMPONENT_CONTROLLER_DESC);
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] {
      CONFIGURATION_BINDING,
      SELECTION_BINDING
    };
  }

  public Object lookupFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      return configuration;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      return selection;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = (Configuration) serverItf;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = (Selection)serverItf;
      for (int i = 0; i < itfTableSelectionModel.length; ++i) {
        itfTableSelectionModel[i].setSelection(selection);
      }
    }
  }

  public void unbindFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = null;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = null;
      for (int i = 0; i < itfTableSelectionModel.length; ++i) {
        itfTableSelectionModel[i].setSelection(null);
      }
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the DialogModel interface
  // -------------------------------------------------------------------------

  public TableModel getClientInterfacesTableModel () {
    return itfTableModel[0];
  }

  public ListSelectionModel getClientInterfacesTableSelectionModel() {
    return itfTableSelectionModel[0];
  }

  public TableModel getServerInterfacesTableModel() {
    return itfTableModel[1];
  }

  public InterfaceTableSelectionModel getServerInterfacesTableSelectionModel() {
    return itfTableSelectionModel[1];
  }

  public TableModel getAttributesTableModel() {
    return attrTableModel;
  }

  public ListSelectionModel getAttributesTableSelectionModel() {
    return attrTableSelectionModel;
  }

  public Document getNameFieldModel() {
    return nameFieldModel;
  }

  public Document getTypeFieldModel() {
    return typeFieldModel;
  }

  public Document getImplementationFieldModel() {
    return implementationFieldModel;
  }

  public Document getAttrControllerFieldModel() {
    return attrControllerFieldModel;
  }

  public Document getTmplControllerDescFieldModel() {
    return tmplControllerDescFieldModel;
  }

  public Document getCompControllerDescFieldModel() {
    return compControllerDescFieldModel;
  }

  // -------------------------------------------------------------------------
  // Implementation of the ConfigurationListener interface
  // -------------------------------------------------------------------------

  public void changeCountChanged (Component component, long changeCount) {
    // does nothing
  }

  public void rootComponentChanged (final Component oldValue) {
    final Component root = configuration.getRootComponent();
    for (int i = 0; i < itfTableModel.length; ++i) {
      itfTableModel[i].setComponentModel(root);
      itfTableSelectionModel[i].setComponentModel(root);
    }

    attrTableModel.setComponentModel(root);

    nameFieldModel.setComponentModel(root);
    typeFieldModel.setComponentModel(root);
    implementationFieldModel.setComponentModel(root);
    attrControllerFieldModel.setComponentModel(root);
    tmplControllerDescFieldModel.setComponentModel(root);
    compControllerDescFieldModel.setComponentModel(root);
  }

  public void nameChanged (final Component component, final String oldValue) {
    if (component == configuration.getRootComponent()) {
      nameFieldModel.componentTextChanged(component.getName());
    }
  }

  public void typeChanged (final Component component, final String oldValue) {
    if (component == configuration.getRootComponent()) {
      typeFieldModel.componentTextChanged(component.getType());
    }
  }

  public void implementationChanged (
    final Component component,
    final String oldValue)
  {
    if (component == configuration.getRootComponent()) {
      implementationFieldModel.componentTextChanged(
        component.getImplementation());
    }
  }

  public void interfaceNameChanged (final Interface i, final String oldValue) {
    interfaceChanged(i);
  }

  public void interfaceSignatureChanged (
    final Interface i,
    final String oldValue)
  {
    interfaceChanged(i);
  }

  public void interfaceContingencyChanged (
    final Interface i,
    final boolean oldValue)
  {
    interfaceChanged(i);
  }

  public void interfaceCardinalityChanged (
    final Interface i,
    final boolean oldValue)
  {
    interfaceChanged(i);
  }

  /**
   * Notifies the appropriate table model that an interface has changed.
   *
   * @param i the interface that has changed.
   */

  private void interfaceChanged (final Interface i) {
    if (i.getOwner() == configuration.getRootComponent()) {
      if (i instanceof ClientInterface) {
        itfTableModel[0].interfaceChanged(i);
      } else {
        itfTableModel[1].interfaceChanged(i);
      }
    }
  }

  public void clientInterfaceAdded (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    if (component == configuration.getRootComponent()) {
      itfTableModel[0].interfaceAdded(i, index);
    }
  }

  public void clientInterfaceRemoved (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    if (component == configuration.getRootComponent()) {
      itfTableModel[0].interfaceRemoved(i, index);
    }
  }

  public void serverInterfaceAdded (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    if (component == configuration.getRootComponent()) {
      itfTableModel[1].interfaceAdded(i, index);
    }
  }

  public void serverInterfaceRemoved (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    if (component == configuration.getRootComponent()) {
      itfTableModel[1].interfaceRemoved(i, index);
    }
  }

  public void interfaceBound (
    final ClientInterface citf,
    final ServerInterface sitf)
  {
    if (citf.getOwner() == configuration.getRootComponent()) {
      // TODO
    }
  }

  public void interfaceRebound (
    final ClientInterface citf,
    final ServerInterface oldSitf)
  {
    if (citf.getOwner() == configuration.getRootComponent()) {
      // TODO
    }
  }

  public void interfaceUnbound (
    final ClientInterface citf,
    final ServerInterface sitf)
  {
    if (citf.getOwner() == configuration.getRootComponent()) {
      // TODO
    }
  }

  public void attributeControllerChanged (
    final Component component,
    final String oldValue)
  {
    if (component == configuration.getRootComponent()) {
      attrControllerFieldModel.componentTextChanged(
        component.getAttributeController());
    }
  }

  public void attributeChanged (
    final Component component,
    final String attributeName,
    final String oldValue)
  {
    if (component == configuration.getRootComponent()) {
      attrTableModel.attributeChanged(attributeName, oldValue);
    }
  }

  public void templateControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    if (component == configuration.getRootComponent()) {
      tmplControllerDescFieldModel.componentTextChanged(
        component.getTemplateControllerDescriptor());
    }
  }

  public void componentControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    if (component == configuration.getRootComponent()) {
      compControllerDescFieldModel.componentTextChanged(
        component.getComponentControllerDescriptor());
    }
  }

  public void subComponentAdded (
    final Component parent,
    final Component child,
    final int index)
  {
    // does nothing
  }

  public void subComponentRemoved (
    final Component parent,
    final Component child,
    final int index)
  {
    // does nothing
  }

  // -------------------------------------------------------------------------
  // Implementation of the SelectionListener interface
  // -------------------------------------------------------------------------

  public void selectionChanged () {
    Object o = selection.getSelection();
    if (o != null) {
      if (o instanceof Interface) {
        o = ((Interface)o).getOwner();
      }
      configuration.setRootComponent((Component)o);
    }
    for (int i = 0; i < itfTableSelectionModel.length; ++i) {
      itfTableSelectionModel[i].selectionChanged();
    }
  }
}
