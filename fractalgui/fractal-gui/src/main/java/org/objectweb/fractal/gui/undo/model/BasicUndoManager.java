/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.undo.model;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.graph.model.GraphModel;
import org.objectweb.fractal.gui.graph.model.GraphModelListener;
import org.objectweb.fractal.gui.graph.model.Rect;
import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.ConfigurationListener;
import org.objectweb.fractal.gui.model.Interface;
import org.objectweb.fractal.gui.model.ServerInterface;
import org.objectweb.fractal.gui.model.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.awt.Color;

/**
 * Basic implementation of the {@link UndoManager} interface.
 */

public class BasicUndoManager implements
  UndoManager,
  ConfigurationListener,
  GraphModelListener,
  BindingController
{

  /**
   * A mandatory client interface bound to a {@link Configuration configuration}
   * model. This is the configuration whose root component is changed when
   * undoing or redoing modifications.
   */

  public final static String CONFIGURATION_BINDING = "configuration";

  /**
   * An optional client interface bound to a {@link GraphModel graph} model.
   * This is the graph model that is changed when undoing or redoing
   * modifications.
   */

  public final static String GRAPH_BINDING = "graph";

  /**
   * A collection client interface bound to the {@link UndoListener listeners}
   * of this component.
   */

  public final static String UNDO_LISTENERS_BINDING = "undo-listeners";

  /**
   * Maximum number of modifications stored by this component.
   */

  private final static int MAX_UNDO = 50;

  /**
   * The configuration client interface.
   */

  protected Configuration configuration;

  /**
   * The graph client interface.
   */

  private GraphModel graph;

  /**
   * The listeners client interface.
   */

  private Map undoListeners;

  /**
   * The list of changes that can be undone.
   */

  private List undoList;

  /**
   * The list of changes that can be redone.
   */

  private List redoList;

  /**
   * If the {@link #undo undo} method is currently executing.
   */

  private boolean isUndoing;

  /**
   * If the {@link #redo redo} method is currently executing.
   */

  private boolean isRedoing;

  /**
   * Constructs a new {@link BasicUndoManager} component.
   */

  public BasicUndoManager () {
    undoList = new ArrayList();
    redoList = new ArrayList();
    undoListeners = new HashMap();
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    int size = undoListeners.size();
    String[] names = new String[size + 2];
    undoListeners.keySet().toArray(names);
    names[size] = CONFIGURATION_BINDING;
    names[size + 1] = GRAPH_BINDING;
    return names;
  }

  public Object lookupFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      return configuration;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      return graph;
    } else if (clientItfName.startsWith(UNDO_LISTENERS_BINDING)) {
      return undoListeners.get(clientItfName);
    }
    return null;
  }


  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = (Configuration)serverItf;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      graph = (GraphModel)serverItf;
    } else if (clientItfName.startsWith(UNDO_LISTENERS_BINDING)) {
      undoListeners.put(clientItfName, serverItf);
    }
  }

  public void unbindFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = null;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      graph = null;
    } else if (clientItfName.startsWith(UNDO_LISTENERS_BINDING)) {
      undoListeners.remove(clientItfName);
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the ConfigurationListener interface
  // -------------------------------------------------------------------------

  public void changeCountChanged (Component component, long changeCount) {
    // does nothing
  }

  public void rootComponentChanged (final Component oldValue) {
    clear();
  }

  public void nameChanged (final Component component, final String oldValue) {
    if (configuration.getRootComponent().contains(component)) {
      addAction(new Action(component, 1) {
        protected void run () {
          ((Component)target).setName(oldValue);
        }
      });
    }
  }

  public void typeChanged (final Component component, final String oldValue) {
    if (configuration.getRootComponent().contains(component)) {
      addAction(new Action(component, 1) {
        protected void run () {
          ((Component)target).setType(oldValue);
        }
      });
    }
  }

  public void implementationChanged (
    final Component component,
    final String oldValue)
  {
    if (configuration.getRootComponent().contains(component)) {
      addAction(new Action(component, 1) {
        protected void run () {
          ((Component)target).setImplementation(oldValue);
        }
      });
    }
  }

  public void interfaceNameChanged (final Interface i, final String oldValue) {
    if (configuration.getRootComponent().contains(i.getOwner())) {
      addAction(new Action(i, 1) {
        protected void run () {
          ((Interface)target).setName(oldValue);
        }
      });
    }
  }

  public void interfaceSignatureChanged (
    final Interface i,
    final String oldValue)
  {
    if (configuration.getRootComponent().contains(i.getOwner())) {
      addAction(new Action(i, 1) {
        protected void run () {
          ((Interface)target).setSignature(oldValue);
        }
      });
    }
  }

  public void interfaceContingencyChanged (
    final Interface i,
    final boolean oldValue)
  {
    if (configuration.getRootComponent().contains(i.getOwner())) {
      addAction(new Action(i, 1) {
        protected void run () {
          ((Interface)target).setIsOptional(oldValue);
        }
      });
    }
  }

  public void interfaceCardinalityChanged (
    final Interface i,
    final boolean oldValue)
  {
    if (configuration.getRootComponent().contains(i.getOwner())) {
      addAction(new Action(i, 1) {
        protected void run () {
          ((Interface)target).setIsCollection(oldValue);
        }
      });
    }
  }

  public void clientInterfaceAdded (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    if (configuration.getRootComponent().contains(component)) {
      addAction(new Action(null, 1) {
        protected void run () {
          component.removeClientInterface(i);
        }
      });
    }
  }

  public void clientInterfaceRemoved (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    if (configuration.getRootComponent().contains(component)) {
      addAction(new Action(null, 1) {
        protected void run () {
          component.addClientInterface(i);
        }
      });
    }
  }

  public void serverInterfaceAdded (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    if (configuration.getRootComponent().contains(component)) {
      addAction(new Action(null, 1) {
        protected void run () {
          component.removeServerInterface(i);
        }
      });
    }
  }

  public void serverInterfaceRemoved (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    if (configuration.getRootComponent().contains(component)) {
      addAction(new Action(null, 1) {
        protected void run () {
          component.addServerInterface(i);
        }
      });
    }
  }

  public void interfaceBound (
    final ClientInterface citf,
    final ServerInterface sitf)
  {
    if (configuration.getRootComponent().contains(citf.getOwner())) {
      addAction(new Action(null, 1) {
        protected void run () {
          citf.getOwner().unbind(citf);
        }
      });
    }
  }

  public void interfaceRebound (
    final ClientInterface citf,
    final ServerInterface oldSitf)
  {
    if (configuration.getRootComponent().contains(citf.getOwner())) {
      addAction(new Action(null, 1) {
        protected void run () {
          citf.getOwner().rebind(citf, oldSitf);
        }
      });
    }
  }

  public void interfaceUnbound (
    final ClientInterface citf,
    final ServerInterface sitf)
  {
    if (configuration.getRootComponent().contains(citf.getOwner())) {
      addAction(new Action(null, 1) {
        protected void run () {
          citf.getOwner().bind(citf, null, sitf);
        }
      });
    }
  }

  public void attributeControllerChanged (
    final Component component,
    final String oldValue)
  {
    if (configuration.getRootComponent().contains(component)) {
      addAction(new Action(component, 1) {
        protected void run () {
          ((Component)target).setAttributeController(oldValue);
        }
      });
    }
  }

  public void attributeChanged (
    final Component component,
    final String attributeName,
    final String oldValue)
  {
    if (configuration.getRootComponent().contains(component)) {
      addAction(new Action(component, 1) {
        protected void run () {
          ((Component)target).setAttribute(attributeName, oldValue);
        }
      });
    }
  }

  public void templateControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    if (configuration.getRootComponent().contains(component)) {
      addAction(new Action(component, 1) {
        protected void run () {
          ((Component)target).setTemplateControllerDescriptor(oldValue);
        }
      });
    }
  }

  public void componentControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    if (configuration.getRootComponent().contains(component)) {
      addAction(new Action(component, 1) {
        protected void run () {
          ((Component)target).setComponentControllerDescriptor(oldValue);
        }
      });
    }
  }

  public void subComponentAdded (
    final Component parent,
    final Component child,
    final int index)
  {
    if (configuration.getRootComponent().contains(parent)) {
      addAction(new Action(null, 1) {
        protected void run () {
          parent.removeSubComponent(child);
        }
      });
    }
  }

  public void subComponentRemoved (
    final Component parent,
    final Component child,
    final int index)
  {
    if (configuration.getRootComponent().contains(parent)) {
      addAction(new Action(null, 1) {
        protected void run () {
          parent.addSubComponent(child);
        }
      });
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the GraphModelListener
  // -------------------------------------------------------------------------

  public void componentColorChanged (
    final Component component,
    final Color oldColor)
  {
    if (configuration.getRootComponent().contains(component)) {
      addAction(new Action(component, 0) {
        protected void run () {
          graph.setComponentColor((Component)target, oldColor);
        }
      });
    }
  }

  public void componentPositionChanged (
    final Component component,
    final Rect oldValue)
  {
    if (configuration.getRootComponent().contains(component)) {
      addAction(new Action(component, 0) {
        protected void run () {
          graph.setComponentPosition((Component)target, oldValue);
        }
      });
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the UndoManager interface
  // -------------------------------------------------------------------------

  public boolean canUndo () {
    return undoList.size() > 0;
  }

  public void undo () {
    isUndoing = true;
    try {
      Action a = (Action)undoList.remove(undoList.size() - 1);
      configuration.setChangeCount(
        configuration.getChangeCount() - 2*a.deltaChangeCount);
      a.run();
    } finally {
      isUndoing = false;
    }
    if (undoList.size() == 0) {
      notifyListeners();
    }
  }

  public boolean canRedo () {
    return redoList.size() > 0;
  }

  public void redo () {
    isRedoing = true;
    try {
      ((Action)redoList.remove(redoList.size() - 1)).run();
    } finally {
      isRedoing = false;
    }
    if (redoList.size() == 0) {
      notifyListeners();
    }
  }

  public void clear () {
    undoList.clear();
    redoList.clear();
    notifyListeners();
  }

  // -------------------------------------------------------------------------
  // Other methods
  // -------------------------------------------------------------------------

  /**
   * An action that can executed to undo or redo something.
   */

  protected abstract class Action {

    /**
     * The target of this action. Actions with identical target may be
     * coalesced (see {@link BasicUndoManager#addAction addAction}).
     */
    protected Object target;

    /**
     * The changeCount variation when this action or its opposite is run.
     */

    int deltaChangeCount;

    /**
     * Constructs a new {@link BasicUndoManager.Action}.
     *
     * @param target the target of this action.
     * @param deltaChangeCount the changeCount variation when this action or its
     *      opposite is run.
     */
    protected Action (final Object target, int deltaChangeCount) {
      this.target = target;
      this.deltaChangeCount = deltaChangeCount;
    }

    /**
     * Executes this action.
     */
    protected abstract void run ();
  }

  /**
   * Adds the given action to the undo or redo list.
   *
   * @param a an action.
   */
  protected void addAction (final Action a) {
    if (!isUndoing && !isRedoing && redoList.size() > 0) {
      redoList.clear();
      notifyListeners();
    }
    List l = isUndoing ? redoList : undoList;
    if (l.size() == 0) {
      l.add(a);
      notifyListeners();
    } else {
      Action last = (Action)l.get(l.size() - 1);
      if (last.getClass() == a.getClass() &&
          last.target == a.target &&
          a.target != null)
      {
        // ignore a
        return;
      }
      if (l.size() >= MAX_UNDO) {
        l.remove(0);
      }
      l.add(a);
    }
  }

  /**
   * Notifies the listeners of this model that its state has changed.
   */

  private void notifyListeners () {
    Iterator i = undoListeners.values().iterator();
    while (i.hasNext()) {
      ((UndoListener)i.next()).undoStateChanged();
    }
  }
}
