/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.history.model;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Configuration;
import org.objectweb.fractal.gui.model.ConfigurationListener;
import org.objectweb.fractal.gui.model.Interface;
import org.objectweb.fractal.gui.model.ServerInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Basic implementation of the {@link HistoryManager} interface.
 */

public class BasicHistoryManager
  implements HistoryManager, ConfigurationListener, BindingController
{

  /**
   * A mandatory client interface bound to a {@link Configuration configuration}
   * model. This is the configuration whose root component is changed when going
   * to the next or previous 'view'.
   */

  public final static String CONFIGURATION_BINDING = "configuration";

  /**
   * A collection client interface bound to the {@link HistoryListener
   * listeners} of this component.
   */

  public final static String HISTORY_LISTENERS_BINDING = "history-listeners";

  /**
   * Maximum number of 'views' stored by this component.
   */

  private final static int MAX_HISTORY = 50;

  /**
   * The configuration client interface.
   */

  private Configuration configuration;

  /**
   * The listeners client interface.
   */

  private Map historyListeners;

  /**
   * The previous root components of {@link #configuration}. This list is used
   * to go to previous 'views' in the navigation history.
   */

  private List previousRoots;

  /**
   * The next root components of {@link #configuration}. This list is used to
   * undo the 'go previous' actions, i.e., to go to next 'views' in the
   * navigation history.
   */

  private List nextRoots;

  /**
   * If the {@link #goPrevious goPrevious} method is currently executing.
   */

  private boolean isGoingPrevious;

  /**
   * If the {@link #goNext goNext} method is currently executing.
   */

  private boolean isGoingNext;

  /**
   * Constructs a new {@link BasicHistoryManager} component.
   */

  public BasicHistoryManager () {
    previousRoots = new ArrayList();
    nextRoots = new ArrayList();
    historyListeners = new HashMap();
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    int size = historyListeners.size();
    String[] names = new String[size + 1];
    historyListeners.keySet().toArray(names);
    names[size] = CONFIGURATION_BINDING;
    return names;
  }

  public Object lookupFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      return configuration;
    } else if (clientItfName.startsWith(HISTORY_LISTENERS_BINDING)) {
      return historyListeners.get(clientItfName);
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = (Configuration)serverItf;
    } else if (clientItfName.startsWith(HISTORY_LISTENERS_BINDING)) {
      historyListeners.put(clientItfName, serverItf);
    }
  }

  public void unbindFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = null;
    } else if (clientItfName.startsWith(HISTORY_LISTENERS_BINDING)) {
      historyListeners.remove(clientItfName);
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the ConfigurationListener interface
  // -------------------------------------------------------------------------

  public void changeCountChanged (Component component, long changeCount) {
    // does nothing
  }

  public void rootComponentChanged (final Component oldModel) {
    if (oldModel == null) {
      return;
    }
    if (oldModel.getRootComponent() !=
        configuration.getRootComponent().getRootComponent())
    {
      clear();
      return;
    }
    if (isGoingPrevious) {
      nextRoots.add(oldModel);
      if (nextRoots.size() == 1) {
        notifyListeners();
      }
    } else {
      int size = previousRoots.size();
      if (size >= MAX_HISTORY) {
        previousRoots.remove(0);
      }
      previousRoots.add(oldModel);
      if (!isGoingNext) {
        nextRoots.clear();
      }
      if (previousRoots.size() == 1 || !isGoingNext) {
        notifyListeners();
      }
    }
  }

  public void nameChanged (final Component component, final String oldValue) {
    // does nothing
  }

  public void typeChanged (final Component component, final String oldValue) {
    // does nothing
  }

  public void implementationChanged (
    final Component component,
    final String oldValue) {
    // does nothing
  }

  public void interfaceNameChanged (final Interface i, final String oldValue) {
    // does nothing
  }

  public void interfaceSignatureChanged (
    final Interface i,
    final String oldValue)
  {
    // does nothing
  }

  public void interfaceContingencyChanged (
    final Interface i,
    final boolean oldValue)
  {
    // does nothing
  }

  public void interfaceCardinalityChanged (
    final Interface i,
    final boolean oldValue)
  {
    // does nothing
  }

  public void clientInterfaceAdded (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    // does nothing
  }

  public void clientInterfaceRemoved (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    // does nothing
  }

  public void serverInterfaceAdded (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    // does nothing
  }

  public void serverInterfaceRemoved (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    // does nothing
  }

  public void interfaceBound (
    final ClientInterface citf,
    final ServerInterface sitf)
  {
    // does nothing
  }

  public void interfaceRebound (
    final ClientInterface citf,
    final ServerInterface oldSitf)
  {
    // does nothing
  }

  public void interfaceUnbound (
    final ClientInterface citf,
    final ServerInterface sitf)
  {
    // does nothing
  }

  public void attributeControllerChanged (
    final Component component,
    final String oldValue)
  {
    // does nothing
  }

  public void attributeChanged (
    final Component component,
    final String attributeName,
    final String oldValue)
  {
    // does nothing
  }

  public void templateControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    // does nothing
  }

  public void componentControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    // does nothing
  }

  public void subComponentAdded (
    final Component parent,
    final Component child,
    final int index)
  {
    // does nothing
  }

  public void subComponentRemoved (
    final Component parent,
    final Component child,
    final int index)
  {
    // does nothing
  }

  // -------------------------------------------------------------------------
  // Implementation of the HistoryManager interface
  // -------------------------------------------------------------------------

  public boolean canGoPrevious () {
    return previousRoots.size() > 0;
  }

  public void goPrevious () {
    isGoingPrevious = true;
    try {
      Component root = (Component)previousRoots.remove(previousRoots.size()-1);
      configuration.setRootComponent(root);
    } finally {
      isGoingPrevious = false;
    }
    if (previousRoots.size() == 0) {
      notifyListeners();
    }
  }

  public boolean canGoNext () {
    return nextRoots.size() > 0;
  }

  public void goNext () {
    isGoingNext = true;
    try {
      Component root = (Component)nextRoots.remove(nextRoots.size() - 1);
      configuration.setRootComponent(root);
    } finally {
      isGoingNext = false;
    }
    if (nextRoots.size() == 0) {
      notifyListeners();
    }
  }

  public void clear () {
    nextRoots.clear();
    previousRoots.clear();
    notifyListeners();
  }

  // -------------------------------------------------------------------------
  // Other methods
  // -------------------------------------------------------------------------

  /**
   * Notifies the listeners of this model that its state has changed.
   */

  private void notifyListeners () {
    Iterator i = historyListeners.values().iterator();
    while (i.hasNext()) {
      ((HistoryListener)i.next()).historyStateChanged();
    }
  }
}
