/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.history.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.history.model.HistoryListener;
import org.objectweb.fractal.gui.history.model.HistoryManager;
import org.objectweb.fractal.swing.AbstractAction;

import java.awt.event.ActionEvent;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.KeyStroke;

/**
 * An action that just calls the {@link HistoryManager#goNext goNext} method on
 * an {@link HistoryManager}. This action listens to the history model in order
 * to enable or disable itfself when the {@link HistoryManager#canGoNext
 * canGoNext} state changes.
 */

public class GoNextAction extends AbstractAction implements
  HistoryListener,
  BindingController
{

  /**
   * A mandatory client interface bound to a {@link HistoryManager history}
   * model. This is the model modified by this controller component.
   */

  public final static String HISTORY_MANAGER_BINDING = "history-manager";

  /**
   * The history client interface.
   */

  private HistoryManager historyManager;

  /**
   * Constructs a new {@link GoNextAction} component.
   */

  public GoNextAction () {
    putValue(NAME, "Next");
    putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("F4"));
    putValue(SHORT_DESCRIPTION, "Next");
    URL url = getClass().getResource(
      "/org/objectweb/fractal/gui/resources/1rightarrow.gif");
    putValue(SMALL_ICON, new ImageIcon(url));
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] { HISTORY_MANAGER_BINDING };
  }

  public Object lookupFc (final String clientItfName) {
    if (HISTORY_MANAGER_BINDING.equals(clientItfName)) {
      return historyManager;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (HISTORY_MANAGER_BINDING.equals(clientItfName)) {
      historyManager = (HistoryManager)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (HISTORY_MANAGER_BINDING.equals(clientItfName)) {
      historyManager = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the UndoListener interface
  // -------------------------------------------------------------------------

  public void historyStateChanged () {
    setEnabled(historyManager.canGoNext());
  }

  // -------------------------------------------------------------------------
  // Implementation of the ActionListener interface
  // -------------------------------------------------------------------------

  public void actionPerformed (final ActionEvent e) {
    historyManager.goNext();
  }
}
