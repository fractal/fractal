/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.view;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.graph.model.Tools;
import org.objectweb.fractal.gui.graph.model.ToolsListener;
import org.objectweb.fractal.swing.ActionAttributes;

import java.awt.event.ActionEvent;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JToggleButton;
import javax.swing.Action;

/**
 * A {@link Tools tools} view component. This component displays one tool, as
 * a toggle button which is selected iif the corresponding tool is selected in
 * the model. This view component listens to the tools model, in order to select
 * or deselect itself when the currently selected tool changes.
 */

public class BasicToolView extends JToggleButton implements
  Action,
  ToolsListener,
  BindingController,
  ActionAttributes
{

  /**
   * A mandatory client interface bound to a {@link Tools tools} model. This
   * is the model displayed by this view component.
   */

  public final static String TOOLS_BINDING = "tools";

  /**
   * The tools client interface.
   */

  private Tools tools;

  /**
   * URL of the icon of this action.
   */

  private String iconURL;

  /**
   * Accelerator key of this action.
   */

  private String acceleratorKey;

  /**
   * The tool represented by this view. This field is computed from the name of
   * this action, in the {@link #setName setName} method.
   */

  private int tool;

  /**
   * Constructs a new {@link BasicToolView} component.
   */

  public BasicToolView () {
    setMargin(new Insets(2, 2, 2, 2));
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] { TOOLS_BINDING };
  }

  public Object lookupFc (final String clientItfName) {
    if (TOOLS_BINDING.equals(clientItfName)) {
      return tools;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (TOOLS_BINDING.equals(clientItfName)) {
      tools = (Tools)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (TOOLS_BINDING.equals(clientItfName)) {
      tools = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the Action interface
  // -------------------------------------------------------------------------

  public Object getValue (final String key) {
    return null;
  }

  public void putValue (final String key, final Object value) {
  }

  public void actionPerformed (final ActionEvent e) {
  }

  // -------------------------------------------------------------------------
  // Implementation of the ActionAttributes interface
  // -------------------------------------------------------------------------

  public void setName (final String name) {
    super.setName(name);
    if (name.equals("Edit")) {
      tool = Tools.SELECT;
    } else if (name.equals("Move")) {
      tool = Tools.MOVE;
    } else if (name.equals("Zoomin")) {
      tool = Tools.ZOOM_IN;
    } else if (name.equals("Zoomout")) {
      tool = Tools.ZOOM_OUT;
    }
  }

  public String getIconURL () {
    return iconURL;
  }

  public void setIconURL (final String iconURL) {
    this.iconURL = iconURL;
    setIcon(new ImageIcon(getClass().getResource(iconURL)));
  }

  public String getAcceleratorKey () {
    return acceleratorKey;
  }

  public void setAcceleratorKey (final String acceleratorKey) {
    this.acceleratorKey = acceleratorKey;
  }

  // -------------------------------------------------------------------------
  // Implementation of the ToolsListener interface
  // -------------------------------------------------------------------------

  public void toolChanged () {
    setSelected(tools.getTool() == tool);
  }

  // -------------------------------------------------------------------------
  // Overriden JToggleButton methods
  // -------------------------------------------------------------------------

  protected void fireStateChanged () {
    super.fireStateChanged();
    if (isSelected()) {
      tools.setTool(tool);
    }
  }
}
