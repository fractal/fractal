/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.selection.model;

import org.objectweb.fractal.api.control.BindingController;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

/**
 * A {@link SelectionListener} that notifies other {@link SelectionListener}s.
 */

public class SelectionNotifier implements
  SelectionListener,
  BindingController
{

  /**
   * A collection client interface bound to the {@link SelectionListener
   * listeners} of this component.
   */

  public final static String SELECTION_LISTENERS_BINDING =
    "selection-listeners";

  /**
   * The listeners client interface.
   */

  private Map selectionListeners;

  /**
   * Constructs a new {@link SelectionNotifier} component.
   */

  public SelectionNotifier () {
    selectionListeners = new HashMap();
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return (String[])selectionListeners.keySet().toArray(
      new String[selectionListeners.size()]);
  }

  public Object lookupFc (final String clientItfName) {
    if (clientItfName.startsWith(SELECTION_LISTENERS_BINDING)) {
      return selectionListeners.get(clientItfName);
    }
    return null;
  }


  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (clientItfName.startsWith(SELECTION_LISTENERS_BINDING)) {
      selectionListeners.put(clientItfName, serverItf);
    }
  }


  public void unbindFc (final String clientItfName) {
    if (clientItfName.startsWith(SELECTION_LISTENERS_BINDING)) {
      selectionListeners.remove(clientItfName);
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the SelectionListener interface
  // -------------------------------------------------------------------------

  public void selectionChanged () {
    Iterator i = selectionListeners.values().iterator();
    while (i.hasNext()) {
      ((SelectionListener)i.next()).selectionChanged();
    }
  }
}
