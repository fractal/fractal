/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.selection.model;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Interface;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Basic implementation of the {@link Selection} interface.
 */

public class BasicSelection implements Selection, BindingController {

  /**
   * A collection client interface bound to the {@link SelectionListener
   * listeners} of this component.
   */

  public final static String SELECTION_LISTENERS_BINDING =
    "selection-listeners";

  /**
   * The listeners client interface.
   */

  private Map selectionListeners;

  /**
   * The currently selected component. May be <tt>null</tt>.
   */

  private Component selectedComponent;

  /**
   * The currently selected interface. May be <tt>null</tt>.
   */

  private Interface selectedInterface;

  /**
   * Constructs a new {@link BasicSelection} component.
   */

  public BasicSelection () {
    selectionListeners = new HashMap();
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return (String[])selectionListeners.keySet().toArray(
      new String[selectionListeners.size()]);
  }

  public Object lookupFc (final String clientItfName) {
    if (clientItfName.startsWith(SELECTION_LISTENERS_BINDING)) {
      return selectionListeners.get(clientItfName);
    }
    return null;
  }


  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (clientItfName.startsWith(SELECTION_LISTENERS_BINDING)) {
      selectionListeners.put(clientItfName, serverItf);
    }
  }


  public void unbindFc (final String clientItfName) {
    if (clientItfName.startsWith(SELECTION_LISTENERS_BINDING)) {
      selectionListeners.remove(clientItfName);
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the Selection interface
  // -------------------------------------------------------------------------

  public Object getSelection () {
    if (selectedInterface == null) {
      return selectedComponent;
    } else {
      return selectedInterface;
    }
  }

  public void selectComponent (final Component component) {
    if (selectedComponent != component) {
      selectedComponent = component;
      selectedInterface = null;
      notifyListeners();
    }
  }

  public void selectInterface (final Interface itf) {
    if (selectedInterface != itf) {
      selectedComponent = null;
      selectedInterface = itf;
      notifyListeners();
    }
  }

  public void clearSelection () {
    if (getSelection() != null) {
      selectedComponent = null;
      selectedInterface = null;
      notifyListeners();
    }
  }

  // -------------------------------------------------------------------------
  // Other methods
  // -------------------------------------------------------------------------

  /**
   * Notifies the listeners of this model that its state has changed.
   */

  private void notifyListeners () {
    Iterator i = selectionListeners.values().iterator();
    while (i.hasNext()) {
      ((SelectionListener)i.next()).selectionChanged();
    }
  }
}
