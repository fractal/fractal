/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.model;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.model.Interface;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Basic implementation of the {@link Tools} interface.
 */

public class BasicTools implements Tools, BindingController {

  /**
   * A collection client interface bound to the {@link ToolsListener listeners}
   * of this model.
   */

  public final static String TOOLS_LISTENERS_BINDING = "tools-listeners";

  /**
   * The tool client interface.
   */

  private Map toolsListeners;

  /**
   * The currently selected tool.
   */

  private int tool;

  /**
   * The client interface that will be bound by the {@link #BIND} tool.
   */

  private Interface bindItf;

  /**
   * Constructs a new {@link BasicTools} component.
   */

  public BasicTools () {
    toolsListeners = new HashMap();
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return (String[])toolsListeners.keySet().toArray(
      new String[toolsListeners.size()]);
  }

  public Object lookupFc (final String clientItfName) {
    if (clientItfName.startsWith(TOOLS_LISTENERS_BINDING)) {
      return toolsListeners.get(clientItfName);
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (clientItfName.startsWith(TOOLS_LISTENERS_BINDING)) {
      toolsListeners.put(clientItfName, serverItf);
    }
  }

  public void unbindFc (final String clientItfName) {
    if (clientItfName.startsWith(TOOLS_LISTENERS_BINDING)) {
      toolsListeners.remove(clientItfName);
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the Tools interface
  // -------------------------------------------------------------------------

  public int getTool () {
    return tool;
  }

  public void setTool (final int tool) {
    if (tool != this.tool) {
      this.tool = tool;
      Iterator i = toolsListeners.values().iterator();
      while (i.hasNext()) {
        ((ToolsListener)i.next()).toolChanged();
      }
    }
  }

  public Interface getBindInterface () {
    return bindItf;
  }

  public void setBindInterface (final Interface i) {
    this.bindItf = i;
  }
}
