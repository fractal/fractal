/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.menu.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.UserData;
import org.objectweb.fractal.gui.model.Configuration;
import org.objectweb.fractal.swing.AbstractAction;

import java.awt.event.ActionEvent;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.KeyStroke;
import javax.swing.JOptionPane;
import java.io.File;

import javax.swing.JFileChooser;

/**
 * An action to exit FractalGUI.
 */

public class ChangeExecDirAction extends AbstractAction
  implements BindingController
{

  /**
   * A mandatory client interface bound to a {@link Configuration configuration}
   * model. This is the configuration that is reinitialized by this action.
   */

  public final static String CONFIGURATION_BINDING = "configuration";

  public final static String USER_DATA_BINDING = "user-data";

  /**
   * The configuration client interface.
   */

  private Configuration configuration;

  private UserData userData;
  
  /**
   * Constructs a new {@link ChangeExecDirAction} component.
   */

  public ChangeExecDirAction () {
    putValue(NAME, "Storage...");
    putValue(SHORT_DESCRIPTION, "Storage...");
    URL url = getClass().getResource(
      "/org/objectweb/fractal/gui/resources/empty.gif");
    putValue(SMALL_ICON, new ImageIcon(url));
    putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("control E"));
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] { CONFIGURATION_BINDING, USER_DATA_BINDING };
  }

  public Object lookupFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      return configuration;
    } else if (USER_DATA_BINDING.equals(clientItfName)) {
      return userData;
    }
    return null;
  }

  public void bindFc (final String clientItfName, final Object serverItf) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = (Configuration)serverItf;
    } else if (USER_DATA_BINDING.equals(clientItfName)) {
      userData = (UserData)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = null;
    } else if (USER_DATA_BINDING.equals(clientItfName)) {
      userData = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the ActionListener interface
  // -------------------------------------------------------------------------

  public void actionPerformed (final ActionEvent e) {
    File f = null;
    String UD = System.getProperty("user.dir");
    File fud = new File(UD);
    JFileChooser fileChooser = new JFileChooser();
    String dir = null;
    if (userData != null) {
      try {
        dir = userData.getStringData(UserData.LAST_EXEC_DIR);
      } catch (Exception ignored) {
      }
    }
    fileChooser.setCurrentDirectory(dir == null ? fud : new File(dir));
    fileChooser.setFileSelectionMode (JFileChooser.DIRECTORIES_ONLY);
    fileChooser.setDialogTitle ("Please, select the storage directory");
    if (fileChooser.showOpenDialog (null) != JFileChooser.APPROVE_OPTION) {
      return;
    }
    f = fileChooser.getSelectedFile();
    if (!f.isDirectory()) {
      JOptionPane.showMessageDialog (null,
        f.getName()+": Invalid directory!",
        "Alert ...", JOptionPane.ERROR_MESSAGE);
      return;
    }
    configuration.setStorage(f.getPath());
    if (userData != null) {
      try {
        userData.setStringData(UserData.LAST_EXEC_DIR, f.getAbsolutePath());
        userData.save();
      } catch (Exception ignored) {
      }
    }  
  }
}
