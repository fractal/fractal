/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui;

import org.objectweb.fractal.swing.JTabbedPaneImpl;
import org.objectweb.fractal.gui.model.ConfigurationListener;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Interface;
import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.ServerInterface;

public class TitlePane extends JTabbedPaneImpl
  implements ConfigurationListener
{

  // -------------------------------------------------------------------------
  // Implementation of the ConfigurationListener interface
  // -------------------------------------------------------------------------

  public void changeCountChanged (Component component, long changeCount) {
    updateTitle(component);
  }

  public void rootComponentChanged (final Component oldValue) {
    // does nothing
  }

  public void nameChanged (final Component component, final String oldValue) {
    updateTitle(component);
  }

  public void typeChanged (final Component component, final String oldValue) {
    updateTitle(component);
  }

  public void implementationChanged (
    final Component component,
    final String oldValue)
  {
    updateTitle(component);
  }

  public void interfaceNameChanged (final Interface i, final String oldValue) {
    updateTitle(i.getOwner());
  }

  public void interfaceSignatureChanged (
    final Interface i,
    final String oldValue)
  {
    updateTitle(i.getOwner());
  }

  public void interfaceContingencyChanged (
    final Interface i,
    final boolean oldValue)
  {
    updateTitle(i.getOwner());
  }

  public void interfaceCardinalityChanged (
    final Interface i,
    final boolean oldValue)
  {
    updateTitle(i.getOwner());
  }

  public void clientInterfaceAdded (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    updateTitle(component);
  }

  public void clientInterfaceRemoved (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    updateTitle(component);
  }

  public void serverInterfaceAdded (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    updateTitle(component);
  }

  public void serverInterfaceRemoved (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    updateTitle(component);
  }

  public void interfaceBound (
    final ClientInterface citf, final ServerInterface sitf)
  {
    updateTitle(citf.getOwner());
  }

  public void interfaceRebound (
    final ClientInterface citf,
    final ServerInterface oldSitf)
  {
    updateTitle(citf.getOwner());
  }

  public void interfaceUnbound (
    final ClientInterface citf,
    final ServerInterface sitf)
  {
    updateTitle(citf.getOwner());
  }

  public void attributeControllerChanged (
    final Component component,
    final String oldValue)
  {
    updateTitle(component);
  }

  public void attributeChanged (
    final Component component,
    final String attributeName,
    final String oldValue)
  {
    updateTitle(component);
  }

  public void templateControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    updateTitle(component);
  }

  public void componentControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    updateTitle(component);
  }

  public void subComponentAdded (
    final Component parent,
    final Component child,
    final int index)
  {
    updateTitle(parent);
  }

  public void subComponentRemoved (
    final Component parent,
    final Component child,
    final int index)
  {
    updateTitle(parent);
  }

  // -------------------------------------------------------------------------
  // Other methods
  // -------------------------------------------------------------------------
  protected void updateTitle (final Component c) {
    String title = c.getRootComponent().getName();
    if (c.getConfiguration().getChangeCount() != 0) {
      title += "*";//+c.getConfiguration().getChangeCount();
    }
    if (!getTitleAt(0).equals(title)) {
      setTitleAt(0, title);
    }
  }
}
