/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.repository.lib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.AbstractNode;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.gui.graph.model.GraphModel;
import org.objectweb.fractal.gui.graph.model.Rect;
import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Interface;
import org.objectweb.fractal.gui.model.ServerInterface;
import org.objectweb.fractal.gui.repository.api.Storage;

public class FractalAdlWriter {
  
  protected Storage storage;
    
  Map types = new HashMap();
  
  protected boolean forceInternal;
  
  public String saveTemplate (final Component c, final GraphModel graph) 
    throws Exception 
  {
    AdlNode n = new AdlNode("definition");
    saveComponent(c, n, null, forceInternal, new HashMap());
    if (graph != null) {
      saveCoordinates(graph, c, n);
    }
    storage.store(getComponentType(c), n);
    return getComponentType(c);
  }
  
  public void saveComponent (final Component c, final AdlNode comp, final String name, final boolean internalType, final Map sharing) 
    throws Exception
  {
    saveComponentHeader(c, comp, name, internalType);
    if (c.isComposite()) {
      saveCompositeComponent(c, comp, name, sharing);
    } else {
      savePrimitiveComponent(c, comp, name, sharing);
    }
  }
  
  public void savePrimitiveComponent (final Component c, final AdlNode comp, final String name, final Map sharing) 
    throws Exception
  {
    if (c.getImplementation().length() > 0) {
      AdlNode impl = new AdlNode("content");
      impl.astSetAttribute("class", c.getImplementation());
      comp.astAddNode(impl);
    }    
    saveController(c, comp);
  }

  public void saveCompositeComponent (final Component c, final AdlNode comp, final String name, final Map sharing) 
    throws Exception
  {
    // sub components
    List subComps = c.getSubComponents();
    for (int i = 0; i < subComps.size(); ++i) {
      Component sc = (Component)subComps.get(i);
      boolean shared;
      Component master;
      if (sc.getMasterComponent() != null) {
        shared = true;
        master = sc.getMasterComponent();
      } else if (sc.getSlaveComponents().size() > 0) {
        shared = true;
        master = sc;
      } else {
        shared = false;
        master = null;
      }

      AdlNode subComp = new AdlNode("component");
      if (shared) {
        if (sharing.get(master) != null) {
          subComp.astSetAttribute("name", getComponentName(sc));
          subComp.astSetAttribute("definition", (String)sharing.get(master));
        } else {
          sharing.put(master, getPath(sc));
          if (sc.getType().equals("") || forceInternal) {
            // inline definition
            saveComponent(sc, subComp, getComponentName(sc), true, sharing);
          } else {
            // external definition
            subComp.astSetAttribute("name", getComponentName(sc));
            subComp.astSetAttribute("definition", getComponentType(sc)); 
            AdlNode def = new AdlNode("definition");
            saveComponent(sc, def, null, false, sharing);
            storage.store(getComponentType(sc), def);
          }
        }
      } else {
        if (sc.getType().equals("") || forceInternal) {
          // inline definition
          saveComponent(sc, subComp, getComponentName(sc), true, sharing);
        } else {
          // external definition
          subComp.astSetAttribute("name", getComponentName(sc));
          subComp.astSetAttribute("definition", getComponentType(sc));        
          AdlNode def = new AdlNode("definition");
          saveComponent(sc, def, null, false, sharing);
          storage.store(getComponentType(sc), def);
        }
      }
      comp.astAddNode(subComp);      
    }

    // bindings
    List itfs = c.getServerInterfaces();
    for (int i = 0; i < itfs.size(); ++i) {
      ServerInterface sitf = (ServerInterface)itfs.get(i);
      ClientInterface citf = (ClientInterface)sitf.getComplementaryInterface();
      if (citf.getBinding() != null) {
        AdlNode binding = new AdlNode("binding");
        binding.astSetAttribute("client", "this." + getInterfaceName(citf));
        sitf = citf.getBinding().getServerInterface();
        if (sitf.getOwner() == c) {
          binding.astSetAttribute("server", "this." + getInterfaceName(sitf));
        } else {
          binding.astSetAttribute("server", getComponentName(sitf.getOwner()) + "." + getInterfaceName(sitf));
        }
        comp.astAddNode(binding);
      }
    }
    for (int i = 0; i < subComps.size(); ++i) {
      Component sc = (Component)subComps.get(i);
      if (sc.getMasterComponent() != null) {
        itfs = sc.getMasterComponent().getClientInterfaces();
      } else {
        itfs = sc.getClientInterfaces();
      }
      for (int j = 0; j < itfs.size(); ++j) {
        ClientInterface citf = (ClientInterface)itfs.get(j);
        if (citf.getBinding() != null) {
          AdlNode binding = new AdlNode("binding");
          binding.astSetAttribute("client", getComponentName(sc) + "." + getInterfaceName(citf));
          Interface sitf = citf.getBinding().getServerInterface();
          if (sitf.getOwner() == c) {
            binding.astSetAttribute("server", "this." + getInterfaceName(sitf));
          } else if (sitf.getOwner().getParent() != c) {
            continue;
          } else {
            binding.astSetAttribute("server", getComponentName(sitf.getOwner()) + "." + getInterfaceName(sitf));
          }
          comp.astAddNode(binding);
        }
      }
    }
  
    saveController(c, comp);
  }

  public void saveComponentHeader (final Component c, final AdlNode comp, final String name, final boolean internalType) 
    throws Exception
  {
    if (internalType) {  
      comp.astSetAttribute("name", name != null ? name : getComponentType(c));
      List itfs = c.getServerInterfaces();
      saveInterfaces(itfs, comp, true);
      itfs = c.getClientInterfaces();
      saveInterfaces(itfs, comp, false);
    } else {
      String type = saveType(c);
      comp.astSetAttribute("name", name != null ? name : getComponentType(c));
      comp.astSetAttribute("extends", type);
    }
  }

  public String saveType (final Component c) throws Exception {
    ComponentType ctype = new ComponentType(c);
    String name = (String)types.get(ctype);
    if (name != null) {
      return name;
    }
    name = getComponentType(c) + "Type";
    AdlNode type = new AdlNode("definition");
    type.astSetAttribute("name", name);
    saveInterfaces(c.getServerInterfaces(), type, true);
    saveInterfaces(c.getClientInterfaces(), type, false);    
    storage.store(name, type);
    types.put(ctype, name);
    return name;
  }
  
  public void saveInterfaces (final List itfs, AdlNode itfTypes, boolean server) 
    throws Exception
  {
    for (int i = 0; i < itfs.size(); ++i) {
      Interface itf = (Interface)itfs.get(i);
      if (itf.isCollection() && itf.getMasterCollectionInterface() != null) {
        // do not take into account slave collection interfaces
        continue;
      }
      AdlNode itfType = new AdlNode("interface");
      itfType.astSetAttribute("name", getInterfaceName(itf));
      itfType.astSetAttribute("role", server ? "server" : "client");
      itfType.astSetAttribute("signature", itf.getSignature());
      if (itf.isOptional()) {
        itfType.astSetAttribute("contingency", "optional");
      }
      if (itf.isCollection()) {
        itfType.astSetAttribute("cardinality", "collection");
      }
      itfTypes.astAddNode(itfType);
    }
  }

  public void saveController (final Component c, final AdlNode comp) 
    throws Exception 
  {
    saveAttributes(c, comp);
    if (c.getTemplateControllerDescriptor().length() > 0) {
      AdlNode compDesc = new AdlNode("template-controller");
      compDesc.astSetAttribute("desc", c.getComponentControllerDescriptor());
      comp.astAddNode(compDesc);
    }
    if (c.getComponentControllerDescriptor().length() > 0) {
      AdlNode compDesc = new AdlNode("controller");
      compDesc.astSetAttribute("desc", c.getComponentControllerDescriptor());
      comp.astAddNode(compDesc);
    }
  }

  public void saveAttributes (final Component c, final AdlNode comp) 
    throws Exception
  {
    String attrController = c.getAttributeController();
    List attrNames = c.getAttributeNames();
    if (attrController.length() > 0 || attrNames.size() > 0) {
      AdlNode attrs = new AdlNode("attributes");
      if (attrController.length() > 0) {
        attrs.astSetAttribute("signature", attrController);
      }
      for (int i = 0; i < attrNames.size(); ++i) {
        AdlNode attr = new AdlNode("attribute");
        String attrName = (String)attrNames.get(i);
        attr.astSetAttribute("name", attrName);
        attr.astSetAttribute("value", c.getAttribute(attrName));
        attrs.astAddNode(attr);
      }
      comp.astAddNode(attrs);
    }
  }
  
  public void saveCoordinates (final GraphModel g, final Component c, final AdlNode comp) 
    throws Exception
  {
    List comps = c.getSubComponents();
    for (int i = 0; i < comps.size(); ++i) {
      Component sc = (Component)comps.get(i);
      Rect rect = g.getComponentPosition(sc);
      int color = g.getComponentColor(sc).getRGB();
      AdlNode n = new AdlNode("coordinates");
      n.astSetAttribute("name", sc.getName());
      n.astSetAttribute("x0", Double.toString(rect.x0));
      n.astSetAttribute("y0", Double.toString(rect.y0));
      n.astSetAttribute("x1", Double.toString(rect.x1));
      n.astSetAttribute("y1", Double.toString(rect.y1));
      n.astSetAttribute("color", Integer.toString(color));
      comp.astAddNode(n);
      saveCoordinates(g, sc, n);
    }
  }
   
  String getComponentName (final Component c) {
    String s = c.getName();
    if (c.getMasterComponent() != null) {
      s = c.getMasterComponent().getName();
    }
    if (s.length() == 0) {
      s = "GENERATED-" + Integer.toHexString(c.hashCode());
    }
    return s;
  }

  String getComponentType (final Component c) {
    String s = c.getType();
    if (s.length() == 0) {
      s = "org.objectweb.fractal.gui.tmp.C" + Integer.toHexString(c.hashCode());
    }
    return s;
  }

  String getPath (final Component c) {
    String p = getComponentName(c);
    if (c.getParent() != null && c.getParent().getParent() != null) {
      p = getPath(c.getParent()) + "/" + p;
    }
    return p;
  }
  
  String getInterfaceName (Interface i) {
    if (i.isInternal()) {
      i = i.getComplementaryInterface();
    }
    if (i.getMasterInterface() != null) {
      i = i.getMasterInterface();
    }
    String s = i.getName();
    if (i.getMasterCollectionInterface() != null) {
      if (i.getMasterCollectionInterface().getName().length() == 0) {
        s = getInterfaceName(i.getMasterCollectionInterface()) + s;
      }
    } else if (s.length() == 0) {
      s = "GENERATED-" + Integer.toHexString(i.hashCode());
    }
    return s;
  }

// -------------------------------------------------------------------------

  protected static class AdlNode extends AbstractNode {

    public AdlNode (String type) {
      super(type);
    }

    private Map attributes = new HashMap();

    private List nodes = new ArrayList();
    
    public Map astGetAttributes() {
      return attributes;
    }

    public void astSetAttribute (String name, String value) {
      attributes.put(name, value);
    }
    
    public void astSetAttributes (Map attributes) {
      this.attributes = attributes;
    }

    public String[] astGetNodeTypes () {
      List l = new ArrayList();
      for (int i = 0; i < nodes.size(); ++i) {
        String n = ((Node)nodes.get(i)).astGetType();
        if (!l.contains(n)) {
          l.add(n);
        }
      }
      return (String[])l.toArray(new String[l.size()]);
    }

    public Node[] astGetNodes (String type) {
      List l = new ArrayList();
      for (int i = 0; i < nodes.size(); ++i) {
        Node n = (Node)nodes.get(i);
        if (n.astGetType().equals(type)) {
          l.add(n);
        }
      }
      return (Node[])l.toArray(new Node[l.size()]);
    }

    public void astAddNode (Node n) {
      nodes.add(n);
    }

    public void astRemoveNode(Node n) {
      nodes.remove(n);
    }

    public Node astNewInstance() {
      return new AdlNode(astGetType());
    }
  }

  /**
   * A component type.
   */

  class ComponentType {

    /**
     * The types of the interfaces provided by components of this type.
     */

    private Set provides;

    /**
     * The types of the interfaces required by components of this type.
     */

    private Set requires;

    /**
     * Constructs the {@link FractalAdlWriter.ComponentType} of the given
     * component.
     *
     * @param c a component.
     */

    public ComponentType (final Component c) {
      provides = new HashSet();
      requires = new HashSet();
      List l = c.getServerInterfaces();
      for (int i = 0; i < l.size(); ++i) {
        Interface itf = (Interface)l.get(i);
        if (itf.getMasterCollectionInterface() == null) {
          // do not take into account slave collection interfaces
          provides.add(new InterfaceType(itf));
        }
      }
      l = c.getClientInterfaces();
      for (int i = 0; i < l.size(); ++i) {
        Interface itf = (Interface)l.get(i);
        if (itf.getMasterCollectionInterface() == null) {
          // do not take into account slave collection interfaces
          requires.add(new InterfaceType(itf));
        }
      }
    }

    public boolean equals (final Object o) {
      if (o instanceof ComponentType) {
        ComponentType other = (ComponentType)o;
        if (other.provides.equals(provides)) {
          return other.requires.equals(requires);
        }
      }
      return false;
    }

    public int hashCode () {
      int hashCode = 1;
      Iterator i = provides.iterator();
      while (i.hasNext()) {
        hashCode *= i.next().hashCode();
      }
      i = requires.iterator();
      while (i.hasNext()) {
        hashCode *= i.next().hashCode();
      }
      return hashCode;
    }
  }

  /**
   * An interface type.
   */

  class InterfaceType {

    /**
     * The interface from which this interface type is derived.
     */

    private Interface i;

    /**
     * Constructs the {@link FractalAdlWriter.InterfaceType} of the given
     * interface.
     *
     * @param i an interface.
     */

    public InterfaceType (final Interface i) {
      this.i = i;
    }

    public boolean equals (final Object o) {
      if (o instanceof InterfaceType) {
        Interface j = ((InterfaceType)o).i;
        if (getInterfaceName(i).equals(getInterfaceName(j))) {
          if (i.getSignature().equals(j.getSignature())) {
            if (i.isOptional() == j.isOptional()) {
              return i.isCollection() == j.isCollection();
            }
          }
        }
      }
      return false;
    }

    public int hashCode () {
      return getInterfaceName(i).hashCode();
    }
  }
}
