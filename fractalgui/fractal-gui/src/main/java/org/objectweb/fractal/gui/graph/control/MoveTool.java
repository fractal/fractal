/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.graph.model.Display;
import org.objectweb.fractal.gui.graph.model.Rect;
import org.objectweb.fractal.gui.graph.model.Tools;
import org.objectweb.fractal.gui.graph.view.ComponentPart;

import java.awt.Cursor;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;

/**
 * A controller component to scroll a display. This controller component
 * modifies a display model, in reaction to events emited by a graph view.
 */

public class MoveTool extends EmptyGraphViewListener
  implements BindingController
{

  /**
   * A mandatory client interface bound to a {@link Tools tools} model. This
   * model is used to know if this tool is the currently selected tool or not.
   */

  public final static String TOOLS_BINDING = "tools";

  /**
   * A mandatory client interface bound to a {@link Display display} model. This
   * model is modified by this controller component when the mouse is dragged.
   */

  public final static String DISPLAY_BINDING = "display";

  /**
   * The tools client interface.
   */

  private Tools tools;

  /**
   * The display client interface.
   */

  private Display display;

  /**
   * x coordinate of the point from which the mouse is being dragged.
   */

  private int startX;

  /**
   * y coordinate of the point from which the mouse is being dragged.
   */

  private int startY;

  /**
   * {@link Rect#x0} coordinate of the displayed area, just before the mouse
   * was dragged.
   */

  private double startX0;

  /**
   * {@link Rect#y0} coordinate of the displayed area, just before the mouse
   * was dragged.
   */

  private double startY0;

  /**
   * {@link Rect#x1} coordinate of the displayed area, just before the mouse
   * was dragged.
   */

  private double startX1;

  /**
   * {@link Rect#y1} coordinate of the displayed area, just before the mouse
   * was dragged.
   */

  private double startY1;

  /**
   * If the display must be dragged when a mouse dragged event is received.
   */

  private boolean drag;

  /**
   * Constructs a new {@link MoveTool} component.
   */

  public MoveTool () {
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] { DISPLAY_BINDING, TOOLS_BINDING };
  }

  public Object lookupFc (final String clientItfName) {
    if (DISPLAY_BINDING.equals(clientItfName)) {
      return display;
    } else if (TOOLS_BINDING.equals(clientItfName)) {
      return tools;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (DISPLAY_BINDING.equals(clientItfName)) {
      display = (Display)serverItf;
    } else if (TOOLS_BINDING.equals(clientItfName)) {
      tools = (Tools)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (DISPLAY_BINDING.equals(clientItfName)) {
      display = null;
    } else if (TOOLS_BINDING.equals(clientItfName)) {
      tools = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the GraphViewListener interface
  // -------------------------------------------------------------------------

  public void viewChanged () {
    drag = false;
  }

  public void mousePressed (final MouseEvent e, final ComponentPart p) {
    if (tools.getTool() == Tools.MOVE) {
      startX = e.getX();
      startY = e.getY();
      Rect position = display.getDisplayedArea();
      startX0 = position.x0;
      startY0 = position.y0;
      startX1 = position.x1;
      startY1 = position.y1;
      drag = true;
    } else {
      drag = false;
    }
  }

  public void mouseReleased (final MouseEvent e, final ComponentPart p) {
    drag = false;
  }

  public void mouseClicked (final MouseEvent e, final ComponentPart p) {
    drag = false;
  }

  public void mouseDragged (final MouseEvent e) {
    if (drag) {
      Rectangle r = display.getScreenSize();
      double X0 = startX0 + ((double)(e.getX() - startX))/r.width;
      double Y0 = startY0 + ((double)(e.getY() - startY))/r.height;
      double X1 = startX1 + ((double)(e.getX() - startX))/r.width;
      double Y1 = startY1 + ((double)(e.getY() - startY))/r.height;
      display.setDisplayedArea(new Rect(X0, Y0, X1, Y1));
    }
  }

  public void mouseMoved (final MouseEvent e, final ComponentPart p) {
    // drag = false; // TODO bug JDK1.4? Windows?
    if (tools.getTool() == Tools.MOVE) {
      ((JComponent)e.getSource()).setCursor(
        Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
    }
  }
}
