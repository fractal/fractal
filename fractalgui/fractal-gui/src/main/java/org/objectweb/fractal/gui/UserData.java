/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui;

public interface UserData
{
  final static int  CURRENT_DEPTH = 0;
  final static int  CURRENT_WIDTH = 1;
  final static int  CURRENT_HEIGHT = 2;
  final static int  CURRENT_CONFIG = 3;

  final static int  LAST_OPEN_DIR = 10;
  final static int  LAST_SAVE_DIR = 11;
  final static int  LAST_OPEN_CONF = 12;
  final static int  LAST_OPEN_FILE = 13;
  final static int  LAST_SAVE_FILE = 14;
  final static int  LAST_EXEC_DIR = 15;

  final static int  NB_DIR = 6;
  final static int  START_INDEX = 10;

  final static int  NO_MODIFIED = 0;
  final static int  MODIFIED = 1;

  /**
   * Open the file which contains all registered User Data. If this file doesn't
   * exist, a new empty file is created.
   *
   */
  void open ();

  /**
   * Save all registered data in the file of the user. If this file doesn't
   * exist, a new empty file is created.
   *
   */
  void save ();

  /**
   * Clean the user's data
   */
  void clean ();

  /**
   * set the value for a given type. The type must belong to the CURRENT_
   * family type.
   *
   * @param typ is the Data type.
   * @param v is the int value for the type.
   */
  void setIntData (int typ, int v) throws Exception;

  /**
   * get the int value for a given type. The type must belong to the CURRENT_
   * family type.
   * or else an Exception is thrown.
   *
   * @param typ is the Data type.
   * @return the int data according to the <b>typ</b>
   */
  int getIntData (int typ) throws Exception;

  /**
   * set the value for a given type. The type must belong to the LAST_ family
   * type.
   *
   * @param typ is the Data type.
   * @param s is the String value for the type.
   */
  void setStringData (int typ, String s) throws Exception;

  /**
   * get the int value for a given type. The type must belong to the LAST_
   * family type or else an Exception is thrown.
   *
   * @param typ is the Data type.
   * @return the String data according to the <b>typ</b>
   */
  String getStringData (int typ) throws Exception;

  /**
   * Adds a new FProject to the project list. If the project already exists,
   * nothing is done.
   *
   * @param id is the Data type.
   * @return <b>true</b> if the FProject exist, <b>false</b> otherwise.
   */
  boolean projectExists (int id);

  /**
   * Adds a new FProject to the project list. If the project already exists,
   * nothing is done.
   * If thomething goes wrong an Exception is thrown.
   *
   * @param proj is the FProject to add.
   */
  void addProject (FProject proj) throws Exception;

  /**
   * Removes the FProject with specified <b>id</b> from the project list.
   * If the project doesn't exist, nothing is done.
   *
   * @param id is the FProject id to remove.
   */
  void removeProject (int id);

  /**
   * Adds a new FProject to the project list. If the project already exists,
   * nothing is done.
   * If thomething goes wrong an Exception is thrown.
   *
   * @param id is the FProject identifier.
   * @return the FProject if exists or null otherwise.
   */
  FProject getProject (int id);

  /**
   * Asks for a new FProject.
   *
   * @return the new FProject
   */
  FProject getNewProject ();

  /* ------------------------------------------------------------- */

  public interface FProject {

    /**
     * set the id for the project
     *
     * @param id is the Data type.
     */
    void setId (int id);

    /**
     * get the id of the project
     *
     * @return the identifier of the FRroject.
     */
    int getId ();

    /**
     * set the value for a given type. The type must belong to the LAST_ family
     * type.
     *
     * @param typ is the Data type.
     * @param v is the String value for the type.
     */
    void setStringData (int typ, String s) throws Exception;

    /**
     * get the int value for a given type. The type must belong to the LAST_
     * family type or else an Exception is thrown.
     *
     * @param typ is the Data type.
     */
    String getStringData (int typ) throws Exception;
  }
}
