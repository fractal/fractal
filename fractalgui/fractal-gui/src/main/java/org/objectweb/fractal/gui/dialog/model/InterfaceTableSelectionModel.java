/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.dialog.model;

import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Interface;
import org.objectweb.fractal.gui.model.ServerInterface;
import org.objectweb.fractal.gui.selection.model.Selection;

import javax.swing.DefaultListSelectionModel;

/**
 * A {@link javax.swing.ListSelectionModel} based on a {@link Component} and
 * a {@link Selection} models. This model makes a conversion from a {@link
 * Selection} model to a {@link javax.swing.ListSelectionModel}. It represents
 * the currently selected component interface, as given by the {@link Selection}
 * model.
 */

public class InterfaceTableSelectionModel extends DefaultListSelectionModel {

  /**
   * If this model represents client interfaces or server interfaces.
   */

  private boolean isClient;

  /**
   * The component model on which this model is based.
   */

  private Component model;

  /**
   * The selection model on which this model is based.
   */

  private Selection selection;

  /**
   * Constructs a new {@link InterfaceTableSelectionModel}.
   *
   * @param isClient if this model must represent client or server interfaces.
   */

  InterfaceTableSelectionModel (final boolean isClient) {
    this.isClient = isClient;
    setSelectionMode(SINGLE_SELECTION);
  }

  /**
   * Sets the component model on which this model is based.
   *
   * @param model a component.
   */

  void setComponentModel (final Component model) {
    this.model = model;
  }

  /**
   * Sets the selection model on which this model is based.
   *
   * @param selection a selection model.
   */

  void setSelection (final Selection selection) {
    this.selection = selection;
  }

  /**
   * Notifies this listener that the selection model on which it is based has
   * changed.
   */

  void selectionChanged () {
    int index = -1;
    Object o = selection.getSelection();
    if (o instanceof Interface) {
      Interface i = (Interface)o;
      if (i.isInternal()) {
        i = i.getComplementaryInterface();
      }
      if (i instanceof ClientInterface && isClient) {
        index = i.getOwner().getClientInterfaces().indexOf(i);
      } else if (i instanceof ServerInterface && !isClient) {
        index = i.getOwner().getServerInterfaces().indexOf(i);
      }
    }
    if (index == -1) {
      clearSelection();
    } else {
      super.setSelectionInterval(index, index);
    }
  }

  // -------------------------------------------------------------------------
  // Overriden DefaultListSelectionListener methods
  // -------------------------------------------------------------------------

  public void setSelectionInterval (final int index0, final int index1) {
    super.setSelectionInterval(index0, index1);
    if (model != null && selection != null) {
      Interface i = null;
      try {
        if (isClient) {
          i = (Interface)model.getClientInterfaces().get(index0);
        } else {
          i = (Interface)model.getServerInterfaces().get(index0);
        }
      } catch (Exception e) {
      }
      if (i != null) {
        selection.selectInterface(i);
      }
    }
  }
}
