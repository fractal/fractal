/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.tree.model;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Configuration;
import org.objectweb.fractal.gui.model.ConfigurationListener;
import org.objectweb.fractal.gui.model.Interface;
import org.objectweb.fractal.gui.model.ServerInterface;

import java.util.EventListener;

import javax.swing.event.EventListenerList;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * A {@link TreeModel} based on a {@link Configuration}. This model makes a
 * conversion from a {@link Configuration} model to a {@link TreeModel}. It
 * listens to the configuration in order to update its state when the
 * configuration changes.
 */

public class BasicTreeModel implements
  TreeModel,
  ConfigurationListener,
  BindingController
{

  /**
   * A mandatory client interface bound to a {@link Configuration configuration}
   * model. This is the model on which this model is based and synchronized.
   */

  public final static String CONFIGURATION_BINDING = "configuration";

  /**
   * Root of this tree model when its content is empty.
   */

  private final static Object EMPTY_TREE = new Object();

  /**
   * The configuration client interface.
   */

  private Configuration configuration;

  /**
   * The Swing listeners of this swing tree model.
   */

  private EventListenerList listeners;

  /**
   * Constructs a new {@link BasicTreeModel} component.
   */

  public BasicTreeModel () {
    this.listeners = new EventListenerList();
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] { CONFIGURATION_BINDING };
  }

  public Object lookupFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      return configuration;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = (Configuration)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the ConfigurationListener interface
  // -------------------------------------------------------------------------

  public void changeCountChanged (Component component, long changeCount) {
    // does nothing
  }

  public void rootComponentChanged (final Component oldValue) {
    Object oldRoot = oldValue == null ? EMPTY_TREE : oldValue;
    TreeModelEvent e = new TreeModelEvent(
      this,
      new Object[]{oldRoot},
      null,
      null
    );
    EventListener[] l = listeners.getListeners(TreeModelListener.class);
    for (int i = 0; i < l.length; ++i) {
      ((TreeModelListener)l[i]).treeStructureChanged(e);
    }
  }

  public void nameChanged (final Component component, final String oldValue) {
    if (!configuration.getRootComponent().contains(component)) {
      return;
    }
    TreeModelEvent e;
    if (component == configuration.getRootComponent()) {
      e = new TreeModelEvent(
        this,
        new Object[]{component},
        new int[]{0},
        new Object[]{component});
    } else {
      e = new TreeModelEvent(
        this,
        component.getParent().getPath(),
        new int[]{getIndexOfChild(component.getParent(), component)},
        new Object[]{component});
    }
    EventListener[] l = listeners.getListeners(TreeModelListener.class);
    for (int i = 0; i < l.length; ++i) {
      ((TreeModelListener)l[i]).treeNodesChanged(e);
    }
  }

  public void typeChanged (final Component component, final String oldValue) {
    // does nothing
  }

  public void implementationChanged (
    final Component component,
    final String oldValue)
  {
    // does nothing
  }

  public void interfaceNameChanged (final Interface i, final String oldValue) {
    // does nothing
  }

  public void interfaceSignatureChanged (
    final Interface i,
    final String oldValue)
  {
    // does nothing
  }

  public void interfaceContingencyChanged (
    final Interface i,
    final boolean oldValue)
  {
    // does nothing
  }

  public void interfaceCardinalityChanged (
    final Interface i,
    final boolean oldValue)
  {
    // does nothing
  }

  public void clientInterfaceAdded (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    // does nothing
  }

  public void clientInterfaceRemoved (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    // does nothing
  }

  public void serverInterfaceAdded (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    // does nothing
  }

  public void serverInterfaceRemoved (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    // does nothing
  }

  public void interfaceBound (
    final ClientInterface citf,
    final ServerInterface sitf)
  {
    // does nothing
  }

  public void interfaceRebound (
    final ClientInterface citf,
    final ServerInterface oldSitf)
  {
    // does nothing
  }

  public void interfaceUnbound (
    final ClientInterface citf,
    final ServerInterface sitf)
  {
    // does nothing
  }

  public void attributeControllerChanged (
    final Component component,
    final String oldValue)
  {
    // does nothing
  }

  public void attributeChanged (
    final Component component,
    final String attributeName,
    final String oldValue)
  {
    // does nothing
  }

  public void templateControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    // does nothing
  }

  public void componentControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    // does nothing
  }

  public void subComponentAdded (
    final Component parent,
    final Component child,
    final int index)
  {
    if (!configuration.getRootComponent().contains(parent)) {
      return;
    }
    TreeModelEvent e = new TreeModelEvent(
      this,
      parent.getPath(),
      new int[]{index},
      new Object[]{child});
    EventListener[] l = listeners.getListeners(TreeModelListener.class);
    for (int i = 0; i < l.length; ++i) {
      ((TreeModelListener)l[i]).treeNodesInserted(e);
    }
  }

  public void subComponentRemoved (
    final Component parent,
    final Component child,
    final int index)
  {
    if (!configuration.getRootComponent().contains(parent)) {
      return;
    }
    TreeModelEvent e = new TreeModelEvent(
      this,
      parent.getPath(),
      new int[]{index},
      new Object[]{child});
    EventListener[] l = listeners.getListeners(TreeModelListener.class);
    for (int i = 0; i < l.length; ++i) {
      ((TreeModelListener)l[i]).treeNodesRemoved(e);
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the TreeModel interface
  // -------------------------------------------------------------------------

  public Object getRoot () {
    if (configuration == null) {
      return EMPTY_TREE;
    } else {
      return configuration.getRootComponent();
    }
  }

  public int getChildCount (final Object parent) {
    if (parent == EMPTY_TREE) {
      return 0;
    } else {
      return ((Component)parent).getSubComponents().size();
    }
  }

  public Object getChild (final Object parent, final int index) {
    return ((Component)parent).getSubComponents().get(index);
  }

  public int getIndexOfChild (final Object parent, final Object child) {
    if (parent == null) {
      return 0;
    }
    return ((Component)parent).getSubComponents().indexOf(child);
  }

  public boolean isLeaf (final Object node) {
    if (node == EMPTY_TREE) {
      return true;
    } else {
      return ((Component)node).getSubComponents().size() == 0;
    }
  }

  public void valueForPathChanged (final TreePath path, final Object newValue) {
    // should not be called
  }

  public void addTreeModelListener (final TreeModelListener l) {
    listeners.add(TreeModelListener.class, l);
  }

  public void removeTreeModelListener (final TreeModelListener l) {
    listeners.remove(TreeModelListener.class, l);
  }
}
