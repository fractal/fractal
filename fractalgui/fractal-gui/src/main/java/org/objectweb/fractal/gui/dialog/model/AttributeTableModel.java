/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.dialog.model;

import org.objectweb.fractal.gui.model.Component;

import javax.swing.table.AbstractTableModel;

/**
 * A {@link javax.swing.table.TableModel} based on a {@link Component} model
 * that represents attribute names and values. This model makes a conversion
 * from a {@link Component} model to a {@link javax.swing.table.TableModel}.
 */

public class AttributeTableModel extends AbstractTableModel {

  /**
   * The component model on which this model is based.
   */

  private Component model;

  /**
   * Sets the component model on which this model is based.
   *
   * @param model a component.
   */

  void setComponentModel (final Component model) {
    this.model = model;
    fireTableDataChanged();
  }

  /**
   * Notifies this model that an attribute has changed in {@link #model}.
   *
   * @param attributeName the attribute that has changed.
   * @param oldValue the old value of this attribute.
   */

  void attributeChanged (final String attributeName, final String oldValue) {
    fireTableDataChanged();
  }

  // -------------------------------------------------------------------------
  // Implementation of the TableModel interface
  // -------------------------------------------------------------------------

  public int getRowCount () {
    if (model == null) {
      return 0;
    }
    return model.getAttributeNames().size();
  }

  public int getColumnCount () {
    return 2;
  }

  public String getColumnName (final int column) {
    return column == 0 ? "Name" : "Value";
  }

  public boolean isCellEditable (final int rowIndex, final int columnIndex) {
    return true;
  }

  public Object getValueAt (final int rowIndex, final int columnIndex) {
    Object key = model.getAttributeNames().get(rowIndex);
    if (columnIndex == 0) {
      return key;
    } else {
      return model.getAttribute((String)key);
    }
  }

  public void setValueAt (
    final Object aValue,
    final int rowIndex,
    final int columnIndex)
  {
    String key = (String)model.getAttributeNames().get(rowIndex);
    if (columnIndex == 0) {
      String value = model.getAttribute(key);
      model.setAttribute(key, null);
      model.setAttribute((String)aValue, value);
    } else {
      model.setAttribute(key, (String)aValue);
    }
  }
}
