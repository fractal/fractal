/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.view;

import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Interface;

import java.awt.Rectangle;

/**
 * A component part.
 */

public class ComponentPart {

  /**
   * Header part of a component representation.
   */

  public final static int HEADER = 0;

  /**
   * Top left corner of a component representation.
   */

  public final static int TOP_LEFT_CORNER = 1;

  /**
   * Top right corner of a component representation.
   */

  public final static int TOP_RIGHT_CORNER = 2;

  /**
   * Bottom left corner of a component representation.
   */

  public final static int BOTTOM_LEFT_CORNER = 3;

  /**
   * Bottom right corner of a component representation.
   */

  public final static int BOTTOM_RIGHT_CORNER = 4;

  /**
   * Left border of a component representation.
   */

  public final static int LEFT_BORDER = 5;

  /**
   * Top border of a component representation.
   */

  public final static int TOP_BORDER = 6;

  /**
   * Right border of a component representation.
   */

  public final static int RIGHT_BORDER = 7;

  /**
   * Bottom border of a component representation.
   */

  public final static int BOTTOM_BORDER = 8;

  /**
   * Content part of a component representation.
   */

  public final static int CONTENT = 9;

  /**
   * Interface part of a component representation.
   */

  public final static int INTERFACE = 9;

  /**
   * The component designated by this component part.
   */

  private final Component component;

  /**
   * The component interface designated by this component part.
   * May be <tt>null</tt>.
   */

  private final Interface itf;

  /**
   * The type of this component part. This type must be one of the constants
   * defined in this class.
   */

  private final int part;

  /**
   * The position of the component designated by this component part.
   */

  private final Rectangle position;

  /**
   * Constructs a new {@link ComponentPart} object.
   *
   * @param component the component designated by this component part.
   * @param itf the interface of this component designated by this component
   *      part. May be <tt>null</tt>.
   * @param part the type of this component part.
   * @param position the position of the component designated by this component
   *      part.
   */

  public ComponentPart (
    final Component component,
    final Interface itf,
    final int part,
    final Rectangle position)
  {
    this.component = component;
    this.itf = itf;
    this.part = part;
    this.position = position;
  }

  /**
   * Returns the component designated by this component part.
   *
   * @return the component designated by this component part.
   */

  public Component getComponent () {
    return component;
  }

  /**
   * Returns the precise interface designated by this component part.
   *
   * @return the precise interface designated by this component part, or
   *      <tt>null</tt> if this component part does not designate an interface,
   *      i.e., if {@link #getPart getPart} is not equal to {@link #INTERFACE}.
   */

  public Interface getInterface () {
    return itf;
  }

  /**
   * Returns the type of this component part.
   *
   * @return the type of this component part, as one the constants defined in
   *      this class.
   */

  public int getPart () {
    return part;
  }

  /**
   * Returns the position of the component designated by this component part.
   *
   * @return the position of the component designated by this component part.
   */

  public Rectangle getPosition () {
    return position;
  }
}
