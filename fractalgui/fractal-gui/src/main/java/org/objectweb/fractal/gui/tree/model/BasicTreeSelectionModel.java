/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.tree.model;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Configuration;
import org.objectweb.fractal.gui.model.Interface;
import org.objectweb.fractal.gui.selection.model.Selection;
import org.objectweb.fractal.gui.selection.model.SelectionListener;

import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreePath;

/**
 * A {@link javax.swing.tree.TreeSelectionModel} based on a {@link Selection}.
 * This model makes a conversion from a {@link Selection} model to a {@link
 * javax.swing.tree.TreeSelectionModel}. It listens to the
 * selection in order to update its state when the selection changes.
 */

public class BasicTreeSelectionModel extends DefaultTreeSelectionModel
  implements SelectionListener, BindingController
{

  /**
   * An optional client interface bound to a {@link Configuration configuration}
   * model. The root component of this configuration is changed each time the
   * selection is changed through <i>this</i> model (but not if the selection
   * model on which this model is based changes).
   */

  public final static String CONFIGURATION_BINDING = "configuration";

  /**
   * A mandatory client interface bound to a {@link Selection selection} model.
   * This is the model on which this model is based and synchronized.
   */

  public final static String SELECTION_BINDING = "selection";

  /**
   * The configuration client interface.
   */

  private Configuration configuration;

  /**
   * The selection client interface.
   */

  private Selection selection;

  /**
   * Constructs a new {@link BasicTreeSelectionModel} component.
   */

  public BasicTreeSelectionModel () {
    setSelectionMode(SINGLE_TREE_SELECTION);
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] { CONFIGURATION_BINDING, SELECTION_BINDING };
  }

  public Object lookupFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      return configuration;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      return selection;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = (Configuration)serverItf;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = (Selection)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = null;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the SelectionListener interface
  // -------------------------------------------------------------------------

  public void selectionChanged () {
    if (selection == null) {
      clearSelection();
    } else {
      Object o = selection.getSelection();
      if (o instanceof Interface) {
        o = ((Interface)o).getOwner();
      }
      if (o == null) {
        clearSelection();
      } else {
        super.setSelectionPath(new TreePath(((Component)o).getPath()));
      }
    }
  }

  // -------------------------------------------------------------------------
  // Overriden DefaultTreeSelectionModel methods
  // -------------------------------------------------------------------------

  public void setSelectionPath (final TreePath path) {
    super.setSelectionPath(path);
    Component component = (Component)path.getLastPathComponent();
    if (configuration != null) {
      configuration.setRootComponent(component);
    }
    selection.selectComponent(component);
  }
}
