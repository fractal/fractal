/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.menu.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.model.Configuration;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.repository.api.Repository;
import org.objectweb.fractal.gui.repository.api.Storage;
import org.objectweb.fractal.gui.graph.model.GraphModel;
import org.objectweb.fractal.gui.UserData;
import org.objectweb.fractal.swing.AbstractAction;

import org.objectweb.fractal.gui.model.Interface;

import java.net.URL;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.List;
import java.util.HashMap;

import javax.swing.ImageIcon;
import javax.swing.KeyStroke;
import javax.swing.JOptionPane;

/**
 * An action to save a configuration in a repository.
 */

public class SaveAction extends AbstractAction implements
  BindingController
{

  static String LS = new String (System.getProperty("line.separator"));
  private static int OK = 0;
  private static int WARNING = 1;
  private static int ERROR = 2;
  private HashMap hmt = new HashMap ();

  /**
   * A mandatory client interface bound to a {@link Configuration configuration}
   * model. This is the configuration that is saved by this action.
   */

  public final static String CONFIGURATION_BINDING = "configuration";

  /**
   * An optional server interface bound to a {@link GraphModel graph} model.
   * This is the configuration graphical information that are saved by this
   * action.
   */

  public final static String GRAPH_BINDING = "graph";

  /**
   * A mandatory client interface bound to a {@link Repository repository}. This
   * repository is used to store the configurations in the storage.
   */

  public final static String REPOSITORY_BINDING = "repository";

  /**
   * A mandatory client interface bound to a {@link Storage storage}. This is
   * the storage into which the saved configurations are stored.
   */

  public final static String STORAGE_BINDING = "storage";

  /**
   * An optional client interface bound to a {@link UserData userdata}. This is
   * the storage into/from which some personal data peculiar to each user are
   * written/read.
   */

  public final static String USER_DATA_BINDING = "user-data";

  /**
   * The configuration client interface.
   */

  private Configuration configuration;

  /**
   * The graph client interface.
   */

  private GraphModel graph;

  /**
   * The repository client interface.
   */

  private Repository repository;

  /**
   * The storage client interface.
   */

  private Storage storage;

  /**
   * The user data client interface.
   */

  private UserData userData;

  /**
   * Constructs a new {@link SaveAction} component.
   */

  public SaveAction () {
    putValue(NAME, "Save");
    putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("control S"));
    putValue(SHORT_DESCRIPTION, "Save");
    URL url = getClass().getResource(
      "/org/objectweb/fractal/gui/resources/filesave.gif");
    putValue(SMALL_ICON, new ImageIcon(url));
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------


  public String[] listFc () {
    return new String[] {
      CONFIGURATION_BINDING,
      GRAPH_BINDING,
      REPOSITORY_BINDING,
      STORAGE_BINDING,
      USER_DATA_BINDING
    };
  }

  public Object lookupFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      return configuration;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      return graph;
    } else if (REPOSITORY_BINDING.equals(clientItfName)) {
      return repository;
    } else if (STORAGE_BINDING.equals(clientItfName)) {
      return storage;
    } else if (USER_DATA_BINDING.equals(clientItfName)) {
      return userData;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = (Configuration)serverItf;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      graph = (GraphModel)serverItf;
    } else if (REPOSITORY_BINDING.equals(clientItfName)) {
      repository = (Repository)serverItf;
    } else if (STORAGE_BINDING.equals(clientItfName)) {
      storage = (Storage)serverItf;
    } else if (USER_DATA_BINDING.equals(clientItfName)) {
      userData = (UserData)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = null;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      graph = null;
    } else if (REPOSITORY_BINDING.equals(clientItfName)) {
      repository = null;
    } else if (STORAGE_BINDING.equals(clientItfName)) {
      storage = null;
    } else if (USER_DATA_BINDING.equals(clientItfName)) {
      userData = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the ActionListener interface
  // -------------------------------------------------------------------------

  public void actionPerformed (final ActionEvent e) {
    try {
      File storage = null;
      if (configuration.getStorage() != null) {
        storage = new File(configuration.getStorage());
        if (!storage.exists() || !storage.isDirectory()) {
          storage = null;
        }
      }
      if (storage == null) {
        JOptionPane.showMessageDialog(
          null,
          "A storage directory must be selected before files can be saved", 
          "Error",
          JOptionPane.ERROR_MESSAGE);
      }
      
      // preverification
      int ind = verify (configuration.getRootComponent());

      if (ind == WARNING) {
        Object[] options = { "Yes", "No" };
        int n = JOptionPane.showOptionDialog (null,
        "Configuration not completed\nDo you want to save it anyway ?", "Warning",
        JOptionPane.YES_NO_CANCEL_OPTION,
        JOptionPane.QUESTION_MESSAGE, null,
        options,
        options[0]);
        if (n == 1) return;
      }
      else if (ind == ERROR) return;

      // TODO no longer necessary, or must be changed to the selection of the storage directory.
      /*
      String pwd = null;
      if (userData != null) {
        pwd = userData.getStringData(UserData.LAST_SAVE_DIR);
      }
      if (pwd == null) {
        pwd = System.getProperty("user.dir");
      }
      JFileChooser fileChooser = new JFileChooser();
      fileChooser.setCurrentDirectory(new File(pwd));

      String curfilename = null;
      if (userData != null) {
        curfilename = userData.getStringData(UserData.LAST_OPEN_FILE);
      }
      if (curfilename == null && userData != null) {
        curfilename = userData.getStringData(UserData.LAST_SAVE_FILE);
      }
      if (curfilename != null) {
        fileChooser.setSelectedFile (new File(curfilename));
      }

      fileChooser.addChoosableFileFilter(
        new SimpleFileFilter("fgl", "Fractal GUI files"));
      if (fileChooser.showSaveDialog(null) != JFileChooser.APPROVE_OPTION) {
        return;
      }
      File f = fileChooser.getSelectedFile();
      if (userData != null) {
        userData.setStringData(UserData.LAST_SAVE_FILE, f.getAbsolutePath());
        userData.setStringData(UserData.LAST_SAVE_DIR, f.getParent());
      }
      */
      
      try {
        this.storage.open(storage.getAbsolutePath());
        Component c = configuration.getRootComponent();
        String root = repository.storeComponent(c, graph, null);
        configuration.setChangeCount(0);
      } finally {
        try {
          this.storage.close();
        } catch (Exception ex) {
          JOptionPane.showMessageDialog (null,
            ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
      }
    } catch (Exception ignored) {
      ignored.printStackTrace();
    }
  }

  // ----- verify : recursive verification of configuration

  private int verify (Component c) {
    List subComponents = c.getSubComponents();
    int avert = 0;
    HashMap hm = new HashMap ();
    HashMap hmn = new HashMap ();

    for (int i = 0; i < subComponents.size(); i++) {
      Component subC = (Component)subComponents.get(i);
      if (subC.getStatus() != Component.OK) avert = WARNING;

      if (hmn.get(subC.getName()) != null) {
        JOptionPane.showMessageDialog (null,
        "Error in configuration : two components\nwith the same name ("+subC.getName()
        +")\nin component '"+c.getName()+"'", "Error",
        JOptionPane.ERROR_MESSAGE);
        return ERROR;
      }
      else hmn.put(subC.getName(), "z");

      String typ = subC.getType();
      if ((typ != null) && (typ.length() > 0)) {
        String cpsit = (String)hmt.get(typ);
        if (cpsit != null) {
          /* TODO bug with shared composite components
          if (((subC.isComposite()) && (cpsit.equals("p")))
           || ((!subC.isComposite()) && (cpsit.equals("c")))) {
            JOptionPane.showMessageDialog (null,
              "Error in configuration : the type '"+typ+"' is declared \nboth for "
              +"a composite-template and primitive-template\n"
              +"(see '"+subC.getName()+"')", "Error",
              JOptionPane.ERROR_MESSAGE);
            return ERROR;
          }*/
        }
        else hmt.put(typ, (subC.isComposite() ? "c" : "p"));
      }
      hm.clear();

      List itfc = subC.getClientInterfaces();
      for (int j = 0; j < itfc.size(); ++j) {
        Interface itf = (Interface)itfc.get(j);
        if (itf.getName().length() < 1) continue;  // specific case

        if (itf.getStatus() != Interface.OK) avert = WARNING;
        if (hm.get(itf.getName()) != null) {
          JOptionPane.showMessageDialog (null,
          "     Error in configuration : two client\ninterfaces with the same name ("+itf.getName()
          +")\n    in component '"+subC.getName()+"'", "Error",
          JOptionPane.ERROR_MESSAGE);
          return ERROR;
        }
        else hm.put(itf.getName(), "z");

      }
      hm.clear();

      List itfs = subC.getServerInterfaces();
      for (int k = 0; k < itfs.size(); ++k) {
        Interface itf = (Interface)itfs.get(k);
        if (itf.getName().length() < 1) continue;  // specific case

        if (itf.getStatus() != Interface.OK) avert = WARNING;
        if (hm.get(itf.getName()) != null) {
          JOptionPane.showMessageDialog (null,
          "     Error in configuration : two server\ninterfaces with the same name ("+itf.getName()
          +")\nin component '"+subC.getName()+"'", "Error",
          JOptionPane.ERROR_MESSAGE);
          return ERROR;
        }
        else hm.put(itf.getName(), "z");
      }

      int ind = verify (subC);
      if (ind == ERROR) return ind;
    }
    hmn.clear();
    return avert;
  }
}
