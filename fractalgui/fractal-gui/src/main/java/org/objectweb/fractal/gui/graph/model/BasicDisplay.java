/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.model;

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.gui.UserData;

import java.awt.Rectangle;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Basic implementation of the {@link Display} interface.
 */

public class BasicDisplay implements Display, BindingController {

  /**
   * A collection client interface bound to the {@link DisplayListener
   * listeners} of this model.
   */

  public final static String DISPLAY_LISTENERS_BINDING = "display-listeners";

  /**
   * An optional client interface TODO javadoc.
   */

  public final static String USER_DATA_BINDING = "user-data";

  /**
   * The listeners client interface.
   */

  private Map displayListeners;

  /**
   * The user data client interface.
   */

  private UserData userData;

  /**
   * The size of this display.
   */

  private Rectangle screenSize;

  /**
   * The area of the component graph shown by this display.
   */

  private Rect displayedArea;

  /**
   * <tt>true</tt> if this display is anti aliased.
   */

  private boolean isAntialiasing;

  /**
   * The max depth at which the component graph is shown by this display.
   */

  private int maxDepth;

  /**
   * The max depth at which the component graph is shown by this display.
   */

  private int displayModeForInterface;

  /**
   * Constructs a new {@link BasicDisplay} component.
   */

  public BasicDisplay () {
    displayedArea = new Rect(0.01, 0.01, 0.99, 0.99);
    displayListeners = new HashMap();
    maxDepth = 2;
    displayModeForInterface = 0;
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    int size = displayListeners.size();
    String[] names = new String[size + 1];
    displayListeners.keySet().toArray(names);
    names[size] = USER_DATA_BINDING;
    return names;
  }

  public Object lookupFc (final String clientItfName) {
    if (clientItfName.startsWith(DISPLAY_LISTENERS_BINDING)) {
      return displayListeners.get(clientItfName);
    } else if (USER_DATA_BINDING.equals(clientItfName)) {
      return userData;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (clientItfName.startsWith(DISPLAY_LISTENERS_BINDING)) {
      displayListeners.put(clientItfName, serverItf);
    } else if (USER_DATA_BINDING.equals(clientItfName)) {
      userData = (UserData)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (clientItfName.startsWith(DISPLAY_LISTENERS_BINDING)) {
      displayListeners.remove(clientItfName);
    } else if (USER_DATA_BINDING.equals(clientItfName)) {
      userData = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the Display interface
  // -------------------------------------------------------------------------

  public Rectangle getScreenSize () {
    return new Rectangle(screenSize);
  }

  public void setScreenSize (final Rectangle screenSize) {
    try {
      userData.setIntData(UserData.CURRENT_WIDTH, screenSize.width);
      userData.setIntData(UserData.CURRENT_HEIGHT, screenSize.height);
    } catch (Exception ex)  { }
    this.screenSize = new Rectangle(screenSize);
  }

  public Rect getDisplayedArea () {
    return displayedArea;
  }

  public void setDisplayedArea (final Rect displayedArea) {
    Rect oldDisplayedArea = this.displayedArea;
    if (!displayedArea.equals(oldDisplayedArea)) {
      this.displayedArea = displayedArea;
      Iterator i = displayListeners.values().iterator();
      while (i.hasNext()) {
        DisplayListener l = (DisplayListener)i.next();
        l.displayedAreaChanged(oldDisplayedArea);
      }
    }
  }

  public boolean isAntialiasing () {
    return isAntialiasing;
  }

  public void setIsAntialiasing (final boolean isAntialiasing) {
    if (isAntialiasing != this.isAntialiasing) {
      this.isAntialiasing = isAntialiasing;
      Iterator i = displayListeners.values().iterator();
      while (i.hasNext()) {
        DisplayListener l = (DisplayListener)i.next();
        l.antialiasingChanged();
      }
    }
  }

  public int getMaxDepth () {
    return maxDepth;
  }

  public void setMaxDepth (final int maxDepth) {
    if (maxDepth != this.maxDepth) {
      this.maxDepth = maxDepth;
      Iterator i = displayListeners.values().iterator();
      while (i.hasNext()) {
        DisplayListener l = (DisplayListener)i.next();
        l.maxDepthChanged();
        try {
          userData.setIntData(UserData.CURRENT_DEPTH, maxDepth);
        } catch (Exception ex) { }
      }
    }
  }

  public int getItfNameDisplayMode () {
    return displayModeForInterface;
  }

  public void setItfNameDisplayMode () {
    displayModeForInterface++;
    if (displayModeForInterface > 2) displayModeForInterface = 0;
    Iterator i = displayListeners.values().iterator();
    while (i.hasNext()) {
      DisplayListener l = (DisplayListener)i.next();
      l.itfNameDisplayModeChanged();
    }
  }
}
