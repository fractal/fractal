/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.model;

/**
 * A rectangle whose coordinates are stored as <tt>double</tt>.
 */

public class Rect {

  /**
   * x coordinate of the left border of this rectangle.
   */

  public final double x0;

  /**
   * y coordinate of the top border of this rectangle.
   */

  public final double y0;

  /**
   * x coordinate of the right border of this rectangle.
   */

  public final double x1;

  /**
   * y coordinate of the bottom border of this rectangle.
   */

  public final double y1;

  /**
   * Constructs a new {@link Rect} object.
   *
   * @param x0 x coordinate of the left border of this rectangle.
   * @param y0 y coordinate of the top border of this rectangle.
   * @param x1 x coordinate of the right border of this rectangle.
   * @param y1 y coordinate of the bottom border of this rectangle.
   */

  public Rect (
    final double x0,
    final double y0,
    final double x1,
    final double y1)
  {
    this.x0 = x0;
    this.y0 = y0;
    this.x1 = x1;
    this.y1 = y1;
  }

  public boolean equals (final Object o) {
    if (o instanceof Rect) {
      Rect r = (Rect)o;
      return x0 == r.x0 && y0 == r.y0 && x1 == r.x1 && y1 == r.y1;
    }
    return false;
  }
}
