/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.model;

import org.objectweb.fractal.gui.model.Interface;

/**
 * A model for graph editing tools.
 */

public interface Tools {

  /**
   * The selection tool. This tool can be used to select, move and resize
   * components.
   */

  int SELECT = 0;

  /**
   * The binding tool. This tool can be used to create and remove bindings
   * between components.
   */

  int BIND = 1;

  /**
   * The move tool. This tool can be used to scroll the graph area shown by the
   * display.
   */

  int MOVE = 2;

  /**
   * The zoom in tool. This tool can be used to reduce the graph area shown by
   * the display, i.e. to zoom in.
   */

  int ZOOM_IN = 3;

  /**
   * The zoom out tool. This tool can be used to increase the graph area shown
   * by the display, i.e. to zoom out.
   */

  int ZOOM_OUT = 4;

  /**
   * Returns the currently selected tool.
   *
   * @return the currently selected tool.
   * @see #setTool
   */

  int getTool ();

  /**
   * Sets the currently selected tool.
   *
   * @param tool the new selected tool.
   * @see #getTool
   */

  void setTool (int tool);

  /**
   * Returns the client interface that will be bound by the {@link #BIND} tool.
   *
   * @return the client interface that will be bound by the {@link #BIND} tool.
   * @see #setBindInterface
   */

  Interface getBindInterface ();

  /**
   * Sets the client interface that will be bound by the {@link #BIND} tool.
   *
   * @param i the client interface that will be bound by the {@link #BIND} tool.
   * @see #getBindInterface
   */

  void setBindInterface (Interface i);
}
