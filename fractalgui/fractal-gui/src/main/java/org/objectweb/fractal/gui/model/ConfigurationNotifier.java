/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.model;

import org.objectweb.fractal.api.control.BindingController;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

/**
 * A {@link ConfigurationListener} that notifies other {@link
 * ConfigurationListener}s.
 */

public class ConfigurationNotifier implements
  ConfigurationListener,
  BindingController
{

  /**
   * A collection client interface bound to the {@link ConfigurationListener
   * listeners} of this component.
   */

  public final static String CONFIGURATION_LISTENERS_BINDING =
    "configuration-listeners";

  /**
   * The listeners client interface.
   */

  protected Map listeners;

  /**
   * Constructs a new configuration notifier component.
   */

  public ConfigurationNotifier () {
    listeners = new HashMap();
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return (String[])listeners.keySet().toArray(new String[listeners.size()]);
  }

  public Object lookupFc (final String clientItfName) {
    if (clientItfName.startsWith(CONFIGURATION_LISTENERS_BINDING)) {
      return listeners.get(clientItfName);
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (clientItfName.startsWith(CONFIGURATION_LISTENERS_BINDING)) {
      listeners.put(clientItfName, serverItf);
    }
  }

  public void unbindFc (final String clientItfName) {
    if (clientItfName.startsWith(CONFIGURATION_LISTENERS_BINDING)) {
      listeners.remove(clientItfName);
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the ConfigurationListener interface
  // -------------------------------------------------------------------------

  public void changeCountChanged (Component component, long changeCount) {
    Iterator i = listeners.values().iterator();
    while (i.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)i.next();
      l.changeCountChanged(component, changeCount);
    }
  }

  public void rootComponentChanged (final Component oldValue) {
    Iterator i = listeners.values().iterator();
    while (i.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)i.next();
      l.rootComponentChanged(oldValue);
    }
  }

  public void nameChanged (final Component component, final String oldValue) {
    Iterator i = listeners.values().iterator();
    while (i.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)i.next();
      l.nameChanged(component, oldValue);
    }
  }

  public void typeChanged (final Component component, final String oldValue) {
    Iterator i = listeners.values().iterator();
    while (i.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)i.next();
      l.typeChanged(component, oldValue);
    }
  }

  public void implementationChanged (
    final Component component,
    final String oldValue)
  {
    Iterator i = listeners.values().iterator();
    while (i.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)i.next();
      l.implementationChanged(component, oldValue);
    }
  }

  public void interfaceNameChanged (final Interface i, final String oldValue) {
    Iterator j = listeners.values().iterator();
    while (j.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)j.next();
      l.interfaceNameChanged(i, oldValue);
    }
  }

  public void interfaceSignatureChanged (
    final Interface i,
    final String oldValue)
  {
    Iterator j = listeners.values().iterator();
    while (j.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)j.next();
      l.interfaceSignatureChanged(i, oldValue);
    }
  }

  public void interfaceContingencyChanged (
    final Interface i,
    final boolean oldValue)
  {
    Iterator j = listeners.values().iterator();
    while (j.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)j.next();
      l.interfaceContingencyChanged(i, oldValue);
    }
  }

  public void interfaceCardinalityChanged (
    final Interface i,
    final boolean oldValue)
  {
    Iterator j = listeners.values().iterator();
    while (j.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)j.next();
      l.interfaceCardinalityChanged(i, oldValue);
    }
  }

  public void clientInterfaceAdded (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    Iterator j = listeners.values().iterator();
    while (j.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)j.next();
      l.clientInterfaceAdded(component, i, index);
    }
  }

  public void clientInterfaceRemoved (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    Iterator j = listeners.values().iterator();
    while (j.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)j.next();
      l.clientInterfaceRemoved(component, i, index);
    }
  }

  public void serverInterfaceAdded (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    Iterator j = listeners.values().iterator();
    while (j.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)j.next();
      l.serverInterfaceAdded(component, i, index);
    }
  }

  public void serverInterfaceRemoved (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    Iterator j = listeners.values().iterator();
    while (j.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)j.next();
      l.serverInterfaceRemoved(component, i, index);
    }
  }

  public void interfaceBound (
    final ClientInterface citf, final ServerInterface sitf)
  {
    Iterator i = listeners.values().iterator();
    while (i.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)i.next();
      l.interfaceBound(citf, sitf);
    }
  }

  public void interfaceRebound (
    final ClientInterface citf,
    final ServerInterface oldSitf)
  {
    Iterator i = listeners.values().iterator();
    while (i.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)i.next();
      l.interfaceRebound(citf, oldSitf);
    }
  }

  public void interfaceUnbound (
    final ClientInterface citf,
    final ServerInterface sitf)
  {
    Iterator i = listeners.values().iterator();
    while (i.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)i.next();
      l.interfaceUnbound(citf, sitf);
    }
  }

  public void attributeControllerChanged (
    final Component component,
    final String oldValue)
  {
    Iterator i = listeners.values().iterator();
    while (i.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)i.next();
      l.attributeControllerChanged(component, oldValue);
    }
  }

  public void attributeChanged (
    final Component component,
    final String attributeName,
    final String oldValue)
  {
    Iterator i = listeners.values().iterator();
    while (i.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)i.next();
      l.attributeChanged(component, attributeName, oldValue);
    }
  }

  public void templateControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    Iterator i = listeners.values().iterator();
    while (i.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)i.next();
      l.templateControllerDescriptorChanged(component, oldValue);
    }
  }

  public void componentControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    Iterator i = listeners.values().iterator();
    while (i.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)i.next();
      l.componentControllerDescriptorChanged(component, oldValue);
    }
  }

  public void subComponentAdded (
    final Component parent,
    final Component child,
    final int index)
  {
    Iterator i = listeners.values().iterator();
    while (i.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)i.next();
      l.subComponentAdded(parent, child, index);
    }
  }

  public void subComponentRemoved (
    final Component parent,
    final Component child,
    final int index)
  {
    Iterator i = listeners.values().iterator();
    while (i.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)i.next();
      l.subComponentRemoved(parent, child, index);
    }
  }
}
