/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.repository.lib;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.attributes.AttributeBuilder;
import org.objectweb.fractal.adl.bindings.BindingBuilder;
import org.objectweb.fractal.adl.components.ComponentBuilder;
import org.objectweb.fractal.adl.coordinates.CoordinatesBuilder;
import org.objectweb.fractal.adl.implementations.ImplementationBuilder;
import org.objectweb.fractal.adl.types.TypeBuilder;
import org.objectweb.fractal.gui.graph.model.GraphModel;
import org.objectweb.fractal.gui.graph.model.Rect;
import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Factory;
import org.objectweb.fractal.gui.model.Interface;
import org.objectweb.fractal.gui.model.ServerInterface;

public class FractalGUIBackend implements 
  TypeBuilder, ComponentBuilder, ImplementationBuilder, BindingBuilder, AttributeBuilder, CoordinatesBuilder
{

  public Object createInterfaceType (
    String name, 
    String signature, 
    String role, 
    String contingency, 
    String cardinality,
    Object context) throws Exception
  {
    return new String[] {
      name, signature, role, contingency, cardinality
    };
  }
  
  public Object createComponentType (
    String name, 
    Object[] interfaceTypes, 
    Object context) throws Exception
  {
    return interfaceTypes;
  }

  public Object createComponent (
    Object type, 
    String name,
    String definition,
    Object controllerDesc, 
    Object contentDesc, 
    Object context) throws Exception
  {
    Factory f = (Factory)((Map)context).get("factory");
    Component c = f.createComponent();
    c.setName(name);
    if (definition != null && 
        definition.indexOf(',') == -1 && definition.indexOf('/') == -1) 
    {
      c.setType(definition);
    }
    c.setComponentControllerDescriptor((String)controllerDesc);
    if (contentDesc != null) {
      c.setImplementation((String)contentDesc);
    }
    
    Object[] itfTypes = (Object[])type;
    for (int i = 0; i < itfTypes.length; ++i) {
      String[] itf = (String[])itfTypes[i];
      String itfName = itf[0];
      if (itfName.equals("attribute-controller")) {
        continue;
      }
      String itfSignature = itf[1];
      String itfRole = itf[2];
      String itfContingency = itf[3];
      String itfCardinality = itf[4];
      if (itfRole.equals("server")) {
        ServerInterface sitf = f.createServerInterface(c);
        sitf.setName(itfName);
        sitf.setSignature(itfSignature);
        sitf.setIsOptional("optional".equals(itfContingency));
        sitf.setIsCollection("collection".equals(itfCardinality));
        c.addServerInterface(sitf);
      } else {
        ClientInterface citf = f.createClientInterface(c);
        citf.setName(itfName);
        citf.setSignature(itfSignature);
        citf.setIsOptional("optional".equals(itfContingency));
        citf.setIsCollection("collection".equals(itfCardinality));
        c.addClientInterface(citf);
      }
    }
    
    return c;
  }
  
  public void addComponent (
    Object superComponent, 
    Object subComponent, 
    String name,
    Object context) throws Exception
  {
    Component sc = (Component)superComponent;
    Component c = (Component)subComponent;
    if (c.getParent() == null) {
      sc.addSubComponent(c);
    } else {
      Factory f = (Factory)((Map)context).get("factory");
      Component d = f.createComponent(c);
      d.setName(c.getName());
      sc.addSubComponent(d);
    }
  }
  
  public void bindComponent (
    int type,
    Object client, 
    String clientItf, 
    Object server, 
    String serverItf, 
    Object context) throws Exception
  {
    Component c = (Component)client;
    Component s = (Component)server;
    if (type == NORMAL_BINDING) {
      if (c.getParent() != s.getParent()) {
        if (c.getMasterComponent() != null) {
          c = c.getMasterComponent();
        }
        if (s.getMasterComponent() != null) {
          s = s.getMasterComponent();
        }
        List cl = new ArrayList(c.getSlaveComponents());
        List sl = new ArrayList(s.getSlaveComponents());
        cl.add(c);
        sl.add(s);
        c = null;
        s = null;
        for (int i = 0; i < cl.size(); ++i) {
          for (int j = 0; j < sl.size(); ++j) {
            Component ci = (Component)cl.get(i);
            Component sj = (Component)sl.get(j);
            if (ci.getParent() == sj.getParent()) {
              c = ci;
              s = sj;
              break;
            }
          }
        }
        if (c == null || s == null) {
          throw new RuntimeException("Internal error");
        }
      }
    } else if (type == EXPORT_BINDING) {
      if (c != s.getParent()) {
        if (c.getMasterComponent() != null) {
          c = c.getMasterComponent();
        }
        if (s.getMasterComponent() != null) {
          s = s.getMasterComponent();
        }
        List cl = new ArrayList(c.getSlaveComponents());
        List sl = new ArrayList(s.getSlaveComponents());
        cl.add(c);
        sl.add(s);
        c = null;
        s = null;
        for (int i = 0; i < cl.size(); ++i) {
          for (int j = 0; j < sl.size(); ++j) {
            Component ci = (Component)cl.get(i);
            Component sj = (Component)sl.get(j);
            if (ci == sj.getParent()) {
              c = ci;
              s = sj;
              break;
            }
          }
        }
        if (c == null || s == null) {
          throw new RuntimeException("Internal error");
        }
      }
    } else {
      if (c.getParent() != s) {
        if (c.getMasterComponent() != null) {
          c = c.getMasterComponent();
        }
        if (s.getMasterComponent() != null) {
          s = s.getMasterComponent();
        }
        List cl = new ArrayList(c.getSlaveComponents());
        List sl = new ArrayList(s.getSlaveComponents());
        cl.add(c);
        sl.add(s);
        c = null;
        s = null;
        for (int i = 0; i < cl.size(); ++i) {
          for (int j = 0; j < sl.size(); ++j) {
            Component ci = (Component)cl.get(i);
            Component sj = (Component)sl.get(j);
            if (ci.getParent() == sj) {
              c = ci;
              s = sj;
              break;
            }
          }
        }
        if (c == null || s == null) {
          throw new RuntimeException("Internal error");
        }
      }
    }
    
    Interface citf, sitf;    
    if (type == NORMAL_BINDING) {
      citf = getInterface(c.getClientInterfaces(), clientItf);
      sitf = s.getServerInterface(serverItf);
    } else if (type == EXPORT_BINDING) {
      citf = getInterface(c.getServerInterfaces(), clientItf).getComplementaryInterface();
      sitf = s.getServerInterface(serverItf);
    } else {
      citf = getInterface(c.getClientInterfaces(), clientItf);
      sitf = s.getClientInterface(serverItf).getComplementaryInterface();
    }
    
    c.bind((ClientInterface)citf, clientItf.substring(citf.getName().length()), (ServerInterface)sitf);
  }

  private static Interface getInterface (List interfaces, String name) {
    for (int i = 0; i < interfaces.size(); ++i) {
      Interface itf = (Interface)interfaces.get(i);
      if (itf.isCollection()) {
        if (itf.getMasterCollectionInterface() != null) {
          itf = itf.getMasterCollectionInterface();
        }
        if (name.startsWith(itf.getName())) {
          return itf;
        }
      } else if (name.equals(itf.getName())) {
        return itf;
      }
    }
    throw new RuntimeException("Internal error");
  }
  
  public void startComponent (Object component, Object context) throws Exception {
    // does nothing
  }

  public void setAttribute (
    Object component, 
    String attributeController, 
    String name, 
    String value,
    Object context) throws Exception
  {
    Component c = (Component)component;
    c.setAttributeController(attributeController);
    c.setAttribute(name, value);
  }
  
  public void setCoordinates (
    Object parent,
    Object component, 
    double x0, 
    double y0, 
    double x1, 
    double y1, 
    int color,
    Object context)
  {
    Component c = (Component)component;
    if (c.getParent() != parent) {
      if (c.getMasterComponent() != null) {
        c = c.getMasterComponent();
      }
      if (c.getParent() != parent) {
        List cl = c.getSlaveComponents();
        for (int i = 0; i < cl.size(); ++i) {
          c = (Component)cl.get(i);
          if (c.getParent() == parent) {
            break;
          }
        }
      }
    }
    GraphModel g = (GraphModel)((Map)context).get("graph");
    if (g != null) {
      g.setComponentPosition(c, new Rect(x0, y0, x1, y1));
      g.setComponentColor(c, new Color(color));
    }
  }
}
