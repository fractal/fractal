/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.dialog.view;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.AWTEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.Insets;
import java.awt.Component;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.ArrayList;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import java.lang.reflect.Method;

import java.net.URL;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import javax.swing.BorderFactory;
import javax.swing.ListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.text.Document;
import javax.swing.event.DocumentListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

public class ClassSelector extends JDialog implements
  ActionListener,
  DocumentListener
{
  static String     LS = new String (System.getProperty ("line.separator"));
  static String     FS = new String (System.getProperty ("file.separator"));
  static String     UD = new String (System.getProperty ("user.dir"));
  static String     CP = new String (System.getProperty ("java.class.path"));
  static String     PS = new String (System.getProperty ("path.separator"));

  static String     EXIT = "Exit";
  static String     UP = "Up";
  static String     SELECT = "Select";
  static String     SEARCH = "Implem.";

  static int        SEL_INT = 1;
  static int        SEL_CLA = 2;
  int        		    WIDTH = 600;
  int        		    HEIGHT = 600;

  int               filter = SEL_INT + SEL_CLA;

  List              lp = new ArrayList ();
  List              lpgen = new ArrayList ();

  URL               urlc, urli, urlp;

  ImageIcon         imgClasse;
  ImageIcon         imgInterface;
  ImageIcon         imgPackage;
  JMenu             menu, menuo;
  File			        f;
  FileInputStream   fis;
  FileOutputStream  fos = null;
  PrintStream       of = null;
  Container         cp;
//  Color             buttonColor = new Color(202, 153, 153);
  Color             buttonColor = new Color(204, 255, 204);
  Color             buttonColor1 = new Color(180, 224, 180);
  Color             baseColor = new Color(200, 200, 164);
  Color             classColor = new Color (255, 255, 204);
  Font              buttonFont = new Font("Arial", Font.PLAIN, 9);
  Font              normalFont = new Font("Arial", Font.PLAIN, 11);
  Font              menuFont = new Font("Arial", Font.BOLD, 12);
  Font              listFont = new Font("MonoSpaced", Font.PLAIN, 11);
  int               nc = 0;
  int               MAX = 3;
  JTabbedPane       jtp;
  TreeMap           mapImport = new TreeMap ();

  String []         path = new String[MAX];
  CapLabel []       clb = new CapLabel [MAX];
  DefaultListModel  lmAll;
  CapList           cplAll;
  DefaultListModel  lmClass;
  CapList           cplClass;
  CapTextArea       transit;
  CapButton         b_up = new CapButton (UP, buttonColor1);
  CapButton         b_search = new CapButton (SEARCH, buttonColor1);
  CapButton         b_select = new CapButton (SELECT, buttonColor);
  CapButton         b_exit = new CapButton (EXIT, buttonColor);
  String            libIntrf = "(Interface)";
  String            libClass = "(Class)";
  String            libPackg = "(Package)";
  String []         noms = {
    "Packages, Classes and Interfaces",
    "Looking for Implementation ",
  };
  String            prefix = new String ("");
  String            curInterf = new String ("");
  static String     retour = "";
  int               z = 1;

  public ClassSelector () {
    this.setTitle("Please, make your selection");
    this.setModal(true);

    // ---
    enableEvents (AWTEvent.WINDOW_EVENT_MASK);
    // ---

    urlc = getClass().getResource("/org/objectweb/fractal/gui/resources/classe.gif");
    imgClasse = new ImageIcon(urlc);
    urli = getClass().getResource("/org/objectweb/fractal/gui/resources/interface.gif");
    imgInterface = new ImageIcon(urli);
    urlp = getClass().getResource("/org/objectweb/fractal/gui/resources/package.gif");
    imgPackage = new ImageIcon(urlp);

    cp = getContentPane();
    cp.setForeground (Color.black);

    for (int i = 0; i < 2; i++) {
      clb[i] = new CapLabel (noms[i]);
    }
    lmAll = new DefaultListModel();
    lmClass = new DefaultListModel();
    cplAll = new CapList (lmAll, "All");
    cplClass = new CapList (lmClass, "Class");
    cplAll.setCellRenderer(new MyRenderer(1024));
    cplClass.setCellRenderer(new MyRenderer(1024));
    cplClass.setBackground(classColor);

    transit = new CapTextArea (true);
    transit.getDocument().addDocumentListener(this);
    GridBagLayout gbl = new GridBagLayout ();
    GridBagConstraints gbc = new GridBagConstraints();
    cp.setLayout (gbl);

    gbc.insets = new Insets (5, 2, 2, 5);

    // --- row #0

    gbc.gridy = 0;
    gbc.weightx = 0;
    gbc.weighty = 0;
    gbc.fill = GridBagConstraints.BOTH;
    gbc.anchor = GridBagConstraints.NORTHWEST;
    gbc.gridx = 0;
    b_up.addActionListener(this);
    JPanel jp = new JPanel ();
    jp.setLayout(new BorderLayout());
    jp.add(clb[0], BorderLayout.WEST);
    jp.add(b_up, BorderLayout.EAST);
    gbl.setConstraints(jp, gbc);
    cp.add (jp);

    // --- row #1

    gbc.gridx = 0;
    gbc.gridy = 1;
    gbc.weightx = 1;
    gbc.weighty = 0.8;
    JPanel jp1 = new JPanel ();
    jp1.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
    jp1.add(cplAll);
    jp1.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    JScrollPane jsp = new JScrollPane (jp1);
    jsp.setPreferredSize(new Dimension (WIDTH, 60));
    //jsp.setWheelScrollingEnabled(true); // JDK1.4?
    jsp.getVerticalScrollBar().setUnitIncrement(6);
    jsp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    gbl.setConstraints(jsp, gbc);
    cp.add (jsp);

    // --- row #2

    gbc.gridx = 0;
    gbc.gridy = 2;
    gbc.weightx = 0;
    gbc.weighty = 0;
    b_select.addActionListener(this);
    b_search.addActionListener(this);
    b_exit.addActionListener(this);
    b_select.setUsable(false);
    JPanel jpb = new JPanel (new GridLayout (1, 3));
    jpb.add(b_exit);
    jpb.add(b_select);
    jpb.add(b_search);

    JPanel jpi = new JPanel ();
    jpi.setLayout(new BorderLayout());
    jpi.add(clb[1], BorderLayout.WEST);
    jpi.add(jpb, BorderLayout.EAST);
    gbl.setConstraints(jpi, gbc);
    cp.add (jpi);

    // --- row #3

    gbc.gridx = 0;
    gbc.gridy = 3;
    gbc.weightx = 0;
    gbc.weighty = 1;

    jtp = new JTabbedPane ();

    JPanel jp1c = new JPanel ();
    jp1c.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
    jp1c.add(cplClass);
    jp1c.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    JScrollPane jspc = new JScrollPane (jp1c);
    jspc.setPreferredSize(new Dimension (WIDTH, 60));
    //jspc.setWheelScrollingEnabled(true); // JDK1.4?
    jspc.getVerticalScrollBar().setUnitIncrement(6);
    jspc.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    jtp.addTab ("Implementation(s)", imgClasse, jspc);

    JScrollPane jsinf = new JScrollPane (transit);
    jtp.addTab ("interface's method(s)", jsinf);
    b_up.setUsable(false);
    b_search.setUsable(false);

    gbl.setConstraints(jtp, gbc);
    cp.add (jtp);

    // ---------------------------------

    initList();
    remakeList ();

    // ---------------------------------
    pack ();
    setSize (WIDTH, HEIGHT);
    show ();
  }

  public static String initSelector () {
    new ClassSelector ();
    // ----
    return (retour);
  }

  private void refreshLooking () {
    clb[1].setText(noms[1]);
    b_select.setUsable(false);
    lmClass.clear();
    transit.setText("");
  }

  // ------------------------------------------------------
  // DocumentListener
  // ------------------------------------------------------

  public void insertUpdate(DocumentEvent e) {
    modifyListCommand (e);
  }

  public void removeUpdate(DocumentEvent e) {
    modifyListCommand (e);
  }

  public void changedUpdate(DocumentEvent e) {
    modifyListCommand (e);
  }

  private void modifyListCommand (DocumentEvent e) {
    try {
      Document d = e.getDocument();
      int i = 0;
      String st = d.getText(0, d.getLength());
      transit.setText(st);
    } catch (Exception ex) { }
  }

  // ------------------------------------------------------
  // Utilitaires internes
  // ------------------------------------------------------


  protected void processWindowEvent (WindowEvent e)
  {
    if (e.getID() == WindowEvent.WINDOW_CLOSING) {
      super.processWindowEvent (e);
//      System.exit(0);
    }
  }

  // ------------------------------------------------------
  // ActionListener
  // ------------------------------------------------------

  public void actionPerformed (ActionEvent e) {
    if (e.getActionCommand().equals(EXIT)) {
      if (of != null) of.close();
      retour = "";
      dispose ();
    }
    else if (e.getActionCommand().equals(SELECT)) {
      if (of != null) of.close();
      dispose ();
    }
    else if (e.getActionCommand().equals(UP)) {
      int i = prefix.lastIndexOf('.');
      if (i < 1) {
        prefix = "";
        b_up.setUsable(false);
      }
      else prefix = prefix.substring(0, i);
      remakeList ();
      refreshLooking ();
    }
    else if (e.getActionCommand().equals(SEARCH)) {
      int i = curInterf.lastIndexOf('.');
      if (i < 1) {
        clb[1].setText(noms[1]+"of "+curInterf);
      } else {
        clb[1].setText(noms[1]+"of [...] "+curInterf.substring(i));
      }
      makeClassList ();
      jtp.setSelectedIndex(0);
      b_search.setUsable(false);
    }
  }

  // ------------------------------------------------------
  // initList
  // ------------------------------------------------------

  public void initList () {
    int i = 999999;
    String cp = new String (CP+PS.charAt(0));
    lpgen.clear();

    while (i > 0) {
      i = cp.indexOf(PS.charAt(0)); if (i < 1) break;
      lp.add(cp.substring(0, i));
      cp = cp.substring(i+1);
    }

    for (i = 0; i < lp.size(); i++) {
      String morcif = (String)lp.get(i);

      // --- 1) jar files
      if (morcif.endsWith(".jar")) {
        try {
          JarFile jf = new JarFile (morcif);
          for (Enumeration zipe = jf.entries() ; zipe.hasMoreElements() ;) {
            ZipEntry zip = (ZipEntry)zipe.nextElement();
            if (zip.getName().endsWith(".class")) {
              lpgen.add(zip.getName().replace('/', '.'));
            }
          }
        }
        catch (Exception ex) { ex.printStackTrace(); continue; }
      }

      // --- 2) directories
      else {
        try {
          scanDir (morcif, morcif);
        } catch (Exception ex) { ex.printStackTrace(); continue; }
      }
    }
  }

  // -----

  public void scanDir (String namefile, String base) throws Exception {
    if (namefile == null) return;
    File f = new File (namefile);

    if (f.isDirectory()) {
      File []files = f.listFiles();
      if (files == null) return;
      for (int i = 0; i < files.length; i++) {
        scanDir (namefile+FS+files[i].getName(), base);
      }
    } else {
      if (!(namefile.endsWith(".class"))) return;
      String classname = namefile.substring(base.length()+1);
      lpgen.add (classname.replace(FS.charAt(0), '.'));
    }
  }

  // ------------------------------------------------------
  // remakeList
  // ------------------------------------------------------

  public void remakeList () {
    TreeMap lcomb = new TreeMap ();
    ClassLoader cl = this.getClass().getClassLoader();
    b_search.setUsable(false);

    // --- making and displaying lcomb

    int len = prefix.length();

    for (int i = 0; i < lpgen.size(); i++) {
      String item = (String)lpgen.get(i);
      if ((len > 0) && (!item.startsWith(prefix+"."))) continue;
      if (item.length() <= len) continue;
      String litem;
      if (len == 0) litem = item;
      else litem = item.substring(len+1);
      char c = litem.charAt(0);
      int d;
      if (c > 'Z') d = (int)c-32;
      else d = c;
      try {
        String subName = (char)d+litem.substring(0, litem.indexOf('.'));
        String nature = (String)lcomb.get(subName);
        if (nature != null) continue;
        lcomb.put(subName, item);
      } catch (Exception ignore) { continue; }
    }
    lmAll.clear();

    TreeSet ts = new TreeSet (lcomb.keySet());
    for (Iterator it = ts.iterator(); it.hasNext();) {
      String sta = (String)it.next();
      String st = sta.substring(1);
      if ((st.charAt(0) < 'A') || (st.charAt(0) > 'Z')) {
        lmAll.addElement (new JLabel(putLabel(st, libPackg),
          imgPackage, JLabel.LEFT));
      } else {
        if (prefix.length() > 1) st = prefix+"."+st;
  		  try {
	        Class cla = cl.loadClass (st);
          if (cla.isInterface()) {
            if ((filter & SEL_INT) == 0) continue;
            lmAll.addElement (new JLabel(putLabel(st, libIntrf),
              imgInterface, JLabel.LEFT));
          } else {
            if ((filter & SEL_CLA) == 0) continue;
            lmAll.addElement (new JLabel(putLabel(st, libClass),
              imgClasse, JLabel.LEFT));
          }
        }
        catch (Error er) { continue; }
        catch (Exception ex) { continue; }
      }
    }
    int z = lmAll.getSize();
    while (z < 20) {
      lmAll.addElement(new JLabel("", JLabel.LEFT));
      z++;
    }
  }

  private String putLabel (String st, String label) {
    String white =
      "                                                                   ";
    FontMetrics fm = cplAll.fm;
    int space = fm.charWidth(' ');
    int d = 0;
    for (int z = 0; z < st.length(); z++) d = d + fm.charWidth(st.charAt(z));
    int n = (WIDTH - 140 - d)/space;
    if (n < 1) n = 1;
    return (st+white.substring(0, n)+label);
  }

  // ------------------------------------------------------
  // makeClassList
  // ------------------------------------------------------

  public void makeClassList () {
    TreeMap lcomb = new TreeMap ();
    ClassLoader cl = this.getClass().getClassLoader();

    // --- making and displaying lcomb

    for (int i = 0; i < lpgen.size(); i++) {
      String item = (String)lpgen.get(i);

      int j = item.indexOf(".class");
      if (j < 1) continue;

      try {
	      Class cla = cl.loadClass (item.substring(0, j));
        if (cla.isInterface()) continue;

        Class[] lcl = cla.getInterfaces();
        for (int z = 0; z < lcl.length; z++) {
          if (lcl[z].getName().equals(curInterf)) {
            lcomb.put (item, "zz");
            break;
          }
        }
      }
      catch (Error er) { continue; }
      catch (Exception ex) { continue; }
    }
    lmClass.clear();

    // -----

    TreeSet ts = new TreeSet (lcomb.keySet());
    for (Iterator it = ts.iterator(); it.hasNext();) {
      String sta = (String)it.next();
      String st = sta.substring(0);
      lmClass.addElement (new JLabel(st, JLabel.LEFT));
    }
    int z = lmClass.getSize();
    while (z < 20) {
      lmClass.addElement(new JLabel("", JLabel.LEFT));
      z++;
    }
  }

  // ------------------------------------------------------
  //  display of interface's method(s)
  // ------------------------------------------------------

  public void display () {
    ClassLoader cl = this.getClass().getClassLoader();
    transit.setText("");
    mapImport.clear();

    try {
      Class cla = cl.loadClass (curInterf);
      int in = cla.getName().lastIndexOf('.');
      if (in < 1) {
        transit.append("  interface "+cla.getName()+" ");
      }
      else {
        transit.append("  package "+cla.getName().substring(0, in)+";"+LS+LS);
        transit.append("  interface "+cla.getName().substring(in+1)+" ");
      }

      Class[] interf = cla.getInterfaces();
      if (interf.length == 0) transit.append("{"+LS);
      else transit.append("extends "+LS);
      for (int i = 0; i < interf.length; i++) {
        transit.append("    "+reduc(interf[i].getName()));
        if (i == interf.length-1) transit.append (" {");
        transit.append (LS);
      }
      transit.append (LS);

      Method[] mets = cla.getDeclaredMethods();
      for (int i = 0; i < mets.length; i++) {
        transit.append("    "+reduc(mets[i].getReturnType().getName())+" "
          +reduc(mets[i].getName()));

        Class [] param = mets[i].getParameterTypes();
        if (param.length == 0) {
          transit.append (" ();"+LS+LS);
          continue;
        }
        transit.append(" (");
        char virgule = ',';
        for (int j = 0; j < param.length; j++) {
          if (j == param.length-1) virgule = ')';
          if (j != 0) transit.append("      ");
          transit.append (reduc(param[j].getName())+"  p"+j+virgule+LS);
        }
        transit.append(LS);
      }
      transit.append("  }"+LS);

      if (mapImport.size() == 0) return;

      String total = transit.getText();
      transit.setText("");
      TreeSet ts = new TreeSet (mapImport.keySet());
      int d = 0;
      for (Iterator it = ts.iterator(); it.hasNext();) {
        transit.append ("  import "+(String)it.next()+";"+LS);
      }
      transit.append(LS);
      transit.append(total);
    }
    catch (Error er) { }
    catch (Exception ex) { }
  }

  private String reduc (String st) {
    int i = st.lastIndexOf('.');
    if (i < 1) return st;
    if (st.substring(0, i).equals("java.lang")) return st;
    mapImport.put(st, "zz");
    return st.substring(i+1);
  }

  // ------------------------------------------------------
  //  Inner Class : CapLabel
  // ------------------------------------------------------

  class CapLabel extends JLabel {
    CapLabel (String nom) {
      super ("   "+nom);
      this.setFont(new Font ("Arial", Font.PLAIN, 12));
      this.setForeground(Color.black);
    }
  }

  // ------------------------------------------------------
  //  Inner Class : CapTextArea
  // ------------------------------------------------------

  class CapTextArea extends JTextArea {
    CapTextArea (boolean editable) {
      super ("", 80, 80);
      this.setBackground (Color.white);
      this.setFont(listFont);
      this.setBorder(BorderFactory.createLoweredBevelBorder());
      this.setEditable(editable);
    }
  }

  // ------------------------------------------------------
  //  Inner Class : CapButton
  // ------------------------------------------------------

  class CapButton extends JButton {
    CapButton (String lib, Color color) {
      super (lib);
//      setBackground (buttonColor);
      this.setBackground (color);
      this.setForeground (Color.black);
      this.setFont(buttonFont);
      FontMetrics fm = this.getFontMetrics(buttonFont);
		  int d = 0;
  		for (int z = 0; z< lib.length(); z++) d = d + fm.charWidth(lib.charAt(z));
      this.setPreferredSize (new Dimension (36+d, 10));
      this.setEnabled(true);
    }

    public void setUsable (boolean b) {
      this.setEnabled (b);
      this.setOpaque (b);
    }
  }

  // ------------------------------------------------------
  //  Inner Class : CapList
  // ------------------------------------------------------

  class CapList extends JList implements ListSelectionListener {
    DefaultListModel listmodel;
    String ident = null;
    FontMetrics fm;

    CapList (DefaultListModel listModel, String id) {
      super (listModel);
      this.setBackground (Color.white);
      this.setFont(listFont);
      fm = this.getFontMetrics(this.getFont());
      setBorder(BorderFactory.createLoweredBevelBorder());
      listmodel = listModel;
      this.addListSelectionListener(this);
      ident = new String (id);
    }

    // ----- ListSelectionListener

    public void valueChanged (ListSelectionEvent e) {
      if (e.getValueIsAdjusting() == false) {

        if (this.getSelectedIndex() > -1) {
          JLabel jlab = (JLabel)listmodel.getElementAt(this.getSelectedIndex());
          String texte = jlab.getText();

          String util = null;
          int i = texte.indexOf(' ');
          if (i > 0) util = texte.substring(0, i);
          else util = texte;

          // -- Package
          if (texte.endsWith(libPackg)) {
            if (prefix.length() < 1) {
              prefix = prefix.concat(util);
            } else {
              prefix = prefix.concat("."+util);
            }
            remakeList ();
            b_up.setUsable(true);
            refreshLooking ();
            retour = "";
          }

          // -- Interface
          else if (texte.endsWith(libIntrf)) {
            b_search.setUsable(true);
            curInterf = util;
            retour = "";
            jtp.setSelectedIndex(1);
            refreshLooking ();
            display ();
          }

          // -- Class
          else {
            if (ident.equals("Class")) retour = new String(texte);
            else retour = new String (util);
            b_select.setUsable(true);
          }
        }
      }
    }
  }

  // ------------------------------------------------------
  // Inner Class : MyRenderer
  // ------------------------------------------------------

  class MyRenderer extends JLabel implements ListCellRenderer {
    int w;

    public MyRenderer (int width) {
      setOpaque(true);
      setHorizontalAlignment(LEFT);
      setVerticalAlignment(CENTER);
      w = width;
    }

    public Component getListCellRendererComponent (
      JList list,
      Object value,
      int index,
      boolean isSelected, boolean cellHasFocus) {

      if (isSelected) {
        this.setBackground(list.getSelectionBackground());
        this.setForeground(list.getSelectionForeground());
      } else {
        this.setBackground(list.getBackground());
        this.setForeground(list.getForeground());
      }

      if (value instanceof JLabel) {
        JLabel jlab = (JLabel)value;
        ImageIcon icon = (ImageIcon)jlab.getIcon();
        setText((String)jlab.getText());
        setIcon (icon);
        this.setFont (listFont);
        setPreferredSize(new Dimension(w, 17));
      }
      return this;
    }
  }
  // ----- fin classe
}
