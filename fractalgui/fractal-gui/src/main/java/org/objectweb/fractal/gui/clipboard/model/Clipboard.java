/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.clipboard.model;

import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Factory;
import org.objectweb.fractal.gui.graph.model.GraphModel;

/**
 * A model for the clipboard.
 */

public interface Clipboard {

  /**
   * Returns <tt>true</tt> if the given component can be cut.
   *
   * @param srcComponent the component to be cut.
   * @return <tt>true</tt> if the given component can be cut.
   */

  boolean canCut (Component srcComponent);

  /**
   * Cuts the given component.
   *
   * @param srcComponent the component to be cut.
   * @param srcGraph the graph model that contains the coordinates of the
   *      component.
   * @param srcFactory the factory to be used to create a clone of the given
   *      component. This clone is then stored in the clipboard.
   */

  void cut (Component srcComponent, GraphModel srcGraph, Factory srcFactory);

  /**
   * Returns <tt>true</tt> if the given component can be copied.
   *
   * @param srcComponent the component to be copied.
   * @return <tt>true</tt> if the given component can be copie.
   */

  boolean canCopy (Component srcComponent);

  /**
   * Copies the given component.
   *
   * @param srcComponent the component to be copied.
   * @param srcGraph the graph model that contains the coordinates of the
   *      component.
   * @param srcFactory the factory to be used to create a clone of the given
   *      component. This clone is then stored in the clipboard.
   */

  void copy (Component srcComponent, GraphModel srcGraph, Factory srcFactory);

  /**
   * Returns <tt>true</tt> if the clipboard's content can be pasted into the
   * given component.
   *
   * @param dstComponent the component into which the clipboard's content must
   *      be pasted.
   * @return <tt>true</tt> if the clipboard is not empty, and if its content can
   *      be pasted into the given component.
   */

  boolean canPaste (Component dstComponent);

  /**
   * Pastes the clipboard's content into the given component.
   *
   * @param dstComponent the component into which the clipboard's content must
   *      be pasted.
   * @param dstGraph the graph model into which the coordinates of the
   *      clipboard's component must be pasted.
   * @param dstFactory the factory to be used to create a clone of the
   *      clipboard's content. This clone is then added as a sub component of
   *      the given component.
   */

  void paste (Component dstComponent, GraphModel dstGraph, Factory dstFactory);

  /**
   * Returns <tt>true</tt> if the clipboard's content can be pasted into the
   * given component as a shared component.
   *
   * @param dstComponent the component into which the clipboard's content must
   *      be pasted as a shared component.
   * @return <tt>true</tt> if the clipboard is not empty, and if its content can
   *      be pasted into the given component as a shared component.
   */

  boolean canPasteAsShared (Component dstComponent);

  /**
   * Pastes the clipboard's content into the given component, as a shared
   * component.
   *
   * @param dstComponent the component into which the clipboard's content must
   *      be pasted.
   * @param dstGraph the graph model into which the coordinates of the
   *      clipboard's component must be pasted.
   * @param dstFactory the factory to be used to create a slave component of the
   *      clipboard's content. This slave component is then added as a sub
   *      component of the given component.
   */

  void pasteAsShared (
    Component dstComponent,
    GraphModel dstGraph,
    Factory dstFactory);
}
