/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.graph.model.GraphModel;
import org.objectweb.fractal.gui.graph.model.Rect;
import org.objectweb.fractal.gui.graph.model.Tools;
import org.objectweb.fractal.gui.graph.view.ComponentPart;
import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Configuration;
import org.objectweb.fractal.gui.selection.model.Selection;

import java.awt.Cursor;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;

/**
 * A controller component to select, move and resize components. This controller
 * component modifies a graph model, in reaction to events emited by a graph
 * view.
 */

public class SelectTool extends EmptyGraphViewListener
  implements BindingController
{

  /**
   * A mandatory client interface bound to a {@link Tools tools} model. This
   * model is used to know if this tool is the currently selected tool or not.
   */

  public final static String TOOLS_BINDING = "tools";

  /**
   * A mandatory client interface bound to a {@link Configuration configuration}
   * model. When the user double clicks on a componnt, this tools changes the
   * root component of this configuration to the previous component.
   */

  public final static String CONFIGURATION_BINDING = "configuration";

  /**
   * A mandatory client interface bound to a {@link GraphModel graph} model.
   * This is the graph model that is used to move and resize the components.
   */

  public final static String GRAPH_BINDING = "graph";

  /**
   * A mandatory client interface bound to a {@link Selection selection} model.
   * This model is used to select components or interfaces, or to know the
   * component that must be moved or resized.
   */

  public final static String SELECTION_BINDING = "selection";

  /**
   * The tools client interface.
   */

  private Tools tools;

  /**
   * The configuratioon client interface.
   */

  private Configuration configuration;

  /**
   * The graph model client interface.
   */

  private GraphModel graph;

  /**
   * The selection client interface.
   */

  private Selection selection;

  /**
   * x coordinate of the point from which the mouse is being dragged.
   */

  private int startX;

  /**
   * y coordinate of the point from which the mouse is being dragged.
   */

  private int startY;

  /**
   * {@link Rect#x0} coordinate of the moved or resized component, just before
   * the mouse was dragged.
   */

  private double startX0;

  /**
   * {@link Rect#y0} coordinate of the moved or resized component, just before
   * the mouse was dragged.
   */

  private double startY0;

  /**
   * {@link Rect#x1} coordinate of the moved or resized component, just before
   * the mouse was dragged.
   */

  private double startX1;

  /**
   * {@link Rect#y1} coordinate of the moved or resized component, just before
   * the mouse was dragged.
   */

  private double startY1;

  /**
   * The component part that is moved (a corner, a border, of all the
   * component).
   */

  private ComponentPart draggedPart;

  /**
   * Constructs a new {@link SelectTool} component.
   */

  public SelectTool () {
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] {
      TOOLS_BINDING,
      CONFIGURATION_BINDING,
      GRAPH_BINDING,
      SELECTION_BINDING
    };
  }

  public Object lookupFc (final String clientItfName) {
    if (TOOLS_BINDING.equals(clientItfName)) {
      return tools;
    } else if (CONFIGURATION_BINDING.equals(clientItfName)) {
      return configuration;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      return graph;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      return selection;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (TOOLS_BINDING.equals(clientItfName)) {
      tools = (Tools)serverItf;
    } else if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = (Configuration)serverItf;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      graph = (GraphModel)serverItf;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = (Selection)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (TOOLS_BINDING.equals(clientItfName)) {
      tools = null;
    } else if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = null;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      graph = null;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = null;
    }
  }

  // -------------------------------------------------------------------------
  // Overriden EmptyGraphViewListener methods
  // -------------------------------------------------------------------------

  public void viewChanged () {
    draggedPart = null;
  }

  public void mousePressed (final MouseEvent e, final ComponentPart p) {
    if (tools.getTool() != Tools.SELECT) {
      return;
    }
    draggedPart = null;
    if (p.getComponent() != null) {
      if (p.getInterface() != null) {
        selection.selectInterface(p.getInterface());
        if (p.getInterface() instanceof ClientInterface) {
          tools.setBindInterface(p.getInterface());
          tools.setTool(Tools.BIND);
          e.consume();
        }
      } else {
        Component c = p.getComponent();
        if (e.getClickCount() == 2) {
          if (c.getMasterComponent() != null) {
            c = c.getMasterComponent();
          }
          selection.selectComponent(c);
          configuration.setRootComponent(c);
        } else {
          selection.selectComponent(c);
        }
        if (c != configuration.getRootComponent()) {
          Rect position = graph.getComponentPosition(c);
          startX = e.getX();
          startY = e.getY();
          startX0 = position.x0;
          startY0 = position.y0;
          startX1 = position.x1;
          startY1 = position.y1;
          draggedPart = p;
        }
      }
    }
  }

  public void mouseReleased (final MouseEvent e, final ComponentPart p) {
    draggedPart = null;
  }

  public void mouseClicked (final MouseEvent e, final ComponentPart p) {
    draggedPart = null;
  }

  public void mouseDragged (final MouseEvent e) {
    if (draggedPart != null) {
      Rectangle r = draggedPart.getPosition();
      int x0 = r.x;
      int y0 = r.y;
      int x1 = x0 + r.width;
      int y1 = y0 + r.height;
      int x = e.getX();
      int y = e.getY();
      switch (draggedPart.getPart()) {
        case ComponentPart.HEADER:
        case ComponentPart.CONTENT:
          x0 += x - startX;
          y0 += y - startY;
          x1 += x - startX;
          y1 += y - startY;
          break;
        case ComponentPart.TOP_LEFT_CORNER:
          x0 = x;
          y0 = y;
          break;
        case ComponentPart.TOP_RIGHT_CORNER:
          x1 = x;
          y0 = y;
          break;
        case ComponentPart.BOTTOM_LEFT_CORNER:
          x0 = x;
          y1 = y;
          break;
        case ComponentPart.BOTTOM_RIGHT_CORNER:
          x1 = x;
          y1 = y;
          break;
        case ComponentPart.LEFT_BORDER:
          x0 = x;
          break;
        case ComponentPart.TOP_BORDER:
          y0 = y;
          break;
        case ComponentPart.RIGHT_BORDER:
          x1 = x;
          break;
        case ComponentPart.BOTTOM_BORDER:
          y1 = y;
          break;
        default:
          break;
      }
      double X0 = ((x0 - r.x)*(startX1 - startX0))/r.width + startX0;
      double Y0 = ((y0 - r.y)*(startY1 - startY0))/r.height + startY0;
      double X1 = ((x1 - r.x)*(startX1 - startX0))/r.width + startX0;
      double Y1 = ((y1 - r.y)*(startY1 - startY0))/r.height + startY0;
      switch (draggedPart.getPart()) {
        case ComponentPart.HEADER:
        case ComponentPart.CONTENT:
          if (X0 < 0) {
            X1 = X1 - X0;
            X0 = 0;
          } else if (X1 > 1) {
            X0 = X0 - X1 + 1;
            X1 = 1;
          }
          if (Y0 < 0) {
            Y1 = Y1 - Y0;
            Y0 = 0;
          } else if (Y1 > 1) {
            Y0 = Y0 - Y1 + 1;
            Y1 = 1;
          }
          break;
        case ComponentPart.TOP_LEFT_CORNER:
          X0 = Math.max(0, Math.min(X0, X1));
          Y0 = Math.max(0, Math.min(Y0, Y1));
          break;
        case ComponentPart.TOP_RIGHT_CORNER:
          X1 = Math.max(X0, Math.min(X1, 1));
          Y0 = Math.max(0, Math.min(Y0, Y1));
          break;
        case ComponentPart.BOTTOM_LEFT_CORNER:
          X0 = Math.max(0, Math.min(X0, X1));
          Y1 = Math.max(Y0, Math.min(Y1, 1));
          break;
        case ComponentPart.BOTTOM_RIGHT_CORNER:
          X1 = Math.max(X0, Math.min(X1, 1));
          Y1 = Math.max(Y0, Math.min(Y1, 1));
          break;
        case ComponentPart.LEFT_BORDER:
          X0 = Math.max(0, Math.min(X0, X1));
          break;
        case ComponentPart.TOP_BORDER:
          Y0 = Math.max(0, Math.min(Y0, Y1));
          break;
        case ComponentPart.RIGHT_BORDER:
          X1 = Math.max(X0, Math.min(X1, 1));
          break;
        case ComponentPart.BOTTOM_BORDER:
          Y1 = Math.max(Y0, Math.min(Y1, 1));
          break;
        default:
          break;
      }
      graph.setComponentPosition(
        draggedPart.getComponent(),
        new Rect(X0, Y0, X1, Y1));
    }
  }

  public void mouseMoved (final MouseEvent e, final ComponentPart p) {
    // draggedPart = null; // TODO bug JDK1.4? Windows?
    if (tools.getTool() == Tools.SELECT) {
      updateCursor((JComponent)e.getSource(), p);
    }
  }

  // -------------------------------------------------------------------------
  // Other methods
  // -------------------------------------------------------------------------

  /**
   * Updates the cursor of the given Swing component, depending above which
   * component part it is.
   *
   * @param component the Swing component whose cursor must be updated.
   * @param p the component part above which the cursor is.
   */

  private void updateCursor (
    final JComponent component,
    final ComponentPart p)
  {
    int type = Cursor.DEFAULT_CURSOR;
    if (p.getComponent() != null &&
        p.getComponent() != configuration.getRootComponent())
    {
      switch (p.getPart()) {
        case ComponentPart.HEADER:
          type = Cursor.MOVE_CURSOR;
          break;
        case ComponentPart.TOP_LEFT_CORNER:
          type = Cursor.NW_RESIZE_CURSOR;
          break;
        case ComponentPart.TOP_RIGHT_CORNER:
          type = Cursor.NE_RESIZE_CURSOR;
          break;
        case ComponentPart.BOTTOM_LEFT_CORNER:
          type = Cursor.SW_RESIZE_CURSOR;
          break;
        case ComponentPart.BOTTOM_RIGHT_CORNER:
          type = Cursor.SE_RESIZE_CURSOR;
          break;
        case ComponentPart.LEFT_BORDER:
          type = Cursor.W_RESIZE_CURSOR;
          break;
        case ComponentPart.TOP_BORDER:
          type = Cursor.N_RESIZE_CURSOR;
          break;
        case ComponentPart.RIGHT_BORDER:
          type = Cursor.E_RESIZE_CURSOR;
          break;
        case ComponentPart.BOTTOM_BORDER:
          type = Cursor.S_RESIZE_CURSOR;
          break;
        default:
          break;
      }
    }
    component.setCursor(Cursor.getPredefinedCursor(type));
  }
}
