/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.view;

import java.awt.event.MouseEvent;

/**
 * An interface to be notified of events emitted by a graph view component.
 */

public interface GraphViewListener {

  /**
   * Notifies this listener that the model represented by the view has changed. 
   */

  void viewChanged ();

  /**
   * Notifies this listener that the mouse has been pressed.
   *
   * @param e a mouse pressed mouse event.
   * @param p the component part corresponding to the event's coordinates.
   */

  void mousePressed (MouseEvent e, ComponentPart p);

  /**
   * Notifies this listener that the mouse has been released.
   *
   * @param e a mouse released mouse event.
   * @param p the component part corresponding to the event's coordinates.
   */

  void mouseReleased (MouseEvent e, ComponentPart p);

  /**
   * Notifies this listener that the mouse has been clicked.
   *
   * @param e a mouse clicke mouse event.
   * @param p the component part corresponding to the event's coordinates.
   */

  void mouseClicked (MouseEvent e, ComponentPart p);

  /**
   * Notifies this listener that the mouse has entered the graph view.
   *
   * @param e a mouse entered mouse event.
   */

  void mouseEntered (MouseEvent e);

  /**
   * Notifies this listener that the mouse has exited the graph view.
   *
   * @param e a mouse exited mouse event.
   */

  void mouseExited (MouseEvent e);

  /**
   * Notifies this listener that the mouse has been dragged in the graph view.
   *
   * @param e a mouse dragged mouse event.
   */

  void mouseDragged (MouseEvent e);

  /**
   * Notifies this listener that the mouse has mouved in the graph view.
   *
   * @param e a mouse moved mouse event.
   * @param p the component part corresponding to the event's coordinates.
   */

  void mouseMoved (MouseEvent e, ComponentPart p);
}
