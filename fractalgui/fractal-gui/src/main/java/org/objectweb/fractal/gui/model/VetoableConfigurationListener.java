/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.model;

/**
 * An interface to veto changes in a {@link Configuration} model.
 */

public interface VetoableConfigurationListener {

  /**
   * Notifies this listener that the root component is about to change.
   */

  void canChangeRootComponent ();

  // -------------------------------------------------------------------------
  // Name, Type, Implementation
  // -------------------------------------------------------------------------

  /**
   * Notifies this listener that the name of a component is about to change.
   *
   * @param component the component whose name is about to change.
   */

  void canChangeName (Component component);

  /**
   * Notifies this listener that the type of a component is about to change.
   *
   * @param component the component whose type is about to change.
   */

  void canChangeType (Component component);

  /**
   * Notifies this listener that the implementation of a component is about to
   * change.
   *
   * @param component the component whose implementation is about to change.
   */

  void canChangeImplementation (Component component);

  // -------------------------------------------------------------------------
  // Client and server interfaces
  // -------------------------------------------------------------------------

  /**
   * Notifies this listener that the name of an interface is about to change.
   *
   * @param i the interface whose name is about to change.
   */

  void canChangeInterfaceName (Interface i);

  /**
   * Notifies this listener that the signature of an interface is about to
   * change.
   *
   * @param i the interface whose signature is about to change.
   */

  void canChangeInterfaceSignature (Interface i);

  /**
   * Notifies this listener that the contigency of an interface is about to
   * change.
   *
   * @param i the interface whose contingency is about to change.
   */

  void canChangeInterfaceContingency (Interface i);

  /**
   * Notifies this listener that the cardinality of an interface is about to
   * change.
   *
   * @param i the interface whose cardinality is about to change.
   */

  void canChangeInterfaceCardinality (Interface i);

  /**
   * Notifies this listener that a client interface is about to be added to a
   * component.
   *
   * @param component the component into which the new interface is about to be
   *      added.
   * @param i the interface that is about to be added.
   */

  void canAddClientInterface (Component component, ClientInterface i);

  /**
   * Notifies this listener that a client interface is about to be removed from
   * a component.
   *
   * @param component the component from which the interface is about to be
   *      removed.
   * @param i the interface that is about to be removed.
   */

  void canRemoveClientInterface (Component component, ClientInterface i);

  /**
   * Notifies this listener that a server interface is about to be added to a
   * component.
   *
   * @param component the component into which the new interface is about to be
   *      added.
   * @param i the interface that is about to be added.
   */

  void canAddServerInterface (Component component, ServerInterface i);

  /**
   * Notifies this listener that a server interface is about to be removed from
   * a component.
   *
   * @param component the component from which the interface is about to be
   *      removed.
   * @param i the interface that is about to be removed.
   */

  void canRemoveServerInterface (Component component, ServerInterface i);

  // -------------------------------------------------------------------------
  // Bindings
  // -------------------------------------------------------------------------

  /**
   * Notifies this listener that an interface is about to be bound.
   *
   * @param citf the client interface that is about to be bound.
   */

  void canBindInterface (ClientInterface citf);

  /**
   * Notifies this listener that an interface is about to be rebound.
   *
   * @param citf the client interface that is about to be rebound.
   */

  void canRebindInterface (ClientInterface citf);

  /**
   * Notifies this listener that an interface is about to be unbound.
   *
   * @param citf the client interface that is about to be unbound.
   */

  void canUnbindInterface (ClientInterface citf);

  // -------------------------------------------------------------------------
  // Attributes
  // -------------------------------------------------------------------------

  /**
   * Notifies this listener that the attribute controller of a component is
   * about to change.
   *
   * @param component the component whose attribute controller is about to
   *      change.
   */

  void canChangeAttributeController (Component component);

  /**
   * Notifies this listener that the value of an attribute is about to change.
   *
   * @param component the component whose attribute is about to change.
   * @param attributeName the name of the attribute whose value is about to
   *      change.
   */

  void canChangeAttribute (Component component, String attributeName);

  // -------------------------------------------------------------------------
  // Controller descriptors
  // -------------------------------------------------------------------------

  /**
   * Notifies this listener that the template controller descriptor of a
   * component is about to change.
   *
   * @param component the component whose template contoller descriptor is
   *      about to change.
   */

  void canChangeTemplateControllerDescriptor (Component component);

  /**
   * Notifies this listener that the component controller descriptor of a
   * component is about to change.
   *
   * @param component the component whose component contoller descriptor is
   *      about to change.
   */

  void canChangeComponentControllerDescriptor (Component component);

  // -------------------------------------------------------------------------
  // Composite specific informations
  // -------------------------------------------------------------------------

  /**
   * Notifies this listener that a sub component is about to be added to a
   * component.
   *
   * @param parent the component into which the sub component is about to be
   *      added.
   * @param child the sub component that is about to be added.
   */

  void canAddSubComponent (Component parent, Component child);

  /**
   * Notifies this listener that a sub component is about to be removed from a
   * component.
   *
   * @param parent the component from which the sub component is about to be
   *      removed.
   * @param child the sub component that is about to be removed.
   */

  void canRemoveSubComponent (Component parent, Component child);
}
