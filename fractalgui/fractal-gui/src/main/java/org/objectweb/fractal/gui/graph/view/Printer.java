/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.view;

import org.objectweb.fractal.gui.model.Component;

/**
 * An interface to print components.
 */

public interface Printer {

	/**
	 * For printing.
	 */
	static final int  TYPE_PRINT     = 0;

	/**
	 * For exportation as a SVG File
	 */
	static final int  TYPE_EXPORT    = 1;

  /**
   * Prints the given component.
   *
   * @param ask TODO javadoc.
   * @param c the component to be printed.
   */
  void setType (int ntype);
  boolean print (boolean ask, Component c);
}
