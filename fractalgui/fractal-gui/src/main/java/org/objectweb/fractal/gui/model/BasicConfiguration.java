/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.model;

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.LifeCycleController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Basic implementation of the {@link Configuration} interface.
 */

public class BasicConfiguration implements
  Configuration,
  Factory,
  BindingController,
  LifeCycleController
{

  /**
   * A collection client interface bound to the {@link
   * VetoableConfigurationListener vetoable listeners} of this model.
   */

  public final static String VETOABLE_CONFIGURATION_LISTENERS_BINDING =
    "vetoable-configuration-listeners";

  /**
   * A collection client interface bound to the {@link ConfigurationListener
   * listeners} of this model.
   */

  public final static String CONFIGURATION_LISTENERS_BINDING =
    "configuration-listeners";

  /**
   * The vetoable listeners client interface.
   */

  private Map vetoableListeners;

  /**
   * The listeners client interface.
   */

  protected Map listeners;

  /**
   * Root component of this configuration.
   */

  private Component root;

  /**
   * If this configuration has been initialized or not.
   */

  private boolean initialized;

  /**
   * If this configuration has been started or not.
   */

  private boolean started;

  /**
   * ChangeCount of this configuration.
   */

  private long changeCount;

  /**
   * Storage directory of this configuration.
   */

  private String storage;

  /**
   * Constructs a new basic configuration component.
   */

  public BasicConfiguration () {
    root = createComponent();
    vetoableListeners = new HashMap();
    listeners = new HashMap();
    listeners.put("", new StatusManager());
  }

  /**
   * Returns the vetoable listeners of this configuration.
   *
   * @return the vetoable listeners of this configuration.
   */
  public List getVetoableListeners () {
    return new ArrayList(vetoableListeners.values());
  }

  /**
   * Returns the listeners of this configuration.
   *
   * @return the listeners of this configuration.
   */
  public List getListeners () {
    return new ArrayList(listeners.values());
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    Set itfs = new HashSet();
    itfs.addAll(listeners.keySet());
    itfs.addAll(vetoableListeners.keySet());
    return (String[])itfs.toArray(new String[itfs.size()]);
  }

  public Object lookupFc (final String clientItfName) {
    if (clientItfName.startsWith(CONFIGURATION_LISTENERS_BINDING)) {
      return listeners.get(clientItfName);
    } else if (clientItfName.startsWith(VETOABLE_CONFIGURATION_LISTENERS_BINDING)) {
      vetoableListeners.get(clientItfName);
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (clientItfName.startsWith(CONFIGURATION_LISTENERS_BINDING)) {
      listeners.put(clientItfName, serverItf);
    } else if (clientItfName.startsWith(VETOABLE_CONFIGURATION_LISTENERS_BINDING)) {
      vetoableListeners.put(clientItfName, serverItf);
    }
  }

  public void unbindFc (final String clientItfName) {
    if (clientItfName.startsWith(CONFIGURATION_LISTENERS_BINDING)) {
      listeners.remove(clientItfName);
    } else if (clientItfName.startsWith(VETOABLE_CONFIGURATION_LISTENERS_BINDING)) {
      vetoableListeners.remove(clientItfName);
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the LifeCycleController interface
  // -------------------------------------------------------------------------

  public String getFcState () {
    return started ? STARTED : STOPPED;
  }

  public void startFc () {
    if (!initialized) {
      setRootComponent(createComponent());
      initialized = true;
    }
    started = true;
    changeCount = 0;
  }

  public void stopFc () {
    started = false;
  }

  // -------------------------------------------------------------------------
  // Implementation of the Configuration interface
  // -------------------------------------------------------------------------

  public Component getRootComponent () {
    return root;
  }

  public void setRootComponent (final Component root) {
    if (root == null) {
      throw new IllegalArgumentException();
    }
    Component oldRoot = this.root;
    if (root != oldRoot) {
      Iterator i = vetoableListeners.values().iterator();
      while (i.hasNext()) {
        VetoableConfigurationListener l =
          (VetoableConfigurationListener)i.next();
        l.canChangeRootComponent();
      }
      this.root = root;
      i = listeners.values().iterator();
      while (i.hasNext()) {
        ConfigurationListener l = (ConfigurationListener)i.next();
        l.rootComponentChanged(oldRoot);
      }
    }
    initialized = true;
    changeCount = 0;
  }

  public long getChangeCount () {
    return changeCount;
  }

  public void setChangeCount (long changeCount) {
    this.changeCount = changeCount;
    Iterator i = listeners.values().iterator();
    while (i.hasNext()) {
      ConfigurationListener l = (ConfigurationListener)i.next();
      l.changeCountChanged(getRootComponent(), changeCount);
    }
  }

  public String getStorage () {
    return storage;
  }

  public void setStorage (String storage) {
    this.storage = storage;
  }

  // -------------------------------------------------------------------------
  // Implementation of the Factory interface
  // -------------------------------------------------------------------------

  public Component createComponent () {
    return new BasicComponent(this);
  }

  public Component createComponent (final Component component) {
    BasicComponent c;
    if (component.getMasterComponent() != null) {
      c = (BasicComponent)component.getMasterComponent();
    } else {
      c = (BasicComponent)component;
    }
    return new SharedComponent(c);
  }

  public ClientInterface createClientInterface (Component owner) {
    if (owner.getMasterComponent() != null) {
      owner = owner.getMasterComponent();
    }
    return new BasicClientInterface((BasicComponent)owner);
  }

  public ServerInterface createServerInterface (Component owner) {
    if (owner.getMasterComponent() != null) {
      owner = owner.getMasterComponent();
    }
    return new BasicServerInterface((BasicComponent)owner);
  }
}
