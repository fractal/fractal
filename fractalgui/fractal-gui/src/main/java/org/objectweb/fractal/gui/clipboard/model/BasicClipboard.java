/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.clipboard.model;

import org.objectweb.fractal.gui.model.Factory;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Interface;
import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.ServerInterface;
import org.objectweb.fractal.gui.graph.model.GraphModel;
import org.objectweb.fractal.gui.graph.model.BasicGraphModel;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

/**
 * Basic implementation of the {@link Clipboard} interface.
 */

public class BasicClipboard implements Clipboard {

  /**
   * Reference to the last component that was cut or copied.
   */

  private Component original;

  /**
   * Clone of {@link #original}, i.e. of the last component that was cut or
   * copied. This clone is null if original is a slave component.
   */

  private Component copy;

  /**
   * Internal graph model used to store the coordinates of {@link #clone}.
   */

  private GraphModel graph;

  /**
   * Constructs a new {@link Clipboard} component.
   */

  public BasicClipboard () {
    graph = new BasicGraphModel();
  }

  // -------------------------------------------------------------------------
  // Implementation of the Clipboard interface
  // -------------------------------------------------------------------------

  public boolean canCut (final Component srcComponent) {
    return srcComponent != null && srcComponent.getParent() != null;
  }

  public void cut (
    final Component srcComponent,
    final GraphModel srcGraph,
    final Factory srcFactory)
  {
    original = srcComponent;
    if (original.getMasterComponent() == null) {
      copy = clone(srcComponent, srcGraph, graph, srcFactory);
    }
    Component parent = srcComponent.getParent();
    parent.removeSubComponent(srcComponent);
  }

  public boolean canCopy (final Component srcComponent) {
    return srcComponent != null;
  }

  public void copy (
    final Component srcComponent,
    final GraphModel srcGraph,
    final Factory srcFactory)
  {
    original = srcComponent;
    if (original.getMasterComponent() == null) {
      copy = clone(srcComponent, srcGraph, graph, srcFactory);
    }
  }

  public boolean canPaste (final Component dstComponent) {
    if (original != null && original.getMasterComponent() != null) {
      return canPasteAsShared(dstComponent);
    } else if (dstComponent != null && copy != null) {
      if (copy.containsSlaveOfExternalComponent(copy)) {
        return dstComponent.getRootComponent() == original.getRootComponent();
      } else {
        return true;
      }
    }
    return false;
  }

  public void paste (
    final Component dstComponent,
    final GraphModel dstGraph,
    final Factory dstFactory)
  {
    if (original != null && original.getMasterComponent() != null) {
      pasteAsShared(dstComponent, dstGraph, dstFactory);
    } else {
      Component child = clone(copy, graph, dstGraph, dstFactory);
      dstComponent.addSubComponent(child);
    }
  }

  public boolean canPasteAsShared (final Component dstComponent) {
    if (dstComponent != null && original != null) {
      // can paste as shared only if the components have the same root component
      return dstComponent.getRootComponent() == original.getRootComponent();
    }
    return false;
  }

  public void pasteAsShared (
    final Component dstComponent,
    final GraphModel dstGraph,
    final Factory dstFactory)
  {
    Component child = dstFactory.createComponent(original);
    dstComponent.addSubComponent(child);
  }

  // -------------------------------------------------------------------------
  // Other methods
  // -------------------------------------------------------------------------

  /**
   * Clones the given component and all its sub components.
   *
   * @param c the component to be cloned. Must not be a slave component.
   * @param srcGraph the model containing the coordinates of c.
   * @param dstGraph the model into which the clone's coordinates must be put.
   * @param f the factory to be used to create the clone.
   * @return a clone of the given component.
   */

  private Component clone (
    final Component c,
    final GraphModel srcGraph,
    final GraphModel dstGraph,
    final Factory f)
  {
    if (c.getMasterComponent() != null) {
      throw new RuntimeException("Internal error");
    }
    Map clones = new HashMap();
    cloneMasterComponents(c, f, clones);
    cloneSlaveComponents(c, f, clones);
    return clone(c, srcGraph, dstGraph, clones);
  }

  /**
   * Clones all the master components in the given component. This method just
   * clones the components and their interfaces (except slave collection
   * interfaces): it does not clone any binding or component's coordinates.
   *
   * @param c the component whose master sub components must be cloned.
   * @param f the factory to be used to create the clones.
   * @param clones a map associating the {@link BasicClipboard.Clone
   *      clones} to the original components. This map is filled in by this
   *      method.
   */

  private void cloneMasterComponents (
    final Component c,
    final Factory f,
    final Map clones)
  {
    if (c.getMasterComponent() == null) {
      Component d = f.createComponent();
      Clone clone = new Clone(d);

      // clone server interface
      List itfs = c.getServerInterfaces();
      for (int i = 0; i < itfs.size(); ++i) {
        Interface itf = (Interface)itfs.get(i);
        if (itf.isCollection() && itf.getMasterCollectionInterface() != null) {
          // do not clone slave collection interfaces
          continue;
        }
        Interface citf = f.createServerInterface(d);
        citf.setName(itf.getName());
        citf.setSignature(itf.getSignature());
        citf.setIsOptional(itf.isOptional());
        citf.setIsCollection(itf.isCollection());
        d.addServerInterface((ServerInterface)citf);
        clone.interfaceClones.put(itf, citf);
        clone.interfaceClones.put(
          itf.getComplementaryInterface(), citf.getComplementaryInterface());
      }

      // clone client interfaces
      itfs = c.getClientInterfaces();
      for (int i = 0; i < itfs.size(); ++i) {
        Interface itf = (Interface)itfs.get(i);
        if (itf.isCollection() && itf.getMasterCollectionInterface() != null) {
          // do not clone slave collection interfaces
          continue;
        }
        Interface citf = f.createClientInterface(d);
        citf.setName(itf.getName());
        citf.setSignature(itf.getSignature());
        citf.setIsOptional(itf.isOptional());
        citf.setIsCollection(itf.isCollection());
        d.addClientInterface((ClientInterface)citf);
        clone.interfaceClones.put(itf, citf);
        clone.interfaceClones.put(
          itf.getComplementaryInterface(), citf.getComplementaryInterface());
      }

      clones.put(c, clone);
    }
    List subComponents = c.getSubComponents();
    for (int i = 0; i < subComponents.size(); ++i) {
      cloneMasterComponents((Component)subComponents.get(i), f, clones);
    }
  }

  /**
   * Clones all the slave components in the given component. This method just
   * clones the components: it does not clone any interface, binding or
   * component's coordinates.
   *
   * @param c the component whose slave sub components must be cloned.
   * @param f the factory to be used to create the clones.
   * @param clones a map associating the {@link BasicClipboard.Clone clones} to
   *      the original components. This map is used to find the clone of the
   *      master components of the slave components, and to store the clones
   *      created by this method.
   */

  private void cloneSlaveComponents (
    final Component c,
    final Factory f,
    final Map clones)
  {
    Component master = c.getMasterComponent();
    if (master != null) {
      if (clones.get(master) != null) {
        master = ((Clone)clones.get(master)).clone;
      }
      clones.put(c, new Clone(f.createComponent(master)));
    }
    List subComponents = c.getSubComponents();
    for (int i = 0; i < subComponents.size(); ++i) {
      cloneSlaveComponents((Component)subComponents.get(i), f, clones);
    }
  }

  /**
   * Finishes the cloning of the given component. This method does not clone any
   * component: it just completes the initialization of the cloned components,
   * that have been previously constructed, and are available in the give map.
   * This method constructs the clones hierarchy (by adding sub component clones
   * in the clone of their parent component). It also clones the bindings and
   * the component's coordinates.
   *
   * @param c the component whose cloning must be finished.
   * @param srcGraph the model containing the coordinates of c.
   * @param dstGraph the model into which the clone's coordinates must be put.
   * @param clones a map associating the {@link BasicClipboard.Clone clones} to
   *      the original components. This map is just read by this method. It must
   *      be properly initialized before calling this method.
   * @return the clone of the given component.
   */

  protected Component clone (
    final Component c,
    final GraphModel srcGraph,
    final GraphModel dstGraph,
    final Map clones)
  {
    Clone clone = (Clone)clones.get(c);
    Component d = clone.clone;
    d.setName(c.getName());
    d.setType(c.getType());
    d.setImplementation(c.getImplementation());

    // controller part
    d.setAttributeController(c.getAttributeController());
    List attrNames = c.getAttributeNames();
    for (int i = 0; i < attrNames.size(); ++i) {
      String attrName = (String)attrNames.get(i);
      d.setAttribute(attrName, c.getAttribute(attrName));
    }
    d.setTemplateControllerDescriptor(c.getTemplateControllerDescriptor());
    d.setComponentControllerDescriptor(c.getComponentControllerDescriptor());

    // sub components
    List subComponents = c.getSubComponents();
    for (int i = 0; i < subComponents.size(); ++i) {
      d.addSubComponent(
        clone((Component)subComponents.get(i), srcGraph, dstGraph, clones));
    }

    // bindings
    // TODO attention aux itfs de type collection
    List itfs = c.getServerInterfaces();
    for (int i = 0; i < itfs.size(); ++i) {
      Interface itf = (Interface)itfs.get(i);
      ClientInterface citf = (ClientInterface)itf.getComplementaryInterface();
      if (citf.getBinding() != null) {
        ServerInterface sitf = citf.getBinding().getServerInterface();
        Clone sclone = (Clone)clones.get(sitf.getOwner());
        if (sclone != null) {
          citf = (ClientInterface)clone.interfaceClones.get(citf);
          sitf = (ServerInterface)sclone.interfaceClones.get(sitf);
          if (citf != null && sitf != null) {
            citf.getOwner().bind(citf, citf.getName(), sitf);
          }
        }
      }
    }
    for (int i = 0; i < subComponents.size(); ++i) {
      Component subComponent = (Component)subComponents.get(i);
      itfs = subComponent.getClientInterfaces();
      for (int j = 0; j < itfs.size(); ++j) {
        ClientInterface citf = (ClientInterface)itfs.get(j);
        if (citf.getBinding() != null) {
          ServerInterface sitf = citf.getBinding().getServerInterface();
          Clone cclone = (Clone)clones.get(citf.getOwner());
          Clone sclone = (Clone)clones.get(sitf.getOwner());
          if (cclone != null && sclone != null) {
            if (citf.getMasterCollectionInterface() != null) {
              citf = (ClientInterface)citf.getMasterCollectionInterface();
            }
            citf = (ClientInterface)cclone.interfaceClones.get(citf);
            sitf = (ServerInterface)sclone.interfaceClones.get(sitf);
            if (citf != null && sitf != null) {
              citf.getOwner().bind(citf, citf.getName(), sitf);
            }
          }
        }
      }
    }

    // positions and colors
    if (srcGraph != null && dstGraph != null) {
      dstGraph.setComponentPosition(d, srcGraph.getComponentPosition(c));
      dstGraph.setComponentColor(d, srcGraph.getComponentColor(c));
    }

    return d;
  }

  /**
   * Information about a cloned component.
   */

  static class Clone {

    /**
     * The clone component.
     */

    public Component clone;

    /**
     * A map associating the interfaces of {@link #clone} to the interfaces of
     * the original component.
     */

    public Map interfaceClones;

    /**
     * Constructs a new {@link BasicClipboard.Clone} object.
     *
     * @param clone a cloned component.
     */

    public Clone (final Component clone) {
      this.clone = clone;
      this.interfaceClones = new HashMap();
    }
  }
}
