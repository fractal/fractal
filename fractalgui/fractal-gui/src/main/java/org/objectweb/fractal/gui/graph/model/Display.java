/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.model;

import java.awt.Rectangle;

/**
 * A model for the display. A display is used to render components as graphs
 * of boxes and lines (representing components and bindings).
 */

public interface Display {

  /**
   * Returns the size of this display.
   *
   * @return the size of this display, in pixels.
   * @see #setScreenSize
   */

  Rectangle getScreenSize ();

  /**
   * Sets the size of this display.
   *
   * @param screenSize the new size of this display, in pixels.
   * @see #getScreenSize
   */

  void setScreenSize (Rectangle screenSize);

  /**
   * Returns the area of the component graph shown by this display. Indeed a
   * display may show only a portion of a component graph, which may be changed
   * by zooming or scrolling.
   *
   * @return the area of the component graph shown by this display. This area
   *      is specified in the following way: if sr is the screen size, and dr is
   *      the displayed area, then the configuration in drawn in the rectangle
   *      (sr.x+sr.w*dr.x0, sr.y+sr.h*dr.y0, sr.x+sr.w*dr.x1, sr.y+sr.h*dr.y1).
   *      So, if sr is equal to (0,0,1,1), the configuration is drawn in the
   *      full screen rectangle, if it is equal to (0.5,0,1,1) the configuration
   *      is drawn in the half right part of the screen rectangle, and so on.
   * @see #setDisplayedArea
   */

  Rect getDisplayedArea ();

  /**
   * Sets the area of the component graph shown by this display.
   *
   * @param displayedArea the new area of the component graph shown by this
   *      display.
   * @see #getDisplayedArea
   */

  void setDisplayedArea (Rect displayedArea);

  /**
   * Returns <tt>true</tt> if this display is anti aliased.
   *
   * @return <tt>true</tt> if this display is anti aliased.
   * @see #setIsAntialiasing
   */

  boolean isAntialiasing ();

  /**
   * Sets the anti aliasing option of this display.
   *
   * @param isAntialiasing the new anti aliasing option of this display.
   */

  void setIsAntialiasing (boolean isAntialiasing);

  /**
   * Returns the max depth at which the component graph is shown by this
   * display.
   *
   * @return the max depth at which the component graph is shown by this
   *      display.
   * @see #setMaxDepth
   */

  int getMaxDepth ();

  /**
   * Sets the max depth at which the component graph is shown by this display.
   *
   * @param maxDepth the new max depth at which the component graph is shown by
   *      this display.
   * @see #getMaxDepth
   */

  void setMaxDepth (int maxDepth);

  /**
   * Sets the displaying mode for the interfaces name of this display.
   *
   */

  void setItfNameDisplayMode ();

  /**
   * Returns the displaying mode for the interfaces name of this display.
   *
   */

  int getItfNameDisplayMode ();
}
