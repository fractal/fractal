/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.history.model;

/**
 * A model of a navigation history.
 */

public interface HistoryManager {

  /**
   * Returns <tt>true</tt> if it is possible to go back to a previous view.
   *
   * @return <tt>true</tt> if it is possible to go back to a previous view.
   */

  boolean canGoPrevious ();

  /**
   * Goes to the previous view.
   */

  void goPrevious ();

  /**
   * Returns <tt>true</tt> if it is possible to go to a next view.
   *
   * @return <tt>true</tt> if it is possible to go to a next view.
   */

  boolean canGoNext ();

  /**
   * Goes to the next view.
   */

  void goNext ();

  /**
   * Clears the state of this model. After a call to this method, {@link
   * #canGoPrevious canGoPrevious} and {@link #canGoNext canGoNext} return
   * <tt>false</tt>.
   */

  void clear ();
}
