/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.model;

import java.util.List;

/**
 * A primitive or composite component.
 */

public interface Component {

  /**
   * The {@link #getStatus status} flags corresponding to a valid component.
   */

  long OK = 0;

  /**
   * A {@link #getStatus status} flag indicating that the component's name is
   * missing.
   */

  long NAME_MISSING = 1;

  /**
   * A {@link #getStatus status} flag indicating that the component's type is
   * missing.
   */

  // long TYPE_MISSING = 1 << 1;

  /**
   * A {@link #getStatus status} flag indicating that the component's
   * implementation is missing.
   */

  long IMPLEMENTATION_MISSING = 1 << 2;

  /**
   * A {@link #getStatus status} flag indicating that the component's
   * implementation class cannot be found.
   */

  long IMPLEMENTATION_CLASS_NOT_FOUND = 1 << 3;

  /**
   * A {@link #getStatus status} flag indicating that the component's
   * implementation class is invalid.
   */

  long IMPLEMENTATION_CLASS_INVALID = 1 << 4; // TODO gerer ce type d'erreur

  /**
   * A {@link #getStatus status} flag indicating that the component's interfaces
   * are invalid.
   */

  long INTERFACES_INVALID = 1 << 5;

  /**
   * A {@link #getStatus status} flag indicating that the component's attribute
   * controller interface is missing.
   */

  long ATTRIBUTE_CONTROLLER_MISSING = 1 << 6;

  /**
   * A {@link #getStatus status} flag indicating that the component's attribute
   * controller interface cannot be found.
   */

  long ATTRIBUTE_CONTROLLER_CLASS_NOT_FOUND = 1 << 7;

  /**
   * A {@link #getStatus status} flag indicating that the component's attribute
   * controller interface is invalid.
   */

  long ATTRIBUTE_CONTROLLER_CLASS_INVALID = 1 << 8; // TODO gerer ce type d'erreur

  // -------------------------------------------------------------------------
  // Configuration
  // -------------------------------------------------------------------------

  /**
   * Returns the configuration to which this component belongs.
   *
   * @return the configuration to which this component belongs.
   */

  Configuration getConfiguration();


  // -------------------------------------------------------------------------
  // Status
  // -------------------------------------------------------------------------

  /**
   * Returns the status of this component.
   *
   * @return the status flags of this component. Each flag, i.e., each bit of
   *     the returned value is independent from the other, and indicates an
   *     error if it is set.
   * @see #setStatus
   */

  long getStatus ();

  /**
   * Sets the status of this component. <i>This method is reserved for model
   * providers, and must not be called by model users</i>.
   *
   * @param status the new status of this component.
   * @see #getStatus
   */

  void setStatus (long status);

  // -------------------------------------------------------------------------
  // Ancestors
  // -------------------------------------------------------------------------

  /**
   * Returns the parent component of this component.
   *
   * @return the parent component of this component, or <tt>null</tt> if this
   *      component is the root component.
   */

  Component getParent ();

  /**
   * Returns the root component of the configuration. This method just calls
   * {@link #getParent getParent} until the root component is found.
   *
   * @return the root component of the configuration.
   */

  Component getRootComponent ();

  /**
   * Returns the path of this component from the root component.
   *
   * @return an ordered array of all the ancestors of this component, from the
   *      root component to this component, included.
   */

  Object[] getPath ();

  // -------------------------------------------------------------------------
  // Name, Type, Implementation
  // -------------------------------------------------------------------------

  /**
   * Returns the name of this component.
   *
   * @return the name of this component.
   * @see #setName
   */

  String getName ();

  /**
   * Sets the name of this component. This method notifies the configuration
   * listeners, via the {@link ConfigurationListener#nameChanged nameChanged}
   * method.
   *
   * @param name the new component name.
   * @see #getName
   */

  void setName (String name);

  /**
   * Returns the type of this component.
   *
   * @return the type of this component.
   * @see #setType
   */

  String getType ();

  /**
   * Sets the type of this component. This method notifies the configuration
   * listeners, via the {@link ConfigurationListener#typeChanged typeChanged}
   * method.
   *
   * @param type the new component type.
   * @see #getType
   */

  void setType (String type);

  /**
   * Returns the implementation of this component.
   *
   * @return the implementation of this component.
   * @see #setImplementation
   */

  String getImplementation ();

  /**
   * Sets the implementation of this component. This method notifies the
   * configuration listeners, via the {@link
   * ConfigurationListener#implementationChanged implementationChanged} method.
   *
   * @param implementation the new component implementation.
   * @see #getImplementation
   */

  void setImplementation (String implementation);

  // -------------------------------------------------------------------------
  // Client and Server interfaces
  // -------------------------------------------------------------------------

  /**
   * Returns the external client interfaces of this component.
   *
   * @return an unmodifiable list of {@link ClientInterface} objects
   *      corresponding to the external client interfaces of this component.
   */

  List getClientInterfaces ();

  /**
   * Returns the external client interface of this component whose name is
   * given.
   *
   * @param name the name of the interface that must be returned.
   * @return the external client interface of this component whose name is
   *      given, or <tt>null</tt> if there is no such interface.
   */

  Interface getClientInterface (String name);

  /**
   * Adds the given client interface to this component. Does nothing if the
   * given interface already belongs to this component. This method notifies the
   * configuration listeners, via the {@link
   * ConfigurationListener#clientInterfaceAdded clientInterfaceAdded} method.
   *
   * @param itf the interface that must be added to this component. This
   *      interface must have been created for this specific component (see
   *      {@link Factory#createClientInterface createClientInterface}).
   */

  void addClientInterface (ClientInterface itf);

  /**
   * Removes the given client interface from this component. Does nothing if the
   * given interface does not belong to this component. This method notifies the
   * configuration listeners, via the {@link
   * ConfigurationListener#clientInterfaceRemoved clientInterfaceRemoved}
   * method.
   *
   * @param itf the interface that must be removed from this component.
   */

  void removeClientInterface (ClientInterface itf);

  /**
   * Returns the external server interfaces of this component.
   *
   * @return an unmodifiable list of {@link ServerInterface}
   *      objects corresponding to the external server interfaces of this
   *      component.
   */

  List getServerInterfaces ();

  /**
   * Returns the external server interface of this component whose name is
   * given.
   *
   * @param name the name of the interface that must be returned.
   * @return the external server interface of this component whose name is
   *      given, or <tt>null</tt> if there is no such interface.
   */

  Interface getServerInterface (String name);

  /**
   * Adds the given server interface to this component. Does nothing if the
   * given interface already belongs to this component. This method notifies the
   * configuration listeners, via the {@link
   * ConfigurationListener#serverInterfaceAdded serverInterfaceAdded} method.
   *
   * @param itf the interface that must be added to this component. This
   *      interface must have been created for this specific component (see
   *      {@link Factory#createServerInterface createServerInterface}).
   */

  void addServerInterface (ServerInterface itf);

  /**
   * Removes the given server interface from this component. Does nothing if the
   * given interface does not belong to this component. This method notifies the
   * configuration listeners, via the {@link
   * ConfigurationListener#serverInterfaceRemoved serverInterfaceRemoved}
   * method.
   *
   * @param itf the interface that must be removed from this component.
   */

  void removeServerInterface (ServerInterface itf);

  // -------------------------------------------------------------------------
  // Bindings
  // -------------------------------------------------------------------------

  /**
   * Binds the given client interface to the given server interface. This method
   * notifies the configuration listeners, via the {@link
   * ConfigurationListener#interfaceBound interfaceBound} method.
   *
   * @param citf a client interface of this component.
   * @param sitf a server interface.
   */

  void bind (ClientInterface citf, String suffix, ServerInterface sitf);

  /**
   * Rebinds the given client interface to the given server interface. This
   * method notifies the configuration listeners, via the {@link
   * ConfigurationListener#interfaceRebound interfaceRebound} method.
   *
   * @param citf a client interface of this component.
   * @param sitf a server interface.
   */

  void rebind (ClientInterface citf, ServerInterface sitf);

  /**
   * Unbinds the given client interface.  This method notifies the configuration
   * listeners, via the {@link ConfigurationListener#interfaceUnbound
   * interfaceUnbound} method.
   *
   * @param citf a client interface of this component.
   */

  void unbind (ClientInterface citf);

  // -------------------------------------------------------------------------
  // Attributes
  // -------------------------------------------------------------------------

  /**
   * Returns the name of the attribute controller interface of this component.
   *
   * @return the name of the attribute controller interface of this component.
   * @see #setAttributeController
   */

  String getAttributeController ();

  /**
   * Sets the name of the attribute controller interface of this component.
   * This method notifies the configuration listeners, via the {@link
   * ConfigurationListener#attributeControllerChanged
   * attributeControllerChanged} method.
   *
   * @param attributeController the new attribute controller interface name.
   * @see #getAttributeController
   */

  void setAttributeController (String attributeController);

  /**
   * Returns the names of the attributes of this component.
   *
   * @return an unmodifiable list of String objects corresponding to the names
   *      of the attributes of this component.
   */

  List getAttributeNames ();

  /**
   * Return the value of the given attribute of this component.
   *
   * @param attributeName the name of an attribute of this component.
   * @return the value of the given attribute, or <tt>null</tt> if this
   *      attribute does not exist.
   * @see #setAttribute
   */

  String getAttribute (String attributeName);

  /**
   * Sets the value of an attribute of this component. This method notifies the
   * configuration listeners, via the {@link
   * ConfigurationListener#attributeChanged attributeChanged} method.
   *
   * @param attributeName the name of an existing or of a new attribute of this
   *      component.
   * @param attributeValue the new value of this attribute.
   * @see #getAttribute
   */

  void setAttribute (String attributeName, String attributeValue);

  // -------------------------------------------------------------------------
  // Controller descriptors
  // -------------------------------------------------------------------------

  /**
   * Returns the template controller descriptor of this component.
   *
   * @return the template controller descriptor of this component.
   * @see #setTemplateControllerDescriptor
   */

  String getTemplateControllerDescriptor ();

  /**
   * Sets the template controller descriptor of this component. This method
   * notifies the configuration listeners, via the {@link
   * ConfigurationListener#templateControllerDescriptorChanged
   * templateControllerDescriptorChanged} method.
   *
   * @param desc the new template controller descriptor of this component.
   * @see #getTemplateControllerDescriptor
   */

  void setTemplateControllerDescriptor (String desc);

  /**
   * Returns the component controller descriptor of this component.
   *
   * @return the component controller descriptor of this component.
   * @see #setComponentControllerDescriptor
   */

  String getComponentControllerDescriptor ();

  /**
   * Sets the component controller descriptor of this component.  This method
   * notifies the configuration listeners, via the {@link
   * ConfigurationListener#componentControllerDescriptorChanged
   * componentControllerDescriptorChanged} method.
   *
   * @param desc the new component controller descriptor of this component.
   * @see #getComponentControllerDescriptor
   */

  void setComponentControllerDescriptor (String desc);

  // -------------------------------------------------------------------------
  // Sharing
  // -------------------------------------------------------------------------

  /**
   * Returns <tt>true</tt> if this component has slave component.
   *
   * @return <tt>true</tt> if the {@link #getSlaveComponents getSlaveComponent}
   *      list is empty.
   */

  boolean isShared ();

  /**
   * Returns the master component of this component.
   *
   * @return the master component of this component, or <tt>null</tt> if this
   *      component is not a slave component.
   */

  Component getMasterComponent ();

  /**
   * Returns the slave components of this component.
   *
   * @return an unmodifiable list of {@link Component Component} objects that
   *      correspond to the slave components of this component. This list is
   *      empty is this component is not a master component.
   */

  List getSlaveComponents ();

  // -------------------------------------------------------------------------
  // Composite specific informations
  // -------------------------------------------------------------------------

  /**
   * Returns <tt>true</tt> if this component is a composite component.
   *
   * @return <tt>true</tt> if the {@link #getSubComponents getSubComponents}
   *      list is empty.
   */

  boolean isComposite ();

  /**
   * Returns <tt>true</tt> if this component contains the given sub component.
   *
   * @param child a component.
   * @return <tt>true</tt> if this component contains the given sub component.
   */

  boolean contains (Component child);

  /**
   * Returns <tt>true</tt> if the given component contains a slave component
   * but does not contain its master.
   *
   * @param root a component.
   * @return <tt>true</tt> if the given component contains a slave component
   *      but does not contain its master.
   */

  boolean containsSlaveOfExternalComponent (Component root);

  /**
   * Returns <tt>true</tt> if the given component contains a master component
   * but does not contain all its slaves.
   *
   * @param root a component.
   * @return <tt>true</tt> if the given component contains a master component
   *      but does not contain all its slaves.
   */

  boolean containsMasterOfExternalComponent (Component root);

  /**
   * Returns the sub components of this component.
   *
   * @return an unmodifiable list of {@link Component} objects that correspond
   *      to the sub components of this component. This list is empty if this
   *      component is a primitive component.
   */

  List getSubComponents ();

  /**
   * Returns the sub component of this component whose name is given.
   *
   * @param name the name of a sub component of this component.
   * @return the sub component of this component whose name is given, or
   *      <tt>null</tt> if there is no such sub component.
   */

  Component getSubComponent (String name);

  /**
   * Adds the given component as a sub component of this component. Does nothing
   * if the given component is already a sub component of this component. This
   * method notifies the configuration listeners, via the {@link
   * ConfigurationListener#subComponentAdded subComponentAdded} method.
   *
   * @param child a component.
   */

  void addSubComponent (Component child);

  /**
   * Removes the given sub component from this component. Does nothing if the
   * given component is not a sub component of this component.  This
   * method notifies the configuration listeners, via the {@link
   * ConfigurationListener#subComponentRemoved subComponentRemoved} method.
   *
   * @param child a sub component of this component.
   */

  void removeSubComponent (Component child);
}
