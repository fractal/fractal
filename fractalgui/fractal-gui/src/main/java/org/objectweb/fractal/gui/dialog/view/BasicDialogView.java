/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.dialog.view;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.Constants;
import org.objectweb.fractal.gui.dialog.model.DialogModel;
import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Configuration;
import org.objectweb.fractal.gui.model.ConfigurationListener;
import org.objectweb.fractal.gui.model.Interface;
import org.objectweb.fractal.gui.model.ServerInterface;
import org.objectweb.fractal.swing.JPanelImpl;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * A configuration view that displays the configuration's root component as a
 * dialog.
 */

public class BasicDialogView extends JPanelImpl implements
  ConfigurationListener,
  BindingController
{

  /**
   * A mandatory client interface bound to a {@link Configuration configuration}
   * model. This is the configuration whose root component is displayed by this
   * view.
   */

  public final static String CONFIGURATION_BINDING = "configuration";

  /**
   * A mandatory client interface bound to a {@link DialogModel dialog} model.
   * This is the model displayed by this view.
   */

  public final static String DIALOG_MODEL_BINDING = "dialog-model";

  /**
   * An optional client interface bound to a {@link JComponent} inner component.
   * This optional inner swing component can, for example, display a graph
   * representation of the component that is displayed by this dialog.
   */

  public final static String INNER_GRAPH_VIEW_BINDING = "inner-graph-view";

  /**
   * A collection client interface bound to the {@link DialogViewListener
   * listeners} of this view.
   */

  public final static String DIALOG_VIEW_LISTENERS_BINDING = "dialog-listeners";

  /**
   * The configuration client interface.
   */

  protected Configuration configuration;

  /**
   * The dialog model client interface.
   */

  protected DialogModel model;

  /**
   * The inner graph view client interface.
   */

  protected JComponent innerGraphView;

  /**
   * The listeners client interface.
   */

  protected Map listeners;

  private JPanel innerGraphPanel;

  private JTextField nameField;

  private JTextField typeField;

  private JTextField classField;
//  private CompletionJTextField classField;

  private JTable[] itfTable;

  private JComboBox contingencyComboBox;

  private JComboBox cardinalityComboBox;

  private JButton[] itfAddButton;

  private JButton[] itfRemoveButton;

  private JTable attrTable;

  private JButton attrAddButton;

  private JButton attrRemoveButton;

  private JButton classFieldButton;

  private JTextField attrControllerField;

  private JTextField tmplDescField;

  private JTextField compDescField;

  private ActionListener buttonListener;

  private ListSelectionListener listListener;

  private ClassFieldButtonListener classFieldButtonListener;

  // -------------------------------------------------------------------------
  // Initialization
  // -------------------------------------------------------------------------

  /**
   * Constructs a new {@link BasicDialogView} component.
   */

  public BasicDialogView () {
    listeners = new HashMap();

    itfTable = new JTable[2];
    itfAddButton = new JButton[2];
    itfRemoveButton = new JButton[2];
    buttonListener = new ButtonListener();
    classFieldButtonListener = new ClassFieldButtonListener ();
    listListener = new ListListener();

    GridBagLayout bagLayout = new GridBagLayout();
    setLayout(bagLayout);

    JPanel hPanel = createHeaderPanel();

    contingencyComboBox = new JComboBox();
    contingencyComboBox.addItem("mandatory");
    contingencyComboBox.addItem("optional");

    cardinalityComboBox = new JComboBox();
    cardinalityComboBox.addItem("single");
    cardinalityComboBox.addItem("collection");

    JPanel cPanel = new JPanel();
    cPanel.setLayout(new BorderLayout());
    cPanel.add(new JLabel("Client interfaces"), "North");
    cPanel.add(createInterfacesPanel(0), "Center");

    JPanel sPanel = new JPanel();
    sPanel.setLayout(new BorderLayout());
    sPanel.add(new JLabel("Server interfaces"), "North");
    sPanel.add(createInterfacesPanel(1), "Center");

    JPanel aPanel = new JPanel();
    aPanel.setLayout(new BorderLayout());
    aPanel.add(new JLabel("Attributes"), "North");
    aPanel.add(createAttributesPanel(), "Center");

    JPanel bPanel = createControllerPanel();

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(3, 3, 3, 3);
    constraints.gridy = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.weightx = 1;
    constraints.weighty = 0;
    bagLayout.setConstraints(hPanel, constraints);
    add(hPanel);

    constraints.gridy = 2;
    constraints.fill = GridBagConstraints.BOTH;
    constraints.weighty = 1;
    bagLayout.setConstraints(sPanel, constraints);
    add(sPanel);

    constraints.gridy = 3;
    constraints.weighty = 2;
    bagLayout.setConstraints(cPanel, constraints);
    add(cPanel);

    constraints.gridy = 4;
    constraints.weighty = 1;
    bagLayout.setConstraints(aPanel, constraints);
    add(aPanel);

    constraints.gridy = 5;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.weighty = 0;
    bagLayout.setConstraints(bPanel, constraints);
    add(bPanel);
  }

  /**
   * Creates the panel that contains the name, type and implementation fields.
   *
   * @return the created panel.
   */

  private JPanel createHeaderPanel () {
    JPanel panel = new JPanel();

    GridBagLayout bagLayout = new GridBagLayout();
    panel.setLayout(bagLayout);

    GridBagConstraints constraints = new GridBagConstraints();
    JLabel label = new JLabel("Name", SwingConstants.LEFT);
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.NORTHWEST;
    bagLayout.setConstraints(label, constraints);
    panel.add(label);

    nameField = new JTextField(30);
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.weightx = 1;
    bagLayout.setConstraints(nameField, constraints);
    panel.add(nameField);

    label = new JLabel("Type ", SwingConstants.LEFT);
    constraints.gridy = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.NORTHWEST;
    constraints.weightx = 0;
    bagLayout.setConstraints(label, constraints);
    panel.add(label);

    typeField = new JTextField(30);
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.weightx = 1;
    bagLayout.setConstraints(typeField, constraints);
    panel.add(typeField);

    label = new JLabel("Class ", SwingConstants.LEFT);
    constraints.gridy = 2;
    constraints.fill = GridBagConstraints.NONE;
    constraints.weightx = 0;
    bagLayout.setConstraints(label, constraints);
    panel.add(label);

    classField = new JTextField(49);
/*
    classField = new CompletionJTextField(30, this);
    classField.makeList();
    classField.mliste.setVisible(false);
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.weightx = 1;
    bagLayout.setConstraints(classField, constraints);
    panel.add(classField);
*/
    JPanel classPanel = new JPanel (new BorderLayout());
    classFieldButton = new JButton("Impl.");
    classFieldButton.setBackground(new Color(204, 255, 204));
    classFieldButton.setPreferredSize(new Dimension (80, 20));
    classFieldButton.addActionListener(classFieldButtonListener);
    classPanel.add(classField, BorderLayout.WEST);
    classPanel.add(classFieldButton, BorderLayout.EAST);
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.weightx = 1;
    bagLayout.setConstraints(classPanel, constraints);
    panel.add(classPanel);

    return panel;
  }

  /**
   * Creates a panel that contains an interface list and its associated buttons.
   *
   * @param i the number of this panel. 0 = client, 1 = server interfaces.
   * @return the created panel.
   */

  private JPanel createInterfacesPanel (final int i) {
    itfTable[i] = new JTable();
    JScrollPane scrollPane = new JScrollPane(itfTable[i]);
    itfTable[i].setDefaultRenderer(
      Object.class,
      new InterfaceTableCellRenderer());

    itfAddButton[i] = new JButton("Add");
    itfAddButton[i].addActionListener(buttonListener);
    itfRemoveButton[i] = new JButton("Remove");
    itfRemoveButton[i].addActionListener(buttonListener);
    itfRemoveButton[i].setEnabled(false);

    JPanel panel = new JPanel();
    GridBagLayout bagLayout = new GridBagLayout();
    panel.setLayout(bagLayout);

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.gridheight = GridBagConstraints.REMAINDER;
    constraints.fill = GridBagConstraints.BOTH;
    constraints.weightx = 1;
    constraints.weighty = 1;
    bagLayout.setConstraints(scrollPane, constraints);
    panel.add(scrollPane);

    constraints.gridx = 1;
    constraints.gridy = 0;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.weightx = 0;
    constraints.weighty = 0;
    bagLayout.setConstraints(itfAddButton[i], constraints);
    panel.add(itfAddButton[i]);

    constraints.gridx = 1;
    constraints.gridy = 1;
    bagLayout.setConstraints(itfRemoveButton[i], constraints);
    panel.add(itfRemoveButton[i]);

    constraints.gridx = 1;
    constraints.gridy = 2;
    constraints.gridheight = GridBagConstraints.REMAINDER;
    constraints.fill = GridBagConstraints.BOTH;
    JPanel emptyPanel = new JPanel();
    bagLayout.setConstraints(emptyPanel, constraints);
    panel.add(emptyPanel);

    return panel;
  }

  /**
   * Creates the panel that contains the attibute table and its asociated
   * buttons.
   *
   * @return the created panel.
   */

  private JPanel createAttributesPanel () {
    attrTable = new JTable();
    JScrollPane scrollPane = new JScrollPane(attrTable);

    attrAddButton = new JButton("Add");
    attrAddButton.addActionListener(buttonListener);
    attrRemoveButton = new JButton("Remove");
    attrRemoveButton.addActionListener(buttonListener);
    attrRemoveButton.setEnabled(false);

    JPanel panel = new JPanel();
    GridBagLayout bagLayout = new GridBagLayout();
    panel.setLayout(bagLayout);

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.gridheight = GridBagConstraints.REMAINDER;
    constraints.fill = GridBagConstraints.BOTH;
    constraints.weightx = 1;
    constraints.weighty = 1;
    bagLayout.setConstraints(scrollPane, constraints);
    panel.add(scrollPane);

    constraints.gridx = 1;
    constraints.gridy = 0;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.weightx = 0;
    constraints.weighty = 0;
    bagLayout.setConstraints(attrAddButton, constraints);
    panel.add(attrAddButton);

    constraints.gridx = 1;
    constraints.gridy = 1;
    bagLayout.setConstraints(attrRemoveButton, constraints);
    panel.add(attrRemoveButton);

    constraints.gridx = 1;
    constraints.gridy = 2;
    constraints.gridheight = GridBagConstraints.REMAINDER;
    constraints.fill = GridBagConstraints.BOTH;
    JPanel emptyPanel = new JPanel();
    bagLayout.setConstraints(emptyPanel, constraints);
    panel.add(emptyPanel);

    return panel;
  }

  /**
   * Creates the panel that contains the attribute controller and the controller
   * descriptor fields.
   *
   * @return the constructed panel.
   */

  private JPanel createControllerPanel () {
    JPanel panel = new JPanel();

    GridBagLayout bagLayout = new GridBagLayout();
    panel.setLayout(bagLayout);

    GridBagConstraints constraints = new GridBagConstraints();

    JLabel label = new JLabel("Attribute controller:", SwingConstants.LEFT);
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.NORTHWEST;
    constraints.insets = new Insets(0, 0, 0, 3);
    bagLayout.setConstraints(label, constraints);
    panel.add(label);

    attrControllerField = new JTextField();
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.insets = new Insets(0, 0, 0, 0);
    constraints.weightx = 1;
    bagLayout.setConstraints(attrControllerField, constraints);
    panel.add(attrControllerField);

    label = new JLabel("Template controller descriptor:", SwingConstants.LEFT);
    constraints.gridy = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.NORTHWEST;
    constraints.insets = new Insets(0, 0, 0, 3);
    constraints.weightx = 0;
    bagLayout.setConstraints(label, constraints);
    panel.add(label);

    tmplDescField = new JTextField();
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.insets = new Insets(0, 0, 0, 0);
    constraints.weightx = 1;
    bagLayout.setConstraints(tmplDescField, constraints);
    panel.add(tmplDescField);

    label = new JLabel("Component controller descriptor:", SwingConstants.LEFT);
    constraints.gridy = 2;
    constraints.fill = GridBagConstraints.NONE;
    constraints.insets = new Insets(0, 0, 0, 3);
    constraints.weightx = 0;
    bagLayout.setConstraints(label, constraints);
    panel.add(label);

    compDescField = new JTextField();
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.insets = new Insets(0, 0, 0, 0);
    constraints.weightx = 1;
    bagLayout.setConstraints(compDescField, constraints);
    panel.add(compDescField);

    return panel;
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    int size = listeners.size();
    String[] names = new String[size + 3];
    listeners.keySet().toArray(names);
    names[size] = CONFIGURATION_BINDING;
    names[size + 1] = DIALOG_MODEL_BINDING;
    names[size + 2] = INNER_GRAPH_VIEW_BINDING;
    return names;
  }

  public Object lookupFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      return configuration;
    } else if (DIALOG_MODEL_BINDING.equals(clientItfName)) {
      return model;
    } else if (INNER_GRAPH_VIEW_BINDING.equals(clientItfName)) {
      return innerGraphView;
    } else if (clientItfName.startsWith(DIALOG_VIEW_LISTENERS_BINDING)) {
      return listeners.get(clientItfName);
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = (Configuration) serverItf;
    } else if (DIALOG_MODEL_BINDING.equals(clientItfName)) {
      model = (DialogModel)serverItf;
      for (int i = 0; i < 2; ++i) {
        if (i == 0) {
          itfTable[i].setModel(model.getClientInterfacesTableModel());
          itfTable[i].setSelectionModel(
            model.getClientInterfacesTableSelectionModel());
        } else {
          itfTable[i].setModel(model.getServerInterfacesTableModel());
          itfTable[i].setSelectionModel(
            model.getServerInterfacesTableSelectionModel());
        }
        itfTable[i].getColumn("Contingency").setCellEditor(
          new DefaultCellEditor(contingencyComboBox));
        itfTable[i].getColumn("Cardinality").setCellEditor(
          new DefaultCellEditor(cardinalityComboBox));
        // specifies the relative widths of the columns (the absolute values are
        // voluntarily big, so that only the relative values are taken into
        // account)
        itfTable[i].getColumn("Name").setPreferredWidth(1000);
        itfTable[i].getColumn("Signature").setPreferredWidth(4000);
        itfTable[i].getColumn("Contingency").setPreferredWidth(1000);
        itfTable[i].getColumn("Cardinality").setPreferredWidth(1000);
        itfTable[i].getSelectionModel().addListSelectionListener(listListener);
      }
      attrTable.setModel(model.getAttributesTableModel());
      attrTable.setSelectionModel(model.getAttributesTableSelectionModel());
      attrTable.getSelectionModel().addListSelectionListener(listListener);
      nameField.setDocument(model.getNameFieldModel());
      typeField.setDocument(model.getTypeFieldModel());
      classField.setDocument(model.getImplementationFieldModel());
      attrControllerField.setDocument(model.getAttrControllerFieldModel());
      tmplDescField.setDocument(model.getTmplControllerDescFieldModel());
      compDescField.setDocument(model.getCompControllerDescFieldModel());
    } else if (INNER_GRAPH_VIEW_BINDING.equals(clientItfName)) {
      innerGraphView = (JComponent)serverItf;

      innerGraphPanel = new JPanel();
      innerGraphPanel.setLayout(new BorderLayout());
      innerGraphPanel.setBorder(BorderFactory.createLoweredBevelBorder());
      innerGraphPanel.add(innerGraphView, "Center");

      GridBagLayout bagLayout = (GridBagLayout)getLayout();
      GridBagConstraints constraints = new GridBagConstraints();
      constraints.insets = new Insets(3, 3, 3, 3);
      constraints.fill = GridBagConstraints.BOTH;
      constraints.weightx = 1;
      constraints.weighty = 6;
      bagLayout.setConstraints(innerGraphPanel, constraints);
      add(innerGraphPanel);
    } else if (clientItfName.startsWith(DIALOG_VIEW_LISTENERS_BINDING)) {
      listeners.put(clientItfName, serverItf);
    }
  }

  public void unbindFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = null;
    } else if (DIALOG_MODEL_BINDING.equals(clientItfName)) {
      model = null;
    } else if (INNER_GRAPH_VIEW_BINDING.equals(clientItfName)) {
      innerGraphView = null;
      remove(innerGraphPanel);
    } else if (clientItfName.startsWith(DIALOG_VIEW_LISTENERS_BINDING)) {
      listeners.remove(clientItfName);
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the ConfigurationListener interface
  // -------------------------------------------------------------------------

  public void changeCountChanged (Component component, long changeCount) {
    // does nothing
  }

  public void rootComponentChanged (final Component oldValue) {
    statusChanged();
  }

  public void nameChanged (final Component component, final String oldValue) {
    if (component == configuration.getRootComponent()) {
      statusChanged();
    }
  }

  public void typeChanged (final Component component, final String oldValue) {
    if (component == configuration.getRootComponent()) {
      statusChanged();
    }
  }

  public void implementationChanged (
    final Component component,
    final String oldValue)
  {
    if (component == configuration.getRootComponent()) {
      statusChanged();
    }
  }

  public void interfaceNameChanged (final Interface i, final String oldValue) {
    // does nothing
  }

  public void interfaceSignatureChanged (
    final Interface i,
    final String oldValue)
  {
    // does nothing
  }

  public void interfaceContingencyChanged (
    final Interface i,
    final boolean oldValue)
  {
    // does nothing
  }

  public void interfaceCardinalityChanged (
    final Interface i,
    final boolean oldValue)
  {
    // does nothing
  }

  public void clientInterfaceAdded (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    // does nothing
  }

  public void clientInterfaceRemoved (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    // does nothing
  }

  public void serverInterfaceAdded (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    // does nothing
  }

  public void serverInterfaceRemoved (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    // does nothing
  }

  public void interfaceBound (
    final ClientInterface citf,
    final ServerInterface sitf)
  {
    // does nothing
  }

  public void interfaceRebound (
    final ClientInterface citf,
    final ServerInterface oldSitf)
  {
    // does nothing
  }

  public void interfaceUnbound (
    final ClientInterface citf,
    final ServerInterface sitf)
  {
    // does nothing
  }

  public void attributeControllerChanged (
    final Component component,
    final String oldValue)
  {
    if (component == configuration.getRootComponent()) {
      statusChanged();
    }
  }

  public void attributeChanged (
    final Component component,
    final String attributeName,
    final String oldValue)
  {
    if (component == configuration.getRootComponent()) {
      statusChanged();
    }
  }

  public void templateControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    // does nothing
  }

  public void componentControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    // does nothing
  }

  public void subComponentAdded (
    final Component parent,
    final Component child,
    final int index)
  {
    if (parent == configuration.getRootComponent()) {
      statusChanged();
    }
  }

  public void subComponentRemoved (
    final Component parent,
    final Component child,
    final int index)
  {
    if (parent == configuration.getRootComponent()) {
      statusChanged();
    }
  }

  /**
   * Updates the colors of the text fields to reflect a change in the
   * component's status. This method is called each time the component's status
   * may have changed.
   */

  protected void statusChanged () {
    long status = configuration.getRootComponent().getStatus();
    nameField.setBackground(
      (status & Component.NAME_MISSING) == 0 ?
      Color.white :
      Constants.ERROR_COLOR);
    /*typeField.setBackground(
      (status & Component.TYPE_MISSING) == 0 ?
      Color.white :
      Constants.ERROR_COLOR);*/
    classField.setBackground(
      (status & Component.IMPLEMENTATION_MISSING) == 0 ?
      Color.white :
      Constants.ERROR_COLOR);
    classField.setForeground(
      (status & Component.IMPLEMENTATION_CLASS_NOT_FOUND) == 0 ?
      Color.black :
      Constants.ERROR_COLOR);
    attrControllerField.setBackground(
      (status & Component.ATTRIBUTE_CONTROLLER_MISSING) == 0 ?
      Color.white :
      Constants.ERROR_COLOR);
    attrControllerField.setForeground(
      (status & Component.ATTRIBUTE_CONTROLLER_CLASS_NOT_FOUND) == 0 ?
      Color.black :
      Constants.ERROR_COLOR);
  }

  // -------------------------------------------------------------------------
  // Other methods
  // -------------------------------------------------------------------------

  public void setVisible (final boolean visible) {
    super.setVisible(visible);
    if (innerGraphView != null) {
      innerGraphView.setVisible(visible);
    }
  }

  /**
   * An action listener used to notify the listeners of this view when buttons
   * are clicked.
   */
  protected class ButtonListener implements ActionListener {

    public void actionPerformed (final ActionEvent e) {
      Object o = e.getSource();
      List listeners = new ArrayList(BasicDialogView.this.listeners.values());
      if (o == itfAddButton[0]) {
        for (int i = 0; i < listeners.size(); ++i) {
          DialogViewListener l = (DialogViewListener)listeners.get(i);
          l.addClientInterfaceButtonClicked();
        }
      } else if (o == itfAddButton[1]) {
        for (int i = 0; i < listeners.size(); ++i) {
          DialogViewListener l = (DialogViewListener)listeners.get(i);
          l.addServerInterfaceButtonClicked();
        }
      } else if (o == itfRemoveButton[0]) {
        for (int i = 0; i < listeners.size(); ++i) {
          DialogViewListener l = (DialogViewListener)listeners.get(i);
          l.removeClientInterfaceButtonClicked();
        }
        itfRemoveButton[0].setEnabled(false);
      } else if (o == itfRemoveButton[1]) {
        for (int i = 0; i < listeners.size(); ++i) {
          DialogViewListener l = (DialogViewListener)listeners.get(i);
          l.removeServerInterfaceButtonClicked();
        }
        itfRemoveButton[1].setEnabled(false);
      } else if (o == attrAddButton) {
        for (int i = 0; i < listeners.size(); ++i) {
          DialogViewListener l = (DialogViewListener)listeners.get(i);
          l.addAttributeButtonClicked();
        }
      } else if (o == attrRemoveButton) {
        for (int i = 0; i < listeners.size(); ++i) {
          DialogViewListener l = (DialogViewListener)listeners.get(i);
          l.removeAttributeButtonClicked();
        }
      }
    }
  }

  /**
   * A table selection listener to enable or disable the "add" and "remove"
   * buttons when the selection of a table changes.
   */

  protected class ListListener implements ListSelectionListener {

    public void valueChanged (final ListSelectionEvent e) {
      ListSelectionModel l;
      l = model.getClientInterfacesTableSelectionModel();
      if (e.getSource() == l) {
        itfRemoveButton[0].setEnabled(!l.isSelectionEmpty());
      }
      l = model.getServerInterfacesTableSelectionModel();
      if (e.getSource() == l) {
        itfRemoveButton[1].setEnabled(!l.isSelectionEmpty());
      }
      l = model.getAttributesTableSelectionModel();
      if (e.getSource() == l) {
        attrRemoveButton.setEnabled(!l.isSelectionEmpty());
      }
    }
  }

  /**
   * A specific button listener to fill classField.
   */

  class ClassFieldButtonListener implements ActionListener {
    public void actionPerformed (final ActionEvent e) {
      if (e.getActionCommand().equals("Impl.")) {
        String st = ClassSelector.initSelector();
        if (st.length() < 1) classField.setText(st);
        else {
          int i = st.indexOf(".class");
          if (i < 0) {
            classField.setText(st);
          } else {
            classField.setText(st.substring(0, i));
          }
        }
        statusChanged ();
      }
    }
  }
}
