/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Basic implementation of the {@link ServerInterface}
 * interface.
 */

public class BasicServerInterface extends BasicInterface
  implements ServerInterface
{

  /**
   * The list of the {@link Binding} objects that refer to this server
   * interface.
   */

  private List bindings;

  /**
   * Constructs a new external server interface.
   *
   * @param owner the component that will own this interface.
   */

  BasicServerInterface (final BasicComponent owner) {
    super(owner);
    complementaryItf = new BasicClientInterface(this);
    bindings = new ArrayList();
  }

  /**
   * Constructs a new internal server interface.
   *
   * @param externalItf the complementary interface of this interface.
   */

  BasicServerInterface (final ClientInterface externalItf) {
    super(externalItf);
    bindings = new ArrayList();
  }

  /**
   * Constructs a new slave, collection server interface.
   *
   * @param rootCollectionItf a master collection server interface.
   */

  BasicServerInterface (final ServerInterface rootCollectionItf, final String suffix) {
    super(rootCollectionItf, 0);
    if (suffix != null) {
      this.name = suffix;
    }
    complementaryItf = new BasicClientInterface(this);
    bindings = new ArrayList();
    ((BasicInterface)rootCollectionItf).addSlaveCollectionInterface(this);
  }

  public List getBindings () {
    return bindings;
  }

  /**
   * Adds the given binding to the list of bindings that refer to this
   * interface.
   *
   * @param binding a new binding that refer to this interface.
   */

  void addBinding (final Binding binding) {
    for (int i = 0; i < bindings.size(); ++i) {
      Binding b = (Binding)bindings.get(i);
      if (b.getClientInterface() == binding.getClientInterface() && b.getServerInterface() == binding.getServerInterface()) {
        return;
      }
    }
    bindings.add(binding);
  }

  /**
   * Removes the given binding from the list of bindings that refer to this
   * interface.
   *
   * @param binding a binding that refer to this interface.
   */

  void removeBinding (final Binding binding) {
    bindings.remove(binding);
  }
}
