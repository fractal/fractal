/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Stroke;

/**
 * Defines font and color constants used in FractalGUI.
 */

public interface Constants {

  /**
   * Font used to draw component names.
   */

  Font NAME_FONT = new Font("MonoSpaced", Font.BOLD, 12);

  /**
   * Font used to draw server interfaces.
   */

  Font PROVIDED_FONT = new Font("MonoSpaced", Font.PLAIN, 12);

  /**
   * Font used to draw client interfaces.
   */

  Font REQUIRED_FONT = new Font("MonoSpaced", Font.ITALIC, 12);

  /**
   * Color used to draw server interfaces.
   */

  Color PROVIDED_COLOR = Color.black; //Color.red;

  /**
   * Color used to draw client interfaces.
   */

  Color REQUIRED_COLOR = Color.black; //new Color(0, 128, 0);

  /**
   * Color used to draw component's controller part.
   */

//  Color COMPONENT_COLOR = new Color(153, 204, 153); //	Color.lightGray;
  Color COMPONENT_COLOR = new Color(255, 255, 183);

  /**
   * Color used to draw bindings.
   */

  Color BINDING_COLOR = new Color(0, 128, 0);

  /**
   * Color used to show errors (in graph, dialog, or tree views).
   */

  Color ERROR_COLOR  = Color.red;

  /**
   * Color used to show warnings (especially for bindings).
   */

  Color WARNING_COLOR  = new Color (153, 0, 0);

  /**
   * Color used to draw selected components or interfaces.
   */

  Color SELECTION_COLOR = new Color(255, 102, 51); //new Color(128, 128, 255);

  /**
   * Default stroke.
   */

  Stroke NORMAL_STROKE = new BasicStroke();

  /**
   * Bold stroke.
   */

  Stroke BOLD_STROKE = new BasicStroke(2);

  /**
   * Dashed stroke.
   */

  Stroke DASHED_STROKE = new BasicStroke(
    1,
    BasicStroke.CAP_SQUARE,
    BasicStroke.JOIN_MITER,
    10,
    new float[] { 5, 5 },
    0);
}
