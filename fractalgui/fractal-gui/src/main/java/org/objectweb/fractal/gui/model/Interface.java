/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.model;

import java.util.List;

/**
 * A component interface.
 */

public interface Interface {

  /**
   * The {@link #getStatus status} flags corresponding to a valid interface.
   */

  long OK = 0;

  /**
   * A {@link #getStatus status} flag indicating that the interface's name is
   * missing.
   */

  long NAME_MISSING = 1;

  /**
   * A {@link #getStatus status} flag indicating that the interface's name is
   * already used by another interface.
   */

  long NAME_ALREADY_USED = 1 << 1;

  /**
   * A {@link #getStatus status} flag indicating that the interface's signature
   * is missing.
   */

  long SIGNATURE_MISSING = 1 << 2;

  /**
   * A {@link #getStatus status} flag indicating that the Java interface
   * designated by the interface's signature cannot be found.
   */

  long SIGNATURE_CLASS_NOT_FOUND = 1 << 3;

  /**
   * A {@link #getStatus status} flag indicating that the Java interface
   * designated by the interface's signature is invalid.
   */

  long SIGNATURE_CLASS_INVALID = 1 << 4;

  /**
   * A {@link #getStatus status} flag indicating that this mandatory interface
   * is not bound.
   */

  long MANDATORY_INTERFACE_NOT_BOUND = 1 << 5;

  /**
   * Returns the component that owns this interface.
   *
   * @return the component that owns this interface.
   */

  Component getOwner ();

  /**
   * Returns the status of this interface.
   *
   * @return the status flags of this interface. Each flag, i.e., each bit of
   *     the returned value is independent from the other, and indicates an
   *     error if it is set.
   * @see #setStatus
   */

  long getStatus ();

  /**
   * Sets the status of this interface. <i>This method is reserved for model
   * providers, and must not be called by model users</i>.
   *
   * @param status the new status of this interface.
   * @see #getStatus
   */

  void setStatus (long status);

  /**
   * Returns the name of this interface.
   *
   * @return the name of this interface.
   * @see #setName
   */

  String getName ();

  /**
   * Sets the name of this interface. This method notifies the configuration
   * listeners, via the {@link ConfigurationListener#interfaceNameChanged
   * interfaceNameChanged} method.
   *
   * @param name the new interface name.
   * @see #getName
   */

  void setName (String name);

  /**
   * Returns the signature of this interface.
   *
   * @return the signature of this interface.
   * @see #setSignature
   */

  String getSignature ();

  /**
   * Sets the signature of this interface. This method notifies the
   * configuration listeners, via the {@link
   * ConfigurationListener#interfaceSignatureChanged interfaceSignatureChanged}
   * method.
   *
   * @param signature the new interface signature.
   * @see #getSignature
   */

  void setSignature (String signature);

  /**
   * Returns <tt>true</tt> if this interface is an internal interface.
   *
   * @return <tt>true</tt> if this interface is an internal interface,
   *      <tt>false</tt> otherwise.
   */

  boolean isInternal ();

  /**
   * Returns the contingency of this interface.
   *
   * @return <tt>true</tt> if this interface is optional, <tt>false</tt>
   *      otherwise.
   */

  boolean isOptional ();

  /**
   * Sets the contingency of this interface. This method notifies the
   * configuration listeners, via the {@link
   * ConfigurationListener#interfaceContingencyChanged
   * interfaceContingencyChanged} method.
   *
   * @param isOptional the new interface contingency.
   */

  void setIsOptional (boolean isOptional);

  /**
   * Returns the cardinality of this interface.
   *
   * @return <tt>true</tt> if this interface is a collection interface,
   *      <tt>false</tt> otherwise.
   */

  boolean isCollection ();

  /**
   * Sets the cadrinality of this interface. This method notifies the
   * configuration listeners, via the {@link
   * ConfigurationListener#interfaceCardinalityChanged
   * interfaceCardinalityChanged} method.
   *
   * @param isCollection the new interface cardinality.
   */

  void setIsCollection (boolean isCollection);

  /**
   * Returns the master collection interface of this interface.
   *
   * @return the master collection interface of this interface, or <tt>null</tt>
   *      if this interface is not a slave collection interface.
   */

  Interface getMasterCollectionInterface ();

  /**
   * Returns the slave collection interfaces of this interface.
   *
   * @return an unmodifiable list of the slave collection interfaces of this
   *      interface. This list is empty if this interface is not a master
   *      collection interface.
   */

  List getSlaveCollectionInterfaces ();

  /**
   * Returns the complementary interface of this interface.
   *
   * @return the complementary interface of this interface.
   */

  Interface getComplementaryInterface ();

  /**
   * Return the master interface of this interface.
   *
   * @return the master interface of this interface, or <tt>null</tt> if this
   *      interface is not an interface of a slave component.
   */

  Interface getMasterInterface ();
}
