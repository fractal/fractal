/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.graph.model.Display;
import org.objectweb.fractal.gui.graph.model.DisplayListener;
import org.objectweb.fractal.gui.graph.model.Rect;
import org.objectweb.fractal.swing.AbstractAction;

import java.awt.event.ActionEvent;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.KeyStroke;

/**
 * An action that just calls the {@link Display#setIsAntialiasing
 * setIsAntiAliasing} method on a {@link Display}. This action listens to the
 * display model in order to change its name when the anti aliasing option
 * changes.
 */

public class AntiAliasAction extends AbstractAction implements
  DisplayListener,
  BindingController
{

  /**
   * A mandatory client interface bound to a {@link Display display} model. This
   * is the display whose anti aliasing option is modified by this controller
   * component.
   */

  public final static String DISPLAY_BINDING = "display";

  /**
   * The display client interface.
   */

  private Display display;

  /**
   * Constructs a new {@link AntiAliasAction} component.
   */

  public AntiAliasAction () {
    putValue(NAME, "Antialiasing");
    URL url = getClass().getResource(
      "/org/objectweb/fractal/gui/resources/empty.gif");
    putValue(SMALL_ICON, new ImageIcon(url));
    putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("alt L"));
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] { DISPLAY_BINDING };
  }

  public Object lookupFc (final String clientItfName) {
    if (DISPLAY_BINDING.equals(clientItfName)) {
      return display;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (DISPLAY_BINDING.equals(clientItfName)) {
      display = (Display)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (DISPLAY_BINDING.equals(clientItfName)) {
      display = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the DisplayListener interface
  // -------------------------------------------------------------------------

  public void displayedAreaChanged (final Rect oldValue) {
    // does nothing
  }

  public void antialiasingChanged () {
    if (display.isAntialiasing()) {
      putValue(NAME, "Aliasing");
    } else {
      putValue(NAME, "Antialiasing");
    }
  }

  public void maxDepthChanged () {
    // does nothing
  }

  public void itfNameDisplayModeChanged () {
    // does nothing
  }

  // -------------------------------------------------------------------------
  // Overriden JCheckBoxMenuItem methods
  // -------------------------------------------------------------------------

  public void actionPerformed (final ActionEvent e) {
    display.setIsAntialiasing(!display.isAntialiasing());
  }
}
