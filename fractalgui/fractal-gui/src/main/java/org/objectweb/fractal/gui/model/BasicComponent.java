/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO notifier aussi tous les slave components

/**
 * Basic implementation of the {@link Component} interface.
 */

public class BasicComponent extends AbstractComponent {

  /**
   * The configuration to which this component belongs.
   */

  private BasicConfiguration owner;

  /**
   * Status of this component.
   */

  private long status;

  /**
   * Name of this component.
   */

  private String name;

  /**
   * Name of the type of this component.
   */

  private String type;

  /**
   * Name of the class that implements this component.
   */

  private String implementation;

  /**
   * The external client interfaces of this component. This list is a list of
   * {@link Interface} objects.
   */

  private List clientInterfaces;

  /**
   * The external server interfaces of this component. This list is a list of
   * {@link Interface} objects.
   */

  protected List serverInterfaces;

  /**
   * Name of the attribute controller interface of this component.
   */

  private String attributeController;

  /**
   * Values of the attributes of this component.
   */

  private Map attributes;

  /**
   * Template controller descriptor of this component.
   */

  private String tmplDesc;

  /**
   * Component controller descriptor of this component.
   */

  private String compDesc;

  /**
   * List of the slave components of this master component. This list is a list
   * of {@link Component} objects. It is empty if this component is not a master
   * component.
   */

  private List slaveComponents;

  /**
   * List of the sub components of this component. This list is a list of {@link
   * Component} objects. It is empty if this component is not a composite
   * component.
   */

  private List subComponents;

  /**
   * If addXXXInterface and removeXXXInterface methods must notify the listeners
   * or not. See {@link #setBinding setBinding}.
   */

  private static boolean notificationEnabled;

  /**
   * Constructs a new component.
   *
   * @param owner the configuration to which the component will belong.
   */

  protected BasicComponent (final BasicConfiguration owner) {
    this.owner = owner;
    status = NAME_MISSING | /*TYPE_MISSING |*/ IMPLEMENTATION_MISSING;
    name = "";
    type = "";
    implementation = "";
    clientInterfaces = new ArrayList();
    serverInterfaces = new ArrayList();
    attributeController = "";
    attributes = new HashMap();
    tmplDesc = "";
    compDesc = "";
    slaveComponents = new ArrayList();
    subComponents = new ArrayList();
    notificationEnabled = true;
  }

  /**
   * Returns the configuration to which this component belongs.
   *
   * @return the configuration to which this component belongs.
   */

  public BasicConfiguration getOwner () {
    return owner;
  }

  public Configuration getConfiguration () {
    return owner;
  }

  public long getStatus () {
    return status;
  }

  public void setStatus (final long status) {
    this.status = status;
  }

  // -------------------------------------------------------------------------
  // Name, Type, Implementation
  // -------------------------------------------------------------------------

  public String getName () {
    return name;
  }

  public void setName (final String name) {
    if (name == null) {
      throw new IllegalArgumentException();
    }
    String oldName = this.name;
    if (!name.equals(oldName)) {
      List vetoableListeners = getOwner().getVetoableListeners();
      for (int i = 0; i < vetoableListeners.size(); ++i) {
        VetoableConfigurationListener l =
          (VetoableConfigurationListener)vetoableListeners.get(i);
        l.canChangeName(this);
      }
      this.name = name;
      List listeners = getOwner().getListeners();
      for (int i = 0; i < listeners.size(); ++i) {
        ConfigurationListener l = (ConfigurationListener)listeners.get(i);
        l.nameChanged(this, oldName);
      }
    }
  }

  public String getType () {
    return type;
  }

  public void setType (final String type) {
    if (type == null) {
      throw new IllegalArgumentException();
    }
    String oldType = this.type;
    if (!type.equals(oldType)) {
      List vetoableListeners = getOwner().getVetoableListeners();
      for (int i = 0; i < vetoableListeners.size(); ++i) {
        VetoableConfigurationListener l =
          (VetoableConfigurationListener)vetoableListeners.get(i);
        l.canChangeType(this);
      }
      this.type = type;
      List listeners = getOwner().getListeners();
      for (int i = 0; i < listeners.size(); ++i) {
        ConfigurationListener l = (ConfigurationListener)listeners.get(i);
        l.typeChanged(this, oldType);
      }
    }
  }

  public String getImplementation () {
    return implementation;
  }

  public void setImplementation (final String implementation) {
    if (implementation == null) {
      throw new IllegalArgumentException();
    }
    String oldImplementation = this.implementation;
    if (!implementation.equals(oldImplementation)) {
      List vetoableListeners = getOwner().getVetoableListeners();
      for (int i = 0; i < vetoableListeners.size(); ++i) {
        VetoableConfigurationListener l =
          (VetoableConfigurationListener)vetoableListeners.get(i);
        l.canChangeImplementation(this);
      }
      this.implementation = implementation;
      List listeners = getOwner().getListeners();
      for (int i = 0; i < listeners.size(); ++i) {
        ConfigurationListener l = (ConfigurationListener)listeners.get(i);
        l.implementationChanged(this, oldImplementation);
      }
    }
  }

  // -------------------------------------------------------------------------
  // Client and Server interfaces
  // -------------------------------------------------------------------------

  public List getClientInterfaces () {
    return Collections.unmodifiableList(clientInterfaces);
  }

  public Interface getClientInterface (final String name) {
    if (name == null) {
      throw new IllegalArgumentException();
    }
    for (int i = 0; i < clientInterfaces.size(); ++i) {
      Interface itf = (Interface)clientInterfaces.get(i);
      if (itf.getName().equals(name)) {
        return itf;
      }
    }
    return null;
  }

  public void addClientInterface (final ClientInterface itf) {
    if (itf == null) {
      throw new IllegalArgumentException();
    }
    if (itf.getOwner() != this || itf.isInternal()) {
      throw new RuntimeException("Internal error");
    }
    if (!clientInterfaces.contains(itf)) {
      List vetoableListeners = getOwner().getVetoableListeners();
      for (int i = 0; i < vetoableListeners.size(); ++i) {
        VetoableConfigurationListener l =
          (VetoableConfigurationListener)vetoableListeners.get(i);
        l.canAddClientInterface(this, itf);
      }
      int index = clientInterfaces.size();
      // add itf
      clientInterfaces.add(itf);
      // restore link in master collection interface, if any
      if (itf.getMasterCollectionInterface() != null) {
        BasicInterface bitf =
          (BasicInterface)itf.getMasterCollectionInterface();
        bitf.addSlaveCollectionInterface(itf);
      }
      // restore bindings, if any
      restoreBinding(itf, true);
      // notify listeners
      if (notificationEnabled) {
        List listeners = getOwner().getListeners();
        for (int i = 0; i < listeners.size(); ++i) {
          ConfigurationListener l = (ConfigurationListener)listeners.get(i);
          l.clientInterfaceAdded(this, itf, index);
        }
      }
    }
  }

  public void removeClientInterface (final ClientInterface itf) {
    if (itf == null) {
      throw new IllegalArgumentException();
    }
    if (itf.getOwner() != this || itf.isInternal()) {
      throw new RuntimeException("Internal error");
    }
    if (clientInterfaces.contains(itf)) {
      List vetoableListeners = getOwner().getVetoableListeners();
      for (int i = 0; i < vetoableListeners.size(); ++i) {
        VetoableConfigurationListener l =
          (VetoableConfigurationListener)vetoableListeners.get(i);
        l.canRemoveClientInterface(this, itf);
      }
      if (itf.getSlaveCollectionInterfaces().size() > 0) {
        throw new IllegalOperationException(
          "Cannot remove this root collection interface. " +
          "You must first remove all the interfaces of the collection");
      }
      // remove itf
      int index = clientInterfaces.indexOf(itf);
      clientInterfaces.remove(itf);
      // remove link in master collection interface, if any
      if (itf.getMasterCollectionInterface() != null) {
        BasicInterface bitf =
          (BasicInterface)itf.getMasterCollectionInterface();
        bitf.removeSlaveCollectionInterface(itf);
      }
      // remove bindings, if any
      removeBinding(itf, true);
      // notify listeners
      if (notificationEnabled) {
        List listeners = getOwner().getListeners();
        for (int i = 0; i < listeners.size(); ++i) {
          ConfigurationListener l = (ConfigurationListener)listeners.get(i);
          l.clientInterfaceRemoved(this, itf, index);
        }
      }
    }
  }

  public List getServerInterfaces () {
   return Collections.unmodifiableList(serverInterfaces);
  }

  public Interface getServerInterface (final String name) {
    if (name == null) {
      throw new IllegalArgumentException();
    }
    for (int i = 0; i < serverInterfaces.size(); ++i) {
      Interface itf = (Interface)serverInterfaces.get(i);
      if (itf.getName().equals(name)) {
        return itf;
      }
    }
    return null;
  }


  public void addServerInterface (final ServerInterface itf) {
    if (itf == null) {
      throw new IllegalArgumentException();
    }
    if (itf.getOwner() != this || itf.isInternal()) {
      throw new RuntimeException("Internal error");
    }
    if (!serverInterfaces.contains(itf)) {
      List vetoableListeners = getOwner().getVetoableListeners();
      for (int i = 0; i < vetoableListeners.size(); ++i) {
        VetoableConfigurationListener l =
          (VetoableConfigurationListener)vetoableListeners.get(i);
        l.canAddServerInterface(this, itf);
      }
      int index = serverInterfaces.size();
      // add itf
      serverInterfaces.add(itf);
      // restore link in master collection interface, if any
      if (itf.getMasterCollectionInterface() != null) {
        BasicInterface bitf =
          (BasicInterface)itf.getMasterCollectionInterface();
        bitf.addSlaveCollectionInterface(itf);
      }
      // restore bindings, if any
      restoreBinding(itf, true);
      // notify listeners
      if (notificationEnabled) {
        List listeners = getOwner().getListeners();
        for (int i = 0; i < listeners.size(); ++i) {
          ConfigurationListener l = (ConfigurationListener)listeners.get(i);
          l.serverInterfaceAdded(this, itf, index);
        }
      }
    }
  }

  public void removeServerInterface (final ServerInterface itf) {
    if (itf == null) {
      throw new IllegalArgumentException();
    }
    if (itf.getOwner() != this || itf.isInternal()) {
      throw new RuntimeException("Internal error");
    }
    if (serverInterfaces.contains(itf)) {
      List vetoableListeners = getOwner().getVetoableListeners();
      for (int i = 0; i < vetoableListeners.size(); ++i) {
        VetoableConfigurationListener l =
          (VetoableConfigurationListener)vetoableListeners.get(i);
        l.canRemoveServerInterface(this, itf);
      }
      if (itf.getSlaveCollectionInterfaces().size() > 0) {
        throw new IllegalOperationException(
          "Cannot remove this root collection interface. " +
          "You must first remove all the interfaces of the collection");
      }
      // remove itf
      int index = serverInterfaces.indexOf(itf);
      serverInterfaces.remove(itf);
      // remove link in master collection interface, if any
      if (itf.getMasterCollectionInterface() != null) {
        BasicInterface bitf =
          (BasicInterface)itf.getMasterCollectionInterface();
        bitf.removeSlaveCollectionInterface(itf);
      }
      // remove bindings, if any
      removeBinding(itf, true);
      // notify listeners
      if (notificationEnabled) {
        List listeners = getOwner().getListeners();
        for (int i = 0; i < listeners.size(); ++i) {
          ConfigurationListener l = (ConfigurationListener)listeners.get(i);
          l.serverInterfaceRemoved(this, itf, index);
        }
      }
    }
  }

  // -------------------------------------------------------------------------
  // Bindings
  // -------------------------------------------------------------------------

  public void bind (ClientInterface citf, final String suffix, final ServerInterface sitf) {
    if (citf == null || sitf == null) {
      throw new IllegalArgumentException();
    }
    if (citf.getBinding() != null) {
      throw new IllegalOperationException("Interface already bound");
    }
    List vetoableListeners = getOwner().getVetoableListeners();
    for (int i = 0; i < vetoableListeners.size(); ++i) {
      VetoableConfigurationListener l =
        (VetoableConfigurationListener)vetoableListeners.get(i);
      l.canBindInterface(citf);
    }
    // TODO check citf belongs to this
    checkBinding(citf, sitf);

    // if citf is a master collection itf, create a new client interface
    if (citf.isCollection() && citf.getMasterCollectionInterface() == null) {
      if (citf.isInternal()) {
        ServerInterface s = (ServerInterface)citf.getComplementaryInterface();
        s = new BasicServerInterface(s, suffix);
        serverInterfaces.add(s);
        citf = (ClientInterface)s.getComplementaryInterface();
      } else {
        citf = new BasicClientInterface(citf, suffix);
        clientInterfaces.add(citf);
      }
    }

    Binding b = new BasicBinding(citf, sitf);
    setBinding(citf, b);
    addBinding(sitf, b);
    // notify listeners
    List listeners = getOwner().getListeners();
    for (int i = 0; i < listeners.size(); ++i) {
      ConfigurationListener l = (ConfigurationListener)listeners.get(i);
      l.interfaceBound(citf, sitf);
    }
  }

  public void rebind (final ClientInterface citf, final ServerInterface sitf) {
    if (citf == null || sitf == null) {
      throw new IllegalArgumentException();
    }
    if (citf.getBinding() == null) {
      throw new IllegalOperationException("Interface not bound");
    }
    List vetoableListeners = getOwner().getVetoableListeners();
    for (int i = 0; i < vetoableListeners.size(); ++i) {
      VetoableConfigurationListener l =
        (VetoableConfigurationListener)vetoableListeners.get(i);
      l.canRebindInterface(citf);
    }
    // TODO check citf belongs to this
    checkBinding(citf, sitf);

    Binding b = citf.getBinding();
    ServerInterface oldSitf = b.getServerInterface();
    removeBinding(oldSitf, b);
    b = new BasicBinding(citf, sitf);
    setBinding(citf, b);
    addBinding(sitf, b);
    // notify listeners
    List listeners = getOwner().getListeners();
    for (int i = 0; i < listeners.size(); ++i) {
      ConfigurationListener l = (ConfigurationListener)listeners.get(i);
      l.interfaceRebound(citf, oldSitf);
    }
  }

  public void unbind (final ClientInterface citf) {
    if (citf == null) {
      throw new IllegalArgumentException();
    }
    if (citf.getBinding() == null) {
      throw new IllegalOperationException("Interface not bound");
    }
    List vetoableListeners = getOwner().getVetoableListeners();
    for (int i = 0; i < vetoableListeners.size(); ++i) {
      VetoableConfigurationListener l =
        (VetoableConfigurationListener)vetoableListeners.get(i);
      l.canUnbindInterface(citf);
    }
    // TODO check citf belongs to this

    // if citf is a slave collection interface, delete it
    if (citf.isCollection()) {
      if (citf.isInternal()) {
        removeServerInterface((ServerInterface)citf.getComplementaryInterface());
      } else {
        removeClientInterface(citf);
      }
      return;
    }

    Binding b = citf.getBinding();
    ServerInterface sitf = b.getServerInterface();
    setBinding(citf, null);
    removeBinding(sitf, b);
    // notify listeners
    List listeners = getOwner().getListeners();
    for (int i = 0; i < listeners.size(); ++i) {
      ConfigurationListener l = (ConfigurationListener)listeners.get(i);
      l.interfaceUnbound(citf, sitf);
    }
  }

  /**
   * Checks that the given binding verifies the structural constraints for
   * bindings. There are several cases, for normal, import and export bindings.
   *
   * @param citf a client interface.
   * @param sitf a server interface.
   */

  private void checkBinding (
    final ClientInterface citf,
    final ServerInterface sitf)
  {
    List cparents = new ArrayList();
    Component c = citf.getOwner();
    cparents.add(c.getParent());
    List slaves = c.getSlaveComponents();
    for (int i = 0; i < slaves.size(); ++i) {
      cparents.add(((Component)slaves.get(i)).getParent());
    }
    if (citf.isInternal()) {
      if (sitf.isInternal()) {
        if (sitf.getOwner() != citf.getOwner()) {
          throw new IllegalOperationException(
            "Cannot bind internal interfaces of two distinct components");
        }
      } else if (sitf.getOwner().getParent() != citf.getOwner()) {
        throw new IllegalOperationException(
          "An internal interface must be bound to a direct sub component " +
          "or to another internal interface of the same component");
      }
    } else if (sitf.isInternal()) {
      if (!cparents.contains(sitf.getOwner())) {
        throw new IllegalOperationException(
          "Cannot bind a component to the internal interface of another " +
          "component, unless it is its parent component");
      }
    } else if (!cparents.contains(sitf.getOwner().getParent())) {
      throw new IllegalOperationException(
        "Cannot bind two components that do not belong to the same component");
    }
  }

  // -------------------------------------------------------------------------
  // Attributes
  // -------------------------------------------------------------------------

  public String getAttributeController () {
    return attributeController;
  }

  public void setAttributeController (final String attributeController) {
    if (attributeController == null) {
      throw new IllegalArgumentException();
    }
    String oldAttributeController = this.attributeController;
    if (!attributeController.equals(oldAttributeController)) {
      List vetoableListeners = getOwner().getVetoableListeners();
      for (int i = 0; i < vetoableListeners.size(); ++i) {
        VetoableConfigurationListener l =
          (VetoableConfigurationListener)vetoableListeners.get(i);
        l.canChangeAttributeController(this);
      }
      this.attributeController = attributeController;
      List listeners = getOwner().getListeners();
      for (int i = 0; i < listeners.size(); ++i) {
        ConfigurationListener l = (ConfigurationListener)listeners.get(i);
        l.attributeControllerChanged(this, oldAttributeController);
      }
    }
  }

  public List getAttributeNames () {
    return Collections.unmodifiableList(new ArrayList(attributes.keySet()));
  }

  public String getAttribute (final String attributeName) {
    if (attributeName == null) {
      throw new IllegalArgumentException();
    }
    return (String)attributes.get(attributeName);
  }

  public void setAttribute (
    final String attributeName,
    final String attributeValue)
  {
    if (attributeName == null) {
      throw new IllegalArgumentException();
    }
    String oldValue = (String)attributes.get(attributeName);
    if (oldValue == null && attributeValue == null) {
      return;
    }
    if ((oldValue == null && attributeValue != null) ||
        (oldValue != null && attributeValue == null) ||
        !oldValue.equals(attributeValue))
    {
      List vetoableListeners = getOwner().getVetoableListeners();
      for (int i = 0; i < vetoableListeners.size(); ++i) {
        VetoableConfigurationListener l =
          (VetoableConfigurationListener)vetoableListeners.get(i);
        l.canChangeAttribute(this, attributeName);
      }
      if (attributeValue == null) {
        attributes.remove(attributeName);
      } else {
        attributes.put(attributeName, attributeValue);
      }
      List listeners = getOwner().getListeners();
      for (int i = 0; i < listeners.size(); ++i) {
        ConfigurationListener l = (ConfigurationListener)listeners.get(i);
        l.attributeChanged(this, attributeName, oldValue);
      }
    }
  }

  // -------------------------------------------------------------------------
  // Controller descriptors
  // -------------------------------------------------------------------------

  public String getTemplateControllerDescriptor () {
    return tmplDesc;
  }

  public void setTemplateControllerDescriptor (final String desc) {
    if (desc == null) {
      throw new IllegalArgumentException();
    }
    String oldDesc = this.tmplDesc;
    if (!desc.equals(oldDesc)) {
      List vetoableListeners = getOwner().getVetoableListeners();
      for (int i = 0; i < vetoableListeners.size(); ++i) {
        VetoableConfigurationListener l =
          (VetoableConfigurationListener)vetoableListeners.get(i);
        l.canChangeTemplateControllerDescriptor(this);
      }
      this.tmplDesc = desc;
      List listeners = getOwner().getListeners();
      for (int i = 0; i < listeners.size(); ++i) {
        ConfigurationListener l = (ConfigurationListener)listeners.get(i);
        l.templateControllerDescriptorChanged(this, oldDesc);
      }
    }
  }

  public String getComponentControllerDescriptor () {
    return compDesc;
  }

  public void setComponentControllerDescriptor (final String desc) {
    if (desc == null) {
      throw new IllegalArgumentException();
    }
    String oldDesc = this.compDesc;
    if (!desc.equals(oldDesc)) {
      List vetoableListeners = getOwner().getVetoableListeners();
      for (int i = 0; i < vetoableListeners.size(); ++i) {
        VetoableConfigurationListener l =
          (VetoableConfigurationListener)vetoableListeners.get(i);
        l.canChangeComponentControllerDescriptor(this);
      }
      this.compDesc = desc;
      List listeners = getOwner().getListeners();
      for (int i = 0; i < listeners.size(); ++i) {
        ConfigurationListener l = (ConfigurationListener)listeners.get(i);
        l.componentControllerDescriptorChanged(this, oldDesc);
      }
    }
  }

  // -------------------------------------------------------------------------
  // Sharing
  // -------------------------------------------------------------------------

  public boolean isShared () {
    return slaveComponents.size() > 0;
  }

  public Component getMasterComponent () {
    return null;
  }

  public List getSlaveComponents () {
    return Collections.unmodifiableList(slaveComponents);
  }

  /**
   * Adds the given component to the list of slave components of this component.
   *
   * @param slaveComponent a new slave component of this component.
   */

  void addSlaveComponent (final Component slaveComponent) {
    if (!slaveComponents.contains(slaveComponent)) {
      slaveComponents.add(slaveComponent);
    }
  }

  /**
   * Removes the given component from the list of slave components of this
   * component.
   *
   * @param slaveComponent a slave component of this component.
   */

  void removeSlaveComponent (final Component slaveComponent) {
    slaveComponents.remove(slaveComponent);
  }

  // -------------------------------------------------------------------------
  // Composite specific informations
  // -------------------------------------------------------------------------

  public boolean isComposite () {
    return subComponents.size() > 0;
  }

  public List getSubComponents () {
    return Collections.unmodifiableList(subComponents);
  }

  public Component getSubComponent (final String name) {
    if (name == null) {
      throw new IllegalArgumentException();
    }
    for (int i = 0; i < subComponents.size(); ++i) {
      Component c = (Component)subComponents.get(i);
      if (c.getName().equals(name)) {
        return c;
      }
    }
    return null;
  }

  public void addSubComponent (final Component child) {
    if (!subComponents.contains(child)) {
      implementation = "";
      int index = subComponents.size();

      if (child.getMasterComponent() != null) {
        if (child.getMasterComponent() == this ||
            child.getMasterComponent().contains(this))
        {
          throw new IllegalOperationException(
            "Cannot add shared a component inside its master component");
        }
      }

      List vetoableListeners = getOwner().getVetoableListeners();
      for (int i = 0; i < vetoableListeners.size(); ++i) {
        VetoableConfigurationListener l =
          (VetoableConfigurationListener)vetoableListeners.get(i);
        l.canAddSubComponent(this, child);
      }

      // add sub component
      subComponents.add(child);
      ((AbstractComponent)child).setParent(this);
      // restore bindings, if any
      List itfs = child.getClientInterfaces();
      for (int i = 0; i < itfs.size(); ++i) {
        restoreBinding((ClientInterface)itfs.get(i), false);
      }
      itfs = child.getServerInterfaces();
      for (int i = 0; i < itfs.size(); ++i) {
        restoreBinding((ServerInterface)itfs.get(i), false);
      }
      // restores references from external master components to
      // slave components inside child
      List slaves = new ArrayList();
      getSlavesOfExternalComponents(child, child, slaves);
      for (int i = 0; i < slaves.size(); ++i) {
        Component slave = (Component)slaves.get(i);
        ((BasicComponent)slave.getMasterComponent()).addSlaveComponent(slave);
      }
      // notify listeners
      List listeners = getOwner().getListeners();
      for (int i = 0; i < listeners.size(); ++i) {
        ConfigurationListener l = (ConfigurationListener)listeners.get(i);
        l.subComponentAdded(this, child, index);
      }
    }
  }

  public void removeSubComponent (final Component child) {
    if (subComponents.contains(child)) {
      int index = subComponents.indexOf(child);

      if (child.containsMasterOfExternalComponent(child)) {
        throw new IllegalOperationException(
          "Cannot remove a component that contains or is the master " +
          "component of an external shared component");
      }

      List vetoableListeners = getOwner().getVetoableListeners();
      for (int i = 0; i < vetoableListeners.size(); ++i) {
        VetoableConfigurationListener l =
          (VetoableConfigurationListener)vetoableListeners.get(i);
        l.canRemoveSubComponent(this, child);
      }

      // remove sub component
      subComponents.remove(child);
      ((AbstractComponent)child).setParent(null);
      // remove bindings, if any
      List itfs = child.getClientInterfaces();
      for (int i = 0; i < itfs.size(); ++i) {
        removeBinding((ClientInterface)itfs.get(i), false);
      }
      itfs = child.getServerInterfaces();
      for (int i = 0; i < itfs.size(); ++i) {
        removeBinding((ServerInterface)itfs.get(i), false);
      }
      // removes references from external master components to
      // slave components inside child
      List slaves = new ArrayList();
      getSlavesOfExternalComponents(child, child, slaves);
      for (int i = 0; i < slaves.size(); ++i) {
        Component slave = (Component)slaves.get(i);
        ((BasicComponent)slave.getMasterComponent()).removeSlaveComponent(slave);
      }
      // notify listeners
      List listeners = getOwner().getListeners();
      for (int i = 0; i < listeners.size(); ++i) {
        ConfigurationListener l = (ConfigurationListener)listeners.get(i);
        l.subComponentRemoved(this, child, index);
      }
    }
  }

  /**
   * Returns the sub components of the given root component that are slaves of
   * external components. 'External' meaning components that are not sub
   * components of the given root component.
   *
   * @param c a direct or indirect sub component of the root component.
   * @param root a root component.
   * @param slaves the list to which the sub components found must be added.
   */

  private static void getSlavesOfExternalComponents (
    final Component c,
    final Component root,
    final List slaves)
  {
    Component master = c.getMasterComponent();
    if (master != null && !root.contains(master)) {
      slaves.add(c);
    }
    List subComponents = c.getSubComponents();
    for (int i = 0; i < subComponents.size(); ++i) {
      Component subComponent = (Component)subComponents.get(i);
      getSlavesOfExternalComponents(subComponent, root, slaves);
    }
  }

  // -------------------------------------------------------------------------
  // Other methods
  // -------------------------------------------------------------------------

  /**
   * Restores the binding associated to the given client interface. If the
   * binding associated to the given interface is not <tt>null</tt>, this method
   * restores the redundant reference from the server interface to the binding
   * object, by calling the {@link #addBinding(ServerInterface,Binding)
   * addBinding} method. If <tt>internal</tt> is <tt>true</tt>, the bindings
   * <i>to</i> the complementary interface of the given interface are also
   * restored: for each binding that refer to this complementary interface, the
   * redundant reference from the client interface to the binding is restored by
   * calling the {@link #setBinding setBinding} method.
   *
   * @param itf an external client interface.
   * @param internal <tt>true</tt> if the bindings <i>to</i> the given interface
   *      must be restored also, of <tt>false</tt> if only the binding
   *      <i>from</i> the interface must be restored.
   */

  private void restoreBinding (
    final ClientInterface itf,
    final boolean internal)
  {
    Binding b = itf.getBinding();
    if (b != null) {
      addBinding(b.getServerInterface(), b);
    }
    if (internal && itf.getOwner().isComposite()) {
      List bs = ((ServerInterface)itf.getComplementaryInterface()).getBindings();
      for (int i = 0; i < bs.size(); ++i) {
        b = (Binding)bs.get(i);
        setBinding(b.getClientInterface(), b);
      }
    }
  }

  /**
   * Removes the binding associated to the given client interface. This method
   * only removes the redundant reference from the server interface to the
   * binding object (if it is not <tt>null</tt>). If <tt>internal</tt> is
   * <tt>true</tt>, the bindings <i>to</i> the complementary interface of the
   * given interface are also removed: for each binding that refer to this
   * complementary interface, the redundant reference from the client interface
   * to the binding is removed by calling the {@link #setBinding setBinding}
   * method.
   *
   * @param itf an external client interface.
   * @param internal <tt>true</tt> if the bindings <i>to</i> the given interface
   *      must be removed also, of <tt>false</tt> if only the binding
   *      <i>from</i> the interface must be removed.
   */

  private void removeBinding (
    final ClientInterface itf,
    final boolean internal)
  {
    Binding b = itf.getBinding();
    if (b != null) {
      removeBinding(b.getServerInterface(), b);
    }
    if (internal && itf.getOwner().isComposite()) {
      List bs = ((ServerInterface)itf.getComplementaryInterface()).getBindings();
      for (int i = 0; i < bs.size(); ++i) {
        b = (Binding)bs.get(i);
        setBinding(b.getClientInterface(), null);
      }
    }
  }

  /**
   * Restores the binding associated the given server interface. For each
   * binding that refer to the given interface, this method restores the
   * redundant reference from the client interface to the binding object, by
   * calling the {@link #setBinding setBinding} method. If <tt>internal</tt> is
   * <tt>true</tt>, the binding <i>from</i> the complementary interface of the
   * given interface is also restored: the redundant reference from the server
   * interface to the binding is restored by calling the {@link #addBinding
   * addBinding} method.
   *
   * @param itf an external server interface.
   * @param internal <tt>true</tt> if the binding <i>from</i> the given
   *      interface must be restored also, of <tt>false</tt> if only the
   *      bindings <i>to</i> the interface must be restored.
   */

  private void restoreBinding (
    final ServerInterface itf,
    final boolean internal)
  {
    List bs = itf.getBindings();
    for (int i = 0; i < bs.size(); ++i) {
      Binding b = (Binding)bs.get(i);
      setBinding(b.getClientInterface(), b);
    }
    if (internal && itf.getOwner().isComposite()) {
      Binding b = ((ClientInterface)itf.getComplementaryInterface()).getBinding();
      if (b != null) {
        addBinding(b.getServerInterface(), b);
      }
    }
  }

  /**
   * Removes the binding associated the given server interface. For each
   * binding that refer to the given interface, this method removes the
   * redundant reference from the client interface to the binding object, by
   * calling the {@link #setBinding setBinding} method. If <tt>internal</tt> is
   * <tt>true</tt>, the binding <i>from</i> the complementary interface of the
   * given interface is also removed: the redundant reference from the server
   * interface to the binding is removed by calling the {@link #removeBinding
   * removeBinding} method.
   *
   * @param itf an external server interface.
   * @param internal <tt>true</tt> if the binding <i>from</i> the given
   *      interface must be removed also, of <tt>false</tt> if only the
   *      bindings <i>to</i> the interface must be removed.
   */

  private void removeBinding (
    final ServerInterface itf,
    final boolean internal)
  {
    List bs = itf.getBindings();
    for (int i = 0; i < bs.size(); ++i) {
      Binding b = (Binding)bs.get(i);
      setBinding(b.getClientInterface(), null);
    }
    if (internal && itf.getOwner().isComposite()) {
      Binding b = ((ClientInterface)itf.getComplementaryInterface()).getBinding();
      if (b != null) {
        removeBinding(b.getServerInterface(), b);
      }
    }
  }

  /**
   * Sets the binding associated to the given client interface. If the given
   * interface is a slave collection interface, then the interface is added to
   * (or, if the given binding is <tt>null</tt>, removed from) its owner
   * component, by calling the addXXXInterface (or removeXXXInterface) method,
   * <i>with notifications disabled</i>, so that only the main operation is
   * notified, even if this operation internally triggers other operations (when
   * an external collection interface is unbound on a composite component, for
   * example, this interface and its complementary interface are removed, which
   * requires to unbind the interfaces that where bound to this complementary
   * interface, if any, which can again remove an external collection interface
   * of a composite component, which requires to unbind that where bound to this
   * complementary interface, and so on).
   *
   * @param i a client interface.
   * @param b the binding associated to this client interface. May be
   *      <tt>null</tt>.
   */

  private void setBinding (ClientInterface i, final Binding b) {
    if (i instanceof SharedClientInterface) {
      i = (ClientInterface)((SharedClientInterface)i).masterInterface;
    }
    ((BasicClientInterface)i).setBinding(b);
    if (i.isCollection()) {
      notificationEnabled = false;
      try {
        Component c = i.getOwner();
        if (b == null) {
          if (i.isInternal()) {
            c.removeServerInterface((ServerInterface)i.getComplementaryInterface());
          } else {
            c.removeClientInterface(i);
          }
        } else {
          if (i.isInternal()) {
            c.addServerInterface((ServerInterface)i.getComplementaryInterface());
          } else {
            c.addClientInterface(i);
          }
        }
      } finally {
        notificationEnabled = true;
      }
    }
  }

  /**
   * Adds the given binding to the list of bindings that refer to the given
   * interface.
   *
   * @param i a server interface.
   * @param b a binding that refer to this interface.
   */

  private void addBinding (final ServerInterface i, final Binding b) {
    if (i instanceof BasicServerInterface) {
      ((BasicServerInterface)i).addBinding(b);
    } else {
      ((SharedServerInterface)i).addBinding(b);
    }
  }

  /**
   * Removes the given binding from the list of bindings that refer to the given
   * interface.
   *
   * @param i a server interface.
   * @param b a binding that refer to this interface.
   */

  private void removeBinding (final ServerInterface i, final Binding b) {
    if (i instanceof BasicServerInterface) {
      ((BasicServerInterface)i).removeBinding(b);
    } else {
      ((SharedServerInterface)i).removeBinding(b);
    }
  }
}
