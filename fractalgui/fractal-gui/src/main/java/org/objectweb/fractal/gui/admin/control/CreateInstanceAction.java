/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.admin.control;

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Configuration;
import org.objectweb.fractal.swing.AbstractAction;
import org.objectweb.fractal.gui.admin.model.AdminModel;
import org.objectweb.fractal.gui.selection.model.Selection;
import org.objectweb.fractal.gui.selection.model.SelectionListener;
import org.objectweb.fractal.rmi.registry.*;

import org.objectweb.fractal.util.Fractal;

import java.awt.event.ActionEvent;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.KeyStroke;
import javax.swing.JOptionPane;

/**
 * An action to exit FractalGUI.
 */

public class CreateInstanceAction extends AbstractAction
  implements BindingController, SelectionListener
{
  /**
   * A mandatory client interface bound to a {@link Configuration configuration}
   * model. This is the configuration that is saved by this action.
   */

  public final static String CONFIGURATION_BINDING = "configuration";

  /**
   * A mandatory client interface bound to a {@link AdminModel AdminModel}
   * model.
   */

  public final static String ADMIN_MODEL_BINDING = "admin-model";

  /**
   * An mandatory client interface bound to a {@link Selection Selection}.
   */

  public final static String SELECTION_BINDING = "selection";

  /**
   * The configuration client interface.
   */

  private Configuration configuration;

  /**
   * The AdminModel client interface.
   */

  private AdminModel adminmodel;

  /**
   * The SelectionModel client interface.
   */

  private Selection selection;


  /**
   * Constructs a new {@link CreateInstanceAction} component.
   */

  public CreateInstanceAction () {
    putValue(NAME, "Create Instance");
    putValue(SHORT_DESCRIPTION, "CreateInstance");
    URL url = getClass().getResource(
      "/org/objectweb/fractal/gui/resources/empty.gif");
    putValue(SMALL_ICON, new ImageIcon(url));
    putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("control shift C"));
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] {
      CONFIGURATION_BINDING,
      ADMIN_MODEL_BINDING,
      SELECTION_BINDING
    };
  }

  public Object lookupFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      return configuration;
    } else if (ADMIN_MODEL_BINDING.equals(clientItfName)) {
      return adminmodel;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      return selection;
    }
    return null;
  }

  public void bindFc (final String clientItfName, final Object serverItf) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = (Configuration)serverItf;
    } else if (ADMIN_MODEL_BINDING.equals(clientItfName)) {
      adminmodel = (AdminModel)serverItf;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = (Selection)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = null;
    } else if (ADMIN_MODEL_BINDING.equals(clientItfName)) {
      adminmodel = null;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the SelectionListener interface
  // -------------------------------------------------------------------------

  public void selectionChanged () {
    Object o = selection.getSelection();
    if (o instanceof Component) {
      setEnabled (true);
    } else {
      setEnabled(false);
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the ActionListener interface
  // -------------------------------------------------------------------------

  public void actionPerformed (final ActionEvent e) {

    Object o = selection.getSelection();

    String classpath = System.getProperty("java.class.path");
    //System.setProperty("java.rmi.server.codebase", configuration.getStorage());

    try {
      if (o instanceof Component) {
        Component c = (Component)o;

        org.objectweb.fractal.api.Component boot = null;

        String rmi_url = "localhost";
        String port = null;
        String name = "localhost";
        Integer in = new Integer (1234);
        String oneMoreTime = "";
        String debut = "P";
        while (true) {
          String input = (String)JOptionPane.showInputDialog (
            null,
            "TODO",
            debut+"lease input "+oneMoreTime+"the URL for wished registry"+"\n machine (host:port/name)",
            JOptionPane.QUESTION_MESSAGE,
            null,
            null,
            rmi_url+":1234/"+name);
          if (input != null) {
            int ina = input.indexOf(':');
            int inb = input.indexOf('/');
            if ((ina < 1) || (inb < ina)) {
              oneMoreTime = "AGAIN ";
              debut = "ERROR : p";
              continue;
            }
            rmi_url = input.substring(0, ina);
            name = input.substring(inb+1);
            port = input.substring(ina+1, inb);
            in = new Integer (port);
            if (in.intValue() < 80) {
              oneMoreTime = "AGAIN ";
              debut = "ERROR port number : p";
              continue;
            }

            if ((rmi_url.equals("localhost")) &&
                (port.equals("1234")) &&
                (name.equals("localhost"))) {
                boot = Fractal.getBootstrapComponent();
            }
            else {
              NamingService ns = Registry.getRegistry (rmi_url, in.intValue());
              boot = ns.lookup (name);
            }
          }
          else { return; }
          break;
        }

        try {
          adminmodel.createInstance (c, boot);
        } catch (Exception ex) {
          JOptionPane.showMessageDialog (
            null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
      }
    } catch (Exception ex) {
      JOptionPane.showMessageDialog (
        null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }
  }
}

