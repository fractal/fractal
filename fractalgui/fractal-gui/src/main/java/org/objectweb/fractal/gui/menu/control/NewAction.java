/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.menu.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Configuration;
import org.objectweb.fractal.gui.model.Factory;
import org.objectweb.fractal.gui.selection.model.Selection;
import org.objectweb.fractal.gui.UserData;
import org.objectweb.fractal.swing.AbstractAction;

import java.awt.event.ActionEvent;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.KeyStroke;
import javax.swing.JOptionPane;
import javax.swing.Action;

/**
 * An action to create new, empty configurations.
 */

public class NewAction extends AbstractAction implements
  BindingController
{

  /**
   * A mandatory client interface bound to a {@link Configuration configuration}
   * model. This is the configuration that is reinitialized by this action.
   */

  public final static String CONFIGURATION_BINDING = "configuration";

  /**
   * A mandatory client interface bound to a {@link Selection selection} model.
   * This is the selection that is reinitialized by this action.
   */

  public final static String SELECTION_BINDING = "selection";

  /**
   * A mandatory client interface bound to a {@link Factory factory}. This
   * factory is used to create an empty initial component in newly created
   * configurations.
   */

  public final static String FACTORY_BINDING = "configuration-factory";

  /**
   * An optional client interface bound to a {@link UserData userdata}. This is
   * the storage into/from which some personal data peculiar to each user are
   * written/read.
   */

  public final static String USER_DATA_BINDING = "user-data";

  /**
   * An optional client interface bound to a save {@link Action action}. This
   * action is used to save the current configuration, before creating a new
   * one.
   */

  public final static String SAVE_ACTION_BINDING = "save-action";

  /**
   * The configuration client interface.
   */

  private Configuration configuration;

  /**
   * The selection client interface.
   */

  private Selection selection;

  /**
   * The factory client interface.
   */

  private Factory factory;

  /**
   * The user data client interface.
   */

  private UserData userData;

  /**
   * The save action client interface.
   */

  private Action save;

  /**
   * Constructs a new {@link NewAction} component.
   */

  public NewAction () {
    putValue(NAME, "New");
    putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("control N"));
    putValue(SHORT_DESCRIPTION, "New");
    URL url = getClass().getResource(
      "/org/objectweb/fractal/gui/resources/filenew.gif");
    putValue(SMALL_ICON, new ImageIcon(url));
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] {
      CONFIGURATION_BINDING,
      SELECTION_BINDING,
      FACTORY_BINDING,
      USER_DATA_BINDING,
      SAVE_ACTION_BINDING
    };
  }

  public Object lookupFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      return configuration;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      return selection;
    } else if (FACTORY_BINDING.equals(clientItfName)) {
      return factory;
    } else if (USER_DATA_BINDING.equals(clientItfName)) {
      return userData;
    } else if (SAVE_ACTION_BINDING.equals(clientItfName)) {
      return save;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = (Configuration)serverItf;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = (Selection)serverItf;
    } else if (FACTORY_BINDING.equals(clientItfName)) {
      factory = (Factory)serverItf;
    } else if (USER_DATA_BINDING.equals(clientItfName)) {
      userData = (UserData)serverItf;
    } else if (SAVE_ACTION_BINDING.equals(clientItfName)) {
      save = (Action)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = null;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = null;
    } else if (FACTORY_BINDING.equals(clientItfName)) {
      factory = null;
    } else if (USER_DATA_BINDING.equals(clientItfName)) {
      userData = null;
    } else if (SAVE_ACTION_BINDING.equals(clientItfName)) {
      save = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the ActionListener interface
  // -------------------------------------------------------------------------

  public void actionPerformed (final ActionEvent e) {
    try {
      if (configuration.getChangeCount() > 0) {
        Object[] options = {"Yes", "No", "Cancel" };
        int n = JOptionPane.showOptionDialog(
          null,
          "Do you want to save the current configuration " +
          "before creating a new one ?",
          "Warning",
          JOptionPane.YES_NO_CANCEL_OPTION,
          JOptionPane.QUESTION_MESSAGE,
          null,
          options,
          options[0]);
        if (n == 0) {
          save.actionPerformed(e);
        }
        else if (n == 2) return;
      }
      if (userData != null) {
        userData.setStringData(UserData.LAST_OPEN_FILE, null);
        userData.setStringData(UserData.LAST_SAVE_FILE, null);
        userData.setStringData(UserData.LAST_OPEN_CONF, null);
        userData.save();
      }
    } catch (Exception ex) {
    }
    Component c = factory.createComponent();
    configuration.setRootComponent(c);
    selection.selectComponent(c);
  }
}
