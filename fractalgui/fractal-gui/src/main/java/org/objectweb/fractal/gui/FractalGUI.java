/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.util.Fractal;

/**
 * FractalGUI launcher.
 */

public class FractalGUI {

  /**
   * Launches FractalGUI.
   *
   * @param args the application's arguments.
   */

  public static void main (String[] args) throws Exception {
    try {
      if (System.getProperty("fractal.provider") == null) {
        System.setProperty(
          "fractal.provider", "org.objectweb.fractal.julia.Julia");
        System.setProperty(
          "julia.loader", "org.objectweb.fractal.julia.loader.DynamicLoader");
        System.setProperty(
                "julia.config", "org/objectweb/fractal/gui/julia.cfg");
      }

      String template = "org.objectweb.fractal.gui.FractalGUI";
      if (args.length > 0) {
        template = args[0];
      }
      
      System.out.println("Creating components from ADL definition...");

      ///*
      Factory f = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
      System.out.println("Factory: " + f);
      Component comp = (Component)f.newComponent(template, null);
      //*/
      
      /*
      Factory f = FactoryFactory.getFactory();
      Component comp = (Component)f.newComponent(template, null);
      */
      
      // starts the application if it has a lifecycle controller
      System.out.println("Starting application...");
      LifeCycleController lc = null;
      try {
        lc = Fractal.getLifeCycleController(comp);
      } catch (NoSuchInterfaceException ignored) {
      }
      if (lc != null) {
        lc.startFc();
      }
      System.out.println("Ready.");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
