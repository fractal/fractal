/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.repository.api;

/**
 * A generic storage manager. A storage can be a single file, and set of files
 * organized in a several directories, a data base, etc. A storage must be
 * opened before one can read or write values in it. It must be closed after
 * use. Each storage contains a set of (name, value) pairs.
 */

public interface Storage {

  /**
   * Opens the storage whose name is given.
   *
   * @param storage name of the storage to be opened
   * @param create <tt>true</tt> to create a new storage, or to override an
   *      existing one, or <tt>false</tt> to open an existing storage.
   * @throws Exception if the storage cannot be opened.
   */

  void open (String storage) throws Exception;

  /**
   * Loads the object whose name is given, from the currently opened storage.
   *
   * @param name the name of the object to be loaded.
   * @return the object whose name is given.
   * @throws Exception if the given object cannot be loaded.
   */

  Object load (String name) throws Exception;

  /**
   * Stores the given object under the given name in the currently opened
   * storage.
   *
   * @param name the name of the object to be stored.
   * @param value the value of the object to be stored.
   * @throws Exception if the object cannot be stored.
   */

  void store (String name, Object value) throws Exception;

  /**
   * Closes the currently opened storage.
   *
   * @throws Exception if the currently opened storage cannot be closed.
   */

  void close () throws Exception;
}
