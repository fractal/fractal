/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.clipboard.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.selection.model.SelectionListener;
import org.objectweb.fractal.gui.selection.model.Selection;
import org.objectweb.fractal.gui.graph.model.GraphModel;
import org.objectweb.fractal.gui.model.Factory;
import org.objectweb.fractal.gui.clipboard.model.Clipboard;
import org.objectweb.fractal.swing.AbstractAction;

/**
 * Super class of actions that acts on a clipboard model. This action
 * listens to a {@link Selection selection} model in order to enable or disable
 * itfself when the selection changes.
 */

public abstract class ClipboardAction extends AbstractAction implements
  SelectionListener,
  BindingController
{

  /**
   * A mandatory client interface bound to a {@link Selection selection} model.
   * This model is used to known the component that must be cut or copied, or
   * the component into which the clipboard's content must be pasted.
   */

  public final static String SELECTION_BINDING = "selection";

  /**
   * A mandatory client interface bound to a {@link GraphModel graph} model.
   * This graph model is passed as argument to the clipboard model methods by
   * this controller component.
   */

  public final static String GRAPH_BINDING = "graph";

  /**
   * A mandatory client interface bound to a {@link Factory factory}. This
   * factory is passed as argument to the clipboard model methods by this
   * controller component.
   */

  public final static String FACTORY_BINDING = "configuration-factory";

  /**
   * A mandatory client interface bound to a {@link Clipboard clipboard} model.
   * This is the model that is modified by this controller component.
   */

  public final static String CLIPBOARD_BINDING = "clipboard";

  /**
   * The selection client interface.
   */

  Selection selection;

  /**
   * The graph client interface.
   */

  GraphModel graph;

  /**
   * The factory client interface.
   */

  Factory factory;

  /**
   * The clipboard client interface.
   */

  Clipboard clipboard;

  /**
   * Constructs a new {@link ClipboardAction} component.
   */

  public ClipboardAction () {
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] {
      SELECTION_BINDING,
      GRAPH_BINDING,
      FACTORY_BINDING,
      CLIPBOARD_BINDING
    };
  }

  public Object lookupFc (final String clientItfName) {
    if (SELECTION_BINDING.equals(clientItfName)) {
      return selection;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      return graph;
    } else if (FACTORY_BINDING.equals(clientItfName)) {
      return factory;
    } else if (CLIPBOARD_BINDING.equals(clientItfName)) {
      return clipboard;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (SELECTION_BINDING.equals(clientItfName)) {
      selection = (Selection)serverItf;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      graph = (GraphModel)serverItf;
    } else if (FACTORY_BINDING.equals(clientItfName)) {
      factory = (Factory)serverItf;
    } else if (CLIPBOARD_BINDING.equals(clientItfName)) {
      clipboard = (Clipboard)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (SELECTION_BINDING.equals(clientItfName)) {
      selection = null;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      graph = null;
    } else if (FACTORY_BINDING.equals(clientItfName)) {
      factory = null;
    } else if (CLIPBOARD_BINDING.equals(clientItfName)) {
      clipboard = null;
    }
  }
}