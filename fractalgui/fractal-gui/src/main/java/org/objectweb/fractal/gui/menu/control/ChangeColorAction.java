/**
 * copyright area
 */

package org.objectweb.fractal.gui.menu.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.selection.model.Selection;
import org.objectweb.fractal.gui.selection.model.SelectionListener;
import org.objectweb.fractal.gui.graph.model.GraphModel;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.swing.AbstractAction;

import java.awt.event.ActionEvent;
import java.awt.Color;
import java.net.URL;

import javax.swing.JColorChooser;
import javax.swing.ImageIcon;

public class ChangeColorAction extends AbstractAction implements
  SelectionListener,
  BindingController
{

  /**
   * A mandatory client interface bound to a {@link Selection selection} model.
   * This model is used to select the created components or interfaces, and
   * to add them to the selected component.
   */

  public final static String SELECTION_BINDING = "selection";

  /**
   * A mandatory client interface bound to a {@link GraphModel graph model}
   * component.
   */

  public final static String GRAPH_BINDING = "graph";

  /**
   * The selection client interface.
   */

  Selection selection;

  /**
   * The graph model client interface.
   */

  GraphModel graph;

  public ChangeColorAction () {
    putValue(NAME, "Color...");
    putValue(SHORT_DESCRIPTION, "Color");
    URL url = getClass().getResource(
      "/org/objectweb/fractal/gui/resources/empty.gif");
    putValue(SMALL_ICON, new ImageIcon(url));
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] { SELECTION_BINDING, GRAPH_BINDING };
  }

  public Object lookupFc (final String clientItfName) {
    if (SELECTION_BINDING.equals(clientItfName)) {
      return selection;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      return graph;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (SELECTION_BINDING.equals(clientItfName)) {
      selection = (Selection)serverItf;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      graph = (GraphModel)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (SELECTION_BINDING.equals(clientItfName)) {
      selection = null;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      graph = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the SelectionListener interface
  // -------------------------------------------------------------------------

  public void selectionChanged () {
    setEnabled(selection.getSelection() instanceof Component);
  }

  // -------------------------------------------------------------------------
  // Implementation of the ActionListener interface
  // -------------------------------------------------------------------------

  public void actionPerformed (final ActionEvent e) {
    Object o = selection.getSelection();
    Component c = (Component)o;
    Color color = graph.getComponentColor(c);
    color = JColorChooser.showDialog(null, "Choose component color", color);
    if (color != null) {
      graph.setComponentColor(c, color);
    }
  }
}
