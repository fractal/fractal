/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.view;

import org.objectweb.fractal.api.control.BindingController;

import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.awt.event.MouseEvent;

/**
 * A {@link GraphViewListener} that notifies other {@link GraphViewListener}s.
 */

public class GraphViewNotifier implements
  GraphViewListener,
  BindingController
{

  /**
   * A collection client interface bound to the {@link GraphViewListener
   * listeners} of this component.
   */

  public final static String GRAPH_VIEW_LISTENERS_BINDING =
    "graph-view-listeners";

  /**
   * The listeners client interface.
   */

  private Map graphViewListeners;

  /**
   * Constructs a new {@link GraphViewNotifier} component.
   */

  public GraphViewNotifier () {
    graphViewListeners = new HashMap();
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return (String[])graphViewListeners.keySet().toArray(
      new String[graphViewListeners.size()]);
  }

  public Object lookupFc (final String clientItfName) {
    if (clientItfName.startsWith(GRAPH_VIEW_LISTENERS_BINDING)) {
      return graphViewListeners.get(clientItfName);
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (clientItfName.startsWith(GRAPH_VIEW_LISTENERS_BINDING)) {
      graphViewListeners.put(clientItfName, serverItf);
    }
  }

  public void unbindFc (final String clientItfName) {
    if (clientItfName.startsWith(GRAPH_VIEW_LISTENERS_BINDING)) {
      graphViewListeners.remove(clientItfName);
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the GraphViewListener interface
  // -------------------------------------------------------------------------

  public void viewChanged () {
    Iterator i = graphViewListeners.values().iterator();
    while (i.hasNext()) {
      ((GraphViewListener)i.next()).viewChanged();
    }
  }

  public void mousePressed (final MouseEvent e, final ComponentPart p) {
    Iterator i = graphViewListeners.values().iterator();
    while (i.hasNext()) {
      ((GraphViewListener)i.next()).mousePressed(e, p);
    }
  }

  public void mouseReleased (final MouseEvent e, final ComponentPart p) {
    Iterator i = graphViewListeners.values().iterator();
    while (i.hasNext()) {
      ((GraphViewListener)i.next()).mouseReleased(e, p);
    }
  }

  public void mouseClicked (final MouseEvent e, final ComponentPart p) {
    Iterator i = graphViewListeners.values().iterator();
    while (i.hasNext()) {
      ((GraphViewListener)i.next()).mouseClicked(e, p);
    }
  }

  public void mouseEntered (final MouseEvent e) {
    Iterator i = graphViewListeners.values().iterator();
    while (i.hasNext()) {
      ((GraphViewListener)i.next()).mouseEntered(e);
    }
  }

  public void mouseExited (final MouseEvent e) {
    Iterator i = graphViewListeners.values().iterator();
    while (i.hasNext()) {
      ((GraphViewListener)i.next()).mouseExited(e);
    }
  }

  public void mouseDragged (final MouseEvent e) {
    Iterator i = graphViewListeners.values().iterator();
    while (i.hasNext()) {
      ((GraphViewListener)i.next()).mouseDragged(e);
    }
  }

  public void mouseMoved (final MouseEvent e, final ComponentPart p) {
    Iterator i = graphViewListeners.values().iterator();
    while (i.hasNext()) {
      ((GraphViewListener)i.next()).mouseMoved(e, p);
    }
  }
}
