/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.model;

/**
 * Basic implementation of the {@link Binding} interface.
 */

public class BasicBinding implements Binding {

  /**
   * The client interface that is bound by this binding.
   */

  private final ClientInterface citf;

  /**
   * The server interface that is bound by this binding.
   */

  private final ServerInterface sitf;

  /**
   * The status of this binding.
   */

  private long status;

  /**
   * Constructs a new binding.
   *
   * @param citf the client interface that is bound by this binding.
   * @param sitf the server interface that is bound by this binding.
   */

  BasicBinding (final ClientInterface citf, final ServerInterface sitf) {
    this.citf = citf;
    this.sitf = sitf;
  }


  /**
   * Returns the status of this binding.
   *
   * @return the status of this binding.
   * @see #setStatus
   */

  public long getStatus () { return status; }

  /**
   * Sets the status of this binding.
   *
   * @param status the new status of this bindinge.
   * @see #getStatus
   */

  public void setStatus (long status) {
    this.status = status;
  }

  public ClientInterface getClientInterface () {
    return citf;
  }

  public ServerInterface getServerInterface () {
    return sitf;
  }
}
