/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.model;

import org.objectweb.fractal.gui.model.Configuration;
import java.util.List;
import java.util.ArrayList;

/**
 * Abstract implementation of the {@link Component} interface.
 */

public abstract class AbstractComponent implements Component {

  /**
   * The configuration to which this component belongs.
   */

  private Configuration owner;

  /**
   * The parent component of this component. May be <tt>null</tt> if this
   * component is a root component.
   */

  protected Component parent;

  // -------------------------------------------------------------------------
  // Implementation of the Component interface
  // -------------------------------------------------------------------------

  public Component getParent () {
    return parent;
  }

  /**
   * Returns the configuration to which this component belongs.
   *
   * @return the configuration to which this component belongs.
   */

  public Configuration getConfiguration () {
    return owner;
  }

  /**
   * Sets the parent of this component.
   *
   * @param parent the new parent of this component.
   * @see #getParent
   */

  void setParent (final Component parent) {
    this.parent = parent;
  }

  public Component getRootComponent () {
    Component c = this;
    while (c != null && c.getParent() != null) {
      c = c.getParent();
    }
    return c;
  }

  public Object[] getPath () {
    Component c = this;
    List pathList = new ArrayList();
    while (c != null) {
      pathList.add(0, c);
      c = c.getParent();
    }
    return pathList.toArray();
  }

  public boolean contains (final Component child) {
    Component c = child;
    while (c != null && c != this) {
      c = c.getParent();
    }
    return c == this;
  }

  public boolean containsSlaveOfExternalComponent (final Component c) {
    Component master = getMasterComponent();
    if (master != null && !c.contains(master)) {
      return true;
    }
    List subComponents = getSubComponents();
    for (int i = 0; i < subComponents.size(); ++i) {
      Component subComponent = (Component)subComponents.get(i);
      if (subComponent.containsSlaveOfExternalComponent(c)) {
        return true;
      }
    }
    return false;
  }

  public boolean containsMasterOfExternalComponent (final Component c) {
    List slaves = getSlaveComponents();
    for (int i = 0; i < slaves.size(); ++i) {
      Component slave = (Component)slaves.get(i);
      if (!c.contains(slave)) {
        return true;
      }
    }
    List subComponents = getSubComponents();
    for (int i = 0; i < subComponents.size(); ++i) {
      Component subComponent = (Component)subComponents.get(i);
      if (subComponent.containsMasterOfExternalComponent(c)) {
        return true;
      }
    }
    return false;
  }
}
