/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.repository.lib;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.gui.graph.model.GraphModel;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Factory;
import org.objectweb.fractal.gui.model.Interface;
import org.objectweb.fractal.gui.repository.api.Repository;
import org.objectweb.fractal.gui.repository.api.Storage;

/**
 * Basic implementation of the {@link Repository} interface. This implementation
 * supposes that the component definitions are stored in the repository as
 * Fractal ADL definitions, in XML form.
 */

public class BasicRepository implements Repository, BindingController {

  /**
   * A mandatory client interface bound to a {@link Storage storage}. This is
   * the storage from which this repository loads the component definitions.
   */

  public final static String STORAGE_BINDING = "storage";

  /**
   * TODO javadoc.
   */
  
  public final static String DEFINITION_FACTORY_BINDING = "definition-factory";
  
  /**
   * A mandatory client interface bound to a {@link Factory factory}. This
   * factory is used to create components from the component definitions stored
   * in the storage.
   */

  public final static String CONFIGURATION_FACTORY_BINDING = "configuration-factory";

  /**
   * The storage client interface.
   */

  protected Storage storage;

  /**
   * TODO javadoc.
   */
  
  protected org.objectweb.fractal.adl.Factory definitionFactory;

  /**
   * The factory client interface.
   */

  protected Factory configurationFactory;

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] { 
      STORAGE_BINDING,
      DEFINITION_FACTORY_BINDING,
      CONFIGURATION_FACTORY_BINDING 
    };
  }

  public Object lookupFc (final String clientItfName) {
    if (STORAGE_BINDING.equals(clientItfName)) {
      return storage;
    } else if (DEFINITION_FACTORY_BINDING.equals(clientItfName)) {
      return definitionFactory;
    } else if (CONFIGURATION_FACTORY_BINDING.equals(clientItfName)) {
      return configurationFactory;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (STORAGE_BINDING.equals(clientItfName)) {
      storage = (Storage)serverItf;
    } else if (DEFINITION_FACTORY_BINDING.equals(clientItfName)) {
      definitionFactory = (org.objectweb.fractal.adl.Factory)serverItf;
    } else if (CONFIGURATION_FACTORY_BINDING.equals(clientItfName)) {
      configurationFactory = (Factory)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (STORAGE_BINDING.equals(clientItfName)) {
      storage = null;
    } else if (DEFINITION_FACTORY_BINDING.equals(clientItfName)) {
      definitionFactory = null;
    } else if (CONFIGURATION_FACTORY_BINDING.equals(clientItfName)) {
      configurationFactory = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the Repository interface
  // -------------------------------------------------------------------------

  public Component loadComponent (
    final String name, 
    final GraphModel graph) throws Exception 
  {
    Map context = new HashMap();
    context.put("factory", configurationFactory);
    if (graph != null) {
      context.put("graph", graph);
    }
    Component c = (Component)definitionFactory.newComponent(name, context);
    removeGeneratedNames(c);
    return c;
  }

  public String storeComponent (
    final Component component, 
    final GraphModel graph,
    final Object hints) throws Exception 
  {
    FractalAdlWriter writer = new FractalAdlWriter();
    writer.storage = storage;
    writer.forceInternal = "inline".equals(hints);
    return writer.saveTemplate(component, graph);
  }
  
  protected void removeGeneratedNames (Component c) {
    if (c.getName().startsWith("GENERATED-")) {
      c.setName("");
    }
    List l = c.getClientInterfaces();
    for (int i = 0; i < l.size(); ++i) {
      Interface itf = (Interface)l.get(i);
      if (itf.getName().startsWith("GENERATED-")) {
        itf.setName("");
      }
    }
    l = c.getServerInterfaces();
    for (int i = 0; i < l.size(); ++i) {
      Interface itf = (Interface)l.get(i);
      if (itf.getName().startsWith("GENERATED-")) {
        itf.setName("");
      }
    }
    l = c.getSubComponents();
    for (int i = 0; i < l.size(); ++i) {
      removeGeneratedNames((Component)l.get(i));
    }
  }
}
