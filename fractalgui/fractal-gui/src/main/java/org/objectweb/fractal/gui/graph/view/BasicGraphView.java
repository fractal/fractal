/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.view;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.graph.model.Display;
import org.objectweb.fractal.gui.graph.model.DisplayListener;
import org.objectweb.fractal.gui.graph.model.GraphModel;
import org.objectweb.fractal.gui.graph.model.GraphModelListener;
import org.objectweb.fractal.gui.graph.model.Rect;
import org.objectweb.fractal.gui.graph.model.Tools;
import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Configuration;
import org.objectweb.fractal.gui.model.ConfigurationListener;
import org.objectweb.fractal.gui.model.Interface;
import org.objectweb.fractal.gui.model.ServerInterface;
import org.objectweb.fractal.gui.selection.model.Selection;
import org.objectweb.fractal.gui.selection.model.SelectionListener;
import org.objectweb.fractal.gui.admin.model.AdminModel;
import org.objectweb.fractal.gui.admin.model.AdminModelListener;
import org.objectweb.fractal.swing.JPanelImpl;

import cwi.SVGGraphics.SVGGraphics;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.print.Printable;
import java.awt.print.PageFormat;
import java.awt.print.Book;
import java.awt.print.PrinterJob;
import java.awt.print.PrinterException;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

/**
 * A configuration view that displays configurations as graph of boxes and
 * lines, representing components and bindings. This component does not draw the
 * components and bindings itself: it uses auxiliary component renderer and
 * binding renderer components for that. In fact this component just calls these
 * components for each direct and indirect sub component of the configuration's
 * root component. The mouse events are not handled by this component: they are
 * just dispatched to the listeners of this component.
 */

public class BasicGraphView extends JPanelImpl implements
  DisplayListener,
  AdminModelListener,
  GraphModelListener,
  ConfigurationListener,
  SelectionListener,
  Printer,
  Printable,
  BindingController
{

  /**
   * A mandatory client interface bound to a {@link Configuration configuration}
   * model.
   */

  public final static String CONFIGURATION_BINDING = "configuration";

  /**
   * A mandatory client interface bound to a {@link GraphModel graph} model.
   */

  public final static String GRAPH_BINDING = "graph";

  /**
   * An optional client interface bound to a {@link Selection selection} model.
   */

  public final static String SELECTION_BINDING = "selection";

  /**
   * An optional client interface bound to a {@link Display display} model.
   */

  public final static String DISPLAY_BINDING = "display";

  /**
   * An optional client interface bound to an {@link AdminModel admin} model.
   */
  public final static String ADMIN_BINDING = "admin";
  
  /**
   * An optional client interface boudn to a {@link Tools tools} model.
   */

  public final static String TOOLS_BINDING = "tools";

  /**
   * A mandatory client interface bound to a {@link ComponentRenderer}.
   */

  public final static String COMPONENT_RENDERER_BINDING = "component-renderer";

  /**
   * A mandatory client interface bound to a {@link BindingRenderer}.
   */

  public final static String BINDING_RENDERER_BINDING = "binding-renderer";

  /**
   * A collection client interface bound to the {@link GraphViewListener
   * listeners} of this view component.
   */

  public final static String GRAPH_VIEW_LISTENERS_BINDING =
    "graph-view-listeners";

  /**
   * The configuration client interface.
   */

  private Configuration configuration;

  /**
   * The graph model client interface.
   */

  private GraphModel graphModel;

  /**
   * The selection client interface.
   */

  private Selection selection;

  /**
   * The display client interface.
   */

  private Display display;

  /**
   * The admin model client interface.
   */
  
  private AdminModel admin;
  
  /**
   * The tools client interface.
   */

  private Tools tools;

  /**
   * The component renderer client interface.
   */

  private ComponentRenderer cRenderer;

  /**
   * The binding renderer client interface.
   */

  private BindingRenderer bRenderer;

  /**
   * The listeners component interface.
   */

  private Map graphViewListeners;

  private int w, h, xo, yo;

  private int wp, hp, xp, yp;

  private boolean printInProgress = false;

  private Component ctp;          // Component To Print

  private Image offScreen;

  private boolean dirty;

  private Point bindPoint;

  private int typePrint;

  static String UN = new String (System.getProperty("user.name"));

  static final String [] mois = {
    "Jan.", "Feb.", "Mar.", "Apr.", "May", "Jun.",
    "Jul.", "Aug.", "Sep.", "Oct.", "Nov.", "Dec."};

  /**
   * Constructs a new {@link BasicGraphView} component.
   */

  public BasicGraphView () {
    graphViewListeners = new HashMap();
    enableEvents(AWTEvent.MOUSE_EVENT_MASK | AWTEvent.MOUSE_MOTION_EVENT_MASK);
    addComponentListener(
      new ComponentAdapter() {
        public void componentResized (final ComponentEvent e) {
          w = e.getComponent().getSize().width;
          h = e.getComponent().getSize().height;
          xo = 0;
          yo = 0;
          if (w > 0 && h > 0) {
            if (display != null) {
//			  display.setScreenSize(new Rectangle(0, 0, w, h));
              display.setScreenSize(new Rectangle(xo, yo, w, h));
            }
            offScreen = createOffscreenImage(w, h);
            redraw();
          }
        }
      });
  }

  // -------------------------------------------------------------------------
  // Implementation of the UserBindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    int size = graphViewListeners.size();
    String[] names = new String[size + 8];
    graphViewListeners.keySet().toArray(names);
    names[size] = COMPONENT_RENDERER_BINDING;
    names[size + 1] = BINDING_RENDERER_BINDING;
    names[size + 2] = DISPLAY_BINDING;
    names[size + 3] = TOOLS_BINDING;
    names[size + 4] = SELECTION_BINDING;
    names[size + 5] = CONFIGURATION_BINDING;
    names[size + 6] = GRAPH_BINDING;
    names[size + 7] = ADMIN_BINDING;
    return names;
  }

  public Object lookupFc (final String clientItfName) {
    if (COMPONENT_RENDERER_BINDING.equals(clientItfName)) {
      return cRenderer;
    } else if (BINDING_RENDERER_BINDING.equals(clientItfName)) {
      return bRenderer;
    } else if (DISPLAY_BINDING.equals(clientItfName)) {
      return display;
    } else if (ADMIN_BINDING.equals(clientItfName)) {
      return admin;
    } else if (TOOLS_BINDING.equals(clientItfName)) {
      return tools;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      return selection;
    } else if (CONFIGURATION_BINDING.equals(clientItfName)) {
      return configuration;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      return graphModel;
    } else if (clientItfName.startsWith(GRAPH_VIEW_LISTENERS_BINDING)) {
      return graphViewListeners.get(clientItfName);
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (COMPONENT_RENDERER_BINDING.equals(clientItfName)) {
      cRenderer = (ComponentRenderer)serverItf;
    } else if (BINDING_RENDERER_BINDING.equals(clientItfName)) {
      bRenderer = (BindingRenderer)serverItf;
    } else if (DISPLAY_BINDING.equals(clientItfName)) {
      display = (Display)serverItf;
    } else if (ADMIN_BINDING.equals(clientItfName)) {
      admin = (AdminModel)serverItf;
    } else if (TOOLS_BINDING.equals(clientItfName)) {
      tools = (Tools)serverItf;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = (Selection)serverItf;
    } else if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = (Configuration)serverItf;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      graphModel = (GraphModel)serverItf;
    } else if (clientItfName.startsWith(GRAPH_VIEW_LISTENERS_BINDING)) {
      graphViewListeners.put(clientItfName, serverItf);
    }
  }

  public void unbindFc (final String clientItfName) {
    if (COMPONENT_RENDERER_BINDING.equals(clientItfName)) {
      cRenderer = null;
    } else if (BINDING_RENDERER_BINDING.equals(clientItfName)) {
      bRenderer = null;
    } else if (DISPLAY_BINDING.equals(clientItfName)) {
      display = null;
    } else if (ADMIN_BINDING.equals(clientItfName)) {
      admin = null;
    } else if (TOOLS_BINDING.equals(clientItfName)) {
      tools = null;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = null;
    } else if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = null;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      graphModel = null;
    } else if (clientItfName.startsWith(GRAPH_VIEW_LISTENERS_BINDING)) {
      graphViewListeners.remove(clientItfName);
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the DisplayListener interface
  // -------------------------------------------------------------------------

  public void displayedAreaChanged (final Rect oldValue) {
    redraw();
  }

  public void antialiasingChanged () {
    redraw();
  }

  public void maxDepthChanged () {
    redraw();
  }

  public void itfNameDisplayModeChanged () {
    redraw();
  }

  // -------------------------------------------------------------------------
  // Implementation of the AdminModelListener interface
  // -------------------------------------------------------------------------

  public void componentCreated  (Component model) {
    redraw();
  }

  public void componentDeleted  (Component model) {
    redraw();
  }

  public void componentStarted  (Component model) {
    redraw();
  }

  public void componentStopped  (Component model) {
    redraw();
  }

  // -------------------------------------------------------------------------
  // Implementation of the SelectionListener interface
  // -------------------------------------------------------------------------

  public void selectionChanged () {
    redraw();
  }

  // -------------------------------------------------------------------------
  // Implementation of the Printer interface
  // -------------------------------------------------------------------------

  public void setType (int ntype) {
    typePrint = ntype;
  }

  public boolean print (final boolean ask, Component c) {

    if (typePrint == Printer.TYPE_EXPORT) {
      SVGGraphics svg = SVGGraphics.create (this);
      if (svg == null) return false;
      drawShapes(svg, c);
      svg.close();
      return true;
    }

    PrinterJob job = PrinterJob.getPrinterJob();
    Book bk = new Book();
    if (ask) bk.append (new PaintCover(), job.defaultPage());
    ctp = c;

    // Create a page format
    PageFormat page = job.defaultPage();
    page.setOrientation(PageFormat.LANDSCAPE);
    xp = (int)page.getImageableX();
    yp = (int)page.getImageableY()+30;
    wp = w - 2*xp;
    hp = h - 2*yp;
//    wp = (int)page.getWidth() - 2*xp;   r?sultat hideux !
//    hp = (int)page.getHeight() - 2*yp;

    bk.append (this, page);
    if (c.getMasterComponent() != null) {
      c = c.getMasterComponent();
    }
    if (selection != null) {
      selection.selectComponent(c);
    }
    configuration.setRootComponent(c);

    // Pass the book to the PrinterJob
    job.setPageable(bk);

    // Put up the dialog box
    if (ask) {
      if (job.printDialog()) imprime (job);
      else return false;
    } else {
      imprime (job);
    }
    return true;
  }

  private void imprime (final PrinterJob job) {
    try {

      int oldxo = xo;
      int oldyo = yo;
      int oldw = w;
      int oldh = h;
      xo = xp; yo = yp; w = wp; h = hp;

      printInProgress = true;
      job.print();

      xo = oldxo;
      yo = oldyo;
      w = oldw;
      h = oldh;

      printInProgress = false;
    } catch (Exception exc) {
      exc.printStackTrace(); /* Handle Exception */
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the Printable interface
  // -------------------------------------------------------------------------

  public int print (final Graphics g, final PageFormat pf, final int pageIndex)
    throws PrinterException
  {
    drawShapes((Graphics2D) g, ctp);
    return Printable.PAGE_EXISTS;
  }

  // -------------------------------------------------------------------------
  // Implementation of the ConfigurationListener interface
  // -------------------------------------------------------------------------

  public void changeCountChanged (Component component, long changeCount) {
    // does nothing
  }

  public void rootComponentChanged (final Component oldValue) {
    Iterator i = graphViewListeners.values().iterator();
    while (i.hasNext()) {
      GraphViewListener l = (GraphViewListener)i.next();
      l.viewChanged();
    }
    redraw();
  }

  public void nameChanged (final Component component, final String oldValue) {
    redraw();
  }

  public void typeChanged (final Component component, final String oldValue) {
    redraw();
  }

  public void implementationChanged (
    final Component component,
    final String oldValue)
  {
    redraw();
  }

  public void interfaceNameChanged (
    final Interface i,
    final String oldValue)
  {
    redraw();
  }

  public void interfaceSignatureChanged (
    final Interface i,
    final String oldValue)
  {
    redraw();
  }

  public void interfaceContingencyChanged (
    final Interface i,
    final boolean oldValue)
  {
    redraw();
  }

  public void interfaceCardinalityChanged (
    final Interface i,
    final boolean oldValue)
  {
    redraw();
  }

  public void clientInterfaceAdded (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    redraw();
  }

  public void clientInterfaceRemoved (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    redraw();
  }

  public void serverInterfaceAdded (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    redraw();
  }

  public void serverInterfaceRemoved (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    redraw();
  }

  public void interfaceBound (
    final ClientInterface citf,
    final ServerInterface sitf) {
   redraw();
  }

  public void interfaceRebound (
    final ClientInterface citf,
    final ServerInterface oldSitf)
  {
    redraw();
  }

  public void interfaceUnbound (
    final ClientInterface citf,
    final ServerInterface sitf)
  {
    redraw();
  }

  public void attributeControllerChanged (
    final Component component,
    final String oldValue)
  {
    redraw();
  }

  public void attributeChanged (
    final Component component,
    final String attributeName,
    final String oldValue)
  {
    redraw();
  }

  public void templateControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    redraw();
  }

  public void componentControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    redraw();
  }

  public void subComponentAdded (
    final Component parent,
    final Component child,
    final int index)
  {
    redraw();
  }

  public void subComponentRemoved (
    final Component parent,
    final Component child,
    final int index)
  {
    redraw();
  }

  // -------------------------------------------------------------------------
  // Implementation of the GraphModelListener interface
  // -------------------------------------------------------------------------

  public void componentColorChanged (
    final Component component,
    final Color oldColor)
  {
    if (configuration.getRootComponent().contains(component)) {
      redraw();
    }
  }

  public void componentPositionChanged (
    final Component component,
    final Rect oldValue)
  {
    if (configuration.getRootComponent().contains(component)) {
      redraw();
    }
  }

  // -------------------------------------------------------------------------
  // Overriden JPanel methods
  // -------------------------------------------------------------------------

  protected void processMouseEvent (final MouseEvent e) {
    if (configuration == null) {
      return;
    }
    int x = e.getX();
    int y = e.getY();
    ComponentPart p = getComponentPart(x, y);

    GraphViewListener l;
    Iterator i = graphViewListeners.values().iterator();

    boolean needRedraw = tools != null && tools.getTool() == Tools.BIND;

    switch (e.getID()) {
      case MouseEvent.MOUSE_PRESSED:
        while (i.hasNext()) {
          l = (GraphViewListener)i.next();
          l.mousePressed(e, p);
        }
        break;

      case MouseEvent.MOUSE_RELEASED:
        while (i.hasNext()) {
          l = (GraphViewListener)i.next();
          l.mouseReleased(e, p);
        }
        break;

      case MouseEvent.MOUSE_CLICKED:
        while (i.hasNext()) {
          l = (GraphViewListener)i.next();
          l.mouseClicked(e, p);
        }
        break;

      case MouseEvent.MOUSE_ENTERED:
        while (i.hasNext()) {
          l = (GraphViewListener)i.next();
          l.mouseEntered(e);
        }
        break;

      case MouseEvent.MOUSE_EXITED:
        while (i.hasNext()) {
          l = (GraphViewListener)i.next();
          l.mouseExited(e);
        }
        break;

      default:
        break;
    }

    if (needRedraw) {
      redraw();
    }
    super.processMouseEvent(e);
  }

  protected void processMouseMotionEvent (final MouseEvent e) {
    if (configuration == null) {
      return;
    }

    boolean needRedraw = tools != null && tools.getTool() == Tools.BIND;

    Iterator i = graphViewListeners.values().iterator();
    GraphViewListener l;
    bindPoint = e.getPoint();

    switch (e.getID()) {
      case MouseEvent.MOUSE_DRAGGED:
        while (i.hasNext()) {
          l = (GraphViewListener)i.next();
          l.mouseDragged(e);
        }
        break;

      case MouseEvent.MOUSE_MOVED:
        int x = e.getX();
        int y = e.getY();
        ComponentPart p = getComponentPart(x, y);
        while (i.hasNext()) {
          l = (GraphViewListener)i.next();
          l.mouseMoved(e, p);
        }
        break;

      default:
        break;
    }

    if (needRedraw) {
      redraw();
    }

    super.processMouseMotionEvent(e);
  }

  // ----- paint

  public void paint (final Graphics g) {
    if (offScreen != null) {
      if (printInProgress) {
        g.drawImage(offScreen, xo, yo, w, h, this);
      } else {
        g.drawImage(offScreen, xo, yo, w, h, this);
      }
    }
  }

  public void repaint () {
    paint(getGraphics());
  }

  public void setVisible (final boolean visible) {
    super.setVisible(visible);
    if (visible && dirty) {
      redraw();
    }
  }

  // -------------------------------------------------------------------------
  // Other methods
  // -------------------------------------------------------------------------

  Image createOffscreenImage (final int w, final int h) {
    return createImage(w, h);
  }

  private ComponentPart getComponentPart (final int x, final int y) {
    Component root = configuration.getRootComponent();
    Rectangle r = new Rectangle(xo, yo, w, h);
    if (display != null) {
      r = getRectangle(r, display.getDisplayedArea());
    } else {
      r = getRectangle(r, new Rect(0.1, 0.1, 0.9, 0.9));
    }

    int depth = display == null ? 0 : display.getMaxDepth();
    ComponentPart part = getComponentPart(root, r, x, y, depth);
    if (part == null) {
      part = new ComponentPart(null, null, 0, new Rectangle(xo, yo, w, h));
    }
    return part;
  }

  private ComponentPart getComponentPart (
    final Component c,
    final Rectangle r,
    final int x,
    final int y,
    final int depth)
  {
    if (r.width <= 0 || r.height <= 0) {
      return null;
    }
    if (!r.intersects(new Rectangle(xo, yo, w, h))) {
      return null;
    }

    // tests the component itself
    ComponentPart part = cRenderer.getComponentPart(c, r, depth > 0, x, y);

    if (part != null && part.getPart() == ComponentPart.CONTENT) {
      if (depth > 0 && c.isComposite()) {
        // tests the sub-components, if any
        List subComponents = c.getSubComponents();
        for (int i = 0; i < subComponents.size(); ++i) {
          Component subC = (Component)subComponents.get(i);
          Rectangle subR = cRenderer.getSubComponentArea(c, r);
          subR = getRectangle(subR, graphModel.getComponentPosition(subC));
          ComponentPart subPart = getComponentPart(subC, subR, x, y, depth - 1);
          if (subPart != null) {
            return subPart;
          }
        }
      }
    }
    return part;
  }

  protected synchronized void redraw () {
    if (offScreen == null || configuration == null) {
      return;
    }
    if (!isVisible()) {
      dirty = true;
      return;
    }
    dirty = false;

    final Graphics2D g = (Graphics2D)offScreen.getGraphics();
    g.setRenderingHint (
      RenderingHints.KEY_ANTIALIASING,
      display != null && display.isAntialiasing()
      ? RenderingHints.VALUE_ANTIALIAS_ON
      : RenderingHints.VALUE_ANTIALIAS_OFF);

    g.setColor(Color.white);
    g.fillRect(0, 0, w, h);

    final Component root = configuration.getRootComponent();
    Rectangle r = new Rectangle(xo, yo, w, h);
    if (display != null) {
      r = getRectangle(r, display.getDisplayedArea());
    } else {
      r = getRectangle(r, new Rect(0.1, 0.1, 0.9, 0.9));
    }
    drawComponent(g, root, r, display == null ? 0 : display.getMaxDepth());
    repaint();
  }


  synchronized void drawShapes (final Graphics2D g, final Component c) {
    if (offScreen == null || configuration == null) {
      return;
    }
    if (!isVisible()) {
      dirty = true;
      return;
    }
    dirty = false;
    g.setRenderingHint(
      RenderingHints.KEY_ANTIALIASING,
      display != null && display.isAntialiasing()
      ? RenderingHints.VALUE_ANTIALIAS_ON
      : RenderingHints.VALUE_ANTIALIAS_OFF);

    g.setColor(Color.white);
    g.fillRect(0, 0, w, h);

//		Component root = configuration.getRootComponent();
    Rectangle r = new Rectangle(xo, yo, w, h);
    if (display != null) {
      r = getRectangle(r, display.getDisplayedArea());
    } else {
      r = getRectangle(r, new Rect(0.1, 0.1, 0.9, 0.9));
    }
    drawComponent(g, c, r, display == null ? 0 : display.getMaxDepth());
    paint (g);
  }


  private void drawComponent (
    final Graphics g,
    final Component c,
    final Rectangle r,
    final int depth)
  {
    if (r.width <= 0 || r.height <= 0) {
      return;
    }
    if (!r.intersects(new Rectangle(xo, yo, w, h))) {
      return;
    }
    boolean expanded = depth > 0 && c.isComposite();

    int instance = ComponentRenderer.NO_INSTANCE;
    if (admin != null) {
      if (admin.getInstance(c) != null) {
        if (admin.isStarted(c)) {
          instance = ComponentRenderer.STARTED;
        } else {
          instance = ComponentRenderer.STOPPED;
        }
      }
    }
    
    // draws the component itself
    Color color = graphModel.getComponentColor(c);
    if (display == null)
      cRenderer.drawComponent(g, c, selection, r, color, expanded, 0, instance);
    else cRenderer.drawComponent
      (g, c, selection, r, color, expanded, display.getItfNameDisplayMode(), instance);

    if (expanded) {
      // draws its sub-components, if any
      Shape clip = g.getClip();
      Rectangle s = cRenderer.getSubComponentArea(c, r);
      g.clipRect(s.x, s.y, s.width, s.height);

      List subComponents = c.getSubComponents();
      for (int i = subComponents.size() - 1; i >= 0; --i) {
        Component subC = (Component)subComponents.get(i);
        Rectangle subR = getRectangle(s, graphModel.getComponentPosition(subC));
        drawComponent(g, subC, subR, depth - 1);
      }

      for (int i = subComponents.size() - 1; i >= 0; --i) {
        Component subC = (Component)subComponents.get(i);
        Rectangle subR = getRectangle(s, graphModel.getComponentPosition(subC));
        drawExternalBindings(g, c, r, subC, subR, i);
      }
      // draws the bindings of the internal client interfaces
      drawInternalBindings(g, c, r);
      g.setClip(clip);
    }
  }

  private void drawExternalBindings (
    final Graphics g,
    final Component p,
    final Rectangle pr,
    final Component c,
    final Rectangle r,
    final int rg)
  {
    List l = c.getClientInterfaces();
    for (int i = 0; i < l.size(); ++i) {
      ClientInterface citf = (ClientInterface)l.get(i);
      if (citf.getBinding() != null) {
        ServerInterface sitf = citf.getBinding().getServerInterface();
        Component server = sitf.getOwner();
        Point cp = cRenderer.getInterfacePosition(c, r, citf);

        Rectangle t;
        if (server == p) {
          t = pr;
        } else if (server.getParent() != c.getParent()) {
          continue;
        } else {
          Rectangle s = cRenderer.getSubComponentArea(p, pr);
          t = getRectangle(s, graphModel.getComponentPosition(server));
        }
        Point sp = cRenderer.getInterfacePosition(server, t, sitf);
        bRenderer.drawBinding(g, r, t, cp, sp, citf.getBinding(), rg+i);
      }

      if (tools != null &&
          tools.getTool() == Tools.BIND &&
          tools.getBindInterface() == citf)
      {
        Point cp = cRenderer.getInterfacePosition(c, r, citf);
        bRenderer.drawBinding(g, r, null, cp, bindPoint, citf.getBinding(), rg+i);
      }
    }
  }


  private void drawInternalBindings (
    final Graphics g,
    final Component c,
    final Rectangle r)
  {
    if (!c.isComposite()) {
      return;
    }

    List l = c.getServerInterfaces();
    for (int i = 0; i < l.size(); ++i) {
      Interface itf = (Interface)l.get(i);

      ClientInterface citf = (ClientInterface)itf.getComplementaryInterface();
      if (citf.getBinding() != null) {
        Point sp;
        Rectangle t;
        Point cp = cRenderer.getInterfacePosition(c, r, citf);
        ServerInterface sitf = citf.getBinding().getServerInterface();
        Component server = sitf.getOwner();

        if (server != c) {
          Rectangle s = cRenderer.getSubComponentArea(c, r);
          t = getRectangle(s, graphModel.getComponentPosition(server));
          sp = cRenderer.getInterfacePosition(server, t, sitf);
        } else {
          t = r;
          sp = cRenderer.getInterfacePosition(server, r, sitf);
        }
        bRenderer.drawBinding(g, r, t, cp, sp, citf.getBinding(), i);
      }

      if (tools != null &&
          tools.getTool() == Tools.BIND &&
          tools.getBindInterface() == citf)
      {
        Point cp = cRenderer.getInterfacePosition(c, r, citf);
        bRenderer.drawBinding(g, r, null, cp, bindPoint, citf.getBinding(), i);
      }
    }
  }

  private Rectangle getRectangle (final Rectangle r, final Rect rect) {
    int h0 = r.x + (int)(r.width * rect.x0);
    int v0 = r.y + (int)(r.height * rect.y0);
    int h1 = r.x + (int)(r.width * rect.x1);
    int v1 = r.y + (int)(r.height * rect.y1);
    return new Rectangle(h0, v0, h1 - h0, v1 - v0);
  }

  class PaintCover implements Printable
  {
    private Component c;

    public int print(Graphics g, PageFormat pf, int pageIndex)
      throws PrinterException
    {
      g.setColor(Color.black);
      g.setFont(new Font("MonoSpaced", Font.PLAIN, 14));
      GregorianCalendar grcl = new GregorianCalendar ();

      g.drawString("Printing  by FractalGUI", 100, 100);
      g.drawString("for       : "+UN, 120, 120);
      g.drawString("Component : '"+configuration.getRootComponent().getName()
          +"'", 120, 140);
      g.drawString("Date      : "+mois[grcl.get(Calendar.MONTH)]+", "
          +grcl.get(Calendar.DAY_OF_MONTH)+"th  "+grcl.get(Calendar.YEAR)
          , 120, 160);
      g.drawString("Time      : "+grcl.get(Calendar.HOUR_OF_DAY)
          +"H"+grcl.get(Calendar.MINUTE)
          , 120, 180);
      return Printable.PAGE_EXISTS;
    }
  }
}
