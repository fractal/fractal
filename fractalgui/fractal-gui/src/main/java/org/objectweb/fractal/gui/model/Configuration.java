/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.model;

/**
 * A model for a component configuration.
 */

public interface Configuration {

  /**
   * Returns the status of this configuration.
   *
   * @see #setChangeCount
   */

  long getChangeCount ();

  /**
   * Sets the status of this configuration.
   *
   * @see #getChangeCount
   */

  void setChangeCount (long changeCount);

  /**
   * Returns the root component of this configuration.
   *
   * @return the root component of this configuration.
   * @see #setRootComponent
   */

  Component getRootComponent ();

  /**
   * Sets the root component of this configuration. This method notifies the
   * configuration listeners, via the {@link
   * ConfigurationListener#rootComponentChanged rootComponentChanged} method.
   *
   * @param root the new root component of this configuration.
   * @see #getRootComponent
   */

  void setRootComponent (Component root);


  /**
   * Returns the storage directory of this configuration.
   *
   * @return the storage directory of this configuration.
   * @see #setStorage
   */

  String getStorage ();

  /**
   * Sets the storage directory of this configuration.
   *
   * @param execd the new storage directory of this configuration.
   * @see #getStorage
   */

  void setStorage (String execd);
}
