/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.menu.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Configuration;
import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.ServerInterface;
import org.objectweb.fractal.gui.selection.model.Selection;
import org.objectweb.fractal.swing.AbstractAction;
import org.objectweb.fractal.gui.menu.control.SimpleFileFilter;

import java.awt.event.ActionEvent;
import java.net.URL;
import java.util.List;
import java.util.HashMap;
import java.util.Iterator;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.OutputStreamWriter;
import java.lang.reflect.Method;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

/**
 * An action to build a source file for a component.
 */

public class MakeSourceAction extends AbstractAction implements
  BindingController
{
  String	LS = System.getProperty("line.separator");
  String	FS = System.getProperty("file.separator");
  String	UD = System.getProperty("user.dir");
  String	author = System.getProperty("user.name");
  private int includemap = 0;
  private int nomap = 0;

  public final static String CONFIGURATION_BINDING = "configuration";

  public final static String SELECTION_BINDING = "selection";

  private Configuration configuration;

  private Selection selection;

  static final String [] mois = {
    "Jan.", "Feb.", "Mar.", "Apr.", "May", "Jun.",
    "Jul.", "Aug.", "Sep.", "Oct.", "Nov.", "Dec."};

  /**
   * Constructs a new {@link MakeSourceAction} component.
   */

  public MakeSourceAction () {
    putValue(NAME, "Make source");
    putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("alt M"));
    putValue(SHORT_DESCRIPTION, "Make source");
    URL url = getClass().getResource(
      "/org/objectweb/fractal/gui/resources/source.gif");
    putValue(SMALL_ICON, new ImageIcon(url));
    setEnabled(true);
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] { CONFIGURATION_BINDING, SELECTION_BINDING };
  }

  public Object lookupFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      return configuration;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      return selection;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = (Configuration)serverItf;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = (Selection)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = null;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = null;
    }
  }


  // -------------------------------------------------------------------------
  // Implementation of the ActionListener interface
  // -------------------------------------------------------------------------

  public void actionPerformed (final ActionEvent e) {
    Object o = selection.getSelection();
    if (o instanceof Component) {
      Component c = (Component)o;

      if (c.isComposite()) {
        message ("'"+c.getName()+"' is a composite");
        return;
      }
      if (c.getStatus() == Component.OK) {
        message ("Implementation of '"+c.getName()+"' is already a valid class ");
        return;
      }
      /*if ((c.getStatus() & Component.TYPE_MISSING) != 0) {
        message ("Type for '"+c.getName()+"' missing");
        return;
      }*/

      HashMap himport = new HashMap ();
      HashMap himplem = new HashMap ();
      HashMap hbind = new HashMap ();
      StringBuffer sb = new StringBuffer ();

      List cli = c.getClientInterfaces();
      List ser = c.getServerInterfaces();
      for (int j = 0; j < cli.size(); ++j) {
        ClientInterface itf = (ClientInterface)cli.get(j);
        if (itf.isCollection()) {
          includemap++;
        } else {
          nomap++;
        }
        if (itf.getMasterInterface() != null) {
//          himport.put(itf.getMasterInterface().getSignature(), itf.getName());
          himport.put(itf.getMasterInterface().getSignature(), itf);
        } else {
//          himport.put(itf.getSignature(), itf.getName());
          himport.put(itf.getSignature(), itf);
        }
      }
      for (int j = 0; j < ser.size(); ++j) {
        ServerInterface itf = (ServerInterface)ser.get(j);
        himport.put(itf.getSignature(), "z");
        if (!putimplem (itf.getSignature(), himplem, false)) {
          return;
        }
      }
//      if (!putimplem(c.getType(), himplem)) { return; }
//      putimplem(c.getType(), himplem, true);

      String suffixe = ".java";
      String classname = c.getImplementation();
      classname = classname.substring(classname.lastIndexOf('.')+1);
      JFileChooser fileChooser = new JFileChooser();
      fileChooser.setCurrentDirectory(new File(UD));
      fileChooser.setSelectedFile (new File(classname+suffixe));

      fileChooser.addChoosableFileFilter(
        new SimpleFileFilter("java", "Java source files"));
      if (fileChooser.showSaveDialog(null) != JFileChooser.APPROVE_OPTION) {
        return;
      }
      File f = fileChooser.getSelectedFile();

      String filename = f.getName();
      String pathname = f.getPath();
      if (filename.endsWith(suffixe)) {
        classname = filename.substring(0, filename.lastIndexOf(suffixe));
      }
      else {
        classname = filename;
        filename = filename.concat(suffixe);
        pathname = pathname.concat(suffixe);
      }

      FileOutputStream fos = null;
      PrintWriter outf = null;
      try {
        fos = new FileOutputStream (pathname);
        outf = new PrintWriter (new OutputStreamWriter (fos));

        outf.println
          ("/***"+LS+" * This skeleton has been generated automacally "
          +"by FractalGUI");
        outf.println (" * You can complete it as you like");
        outf.println (" *");
        outf.println (" * Author: "+author);
        GregorianCalendar grcl = new GregorianCalendar ();
        outf.println (" * Date: "+mois[grcl.get(Calendar.MONTH)]+", "
          +grcl.get(Calendar.DAY_OF_MONTH)+"th "+grcl.get(Calendar.YEAR)
          +"  at "+grcl.get(Calendar.HOUR_OF_DAY)+"H"+grcl.get(Calendar.MINUTE));
        outf.println (" *");
        outf.println (" */"+LS);


        String impl = c.getImplementation();
        if (impl.lastIndexOf('.') > 1) {
          outf.println
            ("package "+impl.substring(0, impl.lastIndexOf('.'))+";"+LS);
            impl = impl.substring(0, impl.lastIndexOf('.'));
        }

        //-----------------------------------------------------
        // imports et declaration de la classe
        //-----------------------------------------------------

        if (cli.size() > 0) {
          outf.println
            ("import org.objectweb.fractal.api.control.BindingController;");
          himplem.put("BindingController", "z");
        }
        if (c.getAttributeController().length() > 0) {
          outf.println
            ("import "+c.getAttributeController()+";");
          himplem.put(c.getAttributeController(), "z");
        }
        if (includemap > 0) {
          outf.println ("import java.util.Map;");
          outf.println ("import java.util.HashMap;");
        }

        for (Iterator it=himport.keySet().iterator(); it.hasNext(); ) {
          String key = (String)it.next();
          if (key.lastIndexOf('.') > 1) {
            if (impl.equals(key.substring(0, key.lastIndexOf('.')))) continue;
          }
          sb.append("import "+key+";"+LS);
        }

        sb.append (LS+LS);
        sb.append ("public class "+classname);
        sb.append (" implements"+LS);

        for (Iterator it=himplem.keySet().iterator(); it.hasNext(); ) {
          String key = (String)it.next();
          sb.append ("  "+key);
          if (it.hasNext()) {
            sb.append (","+LS);
          } else {
            sb.append (LS);
          }
        }
        sb.append ("{"+LS+LS);

        //-----------------------------------------------------
        // public final static String
        //-----------------------------------------------------

        for (Iterator it=himport.keySet().iterator(); it.hasNext(); ) {
          String key = (String)it.next();
//          String value = (String)himport.get(key);
//          if (value.compareTo("z") == 0) continue;
          Object value = himport.get(key);
          if (!(value instanceof ClientInterface)) continue;

          String mkey = key.substring(key.lastIndexOf('.')+1);
          sb.append("  public final static String "
            +mkey.toUpperCase()+"_BINDING = \""
            +mkey.toLowerCase()+"\";"+LS+LS);
          hbind.put(mkey, value);
        }

        for (Iterator it=hbind.keySet().iterator(); it.hasNext(); ) {
          String key = (String)it.next();
          ClientInterface clif = (ClientInterface)hbind.get(key);
          if (clif.isCollection()) {
            sb.append ("  private Map "+key.toLowerCase()+";"+LS+LS);
          } else {
            sb.append ("  private "+key+" "+key.toLowerCase()+";"+LS+LS);
          }
        }

        //-----------------------------------------------------
        // Contructeur
        //-----------------------------------------------------

        sb.append ("  /**"+LS);
        sb.append ("   * Constructs a new "+classname+LS);
        sb.append ("   */ "+LS);
        sb.append ("  public "+classname);
        sb.append (" () {"+LS);
        for (Iterator it = hbind.keySet().iterator(); it.hasNext(); ) {
          String key = (String)it.next();
          ClientInterface clif = (ClientInterface)hbind.get(key);
          if (clif.isCollection()) {
            sb.append ("    "+key.toLowerCase()+" = new HashMap();"+LS);
          }
        }
        sb.append ("    // ..."+LS+"  }"+LS+LS);

        //-----------------------------------------------------
        // BindingController
        //-----------------------------------------------------

        if (cli.size() > 0) {
          sb.append
            ("  // -----------------------------------------------------"+LS);
          sb.append
            ("  // Implementation of the BindingController interface"+LS);
          sb.append
            ("  // -----------------------------------------------------"+LS+LS);

          // -----------
          // listFc
          // -----------

          String debut = "";
          sb.append ("  public String[] listFc () {"+LS);

          // no map
          if (includemap == 0) {
            sb.append ("    return new String[] { ");
            if (hbind.size() > 1) sb.append(LS+"      ");
            for (Iterator it = hbind.keySet().iterator(); it.hasNext(); ) {
              String key = (String)it.next();
              sb.append (debut+key.toUpperCase()+"_BINDING");
              if (it.hasNext()) {
                sb.append (","+LS);
                debut = "      ";
              } else sb.append (" };"+LS);
            }
          }

          else if (includemap == 1) {
            for (Iterator it = hbind.keySet().iterator(); it.hasNext(); ) {
              String key = (String)it.next();
              ClientInterface clif = (ClientInterface)hbind.get(key);
              if (clif.isCollection()) {
                sb.append ("    int size = "+key.toLowerCase()+".size ();"+LS);
                sb.append ("    String[] names = new String[size+"+nomap+"];"+LS);
                sb.append ("    "+key.toLowerCase()
                  +".keySet().toArray(names);"+LS);
                break;
              }
            }
            int nb = 0;
            for (Iterator it = hbind.keySet().iterator(); it.hasNext(); ) {
              String key = (String)it.next();
              ClientInterface clif = (ClientInterface)hbind.get(key);
              if (clif.isCollection()) continue;
              sb.append("    names[size+"+nb+"] = "+key.toUpperCase()+"_BINDING;"+LS);
              nb++;
            }
          }

          else /* if (includemap > 1) */ {
            int nb = 0;
            for (Iterator it = hbind.keySet().iterator(); it.hasNext(); ) {
              String key = (String)it.next();
              ClientInterface clif = (ClientInterface)hbind.get(key);
              if (clif.isCollection()) {
                sb.append ("    int sz"+nb+" = "+key.toLowerCase()+".size ();"+LS);
                sb.append ("    String [] names"
                  +nb+" = new String[sz"+nb+"];"+LS);
                sb.append ("    "+key.toLowerCase()
                  +".keySet().toArray(names"+nb+");"+LS+LS);
                nb++;
              }
            }
            sb.append ("    int size = ");
            for (int i = 0; i < nb; i++) {
              if (i > 0) sb.append(" + sz"+i);
              else sb.append("sz"+i);
            }
            sb.append(" + "+nomap+";"+LS);
            sb.append("    String [] names = new String[size];"+LS);
            sb.append("    int d = 0;"+LS);
            for (int i = 0; i < nb; i++) {
              sb.append ("    for (int i = 0; i < sz"+i+"; i++) {"+LS);
              sb.append ("      names[d++] = names"+i+"[i];"+LS);
              sb.append ("    }"+LS);
            }
            for (Iterator it = hbind.keySet().iterator(); it.hasNext(); ) {
              String key = (String)it.next();
              ClientInterface clif = (ClientInterface)hbind.get(key);
              if (clif.isCollection()) continue;
              sb.append("    names[d++] = "+key.toUpperCase()+"_BINDING;"+LS);
              nb++;
            }
          }
          sb.append ("    return names;"+LS+"  }"+LS+LS);

          // -----------
          // lookupFc
          // -----------

          sb.append
            ("  public Object lookupFc (final String clientItfName) {"+LS);
          debut = "    ";
          for (Iterator it = hbind.keySet().iterator(); it.hasNext(); ) {
            String key = (String)it.next();
            ClientInterface clif = (ClientInterface)hbind.get(key);
            if (clif.isCollection()) {
              sb.append (debut+"if (clientItfName.startsWith ("
                +key.toUpperCase()+"_BINDING)) {"+LS);
              sb.append ("      return "+key.toLowerCase()+
                ".get(clientItfName);"+LS);
            } else {
              sb.append (debut+"if ("+key.toUpperCase()
                +"_BINDING.equals(clientItfName)) {"+LS);
              sb.append ("      return "+key.toLowerCase()+";"+LS);
            }
            if (it.hasNext()) {
              sb.append ("    }"); debut = " else ";
            }
          }
          sb.append ("    }"+LS);
          sb.append ("    return null;"+LS);
          sb.append ("  }"+LS+LS);

          // -----------
          // bindFc
          // -----------

          sb.append ("  public void bindFc ("+LS);
          sb.append ("    final String clientItfName,"+LS);
          sb.append ("    final Object serverItf)"+LS);
          sb.append ("  {"+LS);
          debut = "    ";
          for (Iterator it = hbind.keySet().iterator(); it.hasNext(); ) {
            String key = (String)it.next();
            ClientInterface clif = (ClientInterface)hbind.get(key);
            if (clif.isCollection()) {
              sb.append (debut+"if (clientItfName.startsWith ("
                +key.toUpperCase()+"_BINDING)) {"+LS);
              sb.append ("      "+key.toLowerCase()+".put(clientItfName, "
                +"serverItf);"+LS);
            } else {
              sb.append (debut+"if ("+key.toUpperCase()
                +"_BINDING.equals(clientItfName)) {"+LS);
              sb.append ("      "+key.toLowerCase()+" = "
                +"("+key+")serverItf;"+LS);
            }
            if (it.hasNext()) {
              sb.append ("    }"); debut = " else ";
            }
          }
          sb.append ("    }"+LS);
          sb.append ("  }"+LS+LS);

          // -----------
          // unbindFc
          // -----------

          sb.append ("  public void unbindFc ("+LS);
          sb.append ("    final String clientItfName)"+LS);
          sb.append ("  {"+LS);
          debut = "    ";
          for (Iterator it = hbind.keySet().iterator(); it.hasNext(); ) {
            String key = (String)it.next();
            ClientInterface clif = (ClientInterface)hbind.get(key);
            if (clif.isCollection()) {
              sb.append (debut+"if (clientItfName.startsWith ("
                +key.toUpperCase()+"_BINDING)) {"+LS);
                sb.append ("      "+key.toLowerCase()+".remove(clientItfName);"+LS);
            } else {
              sb.append (debut+"if ("+key.toUpperCase()
                +"_BINDING.equals(clientItfName)) {"+LS);
                sb.append ("      "+key.toLowerCase()+" = null;"+LS);
            }
            if (it.hasNext()) {
              sb.append ("    }"); debut = " else ";
            }
          }
          sb.append ("    }"+LS);
          sb.append ("  }"+LS+LS);
        }

        //-----------------------------------------------------
        // Implementations (as far as possible)
        //-----------------------------------------------------

        ClassLoader cl = this.getClass().getClassLoader();

        for (Iterator it=himplem.keySet().iterator(); it.hasNext(); ) {
          String key = (String)it.next();
          String value = (String)himplem.get(key);
          if (value.compareTo("z") == 0) continue;

          sb.append
            ("  // -----------------------------------------------------"+LS);
          sb.append
            ("  // Implementation of the "+key+" interface"+LS);
          sb.append
            ("  // -----------------------------------------------------"+LS+LS);

          try {
            Class cla = cl.loadClass(value);

            Method[] mtd = cla.getDeclaredMethods();
            for (int i = 0; i < mtd.length; i++) {
              Class[] param = mtd[i].getParameterTypes();
              sb.append ("  public "
                +lastFieldOf(mtd[i].getReturnType().getName())
                +" "+mtd[i].getName()+" (");

              if (param.length > 0) {
                for (int j=0; j < param.length; j++) {
                  if (j > 0) sb.append ("    ");
                  sb.append (lastFieldOf(param[j].getName())+"  p"+j);
                  if (j < param.length-1) {
                    sb.append(","+LS);
                  }
                  if (param[j].toString().startsWith("class ")) {
                    if (himport.get(param[j].getName()) == null) {
                      himport.put(param[j], "z");
                      outf.println("import "+param[j].getName()+";");
                    }
                  }
                }
              }
              sb.append (") {"+LS);
              sb.append ("    // add your own code here"+LS);
              sb.append ("  }"+LS+LS);
            }

          } catch (Exception ex) {
            sb.append ("  // --- no class found !"+LS);
          }
        }


        //-----------------------------------------------------
        // end of file
        //-----------------------------------------------------

        outf.print(sb);
        sb = null;
        outf.println("}");
        outf.close();
      }
      catch (Exception ex) { }

    }
  }

  private String lastFieldOf (String st) {
    int ind = st.lastIndexOf('.');
    if (ind < 0) return st;
    else return st.substring(ind+1);
  }

  private boolean putimplem (String nkey, HashMap hm, boolean force)
  {
    String key = nkey.substring(nkey.lastIndexOf('.')+1);
    if ((hm.containsKey(key)) && !force) {
      message ("'"+key+" is ambiguous !");
      return false;
    }
    hm.put(key, nkey);
    return true;
  }

  private void message (String motif) {
    JOptionPane.showMessageDialog(null, motif, "Sorry ...",
      JOptionPane.ERROR_MESSAGE);
  }
}
