/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.view;

import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Interface;
import org.objectweb.fractal.gui.selection.model.Selection;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Color;

/**
 * A component that draws components. This component only draws single
 * components, i.e. does not draw the sub components of a component, nor the
 * internal or external bingings of components.
 */

public interface ComponentRenderer {

  /**
   * The flags corresponding to a component whitout instances.
   */

  int NO_INSTANCE = 0;

  /**
   * The flags corresponding to a component whit instances no started
   */

  int INSTANCE = 1;

  /**
   * The flags corresponding to a component whit instances started
   */

  int STARTED = 2;

  /**
   * The flags corresponding to a component whit instances started then stopped
   */

  int STOPPED = 3;

  /**
   * Draws the given component in the given rectangle.
   *
   * @param g the graphics to be used to draw the component.
   * @param c the component to be drawn.
   * @param s the selected component.
   * @param r where the component must be drawn.
   * @param color the component color.
   * @param expanded if the component is exanded or not, i.e., if its sub
   *      components will be drawn (with other calls to this method) also or
   *      not.
   * @param m the displaying mode for interface name.
   * @param state the status for component (either STARTED or STOPPED).
   */

  void drawComponent (
    Graphics g,
    Component c,
    Selection s,
    Rectangle r,
    Color color,
    boolean expanded,
    int m,
    int state);

  /**
   * Returns the part of the given component that corresponds to the given
   * point.
   *
   * @param c a component.
   * @param r where the component is drawn.
   * @param expanded if the component is exanded or not, i.e., if its sub
   *      components will be drawn also or not.
   * @param x the x coordinate of the point of interest.
   * @param y the y coordinate of the point of interest.
   * @return the part of the given component that corresponds to the given
   *      point, or <tt>null</tt> if the given point does not correspond to any
   *      part of the given component.
   */

  ComponentPart getComponentPart (
    Component c,
    Rectangle r,
    boolean expanded,
    int x,
    int y);

  /**
   * Returns the position of the given interface.
   *
   * @param c a component.
   * @param r where the component is drawn.
   * @param i the interface whose position must be returned.
   * @return the position of the given interface.
   */

  Point getInterfacePosition (Component c, Rectangle r, Interface i);

  /**
   * Returns the area of the given component into which its sub components must
   * be drawn.
   *
   * @param c a component.
   * @param r where the component is drawn.
   * @return the area of the given component into which its sub components must
   *      be drawn.
   */

  Rectangle getSubComponentArea (Component c, Rectangle r);
}
