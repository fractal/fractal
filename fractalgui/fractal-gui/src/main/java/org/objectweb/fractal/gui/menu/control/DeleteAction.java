/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.menu.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Interface;
import org.objectweb.fractal.gui.model.ServerInterface;
import org.objectweb.fractal.gui.model.IllegalOperationException;
import org.objectweb.fractal.gui.selection.model.Selection;
import org.objectweb.fractal.gui.selection.model.SelectionListener;
import org.objectweb.fractal.swing.AbstractAction;

import java.awt.event.ActionEvent;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

/**
 * An action to delete the currently selected component or interface. This
 * action listens to the selection model in order to enable or disable itfself
 * when the selection changes.
 */

public class DeleteAction extends AbstractAction implements
  SelectionListener,
  BindingController
{

  /**
   * A mandatory client interface bound to a {@link Selection selection} model.
   * This model is used to know the component or interface to be deleted.
   */

  public final static String SELECTION_BINDING = "selection";

  /**
   * The selection client interface.
   */

  private Selection selection;

  /**
   * Constructs a new {@link DeleteAction} component.
   */

  public DeleteAction () {
    putValue(NAME, "Delete");
    putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("DELETE"));
    putValue(SHORT_DESCRIPTION, "Delete");
    URL url = getClass().getResource(
      "/org/objectweb/fractal/gui/resources/edittrash.gif");
    putValue(SMALL_ICON, new ImageIcon(url));
    setEnabled(false);
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] { SELECTION_BINDING };
  }

  public Object lookupFc (final String clientItfName) {
    if (SELECTION_BINDING.equals(clientItfName)) {
      return selection;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (SELECTION_BINDING.equals(clientItfName)) {
      selection = (Selection)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (SELECTION_BINDING.equals(clientItfName)) {
      selection = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the SelectionListener interface
  // -------------------------------------------------------------------------

  public void selectionChanged () {
    boolean enabled = false;
    Object o = selection.getSelection();
    if (o instanceof Component) {
      Component c = (Component)o;
      enabled = c.getParent() != null;
    } else if (o instanceof Interface) {
      enabled = true;
    }
    setEnabled(enabled);
  }

  // -------------------------------------------------------------------------
  // Implementation of the ActionListener interface
  // -------------------------------------------------------------------------

  public void actionPerformed (final ActionEvent e) {
    Object o = selection.getSelection();
    try {
      if (o instanceof Component) {
        Component c = (Component)o;
        c.getParent().removeSubComponent(c);
      } else if (o instanceof Interface) {
        Interface i = (Interface)o;
        if (i.isInternal()) {
          i = i.getComplementaryInterface();
        }
        if (i instanceof ClientInterface) {
          i.getOwner().removeClientInterface((ClientInterface)i);
        } else {
          i.getOwner().removeServerInterface((ServerInterface)i);
        }
      }
      selection.clearSelection();
    } catch (IllegalOperationException ioe) {
      JOptionPane.showMessageDialog(
        null, ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }
  }
}
