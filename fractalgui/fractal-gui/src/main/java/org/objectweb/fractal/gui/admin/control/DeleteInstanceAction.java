/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.admin.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.swing.AbstractAction;
import org.objectweb.fractal.gui.admin.model.AdminModel;
import org.objectweb.fractal.gui.selection.model.Selection;
import org.objectweb.fractal.gui.selection.model.SelectionListener;

import java.awt.event.ActionEvent;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.KeyStroke;
import javax.swing.JOptionPane;

/**
 * An action to exit FractalGUI.
 */

public class DeleteInstanceAction extends AbstractAction
  implements BindingController, SelectionListener
{

  /**
   * A mandatory client interface bound to a {@link AdminModel AdminModel}
   * model.
   */

  public final static String ADMIN_MODEL_BINDING = "admin-model";

  /**
   * An mandatory client interface bound to a {@link Selection Selection}.
   */

  public final static String SELECTION_BINDING = "selection";

  /**
   * The AdminModel client interface.
   */

  private AdminModel adminmodel;

  /**
   * The SelectionModel client interface.
   */

  private Selection selection;


  /**
   * Constructs a new {@link DeleteInstanceAction} component.
   */

  public DeleteInstanceAction () {
    putValue(NAME, "Delete Instance");
    putValue(SHORT_DESCRIPTION, "DeleteInstance");
    URL url = getClass().getResource(
      "/org/objectweb/fractal/gui/resources/empty.gif");
    putValue(SMALL_ICON, new ImageIcon(url));
    putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("control shift D"));
  }

  // -------------------------------------------------------------------------
  // Implementation of the UserBindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] {
      ADMIN_MODEL_BINDING,
      SELECTION_BINDING
    };
  }

  public Object lookupFc (final String clientItfName) {
    if (ADMIN_MODEL_BINDING.equals(clientItfName)) {
      return adminmodel;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      return selection;
    }
    return null;
  }

  public void bindFc (final String clientItfName, final Object serverItf) {
    if (ADMIN_MODEL_BINDING.equals(clientItfName)) {
      adminmodel = (AdminModel)serverItf;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = (Selection)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (ADMIN_MODEL_BINDING.equals(clientItfName)) {
      adminmodel = null;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the SelectionListener interface
  // -------------------------------------------------------------------------

  public void selectionChanged () {
    Object o = selection.getSelection();
    if (o instanceof Component) {
      setEnabled (true);
    } else {
      setEnabled(false);
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the ActionListener interface
  // -------------------------------------------------------------------------

  public void actionPerformed (final ActionEvent e) {
    Object o = selection.getSelection();
    if (o instanceof Component) {
      Component c = (Component)o;
      try {
      adminmodel.deleteInstance (c);
      } catch (Exception ex) {
        JOptionPane.showMessageDialog (
          null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      }
    }
  }
}
