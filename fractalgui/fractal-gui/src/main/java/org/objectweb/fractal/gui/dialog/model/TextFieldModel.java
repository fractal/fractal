/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.dialog.model;

import org.objectweb.fractal.gui.model.Component;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 * A {@link javax.swing.text.Document} model based on a {@link Component} model.
 * This model makes a conversion from a {@link Component} model to a
 * {@link javax.swing.text.Document}. It represents the name, the type, the
 * implementation... of this component, depending on how it was constructed.
 */

public class TextFieldModel extends PlainDocument {

  /**
   * Type of a {@link TextFieldModel} that represents component names.
   * See {@link #TextFieldModel TextFieldModel}.
   */

  public final static int NAME = 0;

  /**
   * Type of a {@link TextFieldModel} that represents component types.
   * See {@link #TextFieldModel TextFieldModel}.
   */

  public final static int TYPE = 1;

  /**
   * Type of a {@link TextFieldModel} that represents component implementations.
   * See {@link #TextFieldModel TextFieldModel}.
   */

  public final static int IMPLEMENTATION = 2;

  /**
   * Type of a {@link TextFieldModel} that represents component attribute
   * controllers. See {@link #TextFieldModel TextFieldModel}.
   */

  public final static int ATTRIBUTE_CONTROLLER = 3;

  /**
   * Type of a {@link TextFieldModel} that represents component template
   * controller descriptors. See {@link #TextFieldModel TextFieldModel}.
   */

  public final static int TEMPLATE_CONTROLLER_DESC = 4;

  /**
   * Type of a {@link TextFieldModel} that represents component controller
   * descriptors. See {@link #TextFieldModel TextFieldModel}.
   */

  public final static int COMPONENT_CONTROLLER_DESC = 5;

  /**
   * The type of this model. This type must be one of the constants defined in
   * this class.
   */

  protected int type;

  /**
   * The component model on which this model is based.
   */

  protected Component model;

  /**
   * If the {@link #insertString} or {@link #remove} method is currently being
   * executed. This flag is used to ignore some notifications, in order to avoid
   * infinite loops (see
   * <a href="../../../../../../overview-summary.html#mvc">here</a>).
   */
  protected boolean isTyping;

  /**
   * Constructs a new {@link TextFieldModel} object.
   *
   * @param type code of the component's part that must be represented by this
   *      model. This code must be one of the constants defined in this class.
   */

  public TextFieldModel (final int type) {
    this.type = type;
  }

  /**
   * Sets the component model on which this model is based.
   *
   * @param model a component.
   */

  protected void setComponentModel (final Component model) {
    this.model = model;
    String s = null;
    if (model != null) {
      switch (type) {
        case NAME:
          s = model.getName();
          break;
        case TYPE:
          s = model.getType();
          break;
        case IMPLEMENTATION:
          s = model.getImplementation();
          break;
        case ATTRIBUTE_CONTROLLER:
          s = model.getAttributeController();
          break;
        case TEMPLATE_CONTROLLER_DESC:
          s = model.getTemplateControllerDescriptor();
          break;
        case COMPONENT_CONTROLLER_DESC:
          s = model.getComponentControllerDescriptor();
          break;
        default:
      }
    }
    componentTextChanged(s);
  }

  /**
   * Notifies this listener that the part of the component model on which it is
   * based has changed.
   *
   * @param s the new component's name, type, implementation... depending on the
   *      {@link #type} of this model.
   */

  public void componentTextChanged (final String s) {
    try {
      if (!isTyping) {
        super.remove(0, getLength());
        super.insertString(0, s, null);
      }
    } catch (BadLocationException ignored) {
    }
  }

  // -------------------------------------------------------------------------
  // Overriden PlainDocument methods
  // -------------------------------------------------------------------------

  public void insertString (
    final int offs,
    final String str,
    final AttributeSet a) throws BadLocationException
  {
    super.insertString(offs, str, a);
    setComponentText(getText(0, getLength()));
  }

  public void remove (
    final int offs,
    final int len) throws BadLocationException
  {
    super.remove(offs, len);
    setComponentText(getText(0, getLength()));
  }

  /**
   * Modifies the model on which this model is based, to reflect a change in
   * this model.
   *
   * @param s the new component's name, type, implementation... depending on the
   *      {@link #type} of this model.
   */

  protected void setComponentText (final String s) {
    if (model == null) {
      return;
    }
    isTyping = true;
    try {
      switch (type) {
        case NAME:
          model.setName(s);
          break;
        case TYPE:
          model.setType(s);
          break;
        case IMPLEMENTATION:
          model.setImplementation(s);
          break;
        case ATTRIBUTE_CONTROLLER:
          model.setAttributeController(s);
          break;
        case TEMPLATE_CONTROLLER_DESC:
          model.setTemplateControllerDescriptor(s);
          break;
        case COMPONENT_CONTROLLER_DESC:
          model.setComponentControllerDescriptor(s);
          break;
        default:
      }
    } finally {
      isTyping = false;
    }
  }
}
