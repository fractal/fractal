/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Hashtable;

public class UserDataStorage implements UserData
{
  static String FS = new String (System.getProperty("file.separator"));
  static String UH = new String (System.getProperty("user.home"));
  static String UN = new String (System.getProperty("user.name"));
  String filename = UH+FS+UN+".frtlgui";

  int currentdepth = 0;
  int currentwidth = 780;
  int currentheight = 660;
  int currentstatus = UserData.NO_MODIFIED;
  private Integer ZERO = new Integer (0);
  private int   current_id = 0;
  UFProject     ufp = null;
  Hashtable     hfp = new Hashtable ();

  public UserDataStorage () {
    open();
  }

  /**
   * Open the file which contains all registered User Data. If this file doesn't
   * exist, a new empty file is created.
   *
   */
  public void open ()	{
    try {
      FileInputStream fin = new FileInputStream (filename);
      BufferedReader brd = new BufferedReader(new InputStreamReader(fin));
      String st = new String ("");
      UFProject cufp = new UFProject ();

      while (st != null) {
        st = brd.readLine();
        if (st == null) break;
        int i = st.indexOf('=');
        if (st.length() == i+1) continue;
        Integer val = new Integer (st.substring(0, i));

        int vali = val.intValue() % 100;
        int valp = val.intValue() / 100;

        if (current_id < valp) current_id = valp+1;
        ufp = (UFProject)getProject(valp);
        if (ufp == null) {
          ufp = new UFProject ();
          ufp.setId(valp);
        }

        switch (vali) {
          case (UserData.CURRENT_DEPTH) :
            currentdepth = intValue (st); break;
          case (UserData.CURRENT_WIDTH) :
            currentwidth = intValue (st);  break;
          case (UserData.CURRENT_HEIGHT) :
            currentheight = intValue (st);  break;
          case (UserData.CURRENT_CONFIG) :
            currentstatus = intValue (st);  break;
          default :
            ufp.setStringData(vali, st.substring(i+1));
            cufp.setStringData(vali, st.substring(i+1));
          break;
        }
        addProject(ufp);
      }
      brd.close();
      if (hfp.size() == 0) {  }
    }
    catch (Exception ex) {  }
  }

  // -----

  private int intValue (String st)
  {
    int i = st.indexOf('=');
    Integer val = new Integer (st.substring(i+1));
    return val.intValue();
  }

 /**
  * Clean the user's data
  */
  public void clean () {
    hfp.clear();
    save ();
  }

 /**
  * Save all registered data in the file of the user.  If this file doesn't exist, a new empty file
  * is created.
  *
  */
  public void save () {
    try {
      FileOutputStream fos = new FileOutputStream (filename);
      PrintWriter os = new PrintWriter (new OutputStreamWriter (fos));

      os.println (UserData.CURRENT_DEPTH+"="+currentdepth);
      os.println (UserData.CURRENT_WIDTH+"="+currentwidth);
      os.println (UserData.CURRENT_HEIGHT+"="+currentheight);

      for (Enumeration en = hfp.keys(); en.hasMoreElements() ;) {
        Integer key = (Integer)en.nextElement();
			  UFProject fp = (UFProject)hfp.get(key);
        int ind = fp.getId();
        for (int i = 0; i < UserData.NB_DIR; i++) {
          String field = fp.getStringData(i);
          if (field != null) {
            os.println (((100*ind)+i+UserData.START_INDEX)+"="+field);
          } else { os.println (((100*ind)+i+UserData.START_INDEX)+"="); }
        }
      }
      os.close();
    }
    catch (Exception ex) { }
  }

 /**
  * set the value for a given type. The type must belong to the CURRENT_
  * family type or else an Exceptioon is thrown.
  *
  * @param typ is the Data type.
  * @param v is the int value for the type.
  */
  public void setIntData (int typ, int v) throws Exception {
    if (typ == UserData.CURRENT_DEPTH) currentdepth = v;
    else if (typ == UserData.CURRENT_WIDTH) currentwidth = v;
    else if (typ == UserData.CURRENT_HEIGHT) currentheight = v;
    else throw new IllegalArgumentException();
  }

 /**
  * get the int value for a given type. The type must belong to the CURRENT_
  * family type or else an Exception is thrown.
  *
  * @param typ is the Data type.
  */
  public int getIntData (int typ) throws Exception {
    if (typ == UserData.CURRENT_DEPTH) return currentdepth;
    else if (typ == UserData.CURRENT_WIDTH) return currentwidth;
    else if (typ == UserData.CURRENT_HEIGHT) return currentheight;
    else throw new IllegalArgumentException();
  }

 /**
  * set the value for a given type. The type must belong to the LAST_
  * family type.
  *
  * @param typ is the Data type.
  * @param s is the String value for the type.
  */
  public void setStringData (int typ, String s) throws Exception {
    UFProject ufp = (UFProject)hfp.get(ZERO);
    if (ufp == null) {
      ufp = new UFProject ();
      ufp.setId(0);
      for (int i = 0; i < UserData.NB_DIR; i++)
        ufp.setStringData(i+UserData.START_INDEX, "");
      hfp.put(ZERO, ufp);
      save ();
    }
    if ((typ < UserData.START_INDEX) ||
        (typ > UserData.NB_DIR+UserData.START_INDEX-1)) throw
      new IllegalArgumentException();
    ufp.setStringData(typ, s);
  }

 /**
  * get the int value for a given type. The type must belong to the LAST_
  * family type or else an Exception is thrown.
  *
  * @param typ is the Data type.
  */
  public String getStringData (int typ) throws Exception {
    UFProject ufp = (UFProject)hfp.get(ZERO);
    if (ufp == null) return null;
    if ((typ < UserData.START_INDEX) ||
        (typ > UserData.NB_DIR+UserData.START_INDEX-1)) throw
      new IllegalArgumentException();
    return ufp.getStringData(typ-UserData.START_INDEX);
  }

 /**
  * Adds a new FProject to the project list. If the project already exists,
  * nothing is done.
  *
  * @param id is the Data type.
  * @return <b>true</b> if the FProject exist, <b>false</b> otherwise.
  */
  public boolean projectExists (int id) {
    Object fp = hfp.get(new Integer(id));
    if (fp == null) return false;
    else return true;
  }

 /**
  * Adds a new FProject to the project list. If the project already exists,
  * nothing is done.
  * If thomething goes wrong an Exception is thrown.
  *
  * @param proj is the FProject to add.
  */
  public void addProject (UserData.FProject proj) throws Exception {
    int i = proj.getId();
    if (i < 0) throw new IllegalArgumentException();
    Integer in = new Integer(i);
    if (hfp.get(in) != null) {
      in = new Integer (current_id);
      proj.setId(current_id);
    }
    hfp.put(in, proj);
    return;
  }

 /**
  * Removes the FProject with specified <b>id</b> from the project list.
  * If the project doesn't exist, nothing is done.
  *
  * @param id is the FProject id to remove.
  */
  public void removeProject (int id) {
  }

  /**
   * Adds a new FProject to the project list. If the project already exists,
   * nothing is done.
   * If thomething goes wrong an Exception is thrown.
   *
   * @param id is the FProject identifier.
   * @return the FProject if exists or null otherwise.
   */
  public UserData.FProject getProject (int id) {
    UserData.FProject fp = (UserData.FProject)hfp.get(new Integer(id));
    return fp;
  }


  /**
   * Asks for a new FProject.
   *
   * @return the new FProject
   */
  public UserData.FProject getNewProject () {
    UFProject fp = new UFProject ();
    fp.setId(current_id++);
    return fp;
  }


  /* ---------------------------------------------------------- */

  public class UFProject implements UserData.FProject {
    String [] fields = new String[UserData.NB_DIR];
    private int ident = -1;

    /**
     * set the id for the project
     *
     * @param i is the Data type.
     */
    public void setId (int i) { ident = i; }

    /**
     * get the id of the project
     *
     * @return the identifier of the FProject.
     */
    public int getId () { return ident; }

    /**
     * set the value for a given type. The type must belong to the LAST_ family
     * type.
     *
     * @param typ is the Data type.
     * @param s is the String value for the type.
     */
    public void setStringData (int typ, String s) {
    if ((typ < UserData.START_INDEX) ||
        (typ > UserData.NB_DIR+UserData.START_INDEX-1)) return;
      fields[typ-UserData.START_INDEX] = new String (s);
    }

    /**
     * get the int value for a given type. The type must belong to the LAST_
     * family type or else an Exception is thrown.
     *
     * @param typ is the Data type.
     */
    public String getStringData (int typ) {
      if ((typ < 0) || (typ > UserData.NB_DIR-1)) return null;
      return fields[typ];
    }
  }
}
