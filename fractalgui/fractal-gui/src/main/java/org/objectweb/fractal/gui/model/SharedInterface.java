/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.model;

import java.util.List;

/**
 * Abstract implementation of the {@link Interface} interface for interfaces of
 * slave components.
 */

public abstract class SharedInterface implements Interface {

  /**
   * The component that owns this interface.
   */

  protected SharedComponent owner;

  /**
   * The master interface of this slave interface.
   */

  protected Interface masterInterface;

  /**
   * Constructs a new interface for a slave component.
   *
   * @param owner a slave component.
   * @param masterInterface the interface of the master component to which the
   *      constructed interface will correspond.
   */

  SharedInterface (
    final SharedComponent owner,
    final Interface masterInterface)
  {
    this.owner = owner;
    this.masterInterface = masterInterface;
  }

  public Component getOwner () {
    return owner;
  }

  public long getStatus () {
    return masterInterface.getStatus();
  }

  public void setStatus (final long status) {
    // does nothing
  }

  public String getName () {
    return masterInterface.getName();
  }

  public void setName (final String name) {
    masterInterface.setName(name);
  }

  public String getSignature () {
    return masterInterface.getSignature();
  }

  public void setSignature (final String signature) {
    masterInterface.setSignature(signature);
  }

  public boolean isInternal () {
    return masterInterface.isInternal();
  }

  public boolean isOptional () {
    return masterInterface.isOptional();
  }

  public void setIsOptional (final boolean isOptional) {
    masterInterface.setIsOptional(isOptional);
  }

  public boolean isCollection () {
    return masterInterface.isCollection();
  }

  public void setIsCollection (final boolean isCollection) {
    masterInterface.setIsCollection(isCollection);
  }

  public Interface getMasterCollectionInterface () {
    Interface i = masterInterface.getMasterCollectionInterface();
    return i == null ? null : owner.getInterface(i);
  }

  public List getSlaveCollectionInterfaces () { // TODO
    throw new RuntimeException("Internal error");
  }

  public Interface getComplementaryInterface () {
    throw new RuntimeException("Internal error");
  }

  public Interface getMasterInterface () {
    return masterInterface;
  }
}
