/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.model;

/**
 * A binding between a client and a server interface.
 */

public interface Binding {

  /**
   * The {@link #getStatus status} flags corresponding to a valid binding.
   */

  long OK = 0;

  /**
   * A {@link #getStatus status} flag indicating that the client interface and
   * the server interface are not compatible.
   */

  long INCOMPATIBLE_TYPE_INTERFACES = 1;


  /**
   * Returns the status of this binding.
   *
   * @return the status of this binding.
   * @see #setStatus
   */

  long getStatus ();

  /**
   * Sets the status of this binding.
   *
   * @param status the new status of this bindinge.
   * @see #getStatus
   */

  void setStatus (long status);

  /**
   * Returns the client interface that is bound by this binding.
   *
   * @return the client interface that is bound by this binding.
   */

  ClientInterface getClientInterface ();

  /**
   * Returns the server interface that is bound by this binding.
   *
   * @return  the server interface that is bound by this binding.
   */

  ServerInterface getServerInterface ();

  // TODO ajouter un status, le mettre a jour quand il faut
  // (erreurs possibles: incompatible interface types, client and server
  // components do not have a common parent...)
}
