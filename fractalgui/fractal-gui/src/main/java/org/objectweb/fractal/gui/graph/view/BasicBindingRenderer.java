/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.view;

import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.ServerInterface;
import org.objectweb.fractal.gui.Constants;
import org.objectweb.fractal.gui.model.Binding;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

/**
 * Basic implementation of the {@link BindingRenderer} interface.
 */

public class BasicBindingRenderer implements BindingRenderer {

  /**
   * Minimal space between a component and a binding.
   */

  private final static int BORDER = 5;

  /**
   * Constructs a new {@link BasicBindingRenderer} component.
   */

  public BasicBindingRenderer () {
  }

  public void drawBinding (
    final Graphics g,
    final Rectangle c,
    final Rectangle s,
    final Point cp,
    final Point sp,
    final Binding bd,
    final int range)
  {
    g.setColor(Constants.BINDING_COLOR);
    if (bd != null) {
      if (bd.getStatus() != Binding.OK) g.setColor(Constants.ERROR_COLOR);
      else {
        ClientInterface citf = bd.getClientInterface();
        ServerInterface sitf = bd.getServerInterface();
        if ((citf.getName().length() < 1) ||
            (sitf.getName().length() < 1) ||
            (citf.getSignature().length() < 1) ||
            (sitf.getSignature().length() < 1)) g.setColor(Constants.WARNING_COLOR);
      }
    }

    if (sp.x >= cp.x || s == null) {
      g.drawLine(cp.x, cp.y, sp.x, sp.y);
    } else {
      int v;
      if (s.y + s.height < c.y) {
        v = (c.y + s.y + s.height) / 2;
      } else if (s.y > c.y + c.height) {
        v = (s.y + c.y + c.height) / 2;
      } else {
        int vMin = Math.min(s.y, c.y);
        int vMax = Math.max(s.y + s.height, c.y + c.height);
        if (cp.y - vMin < vMax - cp.y) {
          v = vMin - BORDER;
        } else {
          v = vMax + BORDER;
        }
      }
      int decal = 3*range;
      g.drawLine(cp.x, cp.y, cp.x + BORDER+decal, cp.y);
      g.drawLine(cp.x + BORDER+decal, cp.y, cp.x + BORDER+decal, v+decal);
      g.drawLine(cp.x + BORDER+decal, v+decal, sp.x - BORDER-decal, v+decal);
      g.drawLine(sp.x - BORDER-decal, v+decal, sp.x - BORDER-decal, sp.y);
      g.drawLine(sp.x - BORDER-decal, sp.y, sp.x, sp.y);
    }
  }
}
