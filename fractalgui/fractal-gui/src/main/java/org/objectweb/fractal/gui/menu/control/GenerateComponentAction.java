/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.menu.control;

import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.ServerInterface;
import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.Interface;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.Attribute;

import java.awt.event.ActionEvent;
import java.net.URL;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

/**
 * An action to create new components from an existing component class.
 */

public class GenerateComponentAction extends CreateAction {

  /**
   * Name of the last generated component.
   */

  private String lastClassName;

  /**
   * Constructs a new {@link GenerateComponentAction} component.
   */

  public GenerateComponentAction () {
    putValue(NAME, "New component...");
    putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("control INSERT"));
    URL url = getClass().getResource(
      "/org/objectweb/fractal/gui/resources/empty.gif");
    putValue(SMALL_ICON, new ImageIcon(url));
    setEnabled(false);
  }

  // -------------------------------------------------------------------------
  // Implementation of the SelectionListener interface
  // -------------------------------------------------------------------------

  public void selectionChanged () {
    boolean enabled = false;
    Object o = selection.getSelection();
    if (o instanceof Component) {
      Component c = (Component)o;
      enabled = c.getMasterComponent() == null;
    }
    setEnabled(enabled);
  }

  // -------------------------------------------------------------------------
  // Implementation of the ActionListener interface
  // -------------------------------------------------------------------------

  public void actionPerformed (final ActionEvent e) {
    String s = (String)JOptionPane.showInputDialog(
      null,
      "Enter the name of an existing primitive component class",
      "New Component",
      JOptionPane.QUESTION_MESSAGE,
      null,
      null,
      lastClassName);
    if (s != null) {
      lastClassName = s;
      ClassReader cr;
      try {
        cr = new ClassReader(s);
      } catch (IOException ioe) {
        JOptionPane.showMessageDialog(
          null, ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        return;
      }
      Object o = selection.getSelection();
      Component parent = (Component)o;
      Component child = factory.createComponent();
      cr.accept(new ClassAnalyzer(child), ClassReader.SKIP_DEBUG);
      parent.addSubComponent(child);
      //selection.selectComponent(child);
    }
  }

  /**
   * A class visitor to introspect a compiled class and to extract information
   * about provided and required interfaces. Some information are extracted by
   * recognizing naming patterns.
   */

  class ClassAnalyzer implements ClassVisitor {

    /**
     * The component that must be modified to reflect the information extracted
     * from the visited class.
     */

    private Component component;

    /**
     * A map that stores the descriptors of the fields of the visited class.
     */

    private Map fields;

    /**
     * Constructs a new {@link GenerateComponentAction.ClassAnalyzer} object.
     *
     * @param component the component that must be modified to reflect the
     *      information extracted from the visited class.
     */

    public ClassAnalyzer (final Component component) {
      this.component = component;
      this.fields = new HashMap();
    }

    public void visit (
      final int version,
      final int access,
      final String name,
      final String signature,
      final String superName,
      final String[] interfaces)
    {
      if (!superName.equals("java/lang/Object")) {
        try {
          ClassReader cr = new ClassReader(superName.replace('/', '.'));
          cr.accept(this, ClassReader.SKIP_DEBUG);
        } catch (IOException ioe) {
        }
      }
      if (name.indexOf('/') != -1) {
        component.setName(name.substring(name.lastIndexOf('/') + 1));
      } else {
        component.setName(name);
      }
      component.setType(name.replace('/', '.'));
      component.setImplementation(name.replace('/', '.'));
      for (int i = 0; i < interfaces.length; ++i) {
        if (interfaces[i].equals(
              "org/objectweb/fractal/api/control/BindingController") ||
            interfaces[i].equals("java/lang/Cloneable") ||
            interfaces[i].equals("java/io/Serializable"))
        {
          continue;
        }
        String s = interfaces[i].substring(interfaces[i].lastIndexOf('/') + 1);
        StringBuffer buf = new StringBuffer();
        for (int j = 0; j < s.length(); ++j) {
          if (Character.isUpperCase(s.charAt(j))) {
            if (j > 0) {
              buf.append('-');
            }
            buf.append(Character.toLowerCase(s.charAt(j)));
          } else {
            buf.append(s.charAt(j));
          }
        }
        ServerInterface sitf = factory.createServerInterface(component);
        sitf.setSignature(interfaces[i].replace('/', '.'));
        sitf.setName(buf.toString());
        component.addServerInterface(sitf);
      }
    }

    public AnnotationVisitor visitAnnotation (
      final String desc,
      final boolean visible)
    {
      // does nothing
      return null;
    }

    public void visitSource (
      final String source,
      final String debug)
    {
      // does nothing
    }

    public void visitInnerClass (
      final String name,
      final String outerName,
      final String innerName,
      final int access)
    {
      // does nothing
    }

    public void visitOuterClass (
      final String owner,
      final String name,
      final String desc)
    {
      // does nothing
    }

    public FieldVisitor visitField (
      final int access,
      final String name,
      final String desc,
      final String signature,
      final Object value)
    {
      if ((access & Opcodes.ACC_STATIC) != 0) {
        if (name.endsWith("_BINDING") && value instanceof String) {
          ClientInterface citf = factory.createClientInterface(component);
          citf.setName((String)value);
          component.addClientInterface(citf);
        }
      } else {
        fields.put(name, desc);
      }
      return null;
    }

    public MethodVisitor visitMethod (
      final int access,
      final String name,
      final String desc,
      final String signature,
      final String[] exceptions)
    {
      return null;
    }

    public void visitAttribute (final Attribute attribute) {
      // does nothing
    }

    public void visitEnd () {
      List itfs = component.getClientInterfaces();
      for (int i = 0; i < itfs.size(); ++i) {
        Interface itf = (Interface)itfs.get(i);
        String desc = (String)fields.get(itf.getName());
        if (desc != null) {
          Type type = Type.getType(desc);
          if (type.getSort() == Type.OBJECT) {
            itf.setSignature(type.getClassName());
          }
        }
      }
    }
  }
}
