/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.admin.model;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.NoSuchInterfaceException;

import org.objectweb.fractal.util.Fractal;

import org.objectweb.fractal.gui.model.Binding;
import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Configuration;
import org.objectweb.fractal.gui.model.ConfigurationListener;
import org.objectweb.fractal.gui.model.IllegalOperationException;
import org.objectweb.fractal.gui.model.Interface;
import org.objectweb.fractal.gui.model.ServerInterface;
import org.objectweb.fractal.gui.model.VetoableConfigurationListener;
import org.objectweb.fractal.gui.repository.api.Storage;
import org.objectweb.fractal.gui.repository.api.Repository;
import org.objectweb.fractal.gui.admin.model.AdminModelListener;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.List;

import java.lang.reflect.Method;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.FontMetrics;
import javax.swing.JOptionPane;


public class BasicAdminModel implements
  BindingController,
  AdminModel,
  VetoableConfigurationListener,
  ConfigurationListener
{

  /**
   * A mandatory client interface bound to a {@link Configuration configuration}
   * model. This is the model on which this model is based and synchronized.
   */

  public final static String CONFIGURATION_BINDING = "configuration";

  /**
   * A mandatory client interface bound to a {@link Storage storage}. This is
   * the storage into which the saved configurations are stored.
   */

  public final static String STORAGE_BINDING = "storage";

  /**
   * A mandatory client interface bound to a {@link Repository repository}. This
   * repository is used to store the configurations in the storage.
   */

  public final static String REPOSITORY_BINDING = "repository";

  /**
   * A mandatory client interface bound to an {@link AdminModelListener}.
   */

  public final static String ADMIN_MODEL_LISTENER_BINDING = "admin-model-listener";

  /**
   * The configuration client interface.
   */

  private Configuration configuration;

  /**
   * The storage client interface.
   */

  private Storage storage;

  /**
   * The repository client interface.
   */

  private Repository repository;

  /**
   * The admin model listener client interface.
   */

  private AdminModelListener adminlistener;

  /**
   * A map associating ComponentIdentity interfaces to Component objects.
   */

  private Map instances;

  private int range = -1;

  public BasicAdminModel () {
    instances = new WeakHashMap();
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] {
      CONFIGURATION_BINDING,
      STORAGE_BINDING,
      ADMIN_MODEL_LISTENER_BINDING,
      REPOSITORY_BINDING
    };
  }

  public Object lookupFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      return configuration;
    } else if (STORAGE_BINDING.equals(clientItfName)) {
      return storage;
    } else if (ADMIN_MODEL_LISTENER_BINDING.equals(clientItfName)) {
      return adminlistener;
    } else if (REPOSITORY_BINDING.equals(clientItfName)) {
      return repository;
    }
    return null;
  }

  public void bindFc (final String clientItfName, final Object serverItf) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = (Configuration)serverItf;
    } else if (STORAGE_BINDING.equals(clientItfName)) {
      storage = (Storage)serverItf;
    } else if (ADMIN_MODEL_LISTENER_BINDING.equals(clientItfName)) {
      adminlistener = (AdminModelListener)serverItf;
    } else if (REPOSITORY_BINDING.equals(clientItfName)) {
      repository = (Repository)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = null;
    } else if (STORAGE_BINDING.equals(clientItfName)) {
      storage = null;
    } else if (ADMIN_MODEL_LISTENER_BINDING.equals(clientItfName)) {
      adminlistener = null;
    } else if (REPOSITORY_BINDING.equals(clientItfName)) {
      repository = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the AdminModel interface
  // -------------------------------------------------------------------------

  public org.objectweb.fractal.api.Component getInstance (final Component model) {
    if (!configuration.getRootComponent().contains(model)) {
      return null;
    }
    return (org.objectweb.fractal.api.Component)instances.get(model);
  }

  // --- createInstance

  public org.objectweb.fractal.api.Component createInstance (
    final Component model,
    final org.objectweb.fractal.api.Component bootstrapComponent) throws Exception
  {
    if (!configuration.getRootComponent().contains (model)) {
      return null;
    }
    if (getInstance(model) != null) {
      throw new IllegalOperationException (
        "Instance already created !");
    }

    // ADL generation
    storage.open ("tmp");
    try {
      String root = repository.storeComponent (model, null, "inline");
    } catch (Exception ex) {
      throw new IllegalOperationException (ex.getMessage());
    } finally {
      storage.close();
    }

    Factory f = FactoryFactory.getFactory(
      "org.objectweb.fractal.gui.admin.model.TmpFactory", 
      FactoryFactory.FRACTAL_BACKEND, new HashMap());
    
    org.objectweb.fractal.api.Component comp;
    
    try {
      Map ctxt = new HashMap();
      ctxt.put("bootstrap", bootstrapComponent);
      comp = (org.objectweb.fractal.api.Component)f.newComponent(model.getType(), ctxt);
      addInstance(model, comp);
      if (model.getParent() != null && getInstance(model.getParent()) != null) {
        subComponentAdded(model.getParent(), model, 0);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new IllegalOperationException (
        "No instance created : maybe, the exec. dir is no correct or missing");      
    }
    
    adminlistener.componentCreated(model);
    return comp;
  }

  // --- addInstances

  private void addInstance (
    final Component model,
    final org.objectweb.fractal.api.Component instance) throws Exception
  {
    Component c = model;
    if (model.getMasterComponent() != null) {
      c = model.getMasterComponent();
    }
    instances.put(c, instance);
    List slaves = c.getSlaveComponents();
    for (int i = 0; i < slaves.size(); ++i) {
      instances.put(slaves.get(i), instance);
    }
    
    List subComponents = model.getSubComponents();
    if (subComponents.size() > 0) {
      ContentController cc = Fractal.getContentController(instance);
      org.objectweb.fractal.api.Component[] subCi = cc.getFcSubComponents();
      
      for (int i = 0; i < subComponents.size(); ++i) {
        Component subC = (Component)subComponents.get(i);
        Set s = new HashSet();
        if (subC.getMasterComponent() != null) {
          s.add(subC.getMasterComponent().getName());
          List l = subC.getMasterComponent().getSlaveComponents();
          for (int j = 0; j < l.size(); ++j) {
            s.add(((Component)l.get(j)).getName());
          }
        } else {
          s.add(subC.getName());
          List l = subC.getSlaveComponents();
          for (int j = 0; j < l.size(); ++j) {
            s.add(((Component)l.get(j)).getName());
          }
        }

        org.objectweb.fractal.api.Component subM = null;
        for (int j = 0; j < subCi.length; ++j) {
          org.objectweb.fractal.api.Component sci = subCi[j];
          if (s.contains(Fractal.getNameController(sci).getFcName())) {
            subM = sci;
            break;
          }
        }
        
        if (subM == null) {
          throw new Exception("No instance for sub component '" + subC.getName() + "' " + s);
        }
        
        addInstance(subC, subM);
      }
    }
  }

  // --- deleteInstance

  public void deleteInstance (final Component model) {
    if (!configuration.getRootComponent().contains(model)) {
      return;
    }
    if (getInstance(model) == null) {
      throw new IllegalOperationException(
        "Instance does not exist");
    }
    if (isStarted(model)) {
      throw new IllegalOperationException(
        "Cannot delete an instance which is started");
    }

    if (model.getParent() != null) {
      if (isStarted(model.getParent())) {
        throw new IllegalOperationException(
          "Cannot delete an instance whose parent is started");
      }
      
      org.objectweb.fractal.api.Component p = getInstance(model.getParent());
      org.objectweb.fractal.api.Component c = getInstance(model);

      subComponentRemoved(model.getParent(), model, 0);
    }
    
    removeInstance(model);
    
    adminlistener.componentDeleted(model);
  }
  
  private void removeInstance (final Component model) {
    Component c = model;
    if (model.getMasterComponent() != null) {
      c = model.getMasterComponent();
    }
    instances.remove(c);
    List slaves = c.getSlaveComponents();
    for (int i = 0; i < slaves.size(); ++i) {
      instances.remove(slaves.get(i));
    }
    
    List subComponents = model.getSubComponents();
    for (int i = 0; i < subComponents.size(); ++i) {
      removeInstance((Component)subComponents.get(i));
    }
  }

  // --- isStarted

  public boolean isStarted (final Component model) {
    org.objectweb.fractal.api.Component ci = getInstance(model);
    if (ci != null) {
      try {
        LifeCycleController lc = Fractal.getLifeCycleController(ci);
        return lc.getFcState().equals("STARTED");
      } catch (NoSuchInterfaceException e) {
        return true;
      }
    }
    return false;
  }

  // --- start

  public void start (final Component model) {
    org.objectweb.fractal.api.Component ci = getInstance(model);

    if (ci != null) {
      try {
        LifeCycleController lc = Fractal.getLifeCycleController(ci);
        if (lc != null) {
          lc.startFc();

          // faire comme runAction ?
          List lser = model.getServerInterfaces();
          /*if (lser.size() > 0) {
            ServerInterface ser = (ServerInterface)lser.get(0);
            String signat = ser.getSignature();
            String lien = ser.getName();
            
            Object iref = null;
            try {
              iref = ci.getFcInterface (lien);
            } catch (Exception ex) {
              ex.printStackTrace();
            }
            
            // call main method
            ClassLoader cl = this.getClass().getClassLoader();
            try {
              Class cla = cl.loadClass(signat);
              
              java.lang.reflect.Method[] meth = cla.getMethods();
              if (meth.length > 0) {
                Object[] arguments = new Object[] { };
                String [] items = new String [meth.length];
                for (int i = 0; i < meth.length; i++) {
                  items[i] = meth[i].getName();
                }
                FrameChoiceBox fcb =
                  new FrameChoiceBox ("Method for start", items);
                if (range < 0) return;
                meth[range].invoke(iref, arguments);
              }
            } catch (Exception ex) {
              //ex.printStackTrace();
              avert ("Invocation failed : "+ex.getMessage());
              return;
            }
          }*/
        }
        adminlistener.componentStarted(model);
      } catch (NoSuchInterfaceException e) {
      } catch (IllegalLifeCycleException e) {
//        throw new IllegalOperationException (
        avert ("Cannot start this instance");
      }
    }
  }

  // --- stop

  public void stop (final Component model) {
    org.objectweb.fractal.api.Component ci = getInstance(model);
    if (ci != null) {
      try {
        LifeCycleController lc = Fractal.getLifeCycleController(ci);
        if (lc != null) {
          lc.stopFc();

          // faire comme runAction ?
          List lser = model.getServerInterfaces();
          /*if (lser.size() > 0) {
            ServerInterface ser = (ServerInterface)lser.get(0);
            String signat = ser.getSignature();
            String lien = ser.getName();
            
            Object iref = null;
            try {
              iref = ci.getFcInterface (lien);
            } catch (Exception ex) {
              ex.printStackTrace();
            }
            
            // call main method
            ClassLoader cl = this.getClass().getClassLoader();
            try {
              Class cla = cl.loadClass(signat);
              
              java.lang.reflect.Method[] meth = cla.getMethods();
              if (meth.length > 0) {
                Object[] arguments = new Object[] { };
                String [] items = new String [meth.length];
                for (int i = 0; i < meth.length; i++) {
                  items[i] = meth[i].getName();
                }
                FrameChoiceBox fcb =
                  new FrameChoiceBox ("Method for stop", items);
                if (range < 0) return;
                meth[range].invoke(iref, arguments);
              }
            } catch (Exception ex) { return; }
          }*/
        }
        adminlistener.componentStopped(model);
      } catch (NoSuchInterfaceException e) {
      } catch (IllegalLifeCycleException e) {
        throw new IllegalOperationException (
          "Cannot stop this instance");
      }
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the VetoableConfigurationListener interface
  // -------------------------------------------------------------------------

  public void canChangeRootComponent () {
    // nothing to do (no veto)
  }

  public void canChangeName (final Component component) {
    // nothing to do (no veto)
  }

  public void canChangeType (final Component component) {
    // nothing to do (no veto)
  }

  public void canChangeImplementation (final Component component) {
    org.objectweb.fractal.api.Component ci = getInstance(component);
    if (ci != null) {
//      throw new IllegalOperationException(
      avert ("Cannot change the implementation of an instanciated component");
    }
  }

  public void canChangeInterfaceName (final Interface i) {
    org.objectweb.fractal.api.Component ci = getInstance(i.getOwner());
    if (ci != null) {
//      throw new IllegalOperationException(
      avert ("Cannot change the name of an interface of an instanciated component");
    }
  }

  public void canChangeInterfaceSignature (final Interface i) {
    org.objectweb.fractal.api.Component ci = getInstance(i.getOwner());
    if (ci != null) {
//      throw new IllegalOperationException(
      avert ("Cannot change the signature of an interface of an instanciated component");
    }
  }

  public void canChangeInterfaceContingency (final Interface i) {
    org.objectweb.fractal.api.Component ci = getInstance(i.getOwner());
    if (ci != null) {
//      throw new IllegalOperationException(
      avert ("Cannot change the contingency of an interface of an instanciated component");
    }
  }

  public void canChangeInterfaceCardinality (final Interface i) {
    org.objectweb.fractal.api.Component ci = getInstance(i.getOwner());
    if (ci != null) {
//      throw new IllegalOperationException(
      avert ("Cannot change the cardinality of an interface of an instanciated component");
    }
  }

  public void canAddClientInterface (
    final Component component,
    final ClientInterface i)
  {
    org.objectweb.fractal.api.Component ci = getInstance(component);
    if (ci != null) {
      if (i.getMasterCollectionInterface() == null) {
//      throw new IllegalOperationException(
        avert ("Cannot add an interface to an instanciated component");
      }
    }
  }

  public void canRemoveClientInterface (
    final Component component,
    final ClientInterface i)
  {
    org.objectweb.fractal.api.Component ci = getInstance(component);
    if (ci != null) {
      if (i.getMasterCollectionInterface() == null) {
//      throw new IllegalOperationException(
        avert ("Cannot remove an interface from an instanciated component");
      }
    }
  }

  public void canAddServerInterface (
    final Component component,
    final ServerInterface i)
  {
    org.objectweb.fractal.api.Component ci = getInstance(component);
    if (ci != null) {
      if (i.getMasterCollectionInterface() == null) {
//      throw new IllegalOperationException(
        avert ("Cannot add an interface to an instanciated component");
      }
    }
  }

  public void canRemoveServerInterface (
    final Component component,
    final ServerInterface i)
  {
    org.objectweb.fractal.api.Component ci = getInstance(component);
    if (ci != null) {
      if (i.getMasterCollectionInterface() == null) {
//      throw new IllegalOperationException(
        avert ("Cannot remove an interface from an instanciated component");
      }
    }
  }

  public void canBindInterface (final ClientInterface citf) {
    if (isStarted(citf.getOwner())) {
//      throw new IllegalOperationException(
      // avert ("Cannot bind a client interface of a started component");
    }
  }

  public void canRebindInterface (final ClientInterface citf) {
    if (isStarted(citf.getOwner())) {
//      throw new IllegalOperationException(
      avert ("Cannot rebind a client interface of a started component");
    }
  }

  public void canUnbindInterface (final ClientInterface citf) {
    if (isStarted(citf.getOwner())) {
//      throw new IllegalOperationException(
      avert ("Cannot unbind a client interface of a started component");
    }
  }

  public void canChangeAttributeController (final Component component) {
    org.objectweb.fractal.api.Component ci = getInstance(component);
    if (ci != null) {
//      throw new IllegalOperationException(
      avert ("Cannot change the attribute controller "
        +"of an instanciated component");
    }
  }

  public void canChangeAttribute (
    final Component component,
    final String attributeName)
  {
    // nothing to do (no veto)
  }

  public void canChangeTemplateControllerDescriptor (
    final Component component)
  {
    org.objectweb.fractal.api.Component ci = getInstance(component);
    if (ci != null) {
//      throw new IllegalOperationException(
      avert ("Cannot change the template controller descriptor"
        +" of an instanciated component");
    }
  }

  public void canChangeComponentControllerDescriptor (
    final Component component)
  {
    org.objectweb.fractal.api.Component ci = getInstance(component);
    if (ci != null) {
//      throw new IllegalOperationException(
      avert ("Cannot change the controller descriptor "
      +"of an instanciated component");
    }
  }

  public void canAddSubComponent (
    final Component parent,
    final Component child)
  {
    if (isStarted(parent) && getInstance(child) != null) {
//      throw new IllegalOperationException(
      avert ("Cannot add an instantiated sub component in a started component");
    }
  }

  public void canRemoveSubComponent (
    final Component parent,
    final Component child)
  {
    if (isStarted(parent) && getInstance(child) != null) {
//      throw new IllegalOperationException(
      avert ("Cannot remove an instantiated sub component from a started component");
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the ConfigurationListener interface
  // -------------------------------------------------------------------------

  public void changeCountChanged (final Component component, long changeCount) {
    // does nothing
  }

  public void rootComponentChanged (final Component oldValue) {
    // does nothing
  }

  public void nameChanged (final Component component, final String oldValue) {
    org.objectweb.fractal.api.Component ci = getInstance(component);
    if (ci != null) {
      try {
        NameController nc = Fractal.getNameController(ci);
        nc.setFcName(component.getName());
      } catch (NoSuchInterfaceException e) {
        avert (e.getMessage());
      }
    }
  }

  public void typeChanged (final Component component, final String oldValue) {
    // does nothing
  }

  public void implementationChanged (
    final Component component,
    final String oldValue)
  {
    // nothing to do (should never be called, because of veto)
  }

  public void interfaceNameChanged (final Interface i, final String oldValue) {
    // nothing to do (should never be called, because of veto)
  }

  public void interfaceSignatureChanged (
    final Interface i,
    final String oldValue)
  {
    // nothing to do (should never be called, because of veto)
  }

  public void interfaceContingencyChanged (
    final Interface i,
    final boolean oldValue)
  {
    // nothing to do (should never be called, because of veto)
  }

  public void interfaceCardinalityChanged (
    final Interface i,
    final boolean oldValue)
  {
    // nothing to do (should never be called, because of veto)
  }

  public void clientInterfaceAdded (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    // only called for collection interface
    Binding b = i.getBinding();
    if (b != null) {
      interfaceBound(i, b.getServerInterface());
    }
  }

  public void clientInterfaceRemoved (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    // only called for collection interface
    Binding b = i.getBinding();
    if (b != null) {
      interfaceUnbound(i, b.getServerInterface());
    }
  }

  public void serverInterfaceAdded (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    // nothing to do (should never be called, because of veto)
  }

  public void serverInterfaceRemoved (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    // nothing to do (should never be called, because of veto)
  }

  public void interfaceBound (
    final ClientInterface citf,
    final ServerInterface sitf)
  {
    org.objectweb.fractal.api.Component ci = getInstance(citf.getOwner());
    org.objectweb.fractal.api.Component si = getInstance(sitf.getOwner());
    if (ci != null && si != null) {
      BindingController bc;
      try {
        bc = Fractal.getBindingController(ci);
        Object itf;
        if (sitf.isInternal()) {
          itf = Fractal.getContentController(si).getFcInternalInterface(sitf.getName());
        } else {
          itf = si.getFcInterface(sitf.getName());
        }
        bc.bindFc (citf.getName(), itf);
      } catch (Exception e) {
        avert (e.getMessage());
        return;
      }
    }
  }

  public void interfaceRebound (
    final ClientInterface citf,
    final ServerInterface oldSitf)
  {
    org.objectweb.fractal.api.Component ci = getInstance(citf.getOwner());
    interfaceUnbound(citf, oldSitf);
    interfaceBound(citf, citf.getBinding().getServerInterface());
  }

  public void interfaceUnbound (
    final ClientInterface citf,
    final ServerInterface sitf)
  {
    org.objectweb.fractal.api.Component ci = getInstance(citf.getOwner());
    org.objectweb.fractal.api.Component si = getInstance(sitf.getOwner());
    if (ci != null && si != null) {
      BindingController bc;
      try {
        bc = Fractal.getBindingController(ci);
        bc.unbindFc (citf.getName());
      } catch (Exception e) {
        avert (e.getMessage());
      }
    }
  }

  public void attributeControllerChanged (
    final Component component,
    final String oldValue)
  {
    // nothing to do (should never be called, because of veto)
  }

  public void attributeChanged (
    final Component component,
    final String attributeName,
    final String oldValue)
  {
    org.objectweb.fractal.api.Component ci = getInstance(component);
    if (ci != null) {
      try {
        AttributeController ac = (AttributeController)ci.getFcInterface(
          "attribute-controller");
        if (ac != null) {
          Class acc = ac.getClass();

          String attrName = Character.toUpperCase(attributeName.charAt(0)) + attributeName.substring(1);
          String getterName = "get" + attrName;
          String setterName = "set" + attrName;
          Method getter = acc.getMethod (getterName, new Class[0]);
          Method setter = acc.getMethod (setterName, new Class[] {
            getter.getReturnType()
          });

          Class attrType = getter.getReturnType();
          String v = component.getAttribute(attributeName);
          Object value;
          if (attrType.equals(String.class)) {
            value = v;
          } else if (attrType.isPrimitive()) {
            if (attrType.equals(Integer.TYPE)) {
              value = Integer.valueOf(v);
            } else if (attrType.equals(Long.TYPE)) {
              value = Long.valueOf(v);
            } else if (attrType.equals(Float.TYPE)) {
              value = Float.valueOf(v);
            } else if (attrType.equals(Double.TYPE)) {
              value = Double.valueOf(v);
            } else if (attrType.equals(Byte.TYPE)) {
              value = Byte.valueOf(v);
            } else if (attrType.equals(Character.TYPE)) {
              if (v.length() != 1) {
                avert ("Bad char value: " + v);
                return;
              }
              value = new Character(v.charAt(0));
            } else if (attrType.equals(Short.TYPE)) {
              value = Short.valueOf(v);
            } else if (attrType.equals(Boolean.TYPE)) {
              if (!v.equals("true") &&
                !v.equals("false"))
              {
                avert ("Bad boolean value: " + v);
                return;
              }
              value = new Boolean(v.equals("true"));
            } else {
              avert ("Unexpected case");
              return;
            }
          } else {
            avert ("Unsupported attribute type: " + attrType);
            return;
          }
          setter.invoke(ac, new Object[]{value});
        }
      } catch (Exception ex) {
        avert (ex.getMessage());
     }
    }
  }

  public void templateControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    // nothing to do (should never be called, because of veto)
  }

  public void componentControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    // nothing to do (should never be called, because of veto)
  }

  public void subComponentAdded (
    final Component parent,
    final Component child,
    final int index)
  {
    org.objectweb.fractal.api.Component pi = getInstance(parent);
    org.objectweb.fractal.api.Component ci = getInstance(child);
    if (pi != null && ci != null) {
      try {
        ContentController cc = Fractal.getContentController(pi);
        cc.addFcSubComponent(ci);
        List itfs = child.getClientInterfaces();
        for (int i = 0; i < itfs.size(); ++i) {
          Binding b = ((ClientInterface)itfs.get(i)).getBinding();
          if (b != null) {
            addBinding(b, parent, child);
          }
        }
        itfs = child.getServerInterfaces();
        for (int i = 0; i < itfs.size(); ++i) {
          List bs = ((ServerInterface)itfs.get(i)).getBindings();
          for (int j = 0; j < bs.size(); ++j) {
            Binding b = (Binding)bs.get(j);
            addBinding(b, parent, child);
          }
        }
      } catch (Exception e) {
        avert(e.getMessage());
      }
    }
  }
  
  private void addBinding (Binding b, Component parent, Component child) throws Exception {
    Component c = b.getClientInterface().getOwner();
    org.objectweb.fractal.api.Component ci = getInstance(c);
    String citf = b.getClientInterface().getName();
    Component s = b.getServerInterface().getOwner();
    if ((child == s || parent == s || parent == s.getParent()) && 
        (child == c || parent == c || parent == c.getParent())) 
    {
      org.objectweb.fractal.api.Component si = getInstance(s);
      Object sitf;
      if (b.getServerInterface().isInternal()) {
        sitf = Fractal.getContentController(si).getFcInternalInterface(
            b.getServerInterface().getName());
      } else {
        sitf = si.getFcInterface(b.getServerInterface().getName());
      }
      Fractal.getBindingController(ci).bindFc(citf, sitf);
    }
  }
  
  public void subComponentRemoved (
    final Component parent,
    final Component child,
    final int index)
  {
    org.objectweb.fractal.api.Component pi = getInstance(parent);
    org.objectweb.fractal.api.Component ci = (org.objectweb.fractal.api.Component)instances.get(child);
    if (pi != null && ci != null) {
      try {
        List itfs = child.getClientInterfaces();
        for (int i = 0; i < itfs.size(); ++i) {
          Binding b = ((ClientInterface)itfs.get(i)).getBinding();
          if (b != null) {
            removeBinding(b, parent, child);
          }
        }
        itfs = child.getServerInterfaces();
        for (int i = 0; i < itfs.size(); ++i) {
          List bs = ((ServerInterface)itfs.get(i)).getBindings();
          for (int j = 0; j < bs.size(); ++j) {
            Binding b = (Binding)bs.get(j);
            removeBinding(b, parent, child);
          }
        }
        
        ContentController cc;
        cc = Fractal.getContentController(pi);
        cc.removeFcSubComponent(ci);
      } catch (Exception e) {
        e.printStackTrace();
        avert (e.getMessage());
      }
    }
  }
  
  private void removeBinding (Binding b, Component parent, Component child) throws Exception {
    Component c = b.getClientInterface().getOwner();
    org.objectweb.fractal.api.Component ci = 
      (org.objectweb.fractal.api.Component)instances.get(c);
    Component s = b.getServerInterface().getOwner();
    if ((child == s || parent == s || parent == s.getParent()) && 
        (child == c || parent == c || parent == c.getParent())) 
    {
      Fractal.getBindingController(ci).unbindFc(b.getClientInterface().getName());
    }
  }
  
  private void avert (String motif) {
    JOptionPane.showMessageDialog (
      null, motif, "Error", JOptionPane.ERROR_MESSAGE);
    throw new IllegalOperationException(motif);
  }

/**
 * This subclass asks for a method name.
 */

  /**
   * Font Arial, plain, 13 pts
   */
  static public final Font  fnta13 = new Font ("Arial", Font.PLAIN, 13);

  class FrameChoiceBox extends JDialog implements ActionListener {
	  JPanel panel = new JPanel();
    JButton [] unit;

	  protected void processWindowEvent(WindowEvent e) {
		  if (e.getID() == WindowEvent.WINDOW_CLOSING) { cancel(); }
		  super.processWindowEvent(e);
	  }

	  void cancel() { range = -1; dispose(); }

	  public void actionPerformed (ActionEvent e)	{
      range = -1;
      for (int i = 0; i < unit.length; i++) {
        if (e.getActionCommand().equals(unit[i].getText())) {
          range = i; break;
        }
      }
      dispose();
	  }

	  public FrameChoiceBox (String titre, String [] items)	{
		  unit = new JButton [items.length];
		  enableEvents (AWTEvent.WINDOW_EVENT_MASK);
		  Dimension dim = new Dimension (300, 20+items.length*25);

		  try {
			  this.setTitle(titre);
			  Container cp = getContentPane ();
			  cp.setLayout(new BoxLayout(cp, BoxLayout.Y_AXIS));
			  setResizable(true);

			  Color curcol = Color.lightGray;
			  for (int i = 0; i < items.length; i++) {
				  unit[i] = new JButton (items[i]);
				  unit[i].setPreferredSize(new Dimension(150, 20));
				  unit[i].setFont(fnta13);
				  FontMetrics fm = unit[i].getFontMetrics(fnta13);
				  int d = 0;
				  for (int z = 0; z < items[i].length(); z++)
                d = d + fm.charWidth(items[i].charAt(z));
				  unit[i].setBorder(new EmptyBorder (2, 1, 2, 70-d));
				  unit[i].setBackground(curcol);
				  unit[i].setForeground(Color.black);
				  unit[i].addActionListener(this);
				  cp.add(unit[i]);
			  }
			  setModal(true);
			  this.setSize(dim);
			  this.setLocation(300, 300);
			  pack ();
			  show ();
		  }
		  catch(Exception e) { e.printStackTrace(); }
	  }
  }
// --------------------------
}
