/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.status.view;

import org.objectweb.fractal.gui.graph.view.ComponentPart;
import org.objectweb.fractal.gui.graph.view.GraphViewListener;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Interface;
import org.objectweb.fractal.swing.JPanelImpl;
import org.objectweb.fractal.gui.Constants;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * A configuration view that displays the status of the current component or
 * interface. The "current" component or interface is the component or interface
 * above which the cursor is. In order to know this component or interface, this
 * component listens to a graph view.
 */

public class BasicStatusView extends JPanelImpl implements GraphViewListener {

  /**
   * Font used to draw the captions.
   */

  private final static Font CAPTION_FONT =
    new Font("Arial", Font.BOLD, 12);

  /**
   * Font used to draw the status of components and interfaces.
   */

  private final static Font STATUS_FONT =
    new Font("courier", Font.PLAIN, 12);

  /**
   * The label that displays the current component's name.
   */

  private JLabel componentName;

  /**
   * The label that displays the current component's type.
   */

  private JLabel componentType;

  /**
   * The label that displays the current interface's name.
   */

  private JLabel interfaceName;

  /**
   * The label that displays the current interface's type.
   */

  private JLabel interfaceType;

  /**
   * The configuration status to be dispayed, if any
   */

  private JLabel statusstring;

  /**
   * The execution directory to be dispayed, if any
   */

  private JLabel jlabexec;

  /**
   * "Empty" strings for display in order to keep a minimum size.
   */

  private String EMPTY = "<EMPTY>          ";
  private String WHITE = "                 ";

  private Color color = Color.black;

  /**
   * Constructs a new {@link BasicStatusView} component.
   */

  public BasicStatusView () {
    color = new Color (0, 0, 64);

    // TODO remove all hardcoded coordinates, font sizes, ...
    
    JLabel labcomponentName = createLabel("Component name:",
      0, 110, CAPTION_FONT);
    JLabel labcomponentType = createLabel("component type:",
      0, 110, CAPTION_FONT);
    JLabel labinterfaceName = createLabel("Interface name:",
      0, 110, CAPTION_FONT);
    JLabel labinterfaceType = createLabel("Interface type:",
      0, 110, CAPTION_FONT);
    JLabel labstatusstring  = createLabel("Status:",
      0, 110, CAPTION_FONT);

    color = Color.black;
    componentName = createLabel(EMPTY, 0, 170, STATUS_FONT);
    componentType = createLabel(EMPTY, 0, 400, STATUS_FONT);
    interfaceName = createLabel(EMPTY, 0, 170, STATUS_FONT);
    interfaceType = createLabel(EMPTY, 0, 400, STATUS_FONT);
    color = new Color (64, 0, 0);
    jlabexec = createLabel("      ", 0, 550, STATUS_FONT);

    setLayout(null);
    setPreferredSize (new Dimension (800, 40));
    setInLayout (10, 0, 120, 20, labcomponentName);
    setInLayout (10, 20, 120, 20, labinterfaceName);

    setInLayout (135, 0, 200, 20, componentName);
    setInLayout (135, 20, 200, 20, interfaceName);
    setInLayout (135, 40, 560, 20, jlabexec);

    setInLayout (310, 0, 120, 20, labcomponentType);
    setInLayout (310, 20, 120, 20, labinterfaceType);

    setInLayout (435, 0, 460, 20, componentType);
    setInLayout (435, 20, 460, 20, interfaceType);
  }

  // -------------------------------------------------------------------------
  // Implementation of the GraphViewListener interface
  // -------------------------------------------------------------------------

  public void viewChanged () {
    // does nothing
  }

  public void mousePressed (final MouseEvent e, final ComponentPart p) {
    // does nothing
  }

  public void mouseReleased (final MouseEvent e, final ComponentPart p) {
    // does nothing
  }

  public void mouseClicked (final MouseEvent e, final ComponentPart p) {
    // does nothing
  }

  public void mouseEntered (final MouseEvent e) {
    // does nothing
  }

  public void mouseExited (final MouseEvent e) {
    // does nothing
  }

  public void mouseDragged (final MouseEvent e) {
    // does nothing
  }

  public void mouseMoved (final MouseEvent e, final ComponentPart p) {
    if (p == null) {
      componentName.setText(EMPTY);
      componentType.setText(EMPTY);
      interfaceName.setText(EMPTY);
      interfaceType.setText(EMPTY);
    } else {
      Component c = p.getComponent();
      componentName.setText(
        c == null || c.getName().length() == 0 ? EMPTY : c.getName());
      componentType.setText(
        c == null || c.getType().length() == 0 ? EMPTY : c.getType());

      Interface i = p.getInterface();
      interfaceName.setText(
        i == null || i.getName().length() == 0? EMPTY : i.getName());
      interfaceType.setText(
        i == null || i.getSignature().length() == 0 ? EMPTY : i.getSignature());
    }
    ajuste (componentName);
    ajuste (interfaceName);
    ajuste (componentType);
    ajuste (interfaceType);
  }

  private void ajuste (JLabel lb) {
    lb.setForeground(Color.black);
    if (lb.getText().equals(EMPTY)) {
      lb.setForeground(Constants.WARNING_COLOR);
    } else {
      String st = lb.getText();
      if (st.length() < EMPTY.length()) {
        lb.setText(st+WHITE.substring(st.length()));
      }
    }
  }

  // -------------------------------------------------------------------------
  // Other methods
  // -------------------------------------------------------------------------

  /**
   * Creates a new Swing label for this view component.
   *
   * @param label the value of the label to be created.
   * @return the newly created label.
   */

  private JLabel createLabel (final String label, int thickness, int minwidth,
    Font f) {
    JLabel l = new JLabel(label);
    l.setFont(f);
    l.setForeground(color);
    l.setBorder(BorderFactory.createLineBorder(Color.gray, thickness));
    l.setMinimumSize(new Dimension (minwidth, 20));
    return l;
  }

	private void setInLayout (int x, int y, int w, int h, java.awt.Component c) {
		c.setBounds(x, y, w, h);
		add(c);
	}
}
