/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.dialog.model;

import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Interface;

import javax.swing.table.AbstractTableModel;

/**
 * A {@link javax.swing.table.TableModel} based on a {@link Component} model
 * that represents interfaces names, signatures... This model makes a conversion
 * from a {@link Component} model to a {@link javax.swing.table.TableModel}.
 */

public class InterfaceTableModel extends AbstractTableModel {

  /**
   * If this model represents client interfaces or server interfaces.
   */

  private boolean isClient;

  /**
   * The component model on which this model is based.
   */

  private Component model;

  /**
   * Constructs a new {@link InterfaceTableModel}.
   *
   * @param isClient if this model must represent client or server interfaces.
   */

  InterfaceTableModel (final boolean isClient) {
    this.isClient = isClient;
  }

  /**
   * Sets the component model on which this model is based.
   *
   * @param model a component.
   */

  void setComponentModel (final Component model) {
    this.model = model;
    fireTableDataChanged();
  }

  /**
   * Notifies this listener that an interface has changed.
   *
   * @param i the interface that has changed.
   */

  void interfaceChanged (final Interface i) {
    final int index;
    if (isClient) {
      index = i.getOwner().getClientInterfaces().indexOf(i);
    } else {
      index = i.getOwner().getServerInterfaces().indexOf(i);
    }
    if (index != -1) {
      fireTableRowsUpdated(index, index);
    }
  }

  /**
   * Notifies this listener that an interface has been added.
   *
   * @param i the interface that has been added.
   * @param index the index of this interface in the client or server interface
   *      list of the component.
   */

  void interfaceAdded (final Interface i, final int index) {
    fireTableRowsInserted(index, index);
  }

  /**
   * Notifies this listener that an interface has been removed.
   *
   * @param i the interface that has been removed.
   * @param index the index of this interface in the client or server interface
   *      list of the component.
   */

  void interfaceRemoved (final Interface i, final int index) {
    fireTableRowsDeleted(index, index);
  }

  // -------------------------------------------------------------------------
  // Implementation of the TableModel interface
  // -------------------------------------------------------------------------

  public int getRowCount () {
    if (model == null) {
      return 0;
    }
    if (isClient) {
      return model.getClientInterfaces().size();
    } else {
      return model.getServerInterfaces().size();
    }
  }

  public int getColumnCount () {
    return 4;
  }

  public String getColumnName (final int column) {
    switch (column) {
      case 0:
        return "Name";
      case 1:
        return "Signature";
      case 2:
        return "Contingency";
      default:
        return "Cardinality";
    }
  }

  public boolean isCellEditable (final int rowIndex, final int columnIndex) {
    Interface i;
    if (isClient) {
      i = (Interface)model.getClientInterfaces().get(rowIndex);
    } else {
      i = (Interface)model.getServerInterfaces().get(rowIndex);
    }
    return i.getMasterCollectionInterface() == null;
  }

  public Object getValueAt (final int rowIndex, final int columnIndex) {
    Interface i;
    if (isClient) {
      i = (Interface)model.getClientInterfaces().get(rowIndex);
    } else {
      i = (Interface)model.getServerInterfaces().get(rowIndex);
    }
    switch (columnIndex) {
      case 0:
        return i.getName();
      case 1:
        return i.getSignature();
      case 2:
        return i.isOptional() ? "optional" : "mandatory";
      case 3:
        return i.isCollection() ? "collection" : "single";
      default:
        return i;
    }
  }

  public void setValueAt (
    final Object aValue,
    final int rowIndex,
    final int columnIndex)
  {
    Interface i;
    if (isClient) {
      i = (Interface)model.getClientInterfaces().get(rowIndex);
    } else {
      i = (Interface)model.getServerInterfaces().get(rowIndex);
    }
    switch (columnIndex) {
      case 0:
        i.setName((String)aValue);
        break;
      case 1:
        i.setSignature((String)aValue);
        break;
      case 2:
        i.setIsOptional(aValue.equals("optional"));  // catcher exception Joptionpane
        break;
      default:
        i.setIsCollection(aValue.equals("collection"));
    }
  }
}
