/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.model;

import org.objectweb.fractal.api.control.BindingController;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

/**
 * A {@link DisplayListener} that notifies other {@link DisplayListener}s.
 */

public class DisplayNotifier implements DisplayListener, BindingController {

  /**
   * A collection client interface bound to the {@link DisplayListener
   * listeners} of this component.
   */

  public final static String DISPLAY_LISTENERS_BINDING = "display-listeners";

  /**
   * The listeners client interface.
   */

  private Map displayListeners;

  /**
   * Constructs a new {@link DisplayNotifier} component.
   */

  public DisplayNotifier () {
    displayListeners = new HashMap();
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return (String[])displayListeners.keySet().toArray(
      new String[displayListeners.size()]);
  }

  public Object lookupFc (final String clientItfName) {
    if (clientItfName.startsWith(DISPLAY_LISTENERS_BINDING)) {
      return displayListeners.get(clientItfName);
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (clientItfName.startsWith(DISPLAY_LISTENERS_BINDING)) {
      displayListeners.put(clientItfName, serverItf);
    }
  }

  public void unbindFc (final String clientItfName) {
    if (clientItfName.startsWith(DISPLAY_LISTENERS_BINDING)) {
      displayListeners.remove(clientItfName);
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the DisplayListener interface
  // -------------------------------------------------------------------------

  public void displayedAreaChanged (final Rect oldValue) {
    Iterator i = displayListeners.values().iterator();
    while (i.hasNext()) {
      ((DisplayListener)i.next()).displayedAreaChanged(oldValue);
    }
  }

  public void antialiasingChanged () {
    Iterator i = displayListeners.values().iterator();
    while (i.hasNext()) {
      ((DisplayListener)i.next()).antialiasingChanged();
    }
  }

  public void maxDepthChanged () {
    Iterator i = displayListeners.values().iterator();
    while (i.hasNext()) {
      ((DisplayListener)i.next()).maxDepthChanged();
    }
  }

  public void itfNameDisplayModeChanged () {
    Iterator i = displayListeners.values().iterator();
    while (i.hasNext()) {
      ((DisplayListener)i.next()).itfNameDisplayModeChanged();
    }
  }
}

