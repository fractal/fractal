/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.model;

/**
 * Basic implementation of the {@link ClientInterface} interface.
 */

public class BasicClientInterface extends BasicInterface
  implements ClientInterface
{

  /**
   * The binding corresponding to this client interface, or <tt>null</tt> if
   * this interface is not bound.
   */

  private Binding binding;

  /**
   * Constructs a new external client interface.
   *
   * @param owner the component that will own this interface.
   */

  BasicClientInterface (final BasicComponent owner) {
    super(owner);
    complementaryItf = new BasicServerInterface(this);
  }

  /**
   * Constructs a new internal client interface.
   *
   * @param externalItf the complementary interface of this interface.
   */

  BasicClientInterface (final ServerInterface externalItf) {
    super(externalItf);
  }

  /**
   * Constructs a new slave, collection client interface.
   *
   * @param rootCollectionItf a master collection client interface.
   */

  BasicClientInterface (final ClientInterface rootCollectionItf, final String suffix) {
    super(rootCollectionItf, 0);
    if (suffix != null) {
      this.name = suffix;
    }
    complementaryItf = new BasicServerInterface(this);
    ((BasicInterface)rootCollectionItf).addSlaveCollectionInterface(this);
  }

  public Binding getBinding () {
    return binding;
  }

  /**
   * Sets the binding that refer to this client inteface.
   *
   * @param binding the new binding that refer to this client inteface.
   * @see #getBinding
   */

  void setBinding (final Binding binding) {
    this.binding = binding;
  }
}
