/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.menu.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.graph.model.Display;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Configuration;
import org.objectweb.fractal.gui.graph.view.Printer;
import org.objectweb.fractal.gui.selection.model.Selection;
import org.objectweb.fractal.swing.AbstractAction;

import java.awt.event.ActionEvent;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.KeyStroke;

/**
 * An action to export a configuration view to an SVG file.
 */

public class ExportSVGAction extends AbstractAction implements
  BindingController
{

  public final static String CONFIGURATION_BINDING = "configuration";

  public final static String SELECTION_BINDING = "selection";

  public final static String DISPLAY_BINDING = "display";

  public final static String PRINT_BINDING = "print";

  private Configuration configuration;

  private Selection selection;

  private Display display;

  private Printer printer;

  /**
   * Constructs a new {@link PrintAction} component.
   */

  public ExportSVGAction () {
    putValue(NAME, "ExportSVG");
    putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("control 2"));
    putValue(SHORT_DESCRIPTION, "Export to SVG");
    URL url = getClass().getResource(
      "/org/objectweb/fractal/gui/resources/export.gif");
    putValue(SMALL_ICON, new ImageIcon(url));
    setEnabled(true);
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] {
      CONFIGURATION_BINDING,
      SELECTION_BINDING,
      DISPLAY_BINDING,
      PRINT_BINDING
    };
  }

  public Object lookupFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      return configuration;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      return selection;
    } else if (DISPLAY_BINDING.equals(clientItfName)) {
      return display;
    } else if (PRINT_BINDING.equals(clientItfName)) {
      return printer;
    }
    return null;
  }

  public void bindFc (final String clientItfName, final Object serverItf) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = (Configuration)serverItf;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = (Selection)serverItf;
    } else if (DISPLAY_BINDING.equals(clientItfName)) {
      display = (Display)serverItf;
    } else if (PRINT_BINDING.equals(clientItfName)) {
      printer = (Printer)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = null;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = null;
    } else if (DISPLAY_BINDING.equals(clientItfName)) {
      display = null;
    } else if (PRINT_BINDING.equals(clientItfName)) {
      printer = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the ActionListener interface
  // -------------------------------------------------------------------------

  public void actionPerformed (final ActionEvent e) {
    Object o = selection.getSelection();
    if (o instanceof Component) {
      Component c = (Component)o;
      printer.setType(Printer.TYPE_EXPORT);
      printer.print(true, c);
    }
  }
}
