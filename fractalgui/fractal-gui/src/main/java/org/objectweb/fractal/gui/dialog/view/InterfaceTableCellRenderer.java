/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.dialog.view;

import org.objectweb.fractal.gui.Constants;
import org.objectweb.fractal.gui.model.Interface;

import java.awt.Color;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * A {@link javax.swing.table.TableCellRenderer} to draw interfaces with a
 * color indicating their status.
 */

public class InterfaceTableCellRenderer extends DefaultTableCellRenderer {

  public java.awt.Component getTableCellRendererComponent (
    final JTable table,
    final Object value,
    final boolean isSelected,
    final boolean hasFocus,
    final int row,
    final int column)
  {
    super.getTableCellRendererComponent(
      table, value, isSelected, hasFocus, row, column);
    Interface itf = (Interface)table.getModel().getValueAt(row, -1);
    long status = itf.getStatus();
    if (itf.getMasterCollectionInterface() != null) {
      setForeground(Color.gray);
    } else if ((status & Interface.MANDATORY_INTERFACE_NOT_BOUND) != 0) {
      setForeground(Constants.ERROR_COLOR);
    } else if ((status & (Interface.NAME_MISSING)) != 0 && column == 0) {
      setForeground(Constants.ERROR_COLOR);
      setText("<empty>");
    } else if ((status & (Interface.NAME_ALREADY_USED)) != 0 && column == 0) {
      setForeground(Constants.ERROR_COLOR);
    } else if ((status & (Interface.SIGNATURE_MISSING)) != 0 && column == 1) {
      setForeground(Constants.ERROR_COLOR);
      setText("<empty>");
    } else if ((status & (Interface.SIGNATURE_CLASS_NOT_FOUND)) != 0
               && column == 1)
    {
      setForeground(Constants.ERROR_COLOR);
    } else {
      setForeground(Color.black);
    }
    return this;
  }
}
