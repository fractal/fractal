/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.menu.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.model.Configuration;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.graph.model.GraphModel;
import org.objectweb.fractal.gui.repository.api.Repository;
import org.objectweb.fractal.gui.repository.api.Storage;
import org.objectweb.fractal.gui.selection.model.Selection;
import org.objectweb.fractal.swing.WaitGlassPane;
import org.objectweb.fractal.swing.AbstractAction;

import java.net.URL;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.KeyStroke;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;

/**
 * An action to import a configuration stored in a repository.
 */

public class ImportAction extends AbstractAction implements BindingController {

  /**
   * A mandatory client interface bound to a {@link Configuration configuration}
   * model. This is the configuration into which the imported configurations are
   * loaded.
   */

  public final static String CONFIGURATION_BINDING = "configuration";

  /**
   * An optional client interface bound to a {@link GraphModel graph} model.
   * This is the model into which the imported configuration graphical
   * information are loaded.
   */

  public final static String GRAPH_BINDING = "graph";

  /**
   * A mandatory client interface bound to a {@link Selection selection} model.
   */

  public final static String SELECTION_BINDING = "selection";

  /**
   * A mandatory client interface bound to a {@link Repository repository}. This
   * repository is used to load the configurations stored in the storage.
   */

  public final static String REPOSITORY_BINDING = "repository";

  /**
   * A mandatory client interface bound to a {@link Storage storage}. This is
   * the storage from which the imported configurations are read.
   */

  public final static String STORAGE_BINDING = "storage";

  /**
   * The configuration client interface.
   */

  private Configuration configuration;

  /**
   * The graph client interface.
   */

  private GraphModel graph;

  /**
   * The selection client interface.
   */

  private Selection selection;

  /**
   * The repository client interface.
   */

  private Repository repository;

  /**
   * The storage client interface.
   */

  private Storage storage;

  /**
   * Constructs a new {@link OpenAction} component.
   */

  public ImportAction () {
    putValue(NAME, "Import");
    putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("control I"));
    putValue(SHORT_DESCRIPTION, "Import");
    URL url = getClass().getResource(
      "/org/objectweb/fractal/gui/resources/fileopen.gif");
    putValue(SMALL_ICON, new ImageIcon(url));
  }

  // -------------------------------------------------------------------------
  // Implementation of the UserBindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] {
      CONFIGURATION_BINDING,
      GRAPH_BINDING,
      SELECTION_BINDING,
      REPOSITORY_BINDING,
      STORAGE_BINDING
    };
  }

  public Object lookupFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      return configuration;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      return graph;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      return selection;
    } else if (REPOSITORY_BINDING.equals(clientItfName)) {
      return repository;
    } else if (STORAGE_BINDING.equals(clientItfName)) {
      return storage;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = (Configuration)serverItf;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      graph = (GraphModel)serverItf;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = (Selection)serverItf;
    } else if (REPOSITORY_BINDING.equals(clientItfName)) {
      repository = (Repository)serverItf;
    } else if (STORAGE_BINDING.equals(clientItfName)) {
      storage = (Storage)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = null;
    } else if (GRAPH_BINDING.equals(clientItfName)) {
      graph = null;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = null;
    } else if (REPOSITORY_BINDING.equals(clientItfName)) {
      repository = null;
    } else if (STORAGE_BINDING.equals(clientItfName)) {
      storage = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the ActionListener interface
  // -------------------------------------------------------------------------

  public void actionPerformed (final ActionEvent e) {
    try {
      if (!(selection.getSelection() instanceof Component)) {
        JOptionPane.showMessageDialog(
          null,
          "A component must be selected, to which the imported component will be added",
          "Error",
          JOptionPane.ERROR_MESSAGE);
        return;
      }
      File storage = null;
      if (configuration.getStorage() != null) {
        storage = new File(configuration.getStorage());
        if (!storage.exists() || !storage.isDirectory()) {
          storage = null;
        }
      }
      if (storage == null) {
        JOptionPane.showMessageDialog(
          null,
          "A storage directory must be selected before files can be imported", 
          "Error",
          JOptionPane.ERROR_MESSAGE);
        return;
      }
      
      JFileChooser fileChooser = new JFileChooser();
      fileChooser.addChoosableFileFilter(
        new SimpleFileFilter("fractal", "Fractal ADL files"));
      if (fileChooser.showOpenDialog(null) != JFileChooser.APPROVE_OPTION) {
        return;
      }
      File f = fileChooser.getSelectedFile();

      File p = f;
      String name = f.getName().substring(0, f.getName().indexOf('.'));
      while (p.getParentFile() != null && !p.getParentFile().equals(storage)) {
        name = p.getParentFile().getName() + "." + name;
        p = p.getParentFile();
      }
      if (!storage.equals(p.getParentFile())) {
        JOptionPane.showMessageDialog(
          null,
          "Cannot open a file which is not in the storage directory. " +
          "Change the storage directory first.", 
          "Error",
          JOptionPane.ERROR_MESSAGE);
        return;
      }

      new Thread(new Import(e, storage, name)).start();
    } catch (Exception ignored) {
      ignored.printStackTrace();
    }
  }

  /**
   * A runnable action to open a configuration. This action must be performed
   * in a thread separated from the Swing event handler thread, otherwise the
   * wait cursor is not displayed during the action.
   */

  class Import implements Runnable {

    /**
     * The root pane of the frame into which the wait cursor must be displayed.
     */

    private JRootPane rootPane;

    /**
     * The storage that must opened.
     */

    private File storage;
    
    /**
     * The definition that must be opened. 
     */
    
    private String name;

    /**
     * Constructs a new {@link Import} object.
     *
     * @param e the event that triggered this action.
     */

    public Import (final ActionEvent e, final File storage, final String name) {
      JComponent src = (JComponent)e.getSource();
      if (src instanceof JMenuItem) {
        src = (JComponent)src.getParent();
      }
      if (src instanceof JPopupMenu) {
        src = (JComponent)((JPopupMenu)src).getInvoker();
      }
      this.rootPane = src.getRootPane();
      this.storage = storage;
      this.name = name;
    }

    public void run () {
      java.awt.Component glassPane = rootPane.getGlassPane();
      rootPane.setGlassPane(new WaitGlassPane());
      rootPane.getGlassPane().setVisible(true);

      try {
        ImportAction.this.storage.open(storage.getAbsolutePath());
        try {
          Component c = repository.loadComponent(name, graph);
          Component p = (Component)selection.getSelection();
          p.addSubComponent(c);
          selection.selectComponent(c);
        } finally {
          ImportAction.this.storage.close();
        }
      } catch (Exception ignored) {
        ignored.printStackTrace();
      }

      rootPane.getGlassPane().setVisible(false);
      rootPane.setGlassPane(glassPane);
    }
  }
}
