/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.model;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.Constants;

import java.util.Map;
import java.util.WeakHashMap;
import java.util.HashMap;
import java.util.Iterator;
import java.awt.Color;

/**
 * Basic implementation of the {@link GraphModel} interface.
 */

public class BasicGraphModel implements GraphModel, BindingController {

  /**
   * A collection client interface bound to the {@link GraphModelListener
   * listeners} of this model.
   */

  public final static String GRAPH_MODEL_LISTENERS_BINDING =
    "graph-model-listeners";

  /**
   * The listeners client interface.
   */

  private Map listeners;

  /**
   * A map associating {@link Color}s to {@link Component}s.
   */

  private Map colors;

  /**
   * A map associating {@link Rect}s to {@link Component}s.
   */

  private Map positions;

  /**
   * Constructs a new {@link BasicGraphModel} component.
   */

  public BasicGraphModel () {
    colors = new WeakHashMap();
    positions = new WeakHashMap();
    listeners = new HashMap();
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return (String[])listeners.keySet().toArray(new String[listeners.size()]);
  }

  public Object lookupFc (final String clientItfName) {
    if (clientItfName.startsWith(GRAPH_MODEL_LISTENERS_BINDING)) {
      return listeners.get(clientItfName);
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (clientItfName.startsWith(GRAPH_MODEL_LISTENERS_BINDING)) {
      listeners.put(clientItfName, serverItf);
    }
  }

  public void unbindFc (final String clientItfName) {
    if (clientItfName.startsWith(GRAPH_MODEL_LISTENERS_BINDING)) {
      listeners.remove(clientItfName);
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the GraphModel interface
  // -------------------------------------------------------------------------

  public Color getComponentColor (Component component) {
    Color c = (Color)colors.get(component);
    if (c == null) {
      c = Constants.COMPONENT_COLOR;
      colors.put(component, c);
    }
    return c;
  }

  public void setComponentColor (Component component, Color color) {
    Color oldColor = getComponentColor(component);
    if (!color.equals(oldColor)) {
      colors.put(component, color);
      Iterator i = listeners.values().iterator();
      while (i.hasNext()) {
        GraphModelListener l = (GraphModelListener)i.next();
        l.componentColorChanged(component, oldColor);
      }
    }
  }

  public Rect getComponentPosition (final Component component) {
    Rect result = (Rect)positions.get(component);
    if (result == null) {
      result = new Rect(0.1, 0.1, 0.9, 0.9);
      positions.put(component, result);
    }
    return result;
  }

  public void setComponentPosition (
    final Component component,
    final Rect position)
  {
    Rect oldPosition = getComponentPosition(component);
    if (!position.equals(oldPosition)) {
      positions.put(component, position);
      Iterator i = listeners.values().iterator();
      while (i.hasNext()) {
        GraphModelListener l = (GraphModelListener)i.next();
        l.componentPositionChanged(component, oldPosition);
      }
    }
  }
}
