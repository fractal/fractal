/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.clipboard.control;

import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.IllegalOperationException;

import java.net.URL;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.KeyStroke;
import javax.swing.JOptionPane;

/**
 * An action that just calls the {@link
 * org.objectweb.fractal.gui.clipboard.model.Clipboard#cut cut} method on a
 * {@link org.objectweb.fractal.gui.clipboard.model.Clipboard}.
 */

public class CutAction extends ClipboardAction {

  /**
   * Constructs a new {@link CutAction} component.
   */

  public CutAction () {
    putValue(NAME, "Cut");
    putValue(SHORT_DESCRIPTION, "Cut");
    putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
    URL url = getClass().getResource(
      "/org/objectweb/fractal/gui/resources/editcut.gif");
    putValue(SMALL_ICON, new ImageIcon(url));
    setEnabled(false);
  }

  // -------------------------------------------------------------------------
  // Implementation of the SelectionListener interface
  // -------------------------------------------------------------------------

  public void selectionChanged () {
    Object o = selection.getSelection();
    if (o instanceof Component) {
      setEnabled(clipboard.canCut((Component)o));
    } else {
      setEnabled(clipboard.canCut(null));
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the ActionListener interface
  // -------------------------------------------------------------------------

  public void actionPerformed (final ActionEvent e) {
    Object o = selection.getSelection();
    try {
      clipboard.cut((Component)o, graph, factory);
    } catch (IllegalOperationException ioe) {
      JOptionPane.showMessageDialog(
        null, ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }
    selection.clearSelection();
  }
}
