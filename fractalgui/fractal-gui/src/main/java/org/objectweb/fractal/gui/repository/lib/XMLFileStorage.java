/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.repository.lib;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.Parser;
import org.objectweb.fractal.adl.xml.XMLParser;
import org.objectweb.fractal.adl.xml.XMLWriter;
import org.objectweb.fractal.gui.repository.api.Storage;

/**
 * Basic implementation of {@link Storage} interface, based on a single XML
 * file. More precsiely, each storage is a single XML file, and each (name,
 * value) pair is represented by an XML element of these files (the name is
 * given by the 'name' attribute of an XML element).
 */

public class XMLFileStorage implements Storage {

  static String FS = new String (System.getProperty ("file.separator"));

  /**
   * Name of the currently opened storage, or <tt>null</tt>.
   */

  protected String storage;

  private Parser parser = new XMLParser();
  
  public void open (final String storage)
    throws Exception
  {
    if (this.storage != null) {
      throw new Exception("Storage already opened");
    }
    this.storage = storage;
  }

  public Object load (final String name) throws Exception {
    if (storage == null) {
      throw new Exception("Storage not opened");
    }
    String n = name.replace('.', '/') + ".fractal";
    File f = new File(storage, n);
    InputStream is = null;
    if (f.exists()) {
      is = new FileInputStream(f);
    } else {
      ClassLoader cl = getClass().getClassLoader();
      if (cl == null) {
        cl = ClassLoader.getSystemClassLoader();
      }
      is = cl.getResourceAsStream(n);
    }
    if (is == null) {
      throw new ADLException("Cannot find '" + name + "' in the storage or in the classpath", null);
    }
    try {
      return parser.parse(is, f.getAbsolutePath());
    } finally {
      is.close();
    }
  }

  public void store (final String name, final Object value) throws Exception {
    if (storage == null) {
      throw new Exception("Storage not opened");
    }
    String n = name.replace('.', '/') + ".fractal";
    File f = new File(storage, n);
    if (!f.getParentFile().exists()) {
      f.getParentFile().mkdirs();
    }
    FileWriter pw = new FileWriter(f);
    pw.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n");
    pw.write("<!DOCTYPE definition PUBLIC \"-//objectweb.org//DTD Fractal ADL 2.0//EN\" \"classpath://org/objectweb/fractal/adl/xml/standard.dtd\">\n\n");
    XMLWriter xmlw = new XMLWriter(pw);
    xmlw.write((Node)value);
    pw.close();
  }

  public void close () throws Exception {
    if (storage == null) {
      throw new Exception("Storage not opened");
    }
    storage = null;
  }
}
