/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.menu.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.selection.model.SelectionListener;
import org.objectweb.fractal.gui.selection.model.Selection;
import org.objectweb.fractal.gui.model.Factory;
import org.objectweb.fractal.swing.AbstractAction;

/**
 * Super classes of actions that create components or interfaces.
 */

public abstract class CreateAction extends AbstractAction implements
  SelectionListener,
  BindingController
{

  /**
   * A mandatory client interface bound to a {@link Selection selection} model.
   * This model is used to select the created components or interfaces, and
   * to add them to the selected component.
   */

  public final static String SELECTION_BINDING = "selection";

  /**
   * A mandatory client interface bound to a {@link Factory factory} component.
   * This factory is used to create components or interfaces.
   */

  public final static String FACTORY_BINDING = "configuration-factory";

  /**
   * The selection client interface.
   */

  Selection selection;

  /**
   * The factory client interface.
   */

  Factory factory;

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] { SELECTION_BINDING, FACTORY_BINDING };
  }

  public Object lookupFc (final String clientItfName) {
    if (SELECTION_BINDING.equals(clientItfName)) {
      return selection;
    } else if (FACTORY_BINDING.equals(clientItfName)) {
      return factory;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (SELECTION_BINDING.equals(clientItfName)) {
      selection = (Selection)serverItf;
    } else if (FACTORY_BINDING.equals(clientItfName)) {
      factory = (Factory)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (SELECTION_BINDING.equals(clientItfName)) {
      selection = null;
    } else if (FACTORY_BINDING.equals(clientItfName)) {
      factory = null;
    }
  }
}

