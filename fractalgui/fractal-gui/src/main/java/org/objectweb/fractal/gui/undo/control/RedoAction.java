/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.undo.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.undo.model.UndoListener;
import org.objectweb.fractal.gui.undo.model.UndoManager;
import org.objectweb.fractal.swing.AbstractAction;

import java.awt.event.ActionEvent;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.KeyStroke;

/**
 * An action that just calls the {@link UndoManager#redo redo} method on an
 * {@link UndoManager}. This action listens to the undo model in order to enable
 * or disable itfself when the {@link UndoManager#canRedo canRedo} state
 * changes.
 */

public class RedoAction extends AbstractAction implements
  UndoListener,
  BindingController
{

  /**
   * A mandatory client interface bound to a {@link UndoManager undo} model.
   * This is the model modified by this controller component.
   */

  public final static String UNDO_MANAGER_BINDING = "undo-manager";

  /**
   * The undo client interface.
   */

  private UndoManager undoManager;

  /**
   * Constructs a new {@link RedoAction} component.
   */

  public RedoAction () {
    putValue(NAME, "Redo");
    putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("control shift Z"));
    putValue(SHORT_DESCRIPTION, "Redo");
    URL url = getClass().getResource(
      "/org/objectweb/fractal/gui/resources/redo.gif");
    putValue(SMALL_ICON, new ImageIcon(url));
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] { UNDO_MANAGER_BINDING };
  }

  public Object lookupFc (final String clientItfName) {
    if (UNDO_MANAGER_BINDING.equals(clientItfName)) {
      return undoManager;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (UNDO_MANAGER_BINDING.equals(clientItfName)) {
      undoManager = (UndoManager)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (UNDO_MANAGER_BINDING.equals(clientItfName)) {
      undoManager = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the UndoListener interface
  // -------------------------------------------------------------------------

  public void undoStateChanged () {
    setEnabled(undoManager.canRedo());
  }

  // -------------------------------------------------------------------------
  // Implementation of the ActionListener interface
  // -------------------------------------------------------------------------

  public void actionPerformed (final ActionEvent e) {
    undoManager.redo();
  }
}
