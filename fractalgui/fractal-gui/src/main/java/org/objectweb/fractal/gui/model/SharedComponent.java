/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An implementation of the {@link Component} interface for slave components.
 */

public class SharedComponent extends AbstractComponent {

  /**
   * The master component of this slave component.
   */
  protected BasicComponent masterComponent;

  /**
   * Status of this component.
   */

  private long status;

  /**
   * Name of this component.
   */

  private String name;

  /**
   * A map associating the interface of this component to the interfaces of its
   * master component.
   */

  private Map interfaces;

  /**
   * Constructs a new slave component.
   *
   * @param masterComponent the master component of this slave component.
   */
  public SharedComponent (final BasicComponent masterComponent) {
    if (masterComponent.getMasterComponent() != null) {
      throw new RuntimeException("Internal error");
    }
    this.masterComponent = masterComponent;
    this.status = NAME_MISSING;
    this.name = "";
    this.interfaces = new HashMap();
    masterComponent.addSlaveComponent(this);
  }


  public Configuration getConfiguration () {
    return masterComponent.getConfiguration();
  }
  public long getStatus () {
    return status | masterComponent.getStatus();
  }

  public void setStatus (final long status) {
    this.status = status;
  }

  // -------------------------------------------------------------------------
  // Name, Type, Implementation
  // -------------------------------------------------------------------------

  public String getName () {
    return name;
  }

  public void setName (final String name) {
    if (name == null) {
      throw new IllegalArgumentException();
    }
    String oldName = this.name;
    if (!name.equals(oldName)) {
      List vetoableListeners = masterComponent.getOwner().getVetoableListeners();
      for (int i = 0; i < vetoableListeners.size(); ++i) {
        VetoableConfigurationListener l =
          (VetoableConfigurationListener)vetoableListeners.get(i);
        l.canChangeName(this);
      }
      this.name = name;
      if (name.equals("")) {
        status |= NAME_MISSING;
      } else {
        status &= ~NAME_MISSING;
      }
      List listeners = masterComponent.getOwner().getListeners();
      for (int i = 0; i < listeners.size(); ++i) {
        ConfigurationListener cl = (ConfigurationListener)listeners.get(i);
        cl.nameChanged(this, oldName);
      }
    }
  }

  public String getType () {
    return masterComponent.getType();
  }

  public void setType (final String type) {
    masterComponent.setType(type);
  }

  public String getImplementation () {
    return masterComponent.getImplementation();
  }

  public void setImplementation (final String implementation) {
    masterComponent.setImplementation(implementation);
  }

  // -------------------------------------------------------------------------
  // Client and Server interfaces
  // -------------------------------------------------------------------------

  public List getClientInterfaces () {
    List l = masterComponent.getClientInterfaces();
    List result = new ArrayList();
    for (int i = 0; i < l.size(); ++i) {
      result.add(getInterface((Interface)l.get(i)));
    }
    return Collections.unmodifiableList(result);
  }

  public Interface getClientInterface (final String name) {
    return getInterface(masterComponent.getClientInterface(name));
  }

  public void addClientInterface (final ClientInterface itf) {
    masterComponent.addClientInterface(itf);
  }

  public void removeClientInterface (ClientInterface itf) {
    if (itf instanceof SharedInterface) {
      itf = (ClientInterface)((SharedInterface)itf).masterInterface;
    }
    masterComponent.removeClientInterface(itf);
  }

  public List getServerInterfaces () {
    List l = masterComponent.getServerInterfaces();
    List result = new ArrayList();
    for (int i = 0; i < l.size(); ++i) {
      result.add(getInterface((Interface)l.get(i)));
    }
    return Collections.unmodifiableList(result);
  }

  public Interface getServerInterface (final String name) {
    return getInterface(masterComponent.getServerInterface(name));
  }

  public void addServerInterface (final ServerInterface itf) {
    masterComponent.addServerInterface(itf);
  }

  public void removeServerInterface (ServerInterface itf) {
    if (itf instanceof SharedInterface) {
      itf = (ServerInterface)((SharedInterface)itf).masterInterface;
    }
    masterComponent.removeServerInterface(itf);
  }

  // -------------------------------------------------------------------------
  // Bindings
  // -------------------------------------------------------------------------

  public void bind (final ClientInterface citf, final String citfName, final ServerInterface sitf) {
    Interface itf = ((SharedInterface)citf).masterInterface;
    masterComponent.bind((ClientInterface)itf, citfName, sitf);
  }

  public void rebind (final ClientInterface citf, final ServerInterface sitf) {
    Interface itf = ((SharedInterface)citf).masterInterface;
    masterComponent.rebind((ClientInterface)itf, sitf);
  }

  public void unbind (final ClientInterface citf) {
    Interface itf = ((SharedInterface)citf).masterInterface;
    masterComponent.unbind((ClientInterface)itf);
  }

  // -------------------------------------------------------------------------
  // Attributes
  // -------------------------------------------------------------------------

  public String getAttributeController () {
    return masterComponent.getAttributeController();
  }

  public void setAttributeController (final String attributeController) {
    masterComponent.setAttributeController(attributeController);
  }

  public List getAttributeNames () {
    return masterComponent.getAttributeNames();
  }

  public String getAttribute (final String attributeName) {
    return masterComponent.getAttribute(attributeName);
  }

  public void setAttribute (
    final String attributeName,
    final String attributeValue)
  {
    masterComponent.setAttribute(attributeName, attributeValue);
  }

  // -------------------------------------------------------------------------
  // Controller descriptors
  // -------------------------------------------------------------------------

  public String getTemplateControllerDescriptor () {
    return masterComponent.getTemplateControllerDescriptor();
  }

  public void setTemplateControllerDescriptor (final String desc) {
    masterComponent.setTemplateControllerDescriptor(desc);
  }

  public String getComponentControllerDescriptor () {
    return masterComponent.getComponentControllerDescriptor();
  }

  public void setComponentControllerDescriptor (final String desc) {
    masterComponent.setComponentControllerDescriptor(desc);
  }

  // -------------------------------------------------------------------------
  // Sharing
  // -------------------------------------------------------------------------

  public boolean isShared () {
    return true;
  }

  public Component getMasterComponent () {
    return masterComponent;
  }

  public List getSlaveComponents () {
    return Collections.EMPTY_LIST;
  }

  // -------------------------------------------------------------------------
  // Composite specific informations
  // -------------------------------------------------------------------------

  public boolean isComposite () {
    return false;
  }

  public List getSubComponents () {
    return Collections.EMPTY_LIST;
  }

  public Component getSubComponent (final String name) {
    return null;
  }

  public void addSubComponent (final Component child) {
    throw new IllegalOperationException(
      "Cannot add a sub component inside a shared component");
  }

  public void removeSubComponent (final Component child) {
    throw new RuntimeException("Internal error");
  }

  // -------------------------------------------------------------------------
  // Other methods
  // -------------------------------------------------------------------------

  /**
   * Returns the interface of this slave component that correspond to the given
   * interface.
   *
   * @param masterInterface an interface of the master component of this slave
   *      component.
   * @return  the interface of this slave component that correspond to the given
   *      interface.
   */

  Interface getInterface (final Interface masterInterface) {
    if (masterInterface == null) {
      return null;
    }
    Interface i = (Interface)interfaces.get(masterInterface);
    if (i == null) {
      if (masterInterface instanceof ClientInterface) {
        i = new SharedClientInterface(this, masterInterface);
      } else {
        i = new SharedServerInterface(this, masterInterface);
      }
      interfaces.put(masterInterface, i);
    }
    return i;
  }
}
