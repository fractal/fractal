/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.dialog.model;

import javax.swing.ListSelectionModel;
import javax.swing.table.TableModel;
import javax.swing.text.Document;

/**
 * A model for a dialog based configuration view. This model is made of many
 * Swing sub models, one for each element of the dialog view.
 */

public interface DialogModel {

  /**
   * Returns the model for the table representing the external client
   * interfaces.
   *
   * @return the model for the table representing the external client
   *      interfaces.
   */

  TableModel getClientInterfacesTableModel();

  /**
   * Returns the selection model for the table representing the external client
   * interfaces.
   *
   * @return the selection model for the table representing the external client
   *      interfaces.
   */

  ListSelectionModel getClientInterfacesTableSelectionModel();

  /**
   * Returns the model for the table representing the external server
   * interfaces.
   *
   * @return the model for the table representing the external server
   *      interfaces.
   */

  TableModel getServerInterfacesTableModel();

  /**
   * Returns the selection model for the table representing the external server
   * interfaces.
   *
   * @return the selection model for the table representing the external server
   *      interfaces.
   */

  InterfaceTableSelectionModel getServerInterfacesTableSelectionModel();

  /**
   * Returns the model for the table representing the component's attributes.
   *
   * @return the model for the table representing the component's attributes.
   */

  TableModel getAttributesTableModel();

  /**
   * Returns the selection model for the table representing the component's
   * attributes.
   *
   * @return the selection model for the table representing the component's
   *      attributes.
   */

  ListSelectionModel getAttributesTableSelectionModel();

  /**
   * Returns the model for the text field containing the component's name.
   *
   * @return the model for the text field containing the component's name.
   */

  Document getNameFieldModel();

  /**
   * Returns the model for the text field containing the component's type.
   *
   * @return the model for the text field containing the component's type.
   */

  Document getTypeFieldModel();

  /**
   * Returns the model for the text field containing the component's
   * implementation.
   *
   * @return the model for the text field containing the component's
   *      implementation.
   */

  Document getImplementationFieldModel();

  /**
   * Returns the model for the text field containing the component's attribute
   * controller.
   *
   * @return the model for the text field containing the component's attribute
   *      controller.
   */

  Document getAttrControllerFieldModel();

  /**
   * Returns the model for the text field containing the component's template
   * controller descriptor.
   *
   * @return the model for the text field containing the component's template
   *      controller descriptor.
   */

  Document getTmplControllerDescFieldModel();

  /**
   * Returns the model for the text field containing the component's controller
   * descriptor.
   *
   * @return the model for the text field containing the component's controller
   *      descriptor.
   */

  Document getCompControllerDescFieldModel();
}
