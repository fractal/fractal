/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.graph.control;

import org.objectweb.fractal.api.control.AttributeController;

/**
 * Attribute controller interface of {@link ChangeDepthAction} component.
 */

public interface ChangeDepthActionAttributes extends AttributeController {

  /**
   * Returns <tt>true</tt> if the action must increase the max depth of the
   * display.
   *
   * @return <tt>true</tt> if the action must increase the max depth of the
   *      display, or <tt>false</tt> if it must decrease it.
   * @see #setIsIncrease
   */

  boolean getIsIncrease ();

  /**
   * Sets the increase attribute of this action component.
   *
   * @param isIncrease <tt>true</tt> if the action must increase the max depth
   *      of the display, or <tt>false</tt> if it must decrease it.
   * @see #getIsIncrease
   */

  void setIsIncrease (boolean isIncrease);
}
