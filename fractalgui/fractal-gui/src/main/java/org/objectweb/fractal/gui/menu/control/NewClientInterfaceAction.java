/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.menu.control;

import org.objectweb.fractal.gui.model.ClientInterface;
import org.objectweb.fractal.gui.model.Component;

import java.awt.event.ActionEvent;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.KeyStroke;

/**
 * An action to create new client interfaces.
 */

public class NewClientInterfaceAction extends CreateAction {

  /**
   * Constructs a new {@link NewClientInterfaceAction} component.
   */

  public NewClientInterfaceAction () {
    putValue(NAME, "New client interface");
    putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("F7"));
    URL url = getClass().getResource(
      "/org/objectweb/fractal/gui/resources/empty.gif");
    putValue(SMALL_ICON, new ImageIcon(url));
    setEnabled(false);
  }

  // -------------------------------------------------------------------------
  // Implementation of the SelectionListener interface
  // -------------------------------------------------------------------------

  public void selectionChanged () {
    setEnabled(selection.getSelection() instanceof Component);
  }

  // -------------------------------------------------------------------------
  // Implementation of the ActionListener interface
  // -------------------------------------------------------------------------

  public void actionPerformed (final ActionEvent e) {
    Object o = selection.getSelection();
    Component c = (Component)o;
    ClientInterface i = factory.createClientInterface(c);
    c.addClientInterface(i);
    //selection.selectInterface(i);
  }
}
