/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.model;

import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

import javax.swing.JOptionPane;

/**
 * Abstract implementation of the {@link Interface} interface.
 */

public abstract class BasicInterface implements Interface {

  /**
   * The component that owns this interface.
   */

  protected BasicComponent owner;

  /**
   * Status of this interface.
   */

  private long status;

  /**
   * Name of this interface.
   */

  String name;

  /**
   * Singature of this interface.
   */

  private String signature;

  /**
   * If this interface is an internal interface or an external interface.
   */

  private boolean isInternal;

  /**
   * If this interface is optional or mandatory.
   */

  private boolean isOptional;

  /**
   * If this interface is a collection interface or a single interface.
   */

  private boolean isCollection;

  /**
   * The master collection interface of this interface, or <tt>null</tt> if this
   * interface is not a slave collection interface.
   */

  protected Interface masterCollectionItf;

  /**
   * The slave collection interfaces of this master collection interface. This
   * list is a list of {@link Interface} objects. It is empty if this interface
   * is not a master collection interface.
   */

  protected List slaveCollectionItfs;

  /**
   * The complementary interface of this interface.
   */

  protected Interface complementaryItf;

  /**
   * Constructs a new external interface.
   *
   * @param owner the component that will own this interface.
   */

  BasicInterface (final BasicComponent owner) {
    this.owner = owner;
    status = NAME_MISSING | SIGNATURE_MISSING;
    name = "";
    signature = "";
    isInternal = false;
    slaveCollectionItfs = new ArrayList();
  }

  /**
   * Constructs a new internal interface.
   *
   * @param externalItf the complementary interface of this interface.
   */

  BasicInterface (final Interface externalItf) {
    isInternal = true;
    complementaryItf = externalItf;
  }

  /**
   * Constructs a new slave collection interface.
   *
   * @param masterCollectionItf a master collection interface.
   * @param ignored ignored.
   */

  BasicInterface (final Interface masterCollectionItf, final int ignored) {
    this.isCollection = true;
    this.masterCollectionItf = masterCollectionItf;
  }

  public Component getOwner () {
    return isInternal
      ? complementaryItf.getOwner()
      : masterCollectionItf != null
        ? masterCollectionItf.getOwner()
        : owner;
  }

  public long getStatus () {
    return isInternal
      ? complementaryItf.getStatus()
      : masterCollectionItf != null
        ? masterCollectionItf.getStatus()
        : status;
  }

  public void setStatus (final long status) {
    this.status = status;
  }

  public String getName () {
    if (isInternal) {
      return complementaryItf.getName();
    } else if (masterCollectionItf != null) {
      if (name == null) {
        List l = masterCollectionItf.getSlaveCollectionInterfaces();
        Set s = new HashSet();
        for (int i = 0; i < l.size(); ++i) {
          if (l.get(i) != this) {
            s.add(((Interface)l.get(i)).getName());
          }
        }
        int i = 0;
        do {
          name = Integer.toString(100 + (i++)).substring(1); 
        } while (s.contains(masterCollectionItf.getName() + name));
      } 
      return masterCollectionItf.getName() + name;
      /*int i = masterCollectionItf.getSlaveCollectionInterfaces().indexOf(this);
      if (i != -1) {
        name = masterCollectionItf.getName() + Integer.toString(100 + i).substring(1);
      }
      return name;*/
    } else {
      return name;
    }
  }

  public void setName (final String name) {
    if (name == null) {
      throw new IllegalArgumentException();
    }
    if (isInternal) {
      complementaryItf.setName(name);
    } else if (masterCollectionItf != null) {
      masterCollectionItf.setName(name);
    } else {
      String oldName = this.name;
      if (!name.equals(oldName)) {
        List vetoableListeners = owner.getOwner().getVetoableListeners();
        for (int i = 0; i < vetoableListeners.size(); ++i) {
          VetoableConfigurationListener l =
            (VetoableConfigurationListener)vetoableListeners.get(i);
          l.canChangeInterfaceName(this);
        }
        this.name = name;
        List listeners = owner.getOwner().getListeners();
        for (int i = 0; i < listeners.size(); ++i) {
          ConfigurationListener l = (ConfigurationListener)listeners.get(i);
          l.interfaceNameChanged(this, oldName);
        }
      }
    }
  }

  public String getSignature () {
    return isInternal
      ? complementaryItf.getSignature()
      : masterCollectionItf != null
        ? masterCollectionItf.getSignature()
        : signature;
  }

  public void setSignature (final String signature) {
    if (signature == null) {
      throw new IllegalArgumentException();
    }
    if (isInternal) {
      complementaryItf.setSignature(signature);
    } else if (masterCollectionItf != null) {
      masterCollectionItf.setSignature(signature);
    } else {
      String oldSignature = this.signature;
      if (!signature.equals(oldSignature)) {
        List vetoableListeners = owner.getOwner().getVetoableListeners();
        for (int i = 0; i < vetoableListeners.size(); ++i) {
          VetoableConfigurationListener l =
            (VetoableConfigurationListener)vetoableListeners.get(i);
          l.canChangeInterfaceSignature(this);
        }
        this.signature = signature;
        List listeners = owner.getOwner().getListeners();
        for (int i = 0; i < listeners.size(); ++i) {
          ConfigurationListener l = (ConfigurationListener)listeners.get(i);
          l.interfaceSignatureChanged(this, oldSignature);
        }
      }
    }
  }

  public boolean isInternal () {
    return isInternal;
  }

  public boolean isOptional () {
    return isInternal
      ? complementaryItf.isOptional()
      : masterCollectionItf != null
        ? masterCollectionItf.isOptional()
        : isOptional;
  }

  public void setIsOptional (final boolean isOptional) {
    if (isInternal) {
      complementaryItf.setIsOptional(isOptional);
    } else if (masterCollectionItf != null) {
      masterCollectionItf.setIsOptional(isOptional);
    } else {
      boolean oldIsOptional = this.isOptional;
      if (isOptional != oldIsOptional) {
        List vetoableListeners = owner.getOwner().getVetoableListeners();
        for (int i = 0; i < vetoableListeners.size(); ++i) {
          VetoableConfigurationListener l =
            (VetoableConfigurationListener)vetoableListeners.get(i);
          l.canChangeInterfaceContingency(this);
        }
        this.isOptional = isOptional;
        List listeners = owner.getOwner().getListeners();
        for (int i = 0; i < listeners.size(); ++i) {
          ConfigurationListener l = (ConfigurationListener)listeners.get(i);
          l.interfaceContingencyChanged(this, oldIsOptional);
        }
      }
    }
  }

  public boolean isCollection () {
    return isInternal ? complementaryItf.isCollection() : isCollection;
  }


  // continuer � raler et catcher exception dialog
  public void setIsCollection (final boolean isCollection) {
//@@@
    boolean faut_raler = false;
    if (isCollection) {
      if (this instanceof ClientInterface) {
        ClientInterface citf = (ClientInterface)this;
        if (citf.getBinding() != null) {
          faut_raler = true;
        }
      }
      else if (this instanceof ServerInterface) {
        ServerInterface sitf = (ServerInterface)this;
        if (sitf.getBindings().size() > 0) {
          faut_raler = true;
        }
      }
    }
    if (faut_raler) {
      JOptionPane.showMessageDialog (null,
      "Cannot set this interface as a collection interface."+
      "because, it's already bound to another.", "Error",
      JOptionPane.ERROR_MESSAGE);
      return;
    }

    if (isInternal) {
      complementaryItf.setIsCollection(isCollection);
    } else {
      boolean oldIsCollection = this.isCollection;
      if (isCollection != oldIsCollection) {
        if (masterCollectionItf != null ||
            getSlaveCollectionInterfaces().size() > 0)
        {
          throw new IllegalOperationException(
            "Cannot change the cardinality of a root collection interface. " +
            "You must first remove all the interfaces of the collection");
        }
        List vetoableListeners = owner.getOwner().getVetoableListeners();
        for (int i = 0; i < vetoableListeners.size(); ++i) {
          VetoableConfigurationListener l =
            (VetoableConfigurationListener)vetoableListeners.get(i);
          l.canChangeInterfaceCardinality(this);
        }
        this.isCollection = isCollection;
        List listeners = owner.getOwner().getListeners();
        for (int i = 0; i < listeners.size(); ++i) {
          ConfigurationListener l = (ConfigurationListener)listeners.get(i);
          l.interfaceCardinalityChanged(this, oldIsCollection);
        }
      }
    }
  }

  public Interface getMasterCollectionInterface () {
    return masterCollectionItf;
  }

  public List getSlaveCollectionInterfaces () {
    return isInternal
      ? complementaryItf.getSlaveCollectionInterfaces()
      : masterCollectionItf != null
        ? Collections.EMPTY_LIST
        : Collections.unmodifiableList(slaveCollectionItfs);
  }

  /**
   * Adds the given interface to the list of slave collection interface of this
   * interface.
   *
   * @param slaveItf a new slave collection interface of this interface.
   */

  void addSlaveCollectionInterface (final Interface slaveItf) {
    if (slaveCollectionItfs == null) {
      throw new RuntimeException("Internal error");
    }
    if (!slaveCollectionItfs.contains(slaveItf)) {
      slaveCollectionItfs.add(slaveItf);
    }
  }

  /**
   * Removes the given interface from the list of slave collection interface of
   * this interface.
   *
   * @param slaveItf a slave collection interface of this interface.
   */

  void removeSlaveCollectionInterface (final Interface slaveItf) {
    if (slaveCollectionItfs == null) {
      throw new RuntimeException("Internal error");
    }
    slaveCollectionItfs.remove(slaveItf);
  }

  public Interface getComplementaryInterface () {
    return complementaryItf;
  }

  public Interface getMasterInterface () {
    return null;
  }
}
