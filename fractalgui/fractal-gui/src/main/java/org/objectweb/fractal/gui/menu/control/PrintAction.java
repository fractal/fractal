/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.menu.control;

import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.gui.graph.model.Display;
import org.objectweb.fractal.gui.model.Component;
import org.objectweb.fractal.gui.model.Configuration;
import org.objectweb.fractal.gui.graph.view.Printer;
import org.objectweb.fractal.gui.selection.model.Selection;
import org.objectweb.fractal.swing.AbstractAction;

import java.awt.event.ActionEvent;
import java.net.URL;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

/**
 * An action to print a configuration.
 */

public class PrintAction extends AbstractAction implements
  BindingController
{

  public final static String CONFIGURATION_BINDING = "configuration";

  public final static String SELECTION_BINDING = "selection";

  public final static String DISPLAY_BINDING = "display";

  public final static String PRINTER_BINDING = "printer";

  private Configuration configuration;

  private Selection selection;

  private Display display;

  private Printer printer;

  /**
   * Constructs a new {@link PrintAction} component.
   */

  public PrintAction () {
    putValue(NAME, "Print");
    putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("control P"));
    putValue(SHORT_DESCRIPTION, "Print");
    URL url = getClass().getResource(
      "/org/objectweb/fractal/gui/resources/print.gif");
    putValue(SMALL_ICON, new ImageIcon(url));
    setEnabled(true);
  }

  // -------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // -------------------------------------------------------------------------

  public String[] listFc () {
    return new String[] {
      CONFIGURATION_BINDING,
      SELECTION_BINDING,
      DISPLAY_BINDING,
      PRINTER_BINDING
    };
  }

  public Object lookupFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      return configuration;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      return selection;
    } else if (DISPLAY_BINDING.equals(clientItfName)) {
      return display;
    } else if (PRINTER_BINDING.equals(clientItfName)) {
      return printer;
    }
    return null;
  }

  public void bindFc (
    final String clientItfName,
    final Object serverItf)
  {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = (Configuration)serverItf;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = (Selection)serverItf;
    } else if (DISPLAY_BINDING.equals(clientItfName)) {
      display = (Display)serverItf;
    } else if (PRINTER_BINDING.equals(clientItfName)) {
      printer = (Printer)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (CONFIGURATION_BINDING.equals(clientItfName)) {
      configuration = null;
    } else if (SELECTION_BINDING.equals(clientItfName)) {
      selection = null;
    } else if (DISPLAY_BINDING.equals(clientItfName)) {
      display = null;
    } else if (PRINTER_BINDING.equals(clientItfName)) {
      printer = null;
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the ActionListener interface
  // -------------------------------------------------------------------------

  public void actionPerformed (final ActionEvent e) {
    Object o = selection.getSelection();
    if (o instanceof Component) {
      Component c = (Component)o;
      printer.setType(Printer.TYPE_PRINT);

      List subComponents = c.getSubComponents();
      if (subComponents.size() > 0) {
        Object[] options = {"Yes", "No" };
        int n = JOptionPane.showOptionDialog(
          null,
          "Do you want to print all components of the current configuration ?",
          "Warning",
          JOptionPane.YES_NO_CANCEL_OPTION,
          JOptionPane.QUESTION_MESSAGE,
          null,
          options,
          options[0]);
        if (n == 0) {
          imprime (c.getRootComponent(), 0);
          configuration.setRootComponent(c.getRootComponent());
          selection.selectComponent(c.getRootComponent());
        }
        else printer.print(true, c);
      }
    }
  }

  private void imprime (final Component c, final int lvl) {

    boolean on_continue = true;
    if (lvl == 0) on_continue = printer.print(true, c);
    else on_continue = printer.print(false, c);
    if (!on_continue) return;

    List subComponents = c.getSubComponents();
    for (int i = 0; i < subComponents.size(); ++i) {
      Component subC = (Component)subComponents.get(i);
      imprime (subC, lvl+1);
    }
  }
}
