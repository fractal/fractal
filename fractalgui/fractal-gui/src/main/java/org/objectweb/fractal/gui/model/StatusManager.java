/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.model;

import java.util.List;

/**
 * A {@link ConfigurationListener} to manage the status of components,
 * interfaces and bindings.
 */

public class StatusManager implements ConfigurationListener {

  public void rootComponentChanged (final Component oldValue) {
    // does nothing
  }

  public void changeCountChanged (Component component, long changeCount) {
    // does nothing
  }

  public void nameChanged (final Component component, final String oldValue) {
    long status = component.getStatus();
    String name = component.getName();
    if (name.equals("")) {
      status |= Component.NAME_MISSING;
    } else {
      status &= ~Component.NAME_MISSING;
    }
    component.setStatus(status);
    saysItsModified (component);
  }

  public void typeChanged (final Component component, final String oldValue) {
    long status = component.getStatus();
    String type = component.getType();
    /*if (type.equals("")) {
      status |= Component.TYPE_MISSING;
    } else {
      status &= ~Component.TYPE_MISSING;
    }*/
    component.setStatus(status);
    saysItsModified (component);
  }

  public void implementationChanged (
    final Component component,
    final String oldValue)
  {
    long status = component.getStatus();
    String implementation = component.getImplementation();
    if (implementation.equals("")) {
      if (!component.isComposite()) {
        status |= Component.IMPLEMENTATION_MISSING;
      }
    } else {
      status &= ~Component.IMPLEMENTATION_MISSING;
      try {
        Class.forName(implementation);
        status &= ~Component.IMPLEMENTATION_CLASS_NOT_FOUND;
      } catch (ClassNotFoundException e) {
        status |= Component.IMPLEMENTATION_CLASS_NOT_FOUND;
      }
    }
    component.setStatus(status);
    saysItsModified (component);
  }

  public void interfaceNameChanged (final Interface i, final String oldValue) {
    long status = i.getStatus();
    String name = i.getName();
    if (name.equals("")) {
      status |= Interface.NAME_MISSING;
    } else {
      status &= ~Interface.NAME_MISSING;
    }
    i.setStatus(status);
    saysItsModified (i.getOwner());
  }

  public void interfaceSignatureChanged (
    final Interface i,
    final String oldValue)
  {
    long status = i.getStatus();
    String signature = i.getSignature();
    if (signature.equals("")) {
      status |= Interface.SIGNATURE_MISSING;
    } else {
      status &= ~Interface.SIGNATURE_MISSING;
      try {
        Class.forName(signature);
        status &= ~Interface.SIGNATURE_CLASS_NOT_FOUND;
      } catch (ClassNotFoundException e) {
        status |= Interface.SIGNATURE_CLASS_NOT_FOUND;
      }

      if (i instanceof ClientInterface) {
        ClientInterface citf = (ClientInterface)i;
        Binding bd = citf.getBinding();
        if (bd != null) {
          ServerInterface sitf = bd.getServerInterface();
          if (sitf != null) {
            verifyCompatibility (citf, sitf);
          }
        }
      }
      else {
        ServerInterface sitf = (ServerInterface)i;
        List bindings = sitf.getBindings();
        for (int z = 0; z < bindings.size(); ++z) {
          Binding bd = (Binding)bindings.get(z);
          ClientInterface citf = bd.getClientInterface();
          if (citf != null) {
            verifyCompatibility (citf, sitf);
          }
        }
      }
    }
    i.setStatus(status);
    saysItsModified (i.getOwner());
  }

  public void interfaceContingencyChanged (
    final Interface i,
    final boolean oldValue)
  {
    saysItsModified (i.getOwner()); // does nothing
  }

  public void interfaceCardinalityChanged (
    final Interface i,
    final boolean oldValue)
  {
    saysItsModified (i.getOwner()); // does nothing
  }

  public void clientInterfaceAdded (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    saysItsModified (component); // does nothing
  }

  public void clientInterfaceRemoved (
    final Component component,
    final ClientInterface i,
    final int index)
  {
    saysItsModified (component); // does nothing
  }

  public void serverInterfaceAdded (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    saysItsModified (component); // does nothing
  }

  public void serverInterfaceRemoved (
    final Component component,
    final ServerInterface i,
    final int index)
  {
    saysItsModified (component); // does nothing
  }

  public void interfaceBound (
    final ClientInterface citf,
    final ServerInterface sitf)
  {
/*@@@ */
    // TODO ? deplacer ailleurs (CompletionManager ? ?crire)
    String tempo;

    if (citf.getSignature().length() == 0) {
      tempo = sitf.getSignature();
      if (tempo.length() != 0) {
        citf.setSignature(tempo);
      }
    }
    if (sitf.getSignature().length() == 0) {
      tempo = citf.getSignature();
      if (tempo.length() != 0) {
        sitf.setSignature(tempo);
      }
    }
    if (citf.getName().length() == 0) {
      tempo = sitf.getName();
      if (tempo.length() != 0) {
        citf.setName(tempo);
      }
    }
    if (sitf.getName().length() == 0) {
      tempo = citf.getName();
      if (tempo.length() != 0) {
        sitf.setName(tempo);
      }
    }
    verifyCompatibility (citf, sitf);
    saysItsModified (citf.getOwner());
//@@@
  }

  public void interfaceRebound (
    final ClientInterface citf,
    final ServerInterface oldSitf)
  {
    // TODO
    verifyCompatibility (citf, oldSitf);
    saysItsModified (citf.getOwner());
  }

  public void interfaceUnbound (
    final ClientInterface citf,
    final ServerInterface sitf)
  {
    // TODO
    verifyCompatibility (citf, sitf);
    saysItsModified (citf.getOwner());
  }

  public void attributeControllerChanged (
    final Component component,
    final String oldValue)
  {
    long status = component.getStatus();
    String attributeController = component.getAttributeController();
    List attributes = component.getAttributeNames();
    if (attributeController.equals("")) {
      if (attributes.size() > 0) {
        status |= Component.ATTRIBUTE_CONTROLLER_MISSING;
      }
    } else {
      status &= ~Component.ATTRIBUTE_CONTROLLER_MISSING;
      try {
        Class.forName(attributeController);
        status &= ~Component.ATTRIBUTE_CONTROLLER_CLASS_NOT_FOUND;
      } catch (ClassNotFoundException e) {
        status |= Component.ATTRIBUTE_CONTROLLER_CLASS_NOT_FOUND;
      }
    }
    component.setStatus(status);
    saysItsModified (component);
  }

  public void attributeChanged (
    final Component component,
    final String attributeName,
    final String oldValue)
  {
    long status = component.getStatus();
    String attributeController = component.getAttributeController();
    List attributes = component.getAttributeNames();
    if (attributes.size() == 0) {
      status &= ~Component.ATTRIBUTE_CONTROLLER_MISSING;
    } else if (attributeController.equals("")) {
      status |= Component.ATTRIBUTE_CONTROLLER_MISSING;
    }
    component.setStatus(status);
    saysItsModified (component);
  }

  public void templateControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    saysItsModified (component); // does nothing
  }

  public void componentControllerDescriptorChanged (
    final Component component,
    final String oldValue)
  {
    saysItsModified (component); // does nothing
  }

  public void subComponentAdded (
    final Component parent,
    final Component child,
    final int index)
  {
    long status = parent.getStatus();
    status &= ~Component.IMPLEMENTATION_MISSING;
    status &= ~Component.IMPLEMENTATION_CLASS_NOT_FOUND;
    parent.setStatus(status);
    saysItsModified (parent);
  }

  public void subComponentRemoved (
    final Component parent,
    final Component child,
    final int index)
  {
    long status = parent.getStatus();
    List subComponents = parent.getSubComponents();
    String implementation = parent.getImplementation();
    if (subComponents.size() == 0 && implementation.equals("")) {
      status |= Component.IMPLEMENTATION_MISSING;
    }
    parent.setStatus(status);
    saysItsModified (parent);
  }

  // -------------------------------------------------------------------------
  // Other methods
  // -------------------------------------------------------------------------

  protected void saysItsModified (Component component) {
    Configuration cf = component.getConfiguration();
    if (cf.getRootComponent().contains(component)) {
      cf.setChangeCount(cf.getChangeCount() + 1);
    }
  }

  private void verifyCompatibility (
    final ClientInterface citf,
    final ServerInterface sitf)
  {
    if (citf == null || sitf == null) {
      return;
    }
    Binding bd = citf.getBinding();
    if (bd == null) {
      return;
    }
    if ((citf.getSignature().length() > 0) &&
        (sitf.getSignature().length() > 0))
    {
      try {
        Class c = Class.forName(citf.getSignature());
        Class s = Class.forName(sitf.getSignature());
        if (!s.isAssignableFrom(c)) {
        }
      } catch (ClassNotFoundException e) {
        bd.setStatus(Binding.INCOMPATIBLE_TYPE_INTERFACES);
        return;
      }
    }
    bd.setStatus(Binding.OK);
  }
}
