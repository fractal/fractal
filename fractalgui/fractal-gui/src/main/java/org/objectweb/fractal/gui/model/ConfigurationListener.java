/***
 * FractalGUI: a graphical tool to edit Fractal component configurations.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton, Patrice Fauvel
 */

package org.objectweb.fractal.gui.model;

/**
 * An interface to be notified of changes in a {@link Configuration} model.
 */

public interface ConfigurationListener {

  /**
   * Notifies this listener that the root component has changed.
   *
   * @param oldValue the old root component of the configuration.
   */

  void rootComponentChanged (Component oldValue);

  /**
   * Notifies this listener that all the change count of the configuration has
   * changed.
   *
   * @param changeCount the new change count of the configuration.
   */

  void changeCountChanged (Component component, long changeCount);

  // -------------------------------------------------------------------------
  // Name, Type, Implementation
  // -------------------------------------------------------------------------

  /**
   * Notifies this listener that the name of a component has changed.
   *
   * @param component the component whose name has changed.
   * @param oldValue the old name of the component.
   */

  void nameChanged (Component component, String oldValue);

  /**
   * Notifies this listener that the type of a component has changed.
   *
   * @param component the component whose type has changed.
   * @param oldValue the old type of the component.
   */

  void typeChanged (Component component, String oldValue);

  /**
   * Notifies this listener that the implementation of a component has changed.
   *
   * @param component the component whose implementation has changed.
   * @param oldValue the old implementation of the component.
   */

  void implementationChanged (Component component, String oldValue);

  // -------------------------------------------------------------------------
  // Client and server interfaces
  // -------------------------------------------------------------------------

  /**
   * Notifies this listener that the name of an interface has changed.
   *
   * @param i the interface whose name has changed.
   * @param oldValue the old name of the interface.
   */

  void interfaceNameChanged (Interface i, String oldValue);

  /**
   * Notifies this listener that the signature of an interface has changed.
   *
   * @param i the interface whose signature has changed.
   * @param oldValue the old signature of the interface.
   */

  void interfaceSignatureChanged (Interface i, String oldValue);

  /**
   * Notifies this listener that the contigency of an interface has changed.
   *
   * @param i the interface whose contingency has changed.
   * @param oldValue the old contingency of the interface.
   */

  void interfaceContingencyChanged (Interface i, boolean oldValue);

  /**
   * Notifies this listener that the cardinality of an interface has changed.
   *
   * @param i the interface whose cardinality has changed.
   * @param oldValue the old cardinality of the interface.
   */

  void interfaceCardinalityChanged (Interface i, boolean oldValue);

  /**
   * Notifies this listener that a client interface has been added to a
   * component.
   *
   * @param component the component into which the new interface has been added.
   * @param i the interface that has been added.
   * @param index the index of this interface in the client interface list of
   *      the component.
   */

  void clientInterfaceAdded (Component component, ClientInterface i, int index);

  /**
   * Notifies this listener that a client interface has been removed from a
   * component.
   *
   * @param component the component from which the interface has been removed.
   * @param i the interface that has been removed.
   * @param index the index of this interface in the client interface list of
   *      the component.
   */

  void clientInterfaceRemoved (
    Component component,
    ClientInterface i,
    int index);

  /**
   * Notifies this listener that a server interface has been added to a
   * component.
   *
   * @param component the component into which the new interface has been added.
   * @param i the interface that has been added.
   * @param index the index of this interface in the server interface list of
   *      the component.
   */

  void serverInterfaceAdded (Component component, ServerInterface i, int index);

  /**
   * Notifies this listener that a server interface has been removed from a
   * component.
   *
   * @param component the component from which the interface has been removed.
   * @param i the interface that has been removed.
   * @param index the index of this interface in the server interface list of
   *      the component.
   */

  void serverInterfaceRemoved (
    Component component,
    ServerInterface i,
    int index);

  // -------------------------------------------------------------------------
  // Bindings
  // -------------------------------------------------------------------------

  /**
   * Notifies this listener that an interface has been bound.
   *
   * @param citf the client interface that has been bound.
   * @param sitf the server interface to which it has been bound.
   */

  void interfaceBound (ClientInterface citf, ServerInterface sitf);

  /**
   * Notifies this listener that an interface has been rebound.
   *
   * @param citf the client interface that has been rebound.
   * @param oldSitf the server interface to which it <i>was</i> bound.
   */

  void interfaceRebound (ClientInterface citf, ServerInterface oldSitf);

  /**
   * Notifies this listener that an interface has been unbound.
   *
   * @param citf the client interface that has been unbound.
   * @param sitf the server interface to which it <i>was</i> bound.
   */

  void interfaceUnbound (ClientInterface citf, ServerInterface sitf);

  // -------------------------------------------------------------------------
  // Attributes
  // -------------------------------------------------------------------------

  /**
   * Notifies this listener that the attribute controller of a component has
   * changed.
   *
   * @param component the component whose attribute controller has changed.
   * @param oldValue the old attribute controller of the component.
   */

  void attributeControllerChanged (Component component, String oldValue);

  /**
   * Notifies this listener that the value of an attribute has changed.
   *
   * @param component the component whose attribute has changed.
   * @param attributeName the name of the attribute whose value has changed.
   * @param oldValue the old value of the attribute.
   */

  void attributeChanged (
    Component component,
    String attributeName,
    String oldValue);

  // -------------------------------------------------------------------------
  // Controller descriptors
  // -------------------------------------------------------------------------

  /**
   * Notifies this listener that the template controller descriptor of a
   * component has changed.
   *
   * @param component the component whose template contoller descriptor has
   *      changed.
   * @param oldValue the old value of the template controller descriptor.
   */

  void templateControllerDescriptorChanged (
    Component component,
    String oldValue);

  /**
   * Notifies this listener that the component controller descriptor of a
   * component has changed.
   *
   * @param component the component whose component contoller descriptor has
   *      changed.
   * @param oldValue the old value of the component controller descriptor.
   */

  void componentControllerDescriptorChanged (
    Component component,
    String oldValue);

  // -------------------------------------------------------------------------
  // Composite specific informations
  // -------------------------------------------------------------------------

  /**
   * Notifies this listener that a sub component has been added to a
   * component.
   *
   * @param parent the component into which the sub component has been added.
   * @param child the sub component that has been added.
   * @param index the index of this sub component in the sub component list of
   *      the parent component.
   */

  void subComponentAdded (Component parent, Component child, int index);

  /**
   * Notifies this listener that a sub component has been removed from a
   * component.
   *
   * @param parent the component from which the sub component has been removed.
   * @param child the sub component that has been removed.
   * @param index the index of this sub component in the sub component list of
   *      the parent component.
   */

  void subComponentRemoved (Component parent, Component child, int index);
}
