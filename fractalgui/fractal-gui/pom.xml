<?xml version="1.0"?>
<!-- 
	* This library is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2 of the License, or (at your option) any later version.
	*
	* This library is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	*
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
-->
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>org.objectweb.fractal.fractalgui</groupId>
		<artifactId>parent</artifactId>
		<version>0.4.3-SNAPSHOT</version>
	</parent>

	<groupId>org.objectweb.fractal.fractalgui</groupId>
	<artifactId>fractal-gui</artifactId>
	<name>The Fractal GUI tool</name>

	<!-- eventually provide a more specific url -->
	<url>http://fractal.objectweb.org/</url>

	<developers>
		<developer>
			<id>bruneton</id>
			<name>Eric Bruneton</name>
			<email>Eric.Bruneton@rd.francetelecom.com</email>
			<organization>France Telecom RD</organization>
			<organizationUrl>
				http://www.rd.francetelecom.com
			</organizationUrl>
			<roles>
				<role>Architect</role>
				<role>Developer</role>
			</roles>
		</developer>
		<developer>
			<id>coupaye</id>
			<name>Thierry Coupaye</name>
			<email>Thierry.Coupaye@rd.francetelecom.com</email>
			<organization>France Telecom RD</organization>
			<organizationUrl>
				http://www.rd.francetelecom.com
			</organizationUrl>
			<roles>
				<role>Project Manager</role>
				<role>Architect</role>
			</roles>
		</developer>
		<developer>
			<id>stefani</id>
			<name>Jean-Bernard Stefani</name>
			<email>Jean-Bernard.Stefani@inrialpes.fr</email>
			<organization>INRIA</organization>
			<organizationUrl>http://www.inria.fr</organizationUrl>
			<roles>
				<role>Project Manager</role>
				<role>Architect</role>
			</roles>
		</developer>
	</developers>
	<contributors>
		<contributor>
			<name>Laurent Andrey</name>
			<organization>Loria</organization>
			<organizationUrl>http://www.loria.fr</organizationUrl>
		</contributor>
		<contributor>
			<name>Pierre Charles David</name>
			<organization>EMN</organization>
			<organizationUrl>http://www.emn.fr</organizationUrl>
		</contributor>
		<contributor>
			<name>Pascal Dechamboux</name>
			<organization>France Telecom RD</organization>
			<organizationUrl>
				http://www.rd.francetelecom.com
			</organizationUrl>
		</contributor>
		<contributor>
			<name>Romain Lenglet</name>
			<organization>France Telecom RD</organization>
			<organizationUrl>
				http://www.rd.francetelecom.com
			</organizationUrl>
		</contributor>
		<contributor>
			<name>Philippe Merle</name>
			<organization>INRIA</organization>
			<organizationUrl>http://www.inria.fr</organizationUrl>
		</contributor>
	</contributors>

	<dependencies>
		<dependency>
			<groupId>org.objectweb.fractal.fractaladl</groupId>
			<artifactId>fractal-adl</artifactId>
			<version>2.1.7-SNAPSHOT</version>
		</dependency>

		<dependency>
			<groupId>org.objectweb.fractal.fractalswing</groupId>
			<artifactId>fractal-swing</artifactId>
			<version>0.2.1-SNAPSHOT</version>
		</dependency>

		<dependency>
			<groupId>org.objectweb.fractal.fractalrmi</groupId>
			<artifactId>fractal-rmi</artifactId>
			<version>0.3.5-SNAPSHOT</version>
		</dependency>

		<dependency>
			<groupId>cwi.SVGGraphics</groupId>
			<artifactId>SVGGraphics</artifactId>
			<version>1.0</version>
		</dependency>


		<dependency>
			<groupId>org.objectweb.fractal.julia</groupId>
			<artifactId>julia-runtime</artifactId>
			<version>2.5.1-SNAPSHOT</version>
		</dependency>
		<dependency>
			<groupId>org.objectweb.fractal.julia</groupId>
			<artifactId>julia-asm</artifactId>
			<version>2.5.1-SNAPSHOT</version>
		</dependency>
		<dependency>
			<groupId>org.objectweb.fractal.julia</groupId>
			<artifactId>julia-mixins</artifactId>
			<version>2.5.1-SNAPSHOT</version>
		</dependency>

	</dependencies>


	<scm>
		<connection>
			${scm.connection}/fractalgui/fractal-gui
		</connection>
		<developerConnection>
			${scm.developerConnection}/fractalgui/fractal-gui
		</developerConnection>
		<url>${scm.url}/fractalgui/fractal-gui</url>
	</scm>

	<reporting>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<configuration>
					<windowtitle>
						Fractal GUI Documentation
					</windowtitle>
					<doctitle>
						Fractal GUI Documentation
					</doctitle>

					<overview>
						${basedir}/src/main/javadoc/overview.html
					</overview>

					<docfilessubdirs>true</docfilessubdirs>
					<excludedocfilessubdir>.svn</excludedocfilessubdir>
					<!-- 
						<links>
						<link href="${jdk.url}" offline="true"
						packagelistLoc="${basedir}/src/main/javadoc/jdoc/jdk-list" />
						</links>
					-->
				</configuration>
			</plugin>
		</plugins>
	</reporting>


	<profiles>
		<profile>
			<id>run</id>
			<build>
				<defaultGoal>compile</defaultGoal>
				<plugins>
					<plugin>
						<groupId>org.codehaus.mojo</groupId>
						<artifactId>exec-maven-plugin</artifactId>
						<executions>
							<execution>
								<goals>
									<goal>exec</goal>
								</goals>
								<phase>compile</phase>
							</execution>
						</executions>
						<configuration>
							<executable>
								${JAVA_HOME}/bin/java
							</executable>
							<arguments>
								<argument>-cp</argument>
								<classpath />
								<argument>
									org.objectweb.fractal.gui.FractalGUI
								</argument>
							</arguments>
						</configuration>
					</plugin>
				</plugins>

			</build>
		</profile>

	</profiles>



</project>
