/*==============================================================================
Fractal XDoclet Plugin - Copyright (C) 2002-2006 INRIA Futurs / LIFL
Fractal Component Model (contact: fractal@objectweb.org)

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free 
Software Foundation; either version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along 
with this library; if not, write to the Free Software Foundation, Inc., 
59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

Initial developer(s): Romain Rouvoy (romain.rouvoy@lifl.fr)
Contributor(s): .
--------------------------------------------------------------------------------
$Id$
==============================================================================*/
package comanche;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

/**
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version $Revision$
 */
public class FileRequestHandler implements RequestHandler {
    
    /** @logger */
    protected Logger log;
    
    /* (non-Javadoc)
     * @see comanche.RequestHandler#handleRequest(comanche.Request)
     */
    public void handleRequest(Request r) throws IOException {
        File f = new File(r.url);
        if (f.exists() && !f.isDirectory()) {
            InputStream is = new FileInputStream(f);
            byte[] data = new byte[is.available()];
            is.read(data);
            is.close();
            r.out.print("HTTP/1.0 200 OK\n\n");
            r.out.write(data);
        } else {
            if (log != null && log.isLoggable(BasicLevel.ERROR))
                log.log(BasicLevel.ERROR, "File \'"+r.url+"\' not found.");
            throw new IOException("File not found");
        }
    }
}
