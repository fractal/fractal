/*==============================================================================
Fractal XDoclet Plugin - Copyright (C) 2002-2006 INRIA Futurs / LIFL
Fractal Component Model (contact: fractal@objectweb.org)

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free 
Software Foundation; either version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along 
with this library; if not, write to the Free Software Foundation, Inc., 
59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

Initial developer(s): Romain Rouvoy (romain.rouvoy@lifl.fr)
Contributor(s): .
--------------------------------------------------------------------------------
$Id$
==============================================================================*/
package comanche;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.PrintStream;

/**
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version $Revision$
 */
public class RequestAnalyzer implements RequestHandler {
    /** @requires name=rhd */
    protected RequestHandler rh;

    /** @requires */
    protected Logger l;

    /* (non-Javadoc)
     * @see comanche.RequestHandler#handleRequest(comanche.Request)
     */
    public void handleRequest(Request r) throws IOException {
        r.in = new InputStreamReader(r.s.getInputStream());
        r.out = new PrintStream(r.s.getOutputStream());
        String rq = new LineNumberReader(r.in).readLine();
        l.log(rq);
        if (rq.startsWith("GET ")) {
            r.url = rq.substring(5, rq.indexOf(' ', 4));
            rh.handleRequest(r);
        }
        r.out.close();
        r.s.close();
    }
}
