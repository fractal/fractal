/*==============================================================================
Fractal XDoclet Plugin - Copyright (C) 2002-2006 INRIA Futurs / LIFL
Fractal Component Model (contact: fractal@objectweb.org)

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free 
Software Foundation; either version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along 
with this library; if not, write to the Free Software Foundation, Inc., 
59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

Initial developer(s): Romain Rouvoy (romain.rouvoy@lifl.fr)
Contributor(s): .
--------------------------------------------------------------------------------
$Id$
==============================================================================*/
package comanche;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version $Revision$
 * @provides name=r signature=java.lang.Runnable
 */
public class RequestReceiver implements Runnable {
    /** @requires */
    protected Scheduler s;

    /** @requires */
    protected RequestHandler rh;
    
    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    public void run() {
        try {
            ServerSocket ss = new ServerSocket(8080);
            System.out.println("Comanche HTTP Server ready on port 8080...");
            while (true) {
                final Socket socket = ss.accept();
                s.schedule(new Runnable() {
                    public void run() {
                        try {
                            rh.handleRequest(new Request(socket));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
