

/**
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version $Revision$
 * @provides
 */
public interface Display {
    /**
     * Prints the helloworld message.
     * @param message the message to print.
     */
    void print(String message);
}