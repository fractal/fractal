import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.util.monolog.api.Logger;


/**
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version $Revision$
 * @monolog.handler name=clientHandler output=System.err pattern="%-5l [%h] <%t> %m%n"
 */
public abstract class ClientAbstract {
    /** @requires name=default */
    protected Display defaut ;
    
    /** @attribute value=Hello */
    protected String message;
    
    /** @logger name=client handler="clientHandler,consoleHandler" */
    protected Logger logger;
    
    /** @control name=lifecycle-controller */
    protected LifeCycleController lcc;
    
    /** @lifecycle when=start
     *  @lifecycle when=stop */
    protected void initClient(){
    	System.out.println("Client "+lcc.getFcState());
    }
}
