

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.objectweb.fractal.api.control.NameController;

/**
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version $Revision$
 * @component name=GUI controller=primitive
 */
public class ServerGui extends ServerText {
    /** @control name=name-controller */
    protected NameController name;
    
    /* (non-Javadoc)
     * @see helloworld.ServerText#print(java.lang.String)
     */
    public void print(final String message) {
        JFrame frame = new JFrame(this.name.getFcName()+"("+this.lcc.getFcState()+")"+": New Message");
        frame.getContentPane().setLayout(new java.awt.BorderLayout());
        Box vbox = Box.createVerticalBox();
        for (int i = 0; i < COUNTER; ++i)
            vbox.add(new JLabel(header + message));
        frame.getContentPane().add(vbox);
        frame.pack();
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation((screen.width - frame.getWidth()) / 2,
                (screen.height - frame.getHeight()) / 2);
        frame.setVisible(true);
    }
}