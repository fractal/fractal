import org.objectweb.fractal.api.control.LifeCycleController;


/**
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version $Revision$
 * @component name="TEXT"
 */
public class ServerText implements Display {
    /** @control name=lifecycle-controller */
    protected LifeCycleController lcc;

    /** @attribute value=">> " */
    protected String header;

    /** @attribute value=3 */
    protected int COUNTER;

    /* (non-Javadoc)
     * @see org.objectweb.fractal.fraclet.adl.api.Service#print()
     */
    public void print(final String message) {
        for (int i = 0; i < this.COUNTER; i++)
            System.out.println(lcc.getFcState()+": "+this.header + message);
    }
}