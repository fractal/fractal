

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.objectweb.util.monolog.api.BasicLevel;

/**
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version $Revision$
 * @provides name=r signature=java.lang.Runnable
 */
public class Client extends ClientAbstract implements Runnable {
    /** @requires cardinality=collection signature=Display */
    protected Map service = new HashMap();
    
    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    public void run() {
        for (Iterator i=service.entrySet().iterator();i.hasNext();) {
            Entry s = (Entry) i.next();
            if (logger!=null && logger.isLoggable(BasicLevel.INFO))
                logger.log(BasicLevel.INFO,"Calling "+s.getKey()+"...");
            ((Display)s.getValue()).print(this.message);
        }
        if (logger!=null && logger.isLoggable(BasicLevel.INFO))
            logger.log(BasicLevel.INFO,"Default service says:");
        this.defaut.print(this.message);
    }
}