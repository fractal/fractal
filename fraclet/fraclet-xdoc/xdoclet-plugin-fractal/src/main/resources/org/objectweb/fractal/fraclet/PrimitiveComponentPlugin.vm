## =============================================================================
## Fractal XDoclet Plugin - Copyright (C) 2002-2006 INRIA Futurs / LIFL
## Fractal Component Model (contact: fractal@objectweb.org)

## This library is free software; you can redistribute it and/or modify it under 
## the terms of the GNU Lesser General Public License as published by the Free 
## Software Foundation; either version 2.1 of the License, or any later version.

## This library is distributed in the hope that it will be useful, but WITHOUT ANY 
## WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
## PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

## You should have received a copy of the GNU Lesser General Public License along 
## with this library; if not, write to the Free Software Foundation, Inc., 
## 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

## Initial developer(s): Romain Rouvoy (romain.rouvoy@lifl.fr)
## Contributor(s): .
## -----------------------------------------------------------------------------
##\$Id$
## =============================================================================*/
#set( $class = $metadata )
#if( $plugin.hasPackage($class) )
package $plugin.componentPackage($class) ;
#end

#set( $needBindingController = $plugin.hasBindings($class,true) || $plugin.hasReflects($class,true) )

#if ( $needBindingController )
import java.util.HashSet;

#if( $plugin.hasReflects($class,true) )
import org.objectweb.fractal.api.Component;
#end
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
#end
#if( $plugin.hasLifecycles($class,true) )
import org.objectweb.fractal.api.control.LifeCycleController;
#end
#if ( $needBindingController || $plugin.hasLifecycles($class,true) )
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
#end
#if( $plugin.hasLogger($class,true) )
import org.objectweb.util.monolog.api.Loggable;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.LoggerFactory;

#end

#if ($class.getPackage())
import $class.fullyQualifiedName ;
#end

#set( $bindings = $plugin.bindings($class,true) )
#set( $attributes = $plugin.attributes($class,true) )
/**
 * $dontedit
 */
public class $plugin.componentName($metadata) extends $class.name $plugin.implementsClause($class) {
#if( $plugin.hasLogger($class,true) )
#set ( $log = $plugin.loggers($class,true) )
    private LoggerFactory loggerFactory ;
    
    /* (non-Javadoc)
     * @see org.objectweb.util.monolog.api.Loggable\#getLogger()
     */
    public Logger getLogger() {
        return null ;
    }
    
    /* (non-Javadoc)
     * @see org.objectweb.util.monolog.api.Loggable\#setLogger(org.objectweb.util.monolog.api.Logger)
     */
    public void setLogger(Logger log) {
        // nothing to do.
    }
    
    /* (non-Javadoc)
     * @see org.objectweb.util.monolog.api.Loggable\#getLoggerFactory()
     */
    public LoggerFactory getLoggerFactory() {
        return this.loggerFactory;
    }

    /* (non-Javadoc)
     * @see org.objectweb.util.monolog.api.Loggable\#setLoggerFactory(org.objectweb.util.monolog.api.LoggerFactory)
     */
    public void setLoggerFactory(LoggerFactory factory) {
        this.loggerFactory = factory;
#foreach ($l in $log)
        super.$l.name = getLoggerFactory().getLogger("$plugin.loggerName($l)");
#end
    } 
    
#end
#foreach ( $field in $attributes )
#set( $name = $plugin.attributeMethod($field) )
    /**
     * Setter for the $name attribute.
     * @param value value for the $name attribute.
     */
    public void set$name(final $field.type value) {
        super.$field.name = value ;
    }

    /**
     * Getter for the $name attribute.
     * @return value of the $name attribute.
     */
    public $field.type get$name() {
        return super.$field.name ;
    }

#end
#if ( $needBindingController )
    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.BindingController\#listFc()
     */
    public String[] listFc() {
        HashSet _fcBindings_ = new HashSet();
#foreach ( $binding in $bindings )
#if ( $binding.getTagByName('requires').cardinality.equals("singleton") )        
        if (this.$binding.name != null) _fcBindings_.add("$plugin.bindingName($binding)");
#else
        _fcBindings_.addAll(this.${binding.name}.keySet());
#end
#end
        return (String[]) _fcBindings_.toArray(new String[_fcBindings_.size()]);
    }
    
    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.BindingController\#lookupFc(java.lang.String)
     */
    public Object lookupFc(final String name) throws NoSuchInterfaceException {
#foreach ( $binding in $bindings )
#if ( $binding.getTagByName('requires').cardinality.equals("singleton") )        
        if(name.equals("$plugin.bindingName($binding)")) 
            return super.$binding.name ;
#else
        if(name.startsWith("$plugin.bindingName($binding)")) 
            return super.${binding.name}.get(name);
#end
#end
#if ( $class.isA('org.objectweb.fractal.api.control.BindingController') )
            return super.lookupFc(name);
#else
            throw new NoSuchInterfaceException("Client interface \'"+name+"\' is undefined.");
#end
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.BindingController\#bindFc(java.lang.String, java.lang.Object)
     */
    public void bindFc(final String name, final Object binding) throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException {
        try {
#if( $plugin.hasLogger($class,true) )
            if(name.equals("logger"))
                setLogger((Logger)binding);
            else if (name.equals("logger-factory"))
                setLoggerFactory((LoggerFactory)binding);
            else
#end
#if( $plugin.hasReflects($class,true) )
            if(name.equals("component")) {
                Component _fcComponent_ = (Component) binding ;
#foreach ( $reflect in $plugin.reflects($class,true) )
#set ( $controller = $reflect.getTagByName('control').name_ )
                super.$reflect.name = ($reflect.type.value) _fcComponent_.getFcInterface("$controller");
#end
            } else
#end
#foreach ( $binding in $bindings )
#if ( $binding.getTagByName('requires').cardinality.equals("singleton") )        
            if(name.equals("$plugin.bindingName($binding)")) 
                super.$binding.name = ($binding.type.value) binding;
#else
            if(name.startsWith("$plugin.bindingName($binding)")) 
                super.${binding.name}.put(name, binding);
#end
            else 
#end
#if ( $class.isA('org.objectweb.fractal.api.control.BindingController') )
                super.bindFc(name,binding);
#else
                throw new NoSuchInterfaceException("Client interface \'"+name+"\' is undefined.");
#end
        } catch (ClassCastException ex) {
            throw new IllegalBindingException("Failure when binding the "+name+" client interface: "+ex.getMessage());
        }
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.BindingController\#unbindFc(java.lang.String)
     */
    public void unbindFc(final String name) throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException {
#foreach ( $binding in $bindings )
#if ( $binding.getTagByName('requires').cardinality.equals("singleton") )        
        if(name.equals("$plugin.bindingName($binding)")) 
            super.$binding.name = null ;
#else
        if(name.startsWith("$plugin.bindingName($binding)")) 
            super.${binding.name}.remove(name);
#end
        else
#end
#if ( $class.isA('org.objectweb.fractal.api.control.BindingController') )
            super.unbindFc(name);
#else
            throw new NoSuchInterfaceException("Client interface \'"+name+"\' is undefined.");
#end
    }
#end
#if( $plugin.hasLifecycles($class,true) )
    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.LifeCycleController\#getFcState()
     */
    public String getFcState() { return null; }
    
#set ( $cycles = $plugin.lifecycles($class,true) )
    /**
     * Component constructor
     */
    public $plugin.componentName($metadata)() {
        super();
#foreach ( $cycle in $cycles )
#foreach ( $lcc in $cycle.getTagsByName('lifecycle') )
#if ( $lcc.when.equals("create") )
        try {
            super.${cycle.name}();
        } catch(Exception e) {
            e.printStackTrace();
        }
#end
#end
#end
     }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.LifeCycleController\#startFc()
     */
    public void startFc() throws IllegalLifeCycleException {
      try {
#foreach ( $cycle in $cycles )
#foreach ( $start in $cycle.getTagsByName('lifecycle') )
#if ( $start.when.equals("start") )
          super.${cycle.name}();
#end
#end
#end
      } catch(Exception e) {
          IllegalLifeCycleException illegal = new IllegalLifeCycleException("Error when component starts: "+e.getMessage());
          illegal.initCause(e);
          throw illegal;
      }
#if ( $class.isA('org.objectweb.fractal.api.control.LifeCycleController') )
          super.startFc();
#end
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.LifeCycleController\#stopFc()
     */
    public void stopFc() throws IllegalLifeCycleException {
      try {
#foreach ( $cycle in $cycles )
#foreach ( $stop in $cycle.getTagsByName('lifecycle') )
#if ( $stop.when.equals("stop") )
          super.${cycle.name}();
#end
#end
#end
      } catch(Exception e) {
          IllegalLifeCycleException illegal = new IllegalLifeCycleException("Error when component stops: "+e.getMessage());
          illegal.initCause(e);
          throw illegal;
      }
#if ( $class.isA('org.objectweb.fractal.api.control.LifeCycleController') )
          super.stopFc();
#end
    }

    /* (non-Javadoc)
     * @see java.lang.Object\#finalize()
     */
    public void finalize() throws Throwable {
#foreach ( $cycle in $cycles )
#foreach ( $lcc in $cycle.getTagsByName('lifecycle') )
#if ( $lcc.when.equals("destroy") )
        try {
            super.${cycle.name}();
        } catch(Exception e) {
            e.printStackTrace();
        }
#end
#end
#end
        super.finalize();
    }
#end
}