<!--
    * Fraclet - Copyright (C) 2006 LIFL / INRIA Futurs
    *
    * This library is free software; you can redistribute it and/or modify it under 
    * the terms of the GNU Lesser General Public License as published by the Free 
    * Software Foundation; either version 2 of the License, or (at your option) any 
    * later version.
    *
    * This library is distributed in the hope that it will be useful, but WITHOUT ANY 
    * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
    * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
    *
    * You should have received a copy of the GNU Lesser General Public License along 
    * with this library; if not, write to the Free Software Foundation, Inc., 
    * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
    *
    * Contact: Romain Rouvoy (romain.rouvoy@inria.fr)
    * Author: Romain Rouvoy
    -->
<html>
<body>
<h1>Overview of the Fraclet Annotation Framework</h1>


<table border="0" cellpadding="5" cellspacing="0" summary="">
	<tr>
		<td></td>
		<td rowspan="1" colspan="1" valign="bottom" align="left">

		<ul>

			<li><strong>Title:</strong> Fraclet Tutorial</li>

			<li><strong>Author:</strong> <em>Romain Rouvoy</em> (INRIA)</li>

			<li><strong>Version:</strong> <em>2.0</em> (Fraclet-xdoc)</li>

			<li><strong>Released:</strong> <em>August 30<sup>th</sup>,
			2006</em></li>


		</ul>

		</td>
	</tr>
</table>

<p>Fraclet is both an implementation of the Fractal specifications
conformed to the level 0.1, and an annotation framework for the Fractal
component model. Fraclet is composed of several annotations and plugins
to generate automatically various artifacts required by the Fractal
component model. Annotations provide a way to describe the component
meta-information directly in the source code of the content class.
Fraclet plugins generate either Fractal component glue, FractalADL
definitions or Monolog configurations.</p>


<p>This approach has several benefits:</p>

<ul>

	<li>You don't have to worry about out dating deployment meta-data
	whenever you touch the code. The deployment <strong>meta-data
	is continuously integrated</strong>.</li>


	<li>Working with only one file per component gives you a better
	overview of what you're doing. If your component consists of several
	files, it's easy to lose track. If you have ever written an Enterprise
	Java Bean, you know what we mean. A single EJB can typically consists
	of 7 or more files. With Fraclet you only <strong>maintain one
	of them, and the rest is generated</strong>.</li>

	<li>You dramatically reduce development time, and can concentrate
	on business logic, while Fraclet <strong>generates 60% of the
	code</strong> for you.</li>

</ul>

<p>Two implementations of the Fraclet annotation framework exist: <strong>Fraclet
XDoc</strong> and <strong>Fraclet Annotation</strong>.</p>


<ul>

	<li><strong>Fraclet XDoc</strong> uses the XDoclet generation
	engine to produce the various artifacts required by the Fractal
	component model.</li>

	<li><strong>Fraclet-Annotation</strong> uses the Spoon
	transformation tool to enhance the handwritten program code with the
	non-functional properties of the component model.</li>

</ul>



<h1>Table of Contents</h1>

<ol>


	<li><a href="index.html#xdoc">Fraclet XDoc: An XDoclet2 plugin
	for Fractal</a></li>

	<li><a href="index.html#fracletannotation">Fraclet Annotation:
	A Java 5 annotation support for Fractal</a></li>

	<li><a href="index.html#availability">Fraclet Availability</a></li>

	<li><a href="index.html#publication">Dissemination</a></li>

</ol>


<h1><a name="xdoc"></a>1. Fraclet XDoc: An XDoclet2 plugin for
Fractal</h1>

<p>Fraclet uses the <a href="http://xdoclet.codehaus.org/">XDoclet2</a>
as annotation parser and generation engine.</p>


<p>XDoclet is an open source code generation engine. It enables <strong>Attribute-Oriented
Programming</strong> for java. In short, this means that you can add more
significance to your code by adding meta data (attributes) to your java
sources. This is done in special JavaDoc tags. This use of JavaDoc tags
for attributes formed the original ideas for Java 5 Annotations.</p>

<p>XDoclet2 is the next generation of this technology. Based on <a
	href="http://generama.codehaus.org/">Generama</a>, it uses standard
template engines such as <a href="http://jakarta.apache.org/velocity/">Velocity</a>
and <a href="http://freemarker.sourceforge.net/">Freemarker</a> for
generation of text-oriented output, and <a
	href="http://jakarta.apache.org/commons/jelly/">Jelly</a> for XML
output. The function of XDoclet2 is to seed the generation contexts for
these template engines.</p>

<p>XDoclet2 is supported by <a
	href="http://xdoclet.codehaus.org/XDoclet+Plugins">XDoclet2 plugins</a>,
which provide task-specific generation functionality.</p>

<p>This part describes the features of this XDoclet2 plugin for
Fractal, and the way to use them.</p>

<ol>

	<li><a href="index.html#xdoc-annotations">Available
	Annotations</a></li>

	<li><a href="index.html#xdoc-plugins">Available Plugins</a></li>

	<li><a href="index.html#example">Revisiting the HelloWorld
	Example</a></li>


</ol>



<h2><a name="xdoc-annotations"></a>1.1 Available Annotations</h2>

<p>Some annotations have been defined in order to describe the
component meta-information. The list below describes these annotations.</p>

<table border="1" cellpadding="5" cellspacing="0" summary="">
	<tr>
		<th rowspan="1" colspan="1" valign="top" align="left">Annotation</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Location</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Description</th>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left"><a
			href="index.html#itf"><code>@provides</code></a></td>
		<td rowspan="1" colspan="1" valign="top" align="left">Class</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Annotation
		to describe a Fractal server interface.</td>

	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left"><a
			href="index.html#ctrl"><code>@component</code></a></td>
		<td rowspan="1" colspan="1" valign="top" align="left">Class</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Annotation
		to describe the controller part of a Fractal component.</td>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left"><a
			href="index.html#data"><code>@data</code></a></td>
		<td rowspan="1" colspan="1" valign="top" align="left">Class</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Annotation
		to describe a data used by a Fractal component.</td>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left"><a
			href="index.html#handler"><code>@monolog.handler</code></a></td>
		<td rowspan="1" colspan="1" valign="top" align="left">Class</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Annotation
		to describe a custom Monolog handler.</td>

	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left"><a
			href="index.html#ac"><code>@attribute</code></a></td>
		<td rowspan="1" colspan="1" valign="top" align="left">Field</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Annotation
		to describe an attribute of a Fractal component.</td>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left"><a
			href="index.html#bc"><code>@requires</code></a></td>
		<td rowspan="1" colspan="1" valign="top" align="left">Field</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Annotation
		to describe a binding of a Fractal component.</td>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left"><a
			href="index.html#rc"><code>@control</code></a></td>
		<td rowspan="1" colspan="1" valign="top" align="left">Field</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Annotation
		to provide a reflective control to the content of a Fractal component.</td>

	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left"><a
			href="index.html#log"><code>@logger</code></a></td>
		<td rowspan="1" colspan="1" valign="top" align="left">Field</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Annotation
		to define a logger for a Fractal component.</td>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left"><a
			href="index.html#lcc"><code>@lifecycle</code></a></td>
		<td rowspan="1" colspan="1" valign="top" align="left">Method</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Annotation
		to handle the lifecycle in a Fractal component.</td>
	</tr>
</table>


<h3><a name="itf"></a>The @provides annotation</h3>

<p><strong>Details:</strong></p>

<p>This class annotation adds the name information to the <strong>definition
of a Fractal interface</strong>. This name is required by the Fractal component
model to identify the interfaces required and provided by a Fractal
component. This annotation can also override the associated interface
signature to introduce interface meta-information at the class level.</p>

<p><strong>Parameters:</strong></p>

<table border="1" cellpadding="5" cellspacing="0" summary="">

	<tr>
		<th rowspan="1" colspan="1" valign="top" align="left">Parameter</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Description</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Contingency</th>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">name</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the name of
		the Fractal interface.</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional<br>
		(default value is the signature short name)</td>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">signature</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the
		signature of the Fractal interface.</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional<br>
		(default value is the Class signature)</td>

	</tr>
</table>

<p><strong>Examples:</strong></p>

<pre>
/** @provides name=m */
public interface Main { ... }

/** @provides name=r signature=java.lang.Runnable */
public class Client implements Runnable { ... }
                </pre>


<h3><a name="ctrl"></a>The @component annotation</h3>

<p><strong>Details:</strong></p>

<p>This class annotation allows the developer to describe the <strong>description
of the controller part</strong> that should be associated to the content part of
the Fractal component.</p>

<p><strong>Parameters:</strong></p>

<table border="1" cellpadding="5" cellspacing="0" summary="">
	<tr>
		<th rowspan="1" colspan="1" valign="top" align="left">Parameter</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Description</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Contingency</th>

	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">controller</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the
		definition name of the controller part of the Fractal component.</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Required</td>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">template</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the
		definition name of the controller part of the Fractal template
		component.</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional</td>
	</tr>
</table>

<p>Example:</p>


<pre>
/** @component controller=primitive */
public class Client implements Runnable { ... }

/** @component controller=primitive template=primitiveTemplate */
public class Client implements Runnable { ... }
                </pre>


<h3><a name="data"></a>The @data annotation</h3>

<p><strong>Details:</strong></p>

<p>This class annotation provides a way to define a <strong>data</strong>
used by a Fractal component.</p>


<p><strong>Example:</strong></p>

<pre>
/** @data */                
public class Message {
  ...
}
                </pre>


<h3><a name="handler"></a>The @monolog.handler annotation</h3>

<p><strong>Details:</strong></p>


<p>This class annotation allows the developer to describe a <strong><a
	href="http://monolog.objectweb.org/">Monolog</a> handler</strong> specific to
the Fractal component. The available annotation attributes support the
various properties that can be defined in a <a
	href="http://monolog.objectweb.org/">Monolog</a> configuration file.</p>

<p><strong>Parameters:</strong></p>

<table border="1" cellpadding="5" cellspacing="0" summary="">
	<tr>

		<th rowspan="1" colspan="1" valign="top" align="left">Parameter</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Description</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Contingency</th>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">name</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the
		identifier of the handler.</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Required</td>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">type</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the type of
		the handler.</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional<br>
		(default value is <code>Console</code>)</td>

	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">output</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the output
		flow of the handler.</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional<br>
		(default value is <code>System.out</code>)</td>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">pattern</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the output
		pattern of the handler.</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional<br>
		(default value is <code>%l: %m%n</code>)</td>

	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">max-size</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the maximal
		size of the output file.</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional</td>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">file-number</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the number
		of files used by the handler.</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional</td>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">append-mode</td>
		<td rowspan="1" colspan="1" valign="top" align="left">tag to use
		the handler in append mode (<code>true</code>|<code>false</code>).</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional<br>
		(default value is <code>false</code>)</td>

	</tr>
</table>

<p><strong>Example:</strong></p>

<pre>
/** @monolog.handler name=clientHandler output=System.err pattern="%-5l [%h] &lt;%t&gt; %m%n" */
public class Client implements Runnable { ... }
                </pre>


<h3><a name="ac"></a>The @attribute annotation</h3>


<p><strong>Details:</strong></p>

<p>This field annotation describes a <strong>Fractal
attribute</strong>. A Fractal attribute is a Java attribute whose value can be
configured and introspected from the Fractal component. Fractal
attributes are managed by the <a
	href="../../specification/index.html#tth_sEc4.2">attribute control</a>
feature of the Fractal component model.</p>

<p><strong>Parameters:</strong></p>


<table border="1" cellpadding="5" cellspacing="0" summary="">
	<tr>
		<th rowspan="1" colspan="1" valign="top" align="left">Parameter</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Description</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Contingency</th>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">name</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the name of
		the Fractal attribute.</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional<br>
		(default value is the Field name)</td>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">argument</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the name of
		the component argument used to configure the Fractal attribute.</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional<br>
		(default value is the Field name is no value is defined)</td>

	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">value</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the default
		value of the Fractal attribute.</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional</td>
	</tr>
</table>

<p><strong>Example:</strong></p>

<pre>
public class Client implements Runnable {
  /** @attribute name=count value=2 */
  protected int counter; // field should be declared as protected or public.

  /** @attribute argument=client-prefix */
  protected String pref;

  /** @attribute */
  protected String suffix;

  ...
}
                </pre>



<h3><a name="bc"></a>The @requires annotation</h3>

<p><strong>Details:</strong></p>

<p>This field annotation describes a <strong>Fractal
binding</strong>. A Fractal binding is a Java attribute representing a client
interface. Fractal bindings are managed by the <a
	href="../../specification/index.html#tth_sEc4.3">binding control</a>
feature of the Fractal component model.</p>

<p><strong>Parameters:</strong></p>

<table border="1" cellpadding="5" cellspacing="0" summary="">
	<tr>
		<th rowspan="1" colspan="1" valign="top" align="left">Parameter</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Description</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Contingency</th>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">name</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the name of
		the Fractal binding.</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional<br>
		(default value is the Field name)</td>
	</tr>

	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">signature</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the
		signature of the Fractal binding.</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional<br>
		(default value is the Field signature)</td>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">contingency</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the
		contingency of the Fractal binding (<code>mandatory</code>|<code>optional</code>).</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional<br>
		(default value is <code>mandatory</code>)</td>

	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">cardinality</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the
		cardinality of the Fractal binding (<code>singleton</code>|<code>collection</code>).</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional<br>
		(default value is <code>singleton</code>)</td>
	</tr>
</table>

<p><strong>Example:</strong></p>

<pre>
public class Client implements Runnable {
  /** @requires name=s signature=Service cardinality=collection */
  protected Map servers = new HashMap(); // collections are stored in a map

  /** @requires */
  protected Service service;

  ...
}
                </pre>


<h3><a name="rc"></a>The @control annotation</h3>

<p><strong>Details:</strong></p>

<p>This field annotation provides a way to refer a <strong>control
interface</strong> provided by the controller part of the associated Fractal
component.</p>


<p><strong>Parameters:</strong></p>

<table border="1" cellpadding="5" cellspacing="0" summary="">
	<tr>
		<th rowspan="1" colspan="1" valign="top" align="left">Parameter</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Description</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Contingency</th>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">name</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the name of
		the Fractal controller.</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional<br>
		(default value is <code>component</code>)</td>

	</tr>
</table>

<p><strong>Example:</strong></p>

<pre>
public class Client implements Runnable {
  /** @control name=name-controller */
  protected NameController nc; // field should be declared as protected or public.

  /** @control  */
  protected Component self;

  ...
}
                </pre>


<h3><a name="log"></a>The @logger annotation</h3>

<p><strong>Details:</strong></p>

<p>This field annotation provides a way to define a <strong>Monolog
logger</strong> to log the execution of the Fractal component.</p>

<p><strong>Parameters:</strong></p>

<table border="1" cellpadding="5" cellspacing="0" summary="">
	<tr>
		<th rowspan="1" colspan="1" valign="top" align="left">Parameter</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Description</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Contingency</th>

	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">name</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the name of
		the <a href="http://monolog.objectweb.org/">Monolog</a> logger.</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional<br>
		(default value is the Class name)</td>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">level</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the level
		of the <a href="http://monolog.objectweb.org/">Monolog</a> logger.</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional<br>
		(default value is <code>INFO</code>)</td>

	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">additivity</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the
		additivity tag for the <a href="http://monolog.objectweb.org/">Monolog</a>
		logger (<code>true</code>|<code>false</code>).</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional<br>
		(default value is <code>true</code>)</td>
	</tr>

	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">clean-handlers</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the
		clean-handlers tag for the <a href="http://monolog.objectweb.org/">Monolog</a>
		logger (<code>true</code>|<code>false</code>).</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Optional<br>
		(default value is <code>true</code>)</td>
	</tr>
</table>


<p><strong>Example:</strong></p>

<pre>
public class Client implements Runnable {
  /** @logger name=c handler="clientHandler,fileHandler" */
  protected Logger log; // field should be declared as protected or public.

  /** @logger */
  protected Logger log2;

  ...
}
                </pre>

<p><strong>Julia Configuration:</strong></p>

<p>To enable the Monolog support in Julia, the <code>julia.cfg</code>
configuration file should be modified to override the <code>lifecycle-controller-impl</code>
definition as follows:</p>


<pre>
...                  
                  
# LifeCycleController implementation (for primitive or composite components)
(lifecycle-controller-impl
  ((org.objectweb.fractal.julia.asm.MixinClassGenerator
        LifeCycleControllerImpl
        org.objectweb.fractal.julia.BasicControllerMixin
        org.objectweb.fractal.julia.UseComponentMixin
        org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleCoordinatorMixin
        org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleControllerMixin
        # to check that mandatory client interfaces are bound in startFc:
        org.objectweb.fractal.julia.control.lifecycle.TypeLifeCycleMixin
        # to automatically assign the logger and logger factory:
        org.objectweb.fractal.julia.BasicInitializableMixin
        org.objectweb.fractal.julia.logger.LoggerLifeCycleMixin        
        # to notify the encapsulated component (if present) when its state changes:
        org.objectweb.fractal.julia.control.lifecycle.ContainerLifeCycleMixin
  ) 
        # optional initialization parameter (monolog configuration file name):
        (monolog-conf-file monolog.properties)
  )
)
 
...                                   
                  </pre>

<p><strong>AOKell Configuration:</strong></p>

<p>To enable the Monolog support in AOKell, the <code>build.properties</code>
configuration file should be modified to activate the <code>feature.loggable.on</code>
property as follows:</p>


<pre>
...
                  
# The loggable feature determines whether primitive components are equipped
# with a monolog logger or not (which is the default case).
# Uncomment the following property for using primitive components equipped
# with a monolog logger.
feature.loggable.on 	true                  

...
                </pre>


<h3><a name="lcc"></a>The @lifecycle annotation</h3>

<p><strong>Details:</strong></p>

<p>This method annotation provides a way to define a <strong>lifecycle
handler</strong> supported by the controller part of the associated Fractal
component.</p>


<p><strong>Parameters:</strong></p>

<table border="1" cellpadding="5" cellspacing="0" summary="">
	<tr>
		<th rowspan="1" colspan="1" valign="top" align="left">Parameter</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Description</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Contingency</th>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left">when</td>
		<td rowspan="1" colspan="1" valign="top" align="left">the
		lifecyle transition to handle (<code>create</code>|<code>start</code>|<code>stop</code>|<code>destroy</code>).</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Required</td>

	</tr>
</table>

<p><strong>Example:</strong></p>

<pre>
public class Client implements Runnable {
  /** @lifecycle when=start */
  protected void init() {
    System.out.println("Starting the component Client...");
  }

  ...
}
                </pre>



<h2><a name="xdoc-plugins"></a>1.2 Available Plugins</h2>

<p>Some plugins have been defined in order to generate the component
artifacts. The list below describes these plugins:</p>


<table border="1" cellpadding="5" cellspacing="0" summary="">
	<tr>
		<th rowspan="1" colspan="1" valign="top" align="left">Plugin</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Dependency</th>
		<th rowspan="1" colspan="1" valign="top" align="left">Description</th>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left"><a
			href="index.html#attribute"><code>AttributeControllerPlugin</code></a></td>
		<td rowspan="1" colspan="1" valign="top" align="left">-</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Plugin to
		generate the AttributeController interface of a Fractal component.</td>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left"><a
			href="index.html#primitive"><code>PrimitiveComponentPlugin</code></a></td>
		<td rowspan="1" colspan="1" valign="top" align="left"><code>AttributeControllerPlugin</code></td>
		<td rowspan="1" colspan="1" valign="top" align="left">Plugin to
		generate the component glue class of a Fractal component.</td>

	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left"><a
			href="index.html#adl"><code>PrimitiveDefinitionPlugin</code></a></td>
		<td rowspan="1" colspan="1" valign="top" align="left">-</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Plugin to
		generate the FractalADL definition associated to the Fractal
		component.</td>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left"><a
			href="index.html#compositeadl"><code>CompositeDefinitionPlugin</code></a></td>
		<td rowspan="1" colspan="1" valign="top" align="left"><code>PrimitiveDefinitionPlugin</code></td>
		<td rowspan="1" colspan="1" valign="top" align="left">Plugin to
		generate the FractalADL assembly definition containing the Fractal
		component.</td>
	</tr>
	<tr>
		<td rowspan="1" colspan="1" valign="top" align="left"><a
			href="index.html#monolog"><code>MonologConfigurationPlugin</code></a></td>
		<td rowspan="1" colspan="1" valign="top" align="left">-</td>
		<td rowspan="1" colspan="1" valign="top" align="left">Plugin to
		generate the <a href="http://monolog.objectweb.org/">Monolog</a>
		configuration file associated to the Fractal components.</td>

	</tr>
</table>


<h3><a name="attribute"></a>The AttributeControllerPlugin generator</h3>

<p><strong>Details:</strong></p>

<p>Generates the <code>&lt;CLASS&gt;Attributes</code> interface
associated to a <code>&lt;CLASS&gt;</code> class if it defines at least
an attribute using the <a href="index.html#ac"><code>@attribute</code></a>
annotation.</p>


<p><strong>Usage:</strong></p>

<pre>
&lt;target name="compile"&gt;
  &lt;taskdef name="xdoclet" classname="org.xdoclet.ant.XDocletTask" classpathref="classpath"/&gt;
  &lt;mkdir dir="${gen}"/&gt;
  &lt;xdoclet&gt;

    &lt;fileset dir="${src}" includes="**/*.java" /&gt;
    &lt;component destdir="${gen}" classname="org.objectweb.fractal.fraclet.AttributeControllerPlugin"/&gt;
  &lt;/xdoclet&gt;
  ...
&lt;/target&gt;
                </pre>


<h3><a name="primitive"></a>The PrimitiveComponentPlugin generator</h3>

<p><strong>Details:</strong></p>

<p>Generates the <code>Fc&lt;CLASS&gt;</code> class associated to a
<code>&lt;CLASS&gt;</code> class. class if it defines either a <a
	href="index.html#ac"><code>@attribute</code></a>, <a
	href="index.html#bc"><code>@requires</code></a>, <a
	href="index.html#rc"><code>@control</code></a> or/and <a
	href="index.html#log"><code>@logger</code></a> annotation.</p>


<p><strong>Usage:</strong></p>

<pre>
&lt;target name="compile"&gt;
  &lt;taskdef name="xdoclet" classname="org.xdoclet.ant.XDocletTask" classpathref="classpath"/&gt;
  &lt;mkdir dir="${gen}"/&gt;
  &lt;xdoclet&gt;

    &lt;fileset dir="${src}" includes="**/*.java" /&gt;
    &lt;component destdir="${gen}" classname="org.objectweb.fractal.fraclet.PrimitiveComponentPlugin"/&gt;
  &lt;/xdoclet&gt;
  ...
&lt;/target&gt;
                </pre>


<h3><a name="adl"></a>The PrimitiveDefinitionPlugin generator</h3>

<p>Generates the <code>&lt;CLASS&gt;.fractal</code> definition
associated to a <code>&lt;CLASS&gt;</code> class (or interface).</p>

<p><strong>Usage:</strong></p>


<pre>
&lt;target name="compile"&gt;
  &lt;taskdef name="xdoclet" classname="org.xdoclet.ant.XDocletTask" classpathref="classpath"/&gt;
  &lt;mkdir dir="${build}"/&gt;
  &lt;xdoclet&gt;
    &lt;fileset dir="${src}" includes="**/*.java" /&gt;
    &lt;component destdir="${build}" classname="org.objectweb.fractal.fraclet.PrimitiveDefinitionPlugin"/&gt;

  &lt;/xdoclet&gt;
  ...
&lt;/target&gt;
                </pre>


<h3><a name="compositeadl"></a>The CompositeDefinitionPlugin
generator</h3>

<p><strong>Details:</strong></p>

<p>Generates the <code>&lt;CLASS&gt;Composite.fractal</code>
definition associated to a <code>&lt;CLASS&gt;</code> class if it
defines at least a <a href="index.html#bc"><code>@requires</code></a>
annotation.</p>

<p><strong>Usage:</strong></p>

<pre>
&lt;target name="compile"&gt;
  &lt;taskdef name="xdoclet" classname="org.xdoclet.ant.XDocletTask" classpathref="classpath"/&gt;
  &lt;mkdir dir="${build}"/&gt;
  &lt;xdoclet&gt;
    &lt;fileset dir="${src}" includes="**/*.java" /&gt;

    &lt;component destdir="${build}" classname="org.objectweb.fractal.fraclet.CompositeDefinitionPlugin"/&gt;
  &lt;/xdoclet&gt;
  ...
&lt;/target&gt;
                </pre>


<h3><a name="monolog"></a>The MonologConfigurationPlugin generator</h3>


<p><strong>Details:</strong></p>

<p>Generates the <code>monolog.properties</code> configuration file
for the <a href="http://monolog.objectweb.org/">Monolog</a> tool.</p>

<p><strong>Usage:</strong></p>

<pre>
&lt;target name="compile"&gt;
  &lt;taskdef name="xdoclet" classname="org.xdoclet.ant.XDocletTask" classpathref="classpath"/&gt;
  &lt;mkdir dir="${build}"/&gt;
  &lt;xdoclet&gt;
    &lt;fileset dir="${src}" includes="**/*.java" /&gt;

    &lt;component destdir="${build}" classname="org.objectweb.fractal.fraclet.MonologConfigurationPlugin"/&gt;
  &lt;/xdoclet&gt;
  ...
&lt;/target&gt;
                </pre>



<h2><a name="example"></a>1.3 Revisiting the HelloWorld Example
with Fraclet</h2>


<p>This section provides a quick overview of the benefits of
Fraclet. It shows that using annotations, Fractal source code becomes
more concise and easier to maintain. It shows that using Fractlet, more
than 60% of the source code (Java, FractalADL, properties) can be saved.</p>

<h3><a name="source"></a>HelloWorld Implementation</h3>


<p>The source code below represents the Java code and the Fraclet
annotations written to implement the <code>Client</code> and the <code>Server</code>
Fractal components:</p>

<pre>
/** @provides name=r signature=java.lang.Runnable */
public class Client implements Runnable {
  /** @requires */
  protected Service s ;
          
  /** @attribute value="Hello !" */
  protected String message;
          
  public void run() {
    this.defaut.print(this.message);
  }
}
                          </pre>

<p>The <code>Client</code> class defines the <code>Runnable</code>
interface as a Fractal interface using the <code>@provides</code>
annotation. The attribute <code>s</code> is annoted as a client
interface (using <code>@requires</code>), which will be named <code>s</code>
as no <code>name</code> parameter is defined. The attribute <code>message</code>
is annoted as a Fractal attribute (using <code>@attribute</code>), which
will be automatically initialized to <code>"Hello !"</code>.</p>


<pre>  
/** @provides name=s */
public interface Service {
  void print(String message);
}
                          </pre>

<p>The <code>Service</code> interface is annoted with <code>@provides</code>
to define the name of the Fractal interface as <code>s</code>.</p>

<pre>  
public class Server implements Service {
  /** @attribute */
  protected String header;
                
  /** @attribute */
  protected int counter;
                
  public void print(final String message) {
    for (int i = 0; i &lt; this.counter; i++)
      System.out.println(this.header + message);
  }
}
                        </pre>


<p>The <code>Server</code> class inherits automatically from the <code>Service</code>
annotations. The <code>header</code> and the <code>counter</code>
attributes are defined as Fractal attributes (using <code>@attribute</code>),
whose initial value will be defined when the component will be created.</p>


<h3><a name="source"></a>HelloWorld Architecture Definition</h3>

<p>The assembly definition below represents the FractalADL assembly
defined to describe the <code>HelloWorld</code> Fractal component:</p>

<pre>
&lt;definition name="HelloWorld" extends="ClientComposite"&gt;
  &lt;component name="s" definition="Server('&gt;&gt;',2)"/&gt;

&lt;/definition&gt;
                </pre>

<p>This definition creates a composite component <code>HelloWorld</code>
extending the generated <code>Client</code> abstract definition. Then it
specifies that the definition of the component named <code>s</code> is <code>FcServer</code>
and that the values of the <code>header</code> and the <code>counter</code>
attributes are <code>'&gt;&gt;'</code> and <code>2</code>, respectively.</p>


<h3><a name="compilation"></a>Compilation with Ant</h3>

<p>The following piece of XML presents the compilation process
enhanced with the Fractal plugins.</p>

<pre>
&lt;project name="helloworld" default="compile"&gt;
  ...
  &lt;target name="compile"&gt;
    &lt;taskdef name="xdoclet" classname="org.xdoclet.ant.XDocletTask" classpathref="classpath"/&gt;

    &lt;mkdir dir="${gen}"/&gt;
    &lt;mkdir dir="${build}"/&gt;
    &lt;xdoclet&gt;
      &lt;fileset dir="${src}" includes="**/*.java" /&gt;
      &lt;component destdir="${gen}" classname="org.objectweb.fractal.fraclet.AttributeControllerPlugin"/&gt;
      &lt;component destdir="${gen}" classname="org.objectweb.fractal.fraclet.PrimitiveComponentPlugin"/&gt;

      &lt;component destdir="${build}" classname="org.objectweb.fractal.fraclet.PrimitiveDefinitionPlugin"/&gt;
      &lt;component destdir="${build}" classname="org.objectweb.fractal.fraclet.CompositeDefinitionPlugin"/&gt;
      &lt;component destdir="${build}" classname="org.objectweb.fractal.fraclet.MonologConfigurationPlugin"/&gt;
    &lt;/xdoclet&gt;
    &lt;javac srcdir="${src}:${gen}" destdir="${build}" classpathref="classpath"&gt;
      &lt;include name="**/*.java"/&gt;

    &lt;/javac&gt;
    &lt;copy todir="${build}"&gt;
      &lt;fileset dir="${src}"&gt;
        &lt;exclude name="**/*.java"/&gt;
        &lt;exclude name="**/*.html"/&gt;
      &lt;/fileset&gt;

    &lt;/copy&gt;
  &lt;/target&gt;
  ...
&lt;/project&gt;
                </pre>

<h3><a name="artifacts"></a>Directory Structure</h3>


<p>The directory structure below presents the list of compiled,
generated and written files when using Fraclet to implement the
HelloWorld example:</p>

<pre>
%example%
  * build/
    - Client.class
    - Client.fractal
    - ClientAttributes.class
    - ClientComposite.fractal
    - FcClient.class
    - FcServer.class
    - HelloWorld.fractal
    - Server.class
    - Server.fractal
    - ServerAttributes.class
    - Service.class
    - Service.fractal
  * generated/
    - ClientAttributes.java
    - FcClient.java
    - FcServer.java
    - ServerAttributes.java
  * src/
    - Client.java
    - HelloWorld.fractal
    - Server.java
    - Service.java
                </pre>
<h1><a name="publication"></a>4. Dissemination</h1>

<ul>

	<li><a href="http://www.lifl.fr/~rouvoy/">R. Rouvoy</a>, <a
		href="http://www.lifl.fr/~pessemie/">N. Pessemier</a>, <a
		href="http://www.lifl.fr/~pawlak/">R. Pawlak</a>, <a
		href="http://www.lifl.fr/~merle/">P. Merle</a>, <strong>Apports
	de la programmation par attributs pour le mod&egrave;le de composants
	Fractal</strong>, at the 5th French speaking days on Component-Oriented
	Progamming, October 2006 [slides]</li>


	<li><a href="http://www.lifl.fr/~rouvoy/">R. Rouvoy</a>, <a
		href="http://www.lifl.fr/~pessemie/">N. Pessemier</a>, <a
		href="http://www.lifl.fr/~pawlak/">R. Pawlak</a>, <a
		href="http://www.lifl.fr/~merle/">P. Merle</a>, <strong>Using
	Attribute-Oriented Programming to Leverage Fractal-Based Developments</strong>,
	at the <a href="../../doc/ecoop06/index.html">5th Fractal workshop</a>
	at <a href="http://2006.ecoop.org/">ECOOP 2006</a>, July 2006 [<a
		href="http:/fractal.objectweb.org/doc/ecoop06/talks/rouvoy.pdf">slides</a>]</li>


	<li><a href="http://www.lifl.fr/~rouvoy/">R. Rouvoy</a>, <strong>Fraclet:
	Making Fractal Developments Easier!</strong>, at the Jacquard Research Meeting,
	January 2006 [<a
		href="http:/fractal.objectweb.org/current/rouvoy-fraclet-06.pdf">slides</a>]</li>

</ul>

<td></td>
<td valign="top" width="10"><img alt=" " height="1" width="10"
	src="../../images/pix.gif"></td>
<tr></tr>
<table></table>
<br>
</body>
</html>
