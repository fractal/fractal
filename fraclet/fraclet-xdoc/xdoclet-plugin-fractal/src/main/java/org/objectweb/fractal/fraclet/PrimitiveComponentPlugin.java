/*==============================================================================
 Fractal XDoclet Plugin - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy (romain.rouvoy@lifl.fr)
 Contributor(s): .
 --------------------------------------------------------------------------------
 $Id$
 ==============================================================================*/
package org.objectweb.fractal.fraclet;

import org.generama.QDoxCapableMetadataProvider;
import org.generama.VelocityTemplateEngine;
import org.generama.WriterMapper;

import com.thoughtworks.qdox.model.JavaClass;

/**
 * XDoclet plugin applied in conjunction with the FractalPrimitivePlugin.vm
 * template to generate the Component implementation.
 * 
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version $Revision$
 */
public class PrimitiveComponentPlugin extends AttributeControllerPlugin {
    /**
     * Primitive component glue generator using the Velocity template engine.
     * 
     * @param velocity the Java template engine.
     * @param qdox the qdox java model provider.
     * @param writer the file writer mapper.
     */
    public PrimitiveComponentPlugin(VelocityTemplateEngine velocity,
            QDoxCapableMetadataProvider qdox, WriterMapper writer) {
        super(velocity, qdox, writer);
//        setFilereplace("Fc$1.java");
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.generama.Plugin#shouldGenerate(java.lang.Object)
     */
    public boolean shouldGenerate(final Object obj) {
        if (!shouldGenerate) return false;
        JavaClass cls = (JavaClass) obj;
        setDestination(componentPackage(cls), componentName(cls), "java");
        debug("Parsing "+cls.getFullyQualifiedName()+" ["+componentFullname(cls)+"]...("+isPrimitive(cls)+"&"+isComponent(cls)+"&"+hasGlue(cls)+")");
        if (!isPrimitive(cls) || !isComponent(cls) || !hasGlue(cls)) return false;
        debug("Generating "+getDestinationPackage(cls)+"/"+getDestinationFilename(cls)+"...");
        return true;
    }

    /**
     * Provides the list of implemented interfaces.
     * 
     * @param cls the class introspected.
     * @return the implements clause.
     */
    public String implementsClause(final JavaClass cls) {
        StringBuffer clause = new StringBuffer();
        if (hasAttributes(cls, true))
            clause.append(", " + attributeController(cls));
        if (hasBindings(cls, true) || hasReflects(cls, true))
            clause.append(", BindingController");
        if (hasLifecycles(cls, true))
            clause.append(", LifeCycleController");
        if (hasLogger(cls, true))
            clause.append(", Loggable");
        if (clause.length() == 0)
            return "";
        return "implements " + clause.substring(2);
    }
}