/*==============================================================================
Fractal XDoclet Plugin - Copyright (C) 2002-2006 INRIA Futurs / LIFL
Fractal Component Model (contact: fractal@objectweb.org)

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free 
Software Foundation; either version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along 
with this library; if not, write to the Free Software Foundation, Inc., 
59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

Initial developer(s): Romain Rouvoy (romain.rouvoy@lifl.fr)
Contributor(s): .
--------------------------------------------------------------------------------
$Id$
==============================================================================*/
package org.objectweb.fractal.fraclet.doclets;


/**
 * Field annotation describing a Fractal client interface.
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version $Revision$
 * @qtags.location field
 * @qtags.deprecated This tag is deprecated please use the requires tag instead.
 */
public interface FractalBcTag extends FractalInterface {
    /**
     * Provides the contingency of the Fractal interface (default is mandatory).
     * @return the contingency of the interface.
     * @qtags.allowed-value mandatory
     * @qtags.allowed-value optional
     * @qtags.default mandatory
     */
    String getContingency();
    
    /**
     * Provides the cardinality of the Fractal interface (default is singleton).
     * @return the cardinality of the interface.
     * @qtags.allowed-value singleton
     * @qtags.allowed-value collection
     * @qtags.default singleton
     */
    String getCardinality();
}