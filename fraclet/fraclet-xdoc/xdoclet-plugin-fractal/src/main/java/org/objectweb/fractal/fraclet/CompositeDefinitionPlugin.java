/*==============================================================================
 Fractal XDoclet Plugin - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy (romain.rouvoy@lifl.fr)
 Contributor(s): .
 --------------------------------------------------------------------------------
 $Id$
 ==============================================================================*/
package org.objectweb.fractal.fraclet;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import org.generama.JellyTemplateEngine;
import org.generama.QDoxCapableMetadataProvider;
import org.generama.WriterMapper;
import org.objectweb.fractal.fraclet.doclets.ProvidesTag;

import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaField;

/**
 * XDoclet plugin applied in conjunction with the FractalAdlPlugin.jelly
 * template to generate primitive component descriptor file.
 * 
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version $Revision$
 */
public class CompositeDefinitionPlugin extends PrimitiveDefinitionPlugin {
    protected static final String ASSEMBLY_SUFFIX = "Composite";

    /**
     * Composite definition generator for abstract composite.
     * 
     * @param jelly the jelly XML template engine.
     * @param qdox the qdox java model provider.
     * @param writer the file writer mapper.
     */
    public CompositeDefinitionPlugin(JellyTemplateEngine jelly,
            QDoxCapableMetadataProvider qdox, WriterMapper writer) {
        super(jelly, qdox, writer);
        // setFilereplace("$1.fractal");
    }

    /**
     * param cls the java class source file parsed.
     * 
     * @return true if a FractalADL file should be generated.
     */
    public boolean shouldGenerate(final Object obj) {
        JavaClass cls = (JavaClass) obj;
        setDestination(componentPackage(cls), compositeName(cls), "fractal");
        debug("Parsing " + cls.getFullyQualifiedName() + " ["
                + compositeFullname(cls) + "]...");
        if (!isComponent(cls) || !hasBindings(cls, true))
            return false;
        debug("Generating " + getDestinationPackage(cls) + "/"
                + getDestinationFilename(cls) + "...");
        return true;
    }

    public String compositeName(final JavaClass cls) {
        return definitionName(cls) + ASSEMBLY_SUFFIX;
    }

    public String compositeFullname(final JavaClass cls) {
        return (hasPackage(cls)) ? componentPackage(cls) + "."
                + compositeName(cls) : compositeName(cls);
    }

    /**
     * Provides the instance of the class element named name.
     * 
     * @param name the name of the class to find.
     * @return the associated Java model element (null if not found).
     */
    public JavaClass interfaceClass(final String name) {
        for (Iterator i = this.getMetadata().iterator(); i.hasNext();) {
            JavaClass cls = (JavaClass) i.next();
            if (name.equals(cls.getFullyQualifiedName()))
                return cls;
        }
        return null;
    }

    /**
     * Provides the name of a server interface compatible with its reference.
     * 
     * @param fld the reference of a server interface.
     * @return the name of the server interface.
     */
    public String abstractInterfaceName(JavaField fld) {
        String name = bindingSignature(fld);
        JavaClass itf = interfaceClass(name);
        if (itf != null)
            return interfaceName(itf, (ProvidesTag) itf.getTagByName(PROVIDES,
                    true));
        int dot = name.lastIndexOf(".");
        return dot < 0 ? name.toLowerCase() : name.substring(dot + 1)
                .toLowerCase();
    }

    /**
     * Provides the list of arguments required by the component.
     * 
     * @param cls the java class source file parsed.
     * @return a comma separated list of argument for the component (null if no
     *         arguments).
     */
    public String argumentsDefinition(final JavaClass cls) {
        String primitiveArguments = super.argumentsDefinition(cls);
        StringBuffer buffer = new StringBuffer();
        if (primitiveArguments != null)
            buffer.append(primitiveArguments);
        buffer.append(DEFINITION_SEP + "client=client");
        JavaField[] bindings = bindings(cls, true);
        for (int i = 0; i < bindings.length; i++)
            buffer.append(DEFINITION_SEP + bindingName(bindings[i]) + "="
                    + bindingName(bindings[i]));
        if (primitiveArguments == null)
            return buffer.substring(DEFINITION_SEP.length());
        return buffer.toString();
    }

    public String primitiveParameters(final JavaClass cls) {
        return super.definitionParameters(cls);
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.fraclet.PrimitiveDefinitionPlugin#extendsElement(com.thoughtworks.qdox.model.JavaClass)
     */
    protected String extendsElement(final JavaClass cls) {
        if (cls == null)
            return "";
        if (isComponent(cls) && hasBindings(cls, true))
            return DEFINITION_SEP + compositeFullname(cls)
                    + definitionParameters(cls);
        if (isComponent(cls))
            return "";
        return super.extendsElement(cls);
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.fraclet.PrimitiveDefinitionPlugin#definitionParameters(com.thoughtworks.qdox.model.JavaClass)
     */
    public String definitionParameters(final JavaClass cls) {
        String def = super.definitionParameters(cls);
        StringBuffer buffer = new StringBuffer();
        if (def.length() > 0)
            buffer.append(DEFINITION_SEP + def.substring(1, def.length() - 1));
        JavaField[] bindings = bindings(cls, true);
        if (bindings.length > 0)
            buffer.append(DEFINITION_SEP + "client=>${client}");
        for (int i = 0; i < bindings.length; i++)
            buffer.append(DEFINITION_SEP + bindingName(bindings[i]) + "=>${"
                    + bindingName(bindings[i]) + "}");
        return buffer.length() > 0 ? "("
                + buffer.substring(DEFINITION_SEP.length()) + ")" : "";
    }

    public String asArgument(String s) {
        return "${" + s + "}";
    }

    protected Collection classTags(final JavaClass cls, final String tag,
            final boolean itf) {
        HashSet result = new HashSet();
        if (cls.getTagByName(tag) != null)
            result.add(cls);
        if (cls.getSuperJavaClass() != null)
            result.addAll(classTags(cls.getSuperJavaClass(), tag, true));
        if (!itf) return result;
        JavaClass[] itfs = cls.getImplementedInterfaces();
        for (int i = 0; i < itfs.length; i++)
            result.addAll(classTags(itfs[i], tag, true));
        return result;
    }

    public JavaClass[] providedInterfaces(final JavaClass cls,
            final boolean itf) {
        Collection prov = classTags(cls, PROVIDES, itf);
        Collection res = new HashSet();
        for (Iterator i = prov.iterator();i.hasNext();) {
            JavaClass c = (JavaClass) i.next();
            if (cls==c || !hasBindings(c, false))
                res.add(c);
        }
        return (JavaClass[]) res.toArray(new JavaClass[res.size()]);
    }
}