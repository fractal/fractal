/*==============================================================================
Fractal XDoclet Plugin - Copyright (C) 2002-2006 INRIA Futurs / LIFL
Fractal Component Model (contact: fractal@objectweb.org)

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free 
Software Foundation; either version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along 
with this library; if not, write to the Free Software Foundation, Inc., 
59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

Initial developer(s): Romain Rouvoy (romain.rouvoy@lifl.fr)
Contributor(s): .
--------------------------------------------------------------------------------
$Id$
==============================================================================*/
package org.objectweb.fractal.fraclet;

import java.util.StringTokenizer;

import org.generama.QDoxCapableMetadataProvider;
import org.generama.VelocityTemplateEngine;
import org.generama.WriterMapper;
import org.objectweb.fractal.fraclet.doclets.LoggerTag;

/**
 * XDoclet plugin applied to applications to generate monolog.properties file automatically.
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version $Revision$
 */
public class MonologConfigurationPlugin extends FractalPlugin {
    public final static String DEFAULT_WRAPPER = "org.objectweb.util.monolog.wrapper.log4j.MonologLoggerFactory"; 

    /**
     * Monolog configuration file generator using the Velocity template engine.
     * @param velocity the Properties template engine.
     * @param qdox the qdox java model provider.
     * @param writer the file writer mapper.
     */
    public MonologConfigurationPlugin(VelocityTemplateEngine velocity,
            QDoxCapableMetadataProvider qdox, WriterMapper writer) {
        super(velocity, qdox, writer);
        setMultioutput(false);
        setDestination("", "monolog", "properties");
    }
    
    private String wrapper = DEFAULT_WRAPPER;
    
    /**
     * Provides the Monolog wrapper to use.
     * @return the logging wrapper class.
     */
    public String getWrapper() {
        return this.wrapper;
    }
    
    /**
     * Defines the Monolog wrapper to use.
     * @param name the logging wrapper class.
     */
    public void setWrapper(final String name) {
        this.wrapper = name;
    }
    
    /**
     * Provides the list of handlers used by the logger.
     * @param tag the logger.
     * @return an array containing the name of the handlers to use.
     */
    public String[] handlers(final LoggerTag tag) {
        StringTokenizer tokens = new StringTokenizer(tag.getHandler().trim(),",");
        String[] handlers = new String[tokens.countTokens()];
        for (int i=0;i<handlers.length;i++)
            handlers[i]=i+" "+tokens.nextToken();
        return handlers;
    }
}