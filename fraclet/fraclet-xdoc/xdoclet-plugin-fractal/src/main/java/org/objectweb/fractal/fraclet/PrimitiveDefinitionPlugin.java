/*==============================================================================
 Fractal XDoclet Plugin - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy (romain.rouvoy@lifl.fr)
 Contributor(s): .
 --------------------------------------------------------------------------------
 $Id$
 ==============================================================================*/
package org.objectweb.fractal.fraclet;

import java.util.Collection;
import java.util.Iterator;

import org.generama.JellyTemplateEngine;
import org.generama.QDoxCapableMetadataProvider;
import org.generama.WriterMapper;
import org.objectweb.fractal.fraclet.doclets.AttributeTag;
import org.objectweb.fractal.fraclet.doclets.ComponentTag;
import org.objectweb.fractal.fraclet.doclets.ProvidesTag;

import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaField;

/**
 * XDoclet plugin applied in conjunction with the FractalAdlPlugin.jelly
 * template to generate primitive component descriptor file.
 * 
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version $Revision$
 */
public class PrimitiveDefinitionPlugin extends FractalPlugin {
    /** Definition list separator. */
    protected static final String DEFINITION_SEP = ",";

    /**
     * Composite definition generator for abstract composite.
     * 
     * @param jelly the jelly XML template engine.
     * @param qdox the qdox java model provider.
     * @param writer the file writer mapper.
     */
    public PrimitiveDefinitionPlugin(JellyTemplateEngine jelly,
            QDoxCapableMetadataProvider qdox, WriterMapper writer) {
        super(jelly, qdox, writer);
        // setFileregex("(.*)(.java)");
        setMultioutput(true);
        // setFilereplace("$1.fractal");
        setValidate(false); // FractalADL DOCTYPE is not XML compliant.
    }

    /* (non-Javadoc)
     * @see org.generama.defaults.QDoxPlugin#shouldGenerate(java.lang.Object)
     */
    public boolean shouldGenerate(final Object obj) {
        if (!shouldGenerate)
            return false;
        JavaClass cls = (JavaClass) obj;
        setDestination(componentPackage(cls), definitionName(cls), "fractal");
        debug("Parsing " + cls.getFullyQualifiedName() + " ["
                + definitionFullname(cls) + "]...");
        if (!isComponent((JavaClass) obj) && !isInterface((JavaClass) obj))
            return false;
        debug("Generating " + getDestinationPackage(cls) + "/"
                + getDestinationFilename(cls) + "...");
        return true;
    }

    /**
     * Provides the short name of the component depending on the filename.
     * 
     * @param cls the java class source file parsed.
     * @return the fully qualified name of the associated definition.
     */
    public String definitionName(final JavaClass cls) {
        ComponentTag tag = (ComponentTag) cls.getTagByName(COMPONENT, false);
        if (tag == null || tag.getName_() == null)
            return cls.getName();
        return tag.getName_().substring(tag.getName_().lastIndexOf(".") + 1);
    }

    /**
     * Provides the fully qualified name of the component depending on the
     * filename.
     * 
     * @param cls the java class source file parsed.
     * @return the fully qualified name of the associated definition.
     */
    public String definitionFullname(final JavaClass cls) {
        return packagePrefix(cls) + definitionName(cls);
    }

    /**
     * Adds a definition extended.
     * 
     * @param cls the java class source file parsed.
     * @return the associated extend clause (empty if no extend).
     */
    protected String extendsElement(final JavaClass cls) {
        if (cls != null && (isComponent(cls) || isInterface(cls)))
            return DEFINITION_SEP + definitionFullname(cls)
                    + definitionParameters(cls);
        return "";
    }

    /**
     * Provides the parameters of the component definition.
     * 
     * @param cls the class considered.
     * @return the list of parameters.
     */
    public String definitionParameters(final JavaClass cls) {
        JavaField[] attrs = attributes(cls, true);
        StringBuffer args = new StringBuffer();
        for (int i = 0; i < attrs.length; i++) {
            // AttributeTag tag = (AttributeTag)
            // attrs[i].getTagByName(ATTRIBUTE_NAME);
            // if (tag.getValue_() == null)
            // args.append(DEFINITION_SEP +
            // attributeName(attrs[i])+"=>"+attributeValue(attrs[i]));
            args.append(DEFINITION_SEP + attributeName(attrs[i]) + "=>${"
                    + attributeName(attrs[i]) + "}");
        }
        return args.length() > 0 ? "("
                + args.substring(DEFINITION_SEP.length()) + ")" : "";
    }

    /**
     * Provides the list of extended definitions.
     * 
     * @param cls the java class source file parsed.
     * @return a comma separated list of extended component definitions (null if
     *         no extended definition).
     */
    public String extendsDefinition(final JavaClass cls) {
        StringBuffer buffer = new StringBuffer();
        JavaClass[] itfs = extend(cls);
        for (int i = 0; i < itfs.length; i++)
            buffer.append(extendsElement(itfs[i]));
        Collection legacy = listLegacyDefinition(cls,false);
        for (Iterator i= legacy.iterator();i.hasNext();)
            buffer.append(DEFINITION_SEP + i.next());
        if (buffer.length() != 0)
            return buffer.substring(DEFINITION_SEP.length());
        return null;
    }

    /**
     * Provides the name of the associated interface.
     * 
     * @param cls the java class source file parsed.
     * @param tag the FractalItfTag associated.
     * @return the name of the interface.
     */
    public String interfaceName(final JavaClass cls, final ProvidesTag tag) {
        if (tag == null)
            return cls.getName().toLowerCase();
        if (tag.getName_() != null)
            return tag.getName_();
        String signature = interfaceSignature((JavaClass) tag.getContext(), tag);
        int dot = signature.lastIndexOf(".");
        return dot < 0 ? signature.toLowerCase() : signature.substring(dot + 1)
                .toLowerCase();
    }

    /**
     * Provides the signature of the associated interface.
     * 
     * @param cls the java class source file parsed.
     * @param tag the FractalItfTag associated.
     * @return the signature of the interface.
     */
    public String interfaceSignature(final JavaClass cls, final ProvidesTag tag) {
        if (tag.getSignature() != null)
            return tag.getSignature();
        return cls.getFullyQualifiedName();
    }

    /**
     * Provides the list of arguments required by the component.
     * 
     * @param cls the java class source file parsed.
     * @return a comma separated list of argument for the component (null if no
     *         arguments).
     */
    public String argumentsDefinition(final JavaClass cls) {
        JavaField[] attrs = attributes(cls, true);
        StringBuffer mandArgs = new StringBuffer();
        StringBuffer optArgs = new StringBuffer();
        for (int i = 0; i < attrs.length; i++) {
            AttributeTag tag = (AttributeTag) attrs[i].getTagByName(ATTRIBUTE_NAME);
            if (tag.getValue_() != null)
                optArgs.append(DEFINITION_SEP + attributeName(attrs[i]) + "=" + tag.getValue_());
            else
                mandArgs.append(DEFINITION_SEP + attributeName(attrs[i]));
        }
        String res = mandArgs.toString() + optArgs.toString();
        return res.length() > 0 ? res.substring(DEFINITION_SEP.length()): null;
    }

    /**
     * Provides the ADL value of the component attribute.
     * 
     * @param fld the java field source file parsed.
     * @return the name of the associated attribute.
     */
    public String attributeValue(final JavaField fld) {
        return "${" + attributeName(fld) + "}";
    }

    /**
     * Provides the name of the content class implementing the component.
     * 
     * @return the name of the primitive component definition.
     */
    public String contentClass(final JavaClass cls) {
        if (hasGlue(cls))
            return componentFullname(cls);
        return cls.getFullyQualifiedName();
    }
}