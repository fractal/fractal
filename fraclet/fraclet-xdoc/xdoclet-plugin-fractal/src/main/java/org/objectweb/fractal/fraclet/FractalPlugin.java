/* =============================================================================
 * Fractal XDoclet Plugin - Copyright (C) 2002-2006 INRIA Futurs / LIFL Fractal
 * Component Model (contact: fractal@objectweb.org)
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Initial developer(s): Romain Rouvoy (romain.rouvoy@lifl.fr) 
 * Contributor(s): .
 * -----------------------------------------------------------------------------
 * $Id$
 * =============================================================================
 */
package org.objectweb.fractal.fraclet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.generama.JellyTemplateEngine;
import org.generama.QDoxCapableMetadataProvider;
import org.generama.VelocityTemplateEngine;
import org.generama.WriterMapper;
import org.objectweb.fractal.fraclet.doclets.AttributeTag;
import org.objectweb.fractal.fraclet.doclets.AttributeTagImpl;
import org.objectweb.fractal.fraclet.doclets.ComponentTag;
import org.objectweb.fractal.fraclet.doclets.ComponentTagImpl;
import org.objectweb.fractal.fraclet.doclets.ControlTagImpl;
import org.objectweb.fractal.fraclet.doclets.DataTagImpl;
import org.objectweb.fractal.fraclet.doclets.LegacyTag;
import org.objectweb.fractal.fraclet.doclets.LegacyTagImpl;
import org.objectweb.fractal.fraclet.doclets.LifecycleTagImpl;
import org.objectweb.fractal.fraclet.doclets.LoggerTag;
import org.objectweb.fractal.fraclet.doclets.LoggerTagImpl;
import org.objectweb.fractal.fraclet.doclets.ProvidesTagImpl;
import org.objectweb.fractal.fraclet.doclets.RequiresTag;
import org.objectweb.fractal.fraclet.doclets.RequiresTagImpl;

import com.thoughtworks.qdox.model.DocletTag;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaField;
import com.thoughtworks.qdox.model.JavaMethod;
import com.thoughtworks.qdox.model.Type;

/**
 * Abstract Fractal plugin supporting Jelly and Velocity as template engines.
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version $Revision$
 */
public abstract class FractalPlugin extends Plugin {
    /** Fractal control interface package exluded by the generation engine. */
    protected static final String FC_CONTROL = "org.objectweb.fractal.api.control";
    protected static final String ATTR_NAME = "Attributes";
    protected static final String ATTR_CTRL = FC_CONTROL + ".AttributeController";
    protected static final String COMP_PREFIX = "Fc";

    public FractalPlugin(JellyTemplateEngine jelly,
            QDoxCapableMetadataProvider qdox, WriterMapper writer) {
        super(jelly, qdox, writer);
    }

    public FractalPlugin(VelocityTemplateEngine velocity,
            QDoxCapableMetadataProvider qdox, WriterMapper writer) {
        super(velocity, qdox, writer);
    }

    /*------------------------------------------------------------------------*/
    /* COMPONENT CONFIGURATION */
    /*------------------------------------------------------------------------*/
    protected static final String COMPONENT = ComponentTagImpl.NAME;

    /**
     * Provides the short name of the component depending on the filename.
     * @param cls the java class source file parsed.
     * @return the fully qualified name of the associated definition.
     */
    public String componentName(final JavaClass cls) {
        ComponentTag tag = (ComponentTag) cls.getTagByName(COMPONENT, false);
        if (tag == null || tag.getName_() == null)
            return hasGlue(cls)?COMP_PREFIX+cls.getName():cls.getName();
        return tag.getName_().substring(tag.getName_().lastIndexOf(".")+1);
    }

    /**
     * Provides the label of the component's package.
     * @param cls the class associated to the component.
     * @return the package of the component (an empty string if no package).
     */
    public String componentPackage(final JavaClass cls){
        ComponentTag tag = (ComponentTag) cls.getTagByName(COMPONENT, false);
        if (tag == null || tag.getName_() == null) 
            return (cls.getPackage()==null)?"":cls.getPackage();
        if (tag.getName_().lastIndexOf(".")==-1) return "";
        return tag.getName_().substring(0,tag.getName_().lastIndexOf("."));
    }
    
    /*------------------------------------------------------------------------*/
    /* COMPONENT HANDLING */
    /*------------------------------------------------------------------------*/
    /**
     * Checks if the class defines a package.
     * @param cls the class to check.
     * @return true if a package is defined.
     */
    public boolean hasPackage(final JavaClass cls) {
        return !componentPackage(cls).equals("");
    }
    
    /**
     * Provides the prefix of the package (empty if no package).
     * @param cls the class used.
     * @return the prefix of the package (e.g., org.objectweb.fractal. ).
     */
    public String packagePrefix(final JavaClass cls) {
        return hasPackage(cls) ? componentPackage(cls) + "." : "";
    }

    /**
     * Provides the fully qualified name of the component depending on the
     * filename.
     * @param cls the java class source file parsed.
     * @return the fully qualified name of the associated definition.
     */
    public String componentFullname(final JavaClass cls) {
        return packagePrefix(cls) + componentName(cls);
    }

    /**
     * Checks if a component glue need to be generated.
     * @param cls the class to check.
     * @return true if some component glue should be generated.
     */
    public boolean hasGlue(final JavaClass cls) {
        debug("=> glue? binding="+hasBindings(cls, true)
                +" || attribute="+hasAttributes(cls, true)
                +" || control="+hasReflects(cls, true)
                +" || logger="+hasLogger(cls, true) 
                +" || lifecycle="+hasLifecycles(cls, true));
        return hasBindings(cls, true) || hasAttributes(cls, true)
               || hasReflects(cls, true) || hasLogger(cls, true) 
               || hasLifecycles(cls, true);
    }

    protected static final String PROVIDES = ProvidesTagImpl.NAME;

    /**
     * Checks if the class defines fractal interface(s).
     * @param cls the class to check.
     * @return true if at least one fractal interface is defined.
     */
    public boolean isInterface(final JavaClass cls) {
        return cls.getTagByName(PROVIDES, false) != null;
    }

    /**
     * Checks if the class defines fractal interface(s).
     * @param cls the class to check.
     * @return true if at least one fractal interface is defined.
     */
    public boolean isData(final JavaClass cls) {
        return cls.getTagByName(DataTagImpl.NAME, true) != null;
    }

    /**
     * Checks if the class is considered as a component.
     * @param cls the class to check.
     * @return true if cls or its parent has a doclet tag.
     */
    public boolean isComponent(final JavaClass cls) {
        return !(cls.getFullyQualifiedName().equalsIgnoreCase(OBJECT) || isController(cls) || cls.isInterface() || isData(cls));
    }

    /**
     * Checks if the class is a primitive component.
     * @param cls the class to check.
     * @return true if cls is a non-abstract class.
     */
    public boolean isPrimitive(final JavaClass cls) {
        return !(cls.isInner() || cls.isNative() || cls.isAbstract() || cls.isInterface());
    }

    /**
     * Checks if the class is a controller (attribute, binding, lifecycle,
     * etc.).
     * @param cls the class to check.
     * @return true if cls is an interface of the org.objectweb.fractal.control
     *         package.
     */
    public boolean isController(final JavaClass cls) {
        return (cls.isInterface() && packagePrefix(cls).startsWith(FC_CONTROL)) || isAttributeController(cls);
    }
    
    public boolean isLegacy(final JavaClass cls, Collection legacy) {
        return legacy.contains(cls.getFullyQualifiedName());
    }
    
    public Collection listLegacySignatures(final JavaClass cls, final boolean all) {
        DocletTag[] tags = cls.getTagsByName(LegacyTagImpl.NAME, all);
        Collection legacy = new HashSet();
        for (int i=0 ; i<tags.length ; i++) {
            LegacyTag tag = (LegacyTag) tags[i];
            if (tag.getSignature()!=null)
                legacy.add(tag.getSignature());
        }
        return legacy;
    }

    public Collection listLegacyDefinition(final JavaClass cls, final boolean all) {
        DocletTag[] tags = cls.getTagsByName(LegacyTagImpl.NAME, all);
        Collection legacy = new HashSet();
        for (int i=0 ; i<tags.length ; i++) {
            LegacyTag tag = (LegacyTag) tags[i];
            if (tag.getDefinition()!=null)
                legacy.add(tag.getDefinition());
        }
        return legacy;
    }

    
    /**
     * Checks if the interface is an attribute controller.
     * @param cls the java class source file parsed.
     * @return true if the java class source file describes an attribute
     *         controller.
     */
    public boolean isAttributeController(final JavaClass cls) {
        return cls.isA(ATTR_CTRL) && cls.isInterface();
    }

    /*------------------------------------------------------------------------*/
    /* CLASSES HANDLING */
    /*------------------------------------------------------------------------*/
    /**
     * Provides a list of business classes at the first level.
     * 
     * @param cls the class considered.
     * @return the list of business interfaces and class.
     */
    protected JavaClass[] extend(final JavaClass cls) {
        ArrayList list = new ArrayList();
        JavaClass[] itfs = cls.getImplementedInterfaces();
        for (int i = 0; i < itfs.length; i++)
            if (isInterface(itfs[i])) list.add(itfs[i]);
        JavaClass sup = cls.getSuperJavaClass();
        Collection legacy = listLegacySignatures(cls,true);
        if ((sup != null) && isComponent(sup) && !isLegacy(sup, legacy)) 
            list.add(sup);
        return (JavaClass[]) list.toArray(new JavaClass[list.size()]);
    }

    /*------------------------------------------------------------------------*/
    /* BINDING FEATURE */
    /*------------------------------------------------------------------------*/
    protected static final String REQUIRES = RequiresTagImpl.NAME;

    private final Map[] bindings = { new HashMap(), new HashMap() };

    /**
     * Provides the list of bindings defined by the class.
     * @param cls the java class source file parsed.
     * @param all performs the search recursively.
     * @return the list of fields associated to fractal client interfaces.
     */
    public JavaField[] bindings(final JavaClass cls, final boolean all) {
        JavaField[] res = (JavaField[]) bindings[toInt(all)].get(cls);
        if (res != null) return res;
        Collection c = fields(cls, REQUIRES, all);
        res = (JavaField[]) c.toArray(new JavaField[c.size()]);
        for (int i = 0; i < res.length; i++) {
            if (res[i].isPrivate())
                failure(res[i], "Required interface \'" + bindingName(res[i]) + "\' can not use a private modifier");
            Type t = res[i].getType();
            if (t.isPrimitive() || t.isArray() || t.getValue().equals("java.lang.String"))
                failure(res[i], "Required interface \'" + bindingName(res[i]) + "\' should refer to an interface");
        }
        bindings[toInt(all)].put(cls, res);
        return res;
    }

    /**
     * Checks if the class defines some binding.
     * @param cls the java class source file parsed.
     * @param all performs the search recursively.
     * @return true if the java class has fractal client interfaces.
     */
    public boolean hasBindings(final JavaClass cls, final boolean all) {
        return bindings(cls, all).length > 0;
    }

    /**
     * Computes the binding name (the field name if no tag specified).
     * @param f the java field source file parsed.
     * @return the name of the binding.
     */
    public String bindingName(final JavaField f) {
        RequiresTag t = (RequiresTag) f.getTagByName(REQUIRES);
        return t.getName_() != null ? t.getName_() : f.getName();
    }

    /**
     * Computes the binding signature (the field signature if no tag specified).
     * @param f the java field source file parsed.
     * @return the signature of the binding.
     */
    public String bindingSignature(final JavaField f) {
        RequiresTag t = (RequiresTag) f.getTagByName(REQUIRES);
        return t.getSignature() != null ? t.getSignature() : f.getType().getValue();
    }

    /*------------------------------------------------------------------------*/
    /* ATTRIBUTE FEATURE */
    /*------------------------------------------------------------------------*/
    protected static final String ATTRIBUTE_NAME = AttributeTagImpl.NAME;

    private final Map[] attributes = { new HashMap(), new HashMap() };

    /**
     * Provides the list of component attribute fields.
     * @param cls the java class source file parsed.
     * @param all performs the search recursively.
     * @return the list of fields associated to fractal attributes.
     */
    public JavaField[] attributes(final JavaClass cls, final boolean all) {
        JavaField[] res = (JavaField[]) attributes[toInt(all)].get(cls);
        if (res != null) return res;
        Collection c = fields(cls, ATTRIBUTE_NAME, all);
        res = (JavaField[]) c.toArray(new JavaField[c.size()]);
        for (int i = 0; i < res.length; i++) {
            if (res[i].isPrivate())
                failure(res[i], "attribute \'" + attributeName(res[i]) + "\' can't use private modifier");
            Type t = res[i].getType();
            if (!(t.isPrimitive() || t.isArray() || t.getValue().equals("java.lang.String")))
                failure(res[i], "attribute \'" + attributeName(res[i]) + "\' should be primitive");
        }
        attributes[toInt(all)].put(cls, res);
        return res;
    }

    /**
     * Checks if the component defines some attribute.
     * @param cls the java class source file parsed.
     * @param all performs the search recursively.
     * @return true if the JavaClass has fractal attributes.
     */
    public boolean hasAttributes(final JavaClass cls, final boolean all) {
        return attributes(cls, all).length > 0;
    }
    
    /**
     * Provides the fullname of the attribute-controller associated to the component.
     * @param cls the component.
     * @return the attribute-controller fullname.
     */
    public String attributeControllerName(final JavaClass cls) {
        return cls.getName() + ATTR_NAME;
    }

    /**
     * Provides the fullname of the attribute-controller associated to the component.
     * @param cls the component.
     * @return the attribute-controller fullname.
     */
    public String attributeControllerFullname(final JavaClass cls) {
        return cls.getFullyQualifiedName() + ATTR_NAME;
    }
    
    /**
     * Return the name of super attribute controller.
     * @param cls the class considered.
     * @return the name of the nearer class needing an attribute controller.
     */
    public String superAttributeController(final JavaClass cls) {
        JavaClass sup = cls.getSuperJavaClass();
        if (sup == null) return null;
        if (sup.getFullyQualifiedName().equals(OBJECT)) return ATTR_CTRL;
        if (hasAttributes(sup, false)) return attributeControllerFullname(sup);
        return superAttributeController(sup);
    }

    /**
     * Returns the name of the attribute controller.
     * @param cls the class considered.
     * @return the name of the nearer class needing an attribute controller.
     */
    public String attributeController(final JavaClass cls) {
        if (hasAttributes(cls, false)) return attributeControllerFullname(cls);
        return hasAttributes(cls, true) ? superAttributeController(cls) : "";
    }

    /**
     * Computes the name of the attribute (field name if no tag specified).
     * 
     * @param f the java field source file parsed.
     * @return the name of the associated attribute.
     */
    public String attributeName(final JavaField f) {
        AttributeTag t = (AttributeTag) f.getTagByName(ATTRIBUTE_NAME);
        return t.getName_() != null ? t.getName_().toLowerCase() : f.getName().toLowerCase();
    }

    /*------------------------------------------------------------------------*/
    /* REFLECTING FEATURE */
    /*------------------------------------------------------------------------*/
    protected static final String CONTROL = ControlTagImpl.NAME;

    /**
     * Provides the list of component control fields.
     * @param cls the java class source file parsed.
     * @param all performs the search recursively.
     * @return the list of fields associated to fractal client interfaces.
     */
    public JavaField[] reflects(final JavaClass cls, final boolean all) {
        Collection c = fields(cls, CONTROL, all);
        return (JavaField[]) c.toArray(new JavaField[c.size()]);
    }

    /**
     * Checks if the component defines some control fields.
     * @param cls the java class source file parsed.
     * @param all performs the search recursively.
     * @return true if the java class has fractal client interfaces.
     */
    public boolean hasReflects(final JavaClass cls, final boolean all) {
        return reflects(cls, all).length > 0;
    }

    /*------------------------------------------------------------------------*/
    /* LOGGING FEATURE */
    /*------------------------------------------------------------------------*/
    protected static final String LOGGER_NAME = LoggerTagImpl.NAME;

    /**
     * Provides the reference to the last logger declared.
     * @param cls the reference to the logger.
     * @param all performs the search recursively.
     * @return the field describing the logger.
     */
    public JavaField[] loggers(final JavaClass cls, final boolean all) {
        Collection c = fields(cls, LOGGER_NAME, all);
        return (JavaField[]) c.toArray(new JavaField[c.size()]);
    }

    /**
     * Checks if the class defines a logger.
     * @param cls the class to check.
     * @param all performs the search recursively.
     * @return true if a logger is defined in this class (use logger(cls) to
     *         retrieve it).
     */
    public boolean hasLogger(final JavaClass cls, final boolean all) {
        return loggers(cls, all).length > 0;
    }

    /**
     * Provides the name of the logger associated to the field.
     * @param f the field referencing the logger.
     * @return the name of the logger.
     */
    public String loggerName(final JavaField f) {
        LoggerTag t = (LoggerTag) f.getTagByName(LOGGER_NAME);
        return t.getName_() != null ? t.getName_() : ((JavaClass) f.getParent()).getName();
    }

    /*------------------------------------------------------------------------*/
    /* LOGGING FEATURE */
    /*------------------------------------------------------------------------*/
    protected static final String LIFECYCLE = LifecycleTagImpl.NAME;

    /**
     * Provides the list of lifecycle handlers.
     * @param cls the java class source file parsed.
     * @param all performs the search recursively.
     * @return the list of methods that handle a lifecyle step.
     */
    public JavaMethod[] lifecycles(final JavaClass cls, final boolean all) {
        Collection c = methods(cls, LIFECYCLE, all);
        return (JavaMethod[]) c.toArray(new JavaMethod[c.size()]);
    }

    /**
     * Checks if the component defines some lifecycle handler.
     * @param cls the java class source file parsed.
     * @param all performs the search recursively.
     * @return true if the java class has at least one lifecycle handler.
     */
    public boolean hasLifecycles(final JavaClass cls, final boolean all) {
        return lifecycles(cls, all).length > 0;
    }
}