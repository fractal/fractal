/*==============================================================================
Fractal XDoclet Plugin - Copyright (C) 2002-2006 INRIA Futurs / LIFL
Fractal Component Model (contact: fractal@objectweb.org)

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free 
Software Foundation; either version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along 
with this library; if not, write to the Free Software Foundation, Inc., 
59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

Initial developer(s): Romain Rouvoy (romain.rouvoy@lifl.fr)
Contributor(s): .
--------------------------------------------------------------------------------
$Id$
==============================================================================*/
package org.objectweb.fractal.fraclet.doclets;

import com.thoughtworks.qdox.model.DocletTag;


/**
 * Class annotation describing a custom Monolog handler.
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version $Revision$
 * @qtags.location class
 */
public interface MonologHandlerTag extends DocletTag {
    /**
     * Provides the name of the handler.
     * @return name of the handler.
     * @qtags.required
     */
    String getName_() ;
    
    /**
     * Provides the Type of the handler.
     * @return the type of the handler (default is Console).
     * @qtags.default Console
     */
    String getType();
    
    /**
     * Provides the output to use.
     * @return the output to use.
     * @qtags.default System.out
     */
    String getOutput();
    
    /**
     * Provides the output pattern.
     * @return the output pattern.
     * @qtags.default %l: %m%n
     */
    String getPattern();
    
    /**
     * The maximal size in bytes of the log file.
     * @return the maximal size in bytes of the log file to use.
     */
    int getMaxSize();
    
    /**
     * The maximal number of rolling files to use.
     * @return the maximal number of rolling files to use.
     */
    int getFileNumber();

    /**
     * The append mode used.
     * @return true if the appendMode flag is enabled.
     * @qtags.default false
     */
    boolean getAppendMode();
}