/*==============================================================================
 Fractal XDoclet Plugin - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy (romain.rouvoy@lifl.fr)
 Contributor(s): .
 --------------------------------------------------------------------------------
 $Id$
 ==============================================================================*/
package org.objectweb.fractal.fraclet;

import org.generama.QDoxCapableMetadataProvider;
import org.generama.VelocityTemplateEngine;
import org.generama.WriterMapper;

import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaField;

/**
 * XDoclet plugin applied in conjunction with the FractalAttributePlugin.vm
 * template to generate the AttributeController interface associated to a
 * component.
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version $Revision$
 */
public class AttributeControllerPlugin extends FractalPlugin {
    /**
     * Attribute controller generator using the Velocity template engine.
     * @param velocity the Java template engine.
     * @param qdox the qdox java model provider.
     * @param writer the file writer mapper.
     */
    public AttributeControllerPlugin(VelocityTemplateEngine velocity,
            QDoxCapableMetadataProvider qdox, WriterMapper writer) {
        super(velocity, qdox, writer);
//        setFileregex("(.*)(.java)");
        setMultioutput(true);
//        setFilereplace("Fc$1.java");
    }
    
    /* (non-Javadoc)
     * @see org.generama.defaults.QDoxPlugin#shouldGenerate(java.lang.Object)
     */
    public boolean shouldGenerate(final Object obj) {
        JavaClass cls = (JavaClass) obj;
        setDestination(componentPackage(cls), attributeControllerName(cls), "java");
        debug("Parsing "+cls.getFullyQualifiedName()+" ["+attributeControllerFullname(cls)+"]...");
        if (!isComponent(cls) || !hasAttributes(cls, false)) return false;
        debug("Generating "+getDestinationPackage(cls)+"/"+getDestinationFilename(cls)+"...");
        return true;
    }

    /**
     * Provides the name of the attribute for the getter/setter methods.
     * @param fld the java field source file parsed.
     * @return the name of the associated attribute (first character is upper
     *         case).
     */
    public String attributeMethod(final JavaField fld) {
        String name = attributeName(fld);
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }
}