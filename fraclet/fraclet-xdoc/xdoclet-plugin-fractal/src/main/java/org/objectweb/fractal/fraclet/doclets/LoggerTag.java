/*==============================================================================
 Fractal XDoclet Plugin - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy (romain.rouvoy@lifl.fr)
 Contributor(s): .
 --------------------------------------------------------------------------------
 $Id$
 ==============================================================================*/

package org.objectweb.fractal.fraclet.doclets;

import com.thoughtworks.qdox.model.DocletTag;

/**
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version $Revision$
 * @qtags.location field
 * @qtags.once
 */
public interface LoggerTag extends DocletTag {
    /**
     * Provides the name of the Fractal logger to use.
     * 
     * @return name of the Fractal controller reflected.
     */
    String getName_();

    /**
     * Provides the default logging level.
     * 
     * @return the default level to use (default is INFO).
     * @qtags.default INFO
     */
    String getLevel();

    /**
     * Provides the list of handlers to use
     * 
     * @return a comma separated list of handlers.
     * @qtags.default consoleHandler
     */
    String getHandler();

    /**
     * Provides the additivity flag used.
     * 
     * @return true if additivity is enabled.
     * @qtags.default true
     */
    boolean getAdditivity();

    /**
     * Provides the cleanHandlers flag used.
     * 
     * @return true if cleanHandlers is enabled.
     * @qtags.default true
     */
    boolean getCleanHandlers();
}
