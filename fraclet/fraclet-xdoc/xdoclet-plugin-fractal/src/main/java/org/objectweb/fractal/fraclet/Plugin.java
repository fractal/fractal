/* =============================================================================
 * Fractal XDoclet Plugin - Copyright (C) 2002-2006 INRIA Futurs / LIFL Fractal
 * Component Model (contact: fractal@objectweb.org)
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Initial developer(s): Romain Rouvoy (romain.rouvoy@lifl.fr) 
 * Contributor(s): .
 * -----------------------------------------------------------------------------
 * $Id$
 * =============================================================================
 */
package org.objectweb.fractal.fraclet;

import java.util.ArrayList;
import java.util.Collection;

import org.generama.JellyTemplateEngine;
import org.generama.QDoxCapableMetadataProvider;
import org.generama.VelocityTemplateEngine;
import org.generama.WriterMapper;
import org.generama.defaults.QDoxPlugin;
import org.objectweb.fractal.fraclet.doclets.TagLibrary;

import com.thoughtworks.qdox.model.AbstractJavaEntity;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaField;
import com.thoughtworks.qdox.model.JavaMethod;

/**
 * Abstract Fractal plugin supporting Jelly and Velocity as template engines.
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version $Revision$
 */
public class Plugin extends QDoxPlugin {
    protected static final String OBJECT = "java.lang.Object";
    protected boolean shouldGenerate = true;
    private String[] destination = null;
    
    /*------------------------------------------------------------------------*/
    /* CONSTRUCTORS */
    /*------------------------------------------------------------------------*/
    /**
     * Constructor used to generate XML files.
     * @param jelly Jelly template engine used to generate artifacts.
     * @param qdox QDox source code metadata provider.
     * @param writer the output writer.
     */
    public Plugin(JellyTemplateEngine jelly,
            QDoxCapableMetadataProvider qdox, WriterMapper writer) {
        super(jelly, qdox, writer);
        new TagLibrary(qdox);
        setPackageregex("^(.*)$");
        setFileregex("^(.*)$");
    }

    /**
     * Constructor used to generate Java or Property files.
     * @param velocity Velocity template engine used to generate artifacts.
     * @param qdox QDox source code metadata provider.
     * @param writer the output writer.
     */
    public Plugin(VelocityTemplateEngine velocity,
            QDoxCapableMetadataProvider qdox, WriterMapper writer) {
        super(velocity, qdox, writer);
        new TagLibrary(qdox);
    }

    /**
     * Method used to abort the generation process.
     * @param message the abort notification message.
     */
    protected void debug(String message) {
        //System.err.println(message);
    }

    /**
     * Method used to abort the generation process.
     * @param message the abort notification message.
     */
    protected void failure(AbstractJavaEntity entity, String message) {
        System.err.println(entity.getSource().getURL().getFile() + ":"
                + entity.getLineNumber() + ": " + message);
        this.shouldGenerate = false;
    }
    /* (non-Javadoc)
     * @see org.generama.defaults.QDoxPlugin#shouldGenerate(java.lang.Object)
     */
    public boolean shouldGenerate(final Object obj) {
        return this.shouldGenerate;
    }
    
    /**
     * Provides the destination characteristics of the artifact.
     * @param pkg the package
     * @param file the file
     * @param ext the extension
     */
    protected void setDestination(String pkg, String file, String ext) {
//        setPackagereplace(pkg);
//        setFilereplace(file+"."+ext);
        //debug("Setting destination to ["+pkg+","+file+","+ext+"]...");
        destination = new String[] {pkg,file+"."+ext};
    }
    
    /* (non-Javadoc)
     * @see org.generama.Plugin#getDestinationPackage(java.lang.Object)
     */
    public String getDestinationPackage(Object metadata) {
        if (destination !=null) return destination[0];
        debug("No destination package is defined");
        return super.getDestinationFilename(metadata);
    }

    /* (non-Javadoc)
     * @see org.generama.Plugin#getDestinationFilename(java.lang.Object)
     */
    public String getDestinationFilename(Object metadata) {
        if (destination !=null) return destination[1];
        debug("No destination filename is defined");
        return super.getDestinationFilename(metadata);
    }
    
    protected Collection classes(final JavaClass cls, final String doclet, final boolean all) {
        ArrayList result = new ArrayList();
        if (cls.getTagByName(doclet)!=null) result.add(cls);
        JavaClass[] itf = cls.getImplementedInterfaces();
        for (int i=0;i<itf.length;i++)
            result.addAll(classes(itf[i],doclet,all));
        if (all && (cls.getSuperJavaClass() != null))
            result.addAll(classes(cls.getSuperJavaClass(), doclet, all));
        return result;
    }
    
    /**
     * Provides a list of filtered fields.
     * @param cls the java class source file parsed.
     * @param doclet the name of the doclet to retrieve.
     * @param all performs the search recursively.
     * @return the list of fields having the doclet tag.
     */
    protected Collection fields(final JavaClass cls, final String doclet, final boolean all) {
        ArrayList result = new ArrayList();
        if (all && (cls.getSuperJavaClass() != null))
            result.addAll(fields(cls.getSuperJavaClass(), doclet, all));
        JavaField[] fields = cls.getFields();
        for (int i = 0; i < fields.length; i++)
            if (fields[i].getTagByName(doclet) != null)
                result.add(fields[i]);
        return result;
    }

    /**
     * Provides a list of filtered methods.
     * @param cls the java class source file parsed.
     * @param doclet the name of the doclet to retrieve.
     * @param all performs the search recursively.
     * @return the list of methods having the doclet tag.
     */
    protected Collection methods(final JavaClass cls, final String doclet, final boolean all) {
        ArrayList result = new ArrayList();
        if (all && (cls.getSuperJavaClass() != null))
            result.addAll(methods(cls.getSuperJavaClass(), doclet, all));
        JavaMethod[] methods = cls.getMethods();
        for (int i = 0; i < methods.length; i++)
            if (methods[i].getTagByName(doclet) != null)
                result.add(methods[i]);
        return result;
    }
    
    /**
     * Converts a boolean to an array index.
     * @param bool
     * @return 1 if bool==true and 0 else.
     */
    protected static int toInt(boolean bool) {
        return bool ? 1 : 0;
    }
}