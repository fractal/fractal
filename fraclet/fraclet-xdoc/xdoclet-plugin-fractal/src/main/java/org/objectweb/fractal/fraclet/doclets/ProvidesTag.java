/*==============================================================================
 Fractal XDoclet Plugin - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy (romain.rouvoy@lifl.fr)
 Contributor(s): .
 --------------------------------------------------------------------------------
 $Id$
 ==============================================================================*/
package org.objectweb.fractal.fraclet.doclets;

import com.thoughtworks.qdox.model.DocletTag;

/**
 * Class annotation describing a Fractal server interface.
 * 
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version $Revision$
 * @qtags.location class
 */
public interface ProvidesTag extends DocletTag {
    /**
     * Provides the name of the Fractal interface. The default value is the
     * short name of the interface. When applied on a class the name and
     * signature parameters are required.
     * 
     * @return the identifier of the interface.
     */
    String getName_();

    /**
     * Provides the fully qualified signature of the associated Java interface.
     * When applied on a class the signature parameter is required.
     * 
     * @return the signature of the interface.
     */
    String getSignature();
}
