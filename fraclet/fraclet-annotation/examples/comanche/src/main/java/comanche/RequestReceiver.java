/*==============================================================================
Fraclet annotation- Copyright (C) 2002-2006 INRIA Futurs / LIFL
Fractal Component Model (contact: fractal@objectweb.org)

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free 
Software Foundation; either version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along 
with this library; if not, write to the Free Software Foundation, Inc., 
59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
==============================================================================*/
package comanche;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.objectweb.fractal.fraclet.annotation.annotations.FractalComponent;
import org.objectweb.fractal.fraclet.annotation.annotations.Interface;
import org.objectweb.fractal.fraclet.annotation.annotations.Provides;
import org.objectweb.fractal.fraclet.annotation.annotations.Requires;

@FractalComponent
@Provides(interfaces=@Interface(name="r",signature=Runnable.class))
public class RequestReceiver implements Runnable {

	@Requires(name = "s")
	private Scheduler s;

	@Requires(name = "rh")
	private RequestHandler rh;

	// configuration aspect
	// public String[] listFc () { return new String[] { "s", "rh" }; }
	// public Object lookupFc (String itfName) {
	// if (itfName.equals("s")) { return s; }
	// else if (itfName.equals("rh")) { return rh; }
	// else return null;
	// }
	// public void bindFc (String itfName, Object itfValue) {
	// if (itfName.equals("s")) { s = (Scheduler)itfValue; }
	// else if (itfName.equals("rh")) { rh = (RequestHandler)itfValue; }
	// }
	// public void unbindFc (String itfName) {
	// if (itfName.equals("s")) { s = null; }
	// else if (itfName.equals("rh")) { rh = null; }
	// }
	// functional aspect
	public void run() {
		try {
			ServerSocket ss = new ServerSocket(8080);
			System.out.println("Comanche HTTP Server ready on port 8080.");
		      System.out.println("Load http://localhost:8080/gnu.jpg");
			while (true) {
				final Socket socket = ss.accept();
				s.schedule(new Runnable() {
					public void run() {
						try {
							rh.handleRequest(new Request(socket));
						} catch (IOException _) {
						}
					}
				});
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
