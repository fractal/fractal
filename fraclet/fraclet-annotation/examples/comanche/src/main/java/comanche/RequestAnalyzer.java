/*==============================================================================
Fraclet annotation- Copyright (C) 2002-2006 INRIA Futurs / LIFL
Fractal Component Model (contact: fractal@objectweb.org)

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free 
Software Foundation; either version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along 
with this library; if not, write to the Free Software Foundation, Inc., 
59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
==============================================================================*/
package comanche;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.PrintStream;

import org.objectweb.fractal.fraclet.annotation.annotations.FractalComponent;
import org.objectweb.fractal.fraclet.annotation.annotations.Requires;

@FractalComponent
public class RequestAnalyzer implements RequestHandler {

	@Requires(name="a")
	private RequestHandler rh;

	@Requires(name="l")
	private Logger l;

//	// configuration aspect
//
//	public String[] listFc() {
//		return new String[] { "l", "rh" };
//	}
//
//	public Object lookupFc(String itfName) {
//		if (itfName.equals("l")) {
//			return l;
//		} else if (itfName.equals("rh")) {
//			return rh;
//		} else
//			return null;
//	}
//
//	public void bindFc(String itfName, Object itfValue) {
//		if (itfName.equals("l")) {
//			l = (Logger) itfValue;
//		} else if (itfName.equals("rh")) {
//			rh = (RequestHandler) itfValue;
//		}
//	}
//
//	public void unbindFc(String itfName) {
//		if (itfName.equals("l")) {
//			l = null;
//		} else if (itfName.equals("rh")) {
//			rh = null;
//		}
//	}

	// functional aspect
	public void handleRequest(Request r) throws IOException {
		r.in = new InputStreamReader(r.s.getInputStream());
		r.out = new PrintStream(r.s.getOutputStream());
		String rq = new LineNumberReader(r.in).readLine();
		l.log(rq);
		if (rq.startsWith("GET ")) {
			r.url = rq.substring(5, rq.indexOf(' ', 4));
			rh.handleRequest(r);
		}
		r.out.close();
		r.s.close();
	}
}
