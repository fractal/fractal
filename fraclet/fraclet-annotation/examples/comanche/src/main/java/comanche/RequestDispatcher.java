/*==============================================================================
Fraclet annotation- Copyright (C) 2002-2006 INRIA Futurs / LIFL
Fractal Component Model (contact: fractal@objectweb.org)

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free 
Software Foundation; either version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along 
with this library; if not, write to the Free Software Foundation, Inc., 
59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
==============================================================================*/
package comanche;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.objectweb.fractal.fraclet.annotation.annotations.FractalComponent;
import org.objectweb.fractal.fraclet.annotation.annotations.Requires;
import org.objectweb.fractal.fraclet.annotation.annotations.type.Cardinality;

@FractalComponent
public class RequestDispatcher implements RequestHandler {
	
	@Requires(name="h",cardinality=Cardinality.COLLECTION)
	private Map<String, RequestHandler> handlers = new TreeMap<String, RequestHandler>();
	// configuration aspect
//	public String[] listFc () {
//	return (String[])handlers.keySet().toArray(new String[handlers.size()]);
//	}
	// public Object lookupFc (String itfName) {
	// if (itfName.startsWith("h")) { return handlers.get(itfName); }
//	else return null;
//	}
//	public void bindFc (String itfName, Object itfValue) {
//	if (itfName.startsWith("h")) { handlers.put(itfName, itfValue); }
//	}
//	public void unbindFc (String itfName) {
//	if (itfName.startsWith("h")) { handlers.remove(itfName); }
//	}
	// functional aspect
	public void handleRequest (Request r) throws IOException {
		Iterator<RequestHandler> i = handlers.values().iterator();
		while (i.hasNext()) {
			try {
				((RequestHandler)i.next()).handleRequest(r); return;
			} catch (IOException _) { }
		}
	}
}
