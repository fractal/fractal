/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 ==============================================================================*/
package primitive;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.fraclet.annotation.annotations.FractalComponent;
import org.objectweb.fractal.fraclet.annotation.annotations.Interface;
import org.objectweb.fractal.fraclet.annotation.annotations.LifeCycle;
import org.objectweb.fractal.fraclet.annotation.annotations.MonologHandler;
import org.objectweb.fractal.fraclet.annotation.annotations.Provides;
import org.objectweb.fractal.fraclet.annotation.annotations.Requires;
import org.objectweb.fractal.fraclet.annotation.annotations.type.Cardinality;
import org.objectweb.util.monolog.api.BasicLevel;

/**
 * A Fractal component client
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
@FractalComponent
@MonologHandler(name = "clientHandler", output = "System.err", pattern = "%-5l [%h] <%t> %m%n")
@Provides(interfaces = @Interface(name = "r", signature = Runnable.class))
public class Client extends AbstractClient implements Runnable {

	@org.objectweb.fractal.fraclet.annotation.annotations.Service
	protected Component ref;

	@Requires(name = "service", cardinality = Cardinality.COLLECTION)
	protected Map<String, Service> services = new HashMap<String, Service>();

	@LifeCycle
	public void init() {
		System.out.println("STARTING client component");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		NameController nc = null;
		try {
			nc = (NameController) ref.getFcInterface("name-controller");
		} catch (NoSuchInterfaceException e) {
			e.printStackTrace();
		}
		String componentName = nc.getFcName();
		for (Entry<String, Service> s : services.entrySet()) {
			System.out
			.println(componentName + " Calling " + s.getKey() + "...");
			((primitive.Service) s.getValue()).print();
			if (logger != null)
				logger.setIntLevel(BasicLevel.INFO);
			if ((logger != null) && logger.isLoggable(BasicLevel.INFO))
				logger.log(BasicLevel.INFO, "Calling " + s.getKey() + "...");

		}
		System.out.println("Default service says: ");
		if (logger != null && logger.isLoggable(BasicLevel.INFO))
			logger.log(BasicLevel.INFO, "Default service says:");

		this.service.print();
	}
}