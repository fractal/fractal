/*==============================================================================
Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
Fractal Component Model (contact: fractal@objectweb.org)

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free 
Software Foundation; either version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along 
with this library; if not, write to the Free Software Foundation, Inc., 
59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
==============================================================================*/
package primitive;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.fraclet.annotation.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotation.annotations.FractalComponent;
import org.objectweb.fractal.fraclet.annotation.annotations.Service;


/**
 * A Fractal component "ServerA"
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 *
 */
@FractalComponent
public class ServerA implements primitive.Service {

	@Attribute(value=">>")
    private String header ;
    
	@Service
	private Component ref;
	
	@Attribute(argument="msg")
    private String message ;
    
    /* (non-Javadoc)
     * @see org.objectweb.fractal.fraclet.adl.api.Service#print()
     */
    public void print() {
		NameController nc=null;
		try {
			nc = (NameController)ref.getFcInterface("name-controller");
		} catch (NoSuchInterfaceException e) {
			e.printStackTrace();
		}
        System.out.println(nc.getFcName()+header+message);
    }

}