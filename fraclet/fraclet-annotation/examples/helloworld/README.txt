This is the HelloWorld example of the fraclet-annotation project.

To run the example with Maven, just launch the following command:

   $ mvn clean fractaladl:run
