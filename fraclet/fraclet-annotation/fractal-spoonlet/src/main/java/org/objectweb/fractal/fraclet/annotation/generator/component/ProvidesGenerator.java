/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)

 ==============================================================================*/
package org.objectweb.fractal.fraclet.annotation.generator.component;

import java.io.PrintWriter;

import org.objectweb.fractal.fraclet.annotation.annotations.Interface;
import org.objectweb.fractal.fraclet.annotation.annotations.Provides;
import org.objectweb.fractal.fraclet.annotation.generator.Generator;
import org.objectweb.fractal.fraclet.annotation.processor.util.ADLFileFactory;

import spoon.reflect.declaration.CtClass;

/**
 *  A generator to manage the <code>@Provides</code> annotation .
 *
 * @author  Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 *
 */
public class ProvidesGenerator implements Generator {

	public void prettyprint(PrintWriter pw, CtClass<?> processedClass) {
		Provides providesAnnotation = processedClass
				.getAnnotation(Provides.class);
		if (providesAnnotation != null) {
//			System.out.println(providesAnnotation.interfaces());
			Interface[] itfAnnotations = providesAnnotation.interfaces();
			for (Interface itfAnnotation : itfAnnotations) {
				ADLFileFactory.printItfSignature(pw, itfAnnotation.signature()
						.getCanonicalName(), itfAnnotation.name(),
						itfAnnotation.cardinality(), itfAnnotation
								.contingency(), ADLFileFactory.ROLE_SERVER);
			}
		}

	}

}
