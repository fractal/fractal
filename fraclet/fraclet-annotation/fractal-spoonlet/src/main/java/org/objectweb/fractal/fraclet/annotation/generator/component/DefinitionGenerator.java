/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)

 ==============================================================================*/
package org.objectweb.fractal.fraclet.annotation.generator.component;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.fraclet.annotation.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotation.annotations.FractalComponent;
import org.objectweb.fractal.fraclet.annotation.annotations.Interface;
import org.objectweb.fractal.fraclet.annotation.generator.Generator;

import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;
import spoon.reflect.reference.CtTypeReference;

/**
 * A generator to manage the "definition" declaration of the ADL.
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public class DefinitionGenerator implements Generator {

	public void prettyprint(PrintWriter pw, CtClass<?> processedClass) {

		// -----------------------------------------------------
		// Manage Fractal attributes arguments
		// -----------------------------------------------------
		String arguments = "";
		List<CtField<?>> fields = processedClass.getFields();
		Map<String, String> attributes = new HashMap<String, String>();
		// List<CtField<?>> fields = processedClass.getFields();
		// iterate over processedClass fields
		for (CtField<?> field : fields) {
			// get the Requires annotation
			Attribute acAnnotation = field.getAnnotation(Attribute.class);
			// if the field is not annotated continue
			if (acAnnotation == null)
				continue;
			if (acAnnotation.value() == null)
				attributes.put(field.getSimpleName(), "");
			else
				attributes.put(field.getSimpleName(), acAnnotation.value());
			if (acAnnotation.argument() != null
					&& acAnnotation.argument().length() > 0) {
				arguments += acAnnotation.argument() + ",";
				String argValue = "${" + acAnnotation.argument() + "}";
				attributes.put(field.getSimpleName(), argValue);
			}
		}
		if (arguments != "")
			arguments = arguments.substring(0, (arguments.length() - 1));

		// -----------------------------------------------------
		// Get Fractal server interfaces
		// -----------------------------------------------------

		List<CtTypeReference<?>> itfs = new ArrayList<CtTypeReference<?>>();
		for (CtTypeReference<?> itf : processedClass.getSuperInterfaces()) {
			try {
				Interface itfAnnotation = itf.getAnnotation(Interface.class);

				if (itfAnnotation != null) {
					itfs.add(itf);
				}
			} catch (NullPointerException ex) {
			}
		}
		CtTypeReference<?> superClass = processedClass.getSuperclass();

		if (superClass != null) {
			if (superClass.getAnnotation(FractalComponent.class) != null)
				itfs.add(superClass);	
		}

		if (arguments != "")
			arguments = "arguments=\"" + arguments + "\"";

		if (itfs.size() > 0) {
			String extendedADLFiles = "";
			for (CtTypeReference<?> itf : itfs) {
				extendedADLFiles += itf + ",";
			}
			extendedADLFiles = extendedADLFiles.substring(0, (extendedADLFiles
					.length() - 1));
			pw.println("<definition name=\""
					+ processedClass.getQualifiedName() + "\" extends=\""
					+ extendedADLFiles + "\"" + " " + arguments + ">");
		} else
			pw.println("<definition name=\""
					+ processedClass.getQualifiedName() + "\"" + " "
					+ arguments + ">");

	}

}
