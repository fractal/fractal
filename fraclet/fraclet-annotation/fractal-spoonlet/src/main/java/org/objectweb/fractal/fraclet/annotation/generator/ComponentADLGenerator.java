/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 Contributor(s): Renaud Pawlak (renaud.pawlak@lifl.fr)
 ==============================================================================*/
package org.objectweb.fractal.fraclet.annotation.generator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

import org.objectweb.fractal.fraclet.annotation.annotations.FractalComponent;
import org.objectweb.fractal.fraclet.annotation.processor.util.ADLFileFactory;

import spoon.reflect.declaration.CtClass;

/**
 * An ADL generator for the component definitions.
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public class ComponentADLGenerator {

	private PrintWriter pw = null;

	public ComponentADLGenerator(CtClass<?> processedClass,
			FractalComponent fcAnnotation,List<Generator> generators) {

		// -----------------------------------------------------
		// create .fractal file
		// -----------------------------------------------------
		File outFile = ADLFileFactory.createADLFile(processedClass.getFactory()
				.getEnvironment().getDefaultFileGenerator()
				.getOutputDirectory(), processedClass);
		try {
			pw = new PrintWriter(outFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		for (Generator g : generators)
			g.prettyprint(pw, processedClass);

		// -----------------------------------------------------
		// Print Controller Desc
		// -----------------------------------------------------

		pw.println("  <controller desc=\"" + fcAnnotation.controllerDesc()
				+ "\"/>");

		// -----------------------------------------------------
		// closing file
		// -----------------------------------------------------

		pw.println("</definition>");
		pw.close();
	}

}
