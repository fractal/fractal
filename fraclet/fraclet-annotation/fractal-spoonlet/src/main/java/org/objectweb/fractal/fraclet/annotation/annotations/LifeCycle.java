/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 ==============================================================================*/
package org.objectweb.fractal.fraclet.annotation.annotations;

import org.objectweb.fractal.fraclet.annotation.annotations.type.LifeCycleType;

import spoon.aval.annotation.structure.AValTarget;
import spoon.reflect.declaration.CtMethod;

/**
 * An annotation on a component method which specifies to run the method
 * behavior when the component is starting (LifeCycleType.START), or is stopping
 * (LifeCycleType.STOP).
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
@AValTarget(CtMethod.class)
public @interface LifeCycle {
	LifeCycleType on() default LifeCycleType.START;
}
