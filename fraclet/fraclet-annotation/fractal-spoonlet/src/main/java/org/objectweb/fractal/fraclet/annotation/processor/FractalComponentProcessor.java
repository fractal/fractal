/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 ==============================================================================*/

package org.objectweb.fractal.fraclet.annotation.processor;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.fraclet.annotation.annotations.FractalComponent;
import org.objectweb.fractal.fraclet.annotation.annotations.LifeCycle;
import org.objectweb.fractal.fraclet.annotation.annotations.Service;
import org.objectweb.fractal.fraclet.annotation.annotations.type.LifeCycleType;
import org.objectweb.fractal.fraclet.annotation.generator.ComponentADLGenerator;
import org.objectweb.fractal.fraclet.annotation.generator.Generator;
import org.objectweb.fractal.fraclet.annotation.generator.component.AttributeGenerator;
import org.objectweb.fractal.fraclet.annotation.generator.component.ContentGenerator;
import org.objectweb.fractal.fraclet.annotation.generator.component.DefinitionGenerator;
import org.objectweb.fractal.fraclet.annotation.generator.component.LicenceGenerator;
import org.objectweb.fractal.fraclet.annotation.generator.component.ProvidesGenerator;
import org.objectweb.fractal.fraclet.annotation.generator.component.RequiresGenerator;
import org.objectweb.fractal.fraclet.annotation.generator.template.binding.RequiresTemplate;
import org.objectweb.fractal.fraclet.annotation.generator.template.lifecycle.LifeCycleTemplate;
import org.objectweb.fractal.fraclet.annotation.generator.template.lifecycle.LifeCycleTemplateStart;
import org.objectweb.fractal.fraclet.annotation.generator.template.lifecycle.LifeCycleTemplateStop;
import org.objectweb.fractal.fraclet.annotation.generator.template.util.NonTypedFractalAttributeException;

import spoon.processing.AbstractProcessor;
import spoon.processing.Property;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtElement;
import spoon.reflect.declaration.CtMethod;
import spoon.template.Substitution;

/**
 * An annotation processor to manage the FractalComponent annotation (generation
 * of ".fractal" file).
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public class FractalComponentProcessor extends AbstractProcessor<CtClass<?>> {

	@Property
	Class<?>[] generatorClasses = new Class[] { LicenceGenerator.class,
			DefinitionGenerator.class, RequiresGenerator.class,
			ProvidesGenerator.class, ContentGenerator.class,
			AttributeGenerator.class };

	private List<Generator> generators = new ArrayList<Generator>();

	@Override
	public void init() {
		super.init();

		generators.clear();

		for (Class<?> c : generatorClasses) {
			try {
				generators.add((Generator) c.newInstance());
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * A constructor for the ComponentProcessor
	 * 
	 * @param processedClass
	 *            the CtClass processed
	 */
	public void process(CtClass<?> processedClass) {
		FractalComponent fcAnnotation = processedClass
				.getAnnotation(FractalComponent.class);
		if (fcAnnotation != null) {
			new ComponentADLGenerator(processedClass, fcAnnotation, generators);
			try {
				Substitution.insertAll(processedClass, new RequiresTemplate(
						getFactory(), processedClass));
				List<CtElement> elts = processedClass
						.getAnnotatedChildren(Service.class);
				if (elts.size() > 0)
					elts.get(0).getAnnotations().remove(
							elts.get(0).getAnnotation(
									getFactory().Type().createReference(
											Service.class)));
				new RequiresTemplate(getFactory(), processedClass);
			} catch (NonTypedFractalAttributeException e) {
				e.printStackTrace();
			}
		}
		
		// Manage LifeCycle annotation
		List<CtElement> elts = processedClass.getAnnotatedChildren(LifeCycle.class);

		boolean start = false, stop = false;

		CtMethod<?> startM = null, stopM = null;

		for (CtElement elt : elts) {
			LifeCycle annot = elt.getAnnotation(LifeCycle.class);

			if (annot.on().equals(LifeCycleType.START)) {
				start = true;
				startM = (CtMethod) elt;
				if (startM.getParameters().size() > 0)
					System.err
							.println("WARNING: a method annotated with @LifeCycle shouldn't "
									+ "have parameters! Please revise your method signature.");
			}

			if (annot.on().equals(LifeCycleType.STOP)) {
				stop = true;
				stopM = (CtMethod) elt;
				if (stopM.getParameters().size() > 0)
					System.err
							.println("WARNING: a method annotated with @LifeCycle shouldn't "
									+ "have parameters! Please revise your method signature.");

			}
		}

		if (start && !stop)
			Substitution.insertAll(processedClass, new LifeCycleTemplateStart(startM));

		if (stop && !start)
			Substitution.insertAll(processedClass, new LifeCycleTemplateStop(stopM));
		if (start && stop)
			Substitution
					.insertAll(processedClass, new LifeCycleTemplate(startM, stopM));
		if (start || stop)
			if (!processedClass.getSuperInterfaces().contains(
					getFactory().Type().createReference(
							LifeCycleController.class))) {
				processedClass.getSuperInterfaces().add(
						getFactory().Type().createReference(
								LifeCycleController.class));
			}
		
	}

	/**
	 * Allows to add a new generator in the list
	 * 
	 * @param g
	 *            the new generator
	 */
	public void addGenerator(Generator g) {
		generators.add(g);
	}
}
