/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 Contributor(s): Renaud Pawlak (renaud.pawlak@lifl.fr)
 ==============================================================================*/

package org.objectweb.fractal.fraclet.annotation.processor.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import org.objectweb.fractal.fraclet.annotation.annotations.type.Cardinality;
import org.objectweb.fractal.fraclet.annotation.annotations.type.Contingency;
import org.objectweb.fractal.fraclet.annotation.generator.template.util.None;

import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtSimpleType;

/**
 * An helper class to manage Fractal-ADL files.
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public class ADLFileFactory {

	public static final String ROLE_CLIENT = "client";

	public static final String ROLE_SERVER = "server";

	/**
	 * Create an ADL file corresponding to the processed element
	 * 
	 * @param spoonedDir
	 *            The director where to write the file
	 * @param CtSimpleType
	 *            processed the processed element CtClass/CtInterface
	 */
	public static File createADLFile(File spoonedDir, CtSimpleType<?> processed) {
		String directoryString = spoonedDir.toString() + File.separator;
		if (processed.getPackage().toString().length() > 0)
			directoryString += processed.getPackage().getQualifiedName()
					.replace('.', File.separatorChar);
		File directory = new File(directoryString);
		File outFile = new File(directoryString + File.separator
				+ processed.getSimpleName().toString() + ".fractal");

		if (!directory.exists())
			if (!directory.mkdirs()) {
				System.err
						.println("Cannot create directory for generated ADL files: "
								+ directoryString);
				return null;
			}
		try {
			if (outFile.canWrite())
				outFile.createNewFile();
		} catch (IOException e) {
			System.err.println("Error in processing "
					+ processed.getSimpleName()
					+ ", impossible to create the corresonding .fractal file"
					+ e);
			return null;
		}
		return outFile;

	}

	/**
	 * Print with the pw printwriter the <interface name="" cardinality=""
	 * contingency=""/> tag.
	 * 
	 * @param pw
	 *            the printwriter to write with
	 * @param signature
	 *            the qualified name of the interface
	 * @param fractalItfName
	 *            the Fractal name of the interface
	 * @param cardinality
	 *            the cardinality of the Fractal interface
	 * @param contingency
	 *            the contingency of the Fractal interface
	 * @param clientServer
	 *            the role of the interface "client"|"server"
	 */
	public static void printItfSignature(PrintWriter pw, String signature,
			String fractalItfName, Cardinality cardinality,
			Contingency contingency, String clientServer) {
		pw.println("  <interface name=\"" + fractalItfName + "\" signature=\""
				+ signature + "\" role=\"" + clientServer + "\" cardinality=\""
				+ cardinality.toString().toLowerCase() + "\" contingency=\""
				+ contingency.toString().toLowerCase() + "\"/>");
	}

	public static void printItfSignature(PrintWriter pw, CtField<?> field,
			String fractalItfName, Class<?> signature, Cardinality cardinality,
			Contingency contingency, String clientServer) {
		// if (cardinality == null)
		// cardinality = "singleton";
		String itfName = "";
		if (cardinality.equals(Cardinality.SINGLETON))
			itfName = field.getType().getQualifiedName();
		else if (cardinality.equals(Cardinality.COLLECTION)) {
			if (!signature.equals(None.class))
				itfName = signature.getName();
			else
				itfName = field.getType().getActualTypeArguments().get(1)
						.getQualifiedName();
		}

		printItfSignature(pw, itfName, fractalItfName, cardinality,
				contingency, clientServer);
	}

	/**
	 * Print licence declaration with the given PrintWriter
	 */
	public static void printLicence(PrintWriter pw) {
		pw.println("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		pw
				.println("<!DOCTYPE definition PUBLIC \"-//objectweb.org//DTD Fractal ADL 2.0//EN\" \"classpath://org/objectweb/fractal/adl/xml/standard.dtd\">");

		pw.println();
		pw
				.println("<!--==============================================================================");
		pw
				.println("Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL");
		pw.println("Fractal Component Model (contact: fractal@objectweb.org)");
		pw.println();
		pw
				.println("This library is free software; you can redistribute it and/or modify it under");
		pw
				.println("the terms of the GNU Lesser General Public License as published by the Free");
		pw
				.println("Software Foundation; either version 2.1 of the License, or any later version.");
		pw.println();
		pw
				.println("This library is distributed in the hope that it will be useful, but WITHOUT ANY");
		pw
				.println("WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A");
		pw
				.println("PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.");
		pw.println();
		pw
				.println("You should have received a copy of the GNU Lesser General Public License along");
		pw
				.println("with this library; if not, write to the Free Software Foundation, Inc., ");
		pw.println("59 Temple Place, Suite 330, Boston, MA  02111-1307 USA");
		pw.println();
		pw
				.println("Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)");
		pw
				.println("==============================================================================*/");
		pw.println("-->");
		pw.println();
	}
}
