/*==============================================================================
Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
Fractal Component Model (contact: fractal@objectweb.org)

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free 
Software Foundation; either version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along 
with this library; if not, write to the Free Software Foundation, Inc., 
59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
Contributor(s): Renaud Pawlak (renaud.pawlak@lifl.fr)
==============================================================================*/


package org.objectweb.fractal.fraclet.annotation.generator.template.binding;

import java.util.Map;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.fraclet.annotation.generator.template.util._T_;

import spoon.reflect.reference.CtTypeReference;
import spoon.template.Local;
import spoon.template.Parameter;
import spoon.template.Template;


/**
 * A template to manage the LookupFc method
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public class LookupFcTemplate implements Template {
	// ///////////////////////
	// Parameters for singleton bindings
	// /////////////////////////
	@Parameter
	// The name of the interface (singleton bindings)
	String _itfName_;
	
	@Parameter
	// A Type Reference which is the type of the singleton service
	CtTypeReference<?> _T_;
	
	@Local
	// A Local variable which is the reference to the singeton field
	_T_ _service_;
	
	@Parameter("_service_")
	// The name of the singleton field
	String __service_;
	
	
	// /////////////////////////
	// Parameters for collection bindings
	// /////////////////////////
	@Parameter("_servicesMap_")
	// The name of the java field (collection bindings)
	String __servicesMap_;
	
	@Local
	// The field that contains the collection bindings (collection bindings)
	Map<String,?> _servicesMap_;
	
	
	/**
	 * Use this constructor for singeton bindings
	 * 
	 * @param itfName
	 *            The name of the Fractal interface
	 * @param serviceType
	 *            The java type of the binding
	 * @param serviceName
	 *            The name of the java field
	 */
	public LookupFcTemplate(String itfName, CtTypeReference<?> serviceType,
			String serviceName) {
		this._itfName_ = itfName;
		this._T_ = serviceType;
		this.__service_ = serviceName;
		
	}
	
	
	
	/**
	 * Use this constructor for collection bindings`
	 * 
	 * @param fieldName
	 *            the name of the map field
	 */
	public LookupFcTemplate(String fieldName) {
		this.__servicesMap_ = fieldName;	
	}
	
	
	/**
	 * Block to add into the lookupFc method (for singleton bindings)
	 * 
	 * @param clientItfName
	 *            the Fractal interface name
	 * @return the reference to the server
	 */
	Object lookupSingleton(String clientItfName)  throws NoSuchInterfaceException {
		if (clientItfName.equals(_itfName_)) {
			return _service_;
		}
		return null;
	}
	
	/**
	 * Block to add into the bindFc method (for collection bindings)
	 * 
	 * @param clientItfName
	 *            the Fractal interface name
	 * @return the reference to the server
	 */
	Object lookupCollection(String clientItfName) throws NoSuchInterfaceException{
		if (clientItfName.startsWith(clientItfName)) {
			return _servicesMap_.get(clientItfName);
		}
		return null;
		
	}
}
