/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 Contributor(s): Renaud Pawlak (renaud.pawlak@lifl.fr)
 ==============================================================================*/
package org.objectweb.fractal.fraclet.annotation.generator;

import java.io.PrintWriter;

import spoon.reflect.declaration.CtClass;

/**
 * <p>
 * Generators are used within the CompositeADLGenerator and
 * ComponentADLGenerator classes. Basically these two classes manage the
 * generation of Fractal ADL describtion in XML.
 * </p>
 * <p>
 * If you want to add a new generator to manage a new tag in the ADL, you have
 * to implement this interface and add your new generator to the lists managed
 * by the FractalAnnotationProcessor
 * </p>
 * 
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public interface Generator {

	void prettyprint(PrintWriter pw, CtClass<?> processedClass);

}
