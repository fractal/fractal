/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 Contributor(s): Renaud Pawlak (renaud.pawlak@lifl.fr)
 ==============================================================================*/

package org.objectweb.fractal.fraclet.annotation.generator.template.binding;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

import spoon.template.Local;
import spoon.template.Parameter;
import spoon.template.Template;

/**
 * A template to manage the bindFc method to get the Component reference
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public class BindRcTemplate implements Template {

	@Local
	// A Local variable which is the reference to the singeton field
	Component _service_;

	@Parameter("_service_")
	// The name of the singleton field
	String __service_;

	/**
	 * Use this constructor for singeton bindings
	 * 
	 * @param serviceName
	 *            The name of the java field
	 */
	public BindRcTemplate(String serviceName) {
		this.__service_ = serviceName;

	}

	/**
	 * Block to add into the bindFc method (for singleton bindings)
	 * 
	 * @param clientItfName
	 *            the Fractal binding name
	 * @param serverItf
	 *            the reference to the server
	 * @throws IllegalBindingException
	 *             when attempting to bind incompatible interfaces
	 */
	void bind(String clientItfName, Object serverItf)
			throws NoSuchInterfaceException, IllegalBindingException,
			IllegalLifeCycleException {
		if (clientItfName.equals("component")) {
			_service_ = (Component) serverItf;
			return;
		}
	}

}
