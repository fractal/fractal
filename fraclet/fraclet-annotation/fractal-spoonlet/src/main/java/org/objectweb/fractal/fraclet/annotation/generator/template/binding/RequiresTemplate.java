/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 Contributor(s): Renaud Pawlak (renaud.pawlak@lifl.fr)
 ==============================================================================*/

package org.objectweb.fractal.fraclet.annotation.generator.template.binding;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.fraclet.annotation.annotations.FractalComponent;
import org.objectweb.fractal.fraclet.annotation.annotations.Monolog;
import org.objectweb.fractal.fraclet.annotation.annotations.Requires;
import org.objectweb.fractal.fraclet.annotation.annotations.Service;
import org.objectweb.fractal.fraclet.annotation.annotations.type.Cardinality;
import org.objectweb.fractal.fraclet.annotation.generator.template.monolog.LogTemplate;
import org.objectweb.fractal.fraclet.annotation.generator.template.util.NonTypedFractalAttributeException;
import org.objectweb.fractal.fraclet.annotation.generator.template.util.None;

import spoon.reflect.Factory;
import spoon.reflect.code.CtBlock;
import spoon.reflect.code.CtStatementList;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;
import spoon.reflect.reference.CtTypeReference;
import spoon.template.Local;
import spoon.template.Parameter;
import spoon.template.Substitution;
import spoon.template.Template;
import spoon.template.TemplateParameterList;

/**
 * A template code to insert the binding controller methods.
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public class RequiresTemplate implements Template {

	@Parameter
	TemplateParameterList<?> _listFcAdds_;

	@Parameter
	TemplateParameterList<?> _unbinds_;

	@Parameter
	TemplateParameterList<?> _lookups_;

	@Parameter
	TemplateParameterList<?> _binds_;

	@Local
	Factory spoonFactory;
	@Local
	CtStatementList<?> listFcAdds;
	@Local
	CtStatementList<?> unbinds;
	@Local
	CtStatementList<?> binds;
	@Local
	CtStatementList<?> lookups;

	/**
	 * Constructor for RequiresTemplate. This constructor is called from the
	 * <code>@Requires</code> Processor
	 * 
	 * @param spoonFactory
	 *            the factory
	 * @param target
	 *            the target class
	 */
	@Local
	public RequiresTemplate(Factory spoonFactory, CtClass<?> target)
			throws NonTypedFractalAttributeException {
		// 4 sub templates are used, one for each method to insert
		this.spoonFactory = spoonFactory;
		listFcAdds = spoonFactory.Core().createStatementList();
		unbinds = spoonFactory.Core().createStatementList();
		binds = spoonFactory.Core().createStatementList();
		lookups = spoonFactory.Core().createStatementList();
		CtTypeReference<?> parentTarget = (CtTypeReference<?>) target
				.getSuperclass();
		CtClass<?> localTarget = target;
		while (parentTarget != null
				&& parentTarget.getDeclaration() != null
				&& parentTarget.getDeclaration().getAnnotation(
						FractalComponent.class) != null) {
			parseFields(target, (CtClass<?>) parentTarget.getDeclaration(),
					spoonFactory);
			localTarget = (CtClass<?>) parentTarget.getDeclaration();
			parentTarget = (CtTypeReference<?>) localTarget.getSuperclass();
		}
		parseFields(target, target, spoonFactory);
		// set the references to the list statements
		_listFcAdds_ = listFcAdds;
		_unbinds_ = unbinds;
		_binds_ = binds;
		_lookups_ = lookups;
	}

	@SuppressWarnings("unchecked")
	@Local
	private void parseFields(CtClass<?> target, CtClass<?> parentTarget,
			Factory spoonFactory) throws NonTypedFractalAttributeException {
		List<CtField<?>> fields = parentTarget.getFields();

		// iterate over target fields
		for (CtField<?> field : fields) {

			// ////////////////////////////////////////////////////
			// /////// Manage the @Service annotation //////////
			// ///////////////////////////////////////////////////

			Service serviceAnnotation = field.getAnnotation(Service.class);
			if (serviceAnnotation != null) {
				if (!target.getSuperInterfaces().contains(
						spoonFactory.Type().createReference(
								BindingController.class))) {
					target.getSuperInterfaces().add(
							spoonFactory.Type().createReference(
									BindingController.class));
				}

				Template rcT = new BindRcTemplate(field.getSimpleName());
				CtBlock<?> rcBlock = Substitution.substituteMethodBody(target,
						rcT, "bind", new CtTypeReference<?>[] {
								spoonFactory.Type().createReference(
										String.class),
								spoonFactory.Type().createReference(
										Object.class) });
				binds.getStatements().add(rcBlock.getStatements().get(0));
			}

			// ////////////////////////////////////////////////////
			// /////// Manage the @Monolog annotation //////////
			// ///////////////////////////////////////////////////

			Monolog logAnnotation = field.getAnnotation(Monolog.class);
			if (logAnnotation != null) {
				if (!target.getSuperInterfaces().contains(
						spoonFactory.Type().createReference(
								BindingController.class))) {
					target.getSuperInterfaces().add(
							spoonFactory.Type().createReference(
									BindingController.class));
				}

				Template logT = new LogTemplate(field.getSimpleName());
				CtBlock<?> rcBlock = Substitution.substituteMethodBody(target,
						logT, "bind", new CtTypeReference<?>[] {
								spoonFactory.Type().createReference(
										String.class),
								spoonFactory.Type().createReference(
										Object.class) });
				binds.getStatements().addAll(rcBlock.getStatements());
			}
			// //////////////////////////////////////////////////
			// //////////////////////////////////////////////////

			// get the Requires annotation
			Requires requiresAnnotation = field.getAnnotation(Requires.class);
			// if the field is not annotated continue
			if (requiresAnnotation == null)
				continue;

			if (!target.getSuperInterfaces().contains(
					spoonFactory.Type()
							.createReference(BindingController.class))) {
				target.getSuperInterfaces().add(
						spoonFactory.Type().createReference(
								BindingController.class));
			}

			CtBlock<?> block = null;
			Template t = null;
			// //////////////////////////////////////////////////
			// If the cardinality is singleton
			// //////////////////////////////////////////////////
			if (requiresAnnotation.cardinality().equals(Cardinality.SINGLETON)) {
				// //////////////////////////////////////////////////
				// the listFcTemplate
				// //////////////////////////////////////////////////
				t = new ListFcTemplate(requiresAnnotation.name(), false);
				block = Substitution.substituteMethodBody(target, t,
						"addSingleton", spoonFactory.Type().createReference(
								List.class));
				// insert all the "interfaces.add("myService")"
				listFcAdds.getStatements().add(block.getStatements().get(0));
				// //////////////////////////////////////////////////
				// the UnbindFcTemplate
				// //////////////////////////////////////////////////
				t = new UnbindFcTemplate(requiresAnnotation.name(), field
						.getType(), field.getSimpleName());
				block = Substitution.substituteMethodBody(target, t,
						"unbindSingleton", spoonFactory.Type().createReference(
								String.class));
				unbinds.getStatements().add(block.getStatements().get(0));
				// //////////////////////////////////////////////////
				// the BindFcTemplate
				// //////////////////////////////////////////////////
				t = new BindFcTemplate(requiresAnnotation.name(), field
						.getType(), field.getSimpleName());
				block = Substitution.substituteMethodBody(target, t,
						"bindSingleton", new CtTypeReference<?>[] {
								spoonFactory.Type().createReference(
										String.class),
								spoonFactory.Type().createReference(
										Object.class) });
				binds.getStatements().add(block.getStatements().get(0));
				// //////////////////////////////////////////////////
				// the LookupFcTemplate
				// //////////////////////////////////////////////////
				t = new LookupFcTemplate(requiresAnnotation.name(), field
						.getType(), field.getSimpleName());
				block = Substitution.substituteMethodBody(target, t,
						"lookupSingleton", spoonFactory.Type().createReference(
								String.class));
				lookups.getStatements().add(block.getStatements().get(0));
			}
			// //////////////////////////////////////////////////
			// If the cardinality is collection
			// //////////////////////////////////////////////////
			else if (requiresAnnotation.cardinality().equals(
					Cardinality.COLLECTION)) {

				t = new ListFcTemplate(field.getSimpleName(), true);
				block = Substitution.substituteMethodBody(target, t,
						"addCollection", spoonFactory.Type().createReference(
								List.class));
				// insert all the "interfaces.add("myService")"
				listFcAdds.getStatements().add(block.getStatements().get(0));
				// //////////////////////////////////////////////////
				// the UnbindFcTemplate
				// //////////////////////////////////////////////////
				t = new UnbindFcTemplate(field.getSimpleName());
				block = Substitution.substituteMethodBody(target, t,
						"unbindCollection", spoonFactory.Type()
								.createReference(String.class));
				unbinds.getStatements().add(block.getStatements().get(0));
				// //////////////////////////////////////////////////
				// the BindFcTemplate
				// //////////////////////////////////////////////////
				if (requiresAnnotation.signature().equals(Class.class)
						&& field.getType().getActualTypeArguments().size() < 2)
					throw new NonTypedFractalAttributeException(
							"Fractal attribute "
									+ field.getSimpleName()
									+ " must be typed to generate the appropriate binding controller implementation");
				else if (!requiresAnnotation.signature().equals(None.class)) {
					t = new NonTypedBindFcTemplate(requiresAnnotation.name(),
							field.getSimpleName(), spoonFactory.Type()
									.createReference(
											requiresAnnotation.signature()));
				} else {
					t = new BindFcTemplate(requiresAnnotation.name(), field
							.getSimpleName(), field.getType()
							.getActualTypeArguments().get(1));
				}
				block = Substitution.substituteMethodBody(target, t,
						"bindCollection", new CtTypeReference<?>[] {
								spoonFactory.Type().createReference(
										String.class),
								spoonFactory.Type().createReference(
										Object.class) });
				binds.getStatements().add(block.getStatements().get(0));
				// //////////////////////////////////////////////////
				// the LookupFcTemplate
				// //////////////////////////////////////////////////
				t = new LookupFcTemplate(field.getSimpleName());
				block = Substitution.substituteMethodBody(target, t,
						"lookupCollection", spoonFactory.Type()
								.createReference(String.class));
				lookups.getStatements().add(block.getStatements().get(0));
			}
			// field.getAnnotations().remove(field.getAnnotation(spoonFactory.Type().createReference(Requires.class)));
		}

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.objectweb.fractal.api.control.BindingController#bindFc(java.lang.String,
	 *      java.lang.Object) Method automatically generated with Spoon
	 *      <http://spoon.gforge.inria.fr>
	 */
	public void bindFc(String clientItfName, Object serverItf)
			throws NoSuchInterfaceException, IllegalBindingException,
			IllegalLifeCycleException {
		_binds_.S();

		throw new NoSuchInterfaceException("Client interface \'"
				+ clientItfName + "\' is undefined.");
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.objectweb.fractal.api.control.BindingController#listFc() Method
	 *      automatically generated with Spoon <http://spoon.gforge.inria.fr>
	 */
	public String[] listFc() {
		List<String> __interfaces__ = new ArrayList<String>();
		_listFcAdds_.S();
		return __interfaces__.toArray(new String[] {});
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.objectweb.fractal.api.control.BindingController#lookupFc(java.lang.String)
	 *      Method automatically generated with Spoon
	 *      <http://spoon.gforge.inria.fr>
	 */
	public Object lookupFc(String clientItfName)
			throws NoSuchInterfaceException {
		_lookups_.S();
		throw new NoSuchInterfaceException("Client interface \'"
				+ clientItfName + "\' is undefined.");
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.objectweb.fractal.api.control.BindingController#unbindFc(java.lang.String)
	 *      Method automatically generated with Spoon
	 *      <http://spoon.gforge.inria.fr>
	 */
	public void unbindFc(String clientItfName) throws NoSuchInterfaceException,
			IllegalBindingException, IllegalLifeCycleException {
		_unbinds_.S();
		throw new NoSuchInterfaceException("Client interface \'"
				+ clientItfName + "\' is undefined.");
	}
}
