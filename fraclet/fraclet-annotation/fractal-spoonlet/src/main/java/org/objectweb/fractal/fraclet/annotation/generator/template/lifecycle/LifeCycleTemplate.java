/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 Contributor(s): Renaud Pawlak (renaud.pawlak@lifl.fr)
 ==============================================================================*/

package org.objectweb.fractal.fraclet.annotation.generator.template.lifecycle;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;

import spoon.reflect.code.CtInvocation;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.reference.CtExecutableReference;
import spoon.template.Local;
import spoon.template.Parameter;
import spoon.template.Template;

/**
 * A template code to manage the LifeCycle annotation. This template introdudes
 * the implementation of the LifeCycle controller and then call the annotated
 * method in the start or stop method (depending on the parameter of the
 * annotation).
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public class LifeCycleTemplate implements LifeCycleController, Template {

	@SuppressWarnings("unchecked")
	@Local
	public LifeCycleTemplate(CtMethod<?> methodToStart,
			CtMethod<?> methodToStop) {
		_methodCall_ = methodToStart.getFactory().Core().createInvocation();
		_methodCall_.setTarget(methodToStart.getFactory().Code()
				.createThisAccess(
						methodToStart.getFactory().Type().createReference(
								methodToStart.getParent(CtClass.class)
										.getActualClass())));
		_methodCall_.setExecutable((CtExecutableReference) methodToStart
				.getReference());
		_methodCallStop_ = methodToStop.getFactory().Core().createInvocation();
		_methodCallStop_.setTarget(methodToStop.getFactory()
				.Code().createThisAccess(
						methodToStop.getFactory().Type().createReference(
								methodToStop.getParent(CtClass.class)
										.getActualClass())));
		_methodCallStop_.setExecutable((CtExecutableReference) methodToStop
				.getReference());

	}

	@Parameter
	CtInvocation<?> _methodCall_;

	@Parameter
	CtInvocation<?> _methodCallStop_;

	/**
	 * 
	 */
//	private boolean _fcState = false;

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 * 
	 * @see org.objectweb.fractal.api.control.LifeCycleController#getFcState()
	 */
	public String getFcState() {
		return null;
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 * 
	 * @see org.objectweb.fractal.api.control.LifeCycleController#startFc()
	 */
	public void startFc() throws IllegalLifeCycleException {
		try {
//			_fcState = true;
			_methodCall_.S();
		} catch (Exception e) {
			throw new IllegalLifeCycleException(e.getMessage());
		}
	}

	/**
	 * (non-Javadoc) Method automatically generated with Spoon
	 * 
	 * @see org.objectweb.fractal.api.control.LifeCycleController#stopFc()
	 */
	public void stopFc() throws IllegalLifeCycleException {
		try {
			_methodCallStop_.S();
//			_fcState = false;

		} catch (Exception e) {
			throw new IllegalLifeCycleException(e.getMessage());
		}
	}

}
