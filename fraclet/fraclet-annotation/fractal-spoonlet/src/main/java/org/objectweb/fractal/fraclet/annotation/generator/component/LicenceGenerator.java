/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 Contributor(s): Renaud Pawlak (renaud.pawlak@lifl.fr)
 ==============================================================================*/
package org.objectweb.fractal.fraclet.annotation.generator.component;

import java.io.PrintWriter;

import org.objectweb.fractal.fraclet.annotation.generator.Generator;

import spoon.reflect.declaration.CtClass;

/**
 * A generator to manage the licence declaration in the generated XML files.
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public class LicenceGenerator implements Generator {

	public void prettyprint(PrintWriter pw, CtClass<?> processedClass) {
		pw.println("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		pw
				.println("<!DOCTYPE definition PUBLIC \"-//objectweb.org//DTD Fractal ADL 2.0//EN\" \"classpath://org/objectweb/fractal/adl/xml/standard.dtd\">");

		pw.println();
		pw
				.println("<!--==============================================================================");
		pw
				.println("Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL");
		pw.println("Fractal Component Model (contact: fractal@objectweb.org)");
		pw.println();
		pw
				.println("This library is free software; you can redistribute it and/or modify it under");
		pw
				.println("the terms of the GNU Lesser General Public License as published by the Free");
		pw
				.println("Software Foundation; either version 2.1 of the License, or any later version.");
		pw.println();
		pw
				.println("This library is distributed in the hope that it will be useful, but WITHOUT ANY");
		pw
				.println("WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A");
		pw
				.println("PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.");
		pw.println();
		pw
				.println("You should have received a copy of the GNU Lesser General Public License along");
		pw
				.println("with this library; if not, write to the Free Software Foundation, Inc., ");
		pw.println("59 Temple Place, Suite 330, Boston, MA  02111-1307 USA");
		pw.println();
		pw
				.println("Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)");
		pw
				.println("==============================================================================*/");
		pw.println("-->");
		pw.println();
	}

}
