/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)

 ==============================================================================*/
package org.objectweb.fractal.fraclet.annotation.generator.component;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.fraclet.annotation.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotation.generator.Generator;
import org.objectweb.fractal.fraclet.annotation.generator.template.attribute.AttributeTemplate;

import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;

/**
 *  A generator to manage the <code>@Attribute</code> annotation .
 *
 * @author  Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 *
 */
public class AttributeGenerator implements Generator {

	public void prettyprint(PrintWriter pw, CtClass<?> processedClass) {
		
		Map<String, String> attributes = new HashMap<String, String>();
		// List<CtField<?>> fields = processedClass.getFields();
		// iterate over processedClass fields
		for (CtField<?> field : processedClass.getFields()) {
			// get the Requires annotation
			Attribute acAnnotation = field.getAnnotation(Attribute.class);
			// if the field is not annotated continue
			if (acAnnotation == null)
				continue;
			if (acAnnotation.value() == null)
				attributes.put(field.getSimpleName(), "");
			else
				attributes.put(field.getSimpleName(), acAnnotation.value());
			if (acAnnotation.argument() != null
					&& acAnnotation.argument().length() > 0) {
//				arguments += acAnnotation.argument() + ",";
				String argValue = "${" + acAnnotation.argument() + "}";
				attributes.put(field.getSimpleName(), argValue);
			}
		}
		String attributeControllerSignature = processedClass.getQualifiedName()
		+ AttributeTemplate.ATTRIBUTESUFFIX;
		
		if (attributes.size() > 0) {
			pw.println("  <attributes signature=\""
					+ attributeControllerSignature + "\">");
			for (String attributeName : attributes.keySet()) {
				pw.println("    <attribute name=\"" + attributeName + "\""
						+ " value=\"" + "" + attributes.get(attributeName)
						+ "\"/>");
			}
			pw.println("  </attributes>");
		}
		
	}

}
