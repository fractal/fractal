/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 ==============================================================================*/
package org.objectweb.fractal.fraclet.annotation.processor;

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.fraclet.annotation.annotations.Service;
import org.objectweb.fractal.fraclet.annotation.generator.template.service.ComponentServiceTemplate;

import spoon.processing.AbstractAnnotationProcessor;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;
import spoon.template.Substitution;

/**
 * An annotation processor to manage the
 * 
 * <code>@Service</code> annotation. If no
 * <code>@Requires</code> annotation is defined within the class this processor will be used,
 *           otherwise the RequiresProcessor will manage also the
 * <code>@Service</code> annotation.
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public class ServiceProcessor extends
		AbstractAnnotationProcessor<Service, CtField<?>> {

	@Override
	public void init() {
		addProcessedAnnotationType(Service.class);
		super.init();
	}

	@Override
	public boolean inferConsumedAnnotationType() {
		return false;
	}

	/**
	 * A constructor for the AttributeProcessor
	 * 
	 * @param fractalAnnotation
	 *            the Requires annotation
	 * @param processedField
	 *            the CtField processed
	 */
	public void process(Service fractalAnnotation, CtField<?> processedField) {
		// manage the Requires annotation
		CtClass<?> processedClass = processedField.getParent(CtClass.class);
		Substitution.insertAll(processedClass, new ComponentServiceTemplate(
				processedField.getSimpleName(), processedClass));
		// add the BindingController interface
		processedClass.getSuperInterfaces().add(
				getFactory().Type().createReference(BindingController.class));
	}
}
