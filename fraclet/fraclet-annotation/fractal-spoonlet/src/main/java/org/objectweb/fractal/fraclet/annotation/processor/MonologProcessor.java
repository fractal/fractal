/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 Contributor(s): Renaud Pawlak (renaud.pawlak@lifl.fr)
 ==============================================================================*/
package org.objectweb.fractal.fraclet.annotation.processor;

import org.objectweb.fractal.fraclet.annotation.annotations.Monolog;
import org.objectweb.fractal.fraclet.annotation.generator.template.monolog.MonologTemplate;

import spoon.processing.AbstractAnnotationProcessor;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;
import spoon.template.Substitution;
import spoon.template.Template;


/**
 * An annotation processor for the @Monolog annotation.
 *
 * @author  Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 *
 */
public class MonologProcessor extends
		AbstractAnnotationProcessor<Monolog, CtField<?>> {


	public void process(Monolog logAnnotation, CtField<?> processedField) {
		// manage the @Monolog annotation
//		System.out.println(">>> processing Monolog annotation "+processedField.getSimpleName());
		Template t=new MonologTemplate(processedField, logAnnotation);
		CtClass<?> target = processedField.getParent(CtClass.class);
		Substitution.insertAll(target, t);
	}

}
