/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 Contributor(s): Renaud Pawlak (renaud.pawlak@lifl.fr)
 ==============================================================================*/

package org.objectweb.fractal.fraclet.annotation.generator.template.attribute;

import java.util.HashSet;
import java.util.Set;

import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.fractal.fraclet.annotation.generator.template.util.GetterSetterTemplate;

import spoon.reflect.Factory;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtInterface;
import spoon.reflect.declaration.ModifierKind;
import spoon.template.Local;
import spoon.template.Substitution;
import spoon.template.Template;

/**
 * A template code to manage the Attribute annotation. This template generate
 * the actual interface for the Fractal component and use a subclass to
 * introduce getter/setter.
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public class AttributeTemplate implements Template {

	@Local
	public static String ATTRIBUTESUFFIX = "AttributeController";

	/**
	 * Constructor for AttributeTemplate This constructor is called from the
	 * Requires Processor
	 * 
	 * @param spoonFactory
	 *            the spoon factory
	 * @param field
	 *            the target field
	 */
	@Local
	public AttributeTemplate(Factory spoonFactory, CtField<?> field) {

		CtClass<?> target = field.getParent(CtClass.class);
		// the name of the interface to generate
		String attributeName = target.getQualifiedName() + ATTRIBUTESUFFIX;
		String attributeSimpleName = target.getSimpleName() + ATTRIBUTESUFFIX;
		// creation of the interface
		CtInterface<?> itfTarget = spoonFactory.Interface().get(attributeName);
		if (itfTarget == null) {
			itfTarget = spoonFactory.Interface().create(target.getPackage(),
					attributeSimpleName);
			Set<ModifierKind> modifiers = new HashSet<ModifierKind>();
			modifiers.add(ModifierKind.PUBLIC);
			itfTarget.setModifiers(modifiers);
			itfTarget.getSuperInterfaces().add(
					spoonFactory.Type().createReference(
							AttributeController.class));

		}
		target.getSuperInterfaces().add(itfTarget.getReference());
		// create a GetterSetterTemplate to introduce the right get and set
		// methods
		Template t = new GetterSetterTemplate(field.getSimpleName(), field
				.getType());
		// insert all the methods to the class
		Substitution.insertAll(target, t);
		// insert all the methods to the interface
		Substitution.insertAll(itfTarget, t);
		// }
	}
}
