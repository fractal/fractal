/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 Contributor(s): Renaud Pawlak (renaud.pawlak@lifl.fr)
 ==============================================================================*/

package org.objectweb.fractal.fraclet.annotation.processor;

import org.objectweb.fractal.fraclet.annotation.annotations.Interface;
import org.objectweb.fractal.fraclet.annotation.generator.InterfaceADLGenerator;

import spoon.processing.AbstractAnnotationProcessor;
import spoon.reflect.declaration.CtInterface;
import spoon.reflect.declaration.CtSimpleType;

/**
 * An annotation processor to manage the @Interface annotation.
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public class InterfaceProcessor extends
		AbstractAnnotationProcessor<Interface, CtSimpleType<?>> {

	@Override
	public void init() {
		addProcessedAnnotationType(Interface.class);
		super.init();
	}
	
	@Override
	public boolean inferConsumedAnnotationType() {
		return false;
	}
	
	/**
	 * A constructor for the InterfaceProcessor
	 * 
	 * @param itfAnnotation
	 *            the Interface annotation
	 * @param processedItf
	 *            the CtInterface processed
	 */
	public void process(Interface itfAnnotation, CtSimpleType<?> processedItf) {
		if (CtInterface.class.isAssignableFrom(processedItf.getClass())) {
			new InterfaceADLGenerator(itfAnnotation, processedItf);
		}
	}

}
