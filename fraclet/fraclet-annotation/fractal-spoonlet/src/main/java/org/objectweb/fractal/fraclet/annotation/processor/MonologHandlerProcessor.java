/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 Contributor(s): Renaud Pawlak (renaud.pawlak@lifl.fr)
 ==============================================================================*/
package org.objectweb.fractal.fraclet.annotation.processor;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.fraclet.annotation.annotations.Monolog;
import org.objectweb.fractal.fraclet.annotation.annotations.MonologHandler;
import org.objectweb.fractal.fraclet.annotation.generator.MonologConfigGenerator;

import spoon.processing.AbstractProcessor;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;

/**
 * An annotation processor to manage the
 * 
 * <code>@MonologHandler</code> annotation.
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public class MonologHandlerProcessor extends AbstractProcessor<CtClass<?>> {
	private List<CtClass<?>> processedClasses = new ArrayList<CtClass<?>>();

	private List<MonologHandler> handlerAnnotations = new ArrayList<MonologHandler>();

	private List<Monolog> logAnnotations = new ArrayList<Monolog>();

	public MonologHandlerProcessor() {

	}

	/**
	 * A constructor for the FractalComponentProcessor
	 * 
	 * @param processedClass
	 *            the CtClass processed
	 */
	public void process(CtClass<?> processedClass) {
		MonologHandler handlerAnnotation = processedClass
				.getAnnotation(MonologHandler.class);
		if (handlerAnnotation != null) {
			// System.out.println(">>> adding handler annotation from
			// "+processedClass.getSimpleName());
			processedClasses.add(processedClass);
			handlerAnnotations.add(handlerAnnotation);
		}
		for (CtField<?> field : processedClass.getFields()) {

			Monolog logAnnotation = field.getAnnotation(Monolog.class);
			if (logAnnotation != null) {
				// System.out.println(">>> adding log annotation from
				// "+field.getSimpleName());
				logAnnotations.add(logAnnotation);
			}
		}
	}

	@Override
	public void processingDone() {
		super.processingDone();
		if (handlerAnnotations.size() > 0)
			new MonologConfigGenerator(processedClasses, handlerAnnotations,
					logAnnotations);
	}

}
