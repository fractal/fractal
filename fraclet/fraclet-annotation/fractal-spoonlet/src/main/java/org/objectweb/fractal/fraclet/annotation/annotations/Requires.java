/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 ==============================================================================*/

package org.objectweb.fractal.fraclet.annotation.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

import org.objectweb.fractal.fraclet.annotation.annotations.type.Cardinality;
import org.objectweb.fractal.fraclet.annotation.annotations.type.Contingency;
import org.objectweb.fractal.fraclet.annotation.generator.template.util.None;

import spoon.aval.annotation.structure.Prohibits;

/**
 * An annotation to inject the binding controller implementation.
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
@Prohibits(Attribute.class)
@Target(ElementType.FIELD)
public @interface Requires {
	/**
	 * 
	 * @return the name of the bound Fractal interface
	 */
	String name();

	/**
	 * 
	 * @return the signature of the interface (optional). Thanks to Java5 type
	 *         safety this parameter can be automatically computed, even with
	 *         collection interfaces (HashMap<String,Service>
	 *         collectionBinding)
	 */
	Class<?> signature() default None.class;

	/**
	 * 
	 * @return the cardinality[SINGLETON/COLLECTION] of the Fractal interface
	 */
	Cardinality cardinality() default Cardinality.SINGLETON;
	/**
	 * 
	 * @return the contingency[MANDATORY/OPTIONAL] of the Fractal interface
	 */
	 Contingency contingency() default Contingency.MANDATORY;

}
