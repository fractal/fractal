/*==============================================================================
Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
Fractal Component Model (contact: fractal@objectweb.org)

This library is free software; you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free 
Software Foundation; either version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along 
with this library; if not, write to the Free Software Foundation, Inc., 
59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
Contributor(s): Renaud Pawlak (renaud.pawlak@lifl.fr)
==============================================================================*/


package org.objectweb.fractal.fraclet.annotation.generator.template.binding;

import java.util.List;
import java.util.Map;

import spoon.template.Local;
import spoon.template.Parameter;
import spoon.template.Template;



	/**
	 * A template to manage the listFc method
	 * 
	 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
	 * 
	 */
public	class ListFcTemplate implements Template {
		
		@Parameter
		// The name of the interface (singleton bindings)
		String _itfName_;
		
		@Parameter("_name_")
		// The name of the java field (collection bindings)
		String __name_;
		
		@Local
		// The field that contains the collection bindings (collection bindings)
		Map<String,?> _name_;
		
		/**
		 * 
		 * @param name
		 *            the name of the interface in the case of a singleton binding
		 *            or the name of the field in the case of a collection binding
		 * @param collection
		 *            true if the binding is a collection binding
		 */
		public ListFcTemplate(String name, boolean collection) {
			if (collection){
				this.__name_ = name;
			}else {
				this._itfName_ = name;
			}
		}
		
		void addSingleton(List<String> __interfaces__) {
			__interfaces__.add(_itfName_);
		}
		
		void addCollection(List<String> __interfaces__){
			__interfaces__.addAll(_name_.keySet());
		}
		
	}