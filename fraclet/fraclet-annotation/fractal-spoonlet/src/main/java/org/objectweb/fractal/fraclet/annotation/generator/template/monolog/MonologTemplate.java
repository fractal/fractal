/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 Contributor(s): Renaud Pawlak (renaud.pawlak@lifl.fr)
 ==============================================================================*/
package org.objectweb.fractal.fraclet.annotation.generator.template.monolog;

import org.objectweb.fractal.fraclet.annotation.annotations.Monolog;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.LoggerFactory;

import spoon.reflect.declaration.CtField;
import spoon.template.Local;
import spoon.template.Parameter;
import spoon.template.Template;

/**
 * A template to manage the reference to the logger.
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public class MonologTemplate implements Template {

	@Local
	public MonologTemplate(CtField<?> processedField, Monolog logAnnotation) {
		this.__fractalField_ = processedField.getSimpleName();
		this._loggerName_ = logAnnotation.name();
	}

	@Local
	Logger _fractalField_;

	@Parameter
	String _loggerName_;

	// the simple name of the field
	@Parameter("_fractalField_")
	String __fractalField_;

	private LoggerFactory loggerFactory;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.objectweb.util.monolog.api.Loggable\#getLogger()
	 */
	public Logger getLogger() {
		return _fractalField_;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.objectweb.util.monolog.api.Loggable\#setLogger(org.objectweb.util.monolog.api.Logger)
	 */
	public void setLogger(Logger log) {
		_fractalField_ = log;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.objectweb.util.monolog.api.Loggable\#getLoggerFactory()
	 */
	public LoggerFactory getLoggerFactory() {
		return this.loggerFactory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.objectweb.util.monolog.api.Loggable\#setLoggerFactory(org.objectweb.util.monolog.api.LoggerFactory)
	 */
	public void setLoggerFactory(LoggerFactory factory) {
		this.loggerFactory = factory;
		_fractalField_ = getLoggerFactory().getLogger(_loggerName_);
	}

}
