/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 ==============================================================================*/

package org.objectweb.fractal.fraclet.annotation.annotations;

/**
 * <p>
 * An annotation to manage the particular case where a Fractal component uses
 * one or several Java interface imported from a jar. For instance,
 * java.lang.Runnable, which is regularly used in AOKell and Julia to bootstrap
 * a Fractal application.
 * </p>
 * <p>
 * This annotation encapsulates an array of the <code>@Interface</code> annotation. 
 * </p>
 * <p>
 * For example if your are annotating a content class which implements 3 interfaces 
 * and the java.lang.Runnable interface. And if the interface A is provided into a 
 * jar files, so you cannot annotate this interface, you may annotate your class as 
 * follows:</p>
 * <pre>
 * <code>@FractalComponent</code>
 * <code>@Provides</code>( 
 *          interfaces={
 *                <code>@Interface</code>(name="r", signature=java.lang.Runnable.class),
 *                <code>@Interface</code>("a",signature="A")
 *          }
 * )
 * public class MyContentClass implements A,B,C,Runnable {
 * (...) 
 * }
 * </pre>
 * <p>
 * This way the <code>@FractalComponent</code> annotation will look after your interfaces B, and C which are interfaces that you provide
 * with annotations, and the <code>@Provides</code> will look after your interfaces A and java.lang.Runnable.
 * </p>
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public @interface Provides {
	/**
	 * The list of annotations @Interface to declare
	 */
	Interface[] interfaces();
}
