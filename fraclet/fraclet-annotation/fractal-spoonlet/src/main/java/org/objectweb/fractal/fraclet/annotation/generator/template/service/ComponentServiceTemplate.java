/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 Contributor(s): Renaud Pawlak (renaud.pawlak@lifl.fr)
 ==============================================================================*/
package org.objectweb.fractal.fraclet.annotation.generator.template.service;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

import spoon.reflect.declaration.CtClass;
import spoon.template.Local;
import spoon.template.Parameter;
import spoon.template.Template;

/**
 * A template code to manage the introduction of specific services. In this case
 * it gets the Java reference to "Component".
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public class ComponentServiceTemplate implements Template, BindingController {

	/**
	 * Constructor for ComponentServiceTemplate. This constructor is called from
	 * the Service Processor.
	 * 
	 * @param serviceName
	 *            the name of the service
	 * @param target
	 *            the target class
	 */
	@Local
	public ComponentServiceTemplate(String serviceName, CtClass<?> target) {
		this.__service_ = serviceName;

	}

	@Local
	// A Local variable which is the reference to the singeton field
	Component _service_;

	@Parameter("_service_")
	// The name of the singleton field
	String __service_;

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.objectweb.fractal.api.control.BindingController#bindFc(java.lang.String,
	 *      java.lang.Object) Method automatically generated with Spoon
	 *      <http://spoon.gforge.inria.fr>
	 */
	public void bindFc(String clientItfName, Object serverItf)
			throws NoSuchInterfaceException, IllegalBindingException,
			IllegalLifeCycleException {

		if (clientItfName.equals("component")) {
			_service_ = ((Component) (serverItf));
			return;
		}
		throw new NoSuchInterfaceException(
				("Client interface \'" + clientItfName) + "\' is undefined.");

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.objectweb.fractal.api.control.BindingController#listFc() Method
	 *      automatically generated with Spoon <http://spoon.gforge.inria.fr>
	 */
	public String[] listFc() {
		return new String[] {};
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.objectweb.fractal.api.control.BindingController#lookupFc(java.lang.String)
	 *      Method automatically generated with Spoon
	 *      <http://spoon.gforge.inria.fr>
	 */
	public Object lookupFc(String clientItfName)
			throws NoSuchInterfaceException {
		throw new NoSuchInterfaceException("Client interface \'"
				+ clientItfName + "\' is undefined.");
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.objectweb.fractal.api.control.BindingController#unbindFc(java.lang.String)
	 *      Method automatically generated with Spoon
	 *      <http://spoon.gforge.inria.fr>
	 */
	public void unbindFc(String clientItfName) throws NoSuchInterfaceException,
			IllegalBindingException, IllegalLifeCycleException {
		throw new NoSuchInterfaceException("Client interface \'"
				+ clientItfName + "\' is undefined.");
	}
}
