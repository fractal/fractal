/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 ==============================================================================*/

package org.objectweb.fractal.fraclet.annotation.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

import spoon.aval.annotation.structure.Prohibits;

/**
 * An annotation to generate the attribute controller interface and to inject
 * its implementation into the annotated class.
 * 
 * Basically getter/setter methods are generated to control the annotated
 * fields. These getter/setter methods are assembled together to form a Java
 * interface, which implements the AttributeController interface. See the
 * AttributeController documentation from Fractal API for more informations.
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */

@Prohibits(Requires.class)
@Target(ElementType.FIELD)
public @interface Attribute {
	/**
	 * 
	 * @return used to parameterize a Fractal component (see Fractal-ADL for
	 *         more details)
	 */
	String argument() default "";

	/**
	 * 
	 * @return the initial value of the field
	 */
	String value() default "";
}
