/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 ==============================================================================*/
package org.objectweb.fractal.fraclet.annotation.annotations;

/**
 * Class annotation describing a custom Monolog handler.
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public @interface MonologHandler {
	/**
	 * Provides the name of the handler.
	 * 
	 * @return name of the handler.
	 */
	String name();

	/**
	 * Provides the Type of the handler.
	 * 
	 * @return the type of the handler (default is Console).
	 */
	String type() default "Console";

	/**
	 * Provides the output to use.
	 * 
	 * @return the output to use.
	 */
	String output() default "System.out";

	/**
	 * Provides the output pattern.
	 * 
	 * @return the output pattern.
	 */
	String pattern() default "%l: %m%n";

	/**
	 * The maximal size in bytes of the log file.
	 * 
	 * @return the maximal size in bytes of the log file to use.
	 */
	int maxSize() default 0;

	/**
	 * The maximal number of rolling files to use.
	 * 
	 * @return the maximal number of rolling files to use.
	 */
	int fileNumber() default 0;

	/**
	 * The append mode used.
	 * 
	 * @return true if the appendMode flag is enabled.
	 */
	boolean appendMode() default false;
}
