/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 ==============================================================================*/
package org.objectweb.fractal.fraclet.annotation.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 *  Field annotation refering to a Monolog logger.
 *
 * @author  Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 *
 */
@Target(ElementType.FIELD)
public @interface Monolog {

	/**
	 * Provides the name of the Fractal logger to use.
	 * 
	 * @return name of the Fractal controller reflected.
	 */
	String name();

	/**
	 * Provides the default logging level.
	 * 
	 * @return the default level to use (default is INFO).
	 */
	String level() default "INFO";

	/**
	 * Provides the list of handlers to use
	 * 
	 * @return a comma separated list of handlers.
	 */
	String[] handlers() default { "consoleHandler" };

	/**
	 * Provides the additivity flag used.
	 * 
	 * @return true if additivity is enabled.
	 */
	boolean additivity() default true;

	/**
	 * Provides the cleanHandlers flag used.
	 * 
	 * @return true if cleanHandlers is enabled.
	 */
	boolean cleanHandlers() default true;
}
