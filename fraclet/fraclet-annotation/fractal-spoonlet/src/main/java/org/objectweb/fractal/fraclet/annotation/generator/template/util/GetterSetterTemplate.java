/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 Contributor(s): Renaud Pawlak (renaud.pawlak@lifl.fr)
 ==============================================================================*/
package org.objectweb.fractal.fraclet.annotation.generator.template.util;

import spoon.reflect.reference.CtTypeReference;
import spoon.template.Local;
import spoon.template.Parameter;
import spoon.template.Template;

/**
 * A template to insert getter/setter to a given field.
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public class GetterSetterTemplate implements Template {

	/**
	 * The constructor for the GetterSetterTemplate
	 * 
	 * @param fieldName
	 *            the name of the field to instrument
	 * @param typeRef
	 *            a CtTypeReference of the type of the field (field.getType())
	 */
	@Local
	public GetterSetterTemplate(String fieldName, CtTypeReference<?> typeRef) {
		this.__fractalField_ = fieldName;
		char[] chars = fieldName.toCharArray();
		chars[0] = Character.toUpperCase(chars[0]);
		this.__FractalField_ = new String(chars);
		this._T_ = typeRef;
	}

	// the TypeReference of the field type
	@Parameter
	CtTypeReference<?> _T_;

	// a local variable to reprensent the field itself
	@Local
	_T_ _fractalField_;

	// the simple name of the field
	@Parameter("_fractalField_")
	String __fractalField_;

	@Parameter
	String __FractalField_;

	/**
	 * This method has been automatically injected by Spoon
	 * 
	 * @return the reference of the field
	 */
	public _T_ get__FractalField_() {
		return _fractalField_;
	}

	/**
	 * This method has been automatically injected by Spoon
	 * 
	 * @param value
	 *            the value to set to the field
	 */
	public void set__FractalField_(_T_ value) {
		_fractalField_ = value;
	}

}