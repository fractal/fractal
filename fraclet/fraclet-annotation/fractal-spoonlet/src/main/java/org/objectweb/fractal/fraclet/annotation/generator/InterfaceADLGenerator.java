package org.objectweb.fractal.fraclet.annotation.generator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.objectweb.fractal.fraclet.annotation.annotations.Interface;
import org.objectweb.fractal.fraclet.annotation.processor.util.ADLFileFactory;

import spoon.reflect.declaration.CtSimpleType;

/**
 * An ADL generator for the component interface definitions.
 *
 * @author  Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 *
 */
public class InterfaceADLGenerator {

	private PrintWriter pw = null;

	private Interface annotation;

	private CtSimpleType<?> itf;
	
	public InterfaceADLGenerator(Interface itfAnnotation, CtSimpleType<?> processedItf) {
		this.annotation = itfAnnotation;
		this.itf = processedItf;
		// -----------------------------------------------------
		// create .fractal file
		// -----------------------------------------------------
		File outFile = ADLFileFactory.createADLFile(processedItf.getFactory()
				.getEnvironment().getDefaultFileGenerator()
				.getOutputDirectory(), processedItf);
		try {
			pw = new PrintWriter(outFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		// -----------------------------------------------------
		// print file
		// -----------------------------------------------------

		ADLFileFactory.printLicence(pw);
		printBeginDefinition();
		ADLFileFactory.printItfSignature(pw, processedItf.getQualifiedName(),
				annotation.name(), annotation.cardinality(), annotation
						.contingency(), ADLFileFactory.ROLE_SERVER);
		printEndDefinition();

		// -----------------------------------------------------
		// closing file
		// -----------------------------------------------------
		pw.close();
	}
	
	private void printBeginDefinition() {
		pw.println("<definition name=\"" + itf.getQualifiedName() + "\">");
	}

	private void printEndDefinition() {
		pw.println("</definition>");
	}

	//	
	// @Override
	// public boolean inferConsumedAnnotationType() {
	// // the annotation won't be consumed by the processor.
	// return false;
	// }
	
}
