/*==============================================================================
 Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Fractal Component Model (contact: fractal@objectweb.org)

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)
 Contributor(s): Renaud Pawlak (renaud.pawlak@lifl.fr)
 ==============================================================================*/

package org.objectweb.fractal.fraclet.annotation.generator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.objectweb.fractal.fraclet.annotation.annotations.Monolog;
import org.objectweb.fractal.fraclet.annotation.annotations.MonologHandler;

import spoon.reflect.declaration.CtClass;

/**
 * A generator for the monolog configuration file.
 * 
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 * 
 */
public class MonologConfigGenerator {

	private PrintWriter pw = null;

	public MonologConfigGenerator(List<CtClass<?>> processedClasses,
			List<MonologHandler> monologAnnotations, List<Monolog> logAnnotations) {

		// -----------------------------------------------------
		// create monolog.properties file
		// -----------------------------------------------------
//		System.out.println(processedClasses.size());
		File outFile = createMonologFile(processedClasses.get(0).getFactory()
				.getEnvironment().getDefaultFileGenerator()
				.getOutputDirectory());
		try {
			pw = new PrintWriter(outFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		printLicence(pw);
		// TODO $plugin.wrapper value ??
		pw
				.println("monolog.classname "
						+ "org.objectweb.util.monolog.wrapper.log4j.MonologLoggerFactory");
		pw.println();
		pw.println("##########################");
		pw.println("## Handler configuration #");
		pw.println("##########################");
		pw.println();
		pw.println("## define a simple console handler");
		pw.println("handler.consoleHandler.type Console");
		pw.println("handler.consoleHandler.output System.out");
		pw.println("handler.consoleHandler.pattern %l: %m%n");

		pw.println("## define a simple file handler");
		pw.println("handler.fileHandler.type File");
		pw.println("handler.fileHandler.appendMode false");
		pw.println("handler.fileHandler.output fractal.log");
		pw.println("handler.fileHandler.pattern %-5l [%h] <%t> %m%n");
		for (MonologHandler monologAnnotation : monologAnnotations) {
			pw.println("handler." + monologAnnotation.name() + ".type "
					+ monologAnnotation.type());
			pw.println("handler." + monologAnnotation.name() + ".output "
					+ monologAnnotation.output());
			pw.println("handler." + monologAnnotation.name() + ".pattern "
					+ monologAnnotation.pattern());
			pw.println("handler." + monologAnnotation.name() + ".appendMode "
					+ monologAnnotation.appendMode());
			if (monologAnnotation.maxSize() > 0)
				pw.println("handler." + monologAnnotation.name() + ".maxSize "
						+ monologAnnotation.maxSize());
			if (monologAnnotation.fileNumber() > 0)
				pw.println("handler." + monologAnnotation.name()
						+ ".fileNumber " + monologAnnotation.fileNumber());
		}
		pw.println();
		pw.println("#########################");
		pw.println("## Logger configuration #");
		pw.println("#########################");
		pw.println();

		pw.println("logger.root.level ERROR");
		pw.println("logger.root.cleanHandlers true");
		pw.println("logger.root.handler consoleHandler");
		
		for (Monolog logAnnotation: logAnnotations){
		pw.println("logger."+logAnnotation.name()+".level "+logAnnotation.level());
		for (int i=0;i<logAnnotation.handlers().length;i++){
			pw.println("logger."+logAnnotation.name()+".handler."+i+" "+logAnnotation.handlers()[i]);		
		}
		pw.println("logger."+logAnnotation.name()+".additivity "+logAnnotation.additivity());
		pw.println("logger."+logAnnotation.name()+".cleanHandlers "+logAnnotation.cleanHandlers());
		}
		pw.close();
	}

	/**
	 * Create the monolog.properties file
	 * 
	 * @param spoonedDir
	 *            The directory where to write the file
	 */
	public static File createMonologFile(File spoonedDir) {
		File outFile = new File(spoonedDir.toString() + File.separator
				+ "Monolog.properties");
		try {
			if (outFile.canWrite())
				outFile.createNewFile();
		} catch (IOException e) {
			System.err.println("Error while processing "
					+ "@MonologHandler annotation "
					+ ", impossible to create the monolog.properties file" + e);
			return null;
		}
		return outFile;
	}

	/**
	 * Print licence declaration with the given PrintWriter
	 */
	private void printLicence(PrintWriter pw) {
		pw
				.println("## ==============================================================================");
		pw
				.println("## Fraclet annotation - Copyright (C) 2002-2006 INRIA Futurs / LIFL");
		pw.println();
		pw
				.println("## Fractal Component Model (contact: fractal@objectweb.org)");
		pw
				.println("## This library is free software; you can redistribute it and/or modify it under ");
		pw
				.println("## the terms of the GNU Lesser General Public License as published by the Free ");
		pw
				.println("## Software Foundation; either version 2.1 of the License, or any later version.");
		pw.println();
		pw
				.println("## This library is distributed in the hope that it will be useful, but WITHOUT ANY ");
		pw
				.println("## WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A ");
		pw
				.println("## PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.");
		pw.println();
		pw
				.println("## You should have received a copy of the GNU Lesser General Public License along ");
		pw
				.println("## with this library; if not, write to the Free Software Foundation, Inc., ");
		pw.println("## 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA");
		pw.println();
		pw
				.println("## Initial developer(s): Nicolas Pessemier (nicolas.pessemier@lifl.fr)");
		pw
				.println("## ==============================================================================");
		pw.println();
	}
}
