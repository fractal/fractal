/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.document;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;

import java.util.Map;

import spoon.reflect.declaration.CtType;

/**
 * Fractal ADL description factory.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class DocumentFactory {
    /** List of descriptions already created. */
    private Map<String, Document> documents = newHashMap();

    /**
     * Provides the reference of the Fractal ADL description associated to the
     * component. Creates a new definition if no description has been already
     * created.
     * 
     * @param cls the component processed.
     * @return the reference of the Fractal ADL description associated to this
     *         component.
     */
    public Document document(CtType<?> cls) {
        final String name = cls.getQualifiedName();
        if (this.documents.get(name) == null)
            this.documents.put(name, new Document(name));
        return this.documents.get(name);
    }

    /**
     * Provides the reference of the Fractal ADL definition associated to the
     * component.
     * 
     * @param cls the component processed.
     * @return the reference of the Fractal ADL definition associated to this
     *         component.
     * @throws RuntimeException exception thrown if the root definition
     */
    public Element definition(CtType<?> cls) {
        final String name = cls.getQualifiedName();
        return checkNotNull(checkNotNull(documents.get(name),
                "Document <" + name + "> does not exist: " + this.documents)
                .getRootElement(), "Definition <" + name + "> is empy");
    }

    /**
     * Applies a visitor on each Fractal ADL description created.
     * 
     * @param v the visitor to apply.
     */
    public void accept(DocumentVisitor v) {
        for (Document d : documents.values())
            d.accept(v);
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return this.documents.keySet().toString();
    }
}
