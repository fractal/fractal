/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.document;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;

import java.util.List;
import java.util.Map;

/**
 * Element of a Fractal ADL description.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class Element {
    private final String identifier;
    private final Map<String, Object> attributes = newHashMap();
    private final List<Element> childs = newArrayList();

    /**
     * Element constructor.
     * 
     * @param id identifier of the element.
     */
    public Element(String id) {
        this.identifier = id;
    }

    /**
     * Provides the list of attributes associated to the element.
     * 
     * @return the map containing the list of attributes and their values.
     */
    public Map<String, Object> getAttributes() {
        return this.attributes;
    }

    /**
     * Provides the list of child elements associated to the element.
     * 
     * @return the list containing the child elements.
     */
    public List<Element> getChilds() {
        return this.childs;
    }

    /**
     * Provides the identifier of the element.
     * 
     * @return the element identifier.
     */
    public String getName() {
        return this.identifier;
    }

    /**
     * Associates an attribute to the element
     * 
     * @param n name of the attribute.
     * @param v value of the attribute.
     */
    public void setAttribute(String n, Object v) {
        this.attributes.put(n, v);
    }

    /**
     * Provides the value of a given attribute.
     * 
     * @param n name of the attribute
     * @return value of the attribute (or Constants.EMPTY if not found)
     */
    public Object getAttribute(String n) {
        return this.attributes.get(n);
    }

    /**
     * Applies a given visitor to the element.
     * 
     * @param visitor visitor to apply on the element.
     */
    public void accept(ElementVisitor visitor) {
        boolean hasChild = childs.size() > 0;
        ElementVisitor v = visitor.startElement(this.identifier,
                this.attributes, hasChild);
        for (Element elt : this.childs)
            elt.accept(v);
        visitor.endElement(this.identifier, hasChild);
    }
}
