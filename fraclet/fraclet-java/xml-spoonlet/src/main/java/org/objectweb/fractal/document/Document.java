/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.document;

import static org.objectweb.fractal.fraclet.types.Constants.EMPTY;

/**
 * Representation of a Fractal ADL description.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class Document {
    private static final String PKG_SEPARATOR = ".";
    private static final String DIR_SEPARATOR = "/";

    private String filename;
    private String header;
    private Element root;

    /**
     * Document constructor.
     * 
     * @param n fullname of the document.
     */
    public Document(String n) {
        this.setFullname(n);
    }

    /**
     * Defines the header of the document.
     * 
     * @param header label of the document header.
     */
    public void setHeader(String header) {
        this.header = header;
    }

    /**
     * Defines the root element of the document.
     * 
     * @param elt the root element associated to the document.
     */
    public void setRootElement(Element elt) {
        this.root = elt;
    }

    /**
     * Provides the root element of the document.
     * 
     * @return the root element associated to the document.
     */
    public Element getRootElement() {
        return this.root;
    }

    /**
     * Defines the full name of the document.
     * 
     * @param name fully-qualified name associated to the document.
     */
    public void setFullname(String name) {
        this.filename = name.replace(PKG_SEPARATOR, DIR_SEPARATOR);
    }

    /**
     * Provides the file name associated to the document.
     * 
     * @return the file name of the document (without extension).
     */
    public String getName() {
        int ind = this.filename.lastIndexOf(DIR_SEPARATOR);
        return (ind == -1) ? this.filename : this.filename.substring(ind + 1);
    }

    /**
     * Provides the directory associated to the document.
     * 
     * @return the directory used to store the document.
     */
    public String getDirectory() {
        int ind = this.filename.lastIndexOf(DIR_SEPARATOR);
        return (ind == -1) ? EMPTY : this.filename.substring(0, ind);
    }

    /**
     * Applies a visitor on the document.
     * 
     * @param visitor the visitor to apply.
     */
    public void accept(DocumentVisitor visitor) {
        if (this.root ==null) // Empty document
            return;
        ElementVisitor elt = visitor.startDocument(getDirectory(), getName(),
                this.header);
        this.root.accept(elt);
        visitor.endDocument();
    }
}