/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.spoonlet.controller;

import static org.objectweb.fractal.spoonlet.controller.ControllerHelper.controllerName;
import static spoon.template.Substitution.insertAll;

import org.objectweb.fractal.fraclet.annotations.Controller;
import org.objectweb.fractal.spoonlet.binding.AbstractBindingProcessor;
import org.objectweb.fractal.spoonlet.binding.EmptyBindingControllerTemplate;

import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;

/**
 * An annotation processor to manage the <code>Controller</code> annotation.
 * 
 * @author Romain Rouvoy (Contributor)
 * @author Nicolas Pessemier (Initial developer)
 * @version $Revision$
 */
public class ControllerProcessor extends AbstractBindingProcessor<Controller> {
    /* (non-Javadoc)
     * @see spoon.processing.AbstractProcessor#init()
     */
    @Override
    public void init() {
        addProcessedAnnotationType(Controller.class);
        super.init();
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.spoonlet.binding.AbstractBindingProcessor#createMethodSignatures(CtClass)
     */
    @Override
    protected void createMethodSignatures(CtClass<?> comp) {
        insertAll(comp, new EmptyBindingControllerTemplate());
    }
    
    /* (non-Javadoc)
     * @see org.objectweb.fractal.spoonlet.binding.BindingProcessor#insertMethodBodies(spoon.reflect.declaration.CtClass, spoon.reflect.declaration.CtField, java.lang.annotation.Annotation)
     */
    @Override
    protected void insertMethodBodies(CtClass<?> cls, CtField<?> fld,
            Controller ann) {
        String name = controllerName(fld.getReference(), ann);
        insertBegin(new ControllerBindingTemplate(fld, name), cls, "bindFc",
                String.class, Object.class);
    }
}
