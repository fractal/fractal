/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier
 Contributor(s): Romain Rouvoy
 =============================================================================*/
package org.objectweb.fractal.spoonlet.attribute;

import static java.lang.Character.toUpperCase;
import static org.objectweb.fractal.fraclet.types.Constants.isEmpty;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.getFieldAnnotation;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.hasFieldAnnotation;

import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.fractal.fraclet.annotations.Attribute;

import spoon.reflect.reference.CtFieldReference;
import spoon.reflect.reference.CtTypeReference;

/**
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class AttributeHelper {
    /** Generated attribute controller suffix. */
    public static String ATTRIBUTE_SUFFIX = "Attributes";

    /** Constant defining the attribute controller. */
    public static final Class<AttributeController> AC = AttributeController.class;

    /**
     * Provides the name associated to the component attribute.
     * 
     * @param <T> the type of the field.
     * @param f field representing the component attribute.
     * @return the name of the component attribute.
     */
    public static final <T> String attributeName(CtFieldReference<T> f) {
        Attribute a = getFieldAnnotation(f, Attribute.class);
        if (a == null)
            throw new RuntimeException(f.getQualifiedName()
                    + "is not a component attribute");
        return attributeName(f, a);
    }

    /**
     * Provides the name associated to the component attribute.
     * 
     * @param <T> the type of the field.
     * @param f field representing the component attribute.
     * @param a annotation <code>Attribute</code> marking the field.
     * @return the name of the component attribute.
     */
    public static final <T> String attributeName(CtFieldReference<T> f,
            Attribute a) {
        return (isEmpty(a.name())) ? f.getSimpleName() : a.name();
    }

    /**
     * Provides the default value associated to the component attribute.
     * 
     * @param <T> the type of the field.
     * @param f field representing the component attribute.
     * @return the default value associated to the component attribute.
     */
    public static final <T> String attributeValue(CtFieldReference<T> f) {
        Attribute a = getFieldAnnotation(f, Attribute.class);
        if (a == null)
            throw new RuntimeException(f.getQualifiedName()
                    + "is not a component attribute");
        return attributeValue(f, a);
    }

    /**
     * Provides the default value associated to the component attribute.
     * 
     * @param <T> the type of the field.
     * @param f field representing the component attribute.
     * @param a annotation <code>Attribute</code> marking the field.
     * @return the default value associated to the component attribute.
     */
    public static final <T> String attributeValue(CtFieldReference<T> f,
            Attribute a) {
        return (isEmpty(a.value())) ? null : a.value();
    }

    /**
     * Method signature of an attribute setter.
     * 
     * @param name name of the attribute.
     * @return simple name of the setter method associated to the attribute.
     */
    public static final String attributeSetterName(String name) {
        char[] chars = name.toCharArray();
        chars[0] = toUpperCase(chars[0]);
        return "set" + new String(chars);
    }

    /**
     * Method signature of an attribute getter.
     * 
     * @param name name of the attribute.
     * @return simple name of the getter method associated to the attribute.
     */
    public static final String attributeGetterName(String name) {
        char[] chars = name.toCharArray();
        chars[0] = toUpperCase(chars[0]);
        return "get" + new String(chars);
    }

    /**
     * Provides the name of the associated attribute controller.
     * 
     * @param <T> the type of the component.
     * @param type class containing the attributes.
     * @return the name of the associated attribute controller.
     */
    public static final <T> String controllerName(CtTypeReference<T> type) {
        return type.getSimpleName() + ATTRIBUTE_SUFFIX;
    }

    /**
     * Provides the fully qualified name of the associated attribute controller.
     * 
     * @param <T> the type of the component.
     * @param comp the component processed.
     * @return the fully qualified name of the associated attribute controller.
     */
    public static final <T> String controllerFullname(CtTypeReference<T> type) {
        return type.getQualifiedName() + ATTRIBUTE_SUFFIX;
    }

    /**
     * Provides the attribute controller reference associated to a component.
     * 
     * @param <T> the type of the component.
     * @param type the component processed.
     * @return the reference of the associated attribute controller.
     */
    public static final <T> CtTypeReference<?> attributeController(
            CtTypeReference<T> type) {
        if (hasFieldAnnotation(type, Attribute.class, false))
            return type;
        if (type.getSuperclass() == null)
            return null;
        return attributeController(type.getSuperclass());
    }
}