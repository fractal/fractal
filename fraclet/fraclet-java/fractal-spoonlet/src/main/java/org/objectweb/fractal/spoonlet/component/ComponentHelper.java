/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.spoonlet.component;

import static org.objectweb.fractal.fraclet.types.Constants.isEmpty;

import java.lang.annotation.Annotation;
import java.util.Collection;

import org.objectweb.fractal.fraclet.annotations.Component;

import spoon.reflect.reference.CtExecutableReference;
import spoon.reflect.reference.CtFieldReference;
import spoon.reflect.reference.CtTypeReference;

/**
 * Convenience class providing methods for handling the component concepts.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class ComponentHelper {
    /**
     * Checks if the component supports a given controller.
     * 
     * @param type the component processed.
     * @param ctrl the controller requested.
     * @param recursive checks recursively when true.
     * @return true if the component already implements the requested
     *         controller.
     */
    public static final boolean hasController(final CtTypeReference<?> type,
            final CtTypeReference<?> ctrl, final boolean recursive) {
        if (type == null)
            return false;
        if (recursive)
            return type.isSubtypeOf(ctrl);
        for (CtTypeReference<?> itf : type.getSuperInterfaces())
            if (itf.isSubtypeOf(ctrl))
                return true;
        return false;
    }

    /**
     * @param type
     * @param ann
     * @param recursive
     * @return
     */
    public static final boolean hasFieldAnnotation(
            final CtTypeReference<?> type,
            final Class<? extends Annotation> ann, final boolean recursive) {
        if (type == null)
            return false;
        for (CtFieldReference<?> f : recursive ? type.getAllFields() : type
                .getDeclaredFields())
            if (getFieldAnnotation(f, ann) != null)
                return true;
        return false;
    }

    /**
     * Return the field annotation associated to the field
     * 
     * @param <T>
     * @param field
     * @param annotation
     * @return
     */
    public static final <T extends Annotation> T getFieldAnnotation(
            final CtFieldReference<?> field, final Class<T> annotation) {
        try {
            return field.getAnnotation(annotation);
        } catch (RuntimeException ex) {
            if (ex.getCause() instanceof ClassNotFoundException)
                return null;
            throw ex;
        }
    }

    /**
     * Checks if the component declared a given method annotation.
     * 
     * @param type the component processed.
     * @param ann the annotation requested.
     * @param recursive checks recursively when true.
     * @return true if the component already implements the requested
     *         controller.
     */
    public static final boolean hasMethodAnnotation(
            final CtTypeReference<?> type,
            final Class<? extends Annotation> ann, final boolean recursive) {
        if (type == null)
            return false;
        for (CtExecutableReference<?> m : recursive ? type.getAllExecutables()
                : type.getDeclaredExecutables())
            if (getMethodAnnotation(m, ann) != null)
                return true;
        return false;
    }

    /**
     * Return the method annotation associated to the method.
     * 
     * @param <T>
     * @param field
     * @param annotation
     * @return
     */
    public static final <T extends Annotation> T getMethodAnnotation(
            final CtExecutableReference<?> method, final Class<T> annotation) {
        try {
            return method.getAnnotation(annotation);
        } catch (RuntimeException ex) {
            if (ex.getCause() instanceof ClassNotFoundException)
                return null;
            throw ex;
        }
    }

    /**
     * Return the type annotation associated to the type.
     * 
     * @param <T>
     * @param type
     * @param annotation
     * @return
     */
    public static final <T extends Annotation> T getTypeAnnotation(
            final CtTypeReference<?> type, final Class<T> annotation) {
        try {
            return type.getAnnotation(annotation);
        } catch (RuntimeException ex) {
            if (ex.getCause() instanceof ClassNotFoundException)
                return null;
            throw ex;
        }
    }

    /**
     * Provides the fully-qualified name of the component.
     * 
     * @param type the component processed.
     * @param ann the annotation <code>Component</code> processed.
     * @return the fully-qualified name of the component.
     */
    public static final String componentFullname(final CtTypeReference<?> type,
            Component ann) {
        return isEmpty(ann.name()) ? type.getQualifiedName() : ann.name();
    }

    /**
     * Find the reference of a method identified by its simple name and its
     * arguments among the class hierarchy.
     * 
     * @param cls the class containing the method.
     * @param name identifier of the method.
     * @param arguments list of arguments of the method.
     * @return the reference of the method found (<code>null</code> is not
     *         found).
     */
    public static final CtExecutableReference<?> findMethod(
            CtTypeReference<?> cls, String name,
            CtTypeReference<?>... arguments) {
        for (CtExecutableReference<?> m : cls.getAllExecutables()) {
            if (!m.isConstructor() && name.equals(m.getSimpleName())) {
                Collection<CtTypeReference<?>> args = m.getParameterTypes();
                boolean checked = true;
                for (CtTypeReference<?> arg : arguments)
                    if (!args.contains(arg)) {
                        checked = false;
                        break;
                    }
                if (checked)
                    return m;
            }
        }
        return null;
    }
}