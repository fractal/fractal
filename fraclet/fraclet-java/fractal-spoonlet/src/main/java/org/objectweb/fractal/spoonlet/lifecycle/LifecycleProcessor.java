/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier
 Contributor(s): Romain Rouvoy
 =============================================================================*/
package org.objectweb.fractal.spoonlet.lifecycle;

import static org.objectweb.fractal.spoonlet.component.ComponentHelper.hasController;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.hasMethodAnnotation;
import static spoon.processing.Severity.WARNING;
import static spoon.template.Substitution.insertAll;
import static spoon.template.Substitution.substituteMethodBody;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.fraclet.annotations.Lifecycle;

import spoon.processing.AbstractAnnotationProcessor;
import spoon.reflect.code.CtInvocation;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.reference.CtExecutableReference;
import spoon.reflect.reference.CtTypeReference;
import spoon.template.Template;

/**
 * An annotation processor to manage the Fraclet annotation
 * <code>Requires</code>.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class LifecycleProcessor extends
        AbstractAnnotationProcessor<Lifecycle, CtMethod<?>> {
    /** List of lifecycle methods defined by Fractal. */
    private static final String[] KEYWORDS = { "createFc", "startFc", "stopFc",
            "destroyFc" };
    /** Constant referring to the lifecycle controller. */
    public static final Class<LifeCycleController> LCC = LifeCycleController.class;

    /* (non-Javadoc)
     * @see spoon.processing.AbstractProcessor#init()
     */
    @Override
    public void init() {
        addProcessedAnnotationType(Lifecycle.class);
        super.init();
    }

    /* (non-Javadoc)
     * @see spoon.processing.AbstractAnnotationProcessor#inferConsumedAnnotationType()
     */
    @Override
    public boolean inferConsumedAnnotationType() {
        return false;
    }

    /* (non-Javadoc)
     * @see spoon.processing.AnnotationProcessor#process(java.lang.annotation.Annotation, spoon.reflect.declaration.CtElement)
     */
    public void process(Lifecycle ann, CtMethod<?> mtd) {
        CtClass<?> comp = mtd.getParent(CtClass.class);
        getEnvironment().debugMessage(
                "[Fractal] Processing method " + comp.getSimpleName() + "."
                        + mtd.getSimpleName() + "...");
        check(ann, mtd);
        insertMethodSignatures(comp, mtd, ann);
        insertMethodBodies(comp, mtd, ann);
    }

    /**
     * Checks that the method name does not conflict with lifecycle controller
     * keywords.
     * 
     * @param ann the annotation Lifecycle processed.
     * @param mtd the method lifecycle processed.
     */
    private void check(Lifecycle ann, CtMethod<?> mtd) {
        for (String keyword : KEYWORDS)
            if (keyword.equals(mtd.getSimpleName())) {
                final String renamed = "_" + mtd.getSimpleName();
                getEnvironment().report(
                        this,
                        WARNING,
                        mtd,
                        "Method " + mtd.getSignature()
                                + " has been renamed to " + renamed
                                + " to prevent recursive invocation.");
                mtd.setSimpleName(renamed);
            }
    }

    /**
     * Insert the signatures of the lifecycle controller if needed.
     * 
     * @param cls the class to modify.
     * @param mtd the method annotated with the annotation Lifecycle.
     * @param ann the annotation Lifecycle.
     */
    protected void insertMethodSignatures(CtClass<?> cls, CtMethod<?> mtd,
            Lifecycle ann) {
        final CtTypeReference<LifeCycleController> lcc = getFactory()
                .Interface().createReference(LCC);
        if (hasController(cls.getReference(), lcc, false))
            return; // Class already implements LifecycleController interface
        CtTypeReference<?> ref = cls.getSuperclass();
        if (ref == null) { // No parent class
            // CtConstructor<?> co = comp.getConstructor();
            // if (co.isImplicit()) comp.getConstructors().remove(co);
            insertAll(cls, new LifecycleControllerTemplate(mtd));
        } else if (hasController(ref, lcc, true)
                || hasMethodAnnotation(ref, Lifecycle.class, true))
            insertAll(cls, new LifecycleExtensionTemplate());
        else {
            // CtConstructor<?> co = comp.getConstructor();
            // if (co.isImplicit()) comp.getConstructors().remove(co);
            insertAll(cls, new LifecycleControllerTemplate(mtd));
        }
    }

    /**
     * Insert a new body in the methods.
     * 
     * @param c reference to the class
     * @param m reference to the field
     * @param a reference to the annotation
     */
    @SuppressWarnings("unchecked")
    protected void insertMethodBodies(CtClass<?> c, CtMethod<?> m, Lifecycle a) {
        CtInvocation<?> invocation = m.getFactory().Core().createInvocation();
        invocation.setTarget(m.getFactory().Code().createThisAccess(c.getReference()));
//      m.getFactory().Type().createReference(m.getParent(CtClass.class).getActualClass())));
        invocation.setExecutable((CtExecutableReference) m.getReference());
        String name = "Invoke" + m.getSimpleName();
        insertEnd(
                a.policy().isSynchronous() ? new LifecycleSynchronousTemplate(
                        invocation) : new LifecycleAsynchronousTemplate(
                        invocation, name), c, KEYWORDS[a.step().value()]);
    }

    /**
     * Insert a piece of code in at the beginning of a method.
     * 
     * @param tmpl template used for generating the piece of code to insert.
     * @param comp target class for the insertion
     * @param method target method for the insertion
     * @param params method parameters
     */
    protected void insertEnd(Template tmpl, CtClass<?> comp, String method,
            Class<?>... params) {
        List<CtTypeReference<?>> types = new ArrayList<CtTypeReference<?>>();
        for (Class<?> cls : params)
            types.add(getFactory().Type().createReference(cls));
        CtTypeReference<?>[] args = types.toArray(new CtTypeReference<?>[0]);
        comp.getMethod(method, args).getBody().insertEnd(
                getFactory().Code().createStatementList(
                        substituteMethodBody(comp, tmpl, method, args)));
    }
}