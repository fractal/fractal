/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.spoonlet.attribute;

import static org.objectweb.fractal.fraclet.types.Constants.isEmpty;
import static org.objectweb.fractal.spoonlet.attribute.AttributeHelper.AC;
import static org.objectweb.fractal.spoonlet.attribute.AttributeHelper.attributeGetterName;
import static org.objectweb.fractal.spoonlet.attribute.AttributeHelper.attributeName;
import static org.objectweb.fractal.spoonlet.attribute.AttributeHelper.attributeSetterName;
import static org.objectweb.fractal.spoonlet.attribute.AttributeHelper.controllerFullname;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.findMethod;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.hasFieldAnnotation;
import static spoon.reflect.declaration.ModifierKind.PUBLIC;
import static spoon.template.Substitution.insertAll;

import java.util.HashSet;
import java.util.Set;

import org.objectweb.fractal.fraclet.annotations.Attribute;

import spoon.processing.AbstractAnnotationProcessor;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtInterface;
import spoon.reflect.declaration.ModifierKind;
import spoon.reflect.reference.CtExecutableReference;
import spoon.reflect.reference.CtTypeReference;
import spoon.template.Template;

/**
 * An annotation processor to manage the Fraclet annotation
 * <code>Attribute</code>.
 * 
 * @author Romain Rouvoy (Contributor)
 * @author Nicolas Pessemier (Initial Developer)
 * @version $Revision$
 */
public class AttributeProcessor extends
        AbstractAnnotationProcessor<Attribute, CtField<?>> {
    /* (non-Javadoc)
     * @see spoon.processing.AbstractProcessor#init()
     */
    @Override
    public void init() {
        addProcessedAnnotationType(Attribute.class);
        super.init();
    }

    /* (non-Javadoc)
     * @see spoon.processing.AbstractAnnotationProcessor#inferConsumedAnnotationType()
     */
    @Override
    public boolean inferConsumedAnnotationType() {
        return false;
    }

    /* (non-Javadoc)
     * @see spoon.processing.AnnotationProcessor#process(java.lang.annotation.Annotation,spoon.reflect.declaration.CtElement)
     */
    public void process(Attribute ann, CtField<?> fld) {
        CtClass<?> cls = fld.getParent(CtClass.class);
        getEnvironment().debugMessage(
                "[Fractal] Processing field " + cls.getQualifiedName() + "."
                        + fld.getSimpleName() + "...");
        CtInterface<?> ac = getFactory().Interface().get(
                controllerFullname(cls.getReference()));
        if (ac == null) // creation of the attribute controller interface
            ac = createAttributeController(cls);
        String attr = attributeName(fld.getReference(), ann);
        if (ann.mode().isReadable()) {
            getEnvironment().debugMessage(
                    "[Fractal] Inserting getter for attribute " + attr + "...");
            Template tmpl = new FieldGetterTemplate(fld, attr);
            if (!isEmpty(ann.get())) {
                CtExecutableReference<?> mtd = findMethod(cls.getReference(),
                        ann.get());
                if (mtd != null)
                    tmpl = new MethodGetterTemplate(mtd, attr);
                else
                    getEnvironment().debugMessage(
                            "[Fractal] Getter method " + ann.get()
                                    + "not found.");
            }
            insertAll(ac, new SignatureGetterTemplate(fld, attr)); // insert getter signature into the controller
            if (findMethod(cls.getReference(), attributeGetterName(attr)) == null)
                insertAll(cls, tmpl); // insert getter body into the class
        }
        if (ann.mode().isWritable()) {
            getEnvironment().debugMessage(
                    "[Fractal] Inserting setter for attribute " + attr + "...");
            Template tmpl = new FieldSetterTemplate(fld, attr);
            if (!isEmpty(ann.set())) {
                CtExecutableReference<?> mtd = findMethod(cls.getReference(),
                        ann.set(), fld.getType());
                if (mtd != null)
                    tmpl = new MethodSetterTemplate(mtd, attr);
                else
                    getEnvironment().debugMessage(
                            "[Fractal] Setter method " + ann.get()
                                    + "not found.");
            }
            insertAll(ac, new SignatureSetterTemplate(fld, attr)); // insert setter signature into the controller
            if (findMethod(cls.getReference(), attributeSetterName(attr), fld
                    .getType()) == null)
                insertAll(cls, tmpl); // insert setter body into the class
        }
    }

    /**
     * Creates an attribute controller definition from a component definition.
     * 
     * @param comp the component describing the attributes.
     * @return the associated attribute controller.
     */
    private final CtInterface<?> createAttributeController(CtClass<?> comp) {
        String name = controllerFullname(comp.getReference());
        getEnvironment().debugMessage(
                "[Fractal] Creating attribute controller " + name + "...");
        CtInterface<?> ac = getFactory().Interface().create(name);
        Set<ModifierKind> modifiers = new HashSet<ModifierKind>();
        modifiers.add(PUBLIC);
        ac.setModifiers(modifiers);
        ac.getSuperInterfaces().add(
                getSuperAttributeController(comp.getSuperclass()));
        comp.getSuperInterfaces().add(ac.getReference());
        return ac;
    }

    /**
     * Provides the reference of the super attribute controller class.
     * 
     * @param type class containing the attributes.
     * @return the reference of the super attribute controller class.
     */
    private final CtTypeReference<?> getSuperAttributeController(
            CtTypeReference<?> ref) {
        if (ref == null)
            return getFactory().Type().createReference(AC);
        if (hasFieldAnnotation(ref, Attribute.class, false))
            return getFactory().Type().createReference(controllerFullname(ref));
        return getSuperAttributeController(ref.getSuperclass());
    }
}