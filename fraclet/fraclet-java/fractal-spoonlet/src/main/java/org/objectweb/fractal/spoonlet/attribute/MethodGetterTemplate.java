/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier
 Contributor(s): Romain Rouvoy
 =============================================================================*/
package org.objectweb.fractal.spoonlet.attribute;

import static org.objectweb.fractal.spoonlet.attribute.AttributeHelper.attributeGetterName;

import org.objectweb.fractal.spoonlet.utils._T_;

import spoon.reflect.reference.CtExecutableReference;
import spoon.reflect.reference.CtTypeReference;
import spoon.template.Local;
import spoon.template.Parameter;
import spoon.template.Template;

/**
 * A template to insert getter/setter to a given field.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class MethodGetterTemplate implements Template {
    @Parameter // TypeReference of the attribute type
    protected CtTypeReference<?> _T_;
    @Parameter
    protected String _GetAttribute_;
    @Parameter("_getter_")
    protected String _methodName_;

    /**
     * The constructor for the MethodGetterTemplate
     * 
     * @param mtd the reference of the field considered.
     * @param name the name of the attribute.
     */
    @Local
    public MethodGetterTemplate(CtExecutableReference<?> mtd, String name) {
        this._methodName_ = mtd.getSimpleName();
        this._T_ = mtd.getType();
        this._GetAttribute_ = attributeGetterName(name);
    }

    @Local
    _T_ _getter_() { // local method for getting the value
        return null;
    }

    // ***************************** TEMPLATE PART *****************************
    /**
     * Provides the value of the _AttributeName_ attribute.
     * 
     * @return the value of the associated field.
     */
    public synchronized _T_ _GetAttribute_() {
        return this._getter_();
    }
}