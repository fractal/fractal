/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): Nicolas Pessemier, Renaud Pawlak
 =============================================================================*/
package org.objectweb.fractal.spoonlet.controller;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.spoonlet.utils._T_;

import spoon.reflect.declaration.CtField;
import spoon.reflect.reference.CtTypeReference;
import spoon.template.Local;
import spoon.template.Parameter;
import spoon.template.Template;

/**
 * A template to introduce the binding controller implementation that supports
 * reflection capabilities via controller interfaces.
 * 
 * @author Romain Rouvoy (Contributor)
 * @author Nicolas Pessemier (Initial developer)
 * @version $Revision$
 */
public class ControllerBindingTemplate implements Template {
    @Parameter // The name of the singleton field
    protected String _ControllerName_;
    @Parameter // A Type Reference which is the type of the control interface
    protected CtTypeReference<?> _T_;
    @Parameter("_controller_")
    protected String _fieldName_;

    /**
     * Use this constructor for singleton bindings
     * 
     * @param fld the reference of the field considered.
     * @param name the name of the controller.
     */
    public ControllerBindingTemplate(CtField<?> fld, String name) {
        this._fieldName_ = fld.getSimpleName();
        this._T_ = fld.getType();
        this._ControllerName_ = name;
    }

    @Local // A Local variable which is the reference to the singleton field
    _T_ _controller_;

    // ***************************** TEMPLATE PART *****************************
    /**
     * Block to add into the bindFc method (for singleton bindings)
     * 
     * @param id the Fractal binding name
     * @param ref the reference to the server
     * @throws NoSuchInterfaceException
     * @throws IllegalBindingException when attempting to bind incompatible
     *             interfaces
     */
    public synchronized void bindFc(String id, Object ref) throws NoSuchInterfaceException {
        if (id.equals("component"))
            this._controller_ = (_T_) ((Component) ref).getFcInterface(_ControllerName_);
    }
}
