/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): Nicolas Pessemier
 =============================================================================*/
package org.objectweb.fractal.spoonlet.lifecycle;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;

import spoon.reflect.declaration.CtMethod;
import spoon.template.Local;
import spoon.template.Template;

/**
 * A template code to manage the LifeCycle annotation. This template introduces
 * the implementation of the LifeCycle controller and then call the annotated
 * method in the start or stop method (depending on the parameter of the
 * annotation).
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class LifecycleControllerTemplate implements LifeCycleController,
        Template {
    /**
     * Default constructor.
     */
    @Local
    public LifecycleControllerTemplate(CtMethod<?> m) {
    }

    // ***************************** TEMPLATE PART *****************************
    private boolean __fc_started__ = false;

    /**
     * 
     */
    public LifecycleControllerTemplate() {
        super();
        try {
            createFc();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    protected synchronized void createFc() throws Throwable {
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.LifeCycleController#getFcState()
     */
    public synchronized String getFcState() {
        return __fc_started__ ? STARTED : STOPPED;
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.LifeCycleController#startFc()
     */
    public synchronized void startFc() throws IllegalLifeCycleException {
        __fc_started__ = true;
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.LifeCycleController#stopFc()
     */
    public synchronized void stopFc() throws IllegalLifeCycleException {
        __fc_started__ = false;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#finalize()
     */
    protected void finalize() throws Throwable {
        super.finalize();
        destroyFc();
    }

    protected void destroyFc() throws Throwable {
    }
}
