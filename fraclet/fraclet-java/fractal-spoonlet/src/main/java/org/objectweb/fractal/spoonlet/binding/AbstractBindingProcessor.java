/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2008 Universitetet i Oslo
 Copyright (C) 2008-2009 INRIA Lille - Nord Europe / LIFL

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.spoonlet.binding;

import static org.objectweb.fractal.spoonlet.binding.BindingHelper.BC;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.hasController;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.hasFieldAnnotation;
import static spoon.template.Substitution.insertAll;
import static spoon.template.Substitution.substituteMethodBody;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.fraclet.annotations.Requires;

import spoon.processing.AbstractAnnotationProcessor;
import spoon.reflect.code.CtStatement;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;
import spoon.reflect.reference.CtTypeReference;
import spoon.template.Template;

/**
 * Abstract binding processor.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 * @param <A> Type of annotation processed by the binding processor.
 */
public abstract class AbstractBindingProcessor<A extends Annotation> extends
        AbstractAnnotationProcessor<A, CtField<?>> {
    protected static final String[] KEYWORDS = { "listFc", "bindFc",
            "unbindFc", "lookupFc" };

    /* (non-Javadoc)
     * @see spoon.processing.AbstractAnnotationProcessor#inferConsumedAnnotationType()
     */
    @Override
    public boolean inferConsumedAnnotationType() {
        return false;
    }

    /* (non-Javadoc)
     * @see spoon.processing.AnnotationProcessor#process(java.lang.annotation.Annotation, spoon.reflect.declaration.CtElement)
     */
    public void process(A ann, CtField<?> fld) {
        CtClass<?> comp = fld.getParent(CtClass.class);
        getEnvironment().debugMessage(
                "[Fractal] Processing field " + comp.getQualifiedName() + ":"
                        + fld.getSimpleName() + "<" + fld.getSignature()
                        + ">...");
        insertMethodSignatures(comp, fld, ann);
        insertMethodBodies(comp, fld, ann);
    }

    /**
     * Inserts the method signatures associated to the processed field.
     * 
     * @param comp the component processed.
     * @param fld the field processed.
     * @param ann the annotation processed.
     */
    protected void insertMethodSignatures(CtClass<?> comp, CtField<?> fld, A ann) {
        CtTypeReference<BindingController> bc = getFactory().Interface()
                .createReference(BC);
        if (hasController(comp.getReference(), bc, false))
            return; // Class already implements BindingController (BC)
        CtTypeReference<?> ref = comp.getSuperclass();
        if (ref == null) // No parent class
            createMethodSignatures(comp);
        else if (hasController(ref, bc, true) // Parent class implements BC
                || hasFieldAnnotation(ref, Requires.class, true))
            insertAll(comp, new BindingExtensionTemplate());
        else
            createMethodSignatures(comp);
    }

    protected abstract void createMethodSignatures(CtClass<?> comp);

    /**
     * Insert a new body in the methods.
     * 
     * @param c reference to the class
     * @param f reference to the field
     * @param a reference to the annotation
     */
    protected abstract void insertMethodBodies(CtClass<?> c, CtField<?> f, A a);

    /**
     * Insert a piece of code in at the beginning of a method.
     * 
     * @param tmpl template used for generating the piece of code to insert.
     * @param cls target class for the insertion
     * @param mtd target method for the insertion
     * @param params method parameters
     * @return
     */
    protected void insertBegin(Template tmpl, CtClass<?> cls, String mtd,
            Class<?>... params) {
        List<CtTypeReference<?>> types = new ArrayList<CtTypeReference<?>>();
        for (Class<?> param : params)
            types.add(getFactory().Type().createReference(param));
        CtTypeReference<?>[] args = types.toArray(new CtTypeReference<?>[0]);
        CtStatement code = substituteMethodBody(cls, tmpl, mtd, args)
                .getStatements().get(0);
        cls.getMethod(mtd, args).getBody().insertBegin(code);
    }
}