/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.spoonlet.component;

import static org.objectweb.fractal.fraclet.types.Constants.isEmpty;
import static org.objectweb.fractal.fraclet.types.Constants.isExplicit;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.getTypeAnnotation;

import org.objectweb.fractal.fraclet.annotations.Interface;

import spoon.reflect.reference.CtTypeReference;

/**
 * Convenience class providing methods for handling the interface concepts.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class InterfaceHelper {
    /** Server role constant defined by the specification. */
    public static final String ROLE_SERVER = "server";
    /** Client role constant defined by the specification. */
    public static final String ROLE_CLIENT = "client";
    /** Mandatory contingency constant defined by the specification. */
    public static final String CONTINGENCY_MANDATORY = "mandatory";
    /** Optional contingency constant defined by the specification. */
    public static final String CONTINGENCY_OPTIONAL = "optional";
    /** Singleton cardinality constant defined by the specification. */
    public static final String CARDINALITY_SINGLETON = "singleton";
    /** Collection cardinality constant defined by the specification. */
    public static final String CARDINALITY_COLLECTION = "collection";

    /** Default role constant defined by the specification. */
    public static final String DEFAULT_ROLE = ROLE_SERVER;
    /** Default contingency constant defined by the specification. */
    public static final String DEFAULT_CONTINGENCY = CONTINGENCY_MANDATORY;
    /** Default cardinality constant defined by the specification. */
    public static final String DEFAULT_CARDINALITY = CARDINALITY_SINGLETON;

    /**
     * Provides the signature of the server interface.
     * 
     * @param ann annotation <code>Interface</code> processed.
     * @param itf interface type processed.
     * @return the full signature of the interface.
     */
    public static final String interfaceSignature(Interface ann, CtTypeReference<?> itf) {
        if (isExplicit(ann.signature()))
            return ann.signature().getName();
        return itf.getQualifiedName();
    }

    /**
     * Provides the name of the server interface.
     * 
     * @param ann annotation <code>Interface</code> processed.
     * @param itf interface type processed.
     * @return the identifier of the interface.
     */
    public static final String interfaceName(Interface ann, CtTypeReference<?> itf) {
        if (isEmpty(ann.name()))
            return itf.getSimpleName().toLowerCase();
        return ann.name();
    }
    
    /**
     * Provides the name of the server interface.
     * 
     * @param ann annotation <code>Interface</code> processed.
     * @param itf interface type processed.
     * @return the identifier of the interface.
     */
    public static final String interfaceName(CtTypeReference<?> itf) {
        Interface ann = getTypeAnnotation(itf,Interface.class);
        if (ann==null)
            return itf.getSimpleName().toLowerCase();
        return interfaceName(ann,itf);
    }
}