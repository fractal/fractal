/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier
 Contributor(s): Romain Rouvoy
 =============================================================================*/
package org.objectweb.fractal.spoonlet.lifecycle;

import org.objectweb.fractal.api.control.LifeCycleController;

import spoon.reflect.code.CtInvocation;
import spoon.template.Local;
import spoon.template.Parameter;
import spoon.template.Template;

/**
 * A template code to manage the LifeCycle annotation. This template introduces
 * the implementation of the LifeCycle controller and then call the annotated
 * method in the start or stop method (depending on the parameter of the
 * annotation).
 * 
 * @author Romain Rouvoy (Contributor)
 * @author Nicolas Pessemier (Initial developer)
 * @version $Revision$
 */
public class LifecycleAsynchronousTemplate implements LifeCycleController,
        Template {

    @Parameter
    CtInvocation<?> _methodCall_;
    @Parameter
    protected String _ThreadClass_;

    /**
     * Template constructor.
     * 
     * @param m the method considered
     */
    @Local
    public LifecycleAsynchronousTemplate(CtInvocation<?> i, String thread) {
        _methodCall_ = i;
        _ThreadClass_ = thread;
    }

    // ***************************** TEMPLATE PART *****************************

    /**
     * Component constructor;
     */
    protected void createFc() {
        Runnable r = new Runnable() {
            public void run() {
                try {
                    _methodCall_.S();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        };
        (new Thread(r)).start();
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.LifeCycleController#getFcState()
     */
    @Local
    public synchronized String getFcState() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.LifeCycleController#startFc()
     */
    public synchronized void startFc() {
        Runnable r = new Runnable() {
            public void run() {
                try {
                    _methodCall_.S();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        };
        (new Thread(r)).start();
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.LifeCycleController#stopFc()
     */
    public synchronized void stopFc() {
        Runnable r = new Runnable() {
            public void run() {
                try {
                    _methodCall_.S();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        };
        (new Thread(r)).start();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#finalize()
     */
    protected void destroyFc() {
        Runnable r = new Runnable() {
            public void run() {
                try {
                    _methodCall_.S();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        };
        (new Thread(r)).start();
    }
}
