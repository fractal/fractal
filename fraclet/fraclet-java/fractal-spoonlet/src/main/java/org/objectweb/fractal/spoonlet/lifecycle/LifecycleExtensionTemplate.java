/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): Nicolas Pessemier, Renaud Pawlak
 =============================================================================*/
package org.objectweb.fractal.spoonlet.lifecycle;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;

/**
 * A template code to manage the <code>Lifecycle</code> annotation. This
 * template introduces the implementation of the LifeCycle controller and then
 * call the annotated method in the start or stop method (depending on the
 * parameter of the annotation).
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class LifecycleExtensionTemplate extends LifecycleControllerTemplate {
    // ***************************** TEMPLATE PART *****************************
    /* (non-Javadoc)
     * @see org.objectweb.fractal.spoonlet.lifecycle.LifecycleControllerTemplate#createFc()
     */
    @Override
    protected synchronized void createFc() throws Throwable {
        super.createFc();
    }
    
    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.LifeCycleController#startFc()
     */
    @Override
    public synchronized void startFc() throws IllegalLifeCycleException {
        super.startFc();
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.LifeCycleController#stopFc()
     */
    @Override
    public synchronized void stopFc() throws IllegalLifeCycleException {
        super.stopFc();
    }
    
    /* (non-Javadoc)
     * @see org.objectweb.fractal.spoonlet.lifecycle.LifecycleControllerTemplate#destroyFc()
     */
    @Override
    protected synchronized void destroyFc() throws Throwable {
        super.destroyFc();
    }
}
