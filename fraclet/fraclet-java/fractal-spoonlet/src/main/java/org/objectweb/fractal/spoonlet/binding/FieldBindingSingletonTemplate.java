/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2008 Universitetet i Oslo
 Copyright (C) 2008-2009 INRIA Lille - Nord Europe / LIFL
 

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.spoonlet.binding;

import java.util.Vector;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.spoonlet.utils._T_;

import spoon.reflect.declaration.CtField;
import spoon.reflect.reference.CtTypeReference;
import spoon.template.Local;
import spoon.template.Parameter;
import spoon.template.Template;

/**
 * A template to introduce the binding controller implementation that supports a
 * singleton client interface.
 * 
 * @author Romain Rouvoy (Initial developer)
 * @version $Revision$
 */
public class FieldBindingSingletonTemplate implements Template, BindingController {
    @Parameter // The name of the client interface
    protected String _BindingName_;
    @Parameter // A Type Reference which is the type of the client interface
    protected CtTypeReference<?> _T_;
    @Parameter("_singleton_")
    protected String _fieldName_;

    /**
     * Use this constructor for singleton bindings.
     * 
     * @param fld the reference of the field considered.
     * @param name the name of the binding.
     */
    @Local
    public FieldBindingSingletonTemplate(CtField<?> fld, CtTypeReference<?> type, String name) {
        this._fieldName_ = fld.getSimpleName();
        this._T_ = type; // fld.getType();
        this._BindingName_ = name;
    }

    @Local // A Local variable which is the reference to the singleton field
    _T_ _singleton_;

    @Local
    public synchronized String[] listFc() {
        return null;
    }

    // ***************************** TEMPLATE PART *****************************
    /**
     * Completes the list of bound interfaces.
     * 
     * @param set incomplete list of interface identifiers.
     */
    @SuppressWarnings("unchecked")
    public void listFc(Vector set) {
        if (this._singleton_ != null)
            set.add(_BindingName_);
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.BindingController\#lookupFc(java.lang.String)
     */
    public synchronized Object lookupFc(String id) throws NoSuchInterfaceException {
        if (id.equals(_BindingName_))
            return this._singleton_;
        return null;
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.BindingController\#bindFc(java.lang.String, java.lang.Object)
     */
    public synchronized void bindFc(String id, Object ref) {
        if (id.equals(_BindingName_)) {
            this._singleton_ = (_T_) ref;
            return ;
        }
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.BindingController\#unbindFc(java.lang.String)
     */
    public synchronized void unbindFc(String id) {
        if (id.equals(_BindingName_)) {
            this._singleton_ = null;
            return ; 
        }
    }
}