/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2008 Universitetet i Oslo
 Copyright (C) 2008-2009 INRIA Lille - Nord Europe / LIFL

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.spoonlet.binding;

import static org.objectweb.fractal.fraclet.types.Constants.isEmpty;
import static org.objectweb.fractal.fraclet.types.Constants.isExplicit;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.getFieldAnnotation;
import static org.objectweb.fractal.spoonlet.component.InterfaceHelper.CARDINALITY_COLLECTION;
import static org.objectweb.fractal.spoonlet.component.InterfaceHelper.CARDINALITY_SINGLETON;
import static org.objectweb.fractal.spoonlet.component.InterfaceHelper.CONTINGENCY_MANDATORY;
import static org.objectweb.fractal.spoonlet.component.InterfaceHelper.CONTINGENCY_OPTIONAL;

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.fraclet.annotations.Requires;

import spoon.reflect.Factory;
import spoon.reflect.reference.CtFieldReference;
import spoon.reflect.reference.CtTypeReference;

/**
 * Convenience class providing methods for handling the binding concepts.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class BindingHelper {
    /** Constant class for the binding controller. */
    public static final Class<BindingController> BC = BindingController.class;

    /**
     * Provides the name associated to the component binding.
     * 
     * @param <T> the type of the field.
     * @param f field representing the component binding.
     * @return the name of the component binding.
     */
    public static final <T> String bindingName(CtFieldReference<T> f) {
        Requires a = getFieldAnnotation(f, Requires.class);
        if (a == null)
            throw new RuntimeException(f.getSimpleName()
                    + "is not a component binding");
        return bindingName(f, a);
    }

    /**
     * Provides the name associated to the component binding.
     * 
     * @param <T> the type of the field.
     * @param f field representing the component binding.
     * @param a annotation <code>Requires</code> marking the field.
     * @return the name of the component binding.
     */
    public static final <T> String bindingName(CtFieldReference<T> f, Requires a) {
        return (isEmpty(a.name())) ? f.getSimpleName().toLowerCase() : a
                .name();
    }

    /**
     * Provides the contingency associated to the component binding.
     * 
     * @param <T> the type of the field.
     * @param f field representing the component binding.
     * @param a annotation <code>Requires</code> marking the field.
     * @return the contingency of the component binding.
     */
    public static final <T> String bindingContingency(CtFieldReference<T> fld,
            Requires ann) {
        return ann.contingency().isOptional() ? CONTINGENCY_OPTIONAL
                : CONTINGENCY_MANDATORY;
    }

    /**
     * Provides the cardinality associated to the component binding.
     * 
     * @param <T> the type of the field.
     * @param f field representing the component binding.
     * @param a annotation <code>Requires</code> marking the field.
     * @return the cardinality of the component binding.
     */
    public static final <T> String bindingCardinality(CtFieldReference<T> fld,
            Requires ann) {
        return ann.cardinality().isCollection() ? CARDINALITY_COLLECTION
                : CARDINALITY_SINGLETON;
    }

    /**
     * Provides the signature associated to the component binding.
     * 
     * @param <T> the type of the field.
     * @param fld field representing the component binding.
     * @param ann annotation <code>Requires</code> marking the field.
     * @return the signature of the component binding.
     */
    public static final <T> String bindingSignature(CtFieldReference<T> fld,
            Requires ann) {
        if (isExplicit(ann.signature()))
            return ann.signature().getName();
        if (ann.cardinality().isCollection())
            return fld.getType().getActualTypeArguments().get(1).getQualifiedName();
        return fld.getType().getQualifiedName();
    }

    /**
     * Provides the type of the component binding.
     * 
     * @param <T> the type of the field.
     * @param fld field representing the component binding.
     * @param ann annotation <code>Requires</code> marking the field.
     * @return the type of the component binding (null if not found).
     */
    public static final <T> CtTypeReference<?> bindingType(CtFieldReference<T> fld,
            Requires ann, Factory factory) {
        if (isExplicit(ann.signature()))
            return factory.Type().createReference(ann.signature());
        if (ann.cardinality().isCollection())
            return fld.getType().getActualTypeArguments().get(1);
        return fld.getType();
    }
}
