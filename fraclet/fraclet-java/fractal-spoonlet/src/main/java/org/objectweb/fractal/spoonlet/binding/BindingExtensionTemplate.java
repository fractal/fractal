/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2008 Universitetet i Oslo
 Copyright (C) 2008-2009 INRIA Lille - Nord Europe / LIFL

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.spoonlet.binding;

import java.util.Vector;

import org.objectweb.fractal.api.NoSuchInterfaceException;

import spoon.template.Local;

/**
 * A template to introduce the binding controller method signature by overriding
 * parent's ones.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
@Local
public class BindingExtensionTemplate extends EmptyBindingControllerTemplate {
    // ***************************** TEMPLATE PART *****************************
    /**
     * Completes the list of bound interfaces.
     * 
     * @param set incomplete list of interface identifiers.
     */
    @SuppressWarnings("unchecked")
    protected void listFc(Vector set) {
        super.listFc(set);
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.BindingController#lookupFc(java.lang.String)
     */
    @Override
    public synchronized Object lookupFc(String id) throws NoSuchInterfaceException {
        return super.lookupFc(id);
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.BindingController#bindFc(java.lang.String,java.lang.Object)
     */
    @Override
    public synchronized void bindFc(String id, Object ref) throws NoSuchInterfaceException {
        super.bindFc(id, ref);
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.BindingController#unbindFc(java.lang.String)
     */
    @Override
    public synchronized void unbindFc(String id) throws NoSuchInterfaceException {
        super.unbindFc(id);
    }
}