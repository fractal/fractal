/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2008 Universitetet i Oslo
 Copyright (C) 2008-2009 INRIA Lille - Nord Europe / LIFL

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier
 Contributor(s): Romain Rouvoy
 =============================================================================*/
package org.objectweb.fractal.spoonlet.binding;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.objectweb.fractal.fraclet.types.Constants.isEmpty;
import static org.objectweb.fractal.spoonlet.binding.BindingHelper.bindingName;
import static org.objectweb.fractal.spoonlet.binding.BindingHelper.bindingType;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.findMethod;
import static spoon.processing.Severity.WARNING;
import static spoon.template.Substitution.insertAll;

import java.util.Vector;

import org.objectweb.fractal.fraclet.annotations.Requires;

import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;
import spoon.reflect.reference.CtExecutableReference;
import spoon.reflect.reference.CtTypeReference;
import spoon.template.Template;

/**
 * An annotation processor to manage the Fraclet annotation
 * <code>Requires</code>.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class RequiresProcessor extends AbstractBindingProcessor<Requires> {
    /* (non-Javadoc)
     * @see spoon.processing.AbstractProcessor#init()
     */
    @Override
    public void init() {
        addProcessedAnnotationType(Requires.class);
        super.init();
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.spoonlet.binding.AbstractBindingProcessor#createMethodSignatures(CtClass)
     */
    @Override
    protected void createMethodSignatures(CtClass<?> comp) {
        insertAll(comp, new ExceptionBindingControllerTemplate());
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.spoonlet.binding.BindingProcessor#insertMethodBodies(spoon.reflect.declaration.CtClass, spoon.reflect.declaration.CtField, java.lang.annotation.Annotation)
     */
    @Override
    protected void insertMethodBodies(CtClass<?> cls, CtField<?> fld,
            Requires ann) {
        Template tmpl = getFieldTemplate(fld, ann);
        insertBegin(tmpl, cls, KEYWORDS[0], Vector.class);
        if (isEmpty(ann.bind()))
            insertBegin(tmpl, cls, KEYWORDS[1], String.class, Object.class);
        else
            insertBegin(getMethodTemplate(fld, ann, ann.bind()), cls,
                    KEYWORDS[1], String.class, Object.class);
        if (isEmpty(ann.unbind()))
            insertBegin(tmpl, cls, KEYWORDS[2], String.class);
        else
            insertBegin(getMethodTemplate(fld, ann, ann.unbind()), cls,
                    KEYWORDS[2], String.class);
        insertBegin(tmpl, cls, KEYWORDS[3], String.class);
    }

    /**
     * Retrieves the template code for field-based binding.
     * 
     * @param fld reference of the field to use for binding.
     * @param ann annotation "Requires" associated to the field.
     * @return instance of a field-based binding code.
     */
    private final Template getFieldTemplate(CtField<?> fld, Requires ann) {
        String name = bindingName(fld.getReference(), ann);
        CtTypeReference<?> type = checkNotNull(bindingType(fld.getReference(),
                ann, getFactory()));
        return ann.cardinality().isCollection() ? new FieldBindingCollectionTemplate(
                fld, type, name)
                : new FieldBindingSingletonTemplate(fld, type, name);
    }

    /**
     * Retrieves the template code for method-based binding.
     * 
     * @param fld reference of the field used by the binding.
     * @param ann annotation "Requires" associated to the field.
     * @param name name of the method to use for binding.
     * @return
     */
    private final Template getMethodTemplate(CtField<?> fld, Requires ann,
            String signature) {
        String name = bindingName(fld.getReference(), ann);
        CtTypeReference<?> type = checkNotNull(bindingType(fld.getReference(),
                ann, getFactory()));
        CtTypeReference<?> cls = fld.getParent(CtClass.class).getReference();
        if (ann.cardinality().isCollection()) {
            CtExecutableReference<?> mtd = findMethod(cls, signature,
                    getFactory().Class().createReference(String.class), type);
            if (mtd == null) {
                getFactory().getEnvironment().report(
                        this,
                        WARNING,
                        fld,
                        "[Fractal] No collection binding method \'"
                                + ann.bind() + "(String, "
                                + fld.getType().getSimpleName()
                                + ")\' found in class " + cls.getSimpleName());
                return getFieldTemplate(fld, ann);
            }
            return new MethodBindingCollectionTemplate(mtd, type, name);
        } else {
            CtExecutableReference<?> mtd = findMethod(cls, signature, type);
            if (mtd == null) {
                getFactory().getEnvironment().report(
                        this,
                        WARNING,
                        fld,
                        "[Fractal] No singleton binding method \'" + ann.bind()
                                + "(" + fld.getType().getSimpleName()
                                + ")\' found in class " + cls.getSimpleName());
                return getFieldTemplate(fld, ann);
            }
            return new MethodBindingSingletonTemplate(mtd, type, name);
        }
    }
}