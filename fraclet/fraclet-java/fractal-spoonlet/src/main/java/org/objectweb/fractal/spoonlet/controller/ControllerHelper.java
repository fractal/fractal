/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.spoonlet.controller;

import static org.objectweb.fractal.fraclet.types.Constants.EMPTY;
import static org.objectweb.fractal.fraclet.types.Constants.isEmpty;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.getFieldAnnotation;

import org.objectweb.fractal.fraclet.annotations.Controller;

import spoon.reflect.reference.CtFieldReference;

/**
 * Convenience class providing methods for handling the controller concepts.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class ControllerHelper {
    /**
     * Provides the name of the controller referred by the field.
     * 
     * @param <T> the type of the field.
     * @param f field referring to the component controller.
     * @return the name of the component controller.
     */
    public static final <T> String controllerName(CtFieldReference<T> f) {
        Controller a = getFieldAnnotation(f,Controller.class);
        if (a == null)
            return EMPTY;
        return controllerName(f, a);
    }

    /**
     * Provides the name of the controller referred by the field.
     * 
     * @param <T> the type of the field.
     * @param f field referring to the component controller.
     * @param a annotation Controller marking the field.
     * @return the name of the component controller.
     */
    public static final <T> String controllerName(CtFieldReference<T> f,
            Controller a) {
        return (isEmpty(a.value())) ? f.getSimpleName() : a.value();
    }
}