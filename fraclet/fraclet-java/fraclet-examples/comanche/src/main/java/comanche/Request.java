/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package comanche;

import java.io.PrintStream;
import java.io.Reader;
import java.net.Socket;

import org.objectweb.fractal.fraclet.extensions.Data;

/**
 * Data structure representing a request under processing.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
@Data
public class Request {
    /** Socket associated to the request. */
    public Socket s;
    /** Input stream associated to the request. */
    public Reader in;
    /** Output stream associated to the request. */
    public PrintStream out;
    /** URL triggering the request. */
    public String url;

    /**
     * Request constructor.
     * 
     * @param s the socket associated to the request.
     */
    public Request(Socket s) {
        this.s = s;
    }
}
