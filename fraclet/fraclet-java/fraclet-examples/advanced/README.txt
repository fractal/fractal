This directory contains examples describing advanced features of Fraclet:
- Fractal Lifecycle handlers (directory lifecycle)
- Fractal Inheritance support (directory inheritance)
- Fractal Custom Interface Binding (directory binder)
- Fractal Custom Attribute Setting (directory setter)
- Fractal J2ME support (directory j2me)
- Fractal JuliaC support (directory juliac)
- FractalADL Composite templates (directory composite)
- FractalADL Legacy definitions (directory legacy)
- FractalRMI support (directory fractalrmi)

To compile all examples, type:
    mvn install

To run the Comanche or the HelloWorld example, type:
    cd comanche (or helloworld)
    mvn -Prun.julia
    or mvn -Prun.koch
    or mvn -Prun.java