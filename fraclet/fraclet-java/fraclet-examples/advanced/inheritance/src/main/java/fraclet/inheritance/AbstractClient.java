package fraclet.inheritance;

import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Interface;

/**
 * Partial implementation of a Helloworld client component.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
@Component(provides = @Interface(name = "r", signature = Runnable.class))
public abstract class AbstractClient implements Runnable {
    protected @Attribute(set = "bindMessage")
    String message;
}