package fraclet.inheritance;

import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotations.Component;

/**
 * Partial implementation of the Server.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
@Component
public class AbstractServer {
    @Attribute(value = ">")
    protected String header;
}