/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2008 Universitetet i Oslo
 Copyright (C) 2008-2009 INRIA Lille - Nord Europe / LIFL

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package fraclet.binder;

import static java.lang.System.out;
import static org.objectweb.fractal.fraclet.types.Cardinality.COLLECTION;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Controller;
import org.objectweb.fractal.fraclet.annotations.Interface;
import org.objectweb.fractal.fraclet.annotations.Requires;

/**
 * Implementation of a Helloworld client component.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
@Component(name = "helloworld.Client", provides = @Interface(name = "r", signature = Runnable.class))
public class Client implements Runnable {
    @Requires(name = "srv", bind = "bindMyService", cardinality = COLLECTION)
    private final Map<String, Service> services = new HashMap<String, Service>();

    @Controller("name-controller")
    protected NameController nc;

    @Attribute
    protected String message;

    /**
     * Explicit setter for attribute message.
     * 
     * @param value new value of the attribute message.
     */
    public void bindMyService(String key, Service value) {
        out.println("Binding collection interface \""+key+"\" to " + value);
        this.services.put(key, value);
    }

    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    public void run() {
        for (Service service : this.services.values())
            try {
                service.print(this.nc.getFcName() + " says " + this.message);
            } catch (NullPointerException e) { // NPE when using the JavaBackend
                service.print("Client says " + this.message);
            }
    }
}