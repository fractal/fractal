/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2008 Universitetet i Oslo
 Copyright (C) 2008-2009 INRIA Lille - Nord Europe / LIFL

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package fraclet.lifecycle;

import static java.util.logging.Logger.getLogger;
import static org.objectweb.fractal.fraclet.types.Policy.ASYNCHRONOUS;
import static org.objectweb.fractal.fraclet.types.Step.DESTROY;
import static org.objectweb.fractal.fraclet.types.Step.START;

import java.util.logging.Logger;

import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Controller;
import org.objectweb.fractal.fraclet.annotations.Interface;
import org.objectweb.fractal.fraclet.annotations.Lifecycle;
import org.objectweb.fractal.fraclet.annotations.Requires;

/**
 * Implementation of a Helloworld client component.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
@Component(name = "helloworld.Client", provides = @Interface(name = "r", signature = Runnable.class))
public class Client implements Runnable {
    protected @Attribute
    String message;

    private @Requires(name = "s")
    Service service;
    
    protected @Controller("name-controller")
    NameController nc;

    private final Logger log = getLogger("client");

    @Lifecycle(step=START,policy=ASYNCHRONOUS)
    protected void start() {
        log.info("helloworld.Client - started.");
    }

    @Lifecycle(step=DESTROY)
    protected void stop() {
        log.info("helloworld.Client - stopped.");
    }
    
    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    public void run() {
        try {
            this.service.print(this.nc.getFcName() + " says " + this.message);
        } catch (NullPointerException e) { // NPE when using the JavaBackend
            this.service.print("Client says " + this.message);
        }
    }
}