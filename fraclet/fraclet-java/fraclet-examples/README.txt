To compile all examples, type:
    mvn install

To run the Comanche or the HelloWorld example, type:
    cd comanche (or helloworld)
    mvn -Prun.julia
    or mvn -Prun.juliak
    or mvn -Prun.java
    
The directory "advanced" contains examples describing expert features of Fraclet.
