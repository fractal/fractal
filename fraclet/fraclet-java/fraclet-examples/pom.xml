<?xml version="1.0" encoding="UTF-8"?>
    <!--
        This library is free software; you can redistribute it and/or modify it
        under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation; either version 2 of the License, or (at
        your option) any later version. This library is distributed in the hope
        that it will be useful, but WITHOUT ANY WARRANTY; without even the
        implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
        See the GNU Lesser General Public License for more details. You should
        have received a copy of the GNU Lesser General Public License along with
        this library; if not, write to the Free Software Foundation, Inc., 59
        Temple Place, Suite 330, Boston, MA 02111-1307 USA
    -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.objectweb.fractal.fraclet.java</groupId>
        <artifactId>parent</artifactId>
        <version>3.2.3</version>
    </parent>

    <groupId>org.objectweb.fractal.fraclet.java.examples</groupId>
    <artifactId>parent</artifactId>
    <name>Fraclet Examples</name>
    <packaging>pom</packaging>

    <description>Applications developed with Fraclet</description>

    <modules>
        <module>helloworld</module>
        <module>comanche</module>
        <module>advanced</module>
    </modules>

    <dependencies>
        <dependency>
            <groupId>org.objectweb.fractal</groupId>
            <artifactId>fractal-api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.objectweb.fractal.fraclet.java</groupId>
            <artifactId>fractal-spoonlet</artifactId>
        </dependency>
        <dependency>
            <groupId>org.objectweb.fractal.fraclet.java</groupId>
            <artifactId>fractaladl-spoonlet</artifactId>
        </dependency>
        <dependency>
            <groupId>org.objectweb.fractal.fractaladl</groupId>
            <artifactId>fractal-adl</artifactId>
        </dependency>
        <dependency>
            <groupId>org.objectweb.fractal.julia</groupId>
            <artifactId>julia-runtime</artifactId>
        </dependency>
        <dependency>
            <groupId>org.objectweb.fractal.julia</groupId>
            <artifactId>julia-mixins</artifactId>
        </dependency>
        <dependency>
            <groupId>org.objectweb.fractal.julia</groupId>
            <artifactId>julia-asm</artifactId>
        </dependency>
        <dependency>
            <groupId>org.objectweb.fractal.julia</groupId>
            <artifactId>koch-runtime</artifactId>
        </dependency>
        <dependency>
            <groupId>org.objectweb.fractal.julia</groupId>
            <artifactId>koch-mixins</artifactId>
        </dependency>
    </dependencies>

    <!--
        These profiles are meant to be inherited by sub modules, specyfing the
        specific properties which are either left here as maven $variables, or
        that are specific to each project.
    -->
    <!-- some shared properties, override or add new ones in sub projects -->
    <!-- =============================================== -->
    <!-- Juliac properties for launching the application -->
    <!-- (component and interface)                       -->
    <!-- with the juliac:profile (mvn -Pjuliac:run)      -->
    <!-- =============================================== -->
    <properties>
        <fractal.itf>r</fractal.itf>
    </properties>

    <build>
        <resources>
            <resource>
                <directory>${basedir}/src/main/resources</directory>
            </resource>
            <resource>
                <directory>${basedir}/target/generated-sources/spoon</directory>
                <includes>
                    <include>**/*.fractal</include>
                </includes>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <groupId>net.sf.alchim</groupId>
                <artifactId>spoon-maven-plugin</artifactId>
                <version>0.7.1</version>
                <dependencies>
                    <dependency>
                        <groupId>fr.inria.gforge.spoon</groupId>
                        <artifactId>spoon-core</artifactId>
                        <version>1.4.2</version>
                    </dependency>
                </dependencies>
                <executions>
                    <execution>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <!-- Fractal backend with Julia provider -->
        <profile>
            <id>run:julia</id>
            <build>
                <defaultGoal>compile</defaultGoal>
                <plugins>
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>exec-maven-plugin</artifactId>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>java</goal>
                                </goals>
                                <phase>compile</phase>
                            </execution>
                        </executions>
                        <configuration>
                            <mainClass>
                                org.objectweb.fractal.adl.Launcher
                            </mainClass>
                            <arguments>
                                <argument>
                                    -fractal.provider=org.objectweb.fractal.julia.Julia
                                </argument>
                                <argument>
                                    -fractaladl.backend=org.objectweb.fractal.adl.FractalBackend
                                </argument>
                                <argument>
                                    -fractaladl.definition=${fractal.component}
                                </argument>
                                <argument>
                                    -fractaladl.itf=${fractal.itf}
                                </argument>
                            </arguments>
                            <systemProperties>
                                <systemProperty>
                                    <key>fractal.provider</key>
                                    <value>
                                        org.objectweb.fractal.julia.Julia
                                    </value>
                                </systemProperty>
                            </systemProperties>
                            <!-- the project does not include Fractal ADL and Julia -->
                            <includePluginDependencies>
                                true
                            </includePluginDependencies>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <!-- Fractal backend with Koch provider -->
        <profile>
            <id>run:koch</id>
            <build>
                <defaultGoal>compile</defaultGoal>
                <plugins>
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>exec-maven-plugin</artifactId>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>java</goal>
                                </goals>
                                <phase>compile</phase>
                            </execution>
                        </executions>
                        <configuration>
                            <mainClass>
                                org.objectweb.fractal.adl.Launcher
                            </mainClass>
                            <arguments>
                                <argument>
                                    -fractal.provider=org.objectweb.fractal.juliak.Juliak
                                </argument>
                                <argument>
                                    -fractaladl.backend=org.objectweb.fractal.adl.FractalBackend
                                </argument>
                                <argument>
                                    -fractaladl.definition=${fractal.component}
                                </argument>
                                <argument>
                                    -fractaladl.itf=${fractal.itf}
                                </argument>
                            </arguments>
                            <systemProperties>
                                <systemProperty>
                                    <key>fractal.provider</key>
                                    <value>
                                        org.objectweb.fractal.juliak.Juliak
                                    </value>
                                </systemProperty>
                            </systemProperties>
                            <!-- because the project does not include Fractal ADL and Juliak -->
                            <includePluginDependencies>
                                true
                            </includePluginDependencies>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <!-- The Java backend -->
        <profile>
            <id>run:java</id>
            <build>
                <defaultGoal>compile</defaultGoal>
                <plugins>
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>exec-maven-plugin</artifactId>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>java</goal>
                                </goals>
                                <phase>compile</phase>
                            </execution>
                        </executions>
                        <configuration>
                            <mainClass>
                                org.objectweb.fractal.adl.Launcher
                            </mainClass>
                            <arguments>
                                <argument>
                                    -fractaladl.backend=org.objectweb.fractal.adl.JavaBackend
                                </argument>
                                <argument>
                                    -fractaladl.definition=${fractal.component}
                                </argument>
                                <argument>
                                    -fractaladl.itf=${fractal.itf}
                                </argument>
                            </arguments>
                            <!-- because the project does not include Fractal ADL -->
                            <includePluginDependencies>
                                true
                            </includePluginDependencies>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>