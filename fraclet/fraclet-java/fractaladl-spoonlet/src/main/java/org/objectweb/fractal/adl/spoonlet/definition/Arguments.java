/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.adl.spoonlet.definition;

import static com.google.common.base.Joiner.on;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static org.objectweb.fractal.fraclet.types.Constants.EMPTY;

import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import com.google.common.base.Function;

/**
 * Structure used to store the list of arguments associated to a component.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class Arguments {
    private static final long serialVersionUID = -598596782889619303L;
    private static final String ARGUMENT_SEPARATOR = ",";
    private static final String ARGUMENT_AFFECTATION = "=>";

    private final Set<String> mandatory = newHashSet();
    private final Map<String, String> optional = newHashMap();

    /**
     * Insert an argument to the list of arguments.
     * 
     * @param key the name of the argument
     * @param value the default value of the argument (if null, the argument
     *            will be considered as mandatory)
     * @return the value added
     */
    public String put(String key, String value) {
        if (value == null || EMPTY.equals(value))
            this.mandatory.add(key);
        else {
            this.optional.put(key, value);
            // Remove mandatory argument that is now registered as optional
            this.mandatory.remove(key);
        }
        return value;
    }

    /**
     * Copy the list of arguments into the current list.
     * 
     * @param args the source list of arguments.
     */
    public void putAll(Arguments args) {
        this.mandatory.addAll(args.mandatory);
        this.optional.putAll(args.optional);
        // Remove mandatory arguments that are now registered as optional
        this.mandatory.removeAll(this.optional.keySet());
    }

    /**
     * Checks if the list of arguments (mandatory and optional) is empty.
     * 
     * @return true if empty.
     */
    public boolean isEmpty() {
        return this.optional.isEmpty() && this.mandatory.isEmpty();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        StringBuilder out = (StringBuilder) on(ARGUMENT_SEPARATOR).appendTo(
                new StringBuilder(), this.mandatory);
        if (!this.mandatory.isEmpty() && !this.optional.isEmpty())
            out.append(ARGUMENT_SEPARATOR);
        return on(ARGUMENT_SEPARATOR).appendTo(out,
                transform(this.optional.entrySet(), declareOptionalArgument))
                .toString();
    }

    /**
     * Exports the arguments as a list of parameters.
     * 
     * @return the formatted list of parameters.
     */
    public String asParameters() {
        StringBuilder out = (StringBuilder) on(ARGUMENT_SEPARATOR).appendTo(
                new StringBuilder(),
                transform(this.mandatory, assignMandatoryArgument));
        if (!this.mandatory.isEmpty() && !this.optional.isEmpty())
            out.append(ARGUMENT_SEPARATOR);
        return on(ARGUMENT_SEPARATOR).appendTo(out,
                transform(this.optional.entrySet(), assignOptionalArgument))
                .toString();
    }

    /**
     * Generates the declaration of an argument.
     */
    private static final Function<Entry<String, String>, String> declareOptionalArgument = new Function<Entry<String, String>, String>() {
        public String apply(Entry<String, String> e) {
            return e.getKey() + "=" + e.getValue();
        }
    };

    /**
     * Generates the assignment of a mandatory argument.
     */
    private static final Function<String, String> assignMandatoryArgument = new Function<String, String>() {
        public String apply(String s) {
            return s + ARGUMENT_AFFECTATION + asArgument.apply(s);
        }
    };

    /**
     * Generates the assignment of an optional argument.
     */
    private static final Function<Entry<String, String>, String> assignOptionalArgument = new Function<Entry<String, String>, String>() {
        public String apply(Entry<String, String> e) {
            return e.getKey() + ARGUMENT_AFFECTATION
                    + asArgument.apply(e.getKey());
        }
    };

    /**
     * Generates the reference to an argument.
     */
    public static final Function<String, String> asArgument = new Function<String, String>() {
        public String apply(String arg) {
            return "${" + arg + "}";
        }
    };
}