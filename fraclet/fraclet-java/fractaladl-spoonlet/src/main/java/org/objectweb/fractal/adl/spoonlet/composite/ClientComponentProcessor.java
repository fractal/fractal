/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.adl.spoonlet.composite;

import static org.objectweb.fractal.adl.spoonlet.composite.CompositeDefinitionGenerator.composite;
import static org.objectweb.fractal.adl.spoonlet.definition.Arguments.asArgument;
import static org.objectweb.fractal.adl.spoonlet.definition.PrimitiveDefinitionGenerator.primitive;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.componentFullname;

import org.objectweb.fractal.adl.spoonlet.component.ComponentTags;
import org.objectweb.fractal.adl.spoonlet.definition.Arguments;
import org.objectweb.fractal.adl.spoonlet.definition.DefinitionTags;
import org.objectweb.fractal.document.Element;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Requires;

import spoon.processing.AbstractAnnotationProcessor;
import spoon.reflect.declaration.CtClass;

/**
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class ClientComponentProcessor extends
        AbstractAnnotationProcessor<Component, CtClass<?>> implements
        ComponentTags, DefinitionTags {
    /** Default name of the primitive component. */
    public static final String CLIENT = "client";

    /* (non-Javadoc)
     * @see spoon.processing.AbstractProcessor#init()
     */
    @Override
    public void init() {
        addProcessedAnnotationType(Component.class);
        super.init();
    }

    /* (non-Javadoc)
     * @see spoon.processing.AbstractAnnotationProcessor#inferConsumedAnnotationType()
     */
    @Override
    public boolean inferConsumedAnnotationType() {
        return false;
    }

    /* (non-Javadoc)
     * @see spoon.processing.AnnotationProcessor#process(java.lang.annotation.Annotation, spoon.reflect.declaration.CtElement)
     */
    public void process(Component ann, CtClass<?> cls) {
        if (cls.getAnnotatedChildren(Requires.class).size() == 0)
            return;
        getEnvironment().debugMessage(
                "[FractalADL] Importing client component "
                        + cls.getQualifiedName() + "...");
        Element primitive = primitive().definition(cls);
        Element composite = composite().definition(cls);
        Element component = new Element(COMPONENT_TAG);
        composite.getChilds().add(component);
        component.setAttribute(COMP_NAME, asArgument.apply(CLIENT));
        Arguments arguments = new Arguments();
        Arguments previous = (Arguments) primitive.getAttribute(DEF_ARGUMENTS);
        String def = componentFullname(cls.getReference(), ann);
        if (previous != null) {
            arguments.putAll(previous);
            def += "(" + previous.asParameters() + ")";
        }
        arguments.put(CLIENT, CLIENT);
        composite.setAttribute(DEF_ARGUMENTS, arguments);
        component.setAttribute(COMP_DEFINITION, def);
    }
}
