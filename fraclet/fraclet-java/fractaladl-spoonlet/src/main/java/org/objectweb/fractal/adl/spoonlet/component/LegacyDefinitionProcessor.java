/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.adl.spoonlet.component;

import static com.google.common.base.Joiner.on;
import static org.objectweb.fractal.adl.spoonlet.definition.AbstractDefinitionProcessor.addDefinitionArguments;
import static org.objectweb.fractal.adl.spoonlet.definition.AbstractDefinitionProcessor.addDefinitionExtends;
import static org.objectweb.fractal.adl.spoonlet.definition.PrimitiveDefinitionGenerator.primitive;
import static spoon.processing.Severity.WARNING;

import org.objectweb.fractal.adl.spoonlet.definition.Arguments;
import org.objectweb.fractal.adl.spoonlet.definition.DefinitionTags;
import org.objectweb.fractal.document.Element;
import org.objectweb.fractal.fraclet.annotations.Definition;

import spoon.processing.AbstractAnnotationProcessor;
import spoon.reflect.declaration.CtClass;

/**
 * Processor interpreting the annotation <code>Definition</code> to complete
 * the associated <code>definition</code> elements in Fractal ADL definitions.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class LegacyDefinitionProcessor extends
        AbstractAnnotationProcessor<Definition, CtClass<?>> implements
        DefinitionTags {
    /* (non-Javadoc)
     * @see spoon.processing.AbstractProcessor#init()
     */
    @Override
    public void init() {
        addProcessedAnnotationType(Definition.class);
        super.init();
    }

    /* (non-Javadoc)
     * @see spoon.processing.AbstractAnnotationProcessor#inferConsumedAnnotationType()
     */
    @Override
    public boolean inferConsumedAnnotationType() {
        return false;
    }

    /* (non-Javadoc)
     * @see spoon.processing.AnnotationProcessor#process(java.lang.annotation.Annotation, spoon.reflect.declaration.CtElement)
     */
    public void process(Definition ann, CtClass<?> cls) {
        try {
            addLegacyDefinition(primitive().definition(cls), ann);
        } catch (NullPointerException npe) {
            getEnvironment().report(this, WARNING, cls,
                    "Class " + cls.getQualifiedName()
                    + " cannot be annotated with @Definition.");
        }
    }

    /**
     * Parses the arguments by applying the following rules:
     * <ul>
     * <li>value == value</li>
     * <li>${name} == ${name}</li>
     * <li>${name=value} == ${name} + name=value</li>
     * <li>${=value} == ${value} (error discarded)</li>
     * </ul>
     * @param arguments the list of arguments for the definition
     * @param arg the argument to extract
     * @return the extracted argument name
     */
    private static final String extractArgument(Arguments arguments, String arg) {
        if (arg.startsWith("${") && arg.endsWith("}")) {
            String sub = arg.substring(2, arg.length() - 1);
            int ind = sub.indexOf("=");
            switch (ind) {
            case 0: // Error: argument incorrect -> omit "="
                sub = sub.substring(1);
            case -1: // Mandatory argument
                arguments.put(sub, null);
                return arg;
            default: // Optional argument
                String name = sub.substring(0, ind);
                arguments.put(name, sub.substring(ind + 1));
                return "${"+name+"}";
            }
        }
        return arg;
    }

    /**
     * Parses the arguments by applying the following rules:
     * <ul>
     * <li>name="DEF"+args="value" == definition... extends="DEF(value)"</li>
     * <li>name="DEF"+args="arg=>value" == definition...
     * extends="DEF(arg=>value)"</li>
     * <li>name="DEF"+args="${arg}" == definition... extends="DEF(${arg})"
     * arguments="arg"</li>
     * <li>name="DEF"+args="${arg=value}" == definition...
     * extends="DEF(${arg})" arguments="arg=value"
     * <li>
     * <li>name="DEF"+args="${arg=name}=>${value}" == definition...
     * extends="DEF(${arg}=>${value})" arguments="arg=name,value"
     * <li>
     * </ul>
     * 
     * @param args list of arguments to be parsed
     * @return the content of the extends clause DEF"(...,...)"
     */
    private static final String parseArguments(Element root, String[] args) {
        final String[] res = new String[args.length];
        final Arguments arguments = new Arguments();
        String separator = "=>";
        for (int i = 0; i < args.length; i++) {
            int sep = args[i].indexOf(separator);
            switch (sep) {
            case 0: // Error: argument name incorrect -> omit "=>"
                res[i] = extractArgument(arguments, args[i].substring(separator
                        .length()));
                break;
            case -1: // Anonymous argument
                res[i] = extractArgument(arguments, args[i]);
                break;
            default: // Named argument
                res[i] = extractArgument(arguments, args[i].substring(0, sep))
                        + separator
                        + extractArgument(arguments, args[i].substring(sep
                                + separator.length()));
                break;
            }
        }
        if (!arguments.isEmpty())
            addDefinitionArguments(root, arguments);
        return "(" + on(",").join(res) + ")";
    }

    /**
     * Building the list of extended definitions with their parameters.
     * 
     * @param root definition associated to the component.
     * @param ann annotation describing the extended definition.
     */
    public static final void addLegacyDefinition(Element root, Definition ann) {
        if (ann.arguments().length == 0)
            addDefinitionExtends(root, ann.name());
        else
            addDefinitionExtends(root, ann.name()
                    + parseArguments(root, ann.arguments()));
    }
}
