/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.adl.spoonlet.composite;

import static org.objectweb.fractal.adl.spoonlet.composite.CompositeDefinitionGenerator.composite;
import static org.objectweb.fractal.adl.spoonlet.definition.Arguments.asArgument;
import static org.objectweb.fractal.fraclet.types.Cardinality.COLLECTION;
import static org.objectweb.fractal.fraclet.types.Contingency.OPTIONAL;
import static org.objectweb.fractal.spoonlet.binding.BindingHelper.bindingName;

import org.objectweb.fractal.adl.spoonlet.component.ComponentTags;
import org.objectweb.fractal.adl.spoonlet.definition.Arguments;
import org.objectweb.fractal.adl.spoonlet.definition.DefinitionTags;
import org.objectweb.fractal.document.Element;
import org.objectweb.fractal.fraclet.annotations.Requires;

import spoon.processing.AbstractAnnotationProcessor;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;

/**
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class ServerComponentProcessor extends
        AbstractAnnotationProcessor<Requires, CtField<?>> implements
        ComponentTags, DefinitionTags {
    /* (non-Javadoc)
     * @see spoon.processing.AbstractProcessor#init()
     */
    @Override
    public void init() {
        addProcessedAnnotationType(Requires.class);
        super.init();
    }

    /* (non-Javadoc)
     * @see spoon.processing.AbstractAnnotationProcessor#inferConsumedAnnotationType()
     */
    @Override
    public boolean inferConsumedAnnotationType() {
        return false;
    }

    /* (non-Javadoc)
     * @see spoon.processing.AnnotationProcessor#process(java.lang.annotation.Annotation, spoon.reflect.declaration.CtElement)
     */
    public void process(Requires ann, CtField<?> fld) {
        if (COLLECTION.equals(ann.cardinality())
                || OPTIONAL.equals(ann.contingency()))
            return;
        CtClass<?> cls = (CtClass<?>) fld.getParent();
        getEnvironment().debugMessage(
                "[FractalADL] Importing required component "
                        + cls.getQualifiedName() + "."
                        + bindingName(fld.getReference(), ann) + "...");
        Element comp = new Element(COMPONENT_TAG);
        String name = bindingName(fld.getReference(), ann);
        comp.setAttribute(COMP_NAME, asArgument.apply(name));
//        comp.setAttribute(COMP_DEFINITION, bindingSignature(fld.getReference(),
//                ann));
        Element root = composite().definition(cls);
        root.getChilds().add(comp);
        Arguments args = (Arguments) root.getAttribute(DEF_ARGUMENTS);
        args.put(name, name);
        root.setAttribute(DEF_ARGUMENTS, args);
    }

}
