/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.adl.spoonlet.attribute;

import static org.objectweb.fractal.adl.spoonlet.definition.AbstractDefinitionProcessor.addDefinitionArgument;
import static org.objectweb.fractal.adl.spoonlet.definition.Arguments.asArgument;
import static org.objectweb.fractal.adl.spoonlet.definition.PrimitiveDefinitionGenerator.primitive;
import static org.objectweb.fractal.fraclet.types.Access.READ_ONLY;
import static org.objectweb.fractal.spoonlet.attribute.AttributeHelper.attributeController;
import static org.objectweb.fractal.spoonlet.attribute.AttributeHelper.attributeName;
import static org.objectweb.fractal.spoonlet.attribute.AttributeHelper.attributeValue;
import static org.objectweb.fractal.spoonlet.attribute.AttributeHelper.controllerFullname;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.getFieldAnnotation;

import org.objectweb.fractal.adl.spoonlet.definition.DefinitionTags;
import org.objectweb.fractal.document.Element;
import org.objectweb.fractal.fraclet.annotations.Attribute;

import spoon.processing.AbstractAnnotationProcessor;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;
import spoon.reflect.reference.CtFieldReference;

/**
 * Processor interpreting the annotation <code>Attribute</code> to generate the
 * associated <code>attribute</code> elements in Fractal ADL definitions.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class AttributeProcessor extends
        AbstractAnnotationProcessor<Attribute, CtField<?>> implements
        AtributeTags, DefinitionTags {
    /* (non-Javadoc)
     * @see spoon.processing.AbstractProcessor#init()
     */
    @Override
    public void init() {
        addProcessedAnnotationType(Attribute.class);
        super.init();
    }

    /* (non-Javadoc)
     * @see spoon.processing.AbstractAnnotationProcessor#inferConsumedAnnotationType()
     */
    @Override
    public boolean inferConsumedAnnotationType() {
        return false;
    }

    /* (non-Javadoc)
     * @see spoon.processing.AnnotationProcessor#process(java.lang.annotation.Annotation, spoon.reflect.declaration.CtElement)
     */
    public void process(Attribute ann, CtField<?> fld) {
        CtClass<?> cls = fld.getParent(CtClass.class);
        Element root = primitive().definition(cls);
        String name = attributeName(fld.getReference(), ann);
        getEnvironment().debugMessage(
                "[FractalADL] Processing attribute " + cls.getQualifiedName()
                        + "." + name + "<"+fld.getSignature()+">...");
        if (ann.mode() == READ_ONLY) 
            return; // Not no attribute tag generated for read only attributes
        Element attributes = null;
        for (Element child : root.getChilds())
            if (ATTRIBUTES_TAG.equals(child.getName())) {
                attributes = child;
                break;
            }
        if (attributes == null) { // Create the attributes element if needed
            attributes = new Element(ATTRIBUTES_TAG);
            root.getChilds().add(attributes);
            attributes
                    .setAttribute(ATTR_SIGNATURE,
                            controllerFullname(attributeController(cls
                                    .getReference())));
            if (cls.getSuperclass() != null)
                for (CtFieldReference<?> f : cls.getSuperclass().getAllFields())
                    if (getFieldAnnotation(f,Attribute.class) != null) {
                        attributes.getChilds().add(getAttribute(f));
                        addDefinitionArgument(root, attributeName(f), attributeValue(f));
                    }
        }
        attributes.getChilds().add(getAttribute(fld.getReference()));
        addDefinitionArgument(root, name, attributeValue(fld.getReference(),
                ann));
    }

    /**
     * Provides the element associated to the attribute.
     *
     * @param a the annotation processed.
     * @param f the field processed.
     * @return the element associated to the attribute.
     */
    public static final Element getAttribute(CtFieldReference<?> f) {
        Element attr = new Element(ATTRIBUTE_TAG);
        attr.setAttribute(ATTR_NAME, attributeName(f));
        attr.setAttribute(ATTR_VALUE, asArgument.apply(attributeName(f)));
        return attr;
    }
}