/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.adl.spoonlet.definition;

import static org.objectweb.fractal.adl.spoonlet.definition.PrimitiveDefinitionGenerator.primitive;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.getTypeAnnotation;

import java.lang.annotation.Annotation;

import org.objectweb.fractal.document.Document;
import org.objectweb.fractal.document.DocumentFactory;
import org.objectweb.fractal.document.Element;
import org.objectweb.fractal.fraclet.annotations.Interface;

import spoon.processing.AbstractAnnotationProcessor;
import spoon.processing.Property;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtTypeReference;

/**
 * Processor interpreting the annotation to generate the associated
 * <code>definition</code> element in Fractal ADL definitions.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 * @param <T>
 */
public class AbstractDefinitionProcessor<A extends Annotation, T extends CtType<?>>
        extends AbstractAnnotationProcessor<A, T> implements DefinitionTags {
    @Property("Identifier of the dtd to use when generating (default is standard)")
    private String dtd = "classpath://org/objectweb/fractal/adl/xml/standard.dtd";

    /* (non-Javadoc)
     * @see spoon.processing.AbstractAnnotationProcessor#inferConsumedAnnotationType()
     */
    @Override
    public boolean inferConsumedAnnotationType() {
        return false;
    }

    /* (non-Javadoc)
     * @see spoon.processing.AnnotationProcessor#process(java.lang.annotation.Annotation, spoon.reflect.declaration.CtElement)
     */
    public void process(A ann, T cls) {
        getEnvironment().debugMessage(
                "[FractalADL] Processing component type "
                        + cls.getQualifiedName() + "...");
        Document doc = documents().document(cls);
        setDocumentHeader(doc, this.dtd);
        Element root = new Element(DEFINITION_TAG);
        doc.setRootElement(root);
        root.setAttribute(DEF_NAME, cls.getQualifiedName());
        for (CtTypeReference<?> ref : cls.getSuperInterfaces())
            try {
                if (getTypeAnnotation(ref,Interface.class) != null)
                    addDefinitionExtends(root, ref.getQualifiedName());
            } catch (Exception e) {
                // java.lang.ClassNotFoundException ignored
            }
    }

    /**
     * Sets the header of the XML file.
     * 
     * @param d document processed.
     * @param dtd name of the dtd used for this document.
     */
    private static void setDocumentHeader(Document d, String dtd) {
        String header = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>"
                + "\n";
        header += "<!DOCTYPE definition PUBLIC ";
        header += "\"-//objectweb.org//DTD Fractal ADL 2.0//EN\" ";
        header += "\"" + dtd + "\">";
        d.setHeader(header);
    }

    /**
     * Adds an extends attribute to the definition.
     * 
     * @param root the definition processed.
     * @param def the list of definition to add (separated and prefixed by
     *            DefinitionTags.DEF_SEPARATOR).
     */
    public static final void addDefinitionExtends(Element root, String def) {
        Definitions ext = (Definitions) root.getAttribute(DEF_EXTENDS);
        if (ext == null) {
            ext = new Definitions();
            root.setAttribute(DEF_EXTENDS, ext);
        }
        ext.add(def);
    }

    /**
     * Appends a list of arguments at the end of the current list.
     * 
     * @param root the definition processed.
     * @param name the list of arguments to append.
     */
    public static final void addDefinitionArgument(Element root, String name,
            String value) {
        Arguments args = (Arguments) root.getAttribute(DEF_ARGUMENTS);
        if (args == null) {
            args = new Arguments();
            root.setAttribute(DEF_ARGUMENTS, args);
        }
        args.put(name, value);
    }

    /**
     * Appends a list of arguments at the end of the current list.
     * 
     * @param root the definition processed.
     * @param params the list of parameters to append.
     */
    public static final void addDefinitionArguments(Element root,
            Arguments params) {
        Arguments args = (Arguments) root.getAttribute(DEF_ARGUMENTS);
        if (args == null) {
            args = new Arguments();
            root.setAttribute(DEF_ARGUMENTS, args);
        }
        args.putAll(params);
    }

    /**
     * @return
     */
    protected DocumentFactory documents() {
        return primitive();
    }
}