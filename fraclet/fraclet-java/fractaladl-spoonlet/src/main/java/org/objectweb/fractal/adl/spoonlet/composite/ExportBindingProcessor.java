package org.objectweb.fractal.adl.spoonlet.composite;

import static org.objectweb.fractal.adl.spoonlet.composite.ClientComponentProcessor.CLIENT;
import static org.objectweb.fractal.adl.spoonlet.composite.CompositeDefinitionGenerator.composite;
import static org.objectweb.fractal.adl.spoonlet.definition.Arguments.asArgument;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.getTypeAnnotation;
import static org.objectweb.fractal.spoonlet.component.InterfaceHelper.interfaceName;

import org.objectweb.fractal.adl.spoonlet.binding.BindingTags;
import org.objectweb.fractal.document.Element;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Interface;
import org.objectweb.fractal.fraclet.annotations.Requires;

import spoon.processing.AbstractAnnotationProcessor;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.reference.CtTypeReference;

/**
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class ExportBindingProcessor extends
        AbstractAnnotationProcessor<Component, CtClass<?>> implements
        BindingTags {
    /* (non-Javadoc)
     * @see spoon.processing.AbstractProcessor#init()
     */
    @Override
    public void init() {
        addProcessedAnnotationType(Component.class);
        super.init();
    }

    /* (non-Javadoc)
     * @see spoon.processing.AbstractAnnotationProcessor#inferConsumedAnnotationType()
     */
    @Override
    public boolean inferConsumedAnnotationType() {
        return false;
    }

    /* (non-Javadoc)
     * @see spoon.processing.AnnotationProcessor#process(java.lang.annotation.Annotation, spoon.reflect.declaration.CtElement)
     */
    public void process(Component comp, CtClass<?> cls) {
        if (cls.getAnnotatedChildren(Requires.class).size() == 0)
            return;
        getEnvironment().debugMessage(
                "[FractalADL] Exporting server interfaces of "
                        + cls.getQualifiedName() + "...");
        Element root = composite().definition(cls);
        for (CtTypeReference<?> type = cls.getReference(); type != null; type = type
                .getSuperclass()) {
            for (CtTypeReference<?> itf : type.getSuperInterfaces()) {
                try {
                    Interface ann = getTypeAnnotation(itf, Interface.class);
                    if (ann != null)
                        exportBinding(root, interfaceName(ann, itf));
                } catch (Exception e) {
                    // java.lang.ClassNotFoundException ignored
                }
            }
            Component itfs = getTypeAnnotation(type, Component.class);
            if (itfs != null)
                for (Interface itf : itfs.provides())
                    exportBinding(root, interfaceName(itf, getFactory().Type()
                            .createReference(itf.signature())));
        }
    }

    /**
     * Declares an export binding from contained primitive to the composite.
     * 
     * @param root root definition of the composite.
     * @param itf server interface name to export.
     */
    private static final void exportBinding(Element root, String itf) {
        Element binding = new Element(BINDING_TAG);
        binding.setAttribute(BIND_CLIENT, "this." + itf);
        binding.setAttribute(BIND_SERVER, asArgument.apply(CLIENT) + "." + itf);
        root.getChilds().add(binding);
    }
}