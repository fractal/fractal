/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.adl.spoonlet.controller;

import static org.objectweb.fractal.adl.spoonlet.definition.PrimitiveDefinitionGenerator.primitive;
import static org.objectweb.fractal.fraclet.types.Constants.EMPTY;
import static spoon.processing.Severity.ERROR;

import org.objectweb.fractal.document.Element;
import org.objectweb.fractal.fraclet.extensions.Membrane;

import spoon.processing.AbstractAnnotationProcessor;
import spoon.reflect.declaration.CtClass;

/**
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class MembraneProcessor extends
        AbstractAnnotationProcessor<Membrane, CtClass<?>> implements
        ControllerTags, TemplateTags {
    /* (non-Javadoc)
     * @see spoon.processing.AbstractProcessor#init()
     */
    @Override
    public void init() {
        addProcessedAnnotationType(Membrane.class);
        super.init();
    }

    /* (non-Javadoc)
     * @see spoon.processing.AbstractAnnotationProcessor#inferConsumedAnnotationType()
     */
    @Override
    public boolean inferConsumedAnnotationType() {
        return false;
    }

    /* (non-Javadoc)
     * @see spoon.processing.AnnotationProcessor#process(java.lang.annotation.Annotation, spoon.reflect.declaration.CtElement)
     */
    public void process(Membrane ann, CtClass<?> cls) {
        getEnvironment().debugMessage(
                "[FractalADL] Processing membrane for component "
                        + cls.getQualifiedName() + "...");
        if (!EMPTY.equals(ann.controller())) {
            Element ctrl = new Element(CONTROLLER_TAG);
            ctrl.setAttribute(CTRL_DESC, ann.controller());
            primitive().definition(cls).getChilds().add(ctrl);
        } else if (!EMPTY.equals(ann.template())) {
            Element tmpl = new Element(TEMPLATE_TAG);
            tmpl.setAttribute(TMPL_DESC, ann.template());
            primitive().definition(cls).getChilds().add(tmpl);
        } else
            getEnvironment()
                    .report(this, ERROR, cls,
                            "Annotation @Membrane should declare a controller or/and a template attribute");
    }
}