/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.adl.spoonlet.interfaces;

/**
 * XML Tags used to describes component interfaces.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public interface InterfaceTags {
    /** Tag describing a component interface. */
    public static final String INTERFACE_TAG = "interface";
    /** Attribute describing the name of a component interface. */
    public static final String ITF_NAME = "name";
    /** Attribute describing the role of a component interface. */
    public static final String ITF_ROLE = "role";
    /** Attribute describing the contingency of a component interface. */
    public static final String ITF_CONTINGENCY = "contingency";
    /** Attribute describing the cardinality of a component interface. */
    public static final String ITF_CARDINALITY = "cardinality";
    /** Attribute describing the signature of a component interface. */
    public static final String ITF_SIGNATURE = "signature";
}