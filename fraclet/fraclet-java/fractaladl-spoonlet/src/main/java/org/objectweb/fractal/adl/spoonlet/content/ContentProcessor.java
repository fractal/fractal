/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.adl.spoonlet.content;

import static org.objectweb.fractal.adl.spoonlet.definition.PrimitiveDefinitionGenerator.primitive;
import static spoon.reflect.declaration.ModifierKind.ABSTRACT;

import org.objectweb.fractal.document.Element;
import org.objectweb.fractal.fraclet.annotations.Component;

import spoon.processing.AbstractAnnotationProcessor;
import spoon.reflect.declaration.CtClass;

/**
 * Processor interpreting the annotation <code>Component</code> to generate
 * the associated <code>content</code> elements in Fractal ADL definitions.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class ContentProcessor extends
AbstractAnnotationProcessor<Component, CtClass<?>> implements
        ContentTags {
//    private final Logger log = getLogger("spoonlet.fractaladl.definition.content");

    /* (non-Javadoc)
     * @see spoon.processing.AbstractProcessor#init()
     */
    @Override
    public void init() {
        addProcessedAnnotationType(Component.class);
        super.init();
    }

    @Override
    public boolean inferConsumedAnnotationType() {
        return false;
    }
    
    /* (non-Javadoc)
     * @see org.objectweb.fractal.adl.spoonlet.component.AbstractDefinitionProcessor#process(java.lang.annotation.Annotation, spoon.reflect.declaration.CtType)
     */
    public void process(Component ann, CtClass<?> cls) {
        if (!cls.hasModifier(ABSTRACT)) {
            Element content = new Element(CONTENT_TAG);
            content.setAttribute(CONT_CLASS, cls.getQualifiedName());
            primitive().definition(cls).getChilds().add(content);
        }
    }
}
