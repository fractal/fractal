/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.adl.spoonlet.definition;

/**
 * XML Tags used to describes a component.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public interface DefinitionTags {
    /** Tag describing a component. */
    static final String DEFINITION_TAG = "definition";
    /** Attribute describing the component name. */
    static final String DEF_NAME = "name";
    /** Attribute describing the component definition(s) extended. */
    static final String DEF_EXTENDS = "extends";
    /** Attribute describing the component configurable arguments. */
    static final String DEF_ARGUMENTS = "arguments";
}