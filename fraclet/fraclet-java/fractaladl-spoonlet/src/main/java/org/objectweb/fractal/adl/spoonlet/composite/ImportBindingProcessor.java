/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.adl.spoonlet.composite;

import static org.objectweb.fractal.adl.spoonlet.composite.ClientComponentProcessor.CLIENT;
import static org.objectweb.fractal.adl.spoonlet.composite.CompositeDefinitionGenerator.composite;
import static org.objectweb.fractal.adl.spoonlet.definition.Arguments.asArgument;
import static org.objectweb.fractal.fraclet.types.Cardinality.COLLECTION;
import static org.objectweb.fractal.fraclet.types.Contingency.OPTIONAL;
import static org.objectweb.fractal.spoonlet.binding.BindingHelper.bindingName;
import static org.objectweb.fractal.spoonlet.binding.BindingHelper.bindingType;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.getFieldAnnotation;
import static org.objectweb.fractal.spoonlet.component.InterfaceHelper.interfaceName;

import org.objectweb.fractal.adl.spoonlet.binding.BindingTags;
import org.objectweb.fractal.document.Element;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Requires;

import spoon.processing.AbstractAnnotationProcessor;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.reference.CtFieldReference;

/**
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class ImportBindingProcessor extends
        AbstractAnnotationProcessor<Component, CtClass<?>> implements
        BindingTags {
    /* (non-Javadoc)
     * @see spoon.processing.AbstractProcessor#init()
     */
    @Override
    public void init() {
        addProcessedAnnotationType(Component.class);
        super.init();
    }

    /* (non-Javadoc)
     * @see spoon.processing.AbstractAnnotationProcessor#inferConsumedAnnotationType()
     */
    @Override
    public boolean inferConsumedAnnotationType() {
        return false;
    }

    /* (non-Javadoc)
     * @see spoon.processing.AnnotationProcessor#process(java.lang.annotation.Annotation, spoon.reflect.declaration.CtElement)
     */
    public void process(Component ann, CtClass<?> cls) {
        for (CtFieldReference<?> fld : cls.getReference().getAllFields()) {
            Requires req = getFieldAnnotation(fld,Requires.class);
            if (req != null)
                generate(cls, fld, req);
        }
    }

    /**
     * Generates the local binding definition element.
     * 
     * @param cls definition declaring the local binding.
     * @param fld field associated to the local binding.
     * @param ann annotation declaring the binding as a field.
     */
    private void generate(CtClass<?> cls, CtFieldReference<?> fld, Requires ann) {
        if (COLLECTION.equals(ann.cardinality())
                || OPTIONAL.equals(ann.contingency()))
            return;
        getEnvironment().debugMessage(
                "[FractalADL] Importing client interface "
                        + cls.getQualifiedName() + "." + bindingName(fld, ann)
                        + "...");
        Element root = composite().definition(cls);
        Element binding = new Element(BINDING_TAG);
        String clientItf = bindingName(fld, ann);
        binding.setAttribute(BIND_CLIENT, asArgument.apply(CLIENT) + "."
                + clientItf);
        String serverItf = interfaceName(bindingType(fld, ann, getFactory()));
        binding.setAttribute(BIND_SERVER, asArgument.apply(clientItf) + "."
                + serverItf);
        root.getChilds().add(binding);
    }
}