/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.adl.spoonlet.binding;

import static org.objectweb.fractal.adl.spoonlet.definition.PrimitiveDefinitionGenerator.primitive;
import static org.objectweb.fractal.spoonlet.binding.BindingHelper.bindingCardinality;
import static org.objectweb.fractal.spoonlet.binding.BindingHelper.bindingContingency;
import static org.objectweb.fractal.spoonlet.binding.BindingHelper.bindingName;
import static org.objectweb.fractal.spoonlet.binding.BindingHelper.bindingSignature;
import static org.objectweb.fractal.spoonlet.component.InterfaceHelper.ROLE_CLIENT;

import org.objectweb.fractal.adl.spoonlet.interfaces.InterfaceTags;
import org.objectweb.fractal.document.Element;
import org.objectweb.fractal.fraclet.annotations.Requires;

import spoon.processing.AbstractAnnotationProcessor;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;
import spoon.reflect.reference.CtFieldReference;

/**
 * Processor interpreting the annotation <code>Requires</code> to generate the
 * associated <code>binding</code> elements in Fractal ADL definitions.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class RequiresProcessor extends
        AbstractAnnotationProcessor<Requires, CtField<?>> implements
        InterfaceTags {
    /* (non-Javadoc)
     * @see spoon.processing.AbstractProcessor#init()
     */
    @Override
    public void init() {
        addProcessedAnnotationType(Requires.class);
        super.init();
    }

    /* (non-Javadoc)
     * @see spoon.processing.AbstractAnnotationProcessor#inferConsumedAnnotationType()
     */
    @Override
    public boolean inferConsumedAnnotationType() {
        return false;
    }

    /* (non-Javadoc)
     * @see spoon.processing.AnnotationProcessor#process(java.lang.annotation.Annotation, spoon.reflect.declaration.CtElement)
     */
    public void process(Requires ann, CtField<?> fld) {
        CtClass<?> cls = fld.getParent(CtClass.class);
        getEnvironment().debugMessage(
                "[FractalADL] Processing binding " + cls.getQualifiedName()
                        + "." + bindingName(fld.getReference(), ann) + "...");
        primitive().definition(cls).getChilds().add(
                getRequires(ann, fld.getReference()));
    }

    /**
     * @param ann
     * @param fld
     * @return
     */
    public static Element getRequires(Requires ann, CtFieldReference<?> fld) {
        Element itf = new Element(INTERFACE_TAG);
        itf.setAttribute(ITF_NAME, bindingName(fld, ann));
        itf.setAttribute(ITF_ROLE, ROLE_CLIENT);
        itf.setAttribute(ITF_CONTINGENCY, bindingContingency(fld, ann));
        itf.setAttribute(ITF_CARDINALITY, bindingCardinality(fld, ann));
        itf.setAttribute(ITF_SIGNATURE, bindingSignature(fld, ann));
        return itf;
    }
}
