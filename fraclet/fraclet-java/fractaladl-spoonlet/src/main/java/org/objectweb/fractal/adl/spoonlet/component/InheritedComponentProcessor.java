/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2008 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.adl.spoonlet.component;

import static org.objectweb.fractal.adl.spoonlet.component.LegacyDefinitionProcessor.addLegacyDefinition;
import static org.objectweb.fractal.adl.spoonlet.definition.AbstractDefinitionProcessor.addDefinitionArguments;
import static org.objectweb.fractal.adl.spoonlet.definition.AbstractDefinitionProcessor.addDefinitionExtends;
import static org.objectweb.fractal.adl.spoonlet.definition.PrimitiveDefinitionGenerator.primitive;
import static org.objectweb.fractal.spoonlet.attribute.AttributeHelper.attributeName;
import static org.objectweb.fractal.spoonlet.attribute.AttributeHelper.attributeValue;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.componentFullname;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.getTypeAnnotation;

import org.objectweb.fractal.adl.spoonlet.definition.Arguments;
import org.objectweb.fractal.adl.spoonlet.definition.DefinitionTags;
import org.objectweb.fractal.document.Element;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Definition;

import spoon.processing.AbstractAnnotationProcessor;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtFieldReference;
import spoon.reflect.reference.CtTypeReference;

/**
 * Processor inserting the arguments referring to attributes inherited from
 * parent classes.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class InheritedComponentProcessor extends
        AbstractAnnotationProcessor<Component, CtClass<?>> implements
        DefinitionTags {
    /* (non-Javadoc)
     * @see spoon.processing.AbstractProcessor#init()
     */
    @Override
    public void init() {
        addProcessedAnnotationType(Component.class);
        super.init();
    }

    /* (non-Javadoc)
     * @see spoon.processing.AbstractAnnotationProcessor#inferConsumedAnnotationType()
     */
    @Override
    public boolean inferConsumedAnnotationType() {
        return false;
    }

    /* (non-Javadoc)
     * @see spoon.processing.AnnotationProcessor#process(java.lang.annotation.Annotation, spoon.reflect.declaration.CtElement)
     */
    public void process(Component ann, CtClass<?> cls) {
        getEnvironment().debugMessage(
                "[FractalADL] Inferring extended definitions for "
                        + cls.getQualifiedName() + "...");
        Element root = primitive().definition(cls);
        for (CtTypeReference<?> type = cls.getSuperclass(); type != null; type = type
                .getSuperclass()) {
            Component a = getTypeAnnotation(type, Component.class);
            if (a != null) {
                String def = componentFullname(type, a);
                Arguments arg = getArguments(type);
                if (arg != null && !arg.isEmpty()) {
                    def += "(" + arg.asParameters() + ")";
                    addDefinitionArguments(root, arg);
                }
                getEnvironment().debugMessage(
                        "[FractalADL] Adding extended definition " + def);
                addDefinitionExtends(root, def);
                break;
            }
        }
        for (Definition def : ann.uses())
            addLegacyDefinition(root, def);
    }

    /**
     * Retrieves the list of arguments associated to the type. If the associated
     * definition is not available, it uses introspection to retrieve the list
     * of attributes.
     * 
     * @param type type associated to a component definition.
     * @return the list of arguments for the requested type.
     */
    private final Arguments getArguments(CtTypeReference<?> type) {
        if (type.getDeclaration() != null)
            return (Arguments) primitive().definition(
                    (CtType<?>) type.getDeclaration()).getAttribute(
                    DEF_ARGUMENTS);
        Arguments args = new Arguments();
        for (CtFieldReference<?> fld : type.getAllFields())
            try {
                args.put(attributeName(fld), attributeValue(fld));
            } catch (RuntimeException ex) {
                // Ignore exception when trying to infer wrong Fractal elements.
            }
        getEnvironment().debugMessage(
                "[FractalADL] Introspecting arguments for \"" + type + "\": "
                        + args);
        return args;
    }
}