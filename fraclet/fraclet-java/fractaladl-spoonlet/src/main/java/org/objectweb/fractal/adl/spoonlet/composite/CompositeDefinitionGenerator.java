package org.objectweb.fractal.adl.spoonlet.composite;

import org.objectweb.fractal.document.DocumentFactory;
import org.objectweb.fractal.xml.AbstractDocumentGenerator;

/**
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class CompositeDefinitionGenerator extends AbstractDocumentGenerator {
    /** Default instance of the Fractal ADL description factory. */
    private static final DocumentFactory composite = new DocumentFactory();

    /* (non-Javadoc)
     * @see org.objectweb.fractal.xml.AbstractDocumentGenerator#factory()
     */
    protected DocumentFactory factory() {
        return composite;
    }

    /**
     * Provides the instance of the default factory.
     * 
     * @return the reference of the Fractal ADL description factory.
     */
    public static final DocumentFactory composite() {
        return composite;
    }
}