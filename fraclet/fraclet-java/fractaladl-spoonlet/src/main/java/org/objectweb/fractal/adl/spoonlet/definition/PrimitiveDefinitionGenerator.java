/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.adl.spoonlet.definition;

import org.objectweb.fractal.document.DocumentFactory;
import org.objectweb.fractal.xml.AbstractDocumentGenerator;

/**
 * This processor dumps the memory representation of Fractal ADL descriptions
 * into files.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class PrimitiveDefinitionGenerator extends AbstractDocumentGenerator {
    /** Default instance of the Fractal ADL description factory. */
    private static final DocumentFactory primitive = new DocumentFactory();

    /* (non-Javadoc)
     * @see org.objectweb.fractal.xml.AbstractDocumentGenerator#factory()
     */
    protected DocumentFactory factory() {
        return primitive;
    }

    /**
     * Provides the instance of the default factory.
     * 
     * @return the reference of the Fractal ADL description factory.
     */
    public static final DocumentFactory primitive() {
        return primitive;
    }
}