/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.adl.spoonlet.composite;

import static org.objectweb.fractal.adl.spoonlet.composite.CompositeDefinitionGenerator.composite;
import static org.objectweb.fractal.adl.spoonlet.interfaces.InterfaceProcessor.addProvidedInterface;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.getTypeAnnotation;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.hasFieldAnnotation;

import org.objectweb.fractal.adl.spoonlet.component.PrimitiveComponentProcessor;
import org.objectweb.fractal.document.DocumentFactory;
import org.objectweb.fractal.document.Element;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Interface;
import org.objectweb.fractal.fraclet.annotations.Requires;

import spoon.processing.Property;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtSimpleType;
import spoon.reflect.reference.CtTypeReference;

/**
 * Processor interpreting the annotation <code>Component</code> to generate
 * the associated composite <code>definition</code> element in Fractal ADL
 * definitions.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class CompositeComponentProcessor extends PrimitiveComponentProcessor {
    @Property("Suffix added to the composite definition (default is Composite)")
    private String extension = "Composite";

    /* (non-Javadoc)
     * @see org.objectweb.fractal.adl.spoonlet.component.ComponentProcessor#process(org.objectweb.fractal.fraclet.annotations.Component, spoon.reflect.declaration.CtClass)
     */
    @Override
    public void process(Component ann, CtClass<?> cls) {
        if (!hasFieldAnnotation(cls.getReference(), Requires.class, true))
            return;
        getEnvironment().debugMessage(
                "[FractalADL] Processing composite component "
                        + cls.getQualifiedName() + "...");
        super.process(ann, cls);
        Element root = composite().definition(cls);
        for (CtTypeReference<?> type = cls.getSuperclass(); type != null; type = type
                .getSuperclass()) {
            Component comp = getTypeAnnotation(type, Component.class);
            if (comp != null) {
                for (CtTypeReference<?> itf : cls.getSuperInterfaces())
                    try { // Quick fix for a bug detected in Spoon
                    if (getTypeAnnotation(itf,Interface.class) != null)
                        addDefinitionExtends(root, itf.getQualifiedName());
                    } catch (Exception e) {}
                for (Interface itf : comp.provides())
                    addProvidedInterface(root, itf, getFactory().Type()
                            .createReference(itf.signature()));
            }
        }
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.adl.spoonlet.component.ComponentProcessor#getComponentName(spoon.reflect.declaration.CtSimpleType, org.objectweb.fractal.fraclet.annotations.Component)
     */
    @Override
    protected String getComponentName(CtSimpleType<?> comp, Component ann) {
        return super.getComponentName(comp, ann) + this.extension;
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.adl.spoonlet.definition.AbstractDefinitionProcessor#factory()
     */
    @Override
    protected DocumentFactory documents() {
        return composite();
    }
}