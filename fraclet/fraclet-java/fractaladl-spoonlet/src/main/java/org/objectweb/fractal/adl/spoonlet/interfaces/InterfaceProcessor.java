/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.adl.spoonlet.interfaces;

import static org.objectweb.fractal.adl.spoonlet.definition.PrimitiveDefinitionGenerator.primitive;
import static org.objectweb.fractal.spoonlet.component.InterfaceHelper.DEFAULT_CARDINALITY;
import static org.objectweb.fractal.spoonlet.component.InterfaceHelper.DEFAULT_CONTINGENCY;
import static org.objectweb.fractal.spoonlet.component.InterfaceHelper.DEFAULT_ROLE;
import static org.objectweb.fractal.spoonlet.component.InterfaceHelper.interfaceName;
import static org.objectweb.fractal.spoonlet.component.InterfaceHelper.interfaceSignature;

import org.objectweb.fractal.adl.spoonlet.definition.AbstractDefinitionProcessor;
import org.objectweb.fractal.document.Element;
import org.objectweb.fractal.fraclet.annotations.Interface;

import spoon.reflect.declaration.CtInterface;
import spoon.reflect.reference.CtTypeReference;

/**
 * Processor interpreting the annotation <code>Interface</code> to generate
 * the associated <code>definition</code> elements in Fractal ADL definitions.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class InterfaceProcessor extends
        AbstractDefinitionProcessor<Interface, CtInterface<?>> implements
        InterfaceTags {
    /* (non-Javadoc)
     * @see spoon.processing.AbstractProcessor#init()
     */
    @Override
    public void init() {
        addProcessedAnnotationType(Interface.class);
        super.init();
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.adl.spoonlet.component.AbstractDefinitionProcessor#process(java.lang.annotation.Annotation, spoon.reflect.declaration.CtType)
     */
    public void process(Interface ann, CtInterface<?> itf) {
        super.process(ann, itf);
        getEnvironment().debugMessage("[FractalADL] Processing server interface "
                        + interfaceName(ann, itf.getReference()) + " <"
                        + itf.getQualifiedName() + ">...");
        Element def = primitive().definition(itf);
        addProvidedInterface(def, ann, itf.getReference());
    }

    /**
     * Add the element associated to an interface processed.
     * 
     * @param ann the annotation Interface processed.
     * @param itf the interface type processed.
     */
    public static final void addProvidedInterface(Element root, Interface ann,
            CtTypeReference<?> itf) {
        Element intf = new Element(INTERFACE_TAG);
        intf.setAttribute(ITF_NAME, interfaceName(ann, itf));
        intf.setAttribute(ITF_ROLE, DEFAULT_ROLE);
        intf.setAttribute(ITF_CONTINGENCY, DEFAULT_CONTINGENCY);
        intf.setAttribute(ITF_CARDINALITY, DEFAULT_CARDINALITY);
        intf.setAttribute(ITF_SIGNATURE, interfaceSignature(ann, itf));
        root.getChilds().add(intf);
    }
}