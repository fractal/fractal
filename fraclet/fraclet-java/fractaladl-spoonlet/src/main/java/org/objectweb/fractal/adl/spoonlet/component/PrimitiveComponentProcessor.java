/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.adl.spoonlet.component;

import static org.objectweb.fractal.adl.spoonlet.interfaces.InterfaceProcessor.addProvidedInterface;
import static org.objectweb.fractal.spoonlet.component.ComponentHelper.componentFullname;

import org.objectweb.fractal.adl.spoonlet.definition.AbstractDefinitionProcessor;
import org.objectweb.fractal.document.Element;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Interface;

import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtSimpleType;

/**
 * Processor interpreting the annotation <code>Component</code> to generate
 * the associated <code>definition</code> element in Fractal ADL definitions.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
public class PrimitiveComponentProcessor extends
        AbstractDefinitionProcessor<Component, CtClass<?>> {
    /* (non-Javadoc)
     * @see spoon.processing.AbstractProcessor#init()
     */
    @Override
    public void init() {
        addProcessedAnnotationType(Component.class);
        super.init();
    }

    /* (non-Javadoc)
     * @see spoon.processing.AbstractAnnotationProcessor#inferConsumedAnnotationType()
     */
    @Override
    public boolean inferConsumedAnnotationType() {
        return false;
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.adl.spoonlet.component.AbstractDefinitionProcessor#process(java.lang.annotation.Annotation, spoon.reflect.declaration.CtType)
     */
    public void process(Component ann, CtClass<?> cls) {
        super.process(ann, cls);
        String name = getComponentName(cls, ann);
        getEnvironment().debugMessage(
                "[FractalADL] Processing component implementation " + name
                        + "...");
        documents().document(cls).setFullname(name);
        Element root = documents().definition(cls);
        root.setAttribute(DEF_NAME, name);
        for (Interface itf : ann.provides())
            addProvidedInterface(root, itf, getFactory().Type().createReference(
                    itf.signature()));
    }

    /**
     * @param comp
     * @param ann
     * @return
     */
    protected String getComponentName(CtSimpleType<?> comp, Component ann) {
        return componentFullname(comp.getReference(), ann);
    }
}
