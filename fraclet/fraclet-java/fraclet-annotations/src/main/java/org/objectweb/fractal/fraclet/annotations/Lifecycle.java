/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2008 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier
 Contributor(s): Romain Rouvoy
 =============================================================================*/
package org.objectweb.fractal.fraclet.annotations;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

import org.objectweb.fractal.fraclet.types.Policy;
import org.objectweb.fractal.fraclet.types.Step;

/**
 * Description of a life-cycle listener.
 * 
 * @author Nicolas Pessemier (Initial developer)
 * @author Romain Rouvoy (Contributor)
 * @version $Revision$
 */
@Retention(RUNTIME)
public @interface Lifecycle {
    /**
     * The life-cycle step handled by the listener.
     * 
     * @return Step handled.
     */
    Step step();

    /**
     * Defines whether the life-cycle listener invocation policy is synchronous
     * or asynchronous.
     * 
     * @return Policy.SYNCHRONOUS if the listener is synchronous.
     */
    Policy policy() default Policy.SYNCHRONOUS;
}
