/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier
 Contributor(s): Romain Rouvoy
 =============================================================================*/
package org.objectweb.fractal.fraclet.types;


/**
 * Cardinality of a required interface (SINGLETON|COLLECTION).
 * 
 * @author Romain Rouvoy (Contributor)
 * @author Nicolas Pessemier (Initial Developer)
 * @version $Revision$
 */
public enum Access {
    /** Read only access mode */
    READ_ONLY(0),
    /** Read/write access mode */
    READ_WRITE(1),
    /** Write only access mode */
    WRITE_ONLY(2);

    /** Internal representation of the access mode. */
    private final int value;

    /**
     * Enumeration constructor.
     * 
     * @param value integer value associated to the enumeration.
     */
    Access(int value) {
        this.value = value;
    }

    /**
     * The integer value associated to the access mode:<br>
     * <ul>
     * <li>value &le; 1 means read mode</li>
     * <li>value &ge; 1 means write mode</li>
     * </ul>
     * 
     * @return the boolean value associated to the cardinality.
     */
    public final int value() {
        return this.value;
    }
    
    /**
     * @return
     */
    public final boolean isReadable() {
        return this.value <2;
    }
    /**
     * @return
     */
    public final boolean isWritable() {
        return this.value >0;
    }
}
