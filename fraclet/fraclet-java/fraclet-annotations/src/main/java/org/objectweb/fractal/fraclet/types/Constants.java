/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.fraclet.types;

/**
 * Constant definitions.
 * 
 * @author Romain Rouvoy (Contributor)
 * @version $Revision$
 */
public class Constants {
    /** Definition of an empty string. */
    public static final String EMPTY = "";
    
    public static final Class<?> NONE = Constants.class;
    
    /** Step triggered when the component is created. */
    public static final int CREATED = 0;
    /** Step triggered when the component is started. */
    public static final int STARTED = 1;
    /** Step triggered when the component is stopped. */
    public static final int STOPPED = 2;
    /** Step triggered when the component is destroyed. */
    public static final int DESTROYED = 3;

    /**
     * Checking an argument is empty.
     * @param arg argument to check
     * @return true is the argument is empty
     */
    public static final boolean isEmpty(String arg) {
        return EMPTY.equals(arg);
    }
    
    /**
     * Checking a signature is explicit
     * @param sig signature to check.
     * @return true if the signature is empty
     */
    public static final boolean isExplicit(Class<?> sig) {
        return sig != Constants.class;
    }
}
