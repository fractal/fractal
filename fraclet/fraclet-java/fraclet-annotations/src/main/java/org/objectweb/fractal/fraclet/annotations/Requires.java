/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2008 Universitetet i Oslo
 Copyright (C) 2008-2009 INRIA Lille - Nord Europe / LIFL

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier
 Contributor(s): Romain Rouvoy
 =============================================================================*/
package org.objectweb.fractal.fraclet.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static org.objectweb.fractal.fraclet.types.Cardinality.SINGLETON;
import static org.objectweb.fractal.fraclet.types.Constants.EMPTY;
import static org.objectweb.fractal.fraclet.types.Contingency.MANDATORY;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.objectweb.fractal.fraclet.types.Cardinality;
import org.objectweb.fractal.fraclet.types.Constants;
import org.objectweb.fractal.fraclet.types.Contingency;

/**
 * Description of a component dependency towards another interface (also known
 * as client/required interface).
 * 
 * @author Nicolas Pessemier (Initial developer)
 * @author Romain Rouvoy (Contributor)
 * @version $Revision$
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface Requires {
    /**
     * Name of the client dependencies.<br>
     * Unique within the scope of the associated component.
     * 
     * @return the name of the bound Fractal interface
     */
    String name() default EMPTY;

    /**
     * Cardinality of the dependency.
     * 
     * @return the cardinality[SINGLETON/COLLECTION] of the Fractal interface
     */
    Cardinality cardinality() default SINGLETON;

    /**
     * Contingency of the dependency.
     * 
     * @return the contingency[MANDATORY/OPTIONAL] of the Fractal interface
     */
    Contingency contingency() default MANDATORY;

    /**
     * Method used to bind the dependency (optional).
     * 
     * @return the simple name of the custom binding method.
     */
    String bind() default EMPTY;

    /**
     * Method used to unbind the dependency (optional).
     * 
     * @return the simple name of the custom unbinding method.
     */
    String unbind() default EMPTY;
    
    /**
     * Signature of the Fractal client interface.
     * 
     * @return the java signature of the Fractal client interface
     */
    Class<?> signature() default Constants.class;
}