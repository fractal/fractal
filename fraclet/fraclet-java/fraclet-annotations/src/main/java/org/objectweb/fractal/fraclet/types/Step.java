/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier
 Contributor(s): Romain Rouvoy
 =============================================================================*/
package org.objectweb.fractal.fraclet.types;

import static org.objectweb.fractal.fraclet.types.Constants.CREATED;
import static org.objectweb.fractal.fraclet.types.Constants.DESTROYED;
import static org.objectweb.fractal.fraclet.types.Constants.STARTED;
import static org.objectweb.fractal.fraclet.types.Constants.STOPPED;

/**
 * Life-cycle steps associated to the life-cycle of component.
 * 
 * @author Romain Rouvoy (Contributor)
 * @author Nicolas Pessemier (Initial Developer)
 * @version $Revision$
 */
public enum Step {
    /** Step triggered when the component is created. */
    CREATE(CREATED),
    /** Step triggered when the component is started/resumed. */
    START(STARTED),
    /** Step triggered when the component is suspended/stopped. */
    STOP(STOPPED),
    /** Step triggered when the component is destroyed. */
    DESTROY(DESTROYED);

    /** Internal representation of the life-cycle. */
    private final int value;

    /**
     * Enumeration constructor.
     * 
     * @param value value associated to the life-cycle step.
     */
    Step(int value) {
        this.value = value;
    }

    /**
     * Provides the internal value associated to the enumeration.
     * 
     * @return internal value associated to the
     */
    public final int value() {
        return this.value;
    }
}
