/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier
 Contributor(s): Romain Rouvoy
 =============================================================================*/
package org.objectweb.fractal.fraclet.annotations;

import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static org.objectweb.fractal.fraclet.types.Constants.EMPTY;

import java.lang.annotation.Retention;

/**
 * Description of a component implementation.
 * <p>
 * This annotation should be systematically associated to the implementation of
 * a component. A component implementation represents the content of a primitive
 * component. The component can be configured with a specific <b>identifier</b>.
 * It also allows the developer to specify the <b>controller</b>, <b>factory</b>,
 * and <b>node</b> meta-data used to deploy the component.
 * </p>
 * <p>
 * This annotation can specify a list of annotations <code>Interface</code> to
 * declare additional provided interfaces that cannot be tagged (<i>e.g.</i>,
 * a component providing an interface <code>java.lang.Runnable<code> and an
 * interface <code>Account</code> handled by an external library). 
 * Thus, an example of use would be:
 * </p>
 * <pre>
 * <code>
 * &#064;Component(provides={@Interface(name=&quot;r&quot;, signature=java.lang.Runnable.class),&lt;/code&gt;
 * <code>
 *                      &#064;Interface(&quot;a&quot;,signature=&quot;Account&quot;)})
 * </code>
 * <code>
 * public class AccountImpl implements Account, Runnable {
 * </code>
 * <code>
 * (...)
 * </code>
 * <code>
 * }
 * </code>
 * </pre>
 * 
 * @author Nicolas Pessemier (Initial developer)
 * @author Romain Rouvoy (Contributor)
 * @version $Revision$
 */
@Retention(RUNTIME)
public @interface Component {
    /**
     * Identifier of the component.<br>
     * Unique.
     * 
     * @return Logical name of the component in the architecture.
     */
    String name() default EMPTY;

    /**
     * The list of annotations interfaces to declare (using annotation
     * <code>@Interface</code>).
     * @return List of provided interfaces.
     */
    Interface[] provides() default {};

    /**
     * The list of legacy definitions extended by the component (using
     * annotation <code>@Definition</code>).
     * @return List of extended definitions.
     */
    Definition[] uses() default {};
}