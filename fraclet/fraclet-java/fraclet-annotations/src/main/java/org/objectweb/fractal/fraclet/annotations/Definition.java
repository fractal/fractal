/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.fraclet.annotations;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/**
 * Description of a legacy artefact.
 * <p>
 * Legacy artefact are used to integrate legacy component libraries in a
 * component-based architecture. By using this annotation, you define explicitly
 * the third-party definition(s) that the component should extend.
 * </p>
 * 
 * @author Romain Rouvoy (Initial developer)
 * @version $Revision$
 */
@Retention(RUNTIME)
public @interface Definition {
    /**
     * Describe the legacy definition to extend (mandatory attribute).
     * 
     * @return the name of the definition.
     */
    String name();

    /**
     * Describes the arguments to propagate to the definition.
     * 
     * @return the list of arguments to propagate (a default value can be
     *         defined as <code>argument=>value</code>).
     */
    String[] arguments() default {};
}
