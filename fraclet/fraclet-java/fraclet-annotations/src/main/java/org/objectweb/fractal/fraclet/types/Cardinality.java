/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier
 Contributor(s): Romain Rouvoy
 =============================================================================*/
package org.objectweb.fractal.fraclet.types;

import org.objectweb.fractal.api.type.TypeFactory;

/**
 * Cardinality of a required interface (SINGLETON|COLLECTION).
 * 
 * @author Romain Rouvoy (Contributor)
 * @author Nicolas Pessemier (Initial Developer)
 * @version $Revision$
 */
public enum Cardinality {
    /** Maximal cardinality is singleton (1) */
    SINGLETON(TypeFactory.SINGLE),
    /** Maximal cardinality is collection (*) */
    COLLECTION(TypeFactory.COLLECTION);

    /** Internal representation of the cardinality. */
    private final boolean value;

    /**
     * Enumeration constructor.
     * 
     * @param value TypeFactory boolean value associated to the enumeration.
     */
    Cardinality(boolean value) {
        this.value = value;
    }

    /**
     * The TypeFactory value associated to the cardinality.
     * 
     * @return the boolean value associated to the cardinality.
     */
    public final boolean isCollection() {
        return this.value;
    }
}
