/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier
 Contributor(s): Romain Rouvoy
 =============================================================================*/
package org.objectweb.fractal.fraclet.extensions;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static org.objectweb.fractal.fraclet.types.Constants.EMPTY;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Description of a dependency to a service provided by the container (also
 * known as controller).
 * <p>
 * The annotation <code>Controller</code> provides a reflective support for
 * the component implementation. It allows the developer to access services
 * provided by the infrastructure at run-time. </b>
 * 
 * @author Nicolas Pessemier (Initial developer)
 * @author Romain Rouvoy (Contributor)
 * @version $Revision$
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface Controller {
    /**
     * Identifier of the service provided by the container.
     * @return name of the service requested.
     */
    String name() default "component";
    
    /**
     * List of mixins used to implement the controller.
     * @return list of mixin classes
     * @since 3.3 
     */
    Class<?>[] mixins() default {};
    
    /**
     * Name of the class generated from the {@link #mixins()} layers.
     * @return name of the generated class
     * @since 3.3
     */
    String impl() default EMPTY;
}
