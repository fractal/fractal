/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2008 Universitetet i Oslo
 Copyright (C) 2008-2009 INRIA Lille - Nord Europe / LIFL

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier
 Contributor(s): Romain Rouvoy
 =============================================================================*/
package org.objectweb.fractal.fraclet.annotations;

import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static org.objectweb.fractal.fraclet.types.Constants.EMPTY;

import java.lang.annotation.Retention;

import org.objectweb.fractal.fraclet.types.Constants;

/**
 * Description of a component interface.
 * <p align=justify>
 * This annotation is used to declare a contract between components. This
 * contract, defined as a set of method, describes the interactions allowed
 * between two components.
 * </p>
 * 
 * @author Nicolas Pessemier (Initial developer)
 * @author Romain Rouvoy (Contributor)
 * @version $Revision$
 */
@Retention(RUNTIME)
public @interface Interface {
    /**
     * Name of the component interface.<br>
     * Unique within the scope of the associated component.
     * 
     * @return the name of the interface (default is interface name in lower
     *         cases)
     */
    String name() default EMPTY;

    /**
     * @return the java signature of the Fractal server interface
     */
    Class<?> signature() default Constants.class;
}