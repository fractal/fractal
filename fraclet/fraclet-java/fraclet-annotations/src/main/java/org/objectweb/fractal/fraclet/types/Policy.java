/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Nicolas Pessemier
 Contributor(s): Romain Rouvoy
 =============================================================================*/
package org.objectweb.fractal.fraclet.types;


/**
 * Policies associated to the activation of a component.
 * 
 * @author Romain Rouvoy (Contributor)
 * @version $Revision$
 */
public enum Policy {
    /** Synchronous invocation policy (blocking). */
    SYNCHRONOUS(true),
    /** Asynchronous invocation policy (non-blocking). */
    ASYNCHRONOUS(false);

    /** Internal representation of the life-cycle. */
    private final boolean value;

    /**
     * Enumeration constructor.
     * 
     * @param value value associated to the life-cycle step.
     */
    Policy(boolean value) {
        this.value = value;
    }

    /**
     * Provides the internal value associated to the enumeration.
     * 
     * @return internal value associated to the
     */
    public final boolean value() {
        return this.value;
    }
    
    /**
     * Checks if the policy is synchronous or not.
     * @return true if the policy is SYNCHRONOUS.
     */
    public final boolean isSynchronous() {
        return this.value;
    }
}
