/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): Nicolas Pessemier
 =============================================================================*/
package org.objectweb.fractal.fraclet.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static org.objectweb.fractal.fraclet.types.Access.READ_WRITE;
import static org.objectweb.fractal.fraclet.types.Constants.EMPTY;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.objectweb.fractal.fraclet.types.Access;

/**
 * Description of a component attribute. <p align=justify> An attribute
 * represents a functional parameter (also known as property) of a component,
 * and is described by a <b>identifier</b> and an <b>initial value</b>. The
 * annotation <code>Attribute</code> must be associated to a field declaration,
 * and it cannot be combined with the annotation <code>Requires</code>. </p>
 * 
 * @author Romain Rouvoy (Contributor)
 * @author Nicolas Pessemier (Initial Developer)
 * @version $Revision$
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface Attribute {
    /**
     * Identifier of the component attribute.<br>
     * Unique within the scope of the associated component.
     * 
     * @return Identifier of the attribute (<i>default value is the field
     *         name</i>).
     */
    String name() default EMPTY;

    /**
     * Access mode of the component attribute.
     * 
     * @return Configured access mode (ReadOnly, ReadWrite, WriteOnly).
     */
    Access mode() default READ_WRITE;

    /**
     * Initial value of the component attribute.
     * 
     * @return Initial value of the attribute.
     */
    String value() default EMPTY;

    /**
     * Name of the method used to get the attribute value (optional).
     * 
     * @return Identifier of the custom getter method (<i>default value is
     *         getATTRIBUTE()</i>).
     */
    String get() default EMPTY;

    /**
     * Name of the method used to set the attribute value (optional).
     * 
     * @return Identifier of the custom setter method (<i>default value is
     *         setATTRIBUTE()</i>).
     */
    String set() default EMPTY;
}