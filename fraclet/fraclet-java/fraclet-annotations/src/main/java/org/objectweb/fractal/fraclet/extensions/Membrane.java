/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package org.objectweb.fractal.fraclet.extensions;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static org.objectweb.fractal.fraclet.types.Constants.EMPTY;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.objectweb.fractal.fraclet.types.Constants;

/**
 * <p>
 * Description of a component container.
 * </p>
 * 
 * <p>This annotation type is used either:</p>
 * <ul>
 * <li>for attaching an existing container to a component implementation
 *     class,</li>
 * <li>or for declaring a new container.</li>
 * </ul>
 * 
 * <p>
 * In the former case, the first four parameters are used. In the latter case,
 * the remaining four parameters are used. Note that {@link #controller()} and
 * {@link #controllerDesc()} are mutually exclusive. In case both are used, the
 * value specified by {@link #controllerDesc()} overrides the one specified by
 * {@link #controller()}. The same rule applies for {@link #template()} and
 * {@link #templateDesc()}.
 * </p>
 * 
 * @author Romain Rouvoy (Initial developer)
 * @version $Revision$
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface Membrane {
    /**
     * Identifier of the component container.
     * @return Logical name of the container hosting the component.
     */
    String controller() default EMPTY;

    /**
     * Identifier of the component factory.
     * @return Logical name of the factory used to instantiate the component.
     */
    String template() default EMPTY;
    
    /**
     * Identifier of the component container.
     * @return Class name of the container hosting the component.
     * @since 3.3
     */
    Class<?> controllerDesc() default Constants.class;
    
    /**
     * Identifier of the component factory.
     * @return Class name of the factory used to instantiate the component.
     * @since 3.3
     */
    Class<?> templateDesc() default Constants.class;
    
    /**
     * Identifier of the component container being defined.
     * @return Name of the container (to be used by controller or template).
     * @since 3.3
     */
    String desc() default EMPTY;
    
    /**
     * Class definition of the interceptor class generator.
     * @return Constants.class if none is defined.
     * @since 3.3
     */
    Class<?> generator() default Constants.class;
    
    /**
     * List of interceptors to inject into the membrane.
     * @return empty list if no interceptor is needed.
     * @since 3.3
     */
    Class<?>[] interceptors() default {};
}