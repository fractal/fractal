/***
 * Objectweb naming framework API
 * Copyright (C) 2001-2002 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: architecture@objectweb.org
 */

package org.objectweb.naming;

/**
 * A binder is a {@link NamingContext} that can also give access
 * to the objects designated by the names it manages.
 */

public interface Binder extends NamingContext {

  /**
   * Returns an object that gives access to the object designated by the given
   * name. This name must belong to this naming context, i.e.,
   * <tt>nc.bind(n, hints)</tt> is valid only if <tt>n.getNamingContext() ==
   * nc</tt>. The object returned by this method implements the Java interface
   * of the designated object. It may also implement the {@link Name}
   * interface. This method creates a binding object (or a binding component
   * when it is used with the <a href="http://www.objectweb.org/fractal">Fractal
   * framework</a>) that gives access to the designated object (this binding
   * object can be empty, i.e., the bind method may directly return the
   * designated object itself).
   *
   * @param n a name that has been created by this naming context.
   * @param hints optional additional information.
   * @return an object that gives access to the object designated by <tt>n</tt>.
   *      This object may also implement the {@link Name} interface.
   * @throws NamingException if the binding fails (this is the case, for
   *      example, if the given name was not created by this naming context).
   */

  Object bind (Name n, Object hints) throws NamingException;
}
