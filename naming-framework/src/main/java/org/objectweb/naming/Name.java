/***
 * Objectweb naming framework API
 * Copyright (C) 2001-2002 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: architecture@objectweb.org
 */

package org.objectweb.naming;

/**
 * A name designates an object or an object interface. A name may also provide
 * access to the object or interface it designates. Such a name implements both
 * the {@link Name} interface and the (or a) Java interface of the object
 * it designates.
 */

public interface Name {

  /**
   * Returns the {@link NamingContext} that created this name.
   * In order for a name to implement both the {@link Name} interface and
   * the Java interface of the object it designates, the methods of these two
   * interfaces must have distinct names. To increase the probability of this
   * case, this method has been named "getNamingContext" instead of
   * "getContext", since "getNamingContext" is less frequent than "getContext".
   *
   * @return the {@link NamingContext} that created this name.
   */

  NamingContext getNamingContext ();

  /**
   * Encodes this name as an array of bytes. This method can be used to send
   * names over a network, or to store them on disk.
   *
   * @return an encoded form of this name.
   * @throws NamingException if this name can not be encoded (this is the case,
   *      for example, of names that are only valid inside a given Java Virtual
   *      Machine, and that can therefore not be sent over a network or stored
   *      on a disk).
   */

  byte[] encode () throws NamingException;
}
