/***
 * Objectweb naming framework API
 * Copyright (C) 2001-2002 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: architecture@objectweb.org
 */

package org.objectweb.naming;

/**
 * A naming context creates and manages names.
 */

public interface NamingContext {

  /**
   * Creates a name in this naming context to designate the given object. The
   * {@link Name#getNamingContext getNamingContext} method of the returned name
   * will return this naming context, i.e.,
   * <tt>nc.export(o, hints).getNamingContext() == nc</tt>.
   *
   * @param o the object to be exported. This object may be a name of another
   *      naming context.
   * @param hints optional additional information.
   * @return a name that designates the given object in this naming context.
   * @throws NamingException if the given object cannot be exported.
   */

  Name export (Object o, Object hints) throws NamingException;

  /**
   * Decodes the given encoded name. The {@link Name#getNamingContext
   * getNamingContext} method of the returned name will return this naming
   * context, i.e., <tt>nc.decode(b).getNamingContext() == nc</tt>.
   *
   * @param b an array of byte containing the encoded form of a name created by
   *      this naming context.
   * @return the decoded {@link Name} object.
   * @throws NamingException if the given encoded name cannot be decoded (this
   *      is the case, for example, if the given encoded name was not created by
   *      this naming context).
   */

  Name decode (byte[] b) throws NamingException;
}
