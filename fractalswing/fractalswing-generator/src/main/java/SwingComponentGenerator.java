/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

import java.lang.reflect.*;
import java.io.*;

public class SwingComponentGenerator {

  private static PrintStream out = System.out;
  private static String packageName;

  public static void main (String[] args) throws Exception {
    packageName = args[0];
    for (int i = 1; i < args.length; ++i) {
      Class c = Class.forName(args[i]);
      out = new PrintStream(
        new FileOutputStream("generated/"+getName(c)+"Itf.java"));
      generateInterface(c);
      out.close();
      out = new PrintStream(
        new FileOutputStream("generated/"+getName(c)+".fractal"));
      generateTypeDescriptor(c);
      if (!Modifier.isAbstract(c.getModifiers())) {
        out.println();
        generateTemplateDescriptor(c);
      }
      out.close();
      out = new PrintStream(
        new FileOutputStream("generated/"+getName(c)+"Attributes.java"));
      generateAttributes(c);
      out.close();
      if (!Modifier.isAbstract(c.getModifiers())) {
        out = new PrintStream(
          new FileOutputStream("generated/"+getName(c)+"Impl.java"));
        generateImplementation(c);
        out.close();
      }
    }
  }

  private static void generateInterface (Class c) {
    String name = getName(c);
    out.println("// automatically generated");
    out.println();
    out.println("package "+packageName+";\n");
    out.print("public interface " + name + "Itf ");
    if (c.getSuperclass().equals(Object.class)) {
      out.println("{");
    } else {
      out.println("extends " + getName(c.getSuperclass()) + "Itf {");
    }
    Method[] methods = c.getDeclaredMethods();
    Class d = c.getSuperclass();
    for (int i = 0; i < methods.length; ++i) {
      if (Modifier.isPublic(methods[i].getModifiers())) {
        if (!Modifier.isFinal(methods[i].getModifiers())) {
          if (!Modifier.isStatic(methods[i].getModifiers())) {
            try {
              d.getMethod(methods[i].getName(),methods[i].getParameterTypes());
            } catch (NoSuchMethodException e) {
              generateMethodHeader(methods[i]);
              out.println(";");
            }
          }
        }
      }
    }
    out.println("}");
  }

  private static void generateTypeDescriptor (Class c) {
    String name = getName(c);
    String fullName = packageName + "." + name;
    out.println("<!-- skeleton component type automatically generated -->\n");
    out.print("<definition name=\"" + fullName + "Type\"");
    if (c.getSuperclass().equals(Object.class)) {
      out.println(">");
    } else {
      String superFullName = packageName + "." + getName(c.getSuperclass());
      out.println(" extends=\"" + superFullName + "Type\">");
    }
    out.println("  <interface name=\"server\" signature=\"" + fullName + "Itf\" role=\"server\"/>");
    out.println("  <!-- put your own interface types here -->");
    out.println("</definiton>");
  }

  private static void generateTemplateDescriptor (Class c) {
    String name = getName(c);
    String fullName = packageName + "." + name;
    out.println("<!-- skeleton template automatically generated -->\n");
    out.print("<definition name=\"" + fullName + "Impl\"");
    String extendsName = fullName + "Type";
    if (!c.getSuperclass().equals(Object.class) && !Modifier.isAbstract(c.getSuperclass().getModifiers())) {
      String superFullName = packageName + "." + getName(c.getSuperclass());
      extendsName = superFullName + "Impl," + extendsName;
    }
    out.println(" extends=\"" + extendsName + "\">");
    out.println("  <content class=\"" + fullName + "Impl\"/>");
    out.println("  <controller>");
    out.println("    <attributes signature=\"" + fullName + "Attributes\">");
    out.println("      <!-- put your own attribute definitions here -->");
    out.println("    </attributes>");
    out.println("  </controller>");
    out.println("</definition>");
  }

  private static void generateAttributes (Class c) {
    String name = getName(c);
    out.println("// automatically generated");
    out.println();
    out.println("package "+packageName+";\n");
    out.print("public interface " + name + "Attributes ");
    if (c.getSuperclass().equals(Object.class)) {
      out.println("extends org.objectweb.fractal.api.control.AttributeController {");
    } else {
      out.println("extends " + getName(c.getSuperclass()) + "Attributes {");
    }
    Method[] methods = c.getDeclaredMethods();
    //Class d = c.getSuperclass();
    for (int i = 0; i < methods.length; ++i) {
      String mname = methods[i].getName();
      Class type;
      if ((mname.startsWith("get") || mname.startsWith("set"))
          && Modifier.isPublic(methods[i].getModifiers())
          && !Modifier.isFinal(methods[i].getModifiers())
          && !Modifier.isStatic(methods[i].getModifiers()))
      {
        type = null;
        if (mname.startsWith("get")) {
          if (methods[i].getParameterTypes().length != 0) {
            continue;
          }
          type = methods[i].getReturnType();
        }
        if (mname.startsWith("set")) {
          if (methods[i].getReturnType() != Void.TYPE ||
              methods[i].getParameterTypes().length != 1)
          {
            continue;
          }
          type = methods[i].getParameterTypes()[0];
        }
        mname = (mname.charAt(0) == 'g' ? 's' : 'g') + mname.substring(1);
        boolean ok = false;
        for (int j = 0; j < methods.length; ++j) {
          if (methods[j].getName().equals(mname)) {
            if (Modifier.isPublic(methods[j].getModifiers())) {
              if (!Modifier.isFinal(methods[j].getModifiers())) {
                if (!Modifier.isStatic(methods[j].getModifiers())) {
                  Class otherType = null;
                  if (mname.charAt(0) == 'g') {
                    if (methods[j].getParameterTypes().length == 0) {
                      otherType = methods[j].getReturnType();
                    }
                  } else if (methods[j].getParameterTypes().length == 1 && methods[j].getReturnType() == Void.TYPE) {
                    otherType = methods[j].getParameterTypes()[0];
                  }
                  if (type == otherType) {
                    ok = true;
                    break;
                  }
                }
              }
            }
          }
        }
        if (ok && Modifier.isPublic(methods[i].getModifiers())) {
          if (!Modifier.isFinal(methods[i].getModifiers())) {
            if (!Modifier.isStatic(methods[i].getModifiers())) {
              /*try {
                d.getMethod(methods[i].getName(),methods[i].getParameterTypes());
              } catch (NoSuchMethodException e) {*/
                generateMethodHeader(methods[i]);
                out.println(";");
              //}
            }
          }
        }
      }
    }
    out.println("}");
  }

  private static void generateImplementation (Class c) {
    String name = getName(c);
    out.println("// skeleton class automatically generated");
    out.println();
    out.println("package "+packageName+";\n");
    out.println("import org.objectweb.fractal.api.control.BindingController;\n");
    out.println("public class " + name + "Impl");
    out.println("  extends " + c.getName());
    out.print("  implements " + name + "Itf, " + name);
    out.println("Attributes, BindingController");
    out.println("{\n");
    Constructor[] cons = c.getConstructors();
    for (int i = 0; i < cons.length; ++i) {
      generateConstructor(cons[i]);
    }
    out.println();
    out.println("  public String listFc () {");
    out.println("    // put your own code here");
    out.println("    return new String[0];");
    out.println("  }\n");
    out.println("  public Object lookupFc (String clientItfName) {");
    out.println("    // put your own code here");
    out.println("    return null;");
    out.println("  }\n");
    out.println("  public void bindFc (String clientItfName, Object serverItf) {");
    out.println("    // put your own code here");
    out.println("  }\n");
    out.println("  public void unbindFc (String clientItfName) {");
    out.println("    // put your own code here");
    out.println("  }\n");
    out.println("}");
  }

  private static void generateMethodHeader (Method m) {
    out.print("  ");
    generateType(m.getReturnType());
    out.print(" ");
    out.print(m.getName());
    out.print(" (");
    generateFormals(m.getParameterTypes());
    generateExceptions(m.getExceptionTypes());
  }

  private static void generateConstructor (Constructor c) {
    out.print("  public ");
    out.print(getName(c.getDeclaringClass()) + "Impl");
    out.print(" (");
    generateFormals(c.getParameterTypes());
    generateExceptions(c.getExceptionTypes());
    out.println(" {");
    out.print("    super(");
    generateActuals(c.getParameterTypes());
    out.println(");");
    out.println("  }");
  }

  private static void generateFormals (Class[] formals) {
    for (int i = 0; i < formals.length; ++i) {
      generateType(formals[i]);
      out.print(" arg" + i);
      if (i < formals.length - 1) {
        out.print(", ");
      }
    }
    out.print(")");
  }

  private static void generateActuals (Class[] actuals) {
    for (int i = 0; i < actuals.length; ++i) {
      out.print("arg" + i);
      if (i < actuals.length - 1) {
        out.print(",");
      }
    }
  }

  private static void generateExceptions (Class[] exceptions) {
    if (exceptions.length > 0) {
      out.print(" throws ");
      for (int i = 0; i < exceptions.length; ++i) {
        generateType(exceptions[i]);
        if (i < exceptions.length - 1) {
          out.print(", ");
        }
      }
    }
  }

  private static void generateType (Class c) {
    if (c.isArray()) {
      generateType(c.getComponentType());
      out.print("[]");
    } else if (c.isPrimitive()) {
      if (c.equals(Boolean.TYPE)) {
        out.print("boolean");
      } else if (c.equals(Character.TYPE)) {
        out.print("char");
      } else if (c.equals(Byte.TYPE)) {
        out.print("byte");
      } else if (c.equals(Short.TYPE)) {
        out.print("short");
      } else if (c.equals(Integer.TYPE)) {
        out.print("int");
      } else if (c.equals(Long.TYPE)) {
        out.print("long");
      } else if (c.equals(Float.TYPE)) {
        out.print("float");
      } else if (c.equals(Double.TYPE)) {
        out.print("double");
      } else if (c.equals(Void.TYPE)) {
        out.print("void");
      }
    } else {
      out.print(c.getName());
    }
  }

  private static String getName (Class c) {
    String name = c.getName();
    if (name.lastIndexOf('.') > 0) {
      name = name.substring(name.lastIndexOf('.') + 1);
    }
    return name;
  }
}
