// automatically generated

package org.objectweb.fractal.swing;


public interface JToolBarAttributes extends JComponentAttributes {
  int getOrientation ();
  void setUI (javax.swing.plaf.ToolBarUI arg0);
  javax.swing.plaf.ToolBarUI getUI ();
  void setOrientation (int arg0);
  void setMargin (java.awt.Insets arg0);
  java.awt.Insets getMargin ();
}
