// automatically generated

package org.objectweb.fractal.swing;


public interface JToolBarItf extends JComponentItf {
  javax.swing.JButton add (javax.swing.Action arg0);
  int getOrientation ();
  void addSeparator ();
  void addSeparator (java.awt.Dimension arg0);
  void setUI (javax.swing.plaf.ToolBarUI arg0);
  javax.swing.plaf.ToolBarUI getUI ();
  void setOrientation (int arg0);
  int getComponentIndex (java.awt.Component arg0);
  java.awt.Component getComponentAtIndex (int arg0);
  void setMargin (java.awt.Insets arg0);
  java.awt.Insets getMargin ();
  boolean isBorderPainted ();
  void setBorderPainted (boolean arg0);
  boolean isFloatable ();
  void setFloatable (boolean arg0);
}
