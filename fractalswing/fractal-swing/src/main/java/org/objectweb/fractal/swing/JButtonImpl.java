// skeleton class automatically generated

package org.objectweb.fractal.swing;

import org.objectweb.fractal.api.control.BindingController;

import java.util.HashMap;
import java.util.Map;
import java.awt.event.ActionListener;

public class JButtonImpl
  extends javax.swing.JButton
  implements JButtonItf, JButtonAttributes, BindingController
{

  // fields manually added
  public final static String ACTION_LISTENERS_BINDING = "action-listeners";
  private Map actionListeners = new HashMap();

  public JButtonImpl () {
    super();
  }
  public JButtonImpl (javax.swing.Icon arg0) {
    super(arg0);
  }
  public JButtonImpl (String arg0) {
    super(arg0);
  }
  public JButtonImpl (javax.swing.Action arg0) {
    super(arg0);
  }
  public JButtonImpl (String arg0, javax.swing.Icon arg1) {
    super(arg0,arg1);
  }

  public String[] listFc () {
    // put your own code here
    int size = actionListeners.size();
    return (String[])actionListeners.keySet().toArray(new String[size]);
  }

  public Object lookupFc (String clientItfName) {
    // put your own code here
    if (clientItfName.startsWith(ACTION_LISTENERS_BINDING)) {
      return actionListeners.get(clientItfName);
    }
    return null;
  }

  public void bindFc (String clientItfName, Object serverItf) {
    // put your own code here
    if (clientItfName.startsWith(ACTION_LISTENERS_BINDING)) {
      actionListeners.put(clientItfName, serverItf);
      super.addActionListener((java.awt.event.ActionListener)serverItf);
    }
  }

  public void unbindFc (String clientItfName) {
    // put your own code here
    if (clientItfName.startsWith(ACTION_LISTENERS_BINDING)) {
      ActionListener a = (ActionListener)actionListeners.get(clientItfName);
      super.removeActionListener(a);
    }
  }

}
