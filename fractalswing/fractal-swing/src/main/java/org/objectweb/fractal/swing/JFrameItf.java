// automatically generated

package org.objectweb.fractal.swing;


public interface JFrameItf extends FrameItf {
  java.awt.Component getGlassPane ();
  javax.swing.JRootPane getRootPane ();
  void setJMenuBar (javax.swing.JMenuBar arg0);
  javax.swing.JMenuBar getJMenuBar ();
  void setContentPane (java.awt.Container arg0);
  java.awt.Container getContentPane ();
  void setLayeredPane (javax.swing.JLayeredPane arg0);
  javax.swing.JLayeredPane getLayeredPane ();
  void setGlassPane (java.awt.Component arg0);
  void setDefaultCloseOperation (int arg0);
  int getDefaultCloseOperation ();
}
