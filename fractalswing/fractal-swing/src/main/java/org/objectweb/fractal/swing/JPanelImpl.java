// skeleton class automatically generated

package org.objectweb.fractal.swing;

import org.objectweb.fractal.api.control.BindingController;

import java.util.HashMap;
import java.util.Map;
import java.awt.Component;

import javax.swing.JComponent;

public class JPanelImpl
  extends javax.swing.JPanel
  implements JPanelItf, JPanelAttributes, BindingController
{

  // fields manually added
  public final static String LEFT_COMPONENT_BINDING = "left-component";
  public final static String TOP_COMPONENT_BINDING = "top-component";
  public final static String RIGHT_COMPONENT_BINDING = "right-component";
  public final static String BOTTOM_COMPONENT_BINDING = "bottom-component";
  public final static String CENTER_COMPONENT_BINDING = "center-component";
  public final static String COMPONENTS_BINDING = "components";
  public final static String LAYOUT_BINDING = "layout";
  private JComponent left, top, right, bottom, center;
  private Map components = new HashMap();
  private java.awt.LayoutManager layout;

  public JPanelImpl (java.awt.LayoutManager arg0, boolean arg1) {
    super(arg0,arg1);
  }
  public JPanelImpl (java.awt.LayoutManager arg0) {
    super(arg0);
  }
  public JPanelImpl (boolean arg0) {
    super(arg0);
  }
  public JPanelImpl () {
    super();
  }

  public String[] listFc () {
    int s = components.size();
    String[] result = (String[])components.keySet().toArray(new String[s + 6]);
    result[s]   = LEFT_COMPONENT_BINDING;
    result[s+1] = TOP_COMPONENT_BINDING;
    result[s+2] = RIGHT_COMPONENT_BINDING;
    result[s+3] = BOTTOM_COMPONENT_BINDING;
    result[s+4] = CENTER_COMPONENT_BINDING;
    result[s+5] = LAYOUT_BINDING;
    return result;
  }

  public Object lookupFc (String clientItfName) {
    // put your own code here
    if (clientItfName.equals(LEFT_COMPONENT_BINDING)) {
      return left;
    } if (clientItfName.equals(TOP_COMPONENT_BINDING)) {
      return top;
    } if (clientItfName.equals(RIGHT_COMPONENT_BINDING)) {
      return right;
    } if (clientItfName.equals(BOTTOM_COMPONENT_BINDING)) {
      return bottom;
    } if (clientItfName.equals(CENTER_COMPONENT_BINDING)) {
      return center;
    } else if (clientItfName.startsWith(COMPONENTS_BINDING)) {
      return components.get(clientItfName);
    } else if (clientItfName.equals(LAYOUT_BINDING)) {
      return layout;
    }
    return null;
  }

  public void bindFc (String clientItfName, Object serverItf) {
    // put your own code here
    if (clientItfName.equals(LEFT_COMPONENT_BINDING)) {
      left = (JComponent)serverItf;
      super.add(left, "West");
      validate();
      repaint();
    } if (clientItfName.equals(TOP_COMPONENT_BINDING)) {
      top = (JComponent)serverItf;
      super.add(top, "North");
      validate();
      repaint();
    } if (clientItfName.equals(RIGHT_COMPONENT_BINDING)) {
      right = (JComponent)serverItf;
      super.add(right, "East");
      validate();
      repaint();
    } if (clientItfName.equals(BOTTOM_COMPONENT_BINDING)) {
      bottom = (JComponent)serverItf;
      super.add(bottom, "South");
      validate();
      repaint();
    } if (clientItfName.equals(CENTER_COMPONENT_BINDING)) {
      center = (JComponent)serverItf;
      super.add(center, "Center");
      validate();
      repaint();
    } else if (clientItfName.startsWith(COMPONENTS_BINDING)) {
      components.put(clientItfName, serverItf);
      super.add((java.awt.Component)serverItf);
      validate();
      repaint();
    } else if (clientItfName.equals(LAYOUT_BINDING)) {
      layout = (java.awt.LayoutManager)serverItf;
      super.setLayout(layout);
      validate();
      repaint();
    }
  }

  public void unbindFc (String clientItfName) {
    // put your own code here
    if (clientItfName.equals(LEFT_COMPONENT_BINDING)) {
      super.remove(left);
      validate();
      repaint();
      left = null;
    } if (clientItfName.equals(TOP_COMPONENT_BINDING)) {
      super.remove(top);
      validate();
      repaint();
      top = null;
    } if (clientItfName.equals(RIGHT_COMPONENT_BINDING)) {
      super.remove(right);
      validate();
      repaint();
      right = null;
    } if (clientItfName.equals(BOTTOM_COMPONENT_BINDING)) {
      super.remove(bottom);
      validate();
      repaint();
      bottom = null;
    } if (clientItfName.equals(CENTER_COMPONENT_BINDING)) {
      super.remove(center);
      validate();
      repaint();
      center = null;
    } else if (clientItfName.startsWith(COMPONENTS_BINDING)) {
      Component c = (Component)components.get(clientItfName);
      super.remove(c);
      validate();
      repaint();
    } else if (clientItfName.equals(LAYOUT_BINDING)) {
      layout = null;
    }
  }

}
