// automatically generated

package org.objectweb.fractal.swing;

public interface JScrollPaneAttributes extends JComponentAttributes {
  void setUI (javax.swing.plaf.ScrollPaneUI arg0);
  javax.swing.plaf.ScrollPaneUI getUI ();
  int getVerticalScrollBarPolicy ();
  void setVerticalScrollBarPolicy (int arg0);
  int getHorizontalScrollBarPolicy ();
  void setHorizontalScrollBarPolicy (int arg0);
  javax.swing.border.Border getViewportBorder ();
  void setViewportBorder (javax.swing.border.Border arg0);
  javax.swing.JScrollBar getHorizontalScrollBar ();
  void setHorizontalScrollBar (javax.swing.JScrollBar arg0);
  javax.swing.JScrollBar getVerticalScrollBar ();
  void setVerticalScrollBar (javax.swing.JScrollBar arg0);
  javax.swing.JViewport getViewport ();
  void setViewport (javax.swing.JViewport arg0);
  javax.swing.JViewport getRowHeader ();
  void setRowHeader (javax.swing.JViewport arg0);
  javax.swing.JViewport getColumnHeader ();
  void setColumnHeader (javax.swing.JViewport arg0);
}
