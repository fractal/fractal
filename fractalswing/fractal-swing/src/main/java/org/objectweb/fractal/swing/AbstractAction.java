/**
 * copyright area
 */

package org.objectweb.fractal.swing;

import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

public class AbstractAction extends javax.swing.AbstractAction
  implements ActionAttributes
{

  private String iconURL;

  public String getName () {
    return (String)getValue(NAME);
  }

  public void setName (String name) {
    putValue(NAME, name);
  }

  public String getIconURL () {
    return iconURL;
  }

  public void setIconURL (String iconURL) {
    this.iconURL = iconURL;
    putValue(SMALL_ICON, new ImageIcon(getClass().getResource(iconURL)));
  }

  public String getToolTipText () {
    return (String)getValue(SHORT_DESCRIPTION);
  }

  public void setToolTipText (String toolTipText) {
    putValue(SHORT_DESCRIPTION, toolTipText);
  }

  public String getAcceleratorKey () {
    return (String)getValue(ACCELERATOR_KEY);
  }

  public void setAcceleratorKey (String acceleratorKey) {
    putValue(ACCELERATOR_KEY, acceleratorKey);
  }

  public void actionPerformed (ActionEvent e) {
  }
}
