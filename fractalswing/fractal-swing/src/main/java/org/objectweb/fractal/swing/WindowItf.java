// automatically generated

package org.objectweb.fractal.swing;


public interface WindowItf extends ContainerItf {
  void dispose ();
  java.awt.Component getFocusOwner ();
  void pack ();
  void toFront ();
  void toBack ();
  java.awt.Window getOwner ();
  java.awt.Window[] getOwnedWindows ();
  void addWindowListener (java.awt.event.WindowListener arg0);
  void removeWindowListener (java.awt.event.WindowListener arg0);
  void applyResourceBundle (java.util.ResourceBundle arg0);
  void applyResourceBundle (String arg0);
}
