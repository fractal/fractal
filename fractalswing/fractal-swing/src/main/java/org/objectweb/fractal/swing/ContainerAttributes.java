// automatically generated

package org.objectweb.fractal.swing;


public interface ContainerAttributes extends ComponentAttributes {
  java.awt.LayoutManager getLayout ();
  void setLayout (java.awt.LayoutManager arg0);
}
