// automatically generated

package org.objectweb.fractal.swing;

public interface JMenuItf extends JMenuItemItf {
  javax.swing.JMenuItem add (javax.swing.JMenuItem arg0);
  javax.swing.JMenuItem add (java.lang.String arg0);
  javax.swing.JMenuItem add (javax.swing.Action arg0);
  void remove (javax.swing.JMenuItem arg0);
  void insert (java.lang.String arg0, int arg1);
  javax.swing.JMenuItem insert (javax.swing.JMenuItem arg0, int arg1);
  javax.swing.JMenuItem insert (javax.swing.Action arg0, int arg1);
  void setDelay (int arg0);
  javax.swing.JMenuItem getItem (int arg0);
  int getItemCount ();
  boolean isTearOff ();
  void addSeparator ();
  void insertSeparator (int arg0);
  java.awt.Component[] getMenuComponents ();
  boolean isPopupMenuVisible ();
  void setPopupMenuVisible (boolean arg0);
  int getDelay ();
  void setMenuLocation (int arg0, int arg1);
  int getMenuComponentCount ();
  java.awt.Component getMenuComponent (int arg0);
  boolean isTopLevelMenu ();
  boolean isMenuComponent (java.awt.Component arg0);
  javax.swing.JPopupMenu getPopupMenu ();
  void addMenuListener (javax.swing.event.MenuListener arg0);
  void removeMenuListener (javax.swing.event.MenuListener arg0);
}
