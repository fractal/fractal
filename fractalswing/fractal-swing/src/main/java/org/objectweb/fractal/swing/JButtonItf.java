// automatically generated

package org.objectweb.fractal.swing;


public interface JButtonItf extends AbstractButtonItf {
  void setDefaultCapable (boolean arg0);
  boolean isDefaultButton ();
  boolean isDefaultCapable ();
}
