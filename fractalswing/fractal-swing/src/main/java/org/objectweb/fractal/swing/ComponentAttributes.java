// automatically generated

package org.objectweb.fractal.swing;

public interface ComponentAttributes extends org.objectweb.fractal.api.control.AttributeController {
  String getName ();
  void setName (String arg0);
  void setSize (java.awt.Dimension arg0);
  java.awt.Point getLocation ();
  void setDropTarget (java.awt.dnd.DropTarget arg0);
  java.awt.dnd.DropTarget getDropTarget ();
  java.awt.Color getForeground ();
  void setForeground (java.awt.Color arg0);
  java.awt.Color getBackground ();
  void setBackground (java.awt.Color arg0);
  java.awt.Font getFont ();
  void setFont (java.awt.Font arg0);
  java.util.Locale getLocale ();
  void setLocale (java.util.Locale arg0);
  void setLocation (java.awt.Point arg0);
  java.awt.Dimension getSize ();
  java.awt.Rectangle getBounds ();
  void setBounds (java.awt.Rectangle arg0);
  void setCursor (java.awt.Cursor arg0);
  java.awt.Cursor getCursor ();
  void setComponentOrientation (java.awt.ComponentOrientation arg0);
  java.awt.ComponentOrientation getComponentOrientation ();
}
