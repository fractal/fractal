// skeleton class automatically generated

package org.objectweb.fractal.swing;

import org.objectweb.fractal.api.control.BindingController;

public class FrameImpl
  extends java.awt.Frame
  implements FrameItf, FrameAttributes, BindingController
{

  public FrameImpl () {
    super();
  }
  public FrameImpl (java.awt.GraphicsConfiguration arg0) {
    super(arg0);
  }
  public FrameImpl (String arg0) {
    super(arg0);
  }
  public FrameImpl (String arg0, java.awt.GraphicsConfiguration arg1) {
    super(arg0,arg1);
  }

  public String[] listFc () {
    // put your own code here
    return new String[0];
  }

  public Object lookupFc (String clientItfName) {
    // put your own code here
    return null;
  }

  public void bindFc (String clientItfName, Object serverItf) {
    // put your own code here
  }

  public void unbindFc (String clientItfName) {
    // put your own code here
  }

}
