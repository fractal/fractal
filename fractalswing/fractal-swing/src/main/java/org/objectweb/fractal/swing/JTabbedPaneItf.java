// automatically generated

package org.objectweb.fractal.swing;

public interface JTabbedPaneItf extends JComponentItf {
  java.awt.Component getComponentAt (int arg0);
  void setUI (javax.swing.plaf.TabbedPaneUI arg0);
  javax.swing.plaf.TabbedPaneUI getUI ();
  void addChangeListener (javax.swing.event.ChangeListener arg0);
  void removeChangeListener (javax.swing.event.ChangeListener arg0);
  javax.swing.SingleSelectionModel getModel ();
  void setModel (javax.swing.SingleSelectionModel arg0);
  int getTabPlacement ();
  void setTabPlacement (int arg0);
  int getSelectedIndex ();
  void setSelectedIndex (int arg0);
  java.awt.Component getSelectedComponent ();
  void setSelectedComponent (java.awt.Component arg0);
  void insertTab (java.lang.String arg0, javax.swing.Icon arg1, java.awt.Component arg2, java.lang.String arg3, int arg4);
  void addTab (java.lang.String arg0, javax.swing.Icon arg1, java.awt.Component arg2, java.lang.String arg3);
  void addTab (java.lang.String arg0, javax.swing.Icon arg1, java.awt.Component arg2);
  void addTab (java.lang.String arg0, java.awt.Component arg1);
  void removeTabAt (int arg0);
  int getTabCount ();
  int getTabRunCount ();
  java.lang.String getTitleAt (int arg0);
  javax.swing.Icon getIconAt (int arg0);
  javax.swing.Icon getDisabledIconAt (int arg0);
  java.lang.String getToolTipTextAt (int arg0);
  java.awt.Color getBackgroundAt (int arg0);
  java.awt.Color getForegroundAt (int arg0);
  boolean isEnabledAt (int arg0);
  java.awt.Rectangle getBoundsAt (int arg0);
  void setTitleAt (int arg0, java.lang.String arg1);
  void setIconAt (int arg0, javax.swing.Icon arg1);
  void setDisabledIconAt (int arg0, javax.swing.Icon arg1);
  void setToolTipTextAt (int arg0, java.lang.String arg1);
  void setBackgroundAt (int arg0, java.awt.Color arg1);
  void setForegroundAt (int arg0, java.awt.Color arg1);
  void setEnabledAt (int arg0, boolean arg1);
  void setComponentAt (int arg0, java.awt.Component arg1);
  int indexOfTab (java.lang.String arg0);
  int indexOfTab (javax.swing.Icon arg0);
  int indexOfComponent (java.awt.Component arg0);
}
