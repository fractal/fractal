// skeleton class automatically generated

package org.objectweb.fractal.swing;

import org.objectweb.fractal.api.control.BindingController;

import java.util.Map;
import java.util.TreeMap;
import java.util.Iterator;

import javax.swing.JComponent;

public class JTabbedPaneImpl
  extends javax.swing.JTabbedPane
  implements JTabbedPaneItf, JTabbedPaneAttributes, BindingController
{

  // fields manually added
  public final static String TABS_BINDING = "tabs";
  private Map tabs = new TreeMap();

  public JTabbedPaneImpl () {
    super();
  }
  public JTabbedPaneImpl (int arg0) {
    super(arg0);
  }

  public String[] listFc () {
    // put your own code here
    return (String[])tabs.keySet().toArray(new String[tabs.size()]);
  }

  public Object lookupFc (String clientItfName) {
    // put your own code here
    if (clientItfName.startsWith(TABS_BINDING)) {
      return tabs.get(clientItfName);
    }
    return null;
  }

  public void bindFc (String clientItfName, Object serverItf) {
    // put your own code here
    if (clientItfName.startsWith(TABS_BINDING)) {
      tabs.put(clientItfName, serverItf);
      Iterator i = tabs.entrySet().iterator();
      while (i.hasNext()) {
        Map.Entry e = (Map.Entry)i.next();
        remove((java.awt.Component)e.getValue());
      }
      i = tabs.entrySet().iterator();
      while (i.hasNext()) {
        Map.Entry e = (Map.Entry)i.next();
        JComponent comp = (JComponent)e.getValue();
        addTab(comp.getName(), comp);
      }
    }
  }

  public void unbindFc (String clientItfName) {
    // put your own code here
    if (clientItfName.startsWith(TABS_BINDING)) {
      Object tab = tabs.remove(clientItfName);
      remove((java.awt.Component)tab);
    }
  }

}
