// automatically generated

package org.objectweb.fractal.swing;


public interface JTreeAttributes extends JComponentAttributes {
  void setUI (javax.swing.plaf.TreeUI arg0);
  javax.swing.plaf.TreeUI getUI ();
  void setModel (javax.swing.tree.TreeModel arg0);
  javax.swing.tree.TreeModel getModel ();
  javax.swing.tree.TreeCellRenderer getCellRenderer ();
  void setCellRenderer (javax.swing.tree.TreeCellRenderer arg0);
  void setCellEditor (javax.swing.tree.TreeCellEditor arg0);
  javax.swing.tree.TreeCellEditor getCellEditor ();
  void setShowsRootHandles (boolean arg0);
  boolean getShowsRootHandles ();
  void setRowHeight (int arg0);
  int getRowHeight ();
  void setInvokesStopCellEditing (boolean arg0);
  boolean getInvokesStopCellEditing ();
  void setScrollsOnExpand (boolean arg0);
  boolean getScrollsOnExpand ();
  void setToggleClickCount (int arg0);
  int getToggleClickCount ();
  void setExpandsSelectedPaths (boolean arg0);
  boolean getExpandsSelectedPaths ();
  void setSelectionPath (javax.swing.tree.TreePath arg0);
  void setSelectionPaths (javax.swing.tree.TreePath[] arg0);
  void setLeadSelectionPath (javax.swing.tree.TreePath arg0);
  void setAnchorSelectionPath (javax.swing.tree.TreePath arg0);
  void setSelectionRows (int[] arg0);
  javax.swing.tree.TreePath getLeadSelectionPath ();
  javax.swing.tree.TreePath getAnchorSelectionPath ();
  javax.swing.tree.TreePath getSelectionPath ();
  javax.swing.tree.TreePath[] getSelectionPaths ();
  int[] getSelectionRows ();
  void setSelectionModel (javax.swing.tree.TreeSelectionModel arg0);
  javax.swing.tree.TreeSelectionModel getSelectionModel ();
  void setVisibleRowCount (int arg0);
  int getVisibleRowCount ();
}
