// automatically generated

package org.objectweb.fractal.swing;


public interface JSplitPaneItf extends JComponentItf {
  int getOrientation ();
  void setUI (javax.swing.plaf.SplitPaneUI arg0);
  javax.swing.plaf.SplitPaneUI getUI ();
  void setDividerSize (int arg0);
  int getDividerSize ();
  void setLeftComponent (java.awt.Component arg0);
  java.awt.Component getLeftComponent ();
  void setTopComponent (java.awt.Component arg0);
  java.awt.Component getTopComponent ();
  void setRightComponent (java.awt.Component arg0);
  java.awt.Component getRightComponent ();
  void setBottomComponent (java.awt.Component arg0);
  java.awt.Component getBottomComponent ();
  void setOneTouchExpandable (boolean arg0);
  boolean isOneTouchExpandable ();
  void setLastDividerLocation (int arg0);
  int getLastDividerLocation ();
  void setOrientation (int arg0);
  void setContinuousLayout (boolean arg0);
  boolean isContinuousLayout ();
  void setResizeWeight (double arg0);
  double getResizeWeight ();
  void resetToPreferredSizes ();
  void setDividerLocation (double arg0);
  void setDividerLocation (int arg0);
  int getDividerLocation ();
  int getMinimumDividerLocation ();
  int getMaximumDividerLocation ();
}
