// automatically generated

package org.objectweb.fractal.swing;

public interface BorderLayoutItf {
  void addLayoutComponent (java.awt.Component arg0, Object arg1);
  void addLayoutComponent (String arg0, java.awt.Component arg1);
  void removeLayoutComponent (java.awt.Component arg0);
  void layoutContainer (java.awt.Container arg0);
  void invalidateLayout (java.awt.Container arg0);
  java.awt.Dimension preferredLayoutSize (java.awt.Container arg0);
  java.awt.Dimension minimumLayoutSize (java.awt.Container arg0);
  java.awt.Dimension maximumLayoutSize (java.awt.Container arg0);
  float getLayoutAlignmentX (java.awt.Container arg0);
  float getLayoutAlignmentY (java.awt.Container arg0);
  int getHgap ();
  void setHgap (int arg0);
  int getVgap ();
  void setVgap (int arg0);
}
