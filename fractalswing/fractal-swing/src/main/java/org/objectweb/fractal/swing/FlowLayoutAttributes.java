// automatically generated

package org.objectweb.fractal.swing;

public interface FlowLayoutAttributes extends org.objectweb.fractal.api.control.AttributeController {
  int getAlignment ();
  void setAlignment (int arg0);
  int getHgap ();
  void setHgap (int arg0);
  int getVgap ();
  void setVgap (int arg0);
}
