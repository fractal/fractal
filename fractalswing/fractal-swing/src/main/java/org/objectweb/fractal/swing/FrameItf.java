// automatically generated

package org.objectweb.fractal.swing;


public interface FrameItf extends WindowItf {
  void setCursor (int arg0);
  void setMenuBar (java.awt.MenuBar arg0);
  java.awt.MenuBar getMenuBar ();
  String getTitle ();
  void setTitle (String arg0);
  java.awt.Image getIconImage ();
  void setIconImage (java.awt.Image arg0);
  boolean isResizable ();
  void setResizable (boolean arg0);
  void setState (int arg0);
  int getState ();
  int getCursorType ();
}
