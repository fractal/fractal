// skeleton class automatically generated

package org.objectweb.fractal.swing;

import org.objectweb.fractal.api.control.BindingController;

import java.util.Map;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Iterator;

import javax.swing.Action;
import javax.swing.JComponent;

public class JToolBarImpl
  extends javax.swing.JToolBar
  implements JToolBarItf, JToolBarAttributes, BindingController
{

  // fields manually added
  public final static String ACTIONS_BINDING = "actions";
  private Map actions = new TreeMap();
  private Map components = new HashMap();

  public JToolBarImpl () {
    super();
  }
  public JToolBarImpl (int arg0) {
    super(arg0);
  }
  public JToolBarImpl (String arg0) {
    super(arg0);
  }
  public JToolBarImpl (String arg0, int arg1) {
    super(arg0,arg1);
  }

  public String[] listFc () {
    return (String[])actions.keySet().toArray(new String[actions.size()]);
  }

  public Object lookupFc (String clientItfName) {
    // put your own code here
    if (clientItfName.startsWith(ACTIONS_BINDING)) {
      return actions.get(clientItfName);
    }
    return null;
  }

  public void bindFc (String clientItfName, Object serverItf) {
    // put your own code here
    if (clientItfName.startsWith(ACTIONS_BINDING)) {
      removeAll();
      actions.put(clientItfName, serverItf);
      Iterator i = actions.entrySet().iterator();
      while (i.hasNext()) {
        Map.Entry e = (Map.Entry)i.next();
        Object o = e.getValue();
        if (o instanceof JSeparatorItf) {
          addSeparator();
        } else if (o instanceof JComponent) {
          add((JComponent)o);
        } else {
          add((Action)o);
        }
        components.put((String)e.getKey(), getComponent(getComponentCount()-1));
      }
    }
  }

  public void unbindFc (String clientItfName) {
    // put your own code here
    if (clientItfName.startsWith(ACTIONS_BINDING)) {
      actions.remove(clientItfName);
      remove((java.awt.Component)components.remove(clientItfName));
    }
  }

}
