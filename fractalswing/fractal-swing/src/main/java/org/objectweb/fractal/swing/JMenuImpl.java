// skeleton class automatically generated

package org.objectweb.fractal.swing;

import org.objectweb.fractal.api.control.BindingController;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.Iterator;

import javax.swing.JMenuItem;
import javax.swing.Action;
import javax.swing.KeyStroke;

public class JMenuImpl
  extends javax.swing.JMenu
  implements JMenuItf, JMenuAttributes, BindingController
{

  // fields manually added
  public final static String ITEMS_BINDING = "items";
  private Map actions = new TreeMap();
  private Map items = new HashMap();

  public JMenuImpl () {
    super();
  }
  public JMenuImpl (java.lang.String arg0) {
    super(arg0);
  }
  public JMenuImpl (javax.swing.Action arg0) {
    super(arg0);
  }
  public JMenuImpl (java.lang.String arg0, boolean arg1) {
    super(arg0,arg1);
  }

  public String[] listFc () {
    return (String[])actions.keySet().toArray(new String[actions.size()]);
  }

  public Object lookupFc (String clientItfName) {
    // put your own code here
    if (clientItfName.startsWith(ITEMS_BINDING)) {
      return actions.get(clientItfName);
    }
    return null;
  }

  public void bindFc (String clientItfName, Object serverItf) {
    // put your own code here
    if (clientItfName.startsWith(ITEMS_BINDING)) {
      removeAll();
      actions.put(clientItfName, serverItf);
      Iterator i = actions.entrySet().iterator();
      while (i.hasNext()) {
        Map.Entry e = (Map.Entry)i.next();
        Object o = e.getValue();
        if (o instanceof JSeparatorItf) {
          addSeparator();
        } else {
          add((Action)o);
        }
        items.put((String)e.getKey(), getItem(getItemCount()-1));
      }
    }
  }

  public void unbindFc (String clientItfName) {
    // put your own code here
    if (clientItfName.startsWith(ITEMS_BINDING)) {
      actions.remove(clientItfName);
      remove((JMenuItem)items.remove(clientItfName));
    }
  }

  // manually added
  public JMenuItem add (Action a) {
    JMenuItem item = super.add(a);
    KeyStroke stroke = (KeyStroke)a.getValue(Action.ACCELERATOR_KEY);
    if (stroke != null) {
      item.setAccelerator(stroke);
    }
    return item;
  }

}
