// automatically generated

package org.objectweb.fractal.swing;

public interface JMenuBarItf extends JComponentItf {
  javax.swing.JMenu add (javax.swing.JMenu arg0);
  void processKeyEvent (java.awt.event.KeyEvent arg0, javax.swing.MenuElement[] arg1, javax.swing.MenuSelectionManager arg2);
  void processMouseEvent (java.awt.event.MouseEvent arg0, javax.swing.MenuElement[] arg1, javax.swing.MenuSelectionManager arg2);
  java.awt.Component getComponent ();
  boolean isSelected ();
  void setUI (javax.swing.plaf.MenuBarUI arg0);
  javax.swing.plaf.MenuBarUI getUI ();
  int getComponentIndex (java.awt.Component arg0);
  java.awt.Component getComponentAtIndex (int arg0);
  void setMargin (java.awt.Insets arg0);
  java.awt.Insets getMargin ();
  boolean isBorderPainted ();
  void setBorderPainted (boolean arg0);
  void setSelected (java.awt.Component arg0);
  javax.swing.SingleSelectionModel getSelectionModel ();
  void setSelectionModel (javax.swing.SingleSelectionModel arg0);
  javax.swing.JMenu getMenu (int arg0);
  int getMenuCount ();
  void setHelpMenu (javax.swing.JMenu arg0);
  javax.swing.JMenu getHelpMenu ();
  void menuSelectionChanged (boolean arg0);
  javax.swing.MenuElement[] getSubElements ();
}
