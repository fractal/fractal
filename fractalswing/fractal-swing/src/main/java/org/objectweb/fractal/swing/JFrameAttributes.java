// automatically generated

package org.objectweb.fractal.swing;


public interface JFrameAttributes extends FrameAttributes {
  java.awt.Component getGlassPane ();
  void setJMenuBar (javax.swing.JMenuBar arg0);
  javax.swing.JMenuBar getJMenuBar ();
  void setContentPane (java.awt.Container arg0);
  java.awt.Container getContentPane ();
  void setLayeredPane (javax.swing.JLayeredPane arg0);
  javax.swing.JLayeredPane getLayeredPane ();
  void setGlassPane (java.awt.Component arg0);
  void setDefaultCloseOperation (int arg0);
  int getDefaultCloseOperation ();
  // manually added
  int getWidth ();
  void setWidth (int width);
  int getHeight ();
  void setHeight (int height);
}
