// automatically generated

package org.objectweb.fractal.swing;

public interface JMenuBarAttributes extends JComponentAttributes {
  void setUI (javax.swing.plaf.MenuBarUI arg0);
  javax.swing.plaf.MenuBarUI getUI ();
  void setMargin (java.awt.Insets arg0);
  java.awt.Insets getMargin ();
  javax.swing.SingleSelectionModel getSelectionModel ();
  void setSelectionModel (javax.swing.SingleSelectionModel arg0);
  void setHelpMenu (javax.swing.JMenu arg0);
  javax.swing.JMenu getHelpMenu ();
}
