// skeleton class automatically generated

package org.objectweb.fractal.swing;

import org.objectweb.fractal.api.control.BindingController;

public class JSplitPaneImpl
  extends javax.swing.JSplitPane
  implements JSplitPaneItf, JSplitPaneAttributes, BindingController
{

  // fields manually added
  public final static String LEFT_COMPONENT_BINDING = "left-component";
  public final static String RIGHT_COMPONENT_BINDING = "right-component";

  public JSplitPaneImpl () {
    super();
  }
  public JSplitPaneImpl (int arg0) {
    super(arg0);
  }
  public JSplitPaneImpl (int arg0, boolean arg1) {
    super(arg0,arg1);
  }
  public JSplitPaneImpl (int arg0, java.awt.Component arg1, java.awt.Component arg2) {
    super(arg0,arg1,arg2);
  }
  public JSplitPaneImpl (int arg0, boolean arg1, java.awt.Component arg2, java.awt.Component arg3) {
    super(arg0,arg1,arg2,arg3);
  }

  public String[] listFc () {
    // put your own code here
    return new String[] {
      LEFT_COMPONENT_BINDING,
      RIGHT_COMPONENT_BINDING
    };
  }

  public Object lookupFc (String clientItfName) {
    // put your own code here
    if (clientItfName.equals(LEFT_COMPONENT_BINDING)) {
      return super.getLeftComponent();
    } else if (clientItfName.equals(RIGHT_COMPONENT_BINDING)) {
      return super.getRightComponent();
    }
    return null;
  }

  public void bindFc (String clientItfName, Object serverItf) {
    // put your own code here
    if (clientItfName.equals(LEFT_COMPONENT_BINDING)) {
      super.setLeftComponent((java.awt.Component)serverItf);
    } else if (clientItfName.equals(RIGHT_COMPONENT_BINDING)) {
      super.setRightComponent((java.awt.Component)serverItf);
    }
  }

  public void unbindFc (String clientItfName) {
    // put your own code here
    if (clientItfName.equals(LEFT_COMPONENT_BINDING)) {
      super.setLeftComponent(null);
    } else if (clientItfName.equals(RIGHT_COMPONENT_BINDING)) {
      super.setRightComponent(null);
    }
  }

  // manually added
  public boolean getOneTouchExpandable () {
    return isOneTouchExpandable();
  }

}
