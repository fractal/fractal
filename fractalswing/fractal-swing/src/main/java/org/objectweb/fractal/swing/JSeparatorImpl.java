/**
 * copyright area
 */

package org.objectweb.fractal.swing;

import java.beans.PropertyChangeListener;
import java.awt.event.ActionEvent;

public class JSeparatorImpl implements JSeparatorItf {

  public Object getValue (String key) {
    return null;
  }

  public void putValue (String key, Object value) {
  }

  public void setEnabled (boolean b) {
  }

  public boolean isEnabled () {
    return false;
  }

  public void addPropertyChangeListener (PropertyChangeListener listener) {
  }

  public void removePropertyChangeListener (PropertyChangeListener listener) {
  }

  public void actionPerformed (ActionEvent e) {
  }
}
