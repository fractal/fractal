// skeleton class automatically generated

package org.objectweb.fractal.swing;

import org.objectweb.fractal.api.control.BindingController;

public class JMenuItemImpl
  extends javax.swing.JMenuItem
  implements JMenuItemItf, JMenuItemAttributes, BindingController
{

  public JMenuItemImpl () {
    super();
  }
  public JMenuItemImpl (javax.swing.Icon arg0) {
    super(arg0);
  }
  public JMenuItemImpl (java.lang.String arg0) {
    super(arg0);
  }
  public JMenuItemImpl (javax.swing.Action arg0) {
    super(arg0);
  }
  public JMenuItemImpl (java.lang.String arg0, javax.swing.Icon arg1) {
    super(arg0,arg1);
  }
  public JMenuItemImpl (java.lang.String arg0, int arg1) {
    super(arg0,arg1);
  }

  public String[] listFc () {
    // put your own code here
    return new String[0];
  }

  public Object lookupFc (String clientItfName) {
    // put your own code here
    return null;
  }

  public void bindFc (String clientItfName, Object serverItf) {
    // put your own code here
  }

  public void unbindFc (String clientItfName) {
    // put your own code here
  }

}
