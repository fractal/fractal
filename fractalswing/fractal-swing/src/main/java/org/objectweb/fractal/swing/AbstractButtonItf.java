// automatically generated

package org.objectweb.fractal.swing;


public interface AbstractButtonItf extends JComponentItf {
  String getLabel ();
  String getActionCommand ();
  String getText ();
  void setLabel (String arg0);
  void setActionCommand (String arg0);
  void addActionListener (java.awt.event.ActionListener arg0);
  void removeActionListener (java.awt.event.ActionListener arg0);
  void setUI (javax.swing.plaf.ButtonUI arg0);
  javax.swing.plaf.ButtonUI getUI ();
  void setMargin (java.awt.Insets arg0);
  java.awt.Insets getMargin ();
  boolean isBorderPainted ();
  void setBorderPainted (boolean arg0);
  void setAction (javax.swing.Action arg0);
  void setHorizontalTextPosition (int arg0);
  void setVerticalTextPosition (int arg0);
  void setModel (javax.swing.ButtonModel arg0);
  void setText (String arg0);
  void setIcon (javax.swing.Icon arg0);
  boolean isSelected ();
  void setSelected (boolean arg0);
  void doClick ();
  void doClick (int arg0);
  javax.swing.Icon getIcon ();
  javax.swing.Icon getPressedIcon ();
  void setPressedIcon (javax.swing.Icon arg0);
  javax.swing.Icon getSelectedIcon ();
  void setSelectedIcon (javax.swing.Icon arg0);
  javax.swing.Icon getRolloverIcon ();
  void setRolloverIcon (javax.swing.Icon arg0);
  javax.swing.Icon getRolloverSelectedIcon ();
  void setRolloverSelectedIcon (javax.swing.Icon arg0);
  javax.swing.Icon getDisabledIcon ();
  void setDisabledIcon (javax.swing.Icon arg0);
  javax.swing.Icon getDisabledSelectedIcon ();
  void setDisabledSelectedIcon (javax.swing.Icon arg0);
  int getVerticalAlignment ();
  void setVerticalAlignment (int arg0);
  int getHorizontalAlignment ();
  void setHorizontalAlignment (int arg0);
  int getVerticalTextPosition ();
  int getHorizontalTextPosition ();
  javax.swing.Action getAction ();
  boolean isFocusPainted ();
  void setFocusPainted (boolean arg0);
  boolean isContentAreaFilled ();
  void setContentAreaFilled (boolean arg0);
  boolean isRolloverEnabled ();
  void setRolloverEnabled (boolean arg0);
  int getMnemonic ();
  void setMnemonic (int arg0);
  void setMnemonic (char arg0);
  javax.swing.ButtonModel getModel ();
  void addChangeListener (javax.swing.event.ChangeListener arg0);
  void removeChangeListener (javax.swing.event.ChangeListener arg0);
  void addItemListener (java.awt.event.ItemListener arg0);
  void removeItemListener (java.awt.event.ItemListener arg0);
  Object[] getSelectedObjects ();
}
