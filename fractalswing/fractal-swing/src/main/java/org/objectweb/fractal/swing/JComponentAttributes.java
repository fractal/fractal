// automatically generated

package org.objectweb.fractal.swing;


public interface JComponentAttributes extends ContainerAttributes {
  java.awt.Dimension getPreferredSize ();
  java.awt.Dimension getMinimumSize ();
  java.awt.Dimension getMaximumSize ();
  float getAlignmentX ();
  float getAlignmentY ();
  void setNextFocusableComponent (java.awt.Component arg0);
  java.awt.Component getNextFocusableComponent ();
  void setVerifyInputWhenFocusTarget (boolean arg0);
  boolean getVerifyInputWhenFocusTarget ();
  void setPreferredSize (java.awt.Dimension arg0);
  void setMaximumSize (java.awt.Dimension arg0);
  void setMinimumSize (java.awt.Dimension arg0);
  void setBorder (javax.swing.border.Border arg0);
  javax.swing.border.Border getBorder ();
  void setAlignmentY (float arg0);
  void setAlignmentX (float arg0);
  void setInputVerifier (javax.swing.InputVerifier arg0);
  javax.swing.InputVerifier getInputVerifier ();
  void setDebugGraphicsOptions (int arg0);
  int getDebugGraphicsOptions ();
  void setToolTipText (String arg0);
  String getToolTipText ();
  void setAutoscrolls (boolean arg0);
  boolean getAutoscrolls ();
}
