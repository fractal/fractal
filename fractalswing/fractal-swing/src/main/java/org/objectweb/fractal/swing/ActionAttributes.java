/**
 * copyright area
 */

package org.objectweb.fractal.swing;

import org.objectweb.fractal.api.control.AttributeController;

public interface ActionAttributes extends AttributeController {

  String getName ();

  void setName (String name);

  String getIconURL ();

  void setIconURL (String iconURL);

  String getToolTipText ();

  void setToolTipText (String toolTipText);

  String getAcceleratorKey ();

  void setAcceleratorKey (String acceleratorKey);
}
