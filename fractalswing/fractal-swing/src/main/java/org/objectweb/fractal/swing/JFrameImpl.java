// skeleton class automatically generated

package org.objectweb.fractal.swing;

import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.BindingController;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JMenuBar;

public class JFrameImpl
  extends javax.swing.JFrame
  implements JFrameItf, JFrameAttributes, BindingController, LifeCycleController
{

  // fields manually added
  public final static String CONTENT_PANE_BINDING = "content-pane";
  public final static String MENU_BAR_BINDING = "menu-bar";
  private java.awt.Container contentPane;

  public JFrameImpl () {
    super();
    // manually added
    addWindowListener(
      new WindowAdapter() {
        public void windowClosing (WindowEvent e) {
          System.exit(0);
        }
      });
  }
  public JFrameImpl (java.awt.GraphicsConfiguration arg0) {
    super(arg0);
  }
  public JFrameImpl (String arg0) {
    super(arg0);
  }
  public JFrameImpl (String arg0, java.awt.GraphicsConfiguration arg1) {
    super(arg0,arg1);
  }

  public String[] listFc () {
    // put your own code here
    return new String[] {
      CONTENT_PANE_BINDING,
      MENU_BAR_BINDING
    };
  }

  public Object lookupFc (String clientItfName) {
    // put your own code here
    if (clientItfName.equals(CONTENT_PANE_BINDING)) {
      return contentPane;
    } else if (clientItfName.equals(MENU_BAR_BINDING)) {
      return getJMenuBar();
    }
    return null;
  }

  public void bindFc (String clientItfName, Object serverItf) {
    // put your own code here
    if (clientItfName.equals(CONTENT_PANE_BINDING)) {
      contentPane = (java.awt.Container)serverItf;
      super.setContentPane(contentPane);
    } else if (clientItfName.equals(MENU_BAR_BINDING)) {
      super.setJMenuBar((JMenuBar)serverItf);
    }
  }

  public void unbindFc (String clientItfName) {
    // put your own code here
    if (clientItfName.equals(CONTENT_PANE_BINDING)) {
      contentPane = null;
      super.setContentPane(new java.awt.Container());
    } else if (clientItfName.equals(MENU_BAR_BINDING)) {
      super.setJMenuBar(null);
    }
  }

  // methods manually added

  public void setWidth (int width) {
    setSize(width, getHeight());
  }

  public void setHeight (int height) {
    setSize(getWidth(), height);
  }

  public String getFcState () {
    return null;
  }

  public void startFc () {
    show();
  }

  public void stopFc () {
  }

}
