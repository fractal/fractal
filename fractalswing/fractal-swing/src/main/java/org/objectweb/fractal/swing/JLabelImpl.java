// skeleton class automatically generated

package org.objectweb.fractal.swing;

import org.objectweb.fractal.api.control.BindingController;

public class JLabelImpl
  extends javax.swing.JLabel
  implements JLabelItf, JLabelAttributes, BindingController
{

  public JLabelImpl (String arg0, javax.swing.Icon arg1, int arg2) {
    super(arg0,arg1,arg2);
  }
  public JLabelImpl (String arg0, int arg1) {
    super(arg0,arg1);
  }
  public JLabelImpl (String arg0) {
    super(arg0);
  }
  public JLabelImpl (javax.swing.Icon arg0, int arg1) {
    super(arg0,arg1);
  }
  public JLabelImpl (javax.swing.Icon arg0) {
    super(arg0);
  }
  public JLabelImpl () {
    super();
  }

  public String[] listFc () {
    // put your own code here
    return new String[0];
  }

  public Object lookupFc (String clientItfName) {
    // put your own code here
    return null;
  }

  public void bindFc (String clientItfName, Object serverItf) {
    // put your own code here
  }

  public void unbindFc (String clientItfName) {
    // put your own code here
  }

}
