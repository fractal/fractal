// automatically generated

package org.objectweb.fractal.swing;

public interface BorderLayoutAttributes extends org.objectweb.fractal.api.control.AttributeController {
  int getHgap ();
  void setHgap (int arg0);
  int getVgap ();
  void setVgap (int arg0);
}
