// automatically generated

package org.objectweb.fractal.swing;


public interface JLabelAttributes extends JComponentAttributes {
  String getText ();
  void setUI (javax.swing.plaf.LabelUI arg0);
  javax.swing.plaf.LabelUI getUI ();
  void setHorizontalTextPosition (int arg0);
  void setVerticalTextPosition (int arg0);
  void setText (String arg0);
  void setIcon (javax.swing.Icon arg0);
  javax.swing.Icon getIcon ();
  javax.swing.Icon getDisabledIcon ();
  void setDisabledIcon (javax.swing.Icon arg0);
  int getVerticalAlignment ();
  void setVerticalAlignment (int arg0);
  int getHorizontalAlignment ();
  void setHorizontalAlignment (int arg0);
  int getVerticalTextPosition ();
  int getHorizontalTextPosition ();
  void setDisplayedMnemonic (int arg0);
  int getDisplayedMnemonic ();
  int getIconTextGap ();
  void setIconTextGap (int arg0);
  java.awt.Component getLabelFor ();
  void setLabelFor (java.awt.Component arg0);
}
