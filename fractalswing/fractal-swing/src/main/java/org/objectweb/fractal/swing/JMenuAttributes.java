// automatically generated

package org.objectweb.fractal.swing;

public interface JMenuAttributes extends JMenuItemAttributes {
  void setDelay (int arg0);
  int getDelay ();
}
