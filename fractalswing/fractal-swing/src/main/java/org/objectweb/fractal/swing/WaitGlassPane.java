/**
 * copyright area
 */

package org.objectweb.fractal.swing;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Cursor;

import javax.swing.JComponent;

public class WaitGlassPane extends JComponent {

  public WaitGlassPane () {
    addMouseListener(new MouseAdapter() {
      public void mouseEntered (MouseEvent e) {
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
      }
    });
  }
}
