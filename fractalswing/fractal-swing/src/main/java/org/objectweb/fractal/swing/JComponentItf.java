// automatically generated

package org.objectweb.fractal.swing;


public interface JComponentItf extends ContainerItf {
  void repaint (java.awt.Rectangle arg0);
  void firePropertyChange (String arg0, byte arg1, byte arg2);
  void firePropertyChange (String arg0, char arg1, char arg2);
  void firePropertyChange (String arg0, short arg1, short arg2);
  void firePropertyChange (String arg0, int arg1, int arg2);
  void firePropertyChange (String arg0, long arg1, long arg2);
  void firePropertyChange (String arg0, float arg1, float arg2);
  void firePropertyChange (String arg0, double arg1, double arg2);
  void firePropertyChange (String arg0, boolean arg1, boolean arg2);
  java.awt.Insets getInsets (java.awt.Insets arg0);
  void updateUI ();
  String getUIClassID ();
  boolean isPaintingTile ();
  boolean isFocusCycleRoot ();
  boolean isManagingFocus ();
  void setNextFocusableComponent (java.awt.Component arg0);
  java.awt.Component getNextFocusableComponent ();
  void setRequestFocusEnabled (boolean arg0);
  boolean isRequestFocusEnabled ();
  void grabFocus ();
  void setVerifyInputWhenFocusTarget (boolean arg0);
  boolean getVerifyInputWhenFocusTarget ();
  void setPreferredSize (java.awt.Dimension arg0);
  void setMaximumSize (java.awt.Dimension arg0);
  void setMinimumSize (java.awt.Dimension arg0);
  boolean isMinimumSizeSet ();
  boolean isPreferredSizeSet ();
  boolean isMaximumSizeSet ();
  void setBorder (javax.swing.border.Border arg0);
  javax.swing.border.Border getBorder ();
  void setAlignmentY (float arg0);
  void setAlignmentX (float arg0);
  void setInputVerifier (javax.swing.InputVerifier arg0);
  javax.swing.InputVerifier getInputVerifier ();
  void setDebugGraphicsOptions (int arg0);
  int getDebugGraphicsOptions ();
  void registerKeyboardAction (java.awt.event.ActionListener arg0, String arg1, javax.swing.KeyStroke arg2, int arg3);
  void registerKeyboardAction (java.awt.event.ActionListener arg0, javax.swing.KeyStroke arg1, int arg2);
  void unregisterKeyboardAction (javax.swing.KeyStroke arg0);
  javax.swing.KeyStroke[] getRegisteredKeyStrokes ();
  int getConditionForKeyStroke (javax.swing.KeyStroke arg0);
  java.awt.event.ActionListener getActionForKeyStroke (javax.swing.KeyStroke arg0);
  void resetKeyboardActions ();
  boolean requestDefaultFocus ();
  void setToolTipText (String arg0);
  String getToolTipText ();
  String getToolTipText (java.awt.event.MouseEvent arg0);
  java.awt.Point getToolTipLocation (java.awt.event.MouseEvent arg0);
  javax.swing.JToolTip createToolTip ();
  void scrollRectToVisible (java.awt.Rectangle arg0);
  void setAutoscrolls (boolean arg0);
  boolean getAutoscrolls ();
  void setOpaque (boolean arg0);
  void computeVisibleRect (java.awt.Rectangle arg0);
  java.awt.Rectangle getVisibleRect ();
  void addVetoableChangeListener (java.beans.VetoableChangeListener arg0);
  void removeVetoableChangeListener (java.beans.VetoableChangeListener arg0);
  java.awt.Container getTopLevelAncestor ();
  void addAncestorListener (javax.swing.event.AncestorListener arg0);
  void removeAncestorListener (javax.swing.event.AncestorListener arg0);
  void revalidate ();
  boolean isValidateRoot ();
  boolean isOptimizedDrawingEnabled ();
  void paintImmediately (int arg0, int arg1, int arg2, int arg3);
  void paintImmediately (java.awt.Rectangle arg0);
  void setDoubleBuffered (boolean arg0);
  javax.swing.JRootPane getRootPane ();
}
