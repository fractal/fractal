// automatically generated

package org.objectweb.fractal.swing;

public interface JMenuItemItf extends AbstractButtonItf {
  void processKeyEvent (java.awt.event.KeyEvent arg0, javax.swing.MenuElement[] arg1, javax.swing.MenuSelectionManager arg2);
  void processMouseEvent (java.awt.event.MouseEvent arg0, javax.swing.MenuElement[] arg1, javax.swing.MenuSelectionManager arg2);
  java.awt.Component getComponent ();
  void setUI (javax.swing.plaf.MenuItemUI arg0);
  void setArmed (boolean arg0);
  void menuSelectionChanged (boolean arg0);
  javax.swing.MenuElement[] getSubElements ();
  void setAccelerator (javax.swing.KeyStroke arg0);
  boolean isArmed ();
  javax.swing.KeyStroke getAccelerator ();
  void processMenuDragMouseEvent (javax.swing.event.MenuDragMouseEvent arg0);
  void processMenuKeyEvent (javax.swing.event.MenuKeyEvent arg0);
  void addMenuDragMouseListener (javax.swing.event.MenuDragMouseListener arg0);
  void removeMenuDragMouseListener (javax.swing.event.MenuDragMouseListener arg0);
  void addMenuKeyListener (javax.swing.event.MenuKeyListener arg0);
  void removeMenuKeyListener (javax.swing.event.MenuKeyListener arg0);
}
