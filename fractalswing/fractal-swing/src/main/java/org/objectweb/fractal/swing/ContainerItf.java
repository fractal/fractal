// automatically generated

package org.objectweb.fractal.swing;


public interface ContainerItf extends ComponentItf {
  java.awt.Component add (java.awt.Component arg0);
  java.awt.Component add (String arg0, java.awt.Component arg1);
  java.awt.Component add (java.awt.Component arg0, int arg1);
  void add (java.awt.Component arg0, Object arg1);
  void add (java.awt.Component arg0, Object arg1, int arg2);
  void remove (int arg0);
  void remove (java.awt.Component arg0);
  void removeAll ();
  java.awt.Component[] getComponents ();
  int getComponentCount ();
  int countComponents ();
  java.awt.Component getComponent (int arg0);
  java.awt.Insets getInsets ();
  java.awt.Insets insets ();
  java.awt.LayoutManager getLayout ();
  void setLayout (java.awt.LayoutManager arg0);
  void paintComponents (java.awt.Graphics arg0);
  void printComponents (java.awt.Graphics arg0);
  void addContainerListener (java.awt.event.ContainerListener arg0);
  void removeContainerListener (java.awt.event.ContainerListener arg0);
  java.awt.Component findComponentAt (int arg0, int arg1);
  java.awt.Component findComponentAt (java.awt.Point arg0);
  boolean isAncestorOf (java.awt.Component arg0);
}
