// skeleton class automatically generated

package org.objectweb.fractal.swing;

import org.objectweb.fractal.api.control.BindingController;

public class FlowLayoutImpl
  extends java.awt.FlowLayout
  implements FlowLayoutItf, FlowLayoutAttributes, BindingController
{

  public FlowLayoutImpl () {
    super();
  }
  public FlowLayoutImpl (int arg0) {
    super(arg0);
  }
  public FlowLayoutImpl (int arg0, int arg1, int arg2) {
    super(arg0,arg1,arg2);
  }

  public String[] listFc () {
    // put your own code here
    return new String[0];
  }

  public Object lookupFc (String clientItfName) {
    // put your own code here
    return null;
  }

  public void bindFc (String clientItfName, Object serverItf) {
    // put your own code here
  }

  public void unbindFc (String clientItfName) {
    // put your own code here
  }

}
