// automatically generated

package org.objectweb.fractal.swing;

public interface JTabbedPaneAttributes extends JComponentAttributes {
  void setUI (javax.swing.plaf.TabbedPaneUI arg0);
  javax.swing.plaf.TabbedPaneUI getUI ();
  javax.swing.SingleSelectionModel getModel ();
  void setModel (javax.swing.SingleSelectionModel arg0);
  int getTabPlacement ();
  void setTabPlacement (int arg0);
  int getSelectedIndex ();
  void setSelectedIndex (int arg0);
  java.awt.Component getSelectedComponent ();
  void setSelectedComponent (java.awt.Component arg0);
}
