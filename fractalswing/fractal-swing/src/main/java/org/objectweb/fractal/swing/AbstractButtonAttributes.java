// automatically generated

package org.objectweb.fractal.swing;


public interface AbstractButtonAttributes extends JComponentAttributes {
  String getLabel ();
  String getActionCommand ();
  String getText ();
  void setLabel (String arg0);
  void setActionCommand (String arg0);
  void setUI (javax.swing.plaf.ButtonUI arg0);
  javax.swing.plaf.ButtonUI getUI ();
  void setMargin (java.awt.Insets arg0);
  java.awt.Insets getMargin ();
  void setAction (javax.swing.Action arg0);
  void setHorizontalTextPosition (int arg0);
  void setVerticalTextPosition (int arg0);
  void setModel (javax.swing.ButtonModel arg0);
  void setText (String arg0);
  void setIcon (javax.swing.Icon arg0);
  javax.swing.Icon getIcon ();
  javax.swing.Icon getPressedIcon ();
  void setPressedIcon (javax.swing.Icon arg0);
  javax.swing.Icon getSelectedIcon ();
  void setSelectedIcon (javax.swing.Icon arg0);
  javax.swing.Icon getRolloverIcon ();
  void setRolloverIcon (javax.swing.Icon arg0);
  javax.swing.Icon getRolloverSelectedIcon ();
  void setRolloverSelectedIcon (javax.swing.Icon arg0);
  javax.swing.Icon getDisabledIcon ();
  void setDisabledIcon (javax.swing.Icon arg0);
  javax.swing.Icon getDisabledSelectedIcon ();
  void setDisabledSelectedIcon (javax.swing.Icon arg0);
  int getVerticalAlignment ();
  void setVerticalAlignment (int arg0);
  int getHorizontalAlignment ();
  void setHorizontalAlignment (int arg0);
  int getVerticalTextPosition ();
  int getHorizontalTextPosition ();
  javax.swing.Action getAction ();
  int getMnemonic ();
  void setMnemonic (int arg0);
  javax.swing.ButtonModel getModel ();
}
