// skeleton class automatically generated

package org.objectweb.fractal.swing;

import org.objectweb.fractal.api.control.BindingController;

import java.util.Map;
import java.util.TreeMap;
import java.util.Iterator;

import javax.swing.JMenu;

public class JMenuBarImpl
  extends javax.swing.JMenuBar
  implements JMenuBarItf, JMenuBarAttributes, BindingController
{

  // fields manually added
  private final static String MENUS_BINDING = "menus";
  private final Map menus = new TreeMap();

  public JMenuBarImpl () {
    super();
  }

  public String[] listFc () {
    // put your own code here
    return (String[])menus.keySet().toArray(new String[menus.size()]);
  }

  public Object lookupFc (String clientItfName) {
    // put your own code here
    if (clientItfName.startsWith(MENUS_BINDING)) {
      return menus.get(clientItfName);
    }
    return null;
  }

  public void bindFc (String clientItfName, Object serverItf) {
    // put your own code here
    if (clientItfName.startsWith(MENUS_BINDING)) {
      menus.put(clientItfName, serverItf);
      Iterator i = menus.values().iterator();
      while (i.hasNext()) {
        super.remove((JMenu)i.next());
      }
      i = menus.values().iterator();
      while (i.hasNext()) {
        super.add((JMenu)i.next());
      }
    }
  }

  public void unbindFc (String clientItfName) {
    // put your own code here
    if (clientItfName.startsWith(MENUS_BINDING)) {
      Object serverItf = menus.remove(clientItfName);
      super.remove((JMenu)serverItf);
    }
  }

}
