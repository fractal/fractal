// automatically generated

package org.objectweb.fractal.swing;


public interface JSplitPaneAttributes extends JComponentAttributes {
  int getOrientation ();
  void setUI (javax.swing.plaf.SplitPaneUI arg0);
  javax.swing.plaf.SplitPaneUI getUI ();
  void setDividerSize (int arg0);
  int getDividerSize ();
  void setLeftComponent (java.awt.Component arg0);
  java.awt.Component getLeftComponent ();
  void setTopComponent (java.awt.Component arg0);
  java.awt.Component getTopComponent ();
  void setRightComponent (java.awt.Component arg0);
  java.awt.Component getRightComponent ();
  void setBottomComponent (java.awt.Component arg0);
  java.awt.Component getBottomComponent ();
  void setLastDividerLocation (int arg0);
  int getLastDividerLocation ();
  void setOrientation (int arg0);
  void setResizeWeight (double arg0);
  double getResizeWeight ();
  void setDividerLocation (int arg0);
  int getDividerLocation ();

  // manually added
  boolean getOneTouchExpandable ();
  void setOneTouchExpandable (boolean arg0);
}
