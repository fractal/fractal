// skeleton class automatically generated

package org.objectweb.fractal.swing;

import org.objectweb.fractal.api.control.BindingController;

import javax.swing.JComponent;

public class JScrollPaneImpl
  extends javax.swing.JScrollPane
  implements JScrollPaneItf, JScrollPaneAttributes, BindingController
{

  public final static String VIEW_BINDING = "view";
  private JComponent view;

  public JScrollPaneImpl (java.awt.Component arg0, int arg1, int arg2) {
    super(arg0,arg1,arg2);
  }
  public JScrollPaneImpl (java.awt.Component arg0) {
    super(arg0);
  }
  public JScrollPaneImpl (int arg0, int arg1) {
    super(arg0,arg1);
  }
  public JScrollPaneImpl () {
    super();
  }

  public String[] listFc () {
    // put your own code here
    return new String[] { VIEW_BINDING };
  }

  public Object lookupFc (String clientItfName) {
    // put your own code here
    if (clientItfName.equals(VIEW_BINDING)) {
      return view;
    }
    return null;
  }

  public void bindFc (String clientItfName, Object serverItf) {
    // put your own code here
    if (clientItfName.equals(VIEW_BINDING)) {
      this.view = (JComponent)serverItf;
      setViewportView(view);
    }
  }

  public void unbindFc (String clientItfName) {
    // put your own code here
    if (clientItfName.equals(VIEW_BINDING)) {
      this.view = null;
    }
  }

}
