// skeleton class automatically generated

package org.objectweb.fractal.swing;

import org.objectweb.fractal.api.control.BindingController;

import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeSelectionModel;
import javax.swing.tree.TreeCellRenderer;

public class JTreeImpl
  extends javax.swing.JTree
  implements JTreeItf, JTreeAttributes, BindingController
{

  // fields manually added
  public final static String TREE_MODEL_BINDING = "tree-model";
  public final static String TREE_SELECTION_MODEL_BINDING = "tree-selection-model";
  public final static String TREE_CELL_RENDERER_BINDING = "tree-cell-renderer";

  public JTreeImpl () {
    super();
  }
  public JTreeImpl (Object[] arg0) {
    super(arg0);
  }
  public JTreeImpl (java.util.Vector arg0) {
    super(arg0);
  }
  public JTreeImpl (java.util.Hashtable arg0) {
    super(arg0);
  }
  public JTreeImpl (javax.swing.tree.TreeNode arg0) {
    super(arg0);
  }
  public JTreeImpl (javax.swing.tree.TreeNode arg0, boolean arg1) {
    super(arg0,arg1);
  }
  public JTreeImpl (javax.swing.tree.TreeModel arg0) {
    super(arg0);
  }

  public String[] listFc () {
    // put your own code here
    return new String[] {
      TREE_MODEL_BINDING,
      TREE_SELECTION_MODEL_BINDING,
      TREE_CELL_RENDERER_BINDING
    };
  }

  public Object lookupFc (String clientItfName) {
    // put your own code here
    if (clientItfName.equals(TREE_MODEL_BINDING)) {
      return getModel();
    } else if (clientItfName.equals(TREE_SELECTION_MODEL_BINDING)) {
      return getSelectionModel();
    } else if (clientItfName.equals(TREE_CELL_RENDERER_BINDING)) {
      return getCellRenderer();
    }
    return null;
  }

  public void bindFc (String clientItfName, Object serverItf) {
    // put your own code here
    if (clientItfName.equals(TREE_MODEL_BINDING)) {
      setModel((TreeModel)serverItf);
    } else if (clientItfName.equals(TREE_SELECTION_MODEL_BINDING)) {
      setSelectionModel((TreeSelectionModel)serverItf);
    } else if (clientItfName.equals(TREE_CELL_RENDERER_BINDING)) {
      setCellRenderer((TreeCellRenderer)serverItf);
    }
  }

  public void unbindFc (String clientItfName) {
    // put your own code here
    if (clientItfName.equals(TREE_MODEL_BINDING)) {
      setModel(null);
    } else if (clientItfName.equals(TREE_SELECTION_MODEL_BINDING)) {
      setSelectionModel(null);
    } else if (clientItfName.equals(TREE_CELL_RENDERER_BINDING)) {
      setCellRenderer(null);
    }
  }

}
