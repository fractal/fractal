// automatically generated

package org.objectweb.fractal.swing;

public interface JMenuItemAttributes extends AbstractButtonAttributes {
  void setAccelerator (javax.swing.KeyStroke arg0);
  javax.swing.KeyStroke getAccelerator ();
}
