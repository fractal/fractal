################################################################################
#  Important note
################################################################################

This maven plugins is intended to be used only to build the cecilia project. It
should NOT be used by projects that use cecilia. Projects that use cecilia must
use the "org.objectweb.fractal.cecilia:maven-cecilia-plugin" instead.

################################################################################

This plugin provides plexus component descriptors that are used by maven for
building "cecilia-library" modules of the cecilia project.
Because of a maven limitation, these plexus components can't be provided inside
the cecilia project and must be available on a repository at the very beginning
of the build of the cecilia project.
