    -----
    Cecilia examples
    -----

Hello-World

  The <<Hello World example>> shows how to implement the traditional Hello-World
  Fractal example with the Cecilia framework.

  It exists in two versions, one for each implementation language supported by
  the Cecilia toolchain:

  * {{{helloworld/index.html}Hello World example implemented in thinkMC language}},
  with {{{helloworld/running.html} detailed informations}} on build
  processes supported by Cecilia (Maven and Makefile).

  * {{{helloworld-optC/index.html}Hello World example implemented in optC language}}
  with {{{helloworld-optC/running.html} detailed informations}} on build
  processes supported by Cecilia (Maven and Makefile).

  []

  This example is a good starting point for users that are not familiar with the
  Cecilia Framework.

Comanche Tutorial

  The <<Comanche example>> provides a detailed tutorial for
  writing a complex Cecilia application (a web server).

  It exists in two versions, one for each implementation language supported by
  the Cecilia toolchain:

  * {{{comanche/index.html}Comanche example implemented in thinkMC language}}

  * {{{comanche-optC/index.html}Comanche example implemented in optC language}}

  []

  This tutorial is intended for users that want to have a better understanding 
  on the design of component-based applications. 

Cloneable components

  The <<Cloneable component example>> shows how to define
  and instantiate dynamically Fractal components using <cloneable components>.

  It exists in two versions, one for each implementation language supported by
  the Cecilia toolchain:

  * {{{cloneable/index.html}Cloneable component example implemented in thinkMC language}}

  * {{{cloneable-optC/index.html}Cloneable component example implemented in optC language}}

  []

Multimedia application: MPEG-2 decoder

  The <<Libmpeg2 MPEG-2 decoder example>> shows a component-based version of
  the well-known libmpeg2 MPEG-2 decoder library, and describes the making of
  the component-based version.

  It exists in two versions, one for each implementation language supported by
  the Cecilia toolchain:

  * {{{libmpeg2/index.html}Libmpeg2 MPEG-2 decoder implemented in thinkMC language}}

  * {{{libmpeg2-optC/index.html}Libmpeg2 MPEG-2 decoder example implemented in optC language}}

  []
