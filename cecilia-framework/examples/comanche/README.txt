################################################################################
#   Cecilia Comanche example: OVERVIEW
################################################################################

This example shows a Fractal application with two main composite components
containing primitive components and composite components.
It is a port of the Julia Comanche example.


################################################################################
#   LAUNCHING THE BASE EXAMPLE WITH MAVEN
################################################################################
From the command line, simply type:
 $ mvn clean compile

Afterwards, execute the binary "comanche" file with:
 $ target/build/obj/comanche


################################################################################
#   LAUNCHING THE BASE EXAMPLE WITH MAKE
################################################################################
From the command line, simply type:
 $ ceciliac

Afterwards, execute the binary "comanche" file with:
 $ target/build/obj/comanche
