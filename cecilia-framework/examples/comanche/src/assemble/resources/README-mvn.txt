################################################################################
#   Cecilia comanche example: OVERVIEW
################################################################################

Cecilia Simple Web server example.


################################################################################
#   LAUNCHING THE EXAMPLE WITH MAVEN
################################################################################
From the command line, simply type:
 $ mvn clean compile


Afterwards, execute the binary "comanche" file with:
 $ target/build/obj/comanche


To test that Comanche web server is correctly running, open the following URL
in your web browser:

 $ http://localhost:8080/index.html
