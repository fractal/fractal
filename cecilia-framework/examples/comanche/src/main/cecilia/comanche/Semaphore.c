/**
 * Cecilia, an implementation of the Fractal component model in C.
 *
 * Copyright (C) 2007 ST Microelectronics, INRIA
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Matthieu Leclercq, Lionel Debroux.
 */

// START SNIPPET: Content
#include "Semaphore.h"

void semInit(Semaphore *s, int n) {
    s->waiting = n;
    pthread_mutex_init(&(s->mutex), NULL);
    pthread_cond_init(&(s->cond), NULL);
}

void semDeInit(Semaphore *s) {
    pthread_mutex_destroy(&(s->mutex));
    pthread_cond_destroy(&(s->cond));
}

void semP(Semaphore * s) {
    pthread_mutex_lock(&(s->mutex));
    s->waiting--;
    if (s->waiting < 0) {
        pthread_cond_wait(&(s->cond), &(s->mutex));
    }
    pthread_mutex_unlock(&(s->mutex));
}

void semV(Semaphore *s) {
    pthread_mutex_lock(&(s->mutex));
    s->waiting++;
    if (s->waiting >= 0) {
        pthread_cond_signal(&(s->cond));
    }
    pthread_mutex_unlock(&(s->mutex));
}
