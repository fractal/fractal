/**
 * Cecilia, an implementation of the Fractal component model in C.
 *
 * Copyright (C) 2007 ST Microelectronics, INRIA
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Matthieu Leclercq, Lionel Debroux.
 */

// START SNIPPET: Content
#include <pthread.h>

/** Declare component internal data, empty here */
DECLARE_DATA {
} ;


/** Include cecilia.h. Must be included after the DECLARE_DATA */
#include <cecilia.h>

#include "FunctionArgTypes.h"
#include "csapp.h"
#include "Semaphore.h"


// Global variables that cannot be inclued in the internal component data
// because the "DATA." construct uses "_this", and the signal handlers
// cannot be passed the _this variable (they're called by the OS).
// (One could always put a global variable containing the value of _this
// for this component, but this is bad.)
/**
 * Semaphore used to ensure mutual exclusion of calls to the signals (it is
 * initialized with a waiting queue of 1, making it a mutex).
 */
Semaphore semSignals;

/**
 * Boolean indicating that a SIGINT or SIGTERM has already been handled.
 */
int sigHandled;

/**
 * Buffer used by setjmp() and longjmp(), in order to execute cleanup.
 * Using setjmp and longjmp is more portable (and safer ?) than using
 * GCC-specific __attribute__((constructor)) / __attribute__((constructor)).
 */
jmp_buf saveState;


/**
 * SIGPIPE handler: handle SIGPIPE gracefully (the default handler kills the process).
 * Semaphores are used to force mutual exclusion of the signal handler (though it shouldn't
 * matter much for this one).
 */
static void SIGPIPE_handler(int val) {
    int errno_saved; // Save and restore system global variable errno (its value may be changed asynchronously).
    errno_saved = errno;
    semP(&semSignals);
    fprintf(stderr,"Received signal %d (SIGPIPE): pipe incorrectly closed (errno = %d) !\n",val,errno_saved);
    semV(&semSignals);
    errno = errno_saved;
}

/**
 * SIGINT handler: execute cleanup code.
 * Semaphores are used to force mutual exclusion of the signal handler (it is important that
 * assignments to sigHandled are atomic).
 */
static void SIGINT_handler(int val) {
    semP(&semSignals);
    if (sigHandled == 0) {
        sigHandled = 1;
        semV(&semSignals);
        fprintf(stderr,"Received signal %d (SIGINT): CTRL+C, exiting !\n",val);
        longjmp(saveState,1);
    }
    semV(&semSignals);
}

/**
 * SIGTERM handler: execute cleanup code.
 * Semaphores are used to force mutual exclusion of the signal handler (it is important that
 * assignments to sigHandled are atomic).
 */
static void SIGTERM_handler(int val) {
    semP(&semSignals);
    if (sigHandled == 0) {
        sigHandled = 1;
        semV(&semSignals);
        fprintf(stderr,"Received signal %d (SIGTERM): process termination request, exiting !\n",val);
        longjmp(saveState,1);
    }
    semV(&semSignals);
}


// -----------------------------------------------------------------------------
// Implementation of the boot interface.
// -----------------------------------------------------------------------------
/**
 * This method is the user entry point of the program.
 * Cecilia IDL declaration: int main(int argc, string[] argv);
 */
int METHOD(main, main) (void *_this, int argc, char *argv[]) {  
    (void)argc, (void)argv;
    int listenfd = -1;
    int connectfd;
    struct sockaddr_in clientaddr;
    handler_t * oldSIGPIPE;
    handler_t * oldSIGINT;
    handler_t * oldSIGTERM;

    sigHandled = 0;
    semInit(&semSignals, 1); // This semaphore is a mutex.

    // Redirect signals.
    if (   ((oldSIGINT = Signal(SIGINT,SIGINT_handler)) != (handler_t *)-1)
        && ((oldSIGTERM = Signal(SIGTERM,SIGTERM_handler)) != (handler_t *)-1)
        && ((oldSIGPIPE = Signal(SIGPIPE,SIGPIPE_handler)) != (handler_t *)-1)
       ) {

        if (setjmp(saveState) == 0) {
            // This is the first time setjmp returns.
            fprintf(stdout,"Hello, Comanche Web Server starting :-)\n");

            // Create server socket, listening on port 8080.
            listenfd = open_listenfd(8080);
            if (listenfd >= 0) {
                // Socket creation succeeded, we can proceed.
                while (1) {
                    unsigned int clientlen = sizeof(clientaddr);
                    connectfd = accept(listenfd, (struct sockaddr *)&clientaddr, &clientlen);
                    if (connectfd >= 0) {
                        // Pass file descriptor to scheduler.
                        CALL(REQUIRED.s, schedule, (void *)(intptr_t)connectfd); // sizeof(int) <= sizeof(void *), so this conversion is valid.
                    }
                    else {
                        fprintf(stderr,"RequestReceiver: failed accepting a connection !\n");
                    }
                }
            }
            else {
                fprintf(stderr,"RequestReceiver: failed to create server socket !\n");
            }
        }
        else {
            // setjmp has returned non-zero, which means one of the SIGINIT or SIGTERM handlers
            // has been executed: cleanup.
            if (listenfd >= 0) {
                if (close(listenfd) < 0) {
                    fprintf(stderr,"RequestReceiver: failed to close server socket !\n");
                }
            }
        }
        // Restore signals.
        Signal(SIGPIPE,oldSIGPIPE);
        Signal(SIGINT,oldSIGINT);
        Signal(SIGTERM,oldSIGTERM);
    }
    else {
        fprintf(stderr,"RequestReceiver: failed to redirect signals !\n");
    }

    fprintf(stdout,"Comanche Web Server stopping, bye :-)\n");
    fflush(stderr);
    fflush(stdout);

    semDeInit(&semSignals);

    return 0;
}
