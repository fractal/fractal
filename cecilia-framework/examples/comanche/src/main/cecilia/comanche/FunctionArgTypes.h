/**
 * Cecilia, an implementation of the Fractal component model in C.
 *
 * Copyright (C) 2007 ST Microelectronics, INRIA
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Matthieu Leclercq, Lionel Debroux.
 */

// START SNIPPET: Content
/**
 * This files defines the opaque data types passed as void * (Cecilia IDL "any")
 * between multiple components.
 */

/**
 * This structure defines what the scheduler passes to its worker function.
 */
typedef struct {
    /**
     * _this of the scheduler, used by the worker function.
     */
    void * _this;
    /**
     * Worker function arguments. In the multithread scheduler,
     * this is passed to pthread_create as fourth argument.
     */
    void * fd;
} WorkerArgs;

/**
 * This structure defines what the request analyzer passes to the request dispatcher,
 * which passes it to the request handlers it is bound to.
 */
typedef struct {
    /**
     * Request string.
     */
    char * request;
    /**
     * File descriptor used by the request handlers to write the reply.
     */
    int fd;
} RequestHandlerArgs;
