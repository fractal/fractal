/**
 * This source file contains modified portions of csapp.h/c, known from
 * Computer Systems: A Programmer's Perspective
 * R. E. Bryant, D. O'Hallaron
 * Prentice Hall, 2003
 * ISBN-13: 978-0130340740
 */

// START SNIPPET: Content
#include "csapp.h"

handler_t *Signal(int signum, handler_t *handler)
{
    struct sigaction action, old_action;
    int retval;

    action.sa_handler = handler;
    sigemptyset(&action.sa_mask); /* block sigs of type being handled */
    action.sa_flags = SA_RESTART; /* restart syscalls if possible */

    retval = sigaction(signum, &action, &old_action);
    return ((retval < 0) ? (handler_t *)-1 : old_action.sa_handler);
}

int open_listenfd(int port)
{
    int listenfd, optval=1;
    struct sockaddr_in serveraddr;

    // Create a socket descriptor
    if ((listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        return -1;

    // Eliminates "Address already in use" error from bind.
    if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR,
        (const void *)&optval , sizeof(int)) < 0)
        return -1;

    // Listenfd will be an endpoint for all requests to port
    // on any IP address for this host
    memset((char *) &serveraddr, 0, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
    serveraddr.sin_port = htons((unsigned short)port);
    if (bind(listenfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) < 0)
        return -1;

    // Make it a listening socket ready to accept connection requests
    if (listen(listenfd, 64) < 0)
        return -1;

    return listenfd;
}
