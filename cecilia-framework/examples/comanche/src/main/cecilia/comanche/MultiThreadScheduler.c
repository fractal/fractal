/**
 * Cecilia, an implementation of the Fractal component model in C.
 *
 * Copyright (C) 2007 ST Microelectronics, INRIA
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Matthieu Leclercq, Lionel Debroux.
 */

// START SNIPPET: Content
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

/** Declare component internal data, empty here */
DECLARE_DATA {
} ;

/** Include cecilia.h. Must be included after the DECLARE_DATA */
#include <cecilia.h>

#include "FunctionArgTypes.h"




/**
 * Wrapper for worker function, because a pthread_exit() is needed to exit a pthread.
 * This call cannot be put in the worker function, because the worker function has
 * to be independent from the scheduler implementation.
 * Calling pthread_exit() when there's only one thread (sequential scheduler) terminates
 * the program.
 */
void * workerFunctionWrapper(void * args) {
    void * _this = ((WorkerArgs *)args)->_this;
    // Call function directly (synchronous).
    CALL(REQUIRED.rh, handleRequest, ((WorkerArgs *)args)->fd);

    // It's our duty to free our args, because the caller (schedule) has probably already returned.
    free(args);
    pthread_exit(NULL);
}

// -----------------------------------------------------------------------------
// Implementation of the Scheduler interface.
// -----------------------------------------------------------------------------
/**
 * This method is called by the request receiver.
 * Cecilia IDL declaration: void schedule(any task);
 */
void METHOD(s, schedule) (void *_this, void * task) {
    pthread_t thread;
    pthread_attr_t attrs;
    WorkerArgs * wArgs;
#ifdef DEBUG
    fprintf(stdout,"MultiThreadScheduler: schedule\n");
#endif

    wArgs = malloc(sizeof(*wArgs));
    if (wArgs != NULL) {

        // Retrieve arguments from the opaque type passed to this function.
        wArgs->_this = _this;
        wArgs->fd = task;

        // Create worker thread.
        if (// Set "detached" thread attribute. We don't want to join in this function
            // the thread we're creating, otherwise we're a sequential scheduler...
               (pthread_attr_init(&attrs) != 0)
            || (pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_DETACHED) != 0)
            // Create thread.
            || (pthread_create(&thread, &attrs, workerFunctionWrapper, (void*)wArgs) != 0)
        ) {
            // Error...
            fprintf(stderr,"MultiThreadScheduler: cannot create thread !\n");
            // Free wArgs on this error path.
            free(wArgs);
        }
        else {
#ifdef DEBUG
            fprintf(stdout,"MultiThreadScheduler: thread created.\n");
#endif
        }
        // DON'T free wArgs here, this area might still be used by the worker wrapper/function thread.
        // The worker wrapper frees its argument.
    }
    else {
        fprintf(stderr,"MultiThreadScheduler: cannot allocate memory for worker thread args !\n");
    }
}
