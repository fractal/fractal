    -----
    Cecilia Comanche example, implemented in thinkMC
    -----
    Eric Brunteton, Lionel Debroux.
    -----

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Overview

  This tutorial is an introduction to the Fractal component model, using the
  Cecilia framework as implementation. It explains informally how to
  {{{#Design}design}}, {{{#Implementation}implement}} and
  {{{#Configuration}deploy}} component-based applications with Fractal (and with
  some associated tools), in C, by using a concrete example, namely an extremely
  minimal web server.

  This document is intended for those that do not know Fractal, and want to get
  an overview of this component model, of its motivations and benefits. If you
  are in this case, you should read this document first, before reading the
  Fractal component model specification.

  This document is organized as follows:

  * an introduction to the Fractal component model;

  * notes on the design of a component-based web server;

  * description of the implementation of the component-based web server;

  * notes on the way to describe the architecture of the web server in the
    Cecilia ADL;

  * Appendices containing the source code (C, IDL, ADL).

  []

  <<NOTE>>: this document describes the implementation with the <<thinkMC>>
  implementation language, which cannot be optimized at build time by the
  Cecilia toolchain. You should implement your applications with the newer
  <<optC>> language, which allows build-time optimization. See the <<optC>>
  version of this document at
  {{{../comanche-optC/index.html}Cecilia Comanche example implemented in optC}}.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Download

  To get sources of the Comanche example based on <<Makefile>>, download one of
  the following packages:

    * {{{downloads/${project.build.finalName}-makefile.tar.gz}${project.build.finalName}-makefile.tar.gz}}

    * {{{downloads/${project.build.finalName}-makefile.zip}${project.build.finalName}-makefile.zip}}

  To get sources of the Comanche example based on <<Maven>>, download one of the
  following packages:

    * {{{downloads/${project.build.finalName}-mvn.tar.gz}${project.build.finalName}-mvn.tar.gz}}

    * {{{downloads/${project.build.finalName}-mvn.zip}${project.build.finalName}-mvn.zip}}

  Instructions for compiling and running the example, see the <<<README.txt>>>
  file.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Introduction

* What is Fractal?

  Fractal is a modular and extensible component model that can be used with
  various programming languages to design, implement, deploy and reconfigure
  various systems and applications, from operating systems to middleware
  platforms and to graphical user interfaces. Fractal is also a project with
  several sub projects, dealing with the definition of the model, its
  implementations, and the implementation of reusable components and tools
  on top of it.

  The Fractal component model heavily uses the <<separation of concerns>>
  design principle. The idea of this principle is to separate into distinct
  pieces of code or runtime entities the various <concerns or aspects> of an
  application: implementing the service provided by the application, but also
  making the application configurable, secure, available, ...\
  In particular, the Fractal component model uses three specific cases of the
  separation of concerns principle: namely <separation of interface and
  implementation>, <component oriented programming>, and <inversion of
  control>.\
  The first pattern, also called the bridge pattern, corresponds to the
  separation of the design and implementation concerns.\
  The second pattern corresponds to the separation of the implementation
  concern into several composable, smaller concerns, implemented in well
  separated entities called components.\
  The last pattern corresponds to the separation of the functional and
  configuration concerns: instead of finding and configuring themselves the
  components and resources they need, Fractal components are configured and
  deployed by an external, separated entity.

  The separation of concerns principle is also applied to the structure of
  the Fractal components. A Fractal component is indeed composed of two
  parts: a <content> that manages the functional concerns, and a
  <controller> that manages zero or more non functional concerns
  (introspection, configuration, security, transactions, ....).\
  The content is made of other Fractal components, i.e. Fractal components
  can be <nested> at arbitrary levels (Fractal components can also be
  shared, i.e. be nested in several components at the same time).\
  The introspection and configuration interfaces that can be provided by
  the controllers allow components to be deployed and reconfigured
  dynamically.\
  These control interfaces can be used either programmatically, or through
  tools based on them, such as deployment or supervision tools.

  More information about Fractal, including the complete specification of the
  component model, and several tutorials, can be found at {{{http://fractal.objectweb.org}http://fractal.objectweb.org}}.

* Summary

  The main characteristics of the Fractal model are recursion, reflexion and
  sharing. The Fractal project is made of four sub projects: model,
  implementations, components and tools.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Design

  Before programming a component based software system with Fractal, one must
  first <design> it with components and, in particular, identify the components
  to be implemented.\
  Note that this <component oriented design> task is quite independent from the
  subsequent <component oriented programming> task: at programming time, it is
  possible to merge several or even all the design time components into a
  single, monolithic piece of code, if desired (but then, of course, one loses
  the advantages of <component oriented programming>: modularity,
  adaptability, ...).

  This section explains how to design component based applications, by using a
  concrete example. The next sections reuse this example to illustrate how to
  implement and deploy Fractal component based applications. This example
  application is Comanche, an extremely minimal HTTP server. A classical,
  non component oriented, threaded implementation of this application, in
  pseudo-code, is shown below:

+------------------------------------------------------------------------------+
Server {
  Socket server_socket;
  Socket worker;

  while true do
    server_socket = create server socket
    worker_socket = accept(server_socket, port)
    // accept is a blocking call, it returns only when it has accepted a connection
    create new worker thread, passing it worker_socket
  end while
}

WorkerThread {
  read request on socket

  print first line of request
  // analyse request
  if (request starts with "GET /") then
    get actual filename
    // up to the second " " of the first line of a well-formed request

    if ((file exists) and (file is not special, e.g. a directory)) then
      write "HTTP/1.0 200 OK\n\n" to socket
      write contents of file to socket
    else
      write "HTTP/1.0 404 Not Found\n\n" to socket
      write e.g. "<html> Document not found.</html>" to socket
    end if
  end if
  close socket
}
+------------------------------------------------------------------------------+

  As can be seen from the source code, this server accepts connections on a
  server socket and, for each connection, starts a new thread to handle it.
  Each connection is handled in two steps: the request is analyzed and logged
  to the standard output, and then the requested file is sent back to the
  client (or an error is returned if the file is not found).

* Finding the components

  <<In a component based application, some components are>> <dynamic>, i.e they can
  be created and destroyed dynamically, possibly quite frequently, <<while other
  components are>> <static>, i.e. their life time is equal to the life time of
  the application itself. The dynamic components generally correspond to <data>,
  while the static ones generally correspond to <services>.

  <<In order to identify components in an application, it is easier to begin by
  identifying its static components>>. In other words, one should begin by
  identifying the services that are used in the application.\
  In the case of Comanche, we can immediately identify two main services, namely
  a request receiver service and a request processor service. But it is also
  possible to identify other, lower level services.\
  For example, we can see that the request receiver service uses a thread
  factory service, to create a new thread for each request. This thread factory
  service can be generalized into a scheduler service that can be implemented
  in several ways: sequential, multi-thread, multi-thread with a thread pool,
  and so on.\
  Likewise, we can see that the request processor uses a request analyzer
  service, and a logger service, before effectively responding to a request.
  This response is itself constructed by using a file server service, or an
  error manager service. This can be generalized into a request dispatcher
  service that dispatches requests to several request handlers sequentially,
  until one handler can handle the request (we can then imagine file handlers,
  servlet handlers, and so on).

  The caller graph & call graph extraction functionality of the powerful
  {{{http://www.doxygen.org}Doxygen}} tool (free software) can help determining
  services.

  <<After the services have been specified, one can look for the main data
  structures, in order to identify the dynamic components>>. But the
  identification of the dynamic components is not mandatory, and is generally
  not done, because dynamic components themselves are rarely used (this means
  that, at programming time, data structures are generally not implemented as
  components, but as ordinary <struct>s).\
  Indeed components do not have many benefits in the case of highly dynamic,
  short lived structures (introspection and dynamic reconfiguration, for
  instance, are not very useful in this case). In the case of Comanche we
  can consider sockets, HTTP requests, files, streams, and even threads as
  such data structures. But we will not map them to dynamic components.

  <<After the services have been specified, one must assign them to components.>>
  Each component can provide one or more services but, unless two services
  are very strongly coupled, it is better to use one component per service.\
  In the case of Comanche, we will use one component per service. We
  therefore have the seven following components: request receiver, request
  analyzer, request dispatcher, file request handler, error request handler,
  scheduler and logger.

* Defining the component architecture

  After the components have been identified, it is easy to find the
  dependencies between them, and to organize them into composite components.
  Indeed the service dependencies are generally identified at the same time
  as the services themselves. If it is not the case, dependencies can also
  be found by looking at some use cases or scenarios.\
  Likewise, services are generally identified from high level to lower level
  services (or vice versa). It is then easy to find the dependencies and the
  abstraction level of each component, since the components correspond to the
  previous services.

  This is particularly clear in the case of Comanche: indeed, by re-reading the
  previous section, one can see that the service dependencies have already been
  identified. For example:

  * the request receiver service uses the scheduler service and the request
    analyzer (or the request receiver service uses the scheduler and the
    scheduler uses the request analyzer);

  * the request analyzer uses the request dispatcher;

  * the request dispatcher itself uses the file and error request handlers.

  []

  One can also see that the abstraction levels have already been found:

  * the request receiver is ``made of'' the request receiver itself, plus the
  scheduler;

  * the request processor is made of the request analyzer, the logger, and the
    request handler;

  * the request handler is itself made of the request dispatcher, and of the
    file and error request handlers.

  []

  All this can be summarized in the following component architecture:

[archi-cecilia.png] Architecture of the Cecilia Comanche example.

* Defining the component contracts

  After the services have been found and organized into components, and after
  the component hierarchy and the component dependencies have been found, only
  <<one thing remains to be done to finish the design phase, namely to define
  precisely the contracts between the components, at the syntactic and semantic
  level>> (if possible with a formal language - such as pre and post conditions,
  temporal logic formulas, and so on). Classical object oriented design tools,
  such as scenarios and use cases, can be used here.

  The component contracts must be designed with care, so as to be the most
  stable as possible (changing a contract requires to change several components,
  and is therefore more difficult than changing a component). In particular,
  these contracts must deal only with the services provided by the components:
  nothing related to the implementation or configuration of the components
  themselves should appear in these contracts.\
  For example, a <<<setLogger>>> operation has nothing to do in a component
  contract definition: this operation is only needed to set a reference between
  a component and a logger component. In other words, contracts must be defined
  with <<separation of concerns>> in mind (see section {{{#Introduction}Introduction}}):
  contracts must deal only with functional concerns; configuration concerns
  will be dealt with separately, as well as other concerns such as security,
  life cycle, transactions...

  In the case of Comanche, we will use minimalistic contracts, defined in
  Cecilia IDL:

  * The logger service will provide a single <<<log>>> method, with a single
    parameter representing a string;

  * The scheduler component will provide a single <<<schedule>>> method, with a
    single parameter representing a pointer to a <<<struct>>> containing a way
    to run a function with its parameters (function pointer + pointer to
    arguments), and also possibly other implementation-defined data.\
    The role of this schedule method is to execute the given entity at some time
    after the method has been called, possibly in a different thread than the
    caller thread;

  * The request analyzer, the request dispatcher and the file and error request
    handlers will all provide a single <<<handleRequest>>> method, with a single
    parameter representing a socket file descriptor or a <<<char *>>>. This
    single handleRequest method will be implemented in several ways, in the
    different components:

    * the request analyzer will read the request to get the requested URL;

    * the request dispatcher will forward each request to its associated
      request handlers, sequentially, until one request handler successfully
      handles the request;

    * the file request handler will try to find and to send back to the client
      the file whose name corresponds to the requested URL, if it exists;

    * the error request handler will send back to the client an error message,
      and will always succeed.

* Summary

  The first step to design a component based application is to define its
  components. This is done by finding the <services> used in this application.\
  The second step is to find the dependencies and hierarchical levels of these
  components.\
  The last step is to define precisely the contracts between the components.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Implementation

  This section explains how to program Fractal component based applications, by
  using the Comanche example. It also introduces and motivates the concepts and
  APIs of Fractal that are used.

* Choosing the components' granularity

  As explained in section {{{#Design}Design}}, component oriented design is
  quite independent from component oriented implementation. In particular, at
  programming time, it is possible to merge several or even all the design
  time components into a single, monolithic piece of code, if desired.\
  For example, in the case of Comanche, one may choose to implement all the
  design time components into a single class, as shown in section
  {{{#Design}Design}}.\
  One may also choose to implement each component in its own class, or to
  implement some components in their own class, and to merge some other
  components (such as the request dispatcher and its associated request
  handlers) into a single class.

  Using one runtime component per design time component gives maximum
  flexibility and extensibility, but can be less efficient that merging several
  design time components into a single runtime component. When in doubt, <the
  first solution should be preferred>: optimizations can be done later on, if
  needed.\
  In the case of Comanche, we will therefore use one runtime component per
  design time component.

* Implementing the component interfaces

  Before implementing the component themselves, the first step is to implement
  their <interfaces>. Indeed the Fractal component model requires a strict
  separation between interfaces and implementation for all components (see
  section {{{#Introduction}Introduction}}).\
  This design pattern is indeed useful to easily replace one component
  implementation with another. It also offers the possibility to add
  interposition objects between a client and a component implementation, in
  order to transparently manage some non functional concerns of the component
  (as in the Enterprise Java Beans model).\
  The only drawback of this design pattern is that it is a little less
  efficient than a solution without interfaces. This is why it may sometimes
  be needed to merge several design time components into a single Fractal
  component.

  The component interfaces can be implemented easily, since most, if not all,
  of the work has been done during the definition of the component contracts.
  In the case of Comanche, three interfaces must be implemented. They are
  given below (see Appendix Comanche source code for the most importants bits
  of the source code of Comanche):

+------------------------------------------------------------------------------+
interface RequestHandler { int handleRequest (any r); }

interface Scheduler { void schedule(any task); }

interface Logger { void log(string msg); }
+------------------------------------------------------------------------------+

  Here, anonymous type any was used, but non-component types (such as
  <<<struct>>>s) can be used in interface method parameters. <This is not
  recommended>. It is indeed better to use interfaces, even for data structures
  that are not represented as components: these data structures can then be
  implemented in various ways (including as components), without needing to
  change the interfaces that refer to them.\
  Note also that it would have been better to introduce a request factory
  component (this was not done for simplification and efficiency purposes; see
  the {{{../cloneable/index.html}Cecilia Cloneable example}} for more
  information on how to do that).

* Implementing the components

  Now that the component interfaces have been implemented, we can implement the
  components themselves. The components that do not have any dependencies to
  other components can be programmed like ordinary C ``modules''.
  For example, the logger component can be implemented as follows:

+------------------------------------------------------------------------------+
#include <stdio.h>

DECLARE_DATA { } ;
#include <cecilia.h>

void METHOD(l, log) (void *_this, char * msg) {
    fprintf(stdout,"Basic logger: log \"%s\"\n",msg);
}
+------------------------------------------------------------------------------+

  In component oriented programming, and in Fractal in particular, the
  components that have dependencies to other components must be programmed in a
  specific way. In Cecilia, this is done using the thinkMC ``language'' (C
  macros such as <<<METHOD>>>, <<<REQUIRED>>> and <<<CALL>>>).\
  The Cecilia toolchain generates code to glue the components together. This
  code glue is compiled and linked with your application, and it has support
  for one of the goals of Fractal, dynamic reconfigurations.

  In fact, in Fractal, a component with dependencies (or <bindings>) to other
  components must implement the <<<BindingController>>> interface, defined in
  the Fractal specification. This interface defines four generic methods
  <<<listFc>>>, <<<lookupFc>>>, <<<bindFc>>> and <<<unbindFc>>> to manage
  component bindings.

  The <<<listFc>>> method returns the names of the dependencies of the
  component, and the <<<lookupFc>>>, <<<bindFc>>> and <<<unbindFc>>> methods
  are used to read, set and unset the corresponding bindings (the <<<s>>> and
  <<<rh>>> strings do not have to be equal to the names of the corresponding
  fields).\
  This enables good distinction between the <controller> and <content> part of
  Fractal components (see section {{{#Introduction}Introduction}}): the
  controller part corresponds to the <<<BindingController>>> interface, and the
  content part to the <<<boot.api.Main>>> interface (in the logger and scheduler
  components, the controller part was empty).

  <<The>> <<<listFc>>>, <<<lookupFc>>>, <<<bindFc>>> <<and>> <<<unbindFc>>>
  <<methods are already implicitly implemented when a Cecilia application is
  compiled>>, if the <cardinality> of the bindings is <singleton> (a single
  server interface is bound to a client interface). If the <cardinality> is
  <collection>, you <currently> have to implement the <<<BindingController>>>
  methods in your source code. The <<<RequestDispatcher>>> component does it.

  In order to do that, you have to know that each binding must have a name of
  the form <prefix><<postfix>> where <prefix> is common to all the bindings of
  the collection, and where <<postfix>> is arbitrary, but distinct for each
  binding.\
  Have a look at the implementation of the <<<RequestDispatcher>>> component,
  which contains a partial implementation of the collection binding.
  Once again, this partial implementation was done for simplification purposes,
  since <<<Map>>>s (Java) / <<<hash>>>es (Perl and most scripting languages) are
  not part of the standard C language.

  Another simplification and "beautification" of the C code was using the request
  analyzer from the scheduler, instead of using it from the request receiver.
  Older versions of this tutorial did the latter, so as to be closer to the
  way the Java (Julia) version of this tutorial is implemented.

* ADL based configuration

  Configuration and deployment <could> be done using a program that instantiates
  components, but this method has several drawbacks:

  * it's not that trivial to program;

  * it's error prone: it's easy to forget a binding or to create a wrong
    binding.

  * the component architecture is not directly visible (the component's
    hierarchy description, in particular, is completely lost);

  * most importantly, <this method mixes two separate concerns, namely
    architecture description and deployment>. It is impossible to deploy a
    given component architecture in several ways, without rewriting the
    configuration/deployment program).

  []

  In order to solve these problems, a solution is to use an Architecture
  Description Language (ADL). As its name implies, an ADL definition describes a
  component architecture, and only that, i.e. its does not describe the
  instantiation method. This solves the most important drawback of the previous
  configuration method.\
  An ADL is also generally strongly typed, which allows the ADL parser to
  perform verifications about the declared component architecture. Using an ADL
  is therefore less error prone and more flexible than using the programmatic
  approach.

  <<Cecilia ADL>> is a possible, XML based ADL that can be used to describe Fractal
  component configurations. Other ADLs can be created if needed (indeed these
  ADLs are not part of the Fractal component model itself: they are just tools
  based on this model).\
  Cecilia ADL is <<strongly typed>>. The first step to define a component
  architecture is therefore to define the types of the components. Each
  component type must specify what components of this type provide to, and
  require from other components. For example, the type of the file and error
  handler components (but also of the request handler and backend components),
  in Comanche, can be defined as follows (these components provide a
  <<<RequestHandler>>> interface, and do not have dependencies):

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/HandlerType.fractal}

  Components with dependencies are declared in a similar way. For example, the
  type of the request dispatcher component, in Comanche, can be defined as
  follows:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/DispatcherType.fractal}

  Note that this type is declared to <extend> the previous handler type: this
  means that the provided and required interface types declared in the handler
  type are inherited by the dispatcher type. Note also the optional
  <cardinality> attribute in the interface type definition: it means that
  components of this type can have a variable number of bindings (as mentioned
  above).

  After the component types have been defined, the components themselves can be
  defined. Here Cecilia ADL distinguishes between components that do not expose
  their content, called <<primitive components>>, and components that do expose
  it, called <<composite components>>.

  A primitive component is defined by specifying its component type and the C
  ``module'' that implements it. For example, the file handler component can
  be defined as follows:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/FileHandler.fractal}

  A composite component is defined by specifying its sub-components, and the
  bindings between these sub-components. For example, the Comanche composite
  component, which represents the whole application, and which contains the
  Frontend and Backend components, can be defined as follows:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/Comanche.fractal}

  This definition says that the Comanche component provides a <<<Runnable>>>
  interface (to start the application), that it contains two sub components
  named <<<fe>>> and <<<be>>>, that the <<<Runnable>>> interface provided by
  Comanche (<<<this.r>>>) is provided by the <<<Runnable>>> interface of its
  frontend sub-component (<<<fe.r>>>), and that the request handler required
  by the frontend sub-component (<<<fe.rh>>>) is provided by the backend
  component (<<<be.rh>>>).

  Once the application's architecture has been defined, it can be compiled,
  which gives a C ``module'', in source code. The Cecilia ADL parser performs
  preliminary verifications to check the architecture and, in particular,
  to check that there is no missing or invalid binding.

  If one wants to change the implementation of one or more of the components
  used in the program (e.g. use a multi-threaded scheduler instead of a
  sequential, synchronous one), modifying a few characters in an ADL file
  is all that's necessary.

* Summary

  Components must be implemented with an appropriate granularity, resulting from
  a compromise between adaptability and performance.\
  Their interfaces must be separated from their implementation (this rule must
  also be applied for data structures).\
  The implementation must not contain explicit dependencies to other components
  to allow both static and dynamic (run-time) reconfigurations.\
  Components can be configured and deployed in two different ways. The
  programmatic approach mixes different concerns, and is not that trivial to
  implement in C. The ADL-based approach correctly separates these concerns.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Conclusion

  The Fractal component model uses well known design patterns, and organizes
  them into a uniform, language independent model, that can be applied to
  operating systems, middleware platforms or graphical user interfaces.

  The Fractal component model brings several benefits:

  * it enforces the definition of a good, modular design;

  * it enforces the separation of interface and implementation, which ensures a
  minimum level of flexibility;

  * it enforces the separation between the functional, configuration and
  deployment concerns, which allows the application's architecture to be
  described separately from the code, for example by using an Architecture
  Description Language;

  * it allows applications to be instantiated in various ways (from fully
  optimized but unreconfigurable configurations to less efficient but fully
  dynamically reconfigurable configurations).

  []

  All these features should reduce the development time, and should also
  increase the reusability of components and component architectures, i.e.
  they should increase productivity.

  The Fractal component model and its associated tools have already been used
  successfully in a number of applications such as:

  * <<THINK>>, a library of Fractal components to build operating system
    kernels;

  * <<Speedo>>, an implementation of the Java Data Object (JDO) specification;

  * <<Proactive>>, a middleware for parallel, distributed and multi-threaded
  computing;

  * <<Petals>>, a large middleware for Enterprise Application Integration.

  []

  More information about Fractal, including the complete specification of the
  component model, and several tutorials, can be found at
  {{{http://fractal.objectweb.org}http://fractal.objectweb.org}}.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Appendix: Comanche source code

  Below are the most important bits of the source code of Comanche, without some
  implementation-specific files (semaphores, wrappers for several POSIX/*nix
  functions).

  Notice the <<METHOD>>, <<CALL>>, <<REQUIRED>> and <<DECLARE_DATA>> syntactic
  sugar.


* Component interfaces

  File <<<comanche/RequestHandler.idl>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/RequestHandler.idl}

  File <<<comanche/Scheduler.idl>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/Scheduler.idl}

  File <<<comanche/Logger.idl>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/Logger.idl}


* Component implementations

  File <<<comanche/BasicLogger.c>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/BasicLogger.c}

  File <<<comanche/SequentialScheduler.c>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/SequentialScheduler.c}

  File <<<comanche/MultiThreadScheduler.c>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/MultiThreadScheduler.c}

  File <<<comanche/FileRequestHandler.c>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/FileRequestHandler.c}

  File <<<comanche/ErrorRequestHandler.c>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/ErrorRequestHandler.c}

  File <<<comanche/RequestDispatcher.c>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/RequestDispatcher.c}

  File <<<comanche/RequestAnalyzer.c>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/RequestAnalyzer.c}

  File <<<comanche/RequestReceiver.c>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/RequestReceiver.c}

  <<See the rest of source codes in the Cecilia distribution.>>

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Appendix: Comanche architecture definition

  Here are the Cecilia ADL definitions describing the Comanche architecture:


* Component types

  File <<<comanche/SchedulerType.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/SchedulerType.fractal}

  File <<<comanche/HandlerType.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/HandlerType.fractal}

  File <<<comanche/ReceiverType.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/ReceiverType.fractal}

  File <<<comanche/AnalyzerType.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/AnalyzerType.fractal}

  File <<<comanche/LoggerType.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/LoggerType.fractal}

  File <<<comanche/DispatcherType.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/DispatcherType.fractal}

  File <<<comanche/FrontendType.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/FrontendType.fractal}

  File <<<comanche/ComancheType.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/ComancheType.fractal}


* Primitive components

  File <<<comanche/SequentialScheduler.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/SequentialScheduler.fractal}

  File <<<comanche/MultiThreadScheduler.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/MultiThreadScheduler.fractal}

  File <<<comanche/Receiver.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/Receiver.fractal}

  File <<<comanche/Analyzer.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/Analyzer.fractal}

  File <<<comanche/Logger.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/Logger.fractal}

  File <<<comanche/Dispatcher.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/Dispatcher.fractal}

  File <<<comanche/FileHandler.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/FileHandler.fractal}

  File <<<comanche/ErrorHandler.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/ErrorHandler.fractal}


* Composite components

  File <<<comanche/Handler.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/Handler.fractal}

  File <<<comanche/Backend.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/Backend.fractal}

  File <<<comanche/Frontend.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/Frontend.fractal}

  File <<<comanche/Comanche.fractal>>>:

%{snippet|id=Content|url=file://${basedir}/src/main/cecilia/comanche/Comanche.fractal}
