################################################################################
#   Cecilia libmpeg2 example: OVERVIEW
################################################################################

This example shows a component-based version of a multimedia application,
namely the libmpeg2 MPEG-2 decoder and its associated standalone command-line
executable.


################################################################################
#   COMPILING AND LAUNCHING THE EXAMPLE WITH MAVEN
################################################################################
First, we need to configure the application:
    * first, in the src/main/cecilia folder, run `sh bootstrap`. You need the
      GNU autotools and libtool, which generate a script named "configure", and
      files suitable for it.
    * then, in the same folder, run `./configure`. configure auto-detects
      platform, system headers, compiler capabilities / bugs, and configures
      the decoder accordingly (generating a include/config.h file used by the
      decoder). It also generates makefiles, but we won't use those for building
      libmpeg2.

In order to compile the application, go back to the root of the libmpeg2
distribution and type:
 $ make mpeg2dec
You might need to edit the src/main/cecilia/unix/libmpeg2-warnings.td file to
adapt the <compiler>, <linker>, <cFlags>, <ldFlags> elements according to values
of "CC", "CC", "OPT_CFLAGS", "LD_FLAGS", found in src/Makefile.


After successful compilation, the binary "mpeg2dec" file can be executed with:
 $ build/mpeg2dec <options>

`mpeg2dec -h` tells how to use it (NOTE: the available video outputs depend on
the configuration options passed to `configure`):
mpeg2dec-0.5.1 - by Michel Lespinasse <walken@zoy.org> and Aaron Holtzman
usage: target/build/obj/mpeg2dec [-h] [-o <mode>] [-s [<track>]] [-t <pid>] [-p] [-c] \
                [-v] [-b <bufsize>] <file>
        -h      display help and available video output modes
        -s      use program stream demultiplexer, track 0-15 or 0xe0-0xef
        -t      use transport stream demultiplexer, pid 0x10-0x1ffe
        -p      use pva demultiplexer
        -c      ignored (non-component-based version: use c implementation, disables all accelerations)
        -v      verbose information about the MPEG stream
        -b      set input buffer size, default 4096 bytes
        -o      video output mode
                        xv  ->
                        xv2 -> XV video outputs. Don't work with some drivers.
                        x11 -> X11 video output
                        sdl -> SDL video output
                        null      ->
                        nullslice ->
                        nullskip  -> Various benches
                        nullrgb16 ->
                        nullrgb32 ->
                        pgm -> Dump each image in PGM format to a separate file
                        pgmpipe -> Dump each image in PGM format to a pipe
                        md5 -> Compute MD5 sum of each image
                        fb -> Linux Framebuffer video output
