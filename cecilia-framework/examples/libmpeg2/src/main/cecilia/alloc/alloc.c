/*
 * alloc.c
 * Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
 * Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
 *
 * This file is part of mpeg2dec, a free MPEG-2 video stream decoder.
 * See http://libmpeg2.sourceforge.net/ for updates.
 *
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with mpeg2dec; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifdef COMPILING_WITH_CECILIA
/**
 * Internal component data.
 */
DECLARE_DATA {
    
};
#endif

#include <stdlib.h>
#include <inttypes.h>
#include "cecilia.h"

#include "mpeg2.h"

#ifndef COMPILING_WITH_CECILIA
//extern void * before_malloc_hook (void *_this, unsigned size, mpeg2_alloc_t reason); // Currently unused by the program
extern void * after_malloc_hook (void *_this, void *buf, unsigned size, mpeg2_alloc_t reason);
//extern int before_free_hook (void *_this, void * buf); // Currently unused by the program
//extern int after_free_hook (void *_this, void * buf); // Currently unused by the program
#endif

void * METHOD(alloc, mpeg2_malloc) (void * _this, unsigned size, mpeg2_alloc_t reason) // COMP: EXPORT CALLEDBY(VARIOUS)
{
    char * buf;

    if (size) {
	buf = (char *) malloc (size + 63 + sizeof (void **));
	if (buf) {
	    char * align_buf;

	    align_buf = buf + 63 + sizeof (void **);
	    align_buf -= (intptr_t)align_buf & 63;
	    *(((void **)align_buf) - 1) = buf;
            
	    buf = (char *) CALL(REQUIRED.hook, after_malloc_hook, align_buf, size, reason);
	    if (buf)
		return buf;
	}
    }
    return NULL;
}

void METHOD(alloc, mpeg2_free) (void * _this, void * buf) // COMP: EXPORT CALLEDBY(VARIOUS)
{
    if (buf)
	free (*(((void **)buf) - 1));
}
