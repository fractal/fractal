/*
 * video_out_null.c
 * Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
 * Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
 *
 * This file is part of mpeg2dec, a free MPEG-2 video stream decoder.
 * See http://libmpeg2.sourceforge.net/ for updates.
 *
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with mpeg2dec; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
/** \file video_out_null.c
 * This file contains the null video output plugin (doesn't output anything).
 */

#include "config.h"

#if (defined(COMPILING_WITH_CECILIA) && !defined(VIDEOOUT_SPECIFIC_COMP_ARE_EMBEDDED))
/**
 * Internal component data.
 */
DECLARE_DATA {
    
};
#endif

#include <stdlib.h>
#include <inttypes.h>
#include "cecilia.h"

#include "mpeg2.h"
#include "video_out.h"
#include "mpeg2convert.h"

#if (defined(COMPILING_WITH_CECILIA) && !defined(VIDEOOUT_SPECIFIC_COMP_ARE_EMBEDDED))
#include "video_out_common.c"
#endif

static void null_draw_frame (void * _this, vo_instance_t * instance,
			     uint8_t * const * buf, void * id)
{
}

static vo_instance_t * internal_null_open (int setup (void *, vo_instance_t *, unsigned int,
						 unsigned int, unsigned int,
						 unsigned int,
						 int *),
				      void draw (void *, vo_instance_t *,
						 uint8_t * const *, void *))
{
    vo_instance_t * instance;

    instance = (vo_instance_t *) malloc (sizeof (vo_instance_t));
    if (instance == NULL)
	return NULL;

    instance->setup = setup;
    instance->setup_fbuf = NULL;
    instance->set_fbuf = NULL;
    instance->start_fbuf = NULL;
    instance->draw = draw;
    instance->discard = NULL;
    instance->close = (void (*) (vo_instance_t *)) free;

    return instance;
}

static int null_setup (void * _this, vo_instance_t * instance, unsigned int width,
		       unsigned int height, unsigned int chroma_width,
		       unsigned int chroma_height, int * result)
{
    CALL(REQUIRED.convert, mpeg2convert_setup, MPEG2CONVERT_NULL, 0);
    *result = 1; // Succeeded.
    return 0;
}

vo_instance_t * vo_null_open (void * _this)
{
    return internal_null_open (null_setup, null_draw_frame);
}

vo_instance_t * vo_nullskip_open (void * _this)
{
    return internal_null_open (null_setup, NULL);
}

static int nullslice_setup (void * _this, vo_instance_t * instance, unsigned int width,
			    unsigned int height, unsigned int chroma_width,
			    unsigned int chroma_height,
			    int * result)
{
    CALL(REQUIRED.convert, mpeg2convert_setup, MPEG2CONVERT_NONE, 0);
    *result = 1; // Succeeded.
    return 0;
}

vo_instance_t * vo_nullslice_open (void * _this)
{
    return internal_null_open (nullslice_setup, null_draw_frame);
}

static int nullrgb16_setup (void * _this, vo_instance_t * instance, unsigned int width,
			    unsigned int height, unsigned int chroma_width,
			    unsigned int chroma_height,
			    int * result)
{
    if (CALL(REQUIRED.convert, mpeg2convert_setup, MPEG2CONVERT_RGB, 16) != 0) {
        fprintf (stderr, "16 bpp not supported\n");
        *result = 0;
        return 1;
    }
    *result = 1; // Succeeded.
    return 0;
}

static int nullrgb32_setup (void * _this, vo_instance_t * instance, unsigned int width,
			    unsigned int height, unsigned int chroma_width,
			    unsigned int chroma_height,
			    int * result)
{
    if (CALL(REQUIRED.convert, mpeg2convert_setup, MPEG2CONVERT_RGB, 32) != 0) {
        fprintf (stderr, "32 bpp not supported\n");
        *result = 0;
        return 1;
    }
    *result = 1; // Succeeded.
    return 0;
}

vo_instance_t * vo_nullrgb16_open (void * _this)
{
    return internal_null_open (nullrgb16_setup, null_draw_frame);
}

vo_instance_t * vo_nullrgb32_open (void * _this)
{
    return internal_null_open (nullrgb32_setup, null_draw_frame);
}

#if (defined(COMPILING_WITH_CECILIA) && !defined(VIDEOOUT_SPECIFIC_COMP_ARE_EMBEDDED))
/**
 * Private list of all supported video output drivers.
 */
static vo_driver_t video_out_drivers[] = {
    {"null", vo_null_open},
    {"nullslice", vo_nullslice_open},
    {"nullskip", vo_nullskip_open},
    {"nullrgb16", vo_nullrgb16_open},
    {"nullrgb32", vo_nullrgb32_open},
    {NULL, NULL}
};

char * METHOD (vo, vo_get_default_driver_name) (void * _this)
{
    return video_out_drivers[0].name;
}

vo_driver_t * vo_get_drivers (void)
{
    return video_out_drivers;
}

#endif
