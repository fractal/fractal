/*
 * video_out.c
 * Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
 * Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
 *
 * This file is part of mpeg2dec, a free MPEG-2 video stream decoder.
 * See http://libmpeg2.sourceforge.net/ for updates.
 *
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with mpeg2dec; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
/** \file video_out.c
 * This file contains the list of all video output drivers and a getter for it.
 */

#include "config.h"

#ifdef COMPILING_WITH_CECILIA
/**
 * Internal component data.
 */
DECLARE_DATA {
    
};
#endif

#include <stdlib.h>
#include <inttypes.h>
#include "cecilia.h"

#include "video_out.h"

// This #define is used to indicate the included .c files that they mustn't define their own vo_drivers, vo_default_driver and DECLARE_DATA.
#define VIDEOOUT_SPECIFIC_COMP_ARE_EMBEDDED


#ifdef COMPILING_WITH_CECILIA
#include "videoout/video_out_x11.c"
#include "videoout/video_out_sdl.c"
#include "videoout/video_out_null.c"
#include "videoout/video_out_pgm.c"
#include "videoout/video_out_fb.c"
#else
#include "video_out_x11.c"
#include "video_out_sdl.c"
#include "video_out_null.c"
#include "video_out_pgm.c"
#include "video_out_fb.c"
#endif

#include "video_out_common.c"

/**
 * Private list of all video output drivers.
 */
static vo_driver_t video_out_drivers[] = {
#ifdef LIBVO_XV
    {"xv", vo_xv_open},
    {"xv2", vo_xv2_open},
#endif
#ifdef LIBVO_X11
    {"x11", vo_x11_open},
#endif
#ifdef LIBVO_SDL
    {"sdl", vo_sdl_open},
#endif
    {"null", vo_null_open},
    {"nullslice", vo_nullslice_open},
    {"nullskip", vo_nullskip_open},
    {"nullrgb16", vo_nullrgb16_open},
    {"nullrgb32", vo_nullrgb32_open},
    {"pgm", vo_pgm_open},
    {"pgmpipe", vo_pgmpipe_open},
    {"md5", vo_md5_open},
    {"fb", vo_fb_open},
    {NULL, NULL}
};

char * METHOD (vo, vo_get_default_driver_name) (void * _this)
{
    return video_out_drivers[0].name;
}

vo_driver_t * vo_get_drivers (void)
{
    return video_out_drivers;
}
