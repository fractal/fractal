/**
 * Dummy header to ease transition towards Cecilia.
 */
#ifndef __CECILIA_H__
#define __CECILIA_H__

#ifndef NULL
#define NULL (void *)0
#endif

#define CALLMINE CALL
#define CALL(itf, proc, args...) proc(NULL, ##args)
#define DECLARE_DATA typedef struct dummy
#define METHOD(itf,meth) meth

#endif
