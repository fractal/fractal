/*
 * idct_x86_common.c
 * Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
 * Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
 *
 * This file is part of mpeg2dec, a free MPEG-2 video stream decoder.
 * See http://libmpeg2.sourceforge.net/ for updates.
 *
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with mpeg2dec; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "config.h"

#ifdef ARCH_X86

#ifndef __IDCT_X86_COMMON_C__
#define __IDCT_X86_COMMON_C__

#include <stdlib.h>
#include <inttypes.h>
#include "cecilia.h"

#include "mpeg2.h"
#include "attributes.h"
#include "mmx.h"

#if (defined(COMPILING_WITH_CECILIA) && !defined(IDCT_SPECIFIC_COMP_ARE_EMBEDDED))
void METHOD(idct, mpeg2_idct_init) (void * _this, uint32_t accel)
#else
void mpeg2_idct_mmx_init (void * _this)
#endif
{
    uint8_t * mpeg2_scan_norm = CALL(REQUIRED.common, mpeg2_scannorm);
    uint8_t * mpeg2_scan_alt = CALL(REQUIRED.common, mpeg2_scanalt);
    int i, j;

    /* the mmx/mmxext/sse2 idct uses a reordered input, so we patch scan tables */

    for (i = 0; i < 64; i++) {
	j = mpeg2_scan_norm[i];
	mpeg2_scan_norm[i] = (j & 0x38) | ((j & 6) >> 1) | ((j & 1) << 2);
	j = mpeg2_scan_alt[i];
	mpeg2_scan_alt[i] = (j & 0x38) | ((j & 6) >> 1) | ((j & 1) << 2);
    }
}

#endif

#endif
