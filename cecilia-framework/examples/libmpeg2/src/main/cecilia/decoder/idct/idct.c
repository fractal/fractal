/*
 * idct.c
 * Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
 * Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
 *
 * This file is part of mpeg2dec, a free MPEG-2 video stream decoder.
 * See http://libmpeg2.sourceforge.net/ for updates.
 *
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with mpeg2dec; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "config.h"

#ifdef COMPILING_WITH_CECILIA
/**
 * Internal component data.
 */
DECLARE_DATA {
    
};
#endif

#include <stdlib.h>
#include <inttypes.h>
#include "cecilia.h"

#include "mpeg2.h"
#include "attributes.h"
#include "../common.h"
#include "idct.h"

/* idct main entry point  */
static void (* _mpeg2_idct_copy) (void * _this, int16_t * block, uint8_t * dest, int stride);
static void (* _mpeg2_idct_add) (void * _this, int last, int16_t * block, uint8_t * dest, int stride);

// This #define is used to indicate the included .c files that they mustn't define their own mpeg2_idct_init and DECLARE_DATA.
#define IDCT_SPECIFIC_COMP_ARE_EMBEDDED

#include "idct_mmx.c"
#include "idct_mmxext.c"
#include "idct_sse2.c"
#include "idct_altivec.c"
#include "idct_c.c"

void METHOD(idct, mpeg2_idct_init) (void * _this, uint32_t accel) // COMP: EXPORT HELPER CALLEDBY("mpeg2_slice_init")
{
#ifdef ARCH_X86
    if (accel & MPEG2_ACCEL_X86_SSE2) {
	_mpeg2_idct_copy = mpeg2_idct_copy_sse2;
	_mpeg2_idct_add = mpeg2_idct_add_sse2;
	mpeg2_idct_mmx_init (_this);
    } else if (accel & MPEG2_ACCEL_X86_MMXEXT) {
	_mpeg2_idct_copy = mpeg2_idct_copy_mmxext;
	_mpeg2_idct_add = mpeg2_idct_add_mmxext;
	mpeg2_idct_mmx_init (_this);
    } else if (accel & MPEG2_ACCEL_X86_MMX) {
	_mpeg2_idct_copy = mpeg2_idct_copy_mmx;
	_mpeg2_idct_add = mpeg2_idct_add_mmx;
	mpeg2_idct_mmx_init (_this);
    } else
#endif
#ifdef ARCH_PPC
    if (accel & MPEG2_ACCEL_PPC_ALTIVEC) {
	_mpeg2_idct_copy = mpeg2_idct_copy_altivec;
	_mpeg2_idct_add = mpeg2_idct_add_altivec;
	mpeg2_idct_altivec_init (_this);
    } else
#endif
    {
	_mpeg2_idct_copy = mpeg2_idct_copy_c;
	_mpeg2_idct_add = mpeg2_idct_add_c;
	mpeg2_idct_c_init(_this);
    }
}

void METHOD(idct, mpeg2_idct_copy) (void * _this, int16_t * block, uint8_t * dest, int stride) // COMP: EXPORT CALLEDBY(slice)
{
    (*_mpeg2_idct_copy)(_this, block, dest, stride);
}

void METHOD(idct, mpeg2_idct_add) (void * _this, int last, int16_t * block, uint8_t * dest, int stride) // COMP: EXPORT CALLEDBY(slice)
{
    (*_mpeg2_idct_add)(_this, last, block, dest, stride);
}
