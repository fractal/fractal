/*
 * header.h
 * Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
 * Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
 *
 * This file is part of mpeg2dec, a free MPEG-2 video stream decoder.
 * See http://libmpeg2.sourceforge.net/ for updates.
 *
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with mpeg2dec; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __HEADER_H__
#define __HEADER_H__

void mpeg2_header_state_init (void * _this);
void mpeg2_reset_info (void * _this, void * _info);
int mpeg2_header_sequence (void * _this);
int mpeg2_header_gop (void * _this);
mpeg2_state_t mpeg2_header_picture_start (void * _this);
int mpeg2_header_picture (void * _this);
int mpeg2_header_extension (void * _this);
int mpeg2_header_user_data (void * _this);
void mpeg2_header_sequence_finalize (void * _this);
void mpeg2_header_gop_finalize (void * _this);
void mpeg2_header_picture_finalize (void * _this, uint32_t accels);
mpeg2_state_t mpeg2_header_slice_start (void * _this);
mpeg2_state_t mpeg2_header_end (void * _this);
void mpeg2_set_fbuf (void * _this, int b_type);

#endif
