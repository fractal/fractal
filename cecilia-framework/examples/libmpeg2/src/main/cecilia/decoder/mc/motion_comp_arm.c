/*
 * motion_comp_arm.c
 * Copyright (C) 2004 AGAWA Koji <i (AT) atty (DOT) jp>
 *
 * This file is part of mpeg2dec, a free MPEG-2 video stream decoder.
 * See http://libmpeg2.sourceforge.net/ for updates.
 *
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with mpeg2dec; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "config.h"

#ifdef ARCH_ARM

#if (defined(COMPILING_WITH_CECILIA) && !defined(MC_SPECIFIC_COMP_ARE_EMBEDDED))
/**
 * Internal component data.
 */
DECLARE_DATA {
    
};
#endif

#include <inttypes.h>
#include "cecilia.h"

#include "mpeg2.h"
#include "attributes.h"
#include "mc_types.h"
#include "mc.h"

#include "motion_comp_c_template.c"

/* definitions of the actual mc functions */

// These functions are already defined if motion_comp_*.c are inclued into motion_comp.c.
#ifndef MC_SPECIFIC_COMP_ARE_EMBEDDED
MC_FUNC (avg,o)
MC_FUNC (avg,x)
MC_FUNC (put,y)
MC_FUNC (avg,y)
MC_FUNC (put,xy)
MC_FUNC (avg,xy)
#endif


void MC_put_o_16_arm (uint8_t * dest, const uint8_t * ref, int stride, int height);
asm(
"	.text\n"
"	.align\n"
"	.global MC_put_o_16_arm\n"
"MC_put_o_16_arm:\n"
"	# void func(uint8_t * dest, const uint8_t * ref, int stride, int height)\n"
"	pld [r1]\n"
"	stmfd sp!, {r4-r11, lr}\n"
"	and r4, r1, #3\n"
"	adr r5, MC_put_o_16_arm_align_jt\n"
"	add r5, r5, r4, lsl #2\n"
"	ldr pc, [r5]\n"
"\n"
"MC_put_o_16_arm_align0:\n"
"	ldmia r1, {r4-r7}\n"
"	add r1, r1, r2\n"
"	pld [r1]\n"
"	stmia r0, {r4-r7}\n"
"	subs r3, r3, #1\n"
"	add r0, r0, r2\n"
"	bne MC_put_o_16_arm_align0\n"
"	ldmfd sp!, {r4-r11, pc}\n"
"\n"
".macro	PROC shift\n"
"	ldmia r1, {r4-r8}\n"
"	add r1, r1, r2\n"
"	mov r9, r4, lsr #(\\shift)\n"
"	pld [r1]\n"
"	mov r10, r5, lsr #(\\shift)\n"
"	orr r9, r9, r5, lsl #(32-\\shift)\n"
"	mov r11, r6, lsr #(\\shift)\n"
"	orr r10, r10, r6, lsl #(32-\\shift)\n"
"	mov r12, r7, lsr #(\\shift)\n"
"	orr r11, r11, r7, lsl #(32-\\shift)\n"
"	orr r12, r12, r8, lsl #(32-\\shift)\n"
"	stmia r0, {r9-r12}\n"
"	subs r3, r3, #1\n"
"	add r0, r0, r2\n"
".endm\n"
"\n"
"MC_put_o_16_arm_align1:\n"
"	and r1, r1, #0xFFFFFFFC\n"
"1:	PROC(8)\n"
"	bne 1b\n"
"	ldmfd sp!, {r4-r11, pc}\n"
"MC_put_o_16_arm_align2:\n"
"	and r1, r1, #0xFFFFFFFC\n"
"1:	PROC(16)\n"
"	bne 1b\n"
"	ldmfd sp!, {r4-r11, pc}\n"
"MC_put_o_16_arm_align3:\n"
"	and r1, r1, #0xFFFFFFFC\n"
"1:	PROC(24)\n"
"	bne 1b\n"
"	ldmfd sp!, {r4-r11, pc}\n"
"MC_put_o_16_arm_align_jt:\n"
"	.word MC_put_o_16_arm_align0\n"
"	.word MC_put_o_16_arm_align1\n"
"	.word MC_put_o_16_arm_align2\n"
"	.word MC_put_o_16_arm_align3\n"
);

void MC_put_x_16_arm (uint8_t * dest, const uint8_t * ref, int stride, int height);
asm(
".macro	AVG_PW rW1, rW2\n"
"	mov \\rW2, \\rW2, lsl #24\n"
"	orr \\rW2, \\rW2, \\rW1, lsr #8\n"
"	eor r9, \\rW1, \\rW2\n"
"	and \\rW2, \\rW1, \\rW2\n"
"	and r10, r9, r12\n"
"	add \\rW2, \\rW2, r10, lsr #1\n"
"	and r10, r9, r11\n"
"	add \\rW2, \\rW2, r10\n"
".endm\n"
"\n"
"	.text\n"
"	.align\n"
"	.global MC_put_x_16_arm\n"
"MC_put_x_16_arm:\n"
"	# void func(uint8_t * dest, const uint8_t * ref, int stride, int height)\n"
"	pld [r1]\n"
"	stmfd sp!, {r4-r11,lr}\n"
"	and r4, r1, #3\n"
"	adr r5, MC_put_x_16_arm_align_jt\n"
"	ldr r11, [r5]\n"
"	mvn r12, r11\n"
"	add r5, r5, r4, lsl #2\n"
"	ldr pc, [r5, #4]\n"
"\n"
".macro	ADJ_ALIGN_QW shift, R0, R1, R2, R3, R4\n"
"	mov \\R0, \\R0, lsr #(\\shift)\n"
"	orr \\R0, \\R0, \\R1, lsl #(32 - \\shift)\n"
"	mov \\R1, \\R1, lsr #(\\shift)\n"
"	orr \\R1, \\R1, \\R2, lsl #(32 - \\shift)\n"
"	mov \\R2, \\R2, lsr #(\\shift)\n"
"	orr \\R2, \\R2, \\R3, lsl #(32 - \\shift)\n"
"	mov \\R3, \\R3, lsr #(\\shift)\n"
"	orr \\R3, \\R3, \\R4, lsl #(32 - \\shift)\n"
"	mov \\R4, \\R4, lsr #(\\shift)\n"
"#	and \\R4, \\R4, #0xFF\n"
".endm\n"
"\n"
"MC_put_x_16_arm_align0:\n"
"	ldmia r1, {r4-r8}\n"
"	add r1, r1, r2\n"
"	pld [r1]\n"
"	AVG_PW r7, r8\n"
"	AVG_PW r6, r7\n"
"	AVG_PW r5, r6\n"
"	AVG_PW r4, r5\n"
"	stmia r0, {r5-r8}\n"
"	subs r3, r3, #1\n"
"	add r0, r0, r2\n"
"	bne MC_put_x_16_arm_align0\n"
"	ldmfd sp!, {r4-r11,pc}\n"
"MC_put_x_16_arm_align1:\n"
"	and r1, r1, #0xFFFFFFFC\n"
"1:	ldmia r1, {r4-r8}\n"
"	add r1, r1, r2\n"
"	pld [r1]\n"
"	ADJ_ALIGN_QW 8, r4, r5, r6, r7, r8\n"
"	AVG_PW r7, r8\n"
"	AVG_PW r6, r7\n"
"	AVG_PW r5, r6\n"
"	AVG_PW r4, r5\n"
"	stmia r0, {r5-r8}\n"
"	subs r3, r3, #1\n"
"	add r0, r0, r2\n"
"	bne 1b\n"
"	ldmfd sp!, {r4-r11,pc}\n"
"MC_put_x_16_arm_align2:\n"
"	and r1, r1, #0xFFFFFFFC\n"
"1:	ldmia r1, {r4-r8}\n"
"	add r1, r1, r2\n"
"	pld [r1]\n"
"	ADJ_ALIGN_QW 16, r4, r5, r6, r7, r8\n"
"	AVG_PW r7, r8\n"
"	AVG_PW r6, r7\n"
"	AVG_PW r5, r6\n"
"	AVG_PW r4, r5\n"
"	stmia r0, {r5-r8}\n"
"	subs r3, r3, #1\n"
"	add r0, r0, r2\n"
"	bne 1b\n"
"	ldmfd sp!, {r4-r11,pc}\n"
"MC_put_x_16_arm_align3:\n"
"	and r1, r1, #0xFFFFFFFC\n"
"1:	ldmia r1, {r4-r8}\n"
"	add r1, r1, r2\n"
"	pld [r1]\n"
"	ADJ_ALIGN_QW 24, r4, r5, r6, r7, r8\n"
"	AVG_PW r7, r8\n"
"	AVG_PW r6, r7\n"
"	AVG_PW r5, r6\n"
"	AVG_PW r4, r5\n"
"	stmia r0, {r5-r8}\n"
"	subs r3, r3, #1\n"
"	add r0, r0, r2\n"
"	bne 1b\n"
"	ldmfd sp!, {r4-r11,pc}\n"
"MC_put_x_16_arm_align_jt:\n"
"	.word 0x01010101\n"
"	.word MC_put_x_16_arm_align0\n"
"	.word MC_put_x_16_arm_align1\n"
"	.word MC_put_x_16_arm_align2\n"
"	.word MC_put_x_16_arm_align3\n"
);

static void MC_put_y_16_arm (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_put_y_16_c(dest, ref, stride, height);
}

static void MC_put_xy_16_arm (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_put_xy_16_c(dest, ref, stride, height);
}

void MC_put_o_8_arm (uint8_t * dest, const uint8_t * ref, int stride, int height);
asm(
"	.text\n"
"	.align\n"
"	.global MC_put_o_8_arm\n"
"MC_put_o_8_arm:\n"
"	# void func(uint8_t * dest, const uint8_t * ref, int stride, int height)\n"
"	pld [r1]\n"
"	stmfd sp!, {r4-r10, lr}\n"
"	and r4, r1, #3\n"
"	adr r5, MC_put_o_8_arm_align_jt\n"
"	add r5, r5, r4, lsl #2\n"
"	ldr pc, [r5]\n"
"MC_put_o_8_arm_align0:\n"
"	ldmia r1, {r4-r5}\n"
"	add r1, r1, r2\n"
"	pld [r1]\n"
"	stmia r0, {r4-r5}\n"
"	add r0, r0, r2\n"
"	subs r3, r3, #1\n"
"	bne MC_put_o_8_arm_align0\n"
"	ldmfd sp!, {r4-r10, pc}\n"
"\n"
".macro	PROC8 shift\n"
"	ldmia r1, {r4-r6}\n"
"	add r1, r1, r2\n"
"	mov r9, r4, lsr #(\\shift)\n"
"	pld [r1]\n"
"	mov r10, r5, lsr #(\\shift)\n"
"	orr r9, r9, r5, lsl #(32-\\shift)\n"
"	orr r10, r10, r6, lsl #(32-\\shift)\n"
"	stmia r0, {r9-r10}\n"
"	subs r3, r3, #1\n"
"	add r0, r0, r2\n"
".endm\n"
"\n"
"MC_put_o_8_arm_align1:\n"
"	and r1, r1, #0xFFFFFFFC\n"
"1:	PROC8(8)\n"
"	bne 1b\n"
"	ldmfd sp!, {r4-r10, pc}\n"
"\n"
"MC_put_o_8_arm_align2:\n"
"	and r1, r1, #0xFFFFFFFC\n"
"1:	PROC8(16)\n"
"	bne 1b\n"
"	ldmfd sp!, {r4-r10, pc}\n"
"\n"
"MC_put_o_8_arm_align3:\n"
"	and r1, r1, #0xFFFFFFFC\n"
"1:	PROC8(24)\n"
"	bne 1b\n"
"	ldmfd sp!, {r4-r10, pc}\n"
"\n"
"MC_put_o_8_arm_align_jt:\n"
"	.word MC_put_o_8_arm_align0\n"
"	.word MC_put_o_8_arm_align1\n"
"	.word MC_put_o_8_arm_align2\n"
"	.word MC_put_o_8_arm_align3\n"
);

void MC_put_x_8_arm (uint8_t * dest, const uint8_t * ref, int stride, int height);
asm(
"	.text\n"
"	.align\n"
"	.global MC_put_x_8_arm\n"
"MC_put_x_8_arm:\n"
"	# void func(uint8_t * dest, const uint8_t * ref, int stride, int height)\n"
"	pld [r1]\n"
"	stmfd sp!, {r4-r11,lr}\n"
"	and r4, r1, #3\n"
"	adr r5, MC_put_x_8_arm_align_jt\n"
"	ldr r11, [r5]\n"
"	mvn r12, r11\n"
"	add r5, r5, r4, lsl #2\n"
"	ldr pc, [r5, #4]\n"
"\n"
".macro	ADJ_ALIGN_DW shift, R0, R1, R2\n"
"	mov \\R0, \\R0, lsr #(\\shift)\n"
"	orr \\R0, \\R0, \\R1, lsl #(32 - \\shift)\n"
"	mov \\R1, \\R1, lsr #(\\shift)\n"
"	orr \\R1, \\R1, \\R2, lsl #(32 - \\shift)\n"
"	mov \\R2, \\R2, lsr #(\\shift)\n"
"@	and \\R4, \\R4, #0xFF\n"
".endm\n"
"\n"
"MC_put_x_8_arm_align0:\n"
"	ldmia r1, {r4-r6}\n"
"	add r1, r1, r2\n"
"	pld [r1]\n"
"	AVG_PW r5, r6\n"
"	AVG_PW r4, r5\n"
"	stmia r0, {r5-r6}\n"
"	subs r3, r3, #1\n"
"	add r0, r0, r2\n"
"	bne MC_put_x_8_arm_align0\n"
"	ldmfd sp!, {r4-r11,pc}\n"
"MC_put_x_8_arm_align1:\n"
"	and r1, r1, #0xFFFFFFFC\n"
"1:	ldmia r1, {r4-r6}\n"
"	add r1, r1, r2\n"
"	pld [r1]\n"
"	ADJ_ALIGN_DW 8, r4, r5, r6\n"
"	AVG_PW r5, r6\n"
"	AVG_PW r4, r5\n"
"	stmia r0, {r5-r6}\n"
"	subs r3, r3, #1\n"
"	add r0, r0, r2\n"
"	bne 1b\n"
"	ldmfd sp!, {r4-r11,pc}\n"
"MC_put_x_8_arm_align2:\n"
"	and r1, r1, #0xFFFFFFFC\n"
"1:	ldmia r1, {r4-r6}\n"
"	add r1, r1, r2\n"
"	pld [r1]\n"
"	ADJ_ALIGN_DW 16, r4, r5, r6\n"
"	AVG_PW r5, r6\n"
"	AVG_PW r4, r5\n"
"	stmia r0, {r5-r6}\n"
"	subs r3, r3, #1\n"
"	add r0, r0, r2\n"
"	bne 1b\n"
"	ldmfd sp!, {r4-r11,pc}\n"
"MC_put_x_8_arm_align3:\n"
"	and r1, r1, #0xFFFFFFFC\n"
"1:	ldmia r1, {r4-r6}\n"
"	add r1, r1, r2\n"
"	pld [r1]\n"
"	ADJ_ALIGN_DW 24, r4, r5, r6\n"
"	AVG_PW r5, r6\n"
"	AVG_PW r4, r5\n"
"	stmia r0, {r5-r6}\n"
"	subs r3, r3, #1\n"
"	add r0, r0, r2\n"
"	bne 1b\n"
"	ldmfd sp!, {r4-r11,pc}\n"
"MC_put_x_8_arm_align_jt:\n"
"	.word 0x01010101\n"
"	.word MC_put_x_8_arm_align0\n"
"	.word MC_put_x_8_arm_align1\n"
"	.word MC_put_x_8_arm_align2\n"
"	.word MC_put_x_8_arm_align3\n"
);

static void MC_put_y_8_arm (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_put_y_8_c(dest, ref, stride, height);
}

static void MC_put_xy_8_arm (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_put_xy_8_c(dest, ref, stride, height);
}

static void MC_avg_o_16_arm (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_avg_o_16_c(dest, ref, stride, height);
}

static void MC_avg_x_16_arm (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_avg_x_16_c(dest, ref, stride, height);
}

static void MC_avg_y_16_arm (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_avg_y_16_c(dest, ref, stride, height);
}

static void MC_avg_xy_16_arm (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_avg_xy_16_c(dest, ref, stride, height);
}

static void MC_avg_o_8_arm (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_avg_o_8_c(dest, ref, stride, height);
}

static void MC_avg_x_8_arm (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_avg_x_8_c(dest, ref, stride, height);
}

static void MC_avg_y_8_arm (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_avg_y_8_c(dest, ref, stride, height);
}

static void MC_avg_xy_8_arm (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_avg_xy_8_c(dest, ref, stride, height);
}


MPEG2_MC_EXTERN (arm)

#include "motion_comp_common.c"

#if (defined(COMPILING_WITH_CECILIA) && !defined(MC_SPECIFIC_COMP_ARE_EMBEDDED))
void METHOD(mc, mpeg2_mc_init) (void *_this, uint32_t accel) // COMP: HELPER CALLEDBY("mpeg2_slice_init")
{
    mpeg2_mc_set(&mpeg2_mc_arm);
}
#endif

#endif
