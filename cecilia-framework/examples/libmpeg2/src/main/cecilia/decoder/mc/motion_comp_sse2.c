/*
 * motion_comp_sse2.c
 * Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
 * Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
 *
 * This file is part of mpeg2dec, a free MPEG-2 video stream decoder.
 * See http://libmpeg2.sourceforge.net/ for updates.
 *
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with mpeg2dec; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "config.h"

#ifdef ARCH_X86

#if (defined(COMPILING_WITH_CECILIA) && !defined(MC_SPECIFIC_COMP_ARE_EMBEDDED))
/**
 * Internal component data.
 */
DECLARE_DATA {
    
};
#endif

#include <inttypes.h>
#include "cecilia.h"

#include "mpeg2.h"
#include "attributes.h"
#include "mc_types.h"
#include "mc.h"
#include "mmx.h"

/* SSE2 code */

/* (copy of MMXEXT code with some unrolling to avoid double loads) */

static inline void MC_put1_8_sse2 (int height, uint8_t * dest, const uint8_t * ref,
				   const int stride)
{
    do {
	movq_m2r (*ref, xmm0);
	ref += stride;
	movq_r2m (xmm0, *dest);
	dest += stride;
    } while (--height);
}

static inline void MC_put1_16_sse2 (int height, uint8_t * dest, const uint8_t * ref,
				    const int stride)
{
    do {
	movdqu_m2r (*ref, xmm0);
	ref += stride;
	movdqu_r2m (xmm0, *dest);
	dest += stride;
    } while (--height);
}

static inline void MC_avg1_8_sse2 (int height, uint8_t * dest, const uint8_t * ref,
				   const int stride)
{
    do {
	movq_m2r (*ref, xmm0);
	movq_m2r (*dest, xmm1);
	ref += stride;
	pavgb_r2r (xmm1, xmm0);
	movq_r2m (xmm0, *dest);
	dest += stride;
    } while (--height);
}

static inline void MC_avg1_16_sse2 (int height, uint8_t * dest, const uint8_t * ref,
				    const int stride)
{
    do {
	movdqu_m2r (*ref, xmm0);
	movdqu_m2r (*dest, xmm1);
	ref += stride;
	pavgb_r2r (xmm1, xmm0);
	movdqu_r2m (xmm0, *dest);
	dest += stride;
    } while (--height);
}

static inline void MC_put2_8_sse2 (int height, uint8_t * dest, const uint8_t * ref,
				   const int stride, const int offset)
{
    do {
	movq_m2r (*ref, xmm0);
	movq_m2r (*(ref+offset), xmm1);
	ref += stride;
	pavgb_r2r (xmm1, xmm0);
	movq_r2m (xmm0, *dest);
	dest += stride;
    } while (--height);
}

static inline void MC_put2_8_stride_sse2 (int height, uint8_t * dest, const uint8_t * ref,
					  const int stride)
{
        movq_m2r (*ref, xmm0);
        ref += stride;
        do {
	       movq_m2r (*ref, xmm1);
	       ref += stride;
	       pavgb_r2r (xmm1, xmm0);
	       movq_r2m (xmm0, *dest);
	       dest += stride;

	       movdqa_r2r(xmm1, xmm0);
    } while (--height);
}

static inline void MC_put2_16_sse2 (int height, uint8_t * dest, const uint8_t * ref,
				    const int stride, const int offset)
{
    do {
	movdqu_m2r (*ref, xmm0);
	movdqu_m2r (*(ref+offset), xmm1);
	ref += stride;
	pavgb_r2r (xmm1, xmm0);
	movdqu_r2m (xmm0, *dest);
	dest += stride;
    } while (--height);
}

static inline void MC_put2_16_stride_sse2 (int height, uint8_t * dest, const uint8_t * ref,
					   const int stride)
{
    movdqu_m2r (*ref, xmm0);
    ref += stride;
    do {
	movdqu_m2r (*ref, xmm1);
	ref += stride;
	pavgb_r2r (xmm1, xmm0);
	movdqu_r2m (xmm0, *dest);
	dest += stride;

	movdqa_r2r(xmm1, xmm0);
    } while (--height);
}

static inline void MC_avg2_8_sse2 (int height, uint8_t * dest, const uint8_t * ref,
				   const int stride, const int offset)
{
    do {
	movq_m2r (*ref, xmm0);
	movq_m2r (*(ref+offset), xmm1);
	movq_m2r (*dest, xmm2);
	ref += stride;
	pavgb_r2r (xmm1, xmm0);
	pavgb_r2r (xmm2, xmm0);
	movq_r2m (xmm0, *dest);
	dest += stride;
    } while (--height);
}

static inline void MC_avg2_8_stride_sse2 (int height, uint8_t * dest, const uint8_t * ref,
					  const int stride)
{
    movq_m2r (*ref, xmm0);
    ref += stride;
    do {
	movq_m2r (*ref, xmm1);
	movq_m2r (*dest, xmm2);
	ref += stride;
	pavgb_r2r (xmm1, xmm0);
	pavgb_r2r (xmm2, xmm0);
	movq_r2m (xmm0, *dest);
	dest += stride;

	movdqa_r2r(xmm1, xmm0);
    } while (--height);
}

static inline void MC_avg2_16_sse2 (int height, uint8_t * dest, const uint8_t * ref,
				    const int stride, const int offset)
{
    do {
	movdqu_m2r (*ref, xmm0);
	movdqu_m2r (*(ref+offset), xmm2);
	movdqu_m2r (*dest, xmm4);
	ref += stride;
	pavgb_r2r (xmm2, xmm0);
	pavgb_r2r (xmm4, xmm0);
	movdqu_r2m (xmm0, *dest);
	dest += stride;
    } while (--height);
}

static inline void MC_avg2_16_stride_sse2 (int height, uint8_t * dest, const uint8_t * ref,
					   const int stride)
{
    movdqu_m2r (*ref, xmm0);
    ref += stride;
    do {
	movdqu_m2r (*ref, xmm2);
	movdqu_m2r (*dest, xmm4);
	ref += stride;
	pavgb_r2r (xmm2, xmm0);
	pavgb_r2r (xmm4, xmm0);
	movdqu_r2m (xmm0, *dest);
	dest += stride;

	movdqa_r2r(xmm2, xmm0);
    } while (--height);
}

static sse_t mask_one_sse2 = {uq:{UINT64_C(0x0101010101010101),UINT64_C(0x0101010101010101)}};

static inline void MC_put4_8_sse2 (int height, uint8_t * dest, const uint8_t * ref,
				   const int stride)
{
    movq_m2r (*ref, xmm0);
    movq_m2r (*(ref+1), xmm1);
    movdqa_r2r (xmm0, xmm7);
    pxor_r2r (xmm1, xmm7);
    pavgb_r2r (xmm1, xmm0);
    ref += stride;

    do {
	movq_m2r (*ref, xmm2);
	movdqa_r2r (xmm0, xmm5);

	movq_m2r (*(ref+1), xmm3);
	movdqa_r2r (xmm2, xmm6);

	pxor_r2r (xmm3, xmm6);
	pavgb_r2r (xmm3, xmm2);

	por_r2r (xmm6, xmm7);
	pxor_r2r (xmm2, xmm5);

	pand_r2r (xmm5, xmm7);
	pavgb_r2r (xmm2, xmm0);

	pand_m2r (mask_one_sse2, xmm7);

	psubusb_r2r (xmm7, xmm0);

	ref += stride;
	movq_r2m (xmm0, *dest);
	dest += stride;

	movdqa_r2r (xmm6, xmm7);
	movdqa_r2r (xmm2, xmm0);
	} while (--height);
}

static inline void MC_put4_16_sse2 (int height, uint8_t * dest, const uint8_t * ref,
				    const int stride)
{
    movdqu_m2r (*ref, xmm0);
    movdqu_m2r (*(ref+1), xmm2);
    ref += stride;
    do {
	movdqa_r2r (xmm0, xmm7);
	movdqu_m2r (*ref, xmm3);
	movdqu_m2r (*(ref+1), xmm1);
	ref += stride;
	pxor_r2r (xmm1, xmm7);
	movdqa_r2r (xmm2, xmm6);
	pxor_r2r (xmm3, xmm6);
	pavgb_r2r (xmm1, xmm0);
	pavgb_r2r (xmm3, xmm2);
	por_r2r (xmm6, xmm7);
	movdqa_r2r (xmm0, xmm6);
	pxor_r2r (xmm2, xmm6);
	pand_r2r (xmm6, xmm7);
	pand_m2r (mask_one_sse2, xmm7);
	pavgb_r2r (xmm2, xmm0);
	psubusb_r2r (xmm7, xmm0);
	movdqu_r2m (xmm0, *dest);
	dest += stride;

	movdqa_r2r(xmm3, xmm0);
	movdqa_r2r(xmm1, xmm2);
    } while (--height);
}
 
static inline void MC_avg4_8_sse2 (int height, uint8_t * dest, const uint8_t * ref,
				   const int stride)
{
    movq_m2r (*ref, xmm0);
    movq_m2r (*(ref+1), xmm2);
    ref += stride;
    do {
	movdqa_r2r (xmm0, xmm7);
	movq_m2r (*ref, xmm3);
	movq_m2r (*(ref+1), xmm1);
	ref += stride;
	movdqa_r2r (xmm2, xmm6);
	pxor_r2r (xmm1, xmm7);
	pxor_r2r (xmm3, xmm6);
	pavgb_r2r (xmm1, xmm0);
	pavgb_r2r (xmm3, xmm2);
	por_r2r (xmm6, xmm7);
	movdqa_r2r (xmm0, xmm6);
	pxor_r2r (xmm2, xmm6);
	pand_r2r (xmm6, xmm7);
	pand_m2r (mask_one_sse2, xmm7);
	pavgb_r2r (xmm2, xmm0);
	psubusb_r2r (xmm7, xmm0);
	movq_m2r (*dest, xmm5);
	pavgb_r2r (xmm5, xmm0);
	movq_r2m (xmm0, *dest);
	dest += stride;

	movdqa_r2r(xmm3, xmm0);
	movdqa_r2r(xmm1, xmm2);
    } while (--height);
}
 
static inline void MC_avg4_16_sse2 (int height, uint8_t * dest, const uint8_t * ref,
				     const int stride)
{
    movdqu_m2r (*ref, xmm0);
    movdqu_m2r (*(ref+1), xmm2);
    ref += stride;
    do {
	movdqa_r2r (xmm0, xmm7);
	movdqu_m2r (*ref, xmm3);
	movdqu_m2r (*(ref+1), xmm1);
	ref += stride;
	movdqa_r2r (xmm2, xmm6);
	pxor_r2r (xmm1, xmm7);
	pxor_r2r (xmm3, xmm6);
	pavgb_r2r (xmm1, xmm0);
	pavgb_r2r (xmm3, xmm2);
	por_r2r (xmm6, xmm7);
	movdqa_r2r (xmm0, xmm6);
	pxor_r2r (xmm2, xmm6);
	pand_r2r (xmm6, xmm7);
	pand_m2r (mask_one_sse2, xmm7);
	pavgb_r2r (xmm2, xmm0);
	psubusb_r2r (xmm7, xmm0);
	movdqu_m2r (*dest, xmm5);
	pavgb_r2r (xmm5, xmm0);
	movdqu_r2m (xmm0, *dest);
	dest += stride;

	movdqa_r2r(xmm3, xmm0);
	movdqa_r2r(xmm1, xmm2);
    } while (--height);
}


#define DO_NOT_MIX_MMX_AND_SSE2

static void MC_avg_o_16_sse2 (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_avg1_16_sse2 (height, dest, ref, stride);
}

static void MC_avg_o_8_sse2 (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
#ifndef DO_NOT_MIX_MMX_AND_SSE2
    MC_avg1_8 (height, dest, ref, stride, 0);
#else
    MC_avg1_8_sse2 (height, dest, ref, stride);
#endif
}

static void MC_put_o_16_sse2 (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_put1_16_sse2 (height, dest, ref, stride);
}

static void MC_put_o_8_sse2 (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
#ifndef DO_NOT_MIX_MMX_AND_SSE2
    MC_put1_8 (height, dest, ref, stride);
#else
    MC_put1_8_sse2 (height, dest, ref, stride);
#endif
}

static void MC_avg_x_16_sse2 (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_avg2_16_sse2 (height, dest, ref, stride, 1);
}

static void MC_avg_x_8_sse2 (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
#ifndef DO_NOT_MIX_MMX_AND_SSE2
    MC_avg2_8 (height, dest, ref, stride, 1, 0);
#else
    MC_avg2_8_sse2 (height, dest, ref, stride, 1);
#endif
}

static void MC_put_x_16_sse2 (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_put2_16_sse2 (height, dest, ref, stride, 1);
}

static void MC_put_x_8_sse2 (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
#ifndef DO_NOT_MIX_MMX_AND_SSE2
    MC_put2_8 (height, dest, ref, stride, 1, 0);
#else
    MC_put2_8_sse2 (height, dest, ref, stride, 1);
#endif
}

static void MC_avg_y_16_sse2 (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_avg2_16_stride_sse2 (height, dest, ref, stride);
}

static void MC_avg_y_8_sse2 (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
#ifndef DO_NOT_MIX_MMX_AND_SSE2
    MC_avg2_8 (height, dest, ref, stride, stride, 0);
#else
    MC_avg2_8_stride_sse2 (height, dest, ref, stride);
#endif
}

static void MC_put_y_16_sse2 (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_put2_16_stride_sse2 (height, dest, ref, stride);
}

static void MC_put_y_8_sse2 (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
#ifndef DO_NOT_MIX_MMX_AND_SSE2
    MC_put2_8 (height, dest, ref, stride, stride, 0);
#else
    MC_put2_8_stride_sse2 (height, dest, ref, stride);
#endif
}

static void MC_avg_xy_16_sse2 (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_avg4_16_sse2 (height, dest, ref, stride);
}

static void MC_avg_xy_8_sse2 (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
#ifndef DO_NOT_MIX_MMX_AND_SSE2
    MC_avg4_8 (height, dest, ref, stride, 0);
#else
    MC_avg4_8_sse2 (height, dest, ref, stride);
#endif
}

static void MC_put_xy_16_sse2 (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
    MC_put4_16_sse2 (height, dest, ref, stride);
}

static void MC_put_xy_8_sse2 (uint8_t * dest, const uint8_t * ref, int stride, int height)
{
#ifndef DO_NOT_MIX_MMX_AND_SSE2
    MC_put4_8 (height, dest, ref, stride, 0);
#else
    MC_put4_8_sse2 (height, dest, ref, stride);
#endif
}


MPEG2_MC_EXTERN (sse2)

#include "motion_comp_common.c"

#if (defined(COMPILING_WITH_CECILIA) && !defined(MC_SPECIFIC_COMP_ARE_EMBEDDED))
void METHOD(mc, mpeg2_mc_init) (void *_this, uint32_t accel) // COMP: HELPER CALLEDBY("mpeg2_slice_init")
{
    mpeg2_mc_set(&mpeg2_mc_sse2);
}
#endif

#endif
