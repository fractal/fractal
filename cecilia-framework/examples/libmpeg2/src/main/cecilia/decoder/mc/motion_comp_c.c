/*
 * motion_comp_c.c
 * Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
 * Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
 *
 * This file is part of mpeg2dec, a free MPEG-2 video stream decoder.
 * See http://libmpeg2.sourceforge.net/ for updates.
 *
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with mpeg2dec; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "config.h"

#if (defined(COMPILING_WITH_CECILIA) && !defined(MC_SPECIFIC_COMP_ARE_EMBEDDED))
/**
 * Internal component data.
 */
DECLARE_DATA {
    
};
#endif

#include <inttypes.h>
#include "cecilia.h"

#include "mpeg2.h"
#include "attributes.h"
#include "mc.h"

#include "motion_comp_c_template.c"
/* definitions of the actual mc functions */

MC_FUNC (put,o)
MC_FUNC (avg,o)
MC_FUNC (put,x)
MC_FUNC (avg,x)
MC_FUNC (put,y)
MC_FUNC (avg,y)
MC_FUNC (put,xy)
MC_FUNC (avg,xy)

MPEG2_MC_EXTERN (c)

#include "motion_comp_common.c"

#if (defined(COMPILING_WITH_CECILIA) && !defined(MC_SPECIFIC_COMP_ARE_EMBEDDED))
void METHOD(mc, mpeg2_mc_init) (void *_this, uint32_t accel) // COMP: HELPER CALLEDBY("mpeg2_slice_init")
{
    mpeg2_mc_set(&mpeg2_mc_c);
}
#endif
