/*
 * motion_comp_mmxext.c
 * Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
 * Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
 *
 * This file is part of mpeg2dec, a free MPEG-2 video stream decoder.
 * See http://libmpeg2.sourceforge.net/ for updates.
 *
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with mpeg2dec; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "config.h"

#ifdef ARCH_X86

#if (defined(COMPILING_WITH_CECILIA) && !defined(MC_SPECIFIC_COMP_ARE_EMBEDDED))
/**
 * Internal component data.
 */
DECLARE_DATA {
    
};
#endif

#include <inttypes.h>
#include "cecilia.h"

#include "mpeg2.h"
#include "attributes.h"
#include "mc_types.h"
#include "mc.h"
#include "mmx.h"

#include "motion_comp_mmxext_3dnow_common.c"

static void MC_avg_o_16_mmxext (uint8_t * dest, const uint8_t * ref,
				int stride, int height)
{
    MC_avg1_16 (height, dest, ref, stride, CPU_MMXEXT);
}

static void MC_avg_o_8_mmxext (uint8_t * dest, const uint8_t * ref,
			       int stride, int height)
{
    MC_avg1_8 (height, dest, ref, stride, CPU_MMXEXT);
}

static void MC_put_o_16_mmxext (uint8_t * dest, const uint8_t * ref,
				int stride, int height)
{
    MC_put1_16 (height, dest, ref, stride);
}

static void MC_put_o_8_mmxext (uint8_t * dest, const uint8_t * ref,
			       int stride, int height)
{
    MC_put1_8 (height, dest, ref, stride);
}

static void MC_avg_x_16_mmxext (uint8_t * dest, const uint8_t * ref,
				int stride, int height)
{
    MC_avg2_16 (height, dest, ref, stride, 1, CPU_MMXEXT);
}

static void MC_avg_x_8_mmxext (uint8_t * dest, const uint8_t * ref,
			       int stride, int height)
{
    MC_avg2_8 (height, dest, ref, stride, 1, CPU_MMXEXT);
}

static void MC_put_x_16_mmxext (uint8_t * dest, const uint8_t * ref,
				int stride, int height)
{
    MC_put2_16 (height, dest, ref, stride, 1, CPU_MMXEXT);
}

static void MC_put_x_8_mmxext (uint8_t * dest, const uint8_t * ref,
			       int stride, int height)
{
    MC_put2_8 (height, dest, ref, stride, 1, CPU_MMXEXT);
}

static void MC_avg_y_16_mmxext (uint8_t * dest, const uint8_t * ref,
				int stride, int height)
{
    MC_avg2_16 (height, dest, ref, stride, stride, CPU_MMXEXT);
}

static void MC_avg_y_8_mmxext (uint8_t * dest, const uint8_t * ref,
			       int stride, int height)
{
    MC_avg2_8 (height, dest, ref, stride, stride, CPU_MMXEXT);
}

static void MC_put_y_16_mmxext (uint8_t * dest, const uint8_t * ref,
				int stride, int height)
{
    MC_put2_16 (height, dest, ref, stride, stride, CPU_MMXEXT);
}

static void MC_put_y_8_mmxext (uint8_t * dest, const uint8_t * ref,
			       int stride, int height)
{
    MC_put2_8 (height, dest, ref, stride, stride, CPU_MMXEXT);
}

static void MC_avg_xy_16_mmxext (uint8_t * dest, const uint8_t * ref,
				 int stride, int height)
{
    MC_avg4_16 (height, dest, ref, stride, CPU_MMXEXT);
}

static void MC_avg_xy_8_mmxext (uint8_t * dest, const uint8_t * ref,
				int stride, int height)
{
    MC_avg4_8 (height, dest, ref, stride, CPU_MMXEXT);
}

static void MC_put_xy_16_mmxext (uint8_t * dest, const uint8_t * ref,
				 int stride, int height)
{
    MC_put4_16 (height, dest, ref, stride, CPU_MMXEXT);
}

static void MC_put_xy_8_mmxext (uint8_t * dest, const uint8_t * ref,
				int stride, int height)
{
    MC_put4_8 (height, dest, ref, stride, CPU_MMXEXT);
}


MPEG2_MC_EXTERN (mmxext)

#include "motion_comp_common.c"

#if (defined(COMPILING_WITH_CECILIA) && !defined(MC_SPECIFIC_COMP_ARE_EMBEDDED))
void METHOD(mc, mpeg2_mc_init) (void *_this, uint32_t accel) // COMP: HELPER CALLEDBY("mpeg2_slice_init")
{
    mpeg2_mc_set(&mpeg2_mc_mmxext);
}
#endif

#endif
