/*
 * convert_internal.h
 * Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
 * Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
 *
 * This file is part of mpeg2dec, a free MPEG-2 video stream decoder.
 * See http://libmpeg2.sourceforge.net/ for updates.
 *
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with mpeg2dec; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef CONVERT_INTERNAL_H
#define CONVERT_INTERNAL_H

#include "mpeg2convert.h"

#ifdef COMPILING_WITH_CECILIA
#include "mpeg2_sequence_s.idl.h"
#define mpeg2_sequence_t mpeg2_sequence_s
#endif

typedef struct {
    uint8_t * rgb_ptr;
    int width;
    int field;
    int y_stride, rgb_stride, y_increm, uv_increm, rgb_increm, rgb_slice;
    int chroma420, convert420;
    int dither_offset, dither_stride;
    int y_stride_frame, uv_stride_frame, rgb_stride_frame, rgb_stride_min;
} convert_rgb_t;

typedef void mpeg2convert_copy_t (void * id, uint8_t * const * src,
				  unsigned int v_offset);

mpeg2convert_copy_t * mpeg2convert_rgb_mmxext (int bpp, int mode,
					       const mpeg2_sequence_t * seq);
mpeg2convert_copy_t * mpeg2convert_rgb_mmx (int bpp, int mode,
					    const mpeg2_sequence_t * seq);
mpeg2convert_copy_t * mpeg2convert_rgb_vis (int bpp, int mode,
					    const mpeg2_sequence_t * seq);

#ifndef COMPILING_WITH_CECILIA
int mpeg2convert_convert(void * _this, int stage, void * id, const mpeg2_sequence_t * sequence, int stride,
                 uint32_t accel, void * arg, mpeg2_convert_init_t * result);
int mpeg2convert_has_convert (void * _this);

int mpeg2convert_setup_rgb (void * _this, mpeg2convert_conversion_type_t type, unsigned int bpp);
int mpeg2convert_setup_uyvy (void * _this, mpeg2convert_conversion_type_t type, unsigned int bpp);

int mpeg2convert_convert_rgb (void * _this, int stage, void * id, const mpeg2_sequence_t * sequence, int stride,
                 uint32_t accel, void * arg, mpeg2_convert_init_t * result);
int mpeg2convert_convert_uyvy (void * _this, int stage, void * id, const mpeg2_sequence_t * sequence, int stride,
                 uint32_t accel, void * arg, mpeg2_convert_init_t * result);
                 
void mpeg2convert_do_start_rgb (void * _this, void * id, const mpeg2_fbuf_t * fbuf, const mpeg2_picture_t * picture, const mpeg2_gop_t * gop);
void mpeg2convert_do_start_uyvy (void * _this, void * id, const mpeg2_fbuf_t * fbuf, const mpeg2_picture_t * picture, const mpeg2_gop_t * gop);

void mpeg2convert_do_copy_rgb (void * _this, void * id, uint8_t * /*const*/ * src, const unsigned int v_offset);
void mpeg2convert_do_copy_uyvy (void * _this, void * id, uint8_t * /*const*/ * src, const unsigned int v_offset);

#endif

#endif
