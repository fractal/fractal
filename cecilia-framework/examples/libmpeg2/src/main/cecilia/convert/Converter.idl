package convert;

import mpeg2convert_conversion_type_t;
import mpeg2_sequence_s;
import mpeg2_fbuf_s;
import mpeg2_picture_s;
import mpeg2_gop_s;
import mpeg2_convert_init_t;

/**
 * The Converter interface provides the user-visible methods for converting the decoded data into other output formats.
 */
interface Converter {

    int mpeg2convert_setup(mpeg2convert_conversion_type_t type, unsigned int bpp);

    int mpeg2convert_convert (int stage, any id, const mpeg2_sequence_s * sequence, int stride,
                 uint32_t accel, any arg, mpeg2_convert_init_t * result);

    int mpeg2convert_has_convert();

    void mpeg2convert_do_start(any id, const mpeg2_fbuf_s * fbuf, const mpeg2_picture_s * picture, const mpeg2_gop_s * gop);

    void mpeg2convert_do_copy(any id, uint8_t * /*const*/ * src, unsigned int v_offset);

}
