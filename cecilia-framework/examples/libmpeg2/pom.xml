<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>org.objectweb.fractal.cecilia.examples</groupId>
		<artifactId>cecilia-examples</artifactId>
		<version>2.2-SNAPSHOT</version>
	</parent>

	<artifactId>libmpeg2</artifactId>
	<name>Cecilia libmpeg2 MPEG-2 decoder example (thinkMC language)</name>
	<packaging>cecilia-application</packaging>
	<inceptionYear>2007</inceptionYear>

	<!-- Because of a bug in the maven-site-plugin, the URL of the project MUST
		ends with a '/' which is not the case when using the basic maven
		inheritance mechanism -->
	<url>${cecilia-examples.home.url}/${project.artifactId}/</url>

	<dependencies>
		<!-- transitive dependencies for this "car" will be resolved also -->
		<dependency>
			<groupId>org.objectweb.fractal.cecilia</groupId>
			<artifactId>cecilia-baselib</artifactId>
			<version>${cecilia.version}</version>
			<type>car</type>
		</dependency>
	</dependencies>

	<build>
		<resources>
			<resource>
				<directory>src/main/cecilia</directory>
			</resource>
			<resource>
				<directory>${basedir}/src/site/apt</directory>
				<targetPath>../filtered-site/apt</targetPath>
				<filtering>true</filtering>
			</resource>
			<resource>
				<directory>${basedir}/src/site</directory>
				<targetPath>../filtered-site</targetPath>
				<filtering>false</filtering>
				<excludes>
					<exclude>apt/**/*</exclude>
				</excludes>
			</resource>
		</resources>

		<plugins>
			<plugin>
				<artifactId>maven-resources-plugin</artifactId>
				<executions>
					<execution>
						<id>process-site-resources</id>
						<goals>
							<goal>resources</goal>
						</goals>
						<phase>pre-site</phase>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-site-plugin</artifactId>
				<configuration>
					<siteDirectory>
						${basedir}/target/filtered-site
					</siteDirectory>
				</configuration>
			</plugin>

			<!-- ============================================= -->
			<!-- Invoke the custom bootstrap for GNU autotools -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>exec-maven-plugin</artifactId>
				<configuration>
					<executable>sh</executable>
					<workingDirectory>src/main/cecilia</workingDirectory>
					<arguments>
						<argument>bootstrap</argument>
					</arguments>
				</configuration>
				<executions>
					<execution>
						<id>exec</id>
						<phase>initialize</phase>
						<goals>
							<goal>exec</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!-- ============================== -->
			<!-- Invoke the generated configure -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>make-maven-plugin</artifactId>
				<executions>
					<execution>
						<id>exec</id>
						<configuration>
							<!-- The actual values of destDir and prefix are
							     indifferent, since we're not packaging the
							     program anyway -->
							<destDir>/tmp</destDir>
							<prefix>/tmp</prefix>
							<configureWorkDir>src/main/cecilia</configureWorkDir>
						</configuration>
						<goals>
							<goal>configure</goal>
						</goals>
					</execution>

				</executions>
			</plugin>

			<plugin>
				<groupId>org.objectweb.fractal.cecilia</groupId>
				<artifactId>maven-cecilia-plugin</artifactId>
				<version>${cecilia.version}</version>
				<extensions>true</extensions>
			</plugin>

			<!-- =================================================== -->
			<!-- Cecilia ADL MOJO to invoke the Cecilia ADL Launcher -->
			<plugin>
				<groupId>org.objectweb.fractal.cecilia.toolchain</groupId>
				<artifactId>maven-ceciliaadl-plugin</artifactId>
				<version>${cecilia.version}</version>
				<configuration>
					<target>unix.warnings-libmpeg2</target>
					<adl>mpeg2dec.MPEG2Dec:mpeg2dec</adl>
					<arguments>
						<parameter>
							<name>inc-path</name>
							<value>${basedir}/src/main/cecilia/include</value>
						</parameter>
						<parameter>
							<name>c-flags</name>
							<!-- COMPILING_WITH_CECILIA makes both this build process and make-based process work without syntax errors -->
							<value>-DCOMPILING_WITH_CECILIA</value>
						</parameter>
					</arguments>
				</configuration>
			</plugin>
			<!-- =================================================== -->

			<plugin>
				<artifactId>maven-assembly-plugin</artifactId>
				<configuration>
					<descriptors>
						<descriptor>src/assemble/makefile-release.xml</descriptor>
						<descriptor>src/assemble/mvn-release.xml</descriptor>
					</descriptors>
					<tarLongFileMode>gnu</tarLongFileMode>
				</configuration>
				<executions>
					<!-- Generates binary assemblies at package phase.
						These assemblies are placed in target/site/downloads so
						they are automatically deployed with the maven site
						while performing release -->
					<execution>
						<id>example-packages</id>
						<phase>package</phase>
						<goals>
							<goal>attached</goal>
						</goals>
						<configuration>
							<outputDirectory>
								${basedir}/target/site/downloads
							</outputDirectory>
						</configuration>
					</execution>

					<!-- Generates directory assemblies for integration test -->
					<execution>
						<id>example-dir</id>
						<phase>pre-integration-test</phase>
						<goals>
							<goal>directory-inline</goal>
						</goals>
						<configuration>
							<outputDirectory>${basedir}/target</outputDirectory>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<!-- Unpack the cecilia bin assembly at pre-integration-test
				phase to be able to run the makefile based assembly -->
			<plugin>
				<artifactId>maven-dependency-plugin</artifactId>
				<executions>
					<execution>
						<id>unpack cecilia bin</id>
						<goals>
							<goal>unpack</goal>
						</goals>
						<phase>pre-integration-test</phase>
						<configuration>
							<artifactItems>
								<artifactItem>
									<groupId>
										org.objectweb.fractal.cecilia.toolchain
									</groupId>
									<artifactId>cecilia-adl</artifactId>
									<version>${cecilia.version}</version>
									<!-- Use tar.gz type to ensure that
										extracted 'ceciliac' script is
										executable (zip type doesn't work). -->
									<type>tar.gz</type>
									<classifier>bin</classifier>
								</artifactItem>
							</artifactItems>
							<outputDirectory>
								${basedir}/target/cecilia-bin
							</outputDirectory>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<artifactId>maven-invoker-plugin</artifactId>
				<configuration>
					<projectsDirectory>
						${basedir}/target/${project.build.finalName}-mvn/${project.build.finalName}
					</projectsDirectory>
					<pomIncludes>
						<pomInclude>pom.xml</pomInclude>
					</pomIncludes>
				</configuration>
				<executions>
					<execution>
						<id>test mvn assembly</id>
						<goals>
							<goal>run</goal>
						</goals>
						<phase>integration-test</phase>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>exec-maven-plugin</artifactId>
				<configuration>
					<executable>make</executable>
					<workingDirectory>
						${basedir}/target/${project.build.finalName}-makefile/${project.build.finalName}
					</workingDirectory>
					<arguments>
						<argument>
							CECILIA_ROOT=${basedir}/target/cecilia-bin/cecilia-${cecilia.version}
						</argument>
						<argument>all</argument>
					</arguments>
				</configuration>
				<executions>
					<execution>
						<id>test makefile assembly</id>
						<goals>
							<goal>exec</goal>
						</goals>
						<phase>integration-test</phase>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
</project>
