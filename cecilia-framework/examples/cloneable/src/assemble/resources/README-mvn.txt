################################################################################
#   Cecilia cloneable components example: OVERVIEW
################################################################################

This example shows how to define and dynamically instantiate Fractal components
using "cloneable components".


################################################################################
#   LAUNCHING THE EXAMPLE WITH MAVEN
################################################################################
From the command line, simply type:
 $ mvn clean compile
  
  
Afterwards, execute the binary "cloneable" file with:
 $ target/build/obj/cloneable
