################################################################################
#   Cecilia cloneable components example: OVERVIEW
################################################################################

This example shows how to define and dynamically instantiate Fractal components
using "cloneable components".


################################################################################
#   LAUNCHING THE EXAMPLE WITH MAKE
################################################################################
From the command line, simply type:
 $ make
  
  
Afterwards, execute the binary "cloneable" file with:
 $ build/obj/cloneable
