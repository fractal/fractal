
// START SNIPPET: Content
/* all the includes *except* cecilia.h */
#include <stdio.h>

/** Component internal data. */
DECLARE_DATA {
    jint componentNumber;
} ;

/* Include cecilia.h. Must be included after the DECLARE_DATA */
#include <cecilia.h>

// -----------------------------------------------------------------------------
// Implementation of the 'Itf' interface.
// -----------------------------------------------------------------------------

void METHOD(i, setMyComponentNumber) (void *_this, jint val) {
    DATA.componentNumber = val;
}

jint METHOD(i, getMyComponentNumber) (void *_this) {
    return DATA.componentNumber;
}

void METHOD(i, eatCPU) (void * _this) {
    volatile int i = 0;
    while (i < (1000 + DATA.componentNumber*10)) {
        i++;
    }
}
