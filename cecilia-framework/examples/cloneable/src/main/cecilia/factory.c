
// START SNIPPET: Content
/* all the includes *except* cecilia.h */
#include <stdio.h>

/** Component internal data, empty here. */
DECLARE_DATA {
} ;

/* Include cecilia.h. Must be included after the DECLARE_DATA */
#include <cecilia.h>
/* Include used interface definitions */
#include <Itf.idl.h>

// -----------------------------------------------------------------------------
// Implementation of the 'factory' interface.
// -----------------------------------------------------------------------------

int METHOD(mycomponentFactory, createMyComponentInstance) (void * _this, fractal_api_Component * mycomponentCI) {
    fractal_api_Component mycomponent;
    Itf itf;
    jint result;

    result = CALL(REQUIRED.mycomponent_factory, newFcInstance, &mycomponent);

    /* Instanciation can fail due to memory exhaustion */
    if (result != fractal_api_ErrorConst_OK) {
        return MyComponentFactory_CREATE_COMPONENT_ERROR_MEM;
    }

    result = CALL(mycomponent, getFcInterface, "i1", (void *)&itf);
    if (result == fractal_api_ErrorConst_OK) {
        CALL(itf, setMyComponentNumber, ++ATTRIBUTES.count);
    }
    else if (result == fractal_api_ErrorConst_NO_SUCH_INTERFACE) {
        fprintf(stderr, "ERR: no interface 'i1' on component !\n");
    }
    else {
        fprintf(stderr, "ERR: getFcInterface returned a strange return value !\n");
    }

    
    result = CALL(mycomponent, getFcInterface, "i2", (void *)&itf);
    if (result == fractal_api_ErrorConst_OK) {
        CALL(itf, setMyComponentNumber, ++ATTRIBUTES.count);
    }
    else if (result == fractal_api_ErrorConst_NO_SUCH_INTERFACE) {
        fprintf(stderr, "ERR: no interface 'i2' on component !\n");
    }
    else {
        fprintf(stderr, "ERR: getFcInterface returned a strange return value !\n");
    }

    
    result = CALL(mycomponent, getFcInterface, "i3", (void *)&itf);
    if (result == fractal_api_ErrorConst_OK) {
        CALL(itf, setMyComponentNumber, ++ATTRIBUTES.count);
    }
    else if (result == fractal_api_ErrorConst_NO_SUCH_INTERFACE) {
        fprintf(stderr, "ERR: no interface 'i3' on component !\n");
    }
    else {
        fprintf(stderr, "ERR: getFcInterface returned a strange return value !\n");
    }
    
    *mycomponentCI = mycomponent;
    return MyComponentFactory_CREATE_COMPONENT_OK;
}

void METHOD(mycomponentFactory, destroyMyComponentInstance) (void * _this, fractal_api_Component mycomponentCI) {
    CALL(REQUIRED.mycomponent_factory, destroyFcInstance, mycomponentCI);
}
