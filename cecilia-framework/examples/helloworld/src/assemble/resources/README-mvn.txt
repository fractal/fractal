################################################################################
#   Cecilia Helloworld example: OVERVIEW
################################################################################

This example shows a very basic Fractal application with a main composite 
component containing a client and a server.


################################################################################
#   LAUNCHING THE BASE EXAMPLE WITH MAVEN
################################################################################
From the command line, simply type:
 $ mvn clean compile
  
  
Afterwards, execute the binary "helloworld" file with:
 $ target/build/obj/helloworld

 
You should see the client calling 2 times the print method on the server.


################################################################################
#   LAUNCHING THE "COUNTER" EXAMPLE WITH MAVEN
################################################################################
 
You can also compile and run another version of the Helloworld example, which
contains a server component that counts the number of times its print method is
invoked. 
This is due to the fact that the example wants to show how to declare and use
instance variables in the server component, and specifically there is a instance
field storing the number of times the "print" method has been called on the 
server component instance.

To compile this example launch:
 $ mvn -Pcounter clean compile
 
and then execute the binary "helloworld-with-counter" with:
 $ target/build/obj/helloworld-with-counter
