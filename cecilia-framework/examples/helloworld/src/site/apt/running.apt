    ----------
    Compile and run the Hello-World example
    ----------

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Overview

  The Hello-World example, as any Cecilia application, can be compiled using two
  different build systems: Makefile or {{{maven.apache.org}Maven}}. This page 
  provides packages for both build systems and describes how to use them

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Download

  To get the Hello-World example based on <<Makefile>>, download one of the 
  following packages:

    * {{{downloads/${project.build.finalName}-makefile.tar.gz}${project.build.finalName}-makefile.tar.gz}}

    * {{{downloads/${project.build.finalName}-makefile.zip}${project.build.finalName}-makefile.zip}}

  To get the Hello-World example based on <<Maven>>, download one of the 
  following packages:

    * {{{downloads/${project.build.finalName}-mvn.tar.gz}${project.build.finalName}-mvn.tar.gz}}

    * {{{downloads/${project.build.finalName}-mvn.zip}${project.build.finalName}-mvn.zip}}


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Build the Hello-World example with Makefile

  This section describes how to organize sources of the Hello-World example and 
  how to write the associated Makefile.

  Before building the Hello-World example using Makefile, you must download and
  install the Cecilia compiler. See the
  {{{${cecilia.home.url}/toolchain-parent/cecilia-adl/ceciliac.html}installation page}}
  for more details on how to install the Cecilia compiler. 
  
  To be sure that your installation is correct, simply run the following command:

+------------------------------------------------------------------------------+
$ ceciliac
+------------------------------------------------------------------------------+

  This should print a message detailing command-line arguments. If it is not the
  case, checks that the <CECILIA_ROOT><<</bin>>> folder is in your <<<PATH>>>.


* Organization of source files

  If you download and unpack a makefile-based package of the Hello-World 
  example, you can see that sources of the application are located in the 
  <<<src/helloworld>>> sub-directory while the <<<Makefile>>> is in the root 
  directory.
  
  Note that, in the "<<<src/helloworld>>>" path, "<<<src>>>" is the <<source path>>
  whereas "<<<helloworld>>>" is the <<package name>>. This means that the 
  <<<src-path>>> argument passed to the <<<ceciliac>>> compiler will be 
  "<<<src>>>". This also means that the <<<src/helloworld/Helloworld.fractal>>>
  file will be known as <<<helloworld.Helloworld>>> by the compiler.


* Writing the Makefile

  First of all, note that the Cecilia compiler manages the full C compilation 
  process automatically (compilation of C files, linking, incremental and
  conditional re-compilation)

  So the Makefile is simply used as a convenient way to run the <<<ceciliac>>>
  command. It is also used to create and clean build directories.

  The Makefile rule to compile the <<<helloworld.Helloworld>>> ADL is the 
  following:

%{snippet|id=helloworld-rule|url=file://${basedir}/src/assemble/resources/Makefile}

  The <<<$(CECILIAC)>>> variable is initialize to the <<<ceciliac>>> command. 
  (See Complete Makefile section for details on how this variable is 
  initialized).
  
  Arguments passed to the Cecilia compiler are the following:
  
  * <<<-src-path=src>>> specifies the path of the directory containing application 
    sources

  * <<<-o=build/helloworld>>> specifies the directory into which generated files 
    (both generated source files and compiled files) will be placed. This 
    directory must exist prior to the execution of the Cecilia compiler. That's
    why the <<<helloworld>>> rule depends on <<<build/helloworld>>> which is 
    resolved by the following rule

+------------------------------------------------------------------------------+
build/helloworld:
	mkdir -p build/helloworld
+------------------------------------------------------------------------------+


  * <<<-t=unix.warnings>>> specifies the target descriptor to use. Among other things, 
    this target descriptor specify some <c-flags>. See 
    {{{${cecilia.home.url}/toolchain/target-descriptor/index.html}target-descriptor}} 
    page to have details on target-descriptors and
    {{{${cecilia.home.url}/runtime/cecilia-baselib/index.html}this page}} to 
    have details on the <<<unix.warnings>>> <target-descriptor>.
    
  * <<<helloworld.Helloworld:helloworld>>>
    specifies both the ADL to compile and the name of the output file (
    <<<helloworld>>> here).

    Note that the name of the ADL that is actually compiled is not 
    <<<helloworld.Helloworld>>> but 
    <<<unix.boot.BootstrappedApplication(helloworld.Helloworld)>>> thanks to 
    the <<<unix.warnings>>> <target-descriptor>. Indeed, since the 
    <<<helloworld.Helloworld>>> ADL does not contain the definition of the 
    <<<main>>> C function, the Hello-World component
    must be wrapped in an appropriate <<bootstrap component>>. This bootstrap 
    component defines the entry-point of the executable and call the <<<main>>>
    interface of the wrapped component. The bootstrap component may differ from
    one target platform to another. The 
    {{{${cecilia.home.url}/runtime/cecilia-baselib/index.html}Cecilia base library}} 
    provides a bootstrap component that can be used for application running on
    Unix, Linux, MacOS or Windows/Cygwin (this library is packaged with the 
    Cecilia compiler and is implicitly included in the compiler source path).
    

* Compiling and Running Hello-World

  To compile the Hello-World example, issue the following command:

+------------------------------------------------------------------------------+
$ make helloworld
+------------------------------------------------------------------------------+

  The <<<helloworld>>> executable is located in the <<<build/helloworld/obj/>>>
  directory. To run it issue the following command:

+------------------------------------------------------------------------------+
$ build/helloworld/obj/helloworld
+------------------------------------------------------------------------------+


* Complete Makefile

%{snippet|id=Content|url=file://${basedir}/src/assemble/resources/Makefile}

  <<Things to note>>:
  
  * It is a good practice to use different build directory for each compiled 
    ADL. Otherwise some files generated by the compilation of one ADL may be 
    overwritten by files generated by the compilation of another ADL.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Build the Hello-World example with Maven

  This section describes how to organize sources of the Hello-World example and 
  how to write the associated Maven <<<pom.xml>>> file.

  Before building the Hello-World example using Maven, you must install and 
  configure it. See the {{{http://fractal.objectweb.org/cecilia-site/maven-parent/config.html}Maven set-up}}
  pages for more details.

  Note that, when you use the maven build system, it is not necessary to
  download or install the Cecilia compiler manually, this will be performed 
  automatically by Maven.


* Organization of source files

  If you download and unpack a maven-based package of the Hello-World 
  example, you can see that sources of the application are located in the 
  <<<src/main/cecilia/helloworld>>> sub-directory while the root directory 
  contains a <<<pom.xml>>> file.
  
  When using Maven to build Cecilia applications, sources (<<<.fractal>>>, 
  <<<.c>>> and <<<.idl>>> files) must be placed in the <<<src/main/cecilia>>>
  folder.

* Writing the <<<pom.xml>>>

  In addition to traditional POM informations (<<<groupId>>>, <<<artifactId>>>, 
  ...), the POM file mainly contains:
  
  * The specification of the <<<cecilia-application>>> packaging type:
  
~~ WARNING the following snippet only works if pre-integration-test phase has 
~~ been executed before the site generation.
%{snippet|id=packaging|url=file://${basedir}/target/${project.build.finalName}-mvn/${project.build.finalName}/pom.xml}

  * The declaration of the dependency to the <<<cecilia-baselib>>> package:

~~ WARNING the following snippet only works if pre-integration-test phase has 
~~ been executed before the site generation.
%{snippet|id=dependencies|url=file://${basedir}/target/${project.build.finalName}-mvn/${project.build.finalName}/pom.xml}

  * The specification of the Cecilia maven extension in the plugin section:

~~ WARNING the following snippet only works if pre-integration-test phase has 
~~ been executed before the site generation.
%{snippet|id=cecilia-extension|url=file://${basedir}/target/${project.build.finalName}-mvn/${project.build.finalName}/pom.xml}

  * The configuration of the maven cecilia-adl plugin to specify the ADL to 
    compile:

~~ WARNING the following snippet only works if pre-integration-test phase has 
~~ been executed before the site generation.
%{snippet|id=cecilia-adl|url=file://${basedir}/target/${project.build.finalName}-mvn/${project.build.finalName}/pom.xml}

* Compiling and Running Hello-World

  To compile the Hello-World example, issue the following command:

+------------------------------------------------------------------------------+
$ mvn compile
+------------------------------------------------------------------------------+

  The <<<helloworld>>> executable is located in the <<<target/build/obj>>>
  directory. To run it issue the following command:

+------------------------------------------------------------------------------+
$ target/build/obj/helloworld
+------------------------------------------------------------------------------+

* Complete <<<pom.xml>>>

~~ WARNING the following snippet only works if pre-integration-test phase has 
~~ been executed before the site generation.
%{snippet|id=Content|url=file://${basedir}/target/${project.build.finalName}-mvn/${project.build.finalName}/pom.xml}
