################################################################################
#   Cecilia Cloneable Components example: OVERVIEW
################################################################################

This example shows a basic Fractal application with a factory of composite
components.


################################################################################
#   LAUNCHING THE EXAMPLE WITH MAVEN
################################################################################
From the command line, simply type:
 $ mvn clean compile

Afterwards, execute the binary "cloneable" file with:
 $ target/build/obj/cloneable

You should see 100 lines containing the integers from 1 to 300, 3 integers
on each line.


################################################################################
#   LAUNCHING THE EXAMPLE WITH MAKE
################################################################################
From the command line, simply type:
 $ ceciliac

Afterwards, execute the binary "cloneable" file with:
 $ target/build/obj/cloneable

You should see 100 lines containing the integers from 1 to 300, 3 integers
on each line.
