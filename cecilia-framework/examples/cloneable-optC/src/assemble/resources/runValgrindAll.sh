#!/bin/sh
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./cloneable_noopt
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./cloneable
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./cloneable_static
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./cloneable_merge
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./cloneable_all
