#!/bin/sh
# The base definition has merge="true", so we'll disable it if we don't want it.
# Only singleton optimization.
mvn clean compile
mv target/build/obj/cloneable* .
# No optimizations at all.
mvn clean compile -Pnoopt
mv target/build/obj/cloneable* .
# Singleton + dynamic="false" optimizations.
mvn clean compile -Pstatic
mv target/build/obj/cloneable* .
# Singleton + merge optimizations.
mvn clean compile -Pmerge
mv target/build/obj/cloneable* .
# Singleton, merge and _this optimizations.
mvn clean compile -Pall
mv target/build/obj/cloneable* .
# Singleton, merge and _this optimizations, no bootstrap code: IMPOSSIBLE.
# The application _really_ needs a bit of initialization.
#patch -p1 < src/main/cecilia/main_method_cheat.patch
#MAVEN_OPTS="-DCECILIA_ENABLE_THIS_OPTIMIZATION=true" mvn clean compile -Pallnobootstrap
#patch -p1 -R < src/main/cecilia/main_method_cheat.patch
#mv target/build/obj/cloneable* .
# Obviously, there's no plain C version...
# Strip and dissect binaries.
for file in `ls cloneable* | grep -v stripped | grep -v dissect`; do
cp $file "${file}_stripped"
strip -g "${file}_stripped"
size -B "${file}_stripped" > "${file}_dissect".txt
size -A "${file}_stripped" >> "${file}_dissect".txt
objdump -x -d -t -r -s "${file}_stripped" >> "${file}_dissect".txt
echo "$file:"
egrep "^.(rodata|data|text|bss)" "${file}_dissect".txt
done
