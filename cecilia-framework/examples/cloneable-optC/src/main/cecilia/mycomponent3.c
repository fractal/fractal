
// START SNIPPET: Content
#include <stdio.h>

/** Declare component internal data */
typedef struct {
  jint componentNumber;
} PRIVATE_DATA;

// -----------------------------------------------------------------------------
// Implementation of the 'Itf' interface.
// -----------------------------------------------------------------------------

void METH(setMyComponentNumber) (jint val) {
    DATA.componentNumber = val;
}

jint METH(getMyComponentNumber) () {
    return DATA.componentNumber;
}

void METH(eatCPU) () {
    volatile int i = 0;
    while (i < (1000 + DATA.componentNumber*10)) {
        i++;
    }
}
