
// START SNIPPET: Content
#include <stdio.h>

/** Declare component internal data */
typedef struct {
  // empty
} PRIVATE_DATA;

/* Include used interface definitions */
#include <Itf.idl.h>

// -----------------------------------------------------------------------------
// Implementation of the 'main' boot interface.
// -----------------------------------------------------------------------------

// int main(int argc, string[] argv)
int METH(main) (int argc, char *argv[]) {
    juint i;
    jint val;
    jint result;
    fractal_api_Component mycomponent;
    Itf itf;

    printf("Entering main\n");
    // Currently, the toolchain does not support optimization of components
    // exposing attributes. The attributes have therefore been removed.
    for (i = 0; i < ATTR(numberOfComponents); i++) {
        result = CLIENT(factory, createMyComponentInstance) ((void *)&mycomponent);
        if (result != MyComponentFactory_CREATE_COMPONENT_OK) {
            fprintf(stderr,"Couldn't create component !\n");
        }
        else {
            // Retrieve and call 1st interface of MyComponent instance.
            result = CALL(mycomponent, getFcInterface) ("i1", (void *)&itf);
            if (result == fractal_api_ErrorConst_OK) {
                val = CALL(itf, getMyComponentNumber) ();

                printf("\t%d\t",val);

                // Call the workload.
                CALL(itf, eatCPU) ();
            }
            else if (result == fractal_api_ErrorConst_NO_SUCH_INTERFACE) {
                fprintf(stderr, "ERR: no interface 'i1' on MyComponent instance !\n");
            }
            else {
                fprintf(stderr, "ERR: getFcInterface returned a strange return value !\n");
            }

            // Retrieve and call 2nd interface of MyComponent instance.
            result = CALL(mycomponent, getFcInterface) ("i2", (void *)&itf);
            if (result == fractal_api_ErrorConst_OK) {
                val = CALL(itf, getMyComponentNumber) ();

                printf("%d\t",val);

                // Call the workload.
                CALL(itf, eatCPU) ();
            }
            else if (result == fractal_api_ErrorConst_NO_SUCH_INTERFACE) {
                fprintf(stderr, "ERR: no interface 'i2' on MyComponent instance !\n");
            }
            else {
                fprintf(stderr, "ERR: getFcInterface returned a strange return value !\n");
            }

            // Retrieve and call 3rd interface of MyComponent instance.
            result = CALL(mycomponent, getFcInterface) ("i3", (void *)&itf);
            if (result == fractal_api_ErrorConst_OK) {
                val = CALL(itf, getMyComponentNumber) ();

                printf("%d\t",val);

                // Call the workload.
                CALL(itf, eatCPU) ();
            }
            else if (result == fractal_api_ErrorConst_NO_SUCH_INTERFACE) {
                fprintf(stderr, "ERR: no interface 'i3' on MyComponent instance !\n");
            }
            else {
                fprintf(stderr, "ERR: getFcInterface returned a strange return value !\n");
            }

            // Flush stdout by printing a newline.
            printf("\n");

            // Don't forget to destroy the component :-)
            CLIENT(factory, destroyMyComponentInstance) (mycomponent);
        }
    }
    printf("Exiting main\n");

    return 0;
}
