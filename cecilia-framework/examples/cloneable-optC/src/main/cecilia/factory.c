
// START SNIPPET: Content
#include <stdio.h>

/** Declare component internal data */
typedef struct {
} PRIVATE_DATA;

/* Include used interface definitions */
#include <Itf.idl.h>

// -----------------------------------------------------------------------------
// Implementation of the 'factory' interface.
// -----------------------------------------------------------------------------

int METH(createMyComponentInstance) (fractal_api_Component * mycomponentCI) {
    fractal_api_Component mycomponent;
    Itf itf;
    jint result;

    result = CLIENT(mycomponent_factory, newFcInstance) (&mycomponent);

    /* Instanciation can fail due to memory exhaustion */
    if (result != fractal_api_ErrorConst_OK) {
        return MyComponentFactory_CREATE_COMPONENT_ERROR_MEM;
    }

    result = CALL(mycomponent, getFcInterface) ("i1", (void *)&itf);
    if (result == fractal_api_ErrorConst_OK) {
        // Currently, the toolchain does not support optimization of components
        // exposing attributes. The attributes have therefore been removed.
        CALL(itf, setMyComponentNumber) (++ATTR(count));
    }
    else if (result == fractal_api_ErrorConst_NO_SUCH_INTERFACE) {
        fprintf(stderr, "ERR: no interface 'i1' on component !\n");
    }
    else {
        fprintf(stderr, "ERR: getFcInterface returned a strange return value !\n");
    }

    
    result = CALL(mycomponent, getFcInterface) ("i2", (void *)&itf);
    if (result == fractal_api_ErrorConst_OK) {
        // Currently, the toolchain does not support optimization of components
        // exposing attributes. The attributes have therefore been removed.
        CALL(itf, setMyComponentNumber) (++ATTR(count));
    }
    else if (result == fractal_api_ErrorConst_NO_SUCH_INTERFACE) {
        fprintf(stderr, "ERR: no interface 'i2' on component !\n");
    }
    else {
        fprintf(stderr, "ERR: getFcInterface returned a strange return value !\n");
    }

    
    result = CALL(mycomponent, getFcInterface) ("i3", (void *)&itf);
    if (result == fractal_api_ErrorConst_OK) {
        // Currently, the toolchain does not support optimization of components
        // exposing attributes. The attributes have therefore been removed.
        CALL(itf, setMyComponentNumber) (++ATTR(count));
    }
    else if (result == fractal_api_ErrorConst_NO_SUCH_INTERFACE) {
        fprintf(stderr, "ERR: no interface 'i3' on component !\n");
    }
    else {
        fprintf(stderr, "ERR: getFcInterface returned a strange return value !\n");
    }
    
    *mycomponentCI = mycomponent;
    return MyComponentFactory_CREATE_COMPONENT_OK;
}

void METH(destroyMyComponentInstance) (fractal_api_Component mycomponentCI) {
    CLIENT(mycomponent_factory, destroyFcInstance) (mycomponentCI);
}
