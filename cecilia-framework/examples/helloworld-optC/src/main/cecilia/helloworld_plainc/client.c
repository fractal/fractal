/**
 * Cecilia, an implementation of the Fractal component model in C.
 *
 * Copyright (C) 2007 ST Microelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Matthieu Leclercq
 */
// START SNIPPET: Content
/** Declare component internal data */
typedef struct {
  // empty
} PRIVATE_DATA;

#include "functions.h"

// -----------------------------------------------------------------------------
// Implementation of the boot interface.
// -----------------------------------------------------------------------------

// int main(int argc, string[] argv)
int main (int argc, char *argv[]) {

  // call the 'print' method of the 's' client interface.
  helloworld_s_print ("hello world");
  
  // call again the same method to look at invocation count
  helloworld_s_print ("hello world");
  
  return 0;
}
