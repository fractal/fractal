/**
 * Cecilia, an implementation of the Fractal component model in C.
 *
 * Copyright (C) 2007 ST Microelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Matthieu Leclercq
 */
// START SNIPPET: Content
#include <stdio.h>

#define HEADER "-> "
#define COUNT (1)

// -----------------------------------------------------------------------------
// Implementation of the service interface.
// -----------------------------------------------------------------------------

// void print(string msg)
void helloworld_s_print(const char *msg) 
{
  int i;
  
  printf("Server: begin printing...\n");
  for (i = 0; i < COUNT; ++i) {
    printf("%s%s\n", HEADER, msg);
  }
  
  printf("Server: print done\n");
}
