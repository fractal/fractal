<?xml version="1.0" encoding="ISO-8859-1" ?>
<!-- START SNIPPET: Content -->
<!DOCTYPE definition PUBLIC "-//objectweb.org//DTD Fractal ADL 2.0//EN"
  "classpath://org/objectweb/fractal/cecilia/adl/parser/xml/cecilia.dtd">

<!--=========================================================================-->
<!-- Helloworld example                                                      -->
<!--                                                                         -->
<!--   This ADL defines a composite component 'Helloworld' containing two    -->
<!--   sub components 'client' and 'server'. The 'client' provides an        -->
<!--   interface 'r' and is bound to an interface 'r' provided by the        -->
<!--   'server'.                                                             -->
<!--
       XXX if you want to merge something, you have to leave this composite
       "wrapper" because of the way the bootstrap component works (it
       expects a component providing a lifecycle-controller, which would not
       be the case if the top level composite component is merged as a
       single primitive *not* providing any lifecycle-controller).
                                                                             -->
<!--=========================================================================-->
<definition name="helloworld.HelloworldAllNoBootstrap">

  <component name="main_component" merge="true">
    <!--=====================================================================-->
    <!-- The client sub-component                                            -->
    <!--=====================================================================-->
    <component name="client">

      <!-- Interfaces of the client component -->
      <interface name="s" role="client" signature="helloworld.Service" />

      <!-- Implementation of the client component -->
      <content class="helloworld.client" language="optC" />
    </component>

    <!--=====================================================================-->
    <!-- The server sub-component                                            -->
    <!--=====================================================================-->
    <component name="server">

      <!-- Interface of the server component -->
      <interface name="s" role="server" signature="helloworld.Service" />

      <!-- Implementation of the server component -->
      <content class="helloworld.server" language="optC" />

      <!-- Attributes of the server component -->
      <attributes signature="helloworld.ServiceAttributes">
        <attribute name="header" value="-> " />
        <attribute name="count" value="1" />
      </attributes>
    </component>

    <!--=====================================================================-->
    <!-- Bindings                                                            -->
    <!--=====================================================================-->

    <!-- 
      the client interface 's' of the 'client' component is bound to the server 
      interface 's' of the 'server' component.
    -->
    <binding client="client.s" server="server.s" dynamic="false"/>
  </component>
  
</definition>
