#!/bin/sh
# The base definition has merge="true", so we'll disable it if we don't want it.
# Only singleton optimization (default).
mvn clean compile
mv target/build/obj/helloworld* .
# No optimizations at all.
mvn clean compile -Pnoopt
mv target/build/obj/helloworld* .
# Singleton + dynamic="false" optimizations.
mvn clean compile -Pstatic
mv target/build/obj/helloworld* .
# Singleton + merge optimizations.
mvn clean compile -Pmerge
mv target/build/obj/helloworld* .
# Singleton, merge, dynamic="false" and _this optimizations.
mvn clean compile -Pall
mv target/build/obj/helloworld* .
# Singleton, merge, dynamic="false" and _this optimizations, no bootstrap code.
patch -p1 < src/main/cecilia/helloworld/main_method_cheat.patch
mvn clean compile -Pallnobootstrap
patch -p1 -R < src/main/cecilia/helloworld/main_method_cheat.patch
mv target/build/obj/helloworld* .
# Plain C version.
gcc -g3 -Os -Wall -W -Wshadow -Wredundant-decls -Wno-unreachable-code -Wpointer-arith -Wno-unused-parameter -Wwrite-strings -ffunction-sections -fdata-sections -Wl,--gc-sections src/main/cecilia/helloworld_plainc/*.c -I/src/main/cecilia/helloworld_plainc -o helloworld_plainc
# Strip and dissect binaries
for file in `ls helloworld* | grep -v stripped | grep -v dissect`; do
cp $file "${file}_stripped"
strip -g "${file}_stripped"
size -B "${file}_stripped" > "${file}_dissect".txt
size -A "${file}_stripped" >> "${file}_dissect".txt
objdump -x -d -t -r -s "${file}_stripped" >> "${file}_dissect".txt
echo "$file:"
egrep "^.(rodata|data|text|bss)" "${file}_dissect".txt
done
