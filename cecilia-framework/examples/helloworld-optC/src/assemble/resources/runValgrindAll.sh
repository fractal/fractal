#!/bin/sh
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./helloworld_noopt
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./helloworld
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./helloworld_static
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./helloworld_merge
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./helloworld_all
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./helloworld_allnobootstrap
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./helloworld_plainc
