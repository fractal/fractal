####################################################################################################
#   Cecilia Helloworld example: OVERVIEW
####################################################################################################

This example shows a very basic Fractal application with a main composite component containing
a client and a server.


####################################################################################################
#   LAUNCHING THE BASE EXAMPLE WITH MAVEN
####################################################################################################
From the command line, simply type:
 $ mvn clean compile

Afterwards, execute the binary "helloworld" file with:
 $ target/build/obj/helloworld

You should see the client calling 2 times the print method on the server.


####################################################################################################
#   LAUNCHING THE "COUNTER" EXAMPLE WITH MAVEN
####################################################################################################

You can also compile and run another version of the Helloworld example, which contains a server
component that counts the number of times its print method is invoked.
This is due to the fact that the example wants to show how to declare and use instance variables in
the server component, and specifically there is a instance field storing the number of times the
"print" method has been called on the server component instance.

To compile this example launch:
 $ mvn -Pcounter clean compile

and then execute the binary "helloworld-with-counter" with:
 $ target/build/obj/helloworld-with-counter





####################################################################################################
#   ADVANCED: COMPILING DIFFERENT HELLOWORLD OPTIMIZATIONS SCENARIOS
####################################################################################################

This project is also meant to benchmark different configurations of Helloworld.
In order to compile the configurations, use the compileAll.sh scripts, or direct Maven invocations.
The compileAll.sh script also contains code to dissect the generated binaries, especially the
sections' sizes.


####################################################################################################
#    CECILIA VERSION TO USE
####################################################################################################

This Helloworld branch version is meant to be used with the Cecilia modules you can find in the
"debrouxl" sandbox, under the directory project "cecilia-newmacros".


####################################################################################################
#    BENCHMARK RESULTS, x86_64
####################################################################################################

* computer: Asus F3Jv-AS022P, Core 2 Duo T7200 @ 2 GHz, 2 GB RAM operating in dual-channel mode.
* OS: SimplyMEPIS GNU/Linux 8.0 64-bit (2.6.29.1 kernel, glibc 2.7), based on Debian Lenny.
* compiler: gcc (Debian 4.3.2-1.1) 4.3.2.
* relevant (optimization-related) compilation options: -Os
* execution: just run the program.
* processor simulator: valgrind-3.3.1-Debian, valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes

The variation between different runs of the benchmark is as follows:
    * Helloworld itself: zero instructions.
    * libc-2.7.so, ld-2.7.so: up to several hundred instructions, over more than 100K. This
      variation is low.

Figures:
* helloworld_noopt (component-based version, all optimizations disabled):
    * Instruction fetch costs, aggregated by ELF object:
        * helloworld:     848
    * Data accesses for helloworld ELF object:
        * Reads:          272
        * Writes:         144
    * Section sizes:
        .text            2742
        .rodata           152
        .data             960
        .bss               16

* helloworld (component-based version, default optimization enabled, i.e. only singleton
  component optimization is enabled):
    * Instruction fetch costs, aggregated by ELF object:
        * helloworld:     836
    * Data accesses for helloworld ELF object:
        * Reads:          268
        * Writes:         140
    * Section sizes:
        .text            2758 (?? - fewer instructions are executed by helloworld than by helloworld_noopt)
        .rodata           152
        .data             960
        .bss               16

* helloworld_static (component-based version, default optimization enabled, ADL specifies
  dynamic="false" bindings):
    * Instruction fetch costs, aggregated by ELF object:
        * helloworld:     812
    * Data accesses for helloworld ELF object:
        * Reads:          256
        * Writes:         138
    * Section sizes:
        .text            2742
        .rodata           152
        .data             872
        .bss               16
Comment: method calls are slightly more efficient, but the size increases because, in addition
         to the "regular" BC method implementations, a dynamic="false"-aware implementation of
         BC methods is embedded in the program.

* helloworld_merge (component-based version, default optimization enabled, ADL specifies merging):
    * Instruction fetch costs, aggregated by ELF object:
        * helloworld:     432
    * Data accesses for helloworld ELF object:
        * Reads:          140
        * Writes:          81
    * Section sizes:
        .text            2726
        .rodata           150
        .data             616
        .bss               16
Comment: the main benefit of the merging optimization is removing the components' data
         structures, as shown by the significant decrease of the size of the .data section.
         It also decreases the number of instructions executed by the bootstrap code.

* helloworld_all (component-based version, all optimizations enabled):
    * Instruction fetch costs, aggregated by ELF object:
        * helloworld:     430
    * Data accesses for helloworld ELF object:
        * Reads:          140
        * Writes:          80
    * Section sizes:
        .text            2726
        .rodata           150
        .data             616
        .bss               16
Comment: the "void *_this" removal optimization is enabled, i.e. on all methods have one less
         parameter. As there are only two such method calls in the program, the program
         executes two instructions less than helloworld_merge.

* helloworld_allnobootstrap (component-based version, all optimizations enabled; no bootstrap
  code because it isn't necessary for an application without "void * _this"):
    * Instruction fetch costs, aggregated by ELF object:
        * helloworld:     162
    * Data accesses for helloworld ELF object:
        * Reads:           51
        * Writes:          38
    * Section sizes:
        .text             694
        .rodata            77
        .data              96
        .bss               16
Comment: the bootstrap code costs several hundred instructions, but it has a fixed size overhead
         of several kilobytes...

* helloworld_plainc (plain C version):
    * Instruction fetch costs, aggregated by ELF object:
        * helloworld:     138
    * Data accesses for helloworld ELF object:
        * Reads:           41
        * Writes:          34
    * Section sizes:
        .text             534
        .rodata            67
        .bss               16
Comment: the overhead of helloworld_allnobootstrap over helloworld_plainc is due to the attributes
         of the former. Get rid of the attributes and both programs are *exactly* equivalent.


General comments:
    * without any optimizations, the helloworld_noopt ELF object executes ~514% more instructions
      than the helloworld_plainc ELF object. Helloworld is a pretty extreme example, not
      representative at all of real world programs: it has very few methods, which contain very
      few instructions. See the Comanche example for a more realistic example (though some of
      its methods contain very few instructions, too).

    * _cycle estimation_ (as opposed to instruction count) figures given by Valgrind are not
      reported here, because they can swap the order of the executables between compilations of
      Helloworld, due to strong variations in cache misses. We attribute this to nondeterministic
      section ordering by the linker;

    * as shown by the attribute-less variant of helloworld_allnobootstrap (mentioned above),
      the toolchain can *completely* swallow the speed AND size overheads stemming from
      component-based programming. This was achieved without making users litter their C code
      with annotations (currently, all annotations are in the ADL description, which is cleaner);

    * last but not least: more than 99.9% of the execution time is spent outside of Helloworld
      itself, thereby diluting the overhead of components (if any) by three orders of magnitude...
      This example is more interesting for size overhead evaluations than for speed
      overhead evaluations...
