<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>org.objectweb.fractal</groupId>
		<artifactId>parent</artifactId>
		<version>2.0</version>
	</parent>

	<groupId>org.objectweb.fractal.cecilia.examples</groupId>
	<artifactId>cecilia-examples</artifactId>
	<version>2.2-SNAPSHOT</version>
	<name>Cecilia Examples: parent module</name>
	<packaging>pom</packaging>

	<description>Parent module for Cecilia examples.</description>

	<licenses>
		<license>
			<name>LGPL</name>
			<!-- link to the txt version of the LGPL license to avoid embedding
				Gnu web site inside generated site -->
			<url>http://www.gnu.org/licenses/lgpl-3.0.txt</url>
		</license>
	</licenses>

	<modules>
		<module>helloworld</module>
		<module>helloworld-optC</module>
		<module>cloneable</module>
		<module>cloneable-optC</module>
		<module>comanche</module>
		<module>comanche-optC</module>
		<module>libmpeg2</module>
		<module>libmpeg2-optC</module>
	</modules>

	<developers>
		<!-- TODO write developers here -->
	</developers>

	<contributors>
		<!-- TODO write contributors here -->
	</contributors>

	<properties>
		<cecilia.version>2.2-SNAPSHOT</cecilia.version>
		<cecilia-examples.home.url>http://fractal.objectweb.org/cecilia-examples-site/${project.version}</cecilia-examples.home.url>
		<cecilia.home.url>http://fractal.objectweb.org/cecilia-site/${cecilia.version}</cecilia.home.url>
		<!-- The login name on the OW2 forge.  -->
		<ow.username>${user.name}</ow.username>
		<ow.hostname>forge.objectweb.org</ow.hostname>
		<ow.site.deploy.dir>/var/lib/gforge/chroot/home/groups/fractal/htdocs/cecilia-examples-site</ow.site.deploy.dir>
		<deploy.site.url>scp://${ow.hostname}${ow.site.deploy.dir}/${project.version}</deploy.site.url>
		<bootstrap.plugin.version>1.1</bootstrap.plugin.version>
	</properties>

	<!-- Cecilia-examples home page -->
	<!-- Because of a bug in the maven-site-plugin, the URL of the project MUST 
		ends with a '/' which is not the case when using the basic maven
		inheritance mechanism -->
	<url>${cecilia-examples.home.url}/</url>

	<scm>
		<connection>scm:svn:svn://svn.forge.objectweb.org/svnroot/fractal/trunk/cecilia-framework/examples</connection>
		<developerConnection>scm:svn:svn+ssh://svn.forge.objectweb.org/svnroot/fractal/trunk/cecilia-framework/examples</developerConnection>
		<url>http://svn.forge.objectweb.org/cgi-bin/viewcvs.cgi/fractal/trunk/cecilia-framework/examples</url>
	</scm>

	<distributionManagement>
		<site>
			<id>objectweb-site</id>
			<name>ObjectWeb's site deployment</name>
			<url>${deploy.site.url}</url>
		</site>
	</distributionManagement>

	<ciManagement>
  		<system>bamboo</system>
  		<url>http://forge.ow2.org/bamboo/browse/FRACTAL-CECEX</url>
  	</ciManagement>

	<build>
		<plugins>
			<!-- Force version of maven-site-plugin to 2.0-beta-5 since 
				2.0-beta-6 has many bugs in relative path management. -->
			<plugin>
				<artifactId>maven-site-plugin</artifactId>
				<version>2.0-beta-5</version>
			</plugin>

			<!-- Configure the tagBase property of the release plugin -->
			<plugin>
				<artifactId>maven-release-plugin</artifactId>
				<configuration>
					<tagBase>
						svn+ssh://svn.forge.objectweb.org/svnroot/fractal/tags
					</tagBase>
					<autoVersionSubmodules>true</autoVersionSubmodules>

					 <!-- The SCM username if the OW2 username. -->
                                        <username>${ow.username}</username>

					<!-- release goals contain "antrun:run" to update 'current'
						link in documentation on the OW2 forge. -->
					<!-- TODO running the "antrun:run" goal while performing 
						release is not very clean since sub modules may defines 
						there own configuration of this plugin which may produce
						unexpected result. Should use a	more specific MOJO. -->
					<goals>clean deploy site-deploy antrun:run</goals>

					<!-- Forwards property values to be sure that values 
						overridden on the command line will be used while 
						performing release. 
						This is particularly useful for the antrun:run goal.-->
					<arguments>
						-Dow.username=${ow.username} -Dow.hostname=${ow.hostname} -Dow.file.deploy.dir=${ow.file.deploy.dir} -Dow.site.deploy.dir=${ow.site.deploy.dir}
					</arguments>
				</configuration>
			</plugin>
		</plugins>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.codehaus.mojo</groupId>
					<artifactId>maven-exec-plugin</artifactId>
					<version>1.1.1</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-invoker-plugin</artifactId>
					<version>1.3</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-assembly-plugin</artifactId>
					<version>2.2-beta-4</version>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<profiles>
		<profile>
			<id>local-site-deploy</id>
			
			<properties>
				<local.deploy.dir>/tmp/cecilia-examples-site/${project.version}</local.deploy.dir>
			</properties>
			<distributionManagement>
				<site>
					<name>local site deployment</name>
					<url>file://${local.deploy.dir}</url>
				</site>
			</distributionManagement>
		</profile>

		<profile>
			<id>release-profile</id>

			<activation>
				<property>
					<name>performRelease</name>
					<value>true</value>
				</property>
			</activation>

			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-antrun-plugin</artifactId>
						<inherited>false</inherited>
						<configuration>
							<tasks>
								<sshexec host="${ow.hostname}" username="${ow.username}" passphrase="" keyfile="${user.home}/.ssh/id_rsa" command="cd ${ow.site.deploy.dir}; ln -nfs ${project.version} current" />
							</tasks>
						</configuration>
						<dependencies>
							<dependency>
								<groupId>org.apache.ant</groupId>
								<artifactId>ant-jsch</artifactId>
								<version>1.7.0</version>
							</dependency>
						</dependencies>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>
