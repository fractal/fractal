/*
 * motion_comp.c
 * Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
 * Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
 *
 * This file is part of mpeg2dec, a free MPEG-2 video stream decoder.
 * See http://libmpeg2.sourceforge.net/ for updates.
 *
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with mpeg2dec; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "config.h"

#ifdef COMPILING_WITH_CECILIA
/**
 * Internal component data.
 */
typedef struct {
    
} PRIVATE_DATA;
#endif

#include <inttypes.h>
#include "cecilia_opt.h"

#include "mpeg2.h"
#include "attributes.h"
#include "mc.h"

// This #define is used to indicate the included .c files that they mustn't define their own exported methods and typedef struct.
#define MC_SPECIFIC_COMP_ARE_EMBEDDED

#ifdef COMPILING_WITH_CECILIA
#include "decoder/mc/motion_comp_c_template.c"
#include "decoder/mc/motion_comp_c.c"
#include "decoder/mc/motion_comp_mmx.c"
#include "decoder/mc/motion_comp_3dnow.c"
#include "decoder/mc/motion_comp_mmxext.c"
#include "decoder/mc/motion_comp_sse2.c"
#include "decoder/mc/motion_comp_altivec.c"
#include "decoder/mc/motion_comp_vis.c"
#include "decoder/mc/motion_comp_arm.c"
#include "decoder/mc/motion_comp_iwmmxt.c"
#include "decoder/mc/motion_comp_common.c"
#else
/*extern mpeg2_mc_t mpeg2_mc_mmx;
extern mpeg2_mc_t mpeg2_mc_mmxext;
extern mpeg2_mc_t mpeg2_mc_sse2;
extern mpeg2_mc_t mpeg2_mc_3dnow;
extern mpeg2_mc_t mpeg2_mc_altivec;
extern mpeg2_mc_t mpeg2_mc_vis;
extern mpeg2_mc_t mpeg2_mc_arm;
extern mpeg2_mc_t mpeg2_mc_iwmmxt;
extern mpeg2_mc_t mpeg2_mc_c;*/
#include "motion_comp_c_template.c"
#include "motion_comp_c.c"
#include "motion_comp_mmx.c"
#include "motion_comp_mmxext_3dnow_common.c"
#include "motion_comp_3dnow.c"
#include "motion_comp_mmxext.c"
#include "motion_comp_sse2.c"
#include "motion_comp_altivec.c"
#include "motion_comp_vis.c"
#include "motion_comp_arm.c"
#include "motion_comp_iwmmxt.c"
#include "motion_comp_common.c"
#endif

void METH(mpeg2_mc_init) (uint32_t accel) // COMP: HELPER CALLEDBY("mpeg2_slice_init")
{
#ifdef ARCH_X86
    if (accel & MPEG2_ACCEL_X86_SSE2)
	mpeg2_mc_set(&mpeg2_mc_sse2);
    else if (accel & MPEG2_ACCEL_X86_MMXEXT)
	mpeg2_mc_set(&mpeg2_mc_mmxext);
    else if (accel & MPEG2_ACCEL_X86_3DNOW)
	mpeg2_mc_set(&mpeg2_mc_3dnow);
    else if (accel & MPEG2_ACCEL_X86_MMX)
	mpeg2_mc_set(&mpeg2_mc_mmx);
    else
#endif
#ifdef ARCH_PPC
    if (accel & MPEG2_ACCEL_PPC_ALTIVEC)
	mpeg2_mc_set(&mpeg2_mc_altivec);
    else
#endif
#ifdef ARCH_SPARC
    if (accel & MPEG2_ACCEL_SPARC_VIS)
	mpeg2_mc_set(&mpeg2_mc_vis);
    else
#endif
#ifdef ARCH_ARM
    if (accel & MPEG2_ACCEL_ARM) {
#ifdef HAVE_IWMMXT
	if (accel & MPEG2_ACCEL_ARM_IWMMXT)
	    mpeg2_mc_set(&mpeg2_mc_iwmmxt);
	else
#endif
	    mpeg2_mc_set(&mpeg2_mc_arm);
    } else
#endif
	mpeg2_mc_set(&mpeg2_mc_c);
}
