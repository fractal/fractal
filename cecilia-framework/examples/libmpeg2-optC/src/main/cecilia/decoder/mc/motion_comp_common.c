/*
 * motion_comp_common.c
 * Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
 * Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
 *
 * This file is part of mpeg2dec, a free MPEG-2 video stream decoder.
 * See http://libmpeg2.sourceforge.net/ for updates.
 *
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with mpeg2dec; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __MOTION_COMP_COMMON_C__
#define __MOTION_COMP_COMMON_C__

#include "config.h"

#include <stdlib.h>
#include <inttypes.h>
#include "cecilia_opt.h"

#include "mpeg2.h"
#include "attributes.h"
#include "mc.h"

/*! Table of pointers for motion compensation */
static mpeg2_mc_t __mpeg2_mc;

/**
 * Setter for motion compensation table of pointers.
 */
inline void mpeg2_mc_set(mpeg2_mc_t * mpeg2_mc) {
    __mpeg2_mc = *mpeg2_mc;
}

/**
 * Getter for motion compensation table of pointers.
 */
void * METH(mpeg2_mc_get) (void) {
    return &__mpeg2_mc;
}

#endif
