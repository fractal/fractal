/*
 * decode.c
 * Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
 * Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
 *
 * This file is part of mpeg2dec, a free MPEG-2 video stream decoder.
 * See http://libmpeg2.sourceforge.net/ for updates.
 *
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with mpeg2dec; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "config.h"

#ifdef COMPILING_WITH_CECILIA
/**
 * Internal component data.
 */
typedef struct {
    
} PRIVATE_DATA;
#endif

#include <string.h>	/* memcmp/memset, try to remove */
#include <stdlib.h>
#include <inttypes.h>
#include "cecilia_opt.h"

#include "mpeg2.h"
#include "mpeg2convert.h"
#include "attributes.h"
#include "mpeg2_internal.h"
#include "decode.h"
#include "header.h"
#include "slice.h"
#include "common.h"

#ifdef COMPILING_WITH_CECILIA
#include "arch/cpu_accel.c"
#include "header.c"
#else
#include "arch/cpu_accel.h"
#endif

static int mpeg2_accels = 0;
mpeg2dec_t * mpeg2dec;


#define BUFFER_SIZE (1194 * 1024)

mpeg2_info_s * METH(mpeg2_info) (void) // COMP: EXPORT CALLEDBY(decoder_client)
{
    return &(mpeg2dec->info);
}

static inline int skip_chunk (int bytes) // COMP: HELPER
{
    uint8_t * current;
    uint32_t shift;
    uint8_t * limit;
    uint8_t byte;

    if (!bytes)
	return 0;

    current = mpeg2dec->buf_start;
    shift = mpeg2dec->shift;
    limit = current + bytes;

    do {
	byte = *current++;
	if (shift == 0x00000100) {
	    int skipped;

	    mpeg2dec->shift = 0xffffff00;
	    skipped = current - mpeg2dec->buf_start;
	    mpeg2dec->buf_start = current;
	    return skipped;
	}
	shift = (shift | byte) << 8;
    } while (current < limit);

    mpeg2dec->shift = shift;
    mpeg2dec->buf_start = current;
    return 0;
}

static inline int copy_chunk (int bytes) // COMP: HELPER
{
    uint8_t * current;
    uint32_t shift;
    uint8_t * chunk_ptr;
    uint8_t * limit;
    uint8_t byte;

    if (!bytes)
	return 0;

    current = mpeg2dec->buf_start;
    shift = mpeg2dec->shift;
    chunk_ptr = mpeg2dec->chunk_ptr;
    limit = current + bytes;

    do {
	byte = *current++;
	if (shift == 0x00000100) {
	    int copied;

	    mpeg2dec->shift = 0xffffff00;
	    mpeg2dec->chunk_ptr = chunk_ptr + 1;
	    copied = current - mpeg2dec->buf_start;
	    mpeg2dec->buf_start = current;
	    return copied;
	}
	shift = (shift | byte) << 8;
	*chunk_ptr++ = byte;
    } while (current < limit);

    mpeg2dec->shift = shift;
    mpeg2dec->buf_start = current;
    return 0;
}

void METH(mpeg2_buffer) (uint8_t * start, uint8_t * end) // COMP: EXPORT CALLEDBY(decoder_client)
{
    mpeg2dec->buf_start = start;
    mpeg2dec->buf_end = end;
}

int METH(mpeg2_getpos) (void) // COMP: EXPORT CALLEDFOR(dump_state)
{
    return mpeg2dec->buf_end - mpeg2dec->buf_start;
}

static inline mpeg2_state_t seek_chunk (void) // COMP: HELPER
{
    int size, skipped;

    size = mpeg2dec->buf_end - mpeg2dec->buf_start;
    skipped = skip_chunk (size);
    if (!skipped) {
	mpeg2dec->bytes_since_tag += size;
	return STATE_BUFFER;
    }
    mpeg2dec->bytes_since_tag += skipped;
    mpeg2dec->code = mpeg2dec->buf_start[-1];
    return STATE_INTERNAL_NORETURN;
}

#define RECEIVED(code,state) (((state) << 8) + (code))

mpeg2_state_t mpeg2_parse_header (void) // COMP: HELPER EXPORT
{
    int size_buffer, size_chunk, copied;
    int result;

    mpeg2dec->action = mpeg2_parse_header;
    mpeg2dec->info.user_data = NULL;	mpeg2dec->info.user_data_len = 0;
    while (1) {
	size_buffer = mpeg2dec->buf_end - mpeg2dec->buf_start;
	size_chunk = (mpeg2dec->chunk_buffer + BUFFER_SIZE -
		      mpeg2dec->chunk_ptr);
	if (size_buffer <= size_chunk) {
	    copied = copy_chunk (size_buffer);
	    if (!copied) {
		mpeg2dec->bytes_since_tag += size_buffer;
		mpeg2dec->chunk_ptr += size_buffer;
		return STATE_BUFFER;
	    }
	} else {
	    copied = copy_chunk (size_chunk);
	    if (!copied) {
		/* filled the chunk buffer without finding a start code */
		mpeg2dec->bytes_since_tag += size_chunk;
		mpeg2dec->code = 0xb4;
		mpeg2dec->action = mpeg2_seek_header;
		return STATE_INVALID;
	    }
	}
	mpeg2dec->bytes_since_tag += copied;

	switch(mpeg2dec->code & 0x0b) {
	    case 0x00:
		result = mpeg2_header_picture(); break;
	    case 0x01:
		result = mpeg2_header_extension(); break;
	    case 0x02:
		result = mpeg2_header_user_data(); break;
	    case 0x03:
		result = mpeg2_header_sequence(); break;
	    case 0x08:
		result = mpeg2_header_gop(); break;
	    default:
		result = 1; break;
	
        }
	if (result) {
	    mpeg2dec->code = mpeg2dec->buf_start[-1];
	    mpeg2dec->action = mpeg2_seek_header;
	    return STATE_INVALID;
	}

	mpeg2dec->code = mpeg2dec->buf_start[-1];
	switch (RECEIVED (mpeg2dec->code, mpeg2dec->state)) {

	/* state transition after a sequence header */
	case RECEIVED (0x00, STATE_SEQUENCE):
	case RECEIVED (0xb8, STATE_SEQUENCE):
	    mpeg2_header_sequence_finalize();
	    break;

	/* other legal state transitions */
	case RECEIVED (0x00, STATE_GOP):
	    mpeg2_header_gop_finalize();
	    break;
	case RECEIVED (0x01, STATE_PICTURE):
	case RECEIVED (0x01, STATE_PICTURE_2ND):
	    mpeg2_header_picture_finalize(mpeg2_accels);
    
	    mpeg2dec->action = mpeg2_header_slice_start;

	    break;

	/* legal headers within a given state */
	case RECEIVED (0xb2, STATE_SEQUENCE):
	case RECEIVED (0xb2, STATE_GOP):
	case RECEIVED (0xb2, STATE_PICTURE):
	case RECEIVED (0xb2, STATE_PICTURE_2ND):
	case RECEIVED (0xb5, STATE_SEQUENCE):
	case RECEIVED (0xb5, STATE_PICTURE):
	case RECEIVED (0xb5, STATE_PICTURE_2ND):
	    mpeg2dec->chunk_ptr = mpeg2dec->chunk_start;
	    continue;

	default:
	    mpeg2dec->action = mpeg2_seek_header;
	    return STATE_INVALID;
	}

	mpeg2dec->chunk_start = mpeg2dec->chunk_ptr = mpeg2dec->chunk_buffer;
	mpeg2dec->user_data_len = 0;
	return mpeg2dec->state;
    }
}

mpeg2_state_t mpeg2_seek_header (void) // COMP: HELPER EXPORT
{
    while (!(   mpeg2dec->code == 0xb3 /* START OF SEQUENCE code */
              || (   (   mpeg2dec->code == 0xb7 /* END OF SEQUENCE code */
                      || mpeg2dec->code == 0xb8 /* START OF GOP code */
                      || !mpeg2dec->code /* START OF PICTURE code */
                     )
                  && mpeg2dec->sequence.width != (unsigned)-1
                 )
             )
           )
	if (seek_chunk () == STATE_BUFFER)
	    return STATE_BUFFER;
    mpeg2dec->chunk_start = mpeg2dec->chunk_ptr = mpeg2dec->chunk_buffer;
    mpeg2dec->user_data_len = 0;
    return ((mpeg2dec->code == 0xb7) ?
	    mpeg2_header_end() : mpeg2_parse_header());
}

mpeg2_state_t METH(mpeg2_parse) (void) // COMP: EXPORT CALLEDBY(decoder_client)
{
    int size_buffer, size_chunk, copied;

    if (mpeg2dec->action) {
	mpeg2_state_t state;

	state = mpeg2dec->action ();
	// WARNING: the casts are NEEDED, otherwise the program CRASHES !
        if ((int)state > (int)STATE_INTERNAL_NORETURN)
	    return state;
    }

    while (1) {
	while ((unsigned) (mpeg2dec->code - mpeg2dec->first_decode_slice) <
	       mpeg2dec->nb_decode_slices) {
	    size_buffer = mpeg2dec->buf_end - mpeg2dec->buf_start;
	    size_chunk = (mpeg2dec->chunk_buffer + BUFFER_SIZE -
			  mpeg2dec->chunk_ptr);
	    if (size_buffer <= size_chunk) {
		copied = copy_chunk (size_buffer);
		if (!copied) {
		    mpeg2dec->bytes_since_tag += size_buffer;
		    mpeg2dec->chunk_ptr += size_buffer;
		    return STATE_BUFFER;
		}
	    } else {
		copied = copy_chunk (size_chunk);
		if (!copied) {
		    /* filled the chunk buffer without finding a start code */
		    mpeg2dec->bytes_since_tag += size_chunk;
		    mpeg2dec->action = seek_chunk;
		    return STATE_INVALID;
		}
	    }
	    mpeg2dec->bytes_since_tag += copied;

	    CLIENT(slice, mpeg2_slice) (&(mpeg2dec->decoder), mpeg2dec->code,
			 mpeg2dec->chunk_start);
	    mpeg2dec->code = mpeg2dec->buf_start[-1];
	    mpeg2dec->chunk_ptr = mpeg2dec->chunk_start;
	}
	if ((unsigned) (mpeg2dec->code - 1) >= 0xb0 - 1)
	    break;
	if (seek_chunk () == STATE_BUFFER)
	    return STATE_BUFFER;
    }

    mpeg2dec->action = mpeg2_seek_header;
    switch (mpeg2dec->code) {
    case 0x00:
	return mpeg2dec->state;
    case 0xb3: /* START OF SEQUENCE code */
    case 0xb7: /* END OF SEQUENCE code */
    case 0xb8: /* START OF GOP code */
	return (mpeg2dec->state == STATE_SLICE) ? STATE_SLICE : STATE_INVALID;
    default:
	mpeg2dec->action = seek_chunk;
	return STATE_INVALID;
    }
}

int METH(mpeg2_convert) (void * arg) // COMP: EXPORT CALLEDBY(decoder_client)
{
    mpeg2_convert_init_t convert_init;
    int error;

    error = CLIENT(convert, mpeg2convert_convert) (MPEG2_CONVERT_SET, NULL, &(mpeg2dec->sequence), 0,
		     mpeg2_accels, arg, &convert_init);
    if (!error) {
	mpeg2dec->convert_arg = arg;
	mpeg2dec->convert_id_size = convert_init.id_size;
	mpeg2dec->convert_stride = 0;
    }
    return error;
}

int mpeg2_stride (int stride) // COMP: NEVERCALLED
{
    if (!CLIENT(convert, mpeg2convert_has_convert) ()) {
	if (stride < (int) mpeg2dec->sequence.width)
	    stride = mpeg2dec->sequence.width;
	mpeg2dec->decoder.stride_frame = stride;
    } else {
	mpeg2_convert_init_t convert_init;

	stride = CLIENT(convert, mpeg2convert_convert) (MPEG2_CONVERT_STRIDE, NULL,
				    &(mpeg2dec->sequence), stride,
				    mpeg2_accels, mpeg2dec->convert_arg,
				    &convert_init);
	mpeg2dec->convert_id_size = convert_init.id_size;
	mpeg2dec->convert_stride = stride;
    }
    return stride;
}

void METH(mpeg2_set_buf) (uint8_t * buf[3], void * id) // COMP: EXPORT CALLEDBY(decoder_client) CALLEDFOR(custom memory management)
{
    mpeg2_fbuf_t * fbuf;

    if (mpeg2dec->custom_fbuf) {
	if (mpeg2dec->state == STATE_SEQUENCE) {
	    mpeg2dec->fbuf[2] = mpeg2dec->fbuf[1];
	    mpeg2dec->fbuf[1] = mpeg2dec->fbuf[0];
	}
	mpeg2_set_fbuf((mpeg2dec->decoder.coding_type ==
				   PIC_FLAG_CODING_TYPE_B));
	fbuf = mpeg2dec->fbuf[0];
    } else {
	fbuf = &(mpeg2dec->fbuf_alloc[mpeg2dec->alloc_index].fbuf);
	mpeg2dec->alloc_index_user = ++mpeg2dec->alloc_index;
    }
    fbuf->buf[0] = buf[0];
    fbuf->buf[1] = buf[1];
    fbuf->buf[2] = buf[2];
    fbuf->id = id;
}

void METH(mpeg2_custom_fbuf) (int custom_fbuf) // COMP: EXPORT CALLEDBY(decoder_client) CALLEDFOR(custom memory management)
{
    mpeg2dec->custom_fbuf = custom_fbuf;
}

void METH(mpeg2_skip) (int skip) // COMP: EXPORT CALLEDBY(decoder_client)
{
    mpeg2dec->first_decode_slice = 1;
    mpeg2dec->nb_decode_slices = skip ? 0 : (0xb0 - 1);
}

void mpeg2_slice_region (int start, int end) // COMP: NEVERCALLED
{
    start = (start < 1) ? 1 : (start > 0xb0) ? 0xb0 : start;
    end = (end < start) ? start : (end > 0xb0) ? 0xb0 : end;
    mpeg2dec->first_decode_slice = start;
    mpeg2dec->nb_decode_slices = end - start;
}

void METH(mpeg2_tag_picture) (uint32_t tag, uint32_t tag2) // COMP: EXPORT CALLEDBY(decoder_client)
{
    mpeg2dec->tag_previous = mpeg2dec->tag_current;
    mpeg2dec->tag2_previous = mpeg2dec->tag2_current;
    mpeg2dec->tag_current = tag;
    mpeg2dec->tag2_current = tag2;
    mpeg2dec->num_tags++;
    mpeg2dec->bytes_since_tag = 0;
}

uint32_t METH(mpeg2_accel) (uint32_t accel) // COMP: EXPORT CALLEDBY(decoder_client) CALLEDFOR(no acceleration)
{
    if (!mpeg2_accels) {
	mpeg2_accels = mpeg2_detect_accel (accel) | MPEG2_ACCEL_DETECT;
	CLIENT(slice, mpeg2_slice_init) (mpeg2_accels);
    }
    return mpeg2_accels & ~MPEG2_ACCEL_DETECT;
}

void mpeg2_reset (int full_reset) // COMP: CALLEDBY mpeg2_init
{
    mpeg2dec->buf_start = mpeg2dec->buf_end = NULL;
    mpeg2dec->num_tags = 0;
    mpeg2dec->shift = 0xffffff00;
    mpeg2dec->code = 0xb4;
    mpeg2dec->action = mpeg2_seek_header;
    mpeg2dec->state = STATE_INVALID;
    mpeg2dec->first = 1;

    mpeg2_reset_info(&(mpeg2dec->info));
    mpeg2dec->info.gop = NULL;
    mpeg2dec->info.user_data = NULL;
    mpeg2dec->info.user_data_len = 0;
    if (full_reset) {
	mpeg2dec->info.sequence = NULL;
	mpeg2_header_state_init();
    }

}

void * METH(mpeg2_init) (void) // COMP: EXPORT CALLEDBY(decoder_client)
{
    SERVER(decoder, mpeg2_accel) (MPEG2_ACCEL_DETECT);

    mpeg2dec = (mpeg2dec_t *) CLIENT(alloc, mpeg2_malloc) (sizeof (mpeg2dec_t),
					    MPEG2_ALLOC_MPEG2DEC);
    if (mpeg2dec == NULL)
	return NULL;

    memset (mpeg2dec->decoder.DCTblock, 0, 64 * sizeof (int16_t));
    memset (mpeg2dec->quantizer_matrix, 0, 4 * 64 * sizeof (uint8_t));

    mpeg2dec->chunk_buffer = (uint8_t *) CLIENT(alloc, mpeg2_malloc) (BUFFER_SIZE + 4,
						       MPEG2_ALLOC_CHUNK);

    mpeg2dec->sequence.width = (unsigned)-1;
    mpeg2_reset (1);

    return mpeg2dec;
}

void METH(mpeg2_close) (void) // COMP: EXPORT CALLEDBY(decoder_client)
{
    mpeg2_header_state_init();
    CLIENT(alloc, mpeg2_free) (mpeg2dec->chunk_buffer);
    CLIENT(alloc, mpeg2_free) (mpeg2dec);
    mpeg2dec = NULL;
}
