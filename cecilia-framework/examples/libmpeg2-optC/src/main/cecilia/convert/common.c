/*
 * rgb.c
 * Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
 * Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
 *
 * This file is part of mpeg2dec, a free MPEG-2 video stream decoder.
 * See http://libmpeg2.sourceforge.net/ for updates.
 *
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with mpeg2dec; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "config.h"
#include "attributes.h"

#ifdef COMPILING_WITH_CECILIA
/**
 * Internal component data.
 */
typedef struct {
    
} PRIVATE_DATA;
#endif

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include "cecilia_opt.h"

#include "mpeg2.h"
#include "mpeg2convert.h"
#include "convert_internal.h"

static mpeg2convert_conversion_type_t current_convert = (mpeg2convert_conversion_type_t)-1;

static int nullslice_convert (int stage, void * id,
			      const mpeg2_sequence_t * seq,
			      int stride, uint32_t accel, void * arg,
			      mpeg2_convert_init_t * result)
{
    result->id_size = 0;
    result->buf_size[0] = result->buf_size[1] = result->buf_size[2] = 0;
    return 0;
}


int METH(mpeg2convert_setup) (mpeg2convert_conversion_type_t type, unsigned int bpp)
{
    int retval;
    if (!(type & MPEG2CONVERT_UYVY)) {
        if (type & MPEG2CONVERT_NONE) {
            retval = 0;
            current_convert = MPEG2CONVERT_NONE;
        }
        else if (type & MPEG2CONVERT_NULL) {
            retval = 0;
            current_convert = MPEG2CONVERT_NULL;
        }
        else {
#ifdef COMPILING_WITH_CECILIA
            retval = CLIENT(rgbconvert, mpeg2convert_setup) (type, bpp);
#else
            retval = CLIENT(rgbconvert, mpeg2convert_setup_rgb) (type, bpp);
#endif
            current_convert = MPEG2CONVERT_RGB;
        }
    }
    else {
#ifdef COMPILING_WITH_CECILIA
        retval = CLIENT(uyvyconvert, mpeg2convert_setup) (type, bpp);
#else
        retval = CLIENT(uyvyconvert, mpeg2convert_setup_uyvy) (type, bpp);
#endif
        current_convert = MPEG2CONVERT_UYVY;
    }
    return retval;
}

int METH(mpeg2convert_convert) (int stage, void * id, const mpeg2_sequence_t * sequence, int stride,
                 uint32_t accel, void * arg, mpeg2_convert_init_t * result)
{
    switch (current_convert) {
        case MPEG2CONVERT_RGB:
#ifdef COMPILING_WITH_CECILIA
            return CLIENT(rgbconvert, mpeg2convert_convert) (stage, id, sequence, stride, accel, arg, result);
#else
            return CLIENT(rgbconvert, mpeg2convert_convert_rgb) (stage, id, sequence, stride, accel, arg, result);
#endif
        case MPEG2CONVERT_UYVY:
#ifdef COMPILING_WITH_CECILIA
            return CLIENT(uyvyconvert, mpeg2convert_convert) (stage, id, sequence, stride, accel, arg, result);
#else
            return CLIENT(uyvyconvert, mpeg2convert_convert_uyvy) (stage, id, sequence, stride, accel, arg, result);
#endif
        case MPEG2CONVERT_NONE:
            return nullslice_convert(stage, id, sequence, stride, accel, arg, result);
        case MPEG2CONVERT_NULL:
            return 0;
        default:
            fprintf(stderr, "Invalid current conversion in Converter !\n");
            exit(1);
    }
    return 0;
}

int METH(mpeg2convert_has_convert) (void)
{
    return (current_convert != MPEG2CONVERT_NULL);
}

void METH(mpeg2convert_do_copy) (void * id, uint8_t * /*const*/ * src, unsigned int v_offset)
{
    switch (current_convert) {
        case MPEG2CONVERT_RGB:
#ifdef COMPILING_WITH_CECILIA
            return CLIENT(rgbconvert, mpeg2convert_do_copy) (id, src, v_offset);
#else
            return CLIENT(rgbconvert, mpeg2convert_do_copy_rgb) (id, src, v_offset);
#endif
        case MPEG2CONVERT_UYVY:
#ifdef COMPILING_WITH_CECILIA
            return CLIENT(uyvyconvert, mpeg2convert_do_copy) (id, src, v_offset);
#else
            return CLIENT(uyvyconvert, mpeg2convert_do_copy_uyvy) (id, src, v_offset);
#endif
        case MPEG2CONVERT_NONE:
        case MPEG2CONVERT_NULL:
            return; // Nothing to do.
        default:
            fprintf(stderr, "Invalid current conversion in Converter !\n");
            exit(1);
    }
}

void METH(mpeg2convert_do_start) (void * id, const mpeg2_fbuf_t * fbuf, const mpeg2_picture_t * picture, const mpeg2_gop_t * gop)
{
    switch (current_convert) {
        case MPEG2CONVERT_RGB:
#ifdef COMPILING_WITH_CECILIA
            return CLIENT(rgbconvert, mpeg2convert_do_start) (id, fbuf, picture, gop);
#else
            return CLIENT(rgbconvert, mpeg2convert_do_start_rgb) (id, fbuf, picture, gop);
#endif
        case MPEG2CONVERT_UYVY:
#ifdef COMPILING_WITH_CECILIA
            return CLIENT(uyvyconvert, mpeg2convert_do_start) (id, fbuf, picture, gop);
#else
            return CLIENT(uyvyconvert, mpeg2convert_do_start_uyvy) (id, fbuf, picture, gop);
#endif
        case MPEG2CONVERT_NONE:
        case MPEG2CONVERT_NULL:
            return; // Nothing to do.
        default:
            fprintf(stderr, "Invalid current conversion in Converter !\n");
            exit(1);
    }
}
