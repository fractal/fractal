/*
 * uyvy.c
 * Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
 * Copyright (C) 2003      Regis Duchesne <hpreg@zoy.org>
 * Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
 *
 * This file is part of mpeg2dec, a free MPEG-2 video stream decoder.
 * See http://libmpeg2.sourceforge.net/ for updates.
 *
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with mpeg2dec; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "config.h"

#ifdef COMPILING_WITH_CECILIA
/**
 * Internal component data.
 */
typedef struct {
    
} PRIVATE_DATA;
#endif

#include <inttypes.h>
#include "cecilia_opt.h"

#include "mpeg2.h"
#include "mpeg2convert.h"

typedef struct {
    int width;
    int stride;
    int chroma420;
    uint8_t * out;
} convert_uyvy_t;

#ifdef COMPILING_WITH_CECILIA
void METH(mpeg2convert_do_start) (void * _id, const mpeg2_fbuf_t * fbuf, const mpeg2_picture_t * picture, const mpeg2_gop_t * gop)
#else
void METH(mpeg2convert_do_start_uyvy) (void * _id, const mpeg2_fbuf_t * fbuf, const mpeg2_picture_t * picture, const mpeg2_gop_t * gop)
#endif
{
    convert_uyvy_t * instance = (convert_uyvy_t *) _id;

    instance->out = fbuf->buf[0];
    instance->stride = instance->width;
    if (picture->nb_fields == 1) {
	if (! (picture->flags & PIC_FLAG_TOP_FIELD_FIRST))
	    instance->out += 2 * instance->stride;
	instance->stride <<= 1;
    }
}

#ifdef WORDS_BIGENDIAN
#define PACK(a,b,c,d) (((a) << 24) | ((b) << 16) | ((c) << 8) | (d))
#else
#define PACK(a,b,c,d) (((d) << 24) | ((c) << 16) | ((b) << 8) | (a))
#endif

#ifdef COMPILING_WITH_CECILIA
void METH(mpeg2convert_do_copy) (void * /*const*/ _id, uint8_t * /*const*/ * src, const unsigned int v_offset)
#else
void METH(mpeg2convert_do_copy_uyvy) (void * /*const*/ _id, uint8_t * /*const*/ * src, const unsigned int v_offset)
#endif
{
    const convert_uyvy_t * const id = (convert_uyvy_t *) _id;
    uint8_t * _dst;
    uint8_t * py, * pu, * pv;
    int i, j;

    _dst = id->out + 2 * id->stride * v_offset;
    py = src[0]; pu = src[1]; pv = src[2];

    i = 16;
    do {
	uint32_t * dst = (uint32_t *) _dst;

	j = id->width >> 4;
	do {
	    dst[0] = PACK (pu[0],  py[0], pv[0],  py[1]);
	    dst[1] = PACK (pu[1],  py[2], pv[1],  py[3]);
	    dst[2] = PACK (pu[2],  py[4], pv[2],  py[5]);
	    dst[3] = PACK (pu[3],  py[6], pv[3],  py[7]);
	    dst[4] = PACK (pu[4],  py[8], pv[4],  py[9]);
	    dst[5] = PACK (pu[5], py[10], pv[5], py[11]);
	    dst[6] = PACK (pu[6], py[12], pv[6], py[13]);
	    dst[7] = PACK (pu[7], py[14], pv[7], py[15]);
	    py += 16;
	    pu += 8;
	    pv += 8;
	    dst += 8;
	} while (--j);
	py -= id->width;
	pu -= id->width >> 1;
	pv -= id->width >> 1;
	_dst += 2 * id->stride;
	py += id->stride;
	if (! (--i & id->chroma420)) {
	    pu += id->stride >> 1;
	    pv += id->stride >> 1;
	}
    } while (i);
}

#ifdef COMPILING_WITH_CECILIA
int METH(mpeg2convert_convert) (int stage, void * _id, const mpeg2_sequence_t * sequence, int stride,
                 uint32_t accel, void * arg, mpeg2_convert_init_t * result)
#else
int METH(mpeg2convert_convert_uyvy) (int stage, void * _id, const mpeg2_sequence_t * sequence, int stride,
                 uint32_t accel, void * arg, mpeg2_convert_init_t * result)
#endif
{
    convert_uyvy_t * instance = (convert_uyvy_t *)_id;

    if (sequence->chroma_width == sequence->width)
	return 1;

    if (instance) {
	instance->width = sequence->width;
	instance->chroma420 = (sequence->chroma_height < sequence->height);
	result->buf_size[0] = sequence->width * sequence->height * 2;
	result->buf_size[1] = result->buf_size[2] = 0;
    } else {
	result->id_size = sizeof (convert_uyvy_t);
    }

    return 0;
}

#ifdef COMPILING_WITH_CECILIA
int METH(mpeg2convert_setup) (mpeg2convert_conversion_type_t type, unsigned int bpp)
#else
int METH(mpeg2convert_setup_uyvy) (mpeg2convert_conversion_type_t type, unsigned int bpp)
#endif
{
    return 0;
}

#ifdef COMPILING_WITH_CECILIA
int METH(mpeg2convert_has_convert) (void)
{
    return 1;
}
#endif
