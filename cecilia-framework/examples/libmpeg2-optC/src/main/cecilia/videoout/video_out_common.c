/*
 * video_out_common.c
 * Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
 * Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
 *
 * This file is part of mpeg2dec, a free MPEG-2 video stream decoder.
 * See http://libmpeg2.sourceforge.net/ for updates.
 *
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with mpeg2dec; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
/** \file video_out_common.c
 * This file contains method definitions that are common to multiple video output plugins.
 */

#ifndef __VIDEO_OUT_COMMON_C__
#define __VIDEO_OUT_COMMON_C__

#include "config.h"

#include <string.h>
#include <stdlib.h>
#include <inttypes.h>
#include "cecilia_opt.h"

#include "video_out.h"

extern vo_driver_t * vo_get_drivers(void);

static vo_instance_t * current_instance = NULL;

int METH(vo_is_output_available) (const char * name)
{
    int result = 0;
    vo_driver_t * drivers = vo_get_drivers();
    unsigned int i;
    
    for (i = 0; drivers[i].name != NULL; i++)
    {
        if (strcmp (drivers[i].name, name) == 0)
        {
            result = 1;
        }
    }
    return result;
}

int METH(vo_perform_open) (const char * name)
{
    unsigned int i;
    vo_open_t * open_func = NULL;
    vo_driver_t * drivers = vo_get_drivers();
    
    for (i = 0; drivers[i].name != NULL; i++)
    {
        if (strcmp (drivers[i].name, name) == 0)
        {
            open_func = drivers[i].open;
        }
    }
    
    if (open_func == NULL) {
        return 0;
    }
    else {
        current_instance = open_func();
        if (current_instance == NULL) {
            return 0;
        }
        else {
            return 1;
        }
    }
}

int METH(vo_do_setup)(unsigned int width,
		   unsigned int height, unsigned int chroma_width,
		   unsigned int chroma_height, int *result)
{
    return current_instance->setup(current_instance, width, height, chroma_width, chroma_height, result);
}

void METH(vo_do_setup_fbuf) (uint8_t ** buf, void ** id)
{
    current_instance->setup_fbuf(current_instance, buf, id);
}

int METH(vo_has_setup_fbuf) (void)
{
    return (current_instance->setup_fbuf != NULL);
}

void METH(vo_do_set_fbuf) (uint8_t ** buf, void ** id)
{
    current_instance->set_fbuf(current_instance, buf, id);
}

int METH(vo_has_set_fbuf) (void)
{
    return (current_instance->set_fbuf != NULL);
}

void METH(vo_do_start_fbuf) (uint8_t * /*const*/ * buf, void * id)
{
    current_instance->start_fbuf(current_instance, buf, id);
}

int METH(vo_has_start_fbuf) (void)
{
    return (current_instance->start_fbuf != NULL);
}

void METH(vo_do_draw) (uint8_t * /*const*/ * buf, void * id)
{
    if (current_instance->draw)
	current_instance->draw(current_instance, buf, id);
}

int METH(vo_has_draw) (void)
{
    return (current_instance->draw == NULL);
}

void METH(vo_do_discard) (uint8_t * /*const*/ * buf, void * id)
{
    if (current_instance->discard)
	current_instance->discard(current_instance, buf, id);
}

void METH(vo_do_close) (void)
{
    if (current_instance->close)
	current_instance->close(current_instance);
}

void METH(vo_print_driver_list) (void)
{
    unsigned int i;
    vo_driver_t * drivers = vo_get_drivers();
    for (i = 0; drivers[i].name; i++)
	fprintf (stderr, "\t\t\t%s\n", drivers[i].name);
}

#endif
