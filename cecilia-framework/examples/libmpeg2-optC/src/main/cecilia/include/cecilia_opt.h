/**
 * Dummy header to ease transition towards Cecilia.
 */
#ifndef __CECILIA_H__
#define __CECILIA_H__

#define SERVER(itf, proc) proc
#define CLIENT(itf, proc) proc
#define METH(meth) meth

#endif
