/*
 * video_out.h
 * Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
 * Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
 *
 * This file is part of mpeg2dec, a free MPEG-2 video stream decoder.
 * See http://libmpeg2.sourceforge.net/ for updates.
 *
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with mpeg2dec; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef LIBMPEG2_VIDEO_OUT_H
#define LIBMPEG2_VIDEO_OUT_H

struct mpeg2_sequence_t;
struct mpeg2_convert_init_t;
typedef struct {
    int (* convert) (int stage, void * id,
		     const struct mpeg2_sequence_t * sequence,
		     int stride, uint32_t accel, void * arg,
		     struct mpeg2_convert_init_t * result);
} vo_setup_result_t;

typedef struct vo_instance_s vo_instance_t;
struct vo_instance_s {
    int (* setup) (vo_instance_t * instance, unsigned int width,
		   unsigned int height, unsigned int chroma_width,
		   unsigned int chroma_height, int * result);
    void (* setup_fbuf) (vo_instance_t * instance, uint8_t ** buf, void ** id);
    void (* set_fbuf) (vo_instance_t * instance, uint8_t ** buf, void ** id);
    void (* start_fbuf) (vo_instance_t * instance,
			 uint8_t * const * buf, void * id);
    void (* draw) (vo_instance_t * instance, uint8_t * const * buf, void * id);
    void (* discard) (vo_instance_t * instance,
		      uint8_t * const * buf, void * id);
    // No _this for close because of the usage of "free" as close function in multiple output plugins.
    // As a result, close MUST NOT use CALL(...) or any other form of macro that requires _this.
    void (* close) (vo_instance_t * instance);
};

typedef vo_instance_t * vo_open_t (void);

typedef struct {
    const char * name;
    vo_open_t * open;
} vo_driver_t;

//void vo_accel (uint32_t accel);

#ifndef COMPILING_WITH_CECILIA
extern int METH(vo_is_output_available) (const char * name);
extern char * METH(vo_get_default_driver_name) (void);
extern void METH(vo_print_driver_list) (void);
extern int METH(vo_perform_open) (const char * name);
extern int METH(vo_do_setup)(unsigned int width, unsigned int height, unsigned int chroma_width, unsigned int chroma_height, int * result);
extern void METH(vo_do_setup_fbuf) (uint8_t ** buf, void ** id);
extern int METH(vo_has_setup_fbuf) (void);
extern void METH(vo_do_set_fbuf) (uint8_t ** buf, void ** id);
extern int METH(vo_has_set_fbuf) (void);
extern void METH(vo_do_start_fbuf) (uint8_t * /*const*/ * buf, void * id);
extern int METH(vo_has_start_fbuf) (void);
extern void METH(vo_do_draw) (uint8_t * /*const*/ * buf, void * id);
extern int METH(vo_has_draw) (void);
extern void METH(vo_do_discard) (uint8_t * /*const*/ * buf, void * id);
extern void METH(vo_do_close) (void);
#endif

#endif /* LIBMPEG2_VIDEO_OUT_H */
