/*
 * mpeg2convert.h
 * Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
 * Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>
 *
 * This file is part of mpeg2dec, a free MPEG-2 video stream decoder.
 * See http://libmpeg2.sourceforge.net/ for updates.
 *
 * mpeg2dec is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mpeg2dec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with mpeg2dec; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef LIBMPEG2_MPEG2CONVERT_H
#define LIBMPEG2_MPEG2CONVERT_H

#ifdef COMPILING_WITH_CECILIA
#include "mpeg2convert_conversion_type_t.idl.h"
#else
/**
 * This enum is used for telling a conversion setup function:
 * * what kind of output must be set up;
 * * the way R/G/B channel data is expected to be stored for each pixel: RGB or BGR.
 */
typedef enum {
    /*! Convert YUV to RGB */
    MPEG2CONVERT_RGB = 0,
    /*! Convert YUV to BGR */
    MPEG2CONVERT_BGR = 1,
    /*! Convert YUV to UYVY */
    MPEG2CONVERT_UYVY = 2,
    /*! No conversion */
    MPEG2CONVERT_NONE = 4,
    /*! Null conversion */
    MPEG2CONVERT_NULL = 8
} mpeg2convert_conversion_type_t;

#endif

int mpeg2convert_setup (mpeg2convert_conversion_type_t order,
				    unsigned int bpp);

int mpeg2convert_convert (int stage, void * id, const mpeg2_sequence_t * sequence, int stride,
                 uint32_t accel, void * arg, mpeg2_convert_init_t * result);
int mpeg2convert_has_convert(void);

void mpeg2convert_do_copy(void * id, uint8_t * /*const*/ * src, unsigned int v_offset);
int mpeg2convert_has_copy(void);

void mpeg2convert_do_start (void * id, const mpeg2_fbuf_t * fbuf, const mpeg2_picture_t * picture, const mpeg2_gop_t * gop);
int mpeg2convert_has_start(void);

#endif /* LIBMPEG2_MPEG2CONVERT_H */
