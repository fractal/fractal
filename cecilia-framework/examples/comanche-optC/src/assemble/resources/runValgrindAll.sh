#!/bin/sh
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./comanche_noopt
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./comanche
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./comanche_static
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./comanche_merge
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./comanche_staticmerge
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./comanche_all
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./comanche_allnobootstrap
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./comanche_plainc
