#!/bin/sh
valgrind --tool=callgrind --trace-children=yes --dump-instr=yes --trace-jump=yes --simulate-cache=yes --simulate-hwpref=yes ./$1
