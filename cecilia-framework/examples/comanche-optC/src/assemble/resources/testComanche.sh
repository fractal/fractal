#!/bin/bash

# Stress test suite.
# Intended to be run from the working directory of the Web server.

# Don't run tests if the executables don't exist.
HAVE_HTTPERF=`which httperf`
HAVE_TELNET=`which telnet`

if [ "$HAVE_HTTPERF" ]; then
    echo "Test statically created file with httperf, don't print request/reply"
    httperf --timeout=10 --server=localhost --port=8080 --uri=/hello_world --max-connections=300 --rate=1500 --send-buffer=4096 --recv-buffer=16384 --num-conns=2000 --num-calls=1 &> stress_static_noprint.txt
    echo "Test statically created file with httperf, print request/reply (increases time between connection attemps)"
    httperf --print-request --print-reply --timeout=10 --server=localhost --port=8080 --uri=/hello_world --max-connections=300 --rate=1500 --send-buffer=4096 --recv-buffer=16384 --num-conns=2000 --num-calls=1 &> stress_static_print.txt

    echo "Test dynamically created files with httperf"
    cat > stress_httperf_dynamic_print.txt <<EOF
----------------------------------------------------------------------------------------------------
Tests with HTTPerf.
----------------------------------------------------------------------------------------------------
EOF
    # Do 10 times.
    for ((a=1; a <= 10 ; a++))
    do
      TMPFILE=`mktemp HelloWorldf$a.XXXXXXXX` && {
        echo "Test dynamically created file #$a with httperf, print request/reply"
        cat > "$TMPFILE" <<EOF
Hello world $a :-)
EOF
        cat >> stress_httperf_dynamic_print.txt <<EOF
----------------------------------------------------------------------------------------------------
Test #$a with HTTPerf.
----------------------------------------------------------------------------------------------------
EOF
        httperf --print-request --print-reply --timeout=5 --server=localhost --port=8080 --uri="/$TMPFILE" --max-connections=150 --rate=1500 --send-buffer=4096 --recv-buffer=16384 --num-conns=100 --num-calls=1 >> stress_httperf_dynamic_print.txt 2>&1
    }
    done
    rm -f HelloWorldf?.*
    rm -f HelloWorldf??.*
fi


if [ "$HAVE_TELNET" ]; then
    echo "Test dynamically created files with telnet"
    rm -f stress_telnet_dynamic_print.txt
    # Do 100 times.
    for ((a=0; a < 100 ; a++))
    do
      TMPFILE=`mktemp HelloWorldf$a.XXXXXXXX` && {
        cat > "$TMPFILE"  <<EOF
GET /$TMPFILE 

EOF
        # This creates a race condition: the socket is often closed
        # before the server can write everything to it, but not always.
        # Anyhow, doing so spotted the missing SIGPIPE handling.
        telnet localhost 8080 < "$TMPFILE" >> stress_telnet_dynamic_print.txt 2>&1
        # This creates a race condition (sometimes the file
        # is deleted before the server can read it).
        # Interesting for testing.
        # rm -f "$TMPFILE"
      }
    done
    rm -f HelloWorldf?.*
    rm -f HelloWorldf??.*
fi



# Display results.
if [ "$HAVE_HTTPERF" ]; then
    echo "****************************************************************************************************"
    echo "Tests summary:"
    echo "****************************************************************************************************"
    echo "----------------------------------------------------------------------------------------------------"
    echo "HTTPerf on a static file, don't print request/reply"
    echo "----------------------------------------------------------------------------------------------------"
    cat stress_static_noprint.txt
    echo "----------------------------------------------------------------------------------------------------"
    echo "HTTPerf on a static file, print request/reply"
    echo "----------------------------------------------------------------------------------------------------"
    cat stress_static_print.txt | tail -22 # Number of lines is valid for httperf 0.8
    echo "----------------------------------------------------------------------------------------------------"
    echo "HTTPerf on a dynamic file, print request/reply"
    echo "----------------------------------------------------------------------------------------------------"
    cat stress_httperf_dynamic_print.txt | grep "Error"
fi
if [ "$HAVE_TELNET" ]; then
    echo "----------------------------------------------------------------------------------------------------"
    echo "Telnet on a dynamic file"
    echo "----------------------------------------------------------------------------------------------------"
    echo "`cat stress_telnet_dynamic_print.txt | wc -l` total lines in telnet trace."
    echo "`cat stress_telnet_dynamic_print.txt | sort | uniq | wc -l` different lines in telnet trace:"
    cat stress_telnet_dynamic_print.txt | sort | uniq
fi
