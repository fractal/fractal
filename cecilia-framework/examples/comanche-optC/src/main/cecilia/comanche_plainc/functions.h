#ifndef __FUNCTIONS_H__
#define __FUNCTIONS_H__

void comanche_l_log (char * msg);
int comanche_erh_handleRequest (void * r);
int comanche_frh_handleRequest (void * r);
int comanche_ra_handleRequest (void * r);
int comanche_rd_handleRequest (void * r);
void comanche_s_schedule (void * task);

#endif
