/**
 * Cecilia, an implementation of the Fractal component model in C.
 *
 * Copyright (C) 2007 ST Microelectronics, INRIA
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Matthieu Leclercq, Lionel Debroux.
 */

// START SNIPPET: Content
#include <stdio.h>
#include <errno.h>
#include <unistd.h>

/** Declare component internal data */
typedef struct {
  // empty
} PRIVATE_DATA;

#include "ErrorCodes.h"
#include "FunctionArgTypes.h"

/**
 * Reply when no file was found.
 */
#define ERRORSTRING "HTTP/1.0 404 Not found\n\n<html>Document not found.</html>\n"
// -----------------------------------------------------------------------------
// Implementation of the RequestHandler interface.
// -----------------------------------------------------------------------------
/**
 * This method is called by the request dispatcher.
 * Cecilia IDL declaration: int handleRequest(any r);
 * @arg r The name of the requested file.
 */
int comanche_erh_handleRequest (void * r) {
    int fd;
    char * request;
    const char * pInBuffer;
    static const char * const ERROR_STRING = ERRORSTRING;
    size_t nleft;
    ssize_t nwritten;

    request = ((RequestHandlerArgs *)r)->request;
    fd = ((RequestHandlerArgs *)r)->fd;

#ifdef DEBUG
    fprintf(stdout,"ErrorRequestHandler: handleRequest \"%s\"\n",(char *)request);
#endif

    pInBuffer = ERROR_STRING;
    nleft = sizeof(ERRORSTRING) - 1;
    while (nleft > 0) {
        if ((nwritten = write(fd, pInBuffer, nleft)) <= 0) {
            if (errno == EINTR) {
                // Interrupted by sig handler return, retry write.
                nwritten = 0;
            }
            else {
                return Comanche_Error_writing_socket; // THIS RETURNS !
            }
        }
        nleft -= nwritten;
        pInBuffer += nwritten;
    }

    return Comanche_ErrorCode_OK; // Success.
}

#undef ERRORSTRING
