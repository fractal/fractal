// START SNIPPET: Content
/**
 * This header file contains portions of csapp.h/c, known from
 * Computer Systems: A Programmer's Perspective
 * R. E. Bryant, D. O'Hallaron
 * Prentice Hall, 2003
 * ISBN-13: 978-0130340740
 */

#ifndef __CSAPP_H__
#define __CSAPP_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <setjmp.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/**
 * Type of a POSIX signal handler.
 */
typedef void handler_t(int);

/**
 * Safe wrapper for sigaction().
 * @arg signum The number of the signal.
 * @arg handler Pointer to the function that will handle signal signum from now on.
 * @return The previous signal handler.
 */
handler_t *Signal(int signum, handler_t *handler);

/**
 * Wrapper for socket open + listen.
 * @arg port The socket port
 * @return -1 in case of failure, otherwise the file descriptor of the socket.
 */
int open_listenfd(int port);

#endif
