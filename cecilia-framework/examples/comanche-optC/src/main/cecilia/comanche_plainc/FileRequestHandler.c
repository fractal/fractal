/**
 * Cecilia, an implementation of the Fractal component model in C.
 *
 * Copyright (C) 2007 ST Microelectronics, INRIA
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Matthieu Leclercq, Lionel Debroux.
 */

// START SNIPPET: Content
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

/** Declare component internal data */
typedef struct {
  // empty
} PRIVATE_DATA;

#include "ErrorCodes.h"
#include "FunctionArgTypes.h"

/**
 * The file is read by chunks of this many bytes.
 */
#define BUFFER_SIZE (4096)
/**
 * Beginning of reply when file was found.
 */
#define OKSTRING "HTTP/1.0 200 OK\r\n"


// -----------------------------------------------------------------------------
// Implementation of the RequestHandler interface.
// -----------------------------------------------------------------------------
/**
 * This method is called by the request dispatcher.
 * Cecilia IDL declaration: int handleRequest(any r);
 * @arg r The name of the requested file.
 */
int comanche_frh_handleRequest (void * r) {  
    FILE *f;
    char buffer[BUFFER_SIZE];
    int fd;
    char * request;
    const char * pInBuffer;
    static const char * const OK_STRING = OKSTRING;
    size_t nleft;
    ssize_t nwritten;
    struct stat fileinfo;
    off_t contentlength;

    request = ((RequestHandlerArgs *)r)->request;
    fd = ((RequestHandlerArgs *)r)->fd;

#ifdef DEBUG
    fprintf(stdout,"FileRequestHandler: handleRequest \"%s\"\n",(char *)request);
#endif

    f = fopen(request,"r");

    if (f != NULL) {

        // Get file info, and make sure it's a regular file.
        if (stat(request,&fileinfo) < 0) {
            fclose(f);
            fprintf(stderr,"FileRequestHandler: couldn't stat file.\n");
            return Comanche_Error_reading_file; // THIS RETURNS !
        }
        else {
            if (!(S_ISREG(fileinfo.st_mode))) {
                fclose(f);
                fprintf(stderr,"FileRequestHandler: not a regular file.\n");
                return Comanche_Nonexistent_file; // THIS RETURNS !
            }
            contentlength = fileinfo.st_size;
        }

        // Write beginning of reply.
        nleft = sizeof(OKSTRING) - 1;
        pInBuffer = OK_STRING;
#ifdef DEBUG
        fprintf(stdout,"FileRequestHandler: going to write OKSTRING.\n");
#endif
        while (nleft > 0) {
            if ((nwritten = write(fd, pInBuffer, nleft)) <= 0) {
                if (errno == EINTR) {
                    // Interrupted by sig handler return, retry write.
                    nwritten = 0;
                }
                else {
                    // Handle gracefully other errors, for example EPIPE (broken pipe).
                    fclose(f);
                    fprintf(stderr,"FileRequestHandler: couldn't write OKSTRING.\n");
                    return Comanche_Error_writing_socket; // THIS RETURNS !
                }
            }
            nleft -= nwritten;
            pInBuffer += nwritten;
        }
#ifdef DEBUG
        fprintf(stdout,"FileRequestHandler: wrote OKSTRING.\n");
#endif


        // Write Content-Length header, and extra CRLF.
        sprintf(buffer, "Content-Length: %ld\r\n\r\n", contentlength);
        nleft = strlen(buffer);
        pInBuffer = buffer;
#ifdef DEBUG
        fprintf(stdout,"FileRequestHandler: going to write Content-Length.\n");
#endif
        while (nleft > 0) {
            if ((nwritten = write(fd, pInBuffer, nleft)) <= 0) {
                if (errno == EINTR) {
                    // Interrupted by sig handler return, retry write.
                    nwritten = 0;
                }
                else {
                    // Handle gracefully other errors, for example EPIPE (broken pipe).
                    fclose(f);
                    fprintf(stderr,"FileRequestHandler: couldn't write Content-Length.\n");
                    return Comanche_Error_writing_socket; // THIS RETURNS !
                }
            }
            nleft -= nwritten;
            pInBuffer += nwritten;
        }
#ifdef DEBUG
        fprintf(stdout,"FileRequestHandler: wrote Content-Length.\n");
#endif


        // Write contents of file.
        while (1) {
            if (feof(f)) {
                // Success: message and file sent correctly.
                fclose(f);
#ifdef DEBUG
                fprintf(stdout,"FileRequestHandler: feof was true.\n");
#endif
                return Comanche_ErrorCode_OK; // THIS RETURNS !
            }
            else if (ferror(f)) {
                // An error occurred.
                fclose(f);
                fprintf(stderr,"FileRequestHandler: error reading file.\n");
                return Comanche_Error_reading_file; // THIS RETURNS !
            }
#ifdef DEBUG
            fprintf(stdout,"FileRequestHandler: going to read a chunk of file.\n");
#endif
            nleft = fread((void *)buffer, 1, BUFFER_SIZE, f);
#ifdef DEBUG
            fprintf(stdout,"FileRequestHandler: read %d bytes\n",nleft);
#endif
            pInBuffer = buffer;
            while (nleft > 0) {
#ifdef DEBUG
                fprintf(stdout,"FileRequestHandler: going to write file to socket.\n");
#endif
                if ((nwritten = write(fd, pInBuffer, nleft)) <= 0) {
                    if (errno == EINTR) {
                        // Interrupted by sig handler return, retry write.
                        nwritten = 0;
                    }
                    else {
                        // Handle gracefully other errors, for example EPIPE (broken pipe).
                        fclose(f);
                        fprintf(stderr,"FileRequestHandler: couldn't write file to socket.\n");
                        return Comanche_Error_writing_socket; // THIS RETURNS !
                    }
                }
                nleft -= nwritten;
                pInBuffer += nwritten;
            }
#ifdef DEBUG
            fprintf(stdout,"FileRequestHandler: written a chunk of file to socket.\n");
#endif
        }
    }
#ifdef DEBUG
    fprintf(stdout,"FileRequestHandler: file does not exist.\n");
#endif
    return Comanche_Nonexistent_file;
}

#undef OKSTRING
#undef BUFFER_SIZE
