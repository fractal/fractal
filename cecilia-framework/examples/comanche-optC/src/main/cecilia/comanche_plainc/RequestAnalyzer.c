/**
 * Cecilia, an implementation of the Fractal component model in C.
 *
 * Copyright (C) 2007 ST Microelectronics, INRIA
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Matthieu Leclercq, Lionel Debroux.
 */

// START SNIPPET: Content
/** Declare component internal data */
typedef struct {
  // empty
} PRIVATE_DATA;

#include "ErrorCodes.h"
#include "FunctionArgTypes.h"
#include "csapp.h"
#include "functions.h"

/**
 * Maximum length of a request (the request is truncated if it is longer than
 * REQUEST_LEN bytes).
 */
#define REQUEST_LEN (8191)

/**
 * SIGPIPE handler: handle SIGPIPE gracefully (the default handler kills the process).
 */
static void SIGPIPE_handler(int val) {
    fprintf(stderr,"Pipe incorrectly closed (errno = %d, handler argument = %d) !\n",errno,val);
}


// -----------------------------------------------------------------------------
// Implementation of the RequestHandler interface.
// -----------------------------------------------------------------------------
/**
 * This method is called by the request receiver.
 * Cecilia IDL declaration: int handleRequest(any r);
 */
int comanche_ra_handleRequest (void * r) {
#ifdef DEBUG
    fprintf(stdout,"RequestAnalyzer: handleRequest\n");
#endif
    int connectfd;
    char request[REQUEST_LEN+1];
    int nread;
    unsigned int i;
    char * ptr;
    RequestHandlerArgs args;
    int retval;
    handler_t * oldSIGPIPE;

    // Redirect SIGPIPE handler for this thread.
    if ((oldSIGPIPE = Signal(SIGPIPE,SIGPIPE_handler)) != (handler_t *)-1) {

        // Retrieve file descriptor, read on it.
        connectfd = (int)(intptr_t)r; // r was converted from an int, and sizeof(int) <= sizeof(void*), so this is valid.
        nread = read(connectfd,(void *)request, REQUEST_LEN);

        if (nread >= 0) {
            // We read something.
            if (nread == REQUEST_LEN) {
                fprintf(stderr,"RequestAnalyzer: truncating long request.\n");
            }
            request[nread] = '\0'; // Null-terminate string.

            // We want to read only the first line: truncate string at first CR or LF.
            ptr = request;
            for (i = 0; i < (unsigned int)nread; i++) {
                char c = *ptr++;
                if ((c == '\n') || (c == '\r')) {
                    *(ptr-1) = '\0';
                    break;
                }
            }

            // Log the request.
            comanche_l_log (request);

            // Does the request start with "GET /" ?
            if (!strncmp(request,"GET /",5)) {
                // Does the rest of the line contain a space ?
                char * space = strchr((char *)request+5, ' ');
                if (space != NULL) {
                    // Request looks well-formed enough. Null-terminate it.
                    *space = '\0';
                    args.request = request+5;
                    args.fd = connectfd;
                    // Pass request to the request dispatcher.
                    retval = comanche_rd_handleRequest ((void *)&args);
                }
            }
        }
        else {
            retval = Comanche_Error_reading_socket;
            fprintf(stderr,"RequestAnalyzer: error reading from socket !\n");
        }

        // Don't forget to close the socket.
        close(connectfd);

        // Don't forget to restore the old handler.
        Signal(SIGPIPE,oldSIGPIPE);
        retval = Comanche_ErrorCode_OK;
    }
    else {
        retval = Comanche_Error_reading_socket;
        fprintf(stderr,"RequestAnalyzer, handleRequest: cannot redirect SIGPIPE, aborting.\n");
    }
    return retval;
}

#undef REQUEST_LEN
