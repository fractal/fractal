/**
 * Cecilia, an implementation of the Fractal component model in C.
 *
 * Copyright (C) 2007 ST Microelectronics, INRIA
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Matthieu Leclercq, Lionel Debroux.
 */

// START SNIPPET: Content
#include <pthread.h>

/** A semaphore type, implemented with a POSIX mutex and POSIX condition */
typedef struct {
    /** Mutex used to protect accesses to the condition */
    pthread_mutex_t mutex;
    /** Thread queue */
    pthread_cond_t cond;
    /** Number of threads waiting on the condition */
    int waiting;
} Semaphore;

/**
 * Initialize given semaphore with given number of entries in waiting queue
 * @arg s the semaphore to be initialized
 * @arg n the number of entries in waiting queue
 */
void semInit(Semaphore * s, int n);

/**
 * Deinitialize given semaphore (destroy waiting queue).
 * @arg s the semaphore to be deinitialized
 */
void semDeInit(Semaphore *s);

/**
 * P() operation on semaphore: enter.
 * @arg s the semaphore to be entered.
 */
void semP(Semaphore * s);

/**
 * V() operation on semaphore: exit.
 * @arg s the semaphore to be exited.
 */
void semV(Semaphore * s);


