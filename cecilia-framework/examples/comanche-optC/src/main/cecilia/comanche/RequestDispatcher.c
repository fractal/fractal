/**
 * Cecilia, an implementation of the Fractal component model in C.
 *
 * Copyright (C) 2007 ST Microelectronics, INRIA
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Matthieu Leclercq, Lionel Debroux
 */

// START SNIPPET: Content
#include <stdio.h>
#include <string.h>

/** Declare component internal data */
typedef struct {
  // empty
} PRIVATE_DATA;

#include "ErrorCodes.h"

// -----------------------------------------------------------------------------
// Implementation of the RequestHandler interface.
// -----------------------------------------------------------------------------
/**
 * This method is called by the request analyser.
 * Cecilia IDL declaration: int handleRequest(any r);
 */
int METH(handleRequest) (void * r) {
    int result;
#ifdef DEBUG
    fprintf(stdout,"RequestDispatcher: handleRequest\n");
#endif

    result = CLIENT(h1, handleRequest) (r);
    // If the first handler could not handle the first request,
    // check why, and call the second handler if necessary.
    if (result == Comanche_Nonexistent_file) {
        result = CLIENT(h2, handleRequest) (r);
    }

    return result;
}
