This plugin provides the "cecilia-library" and "cecilia-application" packaging
types. 

It also provides specialized
org.apache.maven.plugins:maven-dependency-plugin:unpack-dependencies
goals with an appropriate default configurations for these packaging types.

################################################################################
#                 BUILD NOTES
################################################################################

You may encounter problems when deploying new SNAPSHOT artifact of this module.
In particular the integration tests may fail. This is due to the fact that
artifacts deployed on the OW2 maven repository, are not immediately available
(you have to push the magic button on the OW2 forge to make them available). In
that case, you can skip the execution of the integration tests by using the
following command:

$ mvn clean deploy -Dintegration-test.skip
