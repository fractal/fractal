This plugin provides the capability to build and unpack "car" (Cecilia ARchives)
files.

A .car package is just zip file, but specially treated by the 
"maven-cecilia-plugin" as part of the dependency resolution and unpacking during
the build lifecycle of a Cecilia application or library making use of the Maven
facilities created to handle them.

################################################################################
#                 BUILD NOTES
################################################################################

You may encounter problems when deploying new SNAPSHOT artifact of this module.
In particular the integration tests may fail. This is due to the fact that
artifacts deployed on the OW2 maven repository, are not immediately available
(you have to push the magic button on the OW2 forge to make them available). In
that case, you can skip the execution of the integration tests by using the
following command:

$ mvn clean deploy -Dintegration-test.skip
