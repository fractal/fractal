/***
 * Cecilia
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.maven.plugin.car;

import java.io.File;

/**
 * Build a CAR of the test code for the current project.
 *
 * @goal test-car
 * @phase package
 * @requiresProject
 */
public class TestCarMojo extends AbstractCarMojo {

    /**
     * Directory containing the test classes.
     *
     * @parameter expression="${project.build.testOutputDirectory}"
     * @required
     */
    private File testClassesDirectory;

    protected String getClassifier()
    {
        return "tests";
    }

    /**
     * @return type of the generated artifact
     */
    protected String getType()
    {
        return "test-car";
    }

    /**
     * Return the test-classes directory, to serve as the root of the tests jar.
     */
    protected File getClassesDirectory()
    {
        return testClassesDirectory;
    }
}
