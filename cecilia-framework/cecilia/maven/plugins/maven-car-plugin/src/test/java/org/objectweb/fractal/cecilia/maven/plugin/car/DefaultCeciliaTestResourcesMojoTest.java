/***
 * Cecilia
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.maven.plugin.car;

import java.io.File;

import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.apache.maven.project.MavenProject;

/**
 * Test the {@link DefaultCeciliaResourcesMojo} mojo.
 */
public class DefaultCeciliaTestResourcesMojoTest extends AbstractMojoTestCase {

  private static final String POM = "/target/test-classes/unit/set-default-test-resources/plugin-config.xml";

  protected void setUp() throws Exception {
    // required for mojo lookups to work
    super.setUp();
  }

  /**
   * tests the proper discovery and configuration of the mojo
   * 
   * @throws Exception
   */
  public void testEnvironment() throws Exception {
    File testPom = new File(getBasedir(), POM);
    DefaultCeciliaTestResourcesMojo mojo = (DefaultCeciliaTestResourcesMojo) lookupMojo(
        "set-default-test-resources", testPom);
    assertNotNull(mojo);

    String[] includes = (String[]) getVariableValueFromObject(mojo, "includes");
    String[] excludes = (String[]) getVariableValueFromObject(mojo, "excludes");

    assertEquals(2, includes.length);
    assertEquals("include1", includes[0]);
    assertEquals("include2", includes[1]);

    assertEquals(2, excludes.length);
    assertEquals("exclude1", excludes[0]);
    assertEquals("exclude2", excludes[1]);

    assertEquals(new File("ceciliaTestDirectoryValue"), getVariableValueFromObject(
        mojo, "ceciliaTestDirectory"));

    MavenProject project = (MavenProject) getVariableValueFromObject(mojo, "project");
    
    assertEquals("foo", project.getGroupId());
    assertEquals("bar", project.getArtifactId());
  }
}
