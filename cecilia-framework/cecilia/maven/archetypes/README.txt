This module contains various Maven archetype to quick start Cecilia projects: 
 
 * maven-archetype-cecilia-application   : a maven archetype to kick start a Cecilia application project template
 * maven-archetype-cecilia-library       : a maven archetype to kick start a Cecilia components library project template 