The "src/main/cecilia" folder should contains the sources of your application (*.c, *.h, *.idl, *.fractal), eventually
organized under different packages.

If you have just generated this project using the "maven-archetype-cecilia-library", you can then play with the
above files, for example with:
 
 $ mvn clean package
 
which will create a ".car" (Cecilia ARchive) file in the "./target" directory.