
DECLARE_DATA {
  // empy instance data section
};

// mandatory cecilia.h (*after* DECLARE_DATA)
#include "cecilia.h"

/**
 * Implementation of the "doNothing" method for the provided interface "s".
 * See file ComponentInterface.idl and ComponentDefinition.fractal also.
 */
jint METHOD(s, returnZero)(void* _this) {
	
	return 0;
}