###################################################################################
#                OVERVIEW          
###################################################################################
This directory contains Cecilia related modules:

The organization of the cecilia sources is the following (maven coordinates are
given between '<' and '>'. "o.o.f" stands for "org.objectweb.fractal"):

cecilia <o.o.f:cecilia> : the directory containing this file.
 |
 |- toolchain <o.o.f.cecilia.toolchain:toolchain-parent> : A multi-module 
 |   |                    project containing the Cecilia ADL Toolchain and 
 |   |                    related facilities
 |   |
 |   |- cecilia-adl <o.o.f.cecilia.toolchain:cecilia-adl> : The cecilia ADL
 |   |                    compiler
 |   |
 |   |- idl-parser <o.o.f.cecilia.toolchain:idl-parser> : The cecilia IDL parser
 |   |
 |   |- plugin-framework <o.o.f.cecilia.toolchain:plugin-framework> : The plugin
 |   |                    framework used by the cecilia ADL compiler to 
 |   |                    dynamically load compilation components
 |   |
 |   |- task-codegen <o.o.f.cecilia.toolchain:task-codegen> : Facilities for
 |   |                    code generation tasks.
 |   |
 |   |- maven-cecilia-plugin <o.o.f.cecilia.toolchain:maven-ceciliaadl-plugin>
 |                        A Maven plugin to run the cecilia ADL compiler inside
 |                        a maven build
 |    
 |- runtime <o.o.f.cecilia:runtime> : A multi-module project containing the 
 |   |                    various cecilia runtime libraries
 |   |
 |   |- fractal-cecilia-api <o.o.f.cecilia:fractal-cecilia-api> The Fractal APIs
 |   |                    written in the Cecilia IDL language
 |   |
 |   |- cecilia <o.o.f.cecilia:cecilia> The implementation of the Fractal APIs
 |   |                    in C
 |   |
 |   |- cecilia-baselib <o.o.f.cecilia:cecilia-baselib> A base component library
 |                        for Cecilia
 |
 |- maven <o.o.f.cecilia.maven:parent> : A multi-module project containing the 
     |                    various maven plugins and archetypes that can be used
     |                    for developing cecilia applications using maven.
     |
     |- plugins <o.o.f.cecilia.maven:maven-plugins-parent> : A multi-module 
     |   |                project containing maven plugins
     |   |
     |   |- maven-car-plugin <o.o.f.cecilia:maven-car-plugin> : A maven plugin
     |   |                to build ".car" file. "car" stands for Cecilia Archive
     |   |
     |   |- maven-cecilia-plugin <o.o.f.cecilia:maven-cecilia-plugin> : A maven
     |                    plugins that defines packaging types for cecilia 
     |                    component library and cecilia application.
     |
     |- archetypes <o.o.f.cecilia.maven:maven-archetypes-parent> : A multi-module
         |                project containing maven archetypes
         |
         |- maven-archetype-cecilia-application <o.o.f.cecilia:maven-archetype-cecilia-application>
         |                a maven archetype for cecilia application project.
         |
         |- maven-archetype-cecilia-library <o.o.f.cecilia:maven-archetype-cecilia-library>
                          a maven archetype for cecilia library project.

################################################################################
#                 BUILD NOTES
################################################################################

To build Cecilia and install produced artifact in your local maven repository 
perform the following command:

$ mvn clean install

Binary distribution can then be found in the 'toolchain/cecilia-adl/target' 
sub-directory.


################################################################################

To build a Cecilia source distribution perform the following command:

$ mvn clean assembly:attached

Source distributions can then be found in the 'target' sub-directory.


################################################################################

To deploy new artifacts on the OW2 maven repository perform the following 
command:

$ mvn clean deploy

You may encounter problems when deploying new SNAPSHOT artifact. In particular
the integration tests of various maven plugins may fail. This is due to the fact
that artifacts deployed on the OW2 maven repository, are not immediately 
available (you have to push the magic button on the OW2 forge to make them 
available). In that case, you can skip the execution of the integration tests by
using the following command:

$ mvn clean deploy -Dintegration-test.skip


################################################################################

To release a new stable version of Cecilia perform the two following commands:

$ mvn release:prepare -Dow.username=<your OW2 login>
$ mvn release:perform -Dow.username=<your OW2 login>

Where "<your OW2 login>" is your login on the OW2 forge. This is necessary only
if your local login does not match the OW2 one.
These commands will update version information in POMs, tag the repository, 
build and deploy new artifacts, creates binary and source distributions and copy
them on the objectweb forge. Then, you must add manually the released files in
the "File" section of the OW2 forge.
Notes: 
 - for some mysterious reasons, the first command may fail while creating the
SVN tag. In that case, simply re-run the same command again. (it seems that a 
race condition occurs when two SVN commands are executed one immediately after
the other)
 - the two previous commands can be shortened into the following one:

$ mvn release:prepare release:perform -Dow.username=<your OW2 login>


################################################################################

To generate the documentation web site locally perform the following command:

& mvn clean site-deploy -Plocal-site-deploy -Dlocal.deploy.dir=<output directory>

Where "<output directory>" is the directory into which the site will be 
generated (default output directory is /tmp/cecilia-site/${project.version}).

NOTE: site generation does not work correctly on Windows

By default, some Maven reports (Javadoc, test coverage, ...) are not included in
site generation since they are pretty long to generate, which may be painful 
when writing documentation. To include these reports in generated site you must
activate the "full-reporting" profile. To do so use the following command:

& mvn clean site-deploy -Plocal-site-deploy,full-reporting -Dlocal.deploy.dir=<output directory>

Note that the "full-reporting" profile is automatically activated while 
performing releases.
