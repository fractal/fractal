################################################
#           OVERVIEW
################################################

This module contains component definitions (in the form of Fractal ADL .fractal files) 
and primitive components implementation (in the form of Java classes) of a Fractal ADL module
which allows to load arbitrary components whose client interfaces dependencies
will be bound by the PluginManager. 