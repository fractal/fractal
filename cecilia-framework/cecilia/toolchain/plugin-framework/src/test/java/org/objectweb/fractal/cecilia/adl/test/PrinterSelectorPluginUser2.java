
package org.objectweb.fractal.cecilia.adl.test;

import java.util.HashMap;

import org.objectweb.fractal.cecilia.adl.plugin.PluginException;

public class PrinterSelectorPluginUser2 extends PrinterSelectorPluginUser {

  @Override
  public String getMessage() {
    try {
      // the location of the ADL file for the plugin to be loaded
      super.setADLPackage("test.plugin");
      super.setADLSuffix("");
      final Printer p = super.getPlugin("EnclosingPluginManager",
          new HashMap<Object, Object>(), Printer.class);
      return p.getMessage();

    } catch (final PluginException e) {
      e.printStackTrace();
      throw new RuntimeException("Unable to load plugin", e);
    }
  }

}
