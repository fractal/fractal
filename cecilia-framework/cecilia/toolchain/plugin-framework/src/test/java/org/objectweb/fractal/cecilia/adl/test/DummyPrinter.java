
package org.objectweb.fractal.cecilia.adl.test;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.cecilia.adl.plugin.AbstractPluginManager;

/**
 * @author Alessio Pace
 */
public class DummyPrinter implements Printer, BindingController {

  private Factory factory;

  /**
   * @return the class canonical name.
   */
  public String getMessage() {
    return DummyPrinter.class.getCanonicalName();
  }

  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(AbstractPluginManager.FACTORY)) {
      this.factory = (Factory) o;
    } else {
      throw new NoSuchInterfaceException("No interface named '" + s + "'");
    }

  }

  public String[] listFc() {
    return new String[]{AbstractPluginManager.FACTORY};
  }

  public Object lookupFc(final String s) throws NoSuchInterfaceException {
    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(AbstractPluginManager.FACTORY)) {
      return this.factory;
    } else {
      throw new NoSuchInterfaceException("No interface named '" + s + "'");
    }
  }

  public void unbindFc(final String s) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException {
    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(AbstractPluginManager.FACTORY)) {
      this.factory = null;
    } else {
      throw new NoSuchInterfaceException("No interface named '" + s + "'");
    }
  }

}
