
package org.objectweb.fractal.cecilia.adl.test;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

public class PrinterSelector implements Printer, BindingController {

  public static final String PRINTERS = "printers";

  Map<String, Object>        printers;

  public PrinterSelector() {
    this.printers = new HashMap();
  }

  public String getMessage() {
    return "BLABLA";
  }

  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (s.startsWith(PRINTERS)) {
      this.printers.put(s, o);
    } else {
      throw new NoSuchInterfaceException("No interface named " + s);
    }

  }

  public String[] listFc() {
    return printers.keySet().toArray(new String[0]);
  }

  public Object lookupFc(final String s) throws NoSuchInterfaceException {
    if (s.startsWith(PRINTERS)) {
      return this.printers.get(s);
    } else {
      throw new NoSuchInterfaceException("No interface named " + s);
    }
  }

  public void unbindFc(final String s) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException {
    if (this.printers.containsKey(s)) {
      this.printers.remove(s);
    } else {
      throw new NoSuchInterfaceException("No interface named " + s);
    }

  }

}
