/**
 * 
 */

package org.objectweb.fractal.cecilia.adl.test;

import java.util.HashMap;

import org.objectweb.fractal.cecilia.adl.plugin.AbstractPluginUser;
import org.objectweb.fractal.cecilia.adl.plugin.PluginException;

/**
 * @author Alessio Pace
 */
public class PrinterSelectorPluginUser extends AbstractPluginUser
    implements
      Printer {

  public PrinterSelectorPluginUser() {
    super("plugins");
  }

  public String getMessage() {

    try {
      // the location of the ADL file for the plugin to be loaded
      super.setADLPackage("test.plugin");
      super.setADLSuffix("");
      final Printer p = super.getPlugin("DummyPrinter",
          new HashMap<Object, Object>(), Printer.class);
      return p.getMessage();

    } catch (final PluginException e) {
      e.printStackTrace();
      throw new RuntimeException("Unable to load plugin", e);
    }

  }
}
