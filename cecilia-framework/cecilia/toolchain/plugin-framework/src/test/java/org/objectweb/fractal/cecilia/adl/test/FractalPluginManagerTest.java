
package org.objectweb.fractal.cecilia.adl.test;

import java.util.HashMap;

import junit.framework.TestCase;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.adl.util.ContentControllerHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.cecilia.adl.plugin.AbstractPluginManager;
import org.objectweb.fractal.julia.Julia;
import org.objectweb.fractal.util.Fractal;

/**
 * @author Alessio Pace
 */
public class FractalPluginManagerTest extends TestCase {

  protected Factory                 factory;
  protected HashMap<String, Object> context;

  @Override
  protected void setUp() throws Exception {
    this.factory = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
    assertNotNull("Assert Factory with Fractal Backend not null", this.factory);

    this.context = new HashMap<String, Object>();
    this.context.put(AbstractPluginManager.PLUGIN_FACTORY_BACKEND, "Fractal");

    System.setProperty("fractal.provider", Julia.class.getCanonicalName());
  }

  /**
   * This test loads a primitive plugin, checking that its client interfaces are
   * bound, and that the plugin is present in the plugin-user bindings (inside
   * getFcInterfaces() ).
   */
  public final void testLoadPrimitivePluginWhichHasAClientInterfaceToBeBound()
      throws Exception {

    final Component rootComponent = (Component) this.factory.newComponent(
        "test.plugin.RootComponent", context);
    assertNotNull("Assert root component not null", rootComponent);

    // start the component
    final LifeCycleController lc = Fractal
        .getLifeCycleController(rootComponent);
    lc.startFc();

    // get the interface to test plugins have been loaded
    final Printer printer = (Printer) rootComponent.getFcInterface("printer");
    assertNotNull("Assert printer interface not null", printer);

    final String message = printer.getMessage();
    assertEquals("Assert printed value", DummyPrinter.class.getCanonicalName(),
        message);

    final ContentController cc = Fractal.getContentController(rootComponent);

    final Component[] components = cc.getFcSubComponents();
    /* there should be only the plugin-factory and the enclosing-plugin-manager */
    assertEquals("Assert number of components into root", 2, components.length);

    final Component enclosingPluginComponent = ContentControllerHelper
        .getSubComponentByName(rootComponent, "enclosing-plugin-manager");
    assertNotNull("Assert enclosingPluginComponent is not null",
        enclosingPluginComponent);

    // ===================================================================
    /*
     * The PluginUser should have bound to itself the loaded plugin
     * "test.plugin.DummyPrinter", which should be present in the
     * getFcInterfaces() returned array.
     */
    final Component printerSelectorPluginUser = ContentControllerHelper
        .getSubComponentByName(enclosingPluginComponent, "plugin-user");

    boolean isPresent = false;
    for (final Object o : printerSelectorPluginUser.getFcInterfaces()) {
      final Interface i = (Interface) o;
      if (i.getFcItfName().equals("plugins-" + "DummyPrinter")) {
        isPresent = true;
        break;
      }
    }

    if (!isPresent) {
      fail("The plugin is not present in the plugin-user getFcInterfaces()");
    }

    // ===================================================================

    final ContentController enclosingContentController = Fractal
        .getContentController(enclosingPluginComponent);
    assertEquals("Assert loaded plugin has been added into the content", 3,
        enclosingContentController.getFcSubComponents().length);

    final Component plugin = ContentControllerHelper.getSubComponentByName(
        enclosingPluginComponent, "test.plugin.DummyPrinter".replaceAll("[.]",
            "_"));
    assertNotNull("Assert loader plugin not null", plugin);

    final BindingController pluginBindingController = Fractal
        .getBindingController(plugin);
    assertNotNull("Assert plugin-factory interface not null",
        pluginBindingController.lookupFc(AbstractPluginManager.FACTORY));
  }

  /**
   * This test loads a composite plugin which inside loads another pluging.
   * 
   * @throws Exception
   */
  public void testLoadCompositePluginWhichLoadsAPrimitivePlugin()
      throws Exception {

    final Component rootComponent = (Component) this.factory.newComponent(
        "test.plugin.RootComponent2", context);
    assertNotNull("Assert root component not null", rootComponent);

    // start the component
    final LifeCycleController lc = Fractal
        .getLifeCycleController(rootComponent);
    lc.startFc();

    // get the interface to test plugins have been loaded
    final Printer printer = (Printer) rootComponent.getFcInterface("printer");
    assertNotNull("Assert printer interface not null", printer);

    final String message = printer.getMessage();
    assertEquals("Assert printed value", DummyPrinter.class.getCanonicalName(),
        message);

    final ContentController cc = Fractal.getContentController(rootComponent);

    final Component[] components = cc.getFcSubComponents();
    /* there should be only the plugin-factory and the enclosing-plugin-manager */
    assertEquals("Assert number of components into root", 2, components.length);

    final Component enclosingPluginComponent = ContentControllerHelper
        .getSubComponentByName(rootComponent, "enclosing-plugin-manager");
    assertNotNull("Assert enclosingPluginComponent is not null",
        enclosingPluginComponent);

    // ===================================================================
    /*
     * The PluginUser should have bound to itself the loaded plugin
     * "test.plugin.EnclosingPluginManager", which should be present in the
     * getFcInterfaces() returned array.
     */
    final Component printerSelectorPluginUser = ContentControllerHelper
        .getSubComponentByName(enclosingPluginComponent, "plugin-user");

    boolean isPresent = false;
    for (final Object o : printerSelectorPluginUser.getFcInterfaces()) {
      final Interface i = (Interface) o;
      if (i.getFcItfName().equals("plugins-" + "EnclosingPluginManager")) {
        isPresent = true;
        break;
      }
    }
    if (!isPresent) {
      fail("The plugin interface is not present in the plugin user getFcInterfaces()");
    }

    // ===================================================================

    final ContentController enclosingContentController = Fractal
        .getContentController(enclosingPluginComponent);
    assertEquals("Assert loaded plugin has been added into the content", 3,
        enclosingContentController.getFcSubComponents().length);

    final Component firstPlugin = ContentControllerHelper
        .getSubComponentByName(enclosingPluginComponent,
            "test.plugin.EnclosingPluginManager".replaceAll("[.]", "_"));
    assertNotNull("Assert loader plugin not null", firstPlugin);

    final BindingController firstPluginBindingController = Fractal
        .getBindingController(firstPlugin);
    assertNotNull("Assert plugin-factory interface not null",
        firstPluginBindingController.lookupFc(AbstractPluginManager.FACTORY));

    final Component secondPlugin = ContentControllerHelper
        .getSubComponentByName(firstPlugin, "test.plugin.DummyPrinter"
            .replaceAll("[.]", "_"));
    assertNotNull("Assert loader plugin not null", secondPlugin);

    final BindingController pluginBindingController = Fractal
        .getBindingController(secondPlugin);
    assertNotNull("Assert plugin-factory interface not null",
        pluginBindingController.lookupFc(AbstractPluginManager.FACTORY));
  }

  /**
   * This tests the case in which the plugin-manager is not in the same
   * composite component of the plugin-user. XXX COMMENTED BECAUSE IN THE
   * REQUIREMENT THE plugin-user and plugin-manager should be on the same level.
   */
// public void testPluginUserAndPluginManagerNotAtTheSameLevel()
// throws Exception {
//
// final Component rootComponent = (Component) this.factory.newComponent(
// "test.plugin.RootComponent3", context);
// assertNotNull("Assert root component not null", rootComponent);
//
// // start the component
// final LifeCycleController lc = Fractal
// .getLifeCycleController(rootComponent);
// lc.startFc();
//
// // get the interface to test plugins have been loaded
// final Printer printer = (Printer) rootComponent.getFcInterface("printer");
// assertNotNull("Assert printer interface not null", printer);
//
// final String message = printer.getMessage();
// assertEquals("Assert printed value", DummyPrinter.class.getCanonicalName(),
// message);
//
// }
}
