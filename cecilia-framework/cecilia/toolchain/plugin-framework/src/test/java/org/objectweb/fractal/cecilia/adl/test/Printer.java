
package org.objectweb.fractal.cecilia.adl.test;

public interface Printer {

  String getMessage();
}
