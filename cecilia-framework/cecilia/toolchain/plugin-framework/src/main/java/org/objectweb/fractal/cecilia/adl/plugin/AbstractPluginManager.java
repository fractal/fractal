/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.plugin;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;

/**
 * Abstract plugin manager implementation.
 */
public abstract class AbstractPluginManager
    implements
      PluginManager,
      BindingController {

  /** The name of the interface a loaded plugin must provide. */
  public static final String          PLUGIN_INTERFACE_NAME  = "plugin";

  /**
   * The argument name to indicate the backend which will be used to load
   * plugins.
   */
  public static final String          PLUGIN_FACTORY_BACKEND = "fractaladl.plugin.factoryBackend";

  /** The name of the {@link Factory} client interface of this component. */
  public static final String          FACTORY                = "plugin-factory";

  // --------------------------------------------------------------------------
  // Client interface.
  // --------------------------------------------------------------------------

  /** The {@link Factory} client interface of this component. */
  public Factory                      pluginFactoryItf;

  /** The client interfaces that may be bound to plugin components. */
  protected final Map<String, Object> pluginBindings         = new HashMap<String, Object>();

  // --------------------------------------------------------------------------
  // Abstract methods
  // --------------------------------------------------------------------------

  protected abstract Object createPlugin(final String pluginADL,
      Map<Object, Object> context) throws PluginException;

  // --------------------------------------------------------------------------
  // Implementation of the PluginManager interface
  // --------------------------------------------------------------------------

  public final <T> T getPlugin(final String pluginADL,
      final Map<Object, Object> context, final Class<T> expectedType)
      throws PluginException {

    /* subclasses will provide the actual implementation */
    final Object pluginItf = createPlugin(pluginADL, context);

    if (!expectedType.isInstance(pluginItf))
      throw new PluginException("The plugin '" + pluginADL
          + "' does not provides the expected type");
    return expectedType.cast(pluginItf);
  }

  // --------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // --------------------------------------------------------------------------

  public final String[] listFc() {
    final Set<String> pb = new HashSet<String>(this.pluginBindings.keySet());
    pb.add(FACTORY);

    final String[] result = pb.toArray(new String[0]);
    return result;
  }

  public final Object lookupFc(final String itf)
      throws NoSuchInterfaceException {

    if (itf == null) {
      throw new NoSuchInterfaceException("Interface name can't be null");
    }

    if (itf.equals(FACTORY)) {
      return pluginFactoryItf;
    } else if (pluginBindings.containsKey(itf)) {
      return pluginBindings.get(itf);
    } else {
      /*
       * XXX it is required NOT to throw an Exception because inside a Julia
       * mixin, before performing bindFc() is done a lookUp(), and so if the
       * interface name is not present, the exception would be thrown inhibiting
       * the binding. Usually (see for instance the AbstractPluginUser class)
       * the expected client interfaces name are know "a priori" (with some
       * prefix, ie "client-loader-") but here instead we need to allow
       * arbitrary interface names to be declared in the ADL.
       */
      return null;
    }
  }

  public void bindFc(final String itf, final Object value)
      throws NoSuchInterfaceException {

    if (itf == null) {
      throw new NoSuchInterfaceException("Interface name can't be null");
    }

    if (itf.equals(FACTORY))
      pluginFactoryItf = (Factory) value;
    else
      pluginBindings.put(itf, value);

  }

  public final void unbindFc(final String itf) throws NoSuchInterfaceException {

    if (itf == null) {
      throw new NoSuchInterfaceException("Interface name can't be null");
    }

    if (itf.equals(FACTORY)) {
      pluginFactoryItf = null;
    } else if (this.pluginBindings.containsKey(itf)) {
      pluginBindings.remove(itf);
    } else {
      throw new NoSuchInterfaceException("There is no interface named '" + itf
          + "'");
    }

  }
}