/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.plugin;

import org.objectweb.fractal.api.control.AttributeController;

/**
 * {@link AttributeController} interface for clients of {@link PluginManager}
 * components.
 */
public interface PluginUserAttributes extends AttributeController {

  /**
   * Set the package name that is used by {@link AbstractPluginUser} to
   * determine the name of the plugin ADL.
   * 
   * @param prefix the new value of the <code>ADLPackage</code> attribute.
   * @see AbstractPluginUser#getPlugin(Object, java.util.Map, Class)
   */
  void setADLPackage(String prefix);

  /**
   * Returns the value of the <code>ADLPackage</code> attribute.
   * 
   * @return the value of the <code>ADLPackage</code> attribute.
   * @see #setADLPackage(String)
   */
  String getADLPackage();

  /**
   * Set the suffix that is used by {@link AbstractPluginUser} to determine the
   * name of the plugin ADL.
   * 
   * @param suffix the new value of the <code>ADLSuffix</code> attribute.
   * @see AbstractPluginUser#getPlugin(Object, java.util.Map, Class)
   */
  void setADLSuffix(String suffix);

  /**
   * Returns the value of the <code>ADLSuffix</code> attribute.
   * 
   * @return the value of the <code>ADLSuffix</code> attribute.
   * @see #setADLSuffix(String)
   */
  String getADLSuffix();
}
