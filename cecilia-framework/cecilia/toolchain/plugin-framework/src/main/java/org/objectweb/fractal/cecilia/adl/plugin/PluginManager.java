/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.plugin;

import java.util.Map;

/**
 * Generic plugin manager interface. A plugin is a component which provides only
 * one interface named <code>"plugin"</code> and which is identified by a ADL
 * name.
 */
public interface PluginManager {

  /**
   * Returns the <code>"plugin"</code> interface of the plugin component
   * corresponding to the given ADL.
   * 
   * @param <T> the type of the interface, the plugin should provide.
   * @param pluginADL the name of the ADL of the plugin to returns.
   * @param context additional parameters.
   * @param expectedType Class object of the type of the interface, the plugin
   *            should provides.
   * @return the <code>"plugin"</code> interface of the plugin component.
   * @throws PluginException If the plugin can't be loaded or does not provides
   *             a <code>"plugin"</code> interface with the expected type.
   */
  <T> T getPlugin(String pluginADL, Map<Object, Object> context,
      Class<T> expectedType) throws PluginException;
}
