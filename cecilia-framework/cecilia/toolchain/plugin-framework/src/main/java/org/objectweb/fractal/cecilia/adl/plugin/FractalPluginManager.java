/***
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.plugin;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.util.FractalADLLogManager;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.control.SuperController;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.util.Fractal;

/**
 * A {@link PluginManager} component using a
 * {@link org.objectweb.fractal.adl.FactoryFactory#FRACTAL_BACKEND backend}
 * {@link Factory factory} to load plugin.
 * 
 * @author Alessio Pace
 */
public class FractalPluginManager extends AbstractPluginManager implements
    PluginManager, BindingController {

  Logger logger = FractalADLLogManager.getLogger("fractaladl-plugin-manager");

  /**
   * Julia weavable component.
   */
  Component weaveableC;

  @Override
  public void bindFc(final String itf, final Object value)
      throws NoSuchInterfaceException {

    if ("component".equals(itf)) {
      this.weaveableC = (Component) value;
    } else {
      super.bindFc(itf, value);
    }
  }

  @Override
  protected Object createPlugin(final String pluginADL, final Map ctxt)
      throws PluginException {

    if (logger.isLoggable(Level.FINE)) {
      logger.fine("Load new compiler plugin: \'"
          + pluginADL + "\'");
    }

    // XXX reuse the ctxt Map
    final Map context = new HashMap();
    context.put(PLUGIN_FACTORY_BACKEND, "Fractal");

    /* the result is a Fractal Component */
    try {

      if (this.pluginFactoryItf == null) {
        throw new RuntimeException(
            "The plugin-factory client interface was not bound for component "
                + Fractal.getNameController(this.weaveableC).getFcName());
      }

      /* (1) create the plugin */
      final Component plugin = (Component) pluginFactoryItf.newComponent(
          pluginADL, context);

      escapePluginComponentName(plugin);

      // System.err.println("1. Loaded plugin for ADL: " + pluginADL);

      /* (2) add the newly created plugin to the enclosing composite component */
      this.addPluginToEnclosingComponent(plugin);

      // System.err.println("2. Added plugin for ADL: " + pluginADL
      // + " to enclosing component");

      /*
       * (3) bind the plugin interfaces. XXX this must be done after [2] others
       * ways mixins checks will raise exceptions
       */
      this.bindPluginInterfaces(plugin);

      // System.err.println("3. Bounded interfaces for plugin for ADL: "
      // + pluginADL + " to enclosing component");

      /* (4) start the plugin */
      this.startPlugin(plugin);

      // System.err.println("4. Started plugin for ADL: " + pluginADL
      // + " to enclosing component");

      /* (5) return the "plugin" functional interface */
      final Object pluginInterface = plugin
          .getFcInterface(AbstractPluginManager.PLUGIN_INTERFACE_NAME);

      // System.err.println("5. Getting the plugin interface to return: "
      // + pluginADL + " to enclosing component");

      return pluginInterface;
    } catch (final Exception e) {
      e.printStackTrace();
      throw new PluginException("Unable to load plugin " + pluginADL, e);
    }

  }

  private void escapePluginComponentName(final Component plugin)
      throws NoSuchInterfaceException {
    final NameController nc = Fractal.getNameController(plugin);
    final String originalName = nc.getFcName();
    final String escapedName = originalName.replaceAll("[.]", "_");
    nc.setFcName(escapedName);

  }

  private void startPlugin(final Component plugin)
      throws NoSuchInterfaceException, IllegalLifeCycleException {

    final LifeCycleController lc = Fractal.getLifeCycleController(plugin);

    lc.startFc();

  }

  protected void addPluginToEnclosingComponent(final Component plugin)
      throws NoSuchInterfaceException, IllegalContentException,
      IllegalLifeCycleException {

    /*
     * the pluginFactoryItf Interface which is an internal interface of the
     * enclosing component
     */
    // final Interface i = (Interface) pluginFactoryItf;
    //
    // /* the component which contains this plugin-manager */
    // final Component pluginFactoryItfOwnerComponent = i.getFcItfOwner();
    //
    // // DEBUGGING
    // final NameController nc = Fractal
    // .getNameController(pluginFactoryItfOwnerComponent);
    // System.err.println("The plugin will be added inside: " + nc.getFcName());
    //
    // final ContentController contentController = Fractal
    // .getContentController(pluginFactoryItfOwnerComponent);
    /* add the plugin component inside the enclosing composite component */
    // XXX shoud a mixin be commented?
    // contentController.addFcSubComponent(plugin);
    // System.err.println("Getting super controller for component "
    // + Fractal.getNameController(this.weaveableC).getFcName());
    final SuperController superController = Fractal
        .getSuperController(this.weaveableC);

    final Component superComponent = superController.getFcSuperComponents()[0];
    final ContentController contentController = Fractal
        .getContentController(superComponent);
    // System.err.println("Adding plugin inside: "
    // + Fractal.getNameController(superComponent).getFcName());
    contentController.addFcSubComponent(plugin);

  }

  /**
   * Bind the plugin client interfaces to the same interfaces to which the
   * plugin manager is bound, matching the same name.
   * 
   * @param plugin
   * @throws NoSuchInterfaceException
   * @throws IllegalBindingException
   * @throws IllegalLifeCycleException
   * @throws PluginException
   */
  protected void bindPluginInterfaces(final Component plugin)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException, PluginException {

    final BindingController pluginBindingController = Fractal
        .getBindingController(plugin);

    final BindingController pluginManagerBindingController = Fractal
        .getBindingController(this.weaveableC);

    if (pluginBindingController != null) {

      final String[] pluginClientInterfaces = pluginBindingController.listFc();
      // System.out.println("Plugin required client itfs: "
      // + Arrays.deepToString(pluginClientInterfaces));
      // System.out.println("---> This plugin manager client its: "
      // + Arrays.deepToString(this.listFc()));

      for (final String pluginItfName : pluginClientInterfaces) {
        // System.err.println("Processing plugin interface: " + pluginItfName);

        final Interface i = (Interface) plugin.getFcInterface(pluginItfName);
        final InterfaceType itfType = (InterfaceType) i.getFcItfType();

        /* if it is a client interface */
        if (itfType.isFcClientItf() && !i.isFcInternalItf()) {
          // System.err.println("The interface: " + pluginItfName
          // + " is client and not internal");

          Object pluginManagerItf = null;
          try {
            pluginManagerItf = pluginManagerBindingController
                .lookupFc(pluginItfName);
          } catch (final Exception e) {
            e.printStackTrace();
            throw new RuntimeException(
                "The plugin manager does not have an interface '"
                    + pluginItfName + "'");
          }
          // this may throw NoSuchInterfaceException or IllegalBindingException

          try {
            pluginBindingController.bindFc(pluginItfName, pluginManagerItf);
          } catch (final Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to bind the plugin interface '"
                + pluginItfName + "' with the one of the plugin manager");
          }
        }

      }
    }
  }
}
