/**
 * Cecilia ADL Compiler
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.plugin;

import static org.objectweb.fractal.adl.error.ErrorTemplateValidator.validErrorTemplate;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.error.ErrorTemplate;

/** {@link ErrorTemplate} group for the plugin package. */
public enum PluginErrors implements ErrorTemplate {

  /** Template used when the plugin can't be found in the classpath. */
  PLUGIN_NOT_FOUND("Plugin \"%s\" can't be found", "plugname"),

  /**
   * Template used when an {@link ADLException} occurs while instantiating the
   * plugin.
   */
  PLUGIN_INSTANTIATION("An error occurs while loading plugin \"%s\".",
      "plugname"),

  /**
   * Template used when an exception occurs while binding plugin client
   * interfaces.
   */
  PLUGIN_BINDING(
      "An error occurs while binding \"%s\" client interface of plugin",
      "citfname");

  /** The groupId of ErrorTemplates defined in this enumeration. */
  public static final String   GROUP_ID     = "PLG";

  private int                  id;
  private String               format;

  private PluginErrors(final String format, final Object... args) {
    this.id = ordinal();
    this.format = format;

    assert validErrorTemplate(this, args);
  }

  public int getErrorId() {
    return id;
  }

  public String getGroupId() {
    return GROUP_ID;
  }

  public String getFormatedMessage(final Object... args) {
    return String.format(format, args);
  }

  public String getFormat() {
    return format;
  }
}
