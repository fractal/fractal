/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.plugin;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.util.Fractal;

/**
 * Abstract implementation of component using a
 * {@link PluginManager plugin manager} to retrieve components bound to a client
 * collection interface.
 * 
 * @see #getPlugin(Object, Map, Class)
 */
public abstract class AbstractPluginUser
    implements
      PluginUserAttributes,
      BindingController {

  /**
   * Julia weavable Component reference.
   */
  protected Component              weaveableC;

  // --------------------------------------------------------------------------
  // Attribute fields
  // --------------------------------------------------------------------------

  private String                   adlPackage;
  private String                   adlSuffix;

  // --------------------------------------------------------------------------
  // Client interface.
  // --------------------------------------------------------------------------

  /**
   * The name of the {@link PluginManager} client interface of this component.
   */
  public static final String       PLUGIN_MANAGER   = "plugin-manager";

  /** The {@link PluginManager} client interface of this component. */
  public PluginManager             pluginManagerItf;

  protected final String           clientCollectionItfName;

  /** The already bounds plugins. */
  public final Map<String, Object> clientPluginsItf = new HashMap<String, Object>();

  // --------------------------------------------------------------------------
  // Constructor
  // --------------------------------------------------------------------------

  protected AbstractPluginUser(final String clientCollectionItfName) {
    this.clientCollectionItfName = clientCollectionItfName;
  }

  // --------------------------------------------------------------------------
  // Utility methods
  // --------------------------------------------------------------------------

  /**
   * This method should be used by sub class to retrieve a client plugin.
   * 
   * @param <T> the type of the interface, the plugin should provides.
   * @param pluginDesc the description of the plugin to be returned.
   * @param context additional parameters passed to plugin manager.
   * @param pluginType the type of the plugin to return.
   * @return plugin who's ADL name is
   *         <code>"&lt;{@link PluginUserAttributes#getADLPackage() ADLPrefix}&gt;.&lt;pluginDesc.toString()&gt;&lt;{@link PluginUserAttributes#getADLSuffix() ADLSuffix}&gt;</code>
   * @throws PluginException If the plugin can't be loaded or does not provides
   *             a <code>"plugin"</code> interface with the expected type.
   * @see PluginManager#getPlugin(String, Map, Class)
   */
  public final <T> T getPlugin(final Object pluginDesc,
      final Map<Object, Object> context, final Class<T> pluginType)
      throws PluginException {

    /*
     * XXX escape the pluginDesc name because otherwise there will be problems
     * in the ADL processing
     */
    final String name = clientCollectionItfName + '-'
        + pluginDesc.toString().replaceAll("[.]", "_");

    BindingController bc = null;
    if (this.weaveableC != null) {
      try {
        bc = Fractal.getBindingController(this.weaveableC);
      } catch (final NoSuchInterfaceException e) {
        e.printStackTrace();
        throw new PluginException("Unexpected exception", e);
      }
    } else {
      bc = this;
    }

    /* the plugin interface to return */
    Object pluginItf = null;
    try {
      // original call
      // pluginItf = clientPluginsItf.get(name);
      pluginItf = bc.lookupFc(name);
    } catch (final NoSuchInterfaceException e) {
      //
    }

    T plugin = pluginType.cast(pluginItf);
    if (plugin == null) {
      final String adlName = ((adlPackage == null) ? pluginDesc : adlPackage
          + '.' + pluginDesc)
          + adlSuffix;

      /* ask the PluginManager to load the plugin */
      plugin = pluginManagerItf.getPlugin(adlName, context, pluginType);

      /* cache it locally */
      // original call
      // clientPluginsItf.put(name, plugin);
      try {
        bc.bindFc(name, plugin);
      } catch (final Exception e) {
        e.printStackTrace();
        throw new PluginException(
            "Unable to bind the loaded plugin interface to the interface '"
                + name + "'");
      }
    }
    return plugin;
  }

  // --------------------------------------------------------------------------
  // Implementation of the AttributeController interface
  // --------------------------------------------------------------------------

  public String getADLPackage() {
    return adlPackage;
  }

  public void setADLPackage(final String prefix) {
    if (prefix == null || "null".equalsIgnoreCase(prefix))
      adlPackage = null;
    else
      adlPackage = prefix;
  }

  public String getADLSuffix() {
    return adlSuffix;
  }

  public void setADLSuffix(final String suffix) {
    if (suffix == null || "null".equalsIgnoreCase(suffix))
      adlSuffix = null;
    else
      adlSuffix = suffix;
  }

  // --------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // --------------------------------------------------------------------------

  public String[] listFc() {
    final String[] itfNames = new String[clientPluginsItf.size() + 1];
    clientPluginsItf.keySet().toArray(itfNames);
    itfNames[clientPluginsItf.size()] = PLUGIN_MANAGER;
    return itfNames;
  }

  public Object lookupFc(final String itf) throws NoSuchInterfaceException {

    if (itf == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (itf.equals(PLUGIN_MANAGER))
      return pluginManagerItf;
    else if (itf.startsWith(clientCollectionItfName)) {
      return clientPluginsItf.get(itf);
    } else {
      throw new NoSuchInterfaceException("There is no interface named '" + itf
          + "'");
    }
  }

  /**
   * XXX If you override this method be sure to call it with super.bindFc() or
   * to set the weaveableC component reference in your overridden
   * implementation.
   */
  public void bindFc(final String itf, final Object value)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (itf == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    /**
     * To set the weaveable Component.
     */
    if (itf.equals("component")) {
      this.weaveableC = (Component) value;
    } else if (itf.equals(PLUGIN_MANAGER)) {
      pluginManagerItf = (PluginManager) value;
    } else if (itf.startsWith(clientCollectionItfName)) {
      clientPluginsItf.put(itf, value);
    } else {
      throw new NoSuchInterfaceException("There is no interface named '" + itf
          + "'");
    }
  }

  public void unbindFc(final String itf) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException {

    if (itf == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (itf.equals(PLUGIN_MANAGER)) {
      pluginManagerItf = null;
    } else if (itf.startsWith(clientCollectionItfName)) {
      clientPluginsItf.remove(itf);
    } else {
      throw new NoSuchInterfaceException("There is no interface named '" + itf
          + "'");
    }
  }
}
