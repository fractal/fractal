/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.plugin;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.adl.ADLErrors;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.JavaFactory;
import org.objectweb.fractal.adl.StaticJavaGenerator;
import org.objectweb.fractal.adl.bindings.JavaBindingUtil;
import org.objectweb.fractal.adl.util.FractalADLLogManager;
import org.objectweb.fractal.api.control.IllegalBindingException;

/**
 * {@link PluginManager} component using a
 * {@link org.objectweb.fractal.adl.FactoryFactory#JAVA_BACKEND java-backend}
 * {@link Factory factory} to load plugin.
 */
public class JavaPluginManager extends AbstractPluginManager {

  /**
   * A system property that can be defined to avoid the instantiation of plugin
   * using statically generated JavaFactory.
   */
  public static final String NO_STATIC_PLUGIN_FACTORY_PROPERTY_NAME = "fractal.pluginFramwork.noStaticPluginFactory";

  private final Logger       pluginLogger                           = FractalADLLogManager
                                                                        .getLogger("plugin-manager");

  private final Logger       launcherLogger                         = FractalADLLogManager
                                                                        .getLogger("launcher");

  @Override
  protected Object createPlugin(final String pluginADL,
      Map<Object, Object> context) throws PluginException {

    if (pluginLogger.isLoggable(Level.FINE))
      pluginLogger.fine("Load new plugin: \'" + pluginADL + "\'");

    Object plugin = null;

    // First try to load a statically generated factory
    if (!System.getProperties().containsKey(
        NO_STATIC_PLUGIN_FACTORY_PROPERTY_NAME)) {
      try {
        final Class<?> factoryFactoryClass = getClass().getClassLoader()
            .loadClass(pluginADL + StaticJavaGenerator.CLASSNAME_SUFFIX);
        final JavaFactory factoryFactory = (JavaFactory) factoryFactoryClass
            .newInstance();
        plugin = factoryFactory.newComponent();
        pluginLogger
            .fine("Plugin instantiated using statically generated factory.");
      } catch (final Exception e) {
        // ignore and try to instantiate the compiler using a FractalADL
        // factory.
        if (launcherLogger.isLoggable(Level.FINE))
          launcherLogger
              .fine("Unable to instantiate cecilia compiler using statically generated factory ("
                  + e.getClass().getName() + ":" + e.getMessage() + ").");
      }
    }

    if (plugin == null) {
      try {

        /* XXX reusing the method actual parameter context Map compilation will */
        context = new HashMap<Object, Object>();
        context.put(PLUGIN_FACTORY_BACKEND, "Java");

        plugin = pluginFactoryItf.newComponent(pluginADL, context);
        pluginLogger.fine("Plugin instantiated using FractalADL factory.");
      } catch (final ADLException e) {
        if (e.getError().getTemplate() == ADLErrors.ADL_NOT_FOUND)
          throw new PluginException(PluginErrors.PLUGIN_NOT_FOUND, pluginADL);
        else
          throw new PluginException(PluginErrors.PLUGIN_INSTANTIATION, e,
              pluginADL);
      }
    }

    /* if it is a composite component */
    if (plugin instanceof Map) {
      // bind composite plugin to client interface.
      for (final Map.Entry<String, Object> binding : pluginBindings.entrySet()) {
        bindPlugin(binding.getKey(), plugin, binding.getValue());
      }
      // bind plugin to plugin factory.
      bindPlugin(FACTORY, plugin, pluginFactoryItf);
      return ((Map<?, ?>) plugin)
          .get(AbstractPluginManager.PLUGIN_INTERFACE_NAME);
    } else {
      /* else if it is a primitive component */
      // bind primitive plugin to client interface.
      final String[] clientItfs = JavaBindingUtil.listFc(plugin);
      for (final String itfName : clientItfs) {
        final Object pluginBinding = pluginBindings.get(itfName);
        if (pluginBinding == null) continue;
        try {
          JavaBindingUtil.bindFc(plugin, itfName, pluginBinding);
        } catch (final Exception e) {
          throw new PluginException(PluginErrors.PLUGIN_BINDING, e, itfName);
        }
      }

      return plugin;
    }

  }

  private void bindPlugin(final String itfName, final Object clientComponent,
      final Object server) throws PluginException {
    if (clientComponent instanceof Map) {
      final Map<?, ?> composite = (Map<?, ?>) clientComponent;
      try {
        JavaBindingUtil.bindComposite(composite, itfName, server);
      } catch (final IllegalBindingException e) {
        throw new PluginException(PluginErrors.PLUGIN_BINDING, e, itfName);
      }
    } else {
      try {
        JavaBindingUtil.bindFc(clientComponent, itfName, server);
      } catch (final Exception e) {
        throw new PluginException(PluginErrors.PLUGIN_BINDING, e, itfName);
      }
    }
  }
}
