/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.plugin;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.error.Error;
import org.objectweb.fractal.adl.error.ErrorTemplate;

/**
 * Exception thrown by plugin manager when a problem occurs while loading a
 * plugin.
 */
public class PluginException extends ADLException {

  /**
   * @see ADLException#ADLException(Error)
   */
  public PluginException(Error error) {
    super(error);
  }

  /** 
   * @see ADLException#ADLException(ErrorTemplate, Object...)
   */
  public PluginException(ErrorTemplate template, Object... args) {
    super(template, args);
  }

  /** 
   * @see ADLException#ADLException(ErrorTemplate, Throwable, Object...)
   */
  public PluginException(ErrorTemplate template, Throwable cause,
      Object... args) {
    super(template, cause, args);
  }

  /**
   * Constructs a new {@link PluginException}.
   * 
   * @param msg a detail message.
   */
  @Deprecated
  public PluginException(String msg) {
    super(msg);
  }

  /**
   * Constructs a new {@link PluginException}.
   * 
   * @param msg a detail message.
   * @param e the exception that caused this exception.
   */
  @Deprecated
  public PluginException(String msg, Exception e) {
    super(msg, null, e);
  }
}
