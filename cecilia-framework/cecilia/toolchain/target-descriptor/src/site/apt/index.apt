    -----
    Overview
    -----

Overview
~~~~~~~~

  <<<target-descriptor>>> module provides a very simple DSL (domain specific 
  language) to describe how to compile Cecilia application for a given target 
  platform. It allows to simplify the command line of 'ceciliac' so as the 
  configuration of the 'maven-ceciliaadl-plugin'.
  
  A target descriptor file is an XML-based file whose name ends with <<<.td>>>. 
  It must be placed along-side the cecilia sources and follows naming conventions
  similar to ADL or IDL files.

  It allows to specify the command of the C compiler/linker/archiver so as to
  specify various c-flags, ld-flags and linker-script. A target descriptor may 
  also define a mapping between the input ADL and the ADL that is actually 
  compile by the cecilia compiler. This is useful when Cecilia applications must
  be wrapped in a bootstrap component to be compiled correctly for a given 
  platform.
  
  A target descriptor may extends one or more other descriptors. This allows to
  reuse and extends existing descriptors. This is particularly useful when many
  different target platforms partially shares the same hardware. This can also 
  be usefull to defines different <compilation profiles> for a given target 
  platform. For instance: a debug profile with debug information in binary file
  and without optimization; and a production profile without debug info and with
  optimizations.  


Language description
~~~~~~~~~~~~~~~~~~~~

  A target-descriptor must start with the following XML prolog:

--------------------------------------------------------------------------------
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE target PUBLIC "-//objectweb.org//DTD Fractal ADL 2.0//EN" 
  "classpath://org/objectweb/fractal/cecilia/targetDescriptor/ast/targetDescriptor.dtd">
--------------------------------------------------------------------------------

  A target-descriptor has a very simple XML structure:

================================================================================

  <<target>>: top-level element.
  
  attributes:

    * <<<name>>>: the name of the target descriptor.
    
  sub-elements: extends*, adlMapping?, compiler?, linker?, archiver?, linkerScript?, cFlag*, ldFlag*

================================================================================

  <<extends>>: An extended target descriptor.
  
  attributes:

    * <<<name>>>: the name of the extended target descriptor.

    []

================================================================================

  <<adlMapping>>: Defines a mapping from the <input ADL> and the ADL that is 
    actually compiled.
  
  attributes:

    * <<<mapping>>>: the ADL mapping. Use <<<${inputADL}>>> to refer to the input ADL.

~~    * <<<outputName>>>: the mapping of the . Use <<<${inputADL}>>> to refer to the input ADL.

    []

================================================================================

  <<compiler>>: defines the C-compiler command.
  
  attributes:
  
    * <<<path>>>: the C-compiler command.

    []

================================================================================

  <<linker>>: defines the linker command.
  
  attributes:
  
    * <<<path>>>: the linker command.

    []

================================================================================

  <<archiver>>: defines the archiver command.

  attributes:
 
    * <<<path>>>: the archiver command.

    []

================================================================================

  <<linkerScript>>: defines the linker script.
  
  attributes:
  
    * <<<path>>>: the linker script.

    []

================================================================================

  <<cFlag>>: defines a set of c-flag.
  
  attributes:
  
    * <<<id>>>: give a name to this set of c-flag (optional).

    * <<<value>>>: the c-flags.

    []

================================================================================

  <<ldFlag>>: defines a set of ld-flag.
  
  attributes:
  
    * <<<id>>>: give a name to this set of ld-flag (optional).

    * <<<value>>>: the ld-flags.

    []


Extension Resolution
~~~~~~~~~~~~~~~~~~~~

  Attributes of <<<adlMapping>>>, <<<compiler>>>, <<<linker>>>, <<<archiver>>> 
  and <<<linkerScript>>> elements are inherited from extended descriptors and
  can be overridden.

  <<<cFlag>>> and <<<ldFlag>>> elements are treated differently: every elements 
  without <<<id>>> attribute are inherited and can't be overridden. Whereas 
  <<<cFlag>>> or <<<ldFlag>>> with an <<<id>>> attribute can be overridden by 
  defining a <<<cFlag>>> or a <<<ldFlag>>> with the same <<<id>>>.

Examples
~~~~~~~~

  See the {{{../../runtime/cecilia-baselib/index.html}cecilia-baselib}} component 
  library for concrete examples.

