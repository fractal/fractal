/**
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.targetDescriptor;

public class TargetDescriptorException extends Exception {

  public TargetDescriptorException() {
    super();
  }

  public TargetDescriptorException(String message, Throwable cause) {
    super(message, cause);
  }

  public TargetDescriptorException(String message) {
    super(message);
  }

  public TargetDescriptorException(Throwable cause) {
    super(cause);
  }

}
