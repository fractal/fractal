/**
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.targetDescriptor;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

import org.objectweb.fractal.adl.ParserException;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.adl.xml.XMLParser;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.cecilia.targetDescriptor.ast.Target;

public class BasicTargetDescriptorLoader implements TargetDescriptorLoader, BindingController {

  // ---------------------------------------------------------------------------
  // Client interfaces
  // ---------------------------------------------------------------------------

  /** The {@link XMLNodeFactory} client interface of this component. */
  public XMLNodeFactory nodeFactoryItf;

  // --------------------------------------------------------------------------
  // Implementation of the Loader interface
  // --------------------------------------------------------------------------

  public Target load(final String name, final Map<Object, Object> context)
      throws TargetDescriptorException {
    final XMLParser parser = new XMLParser(nodeFactoryItf);
    final String file = name.replace('.', '/') + ".td";
    ClassLoader cl = (ClassLoader) context.get("classloader");
    if (cl == null) cl = this.getClass().getClassLoader();
    final URL url = cl.getResource(file);
    if (url == null) {
      throw new TargetDescriptorException("Task descriptor \"" + file
          + "\" not found.");
    }
    try {
      final Target target;
      try {
        target = (Target) parser.parse(url.openStream(), file);
      } catch (final IOException e) {
        throw new TargetDescriptorException(e);
      }
      if (target.getName() == null) {
        throw new TargetDescriptorException(file + ": Target name missing");
      }
      if (!target.getName().equals(name)) {
        throw new TargetDescriptorException(file
            + ": Wrong definition name. Found \"" + target.getName()
            + "\" where \"" + name + "\" was expected.");
      }

      return target;
    } catch (final ParserException e) {
      final Throwable cause = e.getCause();
      throw new TargetDescriptorException("XML parse error.", cause);
    }
  }

  // --------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // --------------------------------------------------------------------------

  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {
    if (XMLNodeFactory.ITF_NAME.equals(s)) {
      this.nodeFactoryItf = (XMLNodeFactory) o;
    }
  }

  public String[] listFc() {
    return new String[]{XMLNodeFactory.ITF_NAME};
  }

  public Object lookupFc(final String s) throws NoSuchInterfaceException {
    if (XMLNodeFactory.ITF_NAME.equals(s)) {
      return this.nodeFactoryItf;
    }
    return null;
  }

  public void unbindFc(final String s) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException {
    if (XMLNodeFactory.ITF_NAME.equals(s)) {
      this.nodeFactoryItf = null;
    }
  }
}
