/**
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.targetDescriptor;

import java.util.Map;

import org.objectweb.fractal.cecilia.targetDescriptor.ast.Target;

public interface TargetDescriptorLoader {
  Target load(String name, Map<Object, Object> context)
      throws TargetDescriptorException;
}
