/**
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.targetDescriptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.merger.MergeException;
import org.objectweb.fractal.adl.merger.NodeMerger;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.cecilia.targetDescriptor.ast.Extends;
import org.objectweb.fractal.cecilia.targetDescriptor.ast.Target;

public class ExtensionLoader implements TargetDescriptorLoader, BindingController {

  protected static final Map<String, String> NAME_ATTRIBUTES = new HashMap<String, String>();
  {
    NAME_ATTRIBUTES.put("cFlag", "id");
    NAME_ATTRIBUTES.put("ldFlag", "id");
  }

  // ---------------------------------------------------------------------------
  // Client interfaces
  // ---------------------------------------------------------------------------

  /** Name of the {@link #clientLoaderItf} client interface. */
  public static final String                 LOADER_BINDING  = "client-loader";

  /** The {@link TargetDescriptorLoader} used by this loader. */
  public TargetDescriptorLoader                              clientLoaderItf;

  /** The {@link NodeMerger} used by this loader. */
  public NodeMerger                          nodeMergerItf;

  // ---------------------------------------------------------------------------
  // Implementation of the Loader interface
  // ---------------------------------------------------------------------------

  public Target load(final String name, final Map<Object, Object> context)
      throws TargetDescriptorException {
    return load(new LinkedHashSet<String>(), name, context);
  }

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  protected Target load(final Set<String> loaded, final String name,
      final Map<Object, Object> context) throws TargetDescriptorException {
    if (!loaded.add(name)) {
      throw new TargetDescriptorException(
          "Cycle in target descriptor inheritance: " + loaded);
    }

    Target target = clientLoaderItf.load(name, context);

    final Extends[] extendss = target.getExtendss();
    if (extendss.length == 0) {
      loaded.remove(name);
      return target;
    }

    final List<String> extensions = new ArrayList<String>(extendss.length);

    for (final Extends extend : extendss) {
      if (extend.getName() == null)
        throw new TargetDescriptorException(extend.astGetSource()
            + ": missing name of extended target descriptor.");

      extensions.add(extend.getName());
      target.removeExtends(extend);
    }

    for (final String extension : extensions) {
      final Target superTarget = load(loaded, extension, context);

      try {
        target = (Target) nodeMergerItf.merge(target, superTarget,
            NAME_ATTRIBUTES);
      } catch (final MergeException e) {
        throw new TargetDescriptorException("Merge error.", e);
      }
    }

    return target;
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  public String[] listFc() {
    return new String[]{LOADER_BINDING, NodeMerger.ITF_NAME};
  }

  public Object lookupFc(final String s) throws NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (LOADER_BINDING.equals(s)) {
      return clientLoaderItf;
    } else if (NodeMerger.ITF_NAME.equals(s)) {
      return nodeMergerItf;
    } else {
      throw new NoSuchInterfaceException("No client interface named '" + s
          + "'");
    }
  }

  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (LOADER_BINDING.equals(s)) {
      clientLoaderItf = (TargetDescriptorLoader) o;
    } else if (NodeMerger.ITF_NAME.equals(s)) {
      nodeMergerItf = (NodeMerger) o;
    } else {
      throw new NoSuchInterfaceException("No client interface named '" + s
          + "' for binding the interface");
    }
  }

  public void unbindFc(final String s) throws IllegalBindingException,
      NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (LOADER_BINDING.equals(s)) {
      clientLoaderItf = null;
    } else if (NodeMerger.ITF_NAME.equals(s)) {
      nodeMergerItf = null;
    } else {
      throw new NoSuchInterfaceException("No client interface named '" + s
          + "'");
    }
  }
}
