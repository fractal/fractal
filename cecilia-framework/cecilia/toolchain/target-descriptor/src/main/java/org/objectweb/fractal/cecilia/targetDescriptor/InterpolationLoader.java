/**
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.targetDescriptor;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.util.FractalADLLogManager;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.cecilia.targetDescriptor.ast.Target;

public class InterpolationLoader
    implements
      TargetDescriptorLoader,
      BindingController {

  private static Logger         logger         = FractalADLLogManager
                                                   .getLogger("targetDesc");

  // ---------------------------------------------------------------------------
  // Client interfaces
  // ---------------------------------------------------------------------------

  /** Name of the {@link #clientLoaderItf} client interface. */
  public static final String    LOADER_BINDING = "client-loader";

  /** The {@link TargetDescriptorLoader} used by this loader. */
  public TargetDescriptorLoader clientLoaderItf;

  // ---------------------------------------------------------------------------
  // Implementation of the Loader interface
  // ---------------------------------------------------------------------------

  public Target load(final String name, final Map<Object, Object> context)
      throws TargetDescriptorException {
    final Target target = clientLoaderItf.load(name, context);
    interpolate(target);
    return target;
  }

  protected void interpolate(final Node node) {
    final Map<String, String> attributes = node.astGetAttributes();
    for (final Map.Entry<String, String> attribute : attributes.entrySet()) {
      if (attribute.getValue() != null)
        attribute.setValue(interpolate(node, attribute.getValue()));
    }
    node.astSetAttributes(attributes);

    for (final String subNodeType : node.astGetNodeTypes()) {
      for (final Node subNode : node.astGetNodes(subNodeType)) {
        if (subNode != null) interpolate(subNode);
      }
    }
  }

  protected String interpolate(final Node node, final String s) {
    final int i = s.indexOf("${");
    if (i == -1) return s;

    final int j = s.indexOf("}", i);
    if (j == -1) {
      if (logger.isLoggable(Level.WARNING))
        logger.log(Level.WARNING, "At " + node.astGetSource()
            + ": Invalid string \"" + s + "\"");
      return s;
    }
    final String varName = s.substring(i + 2, j);
    if (varName.equals("inputADL")) {
      // Do not substitute ${inputADL} in ADLMapping
      return s;
    }
    String value = System.getProperty(varName);
    if (value == null) value = System.getenv(varName);

    if (value == null) {
      if (logger.isLoggable(Level.FINE))
        logger.log(Level.FINE, "At " + node.astGetSource()
            + ": Unknown variable \"" + varName + "\"");
      value = "";
    } else if (logger.isLoggable(Level.FINEST)) {
      logger.log(Level.FINEST, "At " + node.astGetSource() + ": replace \""
          + varName + "\" by \"" + value + "\".");
    }

    return interpolate(node, s.substring(0, i) + value + s.substring(j + 1));
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  public String[] listFc() {
    return new String[]{LOADER_BINDING};
  }

  public Object lookupFc(final String s) throws NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (LOADER_BINDING.equals(s)) {
      return clientLoaderItf;
    } else {
      throw new NoSuchInterfaceException("No client interface named '" + s
          + "'");
    }
  }

  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (LOADER_BINDING.equals(s)) {
      clientLoaderItf = (TargetDescriptorLoader) o;
    } else {
      throw new NoSuchInterfaceException("No client interface named '" + s
          + "' for binding the interface");
    }
  }

  public void unbindFc(final String s) throws IllegalBindingException,
      NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (LOADER_BINDING.equals(s)) {
      clientLoaderItf = null;
    } else {
      throw new NoSuchInterfaceException("No client interface named '" + s
          + "'");
    }
  }
}
