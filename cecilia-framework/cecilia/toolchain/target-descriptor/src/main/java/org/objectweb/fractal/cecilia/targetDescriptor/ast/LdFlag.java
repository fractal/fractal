/**
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.targetDescriptor.ast;

import org.objectweb.fractal.adl.Node;

public interface LdFlag extends Node {
  String getId();
  void setId(String id);

  String getValue();
  void setValue(String value);
}
