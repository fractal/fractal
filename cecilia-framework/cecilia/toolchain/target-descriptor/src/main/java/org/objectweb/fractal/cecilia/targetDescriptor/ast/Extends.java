/**
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.targetDescriptor.ast;

import org.objectweb.fractal.adl.Node;

public interface Extends extends Node {

  String getName();
  void setName(String name);

}
