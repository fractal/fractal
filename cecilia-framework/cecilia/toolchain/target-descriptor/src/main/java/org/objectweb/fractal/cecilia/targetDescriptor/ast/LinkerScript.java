/**
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.targetDescriptor.ast;

import org.objectweb.fractal.adl.Node;

public interface LinkerScript extends Node {

  String getPath();

  void setPath(String path);
}
