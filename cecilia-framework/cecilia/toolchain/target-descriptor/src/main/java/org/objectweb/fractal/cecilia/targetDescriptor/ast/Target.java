/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.targetDescriptor.ast;

import org.objectweb.fractal.adl.Node;

public interface Target extends Node {

  String getName();

  void setName(String name);

  void addExtends(Extends extend);

  void removeExtends(Extends extend);

  Extends[] getExtendss();

  ADLMapping getAdlMapping();

  void setAdlMapping(ADLMapping mapping);

  Compiler getCompiler();

  void setCompiler(Compiler compiler);

  Linker getLinker();

  void setLinker(Linker linker);

  LinkerScript getLinkerScript();

  void setLinkerScript(LinkerScript linkerScript);

  Archiver getArchiver();

  void setArchiver(Archiver archiver);

  void addCFlag(CFlag flag);

  void removeCFlag(CFlag flag);

  CFlag[] getCFlags();

  void addLdFlag(LdFlag flag);

  void removeLdFlag(LdFlag flag);

  LdFlag[] getLdFlags();
}
