/**
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.targetDescriptor.ast;

import org.objectweb.fractal.adl.Node;

public interface Linker extends Node {

  String getPath();

  void setPath(String path);
}
