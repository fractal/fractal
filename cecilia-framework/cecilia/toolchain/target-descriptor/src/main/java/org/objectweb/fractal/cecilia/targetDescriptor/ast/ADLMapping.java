/**
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.targetDescriptor.ast;

import org.objectweb.fractal.adl.Node;

public interface ADLMapping extends Node {
  String getMapping();

  void setMapping(String mapping);

  String getOutputName();

  void setOutputName(String outputName);
}
