/**
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.targetDescriptor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;
import org.objectweb.fractal.cecilia.targetDescriptor.ast.CFlag;
import org.objectweb.fractal.cecilia.targetDescriptor.ast.Compiler;
import org.objectweb.fractal.cecilia.targetDescriptor.ast.Linker;
import org.objectweb.fractal.cecilia.targetDescriptor.ast.Target;

public class TestTargetDescriptorLoader {

  protected TargetDescriptorLoader              loader;
  protected Map<Object, Object> context;

  @Before
  public void setUp() {
    final BasicTargetDescriptorLoader tdl = new BasicTargetDescriptorLoader();
    tdl.nodeFactoryItf = new XMLNodeFactoryImpl();
    loader = tdl;

    context = new HashMap<Object, Object>();
    context.put("classloader", this.getClass().getClassLoader());
  }

  @Test
  public void test1() throws Exception {

    final Target target = loader.load("descriptor1", context);

    assertNotNull(loader);

    assertEquals("descriptor1", target.getName());

    final Compiler compiler = target.getCompiler();
    assertNotNull(compiler);
    assertEquals("gcc", compiler.getPath());

    final Linker linker = target.getLinker();
    assertNotNull(linker);
    assertEquals("gcc", linker.getPath());

    final CFlag[] cFlags = target.getCFlags();
    assertEquals(2, cFlags.length);

    assertNull(cFlags[0].getId());
    assertEquals("-g", cFlags[0].getValue());

    assertEquals("optimization", cFlags[1].getId());
    assertEquals("-O3", cFlags[1].getValue());
  }
}
