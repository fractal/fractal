/**
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.targetDescriptor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;
import org.objectweb.fractal.cecilia.targetDescriptor.ast.CFlag;
import org.objectweb.fractal.cecilia.targetDescriptor.ast.Compiler;
import org.objectweb.fractal.cecilia.targetDescriptor.ast.Linker;
import org.objectweb.fractal.cecilia.targetDescriptor.ast.Target;

public class TestInterpolationLoader {

  protected TargetDescriptorLoader loader;
  protected Map<Object, Object>    context;

  @Before
  public void setUp() {
    final BasicTargetDescriptorLoader tdl = new BasicTargetDescriptorLoader();
    final InterpolationLoader il = new InterpolationLoader();
    il.clientLoaderItf = tdl;
    tdl.nodeFactoryItf = new XMLNodeFactoryImpl();
    loader = il;

    context = new HashMap<Object, Object>();
    context.put("classloader", this.getClass().getClassLoader());
  }

  @Test
  public void test1() throws Exception {

    final String prevValue = System.setProperty("TOOL_DIR", "foo");
    final Target target = loader.load("descriptor3", context);
    if (prevValue != null) System.setProperty("TOOL_DIR", prevValue);

    assertNotNull(loader);

    assertEquals("descriptor3", target.getName());

    final Compiler compiler = target.getCompiler();
    assertNotNull(compiler);
    assertEquals("foo/gcc", compiler.getPath());

    final Linker linker = target.getLinker();
    assertNotNull(linker);
    assertEquals("foo/gcc", linker.getPath());

    final CFlag[] cFlags = target.getCFlags();
    assertEquals(1, cFlags.length);

    assertNull(cFlags[0].getId());
    assertEquals("-Ifoo/lib", cFlags[0].getValue());
  }
}
