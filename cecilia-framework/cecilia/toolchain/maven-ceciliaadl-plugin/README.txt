############################################################################
#            OVERVIEW
############################################################################

This Maven plugin is meant to call the Cecilia ADL Launcher, providing support to pass
an *ordered* list of arguments either in the form of (name,value) pairs or as path to Properties file.

This plugin is normally used together with the maven-cecilia-plugin, which is able
to drive the retrieval and unpacking of the Cecilia libraries, by default under 
the directory "target/cecilia-dependencies".



###############################################################
#            USAGE EXAMPLE
###############################################################

<!-- 
    This is the pom.xml of your Cecilia application 
-->
<project>
    
    <groupId>your_project_groupid</groupId>
    <artifactId>your_project_name</artifactId>
	<!-- 
	  this packaging is to indicate the nature of the project and will be
	  handled by the "maven-cecilia-plugin" in the <build> section.  
	-->
    <packaging>cecilia-application</packaging>
  
  
  <properties>
    <cecilia.version>PUT_HERE_THE_LATEST_CECILIA_STABLE_VERSION</cecilia.version>
    <cecilia.src>${project.build.directory}/cecilia-dependencies</cecilia.src>
  </properties>
  
   <!-- -->
  
  <dependencies>
    <dependency>
    	<groupId>org.objectweb.fractal.cecilia</groupId>
		<artifactId>cecilia-baselib</artifactId>
		<!-- value of the cecilia.version property defined before -->
		<version>${cecilia.version}</version>
		<!-- Cecilia libraries are package as .car files -->
		<type>car</type>
    </dependency>
  
    <!-- other Cecilia libraries you need in your application -->
  </dependencies>
  
  <build>
		<plugins>
		
			<!-- =================================================== -->
			<!-- maven-cecilia-plugin  -->
			<plugin>
				<groupId>org.objectweb.fractal.cecilia</groupId>
                <artifactId>maven-cecilia-plugin</artifactId>
                <!-- value of the cecilia.version property defined before -->
				<version>${cecilia.version}</version>
				<extensions>true</extensions>
			</plugin>

			<!-- =================================================== -->
			<!-- maven-ceciliaadl-plugin to invoke the Cecilia ADL Launcher -->
			<plugin>
				<groupId>
					org.objectweb.fractal.cecilia.toolchain
				</groupId>
				<artifactId>maven-ceciliaadl-plugin</artifactId>
				<version>${cecilia.version}</version>
				<configuration>
					<adl>
						unix.boot.BootstrappedApplication(helloworld.Helloworld)
					</adl>
					<arguments>
						<parameter>
							<name>src-path</name>
							<!-- the maven-cecilia-plugin will make unpack here the Cecilia libraries -->
							<value>${project.build.directory}/cecilia-dependencies</value>
						</parameter>
						<parameter>
							<name>exec-name</name>
							<value>helloworld</value>
						</parameter>
						<properties>
							<file>flags.properties</file>
						</properties>
					</arguments>
				</configuration>
			</plugin>
			<!-- =================================================== -->

		</plugins>
	</build>
  <!-- ... -->
</project>


In the <configuration> section of the MOJO you can configure the
following things:
 1) the <adl> you want to compile by means of the Cecilia ADL Launcher
 2) a ORDERED list of <arguments> to pass to it, which can be in 2 forms:
   ** as simple (name,value) pairs (see the <parameter> tag)
   ** as path to properties file (see the <properties> tag)


  The Cecilia ADL Launcher will then be invoked with the following command
  line:
  
  Launcher adl -name_1=value_1 -name_2=value_2 ... -name_n=value_n  
  
  
  
###############################################################
#        USAGE EXAMPLE FOR COMPILING MULTIPLE ADLS
###############################################################
This plugin can also be used for compiling multiple adl files at a glance.
In this use-case, the <adl> tag should be replaced by <adls> as follows:

<!-- 
    This is the pom.xml of your Cecilia application 
-->
<project>
    
    <groupId>your_project_groupid</groupId>
    <artifactId>your_project_name</artifactId>
	<!-- 
	  this packaging is to indicate the nature of the project and will be
	  handled by the "maven-cecilia-plugin" in the <build> section.  
	-->
    <packaging>cecilia-application</packaging>
  
  
  <properties>
    <cecilia.version>PUT_HERE_THE_LATEST_CECILIA_STABLE_VERSION</cecilia.version>
    <cecilia.src>${project.build.directory}/cecilia-dependencies</cecilia.src>
  </properties>
  
   <!-- -->
  
  <dependencies>
    <dependency>
    	<groupId>org.objectweb.fractal.cecilia</groupId>
		<artifactId>cecilia-baselib</artifactId>
		<!-- value of the cecilia.version property defined before -->
		<version>${cecilia.version}</version>
		<!-- Cecilia libraries are package as .car files -->
		<type>car</type>
    </dependency>
  
    <!-- other Cecilia libraries you need in your application -->
  </dependencies>
  
  <build>
		<plugins>
		
			<!-- =================================================== -->
			<!-- maven-cecilia-plugin  -->
			<plugin>
				<groupId>org.objectweb.fractal.cecilia</groupId>
                <artifactId>maven-cecilia-plugin</artifactId>
                <!-- value of the cecilia.version property defined before -->
				<version>${cecilia.version}</version>
				<extensions>true</extensions>
			</plugin>

			<!-- =================================================== -->
			<!-- maven-ceciliaadl-plugin to invoke the Cecilia ADL Launcher -->
			<plugin>
				<groupId>
					org.objectweb.fractal.cecilia.toolchain
				</groupId>
				<artifactId>maven-ceciliaadl-plugin</artifactId>
				<version>${cecilia.version}</version>
				<configuration>
				<adls>
					 <adl>
						 <definition>unix.boot.BootstrappedApplication(helloworld.Helloworld)</definition>
						 <execname>standard-helloworld</execname>
					 </adl>				
				   <adl>
						 <definition>unix.boot.BootstrappedApplication(helloworld.HelloworldWithCounter)</definition>
					   <execname>counter-helloworld</execname>
					 </adl>
         </adls>
					<arguments>
						<parameter>
							<name>src-path</name>
							<!-- the maven-cecilia-plugin will make unpack here the Cecilia libraries -->
							<value>${project.build.directory}/cecilia-dependencies</value>
						</parameter>
						<properties>
							<file>flags.properties</file>
						</properties>
					</arguments>
				</configuration>
			</plugin>
			<!-- =================================================== -->

		</plugins>
	</build>
  <!-- ... -->
</project>
