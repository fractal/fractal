/**
 Copyright 2007 INRIA
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Contact: fractal@objectweb.org
 Author:  Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.maven.plugin;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.cecilia.adl.Launcher;

/**
 * A simple mojo which allows invoking the <strong>Cecilia ADL Toolchain</strong>
 * passing some configuration. The invocation will happen in the same VM which
 * runs the MOJO.
 * 
 * @goal compile
 * @author Alessio Pace
 */
public class CeciliaAdlMojo extends AbstractMojo {

  /**
   * The Maven project reference.
   * 
   * @parameter expression="${project}"
   * @required
   */
  private MavenProject project;

  /**
   * The fully qualified name of the component ADL definition to compile.
   * 
   * @parameter
   */
  private String       adl;

  /**
   * The list of fully qualified name of the component ADL definitions to
   * compile.
   * 
   * @parameter
   */
  private List<?>      adls;

  /**
   * The output directory for the files produced by Cecilia ADL.
   * 
   * @parameter expression="${project.build.directory}/build"
   */
  private String       outDir;

  /**
   * The name of the target descriptor to use.
   * 
   * @parameter
   */
  private String       target;

  /**
   * @parameter
   */
  private List<?>      arguments;

  /**
   *
   */
  protected List<String> allOrderedArguments = null;

  protected void addArgIfNotPresent(List<String> args, String argName,
      String argValue) {
    argName = CeciliaAdlLauncherArguments.ARGUMENT_PREFIX + argName;
    for (String arg : args) {
      if (arg.startsWith(argName)) return;
    }
    args.add(argName
        + CeciliaAdlLauncherArguments.ARGUMENT_NAME_VALUE_SEPARATOR + argValue);
  }

  protected void addArg(List<String> args, String argName, String argValue) {
    args.add(CeciliaAdlLauncherArguments.ARGUMENT_PREFIX + argName
        + CeciliaAdlLauncherArguments.ARGUMENT_NAME_VALUE_SEPARATOR + argValue);
  }

  public void execute() throws MojoExecutionException, MojoFailureException {
    if (allOrderedArguments == null) {
      allOrderedArguments = new ArrayList<String>();
    }
    
    if (adl != null) {
      if (adls != null)
        throw new RuntimeException(
            "<adl> and <adls> tags cannot be used in the same time.");
      getLog().debug("Compiling architecture: " + adl);
      allOrderedArguments.add(adl);
    } else {
      if (adls == null)
        throw new RuntimeException(
            "At least one <adl> or <adls> tag must be specified.");
      getLog().debug("Compiling architecture: " + adls);
      /* the first argument is the ADL to be compiled */
      for (Object adlName : adls) {
        if (!(adlName instanceof Adl)) {
          throw new MojoExecutionException(
              "Invalid element in \"adls\" list. Must have the following form:\n"
                  + "  <adls>\n" + "    <adl>\n"
                  + "      <definition>ADL name</definition>\n"
                  + "      <execname>exec file name</execname>\n"
                  + "    </adl>\n" + "    ...\n" + "  </adls>");
        }
        allOrderedArguments.add(adlName.toString());
      }
    }

    getLog().debug("Properties " + arguments);

    if (allOrderedArguments.contains("--check-adl")) {
      // add by default the project sources directory in the toolchain
      // src-path (this way, the project .c and .idl will be found.
      addArg(allOrderedArguments, "src-path", project.getBasedir()
          .getPath() + "/src/main/cecilia");
    }
    else {
      // add by default the project build output directory in the toolchain
      // src-path (this way, the project .c and .idl will be found.
      addArg(allOrderedArguments, "src-path", project.getBuild()
          .getOutputDirectory());
    }

    File dep = new File(project.getBasedir(), "target/cecilia-dependencies");
    if (dep.isDirectory()) {
      addArg(allOrderedArguments, "src-path", dep.getPath());
    }

    /* creates the output dir if necessary */
    Util.createOutDirIfNecessary(outDir);

    /* append the other parameters */
    if (arguments != null) {
      for (Object argument : arguments) {
        if (!(argument instanceof CeciliaAdlLauncherArguments)) {
          throw new MojoExecutionException(
              "Invalid element in \"arguments\" list. Must have one of the following form:\n"
                  + "  <parameter>\n"
                  + "      <name>param name</name>\n"
                  + "      <value>param value</value>\n"
                  + "  </parameter>\n"
                  + "or\n"
                  + "  <properties>\n"
                  + "      <file>property file name</file>\n"
                  + "  </properties>\n"
                  + "or\n"
                  + "  <flag>\n"
                  + "      <name>flag name</name>\n"
                  + "  </flag>\n"
                  + "or\n"
                  + "  <systemProperty>\n"
                  + "      <name>system property name</name>\n"
                  + "      <value>system property value</value>\n"
                  + "  </systemProperty>");
        }
        List<String> allArguments = ((CeciliaAdlLauncherArguments) argument)
            .getArguments(project);
        if (allArguments != null) {
          allOrderedArguments.addAll(allArguments);
        }
      }
    }

    // add the "out-path" parameter
    addArg(allOrderedArguments, "out-path", outDir);

    // add the "target-descriptor" parameter (if any)
    if (target != null)
      addArg(allOrderedArguments, "target-descriptor", target);

    // Add default exec-name argument, if none has been specified.
    addArgIfNotPresent(allOrderedArguments, "executable-name", project
        .getArtifactId());

    final String[] args = allOrderedArguments.toArray(new String[0]);
    getLog().debug(
        "Invoking Cecilia ADL Launcher with the following command line parameters: "
            + Arrays.deepToString(args));

    try {
      new MojoLauncher(args);
    } catch (Exception e) {
      if (e instanceof MojoFailureException)
        throw (MojoFailureException) e;
      else if (e instanceof MojoExecutionException)
        throw (MojoExecutionException) e;
      else
        throw new MojoExecutionException("Unexpected error", e);
    }
  }

  private class MojoLauncher extends Launcher {

    /** @see Launcher#Launcher(String...) */
    public MojoLauncher(String... args) throws Exception {
      super(args);
    }

    @Override
    protected void handleException(ADLException e) throws Exception {
      MojoFailureException mojoFailureException = new MojoFailureException(e
          .getError().toString());
      mojoFailureException.initCause(e);
      throw mojoFailureException;
    }

    @Override
    protected void handleException(CompilerInstantiationException e)
        throws Exception {
      throw new MojoExecutionException(
          "An error occurs while instantiating CeciliaADL compiler", e);
    }

    @Override
    protected void handleException(InvalidCommandLineException e)
        throws Exception {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      PrintStream ps = new PrintStream(baos);
      printHelp(ps);
      ps.flush();
      MojoFailureException mojoFailureException = new MojoFailureException(
          null, e.getMessage(), baos.toString());
      mojoFailureException.initCause(e);
      throw mojoFailureException;
    }

    @Override
    protected void printHelp(PrintStream ps) {
      ps.println("Configuration must have the following form:");
      ps.println();
      ps.println("  <configuration>");
      ps.println("    <adl>");
      ps.println("      ADL TO COMPILE");
      ps.println("    </adl>");
      ps.println("    <arguments>");
      ps.println("      <parameter>");
      ps.println("        <name>PARAM NAME</name>");
      ps.println("        <value>PARAM VALUE</value>");
      ps.println("      </parameter>");
      ps.println("      ...");
      ps.println("      <properties>");
      ps.println("        <file>PROPERTIES FILE NAME</file>");
      ps.println("      </properties>");
      ps.println("      ...");
      ps.println("      <flag>");
      ps.println("        <name>FLAG NAME</file>");
      ps.println("      </flag>");
      ps.println("      ...");
      ps.println("      <systemProperty>");
      ps.println("          <name>system property name</name>");
      ps.println("          <value>system property value</value>");
      ps.println("      </systemProperty>");
      ps.println("      ...");
      ps.println("    </arguments>");
      ps.println("  </configuration>");
      ps.println();
      ps.println();
      ps.println("Where available arguments are:");

      int maxCol = 0;
      for (final CmdOption opt : options.getOptions()) {
        if (opt == printStackTraceOpt || opt == outDirOpt
            || opt == targetDescOpt) continue;
        String name = opt.getLongName();
        if (name == null) name = opt.getShortName();
        int col = 2 + name.length();
        if (col > maxCol) maxCol = col;
      }

      for (final CmdOption opt : options.getOptions()) {
        if (opt == printStackTraceOpt || opt == outDirOpt
            || opt == targetDescOpt) continue;
        final StringBuffer sb = new StringBuffer("  ");
        String name = opt.getLongName();
        if (name == null) name = opt.getShortName();
        sb.append(name);
        while (sb.length() < maxCol)
          sb.append(' ');
        sb.append(": ").append(opt.getDescription());
        ps.println(sb);
      }
    }
  }
}
