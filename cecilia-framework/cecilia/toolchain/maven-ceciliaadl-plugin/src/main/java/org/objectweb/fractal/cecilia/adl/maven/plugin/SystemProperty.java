/**
 Copyright 2009 INRIA
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Contact: fractal@objectweb.org
 Author:  Lionel Debroux
 */

package org.objectweb.fractal.cecilia.adl.maven.plugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.maven.project.MavenProject;

/**
 * @author Lionel Debroux
 */
public class SystemProperty implements CeciliaAdlLauncherArguments {

  private String name;
  private String value;

  public List<String> getArguments(MavenProject project) {
    System.out.println("Setting system property \"" + name + "\" to \"" + value + "\"");
    System.setProperty(name, value);
    return null;
  }

  public String toString() {
    if (value == null)
      return DEFINE_ARGUMENT_PREFIX + name + ARGUMENT_NAME_VALUE_SEPARATOR;
    else
      return DEFINE_ARGUMENT_PREFIX + name + ARGUMENT_NAME_VALUE_SEPARATOR + value;
  }

}
