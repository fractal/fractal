/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.control;

import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.LifeCycleController;

/**
 * A control interface used to export client collection interface through
 * composite membrane.
 */
public interface ExportedInterfaceController {

  /**
   * Exports a client collection interface through a composite membrane. More
   * precisely, this interface, provided by a composite components, allows to
   * bind a client collection interface of a sub component to the internal
   * interface named <code>clientItfName</code>. Subsequently, every bindings
   * made on interface <code>clientItfName</code> will be propagated to the
   * <code>innerItf</code>.
   * 
   * @param clientItfName the name of the client interface that exports the
   *            <code>innerItf</code> interface.
   * @param innerItf a client collection interface that belongs to a sub
   *            component.
   * @throws NoSuchInterfaceException if there is no interface whose name is
   *             <code>clientItfName</code>.
   * @throws IllegalBindingException if the binding cannot be created.
   * @throws IllegalLifeCycleException if this component has a {@link
   *             LifeCycleController} interface, but it is not in an appropriate
   *             state to perform this operation.
   */
  void exportFcInterface(String clientItfName, Interface innerItf)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException;

  void unexportFcInterface(String clientItfName)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException;
}
