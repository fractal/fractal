/***
 * Fractal ADL Task Framework Parser
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Ali Erdem Ozcan
 */

package org.objectweb.fractal.task.core.composition;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.composition.ast.Export;
import org.objectweb.fractal.task.core.composition.ast.FunctionCall;
import org.objectweb.fractal.task.core.composition.ast.Import;
import org.objectweb.fractal.task.core.composition.ast.ImportContainer;
import org.objectweb.fractal.task.core.composition.ast.Instruction;
import org.objectweb.fractal.task.core.composition.ast.RecordField;
import org.objectweb.fractal.task.core.composition.ast.Task;
import org.objectweb.fractal.task.core.composition.ast.TaskDefinition;
import org.objectweb.fractal.task.core.composition.ast.ValueContainer;

public class FunctionLoader extends AbstractTaskCompositionLoader
    implements
      FunctionLoaderAttrbutes {

  public static final String DEFAULT_FUNCTION_PACKAGE_NAME = "org.objectweb.fractal.task.core.composition.function";
  private String             defaultFunctionsPackageName   = DEFAULT_FUNCTION_PACKAGE_NAME;

  // ---------------------------------------------------------------------------
  // Implementation of the TaskCompositionLoader interface
  // ---------------------------------------------------------------------------

  public TaskDefinition load(final String name,
      final Map<Object, Object> context) throws TaskException {
    final TaskDefinition d = clientLoader.load(name, context);
    loadFunctions(d, context);
    return d;
  }

  private void loadFunctions(final TaskDefinition d,
      final Map<Object, Object> context) throws TaskException {
    // Create a map of imported functions
    final Import[] imports = ((ImportContainer) d).getImports();
    final Map<String, Function> importedFunctions = initializeImportedFunctions(
        context, imports);
    // Map function provider objects to function calls that are found in
    // predicates and record definitions
    final Task task = d.getTask();
    // Exports
    for (final Instruction instruction : task.getInstructions()) {
      final Export export = instruction.getExport();
      if (export != null) {
        // export internal record predicate
        mapFunctions((ValueContainer) export.getInternalRecord(),
            importedFunctions, context);

        if (export.getExternalRecord() != null)
          for (final RecordField recordField : export.getExternalRecord()
              .getRecordFields()) {
            mapFunctions(recordField, importedFunctions, context);
          }
      } else {
        // Binding predicates
        mapFunctions((ValueContainer) instruction.getBinding(),
            importedFunctions, context);
      }
    }
  }

  private void mapFunctions(final ValueContainer container,
      final Map<String, Function> importedFunctions,
      final Map<Object, Object> context) throws TaskException {

    final FunctionCall functionCall = container.getFunction();
    if (functionCall == null) return;

    functionCall.astSetDecoration("function", getFunction(functionCall,
        importedFunctions, context));

    for (final ValueContainer functionParameter : functionCall
        .getFunctionParameters()) {
      mapFunctions(functionParameter, importedFunctions, context);
    }
  }

  private Function getFunction(final FunctionCall functionCall,
      final Map<String, Function> importedFunctions,
      final Map<Object, Object> context) throws TaskException {
    // Check if the function is already imported
    final String functionName = functionCall.getName();
    Function functionObject = importedFunctions.get(functionName);
    if (functionObject == null) {
      // If it is not found in the imported functions set, try to find it in the
      // default functions package
      try {
        // Set the first character of the function name to upper case in order
        // to obtain the conventional provider class name.
        final String firstChar = functionCall.getName().substring(0, 1)
            .toUpperCase();
        final String className = defaultFunctionsPackageName + '.' + firstChar
            + functionName.substring(1);

        functionObject = getFunctionProviderObject(functionCall, className,
            context);
        importedFunctions.put(functionCall.getName(), functionObject);
      } catch (final ClassNotFoundException e) {
        // If it is not found in the default packages map, then throw an
        // exception, since there is no way to find a provider class for this
        // function.
        throw new TaskException("Unknown function '" + functionName + "'.",
            functionCall, e);
      }
    }
    return functionObject;
  }

  private Map<String, Function> initializeImportedFunctions(
      final Map<Object, Object> context, final Import[] imports)
      throws TaskException {
    final Map<String, Function> importedFunctions = new HashMap<String, Function>();
    for (final Import importDeclaration : imports) {
      try {
        final Function function = getFunctionProviderObject(importDeclaration,
            importDeclaration.getClassName(), context);
        if (importedFunctions
            .put(importDeclaration.getFunctionName(), function) != null) {
          throw new TaskException("The function '"
              + importDeclaration.getFunctionName()
              + "' is imported multiple times", importDeclaration);
        }
      } catch (final ClassNotFoundException e) {
        throw new TaskException("Imported class '"
            + importDeclaration.getClassName()
            + "' is not found in the classpath.", importDeclaration, e);
      }
    }
    return importedFunctions;
  }

  private Function getFunctionProviderObject(final Node node,
      final String className, final Map<Object, Object> context)
      throws TaskException, ClassNotFoundException {
    try {
      return (Function) getLoader(context).loadClass(className).newInstance();
    } catch (final InstantiationException e) {
      throw new TaskException(
          "Cannot create an instance of the imported class", node, e);
    } catch (final IllegalAccessException e) {
      throw new TaskException("Cannot access the imported class", node, e);
    }
  }

  protected ClassLoader getLoader(final Map<Object, Object> context) {
    ClassLoader loader = null;
    if (context != null) {
      loader = (ClassLoader) context.get("classloader");
    }
    if (loader == null) {
      loader = getClass().getClassLoader();
    }
    return loader;
  }

  public String getDefaultFunctionsPackageName() {
    return this.defaultFunctionsPackageName;
  }

  public void setDefaultFunctionsPackageName(final String arg) {
    this.defaultFunctionsPackageName = arg;
  }

}
