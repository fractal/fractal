/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.control;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.type.InterfaceType;

/**
 * @author leclercm
 */
public abstract class ExportedInterfaceBindingMixin {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private ExportedInterfaceBindingMixin() {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overridden by the mixin class
  // -------------------------------------------------------------------------

  public void bindFc(final InterfaceType clientItfType,
      final String clientItfName, final Object serverItf)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {
    if (clientItfType.isFcCollectionItf()
        && clientItfName.equals(clientItfType.getFcItfName())) {
      // client interface is a collection interface and clientItfName is equals
      // to the interface name (i.e. it has no suffix).

      if ((serverItf instanceof Interface)) {
        final Interface sItf = (Interface) serverItf;
        if (sItf.isFcInternalItf()) {
          // server interface is an internal interface.

          final InterfaceType serverType = (InterfaceType) sItf.getFcItfType();
          if (serverType.isFcCollectionItf()) {
            // server interface is a collection interface.
            // => this binding exports a client collection interface through a
            // composite membrane.
            ExportedInterfaceController eic;
            try {
              eic = (ExportedInterfaceController) sItf.getFcItfOwner()
                  .getFcInterface("/exported-itf-controller");
            } catch (final NoSuchInterfaceException e) {
              eic = null;
            }

            if (eic != null) {
              eic.exportFcInterface(sItf.getFcItfName(),
                  (Interface) _this_weaveableC.getFcInterface(clientItfName));
              return;
            }
          }
        }
      }
    }
    _super_bindFc(clientItfType, clientItfName, serverItf);
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableC</tt> field required by this mixin. This field is
   * supposed to reference the {@link Component} interface of the component to
   * which this controller object belongs.
   */
  public Component _this_weaveableC;

  public abstract void _super_bindFc(InterfaceType clientItfType,
      String clientItfName, Object serverItf) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException;

}
