/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.composition;

/**
 * Function interface that must be implemented by classes that are imported in
 * composition files. The interface defines a single
 * {@link #invoke(Object...) invoke} interface.
 */
public interface Function {

  /**
   * Invocation method.
   * 
   * @param params the parameter.
   * @return the result of the function.
   * @throws FunctionExecutionException thrown when an error occurs.
   */
  Object invoke(Object... params) throws FunctionExecutionException;
}
