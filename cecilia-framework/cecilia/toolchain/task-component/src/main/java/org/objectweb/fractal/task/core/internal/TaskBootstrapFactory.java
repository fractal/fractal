/**
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.internal;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.asm.Opcodes;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.Julia;
import org.objectweb.fractal.julia.asm.ClassVersionHelper;
import org.objectweb.fractal.julia.factory.BasicGenericFactoryMixin;
import org.objectweb.fractal.julia.factory.ChainedInstantiationException;
import org.objectweb.fractal.julia.loader.Loader;
import org.objectweb.fractal.julia.type.BasicTypeFactoryMixin;

/**
 * Provides access to the Task bootstrap component.
 */
public class TaskBootstrapFactory implements Factory, GenericFactory {

  private static final String JULIA_TASK_CONFIG_FILE = "etc/julia-task.cfg";

  /**
   * The bootstrap component.
   */
  private static Component    bootstrapComponent;

  // ---------------------------------------------------------------------------
  // Implementation of the Factory interface
  // ---------------------------------------------------------------------------

  /**
   * @return <tt>null</tt>.
   */
  public Type getFcInstanceType() {
    return null;
  }

  /**
   * @return <tt>null</tt>.
   */
  public Object getFcControllerDesc() {
    return null;
  }

  /**
   * @return <tt>null</tt>.
   */
  public Object getFcContentDesc() {
    return null;
  }

  public Component newFcInstance() throws InstantiationException {
    return newFcInstance(new HashMap<Object, Object>());
  }

  @SuppressWarnings("unchecked")
  public Component newFcInstance(final Type type, final Object controllerDesc,
      final Object contentDesc) throws InstantiationException {
    Map context;
    if (contentDesc instanceof Map) {
      context = (Map) contentDesc;
    } else {
      context = new HashMap<Object, Object>();
    }
    return newFcInstance(context);
  }

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  @SuppressWarnings("unchecked")
  private Component newFcInstance(final Map context)
      throws InstantiationException {
    if (bootstrapComponent == null) {
      String juliaCfg = (String) context.get("julia.config");
      if (juliaCfg == null) {
        juliaCfg = JULIA_TASK_CONFIG_FILE;
      } else {
        juliaCfg += ',' + JULIA_TASK_CONFIG_FILE;
      }

      // ASM must generate at least 1.5 bytecode.
      final String asmClassVersion = System
          .getProperty(ClassVersionHelper.JULIA_GEN_CLASS_VERSION);
      if (asmClassVersion == null) {
        System.setProperty(ClassVersionHelper.JULIA_GEN_CLASS_VERSION, "1.5");
      } else {
        final int version = ClassVersionHelper.getClassVersion();
        if (version == Opcodes.V1_1 || version == Opcodes.V1_2
            || version == Opcodes.V1_3 || version == Opcodes.V1_4) {
          System.err.println("Warning 'task-component' framework requires 1.5 "
              + "byte code. System property \""
              + ClassVersionHelper.JULIA_GEN_CLASS_VERSION
              + "\" will be automatically upgraded from \"" + asmClassVersion
              + "\" to \"1.5\".");
          System.setProperty(ClassVersionHelper.JULIA_GEN_CLASS_VERSION, "1.5");
        }
      }

      context.put("julia.config", juliaCfg);
      // The Julia class is extended anonymously in order to add a
      // createBootstrap method that takes a map of initialization properties as
      // input.
      bootstrapComponent = new Julia() {
        /**
         * Creates a Julia bootstrap component. This methods always creates and
         * returns a new bootstrap component. this component is created as
         * follows:
         * <ul>
         * <li>a pre bootstrap component is created by assembling a
         * {@link Loader} object, a {@link BasicTypeFactoryMixin} object, and a
         * {@link BasicGenericFactoryMixin} object. The loader object is created
         * by instantiating the class specified in the "julia.loader" system
         * property, or associated to the "julia.loader" key in the contentDesc
         * Map.</li>
         * <li>the pre bootstrap component is used to create the real bootstrap
         * component, by calling the <tt>newFcInstance</tt> method of the
         * <tt>GenericFactory</tt> interface of the pre bootstrap component,
         * with the "bootstrap" string as controller descriptor.</li>
         * </ul>
         * 
         * @param context additional parameters.
         * @return the {@link Component} interface of the component instantiated
         *         from this factory.
         * @throws InstantiationException if the component cannot be created.
         */
        public Component createBootstrap(final Map context)
            throws InstantiationException {
          String boot = (String) context.get("julia.loader");
          if (boot == null) {
            boot = System.getProperty("julia.loader");
          }
          if (boot == null) {
            boot = DEFAULT_LOADER;
          }

          // creates the pre bootstrap controller components
          Loader loader;
          try {
            loader = (Loader) getClass().getClassLoader().loadClass(boot)
                .newInstance();
            loader.init(context);
          } catch (final Exception e) {
            throw new InstantiationException("Cannot find or instantiate the '"
                + boot
                + "' class specified in the julia.loader [system] property");
          }
          final BasicTypeFactoryMixin typeFactory = new BasicTypeFactoryMixin();
          final BasicGenericFactoryMixin genericFactory = new BasicGenericFactoryMixin();
          genericFactory._this_weaveableL = loader;
          genericFactory._this_weaveableTF = typeFactory;

          // use the pre bootstrap component to create the real bootstrap
          // component
          final ComponentType t = typeFactory
              .createFcType(new InterfaceType[0]);
          try {
            final Component bootstrap = genericFactory.newFcInstance(t,
                "bootstrap", null);
            try {
              ((Loader) bootstrap.getFcInterface("loader")).init(context);
            } catch (final NoSuchInterfaceException ignored) {
            }
            return bootstrap;
          } catch (final Exception e) {
            throw new ChainedInstantiationException(e, null,
                "Cannot create the bootstrap component");
          }
        }

      }.createBootstrap(context);
    }
    return bootstrapComponent;
  }
}
