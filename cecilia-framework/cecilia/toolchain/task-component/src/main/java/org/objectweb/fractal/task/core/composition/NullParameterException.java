/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.composition;

/**
 * Exception thrown when a {@link Function} is called with a <code>null</code>
 * parameter.
 */
public class NullParameterException extends FunctionExecutionException {

  private final int paramIndex;

  /**
   * Creates a new {@link NullParameterException}.
   * 
   * @param function the function object that has been invoked with an invalid
   *            parameter.
   * @param paramIndex the index of the invalid parameter.
   */
  public NullParameterException(final Function function, final int paramIndex) {
    this(function, paramIndex, null);
  }

  /**
   * Creates a new {@link NullParameterException}.
   * 
   * @param function the function object that has been invoked with an invalid
   *            parameter.
   * @param paramIndex the index of the invalid parameter.
   * @param message an additional message.
   */
  public NullParameterException(final Function function, final int paramIndex,
      final String message) {
    super(function, message);
    this.paramIndex = paramIndex;
  }

  /**
   * @return the index of the <code>null</code> parameter.
   */
  public int getParamIndex() {
    return paramIndex;
  }

  @Override
  public String getMessage() {
    String message = "In function \"" + getFunctionName() + "\", parameter "
        + paramIndex + "is null.";

    if (super.getMessage() != null) {
      message += ": " + super.getMessage();
    }

    return message;
  }
}
