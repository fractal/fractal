/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.control;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.control.binding.TypeBindingMixin;
import org.objectweb.fractal.task.core.TaskInterface;
import org.objectweb.fractal.util.Fractal;

public abstract class DebugBindingControllerMixin {

  public void bindFc(final InterfaceType clientItfType,
      final String clientItfName, final Object serverItf)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {
    final Object clientItf = _this_weaveableC.getFcInterface(clientItfType
        .getFcItfName());
    if (clientItf instanceof TaskInterface) {
      System.out.println("Bind "
          + ((_this_weaveableOptNC != null)
              ? (_this_weaveableOptNC.getFcName() + '.')
              : "anonymous.") + clientItfName + " with record="
          + ((TaskInterface) clientItf).getFcRecord());
    } else {
      System.out.println("Bind " + clientItf);
    }
    if (serverItf instanceof TaskInterface) {
      System.out.println("  to " + getFullItfName((TaskInterface) serverItf)
          + " with record=" + ((TaskInterface) serverItf).getFcRecord());
    } else {
      System.out.println("  to " + serverItf);
    }
    _super_bindFc(clientItfType, clientItfName, serverItf);
  }

  private String getFullItfName(final TaskInterface itf) {
    try {
      return Fractal.getNameController(itf.getFcItfOwner()).getFcName() + '.'
          + itf.getFcItfName();
    } catch (final NoSuchInterfaceException e) {
      return "anonymous." + itf.getFcItfName();
    }
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The {@link Component} interface of this component.
   */
  public Component      _this_weaveableC;

  /**
   * The optional {@link NameController} required by this mixin.
   */
  public NameController _this_weaveableOptNC;

  /**
   * @see TypeBindingMixin#bindFc(InterfaceType,String,Object)
   */
  public abstract void _super_bindFc(InterfaceType clientItfType,
      String clientItfName, Object serverItf) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException;
}
