/**
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.internal;

import static org.objectweb.fractal.api.type.TypeFactory.CLIENT;
import static org.objectweb.fractal.api.type.TypeFactory.COLLECTION;
import static org.objectweb.fractal.api.type.TypeFactory.MANDATORY;
import static org.objectweb.fractal.api.type.TypeFactory.SERVER;
import static org.objectweb.fractal.api.type.TypeFactory.SINGLE;
import static org.objectweb.fractal.util.Fractal.getGenericFactory;
import static org.objectweb.fractal.util.Fractal.getTypeFactory;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.task.core.Record;
import org.objectweb.fractal.task.core.TaskInterface;

/**
 * Provides utility methods to create task components.
 */
public final class TaskUtil {
  private TaskUtil() {
  }

  private static final TypeFactory    TYPE_FACTORY;
  private static final GenericFactory FACTORY;

  static {
    // create the julia bootstrap component
    try {
      final Component genericTaskFactory = new TaskBootstrapFactory()
          .newFcInstance();
      TYPE_FACTORY = getTypeFactory(genericTaskFactory);
      FACTORY = getGenericFactory(genericTaskFactory);
    } catch (final Exception e) {
      throw new TaskInternalError("Unable to initialize genericTaskFactory", e);
    }
  }

  public static final class TaskInterfaceDesc {
    final String  name;
    final String  signature;
    final boolean role;
    final boolean contingency;
    final boolean cardinality;
    final Record  record;

    private TaskInterfaceDesc(final String name, final String signaure,
        final boolean role, final boolean contingency,
        final boolean cardinality, final Record record) {
      this.name = name;
      this.signature = signaure;
      this.role = role;
      this.contingency = contingency;
      this.cardinality = cardinality;
      this.record = record;
    }
  }

  /**
   * Creates an descriptor of task interface.
   * 
   * @param name the name of the interface.
   * @param signaure the signature of the interface.
   * @param role the role of the interface (either {@link TypeFactory#CLIENT} or
   *            {@link TypeFactory#SERVER}).
   * @param contingency the contingency of the interface (either
   *            {@link TypeFactory#MANDATORY} or {@link TypeFactory#OPTIONAL}).
   * @param cardinality the cardinality of the interface (either
   *            {@link TypeFactory#SINGLE} or {@link TypeFactory#COLLECTION}).
   * @param record the record of the interface.
   * @return an interface descriptor.
   */
  public static TaskInterfaceDesc itf(final String name, final String signaure,
      final boolean role, final boolean contingency, final boolean cardinality,
      final Record record) {
    return new TaskInterfaceDesc(name, signaure, role, contingency,
        cardinality, record);
  }

  /**
   * Creates an descriptor of task interface.
   * 
   * @param name the name of the interface.
   * @param itfType the type of the interface.
   * @param record the record of the interface.
   * @return an interface descriptor.
   */
  public static TaskInterfaceDesc itf(final String name,
      final InterfaceType itfType, final Record record) {
    return itf(name, itfType.getFcItfSignature(), itfType.isFcClientItf(),
        itfType.isFcOptionalItf(), itfType.isFcCollectionItf(), record);
  }

  /**
   * Shortcut method for
   * {@link #itf(String, String, boolean, boolean, boolean, Record) itf} that
   * creates a {@link TypeFactory#MANDATORY mandatory}
   * {@link TypeFactory#SERVER server} interface.
   * 
   * @param name the name of the interface.
   * @param signaure the signature of the interface.
   * @param record the record of the interface.
   * @return an interface descriptor.
   */
  public static TaskInterfaceDesc server(final String name,
      final String signaure, final Record record) {
    return itf(name, signaure, SERVER, MANDATORY, SINGLE, record);
  }

  /**
   * Shortcut for
   * {@link #itf(String, String, boolean, boolean, boolean, Record) itf} that
   * creates a {@link TypeFactory#CLIENT client} interface.
   * 
   * @param name the name of the interface.
   * @param signaure the signature of the interface.
   * @param contingency the contingency of the interface (either
   *            {@link TypeFactory#MANDATORY} or {@link TypeFactory#OPTIONAL}).
   * @param cardinality the cardinality of the interface (either
   *            {@link TypeFactory#SINGLE} or {@link TypeFactory#COLLECTION}).
   * @param record the record of the interface.
   * @return an interface descriptor.
   */
  public static TaskInterfaceDesc client(final String name,
      final String signaure, final boolean contingency,
      final boolean cardinality, final Record record) {
    return itf(name, signaure, CLIENT, contingency, cardinality, record);
  }

  /**
   * Shortcut for
   * {@link #itf(String, String, boolean, boolean, boolean, Record) itf} that
   * creates a {@link TypeFactory#CLIENT client}
   * {@link TypeFactory#MANDATORY mandatory}
   * {@link TypeFactory#SINGLE singleton} interface.
   * 
   * @param name the name of the interface.
   * @param signaure the signature of the interface.
   * @param record the record of the interface.
   * @return an interface descriptor.
   */
  public static TaskInterfaceDesc client(final String name,
      final String signaure, final Record record) {
    return client(name, signaure, MANDATORY, SINGLE, record);
  }

  /**
   * Shortcut for
   * {@link #itf(String, String, boolean, boolean, boolean, Record) itf} that
   * creates a {@link TypeFactory#CLIENT client}
   * {@link TypeFactory#MANDATORY mandatory}
   * {@link TypeFactory#COLLECTION collection} interface.
   * 
   * @param name the name of the interface.
   * @param signaure the signature of the interface.
   * @param record the record of the interface.
   * @return an interface descriptor.
   */
  public static TaskInterfaceDesc clientColl(final String name,
      final String signaure, final Record record) {
    return client(name, signaure, MANDATORY, COLLECTION, record);
  }

  /**
   * Creates a new task component.
   * 
   * @param implementationClass the implementation of the task. If
   *            <code>null</code> create a composite.
   * @param debugBinding if <code>true</code> the execution of the
   *            <code>bindFc</code> will be traced.
   * @param debugExecution if <code>true</code> trace execution of server
   *            interfaces.
   * @param itfs the descriptors of the task interfaces.
   * @return a new task component.
   * @throws InstantiationException if the instantiation of the task fails.
   */
  public static Component newTask(final Object implementationClass,
      final boolean debugBinding, final TaskInterfaceDesc... itfs)
      throws InstantiationException {
    final InterfaceType[] interfaceTypes = new InterfaceType[itfs.length];

    for (int i = 0; i < itfs.length; i++) {
      final TaskInterfaceDesc itf = itfs[i];
      interfaceTypes[i] = TYPE_FACTORY.createFcItfType(itf.name, itf.signature,
          itf.role, itf.contingency, itf.cardinality);
    }

    final Type taskType = TYPE_FACTORY.createFcType(interfaceTypes);

    String ctrlDesc = (implementationClass == null)
        ? "task-composite"
        : "task-primitive";
    if (debugBinding) ctrlDesc += "-debug-binding";

    final Component task = FACTORY.newFcInstance(taskType, ctrlDesc,
        implementationClass);

    for (final TaskInterfaceDesc itf : itfs) {
      TaskInterface tItf;
      try {
        tItf = (TaskInterface) task.getFcInterface(itf.name);
      } catch (final NoSuchInterfaceException e) {
        throw new TaskInternalError("Can't find component interface");
      }
      tItf.setFcRecord(itf.record);
    }

    return task;
  }
}
