/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.composition.function;

import org.objectweb.fractal.task.core.composition.Function;
import org.objectweb.fractal.task.core.composition.FunctionExecutionException;
import org.objectweb.fractal.task.core.composition.InvalidNumberOfParameterException;
import org.objectweb.fractal.task.core.composition.InvalidParameterTypeException;
import org.objectweb.fractal.task.core.composition.NullParameterException;

/**
 * Implements a boolean And function. This function takes at least one
 * {@link Boolean} parameter and returns a {@link Boolean}. It implements a
 * lazy semantics (returns {@link Boolean#FALSE false} at the first
 * {@link Boolean#FALSE false} parameter).
 */
public class And implements Function {

  public Object invoke(final Object... params)
      throws FunctionExecutionException {
    if (params.length == 0) {
      throw new InvalidNumberOfParameterException(this, 1, 0,
          "The And function expects at least one parameter.");
    }
    for (int i = 0; i < params.length; i++) {
      final Object param = params[i];
      if (!(param instanceof Boolean)) {
        if (param == null)
          throw new NullParameterException(this, i);
        else
          throw new InvalidParameterTypeException(this, i, Boolean.class, param
              .getClass());
      }
      if (!((Boolean) param)) { // if param is false
        // return false (lazy evaluation)
        return Boolean.FALSE;
      }
    }
    return Boolean.TRUE;
  }
}
