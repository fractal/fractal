/***
 * Fractal ADL Task Framework Parser
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Ali Erdem Ozcan
 */

package org.objectweb.fractal.task.core.primitive.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.objectweb.fractal.api.type.TypeFactory;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ClientInterfaceForEach {

  /**
   * @return the name of the {@link TaskParameters#value() task parameter} that
   *         correspond to the {@link Iterable iterable} for which a client
   *         interface will be created for each of its element.
   */
  String iterable();

  /**
   * @return the prefix of the client interfaces. The full name of each client
   *         interface is determined at creation time.
   */
  String prefix();

  /**
   * @return Type of the client interfaces.
   */
  Class<?> signature();

  /**
   * @return the contingency value of the interface
   */
  boolean contingency() default TypeFactory.MANDATORY;

  /**
   * @return the record descriptor.
   */
  String record();

  /**
   * @return the list of variable names that have to be passed as parameters to
   *         the record descriptor.
   */
  String[] parameters() default {};
}
