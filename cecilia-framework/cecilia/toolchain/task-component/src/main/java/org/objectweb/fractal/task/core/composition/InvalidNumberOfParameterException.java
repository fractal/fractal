/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.composition;

/**
 * Exception thrown by {@link Function} when an invalid number of parameter is
 * received.
 */
public class InvalidNumberOfParameterException
    extends
      FunctionExecutionException {

  private final int expected;
  private final int actual;

  /**
   * Creates a new {@link InvalidParameterTypeException}.
   * 
   * @param function the function object that has been invoked with an invalid
   *            parameter.
   * @param expected the expected number of parameter
   * @param actual the actual number of parameter
   */
  public InvalidNumberOfParameterException(final Function function,
      final int expected, final int actual) {
    this(function, expected, actual, null);
  }

  /**
   * Creates a new {@link InvalidParameterTypeException}.
   * 
   * @param function the function object that has been invoked with an invalid
   *            parameter.
   * @param expected the expected number of parameter
   * @param actual the actual number of parameter
   * @param message an additional message.
   */
  public InvalidNumberOfParameterException(final Function function,
      final int expected, final int actual, final String message) {
    super(function, message);
    this.expected = expected;
    this.actual = actual;
  }

  @Override
  public String getMessage() {
    String message = "In function \"" + getFunctionName()
        + "\", invalid pnumber of parameter. Found " + actual + " where "
        + expected + " was expected";

    if (super.getMessage() != null) {
      message += ": " + super.getMessage();
    }

    return message;
  }
}
