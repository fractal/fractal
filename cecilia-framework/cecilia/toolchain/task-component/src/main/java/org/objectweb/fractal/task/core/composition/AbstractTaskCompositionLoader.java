/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2006 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributor: Philippe Merle
 *
 * $Id: AbstractLoader.java 3141 2007-06-20 13:46:00Z leclercq $
 */

package org.objectweb.fractal.task.core.composition;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;

/**
 * An abstract {@link Loader} that delegates to another {@link Loader}.
 */
public abstract class AbstractTaskCompositionLoader
    implements
      BindingController,
      TaskCompositionLoader {

  /**
   * Name of the mandatory interface bound to the {@link Loader} used by this
   * loader.
   */
  public static final String   LOADER_BINDING = "client-loader";

  /**
   * The {@link Loader} used by this loader.
   */
  // TODO rename clientLoader to clientLoaderItf
  public TaskCompositionLoader clientLoader;

  // ------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ------------------------------------------------------------------------

  public String[] listFc() {
    return new String[]{LOADER_BINDING};
  }

  public Object lookupFc(final String s) throws NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (LOADER_BINDING.equals(s)) {
      return clientLoader;
    } else {
      throw new NoSuchInterfaceException("No client interface named '" + s
          + "'");
    }
  }

  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (LOADER_BINDING.equals(s)) {
      clientLoader = (TaskCompositionLoader) o;
    } else {
      throw new NoSuchInterfaceException("No client interface named '" + s
          + "' for binding the interface");
    }
  }

  public void unbindFc(final String s) throws IllegalBindingException,
      NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (LOADER_BINDING.equals(s)) {
      clientLoader = null;
    } else {
      throw new NoSuchInterfaceException("No client interface named '" + s
          + "'");
    }
  }
}
