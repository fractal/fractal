/***
 * Fractal ADL Task Framework
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.composition.ast;

import org.objectweb.fractal.adl.Node;

/**
 * Provides helper methods to access common ASTs decorations.
 */
public final class DecorationHelper {
  private DecorationHelper() {
  }

  public static final String DEBUG_DECORATION = "debug";

  public static void setDebugDecoration(final Node node, final String[] value) {
    if (!(node instanceof Task) && !(node instanceof Instruction)) {
      throw new IllegalArgumentException(
          "node must be either a Task or an Instruction");
    }
    node.astSetDecoration(DEBUG_DECORATION, value);
  }

  public static String[] getDebugDecoration(final Node node) {
    return (String[]) node.astGetDecoration(DEBUG_DECORATION);
  }

  public static boolean debugDecorationContains(final Node node, final String s) {
    final String[] debug = getDebugDecoration(node);
    if (debug == null) return false;
    for (final String d : debug) {
      if (d.equals(s)) return true;
    }
    return false;
  }
}