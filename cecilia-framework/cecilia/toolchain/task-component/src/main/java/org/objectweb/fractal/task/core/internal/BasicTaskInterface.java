/**
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.internal;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.julia.BasicComponentInterface;
import org.objectweb.fractal.task.core.Record;
import org.objectweb.fractal.task.core.TaskInterface;

/**
 * Basic implementation of the {@link TaskInterface} interface.
 */
public abstract class BasicTaskInterface extends BasicComponentInterface
    implements
      TaskInterface {

  protected Record record;

  /**
   * @see BasicComponentInterface#BasicComponentInterface()
   */
  public BasicTaskInterface() {
    super();
  }

  /**
   * @see BasicComponentInterface#BasicComponentInterface(Component, String,
   *      Type, boolean, Object)
   */
  public BasicTaskInterface(final Component owner, final String name,
      final Type type, final boolean isInternal, final Object impl) {
    super(owner, name, type, isInternal, impl);
  }

  public Record getFcRecord() {
    return record;
  }

  public void setFcRecord(final Record r) {
    this.record = r;
  }
}
