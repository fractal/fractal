/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.composition;

import static org.objectweb.fractal.task.core.composition.ast.DecorationHelper.debugDecorationContains;
import static org.objectweb.fractal.task.core.composition.ast.Pattern.CLIENT_ROLE;
import static org.objectweb.fractal.task.core.composition.ast.Pattern.CLOSED_PATTERN;
import static org.objectweb.fractal.task.core.internal.TaskUtil.itf;
import static org.objectweb.fractal.task.core.internal.TaskUtil.newTask;
import static org.objectweb.fractal.util.Fractal.getBindingController;
import static org.objectweb.fractal.util.Fractal.getContentController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.control.binding.ChainedIllegalBindingException;
import org.objectweb.fractal.julia.factory.ChainedInstantiationException;
import org.objectweb.fractal.julia.loader.Loader;
import org.objectweb.fractal.task.core.Record;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.TaskInterface;
import org.objectweb.fractal.task.core.composition.ast.Binding;
import org.objectweb.fractal.task.core.composition.ast.Constant;
import org.objectweb.fractal.task.core.composition.ast.Export;
import org.objectweb.fractal.task.core.composition.ast.FunctionCall;
import org.objectweb.fractal.task.core.composition.ast.Instruction;
import org.objectweb.fractal.task.core.composition.ast.Pattern;
import org.objectweb.fractal.task.core.composition.ast.RecordField;
import org.objectweb.fractal.task.core.composition.ast.Task;
import org.objectweb.fractal.task.core.composition.ast.TaskDefinition;
import org.objectweb.fractal.task.core.composition.ast.Value;
import org.objectweb.fractal.task.core.composition.ast.ValueContainer;
import org.objectweb.fractal.task.core.composition.ast.Variable;
import org.objectweb.fractal.task.core.composition.function.Same;
import org.objectweb.fractal.task.core.internal.TaskInternalError;
import org.objectweb.fractal.task.core.internal.TaskUtil.TaskInterfaceDesc;
import org.objectweb.fractal.task.core.primitive.annotations.Debug;
import org.objectweb.fractal.util.Fractal;

public class Interpreter implements CompositeTaskFactory, BindingController {

  // ---------------------------------------------------------------------------
  // Client interfaces
  // ---------------------------------------------------------------------------

  /** the name of the {@link #clientLoaderItf} interface. */
  public static final String   CLIENT_LOADER_NAME = "client-loader";

  /** The {@link Loader} interface used to load the composition schema. */
  public TaskCompositionLoader clientLoaderItf;

  // ---------------------------------------------------------------------------
  // Implementation of the CompositeTaskFactory interface
  // ---------------------------------------------------------------------------

  public Component newCompositeTask(final Collection<Component> subTasks,
      final String compositionName, final Map<Object, Object> context,
      final Object... parameters) throws TaskException {
    final TaskDefinition taskDefinition = clientLoaderItf.load(compositionName,
        context);
    try {
      final String parameterStrings = Arrays.deepToString(parameters);
      return createCompositeComponent(taskDefinition, taskDefinition.getTask()
          .getName()
          + '('
          + parameterStrings.substring(1, parameterStrings.length() - 1)
          + ')', subTasks, parameters);
    } catch (final EvaluationException e) {
      throw new TaskException(
          "An error occurs while evaluating task composition ("
              + compositionName + ")", e);
    } catch (final InstantiationException e) {
      throw new TaskException(
          "An error occurs while instantiating task composite ("
              + compositionName + ")", e);
    } catch (final IllegalBindingException e) {
      throw new TaskException("An error occurs while binding tasks ("
          + compositionName + ")", e);
    }
  }

  public Component newCompositeTask(final Collection<Component> subTasks,
      final String compositionName, final String compositeName,
      final Map<Object, Object> context, final Object... parameters)
      throws TaskException {
    final TaskDefinition taskDefinition = clientLoaderItf.load(compositionName,
        context);
    try {
      return createCompositeComponent(taskDefinition, compositeName, subTasks,
          parameters);
    } catch (final EvaluationException e) {
      throw new TaskException(
          "An error occurs while evaluating task composition ("
              + compositionName + ")", e);
    } catch (final InstantiationException e) {
      throw new TaskException(
          "An error occurs while instantiating task composite ("
              + compositionName + ")", e);
    } catch (final IllegalBindingException e) {
      throw new TaskException("An error occurs while binding tasks ("
          + compositionName + ")", e);
    }
  }

  // ---------------------------------------------------------------------------
  // internal methods
  // ---------------------------------------------------------------------------

  Component createCompositeComponent(final TaskDefinition taskDefinition,
      final String compositeName, final Collection<Component> subTasks,
      final Object... parameters) throws EvaluationException,
      InstantiationException, IllegalBindingException {

    final Task task = taskDefinition.getTask();
    final Map<String, Object> globalVariables = new HashMap<String, Object>();

    final Variable[] formalParamters = task.getVariables();
    if (formalParamters.length != parameters.length) {
      throw new EvaluationException(task, "Invalid number of parameter: found "
          + parameters.length + " where " + formalParamters.length
          + " were expected.");
    }
    for (int i = 0; i < formalParamters.length; i++) {
      globalVariables.put(formalParamters[i].getName(), parameters[i]);
    }

    // print debug message
    final boolean debug = debugDecorationContains(task, Debug.INSTANTIATION);
    final boolean debugBinding = debugDecorationContains(task, Debug.BINDING);
    boolean printDebugHeader;
    if (debug) {
      printDebugHeader = true;
    } else {
      printDebugHeader = false;
      for (final Instruction instruction : task.getInstructions()) {
        if (debugDecorationContains(instruction, Debug.INSTANTIATION)) {
          printDebugHeader = true;
          break;
        }
      }
    }
    if (printDebugHeader) {
      System.out.println();
      System.out
          .println("--------------------------------------------------------------------------------");
      System.out.println("Create a new composite task from " + task.getName());
      if (!globalVariables.isEmpty())
        System.out.println("Parameters = " + globalVariables);
      System.out.println("Task name = " + compositeName);

    }

    // the descriptor of the composite task to create.
    final CompositeTask composite = new CompositeTask(subTasks);

    // use ArrayList for serverInterfaces collection to have efficient iteration
    final Collection<TaskInterface> serverInterfaces = new ArrayList<TaskInterface>();
    // use ArrayList for serverInterfaces collection to have efficient removal
    final Collection<TaskInterface> clientInterfaces = new HashSet<TaskInterface>();

    // prepare the collection of client and server interfaces.
    prepare(subTasks, serverInterfaces, clientInterfaces);

    for (final Instruction instruction : task.getInstructions()) {
      final boolean debugInstruction = debug
          || debugDecorationContains(instruction, Debug.INSTANTIATION);
      if (instruction.getExport() != null) {
        // Evaluate export
        evaluateExport(composite, globalVariables, instruction.getExport(),
            serverInterfaces, clientInterfaces, debugInstruction);
      } else {
        // Evaluate bind
        evaluateBinding(composite, globalVariables, instruction.getBinding(),
            serverInterfaces, clientInterfaces, debugInstruction);
      }
    }
    if (printDebugHeader) {
      System.out
          .println("--------------------------------------------------------------------------------");
    }
    return composite.createComponent(compositeName, debugBinding);
  }

  void prepare(final Collection<Component> tasks,
      final Collection<TaskInterface> serverInterfaces,
      final Collection<TaskInterface> clientInterfaces) {

    // for each task component
    for (final Component task : tasks) {

      // for each interface of the task
      for (final Object itf : task.getFcInterfaces()) {

        // if not a task interface, pass it.
        if (!(itf instanceof TaskInterface)) continue;
        final TaskInterface tItf = (TaskInterface) itf;
        final Record record = tItf.getFcRecord();

        // if it has no record, pass it.
        if (record == null) continue;

        if (((InterfaceType) tItf.getFcItfType()).isFcClientItf()) {
          // interface is a client interface.
          clientInterfaces.add(tItf);
        } else {
          // interface is a server interface.
          serverInterfaces.add(tItf);
        }
      }
    }
  }
  
  @SuppressWarnings("unchecked")
  void evaluateExport(final CompositeTask composite,
      final Map<String, Object> globalVariables, final Export export,
      final Collection<TaskInterface> serverInterfaces,
      final Collection<TaskInterface> clientInterfaces, final boolean debug)
      throws EvaluationException {
    if (debug) {
      System.out.println(" - Evaluate export from " + export.astGetSource());
    }

    final Pattern internalRecord = export.getInternalRecord();
    final Pattern externalRecord = export.getExternalRecord();

    // when evaluating export, there are 2 variable sets : the global on, the
    // variables of the pattern
    final Map<String, Object>[] variables = new Map[2];
    variables[0] = globalVariables;

    final Map<TaskInterface, Map<String, Object>> filteredServerInterfaces = filterInterfaces(
        internalRecord, serverInterfaces, variables);
    final Map<TaskInterface, Map<String, Object>> filteredClientInterfaces = filterInterfaces(
        internalRecord, clientInterfaces, variables);

    ValueContainer predicate = (ValueContainer) internalRecord;
    if (getValue(predicate) == null) predicate = null;

    // For each client interface
    final Iterator<Map.Entry<TaskInterface, Map<String, Object>>> clientEntryIterator = filteredClientInterfaces
        .entrySet().iterator();
    while (clientEntryIterator.hasNext()) {
      final Map.Entry<TaskInterface, Map<String, Object>> clientEntry = clientEntryIterator
          .next();

      // get the client task interface
      final TaskInterface clientItf = clientEntry.getKey();

      // set the variable of pattern
      variables[1] = clientEntry.getValue();

      // evaluate the function
      final Boolean result;
      if (predicate != null) {
        final Object value = evaluateValue(predicate, variables);
        if (!(value instanceof Boolean))
          throw new EvaluationException(predicate,
              "Invalid predicate value: not a boolean");

        result = (Boolean) value;
      } else {
        result = Boolean.TRUE;
      }

      if (result) {
        // clientItf matches the pattern

        final Record record = (externalRecord != null) ? createRecord(
            externalRecord, variables) : clientItf.getFcRecord();
        if (debug) {
          System.out.println("    * exports client interface "
              + getFullInterfaceName(clientItf) + " with record=" + record);
        }
        composite.addExportedInterface(export, record, clientItf);

        if (!((InterfaceType) clientItf.getFcItfType()).isFcCollectionItf()) {
          // client interface is a singleton. It is bound so remove it from
          // available interfaces.
          clientEntryIterator.remove();
          clientInterfaces.remove(clientItf);
        }
      }
    }

    // For each server interface
    for (final Map.Entry<TaskInterface, Map<String, Object>> serverEntry : filteredServerInterfaces
        .entrySet()) {

      // get the server task interface
      final TaskInterface serverItf = serverEntry.getKey();

      // set the variable of pattern
      variables[1] = serverEntry.getValue();

      // evaluate the function
      final Boolean result;
      if (predicate != null) {
        final Object value = evaluateValue(predicate, variables);
        if (!(value instanceof Boolean))
          throw new EvaluationException(predicate,
              "Invalid predicate value: not a boolean");

        result = (Boolean) value;
      } else {
        result = Boolean.TRUE;
      }

      if (result) {
        // serverItf matches the pattern
        final Record record = (externalRecord != null) ? createRecord(
            externalRecord, variables) : serverItf.getFcRecord();
        if (debug) {
          System.out.println("    * exports server interface "
              + getFullInterfaceName(serverItf) + " with record=" + record);
        }
        composite.addExportedInterface(export, record, serverItf);
      }
    }
  }
  
  @SuppressWarnings("unchecked")
  void evaluateBinding(final CompositeTask composite,
      final Map<String, Object> globalVariables, final Binding binding,
      final Collection<TaskInterface> serverInterfaces,
      final Collection<TaskInterface> clientInterfaces, final boolean debug)
      throws EvaluationException {
    if (debug) {
      System.out.println(" - Evaluate binding from " + binding.astGetSource());
    }

    // when evaluating bindings, there are 3 variable sets : the global on, the
    // variables of the client side pattern and the variables of the server side
    // pattern.
    final Map<String, Object>[] variables = new Map[3];
    variables[0] = globalVariables;

    final Map<TaskInterface, Map<String, Object>> filteredServerInterfaces = filterInterfaces(
        binding.getServerRecord(), serverInterfaces, variables);
    final Map<TaskInterface, Map<String, Object>> filteredClientInterfaces = filterInterfaces(
        binding.getClientRecord(), clientInterfaces, variables);

    // get the function to evaluate
    ValueContainer predicate = (ValueContainer) binding;
    if (getValue(predicate) == null) predicate = null;

    // For each client interface
    final Iterator<Map.Entry<TaskInterface, Map<String, Object>>> clientEntryIterator = filteredClientInterfaces
        .entrySet().iterator();
    while (clientEntryIterator.hasNext()) {
      final Map.Entry<TaskInterface, Map<String, Object>> clientEntry = clientEntryIterator
          .next();

      // get the client task interface
      final TaskInterface clientItf = clientEntry.getKey();

      // set the variable of the client side pattern
      variables[1] = clientEntry.getValue();

      // for each server interfaces.
      for (final Map.Entry<TaskInterface, Map<String, Object>> serverEntry : filteredServerInterfaces
          .entrySet()) {

        // get the server interface.
        final TaskInterface serverItf = serverEntry.getKey();

        // set the variable of the server side pattern
        variables[2] = serverEntry.getValue();

        // evaluate the function
        final Boolean result;
        if (predicate != null) {
          final Object value = evaluateValue(predicate, variables);
          if (!(value instanceof Boolean))
            throw new EvaluationException(predicate,
                "Invalid predicate value: not a boolean");

          result = (Boolean) value;
        } else {
          result = Boolean.TRUE;
        }

        if (result) {
          // clientItf and serverItf matches the pattern
          if (debug) {
            System.out.println("    * binds " + getFullInterfaceName(clientItf)
                + " with record=" + clientItf.getFcRecord());
            System.out.println("         to " + getFullInterfaceName(serverItf)
                + " with record=" + serverItf.getFcRecord());
          }

          composite.addBinding(clientItf, serverItf);

          if (!((InterfaceType) clientItf.getFcItfType()).isFcCollectionItf()) {
            // client interface is a singleton. It is bound so remove it from
            // available interfaces.
            clientEntryIterator.remove();
            clientInterfaces.remove(clientItf);
            // and break current loop on server interfaces.
            break;
          }
        }
      }
    }
  }

  Map<TaskInterface, Map<String, Object>> filterInterfaces(
      final Pattern pattern, final Collection<TaskInterface> interfaces,
      final Map<String, Object>[] variables) throws EvaluationException {
    final Map<TaskInterface, Map<String, Object>> filteredInterfaces = new IdentityHashMap<TaskInterface, Map<String, Object>>();
    Map<String, Object> patternVariables = new HashMap<String, Object>();
    for (final TaskInterface itf : interfaces) {
      if (patternMatcher(itf, pattern, variables, patternVariables)) {
        filteredInterfaces.put(itf, patternVariables);
        patternVariables = new HashMap<String, Object>();
      }
    }
    return filteredInterfaces;
  }

  boolean patternMatcher(final TaskInterface itf, final Pattern pattern,
      final Map<String, Object>[] variables,
      final Map<String, Object> patternVariables) throws EvaluationException {
    // first checks role compatibility (if the pattern have one)
    if (pattern.getRole() != null) {
      final boolean clientPattern = pattern.getRole().equals(CLIENT_ROLE);
      if (clientPattern != ((InterfaceType) itf.getFcItfType()).isFcClientItf()) {
        // the pattern and the interface do not have the same role, the pattern
        // does not match.
        return false;
      }
    }
    final Record record = itf.getFcRecord();
    if (CLOSED_PATTERN.equals(pattern.getClosure())) {
      if (record.getFieldCount() != pattern.getRecordFields().length) {
        // the pattern is closed and does not contains as many fields as the
        // record, so it can't match.
        return false;
      }
      // else the pattern and the record have the same number of field, so
      // compare field by field
    }

    // checks that each field in the pattern matches a field of the record.
    for (final RecordField field : pattern.getRecordFields()) {
      // get the value of the field
      final Object recordValue = record.get(field.getName());

      // if the field does not contain a field with the given name.
      // the pattern does not match
      if (recordValue == null) return false;

      final Variable variable = field.getVariable();
      if (variable == null || variables[0].containsKey(variable.getName())) {
        // The pattern field is a defined variable or an expression.
        // It has a value that can be evaluated.
        final Object value = evaluateValue(field, variables);

        // if value is not the same as the record value. the pattern does not
        // match
        if (!Same.same(value, recordValue)) return false;

      } else {
        // The pattern field declares a variable.
        patternVariables.put(variable.getName(), recordValue);
      }
    }
    // the pattern match
    return true;
  }

  Object evaluateValue(final ValueContainer container,
      final Map<String, Object>[] variables) throws EvaluationException {
    final Constant constant = container.getConstant();
    if (constant != null) {
      return evaluateConstant(constant);
    }

    final Variable variable = container.getVariable();
    if (variable != null) {
      // assumes that the variable reference is correct.
      for (final Map<String, Object> v : variables) {
        final Object o = v.get(variable.getName());
        if (o != null) {
          return o;
        }
      }
      // the value of the variable may be null
      return null;

    }

    final FunctionCall functionCall = container.getFunction();
    final ValueContainer[] functionParameters = functionCall
        .getFunctionParameters();
    final Object[] paramValues = new Object[functionParameters.length];
    for (int i = 0; i < functionParameters.length; i++) {
      // evaluate parameter
      paramValues[i] = evaluateValue(functionParameters[i], variables);
    }

    final Function functionObject = (Function) functionCall
        .astGetDecoration("function");

    try {
      return functionObject.invoke(paramValues);
    } catch (final FunctionExecutionException e) {
      throw new EvaluationException(functionCall,
          "An error occurs while evaluating function.", e);
    }
  }

  static Object evaluateConstant(final Constant constant) {
    switch (Constant.ConstantTypeEnum.valueOf(constant.getType())) {
      case BOOLEAN :
        return Boolean.parseBoolean(constant.getValue());
      case INTEGER :
        return Integer.parseInt(constant.getValue());
      case STRING :
        return constant.getValue();
      case NULL :
      default :
        return null;
    }
  }

  static Value getValue(final ValueContainer container) {
    if (container.getConstant() != null) return container.getConstant();
    if (container.getVariable() != null) return container.getVariable();
    if (container.getFunction() != null) return container.getFunction();
    return null;
  }

  Record createRecord(final Pattern specifier,
      final Map<String, Object>[] variables) throws EvaluationException {
    final Map<String, Object> record = new HashMap<String, Object>();

    for (final RecordField field : specifier.getRecordFields()) {
      record.put(field.getName(), evaluateValue(field, variables));
    }
    return new Record(record);
  }

  String getFullInterfaceName(final Interface itf) {
    try {
      final String ownerName = Fractal.getNameController(itf.getFcItfOwner())
          .getFcName();
      return ownerName + '.' + itf.getFcItfName();
    } catch (final NoSuchInterfaceException e) {
      return "anonymous." + itf.getFcItfName();
    }
  }

  private static final class CompositeTask {
    final Map<Record, TaskInterface>       exportedServerInterfaces = new IdentityHashMap<Record, TaskInterface>();
    final Map<Record, List<TaskInterface>> exportedClientInterfaces = new HashMap<Record, List<TaskInterface>>();
    final Collection<TaskBinding>          bindings                 = new ArrayList<TaskBinding>();
    final Collection<Component>            subTasks;
    int                                    collectionCounter        = 0;

    CompositeTask(final Collection<Component> subTasks) {
      this.subTasks = subTasks;
    }

    void addBinding(final TaskInterface client, final TaskInterface server) {
      TaskBinding binding = new TaskBinding(client, server);
      if (((InterfaceType) client.getFcItfType()).isFcCollectionItf()) {
        binding = new TaskBinding(client, client.getFcItfName()
            + (collectionCounter++), server);
      } else {
        binding = new TaskBinding(client, server);
      }
      bindings.add(binding);
    }

    void addExportedInterface(final Node location, final Record record,
        final TaskInterface itf) throws EvaluationException {
      final InterfaceType itfType = ((InterfaceType) itf.getFcItfType());
      if (itfType.isFcClientItf()) {
        List<TaskInterface> exportedClients = exportedClientInterfaces
            .get(record);
        if (exportedClients == null) {
          exportedClients = new ArrayList<TaskInterface>();
          exportedClientInterfaces.put(record, exportedClients);
        } else {
          // checks that the already exported interface with the same record
          // are compatible
          if (!itfType.getFcItfSignature().equals(
              ((InterfaceType) exportedClients.get(0).getFcItfType())
                  .getFcItfSignature())) {
            throw new EvaluationException(location, "Can't export interface ' "
                + itf.getFcItfName() + ", incompatible client interfaces"
                + " with the same record are already exported");
          }
        }
        exportedClients.add(itf);
      } else {
        exportedServerInterfaces.put(record, itf);
      }
    }

    Component createComponent(final String componentName,
        final boolean debugBinding) throws InstantiationException,
        IllegalBindingException {

      // create the list of component interfaces
      final List<TaskInterfaceDesc> interfaces = new ArrayList<TaskInterfaceDesc>();
      final Map<String, TaskInterface> expServerItfs = new HashMap<String, TaskInterface>();
      final Map<String, List<TaskInterface>> expClientItfs = new HashMap<String, List<TaskInterface>>();

      // find a unique name for the exported server interfaces.
      final Set<String> interfaceNames = new HashSet<String>();
      for (final Map.Entry<Record, TaskInterface> exportedEntry : exportedServerInterfaces
          .entrySet()) {
        final TaskInterface itf = exportedEntry.getValue();

        final String interfaceName = itf.getFcItfName();
        String exportedInterfaceName = interfaceName;
        int suffix = 1;
        while (!interfaceNames.add(exportedInterfaceName)) {
          exportedInterfaceName = interfaceName + suffix;
          suffix++;
        }

        interfaces.add(itf(exportedInterfaceName, (InterfaceType) itf
            .getFcItfType(), exportedEntry.getKey()));
        expServerItfs.put(exportedInterfaceName, itf);
      }

      // find a unique name for the exported server interfaces.
      for (final Map.Entry<Record, List<TaskInterface>> exportedEntry : exportedClientInterfaces
          .entrySet()) {
        final TaskInterface itf = exportedEntry.getValue().get(0);

        final String interfaceName = itf.getFcItfName();
        String exportedInterfaceName = interfaceName;
        int suffix = 1;
        while (!interfaceNames.add(exportedInterfaceName)) {
          exportedInterfaceName = interfaceName + suffix;
          suffix++;
        }

        interfaces.add(itf(exportedInterfaceName, (InterfaceType) itf
            .getFcItfType(), exportedEntry.getKey()));
        expClientItfs.put(exportedInterfaceName, exportedEntry.getValue());
      }

      // create the composite
      final Component compositeTask = newTask(null, debugBinding, interfaces
          .toArray(new TaskInterfaceDesc[interfaces.size()]));

      try {
        // set component name
        Fractal.getNameController(compositeTask).setFcName(componentName);

        // add sub components
        final ContentController compositeTaskCC = getContentController(compositeTask);
        for (final Component component : subTasks) {
          compositeTaskCC.addFcSubComponent(component);
        }

        // create internal bindings
        for (final TaskBinding binding : bindings) {
          binding.instantiate();
        }

        // create export server bindings
        for (final Map.Entry<String, TaskInterface> expEntry : expServerItfs
            .entrySet()) {
          final String itfName = expEntry.getKey();
          final TaskInterface exportedInterface = expEntry.getValue();

          // export a server interface
          getBindingController(compositeTask)
              .bindFc(itfName, exportedInterface);
        }

        // create export client bindings
        for (final Map.Entry<String, List<TaskInterface>> expEntry : expClientItfs
            .entrySet()) {
          final String itfName = expEntry.getKey();
          final Object serverItf = compositeTaskCC
              .getFcInternalInterface(itfName);

          for (final TaskInterface exportedInterface : expEntry.getValue()) {
            final BindingController clientBC = getBindingController(exportedInterface
                .getFcItfOwner());
            clientBC.bindFc(exportedInterface.getFcItfName(), serverItf);
          }
        }

        // checks that every mandatory client interfaces of the sub components
        // are bound.
        for (final Component subTask : subTasks) {
          for (final InterfaceType itfType : ((ComponentType) subTask
              .getFcType()).getFcInterfaceTypes()) {
            // if itf is server or is optional or is collection, pass it
            if (!itfType.isFcClientItf() || itfType.isFcOptionalItf()
                || itfType.isFcCollectionItf()) continue;

            if (getBindingController(subTask).lookupFc(itfType.getFcItfName()) == null) {
              // interface is not bound.
              throw new ChainedIllegalBindingException(null, subTask, null,
                  itfType.getFcItfName(), null,
                  "Mandatory client interface is not bound.");
            }
          }
        }

      } catch (final NoSuchInterfaceException e) {
        throw new ChainedInstantiationException(e, null,
            "Unable to created task composite");
      } catch (final IllegalContentException e) {
        throw new ChainedInstantiationException(e, null,
            "Unable to created task composite");
      } catch (final IllegalLifeCycleException e) {
        throw new ChainedInstantiationException(e, null,
            "Unable to created task composite");
      }

      return compositeTask;
    }
  }

  private static final class TaskBinding {
    final BindingController clientBC;
    final String            clientItfName;
    final TaskInterface     server;

    TaskBinding(final TaskInterface client, final TaskInterface server) {
      this(client, client.getFcItfName(), server);
    }

    TaskBinding(final TaskInterface client, final String clientItfName,
        final TaskInterface server) {
      try {
        this.clientBC = getBindingController(client.getFcItfOwner());
      } catch (final NoSuchInterfaceException e) {
        throw new TaskInternalError("Component " + client.getFcItfOwner()
            + " has no binding controller.");
      }
      this.clientItfName = clientItfName;
      this.server = server;
    }

    void instantiate() throws NoSuchInterfaceException,
        IllegalBindingException, IllegalLifeCycleException {
      clientBC.bindFc(clientItfName, server);
    }
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  public void bindFc(final String clientItfName, final Object serverItf)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {
    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.equals(CLIENT_LOADER_NAME)) {
      clientLoaderItf = (TaskCompositionLoader) serverItf;
    } else {
      throw new NoSuchInterfaceException("There is no interface named '"
          + clientItfName + "'");
    }
  }

  public String[] listFc() {
    return new String[]{CLIENT_LOADER_NAME};
  }

  public Object lookupFc(final String clientItfName)
      throws NoSuchInterfaceException {
    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.equals(CLIENT_LOADER_NAME)) {
      return clientLoaderItf;
    } else {
      throw new NoSuchInterfaceException("There is no interface named '"
          + clientItfName + "'");
    }
  }

  public void unbindFc(final String clientItfName)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {
    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.equals(CLIENT_LOADER_NAME)) {
      clientLoaderItf = null;
    } else {
      throw new NoSuchInterfaceException("There is no interface named '"
          + clientItfName + "'");
    }
  }
}
