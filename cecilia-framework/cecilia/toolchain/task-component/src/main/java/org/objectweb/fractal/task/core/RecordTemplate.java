/**
 * Cecilia ADL Task Framework
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core;

/**
 * A RecordTemplate can be used when different {@link Record} must be
 * instantiated with the same <i>record descriptor</i> and different list of
 * arguments.
 */
public class RecordTemplate {

  final Record template = new Record();

  /**
   * Create a template of record for the given record descriptor.
   * 
   * @param recordDesc a record descriptor.
   */
  public RecordTemplate(final String recordDesc) {
    Record.parseRecordDesc(template, recordDesc, false,
        new Record.ArgumentList(new Object[0]));
  }

  /**
   * Instantiate a record by replacing the <code>"%"</code> field values of
   * the descriptor by the given arguments.
   * 
   * @param args the arguments of the record.
   * @return a new record object.
   */
  public Record instantiate(final Object... args) {
    return template.instantiateTemplate(args);
  }
}
