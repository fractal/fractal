/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.control;

import static java.lang.reflect.Modifier.isFinal;
import static java.lang.reflect.Modifier.isProtected;
import static java.lang.reflect.Modifier.isPublic;
import static java.lang.reflect.Modifier.isStatic;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.control.binding.TypeBindingMixin;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterfaceForEach;

/**
 * 
 */
public abstract class TaskContainerBindingControllerMixin
    implements
      Controller,
      TaskBindingController {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private TaskContainerBindingControllerMixin() {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overridden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * The map associating client interface name to field annotated by
   * {@link ClientInterface}.
   */
  private Map<String, Field>  fcAnnotatedClientItf;

  /**
   * The map associating client collection interface name to field annotated by
   * {@link ClientInterface}.
   */
  private Map<String, Field>  fcAnnotatedClientColItf;

  /**
   * The map associating client interface prefix to field annotated by
   * {@link ClientInterfaceForEach}.
   */
  private Map<String, Field>  fcAnnotatedMultipleClientItf;

  private Map<String, Object> fcAnnotatedMultipleClientItfMapping;

  /**
   * Initializes the fields of this mixin and then calls the overridden method.
   * 
   * @param ic information about the component to which this controller object
   *            belongs.
   * @throws InstantiationException if the initialization fails.
   */
  public void initFcController(final InitializationContext ic)
      throws InstantiationException {
    // call _super_initFcController first to ensure that _this_fcContent is
    // initialized
    _super_initFcController(ic);

    final ComponentType compType = (ComponentType) ic.type;
    final Class<?> contentClass = _this_fcContent.getClass();
    final String contentClassName = contentClass.getCanonicalName();

    // checks class modifiers: class must be protected or public
    if ((!isProtected(contentClass.getModifiers()))
        && (!isPublic(contentClass.getModifiers()))) {
      throw new InstantiationException("Class '" + contentClassName
          + "' must be either protected or public.");
    }

    for (final Field field : contentClass.getFields()) {

      final ClientInterface clientInterface = field
          .getAnnotation(ClientInterface.class);
      if (clientInterface != null) {
        // checks field modifiers: field must be public, non-static
        final int modifiers = field.getModifiers();
        if (!isPublic(modifiers)) {
          throw new InstantiationException("Field '" + contentClassName + '#'
              + field.getName()
              + "' is not public. @ClientInterface annotation is only allowed "
              + "for public, non-static fields.");
        }

        if (isStatic(modifiers)) {
          throw new InstantiationException("Field '" + contentClassName + '#'
              + field.getName()
              + "' is static. @ClientInterface annotation is only allowed "
              + "for public, non-static, non-final fields.");
        }

        final InterfaceType itfType;
        try {
          itfType = compType.getFcInterfaceType(clientInterface.name());
        } catch (final NoSuchInterfaceException e) {
          throw new InstantiationException("Annotated field '"
              + contentClassName + '#' + field.getName()
              + "' does not corresond to a correct client interface.");
        }

        if (itfType.isFcCollectionItf()) {
          // if field denotes a collection interface, it must be final, not null
          // and assignable to Map.
          if (!isFinal(modifiers)) {
            throw new InstantiationException("Field '" + contentClassName + '#'
                + field.getName()
                + "' is not final. @ClientInterface annotation for collection "
                + "interface is only allowed on final fields.");
          }

          if (!Map.class.isAssignableFrom(field.getType())) {
            throw new InstantiationException("Field '" + contentClassName + '#'
                + field.getName()
                + "' has an invalid type. @ClientInterface annotation for "
                + "collection interface is only allowed on Map fields.");
          }

          try {
            if (field.get(_this_fcContent) == null) {
              throw new InstantiationException("Field '" + contentClassName
                  + '#' + field.getName()
                  + "' is null. @ClientInterface annotation for "
                  + "collection interface is only allowed on initialized "
                  + "fields.");
            }
          } catch (final IllegalAccessException e) {
            final InstantiationException ie = new InstantiationException(
                "Unable to retrieve value of field '" + contentClassName + '#'
                    + field.getName() + "'.");
            ie.initCause(e);
            throw ie;
          }

        } else { // ! itfType.isFcCollectionItf()
          // if field denotes a singleton interface, it cannot be final and must
          // be assignable to the signature of the interface.
          if (isFinal(modifiers)) {
            throw new InstantiationException("Field '" + contentClassName + '#'
                + field.getName()
                + "' is final. @ClientInterface annotation for singleton "
                + "interface is not allowed on final fields.");
          }
          try {
            if (!field.getType().isAssignableFrom(
                Class.forName(itfType.getFcItfSignature()))) {
              throw new InstantiationException("Invalid type for field '"
                  + contentClassName + '#' + field.getName()
                  + "'. Not a super type of " + itfType.getFcItfSignature());
            }
          } catch (final ClassNotFoundException ignored) {
            // should not append;
          }

          // checks that the field is 'gettable' and 'settable'
          try {
            // try to get value of the field.
            final Object value = field.get(_this_fcContent);
            // try to set its value (has no side effect since reset its current
            // value. This supposes that there is no concurrent access).
            field.set(_this_fcContent, value);
          } catch (final IllegalAccessException e) {
            final InstantiationException ie = new InstantiationException(
                "Unable to retrieve value of field '" + contentClassName + '#'
                    + field.getName() + "'.");
            ie.initCause(e);
            throw ie;
          }

        }

        if (itfType.isFcCollectionItf()) {
          if (fcAnnotatedClientColItf == null)
            fcAnnotatedClientColItf = new HashMap<String, Field>();
          fcAnnotatedClientColItf.put(clientInterface.name(), field);
        } else {
          if (fcAnnotatedClientItf == null)
            fcAnnotatedClientItf = new HashMap<String, Field>();
          fcAnnotatedClientItf.put(clientInterface.name(), field);
        }
      }

      final ClientInterfaceForEach clientInterfaceForEach = field
          .getAnnotation(ClientInterfaceForEach.class);
      if (clientInterfaceForEach != null) {

        // checks field modifiers: field must be public, non-static
        final int modifiers = field.getModifiers();
        if (!isPublic(modifiers)) {
          throw new InstantiationException("Field '" + contentClassName + '#'
              + field.getName()
              + "' is not public. @ClientInterfaceForEach annotation is only "
              + "allowed for public, final, non-static fields.");
        }

        if (isStatic(modifiers)) {
          throw new InstantiationException("Field '" + contentClassName + '#'
              + field.getName()
              + "' is static. @ClientInterfaceForEach annotation is only "
              + "allowed for public, final, non-static fields.");
        }

        if (!isFinal(modifiers)) {
          throw new InstantiationException("Field '" + contentClassName + '#'
              + field.getName()
              + "' is not final. @ClientInterfaceForEach annotation is only "
              + "allowed for public, final, non-static fields.");
        }

        if (!Map.class.isAssignableFrom(field.getType())) {
          throw new InstantiationException("Field '" + contentClassName + '#'
              + field.getName()
              + "' has an invalid type. @ClientInterfaceforEach annotation is"
              + " only allowed on Map fields.");
        }

        try {
          if (field.get(_this_fcContent) == null) {
            throw new InstantiationException("Field '" + contentClassName + '#'
                + field.getName()
                + "' is null. @ClientInterfaceForEach annotation is only "
                + "allowed on initialized fields.");
          }
        } catch (final IllegalArgumentException e) {
          final InstantiationException ie = new InstantiationException(
              "Unable to retrieve value of field '" + contentClassName + '#'
                  + field.getName() + "'.");
          ie.initCause(e);
          throw ie;
        } catch (final IllegalAccessException e) {
          final InstantiationException ie = new InstantiationException(
              "Unable to retrieve value of field '" + contentClassName + '#'
                  + field.getName() + "'.");
          ie.initCause(e);
          throw ie;
        }

        for (final InterfaceType type : compType.getFcInterfaceTypes()) {
          if (type.getFcItfName().startsWith(clientInterfaceForEach.prefix())) {
            if (fcAnnotatedMultipleClientItf == null)
              fcAnnotatedMultipleClientItf = new HashMap<String, Field>();
            fcAnnotatedMultipleClientItf.put(type.getFcItfName(), field);
          }
        }
      }
    }
  }

  // implementation of the BindingController interface

  public void addFcInterfaceNameMapping(final Map<String, Object> itfNameMapping) {
    if (fcAnnotatedMultipleClientItfMapping == null)
      fcAnnotatedMultipleClientItfMapping = new HashMap<String, Object>();

    fcAnnotatedMultipleClientItfMapping.putAll(itfNameMapping);
  }

  public String[] listFc() {
    final String[] superItfs = _super_listFc();

    // Usually, there is only fcAnnotatedClientItf. optimize this case.
    if ((superItfs == null || superItfs.length == 0)
        && fcAnnotatedClientItf != null && fcAnnotatedClientColItf == null
        && fcAnnotatedMultipleClientItf == null) {
      return fcAnnotatedClientItf.keySet().toArray(
          new String[fcAnnotatedClientItf.size()]);
    } else {
      final List<String> clientItfNames = new ArrayList<String>();
      if (superItfs != null && superItfs.length != 0) {
        for (final String itfName : superItfs) {
          clientItfNames.add(itfName);
        }
      }

      if (fcAnnotatedClientItf != null)
        clientItfNames.addAll(fcAnnotatedClientItf.keySet());
      if (fcAnnotatedClientColItf != null)
        clientItfNames.addAll(fcAnnotatedClientColItf.keySet());
      if (fcAnnotatedMultipleClientItf != null)
        clientItfNames.addAll(fcAnnotatedMultipleClientItf.keySet());

      return clientItfNames.toArray(new String[clientItfNames.size()]);
    }
  }

  public Object lookupFc(final InterfaceType clientItfType,
      final String clientItfName) throws NoSuchInterfaceException {
    if (clientItfType.isFcCollectionItf()) {
      if (fcAnnotatedClientColItf != null) {
        for (final Map.Entry<String, Field> entry : fcAnnotatedClientColItf
            .entrySet()) {
          if (clientItfName.startsWith(entry.getKey())) {
            try {
              final Map itfs = (Map) entry.getValue().get(_this_fcContent);
              return itfs.get(clientItfName);
            } catch (final IllegalAccessException ignored) {
              // should not append
            }
          }
        }
      }
    } else {
      if (fcAnnotatedClientItf != null) {
        final Field field = fcAnnotatedClientItf.get(clientItfName);
        if (field != null) {
          try {
            return field.get(_this_fcContent);
          } catch (final IllegalAccessException ignored) {
            // should not append
          }
        }
      }

      if (fcAnnotatedMultipleClientItf != null) {
        final Field field = fcAnnotatedMultipleClientItf.get(clientItfName);
        if (field != null) {
          Object key = null;
          if (fcAnnotatedMultipleClientItfMapping != null)
            key = fcAnnotatedMultipleClientItfMapping.get(clientItfName);
          if (key == null) {
            // If the key is null, the client interface cannot be bound.
            return null;
          }
          try {
            return ((Map<?, ?>) field.get(_this_fcContent)).get(key);
          } catch (final IllegalAccessException ignored) {
            // should not append
          }
        }
      }
    }

    // client interface is not managed
    return _super_lookupFc(clientItfType, clientItfName);
  }

  public void bindFc(final InterfaceType clientItfType,
      final String clientItfName, final Object serverItf)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (clientItfType.isFcCollectionItf()) {
      if (fcAnnotatedClientColItf != null) {
        for (final Map.Entry<String, Field> entry : fcAnnotatedClientColItf
            .entrySet()) {
          if (clientItfName.startsWith(entry.getKey())) {
            try {
              ((Map) entry.getValue().get(_this_fcContent)).put(clientItfName,
                  serverItf);
              return;
            } catch (final IllegalAccessException ignored) {
              // should not append
            }
          }
        }
      }
    } else {
      if (fcAnnotatedClientItf != null) {
        final Field field = fcAnnotatedClientItf.get(clientItfName);
        if (field != null) {
          try {
            field.set(_this_fcContent, serverItf);
            return;
          } catch (final IllegalAccessException ignored) {
            // should not append
          }
        }
      }

      if (fcAnnotatedMultipleClientItf != null) {
        final Field field = fcAnnotatedMultipleClientItf.get(clientItfName);
        if (field != null) {
          Object key = null;
          if (fcAnnotatedMultipleClientItfMapping != null)
            key = fcAnnotatedMultipleClientItfMapping.get(clientItfName);

          if (key == null) {
            throw new IllegalBindingException(
                "Cannot find mapping for client interface '" + clientItfName
                    + "'.");
          }

          try {
            ((Map) field.get(_this_fcContent)).put(key, serverItf);
            return;
          } catch (final IllegalAccessException ignored) {
            ignored.printStackTrace();
            // should not append
          }
        }
      }
    }

    // client interface is not managed
    _super_bindFc(clientItfType, clientItfName, serverItf);
  }

  public void unbindFc(final InterfaceType clientItfType,
      final String clientItfName) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException {
    if (clientItfType.isFcCollectionItf()) {
      if (fcAnnotatedClientColItf != null) {
        for (final Map.Entry<String, Field> entry : fcAnnotatedClientColItf
            .entrySet()) {
          if (clientItfName.startsWith(entry.getKey())) {
            try {
              ((Map) entry.getValue().get(_this_fcContent))
                  .remove(clientItfName);
              return;
            } catch (final IllegalAccessException ignored) {
              // should not append
            }
          }
        }
      }
    } else {
      if (fcAnnotatedClientItf != null) {
        final Field field = fcAnnotatedClientItf.get(clientItfName);
        if (field != null) {
          try {
            field.set(_this_fcContent, null);
            return;
          } catch (final IllegalAccessException ignored) {
            // should not append
          }
        }
      }

      if (fcAnnotatedMultipleClientItf != null) {
        final Field field = fcAnnotatedMultipleClientItf.get(clientItfName);
        if (field != null) {
          Object key = null;
          if (fcAnnotatedMultipleClientItfMapping != null)
            key = fcAnnotatedMultipleClientItfMapping.get(clientItfName);

          if (key == null) {
            throw new IllegalBindingException(
                "Cannot find mapping for client interface '" + clientItfName
                    + "'.");
          }

          try {
            ((Map) field.get(_this_fcContent)).remove(key);
            return;
          } catch (final IllegalAccessException ignored) {
            // should not append
          }
        }
      }
    }

    // client interface is not managed
    _super_unbindFc(clientItfType, clientItfName);
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>fcContent</tt> field required by this mixin. This field is
   * supposed to store the "user component" encapsulated in this container
   * component.
   */
  public Object _this_fcContent;

  /**
   * @see Controller#initFcController initFcController
   */
  public abstract void _super_initFcController(InitializationContext ic)
      throws InstantiationException;

  /**
   * @see BindingController#listFc listFc
   */
  public abstract String[] _super_listFc();

  /**
   * @see TypeBindingMixin#lookupFc(InterfaceType, String)
   */
  public abstract Object _super_lookupFc(final InterfaceType clientItfType,
      final String clientItfName) throws NoSuchInterfaceException;

  /**
   * @see TypeBindingMixin#bindFc(InterfaceType,String,Object)
   */
  public abstract void _super_bindFc(InterfaceType clientItfType,
      String clientItfName, Object serverItf) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException;

  /**
   * @see TypeBindingMixin#unbindFc(InterfaceType,String) unbindFc
   */
  public abstract void _super_unbindFc(InterfaceType clientItfType,
      String clientItfName) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException;

}
