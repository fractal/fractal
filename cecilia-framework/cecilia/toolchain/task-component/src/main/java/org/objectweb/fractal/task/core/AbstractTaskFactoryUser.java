/**
 * 
 */

package org.objectweb.fractal.task.core;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

/**
 * Abstract class that can be overridden by component implementations that
 * requires a {@link TaskFactory}.
 */
public abstract class AbstractTaskFactoryUser implements BindingController {

  // ---------------------------------------------------------------------------
  // Client interfaces
  // ---------------------------------------------------------------------------

  /** The default name of the {@link TaskFactory} client interface. */
  public static final String TASK_FACTORY_ITF_NAME = "task-factory";

  /** The name of the {@link TaskFactory} client interface. */
  private final String       taskFactoryItfName;

  /** The {@link TaskFactory} client interface. */
  public TaskFactory         taskFactoryItf;

  // ---------------------------------------------------------------------------
  // Constructors
  // ---------------------------------------------------------------------------

  /**
   * Default constructor.
   */
  public AbstractTaskFactoryUser() {
    this(TASK_FACTORY_ITF_NAME);
  }

  /**
   * This constructor can be used by the default constructor of sub-classes to
   * specify the name of the {@link TaskFactory} client interface.
   * 
   * @param taskFactoryItfName the name of the {@link TaskFactory} client
   *            interface.
   */
  protected AbstractTaskFactoryUser(final String taskFactoryItfName) {
    this.taskFactoryItfName = taskFactoryItfName;
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  public String[] listFc() {
    return new String[]{taskFactoryItfName};
  }

  public void bindFc(final String clientItfName, final Object serverItf)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (taskFactoryItfName.equals(clientItfName))
      taskFactoryItf = (TaskFactory) serverItf;
    else
      throw new NoSuchInterfaceException("There is no interface named '"
          + clientItfName + "'");
  }

  public Object lookupFc(final String clientItfName)
      throws NoSuchInterfaceException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (taskFactoryItfName.equals(clientItfName))
      return taskFactoryItf;
    else
      throw new NoSuchInterfaceException("There is no interface named '"
          + clientItfName + "'");
  }

  public void unbindFc(final String clientItfName)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (taskFactoryItfName.equals(clientItfName))
      taskFactoryItf = null;
    else
      throw new NoSuchInterfaceException("There is no interface named '"
          + clientItfName + "'");
  }
}
