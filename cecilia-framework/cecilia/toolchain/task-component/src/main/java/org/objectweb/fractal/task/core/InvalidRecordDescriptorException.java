/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core;

/**
 * Runtime exception thrown by {@link Record#Record(String, Object...)} if the
 * given record descriptor is invalid.
 */
public class InvalidRecordDescriptorException extends RuntimeException {

  private static String getErrorMessage(final String recordDesc) {
    if (recordDesc != null)
      return "Invalid record descriptor \"" + recordDesc + "\".";
    else
      return "Invalid record descriptor.";
  }

  /**
   * Create a new <code>InvalidRecordDescriptorException</code>.
   * 
   * @param recordDesc the faulty record descriptor.
   */
  public InvalidRecordDescriptorException(final String recordDesc) {
    super(getErrorMessage(recordDesc));
  }

  /**
   * Create a new <code>InvalidRecordDescriptorException</code>.
   * 
   * @param recordDesc the faulty record descriptor.
   * @param message a detail message.
   * @param cause the cause of this exception.
   */
  public InvalidRecordDescriptorException(final String recordDesc,
      final String message, final Throwable cause) {
    super(getErrorMessage(recordDesc) + " Reason: " + message, cause);
  }

  /**
   * Create a new <code>InvalidRecordDescriptorException</code>.
   * 
   * @param recordDesc the faulty record descriptor.
   * @param message a detail message.
   */
  public InvalidRecordDescriptorException(final String recordDesc,
      final String message) {
    super(getErrorMessage(recordDesc) + " Reason: " + message);
  }

  /**
   * Create a new <code>InvalidRecordDescriptorException</code>.
   * 
   * @param recordDesc the faulty record descriptor.
   * @param cause the cause of this exception.
   */
  public InvalidRecordDescriptorException(final String recordDesc,
      final Throwable cause) {
    super(getErrorMessage(recordDesc), cause);
  }

}
