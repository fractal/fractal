/**
 * 
 */

package org.objectweb.fractal.task.core.primitive;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.task.core.TaskException;

public interface PrimitiveTaskFactory {

  /**
   * Creates a primitive task component.
   * 
   * @param implementation the implementation of the task.
   * @param parameters the parameters of the task.
   * @return a task component.
   * @throws TaskException if an error occurs while instantiating the task.
   */
  Component newPrimitiveTask(final Object implementation,
      final Object... parameters) throws TaskException;
}
