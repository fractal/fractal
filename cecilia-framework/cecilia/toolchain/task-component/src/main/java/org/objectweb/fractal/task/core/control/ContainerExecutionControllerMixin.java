/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.control;

import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.task.core.Executable;

public abstract class ContainerExecutionControllerMixin
    implements
      ExecutionController,
      Controller {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private ContainerExecutionControllerMixin() {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overridden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * The Executable interface of the "user component" encapsulated in this
   * container component.
   */
  public Executable fcExecutableContent;

  /**
   * Boolean that indicates if the task has already been executed.
   */
  public boolean    fcHasBeenExecuted;

  public void initFcController(final InitializationContext ic)
      throws InstantiationException {
    if (ic.content == this) {
      // case of merge...AndContent options
      throw new InstantiationException(
          "This mixin does not support merging of membrane and content");
    }

    if (ic.content instanceof Executable) {
      fcExecutableContent = (Executable) ic.content;
      fcHasBeenExecuted = false;
    } else {
      // Task implementation does not implement execute method.
      fcHasBeenExecuted = true;
    }
    _super_initFcController(ic);
  }

  public synchronized void execute() throws Exception {
    if (fcHasBeenExecuted) return;

    try {
      fcExecutableContent.execute();
    } finally {
      fcHasBeenExecuted = true;
    }
  }

  public boolean isFcExecuted() {
    return fcHasBeenExecuted;
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * @see Controller#initFcController
   */
  public abstract void _super_initFcController(InitializationContext ic)
      throws InstantiationException;

}
