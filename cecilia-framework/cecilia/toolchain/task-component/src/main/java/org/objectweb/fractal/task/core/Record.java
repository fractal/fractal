/**
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

/**
 * A record is a immutable data structure that contains a (non ordered) set of
 * <i>field</i>. Each field has a <i>name</i> and a <i>value</i>. The name of
 * the field is a <code>String</code>. The value of the field can be any
 * <code>Object</code>, in particular it can be a nested <code>Record</code>
 * (note: cycle in record containment is not allows but, for efficiency reason,
 * this implementation does not check this). <br>
 * A record can be created from a <i>record descriptor</i> and a set of
 * arguments (see {@link #Record(String, Object...)}). A record descriptor is a
 * <code>String</code> of the form:
 * 
 * <pre>
 * &quot;{&quot; &lt;field_desc&gt; ( &quot;,&quot; &lt;field_desc&gt; )* &quot;}&quot;
 * </pre>
 * 
 * where a <code><i>&lt;field_desc&gt;</i></code> is of the form:
 * 
 * <pre>
 * &lt;field_name&gt; &quot;:&quot; &lt;field_value&gt;
 * </pre>
 * 
 * A <code><i>&lt;field_name&gt;</i></code> must match the
 * {@link Pattern pattern}: <code>[a-zA-Z]([a-zA-Z_0-9])*</code>. <br>
 * A <code><i>&lt;field_value&gt;</i></code> can be :
 * <ul>
 * <li> character sequence without <code>'}'</code>, <code>':'</code> and
 * <code>','</code> (the actual character sequence that is captured as the
 * field's value starts with the first non whitespace character immediately
 * after the ':' and ends with the last non whitespace character immediately
 * before the ',')</li>
 * <li> a single '%' character. In that case, the actual value of field is taken
 * from the list of argument of the {@link #Record(String, Object...)}
 * constructor.</li>
 * <li> a nested record descriptor.</li>
 * </ul>
 * <b>Note</b>: the outermost braces are not required.
 * <h4>Examples:</h4>
 * 
 * <pre>
 * {n1: foo, n2: bar}
 * </pre>
 * 
 * describe a record that contains two fields: one with the name
 * <code>"n1"</code> and the value <code>"foo"</code>; the other one with
 * the name <code>"n2"</code> and the value <code>"bar"</code>.
 */
public final class Record {

  private static final Pattern NAME_PATTERN   = Pattern
                                                  .compile("[a-zA-Z](\\w)*");
  final Map<String, Object>    fields;
  private FieldsIterable       fieldsIterable = null;

  Record() {
    fields = new HashMap<String, Object>();
  }

  /**
   * Creates a Record with the given content.
   * 
   * @param content a map associating field names to field values.
   */
  public Record(final Map<String, Object> content) {
    fields = new HashMap<String, Object>(content);
  }

  /**
   * Creates a Record from a record description (see {@link Record} class
   * description for details).
   * 
   * @param recordDesc the record descriptor.
   * @param args the actual values of the fields whose values are
   *            <code>"%"</code> in the descriptor.
   */
  public Record(final String recordDesc, final Object... args) {
    this();
    if (recordDesc == null)
      throw new IllegalArgumentException("recordDesc cannot be null");

    parseRecordDesc(this, recordDesc, true, new ArgumentList(args));
  }

  /**
   * Creates a Record from a record description (see {@link Record} class
   * description for details).
   * 
   * @param recordDesc the record descriptor.
   * @param list a list containing the actual values of the fields whose values
   *            are <code>"%"</code> in the descriptor.
   */
  public Record(final String recordDesc, final ArgumentList list) {
    this();
    if (recordDesc == null)
      throw new IllegalArgumentException("recordDesc cannot be null");

    parseRecordDesc(this, recordDesc, true, list);
  }

  /**
   * Returns <code>true</code> if this record contains a field with the given
   * name.
   * 
   * @param fieldName the name of the field to test.
   * @return <code>true</code> if this record contains a field with the given
   *         name.
   */
  public boolean containsField(final String fieldName) {
    return fields.containsKey(fieldName);
  }

  /**
   * Returns the value of the field with the given name.
   * 
   * @param fieldName the name of the field to return.
   * @return the value of the field whose name is <code>fieldName</code>.
   */
  public Object get(final String fieldName) {
    return fields.get(fieldName);
  }

  /**
   * Returns the number of field this record contains.
   * 
   * @return the number of field this record contains.
   */
  public int getFieldCount() {
    return fields.size();
  }

  /**
   * Returns an {@link Iterable} that can be used to iterate over the fields of
   * this record.
   * 
   * @return an {@link Iterable} of {@link Field}.
   */
  public Iterable<Field> fields() {
    if (fieldsIterable == null) {
      fieldsIterable = new FieldsIterable();
    }
    return fieldsIterable;
  }

  /**
   * A record field.
   */
  public interface Field {

    /**
     * Returns the name of the fields.
     * 
     * @return the name of the fields.
     */
    String getName();

    /**
     * Returns the value of the fields.
     * 
     * @return the value of the fields.
     */
    Object getValue();
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof Record) {
      return fields.equals(((Record) obj).fields);
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    return fields.hashCode();
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append('{');
    final Iterator<Map.Entry<String, Object>> fieldIterator = fields.entrySet()
        .iterator();
    if (fieldIterator.hasNext()) {
      final Entry<String, Object> field = fieldIterator.next();
      sb.append(field.getKey()).append(':').append(field.getValue());
    }
    while (fieldIterator.hasNext()) {
      final Entry<String, Object> field = fieldIterator.next();
      sb.append(", ").append(field.getKey()).append(':').append(
          field.getValue());
    }
    sb.append('}');
    return sb.toString();
  }

  static String parseRecordDesc(final Record rec, String recordDesc,
      final boolean replaceRefs, final ArgumentList args) {
    // first remove trailing spaces.
    recordDesc = recordDesc.trim();

    // recordDesc can be surrounded by { }
    final boolean expectClosingBrace;
    boolean endOfRecordReached = false;
    if (recordDesc.charAt(0) == '{') {
      expectClosingBrace = true;
      recordDesc = recordDesc.substring(1);
    } else {
      expectClosingBrace = false;
    }

    int semiIdx = recordDesc.indexOf(':');
    while ((!endOfRecordReached) && semiIdx != -1) {
      final String fieldName = recordDesc.substring(0, semiIdx).trim();
      if (fieldName.length() == 0) {
        throw new InvalidRecordDescriptorException(recordDesc);
      }
      if (!NAME_PATTERN.matcher(fieldName).matches()) {
        throw new InvalidRecordDescriptorException(recordDesc,
            "Invalid field name: \"" + fieldName + "\".");
      }

      final Object fieldValue;
      recordDesc = recordDesc.substring(semiIdx + 1).trim();
      if (recordDesc.charAt(0) == '{') {
        // nested record.
        final Record nestedRec = new Record();
        recordDesc = parseRecordDesc(nestedRec, recordDesc, replaceRefs, args)
            .trim();
        fieldValue = nestedRec;
        if (recordDesc.length() > 0 && recordDesc.charAt(0) == ',') {
          recordDesc = recordDesc.substring(1).trim();
        }

      } else {
        int valueEndIdx = recordDesc.indexOf(',');
        if (expectClosingBrace) {
          // expect a closing brace.
          if (valueEndIdx == -1) {
            // no more comma. this field is the last one so I must find a '}'
            valueEndIdx = recordDesc.indexOf('}');
            if (valueEndIdx != -1) {
              endOfRecordReached = true;
            } else {
              throw new InvalidRecordDescriptorException(recordDesc,
                  "'}' expected.");
            }
          } else {
            final int closingBrace = recordDesc.indexOf('}');
            if (closingBrace >= 0 && closingBrace < valueEndIdx) {
              // closing brace is before next comma. this field is the last one
              valueEndIdx = closingBrace;
              endOfRecordReached = true;
            }
          }
        }

        final String value;
        if (valueEndIdx == -1) {
          value = recordDesc;
          recordDesc = "";
          endOfRecordReached = true;
        } else {
          value = recordDesc.substring(0, valueEndIdx).trim();
          recordDesc = recordDesc.substring(valueEndIdx + 1).trim();
        }

        if (value.equals("%")) {
          if (replaceRefs) {
            if (args.index >= args.arguments.length) {
              throw new InvalidRecordDescriptorException(recordDesc,
                  "Not enougth parameters");
            }
            fieldValue = args.arguments[args.index];
          } else {
            fieldValue = new ValueReference(args.index);
          }
          args.index++;
        } else {
          fieldValue = value;
        }
      }

      rec.fields.put(fieldName, fieldValue);

      semiIdx = recordDesc.indexOf(':');
    }
    return recordDesc;
  }

  Record instantiateTemplate(final Object... args) {
    final Record instance = new Record();
    for (final Map.Entry<String, Object> entry : fields.entrySet()) {
      Object val = entry.getValue();
      if (val instanceof Record) {
        val = ((Record) val).instantiateTemplate(args);
      } else if (val instanceof ValueReference) {
        final ValueReference ref = (ValueReference) val;
        if (ref.index >= args.length)
          throw new InvalidRecordDescriptorException(null,
              "Not enougth parameters");
        val = args[ref.index];
      }
      instance.fields.put(entry.getKey(), val);
    }
    return instance;
  }

  private final class FieldsIterable implements Iterable<Field> {

    public Iterator<Field> iterator() {
      return new FieldsIterator();
    }
  }

  private final class FieldsIterator implements Iterator<Field> {

    private final Iterator<Map.Entry<String, Object>> fieldsIterator;

    private FieldsIterator() {
      fieldsIterator = fields.entrySet().iterator();
    }

    public boolean hasNext() {
      return fieldsIterator.hasNext();
    }

    public Field next() {
      return new FieldEntry(fieldsIterator.next());
    }

    public void remove() {
      throw new UnsupportedOperationException();
    }
  }

  private class FieldEntry implements Field {
    private final Map.Entry<String, Object> entry;

    FieldEntry(final Map.Entry<String, Object> entry) {
      this.entry = entry;
    }

    public String getName() {
      return entry.getKey();
    }

    public Object getValue() {
      return entry.getValue();
    }

    @Override
    public boolean equals(final Object obj) {
      if (obj instanceof Field) {
        final Field candidate = (Field) obj;
        return getName().equals(candidate.getName())
            && getValue().equals(candidate.getValue());
      } else {
        return false;
      }
    }

    @Override
    public int hashCode() {
      return entry.hashCode();
    }

    @Override
    public String toString() {
      return getName() + ':' + getValue();
    }
  }

  static final class ValueReference {
    final int index;

    ValueReference(final int index) {
      this.index = index;
    }
  }

  public static final class ArgumentList {
    int            index;
    final Object[] arguments;

    public ArgumentList(final Object[] arguments) {
      this.arguments = arguments;
      index = 0;
    }
  }
}
