/***
 * Fractal ADL Task Framework Parser
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Ali Erdem Ozcan
 */

package org.objectweb.fractal.task.core.composition;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.composition.ast.Binding;
import org.objectweb.fractal.task.core.composition.ast.Export;
import org.objectweb.fractal.task.core.composition.ast.FunctionCall;
import org.objectweb.fractal.task.core.composition.ast.Instruction;
import org.objectweb.fractal.task.core.composition.ast.RecordField;
import org.objectweb.fractal.task.core.composition.ast.Task;
import org.objectweb.fractal.task.core.composition.ast.TaskDefinition;
import org.objectweb.fractal.task.core.composition.ast.ValueContainer;
import org.objectweb.fractal.task.core.composition.ast.Variable;

/**
 * This loader class verifies the conformity between the variables that are
 * declared in the parameters or in the declarative part of the task composition
 * rules and the variables that are used in the predicates or exported records.
 */
public class VariableLoader extends AbstractTaskCompositionLoader {

  // ---------------------------------------------------------------------------
  // Implementation of the TaskCompositionLoader interface
  // ---------------------------------------------------------------------------

  public TaskDefinition load(final String name,
      final Map<Object, Object> context) throws TaskException {
    final TaskDefinition d = clientLoader.load(name, context);
    checkVariableDeclarations(d);
    return d;
  }

  private void checkVariableDeclarations(final TaskDefinition d)
      throws TaskException {
    final Map<String, Variable> taskVariables = new HashMap<String, Variable>();
    final Task task = d.getTask();
    for (final Variable var : task.getVariables()) {
      checkVariableDeclaration(var, taskVariables);
    }

    for (final Instruction instruction : task.getInstructions()) {
      if (instruction.getExport() != null)
        checkExport(instruction.getExport(), taskVariables);
      else
        checkBinding(instruction.getBinding(), taskVariables);
    }
  }

  private void checkBinding(final Binding binding,
      final Map<String, Variable> taskVariables) throws TaskException {
    final Map<String, Variable> bindingVariables = new HashMap<String, Variable>(
        taskVariables);

    // Check variables declared in clientRecord.
    for (final RecordField recordField : binding.getClientRecord()
        .getRecordFields()) {
      checkPatternField(recordField, taskVariables, bindingVariables);
    }

    // Check variables declared in serverRecord.
    for (final RecordField recordField : binding.getServerRecord()
        .getRecordFields()) {
      checkPatternField(recordField, taskVariables, bindingVariables);
    }

    // Check 'where' predicates
    checkVariableReferences((ValueContainer) binding, bindingVariables);
  }

  private void checkExport(final Export export,
      final Map<String, Variable> taskVariables) throws TaskException {
    // Check variables declared in internalRecord.
    final Map<String, Variable> exportVariables = new HashMap<String, Variable>(
        taskVariables);
    for (final RecordField recordField : export.getInternalRecord()
        .getRecordFields()) {
      checkPatternField(recordField, taskVariables, exportVariables);
    }

    // Check external record fields
    if (export.getExternalRecord() != null)
      for (final RecordField recordField : export.getExternalRecord()
          .getRecordFields()) {
        checkVariableReferences(recordField, exportVariables);
      }

    // Check 'where' predicates
    checkVariableReferences((ValueContainer) export.getInternalRecord(),
        exportVariables);
  }

  private void checkPatternField(final RecordField recordField,
      final Map<String, Variable> taskVariables,
      final Map<String, Variable> localVariables) throws TaskException {
    final Variable variable = recordField.getVariable();
    if (variable != null && !taskVariables.containsKey(variable.getName())) {
      // value of record pattern field is a variable.
      // this variable is not declared as task variable.
      // so it defines a new variable.
      checkVariableDeclaration(variable, localVariables);
    } else {
      // record field is either a function call, a reference to a task variable
      // or a constant. checks variable references (only variables declared at
      // task level are accessible)
      checkVariableReferences(recordField, taskVariables);
    }
  }

  private void checkVariableDeclaration(final Variable var,
      final Map<String, Variable> declaredVariables) throws TaskException {
    final Variable previousDeclaration = declaredVariables.put(var.getName(),
        var);
    if (previousDeclaration != null) {
      throw new TaskException("Invalid variable '" + var.getName()
          + "'. A variable with the same name has already been declared at "
          + previousDeclaration.astGetSource(), var);
    }
  }

  private void checkVariableReferences(final ValueContainer container,
      final Map<String, Variable> declaredVariables) throws TaskException {
    if (container.getVariable() != null)
      checkVariableReferences(container.getVariable(), declaredVariables);
    else if (container.getFunction() != null)
      checkVariableReferences(container.getFunction(), declaredVariables);
    // else container contains a constant.
  }

  private void checkVariableReferences(final Variable variable,
      final Map<String, Variable> declaredVariables) throws TaskException {
    final Variable declaration = declaredVariables.get(variable.getName());
    if (declaration == null) {
      throw new TaskException("Unknown variable '" + variable.getName() + "'.",
          variable);
    }
    variable.astSetDecoration("declaration", declaration);
  }

  private void checkVariableReferences(final FunctionCall functionCall,
      final Map<String, Variable> declaredVariables) throws TaskException {
    for (final ValueContainer parameter : functionCall.getFunctionParameters()) {
      checkVariableReferences(parameter, declaredVariables);
    }
  }
}
