/**
 * 
 */

package org.objectweb.fractal.task.core;

import org.objectweb.fractal.task.core.composition.CompositeTaskFactory;
import org.objectweb.fractal.task.core.primitive.PrimitiveTaskFactory;

public interface TaskFactory extends CompositeTaskFactory, PrimitiveTaskFactory {

  /**
   * The name of the generic composition schema that exports every interfaces of
   * the sub tasks. The constant can be used as argument
   * <code>compositionName</code> of the
   * {@link CompositeTaskFactory#newCompositeTask() newCompositeTask} method.
   */
  String EXPORT_ALL = "org.objectweb.fractal.task.core.ExportAll";
}
