/**
 * 
 */

package org.objectweb.fractal.task.core.internal;

import java.util.Collection;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.TaskFactory;
import org.objectweb.fractal.task.core.composition.CompositeTaskFactory;
import org.objectweb.fractal.task.core.primitive.PrimitiveTaskFactory;

/**
 * Basic {@link TaskFactory} component. This component simply delegates requests
 * to its {@link #compositeTaskFactoryItf} client interface or its
 * {@link #primitiveTaskFactoryItf} client interface.
 */
public class BasicTaskFactory implements TaskFactory, BindingController {

  // ---------------------------------------------------------------------------
  // Client interfaces
  // ---------------------------------------------------------------------------

  /**
   * The name of the client interface bound to the {@link CompositeTaskFactory}.
   */
  public static final String  COMPOSITE_TASK_FACTORY_ITF_NAME = "composite-task-factory";

  /**
   * The client interface bound to the {@link CompositeTaskFactory}.
   */
  public CompositeTaskFactory compositeTaskFactoryItf;

  /**
   * The name of the client interface bound to the {@link PrimitiveTaskFactory}.
   */
  public static final String  PRIMITIVE_TASK_FACTORY_ITF_NAME = "primitive-task-factory";

  /**
   * The client interface bound to the {@link PrimitiveTaskFactory}.
   */
  public PrimitiveTaskFactory primitiveTaskFactoryItf;

  // ---------------------------------------------------------------------------
  // Implementation of the TaskFactory interface
  // ---------------------------------------------------------------------------

  public Component newCompositeTask(final Collection<Component> subTasks,
      final String compositionName, final Map<Object, Object> context,
      final Object... parameters) throws TaskException {
    return compositeTaskFactoryItf.newCompositeTask(subTasks, compositionName,
        context, parameters);
  }

  public Component newCompositeTask(final Collection<Component> subTasks,
      final String compositionName, final String compositeName,
      final Map<Object, Object> context, final Object... parameters)
      throws TaskException {
    return compositeTaskFactoryItf.newCompositeTask(subTasks, compositionName,
        compositeName, context, parameters);
  }

  public Component newPrimitiveTask(final Object implementation,
      final Object... parameters) throws TaskException {
    return primitiveTaskFactoryItf.newPrimitiveTask(implementation, parameters);
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  public String[] listFc() {
    return new String[]{COMPOSITE_TASK_FACTORY_ITF_NAME,
        PRIMITIVE_TASK_FACTORY_ITF_NAME};
  }

  public void bindFc(final String clientItfName, final Object serverItf)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {
    if (COMPOSITE_TASK_FACTORY_ITF_NAME.equals(clientItfName))
      compositeTaskFactoryItf = (CompositeTaskFactory) serverItf;
    else if (PRIMITIVE_TASK_FACTORY_ITF_NAME.equals(clientItfName))
      primitiveTaskFactoryItf = (PrimitiveTaskFactory) serverItf;
  }

  public Object lookupFc(final String clientItfName)
      throws NoSuchInterfaceException {
    if (COMPOSITE_TASK_FACTORY_ITF_NAME.equals(clientItfName))
      return compositeTaskFactoryItf;
    else if (PRIMITIVE_TASK_FACTORY_ITF_NAME.equals(clientItfName))
      return primitiveTaskFactoryItf;
    else
      return null;
  }

  public void unbindFc(final String clientItfName)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {
    if (COMPOSITE_TASK_FACTORY_ITF_NAME.equals(clientItfName))
      compositeTaskFactoryItf = null;
    else if (PRIMITIVE_TASK_FACTORY_ITF_NAME.equals(clientItfName))
      primitiveTaskFactoryItf = null;
  }

}
