/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.composition.function;

import org.objectweb.fractal.task.core.composition.Function;
import org.objectweb.fractal.task.core.composition.FunctionExecutionException;
import org.objectweb.fractal.task.core.composition.InvalidNumberOfParameterException;
import org.objectweb.fractal.task.core.composition.InvalidParameterTypeException;
import org.objectweb.fractal.task.core.composition.NullParameterException;

/**
 * Implements a boolean Not function. This function takes at one {@link Boolean}
 * parameter and returns a {@link Boolean}.
 */
public class Not implements Function {

  public Object invoke(final Object... params)
      throws FunctionExecutionException {
    if (params.length != 1) {
      throw new InvalidNumberOfParameterException(this, 1, params.length);
    }
    Object p = params[0];
    if (!(p instanceof Boolean)) {
      if (p == null)
        throw new NullParameterException(this, 0);
      else
        throw new InvalidParameterTypeException(this, 0, Boolean.class,
            p.getClass());
    }
    return !((Boolean) p);
  }
}
