/***
 * Fractal ADL Task Framework Parser
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Ali Erdem Ozcan
 */

package org.objectweb.fractal.task.core.primitive;

import static java.lang.reflect.Modifier.isPublic;
import static org.objectweb.fractal.api.type.TypeFactory.CLIENT;
import static org.objectweb.fractal.api.type.TypeFactory.COLLECTION;
import static org.objectweb.fractal.api.type.TypeFactory.MANDATORY;
import static org.objectweb.fractal.api.type.TypeFactory.SERVER;
import static org.objectweb.fractal.api.type.TypeFactory.SINGLE;
import static org.objectweb.fractal.task.core.internal.TaskUtil.itf;
import static org.objectweb.fractal.task.core.internal.TaskUtil.newTask;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.task.core.Record;
import org.objectweb.fractal.task.core.RecordTemplate;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.control.TaskBindingController;
import org.objectweb.fractal.task.core.internal.TaskUtil.TaskInterfaceDesc;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterfaceForEach;
import org.objectweb.fractal.task.core.primitive.annotations.Debug;
import org.objectweb.fractal.task.core.primitive.annotations.DefaultSignature;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;
import org.objectweb.fractal.util.Fractal;

public class BasicPrimitiveTaskFactory implements PrimitiveTaskFactory {

  private static final Map<Class<?>, TaskTemplate> CACHE = new HashMap<Class<?>, TaskTemplate>();

  public Component newPrimitiveTask(final Object implementation,
      final Object... args) throws TaskException {
    final Class<?> implementationClass = implementation.getClass();
    TaskTemplate template = CACHE.get(implementationClass);
    if (template == null) {
      template = new TaskTemplate(implementationClass);
      CACHE.put(implementationClass, template);
    }
    return template.instantiate(implementation, args);
  }

  private static final class TaskTemplate {

    final boolean            debug;
    final boolean            debugBinding;
    final String             taskClassName;
    final String[]           taskParameterNames;
    final List<ItfTemplate>  itfs = new ArrayList<ItfTemplate>();
    List<ItfForEachTemplate> forEachItfs;

    TaskTemplate(final Class<?> implementationClass) throws TaskException {
      final Debug debugAnnotation = implementationClass
          .getAnnotation(Debug.class);
      if (debugAnnotation != null) {
        debug = contains(debugAnnotation.value(), Debug.INSTANTIATION);
        debugBinding = contains(debugAnnotation.value(), Debug.BINDING);
      } else {
        debug = false;
        debugBinding = false;
      }

      taskClassName = implementationClass.getSimpleName();

      final String[] parameters = checkClassHierarchy(implementationClass);
      if (parameters == null)
        taskParameterNames = new String[0];
      else
        taskParameterNames = parameters;

      final HashMap<String, ServerInterface> sItfs = new HashMap<String, ServerInterface>();
      processServerInterfaceAnnotation(implementationClass, sItfs);
      for (final ServerInterface serverInterface : sItfs.values()) {
        itfs.add(new ItfTemplate(serverInterface, taskParameterNames, debug));
      }

      processClientInterfaceAnnotation(implementationClass, taskParameterNames);
    }

    String[] checkClassHierarchy(final Class<?> clazz) throws TaskException {

      // get task parameters (if any)
      String[] parameters = null;
      final TaskParameters taskParameters = clazz
          .getAnnotation(TaskParameters.class);
      if (taskParameters != null) {
        parameters = taskParameters.value();
      }

      // process super class.
      final Class<?> superClass = clazz.getSuperclass();
      if (superClass != null) {
        final String[] superParameters = checkClassHierarchy(superClass);
        if (superParameters != null) {
          // super class defines parameters.
          if (parameters == null) {
            // clazz does not define parameter, inherits super ones.
            parameters = superParameters;
          } else {
            // check that clazz parameters contains at least super ones.
            for (final String superParameter : superParameters) {
              if (!contains(parameters, superParameter)) {
                throw new TaskException("Task parameters of class "
                    + clazz.getCanonicalName()
                    + " must contains the inherited parameter '"
                    + superParameter + "'.");
              }
            }
          }
        }
      }

      // checks that the class does not declared a non public annotated field
      for (final Field field : clazz.getDeclaredFields()) {
        if (!isPublic(field.getModifiers())
            && (field.getAnnotation(ClientInterface.class) != null || field
                .getAnnotation(ClientInterfaceForEach.class) != null)) {
          // the field is not public and defines a ClientInterface or
          // ClientInterfaceForEach annotation.
          throw new TaskException("Field " + clazz.getCanonicalName() + '#'
              + field.getName() + " must be public.");
        }
      }

      return parameters;
    }

    void processServerInterfaceAnnotation(final Class<?> clazz,
        final Map<String, ServerInterface> serverItfDeclarator)
        throws TaskException {
      final Class<?> superClass = clazz.getSuperclass();
      if (superClass != null) {
        processServerInterfaceAnnotation(superClass, serverItfDeclarator);
      }

      final ServerInterfaces serverInterfaces = clazz
          .getAnnotation(ServerInterfaces.class);
      if (serverInterfaces != null) {
        for (final ServerInterface serverInterface : serverInterfaces.value()) {
          final ServerInterface previousDeclarator = serverItfDeclarator.put(
              serverInterface.name(), serverInterface);
          if (previousDeclarator != null) {
            // the serverInterface annotation overrides one defines in a super
            // class. check signature compatibility.
            if (!previousDeclarator.signature().isAssignableFrom(
                serverInterface.signature())) {
              throw new TaskException("Invalid \"" + serverInterface.name()
                  + "\" @ServerInterface annotation in class '" + clazz
                  + "' an interface with the same name is defined in a super "
                  + "class but with an incompatible signature.");
            }
          }
        }
      }
    }

    void processClientInterfaceAnnotation(final Class<?> clazz,
        final String[] parameters) throws TaskException {
      final Map<String, Field> cItfDeclarator = new HashMap<String, Field>();
      final Map<String, Field> cItfForEachDeclarator = new HashMap<String, Field>();

      for (final Field field : clazz.getFields()) {
        final ClientInterface cItf = field.getAnnotation(ClientInterface.class);
        final ClientInterfaceForEach cItfForEach = field
            .getAnnotation(ClientInterfaceForEach.class);

        // checks that both annotations are not defined at the same time
        if (cItf != null && cItfForEach != null) {
          throw new TaskException("Invalid field '"
              + field.getDeclaringClass().getName() + '.' + field.getName()
              + "' Cannot be annotated by both @ClientInterface and "
              + "@ClientInterfaceForEach.");
        }

        if (cItf != null) {
          final Field previousDeclarator = cItfDeclarator.put(cItf.name(),
              field);
          if (previousDeclarator != null) {
            throw new TaskException(
                "Invalid @ClientInterface annotation in field '"
                    + field.getDeclaringClass().getName() + '.'
                    + field.getName()
                    + "' an interface with the same name is defined by field '"
                    + previousDeclarator.getDeclaringClass().getName() + '.'
                    + previousDeclarator.getName() + "'.");
          }

          if (isCollectionInterface(field)) {
            // checks that signature is set
            if (cItf.signature().equals(DefaultSignature.class))
              throw new TaskException(
                  "Invalid @ClientInterface annotation in field '"
                      + field.getDeclaringClass().getName() + '.'
                      + field.getName()
                      + "' signature must be set for collection client "
                      + "interfaces.");

            // checks that the signature designate an interface.
            if (!cItf.signature().isInterface())
              throw new TaskException(
                  "Invalid @ClientInterface annotation in field '"
                      + field.getDeclaringClass().getName() + '.'
                      + field.getName()
                      + "': the signature class does not represent an "
                      + "interface type.");

            // add ItfTemplate
            itfs.add(new ItfTemplate(cItf, cItf.signature().getName(),
                COLLECTION, cItf.contingency(), parameters, debug));

          } else {
            final String signature;
            if (cItf.signature() == DefaultSignature.class) {
              // checks that the field type is an interface
              if (!field.getType().isInterface())
                throw new TaskException(
                    "Invalid @ClientInterface annotated field '"
                        + field.getDeclaringClass().getName() + '.'
                        + field.getName()
                        + "': field type is not an interface.");

              signature = field.getType().getName();
            } else {
              // checks that the signature designate an interface.
              if (!cItf.signature().isInterface())
                throw new TaskException(
                    "Invalid @ClientInterface annotation in field '"
                        + field.getDeclaringClass().getName() + '.'
                        + field.getName()
                        + "': the signature class does not represent an "
                        + "interface type.");

              signature = cItf.signature().getName();
            }

            // add ItfTemplate
            itfs.add(new ItfTemplate(cItf, signature, SINGLE, cItf
                .contingency(), parameters, debug));
          }

        } else if (cItfForEach != null) {
          final Field previousDeclarator = cItfForEachDeclarator.put(
              cItfForEach.prefix(), field);
          if (previousDeclarator != null) {
            throw new TaskException(
                "Invalid @ClientInterfaceforEach annotation in field '"
                    + field.getDeclaringClass().getName() + '.'
                    + field.getName()
                    + "' an interface set with the same prefix is defined by "
                    + "field '"
                    + previousDeclarator.getDeclaringClass().getName() + '.'
                    + previousDeclarator.getName() + "'.");
          }

          // checks that the signature designate an interface.
          if (!cItfForEach.signature().isInterface())
            throw new TaskException(
                "Invalid @ClientInterface annotation in field '"
                    + field.getDeclaringClass().getName() + '.'
                    + field.getName()
                    + "': the signature class does not represent an "
                    + "interface type.");

          // add ItfForEachTemplate
          if (forEachItfs == null)
            forEachItfs = new ArrayList<ItfForEachTemplate>();
          forEachItfs.add(new ItfForEachTemplate(cItfForEach, parameters,
              cItfForEach.contingency(), debug));
        }
      }
    }

    Component instantiate(final Object implementation, final Object... args)
        throws TaskException {
      if (args.length != taskParameterNames.length)
        throw new TaskException("Invalid number of parameter: found "
            + args.length + " where " + taskParameterNames.length
            + " were expected.");

      final String parameterStrings = Arrays.deepToString(args);
      final String taskName = taskClassName + '('
          + parameterStrings.substring(1, parameterStrings.length() - 1) + ')';
      if (debug) {
        System.out.println();
        System.out
            .println("--------------------------------------------------------------------------------");
        System.out.println("Create new primitive task from "
            + implementation.getClass());
        if (taskParameterNames.length > 0) {
          System.out.print("Parameters = {");
          for (int i = 0; i < taskParameterNames.length; i++) {
            System.out.print(taskParameterNames[i] + '=' + args[i]);
            if (i != taskParameterNames.length - 1) System.out.print(", ");
          }
          System.out.println("}");
        }
        System.out.println("Task name = " + taskName);
      }

      final List<TaskInterfaceDesc> taskInterfaces = new ArrayList<TaskInterfaceDesc>();
      for (final ItfTemplate template : itfs) {
        taskInterfaces.add(template.instantiate(args));
      }

      Map<String, Object> itfNameMappings = null;
      if (forEachItfs != null) {
        itfNameMappings = new HashMap<String, Object>();
        for (final ItfForEachTemplate template : forEachItfs) {
          template.instantiate(taskInterfaces, itfNameMappings, args);
        }
      }

      Component taskComponent;
      try {
        taskComponent = newTask(implementation, debugBinding, taskInterfaces
            .toArray(new TaskInterfaceDesc[taskInterfaces.size()]));
        Fractal.getNameController(taskComponent).setFcName(taskName);
      } catch (final InstantiationException e) {
        e.printStackTrace();
        throw new TaskException(
            "An error occurs while instantiating task membrane", e);
      } catch (final NoSuchInterfaceException e) {
        throw new TaskException(
            "An error occurs while instantiating task membrane", e);
      }
      if (itfNameMappings != null) {
        try {
          final TaskBindingController bindingController = (TaskBindingController) taskComponent
              .getFcInterface("binding-controller");
          bindingController.addFcInterfaceNameMapping(itfNameMappings);
        } catch (final NoSuchInterfaceException e) {
          throw new TaskException(
              "Could not access to the binding-controller interface of task component.");
        }
      }

      if (debug) {
        System.out
            .println("--------------------------------------------------------------------------------");
      }
      return taskComponent;
    }
  }

  private static final class ItfTemplate {
    final String         name;
    final String         signaure;
    final boolean        role;
    final boolean        contingency;
    final boolean        cardinality;
    final RecordTemplate recordTemplate;
    final int[]          parameterMapping;
    final boolean        debug;

    private ItfTemplate(final String name, final String signature,
        final boolean role, final boolean cardinality,
        final boolean contingency, final String recordDesc,
        final String[] recordParams, final String[] taskParams,
        final boolean debug) throws TaskException {
      this.name = name;
      this.signaure = signature;
      this.role = role;
      this.contingency = contingency;
      this.cardinality = cardinality;
      this.recordTemplate = new RecordTemplate(recordDesc);
      parameterMapping = new int[recordParams.length];
      for (int i = 0; i < recordParams.length; i++) {
        parameterMapping[i] = getParameterIndex(taskParams, recordParams[i]);
      }
      this.debug = debug;
    }

    ItfTemplate(final ServerInterface itf, final String[] taskParams,
        final boolean debug) throws TaskException {
      this(itf.name(), itf.signature().getName(), SERVER, SINGLE, MANDATORY,
          itf.record(), itf.parameters(), taskParams, debug);
    }

    ItfTemplate(final ClientInterface itf, final String signature,
        final boolean cardinality, final boolean contingency,
        final String[] taskParams, final boolean debug) throws TaskException {
      this(itf.name(), signature, CLIENT, cardinality, contingency, itf
          .record(), itf.parameters(), taskParams, debug);
    }

    TaskInterfaceDesc instantiate(final Object[] parameters)
        throws TaskException {
      final Object[] recordParams = new Object[parameterMapping.length];
      for (int i = 0; i < parameterMapping.length; i++) {
        recordParams[i] = parameters[parameterMapping[i]];
      }
      final Record record = recordTemplate.instantiate(recordParams);
      if (debug) {
        if (role == CLIENT) {
          System.out.println(" - add \"" + name
              + "\" client interface with record=" + record);
        } else {
          System.out.println(" - add \"" + name
              + "\" server interface with record=" + record);
        }
      }
      return itf(name, signaure, role, contingency, cardinality, record);
    }
  }

  private static final class ItfForEachTemplate {
    final int            iterableIndex;
    final String         iterableName;
    final String         prefix;
    final String         signaure;
    final boolean        role;
    final boolean        contingency;
    final boolean        cardinality;
    final RecordTemplate recordTemplate;
    final int[]          parameterMapping;
    final boolean        debug;

    ItfForEachTemplate(final ClientInterfaceForEach itf,
        final String[] taskParams, final boolean contingency,
        final boolean debug) throws TaskException {

      iterableName = itf.iterable();
      iterableIndex = getParameterIndex(taskParams, iterableName);
      prefix = itf.prefix();
      signaure = itf.signature().getName();
      role = CLIENT;
      this.contingency = contingency;
      cardinality = SINGLE;
      recordTemplate = new RecordTemplate(itf.record());
      final String[] recordParams = itf.parameters();
      final String elemtParamName = iterableName + ".element";
      parameterMapping = new int[recordParams.length];
      for (int i = 0; i < recordParams.length; i++) {
        if (recordParams[i].equals(elemtParamName))
          parameterMapping[i] = -1;
        else
          parameterMapping[i] = getParameterIndex(taskParams, recordParams[i]);
      }
      this.debug = debug;
    }

    void instantiate(final List<TaskInterfaceDesc> itfs,
        final Map<String, Object> clientItfMapping, final Object[] parameters)
        throws TaskException {
      final Object iterable = parameters[iterableIndex];
      if (!(iterable instanceof Iterable<?>)) {
        throw new TaskException("Invalid parameter '" + iterableName
            + "': iterable parameter must be an instance of "
            + "java.lang.Iterable");
      }

      if (debug) {
        System.out.println(" - instantiate the \"" + prefix
            + "\" forEach client interface with iterable=" + iterable);
      }

      int elementIndex = 0;
      for (final Object element : (Iterable<?>) iterable) {
        final String name = prefix + (++elementIndex);
        clientItfMapping.put(name, element);

        final Object[] recordParams = new Object[parameterMapping.length];
        for (int i = 0; i < parameterMapping.length; i++) {
          final int paramIndex = parameterMapping[i];
          if (paramIndex == -1) {
            recordParams[i] = element;
          } else {
            recordParams[i] = parameters[paramIndex];
          }
        }
        final Record record = recordTemplate.instantiate(recordParams);
        if (debug) {
          System.out
              .println("    * add client interface with record=" + record);
        }
        itfs.add(itf(name, signaure, role, contingency, cardinality, record));
      }

    }
  }

  private static boolean contains(final String[] parameters,
      final String parameterName) {
    for (final String parameter : parameters) {
      if (parameter.equals(parameterName)) return true;
    }
    return false;
  }

  private static int getParameterIndex(final String[] parameters,
      final String parameterName) throws TaskException {
    for (int i = 0; i < parameters.length; i++) {
      if (parameters[i].equals(parameterName)) return i;
    }
    throw new TaskException("Parameter '" + parameterName
        + "' not found in the task parameter list");
  }

  private static boolean isCollectionInterface(final Field field) {
    return Map.class.isAssignableFrom(field.getType());
  }
}
