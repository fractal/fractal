/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.composition;

/**
 * Exception thrown when a {@link Function} is called with a parameter of an
 * invalid type.
 */
public class InvalidParameterTypeException extends FunctionExecutionException {

  private final int      paramIndex;
  private final Class<?> expectedType;
  private final Class<?> actualType;

  /**
   * Creates a new {@link InvalidParameterTypeException}.
   * 
   * @param function the function object that has been invoked with an invalid
   *            parameter.
   * @param paramIndex the index of the invalid parameter.
   * @param expectedType the type that was expected for the parameter.
   * @param actualType the actual type of the parameter.
   */
  public InvalidParameterTypeException(final Function function,
      final int paramIndex, final Class<?> expectedType,
      final Class<?> actualType) {
    this(function, paramIndex, expectedType, actualType, null);
  }

  /**
   * Creates a new {@link InvalidParameterTypeException}.
   * 
   * @param function the function object that has been invoked with an invalid
   *            parameter.
   * @param paramIndex the index of the invalid parameter.
   * @param expectedType the type that was expected for the parameter.
   * @param actualType the actual type of the parameter.
   * @param message an additional message.
   */
  public InvalidParameterTypeException(final Function function,
      final int paramIndex, final Class<?> expectedType,
      final Class<?> actualType, final String message) {
    super(function, message);
    this.paramIndex = paramIndex;
    this.expectedType = expectedType;
    this.actualType = actualType;
  }

  /**
   * @return the index of the invalid parameter.
   */
  public int getParamIndex() {
    return paramIndex;
  }

  /**
   * @return the type that was expected for the parameter.
   */
  public Class<?> getExpectedType() {
    return expectedType;
  }

  /**
   * @return the actual type of the parameter.
   */
  public Class<?> getActualType() {
    return actualType;
  }

  @Override
  public String getMessage() {
    String message = "In function \"" + getFunctionName()
        + "\", invalid parameter type for the parameter " + paramIndex
        + ". Found \"" + actualType.getName() + "\" where expected type is \""
        + expectedType.getName() + "\"";

    if (super.getMessage() != null) {
      message += ": " + super.getMessage();
    }

    return message;
  }
}
