/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.composition;

import java.util.Collection;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.task.core.TaskException;

/**
 * Interface to create composite task components from a collection of sub tasks
 * and a composition schema.
 */
public interface CompositeTaskFactory {

  /**
   * Creates a composite task component.
   * 
   * @param subTasks the sub tasks of the composite task.
   * @param compositionName the name of the composition schema to apply to the
   *            created composite.
   * @param context context.
   * @param parameters the parameters of the composition schema.
   * @return a composite task component that contains the given sub tasks bound
   *         as described in the composition schema.
   * @throws TaskException if an error occurs.
   */
  Component newCompositeTask(Collection<Component> subTasks,
      String compositionName, Map<Object, Object> context, Object... parameters)
      throws TaskException;

  /**
   * Creates a composite task component.
   * 
   * @param subTasks the sub tasks of the composite task.
   * @param compositionName the name of the composition schema to apply to the
   *            created composite.
   * @param compositeName the name of the created composite.
   * @param context context.
   * @param parameters the parameters of the composition schema.
   * @return a composite task component that contains the given sub tasks bound
   *         as described in the composition schema.
   * @throws TaskException if an error occurs.
   */
  Component newCompositeTask(Collection<Component> subTasks,
      String compositionName, String compositeName,
      Map<Object, Object> context, Object... parameters) throws TaskException;
}
