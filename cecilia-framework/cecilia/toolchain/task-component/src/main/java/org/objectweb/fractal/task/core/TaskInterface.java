/**
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core;

import org.objectweb.fractal.julia.ComponentInterface;

/**
 * Extends the {@link ComponentInterface} interface to add the ability to attach
 * {@link Record} object to a component interface.
 */
public interface TaskInterface extends ComponentInterface {

  /**
   * Sets the record attached to this interface.
   * 
   * @param r a record. Can be <code>null</code>.
   */
  void setFcRecord(Record r);

  /**
   * Returns the record attached to this interface.
   * 
   * @return a record, or <code>null</code>.
   */
  Record getFcRecord();
}
