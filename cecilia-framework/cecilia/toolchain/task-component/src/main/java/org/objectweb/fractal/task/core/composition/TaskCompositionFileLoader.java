/***
 * Fractal ADL Task Framework Parser
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Ali Erdem Ozcan
 */

package org.objectweb.fractal.task.core.composition;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;

import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.composition.ast.TaskDefinition;
import org.objectweb.fractal.task.core.composition.parser.TaskCompositionParser;

/**
 * Task composition schema parser. This component locates and parses task
 * composition schema and returns the corresponding {@link TaskDefinition} node.
 */
public class TaskCompositionFileLoader
    implements
      TaskCompositionLoader,
      BindingController {

  /** The DTD that describe the grammar of the AST of task composition schema. */
  public static final String TASK_COMPOSITION_DTD       = "classpath://org/objectweb/fractal/task/core/composition/ast/task-composition.dtd";

  /** The file extension of task composition schema. */
  public static final String TASK_COMPISITION_EXTENSION = ".task";

  // ---------------------------------------------------------------------------
  // Client interfaces
  // ---------------------------------------------------------------------------

  /** The name of the {@link #nodeFactoryItf} client interface. */
  public static final String NODE_FACTORY_ITF_NAME      = "node-factory";

  /** The client interface used to instantiate AST nodes. */
  public XMLNodeFactory      nodeFactoryItf;

  // ---------------------------------------------------------------------------
  // Implementation of the TaskCompositionLoader interface
  // ---------------------------------------------------------------------------

  public TaskDefinition load(final String name,
      final Map<Object, Object> context) throws TaskException {
    final URL srcFile = locateTask(name, context);
    InputStream inputStream;
    try {
      inputStream = srcFile.openStream();
    } catch (final IOException e) {
      throw new TaskException("Can't read file: " + srcFile, e);
    }

    final TaskCompositionParser parser = new TaskCompositionParser(inputStream);
    parser.init(nodeFactoryItf, TASK_COMPOSITION_DTD, srcFile.getPath());
    try {
      return parser.getTaskDefinition();
    } catch (final Exception e) {
      throw new TaskException("Syntax error in file " + srcFile, e);
    }
  }

  protected URL locateTask(final String name, final Map<Object, Object> context)
      throws TaskException {
    final URL srcFile = getLoader(context).getResource(
        name.replace('.', '/') + TASK_COMPISITION_EXTENSION);
    if (srcFile == null) {
      throw new TaskException("Can't find task composition schema: " + name);
    }
    return srcFile;
  }

  protected ClassLoader getLoader(final Map<Object, Object> context) {
    ClassLoader loader = null;
    if (context != null) {
      loader = (ClassLoader) context.get("classloader");
    }
    if (loader == null) {
      loader = getClass().getClassLoader();
    }
    return loader;
  }

  // --------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // --------------------------------------------------------------------------

  public void bindFc(final String itfName, final Object value)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (itfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (itfName.equals(NODE_FACTORY_ITF_NAME)) {
      this.nodeFactoryItf = (XMLNodeFactory) value;
    } else {
      throw new NoSuchInterfaceException("There is no interface named '"
          + itfName + "'");
    }

  }

  public String[] listFc() {
    return new String[]{NODE_FACTORY_ITF_NAME};
  }

  public Object lookupFc(final String itfName) throws NoSuchInterfaceException {

    if (itfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (itfName.equals(NODE_FACTORY_ITF_NAME)) {
      return this.nodeFactoryItf;
    } else {
      throw new NoSuchInterfaceException("There is no interface named '"
          + itfName + "'");
    }
  }

  public void unbindFc(final String itfName) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException {

    if (itfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (itfName.equals(NODE_FACTORY_ITF_NAME)) {
      this.nodeFactoryItf = null;
    } else {
      throw new NoSuchInterfaceException("There is no interface named '"
          + itfName + "'");
    }
  }
}
