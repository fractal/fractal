/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.composition.function;

import org.objectweb.fractal.task.core.composition.Function;
import org.objectweb.fractal.task.core.composition.FunctionExecutionException;
import org.objectweb.fractal.task.core.composition.InvalidNumberOfParameterException;
import org.objectweb.fractal.task.core.composition.NullParameterException;

/**
 * Implements a {@link Object#equals(Object) equals} function. This function
 * takes two parameter of any type and returns {@link Boolean#TRUE true} if and
 * only if <code>params[0].equals(params[1])</code> returns <code>true</code>;
 * returns {@link Boolean#FALSE false} otherwise even if the
 * {@link Object#equals(Object) equals} method throws an exception.
 */
public class Equals implements Function {

  public Object invoke(final Object... params)
      throws FunctionExecutionException {
    if (params.length != 2) {
      throw new InvalidNumberOfParameterException(this, 2, params.length);
    }
    if (params[0] == null) throw new NullParameterException(this, 0);
    if (params[1] == null) throw new NullParameterException(this, 1);

    try {
      return params[0].equals(params[1]);
    } catch (final Exception e) {
      return Boolean.FALSE;
    }
  }
}
