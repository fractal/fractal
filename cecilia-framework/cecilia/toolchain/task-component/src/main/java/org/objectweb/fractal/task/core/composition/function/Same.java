/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.composition.function;

import org.objectweb.fractal.task.core.composition.Function;
import org.objectweb.fractal.task.core.composition.FunctionExecutionException;
import org.objectweb.fractal.task.core.composition.InvalidNumberOfParameterException;

/**
 * Implements the "equivalent" of <code>==</code> operator. This function
 * takes two parameter of any type and returns a {@link Boolean}. The value
 * returned by this function depends on the parameter types:
 * <ul>
 * <li>If the two parameters are instances of {@link Number}, returns
 * {@link Boolean#TRUE true} if they contains the same value.</li>
 * <li>If the two parameters are instances of {@link Boolean}, returns
 * {@link Boolean#TRUE true} if they contains the same boolean value.</li>
 * <li>If the two parameters are instances of {@link String}, returns
 * {@link Boolean#TRUE true} if the two strings are equals.</li>
 * <li>Otherwise returns {@link Boolean#TRUE true} if
 * <code>params[0] == params[1]</code>.</li>
 * </ul>
 */
public class Same implements Function {

  /**
   * Implements same function.
   * 
   * @param p0 first parameter
   * @param p1 second parameter
   * @return <code>true</code> if p0 and p1 are the "same" objects.
   */
  public static boolean same(final Object p0, final Object p1) {
    // if they are same object, don't need to check types
    if (p0 == p1) {
      return true;
    }

    // if one parameter is null and not the other one.
    if ((p0 == null && p1 != null) || (p0 != null && p1 == null)) return false;

    if (p0 instanceof Number) {
      if (p1 instanceof Number)
        return p0.equals(p1);
      else
        return false;
    } else if (p0 instanceof Boolean) {
      if (p1 instanceof Boolean)
        return p0.equals(p1);
      else
        return false;

    } else if (p0 instanceof String) {
      if (p1 instanceof String)
        return p0.equals(p1);
      else
        return false;
    } else {
      return false;
    }
  }

  public Object invoke(final Object... params)
      throws FunctionExecutionException {
    if (params.length != 2) {
      throw new InvalidNumberOfParameterException(this, 2, params.length);
    }
    final Object p0 = params[0];
    final Object p1 = params[1];

    return same(p0, p1);
  }

}
