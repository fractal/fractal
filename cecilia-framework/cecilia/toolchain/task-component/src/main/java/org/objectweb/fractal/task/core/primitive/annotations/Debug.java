/***
 * Fractal ADL Task Framework Parser
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.primitive.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation can be used to customize the messages that are printed at the
 * different life-cycle phases of a task. The value of this annotation is an
 * array of string. Each string identify a life-cycle phases; possible values
 * are:
 * <ul>
 * <li><code>"instantiation"</code> to enable debug messages during the
 * instantiation of the task.</li>
 * <li><code>"binding"</code> to enable debug messages when the task is
 * bound.</li>
 * </ul>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface Debug {

  /** Enable debug messages during the instantiation of the task. */
  String INSTANTIATION = "instantiation";
  /** Enable debug messages when the task is bound. */
  String BINDING       = "binding";

  /**
   * @return the life-cycle phases for which debug messages should be printed.
   */
  String[] value();
}
