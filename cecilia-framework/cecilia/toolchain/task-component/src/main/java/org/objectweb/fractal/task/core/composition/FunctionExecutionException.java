/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.composition;

/**
 * Exception thrown by {@link Function} when an error occurs.
 */
public class FunctionExecutionException extends Exception {

  private final Function function;
  private String         functionName;

  /**
   * Creates a {@link FunctionExecutionException}.
   * 
   * @param function the function object that raise the error.
   */
  public FunctionExecutionException(final Function function) {
    this.function = function;
    this.functionName = function.getClass().getName();
  }

  /**
   * Creates a {@link FunctionExecutionException}.
   * 
   * @param function the function object that raise the error.
   * @param message the detail message
   */
  public FunctionExecutionException(final Function function,
      final String message) {
    super(message);
    this.function = function;
    this.functionName = function.getClass().getName();
  }

  /**
   * Creates a {@link FunctionExecutionException}.
   * 
   * @param function the function object that raise the error.
   * @param cause the cause
   */
  public FunctionExecutionException(final Function function,
      final Throwable cause) {
    super(cause);
    this.function = function;
    this.functionName = function.getClass().getName();
  }

  /**
   * Creates a {@link FunctionExecutionException}.
   * 
   * @param function the function object that raise the error.
   * @param message the detail message
   * @param cause the cause
   */
  public FunctionExecutionException(final Function function,
      final String message, final Throwable cause) {
    super(message, cause);
    this.function = function;
    this.functionName = function.getClass().getName();
  }

  /**
   * @return the function object that has been invoked with an invalid
   *         parameter.
   */
  public Function getFunction() {
    return function;
  }

  /**
   * @return the name of the function to be used in the exception message.
   */
  public String getFunctionName() {
    return functionName;
  }

  /**
   * Sets the name of the function to be used in the exception message.
   * 
   * @param functionName the name of the function to set.
   */
  public void setFunctionName(final String functionName) {
    this.functionName = functionName;
  }
}