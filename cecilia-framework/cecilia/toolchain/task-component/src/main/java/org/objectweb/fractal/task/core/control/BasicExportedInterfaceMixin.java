/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.control;

import static org.objectweb.fractal.util.Fractal.getBindingController;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.ChainedNoSuchInterfaceException;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.control.binding.ChainedIllegalBindingException;
import org.objectweb.fractal.util.Fractal;

/**
 * @author leclercm
 */
public abstract class BasicExportedInterfaceMixin
    implements
      Controller,
      ExportedInterfaceController {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private BasicExportedInterfaceMixin() {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overridden by the mixin class
  // -------------------------------------------------------------------------

  public Map<String, Interface> fcExportedInterfaces;

  /**
   * Initializes the fields of this mixin and then calls the overridden method.
   * 
   * @param ic information about the component to which this controller object
   *            belongs.
   * @throws InstantiationException if the initialization fails.
   */
  public void initFcController(final InitializationContext ic)
      throws InstantiationException {
    fcExportedInterfaces = new HashMap<String, Interface>();
    _super_initFcController(ic);
  }

  public void exportFcInterface(final String clientItfName,
      final Interface innerItf) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException {
    final ComponentType compType = (ComponentType) _this_weaveableC.getFcType();
    InterfaceType clientItfType;
    try {
      clientItfType = compType.getFcInterfaceType(clientItfName);
    } catch (final NoSuchInterfaceException e) {
      throw new ChainedNoSuchInterfaceException(null, _this_weaveableC,
          clientItfName);
    }
    if (!clientItfType.isFcClientItf()) {
      throw new ChainedNoSuchInterfaceException(null, _this_weaveableC,
          clientItfName);
    }
    if (!clientItfType.isFcCollectionItf()) {
      throw new ChainedIllegalBindingException(null, innerItf.getFcItfOwner(),
          _this_weaveableC, innerItf.getFcItfName(), clientItfName,
          "The internal client interface is not a collection interface");
    }
    fcExportedInterfaces.put(clientItfName, innerItf);
  }

  public void unexportFcInterface(final String clientItfName)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {
    if (!fcExportedInterfaces.containsKey(clientItfName)) {
      throw new ChainedNoSuchInterfaceException(null, _this_weaveableC,
          "Interface '" + clientItfName
              + "' does not correspond to an exported client interface.");
    }
    final Interface innerItf = fcExportedInterfaces.get(clientItfName);
    for (final String itfName : getBindingController(innerItf.getFcItfOwner())
        .listFc()) {
      if (itfName.startsWith(innerItf.getFcItfName())) {
        throw new ChainedIllegalBindingException(null,
            innerItf.getFcItfOwner(), _this_weaveableC,
            innerItf.getFcItfName(), clientItfName,
            "Cannot unexport the collection interface. Interface '" + itfName
                + "' must be unbound first.");
      }
    }

    fcExportedInterfaces.remove(clientItfName);
  }

  public void bindFc(final InterfaceType clientItfType,
      final String clientItfName, final Object serverItf)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    // first call super implementation.
    _super_bindFc(clientItfType, clientItfName, serverItf);

    final Interface innerItf = fcExportedInterfaces.get(clientItfType
        .getFcItfName());
    if (innerItf != null) {
      // the interface correspond to an exported collection interface.
      final Object internalItf = Fractal.getContentController(_this_weaveableC)
          .getFcInternalInterface(clientItfName);
      final String suffix = clientItfName.substring(clientItfType
          .getFcItfName().length());
      getBindingController(innerItf.getFcItfOwner()).bindFc(
          innerItf.getFcItfName() + suffix, internalItf);
    }
  }

  public void unbindFc(final InterfaceType clientItfType,
      final String clientItfName) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException {
    _super_unbindFc(clientItfType, clientItfName);

    final Interface innerItf = fcExportedInterfaces.get(clientItfType
        .getFcItfName());
    if (innerItf != null) {
      // the interface correspond to an exported collection interface.
      final String suffix = clientItfName.substring(clientItfType
          .getFcItfName().length());
      getBindingController(innerItf.getFcItfOwner())
          .unbindFc(innerItf + suffix);
    }
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableC</tt> field required by this mixin. This field is
   * supposed to reference the {@link Component} interface of the component to
   * which this controller object belongs.
   */
  public Component _this_weaveableC;

  /**
   * @see Controller#initFcController
   */
  public abstract void _super_initFcController(InitializationContext ic)
      throws InstantiationException;

  public abstract void _super_bindFc(InterfaceType clientItfType,
      String clientItfName, Object serverItf) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException;

  public abstract void _super_unbindFc(final InterfaceType clientItfType,
      final String clientItfName) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException;
}
