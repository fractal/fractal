/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.composition;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.objectweb.fractal.task.core.TaskFactory.EXPORT_ALL;
import static org.objectweb.fractal.task.core.composition.TaskCompositionFileLoader.TASK_COMPOSITION_DTD;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.task.core.Executable;
import org.objectweb.fractal.task.core.TaskFactory;
import org.objectweb.fractal.task.core.internal.BasicTaskFactory;
import org.objectweb.fractal.task.core.primitive.BasicPrimitiveTaskFactory;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterfaceForEach;
import org.objectweb.fractal.task.core.primitive.annotations.Debug;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * @author leclercm
 */
public class TestInterpreterAnnotation {

  private static final String TASKS_PACKAGE = "test.task.composition.";

  private TaskFactory         taskFactory;

  @Before
  public void init() throws Exception {
    // Sub-components
    final XMLNodeFactory nodeFactory = new XMLNodeFactoryImpl();
    final TaskCompositionFileLoader fileLoader = new TaskCompositionFileLoader();
    final VariableLoader variableLoader = new VariableLoader();
    final FunctionLoader functionLoader = new FunctionLoader();
    final Interpreter interpreter = new Interpreter();
    final BasicPrimitiveTaskFactory primitiveFactory = new BasicPrimitiveTaskFactory();
    final BasicTaskFactory basicTaskFactory = new BasicTaskFactory();

    // Bindings
    fileLoader.nodeFactoryItf = nodeFactory;
    variableLoader.clientLoader = fileLoader;
    functionLoader.clientLoader = variableLoader;
    interpreter.clientLoaderItf = functionLoader;
    basicTaskFactory.compositeTaskFactoryItf = interpreter;
    basicTaskFactory.primitiveTaskFactoryItf = primitiveFactory;

    taskFactory = basicTaskFactory;

    // initialize node factory.
    nodeFactory.checkDTD(TASK_COMPOSITION_DTD);
  }

  @Test
  public void testEvaluate1() throws Exception {
    final Object componentNode = new Object();
    final Component t1 = taskFactory.newPrimitiveTask(new Task1(),
        componentNode);
    final Component t2 = taskFactory.newPrimitiveTask(new Task2(),
        componentNode);
    final Collection<Component> tasks = new ArrayList<Component>();
    tasks.add(t1);
    tasks.add(t2);

    final Component composite = taskFactory.newCompositeTask(tasks,
        TASKS_PACKAGE + "T2", null, componentNode);
    final TaskItf tItf = (TaskItf) composite.getFcInterface("s");
    assertEquals("Task1 Task2", tItf.get());
  }

  @Test
  public void testEvaluate2() throws Exception {
    final Object componentNode = new Object();
    final Collection<Object> interfaces = new ArrayList<Object>();
    interfaces.add("titi");
    interfaces.add("toto");
    interfaces.add("tutu");

    final List<Object> executedTasks = new ArrayList<Object>();
    final Collection<Component> tasks = new ArrayList<Component>();
    final Component t1 = taskFactory.newPrimitiveTask(new Task3(interfaces,
        executedTasks), componentNode, interfaces);
    tasks.add(t1);

    for (final Object itf : interfaces) {
      tasks.add(taskFactory.newPrimitiveTask(new Task4(itf, executedTasks),
          componentNode, itf));
    }

    final Component composite = taskFactory.newCompositeTask(tasks,
        TASKS_PACKAGE + "T5", null, componentNode);
// final ExecutionController execCtrl = (ExecutionController) composite
// .getFcInterface(ExecutionController.DEFAULT_NAME);
// assertFalse(execCtrl.isFcExecuted());
// execCtrl.execute();
// assertTrue(execCtrl.isFcExecuted());

    final TaskItf tItf = (TaskItf) composite.getFcInterface("s");
    assertEquals("Task1 tititototutu", tItf.get());
    assertEquals(4, executedTasks.size());
  }

  @Test
  public void testEvaluate3() throws Exception {
    final Object componentNode = new Object();
    final Component t1 = taskFactory.newPrimitiveTask(new Task1(),
        componentNode);
    final Component t2 = taskFactory.newPrimitiveTask(new Task2(),
        componentNode);
    final Collection<Component> clientTasks = new ArrayList<Component>();
    clientTasks.add(t1);
    final Component clientComposite = taskFactory.newCompositeTask(clientTasks,
        EXPORT_ALL, "firstTask", null);

    final Collection<Component> serverTasks = new ArrayList<Component>();
    serverTasks.add(t2);
    final Component serverComposite = taskFactory.newCompositeTask(serverTasks,
        EXPORT_ALL, "secondTask", null);

    final Collection<Component> topTasks = new ArrayList<Component>();
    topTasks.add(clientComposite);
    topTasks.add(serverComposite);

    final Component composite = taskFactory.newCompositeTask(topTasks,
        TASKS_PACKAGE + "T2", null, componentNode);
    final TaskItf tItf = (TaskItf) composite.getFcInterface("s");
    assertEquals("Task1 Task2", tItf.get());
  }

  @Test
  public void testEvaluate4() throws Exception {
    final Object componentNode = new Object();

    final Collection<Component> tasks = new ArrayList<Component>();
    tasks.add(taskFactory.newPrimitiveTask(new Task5(), componentNode));
    tasks.add(taskFactory.newPrimitiveTask(new Task2(), componentNode));
    tasks.add(taskFactory.newPrimitiveTask(new Task2(), componentNode));

    final Component composite = taskFactory.newCompositeTask(tasks,
        TASKS_PACKAGE + "T2", null, componentNode);

    final TaskItf tItf = (TaskItf) composite.getFcInterface("s");
    assertEquals("Task5 Task2Task2", tItf.get());
  }

  @Test
  public void testEvaluate5() throws Exception {
    final Object componentNode = new Object();

    final Collection<Component> clientTasks = new ArrayList<Component>();
    clientTasks.add(taskFactory.newPrimitiveTask(new Task5(), componentNode));
    final Component clientComposite = taskFactory.newCompositeTask(clientTasks,
        EXPORT_ALL, "clientComposite", null);

    final Collection<Component> serverTasks = new ArrayList<Component>();
    serverTasks.add(taskFactory.newPrimitiveTask(new Task2(), componentNode));
    serverTasks.add(taskFactory.newPrimitiveTask(new Task2(), componentNode));
    final Component serverComposite = taskFactory.newCompositeTask(serverTasks,
        EXPORT_ALL, "serverComposite", null);

    final Collection<Component> topTasks = new ArrayList<Component>();
    topTasks.add(clientComposite);
    topTasks.add(serverComposite);

    Component composite;
    try {
      composite = taskFactory.newCompositeTask(topTasks, TASKS_PACKAGE + "T2",
          null, componentNode);
      final TaskItf tItf = (TaskItf) composite.getFcInterface("s");
      assertEquals("Task5 Task2Task2", tItf.get());
    } catch (final Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      e.getCause().printStackTrace();
      fail();
    }
  }

  public static interface TaskItf {
    String get();
  }

  @TaskParameters({"componentNode"})
  @ServerInterfaces({@ServerInterface(name = "s", signature = TaskItf.class, record = "role:s1, id:%", parameters = {"componentNode"})})
  public static class Task1 implements TaskItf {

    @ClientInterface(name = "c", record = "role:foo, id:%", parameters = {"componentNode"})
    public TaskItf clientTaskItf;

    public String get() {
      return "Task1 " + clientTaskItf.get();
    }
  }

  @TaskParameters("componentNode")
  @ServerInterfaces({@ServerInterface(name = "s", signature = TaskItf.class, record = "role:bar, id:%", parameters = "componentNode")})
  public static class Task2 implements TaskItf {
    public String get() {
      return "Task2";
    }
  }

  @TaskParameters({"componentNode", "interfaces"})
  @ServerInterfaces({@ServerInterface(name = "s", signature = TaskItf.class, record = "role:s1, id:%", parameters = {"componentNode"})})
  protected static class Task3 implements Executable, TaskItf {

    final Collection<Object>          interfaces;
    final List<Object>                executedTasks;
    String                            result;

    @ClientInterfaceForEach(iterable = "interfaces", prefix = "c", signature = TaskItf.class, record = "role:foo, id:%, itf:%", parameters = {
        "componentNode", "interfaces.element"})
    public final Map<Object, TaskItf> clientTaskItf = new HashMap<Object, TaskItf>();

    Task3(final Collection<Object> interfaces, final List<Object> executedTasks) {
      this.interfaces = interfaces;
      this.executedTasks = executedTasks;
    }

    public String get() {
      return result;
    }

    public void execute() throws Exception {
      result = "Task1 ";
      for (final Object itf : interfaces) {
        result += clientTaskItf.get(itf).get();
      }
      assertEquals(3, executedTasks.size());
      executedTasks.add(this);
    }
  }

  @TaskParameters({"componentNode", "interface"})
  @ServerInterfaces({@ServerInterface(name = "s", signature = TaskItf.class, record = "role:bar, id:%, itf:%", parameters = {
      "componentNode", "interface"})})
  protected static class Task4 implements Executable, TaskItf {

    final Object       itf;
    final List<Object> executedTasks;

    Task4(final Object itf, final List<Object> executedTasks) {
      this.itf = itf;
      this.executedTasks = executedTasks;
    }

    public String get() {
      return itf.toString();
    }

    public void execute() throws Exception {
      System.out.println("execute Task4");
      executedTasks.add(this);
    }
  }

  @Debug({Debug.INSTANTIATION, Debug.BINDING})
  @TaskParameters({"componentNode"})
  @ServerInterfaces({@ServerInterface(name = "s", signature = TaskItf.class, record = "role:s1, id:%", parameters = {"componentNode"})})
  protected static class Task5 implements TaskItf {

    @ClientInterface(name = "clientTask", signature = TaskItf.class, record = "role:foo, id:%", parameters = "componentNode")
    public final Map<String, TaskItf> clientTaskItfs = new HashMap<String, TaskItf>();

    public String get() {
      String result = "Task5 ";
      for (final TaskItf t : clientTaskItfs.values()) {
        result += t.get();
      }
      return result;
    }
  }
}
