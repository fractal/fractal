/***
 * Fractal ADL Task Framework Parser
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Ali Erdem Ozcan
 */

package org.objectweb.fractal.task.core.composition.parser;

import java.util.HashMap;

import junit.framework.TestCase;

import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;
import org.objectweb.fractal.task.core.composition.FunctionLoader;
import org.objectweb.fractal.task.core.composition.TaskCompositionFileLoader;
import org.objectweb.fractal.task.core.composition.VariableLoader;
import org.objectweb.fractal.task.core.composition.ast.TaskDefinition;

public class CompositionLoaderTest extends TestCase {

  private static final String       TASKS_PACKAGE = "test.task.composition.";

  // Sub-components
  private XMLNodeFactory            nodeFactory;
  private TaskCompositionFileLoader fileLoader;
  private VariableLoader            variableLoader;
  private FunctionLoader            functionLoader;

  @Override
  protected void setUp() throws Exception {
    nodeFactory = new XMLNodeFactoryImpl();
    fileLoader = new TaskCompositionFileLoader();
    variableLoader = new VariableLoader();
    functionLoader = new FunctionLoader();
    // Bindings
    fileLoader.nodeFactoryItf = nodeFactory;
    variableLoader.clientLoader = fileLoader;
    functionLoader.clientLoader = variableLoader;
    functionLoader
        .setDefaultFunctionsPackageName("org.objectweb.fractal.task.core.composition.function");
  }

  /**
   * Tests the semantic analysis that has to be done composition definitions
   * <code> test.task.composition.T2.task </code>.
   * 
   * @throws Exception if test fails
   */
  public void testT2() throws Exception {
    @SuppressWarnings("all")
    final TaskDefinition type = loadTaskDefinition("T2");
  }

  private TaskDefinition loadTaskDefinition(final String name) throws Exception {
    final String resourceName = (TASKS_PACKAGE + name);
    return functionLoader.load(resourceName, new HashMap<Object, Object>());
  }

}
