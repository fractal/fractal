
package org.objectweb.fractal.task.core;

import static org.junit.Assert.assertEquals;
import static org.objectweb.fractal.task.core.internal.TaskUtil.client;
import static org.objectweb.fractal.task.core.internal.TaskUtil.newTask;
import static org.objectweb.fractal.task.core.internal.TaskUtil.server;

import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

/**
 * @author leclercm
 */
public class TaskUtilTest {

  @Test
  public void testNewTask() throws Exception {
    final Record r1 = new Record("f1:v1");
    final Record r2 = new Record("f2:v2");

    final Component t1 = newTask(TaskImpl.class.getName(), false, server("s1",
        TaskItf.class.getName(), r1), client("c1", TaskItf.class.getName(), r2));

    assertEquals(r1, ((TaskInterface) t1.getFcInterface("s1")).getFcRecord());
    assertEquals(r2, ((TaskInterface) t1.getFcInterface("c1")).getFcRecord());
    assertEquals("toto", ((TaskItf) t1.getFcInterface("s1")).getIt());
  }

  public static interface TaskItf {
    String getIt();
  }

  public static class TaskImpl implements TaskItf, BindingController {
    TaskItf c1Itf;

    public String getIt() {
      return "toto";
    }

    public void bindFc(final String clientItfName, final Object serverItf)
        throws NoSuchInterfaceException, IllegalBindingException,
        IllegalLifeCycleException {
      if (clientItfName.equals("c1")) {
        c1Itf = (TaskItf) serverItf;
      }
    }

    public String[] listFc() {
      return new String[]{"c1"};
    }

    public Object lookupFc(final String clientItfName)
        throws NoSuchInterfaceException {
      if (clientItfName.equals("c1")) {
        return c1Itf;
      }
      return null;
    }

    public void unbindFc(final String clientItfName)
        throws NoSuchInterfaceException, IllegalBindingException,
        IllegalLifeCycleException {
      if (clientItfName.equals("c1")) {
        c1Itf = null;
      }
    }
  }
}
