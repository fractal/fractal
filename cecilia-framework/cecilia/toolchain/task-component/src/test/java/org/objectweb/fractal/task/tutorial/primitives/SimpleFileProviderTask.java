/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Ali Erdem Ozcan
 *
 */

package org.objectweb.fractal.task.tutorial.primitives;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.objectweb.fractal.task.core.Executable;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;
import org.objectweb.fractal.task.tutorial.interfaces.CodeProvider;
import org.objectweb.fractal.task.tutorial.interfaces.FileProvider;

// START SNIPPET:Content
@TaskParameters("componentName")
@ServerInterfaces({
    @ServerInterface(name = "file-provider", signature = FileProvider.class, record = "role:file-provider, id:%", parameters = "componentName"),
    @ServerInterface(name = "include-provider", signature = CodeProvider.class, record = "role:include-provider, id:%", parameters = "componentName")})
public class SimpleFileProviderTask
    implements
      Executable,
      FileProvider,
      CodeProvider {

  @ClientInterface(name = "code-provider", record = "role:code-provider, id:%", parameters = "componentName")
  public CodeProvider  sourceCodeProviderItf;

  private final File   outDir;
  private final String fileName;

  public SimpleFileProviderTask(final String componentName, final File outDir) {
    this.outDir = outDir;
    this.fileName = componentName + ".adl.out";
  }

  private File   resultFile;
  private String resultCode;

  public void execute() throws Exception {
    resultFile = new File(outDir, fileName);
    try {
      final BufferedWriter out = new BufferedWriter(new FileWriter(resultFile));
      out.write(sourceCodeProviderItf.getCode());
      out.close();
    } catch (final IOException e) {
      e.printStackTrace();
    }
    resultCode = "#include \"" + fileName + "\"\n";
  }

  public File getFile() {
    return resultFile;
  }

  public String getCode() {
    return resultCode;
  }
}
