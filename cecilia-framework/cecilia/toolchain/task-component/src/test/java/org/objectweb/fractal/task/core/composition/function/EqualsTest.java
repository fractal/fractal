/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.composition.function;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.task.core.composition.Function;
import org.objectweb.fractal.task.core.composition.InvalidNumberOfParameterException;
import org.objectweb.fractal.task.core.composition.NullParameterException;

public class EqualsTest {

  Function equals;

  @Before
  public void init() {
    equals = new Equals();
  }

  @Test
  public void testInvoke() throws Exception {
    assertEquals(Boolean.TRUE, equals.invoke("toto", "toto"));
    assertEquals(Boolean.FALSE, equals.invoke("toto", "tutu"));
    assertEquals(Boolean.FALSE, equals.invoke("toto", new Integer(5)));
    assertEquals(Boolean.TRUE, equals.invoke(new Integer(5), new Integer(5)));
  }

  @Test
  public void testInvalidNumberOfParameterException() {
    try {
      equals.invoke("toto");
      fail("An exception should be thrown here");
    } catch (final Exception e) {
      assertTrue(e instanceof InvalidNumberOfParameterException);
      final InvalidNumberOfParameterException e1 = (InvalidNumberOfParameterException) e;
      assertSame(equals, e1.getFunction());
    }
  }

  @Test
  public void testNullParameterException() {
    try {
      equals.invoke(null, "toto");
      fail("An exception should be thrown here");
    } catch (final Exception e) {
      assertTrue(e instanceof NullParameterException);
      final NullParameterException e1 = (NullParameterException) e;
      assertSame(equals, e1.getFunction());
      assertSame(0, e1.getParamIndex());
    }
  }
}
