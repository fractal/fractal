/**
 * 
 */

package org.objectweb.fractal.task.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.objectweb.fractal.util.Fractal.getGenericFactory;
import static org.objectweb.fractal.util.Fractal.getTypeFactory;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.julia.Julia;

/**
 * @author leclercm
 */
public class JuliaTaskTest {

  private static final String JULIA_TASK_CONFIG_FILE = "etc/julia-task.cfg";

  Component                   taskBootstrapComponent;
  TypeFactory                 taskTypeFactory;
  GenericFactory              taskFactory;

  @Before
  public void setupTaskFactory() throws Exception {
    // get the Julia bootstrap factory
    final Julia julia = new Julia();

    // Create julia bootstrap context.
    final Map<Object, Object> juliaContext = new HashMap<Object, Object>();
    juliaContext.put("julia.config", JULIA_TASK_CONFIG_FILE);

    // create the julia bootstrap component
    taskBootstrapComponent = julia.newFcInstance(null, null, juliaContext);
    taskTypeFactory = getTypeFactory(taskBootstrapComponent);
    taskFactory = getGenericFactory(taskBootstrapComponent);
  }

  @Test
  public void testNewCompositeTask() throws Exception {
    final Record r1 = new Record("f1:v1, f2:v2");
    final InterfaceType itfType1 = taskTypeFactory.createFcItfType("i1",
        TaskItf.class.getName(), false, false, false);
    final ComponentType cType = taskTypeFactory
        .createFcType(new InterfaceType[]{itfType1});

    final Component taskComponent = taskFactory.newFcInstance(cType,
        "task-composite", null);

    final Object taskItf = taskComponent.getFcInterface("i1");
    assertTrue("Task interface is not a TaskInterface",
        taskItf instanceof TaskInterface);
    assertTrue("Task interface does not implement functional interface",
        taskItf instanceof TaskItf);

    ((TaskInterface) taskItf).setFcRecord(r1);
    assertSame("Interface record is not the one that has been set", r1,
        ((TaskInterface) taskItf).getFcRecord());
  }

  @Test
  public void testNewPrimitiveTask() throws Exception {
    final Record r1 = new Record("f1:v1, f2:v2");
    final InterfaceType itfType1 = taskTypeFactory.createFcItfType("i1",
        TaskItf.class.getName(), false, false, false);
    final ComponentType cType = taskTypeFactory
        .createFcType(new InterfaceType[]{itfType1});

    final Component taskComponent = taskFactory.newFcInstance(cType,
        "task-primitive", TaskImpl.class.getName());

    final Object taskItf = taskComponent.getFcInterface("i1");
    assertTrue("Task interface is not a TaskInterface",
        taskItf instanceof TaskInterface);
    assertTrue("Task interface does not implement functional interface",
        taskItf instanceof TaskItf);

    ((TaskInterface) taskItf).setFcRecord(r1);
    assertSame("Interface record is not the one that has been set", r1,
        ((TaskInterface) taskItf).getFcRecord());

    assertEquals("Function task interface does not return correctly", "toto",
        ((TaskItf) taskItf).getIt());
  }

  public static interface TaskItf {
    String getIt();
  }

  public static class TaskImpl implements TaskItf {
    public String getIt() {
      return "toto";
    }
  }
}
