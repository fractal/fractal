/**
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core;

import static org.junit.Assert.assertEquals;
import static org.objectweb.fractal.task.core.RecordTest.checkRecordV1F1V2F2;

import org.junit.Test;

public class RecordTemplateTest {

  @Test
  public void test1() {
    final RecordTemplate rt = new RecordTemplate("{f1:v1, f2:v2}");
    final Record r = rt.instantiate();
    RecordTest.checkRecordV1F1V2F2(r);
  }

  @Test
  public void test2() {
    final RecordTemplate rt = new RecordTemplate("{f1:%, f2:%}");
    Record r = rt.instantiate("v1", "v2");
    checkRecordV1F1V2F2(r);

    // instantiate the template a second time.
    r = rt.instantiate("v1", "v2");
    RecordTest.checkRecordV1F1V2F2(r);
  }

  @Test
  public void test3() {
    final RecordTemplate rt = new RecordTemplate(
        "f1:v1, f2:{f1:{f1:%,f2:v2},f2:%}");
    final Record r = rt.instantiate("v1", "v2");
    assertEquals("v1", r.get("f1"));
    checkRecordV1F1V2F2((Record) ((Record) r.get("f2")).get("f1"));
    assertEquals("v2", ((Record) r.get("f2")).get("f2"));
  }
}
