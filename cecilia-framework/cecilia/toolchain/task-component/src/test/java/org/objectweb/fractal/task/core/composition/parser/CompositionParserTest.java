/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Ali Erdem Ozcan
 */

package org.objectweb.fractal.task.core.composition.parser;

import static org.objectweb.fractal.task.core.composition.TaskCompositionFileLoader.TASK_COMPOSITION_DTD;

import java.io.File;
import java.io.InputStream;

import junit.framework.TestCase;

import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;
import org.objectweb.fractal.task.core.composition.ast.TaskDefinition;

public class CompositionParserTest extends TestCase {
  public static final String TASKS_PACKAGE = "test.task.composition.";

  private XMLNodeFactory     nodeFactory;

  @Override
  protected void setUp() throws Exception {
    nodeFactory = new XMLNodeFactoryImpl();
  }

  /**
   * Tests the composition definition
   * <code> test.task.composition.T1.task </code>.
   * 
   * @throws Exception if test fails
   */
  public void testT1() throws Exception {
    @SuppressWarnings("all")
    final TaskDefinition type = readTaskDefinition("T1");
  }

  /**
   * Tests the composition definition
   * <code> test.task.composition.T2.task </code>.
   * 
   * @throws Exception if test fails
   */
  public void testT2() throws Exception {
    @SuppressWarnings("all")
    final TaskDefinition type = readTaskDefinition("T2");
  }

  /**
   * Tests the composition definition
   * <code> test.task.composition.T3.task </code>.
   * 
   * @throws Exception if test fails
   */
  public void testT3() throws Exception {
    @SuppressWarnings("all")
    final TaskDefinition type = readTaskDefinition("T3");
  }

  private TaskDefinition readTaskDefinition(final String name) throws Exception {
    final String resourceName = (TASKS_PACKAGE + name).replace('.',
        File.separatorChar)
        + ".task";
    final InputStream is = getClass().getClassLoader().getResourceAsStream(
        resourceName);
    if (is == null) fail("Cannot find '" + name + "' type file");
    final TaskCompositionParser parser = new TaskCompositionParser(is);
    parser.init(nodeFactory, TASK_COMPOSITION_DTD, resourceName);
    return parser.getTaskDefinition();
  }

}
