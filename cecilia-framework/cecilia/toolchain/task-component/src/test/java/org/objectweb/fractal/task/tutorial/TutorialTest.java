/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Ali Erdem Ozcan
 */

package org.objectweb.fractal.task.tutorial;

import junit.framework.TestCase;

import org.objectweb.fractal.task.tutorial.steps.Step1;
import org.objectweb.fractal.task.tutorial.steps.Step2;
import org.objectweb.fractal.task.tutorial.steps.Step3;
import org.objectweb.fractal.task.tutorial.steps.Step4;
import org.objectweb.fractal.task.tutorial.steps.Step5;
import org.objectweb.fractal.task.tutorial.steps.Step6;
import org.objectweb.fractal.task.tutorial.steps.Step7;
import org.objectweb.fractal.task.tutorial.steps.Step8;

public class TutorialTest extends TestCase {

  /**
   * Tests the tutorial executable
   * {@link org.objectweb.fractal.task.tutorial.steps.Step1}.
   * 
   * @throws Exception if test fails
   */
  public void testStep1() throws Exception {
    Step1.main(null);
  }

  /**
   * Tests the tutorial executable
   * {@link org.objectweb.fractal.task.tutorial.steps.Step2}.
   * 
   * @throws Exception if test fails
   */
  public void testStep2() throws Exception {
    Step2.main(null);
  }

  /**
   * Tests the tutorial executable
   * {@link org.objectweb.fractal.task.tutorial.steps.Step3}.
   * 
   * @throws Exception if test fails
   */
  public void testStep3() throws Exception {
    Step3.main(null);
  }

  /**
   * Tests the tutorial executable
   * {@link org.objectweb.fractal.task.tutorial.steps.Step4}.
   * 
   * @throws Exception if test fails
   */
  public void testStep4() throws Exception {
    Step4.main(null);
  }

  /**
   * Tests the tutorial executable
   * {@link org.objectweb.fractal.task.tutorial.steps.Step5}.
   * 
   * @throws Exception if test fails
   */
  public void testStep5() throws Exception {
    Step5.main(null);
  }

  /**
   * Tests the tutorial executable
   * {@link org.objectweb.fractal.task.tutorial.steps.Step6}.
   * 
   * @throws Exception if test fails
   */
  public void testStep6() throws Exception {
    Step6.main(null);
  }

  /**
   * Tests the tutorial executable
   * {@link org.objectweb.fractal.task.tutorial.steps.Step7}.
   * 
   * @throws Exception if test fails
   */
  public void testStep7() throws Exception {
    Step7.main(null);
  }

  /**
   * Tests the tutorial executable
   * {@link org.objectweb.fractal.task.tutorial.steps.Step8}.
   * 
   * @throws Exception if test fails
   */
  public void testStep8() throws Exception {
    Step8.main(null);
  }

}
