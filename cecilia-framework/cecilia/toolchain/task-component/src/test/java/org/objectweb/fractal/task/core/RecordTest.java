/**
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * @author leclercm
 */
public class RecordTest {

  @Test
  public void testFieldsIterator() {
    final Record rec = new Record("{f1:v1, f2:v2}");
    int nbField = 0;
    for (final Record.Field field : rec.fields()) {
      nbField++;
      assertNotNull(field);
      assertNotNull(field.getName());
      assertNotNull(field.getValue());
      if (field.getName().equals("f1"))
        assertEquals("v1", field.getValue());
      else if (field.getName().equals("f2"))
        assertEquals("v2", field.getValue());
      else
        fail("Unexpected field in record: " + field);
    }
    assertEquals(2, nbField);
  }

  @Test
  public void testRecordDescriptor1() {
    final Record rec = new Record("f1:v1, f2:v2");
    checkRecordV1F1V2F2(rec);
  }

  @Test
  public void testRecordDescriptor2() {
    final Record rec = new Record("f1   :   v1  ,f2:v2");
    checkRecordV1F1V2F2(rec);
  }

  @Test
  public void testRecordDescriptor3() {
    final Record rec = new Record("f1:v1,f2:v2");
    checkRecordV1F1V2F2(rec);
  }

  @Test
  public void testRecordDescriptor4() {
    final Record rec = new Record("{ f1 : v1 , f2 : v2 }");
    checkRecordV1F1V2F2(rec);
  }

  @Test
  public void testRecordDescriptor5() {
    final Record rec = new Record("f1:v1, f2:%", "v2");
    checkRecordV1F1V2F2(rec);
  }

  @Test
  public void testRecordDescriptor6() {
    final Record rec = new Record("{f1:%, f2:%}", "v1", "v2");
    checkRecordV1F1V2F2(rec);
  }

  @Test
  public void testRecordDescriptor7() {
    final Record rec = new Record("f1:v1, f2:{f1:v1,f2:v2}");
    assertEquals("v1", rec.get("f1"));
    checkRecordV1F1V2F2((Record) rec.get("f2"));
  }

  @Test
  public void testRecordDescriptor8() {
    final Record rec = new Record("f1:{f1:%,f2:%}, f2:v2", "v1", "v2");
    checkRecordV1F1V2F2((Record) rec.get("f1"));
    assertEquals("v2", rec.get("f2"));
  }

  @Test
  public void testRecordDescriptor9() {
    final Record rec = new Record("f1:v1, f2:{f1:{f1:%,f2:v2},f2:%}", "v1",
        "v2");
    assertEquals("v1", rec.get("f1"));
    checkRecordV1F1V2F2((Record) ((Record) rec.get("f2")).get("f1"));
    assertEquals("v2", ((Record) rec.get("f2")).get("f2"));
  }

  static void checkRecordV1F1V2F2(final Record rec) {
    assertEquals(2, rec.getFieldCount());
    assertEquals("v1", rec.get("f1"));
    assertEquals("v2", rec.get("f2"));
  }
}
