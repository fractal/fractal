/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Ali Erdem Ozcan
 *
 */

package org.objectweb.fractal.task.tutorial.steps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.task.tutorial.interfaces.CodeProvider;
import org.objectweb.fractal.task.tutorial.primitives.SimpleCodeProviderTask;

// START SNIPPET:Content
public class Step5 extends AbstractTutorialStep {

  public static void main(final String[] args) throws Exception {
    init();
    final List<Component> subTaskList = new ArrayList<Component>();
    for (int i = 0; i < 10; i++) {
      final String component = "component-" + i;
      subTaskList.add(taskFactory.newPrimitiveTask(
          new SimpleCodeProviderTask(component), component));
    }

    final Component compositeTask = taskFactory.newCompositeTask(
        subTaskList, "tutorial.composites.RenameExportComposition",
        new HashMap<Object, Object>(), new Object[0]);

    final Object[] interfaces = compositeTask.getFcInterfaces();
    for (final Object itf : interfaces) {
      if (itf instanceof CodeProvider) {
        System.out.println(((CodeProvider) itf).getCode());
      }
    }
  }
}
