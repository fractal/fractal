/***
 * Cecilia ADL Task Framework
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core.composition.function;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.task.core.composition.Function;
import org.objectweb.fractal.task.core.composition.InvalidNumberOfParameterException;

public class SameTest {

  Function same;

  @Before
  public void init() {
    same = new Same();
  }

  @Test
  public void testInvoke() throws Exception {
    final Object o = new Object();
    assertEquals(Boolean.TRUE, same.invoke(o, o));
    assertEquals(Boolean.FALSE, same.invoke(o, "toto"));
    assertEquals(Boolean.TRUE, same.invoke("toto", new String("toto")));
    assertEquals(Boolean.TRUE, same.invoke(new Integer(5), new Integer(5)));
    assertEquals(Boolean.TRUE, same
        .invoke(new Boolean(true), new Boolean(true)));
  }

  @Test
  public void testInvalidNumberOfParameterException() {
    try {
      same.invoke("toto");
      fail("An exception should be thrown here");
    } catch (final Exception e) {
      assertTrue(e instanceof InvalidNumberOfParameterException);
      final InvalidNumberOfParameterException e1 = (InvalidNumberOfParameterException) e;
      assertSame(same, e1.getFunction());
    }
  }
}
