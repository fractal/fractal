/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Ali Erdem Ozcan
 *
 */

package org.objectweb.fractal.task.tutorial.steps;

import java.io.File;

import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;
import org.objectweb.fractal.task.core.TaskFactory;
import org.objectweb.fractal.task.core.composition.FunctionLoader;
import org.objectweb.fractal.task.core.composition.Interpreter;
import org.objectweb.fractal.task.core.composition.TaskCompositionFileLoader;
import org.objectweb.fractal.task.core.composition.VariableLoader;
import org.objectweb.fractal.task.core.internal.BasicTaskFactory;
import org.objectweb.fractal.task.core.primitive.BasicPrimitiveTaskFactory;
import org.xml.sax.SAXException;

public abstract class AbstractTutorialStep {

  protected static TaskFactory taskFactory;

  protected static File        outDir;

  protected static void init() throws SAXException {
    final XMLNodeFactory nodeFactory = new XMLNodeFactoryImpl();
    final TaskCompositionFileLoader fileLoader = new TaskCompositionFileLoader();
    final VariableLoader variableLoader = new VariableLoader();
    final FunctionLoader functionLoader = new FunctionLoader();
    final Interpreter interpreter = new Interpreter();
    final BasicPrimitiveTaskFactory primitiveFactory = new BasicPrimitiveTaskFactory();
    final BasicTaskFactory basicTaskFactory = new BasicTaskFactory();

    // Bindings
    fileLoader.nodeFactoryItf = nodeFactory;
    variableLoader.clientLoader = fileLoader;
    functionLoader.clientLoader = variableLoader;
    interpreter.clientLoaderItf = functionLoader;
    basicTaskFactory.compositeTaskFactoryItf = interpreter;
    basicTaskFactory.primitiveTaskFactoryItf = primitiveFactory;

    taskFactory = basicTaskFactory;

    // initialize node factory.
    nodeFactory.checkDTD(TaskCompositionFileLoader.TASK_COMPOSITION_DTD);

  }

  protected static void init(final String outDirName) throws SAXException {
    // Create output dir
    outDir = new File("target/out/" + outDirName);
    outDir.mkdirs();
    init();
  }

  protected AbstractTutorialStep() {
  }
}
