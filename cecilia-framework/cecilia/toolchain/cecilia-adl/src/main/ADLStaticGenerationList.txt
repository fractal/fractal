# This file contains the list of ADL definitions that should be statically 
# generated using the maven-fractaladl-plugin

# The cecilia basic factory with "Java" plugin factory backend.
org.objectweb.fractal.cecilia.adl.CeciliaBasicFactory(Java)

# primitive thinkMC plugins
org.objectweb.fractal.cecilia.primitive.thinkMC.ImplementationCodeLoader
org.objectweb.fractal.cecilia.primitive.thinkMC.CPrimitiveControllerLoader
org.objectweb.fractal.cecilia.primitive.thinkMC.Visitor
org.objectweb.fractal.cecilia.primitive.thinkMC.CPrimitiveDefinitionVisitor
org.objectweb.fractal.cecilia.primitive.thinkMC.CPrimitiveInstantiationVisitor

# primitive optC plugins
org.objectweb.fractal.cecilia.primitive.optC.ImplementationCodeLoader
org.objectweb.fractal.cecilia.primitive.optC.CPrimitiveControllerLoader
org.objectweb.fractal.cecilia.primitive.optC.Visitor
org.objectweb.fractal.cecilia.primitive.optC.CPrimitiveDefinitionVisitor
org.objectweb.fractal.cecilia.primitive.optC.CPrimitiveInstantiationVisitor

# merged primitive optC plugins
org.objectweb.fractal.cecilia.primitive.optC.CMergedPrimitiveControllerLoader
org.objectweb.fractal.cecilia.primitive.optC.CMergedPrimitiveDefinitionVisitor
org.objectweb.fractal.cecilia.primitive.optC.CMergedPrimitiveInstantiationVisitor

# composite plugins
org.objectweb.fractal.cecilia.composite.CCompositeControllerLoader
org.objectweb.fractal.cecilia.composite.CCompositeVisitor

# boot plugins
org.objectweb.fractal.cecilia.composite.CBootControllerLoader
org.objectweb.fractal.cecilia.composite.CBootVisitor
