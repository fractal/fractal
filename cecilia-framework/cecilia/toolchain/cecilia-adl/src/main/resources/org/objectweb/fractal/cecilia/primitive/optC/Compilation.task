Compilation(componentNode, isArchiveOutput, doLink) {

  // ---------------------------------------------------------------------------
  // Internal bindings
  // ---------------------------------------------------------------------------

  // bind aggregator task to the archive task
  // this rule apply only if the output of the component is an archive file.
  bind {role:"aggregatorInputFile", id:componentNode}
    to {role:"archiveFile", id:componentNode}
    where isArchiveOutput;

  // bind archive task to compilation tasks
  // this rule apply only if the output of the component is an archive file.
  //   use ANY as id of compilation tasks since it may be componentNode or a 
  //   module node.
  bind {role:"compiledFile", id:componentNode}
    to {role:"compiledFile", id:ANY}
    where isArchiveOutput;

  // bind aggregator task to compilation tasks
  // this rule apply only if the output of the component is NOT an archive file.
  //   use ANY as id of compilation tasks since it may be componentNode or a 
  //   module node.
  bind {role:"aggregatorInputFile", id:componentNode}
    to {role:"compiledFile", id:ANY}
    where not(isArchiveOutput);

  // bind compilation tasks to assembly file providers (if any)  
  bind {role:"sourceFile", id:moduleClient}
    to {role:"componentAssemblyModuleFile", id:componentNode, module:moduleServer}
    where same(moduleClient, moduleServer);

  // ---------------------------------------------------------------------------
  // Exported interfaces
  // ---------------------------------------------------------------------------

  // export CollectionFileProvider interface of aggregator task (only if not 
  // doing link).
  export server {role:"outputFiles", id:componentNode}
    where not(doLink);

  // bind link task to aggregator task (only if doing link).
  bind {role:"compiledFileCollection", id:componentNode}
    to {role:"outputFiles", id:componentNode}
    where doLink;

  // export link task (only if doing link).
  export server {role:"linkedFile", id:componentNode}
    where doLink
    as {role="outputFiles", id=componentNode};

  // export dependencies to module source file provider task.
  export client {role:"sourceFile", id:module}
    where not(same(module, componentNode))
    as {role="componentModuleFile", id=componentNode, module=module};

  // export dependencies to source file provider task.
  export client {role:"sourceFile", id:componentNode}
    as {role="componentDefinitionFile", id=componentNode};

  // export dependencies to type name provider
  export client {role:"typeNameProvider", id:componentNode};

  // export dependency to compiled controller implementations (only if doing 
  // link).
  // export dependency to compiled runtime implementations (if any).
  export client {role:"runtimeCompiledFile", id:componentNode}
    where doLink;
  export client {role:"runtimeCompiledFileCollection", id:componentNode}
    where doLink;
}
