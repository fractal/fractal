<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE definition PUBLIC "-//objectweb.org//DTD Fractal ADL 2.0//EN"
  "classpath://org/objectweb/fractal/adl/xml/standard.dtd">

<definition name="org.objectweb.fractal.cecilia.primitive.optC.Visitor"
  extends="org.objectweb.fractal.cecilia.adl.AbstractComponentVisitorPlugin(org.objectweb.fractal.cecilia.primitive.optC.ThinkMC)">

  <interface name="compilation-task-factory" role="client"
    signature="org.objectweb.fractal.cecilia.adl.compiler.CompilationTaskFactory" />

  <!-- ===================================================================== -->
  <!-- Type Definition Visitor                                               -->
  <!-- ===================================================================== -->
  <component name="type-definition"
    definition="org.objectweb.fractal.adl.ComponentVisitorType">

    <interface name="plugin-factory" role="client"
      signature="org.objectweb.fractal.adl.Factory" />

    <interface name="compilation-task-factory" role="client"
      signature="org.objectweb.fractal.cecilia.adl.compiler.CompilationTaskFactory" />

    <!-- Plugin manager used to load definition plugins -->
    <component name="plugin-manager"
      definition="org.objectweb.fractal.cecilia.adl.plugin.PluginManager">
      <interface name="task-factory" role="client"
        signature="org.objectweb.fractal.task.core.TaskFactory" />
    </component>

    <!-- type filter determines if the type of the component is already defined 
      or not -->
    <component name="type-filter"
      definition="org.objectweb.fractal.cecilia.adl.types.PrimitiveTypeDefinitionFilter" />

    <component name="dispatcher"
      definition="org.objectweb.fractal.adl.ComponentVisitorDispatcher(org.objectweb.fractal.cecilia.primitive.optC.Definition)" />

    <!-- create the typeNameProvider task. -->
    <component name="type-visitor"
      definition="org.objectweb.fractal.adl.ComponentVisitorType">
      <content class="org.objectweb.fractal.cecilia.adl.types.TypeVisitor" />
    </component>

    <component name="data-definition-selector"
      definition="org.objectweb.fractal.cecilia.adl.controllers.VisitorSelectorForController">
      <attributes>
        <attribute name="ADLPackage"
          value="org.objectweb.fractal.cecilia.primitive.optC" />
        <attribute name="ADLSuffix" value="DefinitionVisitor" />
      </attributes>
    </component>

    <component name="implementation"
      definition="org.objectweb.fractal.adl.ComponentVisitorType">
      <content
        class="org.objectweb.fractal.cecilia.adl.implementations.ImplementationVisitor" />
    </component>

    <component name="macro-definition"
      definition="org.objectweb.fractal.adl.ComponentVisitorType">
      <content
        class="org.objectweb.fractal.cecilia.primitive.optC.implementations.MacroDefinitionVisitor" />
    </component>

    <component name="merged-macro-definition"
      definition="org.objectweb.fractal.adl.ComponentVisitorType">
      <content
        class="org.objectweb.fractal.cecilia.primitive.optC.implementations.MergedMacroDefinitionVisitor" />
    </component>

    <component name="vft-declaration"
      definition="org.objectweb.fractal.adl.ComponentVisitorType">
      <content
        class="org.objectweb.fractal.cecilia.primitive.optC.types.ServerInterfaceVFTInstantiationVisitor" />
    </component>

    <component name="source-file"
      definition="org.objectweb.fractal.adl.ComponentVisitorType">
      <content
        class="org.objectweb.fractal.cecilia.primitive.optC.source.SourceFileVisitor" />
    </component>

    <component name="compilation"
      definition="org.objectweb.fractal.adl.ComponentVisitorType">
      <interface name="compilation-task-factory" role="client"
        signature="org.objectweb.fractal.cecilia.adl.compiler.CompilationTaskFactory" />
      <content
        class="org.objectweb.fractal.cecilia.primitive.optC.source.CompilationVisitor" />
    </component>

    <binding client="this.visitor" server="type-filter.visitor" />
    <binding client="type-filter.definition-visitor"
      server="dispatcher.visitor" />

    <binding client="dispatcher.client-visitor-1" server="type-visitor.visitor" />
    <binding client="dispatcher.client-visitor-2"
      server="data-definition-selector.visitor" />
    <binding client="dispatcher.client-visitor-3"
      server="implementation.visitor" />
    <binding client="dispatcher.client-visitor-4"
      server="macro-definition.visitor" />
    <binding client="dispatcher.client-visitor-5"
      server="merged-macro-definition.visitor" />
    <binding client="dispatcher.client-visitor-6"
      server="vft-declaration.visitor" />
    <binding client="dispatcher.client-visitor-7" server="source-file.visitor" />
    <binding client="dispatcher.client-visitor-8" server="compilation.visitor" />

    <binding client="dispatcher.task-factory" server="this.task-factory" />
    <binding client="type-visitor.task-factory" server="this.task-factory" />
    <binding client="plugin-manager.task-factory" server="this.task-factory" />
    <binding client="implementation.task-factory" server="this.task-factory" />
    <binding client="macro-definition.task-factory" server="this.task-factory" />
    <binding client="merged-macro-definition.task-factory" server="this.task-factory" />
    <binding client="vft-declaration.task-factory" server="this.task-factory" />
    <binding client="source-file.task-factory" server="this.task-factory" />
    <binding client="compilation.task-factory" server="this.task-factory" />

    <binding client="compilation.compilation-task-factory"
      server="this.compilation-task-factory" />

    <binding client="data-definition-selector.plugin-manager"
      server="plugin-manager.plugin-manager" />

    <binding client="plugin-manager.plugin-factory"
      server="this.plugin-factory" />
  </component>

  <!-- ===================================================================== -->
  <!-- Instance Declaration Visitor                                          -->
  <!-- ===================================================================== -->
  <component name="instance-declaration"
    definition="org.objectweb.fractal.adl.ComponentVisitorType">
    <interface name="plugin-factory" role="client"
      signature="org.objectweb.fractal.adl.Factory" />

    <!-- Plugin manager used to load definition plugins -->
    <component name="plugin-manager"
      definition="org.objectweb.fractal.cecilia.adl.plugin.PluginManager">
      <interface name="task-factory" role="client"
        signature="org.objectweb.fractal.task.core.TaskFactory" />
    </component>

    <component name="dispatcher"
      definition="org.objectweb.fractal.adl.ComponentVisitorDispatcher(org.objectweb.fractal.cecilia.primitive.optC.Instantiation)" />

    <component name="instance-visitor"
      definition="org.objectweb.fractal.adl.ComponentVisitorType">
      <content
        class="org.objectweb.fractal.cecilia.adl.components.InstanceVisitor" />
    </component>

    <component name="data-instantiation-selector"
      definition="org.objectweb.fractal.cecilia.adl.controllers.VisitorSelectorForController">
      <attributes>
        <attribute name="ADLPackage"
          value="org.objectweb.fractal.cecilia.primitive.optC" />
        <attribute name="ADLSuffix" value="InstantiationVisitor" />
      </attributes>
    </component>

    <binding client="this.visitor" server="dispatcher.visitor" />
    <binding client="dispatcher.client-visitor-1"
      server="instance-visitor.visitor" />
    <binding client="dispatcher.client-visitor-2"
      server="data-instantiation-selector.visitor" />

    <binding client="dispatcher.task-factory" server="this.task-factory" />
    <binding client="instance-visitor.task-factory" server="this.task-factory" />
    <binding client="plugin-manager.task-factory" server="this.task-factory" />
    <binding client="data-instantiation-selector.plugin-manager"
      server="plugin-manager.plugin-manager" />

    <binding client="plugin-manager.plugin-factory"
      server="this.plugin-factory" />
  </component>

  <!-- =================================================== -->
  <!--         Dispatcher Bindings                         -->
  <!-- =================================================== -->
  <binding client="dispatcher.client-visitor-1"
    server="type-definition.visitor" />
  <binding client="dispatcher.client-visitor-2"
    server="instance-declaration.visitor" />

  <!-- =================================================== -->
  <!--              Other Bindings                         -->
  <!-- =================================================== -->
  <binding client="type-definition.task-factory" server="this.task-factory" />
  <binding client="type-definition.plugin-factory" server="this.plugin-factory" />
    <binding client="type-definition.compilation-task-factory"
      server="this.compilation-task-factory" />

  <binding client="instance-declaration.task-factory"
    server="this.task-factory" />
  <binding client="instance-declaration.plugin-factory"
    server="this.plugin-factory" />
</definition>
