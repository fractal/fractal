/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.primitive.optC.types;

import static org.objectweb.fractal.api.type.TypeFactory.OPTIONAL;

import java.util.List;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractInstantiationTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.interfaces.AbstractInterfaceVisitor;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the instantiation of the interfaces of the
 * component for <code>ThinkMC</code> dialect.
 */
public class InterfaceInstantiationVisitor extends AbstractInterfaceVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractInterfaceVisitor
  // ---------------------------------------------------------------------------

  /**
   * Creates a task that writes the source code containing the interface
   * instantiations.
   */
  @Override
  protected Component createTask(final ComponentContainer container,
      final List<TypeInterface> itfs) throws ADLException, TaskException {
    return taskFactoryItf.newPrimitiveTask(new InterfaceDefinitonTask(),
        container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds definition source code for interface instantiation for the given
   * component node. This task provides the produced source code. It uses the
   * source code that is provided for both exported and imported interface
   * instantiations by provider components.
   */
  @TaskParameters({"componentNode"})
  @ServerInterfaces(@ServerInterface(name = "source-code-provider", signature = SourceCodeProvider.class, record = "role:interfaceInstantiation, id:%", parameters = "componentNode"))
  public static class InterfaceDefinitonTask extends AbstractInstantiationTask {

    /**
     * Source code to be included for server interface instantiation.
     */
    @ClientInterface(name = "server-itf-instance", contingency = OPTIONAL, record = "role:serverInterfaceInstantiation, id:%", parameters = "componentNode")
    public SourceCodeProvider serverItfInstancesItf;

    /**
     * Source code to be included for client interface instantiation.
     */
    @ClientInterface(name = "client-itf-instance", contingency = OPTIONAL, record = "role:clientInterfaceInstantiation, id:%", parameters = "componentNode")
    public SourceCodeProvider clientItfInstancesItf;

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter("Primitive Interfaces Instantiation Builder");
      cw.append("{").endl();
      if (serverItfInstancesItf != null) {
        cw.append(serverItfInstancesItf.getSourceCode()).append(',').endl();
      }
      if (clientItfInstancesItf != null) {
        cw.appendln(clientItfInstancesItf.getSourceCode());
      }
      cw.append("}").endl();
      return cw.toString();
    }
  }
}
