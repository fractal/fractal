/***
 * Cecilia ADL Compiler
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.bindings;

import org.objectweb.fractal.adl.bindings.Binding;

/**
 * A {@link Binding} subtype which add a <code>dynamic="{true|false}"</code>
 * attribute to indicate if the given binding is meant to be
 * <em>dynamically</em> changed during the application lifetime (default
 * should be considered "true").
 * 
 * @author Alessio Pace
 */
public interface ExtendedBinding extends Binding {

  /**
   * @return true if this binding can possibly change during the execution
   *         lifetime, false otherwise.
   */
  String getDynamic();

  /**
   * @param dynamic "true" or "false".
   */
  void setDynamic(String dynamic);
}
