/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.primitive.optC.implementations;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;
import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationHelper.hasConstructor;
import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationHelper.hasDestructor;
import static org.objectweb.fractal.cecilia.adl.SourceCodeHelper.appendSortedSourceCodes;
import static org.objectweb.fractal.cecilia.adl.SourceCodeHelper.appendReverseSortedSourceCodes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

public class FactoryImplementationVisitor extends AbstractTaskFactoryUser
    implements
      ComponentVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link ComponentContainer} nodes and creates a task that includes
   * the implementation file, and that defines some macros that are used in the
   * implementation file.
   */
  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    if (castNodeError(container, ImplementationContainer.class)
        .getImplementation() == null) {
      throw new CompilerError(GenericErrors.INTERNAL_ERROR,
          "This visitor is only applicable for primitive component.");
    }
    return taskFactoryItf.newPrimitiveTask(new FactoryImplementationTask(
        hasConstructor(container), hasDestructor(container)), container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds implementation source code of the factory interface for the given
   * component node.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "factory-implementation", signature = SourceCodeProvider.class, record = "role:factoryImplementation, id:%", parameters = "componentNode"))
  public static class FactoryImplementationTask extends AbstractDefinitionTask {

    protected final boolean hasConstructor;
    protected final boolean hasDestructor;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param hasConstructor <code>true</code> if the component has a
     *          constructor.
     * @param hasDestructor <code>true</code> if the component has a destructor.
     */
    public FactoryImplementationTask(final boolean hasConstructor,
        final boolean hasDestructor) {
      this.hasConstructor = hasConstructor;
      this.hasDestructor = hasDestructor;
    }

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /**
     * Client collection interface used to retrieve the implementation code
     * pieces that contribute local-variable declarations to the 'newFcInstance'
     * method.
     */
    @ClientInterface(name = "factory-instantiate-declaration", signature = SourceCodeProvider.class, record = "role:factoryInstantiateDeclaration, id:%", parameters = "componentNode")
    public final Map<String, SourceCodeProvider> factoryInstantiateDeclarationProviderItfs = new HashMap<String, SourceCodeProvider>();

    /**
     * Client collection interface used to retrieve the implementation code
     * pieces that contribute 'sizeof' in local variable declarations to the
     * 'newFcInstance' method.
     */
    @ClientInterface(name = "factory-instantiate-sizeof", signature = SourceCodeProvider.class, record = "role:factoryInstantiateSizeof, id:%", parameters = "componentNode")
    public final Map<String, SourceCodeProvider> factoryInstantiateSizeofProviderItfs      = new HashMap<String, SourceCodeProvider>();

    /**
     * Client collection interface used to retrieve the implementation code
     * pieces that contribute to the 'newFcInstance' method.
     */
    @ClientInterface(name = "factory-instantiate-codes-pieces", signature = SourceCodeProvider.class, record = "role:factoryInstantiatePiece, id:%", parameters = "componentNode")
    public final Map<String, SourceCodeProvider> factoryInstantiatePieceProviderItfs       = new HashMap<String, SourceCodeProvider>();

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter(
          "Primitive Factory Implementation Builder");
      final String componentCName = typeNameProviderItf.getCTypeName();

      cw.append("#include <string.h>").endl();

      // instantiate method implementation
      // ---------------------------------------
      cw.append("int METHOD(factory, newFcInstance)(")
          .append("Rfractal_api_Component ** instance) {").endl();
      cw.append("struct ").append(componentCName).append("_t *originalComp = ")
          .append("(struct ").append(componentCName).append("_t *) _this;")
          .endl();
      cw.append("struct ").append(componentCName).append("_t *newComp = (")
          .append("struct ").append(componentCName).append(
              "_t *) CLIENT(factory_allocator, alloc) (").append(
          "sizeof(struct ").append(componentCName).appendln("_t)");

      appendReverseSortedSourceCodes(cw, factoryInstantiateSizeofProviderItfs
          .values());

      cw.appendln(");");
      cw.appendln("unsigned char *_ptr = (unsigned char *)newComp;");

      appendReverseSortedSourceCodes(cw,
          factoryInstantiateDeclarationProviderItfs.values());

      cw.append("memcpy(newComp, originalComp, sizeof(struct ").append(
          componentCName).append("_t));").endl();
      cw.append("_ptr += sizeof(struct ").append(componentCName).append("_t);")
          .endl();

      appendReverseSortedSourceCodes(cw, factoryInstantiatePieceProviderItfs
          .values());

      cw.append("*instance = & (newComp->type.exported.component);").endl();
      if (hasConstructor) {
        cw.append(componentCName).append("_constructor(newComp); ").endl();
      }

      cw.append("return fractal_api_ErrorConst_OK;").endl();

      cw.append("}").endl().endl();

      // destroy method implementation
      // -------------------------------------------
      cw.append("int METHOD(factory, destroyFcInstance)").append(
          "(Rfractal_api_Component *comp) {").endl();
      cw.append("struct ").append(componentCName)
          .append("_t *originalComp = (").append("struct ").append(
              componentCName).append("_t *) comp;").endl();

      if (hasDestructor) {
        cw.append(componentCName).append("_destructor(comp); ").endl();
      }

      cw.appendln(
          "CLIENT(factory_allocator, free) ((jbyte *) originalComp);");
      cw.append("return fractal_api_ErrorConst_OK;").endl();

      cw.append("}").endl().endl();

      return cw.toString();
    }
  }
}
