/***
 * Cecilia ADL Compiler
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.objectweb.fractal.cecilia.adl.file.CodeWriter;

/**
 * Helper class that provides utility methods to work with
 * {@link SourceCodeProvider}.
 */
public final class SourceCodeHelper {
  private SourceCodeHelper() {
  }

  /**
   * Appends on the given {@link CodeWriter}, the code pieces provided by the
   * given collection of provider. Each code pieces are separated by a
   * {@link CodeWriter#endl() line break}.
   *
   * @param cw the {@link CodeWriter} on which the code pieces will be written.
   * @param providers the source code providers.
   */
  public static void appendSourceCodes(final CodeWriter cw,
      final Collection<SourceCodeProvider> providers) {
    for (final SourceCodeProvider provider : providers) {
      cw.append(provider.getSourceCode()).endl();
    }
  }

  /**
   * Appends in alphabetic order on the given {@link CodeWriter}, the code
   * pieces provided by the given collection of provider. Code pieces are
   * appended in alphabetic order and are separated by a
   * {@link CodeWriter#endl() line break}.
   *
   * @param cw the {@link CodeWriter} on which the code pieces will be written.
   * @param providers the source code providers.
   */
  public static void appendSortedSourceCodes(final CodeWriter cw,
      final Collection<SourceCodeProvider> providers) {
    for (final String code : getSortedSourceCodes(providers)) {
      cw.append(code).endl();
    }
  }

  /**
   * Appends in alphabetic order on the given {@link CodeWriter}, the code
   * pieces provided by the given collection of provider. Code pieces are
   * appended in alphabetic order and are separated by a
   * {@link CodeWriter#endl() line break}.
   *
   * @param cw the {@link CodeWriter} on which the code pieces will be written.
   * @param providers the source code providers.
   */
  public static void appendReverseSortedSourceCodes(final CodeWriter cw,
      final Collection<SourceCodeProvider> providers) {
    final List<String> codePieces = Arrays.asList(
        getSortedSourceCodes(providers));
    Collections.reverse(codePieces);
    for (final String code : codePieces) {
      cw.append(code).endl();
    }
  }

  /**
   * Returns an array containing the code pieces provided by the given
   * collection of provider. The returned array is sorted in alphabetic order.
   *
   * @param providers a collection of source code provider.
   * @return the provided source codes sorted in the alphabetic order.
   */
  public static String[] getSortedSourceCodes(
      final Collection<SourceCodeProvider> providers) {
    final String[] codes = new String[providers.size()];
    int i = 0;
    for (final SourceCodeProvider provider : providers) {
      codes[i++] = provider.getSourceCode();
    }
    Arrays.sort(codes);
    return codes;
  }
}
