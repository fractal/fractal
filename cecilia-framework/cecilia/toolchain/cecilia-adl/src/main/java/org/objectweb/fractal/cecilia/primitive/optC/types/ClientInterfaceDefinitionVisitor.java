/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 * Contributor: Alessio Pace (static binding optimizations)
 */

package org.objectweb.fractal.cecilia.primitive.optC.types;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;
import static org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil.getBoundTo;
import static org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil.getContainer;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.InstanceNameProvider;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.TypeNameProvider;
import org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.Method;
import org.objectweb.fractal.cecilia.adl.idl.ast.MethodContainer;
import org.objectweb.fractal.cecilia.adl.interfaces.AbstractClientInterfaceVisitor;
import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;
import org.objectweb.fractal.cecilia.adl.types.TypeDecorationUtil;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterfaceForEach;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the definition of the client interfaces of the
 * component for the <code>ThinkMC</code> dialect.
 */
public class ClientInterfaceDefinitionVisitor
    extends
      AbstractClientInterfaceVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractClientInterfaceVisitor
  // ---------------------------------------------------------------------------

  /**
   * Creates a task that writes the source code containing the definition of the
   * client interfaces of the component.
   */
  @Override
  protected Component createTask(final ComponentContainer container,
      final List<TypeInterface> clientInterfaces) throws ADLException,
      TaskException {

    final List<IDLDefinition> clientInterfaceDefinitions = new ArrayList<IDLDefinition>();
    final Set<Object> serverComponents = new HashSet<Object>();

    for (final Interface itf : clientInterfaces) {
      clientInterfaceDefinitions.add(castNodeError(itf,
          IDLDefinitionContainer.class).getIDLDefinition());
    }
    for (final TypeInterface clientInterface : clientInterfaces) {
      final Interface serverItf = BindingDecorationUtil.isDynamic(clientInterface)
          ? null
          : getBoundTo(clientInterface);
      if (serverItf != null) {
        serverComponents.add(getContainer(serverItf));
      }
    }

    return taskFactoryItf.newPrimitiveTask(new ClientInterfaceDefinitionTask(
        clientInterfaces), container, clientInterfaceDefinitions, serverComponents);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds definition source code for the definition of the client interfaces
   * for the given component node. This task provides the produced source code.
   */
  @TaskParameters({"componentNode", "importedInterfaceDefinitions", "serverComponents"})
  @ServerInterfaces(@ServerInterface(name = "source-code-provider", signature = SourceCodeProvider.class, record = "role:clientInterfaceDefinition, id:%", parameters = "componentNode"))
  public static class ClientInterfaceDefinitionTask
      extends
        AbstractDefinitionTask {

    protected String[] decodedNames = {"CLIENT", "CLIENT_OUTER"};
    protected final List<TypeInterface>                 importedItfNodes;

    // -------------------------------------------------------------------------
    // Client interfaces
    // -------------------------------------------------------------------------

    /**
     * Source code to be included to access the interface definition header
     * files (idl.h) for each imported interface of the componentNode.
     */
    @ClientInterfaceForEach(iterable = "importedInterfaceDefinitions", prefix = "idl-header", signature = SourceCodeProvider.class, record = "role:cInterfaceDefinition, id:%", parameters = "importedInterfaceDefinitions.element")
    public final Map<TypeInterface, SourceCodeProvider> cInterfaceDefinitionItfs      = new LinkedHashMap<TypeInterface, SourceCodeProvider>();

    /**
     * The client interface used to retrieve the type name of the server
     * components to which client interfaces are bound to.
     */
    @ClientInterfaceForEach(iterable = "serverComponents", prefix = "server-type-name-provider", signature = TypeNameProvider.class, record = "role:dynamicFalseBindingTypeNameProvider, id:%", parameters = "serverComponents.element")
    public final Map<Object, TypeNameProvider>          serverTypeNameProviderItf     = new LinkedHashMap<Object, TypeNameProvider>();

    /**
     * The client interfaces used to retrieve the instance name of the server
     * components to which client interfaces are bound to.
     */
    @ClientInterfaceForEach(iterable = "serverComponents", prefix = "server-instance-name-provider", signature = InstanceNameProvider.class, record = "role:dynamicFalseBindingInstanceNameProvider, id:%", parameters = "serverComponents.element")
    public final Map<Object, InstanceNameProvider>      serverInstanceNameProviderItf = new LinkedHashMap<Object, InstanceNameProvider>();

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param importedItfNodes the list of imported interface nodes belonging to
     *            the component.
     */
    public ClientInterfaceDefinitionTask(
        final List<TypeInterface> importedItfNodes) {
      this.importedItfNodes = importedItfNodes;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter(
          "Primitive Client Interface Definition Builder");

      /* this component type C name */
      final String componentCName = typeNameProviderItf.getCTypeName()
          .replace('.', '_');

      /* whether this component type is a singleton */
      Boolean isSingleton = false;

      if (importedItfNodes.size() > 0) {
        final TypeInterface aClientItf = importedItfNodes.get(0);
        final ComponentContainer owner = (ComponentContainer)InterfaceDecorationUtil
            .getContainer(aClientItf);

        final ComponentContainer type = TypeDecorationUtil
            .getTypeDecoration(owner);

        isSingleton = TypeDecorationUtil.isSingletonComponentType(type);
      }

      /*
       * Iterate over client interfaces
       */
      for (final TypeInterface itf : importedItfNodes) {
        /*
         * Get the AST interface definition which is contained by the
         * TypeInterface
         */
        final IDLDefinition itfDef = castNodeError(itf,
            IDLDefinitionContainer.class).getIDLDefinition();
        cw.appendln(cInterfaceDefinitionItfs.get(itfDef).getSourceCode());

        /*
         * For each method of the interface..
         */
        final Method[] itfMethods = ((MethodContainer) itfDef)
            .getMethods();

        /* the interface named escaping each "-" with a "_" */
        final String escapedItfName = itf.getName().replaceAll("-", "_");

        final Boolean isDynamic = BindingDecorationUtil.isDynamic((Node) itf);

        /* fully canonical server component type name (with dots) */
        String serverTypeWithDots = null;
        /* the C type name for the canonical type name (escaping the dots with '_' ) */
        String serverTypeEscapedName = null;
        /* server instance name */
        String serverInstanceEscapedName = null;

        String serverItfEscapedName = null;

        final String clientItfEscapedName = itf.getName().replace('-', '_');

        /* if dynamic="false" */
        if (!isDynamic) {
          System.out.println("The client itf '" + itf.getName()
              + "' will have optimized CLIENT macro(s)");

          final Interface serverItf = getBoundTo(itf);
          Object serverComponent = getContainer(serverItf);
          if (serverComponent == null) {
            throw new IllegalStateException("serverItf has no container");
          }
          
          final TypeNameProvider serverTypeName = serverTypeNameProviderItf.get(serverComponent);
          if (serverTypeName == null) {
            throw new IllegalStateException("No serverTypeName for component " + serverComponent);
          }
          serverTypeWithDots = serverTypeName.getTypeName().replaceAll("/",".");
          serverTypeEscapedName = serverTypeName.getCTypeName();

          final InstanceNameProvider serverInstanceName = serverInstanceNameProviderItf.get(
              serverComponent);
          if (serverInstanceName == null) {
            throw new IllegalStateException("No serverInstanceName for component " + serverComponent);
          }
          serverInstanceEscapedName = serverInstanceName.getCInstanceName().replaceAll("[.]", "_");

          
          /*
           * the name of the server interface to which this client interface is
           * boundTo (it will be used in the CLIENT macro definition)
           */
          serverItfEscapedName = InterfaceDecorationUtil.getBoundTo(itf)
              .getName().replace('-', '_');

          cw.append("#include \"").append(serverTypeEscapedName).appendln(
              ".adl.h\"");
          cw.append("#ifndef ").append(serverTypeEscapedName).appendln("_DECLARED");
          cw.append("extern struct ").append(serverTypeEscapedName).append(
              "_exporteds ").append(serverInstanceEscapedName).appendln(";");
          cw.append("#define ").append(serverTypeEscapedName).appendln("_DECLARED");
          cw.appendln("#endif");

          cw.appendln("/* CLIENT_itfName macro definition in case of dynamic='false' */");
          cw.appendln("#define CLIENT_" + clientItfEscapedName + " (&("
                      + serverInstanceEscapedName + "." + serverItfEscapedName
                      + "))").endl();
        }
        /* else if it is dynamic="true" binding */
        else {
          if (isSingleton) {
            cw.appendln("/* CLIENT_itfName macro definition in case of dynamic='true' and singleton */");
            for (final String macroName : decodedNames) {
              cw.append("#define ").append(macroName).append("_")
                  .append(clientItfEscapedName).append(" ").append(componentCName)
                  .append(".type.imported.").appendln(clientItfEscapedName).endl();
            }
          }
          else {
            cw.appendln("/* CLIENT_itfName macro definition in case of dynamic='true' but NOT singleton */");
            for (final String macroName : decodedNames) {
              cw.append("#define ").append(macroName).append("_")
                  .append(clientItfEscapedName).append(" ( ((struct ")
                  .append(componentCName).append("_t*) _this)->type.imported.")
                  .append(clientItfEscapedName).appendln(")").endl();
            }
          }
        }

        for (final Method m : itfMethods) {
          /*
           * ..write a CLIENT_{itfName}_{methodName} specific macro for this
           * component type. TODO this is just the dynamic binding expansion
           */

          /*
           * This is the default and normal *not* optimized CLIENT macro
           * definition
           */
          if (isDynamic) {
            for (final String macroName : decodedNames) {
              cw.appendln(
                  "#define " + macroName + "_" + escapedItfName + "_" + m.getName() + " \\");
              cw.appendln(
                  " CALL( " + macroName + "_" + escapedItfName + ", " + m.getName() + ")");
            }
          }
          /* this is the *optimized* version of the CLIENT macros */
          else {
            System.out.println("--> Generating optimized CLIENT_"
                + escapedItfName + "_" + m.getName() + " macro");

            /* if the self pointer has to be passed or not */
            final boolean isStatic = InterfaceDecorationUtil
                .hasToBeCompiledAsStatic(itf);

            for (final String macroName : decodedNames) {
              cw.append("#define ").append(macroName).append("_")
                  .append(escapedItfName).append("_").append(m.getName()).append(" ");
              final String serverInstanceSelfPointer = "(void*) &"
                  + serverInstanceEscapedName;
              if (isStatic) {
                cw.append(" ").append(serverTypeEscapedName).append("_")
                    .append(serverItfEscapedName).append("_").append(m.getName())
                    .appendln("_method( __CECILIA_PARAMS");
              } else {
                cw.append(" ").append(serverTypeEscapedName).append("_")
                    .append(serverItfEscapedName).append("_").append(m.getName())
                    .append("_method( ").append(serverInstanceSelfPointer)
                    .appendln(" __CECILIA_PARAMS");
              }
            }
          }
        }
        cw.endl(); // add a line to separate from the rest
      }
      cw.endl();

      cw.append("struct ").append(componentCName).appendln("_importeds { ");
      for (final TypeInterface itf : importedItfNodes) {
        String itfEscapedSignature = itf.getSignature().replace('.', '_');
        cw.append("R").append(itfEscapedSignature).append(" *");

        /*
         * if the binding is dynamic=false, the pointer should not be
         * reassignable.
         */
        if (!BindingDecorationUtil.isDynamic((Node) itf)) {
          cw.append(" const ");
        }

        cw.append(itf.getName().replace('-', '_')).appendln(";");
      }
      cw.append("};").endl();

      return cw.toString();
    }
  }
}
