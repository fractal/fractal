/**
 * 
 */

package org.objectweb.fractal.cecilia.adl;

import java.util.Map;

/**
 * @author Alessio Pace
 */
public interface LanguageLoaderAttributesFactory {

  /**
   * @return an unmodifiable <tt>Map</tt> with the <tt>LanguageLoader</tt>
   *         context params
   */
  Map<String, Object> getLanguageContextValues();
}
