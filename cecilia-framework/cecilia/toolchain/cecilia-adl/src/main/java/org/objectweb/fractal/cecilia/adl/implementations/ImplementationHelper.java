/**
 * Cecilia ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.implementations;

import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;

/**
 * Helper class for {@link Implementation} and {@link ExtendedImplementation}
 * AST interfaces.
 */
public final class ImplementationHelper {
  private ImplementationHelper() {
  }

  /**
   * Returns whether the given <code>implemNode</code> is an
   * {@link ExtendedImplementation} node and the
   * {@link ExtendedImplementation#getHasConstructor() hasConstructor} attribute
   * is equals to <code>"true"</code>.
   * 
   * @param implemNode an implementation node.
   * @return <code>true</code> if and only if the given <code>implemNode</code>
   *         implements the {@link ExtendedImplementation} interface and the
   *         {@link ExtendedImplementation#getHasConstructor() hasConstructor}
   *         attribute is equals (ignoring case) to <code>"true"</code>.
   */
  public static boolean hasConstructor(final Implementation implemNode) {
    return (implemNode instanceof ExtendedImplementation)
        && "true".equalsIgnoreCase(((ExtendedImplementation) implemNode)
            .getHasConstructor());
  }

  /**
   * Returns whether the given component node contains an {@link Implementation}
   * node that defines a constructor.
   * 
   * @param container a component node.
   * @return <code>true</code> if and only if the given <code>container</code>
   *         node contains an {@link Implementation} that defines a constructor.
   * @see #hasConstructor(Implementation)
   */
  public static boolean hasConstructor(final ComponentContainer container) {
    return (container instanceof ImplementationContainer)
        && hasConstructor(((ImplementationContainer) container)
            .getImplementation());
  }

  /**
   * Returns whether the given <code>implemNode</code> is an
   * {@link ExtendedImplementation} node and the
   * {@link ExtendedImplementation#getHasDestructor() hasDestructor} attribute
   * is equals to <code>"true"</code>.
   * 
   * @param implemNode an implementation node.
   * @return <code>true</code> if and only if the given <code>implemNode</code>
   *         implements the {@link ExtendedImplementation} interface and the
   *         {@link ExtendedImplementation#getHasDestructor() hasDestructor}
   *         attribute is equals (ignoring case) to <code>"true"</code>.
   */
  public static boolean hasDestructor(final Implementation implemNode) {
    return (implemNode instanceof ExtendedImplementation)
        && "true".equalsIgnoreCase(((ExtendedImplementation) implemNode)
            .getHasDestructor());
  }

  /**
   * Returns whether the given component node contains an {@link Implementation}
   * node that defines a destructor.
   * 
   * @param container a component node.
   * @return <code>true</code> if and only if the given <code>container</code>
   *         node contains an {@link Implementation} that defines a destructor.
   * @see #hasDestructor(Implementation)
   */
  public static boolean hasDestructor(final ComponentContainer container) {
    return (container instanceof ImplementationContainer)
        && hasDestructor(((ImplementationContainer) container)
            .getImplementation());
  }
}
