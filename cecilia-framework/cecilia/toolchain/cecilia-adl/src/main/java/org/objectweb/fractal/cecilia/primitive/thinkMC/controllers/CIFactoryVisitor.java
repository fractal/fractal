/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.primitive.thinkMC.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.interfaces.AbstractServerInterfaceVisitor;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.TaskFactory;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the factory implementation code pieces for
 * cloning component identity controller data structure.
 */
public class CIFactoryVisitor extends AbstractServerInterfaceVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractServerInterfaceVisitor
  // ---------------------------------------------------------------------------

  @Override
  protected Component createTask(final ComponentContainer container,
      final List<TypeInterface> serverInterfaces) throws ADLException,
      TaskException {
    final Collection<Component> tasks = new ArrayList<Component>(3);
    tasks.add(taskFactoryItf.newPrimitiveTask(
        new CIFactoryInstantiateDeclarationTask(), container));
    tasks.add(taskFactoryItf.newPrimitiveTask(
        new CIFactoryInstantiateSizeofTask(), container));
    tasks.add(taskFactoryItf.newPrimitiveTask(new CIFactoryInstantiateTask(
        serverInterfaces), container));

    return taskFactoryItf.newCompositeTask(tasks, TaskFactory.EXPORT_ALL, null);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds the source code that declare the new component identity controller
   * data structure.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "ci-factory-instantiate-declaration", signature = SourceCodeProvider.class, record = "role:factoryInstantiateDeclaration, id:%, codePiece:CI", parameters = "componentNode"))
  public static class CIFactoryInstantiateDeclarationTask
      extends
        AbstractDefinitionTask {

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final String definitionName = typeNameProviderItf.getCTypeName();
      final CodeWriter cw = new CodeWriter();
      cw.append("struct ").append(definitionName).appendln(
          "_CI_data_type * newComp_CI_data;");
      return cw.toString();
    }
  }

  /**
   * Builds the source code that declare the new component identity controller
   * data structure.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "ci-factory-instantiate-sizeof", signature = SourceCodeProvider.class, record = "role:factoryInstantiateSizeof, id:%, codePiece:CI", parameters = "componentNode"))
  public static class CIFactoryInstantiateSizeofTask
      extends
        AbstractDefinitionTask {

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final String definitionName = typeNameProviderItf.getCTypeName();
      final CodeWriter cw = new CodeWriter();
      cw.append("+ sizeof(struct ").append(definitionName)
          .append("_CI_data_type)");
      return cw.toString();
    }
  }

  /**
   * Builds the source code that copy the component identity controller data structure.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "ci-factory-instantiate", signature = SourceCodeProvider.class, record = "role:factoryInstantiatePiece, id:%, codePiece:CI", parameters = "componentNode"))
  public static class CIFactoryInstantiateTask extends AbstractDefinitionTask {

    // The client interfaces array
    final List<TypeInterface> serverInterfaces;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param serverInterfaces the server interface nodes.
     */
    public CIFactoryInstantiateTask(final List<TypeInterface> serverInterfaces) {
      this.serverInterfaces = serverInterfaces;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final String definitionName = typeNameProviderItf.getCTypeName();
      final CodeWriter cw = new CodeWriter("Primitive Component Identity Controller");
      cw.appendln("{");
      cw.append("newComp_CI_data = (struct ").append(
          definitionName).appendln("_CI_data_type *)_ptr;");
      cw.append("struct ").append(definitionName).append(
          "_CI_data_type * original_CI_data = ").append("(struct ").append(
          definitionName).append("_CI_data_type *) ").append(
          "originalComp->type.exported.component.selfdata;").endl();

      cw.append("memcpy(newComp_CI_data, original_CI_data, sizeof(struct ")
          .append(definitionName).append("_CI_data_type));").endl();

      for (int i = 0; i < serverInterfaces.size(); i++) {
        cw.append("newComp_CI_data->exported[").append(i).append(
            "].itf = &(newComp").append("->type.exported.").append(
            serverInterfaces.get(i).getName().replace('-', '_')).append(");")
            .endl();
      }
      cw.append("_ptr += sizeof(struct ").append(definitionName)
          .appendln("_CI_data_type);");
      cw.appendln("}");
      return cw.toString();
    }
  }
}
