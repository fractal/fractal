/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 * Copyright (C) 2009 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 * Contributors: Matthieu Leclercq, Lionel Debroux
 */

package org.objectweb.fractal.cecilia.primitive.optC.controllers;

import static org.objectweb.fractal.adl.types.TypeInterfaceUtil.isClient;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerDecorationUtil.isDefaultBindingController;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerDecorationUtil.setDataSuffix;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerDecorationUtil.setDefaultBindingController;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerDecorationUtil.setVTable;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.AC;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.ATTRIBUTE_CONTROLLER_SIGNATURE;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.BC;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.BINDING_CONTROLLER_SIGNATURE;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.CI;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.COMPONENT_IDENTITY_SIGNATURE;
import static org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil.setNoStaticBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.attributes.Attributes;
import org.objectweb.fractal.adl.attributes.AttributesContainer;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil;
import org.objectweb.fractal.cecilia.adl.controllers.AbstractControllerChecker;
import org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface;
import org.objectweb.fractal.cecilia.adl.idl.ast.Field;
import org.objectweb.fractal.cecilia.adl.idl.ast.FieldContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.RecordDefinition;
import org.objectweb.fractal.cecilia.adl.types.TypeDecorationUtil;

/**
 * Adds controllers interface for <code>"primitive"</code> components.
 */
public class PrimitiveControllerLoader extends AbstractControllerChecker {

  // --------------------------------------------------------------------------
  // Implementation of abstract methods
  // --------------------------------------------------------------------------

  @Override
  protected void checkComponent(final ComponentContainer container)
      throws ADLException {
    if (container instanceof ImplementationContainer
        && (((ImplementationContainer) container).getImplementation() == null))
      throw new ADLException(GenericErrors.GENERIC_ERROR,
          "'primitive' controller can only be used for primitive components",
          container);
  }

  @Override
  protected List<Interface> getControllerInterfaces(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final List<Interface> controllerItfs = new ArrayList<Interface>();
    final Interface ciItf = getComponentIdentityInterface(container, context);
    if (ciItf != null) controllerItfs.add(ciItf);

    final Interface bcItf = getBindingControllerInterface(container, context);
    if (bcItf != null) controllerItfs.add(bcItf);

    final Interface acItf = getAttributeControllerInterface(container, context);
    if (acItf != null) controllerItfs.add(acItf);

    checkCustomLifeCycleControllerInterface(container, context);

    return controllerItfs;
  }

  // --------------------------------------------------------------------------
  // Utility methods
  // --------------------------------------------------------------------------

  protected void checkCustomLifeCycleControllerInterface(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {

    final List<TypeInterface> lcc = this.getServerInterfacesBySignature(
        ControllerInterface.LIFECYCLE_CONTROLLER_SIGNATURE,
        (InterfaceContainer) container);

    if (lcc.size() > 1) {
      throw new ADLException(GenericErrors.GENERIC_ERROR,
          "It is not allowed to specify TWO " + ControllerInterface.LCC
              + " interfaces");
    }

    /* if there is a user provided LCC... */
    if (lcc.size() == 1) {

      final TypeInterface lifeCycleControllerItf = lcc.get(0);

      // .. check that the name matches the canonical name
      if (!lifeCycleControllerItf.getName().equals(ControllerInterface.LCC)) {
        throw new ADLException(GenericErrors.GENERIC_ERROR,
            "Custom server interface of type "
                + ControllerInterface.LIFECYCLE_CONTROLLER_SIGNATURE
                + " must be called '" + ControllerInterface.LCC + "'",
            lifeCycleControllerItf);
      }
    }

  }

  protected Interface getComponentIdentityInterface(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final InterfaceContainer itfContainer = (InterfaceContainer) container;
    TypeInterface ciItf = getInterface(CI, itfContainer);
    if (ciItf == null) {
      ciItf = newServerInterfaceNode(CI, COMPONENT_IDENTITY_SIGNATURE);
      setVTable(ciItf, "__cecilia_default_ComponentMeths");
      setDataSuffix(ciItf, "CI_data");
      setItfCode(ciItf, context, "fractal.lib.CIdelegate");
    } else {
      checkServerInterface(ciItf);
      // remove the interface form the interface container, since it is re-added
      itfContainer.removeInterface(ciItf);
    }
    return ciItf;
  }

  protected Interface getBindingControllerInterface(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final InterfaceContainer itfContainer = (InterfaceContainer) container;
    TypeInterface bcItf = getInterface(BC, itfContainer);
    int bcType = getBCdelegateRequiredByComponent(itfContainer);
    if (bcType != 0) {
      if (bcItf == null) {
        bcItf = newServerInterfaceNode(BC, BINDING_CONTROLLER_SIGNATURE);
        setDataSuffix(bcItf, "BC_data");

        // set DEFAULT_BC_DECORATION on bcItf
        setDefaultBindingController(bcItf, true);
      }
      else {
        /*
         * This can happen when the MergeComponentsLoader triggered
         * re-processing of the AST through the Checker interfaces.
         * We DO want to run the code below again: the BindingResolverLoader
         * (whose execution terminates *after* the execution of this Checker)
         * may have decorated an interface with dynamic="false".
         * Fall through. 
         */
      }

      if (isDefaultBindingController(bcItf)) {
        if (bcType == -1) {
          System.out.println("Setting dynamic=\"false\" aware BC for " + container);
          setVTable(bcItf, "__cecilia_BindingControllerStaticBindingsMeths");
          setItfCode(bcItf, context, "fractal.lib.BCdelegate_staticbindings");
        }
        else {
          setVTable(bcItf, "__cecilia_default_BindingControllerMeths");
          setItfCode(bcItf, context, "fractal.lib.BCdelegate");
        }
      }
    }
    if (bcItf != null) {
      checkServerInterface(bcItf);

      // remove the interface form the interface container, since it is re-added
      itfContainer.removeInterface(bcItf);

      /*
       * Deal with multiple times processing of the AST: the BC interface may
       * already have been created and its vtable set.
       */
      if (!isDefaultBindingController(bcItf)) {
        // if the component has a custom binding controller, do not use
        // statically initialized binding.
        for (final Interface i : itfContainer.getInterfaces()) {
          if (isClient(i)) setNoStaticBinding(i, true);
        }
      }
      else {
        if (bcType == 0) {
          /*
           * A previous run of this checker determined that this component
           * needs a BC, but it turns out that after dynamic="false" bindings
           * have been taken into account, it doesn't.
           * Kill this server interface.
           */
          bcItf = null;
        }
      }
    }
    return bcItf;
  }

  protected Interface getAttributeControllerInterface(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final InterfaceContainer itfContainer = (InterfaceContainer) container;
    TypeInterface acItf = getInterface(AC, itfContainer);
    int acType = getACdelegateRequiredByComponent(container);
    if (acType != 0) {
      if (acItf == null) {
        acItf = newServerInterfaceNode(AC, ATTRIBUTE_CONTROLLER_SIGNATURE);
        setDataSuffix(acItf, "AC_data");
      }
      else {
        /*
         * This can happen when the MergeComponentsLoader triggered
         * re-processing of the AST through the Checker interfaces.
         * Fall through. 
         */
      }

      if (acType == -1) {
        System.out.println("Setting const aware AC for " + container);
        setItfCode(acItf, context, "fractal.lib.ACdelegate_constattributes");
        setVTable(acItf, "__cecilia_AttributeControllerConstAttributesMeths");
      } else {
        setItfCode(acItf, context, "fractal.lib.ACdelegate");
        setVTable(acItf, "__cecilia_default_AttributeControllerMeths");
      }
    }
    if (acItf != null) {
      checkServerInterface(acItf);
      
      // remove the interface form the interface container, since it is re-added
      itfContainer.removeInterface(acItf);
    }
    return acItf;
  }

  /**
   * @param container the <tt>Interface</tt> container.
   * @return <tt>1</tt> if the component needs a BC which needn't handle
   *         dynamic="false" bindings, <tt>-1</tt> if the component needs a BC
   *         which must handle dynamic="false" bindings, <tt>0</tt> otherwise.
   *
   * This helper method is public, because it is useful in BC*Visitor as well.
   */
  public static int getBCdelegateRequiredByComponent(
      final InterfaceContainer container) throws ADLException {
    boolean result = false;
    int dynamicFalseBindingsCount = 0;
    int clientInterfaceCount = 0;

    final String disableDynamicFalse = System.getProperty(
        BindingDecorationUtil.CECILIA_DISABLE_DYNAMICFALSE_BINDINGS, "false");

    Interface[] interfaces = container.getInterfaces();
    for (final Interface itf : interfaces) {
      if (isClient(itf)) {
        clientInterfaceCount++;
        // Don't count dynamic="false" bindings if they're disabled.
        if (   !BindingDecorationUtil.isDynamic((Node)itf)
            && !disableDynamicFalse.equals("true")) {
          System.out.println(container + "." + itf.getName() + " is not dynamic");
          dynamicFalseBindingsCount++;
        }
      }
    }

    // If there are no client interfaces, or all bindings are dynamic="false",
    // no need for a BC... unless the component is cloneable.
    if (TypeDecorationUtil.getCloneableTypeDecorationDefaultFalse(
            (ComponentContainer) container) == false) {
      if (   clientInterfaceCount == 0
           || dynamicFalseBindingsCount == clientInterfaceCount) {
        return 0;
      }
      else if (dynamicFalseBindingsCount == 0 && clientInterfaceCount > 0) {
        // No dynamic="false" bindings: the BC which can't handle
        // dynamic="false" bindings properly is fine.
        return 1;
      }
      else if (   dynamicFalseBindingsCount != 0
             && dynamicFalseBindingsCount < clientInterfaceCount) {
        // Some bindings are dynamic="false", some aren't: need the BC which
        // can handle both kinds of bindings...
        return -1;
      }
    }
    else {
      if (dynamicFalseBindingsCount == 0) {
        // No dynamic="false" bindings: the BC which can't handle
        // dynamic="false" bindings properly is fine.
        return 1;
      }
      else {
        // Cloneable components have at least a dynamic="true" interface.
        // Need the BC which can handle both kinds of bindings.
        return -1;
      }
    }
    throw new ADLException(GenericErrors.GENERIC_ERROR,
        "Unhandled case in getBCdelegateRequiredByComponent");
  }

  /**
   * @param container the <tt>Interface</tt> container.
   * @return <tt>1</tt> if the component needs an AC which needn't handle
   *         constant attributes, <tt>-1</tt> if the component needs an AC
   *         which must handle constant attributes, <tt>0</tt> otherwise.
   *
   * This helper method is public, because it is useful in AC*Visitor as well.
   */
  public static int getACdelegateRequiredByComponent(
      final ComponentContainer container) throws ADLException {
    if (container instanceof AttributesContainer) {
      final Attributes attrs = ((AttributesContainer) container).getAttributes();
      if (attrs != null) {
        final RecordDefinition recordDefinition = (RecordDefinition)
            ((IDLDefinitionContainer) attrs).getIDLDefinition();
        final Field[] fields = ((FieldContainer) recordDefinition).getFields();
        if (fields != null) {
          int constantFieldCount = 0;
          for (final Field f : fields) {
            if (f.getQualifier().equals(Field.CONST)) {
              constantFieldCount++;
            }
          }
          // If all fields are constant, an AC is unneeded.
          if (constantFieldCount == fields.length) {
            return 0;
          }
          else if (constantFieldCount == 0 && fields.length != 0) {
            // No constant fields: the AC which can't handle constant
            // attributes properly is fine.
            return 1;
          }
          else if (constantFieldCount != 0
                   && constantFieldCount < fields.length) {
            // Some fields are constant, some aren't: need the AC which
            // can handle both non-constant and constant attributes.
            return -1;
          }
          else {
            throw new ADLException(GenericErrors.GENERIC_ERROR,
                "Unhandled case in getACdelegateRequiredByComponent");
          }
        }
      }
    }
    return 0;
  }
}
