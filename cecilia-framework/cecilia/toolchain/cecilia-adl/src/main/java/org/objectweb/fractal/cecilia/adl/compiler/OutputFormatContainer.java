/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.compiler;

/**
 * Container interface for nodes that can contain an {@link OutputFormat output}
 * sub node.
 */
public interface OutputFormatContainer {

  /**
   * Returns the {@link OutputFormat output} sub node of this node, or
   * <code>null</code> if this node has no <code>output</code> sub node.
   * 
   * @return the {@link OutputFormat output} sub node of this node.
   */
  OutputFormat getOutput();

  /**
   * Sets the {@link OutputFormat output} sub node of this node.
   * 
   * @param node the {@link OutputFormat output} sub node of this node.
   */
  void setOutput(OutputFormat node);
}
