/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 * Contributors: Alessio Pace
 */

package org.objectweb.fractal.cecilia.primitive.thinkMC.implementations;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;
import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationDecorationUtil.getCode;
import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationHelper.hasConstructor;
import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationHelper.hasDestructor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.directives.Include;
import org.objectweb.fractal.cecilia.adl.directives.IncludeContainer;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.file.SourceFile;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.TaskFactory;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that provides the inclusion code of the implementation file
 * for <code>ThinkMC</code> dialect.
 */
public class MacroDefinitionVisitor extends AbstractTaskFactoryUser
    implements
      ComponentVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link ComponentContainer} nodes and creates a task that includes
   * the implementation file, and that defines some macros that are used in the
   * implementation file.
   */
  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    final Implementation impl = castNodeError(container,
        ImplementationContainer.class).getImplementation();
    if (impl == null) {
      throw new CompilerError(GenericErrors.INTERNAL_ERROR,
          "This visitor is only applicable for primitive component.");
    }
    final Component implTask = createImplementationTask(container, impl);
    if (impl instanceof IncludeContainer
        && ((IncludeContainer) impl).getIncludes().length != 0) {
      final Include[] includes = ((IncludeContainer) impl).getIncludes();

      final Collection<Component> tasks = new ArrayList<Component>(
          includes.length + 1);
      tasks.add(implTask);

      for (final Include include : includes) {
        // if the module is an assembly, file, it should not be included in
        // implementation.
        if (!((SourceFile) getCode((Node) include)).isAssemblyFile())
          tasks.add(createModuleImplementationTask(container, include));
      }
      return taskFactoryItf.newCompositeTask(tasks, TaskFactory.EXPORT_ALL,
          null);
    } else {
      return implTask;
    }
  }

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  protected Component createImplementationTask(
      final ComponentContainer container, final Implementation impl)
      throws TaskException {
    return taskFactoryItf.newPrimitiveTask(new ImplementationTask(
        hasConstructor(container), hasDestructor(container)), container);
  }

  protected Component createModuleImplementationTask(
      final ComponentContainer container, final Include include)
      throws TaskException {
    return taskFactoryItf.newPrimitiveTask(new ModuleImplementationTask(
        hasConstructor(container), hasDestructor(container)), container,
        include);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  protected abstract static class AbstractImplementationTask
      extends
        AbstractDefinitionTask {

    protected final boolean hasConstructor;
    protected final boolean hasDestructor;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param hasConstructor <code>true</code> if the component has a
     *          constructor.
     * @param hasDestructor <code>true</code> if the component has a destructor.
     */
    public AbstractImplementationTask(final boolean hasConstructor,
        final boolean hasDestructor) {
      this.hasConstructor = hasConstructor;
      this.hasDestructor = hasDestructor;
    }

    // -------------------------------------------------------------------------
    // Abstract methods
    // -------------------------------------------------------------------------

    protected abstract void appendImplementationCode(CodeWriter cw);

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter(
          "Primitive Implementation Definition Builder");
      final String componentCName = typeNameProviderItf.getCTypeName();

      /* Define specified DECLARE_DATA macro for this component */
      cw.append("#define DECLARE_DATA \\").endl();
      cw.append("  struct ").append(componentCName).append("_instancedata")
          .endl();
      /* Define the specific METHOD macro for this component */
      if (hasConstructor) {
        cw.append("#define CONSTRUCTOR \\").endl();
        cw.append(componentCName).append("_constructor").endl();
      }
      if (hasDestructor) {
        cw.append("#define DESTRUCTOR \\").endl();
        cw.append(componentCName).append("_destructor").endl();
      }

      cw.append("#define METHOD(i, m) \\").endl();
      cw.append(componentCName).append("##_##i##_##m##_method").endl();
      cw.append("#define CALLMINE(itf,proc,args...) \\").endl();
      cw.append("  ").append(componentCName).append(
          "##_##itf##_##proc##_method(_this, ##args)").endl();
      /* Define REQUIRED, ATTRIBUTES and DATA as a component specific macros */
      cw.append("#define REQUIRED \\").endl();
      cw.append("  ((struct ").append(componentCName).append(
          "_t *)_this)->type.imported ").endl();
      cw.append("#define ATTRIBUTES \\").endl();
      cw.append("  ((struct ").append(componentCName).append(
          "_t *)_this)->attributes ").endl();
      cw.append("#define DATA \\").endl();
      cw.append("  ((struct ").append(componentCName + "_t *)_this)->data ")
          .endl();
      /* Define the GETSELF macro according to above defined structures */
      cw.append("#define GETSELF \\").endl();
      cw.append("  struct ").append(componentCName).append(
          "_t *self = (struct " + componentCName).append("_t *) _this ").endl();

      // Define the GET_MY_OWNER macro
      cw.append("#define GET_MY_OWNER \\").endl();
      cw.append("  ((Rfractal_api_Component *) _this)").endl();

      // Define the GET_MY_INTERFACE macro
      cw.append("#define GET_MY_INTERFACE(itf) \\").endl();
      cw.append("  (&(((struct ").append(componentCName).append(
          "_t *)_this)->type.exported.itf))").endl();

      if (hasConstructor) {
        cw.appendln("// declare constructor.");
        cw.appendln("void CONSTRUCTOR(void *_this);");
      }
      if (hasDestructor) {
        cw.appendln("// declare destructor.");
        cw.appendln("void DESTRUCTOR(void *_this);");
      }

      // Print the implementation code.
      appendImplementationCode(cw);
      return cw.toString();
    }
  }

  /**
   * Builds definition of implementation source code for the given component
   * node. This task defines the macros that are used in the implementation file
   * and then includes the implementation file.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "implementation-provider", signature = SourceCodeProvider.class, record = "role:implementation, id:%", parameters = "componentNode"))
  public static class ImplementationTask extends AbstractImplementationTask {

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /** Client interface used to retrieve the implementation code. */
    @ClientInterface(name = "implementation-source-code", record = "role:implementationSourceCode, id:%", parameters = "componentNode")
    public SourceCodeProvider implementationSourceCodeProviderItf;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param hasConstructor <code>true</code> if the component has a
     *          constructor.
     * @param hasDestructor <code>true</code> if the component has a destructor.
     */
    public ImplementationTask(final boolean hasConstructor,
        final boolean hasDestructor) {
      super(hasConstructor, hasDestructor);
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractImplementationTask
    // -------------------------------------------------------------------------

    @Override
    protected void appendImplementationCode(final CodeWriter cw) {
      cw.appendln(implementationSourceCodeProviderItf.getSourceCode());

      cw.append("#ifndef __CECILIA__COMPONENT_TYPE_DEFINITION_DONE").endl();
      cw.append(
          "#warning implementation file should include the header cecilia.h")
          .endl();
      cw.append("#endif").endl();
    }
  }

  /**
   * Builds definition of implementation source code for the given module of the
   * given component node. This task defines the macros that are used in the
   * implementation file and then includes the implementation file.
   */
  @TaskParameters({"componentNode", "moduleNode"})
  @ServerInterfaces(@ServerInterface(name = "implementation-provider", signature = SourceCodeProvider.class, record = "role:implementation, id:%, module:%", parameters = {
      "componentNode", "moduleNode"}))
  public static class ModuleImplementationTask
      extends
        AbstractImplementationTask {

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /** Client interface used to retrieve the module of implementation code. */
    @ClientInterface(name = "implementation-source-code", record = "role:implementationSourceCode, id:%, module:%", parameters = {
        "componentNode", "moduleNode"})
    public SourceCodeProvider implementationModuleSourceCodeProviderItf;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param hasConstructor <code>true</code> if the component has a
     *          constructor.
     * @param hasDestructor <code>true</code> if the component has a destructor.
     */
    public ModuleImplementationTask(final boolean hasConstructor,
        final boolean hasDestructor) {
      super(hasConstructor, hasDestructor);
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractImplementationTask
    // -------------------------------------------------------------------------

    @Override
    protected void appendImplementationCode(final CodeWriter cw) {
      cw.appendln(implementationModuleSourceCodeProviderItf.getSourceCode());
    }
  }
}
