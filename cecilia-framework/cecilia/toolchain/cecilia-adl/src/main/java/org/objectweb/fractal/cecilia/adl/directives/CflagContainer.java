/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.adl.directives;

/**
 * AST node interface for <code>CFlags</code> container elements.
 */
public interface CflagContainer {

  /**
   * Returns the <code>CFlags</code> sub-elements.
   * 
   * @return the <code>CFlags</code> sub-elements.
   */
  Cflag[] getCflags();

  /**
   * Adds a <code>CFlags</code> sub-element.
   * 
   * @param cflag a <code>CFlags</code> sub-element to add.
   */
  void addCflag(Cflag cflag);

  /**
   * Removes a <code>CFlags</code> sub-element.
   * 
   * @param cflag a <code>CFlags</code> sub-element to remove.
   */
  void removeCflag(Cflag cflag);
}
