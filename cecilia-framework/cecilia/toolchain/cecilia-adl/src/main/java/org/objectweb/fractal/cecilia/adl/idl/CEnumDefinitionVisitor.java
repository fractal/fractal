/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 * Contributors: Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.adl.idl;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.idl.ast.EnumDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.EnumNameValuePair;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.Executable;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component for {@link EnumDefinition} nodes.
 */
public class CEnumDefinitionVisitor extends AbstractTaskFactoryUser
    implements
      IDLDefinitionVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of the IDLVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link EnumDefinition} nodes, and creates a task that writes the
   * corresponding C source code.
   */
  public Component visit(final List<Node> path,
      final IDLCompilationDesc idlCompilationDesc,
      final Map<Object, Object> context) throws ADLException, TaskException {
    IDLDefinition idlDefinition = idlCompilationDesc.getIdlDefinition();
    if (idlDefinition instanceof EnumDefinition) {
      return taskFactoryItf.newPrimitiveTask(new CEnumSourceTask(
          (EnumDefinition) idlDefinition), idlDefinition);
    } else
      return null;
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds enum definitions for C.
   */
  @TaskParameters({"enumDefinitionNode"})
  @ServerInterfaces(@ServerInterface(name = "c-enum-definition-provider", signature = SourceCodeProvider.class, record = "role:cEnumDefinition, id:%", parameters = {"enumDefinitionNode"}))
  public static class CEnumSourceTask implements Executable, SourceCodeProvider {
    protected final EnumDefinition enumDefinition;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * Simple constructor.
     * 
     * @param enumDefinition The enum definition node for which this task must
     *            generate C source code.
     */
    public CEnumSourceTask(final EnumDefinition enumDefinition) {
      this.enumDefinition = enumDefinition;
    }

    // The generated source code.
    protected String sourceCode;

    public void execute() throws Exception {
      // TODO Auto-generated method stub

    }

    public String getSourceCode() {
      final CodeWriter cw = new CodeWriter("C Enum builder");
      final String enumName = enumDefinition.getName();

      cw.append("/* Generated enum :" + enumName + " */").endl();

      final String enumCName = enumName.replace('.', '_');
      cw.append("#ifndef _").append(enumCName.toUpperCase()).append("_IDL_H_")
          .endl();
      cw.append("#define _").append(enumCName.toUpperCase()).append("_IDL_H_")
          .endl();
      cw.endl();
      cw.endl();

      cw.append("enum " + enumCName + " {").endl();
      int i = 1;
      final int nrOfPairs = enumDefinition.getEnumNameValuePairs().length;
      for (final EnumNameValuePair pair : enumDefinition
          .getEnumNameValuePairs()) {
        cw.append("  " + pair.getName());
        final String pairValue = pair.getValue();
        if (pairValue != null && pairValue.length() > 0) {
          cw.append("=" + pairValue);
        }

        if (i != nrOfPairs) {
          cw.append(",");
        }

        i++;
        cw.endl();
      }
      cw.append("};").endl().endl();

      cw.append("typedef enum ").append(enumCName).append(' ')
          .append(enumCName).append(";").endl();
      /* typedef with "R" in front of the enum type name */
      cw.append("typedef enum ").append(enumCName).append(" R").append(
          enumCName).append(";").endl();

      cw.append("#endif").endl();

      return cw.toString();
    }
  }
}
