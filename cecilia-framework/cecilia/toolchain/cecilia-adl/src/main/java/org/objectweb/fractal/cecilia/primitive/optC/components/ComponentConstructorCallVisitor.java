/***
 * Cecilia ADL Compiler
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.primitive.optC.components;

import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationHelper.hasConstructor;
import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationHelper.hasDestructor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractInstantiationTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.TaskFactory;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the code for the call to the
 * constructor/destructor of static component instance.
 */
public class ComponentConstructorCallVisitor extends AbstractTaskFactoryUser
    implements
      ComponentVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link ComponentContainer} nodes and creates a task that writes the
   * source code containing the component data structure definitions.
   */
  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    final boolean hasConstructor = hasConstructor(container);
    final boolean hasDestructor = hasDestructor(container);
    if (hasConstructor || hasDestructor) {
      final Collection<Component> tasks = new ArrayList<Component>();
      tasks.add(createComponentDeclarationTask(container));
      if (hasConstructor) tasks.add(createConstructorCallTask(container));
      if (hasDestructor) tasks.add(createDestructorCallTask(container));
      return taskFactoryItf.newCompositeTask(tasks, TaskFactory.EXPORT_ALL,
          null);
    } else {
      return null;
    }
  }

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  protected Component createComponentDeclarationTask(
      final ComponentContainer container) throws TaskException {
    return taskFactoryItf.newPrimitiveTask(new ComponentDeclarationTask(),
        container);
  }

  protected Component createConstructorCallTask(
      final ComponentContainer container) throws TaskException {
    return taskFactoryItf.newPrimitiveTask(new ComponentConstructorCallTask(),
        container);
  }

  protected Component createDestructorCallTask(
      final ComponentContainer container) throws TaskException {
    return taskFactoryItf.newPrimitiveTask(new ComponentDestructorCallTask(),
        container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Generates the declarations necessary to call the constructor of the
   * component.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "component-constructor-declaration-provider", signature = SourceCodeProvider.class, record = "role:globalInit, id:%, type:declaration, kind:component", parameters = "componentNode"))
  public static class ComponentDeclarationTask
      extends
        AbstractInstantiationTask {

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractInstantiationTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter();
      if (!System.getProperty(InterfaceDecorationUtil
              .CECILIA_ENABLE_THIS_OPTIMIZATION, "false").equals("true")) {
        cw.append("extern void * ").append(
            instanceNameProviderItf.getCInstanceName()).appendln(";");
      }
      return cw.toString();
    }
  }

  /**
   * Generates the call to the constructor of the component.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "component-constructor-call-provider", signature = SourceCodeProvider.class, record = "role:globalInit, id:%, type:call, kind:constructor", parameters = "componentNode"))
  public static class ComponentConstructorCallTask
      extends
        AbstractInstantiationTask {

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractInstantiationTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter();
      cw.append(typeNameProviderItf.getCTypeName()).append("_constructor(");
      if (!System.getProperty(InterfaceDecorationUtil
              .CECILIA_ENABLE_THIS_OPTIMIZATION, "false").equals("true")) {
        cw.append("& ").append(instanceNameProviderItf.getCInstanceName());
      }
      cw.appendln(");");
      return cw.toString();
    }
  }

  /**
   * Generates the call to the destructor of the component.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "component-destructor-call-provider", signature = SourceCodeProvider.class, record = "role:globalInit, id:%, type:call, kind:destructor", parameters = "componentNode"))
  public static class ComponentDestructorCallTask
      extends
        AbstractInstantiationTask {

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractInstantiationTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter();
      cw.append(typeNameProviderItf.getCTypeName()).append("_destructor(");
      if (!System.getProperty(InterfaceDecorationUtil
              .CECILIA_ENABLE_THIS_OPTIMIZATION, "false").equals("true")) {
        cw.append("& ").append(instanceNameProviderItf.getCInstanceName());
      }
      cw.appendln(");");
      return cw.toString();
    }
  }
}
