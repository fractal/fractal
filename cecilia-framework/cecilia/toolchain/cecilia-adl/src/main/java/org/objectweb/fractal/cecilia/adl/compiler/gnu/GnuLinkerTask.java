/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.compiler.gnu;

import static org.objectweb.fractal.cecilia.adl.compiler.ExecutionHelper.exec;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.cecilia.adl.compiler.AbstractLinkTask;
import org.objectweb.fractal.cecilia.adl.compiler.CompilerErrors;

public class GnuLinkerTask extends AbstractLinkTask {

  protected final String ldCommand;

  // ---------------------------------------------------------------------------
  // Task constructor
  // ---------------------------------------------------------------------------

  /**
   * @param ldCommand the command of the Gnu linker to be executed by this task.
   * @param outputName the name of the linked file
   * @param outputDir the directory into which the compiled file will be placed.
   * @param flags compilation flags.
   * @param nThreads the number of concurrent compilation jobs
   */
  public GnuLinkerTask(final String ldCommand, final String outputName,
      final File outputDir, final List<String> flags, final int nThreads) {
    super(outputName, outputDir, flags, nThreads);
    this.ldCommand = ldCommand;
  }

  // ---------------------------------------------------------------------------
  // Overridden methods
  // ---------------------------------------------------------------------------

  @Override
  protected File getOutputFile() {
    File file = super.getOutputFile();
    if (System.getProperty("os.name").contains("Windows") && !file.exists()) {
      // on windows, linker outputFiles may be suffixed by ".exe" in case of
      // host compilation. If such a file exist, use it instead of default file
      final File exeFile = new File(file.getPath() + ".exe");
      if (exeFile.exists()) file = exeFile;
    }
    return file;
  }

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractLinkTask
  // ---------------------------------------------------------------------------

  @Override
  protected File link(File outputFile, final Collection<File> inputFiles)
      throws ADLException, InterruptedException {
    // forge command line.
    final List<String> cmd = new ArrayList<String>();
    cmd.add(ldCommand);
    cmd.add("-o");
    cmd.add(outputFile.getPath());

    // Archive files must be put at the end of the command line
    final List<File> archiveFiles = new ArrayList<File>();

    for (final File inputFile : inputFiles) {
      if (inputFile.getName().endsWith(".a")) {
        archiveFiles.add(inputFile);
      } else {
        cmd.add(inputFile.getPath());
      }
    }

    if (archiveFiles.size() > 1) {
      ioLogger
          .warning("Linking with multiple archive files may lead to unexpected result.");
    }

    // append archive files
    for (final File archiveFile : archiveFiles) {
      cmd.add(archiveFile.getPath());
    }

    // append flags.
    if (flags != null) {
      for (String flag : flags) {
        if (flag != null && !flag.equals("")) {
          cmd.add(flag);
        }
      }
    }

    // execute command
    final int rValue = exec("ld: " + outputFile.getPath(), cmd);
    if (rValue != 0) {
      throw new ADLException(CompilerErrors.LINKER_ERROR, outputFile.getPath());
    }

    // on windows, linker outputFiles may be suffixed by ".exe" in case of
    // host compilation.
    if (System.getProperty("os.name").contains("Windows")
        && !outputFile.exists()) {
      outputFile = new File(outputFile.getPath() + ".exe");
    }
    return outputFile;
  }
}
