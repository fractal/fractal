/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Lionel Debroux
 * Contributor: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl;

import java.util.HashSet;
import java.util.Set;

/**
 * Helper class that provides a method to checks that a given string is not a
 * reserved C/C++ keyword, or a typedef or macro defined in a standard header.
 */
public final class ReservedWordsChecker {

  private static final Set<String> RESERVED_C_WORD = new HashSet<String>();

  static {
    RESERVED_C_WORD.add("asm");
    RESERVED_C_WORD.add("auto");
    RESERVED_C_WORD.add("break");
    RESERVED_C_WORD.add("case");
    RESERVED_C_WORD.add("catch");
    RESERVED_C_WORD.add("char");
    RESERVED_C_WORD.add("class");
    RESERVED_C_WORD.add("const");
    RESERVED_C_WORD.add("continue");
    RESERVED_C_WORD.add("default");
    RESERVED_C_WORD.add("delete");
    RESERVED_C_WORD.add("do");
    RESERVED_C_WORD.add("double");
    RESERVED_C_WORD.add("else");
    RESERVED_C_WORD.add("enum");
    RESERVED_C_WORD.add("extern");
    RESERVED_C_WORD.add("float");
    RESERVED_C_WORD.add("for");
    RESERVED_C_WORD.add("friend");
    RESERVED_C_WORD.add("goto");
    RESERVED_C_WORD.add("if");
    RESERVED_C_WORD.add("inline");
    RESERVED_C_WORD.add("int");
    RESERVED_C_WORD.add("long");
    RESERVED_C_WORD.add("new");
    RESERVED_C_WORD.add("operator");
    RESERVED_C_WORD.add("private");
    RESERVED_C_WORD.add("protected");
    RESERVED_C_WORD.add("public");
    RESERVED_C_WORD.add("register");
    RESERVED_C_WORD.add("return");
    RESERVED_C_WORD.add("short");
    RESERVED_C_WORD.add("signed");
    RESERVED_C_WORD.add("sizeof");
    RESERVED_C_WORD.add("static");
    RESERVED_C_WORD.add("struct");
    RESERVED_C_WORD.add("switch");
    RESERVED_C_WORD.add("template");
    RESERVED_C_WORD.add("this");
    RESERVED_C_WORD.add("throw");
    RESERVED_C_WORD.add("try");
    RESERVED_C_WORD.add("typedef");
    RESERVED_C_WORD.add("unsigned");
    RESERVED_C_WORD.add("union");
    RESERVED_C_WORD.add("virtual");
    RESERVED_C_WORD.add("void");
    RESERVED_C_WORD.add("volatile");
    RESERVED_C_WORD.add("while");
    RESERVED_C_WORD.add("int8_t");
    RESERVED_C_WORD.add("uint8_t");
    RESERVED_C_WORD.add("int16_t");
    RESERVED_C_WORD.add("uint16_t");
    RESERVED_C_WORD.add("int32_t");
    RESERVED_C_WORD.add("uint32_t");
    RESERVED_C_WORD.add("int64_t");
    RESERVED_C_WORD.add("uint64_t");
    RESERVED_C_WORD.add("int_least8_t");
    RESERVED_C_WORD.add("uint_least8_t");
    RESERVED_C_WORD.add("int_least16_t");
    RESERVED_C_WORD.add("uint_least16_t");
    RESERVED_C_WORD.add("int_least32_t");
    RESERVED_C_WORD.add("uint_least32_t");
    RESERVED_C_WORD.add("int_least64_t");
    RESERVED_C_WORD.add("uint_least64_t");
    RESERVED_C_WORD.add("int_fast8_t");
    RESERVED_C_WORD.add("uint_fast8_t");
    RESERVED_C_WORD.add("int_fast16_t");
    RESERVED_C_WORD.add("uint_fast16_t");
    RESERVED_C_WORD.add("int_fast32_t");
    RESERVED_C_WORD.add("uint_fast32_t");
    RESERVED_C_WORD.add("int_fast64_t");
    RESERVED_C_WORD.add("uint_fast64_t");
    RESERVED_C_WORD.add("intptr_t");
    RESERVED_C_WORD.add("uintptr_t");
    RESERVED_C_WORD.add("intmax_t");
    RESERVED_C_WORD.add("uintmax_t");
    RESERVED_C_WORD.add("size_t");
    RESERVED_C_WORD.add("ptrdiff_t");
    RESERVED_C_WORD.add("wchar_t");
    RESERVED_C_WORD.add("offsetof");
    RESERVED_C_WORD.add("NULL");
  }

  private ReservedWordsChecker() {
  }

  /**
   * Checks the given name against a list of C/C++ reserved keywords.
   * 
   * @param name the name to be checked
   * @return true if the name is reserved C/C++ keyword.
   */
  public static boolean isReservedCWord(final String name) {
    return RESERVED_C_WORD.contains(name);
  }
}
