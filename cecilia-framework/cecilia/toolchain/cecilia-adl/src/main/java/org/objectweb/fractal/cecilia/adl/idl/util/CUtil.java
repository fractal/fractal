/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.idl.util;

import static org.objectweb.fractal.cecilia.adl.idl.util.Util.getContainedDefinition;
import static org.objectweb.fractal.cecilia.adl.idl.util.Util.getContainedType;
import static org.objectweb.fractal.cecilia.adl.idl.util.Util.getPrimitiveType;
import static org.objectweb.fractal.cecilia.adl.idl.util.Util.hasVarParams;
import static org.objectweb.fractal.cecilia.adl.idl.util.Util.isConstParameter;
import static org.objectweb.fractal.cecilia.adl.idl.util.Util.isInOutParameter;
import static org.objectweb.fractal.cecilia.adl.idl.util.Util.isOutParameter;

import org.objectweb.fractal.cecilia.adl.idl.ast.ArrayOf;
import org.objectweb.fractal.cecilia.adl.idl.ast.ComplexType;
import org.objectweb.fractal.cecilia.adl.idl.ast.Field;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Method;
import org.objectweb.fractal.cecilia.adl.idl.ast.Parameter;
import org.objectweb.fractal.cecilia.adl.idl.ast.ParameterContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.PointerOf;
import org.objectweb.fractal.cecilia.adl.idl.ast.PrimitiveType;
import org.objectweb.fractal.cecilia.adl.idl.ast.Type;
import org.objectweb.fractal.cecilia.adl.idl.ast.PrimitiveType.PrimitiveTypeEnum;

/**
 * Utility class for C code generation.
 */
public final class CUtil {

  public static final String CECILIA_ARGS_MACRO = "__CECILIA_ARGS";
  public static final String CECILIA_PARAMS_MACRO = "__CECILIA_PARAMS";

  private CUtil() {
  }

  /**
   * Returns the equivalent in C of the given primitive or complex type.
   * 
   * @param type the type
   * @return the equivalent in C of the given primitive or complex type.
   */
  public static String buildComplexOrPrimitiveType(final Type type) {

    if (type == null) {
      throw new IllegalArgumentException("Type argument can't be null");
    }

    if (type instanceof ComplexType) {
      final IDLDefinition containedDefinition = getContainedDefinition((ComplexType) type);
      if (containedDefinition == null) {
        throw new IllegalStateException(
            "The ComplexType contains a null IDLDefinition ("
                + ((ComplexType) type).getName() + ")");
      }
      return containedDefinition.getName().replace('.', '_');
    } else if (type instanceof PrimitiveType) {
      return buildPrimitiveType((PrimitiveType) type);
    } else {
      throw new IllegalArgumentException("Invalid type");
    }
  }

  /**
   * Returns the equivalent in C of the given type.
   * 
   * @param type the primitive type
   * @return the equivalent in C of the given type.
   */
  public static String buildPrimitiveType(final PrimitiveType type) {
    return buildPrimitiveType(getPrimitiveType(type));
  }

  /**
   * Returns the equivalent in C of the given type.
   * 
   * @param type the primitive type
   * @return the equivalent in C of the given type.
   */
  public static String buildPrimitiveType(final PrimitiveTypeEnum type) {
    return type.getCType();
  }

  /**
   * Returns the equivalent in C of the given return type.
   * 
   * @param type the type
   * @return the equivalent in C of the given return type.
   */
  public static String buildReturnType(Type type) {
    final StringBuffer result = new StringBuffer();
    int nbPointer = 0;

    // A return type is an [ArrayOf](PointerOf)*(PrimitiveType|ComplexType)
    if (type instanceof ArrayOf) {
      nbPointer++;
      type = getContainedType((ArrayOf) type);
    }

    while (type instanceof PointerOf) {
      nbPointer++;
      type = getContainedType((PointerOf) type);
    }

    result.append(buildComplexOrPrimitiveType(type));

    for (int i = 0; i < nbPointer; i++)
      result.append('*');

    return result.toString();
  }

  /**
   * @param parameter the parameter definition to be built.
   * @return the parameter definition in C
   */
  public static String buildParameter(final Parameter parameter) {
    final StringBuilder result = new StringBuilder();
    Type type = getContainedType(parameter);

    String arraySpec = "";
    int nbPointer = 0;

    while (type instanceof ArrayOf) {
      final String size = ((ArrayOf) type).getSize();
      if (size != null) {
        arraySpec = "[" + size + "]" + arraySpec;
      } else {
        arraySpec = "[]" + arraySpec;
      }
      type = getContainedType((ArrayOf) type);
    }

    while (type instanceof PointerOf) {
      nbPointer++;
      type = getContainedType((PointerOf) type);
    }

    if (isConstParameter(parameter)) {
      result.append("const ");
    }

    result.append(buildComplexOrPrimitiveType(type));

    for (int i = 0; i < nbPointer; i++)
      result.append('*');
    result.append(' ');

    if (isOutParameter(parameter) || isInOutParameter(parameter)) {
      result.append("(*").append(parameter.getName()).append(')');
    } else {
      result.append(parameter.getName());
    }
    result.append(arraySpec);
    return result.toString();
  }

  /**
   * Builds a definition suitable for inclusion e.g. in attribute initializers
   * (routine similar to buildReturnType, but it handles arrays differently)
   *
   * @param type the type
   * @return the definition in C
   */
  public static String buildDeclarationType(final Type type) {
    final StringBuilder result = new StringBuilder();
    Type type2 = type;

    String arraySpec = "";
    int nbPointer = 0;

    while (type2 instanceof ArrayOf) {
      final String size = ((ArrayOf) type2).getSize();
      if (size != null) {
        arraySpec = "[" + size + "]" + arraySpec;
      } else {
        arraySpec = "[]" + arraySpec;
      }
      type2 = getContainedType((ArrayOf) type2);
    }

    while (type2 instanceof PointerOf) {
      nbPointer++;
      type2 = getContainedType((PointerOf) type2);
    }

    result.append(buildComplexOrPrimitiveType(type2));

    for (int i = 0; i < nbPointer; i++)
      result.append('*');
    result.append(' ');

    result.append(arraySpec);
    return result.toString();
  }

  /**
   * @param field the field of a record definition to be built.
   * @return the field definition in C
   */
  public static String buildRecordField(final Field field) {
    return buildRecordField(field, field.getName());
  }

  /**
   * @param field the field of a union definition to be built.
   * @return the field definition in C
   */
  public static String buildUnionField(final Field field) {
    return buildUnionField(field, field.getName());
  }

  /**
   * @return <code>true</code> if the IDL AST Node being a PrimitiveType is
   *         also a C primitive data type.
   */
  public static boolean isIDLPrimitiveTypeAlsoCPrimitiveType(
      final PrimitiveType type) {
    final String primitiveTypeName = (type).getName();

    /* and if this field is neither STRING nor ANY */
    if (primitiveTypeName.equals(PrimitiveType.PrimitiveTypeEnum.STRING.name())
        || primitiveTypeName.equals(PrimitiveType.PrimitiveTypeEnum.ANY.name())) {
      return false;
    }

    return true;
  }

  /**
   * @param field the field of a record definition to be built.
   * @param name the name of the field to use.
   * @return the field definition in C
   */
  public static String buildRecordField(final Field field, final String name) {
    final StringBuilder result = new StringBuilder();
    Type type = getContainedType(field);

    boolean alreadyWrittenConst = false;

    /* if the field is of a Cecilia IDL primitive type */
    if (Util.isPrimitiveType(type)) {

      if (isIDLPrimitiveTypeAlsoCPrimitiveType((PrimitiveType) type)) {

        /* and if the field has the 'const' qualifier */
        if (Util.isConstField(field)) {
          result.append("const ");
          alreadyWrittenConst = true;
        }
      }

    }

    String arraySpec = "";
    int nbPointer = 0;

    while (type instanceof ArrayOf) {
      final String size = ((ArrayOf) type).getSize();
      if (size != null) {
        arraySpec = "[" + size + "]" + arraySpec;
      } else {
        arraySpec = "[]" + arraySpec;
      }
      type = getContainedType((ArrayOf) type);
    }

    while (type instanceof PointerOf) {
      nbPointer++;
      type = getContainedType((PointerOf) type);
    }

    result.append(buildComplexOrPrimitiveType(type));

    for (int i = 0; i < nbPointer; i++)
      result.append('*');
    result.append(' ');
    if (Util.isConstField(field) && !alreadyWrittenConst) {
      result.append("const ");
    }
    result.append(name).append(arraySpec);
    return result.toString();
  }

  /**
   * @param field the field of a union definition to be built.
   * @param name the name of the field to use.
   * @return the field definition in C
   */
  public static String buildUnionField(final Field field, final String name) {
    final StringBuilder result = new StringBuilder();
    Type type = getContainedType(field);

    boolean alreadyWrittenConst = false;

    /* if the field is of a Cecilia IDL primitive type */
    if (Util.isPrimitiveType(type)) {

      if (isIDLPrimitiveTypeAlsoCPrimitiveType((PrimitiveType) type)) {

        /* and if the field has the 'const' qualifier */
        if (Util.isConstField(field)) {
          result.append("const ");
          alreadyWrittenConst = true;
        }
      }

    }

    String arraySpec = "";
    int nbPointer = 0;

    while (type instanceof ArrayOf) {
      final String size = ((ArrayOf) type).getSize();
      if (size != null) {
        arraySpec = "[" + size + "]" + arraySpec;
      } else {
        arraySpec = "[]" + arraySpec;
      }
      type = getContainedType((ArrayOf) type);
    }

    while (type instanceof PointerOf) {
      nbPointer++;
      type = getContainedType((PointerOf) type);
    }

    result.append(buildComplexOrPrimitiveType(type));

    for (int i = 0; i < nbPointer; i++)
      result.append('*');
    result.append(' ');
    if (Util.isConstField(field) && !alreadyWrittenConst) {
      result.append("const ");
    }
    result.append(name).append(arraySpec);
    return result.toString();
  }

  /**
   * @param type the signature of the type to be included.
   * @return the include definition in C for the given type.
   */
  public static String buildInclude(final String type) {
    final StringBuilder result = new StringBuilder();
    result.append("#include \"").append(type.replace('.', '/')).append(
        ".idl.h\"").append("\n");
    return result.toString();
  }

  /**
   * @param method the method definition
   * @param pointer indicates whether the built string corresponds to a method
   *            definition or to a method definition pointer.
   * @return the method definition in C.
   */
  public static String buildMethodDefinition(final Method method,
      final boolean pointer) {
    return buildMethodDefinition(method, pointer, method.getName());
  }

  /**
   * Builds method with a specific name.
   * 
   * @param method the method definition
   * @param pointer indicates whether the built string corresponds to a method
   *            definition or to a method pointer definition.
   * @param methodName the specific name to be used.
   * @return the method definition in C.
   */
  public static String buildMethodDefinition(final Method method,
      final boolean pointer, final String methodName) {
    return generateMethodDefinition(method, pointer, methodName, "void");
  }

  public static String buildStaticMethodDefinition(final Method method,
      final boolean pointer) {
    return buildStaticMethodDefinition(method, pointer, method.getName());
  }

  public static String buildStaticMethodDefinition(final Method method,
      final boolean pointer, final String methodName) {
    return generateMethodDefinition(method, pointer, methodName, null);
  }

  /**
   * Builds method with a specific name.
   *
   * @param method the method definition
   * @param pointer indicates whether the built string corresponds to a method
   *            definition or to a method pointer definition.
   * @param methodName the specific name to be used.
   * @param thisArgumentPointerType the type of the '*_this' argument.
   * @return the method definition in C.
   */
  protected static String generateMethodDefinition(final Method method,
      final boolean pointer, final String methodName,
      final String thisArgumentPointerType) {

    final StringBuilder result = new StringBuilder();

    /* the return type */
    result.append(buildReturnType(getContainedType(method)));

    result.append(" ");
    if (pointer) result.append("(*");
    result.append(methodName);
    if (pointer) result.append(')');

    /* the parameters */
    final Parameter[] parameters = ((ParameterContainer) method)
        .getParameters();

    /* start argument list */
    result.append("(");
    /* if there is a _this argument type */
    if (thisArgumentPointerType != null) {
      result.append(thisArgumentPointerType + " *_this");
      if (parameters.length > 0) {
        result.append(", ");
      }
    }

    if (parameters.length == 0 && thisArgumentPointerType == null) {
      result.append("void");
    } else {
      for (int i = 0; i < parameters.length; i++) {
        if (i != 0) {
          result.append(", ");
        }
        result.append(buildParameter(parameters[i]));
      }
      if (hasVarParams(method)) {
        result.append(", ...");
      }
    }

    /* end argument list */
    result.append(')');

    return result.toString();
  }
}
