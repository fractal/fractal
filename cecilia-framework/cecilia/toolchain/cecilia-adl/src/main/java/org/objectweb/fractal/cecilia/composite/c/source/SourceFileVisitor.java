/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.composite.c.source;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.TypeNameProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.file.SourceFile;
import org.objectweb.fractal.cecilia.adl.file.SourceFileProvider;
import org.objectweb.fractal.cecilia.adl.file.SourceFileWriter;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.Executable;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the source file containing the components'
 * definition and instance code.
 */
public class SourceFileVisitor extends AbstractTaskFactoryUser
    implements
      ComponentVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits a {@link ComponentContainer} node and creates a task that creates
   * the source file containing the components' definition and instance code.
   */
  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    return taskFactoryItf.newPrimitiveTask(new SourceFileTask((File) context
        .get("adlBuildDirectory")), container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds the ".adl.c" source file that contains the definition of the
   * component structure, the instantiation/initialization of the static
   * instance.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "component-definition-file-provider", signature = SourceFileProvider.class, record = "role:componentDefinitionFile, id:%", parameters = "componentNode"))
  protected static class SourceFileTask
      implements
        Executable,
        SourceFileProvider {

    /** The suffix of the generated file. */
    public static final String FILE_NAME_SUFFIX = ".adl.c";

    protected final File       adlBuildDirectory;

    // Produced source file
    protected SourceFile       sourceFile;

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /** The name of the component type. */
    @ClientInterface(name = "type-name-provider", record = "role:typeNameProvider, id:%", parameters = "componentNode")
    public TypeNameProvider    typeNameProviderItf;

    /** Source code for the component definition. */
    @ClientInterface(name = "component-definition-provider", record = "role:componentDefinition, id:%", parameters = "componentNode")
    public SourceCodeProvider  definitionProviderItf;

    /** Source code for the component instantiation. */
    @ClientInterface(name = "component-instantiation-provider", record = "role:componentInstantiation, id:%", parameters = "componentNode")
    public SourceCodeProvider  instantiationProviderItf;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param adlBuildDirectory The directory into which the generated file will
     *          be placed.
     */
    public SourceFileTask(final File adlBuildDirectory) {
      this.adlBuildDirectory = adlBuildDirectory;
    }

    // -------------------------------------------------------------------------
    // Utility methods
    // -------------------------------------------------------------------------

    protected void prepareSourceCode(final CodeWriter cw) {
      // append source code that defines component type.
      cw.append(definitionProviderItf.getSourceCode()).endl();

      // append instantiation code.
      cw.appendln(instantiationProviderItf.getSourceCode()).endl();
    }

    // -------------------------------------------------------------------------
    // Implementation of the Executable interface
    // -------------------------------------------------------------------------

    public void execute() throws Exception {
      final String fileName = typeNameProviderItf.getCTypeName()
          + FILE_NAME_SUFFIX;
      final File outputFile = new File(adlBuildDirectory, fileName);

      final CodeWriter cw = new CodeWriter();
      cw.appendln("// THIS FILE HAS BEEN GENERATED BY THE CECILIA ADL "
          + "COMPILER.");
      cw.appendln("// DO NOT EDIT").endl();
      prepareSourceCode(cw);

      try {
        SourceFileWriter.writeToFile(outputFile, cw.toString());
      } catch (final IOException e) {
        throw new ADLException(GenericErrors.INTERNAL_ERROR, e,
            "An error occurs while writing to file '"
                + outputFile.getAbsolutePath() + "'");
      }

      sourceFile = new SourceFile(typeNameProviderItf.getTypeName(), outputFile);
    }

    // -------------------------------------------------------------------------
    // Implementation of the SourceFileProvider interface
    // -------------------------------------------------------------------------

    public SourceFile getSourceFile() {
      return sourceFile;
    }
  }
}
