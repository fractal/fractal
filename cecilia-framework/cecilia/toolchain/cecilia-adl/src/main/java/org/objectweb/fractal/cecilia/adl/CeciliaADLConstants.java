/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl;

/**
 * This interface is meant to store commonly used constants and String literals
 * in Cecilia ADL.
 * 
 * @author Alessio Pace
 */
public interface CeciliaADLConstants {

  /**
   * Cecilia ADL DTD.
   */
  String CECILIA_ADL_DTD              = "classpath://org/objectweb/fractal/cecilia/adl/parser/xml/cecilia.dtd";

  /**
   * The qualified name which represents a &lt;definition&gt; element in the
   * ADL. The concrete type of the AST node depends on the DTD being used by the
   * XMLNodeFactory.
   */
  String DEFINITION_AST_NODE_NAME     = "definition";

  /**
   * The qualified name which represents a &lt;component&gt; element in the ADL.
   * The concrete type of the AST node depends on the DTD being used by the
   * XMLNodeFactory.
   */
  String COMPONENT_AST_NODE_NAME      = "component";

  /**
   * The qualified name which represents an &lt;interface&gt; element in the
   * ADL. The concrete type of the AST node depends on the DTD being used by the
   * XMLNodeFactory.
   */
  String INTERFACE_AST_NODE_NAME      = "interface";

  /**
   * The qualified name which represents a &lt;binding&gt; element in the ADL.
   * The concrete type of the AST node depends on the DTD being used by the
   * XMLNodeFactory.
   */
  String BINDING_AST_NODE_NAME        = "binding";

  /**
   * The qualified name which represents a &lt;content&gt; element in the ADL.
   * The concrete type of the AST node depends on the DTD being used by the
   * XMLNodeFactory.
   */
  String IMPLEMENTATION_AST_NODE_NAME = "implementation";

  /**
   * The qualified name which represents an &lt;include&gt; element in the ADL
   * (it's specific to Cecilia ADL). The concrete type of the AST node depends
   * on the DTD being used by the XMLNodeFactory.
   */
  String INCLUDE_AST_NODE_NAME        = "include";

  /**
   * The qualified name which represents a &lt;controller&gt; element in the
   * ADL. The concrete type of the AST node depends on the DTD being used by the
   * XMLNodeFactory.
   */
  String CONTROLLER_AST_NODE_NAME     = "controller";

  /**
   * The qualified name which represents an &lt;attributes&gt; element in the
   * ADL. The concrete type of the AST node depends on the DTD being used by the
   * XMLNodeFactory.
   */
  String ATTRIBUTES_AST_NODE_NAME      = "attributes";

  /**
   * The qualified name which represents an &lt;attribute&gt; element in the ADL
   * (it's specific to Cecilia ADL). The concrete type of the AST node depends
   * on the DTD being used by the XMLNodeFactory.
   */
  String ATTRIBUTE_AST_NODE_NAME      = "attribute";
}
