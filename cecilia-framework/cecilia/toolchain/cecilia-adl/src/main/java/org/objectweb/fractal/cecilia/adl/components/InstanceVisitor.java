/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.adl.components;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.ast.ASTFractalUtil;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.InstanceNameProvider;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Creates a {@link InstanceNameProvider} task for the visited component.
 */
public class InstanceVisitor extends AbstractTaskFactoryUser
    implements
      ComponentVisitor {

  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    return taskFactoryItf.newPrimitiveTask(new InstanceNameProviderImpl(
        getAbsoluteName(path, container)), container);
  }

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  protected String getAbsoluteName(final List<Node> path,
      final ComponentContainer container) {
    String realname = ASTFractalUtil.getFcName((ComponentContainer)container);
    for (int i = path.size() - 1; i >= 0; i--)
      realname = ASTFractalUtil.getFcName((ComponentContainer)(path.get(i))) + "/" + realname;
    return realname;
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Provides the name of the given component.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "instance-name-provider", signature = InstanceNameProvider.class, record = "role:instanceNameProvider, id:%", parameters = "componentNode"))
  public static class InstanceNameProviderImpl implements InstanceNameProvider {

    protected final String instanceName;
    protected final String cInstanceName;

    // -------------------------------------------------------------------------
    // Constructor
    // -------------------------------------------------------------------------

    /**
     * @param instanceName the name provided by this task.
     */
    public InstanceNameProviderImpl(final String instanceName) {
      this.instanceName = instanceName;
      cInstanceName = instanceName.replace('.', '_').replace('-', '_').replace(
          '/', '_');
    }

    // -------------------------------------------------------------------------
    // Implementation of the InstanceNameProvider interface
    // -------------------------------------------------------------------------

    public String getInstanceName() {
      return instanceName;
    }

    public String getCInstanceName() {
      return cInstanceName;
    }
  }
}
