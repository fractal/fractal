/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 * Contributors: Alessio Pace, Lionel Debroux
 */

package org.objectweb.fractal.cecilia.primitive.optC.implementations;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;
import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationDecorationUtil.getCode;
import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationHelper.hasConstructor;
import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationHelper.hasDestructor;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.ast.ASTFractalUtil;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.components.merge.MergeUtil;
import org.objectweb.fractal.cecilia.adl.bindings.MergedBindingResolverLoader;
import org.objectweb.fractal.cecilia.adl.components.MergeableComponent;
import org.objectweb.fractal.cecilia.adl.components.merge.MergeUtil;
import org.objectweb.fractal.cecilia.adl.components.merge.MergedInclude;
import org.objectweb.fractal.cecilia.adl.components.merge.MergedInterface;
import org.objectweb.fractal.cecilia.adl.components.merge.MergedInterfaceRole;
import org.objectweb.fractal.cecilia.adl.directives.Include;
import org.objectweb.fractal.cecilia.adl.directives.IncludeContainer;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.file.SourceFile;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.Method;
import org.objectweb.fractal.cecilia.adl.idl.ast.MethodContainer;
import org.objectweb.fractal.cecilia.adl.idl.util.CUtil;
import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;
import org.objectweb.fractal.cecilia.adl.types.TypeDecorationUtil;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.TaskFactory;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that provides the inclusion code of the implementation file
 * for <code>optC</code> dialect.
 */
public class MergedMacroDefinitionVisitor extends MacroDefinitionVisitor {

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------
  @Override
  protected Component createImplementationTask(
      final ComponentContainer container, final Implementation impl,
      final boolean isSingleton) throws TaskException {
    if (MergeUtil.isMergedPrimitive(container)) {
      return taskFactoryItf.newPrimitiveTask(new MergedImplementationTask(
        hasConstructor(container), hasDestructor(container), container,
        impl, isSingleton), container);
    }
    return null;
  }

  @Override
  protected Component createModuleImplementationTask(
      final ComponentContainer container, final Implementation impl,
      final Include include, final boolean isSingleton) throws TaskException {
    if (MergeUtil.isMergedPrimitive(container)) {
      return taskFactoryItf.newPrimitiveTask(new MergedModuleImplementationTask(
        hasConstructor(container), hasDestructor(container), container,
        impl, include, isSingleton), container, include);
    }
    return null;
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  protected abstract static class MergedAbstractImplementationTask
      extends AbstractImplementationTask {

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param hasConstructor <code>true</code> if the component has a
     *          constructor.
     * @param hasDestructor <code>true</code> if the component has a destructor.
     */
    public MergedAbstractImplementationTask(final boolean hasConstructor,
        final boolean hasDestructor) {
      super(hasConstructor, hasDestructor);
    }

    protected MergeableComponent currentComponent;

    protected Set<String> getInterfaceDefinitionMethodNames(
        final IDLDefinition interfaceDefinition) {

      if (interfaceDefinition == null) {
        throw new IllegalArgumentException(
            "InterfaceDefinition argument can't be null");
      }

      final Set<String> result = new HashSet<String>();

      for (final Method m : ((MethodContainer) interfaceDefinition).getMethods()) {
        result.add(m.getName().replace('-','_'));
      }

      return result;
    }

    protected void undefineServerItf(final CodeWriter cw,
        final String mergedItfName) {
      cw.appendln("/* note: SERVER_ITF(" + mergedItfName
          + ") will no longer be usable in this fused module */");
      cw.appendln("#ifdef " + "SERVER_" + mergedItfName);
      cw.appendln("#undef SERVER_" + mergedItfName);
      cw.appendln("#endif").endl();
    }

    /**
     * @param cw
     * @param includeInterfaceTranslation
     */
    protected void defineServerOuterInterface(final CodeWriter cw,
        final MergedInterface mergedItf) {

      final String mergedItfName = mergedItf.getName().replace('-', '_');
      cw.appendln("/* -START- server outer translation of server interface '"
          + mergedItfName + "' -- */");

      undefineServerItf(cw, mergedItfName);

      final IDLDefinition interfaceDefintion = ((IDLDefinitionContainer) mergedItf)
          .getIDLDefinition();

      if (interfaceDefintion == null) {
        throw new IllegalStateException("The merged server outer interface '"
            + mergedItfName + "' should contain a non null InterfaceDefintion");
      }

      String translatedName = null;
      TypeInterface outerItf = null;

      /*
       * find the server interface on the merged primitive which exports the
       * current server interface of this fused module
       */
      for (final Interface itf : ASTFractalUtil
          .getFcFunctionalServerInterfaces(this.currentComponent)) {

        final TypeInterface mergedPrimitiveServerItf = (TypeInterface) itf;
        if (MergedBindingResolverLoader.getInnerBoundTo(mergedPrimitiveServerItf) == mergedItf) {
          outerItf = mergedPrimitiveServerItf;
          translatedName = mergedPrimitiveServerItf.getName();
          break;
        }
      }

      final String afterOpenParenthesis;
      final String beforeCloseParenthesis;
      if (InterfaceDecorationUtil.hasToBeCompiledAsStatic(outerItf)) {
        afterOpenParenthesis = "";
        beforeCloseParenthesis = CUtil.CECILIA_ARGS_MACRO;
      }
      else {
        afterOpenParenthesis = "_this ";
        beforeCloseParenthesis = CUtil.CECILIA_PARAMS_MACRO;
      }

      final Set<String> allMethodNames = this
          .getInterfaceDefinitionMethodNames(interfaceDefintion);

      final String methodDefinitionMacroToUse;
      if (MergeUtil.hasToBeCompiledAsStatic(mergedItf)) {
        methodDefinitionMacroToUse = "STATIC_METHOD";
      }
      else {
        methodDefinitionMacroToUse = "METHOD";
      }

      /* it should provide the definitions for the various SERVER macros */
      for (final String methName : allMethodNames) {
        final String currentServerItfMethodName = "SERVER_" + mergedItfName + "_"
            + methName;
        cw.appendln("#ifdef " + currentServerItfMethodName);
        cw.appendln("#undef " + currentServerItfMethodName);
        cw.appendln("#endif");

        cw.appendln("#define " + currentServerItfMethodName + " METHOD_NAME("
            + translatedName.replace('-', '_') + "," + methName + ")("
            + afterOpenParenthesis + " " + beforeCloseParenthesis);
      }

      cw.appendln(
          "/* -END- server outer translation of server interface '"
              + mergedItfName + "' -- */").endl();
    }

    /**
     * @param cw
     * @param includeInterfaceTranslation
     */
    protected void defineServerInnerInterface(final CodeWriter cw,
        final MergedInterface mergedItf) {

      final String mergedItfName = mergedItf.getName().replace('-', '_');
      cw.appendln("/* -START- server inner translation of server interface '"
          + mergedItfName + "' */");

      /* undefine previous definitions */
      undefineServerItf(cw, mergedItfName);

      final IDLDefinition interfaceDefintion = ((IDLDefinitionContainer) mergedItf)
          .getIDLDefinition();

      if (interfaceDefintion == null) {
        throw new IllegalStateException("The merged interface '" + mergedItfName
            + "' should contain a non null InterfaceDefintion");
      }


      /* if it is a static interface */
      final boolean hasToBeCompiledAsStatic = MergeUtil
          .hasToBeCompiledAsStatic(mergedItf);

      final String methodDefinitionMacroToUse;
      final String afterOpenParenthesis;
      final String beforeCloseParenthesis;
      if (hasToBeCompiledAsStatic) {
        methodDefinitionMacroToUse = "STATIC_METHOD";
        afterOpenParenthesis = "";
        beforeCloseParenthesis = CUtil.CECILIA_ARGS_MACRO;
      }
      else {
        methodDefinitionMacroToUse = "METHOD";
        afterOpenParenthesis = "_this ";
        beforeCloseParenthesis = CUtil.CECILIA_PARAMS_MACRO;
      }

      final Set<String> allMethodNames = this
          .getInterfaceDefinitionMethodNames(interfaceDefintion);

      /* it should provide the definitions for the various METH and SERVER macros */
      for (final String methName : allMethodNames) {
        final String currentMethMacro = "METH_" + methName;
        cw.appendln("#ifdef " + currentMethMacro);
        cw.appendln("#undef " + currentMethMacro);
        cw.appendln("#endif");
        final String translatedServerItfName = MergeUtil.translateServerInnerItfName(mergedItf);

        cw.appendln("#define " + currentMethMacro + " "
            + methodDefinitionMacroToUse + "(" + translatedServerItfName + ", "
            + methName + ")");

        final String currentServerItfMethodName = "SERVER_" + mergedItfName + "_"
            + methName;
        cw.appendln("#ifdef " + currentServerItfMethodName);
        cw.appendln("#undef " + currentServerItfMethodName);
        cw.appendln("#endif");

        cw.appendln("#define " + currentServerItfMethodName + " "
            + "METHOD_NAME(" + translatedServerItfName + ", " + methName + ")("
            + afterOpenParenthesis + " " + beforeCloseParenthesis);
      }

      cw.appendln("/* -END- server inner translation of server interface '"
              + mergedItfName + "' */").endl();
    }

    /**
     * @param cw
     * @param includeInterfaceTranslation
     */
    protected void defineClientInnerInterface(final CodeWriter cw,
        final MergedInterface mergedItf) {

      final String mergedItfName = mergedItf.getName().replace('-', '_');
      final String clientItfMacro = "CLIENT_" + mergedItfName;

      cw.appendln("/* -START- client inner translation of client interface '"
          + mergedItfName + "' */");

      cw.appendln("/* note: CLIENT_ITF(" + mergedItfName
          + ") will no longer be usable in this fused module */");
      cw.appendln("#ifdef " + clientItfMacro);
      cw.appendln("#undef " + clientItfMacro);
      cw.appendln("#endif").endl();

      final IDLDefinition interfaceDefintion = ((IDLDefinitionContainer) mergedItf)
          .getIDLDefinition();

      if (interfaceDefintion == null) {
        throw new IllegalStateException("The merged interface '" + mergedItfName
            + "' should contain a non null InterfaceDefintion");
      }

      /* if it has to be compiled as static */
      final boolean hasToBeCompiledAsStatic = MergeUtil
          .hasToBeCompiledAsStatic(mergedItf);

      final MergedInterface boundToItf = (MergedInterface) MergedBindingResolverLoader
          .getInnerBoundTo(mergedItf);

      String translatedName = null;
      final MergedInterfaceRole boundToItfMergedRole = MergedBindingResolverLoader
          .getMergedInterfaceRole(boundToItf);
      if (boundToItfMergedRole == MergedInterfaceRole.SERVER_INNER) {
        translatedName = MergeUtil.translateServerInnerItfName(boundToItf);
      } else if (boundToItfMergedRole == MergedInterfaceRole.SERVER_OUTER) {
        TypeInterface exportedInterface = null;
        for (final TypeInterface itf : ASTFractalUtil
            .getFcFunctionalServerInterfaces(currentComponent)) {
          if (MergedBindingResolverLoader.getInnerBoundTo(itf) == boundToItf) {
            exportedInterface = itf;
            break;
          }
        }

        if (exportedInterface == null) {
          // todo logs the name of the include having this interface
          throw new IllegalStateException(
              "There is no which as an INNER_BOUND_TO interface decoration referencing the '"
                  + boundToItf.getName() + "' of the fused include");
        }
        translatedName = exportedInterface.getName();
      } else {
        throw new IllegalStateException(
            "It was expecting to be either SERVER_OUTER or SERVER_INNER but it is: "
                + boundToItfMergedRole);
      }

      /* do not pass the _this if it is a static interface */
      String afterOpenParenthesis = "_this ";
      String beforeCloseParenthesis = CUtil.CECILIA_PARAMS_MACRO;
      if (hasToBeCompiledAsStatic) {
        afterOpenParenthesis = "";
        beforeCloseParenthesis = CUtil.CECILIA_ARGS_MACRO;
      }

      final Set<String> allMethodNames = this
          .getInterfaceDefinitionMethodNames(interfaceDefintion);
      for (final String methName : allMethodNames) {

        final String clientItfMethodName = clientItfMacro + "_" + methName;

        cw.appendln("#ifdef " + clientItfMethodName);
        cw.appendln("#undef " + clientItfMethodName);
        cw.appendln("#endif");

        cw.appendln("#define " + clientItfMethodName + " METHOD_NAME("
            + translatedName.replace('-', '_') + ", " + methName + ")("
            + afterOpenParenthesis + " " + beforeCloseParenthesis);

      }

      cw.appendln("/* -END- client inner translation of client interface '"
              + mergedItfName + "' */").endl();
    }

    protected void defineClientOuterInterface(final CodeWriter cw,
        final MergedInterface mergedItf) {

      final String mergedItfName = mergedItf.getName().replace('-', '_');
      cw.appendln("/* -START- client outer translation of client interface '"
          + mergedItfName + "' */");

      final String clientItfMacro = "CLIENT_" + mergedItfName;

      final TypeInterface boundToItf = MergedBindingResolverLoader
          .getInnerBoundTo(mergedItf);
      final String boundToItfName = boundToItf.getName().replace('-', '_');

      if (!mergedItfName.equals(boundToItfName)) {

        cw.appendln("#ifdef " + clientItfMacro);
        cw.appendln("#undef " + clientItfMacro);
        cw.appendln("#endif");
        cw.appendln("#define " + clientItfMacro + " CLIENT_OUTER_"
            + boundToItfName).endl();

        final IDLDefinition interfaceDefinition = ((IDLDefinitionContainer) boundToItf)
            .getIDLDefinition();

        final Set<String> allMethodNames = this
            .getInterfaceDefinitionMethodNames(interfaceDefinition);

        for (final String methName : allMethodNames) {
          final String currentClientItfMethodmacro = clientItfMacro + "_"
              + methName;
          cw.appendln("#ifdef " + currentClientItfMethodmacro);
          cw.appendln("#undef " + currentClientItfMethodmacro);
          cw.appendln("#endif");
          cw.appendln("#define " + currentClientItfMethodmacro + " CLIENT_OUTER_"
              + boundToItfName + "_" + methName);

        }
      }

      cw.appendln("/* -END- client outer translation of client interface '"
              + mergedItfName + "' */").endl();

    }
  }

            /**
   * Builds definition of implementation source code for the given component
   * node. This task defines the macros that are used in the implementation file
   * and then includes the implementation file.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "implementation-provider", signature = SourceCodeProvider.class, record = "role:implementation, id:%", parameters = "componentNode"))
  public static class MergedImplementationTask extends
      MergedAbstractImplementationTask {

    public MergedImplementationTask(final boolean hasConstructor,
        final boolean hasDestructor, final ComponentContainer container,
        final Implementation impl, final boolean isSingleton) {
      super(hasConstructor, hasDestructor);
      this.container = container;
      this.impl = impl;
      this.isSingleton = isSingleton;
    }

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /** Client interface used to retrieve the implementation code. */
    @ClientInterface(name = "implementation-source-code", record = "role:implementationSourceCode, id:%", parameters = "componentNode")
    public SourceCodeProvider implementationSourceCodeProviderItf;

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of MergedAbstractImplementationTask
    // -------------------------------------------------------------------------
    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter("Merged Primitive Implementation Definition (Macro) Builder");
      if (isSingleton) {
        cw.appendln("#define IS_SINGLETON");
      }

      /* the cecilia header */
      cw.appendln("#include \"cecilia_opt2.h\"").endl();

      commonProcessSourceCode(cw);

      cw.appendln("#ifndef __CECILIA_OPT2_H__");
      cw.appendln("#error You forgot to add an empty `typedef struct { ... } PRIVATE_DATA`. This is required.");
      cw.appendln("#endif");

      if (isSingleton) {
        cw.appendln("#undef IS_SINGLETON");
      }

      return cw.toString();
    }

    @Override
    protected void afterPrivateDataMacroDefinition(final CodeWriter cw,
        final String componentCName) throws Exception {

      super.afterPrivateDataMacroDefinition(cw, componentCName);

      if (!(impl instanceof IncludeContainer)) {
        throw new ADLException("contentDesc argument should be of type "
            + IncludeContainer.class + " but was a " + impl.getClass());
      }

      final IncludeContainer includeContainer = (IncludeContainer) impl;

      cw.appendln(
          "/* defining the PRIVATE_DATA struct of this merged primitive */");
      cw.append("typedef struct {").endl();

      /*
       * generate pointers to the instance data structures of each of the
       * primitive merged components
       */
      final Include[] includes = includeContainer.getIncludes();
      for (int i = 0; i < includes.length; i++) {
        cw.append("void* merged_" + i + ";").endl();
      }
      cw.append("} PRIVATE_DATA;").endl();

      cw.endl();
    }

    @Override
    protected void appendImplementationCode(final CodeWriter cw) {
      cw.appendln(implementationSourceCodeProviderItf.getSourceCode());
    }
  }

  /**
   * Builds definition of implementation source code for the given module of the
   * given component node. This task defines the macros that are used in the
   * implementation file and then includes the implementation file.
   */
  @TaskParameters({"componentNode", "moduleNode"})
  @ServerInterfaces(@ServerInterface(name = "implementation-provider", signature = SourceCodeProvider.class, record = "role:implementation, id:%, module:%", parameters = {
      "componentNode", "moduleNode"}))
  public static class MergedModuleImplementationTask
      extends MergedAbstractImplementationTask {

    public MergedModuleImplementationTask(final boolean hasConstructor,
        final boolean hasDestructor, final ComponentContainer container,
        final Implementation impl, final Include include,
        final boolean isSingleton) {
      super(hasConstructor, hasDestructor);
      this.container = container;
      this.impl = impl;
      this.include = include;
      this.isSingleton = isSingleton;
    }

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /** Client interface used to retrieve the module of implementation code. */
    @ClientInterface(name = "implementation-source-code", record = "role:implementationSourceCode, id:%, module:%", parameters = {
        "componentNode", "moduleNode"})
    public SourceCodeProvider implementationModuleSourceCodeProviderItf;

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of MergedAbstractImplementationTask
    // -------------------------------------------------------------------------
    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter("Merged Primitive Implementation Definition (Macro) (Module) Builder");
      if (isSingleton) {
        cw.appendln("#define IS_SINGLETON");
      }

      commonProcessSourceCode(cw);

      if (isSingleton) {
        cw.appendln("#undef IS_SINGLETON");
      }

      return cw.toString();
    }

    @Override
    protected void beforePrivateDataMacroDefinition(final CodeWriter cw,
        final String componentCName)
        throws ADLException {

      if (!(include instanceof MergedInclude)) {
        throw new ADLException(
            "The contentDesc argument was expected to be of type "
                + MergedInclude.class);
      }

      if (!(container instanceof MergeableComponent)) {
        throw new ADLException(
            "The contentDesc argument was expected to be of type "
                + MergeableComponent.class + " but was a " + container.getClass());
      }

      this.currentComponent = (MergeableComponent)container;

      /* a fused module is an InterfaceContainer, see cecilia.dtd */
      for (final Interface i : ((InterfaceContainer) include).getInterfaces()) {
        final MergedInterface mergedItf = (MergedInterface) i;
  
        switch (MergedBindingResolverLoader.getMergedInterfaceRole(mergedItf)) {
          case SERVER_OUTER :
            defineServerOuterInterface(cw, mergedItf);
            break;
          case SERVER_INNER :
            defineServerInnerInterface(cw, mergedItf);
            break;
          case CLIENT_OUTER :
            defineClientOuterInterface(cw, mergedItf);
            break;
          case CLIENT_INNER :
            defineClientInnerInterface(cw, mergedItf);
            break;
          default :
            throw new IllegalStateException("The merged interface '"
                + mergedItf.getName() + "' has not been decorated with a "
                + MergedInterfaceRole.class);
        }
      }
    }

    /*
     * (non-Javadoc)
     *
     * @see generatePrivateDataMacroDefinition(CodeWriter, String)
     */
    @Override
    protected void generatePrivateDataMacroDefinition(final CodeWriter cw,
        final String componentCName) throws ADLException {

      if (!(include instanceof MergedInclude)) {
        throw new ADLException("content parameter was expected to be of type "
            + MergedInclude.class + " but was a " + include.getClass());
      }

      /* cast to MergedInclude */
      final MergedInclude mergedInclude = (MergedInclude) include;

      final String thisFusedModuleInstanceDataType = MergeUtil.translateMergedIncludeInstanceDataType(mergedInclude);
      final String thisFusedModuleInstanceDataVariable = MergeUtil.translateMergedIncludeInstanceDataVariable(mergedInclude);

      cw.appendln("/* the PRIVATE_DATA for this module to be merged */");
      cw.appendln("#ifdef PRIVATE_DATA");
      cw.appendln("#undef PRIVATE_DATA");
      cw.appendln("#endif");
      cw.appendln("#define PRIVATE_DATA \\");
      cw.appendln(thisFusedModuleInstanceDataType + "; \\");

      cw.appendln(thisFusedModuleInstanceDataType + " "
          + thisFusedModuleInstanceDataVariable + " = {};");
    }

    @Override
    protected void afterPrivateDataMacroDefinition(final CodeWriter cw,
        final String componentCName)
        throws Exception {

      cw.appendln("/* -START- THIS macro redefinitions for this fused module */");

      cw.appendln("#ifdef THIS");
      cw.appendln("#undef THIS");
      cw.appendln("#endif");

      cw.appendln("#define THIS "
          + MergeUtil.translateMergedIncludeInstanceDataVariable((MergedInclude) include));
  
      cw.appendln("/* -END- THIS macro redefinitions for this fused module */")
          .endl();
    }

    @Override
    protected void appendImplementationCode(final CodeWriter cw) {
      cw.appendln(implementationModuleSourceCodeProviderItf.getSourceCode());
    }
  }
}
