/***
 * Cecilia ADL Compiler
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.primitive.optC.components;

import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationHelper.hasConstructor;
import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationHelper.hasDestructor;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the code for the declaration of the
 * constructor/destructor of components.
 */
public class ComponentConstructorDeclarationVisitor
    extends
      AbstractTaskFactoryUser implements ComponentVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link ComponentContainer} nodes and creates a task that writes the
   * source code containing the component data structure definitions.
   */
  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    if (hasConstructor(container) || hasDestructor(container))
      return createConstructorDeclarationTask(container);
    else
      return null;
  }

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  protected Component createConstructorDeclarationTask(
      final ComponentContainer container) throws TaskException {
    return taskFactoryItf.newPrimitiveTask(
        new ComponentConstructorDeclarationTask(hasConstructor(container),
            hasDestructor(container)), container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Generates the declarations necessary to call the constructor/destructor of
   * the component.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "component-constructor-declaration-provider", signature = SourceCodeProvider.class, record = "role:globalInit, id:%, type:declaration, kind:constructor", parameters = "componentNode"))
  public static class ComponentConstructorDeclarationTask
      extends
        AbstractDefinitionTask {

    protected final boolean hasConstructor;
    protected final boolean hasDestructor;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param hasConstructor <code>true</code> if the component has a
     *          constructor.
     * @param hasDestructor <code>true</code> if the component has a destructor.
     */
    public ComponentConstructorDeclarationTask(final boolean hasConstructor,
        final boolean hasDestructor) {
      this.hasConstructor = hasConstructor;
      this.hasDestructor = hasDestructor;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractInstantiationTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter();
      String thisIfAny = "";
      if (!System.getProperty(InterfaceDecorationUtil
              .CECILIA_ENABLE_THIS_OPTIMIZATION, "false").equals("true")) {
        thisIfAny = "void *_this";
      }
      if (hasConstructor) {
        cw.append("void ").append(typeNameProviderItf.getCTypeName()).append(
            "_constructor(" + thisIfAny + "); ").endl();
      }
      if (hasDestructor) {
        cw.append("void ").append(typeNameProviderItf.getCTypeName()).append(
            "_destructor(" + thisIfAny + "); ").endl();
      }

      return cw.toString();
    }
  }
}
