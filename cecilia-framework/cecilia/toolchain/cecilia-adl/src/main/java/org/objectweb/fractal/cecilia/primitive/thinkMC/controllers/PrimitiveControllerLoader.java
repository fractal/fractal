/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 * Contributors: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.primitive.thinkMC.controllers;

import static org.objectweb.fractal.adl.types.TypeInterfaceUtil.isClient;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerDecorationUtil.isDefaultBindingController;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerDecorationUtil.setDataSuffix;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerDecorationUtil.setDefaultBindingController;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerDecorationUtil.setVTable;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.AC;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.BC;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.BINDING_CONTROLLER_SIGNATURE;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.CI;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.COMPONENT_IDENTITY_SIGNATURE;
import static org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil.setNoStaticBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.attributes.Attributes;
import org.objectweb.fractal.adl.attributes.AttributesContainer;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.cecilia.adl.controllers.AbstractControllerChecker;
import org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface;
import org.objectweb.fractal.cecilia.adl.idl.ast.Field;
import org.objectweb.fractal.cecilia.adl.idl.ast.FieldContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.RecordDefinition;

/**
 * Adds controllers interface for <code>"primitive"</code> components.
 */
public class PrimitiveControllerLoader extends AbstractControllerChecker {

  // --------------------------------------------------------------------------
  // Implementation of abstract methods
  // --------------------------------------------------------------------------

  @Override
  protected void checkComponent(final ComponentContainer container)
      throws ADLException {
    if (container instanceof ImplementationContainer
        && (((ImplementationContainer) container).getImplementation() == null))
      throw new ADLException(GenericErrors.GENERIC_ERROR,
          "'primitive' controller can only be used for primitive components",
          container);
  }

  @Override
  protected List<Interface> getControllerInterfaces(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final List<Interface> controllerItfs = new ArrayList<Interface>();
    final Interface ciItf = getComponentIdentityInterface(container, context);
    if (ciItf != null) controllerItfs.add(ciItf);

    final Interface bcItf = getBindingControllerInterface(container, context);
    if (bcItf != null) controllerItfs.add(bcItf);

    final Interface acItf = getAttributeControllerInterface(container, context);
    if (acItf != null) controllerItfs.add(acItf);

    checkCustomLifeCycleControllerInterface(container, context);

    return controllerItfs;
  }

  // --------------------------------------------------------------------------
  // Utility methods
  // --------------------------------------------------------------------------

  protected void checkCustomLifeCycleControllerInterface(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {

    final List<TypeInterface> lcc = this.getServerInterfacesBySignature(
        ControllerInterface.LIFECYCLE_CONTROLLER_SIGNATURE,
        (InterfaceContainer) container);

    if (lcc.size() > 1) {
      throw new ADLException(GenericErrors.GENERIC_ERROR,
          "It is not allowed to specify TWO " + ControllerInterface.LCC
              + " interfaces");
    }

    /* if there is a user provided LCC... */
    if (lcc.size() == 1) {

      final TypeInterface lifeCycleControllerItf = lcc.get(0);

      // .. check that the name matches the canonical name
      if (!lifeCycleControllerItf.getName().equals(ControllerInterface.LCC)) {
        throw new ADLException(GenericErrors.GENERIC_ERROR,
            "Custom server interface of type "
                + ControllerInterface.LIFECYCLE_CONTROLLER_SIGNATURE
                + " must be called '" + ControllerInterface.LCC + "'",
            lifeCycleControllerItf);
      }
    }

  }

  protected Interface getComponentIdentityInterface(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final InterfaceContainer itfContainer = (InterfaceContainer) container;
    TypeInterface ciItf = getInterface(CI, itfContainer);
    if (ciItf == null) {
      ciItf = newServerInterfaceNode(CI, COMPONENT_IDENTITY_SIGNATURE);
      setVTable(ciItf, "__cecilia_default_ComponentMeths");
      setDataSuffix(ciItf, "CI_data");
      setItfCode(ciItf, context, "fractal.lib.CIdelegate");
    } else {
      checkServerInterface(ciItf);
      // remove the interface form the interface container, since it is re-added
      itfContainer.removeInterface(ciItf);
    }
    return ciItf;
  }

  protected Interface getBindingControllerInterface(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final InterfaceContainer itfContainer = (InterfaceContainer) container;
    TypeInterface bcItf = getInterface(BC, itfContainer);
    if (isComponentNeedBindingControllerInterfaces(itfContainer)) {
      if (bcItf == null) {
        bcItf = newServerInterfaceNode(BC, BINDING_CONTROLLER_SIGNATURE);
        setDataSuffix(bcItf, "BC_data");
        setVTable(bcItf, "__cecilia_default_BindingControllerMeths");
        setItfCode(bcItf, context, "fractal.lib.BCdelegate");

        // set DEFAULT_BC_DECORATION on bcItf
        setDefaultBindingController(bcItf, true);
      }
    }
    if (bcItf != null) {
      checkServerInterface(bcItf);

      /*
       * Deal with multiple times processing of the AST: the BC interface may
       * already have been created and its vtable set, if the BC is a default
       * one.
       */
      if (!isDefaultBindingController(bcItf)) {
        // if the component has a custom binding controller, do not use
        // statically initialized binding.
        for (final Interface i : itfContainer.getInterfaces()) {
          if (isClient(i)) setNoStaticBinding(i, true);
        }
      }

      // remove the interface form the interface container, since it is re-added
      itfContainer.removeInterface(bcItf);
    }
    return bcItf;
  }

  protected Interface getAttributeControllerInterface(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final InterfaceContainer itfContainer = (InterfaceContainer) container;
    TypeInterface acItf = getInterface(AC, itfContainer);

    if (container instanceof AttributesContainer
        && ((AttributesContainer) container).getAttributes() != null
        && acItf == null) {

      final Attributes attributes = ((AttributesContainer) container)
          .getAttributes();

      final RecordDefinition recordDefinition = (RecordDefinition) ((IDLDefinitionContainer) attributes)
          .getIDLDefinition();

      final boolean hasConstFields = isComponentNeedAttributeControllerConstAwareDelegate(recordDefinition);

      acItf = newServerInterfaceNode(AC,
          ControllerInterface.ATTRIBUTE_CONTROLLER_SIGNATURE);
      setDataSuffix(acItf, "AC_data");

      if (hasConstFields) {
        setItfCode(acItf, context, "fractal.lib.ACdelegate_constattributes");
        setVTable(acItf, "__cecilia_AttributeControllerConstAttributesMeths");
      } else {
        setItfCode(acItf, context, "fractal.lib.ACdelegate");
        setVTable(acItf, "__cecilia_default_AttributeControllerMeths");
      }
    } else if (acItf != null) {
      checkServerInterface(acItf);
      // remove the interface form the interface container, since it is re-added
      itfContainer.removeInterface(acItf);
    }
    return acItf;
  }

  /**
   * @param container the <tt>Interface</tt> container.
   * @return <tt>true</tt> if the component has at least one client interface,
   *         <tt>false</tt> otherwise.
   */
  protected boolean isComponentNeedBindingControllerInterfaces(
      final InterfaceContainer container) {
    for (final Interface itf : container.getInterfaces()) {
      if (isClient(itf)) return true;
    }
    return false;
  }

  /**
   * @param fields
   * @return <code>true</code> if at least one of the {@link Field}s has a
   *         {@link Field#CONST} qualifier, <code>false</code> otherwise.
   */
  protected boolean isComponentNeedAttributeControllerConstAwareDelegate(
      final RecordDefinition recordDefinition) {

    final Field[] fields = ((FieldContainer) recordDefinition).getFields();

    for (final Field f : fields) {
      if (f.getQualifier().equals(Field.CONST)) {
        return true;
      }
    }
    return false;
  }

}
