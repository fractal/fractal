/***
 * Cecilia ADL Compiler
 * Copyright (C) 2005-2008 STMicroelectronics
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 * Contributor: Lionel Debroux
 */

package org.objectweb.fractal.cecilia.adl.components.merge;

import org.objectweb.fractal.adl.attributes.AttributesContainer;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.cecilia.adl.directives.Include;

/**
 * A <strong>merged include</strong> represents a fused module (a plain old
 * Cecilia component) inside a <em>merged primitive</em> component, and it
 * specifies which interfaces was expecting to use before the fusion, in order
 * to be able to cope with macro translations, see the {@link MergedInterface}
 * documentation for example.
 *
 * @author Alessio Pace
 */
public interface MergedInclude extends AttributesContainer, Include,
    InterfaceContainer {

  // TODO how to identify an include?? Name / Id ?

  String getId();

  void setId(String id);
}
