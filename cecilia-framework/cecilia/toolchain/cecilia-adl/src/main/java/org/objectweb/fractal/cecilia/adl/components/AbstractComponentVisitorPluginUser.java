/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.adl.components;

import java.util.Map;

import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.cecilia.adl.plugin.AbstractPluginUser;
import org.objectweb.fractal.cecilia.adl.plugin.PluginException;

/**
 * Extention of {@link AbstractPluginUser} for component that uses plugins
 * providing the {@link ComponentVisitor} interface.
 */
public abstract class AbstractComponentVisitorPluginUser
    extends
      AbstractPluginUser {

  /** The name of the client visitor plugin(s). */
  public static final String CLIENT_VISITOR_ITF = "client-visitor";

  protected AbstractComponentVisitorPluginUser() {
    super(CLIENT_VISITOR_ITF);
  }

  protected ComponentVisitor getVisitorPlugin(
      final Object pluginDesc, final Map<Object, Object> context)
      throws PluginException {
    return getPlugin(pluginDesc, context, ComponentVisitor.class);
  }
}
