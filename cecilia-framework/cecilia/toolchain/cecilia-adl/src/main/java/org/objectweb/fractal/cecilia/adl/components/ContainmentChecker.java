/***
 * Cecilia
 * Copyright (C) 2006-2008 STMicroelectronics
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.components;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.ast.ASTFractalUtil;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.cecilia.adl.Checker;
import org.objectweb.fractal.cecilia.adl.directives.Include;

/**
 * A simple {@link Checker} that decorates each component in the AST with:
 * <ul>
 * <li>{@link ContainmentChecker#FULL_COMPONENT_NAME_DECORATION}</li>
 * <li>{@link ContainmentChecker#PARENT_COMPONENT_DECORATION}</li>
 * </ul>
 * to simplify the merging algorithm. <strong>NOTE: shared components not
 * handled.</strong>
 *
 * @author Alessio Pace
 */
public class ContainmentChecker implements Checker {

  /**
   * The decoration name for the full canonical name of a component, which is
   * assigned following the ADL names of the various enclosing components along
   * the path to it.
   */
  public static final String FULL_COMPONENT_NAME_DECORATION = "FULL_COMPONENT_NAME_DECORATION";

  /**
   * The decoration name to access the component enclosing a certain component.
   */
  public static final String PARENT_COMPONENT_DECORATION    = "PARENT_COMPONENT_DECORATION";

  public void check(final Definition definition,
      final Map<Object, Object> context) throws ADLException {
    checkComponent((ComponentContainer) definition, null);
  }

  /**
   * Preorder visit of the {@link ComponentContainer} nodes in the AST.
   *
   * @param currentComponent
   * @param parentComponent
   */
  protected void checkComponent(final ComponentContainer currentComponent,
      final ComponentContainer parentComponent) {

    final String currentComponentName = ASTFractalUtil.getFcName(
        currentComponent).replaceAll("-", "_");

    if (parentComponent != null) {
      setParentComponentDecoration((Node) currentComponent, parentComponent);
      final String parentComponentFullName = getFullComponentNameDecoration(parentComponent);
      setFullComponentNameDecoration(currentComponent, parentComponentFullName
          + "_" + currentComponentName);
    } else {
      setFullComponentNameDecoration(currentComponent, currentComponentName);
    }

    /* if it is a primitive */
    if (ASTFractalUtil.isPrimitive(currentComponent)) {
      for (final Include include : ASTFractalUtil.getIncludes(ASTFractalUtil
          .getFcContentDesc(currentComponent))) {

        /* decorate the current Include with the Component enclosing it */
        setParentComponentDecoration((Node) include, currentComponent);
      }
    } else {
      /* else if it is a composite, continue recursively */
      final ComponentContainer[] subComponents = ASTFractalUtil
          .getFcSubComponents(currentComponent);
      /* travel down recursively */
      for (final ComponentContainer subComp : subComponents) {
        checkComponent(subComp, currentComponent);
      }
    }

  }

  public static void setParentComponentDecoration(final Node node,
      final ComponentContainer parentComponent) {

    if (node == null) {
      throw new IllegalArgumentException(
          "Node element to decorate can't be null");
    }

    node.astSetDecoration(PARENT_COMPONENT_DECORATION, parentComponent);
  }

  public static ComponentContainer getParentComponentDecoration(
      final Node node) {

    if (node == null) {
      throw new IllegalArgumentException("Component argument can't be null");
    }

    return (ComponentContainer) node
        .astGetDecoration(PARENT_COMPONENT_DECORATION);
  }

  public static void setFullComponentNameDecoration(
      final ComponentContainer component, final String name) {

    ((Node) component).astSetDecoration(FULL_COMPONENT_NAME_DECORATION, name);
  }

  public static String getFullComponentNameDecoration(
      final ComponentContainer component) {

    if (component == null) {
      throw new IllegalArgumentException("Component argument can't be null");
    }

    return (String) ((Node) component)
        .astGetDecoration(FULL_COMPONENT_NAME_DECORATION);
  }

}
