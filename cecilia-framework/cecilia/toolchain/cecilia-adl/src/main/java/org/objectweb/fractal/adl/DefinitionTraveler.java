/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Ali Erdem Ozcan
 */

package org.objectweb.fractal.adl;

import static java.lang.System.arraycopy;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;

/**
 * This components voyages over the AST pointed by {@link Definition} and
 * invokes its {@link #componentVisitorItf} client interface for each component
 * node that is found during this voyage in deep.
 */
public class DefinitionTraveler extends AbstractTaskFactoryUser
    implements
      DefinitionVisitor,
      BindingController,
      TaskCompositionAttribute {

  // --------------------------------------------------------------------------
  // Client interfaces
  // --------------------------------------------------------------------------

  /** The name of the {@link #componentVisitorItf} client interface. */
  public static final String COMPONENT_VISITOR_ITF_NAME        = "component-visitor";
  /**
   * The client {@link ComponentVisitor} that is invoked for each visited
   * {@link ComponentContainer}. This client interface is invoked only one time
   * for each {@link ComponentContainer} node even if it is shared.
   */
  public ComponentVisitor    componentVisitorItf;

  /** The name of the {@link #sharedComponentVisitorItf} client interface. */
  public static final String SHARED_COMPONENT_VISITOR_ITF_NAME = "shared-component-visitor";
  /**
   * The client {@link ComponentVisitor} that is invoked when a shared component
   * is detected. This client interface is invoked only with
   * {@link ComponentContainer} nodes that have already been visited by
   * {@link #componentVisitorItf} with a different path. This client interface
   * is optional.
   */
  public ComponentVisitor    sharedComponentVisitorItf;

  // --------------------------------------------------------------------------
  // Attributes
  // --------------------------------------------------------------------------

  protected String           taskCompositionFileName           = null;

  // --------------------------------------------------------------------------
  // Implementation of the Compiler interface
  // --------------------------------------------------------------------------

  public Component visit(final List<Node> path, final Definition definition,
      final Map<Object, Object> context) throws ADLException, TaskException {
    final List<Component> taskComponents = new ArrayList<Component>();
    if (definition instanceof ComponentContainer) {
      // Fill the task components list for this definition
      visit(new ArrayList<Node>(),
          new IdentityHashMap<ComponentContainer, Boolean>(),
          (ComponentContainer) definition, context, taskComponents);
    }
    // Create the composite task by applying the composition file to the task
    // component list.
    if (taskCompositionFileName == null)
      throw new TaskException(
          "No task comosition file is specified as attribute.");
    return taskFactoryItf.newCompositeTask(taskComponents,
        taskCompositionFileName, null, definition);
  }

  // --------------------------------------------------------------------------
  // Utility methods
  // --------------------------------------------------------------------------

  protected void visit(final List<Node> path,
      final Map<ComponentContainer, Boolean> visitedNodes,
      final ComponentContainer container, final Map<Object, Object> context,
      final List<Component> taskComponents) throws ADLException, TaskException {
    path.add(container);
    for (final org.objectweb.fractal.adl.components.Component comp : container
        .getComponents()) {
      final Boolean prevValue = visitedNodes.put(comp, Boolean.TRUE);
      if (prevValue == null) {
        visit(path, visitedNodes, comp, context, taskComponents);
      } else if (sharedComponentVisitorItf != null) {
        // component is shared, invokes dedicated visitor.
        final Component task = sharedComponentVisitorItf.visit(path, container,
            context);
        if (task != null) taskComponents.add(task);

      }
    }
    path.remove(path.size() - 1);

    final Component task = componentVisitorItf.visit(path, container, context);
    if (task != null) taskComponents.add(task);
  }

  // --------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // --------------------------------------------------------------------------

  @Override
  public String[] listFc() {
    final String[] superItfs = super.listFc();
    final String[] itfs = new String[superItfs.length + 2];
    arraycopy(superItfs, 0, itfs, 0, superItfs.length);
    itfs[superItfs.length] = COMPONENT_VISITOR_ITF_NAME;
    itfs[superItfs.length + 1] = SHARED_COMPONENT_VISITOR_ITF_NAME;
    return itfs;
  }

  @Override
  public void bindFc(final String clientItfName, final Object serverItf)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.equals(COMPONENT_VISITOR_ITF_NAME)) {
      componentVisitorItf = (ComponentVisitor) serverItf;
    } else if (clientItfName.equals(SHARED_COMPONENT_VISITOR_ITF_NAME)) {
      sharedComponentVisitorItf = (ComponentVisitor) serverItf;
    } else {
      super.bindFc(clientItfName, serverItf);
    }
  }

  @Override
  public Object lookupFc(final String clientItfName)
      throws NoSuchInterfaceException {
    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.equals(COMPONENT_VISITOR_ITF_NAME)) {
      return componentVisitorItf;
    } else if (clientItfName.equals(SHARED_COMPONENT_VISITOR_ITF_NAME)) {
      return sharedComponentVisitorItf;
    } else {
      return super.lookupFc(clientItfName);
    }
  }

  @Override
  public void unbindFc(final String clientItfName)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.equals(COMPONENT_VISITOR_ITF_NAME)) {
      componentVisitorItf = null;
    } else if (clientItfName.equals(SHARED_COMPONENT_VISITOR_ITF_NAME)) {
      sharedComponentVisitorItf = null;
    } else {
      super.unbindFc(clientItfName);
    }
  }

  // --------------------------------------------------------------------------
  // Implementation of the Attribute Controller interface
  // --------------------------------------------------------------------------

  public String getTaskCompositionFileName() {
    return taskCompositionFileName;
  }

  public void setTaskCompositionFileName(final String fileName) {
    taskCompositionFileName = fileName;
  }
}
