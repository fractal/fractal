/***
 * Cecilia ADL Compiler
 * Copyright (C) 2005-2008 STMicroelectronics
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.components.merge;

import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.ast.ASTFractalUtil;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.adl.util.Printer;
import org.objectweb.fractal.cecilia.adl.bindings.MergedBindingResolverLoader;
import org.objectweb.fractal.cecilia.adl.components.ContainmentChecker;
import org.objectweb.fractal.cecilia.adl.components.MergeComponentsLoader;
import org.objectweb.fractal.cecilia.adl.directives.Include;
import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;
import org.objectweb.fractal.cecilia.adl.types.TypeDecorationUtil;

/**
 * Utility methods for <em>merged primitives</em> components.
 *
 * @author Alessio Pace
 */
public final class MergeUtil {

  /**
   * Name of the sytem property checked by MergeComponentsLoader.
   * If it is "true", MergeComponentsLoader won't try merging components.
   */
  public static final String CECILIA_DISABLE_MERGE_OPTIMIZATION = "CECILIA_DISABLE_MERGE_OPTIMIZATION";

  private MergeUtil() {
    // prevent instantiation.
  }

  /**
   * Return <code>true</code> if the component is a primitive and it's
   * implementation is provided by the template
   * {@link MergeComponentsLoader#FRACTAL_LIB_MERGED_PRIMITIVE} merged primitive
   * class, <code>false</code> otherwise.
   *
   * @param subComp
   * @return <code>true</code> if the component is a primitive and it's
   *         implementation is provided by the template
   *         {@link MergeComponentsLoader#FRACTAL_LIB_MERGED_PRIMITIVE} merged
   *         primitive class, <code>false</code> otherwise.
   */
  public static boolean isMergedPrimitive(final ComponentContainer subComp) {

    if (subComp == null) {
      throw new IllegalArgumentException("Sub component is null");
    }

    final Implementation implementation = ASTFractalUtil
        .getFcContentDesc(subComp);

    if (implementation != null
        && implementation.getClassName().equals(
            MergeComponentsLoader.FRACTAL_LIB_MERGED_PRIMITIVE)) {
      return true;
    }
    return false;
  }

  /**
   * Return the {@link MergedInclude} with the given identifier, or
   * <code>null</code> if no one matches.
   *
   * @param mergedPrimitive
   * @param includeId
   * @return the {@link MergedInclude} with the given identifier, or
   *         <code>null</code> if no one matches.
   */
  public static MergedInclude getIncludeById(
      final MergedImplementation mergedPrimitive, final String includeId) {
    if (mergedPrimitive == null) {
      throw new IllegalArgumentException(
          "Merged primitive Implementation can't be null");
    }
    if (includeId == null) {
      throw new IllegalArgumentException("Include identifier can't be null");
    }

    for (final Include i : ASTFractalUtil.getIncludes(mergedPrimitive)) {
      if (!(i instanceof MergedInclude)) {
        throw new IllegalArgumentException("The Include should implement "
            + MergedInclude.class);
      }
      final MergedInclude mergedInclude = ((MergedInclude) i);
      if (mergedInclude.getId().equals(includeId)) {
        return mergedInclude;
      }
    }

    return null;
  }

  /**
   * return <code>true</code> if the given {@link MergedInterface} (being it
   * client or server) has to be compiled as static, <code>false</code>
   * otherwise.
   *
   * @param mergedItf
   * @return true if the given {@link MergedInterface} (being it client or
   *         server) has to be compiled as static.
   */
  public static boolean hasToBeCompiledAsStatic(final MergedInterface mergedItf) {

    if (System.getProperty(InterfaceDecorationUtil.CECILIA_ENABLE_THIS_OPTIMIZATION,
        "false").equals("true")) {

      /* the Include which owns the interface argument */
      final MergedInclude include = (MergedInclude) InterfaceDecorationUtil
          .getContainer(mergedItf);

      /* the (merged) primitive component containing the Include */
      final ComponentContainer mergedPrimitive = ContainmentChecker
          .getParentComponentDecoration((Node) include);

      final ComponentContainer mergedPrimitiveComponentType = TypeDecorationUtil
          .getTypeDecoration(mergedPrimitive);

      /*
       * if the enclosing merged primitive is not a singleton, there will be no
       * inner static interfaces
       */
      if (!TypeDecorationUtil
          .isSingletonComponentType(mergedPrimitiveComponentType)) {
        Printer
            .info("Return not static because the enclosing merged primitive '"
                + ContainmentChecker
                    .getFullComponentNameDecoration(mergedPrimitive)
                + "' whose type is ["
                + ContainmentChecker
                    .getFullComponentNameDecoration(mergedPrimitiveComponentType)
                + "] is NOT a singleton");
        return false;
      } else {
        /*
         * else verify the conditions depending on the merged role of the
         * interface
         */
        switch (MergedBindingResolverLoader.getMergedInterfaceRole(mergedItf)) {
          case SERVER_INNER :
            return handleServerInner(mergedItf);
          case CLIENT_INNER :
            return handleClientInner(mergedItf);
          case SERVER_OUTER :
            return handleServerOuter(mergedItf, mergedPrimitive);
          case CLIENT_OUTER :
            return handleClientOuter(mergedItf);
          default :
            throw new IllegalStateException("The merged interface '"
                + mergedItf.getName() + "' has not been decorated with a "
                + MergedInterfaceRole.class);
        }
      }
    }

    return false;

  }

  /**
   * Return <code>true</code>.
   *
   * @param mergedItf
   * @return true (always)
   */
  private static boolean handleServerInner(final MergedInterface mergedItf) {
    return true;
  }

  /**
   * Delegate the result of the call
   * {@link MergeUtil#hasToBeCompiledAsStatic(MergedInterface)} on the bound to
   * interface.
   *
   * @param mergedItf
   * @return the result of the call
   *         {@link MergeUtil#hasToBeCompiledAsStatic(MergedInterface)} on the
   *         bound to interface.
   */
  private static boolean handleClientInner(final MergedInterface mergedItf) {

    final MergedInterface itf = (MergedInterface) MergedBindingResolverLoader
        .getInnerBoundTo(mergedItf);

    return hasToBeCompiledAsStatic(itf);

  }

  /**
   * @param mergedItf
   * @param mergedPrimitive
   * @return
   */
  private static boolean handleServerOuter(final MergedInterface mergedItf,
      final ComponentContainer mergedPrimitive) {

    for (final TypeInterface outerItf : ASTFractalUtil
        .getFcFunctionalServerInterfaces(mergedPrimitive)) {
      if (MergedBindingResolverLoader.getInnerBoundTo(outerItf) == mergedItf) {
        return InterfaceDecorationUtil.hasToBeCompiledAsStatic(outerItf);

      }
    }

    throw new IllegalStateException(
        "Unexpected condition: no server interface on the enclosing merged primitive is inner bound to the given merged itf passed as parameter");
  }

  /**
   * @param mergedItf
   * @return
   */
  private static boolean handleClientOuter(final MergedInterface mergedItf) {
    final TypeInterface outerItf = MergedBindingResolverLoader
        .getInnerBoundTo(mergedItf);
    return InterfaceDecorationUtil.hasToBeCompiledAsStatic(outerItf);
  }

  /**
   * Translate a server inner merged interface name, assuming that the
   * {@link MergedInterface} has been decorated with a
   * {@link InterfaceDecorationUtil#CONTAINER_DECORATION} with the
   * {@link MergedInclude} actually providing it.
   *
   * @param mergedItf
   * @return
   */
  public static String translateServerInnerItfName(
      final MergedInterface mergedItf) {

    final MergedInclude mergedInclude = (MergedInclude) InterfaceDecorationUtil
        .getContainer(mergedItf);

    return mergedInclude.getId() + "__" + mergedItf.getName().replace('-', '_');
  }
  
  /**
   * @param mergedInclude
   * @return
   */
  public static String translateMergedIncludeInstanceDataVariable(
      final MergedInclude mergedInclude) {
    return "___thisData_" + mergedInclude.getId();
  }

  /**
   * @return the type name of the instance data structure for the fused module.
   */
  public static String translateMergedIncludeInstanceDataType(
      final MergedInclude mergedInclude) {
    return "APPEND(COMPONENT_NAME," + mergedInclude.getId() + ")";
  }
}
