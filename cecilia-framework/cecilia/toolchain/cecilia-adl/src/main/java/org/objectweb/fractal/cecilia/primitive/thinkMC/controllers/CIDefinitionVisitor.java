/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.primitive.thinkMC.controllers;

import java.util.List;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.interfaces.AbstractServerInterfaceVisitor;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the definition of the component identity
 * controller data structure of the component for <code>ThinkMC</code>
 * dialect.
 */
public class CIDefinitionVisitor extends AbstractServerInterfaceVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractServerInterfaceVisitor
  // ---------------------------------------------------------------------------

  @Override
  protected Component createTask(final ComponentContainer container,
      final List<TypeInterface> serverInterfaces) throws ADLException,
      TaskException {
    return taskFactoryItf.newPrimitiveTask(new CIDefinitionTask(
        serverInterfaces.size()), container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds source code for the definition of the component identity controller
   * data structure for the given component node. This task provides the
   * produced source code.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "ci-definition-provider", signature = SourceCodeProvider.class, record = "role:controllerDefinition, id:%, controller:CI", parameters = "componentNode"))
  public static class CIDefinitionTask extends AbstractDefinitionTask {

    // The length of the server interfaces array
    final int serverInterfacesLength;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param serverInterfacesLength the number of server interfaces.
     */
    public CIDefinitionTask(final int serverInterfacesLength) {
      this.serverInterfacesLength = serverInterfacesLength;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter("Primitive Component Identity Definition");

      cw.append("struct ").append(typeNameProviderItf.getCTypeName()).append(
          "_CI_data_type").append("{").endl();
      cw.append("  unsigned int nbexported;").endl();
      cw.append("  struct {").endl();
      cw.append("    const char *name;").endl();
      cw.append("    void *itf;").endl();
      cw.append("  } exported[").append(serverInterfacesLength).append("];")
          .endl();
      cw.append("} ;").endl();

      return cw.toString();
    }
  }
}
