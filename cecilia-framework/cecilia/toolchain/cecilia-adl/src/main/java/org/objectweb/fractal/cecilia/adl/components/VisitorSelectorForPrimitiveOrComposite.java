/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 * Contributor: Alessio Pace, Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.adl.components;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.task.core.TaskException;

/**
 * This visitor component has two client interfaces, the first one is used to
 * delegate the visit operation for primitive components, the other one is used
 * to delegate the visit operation for composite components.
 */
public class VisitorSelectorForPrimitiveOrComposite
    implements
      ComponentVisitor,
      BindingController {

  // --------------------------------------------------------------------------
  // Client interfaces
  // --------------------------------------------------------------------------

  /**
   * The name of the {@link ComponentVisitor} client interface used for
   * primitive components.
   */
  public static final String PRIMITIVE_COMPONENT_ITF = "primitive";

  /**
   * The name of the {@link ComponentVisitor} client interface used for
   * composite components.
   */
  public static final String COMPOSITE_COMPONENT_ITF = "composite";

  /**
   * The {@link ComponentVisitor} client interface used for primitive component.
   */
  protected ComponentVisitor primitiveComponentItf;

  /**
   * The {@link ComponentVisitor} client interface used for composite component.
   */
  protected ComponentVisitor compositeComponentItf;

  // --------------------------------------------------------------------------
  // Implementation of the Visitor interface
  // --------------------------------------------------------------------------
  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    ComponentVisitor visitor = compositeComponentItf;
    if (container instanceof ImplementationContainer) {
      final Implementation impl = ((ImplementationContainer) container)
          .getImplementation();
      if (impl != null) {
        visitor = primitiveComponentItf;
      }
    }
    return visitor.visit(path, container, context);
  }

  // --------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // --------------------------------------------------------------------------

  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(PRIMITIVE_COMPONENT_ITF)) {
      this.primitiveComponentItf = (ComponentVisitor) o;
    } else if (s.equals(COMPOSITE_COMPONENT_ITF)) {
      this.compositeComponentItf = (ComponentVisitor) o;
    } else {
      throw new NoSuchInterfaceException("There is no interface named '" + s
          + "'");
    }
  }

  public String[] listFc() {
    return new String[]{PRIMITIVE_COMPONENT_ITF, COMPOSITE_COMPONENT_ITF};
  }

  public Object lookupFc(final String s) throws NoSuchInterfaceException {
    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(PRIMITIVE_COMPONENT_ITF)) {
      return this.primitiveComponentItf;
    } else if (s.equals(COMPOSITE_COMPONENT_ITF)) {
      return this.compositeComponentItf;
    } else {
      throw new NoSuchInterfaceException("There is no interface named '" + s
          + "'");
    }
  }

  public void unbindFc(final String s) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(PRIMITIVE_COMPONENT_ITF)) {
      this.primitiveComponentItf = null;
    } else if (s.equals(COMPOSITE_COMPONENT_ITF)) {
      this.compositeComponentItf = null;
    } else {
      throw new NoSuchInterfaceException("There is no interface named '" + s
          + "'");
    }

  }
}
