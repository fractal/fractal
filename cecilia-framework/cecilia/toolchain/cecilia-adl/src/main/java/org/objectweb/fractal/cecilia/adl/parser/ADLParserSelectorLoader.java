/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.parser;

import java.util.Map;

import org.objectweb.fractal.adl.ADLErrors;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.cecilia.adl.AbstractParserSelector;

/**
 * A component that choose the ADL Parser to be used for the given definition
 * name based on the file name extension. This component has a client collection
 * interface bound to a set of ADL parser, each one for a given extension.
 */
public class ADLParserSelectorLoader extends AbstractParserSelector<Loader>
    implements
      Loader,
      BindingController {

  /**
   * The name of the client collection interface bound to the available parser.
   * The name of each client interface must have the form
   * <code>"parser-<i>&lt;extension&gt;</i>"</code> where
   * <code><i>&lt;extension&gt;</i></code> is the file name extension used by
   * the parser it is bound to.
   */
  public static final String PARSERS = "parser-";

  // --------------------------------------------------------------------------
  // Implementation of the Loader interface
  // --------------------------------------------------------------------------

  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    try {
      return findParser(name, context).load(name, context);
    } catch (final ParserNotFoundException e) {
      throw new ADLException(ADLErrors.ADL_NOT_FOUND, e, name);
    }
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  public String[] listFc() {
    final String[] s = getAvailableExtensions();
    for (int i = 0; i < s.length; i++) {
      s[i] = PARSERS + s[i];
    }
    return s;
  }

  public Object lookupFc(final String s) throws NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.startsWith(PARSERS)) {
      return getParser(s.substring(PARSERS.length()));
    } else {
      throw new NoSuchInterfaceException("There is no interface named '" + s
          + "'");
    }
  }

  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.startsWith(PARSERS)) {
      addParser(s.substring(PARSERS.length()), (Loader) o);
    } else {
      throw new NoSuchInterfaceException("There is no interface named '" + s
          + "'");
    }
  }

  public void unbindFc(final String s) throws NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.startsWith(PARSERS)) {
      removeParser(s.substring(PARSERS.length()));
    } else {
      throw new NoSuchInterfaceException("There is no interface named '" + s
          + "'");
    }
  }

}
