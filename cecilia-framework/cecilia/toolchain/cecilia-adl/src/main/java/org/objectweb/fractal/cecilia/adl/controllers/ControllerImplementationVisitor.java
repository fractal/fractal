/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.controllers;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.DefinitionVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.compiler.CompilationTaskFactory;
import org.objectweb.fractal.cecilia.adl.file.FutureFileCollectionProvider;
import org.objectweb.fractal.cecilia.adl.file.FutureFileProvider;
import org.objectweb.fractal.cecilia.adl.file.SourceFile;
import org.objectweb.fractal.cecilia.adl.file.SourceFileProvider;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.implementations.ImplementationDecorationUtil;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.Executable;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterfaceForEach;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

public class ControllerImplementationVisitor extends AbstractTaskFactoryUser
    implements
      DefinitionVisitor {

  public static final String    COMPOSTION                        = "org.objectweb.fractal.cecilia.adl.controllers.ControllerCompilation";

  // ---------------------------------------------------------------------------
  // Client interface
  // ---------------------------------------------------------------------------

  /** The name of the {@link #compilationTaskFactoryItf} client interface. */
  public static final String    COMPILATION_TASK_FACTORY_ITF_NAME = "compilation-task-factory";

  /** The client interface used to create compilation tasks. */
  public CompilationTaskFactory compilationTaskFactoryItf;

  // ---------------------------------------------------------------------------
  // Implementation of the DefinitionVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits a {@link ComponentContainer} node and creates a task that creates
   * the source file containing the components' definition and instance code.
   */
  public Component visit(final List<Node> path, final Definition container,
      final Map<Object, Object> context) throws ADLException, TaskException {
    final HashMap<SourceFile, Set<IDLDefinition>> controllerImplementation = new HashMap<SourceFile, Set<IDLDefinition>>();
    findControllers(container, controllerImplementation, context);

    final Collection<Component> tasks = new ArrayList<Component>();
    for (final Map.Entry<SourceFile, Set<IDLDefinition>> entry : controllerImplementation
        .entrySet()) {
      final SourceFile code = entry.getKey();
      final String signature = code.getSignature();

      tasks.add(createControllerImplementationFileProviderTask(entry, code,
          signature));
      tasks.add(createControllerImplementationCompilationTask(context,
          signature));
    }
    tasks.add(taskFactoryItf.newPrimitiveTask(
        new CompiledControllerFileAggregator(), container));
    return taskFactoryItf.newCompositeTask(tasks, COMPOSTION, null, container);
  }

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  protected Component createControllerImplementationCompilationTask(
      final Map<Object, Object> context, final String signature)
      throws TaskException {
    return compilationTaskFactoryItf.newCompileTask(signature, null, context);
  }

  protected Component createControllerImplementationFileProviderTask(
      final Map.Entry<SourceFile, Set<IDLDefinition>> entry,
      final SourceFile code, final String signature) throws TaskException {
    return taskFactoryItf.newPrimitiveTask(
        new ControllerImplementationFileProvider(code), signature, entry
            .getValue());
  }

  protected void findControllers(final Object container,
      final Map<SourceFile, Set<IDLDefinition>> controllerImplementation,
      final Map<Object, Object> context) throws TaskException {
    if (container instanceof InterfaceContainer) {
      for (final Interface itf : ((InterfaceContainer) container)
          .getInterfaces()) {
        final Collection<?> codes = (Collection<?>) ImplementationDecorationUtil
            .getCode(itf);
        if (codes == null) continue;
        for (final Object c : codes) {
          final SourceFile code = (SourceFile) c;

          Set<IDLDefinition> controllerItf = controllerImplementation.get(code);
          if (controllerItf == null) {
            controllerItf = new HashSet<IDLDefinition>();
            controllerItf.add(castNodeError(itf, IDLDefinitionContainer.class)
                .getIDLDefinition());
            controllerImplementation.put(code, controllerItf);
          }
          controllerItf.add(((IDLDefinitionContainer) itf).getIDLDefinition());
        }
      }
    }

    if (container instanceof ComponentContainer) {
      for (final org.objectweb.fractal.adl.components.Component component : ((ComponentContainer) container)
          .getComponents()) {
        findControllers(component, controllerImplementation, context);
      }
    }
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * A {@link SourceFileProvider} task that provides controller implementation
   * source file.
   */
  @TaskParameters({"signature", "implementedInterfaces"})
  @ServerInterfaces(@ServerInterface(name = "controller-implementation-file-provider", signature = SourceFileProvider.class, record = "role:controllerImplementationFile, id:%", parameters = "signature"))
  public static class ControllerImplementationFileProvider
      implements
        Executable,
        SourceFileProvider {

    protected final SourceFile                          providedFile;

    // -------------------------------------------------------------------------
    // Client interfaces
    // -------------------------------------------------------------------------

    @ClientInterfaceForEach(iterable = "implementedInterfaces", prefix = "idl-header", signature = SourceCodeProvider.class, record = "role:cInterfaceDefinition, id:%", parameters = "implementedInterfaces.element")
    public final Map<TypeInterface, SourceCodeProvider> cInterfaceDefinitionItfs = new HashMap<TypeInterface, SourceCodeProvider>();

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param providedFile the file provided by this task.
     */
    public ControllerImplementationFileProvider(final SourceFile providedFile) {
      this.providedFile = providedFile;
    }

    // -------------------------------------------------------------------------
    // Implementation of the Executable interface
    // -------------------------------------------------------------------------

    public void execute() throws Exception {
      // simply get the interface definitions to ensure that they are
      // generated before the controller implementation is compiled.
      for (final SourceCodeProvider itfDef : cInterfaceDefinitionItfs.values()) {
        itfDef.getSourceCode();
      }
    }

    // -------------------------------------------------------------------------
    // Implementation of the SourceFileProvider interface
    // -------------------------------------------------------------------------

    public SourceFile getSourceFile() {
      return providedFile;
    }
  }

  @TaskParameters("definitionNode")
  @ServerInterfaces(@ServerInterface(name = "controller-implementation-files", signature = FutureFileCollectionProvider.class, record = "role:controllerImplementations, id:%", parameters = "definitionNode"))
  public static class CompiledControllerFileAggregator
      implements
        FutureFileCollectionProvider {

    protected Collection<Future<File>>           providedFiles;

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /**
     * Client collection interface used to retrieve files provided by this task.
     */
    @ClientInterface(name = "file-providers", signature = FutureFileProvider.class, record = "role:compiledFile, id:%", parameters = "definitionNode")
    public final Map<String, FutureFileProvider> clientProviderItfs = new HashMap<String, FutureFileProvider>();

    // -------------------------------------------------------------------------
    // Implementation of the FutureFileCollectionProvider interface
    // -------------------------------------------------------------------------

    public synchronized Collection<Future<File>> getFiles(
        final ExecutorService executorService) {
      if (providedFiles == null) {
        providedFiles = new HashSet<Future<File>>();
        for (final FutureFileProvider fileProvider : clientProviderItfs
            .values()) {
          providedFiles.add(fileProvider.getFile(executorService));
        }
      }
      return providedFiles;
    }
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  @Override
  public String[] listFc() {
    final String[] superItfs = super.listFc();
    final String[] itfs = new String[superItfs.length + 1];
    System.arraycopy(superItfs, 0, itfs, 0, superItfs.length);
    itfs[superItfs.length] = COMPILATION_TASK_FACTORY_ITF_NAME;
    return itfs;
  }

  @Override
  public void bindFc(final String clientItfName, final Object serverItf)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.equals(COMPILATION_TASK_FACTORY_ITF_NAME))
      compilationTaskFactoryItf = (CompilationTaskFactory) serverItf;
    else
      super.bindFc(clientItfName, serverItf);
  }

  @Override
  public Object lookupFc(final String clientItfName)
      throws NoSuchInterfaceException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.equals(COMPILATION_TASK_FACTORY_ITF_NAME))
      return compilationTaskFactoryItf;
    else
      return super.lookupFc(clientItfName);
  }

  @Override
  public void unbindFc(final String clientItfName)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.equals(COMPILATION_TASK_FACTORY_ITF_NAME))
      compilationTaskFactoryItf = null;
    else
      super.unbindFc(clientItfName);
  }
}
