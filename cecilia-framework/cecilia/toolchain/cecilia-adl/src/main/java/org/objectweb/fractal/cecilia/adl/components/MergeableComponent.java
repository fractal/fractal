/***
 * Cecilia
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.components;

import org.objectweb.fractal.adl.components.ComponentContainer;

/**
 * This AST node is meant to be able to specify a
 * <code>merge="true|false"</code> for components (either {@link Definition}s
 * or inline {@link Component}s nodes).
 * 
 * @author Alessio Pace
 */
public interface MergeableComponent extends ComponentContainer {

  /**
   * @return <code>true</code> if the component has to be merged,
   *         <code>false</code> if the component should NOT be merged,
   *         <code>null</code> if the choice will depend on the beahviour of
   *         the parent or sibling components.
   */
  String getMerge();

  /**
   * @param merge the value of the <code>merge</code> attribute.
   */
  void setMerge(String merge);
}
