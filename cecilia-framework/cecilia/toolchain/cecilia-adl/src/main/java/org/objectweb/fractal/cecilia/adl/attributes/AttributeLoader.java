/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 *
 * Contributor: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.attributes;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.attributes.Attribute;
import org.objectweb.fractal.adl.attributes.AttributeErrors;
import org.objectweb.fractal.adl.attributes.Attributes;
import org.objectweb.fractal.adl.attributes.AttributesContainer;
import org.objectweb.fractal.adl.error.ChainedErrorLocator;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.merger.MergeException;
import org.objectweb.fractal.adl.merger.NodeMerger;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.cecilia.adl.CeciliaADLConstants;
import org.objectweb.fractal.cecilia.adl.idl.ast.Field;
import org.objectweb.fractal.cecilia.adl.idl.ast.FieldContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.PrimitiveType;
import org.objectweb.fractal.cecilia.adl.idl.ast.RecordDefinition;
import org.xml.sax.SAXException;

/**
 * A {@link org.objectweb.fractal.adl.Loader} to check {@link Attributes} nodes
 * in definitions. This loader checks that the Java attribute controller
 * interfaces specified in these nodes exist, and that the attribute names and
 * values are consistent with the methods of theses interfaces.
 */
public class AttributeLoader
    extends
      org.objectweb.fractal.adl.attributes.AttributeLoader {

  /**
   * The URL of the DTD that defines AST node interfaces for bridging ADL and
   * IDL trees.
   */
  public static final String ADL_IDL_BRIGDE_DTD = "classpath://org/objectweb/fractal/cecilia/adl/idl/adl-idl-bridge.dtd";

  // ---------------------------------------------------------------------------
  // Client interfaces
  // ---------------------------------------------------------------------------

  /** The {@link NodeMerger} client interface used by this component. */
  public NodeMerger          nodeMergerItf;

  /** The {@link XMLNodeFactory} client interface used by this component. */
  protected XMLNodeFactory   nodeFactoryItf;

  // ---------------------------------------------------------------------------
  // Overridden methods
  // ---------------------------------------------------------------------------

  @Override
  protected void checkAttributesContainer(final AttributesContainer container,
      final Map<Object, Object> context) throws ADLException {
    Attributes attrs = container.getAttributes();
    if (attrs != null) {
      final String signature = attrs.getSignature();
      if (signature == null) {
        throw new ADLException(AttributeErrors.SIGNATURE_MISSING, attrs);
      }
      if (!(attrs instanceof IDLDefinitionContainer)) {
        // the attrs node is not an IDLDefinitionContainer
        // - create a bridge node;
        // - merge it with the attrs node.
        try {
          Node bridge = nodeFactoryItf.newXMLNode(ADL_IDL_BRIGDE_DTD, attrs
              .astGetType());
          bridge = nodeMergerItf.merge(attrs, bridge,
              new HashMap<String, String>());
          if (bridge != attrs) {
            // replace attrs node
            attrs = (Attributes) bridge;
            container.setAttributes(attrs);
          }
        } catch (final SAXException e) {
          throw new CompilerError(GenericErrors.INTERNAL_ERROR, e,
              "Unable to instantiate 'attributes' node");
        } catch (final MergeException e) {
          throw new CompilerError(GenericErrors.INTERNAL_ERROR, e,
              "Unable to merge 'attributes' node");
        }
      }

      final Attribute[] assignedAttributes = attrs.getAttributes();
      for (final Attribute attribute : assignedAttributes) {
        if (attribute.getName() == null) {
          throw new ADLException(AttributeErrors.NAME_MISSING, attribute);
        }
        if (attribute.getValue() == null) {
          throw new ADLException(AttributeErrors.VALUE_MISSING, attribute);
        }
      }

      IDLDefinition recordDefinition = ((IDLDefinitionContainer) attrs)
          .getIDLDefinition();
      if (recordDefinition == null) {
        try {
          recordDefinition = (IDLDefinition) interfaceLoaderItf.loadInterface(
              signature, context);
        } catch (final ADLException e) {
          ChainedErrorLocator.chainLocator(e, attrs);
        }
        if (!(recordDefinition instanceof RecordDefinition)) {
          throw new ADLException(
              org.objectweb.fractal.cecilia.adl.attributes.AttributeErrors.INVALID_SIGNATURE,
              container, signature);
        }
        // Add the record definition in the container
        ((IDLDefinitionContainer) attrs).setIDLDefinition(recordDefinition);
      }
      final Field[] recordFields = ((FieldContainer) recordDefinition)
          .getFields();

      final Attribute[] declaredAttributes = new Attribute[recordFields.length];
      // The AST node will be replaced by the set of declared attribute to add
      // the non-assigned attributes respecting the required order

      // Check if there are assigned attribute names which does not exist in
      // the record
      for (final Attribute ass : assignedAttributes) {
        final String assName = ass.getName();
        boolean found = false;
        for (final Field dec : recordFields) {
          if (dec.getName().equals(assName)) {
            found = true;
            break;
          }
        }
        if (!found)
          throw new ADLException(AttributeErrors.NO_SUCH_ATTRIBUTE, ass,
              assName);
      }

      for (int i = 0; i < declaredAttributes.length; i++) {
        // Merge the assigned attribute values into the declared one
        final Field field = recordFields[i];
        final Attribute attrNode;
        Attribute assignedAttribute = null;
        for (final Attribute attr : assignedAttributes) {
          if (field.getName().equals(attr.getName())) {
            assignedAttribute = attr;
            break;
          }
        }

        if (assignedAttribute == null) {
          // Create new node.
          try {
            attrNode = castNodeError(nodeFactoryItf.newXMLNode(
                CeciliaADLConstants.CECILIA_ADL_DTD,
                CeciliaADLConstants.ATTRIBUTE_AST_NODE_NAME), Attribute.class);
          } catch (final SAXException e) {
            throw new CompilerError(GenericErrors.INTERNAL_ERROR, e, e
                .getMessage());
          }
          attrNode.setName(field.getName());
          attrNode.setValue(getDefaultValue(field));
        } else {
          attrNode = assignedAttribute;
          final PrimitiveType pt = field.getPrimitiveType();
          if (pt != null) {
            final String typeName = pt.getName();
            if (typeName.equals(PrimitiveType.PrimitiveTypeEnum.STRING.name())) {
              // If the attribute is a string and it doesn't have
              // surrounding quotes, add them.
              final String attrValue = attrNode.getValue();
              if (!(attrValue.startsWith("\"") && attrValue.endsWith("\""))) {
                attrNode.setValue("\"" + attrValue + "\"");
              }
            } else if (typeName.equals(PrimitiveType.PrimitiveTypeEnum.BOOLEAN
                .name())) {
              // If the attribute is a boolean, turn it into '1' or '0'.
              final String attrValue = attrNode.getValue();
              if (Boolean.parseBoolean(attrValue)) {
                attrNode.setValue("1");
              } else {
                attrNode.setValue("0");
              }
            }
          }
        }

        declaredAttributes[i] = attrNode;
      }

      // remove current attribute nodes and replace them by new ones.
      for (final Attribute attr : assignedAttributes)
        attrs.removeAttribute(attr);

      for (final Attribute attr : declaredAttributes)
        attrs.addAttribute(attr);
    }
  }

  protected String getDefaultValue(final Field field) {
    return "0";
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  @Override
  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(NodeMerger.ITF_NAME)) {
      nodeMergerItf = (NodeMerger) o;
    } else if (s.equals(XMLNodeFactory.ITF_NAME)) {
      nodeFactoryItf = (XMLNodeFactory) o;
    } else {
      super.bindFc(s, o);
    }
  }

  @Override
  public String[] listFc() {
    final String[] superList = super.listFc();
    final String[] list = new String[superList.length + 2];
    list[0] = NodeMerger.ITF_NAME;
    list[1] = XMLNodeFactory.ITF_NAME;
    System.arraycopy(superList, 0, list, 2, superList.length);
    return list;
  }

  @Override
  public Object lookupFc(final String s) throws NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(NodeMerger.ITF_NAME)) {
      return nodeMergerItf;
    } else if (s.equals(XMLNodeFactory.ITF_NAME)) {
      return nodeFactoryItf;
    } else {
      return super.lookupFc(s);
    }
  }

  @Override
  public void unbindFc(final String s) throws IllegalBindingException,
      NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(NodeMerger.ITF_NAME)) {
      nodeMergerItf = null;
    } else if (s.equals(XMLNodeFactory.ITF_NAME)) {
      nodeFactoryItf = null;
    } else {
      super.unbindFc(s);
    }
  }
}
