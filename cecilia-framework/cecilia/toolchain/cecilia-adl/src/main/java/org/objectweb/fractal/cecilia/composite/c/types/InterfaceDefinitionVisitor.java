/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.composite.c.types;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.interfaces.AbstractInterfaceVisitor;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterfaceForEach;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the definition of the interfaces of a default
 * composite component.
 */
public class InterfaceDefinitionVisitor extends AbstractInterfaceVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractInterfaceVisitor
  // ---------------------------------------------------------------------------

  /**
   * Creates a task that writes the source code containing the definition of the
   * interfaces of the component.
   */
  @Override
  protected Component createTask(final ComponentContainer container,
      final List<TypeInterface> itfs) throws ADLException, TaskException {
    final List<IDLDefinition> itfDefinitions = new ArrayList<IDLDefinition>();
    for (final Interface itf : itfs) {
      itfDefinitions.add(castNodeError(itf, IDLDefinitionContainer.class)
          .getIDLDefinition());
    }
    return taskFactoryItf.newPrimitiveTask(new InterfaceDefinitonTask(
        itfDefinitions), container, itfDefinitions);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds source code containing the definition of the interfaces for a
   * default composite component. This task provides the produced source code.
   */
  @TaskParameters({"componentNode", "interfaceDefinitions"})
  @ServerInterfaces(@ServerInterface(name = "source-code-provider", signature = SourceCodeProvider.class, record = "role:interfaceDefinition, id:%", parameters = "componentNode"))
  public static class InterfaceDefinitonTask extends AbstractDefinitionTask {

    protected final List<IDLDefinition>                 itfNodes;

    // -------------------------------------------------------------------------
    // Client interfaces
    // -------------------------------------------------------------------------

    /**
     * Source code to be included to access the interface definition header
     * files (idl.h) for each interface of the componentNode.
     */
    @ClientInterfaceForEach(iterable = "interfaceDefinitions", prefix = "c-interface-header", signature = SourceCodeProvider.class, record = "role:cInterfaceDefinition, id:%", parameters = "interfaceDefinitions.element")
    public final Map<TypeInterface, SourceCodeProvider> cItfDefinitionItfs = new HashMap<TypeInterface, SourceCodeProvider>();

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param itfNodes the list of interface definition nodes belonging to the
     *            component.
     */
    protected InterfaceDefinitonTask(final List<IDLDefinition> itfNodes) {
      this.itfNodes = itfNodes;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      // generate the definition of the structure representing the component
      // type
      final CodeWriter cw = new CodeWriter("Composite Interface Definition Builder");
      for (final IDLDefinition itf : itfNodes) {
        cw.appendln(cItfDefinitionItfs.get(itf).getSourceCode());
      }

      // defines the structure <name>_exporteds
      cw.append("struct ").append(typeNameProviderItf.getCTypeName()).append(
          "_exporteds {").endl();
      cw.append("Rfractal_api_Component           component;").endl();
      cw.append("Rfractal_api_BindingController   binding_controller;").endl();
      cw.append("Rfractal_api_LifeCycleController lifecycle_controller;")
          .endl();
      cw.append("};").endl();

      return cw.toString();
    }
  }
}
