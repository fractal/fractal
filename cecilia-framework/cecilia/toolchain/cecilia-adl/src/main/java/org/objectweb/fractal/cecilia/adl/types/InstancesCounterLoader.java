/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2009 STMicroelectronics
 * Copyright (C) 2007-2009 INRIA 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 * Contributor: Lionel Debroux (adaptation to Cecilia 2.x)
 */

package org.objectweb.fractal.cecilia.adl.types;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.adl.ast.ASTFractalUtil;
import org.objectweb.fractal.adl.components.ComponentContainer;

/**
 * This {@link Loader} component travels the AST and counts the instances of
 * each <em>primitive</em> component type. It sets the following
 * decoration(s):
 * <ul>
 * <li></li>
 * </ul>
 * 
 * @author Alessio Pace
 */
public class InstancesCounterLoader extends AbstractLoader {

  private HashMap<MergedComponentTypeDesc, ComponentContainer> primitiveTypes;

  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    final Definition definition = clientLoader.load(name, context);

    /* inizialize empty map */
    this.primitiveTypes = new HashMap<MergedComponentTypeDesc, ComponentContainer>();

    /* process the definition */
    check((ComponentContainer) definition);

    /* set to null for garbage collection */
    this.primitiveTypes = null;

    return definition;
  }

  protected void check(final ComponentContainer currentComponent) {
    if (!ASTFractalUtil.isPrimitive(currentComponent)) {

      /* it may be already decorated in case of shared components */
      if (TypeDecorationUtil.getTypeDecoration(currentComponent) != null) {
        return;
      }

      /*
       * XXX a composite component type is the component itself (so they are
       * singletons)
       */
      TypeDecorationUtil.setTypeDecoration(currentComponent, currentComponent);
      TypeDecorationUtil
          .incrementComponentTypeInstancesCount(currentComponent);

      final ComponentContainer[] subComponents = ASTFractalUtil
          .getFcSubComponents(currentComponent);
      for (final ComponentContainer subComp : subComponents) {
        check(subComp);
      }

    } else {
      /* else it is a primitive */

      /* it may be already decorated in case of shared components */
      if (TypeDecorationUtil.getTypeDecoration(currentComponent) != null) {
        return;
      }

      final MergedComponentTypeDesc currentCompDesc = new MergedComponentTypeDesc(
          currentComponent);

      final ComponentContainer type = this.primitiveTypes.get(currentCompDesc);

      /* if it is type encountered for the first time */
      if (type == null) {
        this.primitiveTypes.put(currentCompDesc, currentComponent);
        TypeDecorationUtil
            .setTypeDecoration(currentComponent, currentComponent);
        TypeDecorationUtil
            .incrementComponentTypeInstancesCount(currentComponent);
      } else {
        /* it is an already encountered type */
        final ComponentContainer alreadyEncounteredType = this.primitiveTypes
            .get(currentCompDesc);
        TypeDecorationUtil.setTypeDecoration(currentComponent,
            alreadyEncounteredType);
        TypeDecorationUtil
            .incrementComponentTypeInstancesCount(alreadyEncounteredType);
      }
    }
  }
}
