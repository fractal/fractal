/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.adl.controllers;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.error.NodeErrorLocator;
import org.objectweb.fractal.adl.implementations.Controller;
import org.objectweb.fractal.adl.implementations.ControllerContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.components.AbstractComponentVisitorPluginUser;
import org.objectweb.fractal.cecilia.adl.plugin.PluginErrors;
import org.objectweb.fractal.cecilia.adl.plugin.PluginException;
import org.objectweb.fractal.task.core.TaskException;

/**
 * {@link ComponentVisitor} selector that chooses the visitor component to be
 * executed for the given component node. The selection depends on the
 * {@link Controller controller description} of the component. This component
 * use the {@link Controller#getDescriptor() controller description} and the
 * {@link ExtendedController#getLanguage()  controller language} of the
 * component to select the plug-in to be used. For a given component, the name
 * of the plug-in to be used for visiting is found using the following
 * convention:
 * <code>"&lt;{@link #getADLPackage() package}&gt;.&lt;{@link ExtendedController#getLanguage() ctrlLanguage}&gt;&lt;{@link ExtendedController#getLanguage() ctrlDesc}&gt;&lt;{@link #getADLSuffix() ADLSuffix}&gt;"</code>
 * (Note : in order to respect the above naming convention, the first character
 * of {@link ExtendedController#getLanguage() ctrlLanguage} and the first
 * character of {@link ExtendedController#getLanguage() ctrlDesc} are
 * transformed to upper case).
 * 
 * @see org.objectweb.fractal.cecilia.adl.plugin.AbstractPluginUser#getPlugin(Object,
 *      Map, Class)
 */
public class VisitorSelectorForController
    extends
      AbstractComponentVisitorPluginUser implements ComponentVisitor {

  // --------------------------------------------------------------------------
  // Implementation of the PrimitiveCompiler interface
  // --------------------------------------------------------------------------

  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    final Controller controller = castNodeError(container,
        ControllerContainer.class).getController();

    if (controller == null)
      throw new CompilerError(GenericErrors.INTERNAL_ERROR,
          new NodeErrorLocator(container), "Can't find controller node in AST");

    final ExtendedController extendedController = castNodeError(controller,
        ExtendedController.class);

    final String pluginName = extendedController.getLanguage().substring(0, 1)
        .toUpperCase()
        + extendedController.getLanguage().substring(1)
        + extendedController.getDescriptor().substring(0, 1).toUpperCase()
        + extendedController.getDescriptor().substring(1);
    try {
      return getVisitorPlugin(pluginName, context).visit(path, container,
          context);
    } catch (final PluginException e) {
      if (e.getError().getTemplate() == PluginErrors.PLUGIN_NOT_FOUND) {
        throw new ADLException(
            ControllerErrors.CONTROLLER_VISITOR_PLUGIN_NOT_FOUND, pluginName);
      } else {
        throw new CompilerError(GenericErrors.INTERNAL_ERROR, e,
            "Can't load visitor plugin for controller \"" + pluginName + "\"");
      }
    }
  }
}
