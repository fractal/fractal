/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.idl;

import static java.util.Arrays.asList;
import static org.objectweb.fractal.adl.NodeUtil.castNodeError;
import static org.objectweb.fractal.cecilia.adl.idl.util.Util.getImportedAST;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.DefinitionVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.attributes.Attributes;
import org.objectweb.fractal.adl.attributes.AttributesContainer;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.Import;
import org.objectweb.fractal.cecilia.adl.idl.ast.ImportContainer;
import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;

/**
 * Travels the component's interfaces and attribute definitions and invokes
 * recursively its client IDL visitor interfaces for all these definitions as
 * well as all the definitions which are referenced.
 */
public class RecursiveIDLDispatchVisitor extends AbstractTaskFactoryUser
    implements
      DefinitionVisitor,
      BindingController {

  /** The composition schema used by this dispatcher. */
  public static final String               TASK_FILE_NAME         = "org.objectweb.fractal.cecilia.adl.idl.IDLTask";

  // ---------------------------------------------------------------------------
  // Client interfaces
  // ---------------------------------------------------------------------------

  /** The name of the builder collection interface. */
  public static final String               CLIENT_VISITORITF_NAME = "client-visitor";
  /** The builders client interfaces. */
  public Map<String, IDLDefinitionVisitor> visitorsItf            = new HashMap<String, IDLDefinitionVisitor>();

  // ---------------------------------------------------------------------------
  // Implementation of the PrimitiveCompiler interface
  // ---------------------------------------------------------------------------

  public Component visit(final List<Node> path, final Definition container,
      final Map<Object, Object> context) throws ADLException, TaskException {

    final List<Component> taskComponents = new ArrayList<Component>();
    final Set<String> visitedIDL = new HashSet<String>();

    visitComponentContainer(path, (ComponentContainer) container,
        taskComponents, visitedIDL, context);
    return taskFactoryItf
        .newCompositeTask(taskComponents, TASK_FILE_NAME, null);
  }

  // ---------------------------------------------------------------------------
  // Utility method
  // ---------------------------------------------------------------------------

  protected void visitComponentContainer(final List<Node> path,
      final ComponentContainer container, final List<Component> taskComponents,
      final Set<String> visitedIDL, final Map<Object, Object> context)
      throws ADLException, TaskException {

    path.add(container);
    try {
      for (final org.objectweb.fractal.adl.components.Component subComp : container
          .getComponents()) {
        visitComponentContainer(path, subComp, taskComponents, visitedIDL,
            context);
      }

      if (container instanceof InterfaceContainer) {
        visitInterfaceContainer(path, (InterfaceContainer)container, taskComponents, visitedIDL, context);
      }
      if (container instanceof AttributesContainer) {
        visitAttributesContainer(path, (AttributesContainer)container, taskComponents, visitedIDL, context);
      }
      if (container instanceof ImplementationContainer) {
        visitImplementationContainer(path, (ImplementationContainer)container, taskComponents, visitedIDL, context);
      }

    } finally {
      path.remove(path.size() - 1);
    }
  }

  protected void visitInterfaceContainer(final List<Node> path,
      final InterfaceContainer container, final List<Component> taskComponents,
      final Set<String> visitedIDL, final Map<Object, Object> context)
      throws ADLException, TaskException {
    /* for each interface */
    for (final Interface itf : container.getInterfaces()) {

      final IDLDefinition idl = castNodeError(itf,
          IDLDefinitionContainer.class).getIDLDefinition();

      final TypeInterface typeInterface = ((TypeInterface) itf);

      /*
       * invoke auxiliary method to verify if interface has to be compiled as
       * static (singleton style) or not
       */
      final boolean compileIdlAsSingleton = InterfaceDecorationUtil
          .hasToBeCompiledAsStatic(typeInterface);

      invokeVisitors(path, new IDLCompilationDesc(idl,
          compileIdlAsSingleton), taskComponents, visitedIDL, context);
    }
  }

  protected void visitAttributesContainer(final List<Node> path,
      final AttributesContainer container, final List<Component> taskComponents,
      final Set<String> visitedIDL, final Map<Object, Object> context)
      throws ADLException, TaskException {
    final Attributes attributes = container.getAttributes();
    if (attributes != null) {
      final IDLDefinition idl = castNodeError(attributes,
          IDLDefinitionContainer.class).getIDLDefinition();
      invokeVisitors(path, new IDLCompilationDesc(idl, false),
          taskComponents, visitedIDL, context);
    }
  }

  protected void visitImplementationContainer(final List<Node> path,
      final ImplementationContainer container, final List<Component> taskComponents,
      final Set<String> visitedIDL, final Map<Object, Object> context)
      throws ADLException, TaskException {
    // Do nothing.
  }

  protected void invokeVisitors(final List<Node> path,
      final IDLCompilationDesc idlCompilationDesc,
      final List<Component> taskComponents,
      final Set<String> visitedIDL, final Map<Object, Object> context)
      throws ADLException, TaskException {

    if (idlCompilationDesc == null) {
      throw new ADLException("IDLCompilationDesc argument can't be null");
    }

    IDLDefinition idlDefinition = idlCompilationDesc.getIdlDefinition();
    // add the IDLDefinition's name in cache. If it is not added (i.e. it is
    // already processed), that is the corresponding task component is already
    // added. In this case, returns.
    final String idlDefinitionName = idlDefinition.getName();
    if (idlDefinitionName == null) {
      throw new ADLException("The IDLDefinition name can't be null");
    }
    if (!visitedIDL.add(idlDefinitionName)) return;

    // Invoke all the client idl-compiler interfaces for this IDL definition
    for (final IDLDefinitionVisitor idlVisitor : visitorsItf.values()) {
      final Component task = idlVisitor.visit(path, idlCompilationDesc, context);
      if (task != null) taskComponents.add(task);
    }
    if (idlDefinition instanceof ImportContainer) {
      for (final Import imp : ((ImportContainer) idlDefinition).getImports()) {
        final Node importedAST = getImportedAST(imp);
        if (importedAST instanceof IDLDefinition) {
          final IDLDefinition importedDef = (IDLDefinition) importedAST;
          invokeVisitors(path, new IDLCompilationDesc(importedDef, false),
              taskComponents, visitedIDL, context);
        }
      }
    }
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  @Override
  public String[] listFc() {
    final List<String> interfaceList = new ArrayList<String>(visitorsItf
        .keySet());
    interfaceList.addAll(asList(super.listFc()));
    return interfaceList.toArray(new String[interfaceList.size()]);
  }

  @Override
  public Object lookupFc(final String clientItfName)
      throws NoSuchInterfaceException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.startsWith(CLIENT_VISITORITF_NAME)) {
      return visitorsItf.get(clientItfName);
    } else {
      return super.lookupFc(clientItfName);
    }
  }

  @Override
  public void bindFc(final String clientItfName, final Object serverItf)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.startsWith(CLIENT_VISITORITF_NAME)) {
      visitorsItf.put(clientItfName, (IDLDefinitionVisitor) serverItf);
    } else {
      super.bindFc(clientItfName, serverItf);
    }
  }

  @Override
  public void unbindFc(final String clientItfName)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.startsWith(CLIENT_VISITORITF_NAME)) {
      visitorsItf.remove(clientItfName);
    } else {
      super.unbindFc(clientItfName);
    }
  }
}
