/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributor(s): Philippe Merle
 *                 Matthieu Leclercq (externalize interface and implementation
 *   loading)
 *                 Alessio Pace (made this Loader a Checker to ease reuse)
 */

package org.objectweb.fractal.cecilia.adl.implementations;

import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationDecorationUtil.setCode;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.NodeUtil;
import org.objectweb.fractal.adl.error.ChainedErrorLocator;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.adl.implementations.ImplementationErrors;
import org.objectweb.fractal.cecilia.adl.Checker;
import org.objectweb.fractal.cecilia.adl.directives.Include;
import org.objectweb.fractal.cecilia.adl.directives.IncludeContainer;

/**
 * A {@link org.objectweb.fractal.cecilia.adl.Checker} to check
 * {@link Implementation} nodes in definitions.
 * It complains if the _this optimization is activated, but there is a
 * component implemented in a language which doesn't support _this optimization.
 */
public class ImplementationChecker
    extends org.objectweb.fractal.adl.implementations.ImplementationLoader
    implements Checker {

  /**
   * An array of strings corresponding to the implementation languages
   * which cannot handle _this optimization.
   */
  protected static final String[] thisOptimizationUnawareLanguages = {"thinkMC"};

  // ---------------------------------------------------------------------------
  // Implementation of the Checker interface
  // ---------------------------------------------------------------------------
  public void check(Definition definition, Map<Object, Object> context)
      throws ADLException {
    checkNode(definition, context);
  }

  // ---------------------------------------------------------------------------
  // Overridden methods
  // ---------------------------------------------------------------------------

  @Override
  protected void checkImplementationContainer(
      final ImplementationContainer container, final Map<Object, Object> context)
      throws ADLException {
    final Implementation impl = container.getImplementation();
    if (impl == null) return;

    NodeUtil.castNodeError(impl, ExtendedImplementation.class);

    final ExtendedImplementation extImpl = (ExtendedImplementation) impl;
    final String className = impl.getClassName();
    if (className == null)
      throw new ADLException(ImplementationErrors.IMPLEMENTATION_MISSING, impl);

    final String language = extImpl.getLanguage();

    for (String implLanguage : thisOptimizationUnawareLanguages) {
      if (implLanguage.equals(language)) {
        String s2 = System.getProperty("CECILIA_ENABLE_THIS_OPTIMIZATION");
        if (s2 != null && s2.equals("true")) {
          throw new ADLException(
              "_this optimization cannot be enabled because of components implemented in "
              + implLanguage + " language", container);
        }
      }
    }

    Object code;
    try {
      code = implementationCodeLoaderItf.loadImplementation(className,
          language, context);
    } catch (final ADLException e) {
      ChainedErrorLocator.chainLocator(e, impl);
      throw e;
    }
    setCode(impl, code);

    if (impl instanceof IncludeContainer) {
      for (final Include include : ((IncludeContainer) impl).getIncludes()) {
        final String includeFile = include.getFile();
        if (includeFile == null || includeFile.equals("")) {
          throw new ADLException(
              "Include node should specify a file attribute", (Node) include);
        }
        Object includeCode;
        try {
          includeCode = implementationCodeLoaderItf.loadImplementation(
              includeFile, language, context);
        } catch (final ADLException e) {
          ChainedErrorLocator.chainLocator(e, impl);
          throw e;
        }
        setCode((Node) include, includeCode);
      }
    }
  }
}
