/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.types;

import java.util.HashSet;
import java.util.Set;

import org.objectweb.fractal.adl.bindings.Binding;
import org.objectweb.fractal.adl.bindings.BindingContainer;
import org.objectweb.fractal.adl.components.ComponentContainer;

/**
 * A sub class of {@link ComponentTypeDesc} able to verify equality for
 * <em>merged primitive</em> component types.
 *
 * @author Alessio Pace
 */
public class MergedComponentTypeDesc extends ComponentTypeDesc {

  /**
   * @param container
   */
  public MergedComponentTypeDesc(final ComponentContainer container) {
    super(container);
  }

  /*
   * (non-Javadoc)
   *
   * @see org.objectweb.fractal.cecilia.adl.types.ComponentTypeDesc#equals(java.lang.Object)
   */
  @Override
  public boolean equals(final Object o) {

    final ComponentTypeDesc otherComponentTypeDesc = ((ComponentTypeDesc) o);

    final boolean superEquals = super.equals(o);

    if (superEquals) {

      final Set<String> innerBindingsSet = createInnerBindingsSet(this.container);
      final Set<String> otherInnerBindingsSet = createInnerBindingsSet(otherComponentTypeDesc.container);

      if (!innerBindingsSet.equals(otherInnerBindingsSet)) {
        return false;
      }

      return true;
    }

    return false;
  }

  /**
   * @param container
   * @return
   */
  private Set<String> createInnerBindingsSet(final ComponentContainer container) {

    final Set<String> result = new HashSet<String>();

    for (final Binding b : ((BindingContainer) container).getBindings()) {
      result.add("_INNER_BINDING_" + b.getFrom() + "_" + b.getTo());
    }

    return result;
  }

  /*
   * (non-Javadoc)
   *
   * @see org.objectweb.fractal.cecilia.adl.types.ComponentTypeDesc#computeHashCode()
   */
  @Override
  protected void computeHashCode() {
    super.computeHashCode();

    for (final Binding b : ((BindingContainer) container).getBindings()) {
      this.hashCode += ("_INNER_BINDING_" + b.getFrom() + "_" + b.getTo())
          .hashCode() * 37;
    }
  }
}
