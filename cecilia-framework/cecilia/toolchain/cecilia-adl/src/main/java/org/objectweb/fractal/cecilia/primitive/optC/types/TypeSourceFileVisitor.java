/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.primitive.optC.types;

import static org.objectweb.fractal.cecilia.adl.types.TypeDecorationUtil.getTypeDecoration;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.file.SourceFile;
import org.objectweb.fractal.cecilia.adl.file.SourceFileProvider;
import org.objectweb.fractal.cecilia.adl.file.SourceFileWriter;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the source file containing the definition of
 * the component type. More precisely, this visitor creates a C header file that
 * contains the definition of a data structure describing the interfaces (server
 * and client) of the component.
 */
public class TypeSourceFileVisitor extends AbstractTaskFactoryUser
    implements
      ComponentVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits a {@link ComponentContainer} node and creates a task that creates
   * the source file components' type definition.
   */
  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    // sanity check
    final ComponentContainer typeDecoration = getTypeDecoration(container);
    if ((typeDecoration != null) && (typeDecoration != container)) {
      throw new CompilerError(GenericErrors.INTERNAL_ERROR,
          "This compiler must be invoked only on component that defines a type.");
    }

    return taskFactoryItf.newPrimitiveTask(new TypeSourceFileTask(
        (File) context.get("adlBuildDirectory")), container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds the ".adl.h" source file that contains the definition of the
   * component type structure (i.e. its client and server interfaces). Provides
   * a server interface giving access to this file. Provides another server
   * interface that returns the #include file to be used by tasks that need the
   * structure definition.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces({
      @ServerInterface(name = "component-type-file-provider", signature = SourceFileProvider.class, record = "role:componentTypeFile, id:%", parameters = "componentNode"),
      @ServerInterface(name = "component-type-code-provider", signature = SourceCodeProvider.class, record = "role:componentTypeCode, id:%", parameters = "componentNode")})
  public static class TypeSourceFileTask extends AbstractDefinitionTask
      implements
        SourceFileProvider {

    /** The suffix of the generated file. */
    public static final String FILE_NAME_SUFFIX = ".adl.h";

    protected File             adlBuildDirectory;

    // generated code
    protected SourceFile       sourceFile;
    protected String           sourceCode;

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /** Source code for the interface definition. */
    @ClientInterface(name = "type-definition-provider", record = "role:typeDefinition, id:%", parameters = "componentNode")
    public SourceCodeProvider  serverInterfaceDefinitionsProviderItf;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param adlBuildDirectory The directory into which the generated file will
     *            be placed.
     */
    public TypeSourceFileTask(final File adlBuildDirectory) {
      this.adlBuildDirectory = adlBuildDirectory;
    }

    // -------------------------------------------------------------------------
    // Implementation of the SourceFileProvider interface
    // -------------------------------------------------------------------------

    public SourceFile getSourceFile() {
      return sourceFile;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final String fileName = typeNameProviderItf.getCTypeName()
          + FILE_NAME_SUFFIX;
      CodeWriter cw = new CodeWriter();
      cw.appendln("// THIS FILE HAS BEEN GENERATED BY THE CECILIA ADL"
          + " COMPILER.");
      cw.appendln("// DO NOT EDIT");
      final String name = typeNameProviderItf.getCTypeName().toUpperCase()
          + "_H";
      cw.append("#ifndef ").append(name).endl();
      cw.append("#define ").append(name).endl();
      cw.endl();
      cw.appendln(serverInterfaceDefinitionsProviderItf.getSourceCode());
      cw.append("#endif  //").append(name).endl();

      final File file = new File(adlBuildDirectory, fileName);
      if (!file.getParentFile().exists()) file.getParentFile().mkdirs();
      SourceFileWriter.writeToFile(file, cw.toString());
      sourceFile = new SourceFile(typeNameProviderItf.getTypeName(), file);

      cw = new CodeWriter();
      cw.append("#include \"").append(fileName).append("\"").endl();
      return cw.toString();
    }
  }
}
