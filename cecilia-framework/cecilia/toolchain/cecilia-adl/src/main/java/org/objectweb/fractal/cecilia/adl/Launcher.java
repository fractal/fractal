/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl;

import java.io.File;
import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.adl.JavaFactory;
import org.objectweb.fractal.adl.StaticJavaGenerator;
import org.objectweb.fractal.adl.util.FractalADLLogManager;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.cecilia.adl.directives.DirectiveHelper;
import org.objectweb.fractal.cecilia.adl.plugin.AbstractPluginManager;
import org.objectweb.fractal.cecilia.targetDescriptor.TargetDescriptorException;
import org.objectweb.fractal.cecilia.targetDescriptor.TargetDescriptorLoader;
import org.objectweb.fractal.cecilia.targetDescriptor.TargetDescriptorLoaderJavaFactory;
import org.objectweb.fractal.cecilia.targetDescriptor.ast.ADLMapping;
import org.objectweb.fractal.cecilia.targetDescriptor.ast.CFlag;
import org.objectweb.fractal.cecilia.targetDescriptor.ast.LdFlag;
import org.objectweb.fractal.cecilia.targetDescriptor.ast.Target;
import org.objectweb.fractal.util.Fractal;

/**
 * A class to compile a component. Usage: Launcher &lt;definition&gt; where
 * &lt;definition&gt; is the name of the component to be compiled.
 */
public class Launcher extends AbstractLauncher {

  protected static final String   PROGRAM_NAME_PROPERTY_NAME     = "cecilia.launcher.name";

  /**
   * The default value of the {@link #PLUGIN_FACTORY_BACKEND_PROPERTY_NAME}
   * property.
   */
  public static final String      DEFAULT_PLUGIN_FACTORY_BACKEND = "Java";

  /** The name of the default ADL of the compiler. */
  public static final String      CECILIA_BASIC_COMPILER_ADL     = "org.objectweb.fractal.cecilia.adl.CeciliaBasicFactory";

  public static final String      C_FLAGS                        = "c-flags";
  public static final String      COMPILER_COMMAND               = "compiler-command";
  public static final String      LD_FLAGS                       = "ld-flags";
  public static final String      LINKER_COMMAND                 = "linker-command";
  public static final String      LINKER_SCRIPT                  = "linker-script";
  public static final String      ARCHIVER_COMMAND               = "archiver-command";
  public static final String      EXEC_NAME                      = "executable-name";

  protected final CmdArgument     compilerADLOpt                 = new CmdArgument(
                                                                     null,
                                                                     "compiler-adl",
                                                                     "Specify the name of the ADL of the compiler itself",
                                                                     "<adl name>",
                                                                     CECILIA_BASIC_COMPILER_ADL,
                                                                     false);

  protected final CmdArgument     targetDescOpt                  = new CmdArgument(
                                                                     "t",
                                                                     "target-descriptor",
                                                                     "Specify the target descriptor",
                                                                     "<name>");

  protected final CmdArgument     compilerCmdOpt                 = new CmdArgument(
                                                                     null,
                                                                     "compiler-command",
                                                                     "the command of the C compiler",
                                                                     "<path>",
                                                                     "gcc",
                                                                     false);

  protected final CmdAppendOption cFlagsOpt                      = new CmdAppendOption(
                                                                     "c",
                                                                     "c-flags",
                                                                     "the c-flags compiler directives",
                                                                     "<flags>");

  protected final CmdPathOption   includePathOpt                 = new CmdPathOption(
                                                                     "I",
                                                                     "inc-path",
                                                                     "the list of path to be added in compiler include paths",
                                                                     "<path list>");

  protected final CmdArgument     linkerCmdOpt                   = new CmdArgument(
                                                                     null,
                                                                     "linker-command",
                                                                     "the command of the linker",
                                                                     "<path>",
                                                                     "gcc",
                                                                     false);

  protected final CmdAppendOption ldFlagsOpt                     = new CmdAppendOption(
                                                                     "l",
                                                                     "ld-flags",
                                                                     "the ld-flags compiler directives",
                                                                     "<flags>");

  protected final CmdPathOption   ldPathOpt                      = new CmdPathOption(
                                                                     "L",
                                                                     "ld-path",
                                                                     "the list of path to be added to linker library search path",
                                                                     "<path list>");

  protected final CmdArgument     archiverCmdOpt                 = new CmdArgument(
                                                                     null,
                                                                     "archiver-command",
                                                                     "the command of the archiver tool",
                                                                     "<path>",
                                                                     "ar",
                                                                     false);

  protected final CmdArgument     linkerScriptOpt                = new CmdArgument(
                                                                     "T",
                                                                     "linker-script",
                                                                     "linker script to use (given path is resolved in source path)",
                                                                     "<path>");

  protected final CmdArgument     executableNameCmdOpt           = new CmdArgument(
                                                                     null,
                                                                     "executable-name",
                                                                     "the name of the target executable file (can be used with only one ADL to be compiled.)",
                                                                     "<name>");

  protected final CmdArgument     concurrentJobCmdOpt            = new CmdArgument(
                                                                     "j",
                                                                     "jobs",
                                                                     "The number of concurrent compilation jobs",
                                                                     "<number>",
                                                                     "1", false);

  protected final CmdFlag         printStackTraceOpt             = new CmdFlag(
                                                                     "e", null,
                                                                     "Print error stack traces");

  protected final CmdFlag         checkADLModeOpt                = new CmdFlag(
                                                                     null,
                                                                     "check-adl",
                                                                     "Only check input ADL(s), do not compile");

  protected Map<String, String>   adlToExecName;
  protected Map<Object, Object>   compilerContext                = new HashMap<Object, Object>();
  protected Factory               compiler;

  protected Target                targetDescriptor;

  protected boolean               printStackTrace                = false;

  protected boolean               checkADLMode                   = false;

  protected static Logger         logger                         = FractalADLLogManager
                                                                     .getLogger("launcher");

  protected File                  buildDir;

  protected File                  adlBuildDir;
  protected File                  idlBuildDir;
  protected File                  objBuildDir;

  /**
   * Creates a new Compiler launcher context.
   * 
   * @param outDir the output directory
   * @param sources the source path.
   * @throws InvalidCommandLineException
   * @throws CompilerInstantiationException
   */
  public Launcher(final String outDir, final String sources)
      throws InvalidCommandLineException, CompilerInstantiationException {
    init("-o=" + outDir, "-src-path=" + sources);
  }

  /**
   * Creates a new Compiler launcher context.
   * 
   * @param options the options
   * @throws InvalidCommandLineException
   * @throws CompilerInstantiationException
   */
  public Launcher(final Map<String, String> options)
      throws InvalidCommandLineException, CompilerInstantiationException {
    init(toArgs(options));
  }

  protected static String[] toArgs(final Map<String, String> options) {
    final String[] params = new String[options.size()];
    int i = 0;
    for (final Map.Entry<String, String> option : options.entrySet()) {
      if (option.getValue() == null) {
        params[i] = option.getKey();
      } else {
        params[i] = option.getKey() + '=' + option.getValue();
      }
      i++;
    }
    return params;
  }

  /**
   * Creates a new Compiler launcher context and run compilation.
   * 
   * @param args the command line arguments.
   * @throws Exception
   */
  public Launcher(final String... args) throws Exception {
    try {
      init(args);
      compile();
    } catch (final InvalidCommandLineException e) {
      handleException(e);
    } catch (final CompilerInstantiationException e) {
      handleException(e);
    } catch (final ADLException e) {
      handleException(e);
    }
  }

  protected void init(final String... args) throws InvalidCommandLineException,
      CompilerInstantiationException {
    if (logger.isLoggable(Level.CONFIG)) {
      for (final String arg : args) {
        logger.config("[arg] " + arg);
      }
    }

    addOptions();

    // parse arguments to a CommandLine.
    final CommandLine cmdLine = CommandLine.parseArgs(options, false, args);

    // If help is asked, print it and exit.
    if (helpOpt.isPresent(cmdLine)) {
      printHelp(System.out);
      System.exit(0);
    }

    // get list of ADL
    final List<String> adlList = cmdLine.getArguments();
    adlToExecName = parserADLList(adlList, cmdLine);

    // instantiate compiler
    compiler = createCompiler(compilerContext, cmdLine);

    // add source class loader in context
    final ClassLoader sourceClassLoader = getSourceClassLoader(cmdLine);
    compilerContext.put("classloader", sourceClassLoader);

    // load target descriptor (if any)
    final String targetDesc = targetDescOpt.getValue(cmdLine);
    if (targetDesc != null) {
      final TargetDescriptorLoader loader = createTargetDescriptorLoader(compilerContext);
      try {
        targetDescriptor = loader.load(targetDesc, compilerContext);
      } catch (final TargetDescriptorException e) {
        logger.log(Level.FINE, "Error while loading target descriptor", e);
        throw new InvalidCommandLineException(
            "Unable to load target descriptor: " + e.getMessage(), 1);
      }
    }
    if (targetDescriptor != null && targetDescriptor.getLinkerScript() != null) {
      final URL linkerScriptURL = sourceClassLoader
          .getResource(targetDescriptor.getLinkerScript().getPath());
      if (linkerScriptURL == null) {
        throw new InvalidCommandLineException("Invalid linker script: '"
            + targetDescriptor.getLinkerScript().getPath()
            + "'. Cannot find file in the source path", 1);
      }
      targetDescriptor.getLinkerScript().setPath(linkerScriptURL.getPath());
    }

    printStackTrace = printStackTraceOpt.isPresent(cmdLine);
    checkADLMode = checkADLModeOpt.isPresent(cmdLine);

    // add build directories to context
    String optValue = outDirOpt.getValue(cmdLine);
    if (nullOrEmpty(optValue)) {
      throw new InvalidCommandLineException("Invalid output directory ''", 1);
    }
    buildDir = new File(optValue);
    checkDir(buildDir);
    if (!buildDir.exists()) {
      throw new InvalidCommandLineException("Invalid output directory '"
          + optValue + "' does not exist.", 1);
    }
    adlBuildDir = new File(buildDir, "adl");
    idlBuildDir = new File(buildDir, "idl");
    objBuildDir = new File(buildDir, "obj");
    checkDir(adlBuildDir);
    checkDir(idlBuildDir);
    checkDir(objBuildDir);

    // build c-flags
    final List<String> cFlagsList = new ArrayList<String>();
    final List<String> incPaths = new ArrayList<String>();
    if (srcPathOpt.getPathValue(cmdLine) != null) {
      incPaths.addAll(srcPathOpt.getPathValue(cmdLine));
    }
    if (includePathOpt.getValue(cmdLine) != null) {
      incPaths.addAll(includePathOpt.getPathValue(cmdLine));
    }
    incPaths.add(adlBuildDir.getAbsolutePath());
    incPaths.add(idlBuildDir.getAbsolutePath());

    for (final String inc : incPaths) {
      final File incDir = new File(inc);
      cFlagsList.add("-I");
      cFlagsList.add(incDir.getAbsolutePath());
    }

    optValue = cFlagsOpt.getValue(cmdLine);
    if (!nullOrEmpty(optValue)) {
      cFlagsList.addAll(DirectiveHelper.splitOptionString(optValue));
    }
    compilerContext.put(C_FLAGS, cFlagsList);

    // build ld-flags
    final List<String> ldPaths = ldPathOpt.getPathValue(cmdLine);
    final List<String> ldFlagsList = new ArrayList<String>();
    if (ldPaths != null) {
      for (final String ld : ldPaths) {
        final File ldDir = new File(ld);
        ldFlagsList.add("-L");
        ldFlagsList.add(ldDir.getAbsolutePath());
      }
    }
    optValue = ldFlagsOpt.getValue(cmdLine);
    if (!nullOrEmpty(optValue)) {
      ldFlagsList.addAll(DirectiveHelper.splitOptionString(optValue));
    }
    compilerContext.put(LD_FLAGS, ldFlagsList);

    // add compiler arguments to context

    if (compilerCmdOpt.isPresent(cmdLine)) {
      optValue = compilerCmdOpt.getValue(cmdLine);
      if (optValue.length() == 0)
        throw new InvalidCommandLineException("Invalid compiler ''", 1);
      compilerContext.put(COMPILER_COMMAND, optValue);
    }

    if (linkerCmdOpt.isPresent(cmdLine)) {
      optValue = linkerCmdOpt.getValue(cmdLine);
      if (optValue.length() == 0)
        throw new InvalidCommandLineException("Invalid linker ''", 1);
      compilerContext.put(LINKER_COMMAND, optValue);
    }

    if (archiverCmdOpt.isPresent(cmdLine)) {
      optValue = archiverCmdOpt.getValue(cmdLine);
      if (optValue.length() == 0)
        throw new InvalidCommandLineException("Invalid archiver ''", 1);
      compilerContext.put(ARCHIVER_COMMAND, optValue);
    }

    Integer jobs = null;
    try {
      jobs = Integer.decode(concurrentJobCmdOpt.getValue(cmdLine));
    } catch (final NumberFormatException e) {
      throw new InvalidCommandLineException("Invalid jobs value '"
          + concurrentJobCmdOpt.getValue(cmdLine) + "' is not a valid number",
          1);
    }
    compilerContext.put("jobs", jobs);

    // add linker script to the context
    final String linkerScript = linkerScriptOpt.getValue(cmdLine);
    if (linkerScript != null) {
      final URL linkerScriptURL = sourceClassLoader.getResource(linkerScript);
      if (linkerScriptURL == null) {
        throw new InvalidCommandLineException("Invalid linker script: '"
            + linkerScript + "'. Cannot find file in the source path", 1);
      }

      compilerContext.put(LINKER_SCRIPT, linkerScriptURL.getPath());
    }
  }

  protected void addOptions() {
    options.addOptions(compilerADLOpt, targetDescOpt, compilerCmdOpt,
        cFlagsOpt, includePathOpt, linkerCmdOpt, ldFlagsOpt, ldPathOpt,
        archiverCmdOpt, linkerScriptOpt, executableNameCmdOpt,
        concurrentJobCmdOpt, printStackTraceOpt, checkADLModeOpt);
  }

  protected void initBuildDirs(final Map<Object, Object> context) {
    adlBuildDir.mkdirs();
    context.put("adlBuildDirectory", adlBuildDir);

    idlBuildDir.mkdirs();
    context.put("idlBuildDirectory", idlBuildDir);

    objBuildDir.mkdirs();
    context.put("objBuildDirectory", objBuildDir);
  }

  protected Map<String, String> parserADLList(final List<String> adlList,
      final CommandLine cmdLine) throws InvalidCommandLineException {
    final Map<String, String> adlToExecName = new LinkedHashMap<String, String>();

    final String defaultExecName = executableNameCmdOpt.getValue(cmdLine);
    boolean defaultExeNameUsed = false;
    // parse adlNames
    for (final String adlName : adlList) {
      final int i = adlName.indexOf(':');
      if (i == -1) {
        if (defaultExeNameUsed) {
          if (defaultExecName == null)
            throw new InvalidCommandLineException(
                "Executable name must be specified for adl \"" + adlName
                    + "\" using -" + executableNameCmdOpt.getLongName()
                    + "=NAME or the ADL:NAME syntax.", 1);
          else
            throw new InvalidCommandLineException(
                "Executable name must be specified for adl \"" + adlName
                    + "\" using the ADL:NAME syntax.", 1);
        }
        adlToExecName.put(adlName, defaultExecName);
        defaultExeNameUsed = true;
      } else {
        final String adl = adlName.substring(0, i);
        final String exec = adlName.substring(i + 1);
        adlToExecName.put(adl, exec);
      }
    }

    return adlToExecName;
  }

  protected String processContext(final Target targetDesc,
      final String inputADL, final Map<Object, Object> context) {
    processCFlags(targetDesc, context);
    processLdFlags(targetDesc, context);
    processCompiler(targetDesc, context);
    processLinker(targetDesc, context);
    processLinkerScript(targetDesc, context);
    processArchiver(targetDesc, context);
    return processADLMapping(targetDesc, inputADL, context);
  }

  protected void processCFlags(final Target target,
      final Map<Object, Object> context) {
    if (target != null && target.getCFlags().length > 0) {
      final CFlag[] flags = target.getCFlags();

      List<String> targetFlags = new ArrayList<String>();
      for (int i = 0; i < flags.length; i++) {
        targetFlags.addAll(DirectiveHelper.splitOptionString(
            flags[i].getValue()));
      }

      if (logger.isLoggable(Level.FINE))
        logger.log(Level.FINE, "Adding target c-flags: " + targetFlags);

      List<String> contextFlags = (List<String>) context.get(C_FLAGS);
      if (contextFlags == null) {
        contextFlags = new ArrayList<String>();
      }
      contextFlags.addAll(targetFlags);
      context.put(C_FLAGS, contextFlags);
    }
  }

  protected void processLdFlags(final Target target,
      final Map<Object, Object> context) {
    if (target != null && target.getLdFlags().length > 0) {
      final LdFlag[] flags = target.getLdFlags();

      List<String> targetFlags = new ArrayList<String>();
      for (int i = 0; i < flags.length; i++) {
        targetFlags.addAll(DirectiveHelper.splitOptionString(
            flags[i].getValue()));
      }

      if (logger.isLoggable(Level.FINE))
        logger.log(Level.FINE, "Adding target ld-flags: " + targetFlags);

      List<String> contextFlags = (List<String>) context.get(LD_FLAGS);
      if (contextFlags == null) {
        contextFlags = new ArrayList<String>();
      }
      contextFlags.addAll(targetFlags);
      context.put(LD_FLAGS, contextFlags);
    }
  }

  protected void processCompiler(final Target target,
      final Map<Object, Object> context) {
    final String opt = (String) context.get(COMPILER_COMMAND);
    if (opt == null) {
      if (target != null && target.getCompiler() != null) {
        if (logger.isLoggable(Level.FINE)) {
          logger.log(Level.FINE, "Using target compiler : "
              + target.getCompiler().getPath());
        }
        context.put(COMPILER_COMMAND, target.getCompiler().getPath());
      } else {
        context.put(COMPILER_COMMAND, compilerCmdOpt.getDefaultValue());
      }
    }
  }

  protected void processLinker(final Target target,
      final Map<Object, Object> context) {
    final String opt = (String) context.get(LINKER_COMMAND);
    if (opt == null) {
      if (target != null && target.getLinker() != null) {
        if (logger.isLoggable(Level.FINE)) {
          logger.log(Level.FINE, "Using target linker : "
              + target.getLinker().getPath());
        }
        context.put(LINKER_COMMAND, target.getLinker().getPath());
      } else {
        context.put(LINKER_COMMAND, linkerCmdOpt.getDefaultValue());
      }
    }
  }

  protected void processLinkerScript(final Target target,
      final Map<Object, Object> context) {
    if (target != null) {
      final String opt = (String) context.get(LINKER_SCRIPT);
      if (opt == null && target.getLinkerScript() != null) {
        if (logger.isLoggable(Level.FINE)) {
          logger.log(Level.FINE, "Using target linker script : "
              + target.getLinkerScript().getPath());
        }
        context.put(LINKER_SCRIPT, target.getLinkerScript().getPath());
      }
    }
  }

  protected void processArchiver(final Target target,
      final Map<Object, Object> context) {
    final String opt = (String) context.get(ARCHIVER_COMMAND);
    if (opt == null) {
      if (target != null && target.getArchiver() != null) {
        if (logger.isLoggable(Level.FINE)) {
          logger.log(Level.FINE, "Using target archiver : "
              + target.getArchiver().getPath());
        }
        context.put(ARCHIVER_COMMAND, target.getArchiver().getPath());
      } else {
        context.put(ARCHIVER_COMMAND, archiverCmdOpt.getDefaultValue());
      }
    }
  }

  protected String processADLMapping(final Target target,
      final String inputADL, final Map<Object, Object> context) {
    if (target != null) {
      final ADLMapping mapping = target.getAdlMapping();
      if (mapping == null) return inputADL;

      if (mapping.getOutputName() != null) {
        final String outputName = ((String) context.get(EXEC_NAME)).replace(
            "${inputADL}", inputADL);
        if (logger.isLoggable(Level.FINE)) {
          logger.log(Level.FINE, "Compiling ADL : " + outputName);
        }
        context.put(EXEC_NAME, outputName);
      }

      return mapping.getMapping().replace("${inputADL}", inputADL);
    } else {
      return inputADL;
    }
  }

  /**
   * Create the o.o.f.a.Factory class that will be used to read the application
   * definition. Subclasses of the Launcher may extend this.
   */
  protected Factory createCompiler(final Map<Object, Object> compilerContext,
      final CommandLine cmdLine) throws CompilerInstantiationException {
    return createCompiler(compilerADLOpt.getValue(cmdLine), compilerContext);
  }

  protected Factory createCompiler(final String compilerAdl,
      final Map<Object, Object> compilerContext)
      throws CompilerInstantiationException {
    final Object pluginFactoryBackend = compilerContext
        .get(AbstractPluginManager.PLUGIN_FACTORY_BACKEND);
    if (pluginFactoryBackend == null
        || DEFAULT_PLUGIN_FACTORY_BACKEND.equals(pluginFactoryBackend)) {
      /* instantiate the bootstrap factory with the default backend */
      try {
        final Class<?> factoryFactoryClass = getClass().getClassLoader()
            .loadClass(compilerAdl + StaticJavaGenerator.CLASSNAME_SUFFIX);
        final JavaFactory factoryFactory = (JavaFactory) factoryFactoryClass
            .newInstance();
        final Map<?, ?> compilerComponent = (Map<?, ?>) factoryFactory
            .newComponent();
        logger
            .fine("[Launcher] Cecilia compiler instantiated using statically generated factory.");
        return (Factory) compilerComponent.get("factory");
      } catch (final Exception e) {
        // ignore and try to instantiate the compiler using a FractalADL
        // factory.
        if (logger.isLoggable(Level.FINE)) {
          logger
              .fine("[Launcher] Unable to instantiate cecilia compiler using statically generated factory ("
                  + e.getClass().getName() + ":" + e.getMessage() + ").");
        }
      }

      try {
        final Factory factory = FactoryFactory.getFactory();
        /* try to instantiate the CeciliaADL BasicFactory */
        final Map<?, ?> compilerComponent = (Map<?, ?>) factory.newComponent(
            compilerAdl, compilerContext);
        logger
            .fine("[Launcher] Cecilia compiler instantiated using FractalADL factory.");
        return (Factory) compilerComponent.get("factory");
      } catch (final ADLException e) {
        throw new CompilerInstantiationException(
            "Unable to instantiate Cecilia compiler", e, 101);
      }

    } else {
      /* instantiate the bootstrap factory with the Fractal backend */
      Component compilerComponent = null;
      try {
        final Factory factory = FactoryFactory
            .getFactory(FactoryFactory.FRACTAL_BACKEND);

        /* try to instantiate the CeciliaADL BasicFactory */
        compilerComponent = (Component) factory.newComponent(compilerAdl,
            compilerContext);
        /* start compiler */
        Fractal.getLifeCycleController(compilerComponent).startFc();

        final Factory result = (Factory) compilerComponent
            .getFcInterface("factory");
        return result;
      } catch (final ADLException e) {
        throw new CompilerInstantiationException(
            "Unable to instantiate Cecilia compiler", e, 101);
      } catch (final NoSuchInterfaceException e) {
        throw new CompilerInstantiationException(
            "Unable to instantiate Cecilia compiler", e, 101);
      } catch (final IllegalLifeCycleException e) {
        throw new CompilerInstantiationException(
            "Unable to instantiate Cecilia compiler", e, 101);
      }
    }
  }

  protected TargetDescriptorLoader createTargetDescriptorLoader(
      final Map<Object, Object> compilerContext)
      throws CompilerInstantiationException {
    try {
      final JavaFactory factory = new TargetDescriptorLoaderJavaFactory();
      final Map<?, ?> component = (Map<?, ?>) factory.newComponent();
      return (TargetDescriptorLoader) component.get("loader");
    } catch (final Exception e) {
      throw new CompilerInstantiationException(
          "Unable to instantiate target descriptor loader", e, 101);
    }
  }

  protected static boolean nullOrEmpty(final String s) {
    return s == null || s.length() == 0;
  }

  /**
   * Compiles the given ADL.
   * 
   * @param adlName the name of the the name of the architecture (ADL) to
   *          compile.
   * @return the list of objects that are the results of the compilation.
   * @throws ADLException if the compilation fails.
   */
  public List<Object> compile(final String adlName) throws ADLException {
    final List<Object> result = new ArrayList<Object>();
    final Map<Object, Object> additionalParams = new HashMap<Object, Object>();
    compile(adlName, null, result, additionalParams);
    return result;
  }

  /**
   * Compiles the given ADL.
   * 
   * @param adlName the name of the the name of the architecture (ADL) to
   *          compile.
   * @param additionalParams additional parameters for ADL build. Used by
   *          merge tests.
   * @return the list of objects that are the results of the compilation.
   * @throws ADLException if the compilation fails.
   */
  public List<Object> compile(final String adlName, final
      Map<Object, Object> additionalParams) throws ADLException {
    final List<Object> result = new ArrayList<Object>();
    compile(adlName, null, result, additionalParams);
    return result;
  }

  /**
   * Compiles the given ADL.
   * 
   * @param adlName the name of the architecture (ADL) to compile.
   * @param execName the name of the executable file to be created.
   * @param additionalParams additional parameters for ADL build. Used by
   *          merge tests.
   * @return the list of objects that are the results of the compilation.
   * @throws ADLException if the compilation fails.
   */
  public List<Object> compile(final String adlName, final String execName,
      final Map<Object, Object> additionalParams)
      throws ADLException {
    final List<Object> result = new ArrayList<Object>();
    compile(adlName, execName, result, additionalParams);
    return result;
  }

  /**
   * Compiles the list of adl files that are received from the command-line.
   * 
   * @return the list of objects that are the results of the compilation of the
   *         list of adl files received from the command-line.
   * @throws ADLException if the compilation fails.
   * @throws InvalidCommandLineException
   */
  public List<Object> compile() throws ADLException,
      InvalidCommandLineException {
    // Check if at least 1 adlName is specified
    if (adlToExecName.size() == 0) {
      throw new InvalidCommandLineException("no definition name is specified.",
          1);
    }

    final List<Object> result = new ArrayList<Object>();
    final Map<Object, Object> additionalParams = new HashMap<Object, Object>();
    for (final Map.Entry<String, String> e : adlToExecName.entrySet()) {
      compile(e.getKey(), e.getValue(), result, additionalParams);
    }
    return result;
  }

  private void compile(String adlName, final String execName,
      final List<Object> result, Map<Object, Object> additionalParams) throws ADLException {
    final HashMap<Object, Object> contextMap = new HashMap<Object, Object>(
        compilerContext);
    if (execName != null)
      contextMap.put(EXEC_NAME, execName);
    else
      contextMap.put(EXEC_NAME, adlName.replace('.', '_'));
    
    if (additionalParams != null)
      contextMap.putAll(additionalParams);

    initBuildDirs(contextMap);

    adlName = processContext(targetDescriptor, adlName, contextMap);

    if (checkADLMode) {
      contextMap.put("check-adl", Boolean.TRUE);
    }

    final Object compilationResult = this.compiler.newComponent(adlName,
        contextMap);

    if (compilationResult instanceof File) {
      result.add(compilationResult);
    } else if (compilationResult instanceof Collection) {
      for (final Object elem : (Collection<?>) compilationResult) {
        result.add(elem);
      }
    }
  }

  @Override
  protected void printUsage(final PrintStream ps) {
    final String prgName = System.getProperty(PROGRAM_NAME_PROPERTY_NAME,
        getClass().getName());
    ps.println("Usage: " + prgName + " [OPTIONS] (<definition>[:<execname>])+");
    ps.println("  where <definition> is the name of the component to"
        + " be compiled, ");
    ps
        .println("  and <execname> is the name of the output file to be created.");
  }

  protected void handleException(final InvalidCommandLineException e)
      throws Exception {
    logger.log(Level.FINER, "Caught an InvalidCommandLineException", e);
    if (printStackTrace) {
      e.printStackTrace();
    } else {
      System.err.println(e.getMessage());
      printHelp(System.err);
      System.exit(e.exitValue);
    }
  }

  protected void handleException(final CompilerInstantiationException e)
      throws Exception {
    logger.log(Level.FINER, "Caught a CompilerInstantiationException", e);
    e.printStackTrace();
    System.exit(e.exitValue);
  }

  protected void handleException(final ADLException e) throws Exception {
    logger.log(Level.FINER, "Caught an ADL Exception", e);
    if (printStackTrace) {
      e.printStackTrace();
    } else {
      final StringBuffer sb = new StringBuffer();
      sb.append(e.getMessage()).append('\n');
      Throwable cause = e.getCause();
      while (cause != null) {
        sb.append("caused by : ");
        sb.append(cause.getMessage()).append('\n');
        cause = cause.getCause();
      }
      System.err.println(sb);
    }
    System.exit(1);
  }

  /**
   * Entry point.
   * 
   * @param args
   */
  public static void main(final String... args) {
    try {
      new Launcher(args);
    } catch (final Exception e) {
      // never append
      e.printStackTrace();
    }
  }

  /**
   * Exception thrown when the CeciliaADL compiler can't be instantiated.
   */
  public static class CompilerInstantiationException extends Exception {

    final int exitValue;

    /**
     * @param message detail message.
     * @param cause cause.
     * @param exitValue exit value.
     */
    public CompilerInstantiationException(final String message,
        final Throwable cause, final int exitValue) {
      super(message, cause);
      this.exitValue = exitValue;
    }

  }
}
