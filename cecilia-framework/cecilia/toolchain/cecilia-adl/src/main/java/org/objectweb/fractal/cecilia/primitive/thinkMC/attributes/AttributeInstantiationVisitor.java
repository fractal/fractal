/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 * Copyright (C) 2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 *
 * Contributors: Matthieu Leclercq, Lionel Debroux
 */

package org.objectweb.fractal.cecilia.primitive.thinkMC.attributes;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.adl.attributes.Attribute;
import org.objectweb.fractal.adl.attributes.Attributes;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractInstantiationTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.attributes.AbstractAttributeVisitor;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.idl.ast.ArrayOf;
import org.objectweb.fractal.cecilia.adl.idl.ast.Field;
import org.objectweb.fractal.cecilia.adl.idl.ast.FieldContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.Type;
import org.objectweb.fractal.cecilia.adl.idl.ast.ComplexType;
import org.objectweb.fractal.cecilia.adl.idl.ast.PointerOf;
import org.objectweb.fractal.cecilia.adl.idl.ast.PrimitiveType;
import org.objectweb.fractal.cecilia.adl.idl.util.CUtil;
import org.objectweb.fractal.cecilia.adl.idl.util.Util;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the instantiation of the attribute data
 * structures of the component for <code>ThinkMC</code> dialect.
 */
// TODO back port r3677:4017 from AttributeInstanceBuilder.java
public class AttributeInstantiationVisitor extends AbstractAttributeVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractAttributeVisitor
  // ---------------------------------------------------------------------------

  @Override
  protected Component createTask(final ComponentContainer container,
      final Attributes attributes) throws TaskException {
    final Map<String, Type> fieldTypes = new HashMap<String, Type>();
    for (final Field field : ((FieldContainer) ((IDLDefinitionContainer) attributes)
        .getIDLDefinition()).getFields()) {
      fieldTypes.put(field.getName(), Util.getContainedType(field));
    }
    return taskFactoryItf.newPrimitiveTask(new AttributeDefinitionTask(
        attributes.getAttributes(), fieldTypes), container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds definition source code for attribute data structure for the given
   * component node. This task provides the produced source code.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "source-code-provider", signature = SourceCodeProvider.class, record = "role:attributeInstantiation, id:%", parameters = "componentNode"))
  public static class AttributeDefinitionTask extends AbstractInstantiationTask {

    protected final Map<String, Type> fieldTypes;
    // the attributes array
    protected final Attribute[]       fields;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param fields the array of fields of the attribute data structure of the
     *            component.
     * @param fieldTypes the attribute types.
     */
    public AttributeDefinitionTask(final Attribute[] fields,
        final Map<String, Type> fieldTypes) {
      this.fields = fields;
      this.fieldTypes = fieldTypes;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractInstantiationTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter("Attribute Instantiation Builder");
      cw.append("{");
      for (final Attribute field : fields) {
        final Type attType = fieldTypes.get(field.getName());
        final String attTypeStr = CUtil.buildDeclarationType(attType);
        final String valSym = field.getValue();

        // Typecast.
        cw.append('(').append(attTypeStr).append(") ");

        // The value was created by AttributeLoader.
        cw.append(valSym).appendln(", ");
      }
      cw.append("}");
      return cw.toString();
    }
  }
}
