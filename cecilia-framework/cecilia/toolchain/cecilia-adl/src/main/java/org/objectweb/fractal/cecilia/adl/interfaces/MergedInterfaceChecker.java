/***
 * Cecilia ADL Compiler
 * Copyright (C) 2005-2008 STMicroelectronics
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.interfaces;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.ast.ASTFractalUtil;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.cecilia.adl.directives.Include;

/**
 * @author Alessio Pace
 */
public class MergedInterfaceChecker extends InterfaceChecker {

  /*
   * (non-Javadoc)
   *
   * @see org.objectweb.fractal.cecilia.adl.interfaces.InterfaceChecker#checkNode(java.lang.Object,
   *      java.util.Map)
   */
  @Override
  protected void checkNode(final Object node, final Map<Object, Object> context)
      throws ADLException {

    super.checkNode(node, context);

    if (node instanceof ImplementationContainer) {
      final Implementation impl = ASTFractalUtil
          .getFcContentDesc((ComponentContainer) node);

      /* null Implementation in case of a composite component */
      if (impl != null) {
        final Include[] includes = ASTFractalUtil.getIncludes(impl);
        /*
         * iterates over interfaces also because they may specify interfaces in
         * the case of merged primitives
         */
        for (final Include i : includes) {
          if (i instanceof InterfaceContainer) {
            checkNode(i, context);
          }
        }
      }
    }
  }
}
