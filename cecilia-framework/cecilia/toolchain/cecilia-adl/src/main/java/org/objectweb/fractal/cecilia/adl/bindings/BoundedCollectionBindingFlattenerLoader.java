/***
 * Cecilia ADL Compiler
 * Copyright (C) 2009 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Lionel Debroux
 */

package org.objectweb.fractal.cecilia.adl.bindings;

import static org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil.getFlattenedCollectionName;
import static org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil.getFlattenedCollectionCardinality;

import java.util.Map;

import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.NodeUtil;
import org.objectweb.fractal.adl.bindings.Binding;
import org.objectweb.fractal.adl.bindings.BindingContainer;
import org.objectweb.fractal.adl.bindings.BindingLoader;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;

/**
 * A {@link org.objectweb.fractal.adl.Loader} to handle {@link Interface} nodes
 * that contain fake collection interfaces (convenient one-line declaration for
 * n..m cardinality collection interfaces). These are flattened to m singleton
 * interfaces.
 */
public class BoundedCollectionBindingFlattenerLoader extends BindingLoader {

  // --------------------------------------------------------------------------
  // Implementation of the Loader interface
  // --------------------------------------------------------------------------

  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    final Definition d = clientLoader.load(name, context);
    checkNode(d, context);
    return d;
  }

  private void checkNode(final Object node, final Map<Object, Object> context)
      throws ADLException {
    if (node instanceof BindingContainer) {
      checkBindingContainer((BindingContainer) node);
    }
    if (node instanceof ComponentContainer) {
      for (final Component comp : ((ComponentContainer) node).getComponents()) {
        checkNode(comp, context);
      }
    }
  }
  
  private void checkBindingContainer(final BindingContainer container)
      throws ADLException {
    if (container instanceof InterfaceContainer) {
      for (final Interface itf : ((InterfaceContainer)container).getInterfaces()) {
        final String flattenedCollectionName = getFlattenedCollectionName(itf);
        if (flattenedCollectionName != null) {
          // This used to be a bounded "collection" interface, it was
          // flattened by BoundedCollectionInterfaceFlattenerLoader.
          final String itfName = itf.getName();
          final Integer cardinality = getFlattenedCollectionCardinality(itf);
          final Binding [] bindings = container.getBindings();
          for (Binding b : bindings) {
            String to = b.getTo();
            String from = b.getFrom();

            if (   (to.equals("this." + flattenedCollectionName))
                || (from.equals("this." + flattenedCollectionName))
               ) {
              // Found a binding that corresponds to the original bounded
              // "collection" interface. Flatten it.
              for (int i = 1; i <= cardinality; i++) {
                Node newBinding = NodeUtil.cloneNode(b);
                ((Binding)newBinding).setTo(b.getTo().
                    replaceAll("\\." + flattenedCollectionName + "$",
                        "." + flattenedCollectionName + i));
                ((Binding)newBinding).setFrom(b.getFrom().
                    replaceAll("\\." + flattenedCollectionName + "$",
                        "." + flattenedCollectionName + i));
                container.addBinding((Binding)newBinding);
              }

              container.removeBinding(b);
            }
          }
        }
      }
    }
  }
}
