/***
 * Cecilia ADL Compiler
 * Copyright (C) 2005-2008 STMicroelectronics
 * Copyright (C) 2007-2009 INRIA SARDES
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 * Contributor: Lionel Debroux (attributes merging)
 */

package org.objectweb.fractal.cecilia.adl.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.NodeUtil;
import org.objectweb.fractal.adl.ast.ASTFractalUtil;
import org.objectweb.fractal.adl.attributes.Attribute;
import org.objectweb.fractal.adl.attributes.Attributes;
import org.objectweb.fractal.adl.attributes.AttributesContainer;
import org.objectweb.fractal.adl.bindings.Binding;
import org.objectweb.fractal.adl.bindings.BindingContainer;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.implementations.Controller;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.adl.util.Printer;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.cecilia.adl.CeciliaADLConstants;
import org.objectweb.fractal.cecilia.adl.Checker;
import org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil;
import org.objectweb.fractal.cecilia.adl.bindings.MergedBindingResolverLoader;
import org.objectweb.fractal.cecilia.adl.components.merge.MergeUtil;
import org.objectweb.fractal.cecilia.adl.components.merge.MergedImplementation;
import org.objectweb.fractal.cecilia.adl.components.merge.MergedInclude;
import org.objectweb.fractal.cecilia.adl.components.merge.MergedInterface;
import org.objectweb.fractal.cecilia.adl.components.merge.MergedInterfaceRole;
import org.objectweb.fractal.cecilia.adl.controllers.ExtendedController;
import org.objectweb.fractal.cecilia.adl.directives.Include;
import org.objectweb.fractal.cecilia.adl.idl.ast.Field;
import org.objectweb.fractal.cecilia.adl.idl.ast.FieldContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.RecordDefinition;
import org.objectweb.fractal.cecilia.adl.implementations.ExtendedImplementation;
import org.objectweb.fractal.cecilia.adl.implementations.LanguageLoader;
import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;
import org.xml.sax.SAXException;

/**
 * This {@link Loader} component recursively merges components into (possibly) a
 * single one.
 * <p>
 * <strong>ALGORITHM</strong>: TODO
 * </p>
 * <p>
 * <strong>ISSUES</strong>: TODO
 * </p>
 * 
 * @author Alessio Pace
 */
public class MergeComponentsLoader extends AbstractLoader implements Loader {

  /**
   * Decoration to be set on a <em>merged primitive</em> component, whose
   * value is the AST {@link ComponentContainer} node <strong>before</strong>
   * its merge.
   */
  public static final String PREVIOUS_COMPOSITE_DECORATION    = "PREVIOUS_COMPOSITE_DECORATION";

  /**
   * Checker client interface name.
   */
  public static final String CHECKER_ITF_NAME                 = "checker";

  /**
   * AST checker client interface to post process a merged Definition AST.
   */
  protected Checker          checker;

  /**
   * The name of the node-factory client interface.
   */
  public static final String NODE_FACTORY_ITF_NAME            = "node-factory";

  /** The {@link XMLNodeFactory} client interface used by this component. */
  protected XMLNodeFactory      nodeFactoryItf;

  /**
   * The merged primitive component {@link Controller#getDescriptor()}.
   */
  public static final String MERGED_PRIMITIVE_CONTROLLER_DESC = "mergedPrimitive";

  /**
   * The merged primitive component content file.
   */
  public static final String FRACTAL_LIB_MERGED_PRIMITIVE     = "fractal.lib.mergedPrimitive";

  private Definition         topLevelDefinition;

  /**
   * An array of strings corresponding to the implementation languages
   * which cannot handle merge optimization.
   */
  private static final String[] mergeOptimizationUnawareLanguages = {"thinkMC"};

  /**
   * @return a possibly merged {@link Definition} AST node.
   */
  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {

    /* delegate to client loader */
    final Definition definition = clientLoader.load(name, context);

    /* set the top level Definition field */
    this.topLevelDefinition = definition;

    /* merge recursively, if not disabled */
    ComponentContainer result = (ComponentContainer)definition;
    final String disableMerge = System.getProperty(
        MergeUtil.CECILIA_DISABLE_MERGE_OPTIMIZATION, "false");
    if (!disableMerge.equals("true")) {
       result = mergeDefinition(
          (MergeableComponent) definition, context, "false", 0,
          ASTFractalUtil.getFcName(result));
    }

    checker.check((Definition) result, context);

    return (Definition) result;
  }

  /**
   * Travels down the AST and <em>recursively</em> merges components where
   * specified by <code>merge="true"</code>.
   *
   * @param currentComponent
   * @throws ADLException
   */
  protected ComponentContainer mergeDefinition(
      final MergeableComponent currentComponent,
      final Map<Object, Object> context, final String parentMergeAttr,
      final int currentDepthLevel, final String fullComponentName)
      throws ADLException {

    /* if it is a primitive component definition, do nothing */
    if (ASTFractalUtil.isPrimitive(currentComponent)) {
      return currentComponent;
    } else {

      /*
       * "true" if the component has to be fused with the subcomponents.
       */
      final String mergeAttrValue = isComponentToBeMerged(currentComponent,
          parentMergeAttr);

      boolean contentChanged = false;

      /*
       * ..then recursively invoke the merge algorithm on the sub components
       */
      for (final ComponentContainer subComp : ASTFractalUtil
          .getFcSubComponents(currentComponent)) {

        /*
         * invoke merge on each sub component, getting back eventually a merged
         * primitive component with the same name and the same set of client and
         * server interfaces, or the same component in case it was a regular
         * primitive.
         */
        final ComponentContainer mergedSubComponentReplacement = mergeDefinition(
            (MergeableComponent) subComp, context, mergeAttrValue,
            currentDepthLevel + 1, fullComponentName + "_" + ASTFractalUtil.getFcName(subComp));

        if (mergedSubComponentReplacement != subComp) {
          ASTFractalUtil.removeFcSubComponent(currentComponent, subComp);
          ASTFractalUtil.addFcSubComponent(currentComponent,
              mergedSubComponentReplacement);

          /*
           * mark the content has being changed so that later Checkers will be
           * re-run
           */
          contentChanged = true;
        }
      }

      /*
       * if content of the current composite component has changed, rerun
       * checkers
       */
      if (contentChanged) {
        /*
         * rerun Checkers for the goal of having the updates informations of the
         * components INSIDE this current composite. (XXX is this the good
         * positions to rerun them?)
         */
        this.checker.check(this.topLevelDefinition, context);
      }

      if (!mergeAttrValue.equals("true")) {

        Printer
            .debug("The composite "
                + ASTFractalUtil.getFcName(currentComponent)
                + " has NOT being fused with its content, only its sub components (and all decorations) have been updated.");
        return currentComponent;
      } else {

        /*
         * // end if(merge != true) Else if the current composite should be
         * merged
         */
        Printer.debug("The composite "
            + ASTFractalUtil.getFcName(currentComponent)
            + " will be fused with its content");

        // ==================================================================
        /* (1) CREATE A MERGED PRIMITIVE TO REPLACE THE CURRENT COMPOSITE */
        // ==================================================================
        /*
         * create a new merged primitive Component AST Node which will replace
         * the current composite Component AST Node fusing its subcomponents. It
         * will be either a Definition or a Component node depending on the
         * depth level.
         */
        final ComponentContainer resultMergedPrimitive = createNewComponent(currentDepthLevel);

        /* decorate it with the previous composite */
        setPreviousCompositeDecoration(resultMergedPrimitive, currentComponent);

        /*
         * set to the merged component the same name of the merged composite, in
         * order to be able to seamlessy rearrange the bindings.
         */
        ASTFractalUtil.setFcName(resultMergedPrimitive, ASTFractalUtil
            .getFcName(currentComponent));

        /*
         * add to it an Implementation node.
         */
        final ExtendedImplementation mergedPrimitiveImplementationNode = createImplementationForMergedPrimitive();
        ASTFractalUtil.setFcContentDesc(resultMergedPrimitive,
            mergedPrimitiveImplementationNode);

        /* add to it a Controller node with desc="mergedPrimitive" */
        final ExtendedController mergedPrimitiveControllerNode = createControllerForMergedPrimitive();
        ASTFractalUtil.setFcControllerDesc(resultMergedPrimitive,
            mergedPrimitiveControllerNode);

        // ==================================================================
        /* (2) CLONE THE FUNCTIONAL INTERFACES OF THE COMPOSITE */
        // ==================================================================
        /*
         * add to it functional interfaces that are equivalent to the server and
         * client functional interfaces of the composite component which is
         * going to be merged.
         */
        for (final TypeInterface ti : ASTFractalUtil
            .getFcFunctionalInterfaces(currentComponent)) {
          final TypeInterface tiCopy = cloneTypeInterface(ti);
          ((InterfaceContainer) resultMergedPrimitive).addInterface(tiCopy);
        }

        // ==================================================================
        /*
         * (3) Move the subcomponents' attributes to to the merged primitive.
         * XXX TODO LCC, constructor & destructor should be handled, too.
         */
        // ==================================================================
        /* get the subcomponents' attributes */
        Map<ComponentContainer, Attributes> subCompsAttrs = new HashMap<ComponentContainer, Attributes>();
        for (final ComponentContainer subComp : ASTFractalUtil
            .getFcSubComponents(currentComponent)) {
          if (subComp instanceof AttributesContainer) {
            Attributes attrs = ((AttributesContainer)subComp).getAttributes();
            if (attrs != null) {
              subCompsAttrs.put(subComp, attrs);
              // Kill the subcomponent's attributes, otherwise deep merging won't
              // work properly.
              ((AttributesContainer)subComp).setAttributes(null);
            }
          }
        }

        RecordDefinition newRecordDefinition = null;
        Attributes newAttrs = null;

        /* create the merged attribute definition: sub-components */
        if (!subCompsAttrs.isEmpty()) {

          newAttrs = createAttributes();
          newAttrs.setSignature("___" + fullComponentName + "_attrs");
          newRecordDefinition = createRecordDefinition();
          newRecordDefinition.setName("___" + fullComponentName + "_attrs");
          ((IDLDefinitionContainer)newAttrs).setIDLDefinition(newRecordDefinition);

          /*
           * copy all attributes / record definition fields into the merged structures,
           * while modifying names to avoid clashes
           */
          for (ComponentContainer subComp : subCompsAttrs.keySet()) {
            Attributes attrs = subCompsAttrs.get(subComp);
            final String compName = ASTFractalUtil.getFcName(subComp);
            for (Attribute attr : attrs.getAttributes()) {
              newAttrs.addAttribute(attr);
            }
          
            for (final Field field : ((FieldContainer) ((IDLDefinitionContainer) attrs)
                .getIDLDefinition()).getFields()) {
              ((FieldContainer) newRecordDefinition).addField(field);
            }
          }
        }

        /*
         * create the merged attribute definition: merge attributes of the current
         * component and of the sub-components, if necessary
         */
        Attributes attrs = ((AttributesContainer)currentComponent).getAttributes();
        if (attrs != null) {
          if (newAttrs != null) {
            for (Attribute attr : attrs.getAttributes()) {
              newAttrs.addAttribute(attr);
            }

            for (final Field field : ((FieldContainer) ((IDLDefinitionContainer) attrs)
                .getIDLDefinition()).getFields()) {
              ((FieldContainer) newRecordDefinition).addField(field);
            }
          }
          else {
            newAttrs = attrs;
          }
        }
        ((AttributesContainer)resultMergedPrimitive).setAttributes(newAttrs);
        
        if (resultMergedPrimitive instanceof AttributesContainer) {
          attrs = ((AttributesContainer)resultMergedPrimitive).getAttributes();
          if (attrs != null) {
            for (Attribute attr : attrs.getAttributes()) {
            }
          }
        }
        
        // ==================================================================
        /*
         * (4) CREATE INCLUDES FROM THE PREVIOUS SUB COMPONENTS(either regular
         * or already merged primitives) AND PUT THEM IN THE MERGED PRIMITIVE.
         */
        // ==================================================================
        for (final ComponentContainer subComp : ASTFractalUtil
            .getFcSubComponents(currentComponent)) {

          final ExtendedImplementation subCompImplementationNode = (ExtendedImplementation) ASTFractalUtil
              .getFcContentDesc(subComp);
          
          for (String implLanguage : mergeOptimizationUnawareLanguages) {
            if (subCompImplementationNode.getLanguage().equals(implLanguage)) {
              throw new UnsupportedOperationException(
                  "Merge optimization cannot work for components implemented in "
                  + implLanguage + " language");
            }
          }

          Printer.debug("Including sub-component " + subComp);

          /*
           * If it is a regular primitive, create an Include which represents
           * the content of this sub component Implementation
           */
          if (!MergeUtil.isMergedPrimitive(subComp)) {

            if (ASTFractalUtil.getIncludes(subCompImplementationNode).length > 0) {
              throw new UnsupportedOperationException(
                  "Merging a regular primitive which has itself <include> nodes is not supported yet");
            }

            /* create the merged include from the regular primitive */
            final MergedInclude mergedIncludeFromRegularPrimitive = createMergedIncludeFromRegularPrimitive(subComp);

            /* set the id for this Include */
            mergedIncludeFromRegularPrimitive
                .setId(generateIdForIncludeTakenFromRegularPrimitive(subComp));

            /*
             * add this new Include inside the resulting mergedPrimitive
             * Implementation node
             */
            ASTFractalUtil.addInclude(mergedPrimitiveImplementationNode,
                mergedIncludeFromRegularPrimitive);
          } else {
            /*
             * else if it is an already merged primitive, we have to retrieve
             * from it the Include nodes, clone them and put them inside the
             * merged primitive Implementation node.
             */
            final Include[] subCompFusedIncludes = ASTFractalUtil
                .getIncludes(subCompImplementationNode);
            for (final Include include : subCompFusedIncludes) {
              final MergedInclude currentMergedInclude = (MergedInclude) include;

              /* clone the current merged include */
              final MergedInclude clonedMergedInclude = cloneMergedInclude(currentMergedInclude);

              /* set the id for this Include */
              clonedMergedInclude
                  .setId(generateIdForIncludeTakenFromMergedPrimitive(subComp,
                      include));

              /*
               * add the include inside the Implementation AST node of the
               * merged primitive Component result
               */
              mergedPrimitiveImplementationNode.addInclude(clonedMergedInclude);
            }
          }
        }

        // ==================================================================
        /*
         * (5) Add Binding nodes to match the previous content of the composite
         * and of the internal bindings of its enclosed already merged
         * primitives (if there are)
         */
        // ==================================================================

        /* the previously declared bindings of the original composite */
        for (final Binding binding : ASTFractalUtil
            .getBindings(currentComponent)) {

          final List<String> allFroms = new ArrayList<String>();

          final TypeInterface clientItf = (TypeInterface) BindingDecorationUtil
              .getClientInterface(binding);
          if (clientItf == null) {
            throw new IllegalStateException(
                "The binding has no client interface decoration");
          }

          /*
           * inside the composite all the possible interfaces owners are
           * castable to ComponentContainer
           */
          final ComponentContainer clientItfOwner = (ComponentContainer) InterfaceDecorationUtil
              .getContainer(clientItf);
          if (clientItfOwner == null) {
            throw new IllegalStateException(
                "The client itf has no owner decoration");
          }

          /* if the owner is the composite */
          if (clientItfOwner == currentComponent) {
            // newInnerBinding.setFrom("this." + clientItf.getName());
            allFroms.add("this." + clientItf.getName());
          } else if (MergeUtil.isMergedPrimitive(clientItfOwner)) {
            Printer
                .debug("NOTE there could be more than one Include inside the merged which is inner bound to");
            /*
             * NOTE there could be more than one Include inside the merged
             * primitive which is internally bound to the client interface of
             * the merged primitive
             */
            final MergedImplementation client = (MergedImplementation) ASTFractalUtil
                .getFcContentDesc(clientItfOwner);

            final Include[] includes = ASTFractalUtil.getIncludes(client);
            for (final Include include : includes) {
              final MergedInclude mergedInclude = (MergedInclude) include;
              for (final Interface itf : mergedInclude.getInterfaces()) {
                final MergedInterface mergedInterface = (MergedInterface) itf;
                if (mergedInterface.getRole().equals(TypeInterface.CLIENT_ROLE)) {
                  if (MergedBindingResolverLoader
                      .getInnerBoundTo(mergedInterface) == clientItf) {

                    /* add this "from" in the list of bindings to add */
                    allFroms.add(generateIdForIncludeTakenFromMergedPrimitive(
                        clientItfOwner, mergedInclude)
                        + "." + mergedInterface.getName());
                  }
                }
              }
            }

          } else if (ASTFractalUtil.isPrimitive(clientItfOwner)) {
            /*
             *
             */
            allFroms
                .add(generateIdForIncludeTakenFromRegularPrimitive(clientItfOwner)
                    + "." + clientItf.getName());
          } else {
            throw new IllegalStateException("Unexpected case!");
          }

          String to = null;

          // --------------------------
          // same for server itf
          final TypeInterface serverItf = (TypeInterface) BindingDecorationUtil
              .getServerInterface(binding);
          if (serverItf == null) {
            throw new IllegalStateException(
                "The binding has no server interface decoration");
          }

          final ComponentContainer serverItfOwner = (ComponentContainer) InterfaceDecorationUtil
              .getContainer(serverItf);
          if (serverItfOwner == null) {
            throw new IllegalStateException(
                "The server itf has no owner decoration");
          }

          if (serverItfOwner == currentComponent) {
            // newInnerBinding.setTo("this." + serverItf.getName());
            to = "this." + serverItf.getName();
          } else if (MergeUtil.isMergedPrimitive(serverItfOwner)) {
            final TypeInterface realServerItf = MergedBindingResolverLoader
                .getInnerBoundTo(serverItf);
            if (realServerItf == null) {
              throw new IllegalStateException("The interface '"
                  + serverItf.getName() + "' on the merged primitive '"
                  + ASTFractalUtil.getFcName(serverItfOwner)
                  + "'has no inner bound to decoration");
            }
            final MergedInclude realServerItfOwner = (MergedInclude) InterfaceDecorationUtil
                .getContainer(realServerItf);
            if (realServerItfOwner == null) {
              throw new IllegalStateException("The actual server itf '"
                  + realServerItf.getName()
                  + "' has not been decorated with an owner");
            }
            to = generateIdForIncludeTakenFromMergedPrimitive(serverItfOwner,
                realServerItfOwner)
                + "." + realServerItf.getName();

          } else if (ASTFractalUtil.isPrimitive(serverItfOwner)) {
            /*
             * because the include corresponding to a regular primitive has as
             * idenfier the name that it had the regular primitive before the
             * fusion
             */
            to = generateIdForIncludeTakenFromRegularPrimitive(serverItfOwner)
                + "." + serverItf.getName();
          } else {
            throw new IllegalStateException("Unexpected case!");
          }

          for (final String from : allFroms) {

            /*
             * create and add the inner binding which will correspond to the
             * previous binding
             */
            final Binding newInnerBinding = createBinding();

            newInnerBinding.setFrom(from);
            newInnerBinding.setTo(to);

            /* add the Binding to the merged primitive Component AST node */
            ((BindingContainer) resultMergedPrimitive)
                .addBinding(newInnerBinding);

            Printer.debug("Added <binding client='" + from + "' server='" + to
                + "' />");
          }
        }

        // ==================================================================
        /*
         * (6) define the <binding> also from include to include in each sub
         * component which is a merged primitive
         */
        // ==================================================================
        for (final ComponentContainer subComp : ASTFractalUtil
            .getFcSubComponents(currentComponent)) {

          if (MergeUtil.isMergedPrimitive(subComp)) {
            final Include[] includes = ASTFractalUtil
                .getIncludes(ASTFractalUtil.getFcContentDesc(subComp));
            for (final Include include : includes) {
              final MergedInclude mergedInclude = (MergedInclude) include;
              for (final Interface itf : mergedInclude.getInterfaces()) {
                final MergedInterface mergedInterface = (MergedInterface) itf;
                if (mergedInterface.getRole().equals(TypeInterface.CLIENT_ROLE)) {
                  if (MergedBindingResolverLoader
                      .getMergedInterfaceRole(mergedInterface) == MergedInterfaceRole.CLIENT_INNER) {
                    final TypeInterface innerBoundTo = MergedBindingResolverLoader
                        .getInnerBoundTo(mergedInterface);
                    final Binding newClientInnerBinding = createBinding();
                    newClientInnerBinding
                        .setFrom(generateIdForIncludeTakenFromMergedPrimitive(
                            subComp, (Include) InterfaceDecorationUtil
                                .getContainer(mergedInterface))
                            + "." + mergedInterface.getName());
                    newClientInnerBinding
                        .setTo(generateIdForIncludeTakenFromMergedPrimitive(
                            subComp, (Include) InterfaceDecorationUtil
                                .getContainer(innerBoundTo))
                            + "." + innerBoundTo.getName());

                    /*
                     * don't forget to add the Binding in the merged primitive
                     * Component AST node
                     */
                    ((BindingContainer) resultMergedPrimitive)
                        .addBinding(newClientInnerBinding);
                  }
                }
              }
            }
          }
        }

        return resultMergedPrimitive;
      } // end (merge==true)
    }
  }

  /**
   * @param subComp
   * @return
   */
  private String generateIdForIncludeTakenFromRegularPrimitive(
      final ComponentContainer subComp) {
    return ASTFractalUtil.getFcName(subComp);
  }

  /**
   * @param previousMergedPrimitive
   * @param previousInclude
   * @return
   */
  private String generateIdForIncludeTakenFromMergedPrimitive(
      final ComponentContainer previousMergedPrimitive,
      final Include previousInclude) {
    return ASTFractalUtil.getFcName(previousMergedPrimitive) + "__"
        + ((MergedInclude) previousInclude).getId();
  }

  protected Attributes createAttributes() {
    try {
      Attributes newAttrs = (Attributes)nodeFactoryItf.newXMLNode(
          CeciliaADLConstants.CECILIA_ADL_DTD,
          CeciliaADLConstants.ATTRIBUTES_AST_NODE_NAME);
      return newAttrs;
    } catch (final SAXException e) {
      e.printStackTrace();
      throw new RuntimeException("Unable to create node '"
          + CeciliaADLConstants.ATTRIBUTES_AST_NODE_NAME + "' using DTD "
          + CeciliaADLConstants.CECILIA_ADL_DTD);
    }
  }

  protected Binding createBinding() {
    try {
      Binding newBinding = (Binding)nodeFactoryItf.newXMLNode(
          CeciliaADLConstants.CECILIA_ADL_DTD,
          CeciliaADLConstants.BINDING_AST_NODE_NAME);
      BindingDecorationUtil.setInnerBinding(newBinding, true);
      // XXX: declare inner bindings as dynamic="false" ?
      return newBinding;
    } catch (final SAXException e) {
      e.printStackTrace();
      throw new RuntimeException("Unable to create node '"
          + CeciliaADLConstants.BINDING_AST_NODE_NAME + "' using DTD "
          + CeciliaADLConstants.CECILIA_ADL_DTD);
    }
  }

  protected RecordDefinition createRecordDefinition() {
    try {
      RecordDefinition newRecordDefinition = (RecordDefinition)nodeFactoryItf
          .newXMLNode("classpath://org/objectweb/fractal/cecilia/adl/idl/ast/cecilia-idl.dtd",
          "recordDefinition");
      return newRecordDefinition;
    } catch (final SAXException e) {
      e.printStackTrace();
      throw new RuntimeException("Unable to create node '"
          + "recordDefinition' using DTD "
          + CeciliaADLConstants.CECILIA_ADL_DTD);
    }
  }

  /**
   * @param originalInclude
   */
  protected MergedInclude cloneMergedInclude(final MergedInclude originalInclude) {

    final MergedInclude result = createNewMergedInclude();

    /* set the same file */
    result.setFile(originalInclude.getFile());

    /* clone the orignal interfaces */
    for (final Interface originalItf : originalInclude.getInterfaces()) {
      final Interface clonedItf = cloneTypeInterface((TypeInterface) originalItf);
      result.addInterface(clonedItf);
    }

    return result;
  }

  /**
   * @param subComp
   * @return
   */
  protected MergedInclude createMergedIncludeFromRegularPrimitive(
      final ComponentContainer subComp) {

    if (subComp == null) {
      throw new IllegalArgumentException("Component argument is null");
    }

    if (!ASTFractalUtil.isPrimitive(subComp)) {
      throw new IllegalArgumentException(
          "The Component argument can't be a composite component");
    }

    if (MergeUtil.isMergedPrimitive(subComp)) {
      throw new IllegalArgumentException(
          "Component argument should not be an already merged primitive");
    }

    try {
      final MergedInclude result = (MergedInclude) nodeFactoryItf.newXMLNode(
          CeciliaADLConstants.CECILIA_ADL_DTD,
          CeciliaADLConstants.INCLUDE_AST_NODE_NAME);

      /* XXX assign as id the same of the regular primitive it is replacing */
      result.setId(generateIdForIncludeTakenFromRegularPrimitive(subComp));

      /* assign as file the same class of the original primitive */
      result.setFile(ASTFractalUtil.getFcContentDesc(subComp).getClassName());

      for (final Interface itf : ASTFractalUtil
          .getFcFunctionalInterfaces(subComp)) {
        final TypeInterface clonedItf = cloneTypeInterface((TypeInterface) itf);
        result.addInterface(clonedItf);
      }
      
      return result;
    } catch (final Exception e) {
      throw new RuntimeException("Failure while creating AST node for "
          + CeciliaADLConstants.INCLUDE_AST_NODE_NAME);
    }
  }

  /**
   * Decorate the merged primitive AST Node with the former composite component
   * AST Node.
   *
   * @param mergedPrimitive
   * @param previousComposite
   */
  protected void setPreviousCompositeDecoration(
      final ComponentContainer mergedPrimitive,
      final ComponentContainer previousComposite) {

    if (mergedPrimitive == null) {
      throw new IllegalArgumentException(
          "MergedPrimitive component argument can't be null");
    }

    /* actually set the decoration on the merged primitive Node */
    ((Node) mergedPrimitive).astSetDecoration(PREVIOUS_COMPOSITE_DECORATION,
        previousComposite);
  }

  /**
   * Return this composite component merge attribute value, or the parent merge
   * attribute value if it does not specify one.
   *
   * @param component
   * @param parentMergeAttr
   * @return
   */
  protected String isComponentToBeMerged(final MergeableComponent component,
      final String parentMergeAttr) {
    /*
     * the value of the <code>merge</code> as it is declared explicitly on the
     * ADL component.
     */
    String mergeAttrValue = component.getMerge();

    /*
     * if this composite component does not specify a merge attribute, then
     * inherit the value specified on its parent.
     */
    if (mergeAttrValue == null) {
      mergeAttrValue = parentMergeAttr;
    }
    return mergeAttrValue;
  }

  /**
   * @param itf
   * @return a {@link TypeInterface} AST node which has the same
   *         <code>name</code>, <code>cardinality</code>,
   *         <code>contingency</code>, <code>role</code>,
   *         <code>signature</code> and enclosed {@link IDLDefinition} node.
   */
  protected TypeInterface cloneTypeInterface(final TypeInterface itf) {

    TypeInterface result = null;

    try {
      result = (TypeInterface) nodeFactoryItf.newXMLNode(
          CeciliaADLConstants.CECILIA_ADL_DTD,
          CeciliaADLConstants.INTERFACE_AST_NODE_NAME);

      final String cardinality = itf.getCardinality();
      if (cardinality != null && cardinality.equals(TypeInterface.COLLECTION_CARDINALITY)) {
        throw new UnsupportedOperationException(
            "Unsupported merge of a composite which has a client interface with cardinality 'collection'."
            + "Consider using an interface with bounded cardinality.");
      }
      result.setCardinality(cardinality);
      result.setContingency(itf.getContingency());
      result.setName(itf.getName());
      result.setRole(itf.getRole());
      result.setSignature(itf.getSignature());

      /* brand new Map decoration */
      ((Node) result).astSetDecorations(new HashMap<String, Object>());

      /*
       * this interface contains a previously loaded IDLDefinition, so get and
       * add it from the one to clone
       */
      ((IDLDefinitionContainer) result)
          .setIDLDefinition(((IDLDefinitionContainer) itf).getIDLDefinition());

      return result;
    } catch (final Exception e) {
      e.printStackTrace();
      throw new RuntimeException("Unable to create node '"
          + CeciliaADLConstants.INTERFACE_AST_NODE_NAME + "' using DTD "
          + CeciliaADLConstants.CECILIA_ADL_DTD);
    }
  }

  /**
   * @return an {@link Include} AST node.
   * @throws ADLException
   */
  protected MergedInclude createNewMergedInclude() {

    MergedInclude result = null;

    try {
      result = (MergedInclude) nodeFactoryItf.newXMLNode(
          CeciliaADLConstants.CECILIA_ADL_DTD,
          CeciliaADLConstants.INCLUDE_AST_NODE_NAME);
      return result;
    } catch (final Exception e) {
      e.printStackTrace();
      throw new RuntimeException("Unable to create node '"
          + CeciliaADLConstants.INCLUDE_AST_NODE_NAME + "' using DTD "
          + CeciliaADLConstants.CECILIA_ADL_DTD);
    }
  }

  /**
   * @return an {@link ExtendedController} AST node
   * @throws ADLException
   */
  protected ExtendedController createControllerForMergedPrimitive() {
    ExtendedController result = null;

    try {
      result = (ExtendedController) nodeFactoryItf.newXMLNode(
          CeciliaADLConstants.CECILIA_ADL_DTD,
          CeciliaADLConstants.CONTROLLER_AST_NODE_NAME);

      result.setDescriptor(MERGED_PRIMITIVE_CONTROLLER_DESC);
      result.setLanguage(LanguageLoader.DEFAULT_CPL);

      return result;
    } catch (final Exception e) {
      e.printStackTrace();
      throw new RuntimeException("Unable to create node '"
          + CeciliaADLConstants.CONTROLLER_AST_NODE_NAME + "' using DTD "
          + CeciliaADLConstants.CECILIA_ADL_DTD);
    }
  }

  /**
   * @param depthLevel
   * @return a brand new (and empty) component AST node, depending on the
   *         <code>depthLevel</code> it may either be a {@link Definition}
   *         (depthLevel==0) or a {@link Component} type.
   * @throws ADLException
   */
  protected ComponentContainer createNewComponent(final int depthLevel) {
    ComponentContainer result = null;
    String qualifiedName = null;
    try {
      qualifiedName = CeciliaADLConstants.DEFINITION_AST_NODE_NAME;
      if (depthLevel > 0) {
        qualifiedName = CeciliaADLConstants.COMPONENT_AST_NODE_NAME;
      }
      result = (ComponentContainer) nodeFactoryItf.newXMLNode(
          CeciliaADLConstants.CECILIA_ADL_DTD, qualifiedName);
    } catch (final Exception e) {
      e.printStackTrace();
      throw new RuntimeException("Unable to create node '" + qualifiedName
          + "' using DTD " + CeciliaADLConstants.CECILIA_ADL_DTD);
    }

    return result;
  }

  /**
   * @return an {@link ExtendedImplementation} AST node type, setting features
   *         of a merged primitive component.
   * @throws ADLException
   */
  protected ExtendedImplementation createImplementationForMergedPrimitive() {
    ExtendedImplementation result = null;

    try {
      result = (ExtendedImplementation) nodeFactoryItf.newXMLNode(
          CeciliaADLConstants.CECILIA_ADL_DTD,
          CeciliaADLConstants.IMPLEMENTATION_AST_NODE_NAME);

      result.setClassName(FRACTAL_LIB_MERGED_PRIMITIVE);
      result.setLanguage("optC");

      return result;
    } catch (final Exception e) {
      e.printStackTrace();
      throw new RuntimeException("Unable to create node '"
          + CeciliaADLConstants.IMPLEMENTATION_AST_NODE_NAME + "' using DTD "
          + CeciliaADLConstants.CECILIA_ADL_DTD);
    }
  }

  /**
   * @param mergedPrimitive the merged primitive component
   * @return the value of the decoration MERGED_DATAS_DECORATION or
   *         <code>null</code> if nothing is associated with that key.
   */
  public static ComponentContainer getPreviousCompositeDecoration(
      final MergeableComponent mergedPrimitive) {

    if (mergedPrimitive == null) {
      throw new IllegalArgumentException("Argument can't be null");
    }

    return (ComponentContainer) ((Node) mergedPrimitive)
        .astGetDecoration(PREVIOUS_COMPOSITE_DECORATION);
  }

  // ----------------------------------------------------------------------
  // BindingController methods
  // ----------------------------------------------------------------------

  @Override
  public String[] listFc() {
    final String[] superListFc = super.listFc();
    final String[] result = new String[superListFc.length + 2];
    System.arraycopy(superListFc, 0, result, 0, superListFc.length);
    result[result.length - 2] = NODE_FACTORY_ITF_NAME;
    result[result.length - 1] = CHECKER_ITF_NAME;
    return result;
  }

  @Override
  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException {

    if (NODE_FACTORY_ITF_NAME.equals(s)) {
      this.nodeFactoryItf = (XMLNodeFactory) o;
    } else if (CHECKER_ITF_NAME.equals(s)) {
      this.checker = (Checker) o;
    } else {
      super.bindFc(s, o);
    }
  }

  @Override
  public Object lookupFc(final String s) throws NoSuchInterfaceException {
    if (NODE_FACTORY_ITF_NAME.equals(s)) {
      return this.nodeFactoryItf;
    } else if (CHECKER_ITF_NAME.equals(s)) {
      return this.checker;
    } else {
      return super.lookupFc(s);
    }
  }

  @Override
  public void unbindFc(final String s) throws IllegalBindingException,
      NoSuchInterfaceException {
    if (NODE_FACTORY_ITF_NAME.equals(s)) {
      this.nodeFactoryItf = null;
    } else if (CHECKER_ITF_NAME.equals(s)) {
      this.checker = null;
    } else {
      super.unbindFc(s);
    }
  }
}
