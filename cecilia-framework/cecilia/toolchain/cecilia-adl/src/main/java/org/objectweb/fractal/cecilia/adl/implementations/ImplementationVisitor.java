/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.implementations;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;
import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationDecorationUtil.getCode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.directives.Include;
import org.objectweb.fractal.cecilia.adl.directives.IncludeContainer;
import org.objectweb.fractal.cecilia.adl.file.SourceFile;
import org.objectweb.fractal.cecilia.adl.file.SourceFileProvider;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.TaskFactory;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that creates tasks that provides implementation source code
 * of the visited component.
 */
public class ImplementationVisitor extends AbstractTaskFactoryUser
    implements
      ComponentVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link ComponentContainer} nodes and creates a provides
   * implementation source code of the visited component. More precisely, the
   * returned task provides a {@link SourceFileProvider} interfaces and a
   * {@link SourceCodeProvider} for each implementation file. The first
   * interface provides the implementation file itself, the second one provide
   * source code that includes it.
   */
  public Component visit(final List<Node> path, final ComponentContainer node,
      final Map<Object, Object> context) throws ADLException, TaskException {
    final Implementation impl = castNodeError(node,
        ImplementationContainer.class).getImplementation();
    if (impl == null) {
      throw new CompilerError(GenericErrors.INTERNAL_ERROR,
          "This visitor is only applicable for primitive component.");
    }
    final Component implTask = taskFactoryItf.newPrimitiveTask(
        new ImplementationTask((SourceFile) getCode(impl)), node);
    if (impl instanceof IncludeContainer
        && ((IncludeContainer) impl).getIncludes().length != 0) {
      final Include[] includes = ((IncludeContainer) impl).getIncludes();

      final Collection<Component> tasks = new ArrayList<Component>(
          includes.length + 1);
      tasks.add(implTask);

      for (final Include include : includes) {
        tasks.add(taskFactoryItf.newPrimitiveTask(new ModuleImplementationTask(
            ((SourceFile) getCode((Node) include))), node, include));
      }
      return taskFactoryItf.newCompositeTask(tasks, TaskFactory.EXPORT_ALL,
          null);
    } else {
      return implTask;
    }

  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Provides implementation source file and a piece of code that includes it.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces({
      @ServerInterface(name = "implementation-file-provider", signature = SourceFileProvider.class, record = "role:implementationSourceFile, id:%", parameters = "componentNode"),
      @ServerInterface(name = "implementation-source-provider", signature = SourceCodeProvider.class, record = "role:implementationSourceCode, id:%", parameters = "componentNode")})
  public static class ImplementationTask
      implements
        SourceFileProvider,
        SourceCodeProvider {

    protected SourceFile implementationFile;
    protected String     implementationCode;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param implementationFile the {@link SourceFile} that contains the
     *            component implementation.
     */
    public ImplementationTask(final SourceFile implementationFile) {
      this.implementationFile = implementationFile;
      implementationCode = "#include \""
          + implementationFile.getFile().getAbsolutePath() + "\"";
    }

    // -------------------------------------------------------------------------
    // Implementation of the SourceFileProvider interface
    // -------------------------------------------------------------------------

    public SourceFile getSourceFile() {
      return implementationFile;
    }

    // -------------------------------------------------------------------------
    // Implementation of the SourceCodeProvider interface
    // -------------------------------------------------------------------------

    public String getSourceCode() {
      return implementationCode;
    }
  }

  /**
   * Provides implementation module source file and a piece of code that
   * includes it.
   */
  @TaskParameters({"componentNode", "moduleNode"})
  @ServerInterfaces({
      @ServerInterface(name = "implementation-file-provider", signature = SourceFileProvider.class, record = "role:implementationSourceFile, id:%, module:%", parameters = {
          "componentNode", "moduleNode"}),
      @ServerInterface(name = "implementation-source-provider", signature = SourceCodeProvider.class, record = "role:implementationSourceCode, id:%, module:%", parameters = {
          "componentNode", "moduleNode"})})
  public class ModuleImplementationTask extends ImplementationTask {

    /**
     * @param implementationFile the {@link SourceFile} that contains the
     *            component implementation.
     */
    public ModuleImplementationTask(final SourceFile implementationFile) {
      super(implementationFile);
    }

  }
}
