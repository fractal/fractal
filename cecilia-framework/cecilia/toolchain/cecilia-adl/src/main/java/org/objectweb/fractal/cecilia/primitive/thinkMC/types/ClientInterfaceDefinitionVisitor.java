/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.primitive.thinkMC.types;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.interfaces.AbstractClientInterfaceVisitor;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterfaceForEach;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the definition of the client interfaces of the
 * component for the <code>ThinkMC</code> dialect.
 */
public class ClientInterfaceDefinitionVisitor
    extends
      AbstractClientInterfaceVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractClientInterfaceVisitor
  // ---------------------------------------------------------------------------

  /**
   * Creates a task that writes the source code containing the definition of the
   * client interfaces of the component.
   */
  @Override
  protected Component createTask(final ComponentContainer container,
      final List<TypeInterface> clientInterfaces) throws ADLException,
      TaskException {

    final List<IDLDefinition> clientInterfaceDefinitions = new ArrayList<IDLDefinition>();
    for (final Interface itf : clientInterfaces) {
      clientInterfaceDefinitions.add(castNodeError(itf,
          IDLDefinitionContainer.class).getIDLDefinition());
    }

    return taskFactoryItf.newPrimitiveTask(new ClientInterfaceDefinitionTask(
        clientInterfaces), container, clientInterfaceDefinitions);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds definition source code for the definition of the client interfaces
   * for the given component node. This task provides the produced source code.
   */
  @TaskParameters({"componentNode", "importedInterfaceDefinitions"})
  @ServerInterfaces(@ServerInterface(name = "source-code-provider", signature = SourceCodeProvider.class, record = "role:clientInterfaceDefinition, id:%", parameters = "componentNode"))
  public static class ClientInterfaceDefinitionTask
      extends
        AbstractDefinitionTask {

    protected final List<TypeInterface>                 importedItfNodes;

    // -------------------------------------------------------------------------
    // Client interfaces
    // -------------------------------------------------------------------------

    /**
     * Source code to be included to access the interface definition header
     * files (idl.h) for each imported interface of the componentNode.
     */
    @ClientInterfaceForEach(iterable = "importedInterfaceDefinitions", prefix = "idl-header", signature = SourceCodeProvider.class, record = "role:cInterfaceDefinition, id:%", parameters = "importedInterfaceDefinitions.element")
    public final Map<TypeInterface, SourceCodeProvider> cInterfaceDefinitionItfs = new LinkedHashMap<TypeInterface, SourceCodeProvider>();

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param importedItfNodes the list of imported interface nodes belonging to
     *            the component.
     */
    public ClientInterfaceDefinitionTask(
        final List<TypeInterface> importedItfNodes) {
      this.importedItfNodes = importedItfNodes;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter(
          "Primitive Client Interface Definition Builder");
      for (final TypeInterface itf : importedItfNodes) {
        final IDLDefinition itfDef = castNodeError(itf,
            IDLDefinitionContainer.class).getIDLDefinition();
        cw.appendln(cInterfaceDefinitionItfs.get(itfDef).getSourceCode());
      }
      cw.endl();

      cw.append("struct ").append(typeNameProviderItf.getCTypeName()).append(
          "_importeds { ").endl();
      for (final TypeInterface itf : importedItfNodes) {
        cw.append("R").append(itf.getSignature().replace('.', '_'))
            .append(" *").append(itf.getName().replace('-', '_')).append(" ; ")
            .endl();
      }
      cw.append("};").endl();

      return cw.toString();
    }
  }
}
