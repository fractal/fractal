/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.compiler;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.util.FractalADLLogManager;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.cecilia.adl.TypeNameProvider;
import org.objectweb.fractal.cecilia.adl.file.FutureFileCollectionProvider;
import org.objectweb.fractal.cecilia.adl.file.FutureFileProvider;
import org.objectweb.fractal.cecilia.adl.file.SourceFileWriter;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

@TaskParameters("id")
@ServerInterfaces(@ServerInterface(name = "file-provider", signature = FutureFileProvider.class, record = "role:archiveFile, id:%", parameters = "id"))
public abstract class AbstractArchiveTask implements FutureFileProvider {

  /**
   * The suffix of the generated file. This suffix is used only if the output
   * file name is retrieved using the
   * {@link #typeNameProviderItf type name provider interface}.
   */
  public static final String                             FILE_NAME_SUFFIX           = ".a";

  /** The name of the linked file. */
  protected final String                                 outputName;
  /** The compilation flags. */
  protected List<String>                                       flags;
  /** The output directory. */
  protected final File                                   outputDir;

  // Produced file
  protected Future<File>                                 archiveFile;

  // The dep logger
  protected static Logger                                depLogger                  = FractalADLLogManager
                                                                                        .getLogger("dep");
  // The io logger
  protected static Logger                                ioLogger                   = FractalADLLogManager
                                                                                        .getLogger("io");

  // ---------------------------------------------------------------------------
  // Task client interfaces
  // ---------------------------------------------------------------------------

  /** The name of the component type. */
  @ClientInterface(name = "type-name-provider", contingency = TypeFactory.OPTIONAL, record = "role:typeNameProvider, id:%", parameters = "id")
  public TypeNameProvider                                typeNameProviderItf;

  /** The client collection interface used to retrieve the files to linked. */
  @ClientInterface(name = "source-file-provider", signature = FutureFileProvider.class, record = "role:compiledFile, id:%", parameters = "id")
  public final Map<String, FutureFileProvider>           fileProviderItfs           = new HashMap<String, FutureFileProvider>();

  /** The client collection interface used to retrieve the files to linked. */
  @ClientInterface(name = "source-file-collection-provider", signature = FutureFileCollectionProvider.class, record = "role:compiledFileCollection, id:%", parameters = "id")
  public final Map<String, FutureFileCollectionProvider> fileCollectionProviderItfs = new HashMap<String, FutureFileCollectionProvider>();

  // ---------------------------------------------------------------------------
  // Task constructor
  // ---------------------------------------------------------------------------

  /**
   * @param outputName the name of the linked file
   * @param outputDir the directory into which the compiled file will be placed.
   * @param flags compilation flags.
   */
  public AbstractArchiveTask(final String outputName, final File outputDir,
      final List<String> flags) {
    this.outputName = outputName;
    this.outputDir = outputDir;
    this.flags = flags;
  }

  // ---------------------------------------------------------------------------
  // Abstract methods
  // ---------------------------------------------------------------------------

  /**
   * Archive method.
   * 
   * @param outputFile the linked file that is provided by this task.
   * @param inputFiles the input files to be linked.
   * @throws the archive file. Most of the time this is the same the
   *             <code>outputFile</code> parameter but it is not necessary the
   *             case.
   * @throws ADLException if an error occurs.
   * @throws InterruptedException
   */
  protected abstract File archivate(File outputFile, Collection<File> inputFiles)
      throws ADLException, InterruptedException;

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  protected File getOutputFile() {
    final String name;
    if (outputName != null)
      name = outputName;
    else if (typeNameProviderItf != null)
      name = typeNameProviderItf.getCTypeName() + FILE_NAME_SUFFIX;
    else
      throw new IllegalStateException(
          "the output file name must either be specifyed in the contructor parameters or the typeNameProviderItf interface must be bound");

    return new File(outputDir, name);
  }

  // -------------------------------------------------------------------------
  // Implementation of the FileProvider interface
  // -------------------------------------------------------------------------

  public synchronized Future<File> getFile(final ExecutorService executorService) {
    if (archiveFile == null) {
      final Collection<Future<File>> futureFiles = new HashSet<Future<File>>();

      // get every files provided by every FileProvider
      for (final FutureFileProvider fileProvider : fileProviderItfs.values()) {
        futureFiles.add(fileProvider.getFile(executorService));
      }

      // get every files provided by every FileCollectionProvider
      for (final FutureFileCollectionProvider collectionProvider : fileCollectionProviderItfs
          .values()) {
        futureFiles.addAll(collectionProvider.getFiles(executorService));
      }

      archiveFile = executorService.submit(new Callable<File>() {

        public File call() throws Exception {
          final Collection<File> inputFiles = new ArrayList<File>(futureFiles
              .size());
          File outputFile = getOutputFile();
          final long outputTimestamp = outputFile.lastModified();
          boolean relink = false;
          if (outputTimestamp == 0) relink = true;

          for (final Future<File> futureFile : futureFiles) {
            File inputFile;
            try {
              inputFile = futureFile.get();
            } catch (final ExecutionException e) {
              if (e.getCause() instanceof ADLException)
                throw (ADLException) e.getCause();
              else
                throw e;
            }
            inputFiles.add(inputFile);
            if (!relink && inputFile.lastModified() > outputTimestamp) {
              if (depLogger.isLoggable(Level.FINE))
                depLogger.fine("Input file '" + inputFile
                    + " is more recent than file '" + outputFile
                    + "', recompile.");
              relink = true;
            }
          }
          if (!relink && depLogger.isLoggable(Level.FINE))
            depLogger.fine("Output file '" + outputFile
                + "' is up to date, do not recompile.");

          if (relink) {
            SourceFileWriter.createOutputDir(outputFile.getParentFile());
            outputFile = archivate(outputFile, inputFiles);
          }
          return outputFile;
        }
      });
    }
    return archiveFile;
  }
}
