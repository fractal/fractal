/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006 FranceTelecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: J. Polakovic
 */

package org.objectweb.fractal.cecilia.adl.properties;

/**
 * AST node interface for <code>properties</code> elements.
 */
public interface Properties {

  /**
   * Returns the <code>properties</code> sub-elements. Note: this method is
   * named <code>getPropertys</code> and not <code>getProperties</code> to
   * respect naming convension.
   * 
   * @return the <code>properties</code> sub-elements.
   */
  Property[] getPropertys();

  /**
   * Adds a <code>property</code> sub-element.
   * 
   * @param property a <code>property</code> sub-element to add.
   */
  void addProperty(Property property);

  /**
   * Removes a <code>property</code> sub-element.
   * 
   * @param property a <code>property</code> sub-element to remove.
   */
  void removeProperty(Property property);
}
