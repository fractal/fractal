/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.primitive.optC.source;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;
import static org.objectweb.fractal.cecilia.adl.directives.DirectiveHelper.getCFlags;
import static org.objectweb.fractal.cecilia.adl.directives.DirectiveHelper.getLdFlags;
import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationDecorationUtil.getCode;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.cecilia.adl.compiler.CompilationTaskFactory;
import org.objectweb.fractal.cecilia.adl.compiler.OutputFormat;
import org.objectweb.fractal.cecilia.adl.compiler.OutputFormatContainer;
import org.objectweb.fractal.cecilia.adl.directives.Include;
import org.objectweb.fractal.cecilia.adl.directives.IncludeContainer;
import org.objectweb.fractal.cecilia.adl.file.FileCollectionProvider;
import org.objectweb.fractal.cecilia.adl.file.FileProvider;
import org.objectweb.fractal.cecilia.adl.file.FutureFileCollectionProvider;
import org.objectweb.fractal.cecilia.adl.file.FutureFileProvider;
import org.objectweb.fractal.cecilia.adl.file.SourceFile;
import org.objectweb.fractal.cecilia.adl.file.SourceFileProvider;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

public class CompilationVisitor extends AbstractTaskFactoryUser
    implements
      ComponentVisitor {

  /** The name of the task composition schema used by this visitor. */
  public static final String    COMPILATION_COMPOSITION           = "org.objectweb.fractal.cecilia.primitive.optC.Compilation";

  /** The name of the {@link #compilationTaskFactoryItf} client interface. */
  public static final String    COMPILATION_TASK_FACTORY_ITF_NAME = "compilation-task-factory";

  /** The client interface used to create compilation tasks. */
  public CompilationTaskFactory compilationTaskFactoryItf;

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link ComponentContainer} nodes and creates a compilation tasks.
   */
  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    final Implementation impl = castNodeError(container,
        ImplementationContainer.class).getImplementation();
    if (impl == null) {
      throw new CompilerError(GenericErrors.INTERNAL_ERROR,
          "This visitor is only applicable for primitive component.");
    }

    // get cFlag list.
    final List<String> cFlags = new ArrayList<String>();
    cFlags.addAll(getCFlags(container));

    boolean archiveOutput = false;
    if (container instanceof OutputFormatContainer
        && ((OutputFormatContainer) container).getOutput() != null) {
      final String outputFormat = ((OutputFormatContainer) container)
          .getOutput().getFormat();
      if (OutputFormat.ARCHIVE_OUTPUT_FORMAT.equals(outputFormat))
        archiveOutput = true;
    }

    final Collection<Component> tasks = new ArrayList<Component>();

    // create the task that aggregates every compiled files
    tasks.add(createFileProviderAggregatorTask(container));

    // create the compilation task for the component source file
    tasks.add(createComponentCompilationTask(container, cFlags, context));

    if (impl instanceof IncludeContainer
        && ((IncludeContainer) impl).getIncludes().length != 0) {
      final Include[] includes = ((IncludeContainer) impl).getIncludes();

      for (final Include include : includes) {
        // if the module is an assembly, file, it should not be included.
        final SourceFile code = (SourceFile) getCode((Node) include);
        if ((code).isAssemblyFile()) {
          // the module is an assembly file, must add the SourceFileProvider
          // task for this file
          tasks.add(createAssemblyFileProvider(container, include, code));
        }
        // add the compilation task
        tasks.add(createModuleCompilationTask(include, cFlags, context));
      }
    }

    if (archiveOutput) {
      tasks.add(createArchiveTask(container, null, context));
    }

    // If the visited node is the top level node, add a link task.
    final boolean doLink = path.isEmpty();
    if (doLink) {
      final List<String> ldFlags = new ArrayList<String>();
      ldFlags.addAll(getLdFlags(container));
      tasks.add(compilationTaskFactoryItf.newLinkTask(container, null,
          ldFlags, context));
    }

    return createCompilationComposition(container, tasks, archiveOutput, doLink);
  }

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  protected Component createFileProviderAggregatorTask(
      final ComponentContainer container) throws TaskException {
    return taskFactoryItf.newPrimitiveTask(new FileProviderAggregatorTask(),
        container);
  }

  protected Component createModuleCompilationTask(final Include include,
      final List<String> cFlag, final Map<Object, Object> context)
      throws TaskException {
    return compilationTaskFactoryItf.newCompileTask(include, cFlag, context);
  }

  protected Component createComponentCompilationTask(
      final ComponentContainer container, final List<String> cFlag,
      final Map<Object, Object> context) throws TaskException {
    return compilationTaskFactoryItf.newCompileTask(container, cFlag, context);
  }

  protected Component createAssemblyFileProvider(
      final ComponentContainer container, final Include include,
      final SourceFile code) throws TaskException {
    return taskFactoryItf.newPrimitiveTask(new AssemblyFileProvider(code),
        container, include);
  }

  protected Component createArchiveTask(final ComponentContainer container,
      final List<String> cFlag, final Map<Object, Object> context)
      throws TaskException {
    return compilationTaskFactoryItf.newArchiveTask(container, null, cFlag,
        context);
  }

  protected Component createCompilationComposition(
      final ComponentContainer container, final Collection<Component> tasks,
      final boolean archiveOutput, final boolean doLink) throws TaskException {
    return taskFactoryItf.newCompositeTask(tasks, COMPILATION_COMPOSITION,
        null, container, archiveOutput, doLink);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Aggregate files provided by a set of {@link FileProvider} client into a
   * collection provided by the {@link FileCollectionProvider} server interface
   * of this task.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "component-compiled-files-provider", signature = FutureFileCollectionProvider.class, record = "role:outputFiles, id:%", parameters = "componentNode"))
  public static class FileProviderAggregatorTask
      implements
        FutureFileCollectionProvider {

    protected Collection<Future<File>>           providedFiles;

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /**
     * Client collection interface used to retrieve files provided by this task.
     */
    @ClientInterface(name = "file-providers", signature = FutureFileProvider.class, record = "role:aggregatorInputFile, id:%", parameters = "componentNode")
    public final Map<String, FutureFileProvider> clientProviderItfs = new HashMap<String, FutureFileProvider>();

    /**
     * Client interface used to retrieve compiled runtime implementations. This
     * interface is used only if the component is the top-level component.
     */
    @ClientInterface(name = "runtime-compiled-files", signature = FutureFileProvider.class, record = "role:runtimeCompiledFile, id:%", parameters = "componentNode")
    public final Map<String, FutureFileProvider>           runtimeProviderItfs           = new HashMap<String, FutureFileProvider>();

    /**
     * Client interface used to retrieve compiled runtime implementations. This
     * interface is used only if the component is the top-level component.
     */
    @ClientInterface(name = "runtime-compiled-file-collections", signature = FutureFileCollectionProvider.class, record = "role:runtimeCompiledFileCollection, id:%", parameters = "componentNode")
    public final Map<String, FutureFileCollectionProvider> runtimeCollectionProviderItfs = new HashMap<String, FutureFileCollectionProvider>();

    // -------------------------------------------------------------------------
    // Implementation of the FutureFileCollectionProvider interface
    // -------------------------------------------------------------------------

    public synchronized Collection<Future<File>> getFiles(
        final ExecutorService executorService) {
      if (providedFiles == null) {
        providedFiles = new HashSet<Future<File>>();
        for (final FutureFileProvider fileProvider : clientProviderItfs
            .values()) {
          providedFiles.add(fileProvider.getFile(executorService));
        }

        for (final FutureFileProvider fileProvider : runtimeProviderItfs
            .values()) {
          providedFiles.add(fileProvider.getFile(executorService));
        }

        for (final FutureFileCollectionProvider fileCollectionProvider : runtimeCollectionProviderItfs
            .values()) {
          providedFiles
              .addAll(fileCollectionProvider.getFiles(executorService));
        }
      }
      return providedFiles;
    }
  }

  /**
   * A {@link SourceFileProvider} task that provides {@link SourceFile} for
   * component module written in assembly code.
   */
  @TaskParameters({"componentNode", "moduleNode"})
  @ServerInterfaces(@ServerInterface(name = "assembly-file-provider", signature = SourceFileProvider.class, record = "role:componentAssemblyModuleFile, id:%, module:%", parameters = {
      "componentNode", "moduleNode"}))
  public static class AssemblyFileProvider implements SourceFileProvider {

    protected final SourceFile assemblyFile;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param assemblyFile the file provided by this task.
     */
    public AssemblyFileProvider(final SourceFile assemblyFile) {
      this.assemblyFile = assemblyFile;
    }

    // -------------------------------------------------------------------------
    // Implementation of the SourceFileProvider interface
    // -------------------------------------------------------------------------

    public SourceFile getSourceFile() {
      return assemblyFile;
    }
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  @Override
  public String[] listFc() {
    final String[] superItfs = super.listFc();
    final String[] itfs = new String[superItfs.length + 1];
    System.arraycopy(superItfs, 0, itfs, 0, superItfs.length);
    itfs[superItfs.length] = COMPILATION_TASK_FACTORY_ITF_NAME;
    return itfs;
  }

  @Override
  public void bindFc(final String clientItfName, final Object serverItf)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.equals(COMPILATION_TASK_FACTORY_ITF_NAME))
      compilationTaskFactoryItf = (CompilationTaskFactory) serverItf;
    else
      super.bindFc(clientItfName, serverItf);
  }

  @Override
  public Object lookupFc(final String clientItfName)
      throws NoSuchInterfaceException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.equals(COMPILATION_TASK_FACTORY_ITF_NAME))
      return compilationTaskFactoryItf;
    else
      return super.lookupFc(clientItfName);
  }

  @Override
  public void unbindFc(final String clientItfName)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.equals(COMPILATION_TASK_FACTORY_ITF_NAME))
      compilationTaskFactoryItf = null;
    else
      super.unbindFc(clientItfName);
  }
}
