/**
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.idl;

import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;

/**
 * An IDL interface definition can be translated to C function prototypes in two
 * fashions:
 * <ul>
 * <li><em>singleton</em> (without the <code>*_this</code> pointer)</li>
 * <li>or <em>non-singleton</em> style (with the <code>*_this</code>
 * pointer) </li>
 * </ul>. Clearly, IDL Records or Enums always have the
 * {@link IDLCompilationDesc#singleton} field set to <code>false</code>
 * because they don't have methods.
 *
 * @author Alessio Pace
 */
public final class IDLCompilationDesc {

  /**
   * The IDL interface to compile.
   */
  private final IDLDefinition idlDefinition;

  /**
   * If it has to be compiled in <em>singleton mode</em> or not.
   */
  private final boolean       singleton;

  /**
   * Default constructor passing the IDL <code>interfaceDefinition</code> to
   * compile, and the singleton field is implicitly set to <code>false</code>.
   *
   * @param idlDefinition
   */
  public IDLCompilationDesc(final IDLDefinition idlDefinition) {
    this.idlDefinition = idlDefinition;
    this.singleton = false;
  }

  /**
   * Default constructor passing the IDL <code>interfaceDefinition</code> to
   * compile and the <code>singleton</code> argument to describe how to
   * compile this IDL definition.
   *
   * @param interfaceDefinition
   * @param singleton
   */
  public IDLCompilationDesc(final IDLDefinition idlDefinition,
      final boolean singleton) {
    super();
    this.idlDefinition = idlDefinition;
    this.singleton = singleton;
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result
        + ((this.idlDefinition == null) ? 0 : this.idlDefinition.hashCode());
    result = prime * result + (this.singleton ? 1231 : 1237);
    return result;
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(final Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    final IDLCompilationDesc other = (IDLCompilationDesc) obj;
    if (this.idlDefinition == null) {
      if (other.idlDefinition != null) return false;
    } else if (!this.idlDefinition.equals(other.idlDefinition)) return false;
    if (this.singleton != other.singleton) return false;
    return true;
  }

  /**
   * @return the idlDefinition
   */
  public IDLDefinition getIdlDefinition() {
    return this.idlDefinition;
  }

  /**
   * @return the singleton
   */
  public boolean isSingleton() {
    return this.singleton;
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "[IDL: " + this.idlDefinition.getName() + "; singleton: "
        + this.singleton + "]";
  }

}
