/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.idl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.idl.ast.Field;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Import;
import org.objectweb.fractal.cecilia.adl.idl.ast.ImportContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.RecordDefinition;
import org.objectweb.fractal.cecilia.adl.idl.util.CUtil;
import org.objectweb.fractal.cecilia.adl.idl.util.Util;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.Executable;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterfaceForEach;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component for {@link RecordDefinition} nodes.
 */
public class CRecordDefinitionVisitor extends AbstractTaskFactoryUser
    implements
      IDLDefinitionVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of the IDLVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link RecordDefinition} nodes, and creates a task that writes the
   * corresponding C source code.
   */
  public Component visit(final List<Node> path,
      final IDLCompilationDesc idlCompilationDesc,
      final Map<Object, Object> context) throws ADLException, TaskException {
    IDLDefinition idlDefinition = idlCompilationDesc.getIdlDefinition();
    if (idlDefinition instanceof RecordDefinition) {

      final List<IDLDefinition> importedDefinitions = new ArrayList<IDLDefinition>();
      if (idlDefinition instanceof ImportContainer) {
        for (final Import imp : ((ImportContainer) idlDefinition).getImports()) {
          importedDefinitions.add((IDLDefinition) Util.getImportedAST(imp));
        }
      }

      return taskFactoryItf
          .newPrimitiveTask(new CRecordSourceTask(
              (RecordDefinition) idlDefinition), idlDefinition,
              importedDefinitions);
    } else
      return null;
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds record definitions for C. It extends the Abstract Interface visitor
   * and implements the required visit methods.
   */
  @TaskParameters({"recordDefinitionNode", "importedDefinitions"})
  @ServerInterfaces(@ServerInterface(name = "c-record-definition-provider", signature = SourceCodeProvider.class, record = "role:cRecordDefinition, id:%", parameters = {"recordDefinitionNode"}))
  public static class CRecordSourceTask extends AbstractRecordVisitor
      implements
        Executable,
        SourceCodeProvider {

    protected final RecordDefinition                    recordDefinition;

    // -------------------------------------------------------------------------
    // Client interfaces
    // -------------------------------------------------------------------------

    /** Source code to be included to access to definition of imported IDLs. */
    @ClientInterfaceForEach(iterable = "importedDefinitions", prefix = "imported-definition", signature = SourceCodeProvider.class, record = "role:importedDefinition, id:%", parameters = "importedDefinitions.element")
    public final Map<IDLDefinition, SourceCodeProvider> importedDefinitionItfs = new LinkedHashMap<IDLDefinition, SourceCodeProvider>();

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * Simple constructor.
     * 
     * @param recordDefinition The record definition node for which this task
     *            must generate C source code.
     */
    public CRecordSourceTask(final RecordDefinition recordDefinition) {
      this.recordDefinition = recordDefinition;
    }

    // The generated source code.
    protected String     sourceCode;
    // The code writer that is used for code generation
    protected CodeWriter cw;
    // record names translated in C.
    String               recordName;

    // -------------------------------------------------------------------------
    // Implementation of the Executable interface
    // -------------------------------------------------------------------------

    public void execute() throws Exception {
      cw = new CodeWriter("C Record builder");
      this.recordName = recordDefinition.getName();
      if (recordName != null) {
        // Visit
        visit(recordDefinition);
      }
      sourceCode = cw.toString();
    }

    // -------------------------------------------------------------------------
    // Implementation of the SourceCodeProvider interface
    // -------------------------------------------------------------------------

    public String getSourceCode() {
      return sourceCode;
    }

    // -------------------------------------------------------------------------
    // Implementation of the visitor methods
    // -------------------------------------------------------------------------

    public void enterRecord(final RecordDefinition record) throws Exception {
      cw.append("/* Generated record :" + recordName + " */").endl();

      final String struct = recordName.replace('.', '_');
      cw.append("#ifndef _").append(struct.toUpperCase()).append("_IDL_H_")
          .endl();
      cw.append("#define _").append(struct.toUpperCase()).append("_IDL_H_")
          .endl();
      cw.endl();

      cw.endl();
      cw.endl();
      cw.append("/* some cecilia typedefs for predefinite C types */").endl();
      cw.append("#include \"cecilia_types.h\"").endl();
      cw.endl();
      cw.endl();

      for (final SourceCodeProvider importedDefinitionProvider : importedDefinitionItfs
          .values()) {
        cw.append(importedDefinitionProvider.getSourceCode());
      }

      cw.append("struct ").append(struct).append(";").endl();
      cw.append("typedef struct ").append(struct).append(' ').append(struct)
          .append(";").endl();
      cw.append("typedef struct ").append(struct).append(" R").append(struct)
          .append(";").endl();
      cw.append("struct ").append(struct).append("{").endl();
    }

    public void leaveRecord(final RecordDefinition record) throws Exception {
      cw.append("};").endl();
      cw.endl();
      final String struct = recordName.replace('.', '_');
      cw.append("#define ").append(struct.toUpperCase()).append("_POINTER \\")
          .endl();
      cw.append("  struct ").append(struct).append("* ").endl();
      cw.append("#define ").append(struct.toUpperCase())
          .append("_STRUCTURE \\").endl();
      cw.append("  struct ").append(struct).endl();

      cw.append("#endif").endl();
    }

    public void visitField(final Field field) throws Exception {
      cw.append(CUtil.buildRecordField(field));
      cw.append(";").endl();
    }

  }
}
