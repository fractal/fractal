/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.implementations;

import org.objectweb.fractal.api.control.AttributeController;

/**
 * Attribute controller interface for
 * {@link LanguageLoader language loader component}. Defines attributes for
 * default implementation languages.
 */
public interface LanguageLoaderAttributes extends AttributeController {

  /**
   * Sets the default PPL (Primitive-Programming-Language) used if a primitive
   * component does not specify its
   * {@link ExtendedImplementation#setLanguage(String) implementation language}.
   * 
   * @param implemLanguage the new value of the <code>defaultPPL</code>
   *            attribute.
   */
  void setDefaultPPL(String implemLanguage);

  /**
   * Returns the value of the <code>defaultPPL</code> attribute.
   * 
   * @return the value of the <code>defaultPPL</code> attribute.
   * @see #setDefaultPPL(String)
   */
  String getDefaultPPL();

  /**
   * Sets the default CPL (Controller-Programming-Language) used if a component
   * component does not specify its
   * {@link org.objectweb.fractal.cecilia.adl.controllers.ExtendedController#setLanguage(String) controller language}.
   * 
   * @param defaultCPL the new value of the <code>defaultCPL</code> attribute.
   */
  void setDefaultCPL(String defaultCPL);

  /**
   * Returns the value of the <code>defaultCPL</code> attribute.
   * 
   * @return the value of the <code>defaultCPL</code> attribute.
   * @see #setDefaultCPL(String)
   */
  String getDefaultCPL();
}
