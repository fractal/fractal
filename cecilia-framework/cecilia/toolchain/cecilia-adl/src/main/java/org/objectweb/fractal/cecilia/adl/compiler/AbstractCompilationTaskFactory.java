/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.compiler;

import java.io.File;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ContextLocal;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;

public abstract class AbstractCompilationTaskFactory
    extends
      AbstractTaskFactoryUser implements CompilationTaskFactory {

  private final ContextLocal<Map<Object, Component>> compileTaskCache = new ContextLocal<Map<Object, Component>>();
  private final ContextLocal<Map<Object, Component>> linkTaskCache    = new ContextLocal<Map<Object, Component>>();
  private final ContextLocal<Map<Object, Component>> archiveTaskCache = new ContextLocal<Map<Object, Component>>();

  // -------------------------------------------------------------------------
  // Implementation of the CompilationTaskFactory interface
  // -------------------------------------------------------------------------

  public Component newCompileTask(final Object id, List<String> flags,
      final Map<Object, Object> context) throws TaskException {
    final Map<Object, Component> cache = getCompileCache(context);
    Component task = cache.get(id);
    if (task == null) {
      // append context flags.
      if (flags == null) flags = new ArrayList<String>();
      flags = tryAppendFlag(flags, "c-flags", context);

      // get compiler command from context
      final String compilerCommand = getContextStringArg("compiler-command", context);

      // get output directory from context
      final File outputDir = (File) context.get("objBuildDirectory");

      // create compilation task
      task = createCompileTask(id, compilerCommand, flags, outputDir, context);

      // put task in cache
      cache.put(id, task);
    }
    return task;
  }

  public Component newLinkTask(final Object id, String linkedFileName,
      List<String> flags, final Map<Object, Object> context) throws TaskException {
    final Map<Object, Component> cache = getLinkTaskCache(context);
    Component task = cache.get(id);
    if (task == null) {
      // append context flags.
      if (flags == null) flags = new ArrayList<String>();
      flags = tryAppendFlag(flags, "ld-flags", context);

      // get linker command from context
      final String linkerCommand = getContextStringArg("linker-command", context);

      // get output directory from context
      final File outputDir = (File) context.get("objBuildDirectory");

      // get the executable name is not specified as parameter
      if (linkedFileName == null) {
        linkedFileName = getContextStringArg("executable-name", context);
      }

      // get the number of jobs from context
      Integer nThreads = (Integer) context.get("jobs");
      if (nThreads == null) nThreads = 1;

      // create compilation task
      task = createLinkTask(id, linkerCommand, linkedFileName, flags,
          outputDir, nThreads, context);

      // put task in cache
      cache.put(id, task);
    }
    return task;
  }

  public Component newArchiveTask(final Object id,
      final String archiveFileName, List<String> flags,
      final Map<Object, Object> context) throws TaskException {
    final Map<Object, Component> cache = getArchiveTaskCache(context);
    Component task = cache.get(id);
    if (task == null) {
      // append context flags.
      if (flags == null) flags = new ArrayList<String>();
      flags = tryAppendFlag(flags, "ar-flags", context);

      // get linker command from context
      final String archiveCommand = getContextStringArg("archiver-command", context);

      // get output directory from context
      final File outputDir = (File) context.get("objBuildDirectory");

      // create compilation task
      task = createArchiveTask(id, archiveCommand, archiveFileName, flags,
          outputDir, context);

      // put task in cache
      cache.put(id, task);
    }
    return task;
  }

  // ---------------------------------------------------------------------------
  // Abstract methods
  // ---------------------------------------------------------------------------

  protected abstract Component createCompileTask(final Object id,
      String compileCommand, List<String> flags, File outputDir,
      final Map<Object, Object> context) throws TaskException;

  protected abstract Component createLinkTask(final Object id,
      String linkCommand, String linkedFileName, List<String> flags, File outputDir,
      int nbJobs, final Map<Object, Object> context) throws TaskException;

  protected abstract Component createArchiveTask(final Object id,
      String archiveCommand, String archiveFileName, List<String> flags,
      File outputDir, final Map<Object, Object> context) throws TaskException;

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  private Map<Object, Component> getCompileCache(final Object context) {
    Map<Object, Component> cache = compileTaskCache.get(context);
    if (cache == null) {
      cache = new IdentityHashMap<Object, Component>();
      compileTaskCache.set(context, cache);
    }
    return cache;
  }

  private Map<Object, Component> getLinkTaskCache(final Object context) {
    Map<Object, Component> cache = linkTaskCache.get(context);
    if (cache == null) {
      cache = new IdentityHashMap<Object, Component>();
      compileTaskCache.set(context, cache);
    }
    return cache;
  }

  private Map<Object, Component> getArchiveTaskCache(final Object context) {
    Map<Object, Component> cache = archiveTaskCache.get(context);
    if (cache == null) {
      cache = new IdentityHashMap<Object, Component>();
      compileTaskCache.set(context, cache);
    }
    return cache;
  }

  protected List<String> getContextArg(final String name,
      final Map<Object, Object> context) {
    final List<String> s = tryGetContextArg(name, context);
    if (s == null)
      throw new IllegalArgumentException("Can't find contextual parameter \'"
          + name + "\'.");
    return s;
  }

  protected String getContextStringArg(final String name,
      final Map<Object, Object> context) {
    final String s = tryGetContextStringArg(name, context);
    if (s == null)
      throw new IllegalArgumentException("Can't find contextual parameter \'"
          + name + "\'.");
    return s;
  }

  protected List<String> tryGetContextArg(final String name,
      final Map<Object, Object> context) {
    final Object arg = context.get(name);
    if ((arg != null) && !(arg instanceof List)) {
      throw new IllegalArgumentException("Contextual parameter \'" + name
          + "\' is not a List<String>.");
    }
    return (List<String>) arg;
  }

  protected String tryGetContextStringArg(final String name,
      final Map<Object, Object> context) {
    final Object arg = context.get(name);
    if ((arg != null) && !(arg instanceof String)) {
      throw new IllegalArgumentException("Contextual parameter \'" + name
          + "\' is not a String.");
    }
    return (String) arg;
  }

  protected List<String> tryAppendFlag(final List<String> flags, final String contextArg,
      final Map<Object, Object> context) {
    final List<String> arg = tryGetContextArg(contextArg, context);
    if (arg != null) {
      //flags.addAll(arg);
      for (String s : arg) {
        flags.add(s);
      }
    }
    return flags;
  }
}
