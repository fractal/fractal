/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.interfaces;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.ast.ASTFractalUtil;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.cecilia.adl.controllers.ControllerDecorationUtil;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.types.TypeDecorationUtil;

/**
 * Utility class to manipulate decorations of {@link Interface} node.
 */
public final class InterfaceDecorationUtil {

  /**
   * Name of the sytem property which controls generation of components
   * that don't use 'void *_this'.
   */
  public static final String CECILIA_ENABLE_THIS_OPTIMIZATION     = "CECILIA_ENABLE_THIS_OPTIMIZATION";

  /**
   * A decoration set on client {@link Interface} nodes to say that binding to
   * this client interface must not be established statically. The value of this
   * decoration must be a {@link Boolean}.
   * 
   * @see #setNoStaticBinding(Interface, boolean)
   * @see #isNoStaticBinding(Interface)
   * @see org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil#STATICALLY_BOUND_DECORATION
   */
  public static final String NO_STATIC_BINDING_DECORATION = "no-static-binding";

  /**
   * A decoration set on a client {@link Interface} node to indicates to which
   * server interface it is bound to. The value of this decoration must be an
   * {@link Interface} node.
   * 
   * @see #setBoundTo(Interface, Interface)
   * @see #getBoundTo(Interface)
   */
  public static final String BOUND_TO_DECORATION          = "bound-to";

  /**
   * A decoration set on {@link Interface} node to find the component to which
   * the interface belongs to. The value of this decoration must be an
   * {@link InterfaceContainer} node.
   */
  public static final String CONTAINER_DECORATION         = "container";

  /**
   * A decoration set on {@link IDLDefinition} node to add the ordered list of
   * IDL interfaces along the inheritance path, starting from the IDLDefinition
   * signature itself.
   */
  public static final String INHERITANCE_PATH_DECORATION  = "INHERITANCE_PATH_DECORATION";

  /**
   * A decoration set on {@link Interface} node to record the name of the
   * original, non-flattened a bounded "collection" interface, on singleton
   * interfaces that were created by flattening a bounded "collection" interface.
   */
  public static final String FLATTENED_COLLECTION_NAME  = "flattened-collection-name";

  /**
   * A decoration set on {@link Interface} node to record the cardinality of the
   * original, non-flattened a bounded "collection" interface, on singleton
   * interfaces that were created by flattening a bounded "collection" interface.
   */
  public static final String FLATTENED_COLLECTION_CARDINALITY  = "flattened-collection-cardinality";

  /**
   * The static interface decoration name.
   */
  public static final String STATIC_INTERFACE_DECORATION  = "static-interface-decoration";

  private InterfaceDecorationUtil() {
  }

  /**
   * Sets the {@link #NO_STATIC_BINDING_DECORATION} decoration to the given node
   * with the given boolean value.
   * 
   * @param itf the interface node to which the decoration is set.
   * @param b the value of the decoration.
   * @see #NO_STATIC_BINDING_DECORATION
   */
  public static void setNoStaticBinding(final Interface itf, final boolean b) {
    ((Node) itf).astSetDecoration(NO_STATIC_BINDING_DECORATION, b);
  }

  /**
   * Returns <code>true</code> if the given interface node has a
   * {@link #NO_STATIC_BINDING_DECORATION} decoration with the <code>true</code>
   * value.
   * 
   * @param itf an interface node.
   * @return <code>true</code> if the given binding node has a
   *         {@link #NO_STATIC_BINDING_DECORATION} decoration with the
   *         <code>true</code> value.
   * @see #NO_STATIC_BINDING_DECORATION
   */
  public static boolean isNoStaticBinding(final Interface itf) {
    final Boolean b = (Boolean) ((Node) itf)
        .astGetDecoration(NO_STATIC_BINDING_DECORATION);
    return b != null && b;
  }

  /**
   * Sets the {@link #BOUND_TO_DECORATION} decoration to the given client
   * interface node with the given server interface node value.
   * 
   * @param clientItf an interface node on which the decoration will be set.
   * @param serverItf the value of the decoration.
   * @see #BOUND_TO_DECORATION
   */
  public static void setBoundTo(final Interface clientItf,
      final Interface serverItf) {
    ((Node) clientItf).astSetDecoration(BOUND_TO_DECORATION, serverItf);
  }

  /**
   * Returns the value of the {@link #BOUND_TO_DECORATION} decoration of the
   * given client interface node.
   * 
   * @param clientItf an interface node.
   * @return The value of the {@link #BOUND_TO_DECORATION} decoration or
   *         <code>null</code> if the given node has no such decoration.
   * @see #BOUND_TO_DECORATION
   */
  public static Interface getBoundTo(final Interface clientItf) {
    return (Interface) ((Node) clientItf).astGetDecoration(BOUND_TO_DECORATION);
  }

  /**
   * Sets the value of the {@link #CONTAINER_DECORATION} decoration to the given
   * interface container node.
   * 
   * @param itf an interface node.
   * @param itfContainer the value of the decoration.
   * @see #CONTAINER_DECORATION
   */
  public static void setContainer(final Interface itf,
      final InterfaceContainer itfContainer) {
    ((Node) itf).astSetDecoration(CONTAINER_DECORATION, itfContainer);
  }

  /**
   * Returns the value of the {@link #CONTAINER_DECORATION} decoration of the
   * given interface node.
   * 
   * @param itf an interface node.
   * @return The value of the {@link #CONTAINER_DECORATION} decoration or
   *         <code>null</code> if the given node has no such decoration.
   * @see #CONTAINER_DECORATION
   */
  public static InterfaceContainer getContainer(final Interface itf) {
    return (InterfaceContainer) ((Node) itf)
        .astGetDecoration(CONTAINER_DECORATION);
  }

  /**
   * Adds the given signature to the value of the
   * {@link #INHERITANCE_PATH_DECORATION} decoration.
   * 
   * @param node the node on which the decoration is updated
   * @param signature the signature to be added.
   */
  public static void updateInheritancePathDecoration(final IDLDefinition node,
      final String signature) {

    if (node == null) {
      throw new IllegalArgumentException("Node argument can't be null");
    }

    List<String> inheritanceList = getInheritancePathDecoration(node);

    if (inheritanceList == null) {
      inheritanceList = new ArrayList<String>();
      ((Node) node).astSetDecoration(INHERITANCE_PATH_DECORATION,
          inheritanceList);
    }

    inheritanceList.add(signature);
  }

  /**
   * Adds the given list of signature to the value of the
   * {@link #INHERITANCE_PATH_DECORATION} decoration.
   * 
   * @param node the node on which the decoration is updated
   * @param parentInheritancePath the list of signature to be added.
   */
  public static void updateInheritancePathDecoration(final IDLDefinition node,
      final List<String> parentInheritancePath) {

    List<String> inheritanceList = getInheritancePathDecoration(node);

    if (inheritanceList == null) {
      inheritanceList = new ArrayList<String>();
      ((Node) node).astSetDecoration(INHERITANCE_PATH_DECORATION,
          inheritanceList);
    }

    inheritanceList.addAll(parentInheritancePath);
  }

  /**
   * Returns the value of the {@link #INHERITANCE_PATH_DECORATION} decoration of
   * the given node.
   * 
   * @param node a node
   * @return the value of the {@link #INHERITANCE_PATH_DECORATION} decoration of
   *         the given node. May be <code>null</code>.
   */
  @SuppressWarnings("unchecked")
  public static List<String> getInheritancePathDecoration(
      final IDLDefinition node) {

    if (node == null) {
      throw new IllegalArgumentException("Node argument can't be null");
    }

    return (List<String>) node.astGetDecoration(INHERITANCE_PATH_DECORATION);
  }

  /**
   * Returns <code>true</code> is the {@link #INHERITANCE_PATH_DECORATION}
   * decoration is present on the given node.
   * 
   * @param node a node
   * @return <code>true</code> is the {@link #INHERITANCE_PATH_DECORATION}
   *         decoration is present on the given node.
   */
  public static boolean isInheritancePathDecorationPresent(
      final IDLDefinition node) {
    return getInheritancePathDecoration(node) != null;
  }

  /**
   * Sets the {@link #FLATTENED_COLLECTION_NAME} decoration on the given node
   * 
   * @param itf the interface node on which the decoration is set.
   * @param name the name of the un-flattened bounded "collection" interface
   * @see #FLATTENED_COLLECTION_NAME
   */
  public static void setFlattenedCollectionName(final Interface itf,
      final String name) {
    ((Node) itf).astSetDecoration(FLATTENED_COLLECTION_NAME, name);
  }

  /**
   * Returns the value of the {@link #FLATTENED_COLLECTION_NAME} decoration of the
   * given interface node.
   * 
   * @param itf an interface node.
   * @return The value of the {@link #FLATTENED_COLLECTION_NAME} decoration or
   *         <code>null</code> if the given node has no such decoration.
   */
  public static String getFlattenedCollectionName(final Interface itf) {
    return (String) ((Node) itf).astGetDecoration(FLATTENED_COLLECTION_NAME);
  }

  /**
   * Sets the {@link #FLATTENED_COLLECTION_CARDINALITY} decoration on the given node
   * 
   * @param itf the interface node on which the decoration is set.
   * @param cardinality the cardinality of the un-flattened bounded "collection" interface
   * @see #FLATTENED_COLLECTION_CARDINALITY
   */
  public static void setFlattenedCollectionCardinality(final Interface itf,
      final Integer cardinality) {
    ((Node) itf).astSetDecoration(FLATTENED_COLLECTION_CARDINALITY, cardinality);
  }

  /**
   * Returns the value of the {@link #FLATTENED_COLLECTION_CARDINALITY} decoration of the
   * given interface node.
   * 
   * @param itf an interface node.
   * @return The value of the {@link #FLATTENED_COLLECTION_CARDINALITY} decoration or
   *         <code>null</code> if the given node has no such decoration.
   */
  public static Integer getFlattenedCollectionCardinality(final Interface itf) {
    return (Integer) ((Node) itf).astGetDecoration(FLATTENED_COLLECTION_CARDINALITY);
  }

  /**
   * @param typeItf
   * @return
   */
  public static Boolean getStaticInterfaceDecoration(
      final TypeInterface typeItf) {

    if (typeItf == null) {
      throw new IllegalArgumentException("Interface argument can't be null");
    }

    return (Boolean) ((Node) typeItf)
        .astGetDecoration(InterfaceDecorationUtil.STATIC_INTERFACE_DECORATION);
  }

  /**
   * @param typeItf
   * @param value
   */
  public static void setStaticInterfaceDecoration(
      final TypeInterface typeItf, final Boolean value) {
    ((Node) typeItf).astSetDecoration(
        InterfaceDecorationUtil.STATIC_INTERFACE_DECORATION, value);
     }

  /**
   * @param typeItf
   * @return
   */
  public static Boolean getStaticInterfaceDecorationDefaultToTrue(
      final TypeInterface typeItf) {

    final Boolean decoration = getStaticInterfaceDecoration(typeItf);

    if (decoration == null) {
      return true;
    }

    return decoration;
  }

  /**
   * return <code>true</code> if the given {@link TypeInterface} (being it
   * client or server) has to be compiled as static, <code>false</code>
   * otherwise.
   *
   * @param typeInterface
   * @return true if the given {@link TypeInterface} (being it client or server)
   *         has to be compiled as static.
   */
  public static Boolean hasToBeCompiledAsStatic(
      final TypeInterface typeInterface) {

    if (System.getProperty(InterfaceDecorationUtil.CECILIA_ENABLE_THIS_OPTIMIZATION,
        "false").equals("true")) {

      boolean compileIdlAsSingleton = false; // default

      final ComponentContainer container = (ComponentContainer) InterfaceDecorationUtil
          .getContainer(typeInterface);

      /* if it is a server interface */
      if (typeInterface.getRole().equals(TypeInterface.SERVER_ROLE)) {

        /*
         * XXX controller generated interfaces are not considered as being
         * candidates for singleton interface generation (at least, for the
         * moment).
         */
        if (ASTFractalUtil.isPrimitive(container)
            && ControllerDecorationUtil.getVTable(typeInterface) == null) {

          System.out.println("Interface " + typeInterface.getName() + " of container "
              + container + " is provided by developer");
          final ComponentContainer componentType = TypeDecorationUtil
              .getTypeDecoration(container);
          final boolean isSingletonCompType = TypeDecorationUtil
              .isSingletonComponentType(componentType);

          if (isSingletonCompType
              && InterfaceDecorationUtil
                  .getStaticInterfaceDecoration(typeInterface)) {

            System.out.println("Server interface " + typeInterface.getName() + " ["
                + typeInterface.getSignature()
                + "] will be compiled in singleton style");
            compileIdlAsSingleton = true;
          }
        }
      } else {
        /* if it is a client interface */
        final TypeInterface boundToItf = (TypeInterface) InterfaceDecorationUtil
            .getBoundTo(typeInterface);

        /* could eventually be null if the interface is not set statically */
        if (boundToItf != null) {
          final ComponentContainer boundToItfOwner = (ComponentContainer) InterfaceDecorationUtil
              .getContainer(boundToItf);
          final ComponentContainer boundToCompType = TypeDecorationUtil
              .getTypeDecoration(boundToItfOwner);
          /* get the corresponding interface on the component type */
          final TypeInterface boundToItfOnCompType = ASTFractalUtil
              .getFcInterface(boundToCompType, boundToItf.getName());

          /*
           * XXX controller generated interfaces are not considered as being
           * candidates for singleton interface generation (at least, for the
           * moment).
           */
          if (ASTFractalUtil.isPrimitive(boundToCompType)
              && ControllerDecorationUtil.getVTable(boundToItfOnCompType) == null) {
            System.out.println("Interface " + typeInterface.getName() + " of container "
                + container + " is provided by developer");
            if (TypeDecorationUtil.isSingletonComponentType(boundToCompType)
                && InterfaceDecorationUtil
                    .getStaticInterfaceDecoration(boundToItfOnCompType)) {
              System.out.println("Client interface " + typeInterface.getName()
                  + " [" + typeInterface.getSignature()
                  + "] will be compiled in singleton style");
              compileIdlAsSingleton = true;
            }
          }
        }
      }

      return compileIdlAsSingleton;
    }

    return false;
   }
}
