/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.primitive.thinkMC.controllers;

import org.objectweb.fractal.adl.attributes.Attributes;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.attributes.AbstractAttributeVisitor;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.idl.ast.Field;
import org.objectweb.fractal.cecilia.adl.idl.ast.FieldContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the definition of the attribute controller data
 * structure of the component for <code>ThinkMC</code> dialect.
 */
public class ACDefinitionVisitor extends AbstractAttributeVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractAttributeVisitor
  // ---------------------------------------------------------------------------

  @Override
  protected Component createTask(final ComponentContainer container,
      final Attributes attributes) throws TaskException {
    return taskFactoryItf.newPrimitiveTask(new ACDefinitionTask(
        ((FieldContainer) ((IDLDefinitionContainer) attributes)
            .getIDLDefinition()).getFields()), container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds source code for the definition of the attribute controller data
   * structure for the given component node. This task provides the produced
   * source code.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "ac-definition-provider", signature = SourceCodeProvider.class, record = "role:controllerDefinition, id:%, controller:AC", parameters = "componentNode"))
  public static class ACDefinitionTask extends AbstractDefinitionTask {
    // Array of attribute fields
    final Field[] attributeFields;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param attributeFields the attributes.
     */
    public ACDefinitionTask(final Field[] attributeFields) {
      this.attributeFields = attributeFields;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      boolean hasConstAttributes = false;
      for (final Field f : attributeFields) {
        if (f.getQualifier().equals(Field.CONST)) {
          hasConstAttributes = true;
          break;
        }
      }

      final CodeWriter cw = new CodeWriter("Primitive Attribute Controller Definition");
      if (hasConstAttributes) {
        cw.append("#include \"fractal/lib/ACdelegate_constattributes.h\"")
            .endl();
        cw.append("DEFINE_CONST_AC_DATA_TYPE(");
      } else {
        cw.append("#include \"fractal/lib/ACdelegate.h\"").endl();
        cw.append("DEFINE_AC_DATA_TYPE(");
      }
      cw.append(typeNameProviderItf.getCTypeName()).append(", ").append(
          attributeFields.length).append(");").endl();

      cw.endl();
      return cw.toString();
    }
  }
}
