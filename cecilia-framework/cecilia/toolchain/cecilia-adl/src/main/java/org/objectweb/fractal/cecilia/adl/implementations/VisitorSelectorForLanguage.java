/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan, Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.implementations;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.components.AbstractComponentVisitorPluginUser;
import org.objectweb.fractal.cecilia.adl.plugin.PluginErrors;
import org.objectweb.fractal.cecilia.adl.plugin.PluginException;
import org.objectweb.fractal.task.core.TaskException;

/**
 * Delegates compilation to a client {@link ComponentVisitor} depending on the
 * implementation language of the component for which its visit method is
 * invoked.
 */
public class VisitorSelectorForLanguage
    extends
      AbstractComponentVisitorPluginUser implements ComponentVisitor {

  // --------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // --------------------------------------------------------------------------

  public Component visit(final List<Node> path, final ComponentContainer node,
      final Map<Object, Object> context) throws ADLException, TaskException {
    String implementationLanguage = null;
    final Implementation impl = castNodeError(node,
        ImplementationContainer.class).getImplementation();
    if (impl == null) {
      throw new CompilerError(GenericErrors.INTERNAL_ERROR,
          "This visitor is only applicable for primitive component.");
    }
    final ExtendedImplementation implem = castNodeError(impl,
        ExtendedImplementation.class);
    implementationLanguage = implem.getLanguage();
    if (implementationLanguage == null) {
      throw new CompilerError(GenericErrors.INTERNAL_ERROR,
          "This visitor is only applicable for primitive component.");
    }

    try {
      return getVisitorPlugin(implementationLanguage, context).visit(path,
          node, context);
    } catch (final PluginException e) {
      if (e.getError().getTemplate() == PluginErrors.PLUGIN_NOT_FOUND) {
        throw new ADLException(
            ImplementationErrors.LANGUAGE_VISITOR_PLUGIN_NOT_FOUND,
            implementationLanguage);
      } else {
        throw new CompilerError(GenericErrors.INTERNAL_ERROR, e,
            "Can't load visitor plugin for language \""
                + implementationLanguage + "\"");
      }
    }
  }
}
