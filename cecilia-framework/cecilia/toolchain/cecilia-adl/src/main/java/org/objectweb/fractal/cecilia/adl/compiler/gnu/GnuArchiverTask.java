/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.compiler.gnu;

import static org.objectweb.fractal.cecilia.adl.compiler.ExecutionHelper.exec;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.cecilia.adl.compiler.AbstractArchiveTask;
import org.objectweb.fractal.cecilia.adl.compiler.CompilerErrors;

public class GnuArchiverTask extends AbstractArchiveTask {

  protected final String arCommand;

  // ---------------------------------------------------------------------------
  // Task constructor
  // ---------------------------------------------------------------------------

  /**
   * @param arCommand the command of the Gnu archiver to be executed by this
   *            task.
   * @param outputName the name of the linked file
   * @param outputDir the directory into which the compiled file will be placed.
   * @param flags compilation flags.
   */
  public GnuArchiverTask(final String arCommand, final String outputName,
      final File outputDir, final List<String> flags) {
    super(outputName, outputDir, flags);
    this.arCommand = arCommand;
  }

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractLinkTask
  // ---------------------------------------------------------------------------

  @Override
  protected File archivate(final File outputFile,
      final Collection<File> inputFiles) throws ADLException,
      InterruptedException {
    // forge command line.
    final List<String> cmd = new ArrayList<String>();
    cmd.add(arCommand);

    if (flags != null) {
      for (String flag : flags) {
        if (flag != null && !flag.equals("")) {
          cmd.add(flag);
        }
      }
    }

    cmd.add("r");
    cmd.add(outputFile.getPath());

    for (final File inputFile : inputFiles) {
      cmd.add(inputFile.getPath());
    }

    // execute command
    final int rValue = exec("ar: " + outputFile.getPath(), cmd);
    if (rValue != 0) {
      throw new ADLException(CompilerErrors.ARCHIVER_ERROR, outputFile
          .getPath());
    }

    return outputFile;
  }
}
