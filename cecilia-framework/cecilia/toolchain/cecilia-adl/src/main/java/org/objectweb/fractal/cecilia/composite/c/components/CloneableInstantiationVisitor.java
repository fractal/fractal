/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.composite.c.components;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.task.core.TaskException;

/**
 * Visitor component that builds the instantiation of a static component
 * instance for cloneable composite component.
 */
public class CloneableInstantiationVisitor
    extends
      ComponentInstantiationVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link ComponentContainer} nodes and creates a task that writes the
   * source code containing the initialization of the component data structure.
   */
  @Override
  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    return taskFactoryItf.newPrimitiveTask(new CloneableInstantiationTask(),
        container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds source code for the instantiation/initialization of a default
   * composite component. This task provides the produced source code.
   */
  public static class CloneableInstantiationTask
      extends
        ComponentInstantiationTask {

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractInstantiationTask
    // -------------------------------------------------------------------------
    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter(
          "Cloneable Composite Instantiation Builder");
      final String instanceName = instanceNameProviderItf.getCInstanceName();

      cw.appendln("// --------------------------------------------------------"
          + "---------------------");
      cw.append("// Declare instance ").append(
          instanceNameProviderItf.getInstanceName()).endl();
      cw.append("//        from type ").append(
          typeNameProviderItf.getTypeName()).endl();
      cw.appendln("// --------------------------------------------------------"
          + "---------------------");

      // Declare the component structure since it may be referenced by the
      // controller structures.
      // This declaration is extern since it is redeclared and initialized
      // latter.
      // This avoid warnings when compiling with -Wredundant-decls.
      cw.append("extern struct __cecilia_cloneableCompositeData_t ").append(
          instanceName).append(";").endl();

      // Append the code for the initialization of the controller data
      // structures (content controller must be appended before binding
      // controller).
      cw.appendln(ccInstantiationProviderItf.getSourceCode()).endl();
      cw.appendln(bcInstantiationProviderItf.getSourceCode()).endl();

      // Initialize the component data structure.
      cw.append("struct __cecilia_cloneableCompositeData_t ").append(
          instanceName).append(" = {").endl();
      cw.append("{").endl();

      cw.appendln("// Server interfaces");
      cw.append("{&__cecilia_cloneableComposite_ComponentMeths, &").append(
          instanceName).append("}, // myreference").endl();
      cw.append("{&__cecilia_cloneableComposite_BindingControllerMeths, &")
          .append(instanceName).append("}, // bc").endl();
      cw.append("{&__cecilia_defaultComposite_LifeCycleControllerMeths, &(")
          .append(instanceName).append(".defaultComposite)}, ").append(
              "// lcc: use the default composite implementation").endl();
      cw.append("{&__cecilia_defaultComposite_ContentControllerMeths, &(")
          .append(instanceName).append(".defaultComposite)}, ").append(
              "// cc: use the default composite implementation").endl();

      cw.appendln("// private data");
      cw.append("sizeof(").append(instanceName).append(
          "_subComponents) / sizeof(").append(instanceName).append(
          "_subComponents[0]), // nbSubComponents").endl();
      cw.append("sizeof(").append(instanceName).append("_bindings) / sizeof(")
          .append(instanceName).append("_bindings[0]), // nbBindings").endl();
      cw.append("0, // started").endl();
      cw.append(instanceName).append("_subComponents,").endl();
      cw.append(instanceName).append("_bindings,").endl();
      cw.appendln("},");

      cw.append("{&__cecilia_cloneableComposite_FactoryMeths, &").append(
          instanceName).append("},").endl();
      cw.append("0 // factory-allocator client interface").endl();
      cw.appendln("};");

      return cw.toString();
    }
  }
}
