/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.implementations;

import static java.lang.System.arraycopy;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.implementations.ImplementationCodeLoader;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.cecilia.adl.plugin.AbstractPluginUser;
import org.objectweb.fractal.cecilia.adl.plugin.PluginErrors;
import org.objectweb.fractal.cecilia.adl.plugin.PluginException;

/**
 * Delegates implementation code loading to a client plugin depending on the
 * language. More over, if the <code>language</code> is <code>null</code>,
 * this selector use a default client loader.
 */
public class ImplementationCodeLoaderSelector extends AbstractPluginUser
    implements
      ImplementationCodeLoader {

  // ---------------------------------------------------------------------------
  // Client interface.
  // ---------------------------------------------------------------------------

  /** the client interface prefix for loaded client loader plugins. */
  public static final String      CLIENT_LOADER_ITF  = "client-loader";

  /** the name of the default loader client interface. */
  public static final String      DEFAULT_LOADER_ITF = "default-loader";

  /**
   * The default {@link ImplementationCodeLoader} used if the language is
   * <code>null</code>.
   */
  public ImplementationCodeLoader defaultImplementationCodeLoaderItf;

  /** Default constructor. */
  public ImplementationCodeLoaderSelector() {
    super(CLIENT_LOADER_ITF);
  }

  // ---------------------------------------------------------------------------
  // Implementation of the ImplementationCodeLoader interface
  // ---------------------------------------------------------------------------

  public Object loadImplementation(final String signature,
      final String language, final Map<Object, Object> context)
      throws ADLException {

    if (language == null)
      return defaultImplementationCodeLoaderItf.loadImplementation(signature,
          language, context);

    try {
      return getPlugin(language, context, ImplementationCodeLoader.class)
          .loadImplementation(signature, language, context);
    } catch (final PluginException e) {
      if (e.getError().getTemplate() == PluginErrors.PLUGIN_NOT_FOUND) {
        throw new ADLException(
            ImplementationErrors.IMPL_LOADER_PLUGIN_NOT_FOUND, language);
      } else {
        throw new CompilerError(GenericErrors.INTERNAL_ERROR, e,
            "Unable to load ImplementationCodeLoader plugin for language \""
                + language + "\"");
      }
    }
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  @Override
  public void bindFc(final String itf, final Object value)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (itf == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (itf.equals(DEFAULT_LOADER_ITF)) {
      this.defaultImplementationCodeLoaderItf = (ImplementationCodeLoader) value;
    } else {
      super.bindFc(itf, value);
    }
  }

  @Override
  public String[] listFc() {
    final String[] superItfNames = super.listFc();
    final String[] itfNames = new String[superItfNames.length + 1];
    arraycopy(superItfNames, 0, itfNames, 0, superItfNames.length);
    itfNames[superItfNames.length] = DEFAULT_LOADER_ITF;
    return itfNames;
  }

  @Override
  public Object lookupFc(final String itf) throws NoSuchInterfaceException {
    if (itf == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (itf.equals(DEFAULT_LOADER_ITF)) {
      return this.defaultImplementationCodeLoaderItf;
    } else {
      return super.lookupFc(itf);
    }

  }

  @Override
  public void unbindFc(final String itf) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException {
    if (itf == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (itf.equals(DEFAULT_LOADER_ITF)) {
      this.defaultImplementationCodeLoaderItf = null;
    } else {
      super.unbindFc(itf);
    }
  }

}
