
package org.objectweb.fractal.cecilia.adl.controllers;

import org.objectweb.fractal.adl.implementations.Controller;

/**
 * AST Node interface which adds a <code>language</code> attribute to
 * <code>controller</code> element.
 */
public interface ExtendedController extends Controller {
  /**
   * Returns the language of the controller.
   * 
   * @return the language of the controller.
   */
  String getLanguage();

  /**
   * Set the language of the controller.
   * 
   * @param language the language of the controller.
   */
  void setLanguage(String language);
}
