/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.primitive.thinkMC.types;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerDecorationUtil.getVTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.interfaces.AbstractServerInterfaceVisitor;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterfaceForEach;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the VFT declarations for the exported
 * interfaces of the component for <code>ThinkMC</code> dialect.
 */
public class ServerInterfaceVFTInstantiationVisitor
    extends
      AbstractServerInterfaceVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractServerInterfaceVisitor
  // ---------------------------------------------------------------------------

  /**
   * Creates a task that writes the VFT Declarations and initializations for
   * each server interface of the component.
   */
  @Override
  protected Component createTask(final ComponentContainer container,
      final List<TypeInterface> serverInterfaces) throws ADLException,
      TaskException {
    final List<IDLDefinition> serverInterfaceDefinitions = new ArrayList<IDLDefinition>();
    for (final Interface itf : serverInterfaces) {
      serverInterfaceDefinitions.add(castNodeError(itf,
          IDLDefinitionContainer.class).getIDLDefinition());
    }

    return taskFactoryItf.newPrimitiveTask(new ExportedVFTDeclarationTask(
        serverInterfaces), container, serverInterfaceDefinitions);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds the declaration/initialization of the VFT of a component written in
   * <code>thinkMC</code> language. The VFT declared by this task is supposed to
   * be shared by each instance of the same component type. The code generated
   * by this task is surrounded by pre-processing instructions so that it can be
   * used multiple times in the same file without causing multiple declaration.
   */
  @TaskParameters({"componentNode", "exportedInterfaceDefinitions"})
  @ServerInterfaces(@ServerInterface(name = "source-code-provider", signature = SourceCodeProvider.class, record = "role:serverInterfaceVFTInstantiation, id:%", parameters = "componentNode"))
  public static class ExportedVFTDeclarationTask extends AbstractDefinitionTask {

    /**
     * Source code to be included to access the VFT template header files for
     * each exported interface of the componentNode.
     */
    @ClientInterfaceForEach(iterable = "exportedInterfaceDefinitions", prefix = "c-vft-header", signature = SourceCodeProvider.class, record = "role:cVFTDefinition, id:%", parameters = "exportedInterfaceDefinitions.element")
    public final Map<TypeInterface, SourceCodeProvider> cVFTHeaderItfs = new HashMap<TypeInterface, SourceCodeProvider>();

    protected final List<TypeInterface>                 exportedItfNodes;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param exportedItfNodes the list of exported interface nodes belonging to
     *          the component.
     */
    public ExportedVFTDeclarationTask(final List<TypeInterface> exportedItfNodes) {
      this.exportedItfNodes = exportedItfNodes;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter(
          "Server Interface, VFT instantiation");

      for (final TypeInterface itf : exportedItfNodes) {
        if (getVTable(itf) != null) continue;

        // include VFTTemplate header file
        final IDLDefinition itfDef = castNodeError(itf,
            IDLDefinitionContainer.class).getIDLDefinition();
        cw.append(cVFTHeaderItfs.get(itfDef).getSourceCode()).endl();
      }

      for (final TypeInterface itf : exportedItfNodes) {
        final String vTable = getVTable(itf);
        if (vTable != null) {
          cw.append("extern struct M").append(
              itf.getSignature().replace('.', '_')).append(" ").append(vTable)
              .append(";").endl();
        } else {

          // use the <itf>_VFT_DECLARATION macro to declare and initialize the
          // VFT.
          cw.append("// declare and initialize the VFT for the interface")
              .append(itf.getName()).endl();
          cw.append(itf.getSignature().replace('.', '_').toUpperCase()).append(
              "_VFT_DECLARATION(").append(typeNameProviderItf.getCTypeName())
              .append(",").append(itf.getName().replace('-', '_'))
              .append(") ;").endl();
        }
      }

      return cw.toString();
    }
  }
}
