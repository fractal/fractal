/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.controllers;

/**
 * Utility class that defines the standard names and signatures of the common
 * controller interfaces.
 */
public final class ControllerInterface {
  private ControllerInterface() {
  }

  /** The name of the component identity controller interface. */
  public static final String COMPONENT_IDENTITY             = "component";
  /** A shortcut to the {@link #COMPONENT_IDENTITY} constant. */
  public static final String CI                             = COMPONENT_IDENTITY;
  /** The signature of the {@link #COMPONENT_IDENTITY} interface. */
  public static final String COMPONENT_IDENTITY_SIGNATURE   = "fractal.api.Component";

  /** The name of the binding controller interface. */
  public static final String BINDING_CONTROLLER             = "binding-controller";
  /** A shortcut to the {@link #BINDING_CONTROLLER} constant. */
  public static final String BC                             = BINDING_CONTROLLER;
  /** The signature of the {@link #BINDING_CONTROLLER} interface. */
  public static final String BINDING_CONTROLLER_SIGNATURE   = "fractal.api.BindingController";

  /** The name of the attribute controller interface. */
  public static final String ATTRIBUTE_CONTROLLER           = "attribute-controller";
  /** A shortcut to the {@link #ATTRIBUTE_CONTROLLER} constant. */
  public static final String AC                             = ATTRIBUTE_CONTROLLER;
  /** The signature of the {@link #ATTRIBUTE_CONTROLLER} interface. */
  public static final String ATTRIBUTE_CONTROLLER_SIGNATURE = "fractal.api.AttributeController";

  /** The name of the life cycle controller interface. */
  public static final String LIFECYCLE_CONTROLLER           = "lifecycle-controller";
  /** A shortcut to the {@link #LIFECYCLE_CONTROLLER} constant. */
  public static final String LCC                            = LIFECYCLE_CONTROLLER;
  /** The signature of the {@link #LIFECYCLE_CONTROLLER} interface. */
  public static final String LIFECYCLE_CONTROLLER_SIGNATURE = "fractal.api.LifeCycleController";

  /** The name of the content controller interface. */
  public static final String CONTENT_CONTROLLER             = "content-controller";
  /** A shortcut to the {@link #CONTENT_CONTROLLER} constant. */
  public static final String CC                             = CONTENT_CONTROLLER;
  /** The signature of the {@link #CONTENT_CONTROLLER} interface. */
  public static final String CONTENT_CONTROLLER_SIGNATURE   = "fractal.api.ContentController";

  /** The name of the factory interface provided by cloneable component. */
  public static final String FACTORY                        = "factory";
  /** The name of the factory allocator interface used by cloneable component. */
  public static final String FACTORY_ALLOCATOR              = "factory-allocator";
  /** The signature of the {@link #FACTORY} interface. */
  public static final String FACTORY_SIGNATURE              = "fractal.api.Factory";
  /** The signature of the {@link #FACTORY_ALLOCATOR} interface. */
  public static final String FACTORY_ALLOCATOR_SIGNATURE    = "memory.api.Allocator";

}
