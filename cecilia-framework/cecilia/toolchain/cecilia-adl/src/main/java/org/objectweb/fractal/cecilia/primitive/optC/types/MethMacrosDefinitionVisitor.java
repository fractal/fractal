/***
 * Cecilia ADL Compiler
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.primitive.optC.types;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerDecorationUtil.getVTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.ast.ASTFractalUtil;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.Method;
import org.objectweb.fractal.cecilia.adl.idl.ast.MethodContainer;
import org.objectweb.fractal.cecilia.adl.idl.util.CUtil;
import org.objectweb.fractal.cecilia.adl.interfaces.AbstractServerInterfaceVisitor;
import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;
import org.objectweb.fractal.cecilia.adl.types.TypeDecorationUtil;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterfaceForEach;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * An {@link InterfaceBuilder} which generates the definition of the specific
 * <code>SERVER_${itfName}</code>, <code>SERVER_${itfName}_${methName}</code>,
 * <code>METH_${methodName}</code> macros for the given component type.
 * 
 * @author Alessio Pace
 */
public class MethMacrosDefinitionVisitor extends
      AbstractServerInterfaceVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractServerInterfaceVisitor
  // ---------------------------------------------------------------------------

  /**
   * Creates a task that writes the source code containing the definition of the
   * client interfaces of the component.
   */
  @Override
  protected Component createTask(final ComponentContainer container,
      final List<TypeInterface> serverInterfaces) throws ADLException,
      TaskException {
    final List<IDLDefinition> serverInterfaceDefinitions = new ArrayList<IDLDefinition>();
    for (final Interface itf : serverInterfaces) {
      serverInterfaceDefinitions.add(castNodeError(itf,
          IDLDefinitionContainer.class).getIDLDefinition());
    }

    return taskFactoryItf.newPrimitiveTask(new MethMacrosDefinitionTask(
        serverInterfaces), container, serverInterfaceDefinitions);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds definition source code for the definition of the client interfaces
   * for the given component node. This task provides the produced source code.
   */
  @TaskParameters({"componentNode", "exportedInterfaceDefinitions"})
  @ServerInterfaces(@ServerInterface(name = "source-code-provider", signature = SourceCodeProvider.class, record = "role:methMacrosDefinition, id:%", parameters = "componentNode"))
  public static class MethMacrosDefinitionTask
      extends
        AbstractDefinitionTask {

    protected final List<TypeInterface> exportedItfNodes;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param importedItfNodes the list of imported interface nodes belonging to
     *            the component.
     */
    public MethMacrosDefinitionTask(
        final List<TypeInterface> exportedItfNodes) {
      this.exportedItfNodes = exportedItfNodes;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter("SERVER_ITF, SERVER and METH macros definitions");

      /* this component type C name */
      final String componentCName = typeNameProviderItf.getCTypeName()
          .replace('.', '_');

      ComponentContainer owner = null;

      /* if there are server interfaces */
      if (!exportedItfNodes.isEmpty()) {

        owner = (ComponentContainer) InterfaceDecorationUtil
            .getContainer(exportedItfNodes.get(0));

        if (owner == null) {
          throw new ADLException("The interface " + exportedItfNodes.get(0).getName()
              + " has no container decoration");
        }

        final ComponentContainer type = TypeDecorationUtil
            .getTypeDecoration(owner);

        Boolean isSingleton = TypeDecorationUtil.isSingletonComponentType(type);

        /*
         * associate each method name (found on a server functional interface) to
         * the list of interfaces providing it
         */
        final Map<String, List<String>> methodNameToItfName = new HashMap<String, List<String>>();
        for (final TypeInterface itf : exportedItfNodes) {

          /* escape the '-' character */
          final String currentItfName = itf.getName().replace('-', '_');

          /*
           * SERVER_${itfName} macro to have a short cut for an own server
           * interface pointer given the name of the client interface.
           */

          if (!isSingleton) {
            cw.appendln("/* NOT singleton definition */");
            cw.append("#define SERVER_" + currentItfName + " ");
            cw.append(
                "  (& ( ((struct " + componentCName
                    + "_t*)  _this)->type.exported." + currentItfName + ") )")
                .endl();
          } else {
            cw.appendln("/* singleton definition */");
            cw.append("#define SERVER_" + currentItfName + " ");
            cw.append(
                " (&(" + componentCName + ".type.exported." + currentItfName
                    + "))").endl();
          }
          cw.endl();

          /* if it is a server interface which is NOT generated by the toolchain */
          if (getVTable(itf) == null) {
            final IDLDefinition itfDefinition = ((IDLDefinitionContainer) itf)
                .getIDLDefinition();
            /* if it is a static interface */
            final boolean isStatic = InterfaceDecorationUtil
                .hasToBeCompiledAsStatic(itf);
            final Method[] methods = ((MethodContainer) itfDefinition)
                .getMethods();
            for (final Method m : methods) {
              final String currentMethodName = m.getName();
              /*
               * SERVER_${itfName}_${methName} macro
               */
              cw.append("#define SERVER_" + currentItfName + "_"
                  + currentMethodName + " ");
              cw.append("  " + componentCName + "##_##" + currentItfName
                  + "##_##" + currentMethodName + "##_method(");
              if (!isStatic) {
                cw.appendln(" _this " + CUtil.CECILIA_PARAMS_MACRO);
              } else {
                cw.appendln("" + CUtil.CECILIA_ARGS_MACRO);
              }
              cw.endl();

              if (methodNameToItfName.containsKey(currentMethodName)) {
                final List<String> itfsProvidingCurrentMethodName = methodNameToItfName
                    .get(currentMethodName);
                itfsProvidingCurrentMethodName.add(currentMethodName);
              } else {
                final List<String> itfsProvidingCurrentMethodName = new ArrayList<String>();
                itfsProvidingCurrentMethodName.add(currentItfName);
                methodNameToItfName.put(currentMethodName,
                    itfsProvidingCurrentMethodName);
              }
            }
          }
        }

        /*
         * once all the methods name have been assigned to one or more interface
         * names, define the METH macros for those who are associated to just ONE
         * interface name
         */
        for (final String methodName : methodNameToItfName.keySet()) {
          final List<String> itfsProvidingCurrentMethodName = methodNameToItfName
              .get(methodName);
          if (itfsProvidingCurrentMethodName.size() == 1) {

            final TypeInterface itfProvidingMethod = ASTFractalUtil
                .getFcInterface(owner, itfsProvidingCurrentMethodName.get(0));
            final boolean isStatic = InterfaceDecorationUtil
                .hasToBeCompiledAsStatic(itfProvidingMethod);
            /*
             * depending on the staticness of the interface, adopt the corret
             * method definition macro (they are defined in cecilia.h)
             */
            String methodDefinitionMacroToUse = null;
            if (!isStatic) {
              methodDefinitionMacroToUse = "METHOD";
            } else {
              methodDefinitionMacroToUse = "STATIC_METHOD";
            }

            cw.append(
                "#define METH_" + methodName + " " + methodDefinitionMacroToUse
                    + "(" + itfsProvidingCurrentMethodName.get(0) + ","
                    + methodName + ")").endl();
          }
        }
      }
    
      return cw.toString();
    }
  }
}
