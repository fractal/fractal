/***
 * Cecilia ADL Compiler
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.components;

import org.objectweb.fractal.adl.components.Component;

/**
 * Helper class for {@link Component} and {@link ExtendedComponent} nodes.
 */
public class ComponentHelper {

  private ComponentHelper() {
  }

  /**
   * Returns the integer value of the {@link ExtendedComponent#getStartOrder()
   * startOrder} attribute of the given component node. If the given component
   * node does not have such attribute (either it does not implement
   * {@link ExtendedComponent} or the value of the attribute is
   * <code>null</code>), this method returns {@link Integer#MAX_VALUE}.
   * 
   * @param component a component node.
   * @return the integer value of the <code>startOrder</code> attribute of the
   *         given component node or {@link Integer#MAX_VALUE}.
   * @throws NumberFormatException if the value of the <code>startOrder</code>
   *           attribute is not a valid integer.
   */
  public static int getStartOrder(final Component component) {
    if (component instanceof ExtendedComponent) {
      final String startOrder = ((ExtendedComponent) component).getStartOrder();
      if (startOrder != null) {
        return Integer.parseInt(startOrder);
      }
    }

    return Integer.MAX_VALUE;
  }
}
