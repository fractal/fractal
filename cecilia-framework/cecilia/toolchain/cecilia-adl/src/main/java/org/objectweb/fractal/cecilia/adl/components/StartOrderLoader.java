/***
 * Cecilia ADL Compiler
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.components;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;

/**
 * Reorder {@link Component} nodes according to their
 * {@link ExtendedComponent#getStartOrder() startOrder} attributes. This
 * supposes that components will be actually started at runtime in the same
 * order as they are defined in the {@link ComponentContainer}.
 */
public class StartOrderLoader extends AbstractLoader {

  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    final Definition d = clientLoader.load(name, context);
    if (d instanceof ComponentContainer) checkOrder((ComponentContainer) d);

    return d;
  }

  protected void checkOrder(final ComponentContainer container)
      throws ADLException {
    // In most cases, no component node will contain a 'startOrder' attribute.
    // In other words, most of the time, this method has nothing to do. So avoid
    // the creation of data structure unless it is actually necessary.
    SortedMap<Integer, List<Component>> sortedComps = null;
    final Component[] components = container.getComponents();
    for (final Component component : components) {
      checkOrder(component);
      final int startOrder;
      try {
        startOrder = ComponentHelper.getStartOrder(component);
      } catch (final NumberFormatException e) {
        throw new ADLException(ComponentErrors.INVALID_START_ORDER, component);
      }
      if (startOrder < 0) {
        throw new ADLException(ComponentErrors.INVALID_NEGATIVE_START_ORDER,
            component);
      } else if (startOrder != Integer.MAX_VALUE) {
        if (sortedComps == null)
          sortedComps = new TreeMap<Integer, List<Component>>();
        List<Component> comps = sortedComps.get(startOrder);
        if (comps == null) {
          comps = new ArrayList<Component>();
          sortedComps.put(startOrder, comps);
        }
        comps.add(component);
      }
    }

    if (sortedComps != null) {
      // If at least one component node specify a start order,
      // remove every component nodes from the container and re-add them in the
      // correct order.
      final List<Component> unsortedComps = new ArrayList<Component>(
          components.length - sortedComps.size());
      for (final Component component : components) {
        if (ComponentHelper.getStartOrder(component) == Integer.MAX_VALUE)
          unsortedComps.add(component);

        container.removeComponent(component);
      }
      for (final List<Component> comps : sortedComps.values()) {
        for (final Component component : comps) {
          container.addComponent(component);
        }
      }
      for (final Component component : unsortedComps) {
        container.addComponent(component);
      }
    }
  }
}
