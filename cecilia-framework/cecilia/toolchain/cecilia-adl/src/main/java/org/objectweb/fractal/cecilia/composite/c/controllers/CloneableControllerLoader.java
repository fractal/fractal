/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 *
 */

package org.objectweb.fractal.cecilia.composite.c.controllers;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.FACTORY;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.FACTORY_ALLOCATOR;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.FACTORY_SIGNATURE;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.FACTORY_ALLOCATOR_SIGNATURE;
import static org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil.setDynamic;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.bindings.Binding;
import org.objectweb.fractal.adl.bindings.BindingContainer;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.error.NodeErrorLocator;
import org.objectweb.fractal.adl.implementations.Controller;
import org.objectweb.fractal.adl.implementations.ControllerContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.cecilia.adl.CeciliaADLConstants;
import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;
import org.xml.sax.SAXException;

/**
 * Adds controllers interface for <code>"composite"</code> components.
 */
public class CloneableControllerLoader extends CompositeControllerLoader {

  private static final String CLONEABLE_COMPOSITE        = "fractal.lib.cloneableComposite";

  // ---------------------------------------------------------------------------
  // Overridden methods
  // ---------------------------------------------------------------------------

  @Override
  public void beforeSubComponentCheck(final List<ComponentContainer> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {

    final Component[] subComponents = container.getComponents();
    if (subComponents.length == 0) return;

    final Binding[] bindings = (container instanceof BindingContainer)
        ? ((BindingContainer) container).getBindings()
        : null;

    for (final Component subComp : subComponents) {
      final Controller controller = castNodeError(subComp,
          ControllerContainer.class).getController();
      if (controller == null)
        throw new CompilerError(GenericErrors.INTERNAL_ERROR,
            new NodeErrorLocator(container),
            "Can't find controller node in AST");
      final String controllerDesc = controller.getDescriptor();
      if (controllerDesc == null || controllerDesc.equals("composite")
          || controllerDesc.equals("primitive")) {
        controller.setDescriptor("cloneable");
      }

      if (bindings != null) {
        // try to find binding of from "<subComp>.factory-allocator".
        final String from = subComp.getName() + "."
            + FACTORY_ALLOCATOR;
        boolean found = false;
        for (final Binding binding : bindings) {
          if (from.equals(binding.getFrom())) {
            found = true;
            break;
          }
        }
        if (!found) {
          Binding binding;
          try {
            binding = (Binding) nodeFactoryItf.newXMLNode(
                CeciliaADLConstants.CECILIA_ADL_DTD,
                CeciliaADLConstants.BINDING_AST_NODE_NAME);
          } catch (final SAXException e) {
            throw new CompilerError(GenericErrors.INTERNAL_ERROR, e,
                "Unable to create controller node.");
          }
          binding.astSetSource(controller.astGetSource());
          binding.setFrom(from);
          binding.setTo("this." + FACTORY_ALLOCATOR);
          setDynamic((Node)binding, true);

          ((BindingContainer) container).addBinding(binding);
        }
      }
    }
  }

  @Override
  protected void checkComponent(final ComponentContainer container)
      throws ADLException {
    super.checkComponent(container);

    // checks that each sub component has a factory interface
    for (final Component subComp : container.getComponents()) {
      if (subComp instanceof InterfaceContainer) {
        final TypeInterface subFItf = getInterface(FACTORY,
            ((InterfaceContainer) subComp));
        if (subFItf == null)
          throw new ADLException(CompositeControllerErrors.MISSING_FACTORY_ITF,
              subComp.getName(), subComp);

        checkServerInterface(subFItf);

        /*
         * set the NO_STATIC_BINDING_DECORATION on the factory-allocator of the
         * sub components.
         */
        final TypeInterface subFAItf = getInterface(FACTORY_ALLOCATOR,
            ((InterfaceContainer) subComp));
        if (subFAItf != null) {
          InterfaceDecorationUtil.setNoStaticBinding(subFAItf, true);
        }
      } else {
        throw new ADLException(CompositeControllerErrors.MISSING_FACTORY_ITF,
            subComp.getName(), subComp);
      }
    }
  }

  @Override
  protected List<Interface> getControllerInterfaces(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final List<Interface> controllerInterfaces = super.getControllerInterfaces(
        container, context);

    final Interface factoryInterface = getFactoryInterface(container, context);
    if (factoryInterface != null) controllerInterfaces.add(factoryInterface);

    final Interface factoryAllocatorInterface = getFactoryAllocatorInterface(
        container, context);
    if (factoryAllocatorInterface != null)
      controllerInterfaces.add(factoryAllocatorInterface);

    return controllerInterfaces;
  }

  @Override
  protected Interface getComponentIdentityInterface(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final Interface ciItf = super.getComponentIdentityInterface(container,
        context);
    setItfCode(ciItf, context, CLONEABLE_COMPOSITE, DEFAULT_COMPOSITE);
    return ciItf;
  }

  @Override
  protected Interface getBindingControllerInterface(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final Interface bcItf = super.getBindingControllerInterface(container,
        context);
    setItfCode(bcItf, context, CLONEABLE_COMPOSITE, DEFAULT_COMPOSITE);
    return bcItf;
  }

  @Override
  protected Interface getContentControllerInterface(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final Interface ccItf = super.getContentControllerInterface(container,
        context);
    setItfCode(ccItf, context, CLONEABLE_COMPOSITE, DEFAULT_COMPOSITE);
    return ccItf;
  }

  @Override
  protected Interface getLifeCycleControllerInterface(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final Interface lccItf = super.getLifeCycleControllerInterface(container,
        context);
    setItfCode(lccItf, context, CLONEABLE_COMPOSITE, DEFAULT_COMPOSITE);
    return lccItf;
  }

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  protected Interface getFactoryInterface(final ComponentContainer container,
      final Map<Object, Object> context) throws ADLException {
    final InterfaceContainer itfContainer = (InterfaceContainer) container;
    TypeInterface fItf = getInterface(FACTORY, itfContainer);
    if (fItf == null) {
      fItf = newServerInterfaceNode(FACTORY, FACTORY_SIGNATURE);
    } else {
      checkServerInterface(fItf);
      // remove the interface form the interface container, since it is re-added
      itfContainer.removeInterface(fItf);
    }
    setItfCode(fItf, context, CLONEABLE_COMPOSITE);
    return fItf;
  }

  protected Interface getFactoryAllocatorInterface(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final InterfaceContainer itfContainer = (InterfaceContainer) container;
    // Add the factory-allocator client interface
    TypeInterface allocItf = getInterface(FACTORY_ALLOCATOR,
        itfContainer);
    if (allocItf == null) {
      allocItf = newClientInterfaceNode(FACTORY_ALLOCATOR,
          FACTORY_ALLOCATOR_SIGNATURE);
    } else {
      checkClientInterface(allocItf);
      // remove the interface form the interface container, since it is re-added
      itfContainer.removeInterface(allocItf);
    }
    ((Node) allocItf).astSetDecoration(
        InterfaceDecorationUtil.NO_STATIC_BINDING_DECORATION, true);

    // Adds the CLONEABLE_COMPOSITE code decoration on the "factory-allocator"
    // interface of the composite to ensure that the memory/api/Allocator.idl.h
    // is generated before the compilation of the CLONEABLE_COMPOSITE.
    setItfCode(allocItf, context, CLONEABLE_COMPOSITE);

    return allocItf;
  }

}
