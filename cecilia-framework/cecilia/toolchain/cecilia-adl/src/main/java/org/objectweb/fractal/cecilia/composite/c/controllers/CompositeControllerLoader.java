/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 *
 */

package org.objectweb.fractal.cecilia.composite.c.controllers;

import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.BC;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.BINDING_CONTROLLER_SIGNATURE;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.CC;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.CI;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.COMPONENT_IDENTITY_SIGNATURE;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.CONTENT_CONTROLLER_SIGNATURE;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.LCC;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.LIFECYCLE_CONTROLLER_SIGNATURE;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.attributes.AttributesContainer;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.cecilia.adl.controllers.AbstractControllerChecker;

/**
 * Adds controllers interface for <code>"composite"</code> components.
 */
public class CompositeControllerLoader extends AbstractControllerChecker {

  protected static final String DEFAULT_COMPOSITE = "fractal.lib.defaultComposite";

  // --------------------------------------------------------------------------
  // Overridden methods
  // --------------------------------------------------------------------------

  @Override
  protected void checkComponent(final ComponentContainer container)
      throws ADLException {
    if (container instanceof ImplementationContainer
        && (((ImplementationContainer) container).getImplementation() != null))
      throw new ADLException(
          GenericErrors.GENERIC_ERROR,
          "This controller description can only be used for composite components",
          container);

    if (container instanceof AttributesContainer
        && ((AttributesContainer) container).getAttributes() != null) {
      throw new ADLException(GenericErrors.GENERIC_ERROR,
          "This controller description does not allows "
              + "attributes on composite component", container);
    }
  }

  @Override
  protected List<Interface> getControllerInterfaces(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final List<Interface> controllerItfs = new ArrayList<Interface>();
    final Interface ciItf = getComponentIdentityInterface(container, context);
    if (ciItf != null) controllerItfs.add(ciItf);

    final Interface bcItf = getBindingControllerInterface(container, context);
    if (bcItf != null) controllerItfs.add(bcItf);

    final Interface lccItf = getLifeCycleControllerInterface(container, context);
    if (lccItf != null) controllerItfs.add(lccItf);

    final Interface ccItf = getContentControllerInterface(container, context);
    if (ccItf != null) controllerItfs.add(ccItf);

    return controllerItfs;
  }

  // --------------------------------------------------------------------------
  // Utility methods
  // --------------------------------------------------------------------------

  protected Interface getComponentIdentityInterface(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final TypeInterface ciItf = newServerInterfaceNode(CI,
        COMPONENT_IDENTITY_SIGNATURE);
    setItfCode(ciItf, context, DEFAULT_COMPOSITE);
    return ciItf;
  }

  protected Interface getBindingControllerInterface(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final TypeInterface bcItf = newServerInterfaceNode(BC,
        BINDING_CONTROLLER_SIGNATURE);
    setItfCode(bcItf, context, DEFAULT_COMPOSITE);
    return bcItf;
  }

  protected Interface getLifeCycleControllerInterface(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final InterfaceContainer itfContainer = (InterfaceContainer) container;
    TypeInterface lccItf = getInterface(LCC, itfContainer);
    if (lccItf == null) {
      lccItf = newServerInterfaceNode(LCC, LIFECYCLE_CONTROLLER_SIGNATURE);
    } else {
      checkServerInterface(lccItf);
      // remove the interface form the interface container, since it is re-added
      itfContainer.removeInterface(lccItf);
    }
    setItfCode(lccItf, context, DEFAULT_COMPOSITE);
    return lccItf;
  }

  protected Interface getContentControllerInterface(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final TypeInterface ccItf = newServerInterfaceNode(CC,
        CONTENT_CONTROLLER_SIGNATURE);
    setItfCode(ccItf, context, DEFAULT_COMPOSITE);
    return ccItf;
  }
}
