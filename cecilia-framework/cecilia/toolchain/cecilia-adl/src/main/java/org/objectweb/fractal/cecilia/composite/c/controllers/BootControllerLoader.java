/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 *
 */

package org.objectweb.fractal.cecilia.composite.c.controllers;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.bindings.Binding;
import org.objectweb.fractal.adl.bindings.BindingContainer;
import org.objectweb.fractal.adl.components.ComponentContainer;

/**
 * Adds controllers interface for <code>"boot"</code> composite component.
 */
public class BootControllerLoader extends CompositeControllerLoader {

  // --------------------------------------------------------------------------
  // Overridden method
  // --------------------------------------------------------------------------

  @Override
  protected void checkComponent(final ComponentContainer container)
      throws ADLException {
    super.checkComponent(container);

    if (!(container instanceof BindingContainer))
      throw new ADLException("A component with 'boot' controller must have at"
          + " least a binding for the bootstrap component", container);

    boolean bootClientItfFound = false;
    for (final Binding element : ((BindingContainer) container).getBindings()) {
      final int index = element.getFrom().indexOf('.');
      final String itf = element.getFrom().substring(index + 1);
      if (itf.equals("boot")) {
        if (bootClientItfFound)
          throw new ADLException(
              "Two or more 'boot' client interfaces in 'boot' composite",
              element);
        bootClientItfFound = true;
      }
    }
    if (!bootClientItfFound)
      throw new ADLException("A composite with 'boot' controller must have an"
          + " internal binding with a 'boot' client interface", container);

  }
}
