/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Ali Erdem Ozcan
 */

package org.objectweb.fractal.adl;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.composition.CompositeTaskFactory;

/**
 * An abstract and generic visitor implementation that is intended to dispatch
 * visit requests on a collection of client visitors. This visitor returns a
 * composite task that contains the tasks returned by each client visitor. This
 * composite task is created using the composition schema specified by the
 * {@link #getTaskCompositionFileName() taskCompositionFileName} attribute. <br>
 * This class is abstract because it has a generic parameters. A sub-class must
 * be written for each type of visited node (see
 * {@link ComponentVisitorDispatcher} for instance). This ensure type safetyness
 * since this class is intended to be instantiated dynamically through
 * reflexivity.
 * 
 * @param <T> the type of the visited nodes.
 */
public abstract class VisitorDispatcher<T> extends AbstractTaskFactoryUser
    implements
      Visitor<T>,
      TaskCompositionAttribute {

  // ---------------------------------------------------------------------------
  // Client interfaces
  // ---------------------------------------------------------------------------

  /** The name of the {@link Compiler} client interface of this component. */
  public static final String     CLIENT_VISITOR          = "client-visitor";

  /** The builders client interfaces. */
  // use a LinkedHashMap to ensure the iteration order which improves
  // stability when debugging
  public Map<String, Visitor<T>> visitorsItf             = new LinkedHashMap<String, Visitor<T>>();

  // ---------------------------------------------------------------------------
  // Attributes
  // ---------------------------------------------------------------------------

  protected String               taskCompositionFileName = null;

  // ---------------------------------------------------------------------------
  // Abstract methods
  // ---------------------------------------------------------------------------

  /**
   * This method is used by {@link #bindFc(String, Object) bindFc} to cast the
   * given <code>serverItf</code> to a correct type. Sub-classes must
   * implements this method to guaranty type safety.
   */
  protected abstract Visitor<T> castVisitorInterface(Object serverItf);

  /**
   * Returns the parameter to be passed to the
   * {@link CompositeTaskFactory#newCompositeTask newCompositeTask} method.
   * Cannot return <code>null</code>.
   */
  protected abstract Object[] getCompositionParameters(final List<Node> path,
      final T node, final Map<Object, Object> context);

  // --------------------------------------------------------------------------
  // Implementation of the Visitor interface
  // --------------------------------------------------------------------------

  public Component visit(final List<Node> path, final T node,
      final Map<Object, Object> context) throws ADLException, TaskException {
    final List<Component> taskComponents = new ArrayList<Component>();
    for (final Visitor<T> visitorItf : visitorsItf.values()) {
      final Component task = visitorItf.visit(path, node, context);
      if (task != null) taskComponents.add(task);
    }
    // Create the composite task by applying the composition file to the task
    // component list.
    if (taskCompositionFileName == null)
      throw new TaskException(
          "No task composition file is specified as attribute.");
    return createTask(path, node, context, taskComponents);
  }

  // --------------------------------------------------------------------------
  // Utility methods
  // --------------------------------------------------------------------------

  /**
   * Retrieves the composition parameters using the
   * {@link #getCompositionParameters getCompositionParameters} method, then
   * creates and returns the composite task using the
   * {@link CompositeTaskFactory#newCompositeTask newCompositeTask} method.
   */
  protected Component createTask(final List<Node> path, final T node,
      final Map<Object, Object> context, final List<Component> taskComponents)
      throws ADLException, TaskException {
    return taskFactoryItf.newCompositeTask(taskComponents,
        taskCompositionFileName, null, getCompositionParameters(path, node,
            context));
  }

  /**
   * Utility method that can be used to implements the
   * {@link #getCompositionParameters getCompositionParameters} method. It
   * allows to creates an array of object in a concise way.
   */
  protected static Object[] toArray(final Object... parameters) {
    return parameters;
  }

  // --------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // --------------------------------------------------------------------------

  @Override
  public String[] listFc() {
    final List<String> interfaceList = new ArrayList<String>(visitorsItf
        .keySet());
    interfaceList.addAll(asList(super.listFc()));
    return interfaceList.toArray(new String[interfaceList.size()]);
  }

  @Override
  public void bindFc(final String clientItfName, final Object serverItf)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.startsWith(CLIENT_VISITOR))
      visitorsItf.put(clientItfName, castVisitorInterface(serverItf));
    else
      super.bindFc(clientItfName, serverItf);
  }

  @Override
  public Object lookupFc(final String clientItfName)
      throws NoSuchInterfaceException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.startsWith(CLIENT_VISITOR))
      return visitorsItf.get(clientItfName);
    else
      return super.lookupFc(clientItfName);
  }

  @Override
  public void unbindFc(final String clientItfName)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.startsWith(CLIENT_VISITOR)) {
      if (visitorsItf.remove(clientItfName) == null) {
        throw new NoSuchInterfaceException("There is no interface named '"
            + clientItfName + "'");
      }
    } else
      super.unbindFc(clientItfName);
  }

  // --------------------------------------------------------------------------
  // Implementation of the Attribute Controller interface
  // --------------------------------------------------------------------------

  public String getTaskCompositionFileName() {
    return this.taskCompositionFileName;
  }

  public void setTaskCompositionFileName(final String fileName) {
    this.taskCompositionFileName = fileName;
  }
}
