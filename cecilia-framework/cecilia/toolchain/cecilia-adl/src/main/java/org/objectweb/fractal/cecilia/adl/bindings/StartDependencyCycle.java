/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.bindings;

import java.util.List;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.Component;

/**
 * Exception thrown when a in a composite, no correct start order can be found.
 */
public class StartDependencyCycle extends ADLException {
  protected final List<Object> cycle;
  protected final Object       container;

  /**
   * Construct a new {@link StartDependencyCycle}.
   * 
   * @param cycle the list of sub component involved in the cycle.
   * @param container the container in which the cycle occurs.
   */
  public StartDependencyCycle(final List<Object> cycle, final Object container) {
    super("Unable to find correct start order", (Node) container);
    if (!(container instanceof Component || container instanceof Definition))
      throw new IllegalArgumentException(
          "container argument must be either a Component node or a "
              + "Definition node.");
    this.cycle = cycle;
    this.container = container;
  }

  @Override
  public String getMessage() {
    final String compName;
    if (container instanceof Component)
      compName = ((Component) container).getName();
    else
      compName = ((Definition) container).getName();
    final StringBuilder sb = new StringBuilder(super.getMessage());
    sb.append(": A cycle in start dependancy has been detected in ").append(
        "composite component \"").append(compName).append(
        "\". (component defined at: ")
        .append(((Node) container).astGetSource()).append(")\n");
    sb.append("Sub-components involved in this cycle are :");
    for (final Object component : cycle) {
      sb.append("\n  ").append(((Component) component).getName()).append(
          " (defined at ").append(((Node) component).astGetSource())
          .append(")");
    }
    return sb.toString();
  }

  /**
   * @return the container
   */
  public Object getContainer() {
    return container;
  }

  /**
   * @return the cycle
   */
  public List<Object> getCycle() {
    return cycle;
  }
}
