/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.adl;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.task.core.TaskException;

/**
 * Generic AST visitor interface.
 * 
 * @param <T> Type of the AST node.
 */
public interface Visitor<T> {

  /**
   * Visits an AST node and creates a component task that models the processing
   * to be done on this node.
   * 
   * @param path a list containing all the parent nodes of the visited node
   *            stating from the root of the AST.
   * @param node the AST Node to be visited.
   * @param context additional parameters.
   * @return a task component that models the processing to be done on this
   *         node.
   * @throws ADLException if something wrong is found in the AST.
   * @throws TaskException if something wrong happens during the instantiation
   *             of the task component.
   */
  Component visit(List<Node> path, T node, Map<Object, Object> context)
      throws ADLException, TaskException;
}
