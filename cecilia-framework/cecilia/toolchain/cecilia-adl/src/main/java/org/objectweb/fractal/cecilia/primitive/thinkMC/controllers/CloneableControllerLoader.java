/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 *
 */

package org.objectweb.fractal.cecilia.primitive.thinkMC.controllers;

import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.FACTORY;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.FACTORY_ALLOCATOR;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.FACTORY_SIGNATURE;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerInterface.FACTORY_ALLOCATOR_SIGNATURE;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;

/**
 * Adds controllers interface for <code>"cloneable"</code> components.
 */
public class CloneableControllerLoader extends PrimitiveControllerLoader {

  @Override
  protected List<Interface> getControllerInterfaces(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final List<Interface> controllerInterfaces = super.getControllerInterfaces(
        container, context);

    final Interface factoryInterface = getFactoryInterface(container, context);
    if (factoryInterface != null)
      controllerInterfaces.add(factoryInterface);

    final Interface factoryAllocatorInterface = getFactoryAllocatorInterface(
        container, context);
    if (factoryAllocatorInterface != null)
      controllerInterfaces.add(factoryAllocatorInterface);

    return controllerInterfaces;
  }

  /*
   * cloneable component always has at least the "factory-allocator" client
   * interface. So this method always return true.
   */
  @Override
  protected boolean isComponentNeedBindingControllerInterfaces(
      final InterfaceContainer container) {
    return true;
  }

  protected Interface getFactoryInterface(final ComponentContainer container,
      final Map<Object, Object> context) throws ADLException {
    final InterfaceContainer itfContainer = (InterfaceContainer) container;
    TypeInterface fItf = getInterface(FACTORY, itfContainer);
    if (fItf == null) {
      fItf = newServerInterfaceNode(FACTORY, FACTORY_SIGNATURE);
    } else {
      checkServerInterface(fItf);
      // remove the interface form the interface container, since it is
      // re-added
      itfContainer.removeInterface(fItf);
    }
    return fItf;
  }

  protected Interface getFactoryAllocatorInterface(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final InterfaceContainer itfContainer = (InterfaceContainer) container;
    TypeInterface allocItf = getInterface(FACTORY_ALLOCATOR, itfContainer);
    if (allocItf == null) {
      allocItf = newClientInterfaceNode(FACTORY_ALLOCATOR,
          FACTORY_ALLOCATOR_SIGNATURE);
    } else {
      checkClientInterface(allocItf);
      // remove the interface form the interface container, since it is
      // re-added
      itfContainer.removeInterface(allocItf);
    }
    return allocItf;
  }
}
