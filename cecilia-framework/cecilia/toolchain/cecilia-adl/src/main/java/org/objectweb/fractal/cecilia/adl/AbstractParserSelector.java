/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl;

import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Abstract class that can be used to implement a parser selector based on file
 * name extension.
 * 
 * @param <T> the type of the parsers.
 */
public abstract class AbstractParserSelector<T> {

  private final Map<String, T> availableParsers = new LinkedHashMap<String, T>();

  /**
   * Find a parser that can be used to parse the resource with the given
   * <code>signature</code>. This method tries to
   * {@link ClassLoader#getResource(String) find a resource} who's name is: <br>
   * <code>signature.replace('.', '/') + '.' + <i>extension</i></code> <br>
   * where <code><i>extension</i></code> is one the
   * {@link #getAvailableExtensions() available extensions} and returns the
   * associated parser.
   * 
   * @param signature the name of the resource to be parsed
   * @param context additional parameters.
   * @return A parser that can be used to parse the resource.
   * @throws ParserNotFoundException the no parser can be found.
   */
  protected T findParser(final String signature,
      final Map<Object, Object> context) throws ParserNotFoundException {
    ClassLoader cl = null;
    if (context != null) {
      cl = (ClassLoader) context.get("classloader");
    }
    if (cl == null) {
      cl = getClass().getClassLoader();
    }
    final String file = signature.replace('.', '/') + '.';

    for (final Map.Entry<String, T> binding : availableParsers.entrySet()) {
      final URL url = cl.getResource(file + binding.getKey());
      if (url != null) return binding.getValue();
    }

    final String errorMsg = "Cannot read '" + signature
        + "'. Checks that the file is in the source path and has a known"
        + " extension. (known extensions are: " + availableParsers.keySet()
        + ")";

    throw new ParserNotFoundException(errorMsg);
  }

  protected void addParser(final String extension, final T parser) {
    availableParsers.put(extension, parser);
  }

  protected void removeParser(final String extension) {
    availableParsers.remove(extension);
  }

  protected T getParser(final String extension) {
    return availableParsers.get(extension);
  }

  protected String[] getAvailableExtensions() {
    return availableParsers.keySet().toArray(new String[0]);
  }

  protected static final class ParserNotFoundException extends Exception {
    /**
     * @see Exception#Exception(String)
     */
    public ParserNotFoundException(final String message) {
      super(message);
    }
  }
}
