/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Ali Erdem Ozcan, Alessio Pace, Lionel Debroux
 */

package org.objectweb.fractal.cecilia.adl.idl;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.timestamp.Timestamp;
import org.objectweb.fractal.adl.util.FractalADLLogManager;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.file.SourceFile;
import org.objectweb.fractal.cecilia.adl.file.SourceFileProvider;
import org.objectweb.fractal.cecilia.adl.file.SourceFileWriter;
import org.objectweb.fractal.cecilia.adl.idl.ast.UnionDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.Executable;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component for {@link UnionDefinition} nodes.
 */
public class CUnionFileVisitor extends AbstractTaskFactoryUser
    implements
      IDLDefinitionVisitor {

  // The logger
  protected static Logger logger = FractalADLLogManager.getLogger("dep");

  // ---------------------------------------------------------------------------
  // Implementation of the IDLVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link UnionDefinition} nodes, and creates a task that creates a
   * source file containing the corresponding C code.
   */
  public Component visit(final List<Node> path,
      final IDLCompilationDesc idlCompilationDesc,
      final Map<Object, Object> context) throws ADLException, TaskException {
    IDLDefinition idlDefinition = idlCompilationDesc.getIdlDefinition();
    if (idlDefinition instanceof UnionDefinition) {
      return taskFactoryItf.newPrimitiveTask(new CUnionFileTask(
          (UnionDefinition) idlDefinition, (File) context
              .get(IDL_BUILD_DIRECTORY)), idlDefinition);
    } else
      return null;
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds interface definitions file and puts the source code that is received
   * for a given unionDefinitionNode. Provides a server interface giving access
   * to this file. Provides another server interface that returns the #include
   * file to be included in the files referencing the union defined by the given
   * unionDefinitionNode.
   */
  @TaskParameters("unionDefinitionNode")
  @ServerInterfaces({
      @ServerInterface(name = "c-union-definition-provider", signature = SourceCodeProvider.class, record = "role:cUnionHeaderSource, id:%", parameters = {"unionDefinitionNode"}),
      @ServerInterface(name = "c-union-header-file-provider", signature = SourceFileProvider.class, record = "role:cUnionHeaderFile, id:%", parameters = {"unionDefinitionNode"})})
  public static class CUnionFileTask
      implements
        Executable,
        SourceCodeProvider,
        SourceFileProvider {

    // constructor parameters
    protected final UnionDefinition unionDefinition;
    protected final File           idlBuildDirectory;

    // The produced source file
    protected SourceFile           sourceFile;
    // The produced source code
    protected String               sourceCode;
    // The name of the produced source file
    protected String               headerFileName;

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /** Client interface used to retrieve the content of the generated files. */
    @ClientInterface(name = "c-union-definition", record = "role:cUnionDefinition, id:%", parameters = "unionDefinitionNode")
    public SourceCodeProvider      cItfSourceItf;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param unionDefinition The union definition node for which this task must
     *            generate C source code.
     * @param idlBuildDirectory The directory into which the generated file will
     *            be placed.
     */
    public CUnionFileTask(final UnionDefinition unionDefinition,
        final File idlBuildDirectory) {
      this.unionDefinition = unionDefinition;
      this.idlBuildDirectory = idlBuildDirectory;
    }

    // -------------------------------------------------------------------------
    // Implementation of the Executable interface
    // -------------------------------------------------------------------------

    public void execute() throws Exception {
      // The execute method may be reentrant. To avoid stack overflow, generate
      // code only if it has not been already generated.
      if (sourceFile == null) {
        headerFileName = unionDefinition.getName().replace('.',
            File.separatorChar)
            + ".idl.h";
        prepareSourceCode();
        prepareSourceFile();
      }
    }

    // -------------------------------------------------------------------------
    // Implementation of the SourceCodeProvider interface
    // -------------------------------------------------------------------------

    public String getSourceCode() {
      return sourceCode;
    }

    // -------------------------------------------------------------------------
    // Implementation of the SourceFileProvider interface
    // -------------------------------------------------------------------------

    public SourceFile getSourceFile() {
      return sourceFile;
    }

    // -------------------------------------------------------------------------
    // Utility methods
    // -------------------------------------------------------------------------

    protected void prepareSourceCode() {
      final CodeWriter cw = new CodeWriter();
      cw.append("#include \"").append(headerFileName).append("\"").endl();
      sourceCode = cw.toString();
    }

    protected void prepareSourceFile() throws Exception {
      final File file = new File(idlBuildDirectory, headerFileName);
      sourceFile = new SourceFile(unionDefinition.getName(), file);

      if (Timestamp.isNodeMoreRecentThan(unionDefinition, file)) {
        SourceFileWriter.createOutputDir(file.getParentFile());
        SourceFileWriter.writeToFile(file, cItfSourceItf.getSourceCode());
      } else if (logger.isLoggable(Level.FINE)) {
        logger.fine("IDL file '" + file + "' is uptodate");
      }
    }
  }
}
