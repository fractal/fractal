/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.composite.c.types;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.task.core.TaskException;

/**
 * Visitor component that builds the definition of the interfaces of a cloneable
 * composite component.
 */
public class CloneableInterfaceDefinitionVisitor
    extends
      InterfaceDefinitionVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractInterfaceVisitor
  // ---------------------------------------------------------------------------

  /**
   * Creates a task that writes the source code containing the definition of the
   * interfaces of the component.
   */
  @Override
  protected Component createTask(final ComponentContainer container,
      final List<TypeInterface> itfs) throws ADLException, TaskException {
    final List<IDLDefinition> itfDefinitions = new ArrayList<IDLDefinition>();
    for (final Interface itf : itfs) {
      itfDefinitions.add(castNodeError(itf, IDLDefinitionContainer.class)
          .getIDLDefinition());
    }
    return taskFactoryItf.newPrimitiveTask(new CloneableInterfaceDefinitonTask(
        itfDefinitions), container, itfDefinitions);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds source code containing the definition of the interfaces for a
   * cloneable composite component. This task provides the produced source code.
   */
  public static class CloneableInterfaceDefinitonTask
      extends
        InterfaceDefinitonTask {

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param itfNodes the list of interface nodes belonging to the component.
     */
    protected CloneableInterfaceDefinitonTask(final List<IDLDefinition> itfNodes) {
      super(itfNodes);
      // TODO Auto-generated constructor stub
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      // generate the definition of the structure representing the component
      // type
      final CodeWriter cw = new CodeWriter("Cloneable Composite Interface Definition Builder");
      for (final IDLDefinition itf : itfNodes) {
        cw.appendln(cItfDefinitionItfs.get(itf).getSourceCode());
      }

      // defines the structure <name>_exporteds
      cw.append("struct ").append(typeNameProviderItf.getCTypeName()).append(
          "_exporteds {").endl();
      cw.append("Rfractal_api_Component           component;").endl();
      cw.append("Rfractal_api_BindingController   binding_controller;").endl();
      cw.append("Rfractal_api_LifeCycleController lifecycle_controller;")
          .endl();
      cw.append("Rfractal_api_ContentController   content_controller;").endl();
      cw.append("// The following structure is there because of the current ")
          .endl();
      cw.append("// definition of a template composite component and should")
          .endl();
      cw.append("// be removed later").endl();
      cw.append("struct {").endl();
      cw.append("unsigned char              nbSubComponents;").endl();
      cw.append("unsigned char              nbBindings;").endl();
      cw.append("unsigned char              started;").endl();
      cw.append("struct SubComponentDesc_t* subComponents;").endl();
      cw.append("struct BindingDesc_t*      bindings;").endl();
      cw.append("} __padding__;").endl();
      cw.append("Rfractal_api_Factory factory;").endl();
      cw.append("};").endl();

      return cw.toString();
    }
  }
}
