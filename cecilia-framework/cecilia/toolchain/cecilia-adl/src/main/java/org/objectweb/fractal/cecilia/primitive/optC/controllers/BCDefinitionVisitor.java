/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.primitive.optC.controllers;

import static org.objectweb.fractal.cecilia.adl.controllers.ControllerDecorationUtil.hasDefaultBindingController;

import java.util.List;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.interfaces.AbstractClientInterfaceVisitor;
import org.objectweb.fractal.cecilia.primitive.optC.controllers.PrimitiveControllerLoader;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the definition of the binding controller data
 * structure of the component for <code>ThinkMC</code> dialect.
 */
public class BCDefinitionVisitor extends AbstractClientInterfaceVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractClientInterfaceVisitor
  // ---------------------------------------------------------------------------

  @Override
  protected Component createTask(final ComponentContainer container,
      final List<TypeInterface> clientInterfaces) throws ADLException,
      TaskException {
    if (clientInterfaces.isEmpty()) {
      return null; // obviously...
    }
    else {
      // Don't create a default BC struct definition if user specified a
      // custom BC.
      if (!hasDefaultBindingController((InterfaceContainer) container)) {
        return null;
      }
      // Don't create a BC struct definition if the component doesn't
      // actually require a BC.
      if (PrimitiveControllerLoader.getBCdelegateRequiredByComponent(
              (InterfaceContainer) container) == 0) {
        return null;
      }
    }

    return taskFactoryItf.newPrimitiveTask(new BCPrimitiveDefinitionTask(
        container, clientInterfaces), container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds source code for the definition of the binding controller data
   * structure for the given component node. This task provides the produced
   * source code.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "bc-definition-provider", signature = SourceCodeProvider.class, record = "role:controllerDefinition, id:%, controller:BC", parameters = "componentNode"))
  public static class BCPrimitiveDefinitionTask extends AbstractDefinitionTask {
    // Component container
    final ComponentContainer container;

    // The length of the client interfaces array
    final List<TypeInterface> clientInterfaces;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param clientInterfacesLength the number of client interfaces.
     */
    public BCPrimitiveDefinitionTask(final ComponentContainer container,
        final List<TypeInterface> clientInterfaces) {
      this.container = container;
      this.clientInterfaces = clientInterfaces;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      int bcType = PrimitiveControllerLoader.getBCdelegateRequiredByComponent(
          (InterfaceContainer) container);

      final CodeWriter cw = new CodeWriter("Primitive Binding Controller Definition");

      cw.append("struct ").append(typeNameProviderItf.getCTypeName()).append(
          "_BC_data_type").append(" {").endl();
      cw.append("unsigned int nbimported;").endl();
      cw.append("struct {").endl();
      for (final TypeInterface itf : clientInterfaces) {
        final String itfName = itf.getName().replace('-', '_');
        cw.appendln("const char *name_" + itfName + ";");
        cw.append("void *itf_" + itfName).appendln(
            "; // In fact, this is a pointer on an interface reference");
        if (bcType == -1) {
          cw.appendln("jboolean isDynamicFalse_" + itfName + ";");
        }
      }
      cw.appendln("} imported[1];");
      cw.append("};").endl();
      return cw.toString();
    }

  }
}
