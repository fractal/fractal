/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 * Copyright (C) 2008-2009 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 * Contributors: Alessio Pace (merged component definition)
 *               Lionel Debroux (adaptation to Cecilia 2.x)
 */

package org.objectweb.fractal.cecilia.primitive.optC.components;

import static org.objectweb.fractal.api.type.TypeFactory.OPTIONAL;
import static org.objectweb.fractal.cecilia.adl.SourceCodeHelper.appendSortedSourceCodes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.ast.ASTFractalUtil;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.bindings.MergedBindingResolverLoader;
import org.objectweb.fractal.cecilia.adl.components.merge.MergeUtil;
import org.objectweb.fractal.cecilia.adl.components.merge.MergedImplementation;
import org.objectweb.fractal.cecilia.adl.components.merge.MergedInclude;
import org.objectweb.fractal.cecilia.adl.components.merge.MergedInterface;
import org.objectweb.fractal.cecilia.adl.components.merge.MergedInterfaceRole;
import org.objectweb.fractal.cecilia.adl.directives.Include;
import org.objectweb.fractal.cecilia.adl.components.merge.MergeUtil;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.idl.IDLDefinitionVisitor;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterfaceForEach;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the definition of the component data structure
 * of the component for <code>optC</code> dialect.
 */
public class MergedComponentDefinitionVisitor extends ComponentDefinitionVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link ComponentContainer} nodes and creates a task that writes the
   * source code containing the definition of the component data structure.
   */
  @Override
  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    if (MergeUtil.isMergedPrimitive(container)) {
      final List<IDLDefinition> innerInterfaceDefinitions = new ArrayList<IDLDefinition>();

      visitComponentContainer(container, innerInterfaceDefinitions);
      return taskFactoryItf.newPrimitiveTask(new MergedComponentDefinitionTask(
          container, innerInterfaceDefinitions), container, innerInterfaceDefinitions);
    }
    else {
      System.err.println("MergedComponentDefinitionVisitor was asked to visit "
          + container + " which is not a merged primitive !");
      return null;
    }
  }

  protected void visitComponentContainer(final ComponentContainer container,
      final List<IDLDefinition> innerInterfaceDefinitions)
      throws ADLException, TaskException {

    for (final org.objectweb.fractal.adl.components.Component subComp : container
        .getComponents()) {
      visitComponentContainer(subComp, innerInterfaceDefinitions);
    }
    if (container instanceof ImplementationContainer) {
      visitImplementationContainer((ImplementationContainer)container, innerInterfaceDefinitions);
    }
  }

  // Code copied&pasted, and slightly modified, from that in
  // MergedPrimitiveRecursiveIDLDispatchVisitor.
  protected void visitImplementationContainer(final ImplementationContainer container,
      final List<IDLDefinition> innerInterfaceDefinitions)
      throws ADLException {
    /*
     * travel down the includes to get the IDL used by the included modules. The
     * container is a primitive which could be a singleton or not.
     */
    final Implementation implementation = container.getImplementation();
    if (implementation != null) {

      final Include[] includes = ASTFractalUtil.getIncludes(implementation);
      for (final Include include : includes) {
        /* iterate over the interfaces of the current Include */
        for (final Interface itf : ((InterfaceContainer) include).getInterfaces()) {

          final MergedInterface mergedItf = (MergedInterface) itf;

          final MergedInterfaceRole mergedItfRole = MergedBindingResolverLoader
              .getMergedInterfaceRole(mergedItf);

          /*
           * dispatch compilation ONLY if it is either a CLIENT_INNER or a
           * SERVER_INNER interface, because the IDL compilation has been already
           * dispatched during the enclosing merged primitive processing for the
           * CLIENT_OUTER and SERVER_OUTER IDL interface types.
           */
          if (mergedItfRole == MergedInterfaceRole.CLIENT_INNER
              || mergedItfRole == MergedInterfaceRole.SERVER_INNER) {
            final IDLDefinition idl = ((IDLDefinitionContainer) mergedItf)
                .getIDLDefinition();

            innerInterfaceDefinitions.add(idl);
          }
        }
      }
    }
  }

  /**
   * Builds source code for the definition of the component data structure for
   * the given component node. This task provides the produced source code. It
   * uses the source code provided for client/server interfaces, controllers,
   * and the attributes of the component.
   */
  @TaskParameters({"componentNode", "innerInterfaceDefinitions"})
  @ServerInterfaces(@ServerInterface(name = "component-definition-provider", signature = SourceCodeProvider.class, record = "role:componentDefinition, id:%", parameters = "componentNode"))
  public static class MergedComponentDefinitionTask
      extends AbstractComponentDefinitionTask {

    protected List<IDLDefinition> innerInterfaceDefinitions;

    public MergedComponentDefinitionTask(final ComponentContainer container,
        final List<IDLDefinition> innerInterfaceDefinitions) {
      this.container = container;
      this.innerInterfaceDefinitions = innerInterfaceDefinitions;
    }

    /**
     * Source code to be included to access the interface definition header
     * files (idl.h) for each imported interface of the componentNode.
     */
    @ClientInterfaceForEach(iterable = "innerInterfaceDefinitions", prefix = "inner-idl-header", signature = SourceCodeProvider.class, record = "role:cInterfaceDefinition, id:%", parameters = "innerInterfaceDefinitions.element")
    public final Map<IDLDefinition, SourceCodeProvider> cInnerInterfaceDefinitionItfs      = new LinkedHashMap<IDLDefinition, SourceCodeProvider>();

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------
    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter("Merged Primitive Definition Builder");
      final String componentCName = typeNameProviderItf.getCTypeName();

      generateRegularPrimitiveDefinition(cw, componentCName);

      /*
       * Define function prototypes for server inner interfaces so that the
       * various fused modules can call each other.
       */

      final Include[] includes = ASTFractalUtil
          .getIncludes(((ImplementationContainer) container).getImplementation());

      for (final Include include : includes) {
        final MergedInclude mergedInclude = (MergedInclude) include;
        for (final Interface itf : mergedInclude.getInterfaces()) {
          final MergedInterface mergedInterface = (MergedInterface) itf;

          final MergedInterfaceRole mergedInterfaceRole = MergedBindingResolverLoader
              .getMergedInterfaceRole(mergedInterface);
          if (mergedInterfaceRole == null) {
            throw new IllegalStateException("The merged interface "
                + mergedInterface.getName() + " on the include "
                + mergedInclude.getId() + " does not specify a "
                + MergedInterfaceRole.class);
          }

          if (mergedInterfaceRole == MergedInterfaceRole.SERVER_INNER) {
            final IDLDefinition interfaceDefinition = ((IDLDefinitionContainer) mergedInterface)
                .getIDLDefinition();
            if (interfaceDefinition == null) {
              throw new IllegalStateException("The merged interface "
                  + mergedInterface.getName() + " on the include "
                  + mergedInclude.getId()
                  + " does not enclose an InterfaceDefinition");
            }

            String interfaceDefinitionFullName = interfaceDefinition.getName();

            System.out.println("\tDefining inner server interface method definitions for "
                    + interfaceDefinitionFullName);
            cw
                .appendln("/* This interface is used only within the merged components */");
            cw.appendln(cInnerInterfaceDefinitionItfs.get(interfaceDefinition).getSourceCode());

            String interfaceMacroName = interfaceDefinitionFullName
                .toUpperCase().replace('.', '_');

            if (MergeUtil.hasToBeCompiledAsStatic(mergedInterface)) {
              /* if the interface is static, use the correct include name */
              interfaceMacroName += IDLDefinitionVisitor.STATIC_POSTFIX.toUpperCase();
            }

            cw.appendln(interfaceMacroName
                    + "_METHOD_DEFINITION("
                    + componentCName
                    + ","
                    + MergeUtil
                        .translateServerInnerItfName(mergedInterface) + ") ;")
                .endl();
          }
        }
      }

      return cw.toString();
    }

    @Override
    protected void writeInstanceDataMember(final CodeWriter cw) {
      cw.append(" const ");
      super.writeInstanceDataMember(cw);
    }
  }
}
