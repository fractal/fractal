/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.cecilia.adl.components.PrimitiveChecker;

/**
 * Basic implementation of the {@link PrimitiveChecker} interface. This
 * implementation delegates definition compilation requests to a set of
 * {@link PrimitiveChecker primitive-checkers} according to the alphabetical
 * order of their client interface names.
 */
public class PrimitiveCheckerDispatcher
    implements
      PrimitiveChecker,
      BindingController {
  /**
   * Name of the collection interface bound to the {@link PrimitiveChecker
   * PrimitiveCheckers} used by this checker.
   */
  public static final String           PRIMITIVE_CHECKERS   = "client-checker";

  /**
   * The primitive checkers used by this checker.
   */
  public Map<String, PrimitiveChecker> primitiveCheckersItf = new TreeMap<String, PrimitiveChecker>();

  // ---------------------------------------------------------------------------
  // Implementation of the PrimitiveChecker interface
  // ---------------------------------------------------------------------------

  public void beforeSubComponentCheck(final List<ComponentContainer> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    for (final PrimitiveChecker checker : primitiveCheckersItf.values()) {
      checker.beforeSubComponentCheck(path, container, context);
    }
  }

  public void afterSubComponentCheck(final List<ComponentContainer> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    for (final PrimitiveChecker checker : primitiveCheckersItf.values()) {
      checker.afterSubComponentCheck(path, container, context);
    }
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  public String[] listFc() {
    return primitiveCheckersItf.keySet().toArray(
        new String[primitiveCheckersItf.size()]);
  }

  public Object lookupFc(final String itf) {
    if (itf.startsWith(PRIMITIVE_CHECKERS)) {
      return primitiveCheckersItf.get(itf);
    }
    return null;
  }

  public void bindFc(final String itf, final Object value) {
    if (itf.startsWith(PRIMITIVE_CHECKERS)) {
      primitiveCheckersItf.put(itf, (PrimitiveChecker) value);
    }
  }

  public void unbindFc(final String itf) {
    if (itf.startsWith(PRIMITIVE_CHECKERS)) {
      primitiveCheckersItf.remove(itf);
    }
  }
}
