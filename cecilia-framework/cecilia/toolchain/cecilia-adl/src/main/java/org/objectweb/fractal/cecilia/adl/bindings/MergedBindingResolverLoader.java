/***
 * Cecilia ADL Compiler
 * Copyright (C) 2005-2008 STMicroelectronics
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.bindings;

import java.util.Map;
import java.util.HashMap;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.ast.ASTFractalUtil;
import org.objectweb.fractal.adl.bindings.Binding;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.cecilia.adl.components.MergeableComponent;
import org.objectweb.fractal.cecilia.adl.components.merge.MergeUtil;
import org.objectweb.fractal.cecilia.adl.components.merge.MergedImplementation;
import org.objectweb.fractal.cecilia.adl.components.merge.MergedInclude;
import org.objectweb.fractal.cecilia.adl.components.merge.MergedInterface;
import org.objectweb.fractal.cecilia.adl.components.merge.MergedInterfaceRole;
import org.objectweb.fractal.cecilia.adl.directives.Include;
import org.objectweb.fractal.cecilia.adl.directives.IncludeContainer;
import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;

/**
 * Extends the {@link BindingResolverLoader} to add decorations also for
 * interfaces and bindings inside a merged primitive. In particular it adds:
 * <ul>
 * <li> {@link BindingDecorationUtil#CLIENT_INTERFACE_DECORATION} on each
 * binding node. It is set to the client interface node designated by the
 * {@link Binding#getFrom() client} attribute. </li>
 * <li> {@link BindingDecorationUtil#SERVER_INTERFACE_DECORATION} on each
 * binding node. It is set to the server interface node designated by the
 * {@link Binding#getTo() server} attribute. </li> *
 * <li> {@link InterfaceDecorationUtil#CONTAINER_DECORATION} on each interface
 * node. It is set to the interface container node (i.e. the component or
 * include node that contains this interface node).</li>
 * </ul>
 *
 * @author Alessio Pace
 */
public class MergedBindingResolverLoader extends BindingResolverLoader {

  /**
   *
   */
  public static final String INNER_BOUND_FROM                 = "INNER_BOUND_FROM";
  /**
   *
   */
  public static final String INNER_BOUND_TO                   = "INNER_BOUND_TO";
  /**
   *
   */
  public static final String MERGED_INTERFACE_ROLE_DECORATION = "MERGED_INTERFACE_ROLE";

  // --------------------------------------------------------------------------
  // Overridden BindingLoader methods
  // --------------------------------------------------------------------------

  @Override
  protected boolean ignoreBinding(final Binding binding,
      final String fromCompName, final String fromItfName,
      final String toCompName, final String toItfName) {
    return BindingDecorationUtil.isInnerBinding(binding);
  }

  @Override
  protected void addDecoration(final Object node,
      final Map<Object, Object> context) {

    super.addDecoration(node, context);

    /*
     * this will obviously not executed if the passed node argument is an
     * Include
     */
    if (node instanceof ComponentContainer) {
      final MergeableComponent component = (MergeableComponent) node;
      if (ASTFractalUtil.isPrimitive(component)) {

        /*
         * these checks and decorations are done only in case of a merged
         * primitive
         */
        if (MergeUtil.isMergedPrimitive(component)) {
          final MergedImplementation mergedImplementation = (MergedImplementation) ASTFractalUtil
              .getFcContentDesc(component);

          /*
           * decorate each Include's <interface> with the owner interface
           * decoration which will have as value the Include itself
           */
          for (final Include i : ASTFractalUtil
              .getIncludes(mergedImplementation)) {
            addDecoration(i, context);
          }

          /*
           * check inner bindings and set the client/server decorations on each
           * Binding node
           */
          checkMergedPrimitiveInnerBindings(component, mergedImplementation);
        }
      }
    }
  }

  /**
   * Add the Includes to the interface map.
   */
  @Override
  protected void getAdditionalInterfaces(final Object node,
      final Map<Object, Object> context,
      final Map<String, Map<String, Interface>> itfMap) throws ADLException {
    if (node instanceof ImplementationContainer) {
      Implementation impl = ((ImplementationContainer)node).getImplementation();
      if (impl instanceof IncludeContainer) {
        for (final Include incl : ((IncludeContainer) impl).getIncludes()) {
          if (incl instanceof InterfaceContainer
              && incl instanceof MergedInclude) {
            final Map<String, Interface> inclItfs = new HashMap<String, Interface>();
            for (final Interface itf : ((InterfaceContainer) incl)
                .getInterfaces()) {
              inclItfs.put(itf.getName(), itf);
            }
            itfMap.put(((MergedInclude)incl).getId(), inclItfs);
          }
        }
      }
    }
  }

  /**
   * Decorate each {@link Binding} with a
   * {@link BindingDecorationUtil#CLIENT_INTERFACE_DECORATION} and
   * {@link BindingDecorationUtil#SERVER_INTERFACE_DECORATION} with the
   * {@link TypeInterface} nodes forming the binding, being them on the merged
   * primitive or on one of its includes.
   *
   * @param mergedPrimitive
   * @param mergedImplementation
   */
  protected void checkMergedPrimitiveInnerBindings(
      final MergeableComponent mergedPrimitive,
      final MergedImplementation mergedImplementation) {

    for (final Binding b : ASTFractalUtil.getBindings(mergedPrimitive)) {

      final TypeInterface fromItf = getFromItf(b, mergedPrimitive,
          mergedImplementation);

      if (fromItf == null) {
        throw new IllegalStateException(
            "Null interface node found for the binding 'from': " + b.getFrom());
      }

      final TypeInterface toItf = getToItf(b, mergedPrimitive,
          mergedImplementation);

      if (toItf == null) {
        throw new IllegalStateException(
            "Null interface node found for the binding 'to': " + b.getTo());
      }

      /* decorate with client interface */
      BindingDecorationUtil.setClientInterface(b, fromItf);

      /* decorate with server interface */
      BindingDecorationUtil.setServerInterface(b, toItf);

      /*
       * decorate the "from" itf of this binding with the INNER_BOUND_TO
       * decoration
       */
      setInnerBoundTo(fromItf, toItf);

      /*
       * decorate the "to" interface of this binding with the INNER_BOUND_FROM
       * decoration
       */
      setInnerBoundFrom(fromItf, toItf);

      final InterfaceContainer fromItfOwner = InterfaceDecorationUtil
          .getContainer(fromItf);
      if (fromItfOwner == null) {
        throw new IllegalStateException(
            "The interface owner decoration should have been set on the interface named: "
                + fromItf.getName());
      }
      final InterfaceContainer toItfOwner = InterfaceDecorationUtil
          .getContainer(toItf);
      if (toItfOwner == null) {
        throw new IllegalStateException(
            "The interface owner decoration should have been set on the interface named: "
                + fromItf.getName());
      }
      /* decorate with the merged role */
      if (fromItfOwner == mergedPrimitive) {
        setMergedInterfaceRoleDecoration(toItf,
            MergedInterfaceRole.SERVER_OUTER);
      } else {
        if (toItfOwner == mergedPrimitive) {
          setMergedInterfaceRoleDecoration(fromItf,
              MergedInterfaceRole.CLIENT_OUTER);
        } else if (toItfOwner != mergedPrimitive) {
          setMergedInterfaceRoleDecoration(fromItf,
              MergedInterfaceRole.CLIENT_INNER);
        }
      }

    }

    checkAllInnerInterfacesAreBound(mergedPrimitive);
  }

  @Deprecated
  protected static void setInnerBoundFrom(final TypeInterface fromItf,
      final TypeInterface toItf) {

    ((Node) toItf).astSetDecoration(INNER_BOUND_FROM, fromItf);

  }

  @Deprecated
  public static TypeInterface getInnerBoundFrom(final TypeInterface ti) {
    return (TypeInterface) ((Node) ti).astGetDecoration(INNER_BOUND_FROM);
  }

  protected static void setInnerBoundTo(final TypeInterface fromItf,
      final TypeInterface toItf) {
    ((Node) fromItf).astSetDecoration(INNER_BOUND_TO, toItf);
  }

  public static TypeInterface getInnerBoundTo(final TypeInterface ti) {
    return (TypeInterface) ((Node) ti).astGetDecoration(INNER_BOUND_TO);
  }

  /**
   * Checks that the server and client interfaces of the merged primitive are
   * indeed connected internally to interfaces of the fused modules, and that
   * there are no fused module client interfaces which are not bound.
   *
   * @param mergedPrimitive
   */
  protected void checkAllInnerInterfacesAreBound(
      final MergeableComponent mergedPrimitive) {

    /*
     * server interfaces of the merged primitive should be all bounded with an
     * INNER_BOUND_TO pointing to an interface provided by a fused module.
     */
    for (final TypeInterface ti : ASTFractalUtil
        .getFcFunctionalServerInterfaces(mergedPrimitive)) {

      final TypeInterface boundTo = getInnerBoundTo(ti);
      if (boundTo == null) {
        throw new IllegalStateException("The server interface " + ti.getName()
            + " on the merged primitive "
            + ASTFractalUtil.getFcName(mergedPrimitive)
            + " is not internally bound to any fused module");
      }
    }

    final Implementation mergedImplementationNode = ASTFractalUtil
        .getFcContentDesc(mergedPrimitive);
    for (final Include include : ASTFractalUtil
        .getIncludes(mergedImplementationNode)) {
      final MergedInclude mergedInclude = (MergedInclude) include;
      for (final Interface itf : mergedInclude.getInterfaces()) {
        final MergedInterface mergedInterface = (MergedInterface) itf;

        /*
         * client interfaces of fused modules should have an INNER_BOUND_TO
         * decoration
         */
        if (mergedInterface.getRole().equals(TypeInterface.CLIENT_ROLE)) {
          final TypeInterface innerBoundTo = getInnerBoundTo(mergedInterface);
          if (innerBoundTo == null) {
            throw new IllegalStateException(
                "The client interface '"
                    + mergedInterface.getName()
                    + "' of the fused include '"
                    + mergedInclude.getId()
                    + "' is not bound to any server interface (neither of another include nor the merged primitive)");
          }
        } else {
          /*
           * if it is a server role, than it could have been already marked as
           * SERVER_OUTER during the binding, otherwise mark it as SERVER_INNER
           */
          if (getMergedInterfaceRole(mergedInterface) == null) {
            setMergedInterfaceRoleDecoration((TypeInterface) itf,
                MergedInterfaceRole.SERVER_INNER);
          }
        }
      }
    }
  }

  public static void setMergedInterfaceRoleDecoration(final TypeInterface itf,
      final MergedInterfaceRole role) {

    ((Node) itf).astSetDecoration(MERGED_INTERFACE_ROLE_DECORATION, role);

  }

  public static MergedInterfaceRole getMergedInterfaceRole(
      final TypeInterface itf) {
    return (MergedInterfaceRole) ((Node) itf)
        .astGetDecoration(MERGED_INTERFACE_ROLE_DECORATION);
  }

  protected TypeInterface getToItf(final Binding b,
      final MergeableComponent mergedPrimitive,
      final MergedImplementation mergedImplementation) {

    final String to = b.getTo();

    if (to == null) {
      throw new IllegalStateException("Binding has null 'to' attribute");
    }

    final String[] toTokens = to.split("[.]");

    final String toCompName = toTokens[0];
    final String toItfName = toTokens[1];

    return getItf(b, toCompName, toItfName, mergedPrimitive,
        mergedImplementation);
  }

  protected TypeInterface getFromItf(final Binding b,
      final MergeableComponent mergedPrimitive,
      final MergedImplementation mergedImplementation) {

    final String from = b.getFrom();

    if (from == null) {
      throw new IllegalStateException("Binding has null 'from' attribute");
    }

    final String[] fromTokens = from.split("[.]");
    final String fromCompName = fromTokens[0];
    final String fromItfName = fromTokens[1];

    return getItf(b, fromCompName, fromItfName, mergedPrimitive,
        mergedImplementation);
  }

  protected TypeInterface getItf(final Binding b, final String compName,
      final String itfName, final MergeableComponent mergedPrimitive,
      final MergedImplementation mergedImplementation) {
    if (compName.equals("this")) {
      return ASTFractalUtil.getFcInterface(mergedPrimitive, itfName);
    } else {
      final MergedInclude include = MergeUtil.getIncludeById(
          mergedImplementation, compName);
      if (include == null) {
        throw new IllegalStateException("Unable to get the include with id '"
            + compName + "' in the merged primitive named '"
            + ASTFractalUtil.getFcName(mergedPrimitive) + "'");
      }
      return ASTFractalUtil.getFcInterface(include, itfName);
    }
  }

}
