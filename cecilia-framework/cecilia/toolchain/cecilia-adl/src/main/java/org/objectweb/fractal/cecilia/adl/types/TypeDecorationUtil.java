/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.types;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.ast.ASTFractalUtil;
import org.objectweb.fractal.adl.util.Printer;

/**
 * Utility class to manipulate Type decorations of {@link ComponentContainer}
 * node.
 */
public final class TypeDecorationUtil {
  /**
   * A decoration set of {@link ComponentContainer} nodes that give the AST that
   * defines the component type.
   */
  public static final String TYPE_DECORATION                             = "type";

  /**
   * A decoration to keep track of the number of instances of a certain
   * component type.
   */
  public static final String COMPONENT_TYPE_INSTANCES_COUNTER_DECORATION = "instances-counter";

  /**
   * A decoration set on a {@link ComponentContainer} node to specify if the
   * given component type is a factory component ("cloneable").
   */
  public static final String IS_CLONEABLE_DECORATION                     = "is-cloneable";

  /**
   *
   */
  public static final String CECILIA_DISABLE_SINGLETONS_OPTIMIZATION     = "CECILIA_DISABLE_SINGLETONS_OPTIMIZATION";

  /**
   * Private constructor to prevent instantiation.
   */
  private TypeDecorationUtil() {
    // empty.
  }

  /**
   * Set the {@link #TYPE_DECORATION} decoration to the given component node
   * with the given type value.
   * 
   * @param component a component node.
   * @param type the type of the component node.
   * @throws IllegalArgumentException if the given component node already
   *             contains a {@link #TYPE_DECORATION} decoration.
   */
  public static void setTypeDecoration(final ComponentContainer component,
      final ComponentContainer type) {
    final Node compNode = component;
    if (compNode.astGetDecoration(TYPE_DECORATION) != null) {
      throw new IllegalArgumentException(
          "The given component node has already a \"" + TYPE_DECORATION
              + "\" decoration.");
    }
    compNode.astSetDecoration(TYPE_DECORATION, type);
  }

  /**
   * Returns the value of the {@link #TYPE_DECORATION} decoration for the given
   * component node.
   * 
   * @param component a component node.
   * @return the value of the {@link #TYPE_DECORATION} decoration for the given
   *         component node or <code>null</code> if the given node has no such
   *         decoration.
   */
  public static ComponentContainer getTypeDecoration(
      final ComponentContainer component) {

    if (component == null) {
      throw new IllegalArgumentException("Component arg can't be null");
    }

    return (ComponentContainer) ((Node) component)
        .astGetDecoration(TYPE_DECORATION);
  }

  /**
   * @param container the {@link ComponentContainer} representing a component
   *            type.
   */
  @SuppressWarnings("unchecked")
  public static void incrementComponentTypeInstancesCount(
      final ComponentContainer container) {

    final Node node = ((Node) container);
    List<Integer> currentCounter = null;
    currentCounter = (List<Integer>) node
        .astGetDecoration(TypeDecorationUtil.COMPONENT_TYPE_INSTANCES_COUNTER_DECORATION);

    if (currentCounter == null) {
      currentCounter = new ArrayList<Integer>(1);
      currentCounter.add(1);
    } else {
      final int actualCounter = currentCounter.remove(0);
      currentCounter.add(actualCounter + 1);
    }

    node.astSetDecoration(
        TypeDecorationUtil.COMPONENT_TYPE_INSTANCES_COUNTER_DECORATION,
        currentCounter);
  }

  @SuppressWarnings("unchecked")
  public static List<Integer> getComponentTypeInstancesCount(
      final ComponentContainer componentType) {

    final Node node = ((Node) componentType);

    return (List<Integer>) node
        .astGetDecoration(TypeDecorationUtil.COMPONENT_TYPE_INSTANCES_COUNTER_DECORATION);

  }

  /**
   * Set the {@link #IS_CLONEABLE_DECORATION} decoration to on the
   * <code>componentContainer</code> using the given boolean
   * <code>value</code>.
   * 
   * @param componentContainer
   * @param value
   */
  public static void setCloneableTypeDecoration(
      final ComponentContainer componentContainer, final Boolean value) {

    ((Node) componentContainer)
        .astSetDecoration(IS_CLONEABLE_DECORATION, value);
  }

  /**
   * Return the {@link #IS_CLONEABLE_DECORATION} decoration value, or
   * <code>false</code> if no value is found.
   * 
   * @param componentContainer
   * @return the {@link #IS_CLONEABLE_DECORATION} decoration value, or
   *         <code>false</code> if no value is found.
   */
  public static Boolean getCloneableTypeDecorationDefaultFalse(
      final ComponentContainer componentContainer) {

    final Boolean result = (Boolean) ((Node) componentContainer)
        .astGetDecoration(IS_CLONEABLE_DECORATION);

    /* return false by default */
    if (result == null) {
      return false;
    }
    return result;
  }

  /**
   * Return <code>true</code> if the given <code>componentType</code>
   * (represented by the {@link ComponentContainer} argument) <strong>has one
   * instance and is not cloneable</strong>.
   * 
   * @param componentType
   * @return <code>true</code> if the given <code>componentType</code>
   *         (represented by the {@link ComponentContainer} argument)
   *         <strong>has one instance and is not cloneable</strong>.
   */
  public static Boolean isSingletonComponentType(
      final ComponentContainer componentType) {

    if (componentType == null) {
      throw new IllegalArgumentException("Component arg can't be null");
    }

    if (TypeDecorationUtil.getTypeDecoration(componentType) != componentType) {
      throw new IllegalStateException(
          "Are you sure you passed a componentType and not just a component instance as parameter?");
    }

    final String disableSingletonsProperty = System
        .getProperty(CECILIA_DISABLE_SINGLETONS_OPTIMIZATION, "false");
    if (disableSingletonsProperty.equals("true")) {
      Printer.debug("Singletons optimization disabled: TypeDecorationUtil.isSingletonComponentType returning false");
      return false;
    }

    Boolean isSingleton = false;

    final List<Integer> instancesCounter = TypeDecorationUtil
        .getComponentTypeInstancesCount(componentType);

    if (instancesCounter == null) {
      throw new IllegalStateException(
          "Can't use this method on a componentType with no "
              + TypeDecorationUtil.COMPONENT_TYPE_INSTANCES_COUNTER_DECORATION
              + " decoration");
    }

    /* should get the value of the first element */
    final Integer counter = instancesCounter.get(0);

    final Boolean isCloneable = TypeDecorationUtil
        .getCloneableTypeDecorationDefaultFalse(componentType);

    if (counter == 1 && !isCloneable) {
      isSingleton = true;
    }

    return isSingleton;
  }
}
