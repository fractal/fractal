/**
 * Cecilia ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.bindings;

import static org.objectweb.fractal.adl.error.ErrorTemplateValidator.validErrorTemplate;

import org.objectweb.fractal.adl.error.ErrorTemplate;

/**
 * {@link ErrorTemplate} group for the bindings package. This enumeration
 * complete the {@link org.objectweb.fractal.adl.bindings.BindingErrors
 * fractalADL BindingErrors enumeration}. {@link #getErrorId() errorIds} defined
 * in this enumeration starts at <code>100</code>.
 */
public enum BindingErrors implements ErrorTemplate {

  /** */
  COMPOSITE_CLIENT_COLLECTION(
      "Client collection interface on composite must have the same name as the client collection interface it exports"),

  /** */
  COMPOSITE_SERVER_COLLECTION(
      "Server collection interface on composite is not yet supported"),

  /** */
  INVALID_DYNAMIC_FALSE(
      "Binding cannot be declared dynamic=\"false\" because it cannot be statically initialized");

  private static final int ORDINAL_OFFSET = 100;

  private int              id;
  private String           format;

  private BindingErrors(final String format, final Object... args) {
    this.id = ORDINAL_OFFSET + ordinal();
    this.format = format;

    assert validErrorTemplate(this, args);
  }

  public int getErrorId() {
    return id;
  }

  public String getGroupId() {
    return org.objectweb.fractal.adl.bindings.BindingErrors.GROUP_ID;
  }

  public String getFormatedMessage(final Object... args) {
    return String.format(format, args);
  }

  public String getFormat() {
    return format;
  }
}
