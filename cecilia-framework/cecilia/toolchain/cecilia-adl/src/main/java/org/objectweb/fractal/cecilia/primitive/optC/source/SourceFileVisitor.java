/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.primitive.optC.source;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;
import static org.objectweb.fractal.api.type.TypeFactory.OPTIONAL;
import static org.objectweb.fractal.cecilia.adl.SourceCodeHelper.appendSortedSourceCodes;
import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationDecorationUtil.getCode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.TypeNameProvider;
import org.objectweb.fractal.cecilia.adl.directives.Include;
import org.objectweb.fractal.cecilia.adl.directives.IncludeContainer;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.file.SourceFile;
import org.objectweb.fractal.cecilia.adl.file.SourceFileProvider;
import org.objectweb.fractal.cecilia.adl.file.SourceFileWriter;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.Executable;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.TaskFactory;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the source file containing the components'
 * definition and instance code.
 */
public class SourceFileVisitor extends AbstractTaskFactoryUser
    implements
      ComponentVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits a {@link ComponentContainer} node and creates a task that creates
   * the source file containing the components' definition and instance code.
   */
  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    final Implementation impl = castNodeError(container,
        ImplementationContainer.class).getImplementation();
    if (impl == null) {
      throw new CompilerError(GenericErrors.INTERNAL_ERROR,
          "This visitor is only applicable for primitive component.");
    }
    final File adlBuildDirectory = (File) context.get("adlBuildDirectory");
    final Component implTask = createSourceFileTask(container, impl,
        adlBuildDirectory);
    if (impl instanceof IncludeContainer
        && ((IncludeContainer) impl).getIncludes().length != 0) {
      final Include[] includes = ((IncludeContainer) impl).getIncludes();

      final Collection<Component> tasks = new ArrayList<Component>(
          includes.length + 1);
      tasks.add(implTask);

      final Set<String> moduleNames = new HashSet<String>();
      for (final Include include : includes) {
        // if the module is an assembly, file, it should not be included.
        if (!((SourceFile) getCode((Node) include)).isAssemblyFile()) {
          // find a unique module name.
          String moduleName = include.getFile();

          // remove package name (if any)
          final int i = moduleName.lastIndexOf('.');
          if (i != -1) {
            moduleName = moduleName.substring(i);
          }

          if (!moduleNames.add(moduleName)) {
            // a module with the same name already exist, append a number as
            // suffix.
            int suffix = 1;
            // increment the suffix until a new name is found.
            while (!moduleNames.add(moduleName + suffix))
              suffix++;

            moduleName = moduleName + suffix;
          }

          tasks.add(createModuleSourceFileTask(container, include, moduleName,
              adlBuildDirectory));
        }
      }
      return taskFactoryItf.newCompositeTask(tasks, TaskFactory.EXPORT_ALL,
          null);
    } else {
      return implTask;
    }
  }

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  protected Component createModuleSourceFileTask(
      final ComponentContainer container, final Include include,
      final String moduleName, final File adlBuildDirectory)
      throws TaskException {
    return taskFactoryItf.newPrimitiveTask(new ModuleSourceFileTask(
        adlBuildDirectory, moduleName), container, include);
  }

  protected Component createSourceFileTask(final ComponentContainer container,
      final Implementation impl, final File adlBuildDirectory)
      throws TaskException {
    return taskFactoryItf.newPrimitiveTask(
        new SourceFileTask(adlBuildDirectory), container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  protected abstract static class AbstractSourceFileTask
      implements
        Executable,
        SourceFileProvider {
    protected final File      adlBuildDirectory;

    // Produced source file
    protected SourceFile      sourceFile;

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /** The name of the component type. */
    @ClientInterface(name = "type-name-provider", record = "role:typeNameProvider, id:%", parameters = "componentNode")
    public TypeNameProvider   typeNameProviderItf;

    /** Source code for the component definition. */
    @ClientInterface(name = "component-definition-provider", record = "role:componentDefinition, id:%", parameters = "componentNode")
    public SourceCodeProvider definitionProviderItf;

    /** Source code for the implementation code. */
    @ClientInterface(name = "implementation-provider", record = "role:implementation, id:%", parameters = "componentNode")
    public SourceCodeProvider implementationProviderItf;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param adlBuildDirectory The directory into which the generated file will
     *          be placed.
     */
    public AbstractSourceFileTask(final File adlBuildDirectory) {
      this.adlBuildDirectory = adlBuildDirectory;
    }

    // -------------------------------------------------------------------------
    // Abstract methods
    // -------------------------------------------------------------------------

    protected abstract void prepareSourceCode(CodeWriter cw);

    protected abstract String getFileName();

    protected abstract String getSignature();

    // -------------------------------------------------------------------------
    // Implementation of the Executable interface
    // -------------------------------------------------------------------------

    public void execute() throws Exception {
      final String fileName = getFileName();
      final File outputFile = new File(adlBuildDirectory, fileName);

      final CodeWriter cw = new CodeWriter();
      cw.appendln("// THIS FILE HAS BEEN GENERATED BY THE CECILIA ADL "
          + "COMPILER.");
      cw.appendln("// DO NOT EDIT").endl();
      prepareSourceCode(cw);

      try {
        SourceFileWriter.writeToFile(outputFile, cw.toString());
      } catch (final IOException e) {
        throw new ADLException(GenericErrors.INTERNAL_ERROR, e,
            "An error occurs while writing to file '"
                + outputFile.getAbsolutePath() + "'");
      }

      sourceFile = new SourceFile(getSignature(), outputFile);
    }

    // -------------------------------------------------------------------------
    // Implementation of the SourceFileProvider interface
    // -------------------------------------------------------------------------

    public SourceFile getSourceFile() {
      return sourceFile;
    }
  }

  /**
   * Builds the ".adl.c" source file that contains the definition of the
   * component structure, the inclusion of the implementation file and the
   * instantiation/initialization of the static instances.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "component-definition-file-provider", signature = SourceFileProvider.class, record = "role:componentDefinitionFile, id:%", parameters = "componentNode"))
  public static class SourceFileTask extends AbstractSourceFileTask {

    /** The suffix of the generated file. */
    public static final String                   FILE_NAME_SUFFIX                              = ".adl.c";

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /** Source code for VFT declarations (optional). */
    @ClientInterface(name = "vft-instantiation-provider", contingency = OPTIONAL, record = "role:interfaceVFTInstantiation, id:%", parameters = "componentNode")
    public SourceCodeProvider                    vftInstantiationProviderItf;

    /**
     * Collection interface to collect source code for the static component
     * instances.
     */
    @ClientInterface(name = "component-instantiation-aggregator", signature = SourceCodeProvider.class, record = "role:componentInstantiationAggregator, id:%", parameters = "componentNode")
    public final Map<String, SourceCodeProvider> instantiationProviderItf                      = new HashMap<String, SourceCodeProvider>();

    /**
     * Client interface used to retrieve the additional implementation code.
     * This interface can be used for generated implementation code (factory
     * interface for instance).
     */
    @ClientInterface(name = "additional-implementation-source-code", signature = SourceCodeProvider.class, record = "role:additionalImplementationSourceCode, id:%", parameters = "componentNode")
    public final Map<String, SourceCodeProvider> additionalImplementationSourceCodeProviderItf = new HashMap<String, SourceCodeProvider>();

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param adlBuildDirectory The directory into which the generated file will
     *          be placed.
     */
    public SourceFileTask(final File adlBuildDirectory) {
      super(adlBuildDirectory);
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractImplementationTask
    // -------------------------------------------------------------------------

    @Override
    protected String getFileName() {
      return typeNameProviderItf.getCTypeName() + FILE_NAME_SUFFIX;
    }

    @Override
    protected String getSignature() {
      return typeNameProviderItf.getTypeName();
    }

    @Override
    protected void prepareSourceCode(final CodeWriter cw) {
      // append source code that defines component type.
      cw.append(definitionProviderItf.getSourceCode()).endl();

      // append implementation code.
      cw.appendln(implementationProviderItf.getSourceCode()).endl();

      // append additional implementation code.
      for (final SourceCodeProvider codeProvider : additionalImplementationSourceCodeProviderItf
          .values()) {
        cw.appendln(codeProvider.getSourceCode());
      }

      // append source code that declare VTable of interfaces (if any)
      if (vftInstantiationProviderItf != null)
        cw.appendln(vftInstantiationProviderItf.getSourceCode());

      // append source codes that declare/initialize static instances.
      // To ensure reproducible code generation, append controller definition
      // codes in their alphabetic order.
      appendSortedSourceCodes(cw, instantiationProviderItf.values());
    }
  }

  /**
   * Builds the ".adl.c" source file that contains the definition of the
   * component structure and the inclusion of a module of implementation.
   */
  @TaskParameters({"componentNode", "moduleNode"})
  @ServerInterfaces(@ServerInterface(name = "component-module-file-provider", signature = SourceFileProvider.class, record = "role:componentModuleFile, id:%, module:%", parameters = {
      "componentNode", "moduleNode"}))
  public static class ModuleSourceFileTask extends AbstractSourceFileTask {

    /** The suffix of the generated file. */
    public static final String FILE_NAME_SUFFIX = ".adl.c";

    protected final String     moduleName;

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /** Source code for module implementation. */
    @ClientInterface(name = "module-implementation-provider", record = "role:implementation, id:%, module:%", parameters = {
        "componentNode", "moduleNode"})
    public SourceCodeProvider  moduleImplementationProviderItf;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param adlBuildDirectory The directory into which the generated file will
     *          be placed.
     * @param moduleName The name of the module to which this task correspond.
     *          This name is used to generate a comprehensive file name.
     */
    public ModuleSourceFileTask(final File adlBuildDirectory,
        final String moduleName) {
      super(adlBuildDirectory);
      this.moduleName = moduleName;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractImplementationTask
    // -------------------------------------------------------------------------

    @Override
    protected String getFileName() {
      return typeNameProviderItf.getCTypeName() + '_' + moduleName
          + FILE_NAME_SUFFIX;
    }

    @Override
    protected String getSignature() {
      return typeNameProviderItf.getTypeName() + '.' + moduleName;
    }

    @Override
    protected void prepareSourceCode(final CodeWriter cw) {
      // append source code that defines component type.
      cw.append(definitionProviderItf.getSourceCode()).endl();

      // append implementation code: in case of multi-module component, this
      // source code defines only the private component data.
      cw.appendln(implementationProviderItf.getSourceCode()).endl();

      // append module source code
      cw.appendln(moduleImplementationProviderItf.getSourceCode());
    }
  }
}
