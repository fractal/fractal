/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.primitive.optC.types;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerDecorationUtil.getDataSuffix;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.interfaces.AbstractServerInterfaceVisitor;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the factory implementation code pieces for the
 * cloning server interfaces.
 */
public class ServerInterfaceFactoryVisitor
    extends
      AbstractServerInterfaceVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractServerInterfaceVisitor
  // ---------------------------------------------------------------------------

  /**
   * Creates a task that writes the source code containing the definition of the
   * server interfaces of the component.
   */
  @Override
  protected Component createTask(final ComponentContainer container,
      final List<TypeInterface> serverInterfaces) throws ADLException,
      TaskException {
    final List<IDLDefinition> serverInterfaceDefinitions = new ArrayList<IDLDefinition>();
    for (final Interface itf : serverInterfaces) {
      serverInterfaceDefinitions.add(castNodeError(itf,
          IDLDefinitionContainer.class).getIDLDefinition());
    }

    return taskFactoryItf.newPrimitiveTask(
        new ServerInterfaceFactoryInstantiateTask(serverInterfaces), container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds the source code that copy the server interfaces of the component.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "sItf-factory-instantiate", signature = SourceCodeProvider.class, record = "role:factoryInstantiatePiece, id:%, codePiece:serverInterface", parameters = "componentNode"))
  public static class ServerInterfaceFactoryInstantiateTask
      extends
        AbstractDefinitionTask {

    protected final List<TypeInterface> exportedItfNodes;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param exportedItfNodes the list of exported interface nodes belonging to
     *            the component.
     */
    protected ServerInterfaceFactoryInstantiateTask(
        final List<TypeInterface> exportedItfNodes) {
      this.exportedItfNodes = exportedItfNodes;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter(
          "Exported Interfaces Factory Builder");

      for (final TypeInterface itf : exportedItfNodes) {
        final String data = getDataSuffix(itf);
        cw.append("newComp->type.exported.").append(
            itf.getName().replace('.', '_').replace('-', '_')).append(
            ".selfdata = ");
        if (data == null) {
          // data of this interface is the component data
          cw.append("newComp;");
        } else {
          // data of this interface is controller data
          cw.append("newComp_").append(data).append(";");
        }
        cw.endl();
      }
      return cw.toString();
    }
  }
}
