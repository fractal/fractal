/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.composite.c.controllers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.cecilia.adl.AbstractInstantiationTask;
import org.objectweb.fractal.cecilia.adl.InstanceNameProvider;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.TypeNameProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterfaceForEach;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the instantiation of the content controller
 * data structure of a default composite component.
 */
public class CCInstantiationVisitor extends AbstractTaskFactoryUser
    implements
      ComponentVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractClientInterfaceVisitor
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link ComponentContainer} nodes and creates a task that writes the
   * source code containing the initialization of the content controller data
   * structure.
   */
  public org.objectweb.fractal.api.Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    final Component[] subComponents = container.getComponents();
    return taskFactoryItf.newPrimitiveTask(new CCInstantiationTask(
        subComponents), container, Arrays.asList(subComponents));
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds source code for the instantiation of the content controller data
   * structure for the given component node. This task provides the produced
   * source code.
   */
  @TaskParameters({"componentNode", "subComponents"})
  @ServerInterfaces(@ServerInterface(name = "cc-instantiation-provider", signature = SourceCodeProvider.class, record = "role:controllerInstantiation, id:%, controller:CC", parameters = "componentNode"))
  public static class CCInstantiationTask extends AbstractInstantiationTask {

    protected final Component[]                       subComponents;

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /** The {@link TypeNameProvider} task for each sub component. */
    @ClientInterfaceForEach(prefix = "subComp-type-names", signature = TypeNameProvider.class, iterable = "subComponents", record = "role:subComponentTypeNameProvider, id:%", parameters = "subComponents.element")
    public final Map<Component, TypeNameProvider>     subComponentTypeNameProviderItfs       = new HashMap<Component, TypeNameProvider>();

    /**
     * The {@link SourceCodeProvider} task that provides definition of component
     * type for each sub component.
     */
    @ClientInterfaceForEach(prefix = "subComp-type-definitions", signature = SourceCodeProvider.class, iterable = "subComponents", record = "role:subComponentTypeDefinitionProvider, id:%", parameters = "subComponents.element")
    public final Map<Component, SourceCodeProvider>   subComponentTypeDefinitionProviderItfs = new HashMap<Component, SourceCodeProvider>();

    /** The {@link InstanceNameProvider} task for each sub component. */
    @ClientInterfaceForEach(prefix = "subComp-instance-names", signature = InstanceNameProvider.class, iterable = "subComponents", record = "role:subComponentInstanceNameProvider, id:%", parameters = "subComponents.element")
    public final Map<Component, InstanceNameProvider> subComponentInstanceNameProviderItfs   = new HashMap<Component, InstanceNameProvider>();

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param subComponents the sub component nodes.
     */
    public CCInstantiationTask(final Component[] subComponents) {
      this.subComponents = subComponents;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractInstantiationTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter(
          "Content Controller Data Instantiation");

      cw.appendln("// extern reference to composite's sub components.").endl();
      for (final Component subComponent : subComponents) {
        cw.appendln(subComponentTypeDefinitionProviderItfs.get(subComponent)
            .getSourceCode());
        cw.append("extern struct ").append(
            subComponentTypeNameProviderItfs.get(subComponent).getCTypeName())
            .append("_exporteds ").append(
                subComponentInstanceNameProviderItfs.get(subComponent)
                    .getCInstanceName()).append(";").endl();
      }

      cw.append("// Define composite's sub component indexes.").endl();
      for (int i = 0; i < subComponents.length; i++) {
        cw.append("#define IDX_").append(
            subComponents[i].getName().replace('-', '_').toUpperCase()).append(
            ' ').append(i).endl();
      }
      cw.endl();

      cw.append("// table of composite's sub components.").endl();
      cw.append("struct SubComponentDesc_t ").append(
          instanceNameProviderItf.getCInstanceName()).append(
          "_subComponents[] = {").endl();
      for (final Component subComponent : subComponents) {
        cw.append("{&(").append(
            subComponentInstanceNameProviderItfs.get(subComponent)
                .getCInstanceName())
            .append(".component), // instance").endl();
        cw.append("SUB_COMPONENT_NOT_INITIALIZED, // state").endl();
        cw.appendln("#ifdef DEBUGCOMPOSITE");
        cw.append("\"").append(subComponent.getName()).append("\", // name")
            .endl();
        cw.append("\"").append(
            subComponentTypeNameProviderItfs.get(subComponent).getTypeName())
            .append("\", // implementationName").endl();
        cw.appendln("#endif // DEBUGCOMPOSITE");
        cw.appendln("},");
      }
      cw.append("};").endl();

      return cw.toString();
    }
  }
}
