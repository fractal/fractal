/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.types;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.ContextLocal;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.task.core.TaskException;

/**
 * This visitor determines the type of a given primitive component and if this
 * type is already defined. If the type is not yet defined, this visitor calls
 * its {@link #definitionVisitorItf} client interface, otherwise it returns
 * <code>null</code>.
 * 
 * @see ComponentTypeDesc
 */
public class PrimitiveTypeDefinitionFilter
    implements
      ComponentVisitor,
      BindingController {

  /**
   * A context local map associating {@link ComponentTypeDesc} to themselves.
   * Note that : thanks to the implementation of the
   * {@link ComponentTypeDesc#equals(Object) equals} method of this class, if
   * two components A and B share the same type, then ComponentTypeDesc(A) is
   * equals to ComponentTypeDesc(B).
   */
  protected final ContextLocal<Set<ComponentContainer>> componentTypes              = new ContextLocal<Set<ComponentContainer>>();

  // ---------------------------------------------------------------------------
  // Client interfaces
  // ---------------------------------------------------------------------------

  /** The name of the {@link #definitionVisitorItf} client interface. */
  public static final String                            DEFINITION_VISITOR_ITF_NAME = "definition-visitor";

  /**
   * The {@link ComponentVisitor} client interface use to create the task that
   * defines the component type.
   */
  public ComponentVisitor                               definitionVisitorItf;

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    if (needTypeDefinition(path, container, context)) {
      return definitionVisitorItf.visit(path, container, context);
    } else {
      return null;
    }
  }

  // --------------------------------------------------------------------------
  // Utility methods
  // --------------------------------------------------------------------------

  protected boolean needTypeDefinition(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {

    // retrieve the component type map for the current context
    Set<ComponentContainer> types = componentTypes.get(context);
    if (types == null) {
      types = new HashSet<ComponentContainer>();
      componentTypes.set(context, types);
    }

    /* get the previously set (in the Loader chain) component type decoration */
    final ComponentContainer currentComponentType = TypeDecorationUtil
        .getTypeDecoration(container);

    /*
     * if this component is not the component type, rearrange the order of the
     * interfaces to match the order of the component type which represents it.
     */
    if (container != currentComponentType) {
      if (container instanceof InterfaceContainer) {
        final Map<String, Interface> itfs = new HashMap<String, Interface>();
        // We first remove all the interfaces of 'container' and store
        // them in a map.
        final InterfaceContainer interfaceContainer = (InterfaceContainer) container;
        for (final Interface itf : interfaceContainer.getInterfaces()) {
          itfs.put(itf.getName(), itf);
          interfaceContainer.removeInterface(itf);
        }
        // Then re-add the stored interfaces respecting the order of 'typeComp'.
        for (final Interface itf : ((InterfaceContainer) currentComponentType)
            .getInterfaces()) {
          interfaceContainer.addInterface(itfs.get(itf.getName()));
        }
      }

    }

    /*
     * If this type wasn't visited yet, add it to the set of visited types,
     * and tell the caller to visit this type.
     */
    if (!types.contains(currentComponentType)) {
      types.add(currentComponentType);
      return true;
    }
    else {
      return false;
    }
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  public String[] listFc() {
    return new String[]{DEFINITION_VISITOR_ITF_NAME};
  }

  public void bindFc(final String clientItfName, final Object serverItf)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (DEFINITION_VISITOR_ITF_NAME.equals(clientItfName))
      definitionVisitorItf = (ComponentVisitor) serverItf;
    else
      throw new NoSuchInterfaceException("There is no interface named '"
          + clientItfName + "'");
  }

  public Object lookupFc(final String clientItfName)
      throws NoSuchInterfaceException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (DEFINITION_VISITOR_ITF_NAME.equals(clientItfName))
      return definitionVisitorItf;
    else
      throw new NoSuchInterfaceException("There is no interface named '"
          + clientItfName + "'");
  }

  public void unbindFc(final String clientItfName)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (DEFINITION_VISITOR_ITF_NAME.equals(clientItfName))
      definitionVisitorItf = null;
    else
      throw new NoSuchInterfaceException("There is no interface named '"
          + clientItfName + "'");
  }
}
