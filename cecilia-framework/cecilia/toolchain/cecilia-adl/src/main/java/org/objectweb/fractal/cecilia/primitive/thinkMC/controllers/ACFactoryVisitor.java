/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.primitive.thinkMC.controllers;

import java.util.ArrayList;
import java.util.Collection;

import org.objectweb.fractal.adl.attributes.Attribute;
import org.objectweb.fractal.adl.attributes.Attributes;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.attributes.AbstractAttributeVisitor;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.TaskFactory;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the factory implementation code pieces for
 * cloning attribute controller data structure.
 */
public class ACFactoryVisitor extends AbstractAttributeVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractAttributeVisitor
  // ---------------------------------------------------------------------------

  @Override
  protected Component createTask(final ComponentContainer container,
      final Attributes attributes) throws TaskException {
    final Collection<Component> tasks = new ArrayList<Component>(3);
    tasks.add(taskFactoryItf.newPrimitiveTask(
        new ACFactoryInstantiateDeclarationTask(), container));
    tasks.add(taskFactoryItf.newPrimitiveTask(
        new ACFactoryInstantiateSizeofTask(), container));
    tasks.add(taskFactoryItf.newPrimitiveTask(new ACFactoryInstantiateTask(
        attributes.getAttributes()), container));

    return taskFactoryItf.newCompositeTask(tasks, TaskFactory.EXPORT_ALL, null);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds the source code that declare the new attribute controller data
   * structure.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "ac-factory-instantiate-declaration", signature = SourceCodeProvider.class, record = "role:factoryInstantiateDeclaration, id:%, codePiece:AC", parameters = "componentNode"))
  public static class ACFactoryInstantiateDeclarationTask
      extends
        AbstractDefinitionTask {

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final String definitionName = typeNameProviderItf.getCTypeName();
      final CodeWriter cw = new CodeWriter();
      cw.append("struct ").append(definitionName).appendln(
          "_AC_data_type * newComp_AC_data;");
      return cw.toString();
    }
  }

  /**
   * Builds the source code that declare the new attribute controller data
   * structure.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "ac-factory-instantiate-sizeof", signature = SourceCodeProvider.class, record = "role:factoryInstantiateSizeof, id:%, codePiece:AC", parameters = "componentNode"))
  public static class ACFactoryInstantiateSizeofTask
      extends
        AbstractDefinitionTask {

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final String definitionName = typeNameProviderItf.getCTypeName();
      final CodeWriter cw = new CodeWriter();
      cw.append("+ sizeof(struct ").append(definitionName)
          .append("_AC_data_type)");
      return cw.toString();
    }
  }

  /**
   * Builds the source code that copy the attribute controller data structure.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "ac-factory-instantiate", signature = SourceCodeProvider.class, record = "role:factoryInstantiatePiece, id:%, codePiece:AC", parameters = "componentNode"))
  public static class ACFactoryInstantiateTask extends AbstractDefinitionTask {

    // the attributes array
    protected final Attribute[] fields;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param fields the attributes to initialize.
     * @param fieldTypes the attribute types.
     */
    public ACFactoryInstantiateTask(final Attribute[] fields) {
      this.fields = fields;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final String definitionName = typeNameProviderItf.getCTypeName();
      final CodeWriter cw = new CodeWriter("Primitive Attribute Controller");
      cw.appendln("{");
      cw.append("newComp_AC_data = (struct ").append(
          definitionName).appendln("_AC_data_type *)_ptr;");
      cw.append("struct ").append(definitionName).append(
          "_AC_data_type * original_AC_data = ").append("(struct ").append(
          definitionName).append("_AC_data_type *) ").append(
          "originalComp->type.exported.attribute_controller.selfdata;").endl();

      cw.append("memcpy(newComp_AC_data, original_AC_data, sizeof(struct ")
          .append(definitionName).append("_AC_data_type));").endl();

      for (int i = 0; i < fields.length; i++) {
        cw.append("newComp_AC_data->attributes[").append(i).append(
            "].value = &(newComp->attributes.").append(fields[i].getName())
            .append(");").endl();

      }
      // AC is the last piece of code, it doesn't need to increment _ptr.
      cw.appendln("}");
      return cw.toString();
    }
  }
}
