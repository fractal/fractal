/***
 * Cecilia ADL Compiler
 * Copyright (C) 2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Lionel Debroux
 *
 * Contributors: Ali Erdem Ozcan, Matthieu Leclercq
 * (for the code in AttributeLoader, of which this file is inspired)
 */

package org.objectweb.fractal.cecilia.adl.attributes;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.attributes.Attribute;
import org.objectweb.fractal.adl.attributes.AttributeLoader;
import org.objectweb.fractal.adl.attributes.Attributes;
import org.objectweb.fractal.adl.attributes.AttributesContainer;
import org.objectweb.fractal.adl.xml.XMLNode;

/**
 * A {@link org.objectweb.fractal.adl.Loader} to handle {@link Attributes} nodes
 * that contain {@link Attribute} nodes of the special form <attribute
 * name="...name..."><![CDATA[...definition...]]></attribute> This form is the
 * key to non-trivial initializers for attributes. This loader transforms this
 * special form of {@link Attribute} to the usual form <attribute
 * name="...name..." value="...definition..." />
 */
public class AttributeModifierLoader extends AttributeLoader {

  // ---------------------------------------------------------------------------
  // Overridden methods
  // ---------------------------------------------------------------------------

  @Override
  protected void checkAttributesContainer(final AttributesContainer container,
      final Map<Object, Object> context) throws ADLException {
    final Attributes attrs = container.getAttributes();
    if (attrs != null) {
      final Attribute[] attribute = attrs.getAttributes();
      for (final Attribute attr : attribute) {
        if (attr instanceof XMLNode) {
          final String attrContent = ((XMLNode) attr).xmlGetContent();
          if (attrContent != null) {
            if (attr.getValue() != null) {
              throw new ADLException(AttributeErrors.TWO_VALUES, attr, attr
                  .getName());
            } else {
              attr.setValue(attrContent.trim());
            }
          }
        }
      }
    }
  }
}
