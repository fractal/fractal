/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.types;

import java.util.HashSet;
import java.util.Set;

import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.attributes.Attributes;
import org.objectweb.fractal.adl.attributes.AttributesContainer;
import org.objectweb.fractal.adl.bindings.Binding;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.implementations.Controller;
import org.objectweb.fractal.adl.implementations.ControllerContainer;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil;
import org.objectweb.fractal.cecilia.adl.directives.Cflag;
import org.objectweb.fractal.cecilia.adl.directives.CflagContainer;
import org.objectweb.fractal.cecilia.adl.directives.Include;
import org.objectweb.fractal.cecilia.adl.directives.IncludeContainer;
import org.objectweb.fractal.cecilia.adl.directives.Ldflag;
import org.objectweb.fractal.cecilia.adl.directives.LdflagContainer;
import org.objectweb.fractal.cecilia.adl.implementations.ExtendedImplementation;
import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;

/**
 * Descriptor of <em>primitive component definition</em>. It is used to find
 * if the definition of a <em>primitive component</em> has already been compiled.
 * <p>
 * Definition for a primitive component type: two components have the same
 * definition if and only if they have the same implementation, the same
 * attribute signature, the same interfaces (signature and names) <strong>and
 * the same dynamic binding configuration</strong>. The last thing means that
 * two components are considered of two different types if at least one of these
 * situations arise:
 * <ul>
 * <li>for the same client interface type, one component definition has it as
 * part of a &lt;binding dynamic="true" /&gt; , the other instead as part of
 * &lt;binding dynamic="false" /&gt;</li>
 * <li>for the same client interface type, the two component definitions both
 * declare to bind it as lt;binding dynamic="false" /&gt; but the target server
 * interface is owned by different components</li>
 * </ul>
 * In checking the equality in this way, it relies on some decorations that have
 * been put on each {@link TypeInterface} of a component during the loading
 * chain, more specifically for example the decorations made by
 * {@link BindingDecorationUtil}.
 * </p>
 */
public class ComponentTypeDesc {

  protected int                        hashCode = 0;

  protected final ComponentContainer container;

  // --------------------------------------------------------------------------
  // Constructor
  // --------------------------------------------------------------------------

  public ComponentTypeDesc(final ComponentContainer container) {
    this.container = container;
    computeHashCode();
  }

  // --------------------------------------------------------------------------
  // Overridden Object methods
  // --------------------------------------------------------------------------

  @Override
  public boolean equals(final Object o) {
    final ComponentContainer container1 = ((ComponentTypeDesc) o).container;

    // compare implementations
    if (container instanceof ImplementationContainer) {
      final Implementation impl = ((ImplementationContainer) container)
          .getImplementation();
      if (container1 instanceof ImplementationContainer) {
        final Implementation impl1 = ((ImplementationContainer) container1)
            .getImplementation();
        if (impl != null || impl1 != null) {
          if (impl == null || impl1 == null) return false;
          if (!equalsIgnoreNull(impl.getClassName(), impl1.getClassName()))
            return false;
        }

        final Set<String> contentElementSet = new HashSet<String>();
        final Set<String> contentElementSet1 = new HashSet<String>();
        contentElementsSet(impl, contentElementSet);
        contentElementsSet(impl1, contentElementSet1);
        if (contentElementSet.size() != contentElementSet1.size()
            || !contentElementSet.containsAll(contentElementSet1))
          return false;

      } else { // container1 NOT instanceof ImplementationContainer
        if (impl != null) return false;
      }
    } else if (container1 instanceof ImplementationContainer) {
      if (((ImplementationContainer) container1).getImplementation() != null)
        return false;
    }

    // compare interfaces
    if (container instanceof InterfaceContainer) {
      final Interface[] itfs = ((InterfaceContainer) container).getInterfaces();
      if (container1 instanceof InterfaceContainer) {
        final Interface[] itfs1 = ((InterfaceContainer) container1)
            .getInterfaces();
        for (final Interface itf : itfs) {
          if (itf.getName().endsWith("-controller")) continue;
          boolean found = false;
          for (final Interface itf1 : itfs1) {
            if (!equalsIgnoreNull(itf.getName(), itf1.getName())) continue;
            if (itf instanceof TypeInterface) {
              if (!(itf1 instanceof TypeInterface)) continue;
              final TypeInterface itfType = (TypeInterface) itf;
              final TypeInterface itfType1 = (TypeInterface) itf1;
              if (!equalsIgnoreNull(itfType.getSignature(), itfType1
                  .getSignature())) continue;
              if (!equalsIgnoreNull(itfType.getRole(), itfType1.getRole()))
                continue;
              if (!equalsIgnoreNull(itfType.getContingency(), itfType1
                  .getContingency())) continue;

              final boolean firstItfIsBoundDynamically = BindingDecorationUtil.isDynamic(itfType);
              final boolean secondItfIsBoundDynamically = BindingDecorationUtil.isDynamic(itfType1);
              if (firstItfIsBoundDynamically && !secondItfIsBoundDynamically) {
                System.out.println("First interface is dynamic='true', the other is dynamic='false'");
                continue;
              }
              if (!firstItfIsBoundDynamically && secondItfIsBoundDynamically) {
                System.out.println("First interface is dynamic='false', the other is dynamic='true'");
                continue;
              }
              if (!firstItfIsBoundDynamically && !secondItfIsBoundDynamically) {
                if (!equalsIgnoreNull(InterfaceDecorationUtil
                    .getBoundTo(itfType), InterfaceDecorationUtil
                    .getBoundTo(itfType1))) {
                  System.out.println("Both interfaces are dynamic='false' but are bound to different interfaces");
                  continue;
                }
              }
            }
            found = true;
            break;
          }
          if (!found) return false;
        }
      } else { // container1 NOT instanceof InterfaceContainer
        if (itfs != null && itfs.length != 0) return false;
      }
    } else if (container1 instanceof InterfaceContainer) {
      final Interface[] itfs1 = ((InterfaceContainer) container1)
          .getInterfaces();
      if (itfs1 != null && itfs1.length != 0) return false;
    }

    // compare attributes
    if (container instanceof AttributesContainer) {
      final Attributes attrs = ((AttributesContainer) container)
          .getAttributes();
      if (container1 instanceof AttributesContainer) {
        final Attributes attrs1 = ((AttributesContainer) container1)
            .getAttributes();
        if (attrs != null || attrs1 != null) {
          if (attrs == null || attrs1 == null) return false;
          if (!equalsIgnoreNull(attrs.getSignature(), attrs1.getSignature()))
            return false;
        }
      } else { // container1 NOT instanceof AttributesContainer
        if (attrs != null) return false;
      }
    } else if (container1 instanceof AttributesContainer) {
      if (((AttributesContainer) container1).getAttributes() != null)
        return false;
    }

    // compare controller
    if (container instanceof ControllerContainer) {
      final Controller controller = ((ControllerContainer) container)
          .getController();
      if (container1 instanceof ControllerContainer) {
        final Controller controller1 = ((ControllerContainer) container1)
            .getController();
        if (controller != null || controller1 != null) {
          if (controller == null || controller1 == null) return false;
          if (!equalsIgnoreNull(controller.getDescriptor(), controller1
              .getDescriptor())) return false;
        }
      } else { // container1 NOT instanceof ControllerContainer
        if (controller != null) return false;
      }
    } else if (container1 instanceof ControllerContainer) {
      if (((ControllerContainer) container1).getController() != null)
        return false;
    }

    return true;
  }

  /**
   * If you want to modify the way the <code>hashCode</code> is computed,
   * override the {@link ComponentDefinitionDesc#computeHashCode()} method.
   */
  @Override
  public final int hashCode() {
    return hashCode;
  }

  // --------------------------------------------------------------------------
  // Utility methods
  // --------------------------------------------------------------------------

  protected void computeHashCode() {
    hashCode = 23;
    if (container instanceof ImplementationContainer) {
      final Implementation impl = ((ImplementationContainer) container)
          .getImplementation();
      if (impl != null && impl.getClassName() != null) {
        hashCode += impl.getClassName().hashCode() * 37;
      }
    }

    if (container instanceof InterfaceContainer) {
      final Interface[] itfs = ((InterfaceContainer) container).getInterfaces();
      for (final Interface element : itfs) {
        if (element.getName() != null) {
          hashCode += element.getName().hashCode() * 127;
        }

        hashCode += new Boolean(BindingDecorationUtil.isDynamic((Node)element))
            .hashCode();
        /*final Interface boundTo = InterfaceDecorationUtil.getBoundTo(element);
        if (boundTo != null) {
          hashCode += boundTo.hashCode();
        }*/
      }
    }

    if (container instanceof AttributesContainer) {
      final Attributes attrs = ((AttributesContainer) container)
          .getAttributes();
      if (attrs != null) hashCode += attrs.getSignature().hashCode() * 73;
    }
  }

  /**
   * @param o1
   * @param o2
   * @return true if the two Object references are both null, otherwise the
   *         result of <code>o1.equals(o2)</code>;
   */
  protected static boolean equalsIgnoreNull(final Object o1, final Object o2) {
    if (o1 == null) return o2 == null;
    return o2 != null && o1.equals(o2);
  }

  protected void contentElementsSet(final Implementation impl,
      final Set<String> elementSet) {
    if (impl == null) return;

    String language = "";
    if (impl instanceof ExtendedImplementation)
      language = ((ExtendedImplementation) impl).getLanguage();
    if (impl instanceof CflagContainer) {
      final String header = language + "_CFLAG_";
      for (final Cflag cflag : ((CflagContainer) impl).getCflags())
        elementSet.add(header + cflag.getValue());
    }
    if (impl instanceof LdflagContainer) {
      final String header = language + "_LDFLAG_";
      for (final Ldflag ldflag : ((LdflagContainer) impl).getLdflags())
        elementSet.add(header + ldflag.getValue());
    }
    if (impl instanceof IncludeContainer) {
      final String header = language + "_INCLUDE_";
      for (final Include include : ((IncludeContainer) impl).getIncludes())
        elementSet.add(header + include.getFile());
    }
  }

}
