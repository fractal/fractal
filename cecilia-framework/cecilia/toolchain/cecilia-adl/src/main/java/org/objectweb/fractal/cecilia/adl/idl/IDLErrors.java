/**
 * Cecilia ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.idl;

import static org.objectweb.fractal.adl.error.ErrorTemplateValidator.validErrorTemplate;

import org.objectweb.fractal.adl.error.ErrorTemplate;

/** {@link ErrorTemplate} group for the idl package. */
public enum IDLErrors implements ErrorTemplate {

  /** */
  IDL_NOT_FOUND("IDL not found \"%s\".", "idlname"),

  /** */
  IO_ERROR("Can't read IDL file \"%s\".", "idlFile"),

  /** */
  SYNTAX_ERROR("Syntax error in IDL \"%s\": %s", "idlname", "details"),

  /** */
  INHERITANCE_CYCLE(
      "Cycle in inheritance graph: between \"%s\" and \"%s\" interfaces.",
      "idl1", "idl2"),

  /** */
  INVALID_SUPER_INTERFACE(
      "Invalid super-interface \"%s\": an interface can only extends another interface.",
      "superitf"),

  /** */
  DUPLICATED_FIELD_NAME(
      "Invalid field name \"%s\": an element with the same name already exists (at \"%s\").",
      "name", "location"),

  /** */
  DUPLICATED_METHOD_NAME(
      "Invalid method name \"%s\": an element with the same name already exists (at \"%s\").",
      "name", "location"),

  /** */
  DUPLICATED_PARAMETER_NAME(
      "Duplicated parameter name \"%s\": a parameter with the same name already exists (at \"%s\").",
      "name", "location"),

  /** */
  INVALID_PACKAGE_NAME(
      "Invalid package name: found \"%s\" where \"%s\" was expected",
      "pkgname", "expected"),

  /** */
  INVALID_IDL_NAME("Invalid name: found \"%s\" where \"%s\" was expected",
      "name", "expected"),

  /** */
  IDL_NAME_IS_RESERVED_WORD("Invalid name: \"%s\" is a reserved keyword",
      "name"),

  ;

  /** The groupId of ErrorTemplates defined in this enumeration. */
  public static final String GROUP_ID = "IDL";

  private int                id;
  private String             format;

  private IDLErrors(final String format, final Object... args) {
    this.id = ordinal();
    this.format = format;

    assert validErrorTemplate(this, args);
  }

  public int getErrorId() {
    return id;
  }

  public String getGroupId() {
    return GROUP_ID;
  }

  public String getFormatedMessage(final Object... args) {
    return String.format(format, args);
  }

  public String getFormat() {
    return format;
  }
}
