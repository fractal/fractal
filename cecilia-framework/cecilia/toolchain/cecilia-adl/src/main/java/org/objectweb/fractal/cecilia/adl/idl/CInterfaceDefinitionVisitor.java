/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 * Contributor: Alessio Pace
 *
 */

package org.objectweb.fractal.cecilia.adl.idl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.idl.ast.Field;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Import;
import org.objectweb.fractal.cecilia.adl.idl.ast.ImportContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.InterfaceDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Method;
import org.objectweb.fractal.cecilia.adl.idl.ast.PrimitiveType;
import org.objectweb.fractal.cecilia.adl.idl.ast.TypeContainer;
import org.objectweb.fractal.cecilia.adl.idl.util.CUtil;
import org.objectweb.fractal.cecilia.adl.idl.util.Util;
import org.objectweb.fractal.cecilia.adl.idl.IDLDefinitionVisitor;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.Executable;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterfaceForEach;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component for interface definition in C.
 */
public class CInterfaceDefinitionVisitor extends AbstractTaskFactoryUser
    implements
      IDLDefinitionVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of the IDLVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link InterfaceDefinition} nodes, and creates a task that writes the
   * corresponding C source code.
   */
  public Component visit(final List<Node> path,
      final IDLCompilationDesc idlCompilationDesc,
      final Map<Object, Object> context) throws ADLException, TaskException {
    IDLDefinition idlDefinition = idlCompilationDesc.getIdlDefinition();
    /* this component can be used only to compile InterfaceDefinition types */
    if (idlDefinition instanceof InterfaceDefinition) {

      final List<IDLDefinition> importedDefinitions = new ArrayList<IDLDefinition>();
      if (idlDefinition instanceof ImportContainer) {
        for (final Import imp : ((ImportContainer) idlDefinition).getImports()) {
          importedDefinitions.add((IDLDefinition) Util.getImportedAST(imp));
        }
      }

      return taskFactoryItf.newPrimitiveTask(new CInterfaceHeaderTask(
          (InterfaceDefinition) idlDefinition), idlDefinition,
          importedDefinitions);
    } else
      return null;
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds interface definitions for C. It extends the Abstract Interface
   * visitor and implements the required visit methods.
   */
  @TaskParameters({"interfaceDefinitionNode", "importedDefinitions"})
  @ServerInterfaces(@ServerInterface(name = "c-interface-definition-provider", signature = SourceCodeProvider.class, record = "role:cInterfaceDefinition, id:%", parameters = {"interfaceDefinitionNode"}))
  public static class CInterfaceHeaderTask extends AbstractInterfaceVisitor
      implements
        Executable,
        SourceCodeProvider {

    protected final InterfaceDefinition                 itfDefinition;

    // -------------------------------------------------------------------------
    // Client interfaces
    // -------------------------------------------------------------------------

    /** Source code to be included to access to definition of imported IDLs. */
    @ClientInterfaceForEach(iterable = "importedDefinitions", prefix = "imported-definition", signature = SourceCodeProvider.class, record = "role:importedDefinition, id:%", parameters = "importedDefinitions.element")
    public final Map<IDLDefinition, SourceCodeProvider> importedDefinitionItfs = new LinkedHashMap<IDLDefinition, SourceCodeProvider>();

    /** Client interface used to retrieve the content of the generated files. */
    @ClientInterface(name = "c-vft-definition", record = "role:cVFTDefinition, id:%", parameters = "interfaceDefinitionNode")
    public SourceCodeProvider           cVFTSourceItf;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param itfDefinition The interface definition node for which this task
     *            must generate C source code.
     */
    public CInterfaceHeaderTask(final InterfaceDefinition itfDefinition) {
      this.itfDefinition = itfDefinition;
    }

    // The generated source code.
    protected String     sourceCode;
    // The code writer that is used for code generation
    protected CodeWriter cw;
    // The name of the interface (in IDL naming space)
    protected String     itfName;
    // The name of the interface (in C naming space)
    protected String     cItfName;
    // The uppercasename of the interface (in C naming space)
    protected String     cUpItfName;
    // Whether to generate static methods.
    protected boolean    generateStaticMethods;

    // -------------------------------------------------------------------------
    // Implementation of the Executable interface
    // -------------------------------------------------------------------------

    public void execute() throws Exception {
      cw = new CodeWriter("C Interface builder");
      itfName = itfDefinition.getName();
      cItfName = itfName.replace('.', '_');
      generateStaticMethods = false;

      // Create beginning of header.
      // This used to be in enterInterface(), it was moved one level up
      // because of breakage (use before define) in the A1 and R IDLTests
      // (where A1.idl.h and R.idl.h include each other) otherwise.
      cw.append("/* Interface ").append(itfName).endl();
      cw.appendln("Generated automatically, do not edit ! */");
      cw.endl();

      cw.append("#ifndef H").append(cItfName).endl();
      cw.append("#define H").append(cItfName).endl();
      cw.endl();
      // Visit non-static interface definition
      visit(itfDefinition);
      cw.endl();

      // Visit static interface definition
      generateStaticMethods = true;
      itfName += IDLDefinitionVisitor.STATIC_POSTFIX;
      cItfName += IDLDefinitionVisitor.STATIC_POSTFIX;
      visit(itfDefinition);
      
      cw.appendln("#endif");

      // Append source code of VFT (which has both static and non-static part).
      cw.appendln(cVFTSourceItf.getSourceCode());
      cw.endl();

      sourceCode = cw.toString();
    }

    // -------------------------------------------------------------------------
    // Implementation of the SourceCodeProvider interface
    // -------------------------------------------------------------------------

    public String getSourceCode() {
      return sourceCode;
    }

    // -------------------------------------------------------------------------
    // Implementation of the visitor methods
    // -------------------------------------------------------------------------

    public void enterInterface(final InterfaceDefinition itf) throws Exception {

      /* include the definition of the cecilia short cut types */
      cw.endl();
      cw.append("/* some cecilia typedefs for predefinite C types */").endl();
      cw.append("#include \"cecilia_types.h\"").endl();
      cw.endl();

      cw.append("// Interface descriptor");
      cw.append("struct M").append(cItfName).append(";").endl();
      cw.appendln("typedef struct {");
      cw.append("  struct M").append(cItfName).append(" *meth;").endl();
      cw.appendln("  void *selfdata;");
      cw.appendln("#if defined(TYPEDINTERFACE)");
      cw.appendln("  const char *type;");
      cw.appendln("#endif");
      cw.append("} ");
      cw.append(" *").append(cItfName).append(",");
      /* typedef with "R" is leaved for legacy reason. */
      cw.append(" R").append(cItfName).append(";").endl();

      for (final SourceCodeProvider importedDefinitionProvider : importedDefinitionItfs
          .values()) {
        cw.append(importedDefinitionProvider.getSourceCode());
      }

      cw.endl();

      cw.appendln("  /* Virtual table */");
      cw.append("struct M").append(cItfName).append(" {").endl();
    }

    public void leaveInterface(final InterfaceDefinition itf) throws Exception {
      cw.appendln("};").endl();
    }

    public void visitMethod(final Method method) throws Exception {
      if (!generateStaticMethods) {
        cw.append(CUtil.buildMethodDefinition(method, true)).appendln(";");
      }
      else {
        cw.append(CUtil.buildStaticMethodDefinition(method, true)).appendln(";");
      }
    }

    public void visitField(final Field field) throws Exception {
      final PrimitiveType type = ((TypeContainer) field).getPrimitiveType();
      cw.append("#define ").append(cItfName).append("_")
          .append(field.getName()).append(" ((").append(
              CUtil.buildPrimitiveType(type)).append(") ").append(
              field.getValue()).append(')').endl();
      cw.endl();
    }
  }
}
