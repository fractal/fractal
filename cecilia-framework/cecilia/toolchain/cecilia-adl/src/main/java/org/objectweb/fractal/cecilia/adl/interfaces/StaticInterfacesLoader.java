/***
 * Cecilia ADL Compiler
 * Copyright (C) 2005-2008 STMicroelectronics
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.interfaces;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.ast.ASTFractalUtil;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.cecilia.adl.idl.IDLDefinitionVisitor;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.adl.util.Printer;
import org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil;
import org.objectweb.fractal.cecilia.adl.components.ContainmentChecker;
import org.objectweb.fractal.cecilia.adl.types.InstancesCounterLoader;
import org.objectweb.fractal.cecilia.adl.types.TypeDecorationUtil;

/**
 * This {@link Loader} assumes that each Component node that is encoutered has
 * been previously decorated with the {@link TypeDecorationUtil#TYPE_DECORATION}
 * (eventually the same node instance) representing its component type, and this
 * latter has been decorated with the
 * {@link TypeDecorationUtil#COMPONENT_TYPE_INSTANCES_COUNTER_DECORATION} of the
 * number of instances of that component type (to see if a component type is a
 * singleton or not). See {@link InstancesCounterLoader} component for more
 * infos.
 * <p>
 * This component algorithm assumes that the functions of an interface "s"
 * server of a component "foo" can be generated in "static" fashion (singleton
 * style, == without the _this) if and only if:
 * <ol>
 * <li>the component "foo" is a singleton component type</li>
 * <li>every client interface which is bound to the interface "s" of the
 * component "foo" is bound as &lt;binding dynamique='false' /&gt; </li>
 * <li>the component "foo" 's "component" interface is not bound by any other
 * component (TO BE IMPLEMENTED)</li>
 * <li>(how should composite component's interfaces and controller (even
 * implemented explicitly ones) interfaces be considered ?)</li>
 * </ol>
 * </p>
 * <p>
 * This component decorate components' {@link TypeInterface} nodes with the
 * {@link InterfaceDecorationUtil#STATIC_INTERFACE_DECORATION} with a
 * <code>false</code> if the interface does not match the conditions listed
 * above.
 * </p>
 *
 * @author Alessio Pace
 */
public class StaticInterfacesLoader extends AbstractLoader {

  /*
   * (non-Javadoc)
   *
   * @see org.objectweb.fractal.adl.Loader#load(java.lang.String, java.util.Map)
   */
  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    final Definition definition = clientLoader.load(name, context);

    check((ComponentContainer) definition);

    return definition;
  }

  protected void check(final ComponentContainer component) {

    /* if it's a composite, invoke recursively on subcomponents */
    if (!ASTFractalUtil.isPrimitive(component)) {
      final ComponentContainer[] subComponents = ASTFractalUtil
          .getFcSubComponents(component);
      for (final ComponentContainer subComp : subComponents) {
        check(subComp);
      }
    } else {
      Printer.debug("Processing component: "
          + ContainmentChecker.getFullComponentNameDecoration(component));

      /* for each client interface */
      for (final TypeInterface ti : ASTFractalUtil
          .getFcFunctionalClientInterfaces(component)) {

        final Boolean isDynamic = BindingDecorationUtil.isDynamic((Node)ti);

        /* if it's dynamic.. */
        if (isDynamic) {

          final TypeInterface boundToItf = (TypeInterface) InterfaceDecorationUtil
              .getBoundTo(ti);
          /* ..and it if is actually bound to a server itf */
          if (boundToItf != null) {

            /*
             * set explicitly as NOT static the server interface on the
             * component type representing the actual owner of the boundTo
             * interface (because the component type is the one processed for
             * compiling component definitions).
             */
            final ComponentContainer boundToItfOwer = (ComponentContainer) InterfaceDecorationUtil
                .getContainer(boundToItf);
            if (boundToItfOwer == null) {
              throw new IllegalStateException(
                  "The itf should have been previously decorated with the owener (container) decoration");
            }
            final ComponentContainer boundToItfCompType = TypeDecorationUtil
                .getTypeDecoration(boundToItfOwer);
            if (boundToItfCompType == null) {
              throw new IllegalStateException(
                  "The component '"
                      + ASTFractalUtil.getFcName(boundToItfOwer)
                      + "' should have been previouly decorated with a component type decoration");
            }
            final TypeInterface boundToItfOnCompType = ASTFractalUtil
                .getFcInterface(boundToItfCompType, boundToItf.getName());
            InterfaceDecorationUtil.setStaticInterfaceDecoration(
                boundToItfOnCompType, false);
          }
        } else {

          /* else if dynamic="false" for this client interface */
          final TypeInterface boundToItf = (TypeInterface) InterfaceDecorationUtil
              .getBoundTo(ti);
          final ComponentContainer serverItfOwner = (ComponentContainer) InterfaceDecorationUtil
              .getContainer(boundToItf);
          final ComponentContainer serverItfOwnerComponentType = TypeDecorationUtil
              .getTypeDecoration(serverItfOwner);
          final TypeInterface serverItfOnCompType = ASTFractalUtil
              .getFcInterface(serverItfOwnerComponentType, boundToItf.getName());
          if (TypeDecorationUtil
              .isSingletonComponentType(serverItfOwnerComponentType)) {
            // This interface is static.

            // Get the decoration before setting it to true.
            Boolean wasStatic = InterfaceDecorationUtil
                .getStaticInterfaceDecoration(serverItfOnCompType);
            InterfaceDecorationUtil.setStaticInterfaceDecoration(
                serverItfOnCompType, true);

            if (   (wasStatic == null || !wasStatic)
                && (InterfaceDecorationUtil
                        .hasToBeCompiledAsStatic(serverItfOnCompType))
               ) {
              // If:
              // * it's the first time we're seeing this interface
              // AND
              // * after decorating this interface, it can actually be compiled
              // as static (which means that CECILIA_ENABLE_THIS_OPTIMIZATION is
              // "true")
              // then change the signatures on both sides of the interface.
              // If the second condition is not present, multiple tests break.
              System.out.println(boundToItf + " (signature "
              + boundToItf.getSignature() + ") of " + serverItfOwner +
              " is static");

              // Change the interface's signature.
              boundToItf.setSignature(boundToItf.getSignature()
                  + IDLDefinitionVisitor.STATIC_POSTFIX);
              InterfaceDecorationUtil.setBoundTo(ti,boundToItf);

              ((InterfaceContainer) component).removeInterface(ti);
              ti.setSignature(ti.getSignature()
                  + IDLDefinitionVisitor.STATIC_POSTFIX);
              ((InterfaceContainer) component).addInterface(ti);
            }
          } else {
            InterfaceDecorationUtil.setStaticInterfaceDecoration(
                serverItfOnCompType, false);
          }
        }
      }
    }
  }
}
