/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.composite.c.controllers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.bindings.Binding;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.cecilia.adl.InstanceNameProvider;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterfaceForEach;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the instantiation of the boot binding controller
 * data structure of a default composite component.
 */
public class BCBootInstantiationVisitor extends BCInstantiationVisitor {

  // ---------------------------------------------------------------------------
  // Overridden methods of BCInstantiationVisitor
  // ---------------------------------------------------------------------------

  @Override
  protected org.objectweb.fractal.api.Component createTask(
      final ComponentContainer container, final Binding[] bindings)
      throws TaskException {
    return taskFactoryItf.newPrimitiveTask(
        new BCBootInstantiationTask(bindings), container, Arrays
            .asList(container.getComponents()));
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  @TaskParameters({"componentNode", "subComponents"})
  @ServerInterfaces(@ServerInterface(name = "bc-instantiation-provider", signature = SourceCodeProvider.class, record = "role:controllerInstantiation, id:%, controller:BC", parameters = "componentNode"))
  public static class BCBootInstantiationTask extends BCInstantiationTask {

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param bindings the binding nodes.
     */
    public BCBootInstantiationTask(final Binding[] bindings) {
      super(bindings);
    }

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /** The {@link InstanceNameProvider} task for each sub component. */
    @ClientInterfaceForEach(prefix = "subComp-instance-names", signature = InstanceNameProvider.class, iterable = "subComponents", record = "role:subComponentInstanceNameProvider, id:%", parameters = "subComponents.element")
    public final Map<Component, InstanceNameProvider> subComponentInstanceNameProviderItfs = new HashMap<Component, InstanceNameProvider>();

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractInstantiationTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter();
      cw.appendln(super.processSourceCode());

      for (final Binding binding : bindings) {
        final String from = binding.getFrom();
        int index = from.indexOf('.');
        final String clientComp = from.substring(0, index);
        final String clientItf = from.substring(index + 1);

        final String to = binding.getTo();
        index = to.indexOf('.');
        final String serverComp = to.substring(0, index);
        final String serverItf = to.substring(index + 1);

        // look for the "boot" binding: it must be an internal binding
        // and the name of the client interface must be "boot"
        if ((!serverComp.equals("this")) && (!clientComp.equals("this"))) {
          // binding is a normal binding
          if (!clientItf.equals("boot"))
            throw new ADLException(
                "A boot controller can only accept an internal binding for a client"
                    + " interface called 'boot'", (Node) null);

          // find the InstanceNameProvider task for the server component
          InstanceNameProvider serverInstanceNameProvider = null;
          for (final Map.Entry<Component, InstanceNameProvider> entry : subComponentInstanceNameProviderItfs
              .entrySet()) {
            if (entry.getKey().getName().equals(serverComp)) {
              serverInstanceNameProvider = entry.getValue();
              break;
            }
          }
          if (serverInstanceNameProvider == null) {
            throw new CompilerError("Can't find component '" + serverComp + "'");
          }
          cw.append("void *__").append(clientItf).append("_itf__ = &(").append(
              serverInstanceNameProvider.getCInstanceName()).append(
              ".").append(serverItf).append(");").endl();
        }
      }

      return cw.toString();
    }
  }
}
