/***
 * Cecilia ADL Compiler
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.implementations;

import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.cecilia.adl.directives.IncludeContainer;

/**
 * AST Node interface which adds a <code>language</code> attribute to
 * <code>implementation</code> element.
 */
public interface ExtendedImplementation
    extends
      Implementation,
      IncludeContainer {
  /**
   * Returns the language of the implementation.
   * 
   * @return the language of the implementation.
   */
  String getLanguage();

  /**
   * Set the language of the implementation.
   * 
   * @param language the language of the implementation.
   */
  void setLanguage(String language);

  /**
   * Returns the value of the <code>hasConstructor</code> attribute.
   * 
   * @return the value of the <code>hasConstructor</code> attribute.
   */
  String getHasConstructor();

  /**
   * Sets the value of the <code>hasConstructor</code> attribute.
   * 
   * @param hasConstructor the value of the <code>hasConstructor</code>
   *          attribute. Must be either <code>"true"</code> or
   *          <code>"false"</code>.
   * @see #getHasConstructor()
   */
  void setHasConstructor(String hasConstructor);

  /**
   * Returns the value of the <code>hasDestructor</code> attribute.
   * 
   * @return the value of the <code>hasDestructor</code> attribute.
   */
  String getHasDestructor();

  /**
   * Sets the value of the <code>hasDestructor</code> attribute.
   * 
   * @param hasDestructor the value of the <code>hasDestructor</code> attribute.
   *          Must be either <code>"true"</code> or <code>"false"</code>.
   * @see #getHasConstructor()
   */
  void setHasDestructor(String hasDestructor);
}
