/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.implementations;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.implementations.Controller;
import org.objectweb.fractal.adl.implementations.ControllerContainer;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.cecilia.adl.CeciliaADLConstants;
import org.objectweb.fractal.cecilia.adl.controllers.ExtendedController;
import org.xml.sax.SAXException;

/**
 * Checks {@link ExtendedImplementation#setLanguage(String) implementation} and
 * {@link ExtendedController#setLanguage(String) controller} language. If the
 * component does not define a "controller" node, this loader creates and adds
 * one initialized with default values.
 */
public class LanguageLoader extends AbstractLoader
    implements
      LanguageLoaderAttributes {

  // -------------------------------------------------------------------------
  // Constants
  // -------------------------------------------------------------------------

  /**
   * The name of the system property that specify the value of the
   * <code>defaultPPL</code> attribute.
   * 
   * @see LanguageLoaderAttributes#setDefaultPPL(String)
   */
  public static final String DEFAULT_PPL_PROPERTY_NAME  = "fractaladl.defaultPPL";

  /**
   * The name of the system property that specify the value of the
   * <code>defaultCPL</code> attribute.
   * 
   * @see LanguageLoaderAttributes#setDefaultCPL(String)
   */
  public static final String DEFAULT_CPL_PROPERTY_NAME  = "fractaladl.defaultCPL";

  /**
   * The name of the system property that specify the value of the
   * <code>defaultCPLs</code> attribute.
   * 
   * @see LanguageLoaderAttributes#setDefaultCPLs(String)
   */
  public static final String DEFAULT_CPLS_PROPERTY_NAME = "fractaladl.defaultCPLs";

  /**
   * The default value of the <code>defaultPPL</code> attribute.
   * 
   * @see LanguageLoaderAttributes#setDefaultPPL(String)
   */
  public static final String DEFAULT_PPL                = "thinkMC";

  /**
   * The default value of the <code>defaultCPL</code> attribute.
   * 
   * @see LanguageLoaderAttributes#setDefaultCPL(String)
   */
  public static final String DEFAULT_CPL                = "c";

  // --------------------------------------------------------------------------
  // Attribute fields
  // --------------------------------------------------------------------------

  protected String           defaultPPL;
  protected String           defaultCPL;
  protected String           defaultCPLs;

  // --------------------------------------------------------------------------
  // Client interface
  // --------------------------------------------------------------------------

  /** The name of the {@link XMLNodeFactory} client interface. */
  public static final String NODE_FACTORY_ITF           = "node-factory";

  /** The {@link XMLNodeFactory} client interface. */
  protected XMLNodeFactory   nodeFactoryItf;

  // --------------------------------------------------------------------------
  // Implementation of the Loader interface
  // --------------------------------------------------------------------------

  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    final Definition d = clientLoader.load(name, context);
    checkNode(d, context);
    return d;
  }

  // --------------------------------------------------------------------------
  // Checking methods
  // --------------------------------------------------------------------------

  protected void checkNode(final Node node, final Map<Object, Object> context)
      throws ADLException {
    boolean composite = false;
    String implemLanguage = null;
    Implementation impl = null;
    if (node instanceof ImplementationContainer) {
      final ImplementationContainer implContainer = (ImplementationContainer) node;
      impl = (implContainer).getImplementation();
      if (impl == null)
        composite = true;
      else {
        ExtendedImplementation extImpl;
        if (impl instanceof ExtendedImplementation) {
          extImpl = (ExtendedImplementation) impl;
        } else {
          try {
            extImpl = (ExtendedImplementation) nodeFactoryItf.newXMLNode(
                CeciliaADLConstants.CECILIA_ADL_DTD,
                CeciliaADLConstants.IMPLEMENTATION_AST_NODE_NAME);
          } catch (final SAXException e) {
            throw new CompilerError(GenericErrors.INTERNAL_ERROR, e,
                "Unable to create implementation node.");
          }

          // transfert node state
          final Node implNode = (impl);
          final Node extImplNode = extImpl;
          extImplNode.astSetAttributes(implNode.astGetAttributes());
          extImplNode.astSetDecorations(implNode.astGetDecorations());
          extImplNode.astSetSource(implNode.astGetSource());
        }

        implemLanguage = extImpl.getLanguage();
        if (implemLanguage == null) {
          if (defaultPPL == null)
            throw new ADLException(ImplementationErrors.MISSING_LANGUAGE,
                extImpl);
          implemLanguage = defaultPPL;
          extImpl.setLanguage(implemLanguage);
        }
      }
    }
    if (node instanceof ControllerContainer) {
      final ControllerContainer ctrlContainer = (ControllerContainer) node;
      final Controller ctrl = ctrlContainer.getController();
      ExtendedController extCtrl;
      if (ctrl == null || !(ctrl instanceof ExtendedController)) {
        try {
          extCtrl = (ExtendedController) nodeFactoryItf.newXMLNode(
              CeciliaADLConstants.CECILIA_ADL_DTD,
              CeciliaADLConstants.CONTROLLER_AST_NODE_NAME);
        } catch (final SAXException e) {
          throw new CompilerError(GenericErrors.INTERNAL_ERROR, e,
              "Unable to create controller node.");
        }
        if (ctrl != null) {
          extCtrl.astSetAttributes(ctrl.astGetAttributes());
          extCtrl.astSetDecorations(ctrl.astGetDecorations());
          extCtrl.astSetSource(ctrl.astGetSource());
        } else {
          if (impl != null)
            extCtrl.astSetSource(impl.astGetSource());
          else
            extCtrl.astSetSource(node.astGetSource());
        }
        ctrlContainer.setController(extCtrl);
      } else {
        extCtrl = (ExtendedController) ctrl;
      }

      if (extCtrl.getDescriptor() == null)
        extCtrl.setDescriptor(composite ? "composite" : "primitive");

      if (extCtrl.getLanguage() == null) {
        extCtrl.setLanguage(defaultCPL);
      }
    }
    if (node instanceof ComponentContainer) {
      for (final Component comp : ((ComponentContainer) node).getComponents()) {
        checkNode(comp, context);
      }
    }
  }

  // --------------------------------------------------------------------------
  // Implementation of the AttributeController interface
  // --------------------------------------------------------------------------

  public void setDefaultPPL(final String implemLanguage) {
    defaultPPL = implemLanguage;
  }

  public String getDefaultPPL() {
    return defaultPPL;
  }

  public void setDefaultCPL(final String compositeLanguage) {
    defaultCPL = compositeLanguage;
  }

  public String getDefaultCPL() {
    return defaultCPL;
  }

  // --------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // --------------------------------------------------------------------------

  @Override
  public String[] listFc() {
    final String[] superList = super.listFc();
    final String[] list = new String[superList.length + 1];
    System.arraycopy(superList, 0, list, 0, superList.length);
    list[superList.length] = NODE_FACTORY_ITF;
    return list;
  }

  @Override
  public Object lookupFc(final String s) throws NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(NODE_FACTORY_ITF)) {
      return this.nodeFactoryItf;
    } else {
      return super.lookupFc(s);
    }
  }

  @Override
  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(NODE_FACTORY_ITF)) {
      this.nodeFactoryItf = (XMLNodeFactory) o;
    } else {
      super.bindFc(s, o);
    }
  }

  @Override
  public void unbindFc(final String s) throws IllegalBindingException,
      NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(NODE_FACTORY_ITF)) {
      this.nodeFactoryItf = null;
    } else {
      super.unbindFc(s);
    }
  }

}
