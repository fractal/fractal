/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.composite.c.components;

import static org.objectweb.fractal.api.type.TypeFactory.OPTIONAL;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Builds the definition of a composite component.
 */
public class ComponentDefinitionVisitor extends AbstractTaskFactoryUser
    implements
      ComponentVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link ComponentContainer} nodes and creates a task that writes the
   * source code containing the definition of the component data structure.
   */
  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    return taskFactoryItf.newPrimitiveTask(new ComponentDefinitionTask(),
        container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds source code for the definition of the component data structure for a
   * default composite component. This task provides the produced source code.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "component-definition-provider", signature = SourceCodeProvider.class, record = "role:componentDefinition, id:%", parameters = "componentNode"))
  public static class ComponentDefinitionTask extends AbstractDefinitionTask {

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /** Source code to be included for interface definitions (optional). */
    @ClientInterface(name = "interface-definition-provider", contingency = OPTIONAL, record = "role:interfaceDefinition, id:%", parameters = "componentNode")
    public SourceCodeProvider interfaceDefinitionsProviderItf;

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------
    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter("Composite Definition Builder");
      cw.append("#define IN_CECILIA_MECHANISM").endl();
      cw.append("#include <fractal/api/defaultComposite.h>").endl();

      if (interfaceDefinitionsProviderItf != null)
        cw.appendln(interfaceDefinitionsProviderItf.getSourceCode()).endl();

      cw.endl();
      return cw.toString();
    }
  }
}
