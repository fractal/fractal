/***
 * Cecilia
 * Copyright (C) 2006-2009 STMicroelectronics
 * Copyright (C) 2007-2009 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.adl.ast;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.bindings.Binding;
import org.objectweb.fractal.adl.bindings.BindingContainer;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.implementations.Controller;
import org.objectweb.fractal.adl.implementations.ControllerContainer;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.cecilia.adl.directives.Include;
import org.objectweb.fractal.cecilia.adl.directives.IncludeContainer;
import org.objectweb.fractal.cecilia.adl.implementations.ExtendedImplementation;

/**
 * An utility class which tries to mimic the Fractal API to simplify AST nodes
 * processing and manipulation. The following "mappings" are used:
 * <ul>
 * <li>{@link ComponentContainer} -&gt; {@link fractal.api.Component}</li>
 * <li>{@link TypeInterface} -&gt; {@link fractal.api.Interface}</li>
 * <li>{@link Controller} -&gt; {@link Object} (<em>controller</em> is not
 * typed in the Fractal API)</li>
 * <li>{@link Implementation} -&gt; {@link Object} (<em>content</em> is not
 * typed in the Fractal API)</li>
 * </ul>
 * <strong>NOTE</strong>: some methods may or may not be implemented, depending
 * on the fact that this utility class should be aware or not of decorations on
 * nodes.
 * 
 * @author Alessio Pace
 */
public final class ASTFractalUtil {

  private ASTFractalUtil() {
    // empty to prevent instantiation.
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /*
   * methods resembling {@link fractal.api.Component}
   */

  /**
   * The interfaces (client and server) of the <code>component</code> passed
   * as parameter, an empty array is returned if the component has no
   * interfaces.
   * 
   * @return the interfaces (client and server) of the <code>component</code>
   *         passed as parameter, an empty array is returned if the component
   *         has no interfaces.
   */
  public static TypeInterface[] getFcInterfaces(
      final ComponentContainer component) {

    if (component == null) {
      throw new IllegalArgumentException("Component argument is can't be null");
    }

    final Interface[] itfs = ((InterfaceContainer) component).getInterfaces();
    final TypeInterface[] result = new TypeInterface[itfs.length];
    System.arraycopy(itfs, 0, result, 0, itfs.length);
    return result;
  }

  /**
   * Return an array (possibly empty) of the controller interfaces of the
   * <code>component</code>, where controller interfaces are either the
   * <code>"component"</code> itf or the ones whose name ends with
   * <code>"-controller"</code>.
   * 
   * @param component
   * @return an array (possibly empty) of the controller interfaces of the
   *         <code>component</code>, where controller interfaces are either
   *         the "component" itf or the ones ending with "-controller".
   */
  public static TypeInterface[] getFcControlInterfaces(
      final ComponentContainer component) {

    if (component == null) {
      throw new IllegalArgumentException("Component argument is null");
    }

    final Interface[] itfs = ((InterfaceContainer) component).getInterfaces();
    final List<TypeInterface> controlInterfaces = new ArrayList<TypeInterface>();
    for (final Interface ti : itfs) {
      final String name = ti.getName();
      if (!isFunctionalInterfaceName(name)) {
        controlInterfaces.add((TypeInterface) ti);
      }
    }

    final TypeInterface[] result = new TypeInterface[controlInterfaces.size()];
    return controlInterfaces.toArray(result);

  }

  /**
   * return <code>true</code> if the itfName is neither called "component" nor
   * ends with "-controller", <code>false</code> otherwise.
   * 
   * @param itfName name of the interface
   * @return <code>true</code> if the itfName is neither called "component"
   *         nor ends with "-controller", <code>false</code> otherwise.
   */
  public static boolean isFunctionalInterfaceName(final String itfName) {
    return !itfName.equals("component") && !itfName.endsWith("-controller");
  }

  /**
   * Return an array (possibly empty) of the functional interfaces of the
   * <code>component</code>, where functional interfaces are the ones NOT
   * called <code>"component"</code>, NOR the ones ending with
   * <code>"-controller"</code>.
   * 
   * @param component
   * @return an array (possibly empty) of the functional interfaces of the
   *         <code>component</code>, where functional interfaces are the ones
   *         NOT called "component", NOR the ones ending with "-controller".
   */
  public static TypeInterface[] getFcFunctionalInterfaces(
      final ComponentContainer component) {

    if (component == null) {
      throw new IllegalArgumentException("Component argument is null");
    }

    final Interface[] itfs = ((InterfaceContainer) component).getInterfaces();
    final List<TypeInterface> functionalInterfaces = new ArrayList<TypeInterface>();
    for (final Interface ti : itfs) {
      final String name = ti.getName();
      if (isFunctionalInterfaceName(name)) {
        functionalInterfaces.add((TypeInterface) ti);
      }
    }

    final TypeInterface[] result = new TypeInterface[functionalInterfaces
        .size()];
    return functionalInterfaces.toArray(result);
  }

  /**
   * Return an array (possibly empty) of the <strong>server</strong> functional
   * interfaces of the <code>component</code>, where the server functional
   * interfaces are the ones NOT called <code>"component"</code>, NOR the
   * ones ending with <code>"-controller"</code>.
   * 
   * @param component
   * @return an array (possibly empty) of the server functional interfaces of
   *         the <code>component</code>, where server functional interfaces
   *         are the ones NOT called "component", NOR the ones ending with
   *         "-controller".
   */
  public static TypeInterface[] getFcFunctionalServerInterfaces(
      final ComponentContainer component) {

    if (component == null) {
      throw new IllegalArgumentException("Component argument is null");
    }

    final Interface[] itfs = ((InterfaceContainer) component).getInterfaces();
    final List<TypeInterface> functionalInterfaces = new ArrayList<TypeInterface>();
    for (final Interface ti : itfs) {
      final String name = ti.getName();
      if (isFunctionalInterfaceName(name)
          && ((TypeInterface) ti).getRole().equals(TypeInterface.SERVER_ROLE)) {
        functionalInterfaces.add((TypeInterface) ti);
      }
    }

    final TypeInterface[] result = new TypeInterface[functionalInterfaces
        .size()];
    return functionalInterfaces.toArray(result);
  }

  /**
   * Return an array (possibly empty) of the <strong>client</strong> functional
   * interfaces of the <code>component</code>, where functional interfaces
   * are the ones NOT called "component", NOR the ones ending with
   * "-controller".
   * 
   * @param component
   * @return an array (possibly empty) of the <strong>client</strong>
   *         functional interfaces of the <code>component</code>, where
   *         functional interfaces are the ones NOT called "component", NOR the
   *         ones ending with "-controller".
   */
  public static TypeInterface[] getFcFunctionalClientInterfaces(
      final ComponentContainer component) {

    if (component == null) {
      throw new IllegalArgumentException("Component argument is null");
    }

    final Interface[] itfs = ((InterfaceContainer) component).getInterfaces();
    final List<TypeInterface> functionalInterfaces = new ArrayList<TypeInterface>();
    for (final Interface ti : itfs) {
      final String name = ti.getName();
      if (isFunctionalInterfaceName(name)
          && ((TypeInterface) ti).getRole().equals(TypeInterface.CLIENT_ROLE)) {
        functionalInterfaces.add((TypeInterface) ti);
      }
    }

    final TypeInterface[] result = new TypeInterface[functionalInterfaces
        .size()];
    return functionalInterfaces.toArray(result);
  }

  /**
   * Return the {@link TypeInterface} with the given name, or <code>null</code>
   * if none is found with that name. This method delegates to
   * {@link #getFcInterface(InterfaceContainer, String)} casting the
   * <code>component</code> argument to {@link InterfaceContainer}.
   * 
   * @param component a component
   * @param name the name of the interface to be looked up
   * @return the {@link TypeInterface} with the given name, or <code>null</code>
   *         if none is found with that name.
   */
  public static TypeInterface getFcInterface(
      final ComponentContainer component, final String name) {
    return getFcInterface((InterfaceContainer) component, name);
  }

  /**
   * Return the {@link TypeInterface} with the given name, or <code>null</code>
   * if none is found with that name.
   * 
   * @param interfaceContainer an {@link InterfaceContainer}
   * @param name the name of the interface to be looked up
   * @return the {@link TypeInterface} with the given name, or <code>null</code>
   *         if none is found with that name.
   */
  public static TypeInterface getFcInterface(
      final InterfaceContainer interfaceContainer, final String name) {

    if (interfaceContainer == null) {
      throw new IllegalArgumentException("Component argument can't be null");
    }

    if (name == null) {
      throw new IllegalArgumentException(
          "Interface name argument can't be null");
    }

    for (final Interface i : (interfaceContainer).getInterfaces()) {
      if (i.getName().equals(name)) {
        return (TypeInterface) i;
      }
    }

    return null;
  }

  /**
   * XXX this method is not implemented at the moment, and it could be
   * implemented (for Cecilia) returning the value of the component type
   * decoration (which is set by an AST Loader), or <code>null</code> if there
   * is nothing to return.
   * 
   * @param component a component
   * @return the type of the component being passed as paramater.
   */
  public static Object getFcType(final ComponentContainer component) {
    throw new UnsupportedOperationException("NOT IMPLEMENTED");
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /*
   * methods resembling {@link fractal.api.Interface}
   */

  /**
   * XXX this method is not implemented at the moment, and it could be
   * implemented (for Cecilia) returning the value of the interface owner (aka
   * "interface container) decoration (which is set by an AST Loader), or
   * <code>null</code> if there is nothing to return.
   * 
   * @param component a component
   * @return the type of the component being passed as paramater.
   */
  public static ComponentContainer getFcItfOwner(final TypeInterface interfase) {
    throw new UnsupportedOperationException("NOT IMPLEMENTED");
  }

  /**
   * Return the result of the invocation of {@link TypeInterface#getName()}.
   * 
   * @param interfase
   * @return the result of the invocation of {@link TypeInterface#getName()}
   */
  public static String getFcItfName(final TypeInterface interfase) {

    if (interfase == null) {
      throw new IllegalArgumentException("Interface argument can't be null");
    }

    return interfase.getName();
  }

  /**
   * XXX this method is not implemented, and not clear what to return: the
   * signature (a String) of the interface, or (for Cecilia) the AST of the
   * Cecilia IDL interface matching the signature of the interface passed as
   * parameter.
   * 
   * @param interfase
   * @return
   */
  public static Object getFcItfType(final TypeInterface interfase) {
    throw new UnsupportedOperationException("NOT IMPLEMENTED");
  }

  /**
   * XXX not implemented, probably it is not very useful.
   * 
   * @param interfase
   * @return
   */
  public static boolean isFcInternalItf(final TypeInterface interfase) {
    throw new UnsupportedOperationException("NOT IMPLEMENTED");
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /*
   * methods resembling {@link fractal.api.control.ContentController}
   */

  /**
   * XXX this method is not implemented, probably it is not very useful.
   * 
   * @param interfase
   * @return
   */
  public static TypeInterface[] getFcInternalInterfaces() {
    throw new UnsupportedOperationException("NOT IMPLEMENTED");
  }

  /**
   * XXX not implemented, probably it is not very useful.
   * 
   * @param interfase
   * @return
   */
  public static TypeInterface getFcInternalInterface(final String interfaceName)
      throws NoSuchInterfaceException {
    throw new UnsupportedOperationException("NOT IMPLEMENTED");
  }

  /**
   * Return the subcomponents of the given <code>component</code>, an empty
   * array is returned if there are no subcomponents.
   * 
   * @return the subcomponents of the given <code>component</code>, an empty
   *         array is returned if there are no subcomponents.
   */
  public static ComponentContainer[] getFcSubComponents(
      final ComponentContainer component) {

    if (component == null) {
      throw new IllegalArgumentException("Component argument can't be null");
    }

    final ComponentContainer[] components = component.getComponents();
    return components;
  }

  /**
   * Return the subcomponent with the given <code>subComponentName</code>, or
   * <code>null</code> if none is found.
   * 
   * @param component
   * @param subComponentName
   * @return the subcomponent with the given <code>subComponentName</code>,
   *         or <code>null</code> if none is found.
   */
  public static ComponentContainer getFcSubComponentByName(
      final ComponentContainer component, final String subComponentName) {

    if (component == null) {
      throw new IllegalArgumentException("Component argument can't be null");
    }

    final ComponentContainer[] subComponents = component.getComponents();

    for (final ComponentContainer c : subComponents) {
      if (ASTFractalUtil.getFcName(c).equals(subComponentName)) {
        return c;
      }
    }

    return null;
  }

  /**
   * Add the <code>subComponent></code> into the <code>parentComponent</code>
   * if the latter is a composite componenent and it does not contain already a
   * the sub compoenent or a component with the same <code>subComponent</code>'s
   * name.
   * 
   * @param parentComponent
   * @param subComponent the sub componenent to eventually add
   */
  public static void addFcSubComponent(
      final ComponentContainer parentComponent,
      final ComponentContainer subComponent) {

    if (parentComponent == null) {
      throw new IllegalArgumentException("Parent component can't be null");
    }

    if (subComponent == null) {
      throw new IllegalArgumentException("Sub component can't be null");
    }

    if (parentComponent == subComponent) {
      throw new IllegalArgumentException(
          "Parent and sub component are the same object");
    }

    if (isPrimitive(parentComponent)) {
      throw new IllegalArgumentException(
          "Can't add a sub component to a primitive component");
    }

    final String subComponentName = ASTFractalUtil.getFcName(subComponent);
    if (subComponentName == null || subComponentName.length() == 0) {
      throw new IllegalArgumentException(
          "The sub component you want to add has no name");
    }

    final Component[] components = parentComponent.getComponents();
    for (final Component c : components) {
      if (c == subComponent) {
        throw new IllegalArgumentException(
            "The sub component was already added to the parent component");
      } else {
        if (c.getName().equals(subComponentName)) {
          throw new IllegalArgumentException(
              "There is already a sub component named '" + c.getName() + "'");
        }
      }
    }

    /*
     * after all the precondition checks, it is possible to add the
     * subcomponent.
     */
    parentComponent.addComponent((Component) subComponent);

  }

  /**
   * Remove from the <code>parentComponent</code> the specific
   * <code>subComponent</code> passed as parameter, or do nothing if the
   * <code>subComponent</code> is not present in the
   * <code>parentComponent</code>.
   * 
   * @param parentComponent the parent component
   * @param subComponent the sub component to remove
   */
  public static void removeFcSubComponent(
      final ComponentContainer parentComponent,
      final ComponentContainer subComponent) {

    if (parentComponent == null) {
      throw new IllegalArgumentException(
          "Parent component argument can't be null");
    }

    if (subComponent == null) {
      throw new IllegalArgumentException("Subcomponent argument can't be null");
    }

    parentComponent.removeComponent((Component) subComponent);
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  /**
   * XXX this method is not implemented at the moment, at the AST level a given
   * component node does not have (out of the box) informations about its
   * enclosing component(s), this information is/could be added by means of
   * decorations during AST processing and thus this method could return the
   * value of the given decoration, or null if no value is associated with it.
   * 
   * @param component the component we are interested to know the enclosing
   *            parent component(s).
   */
  public static ComponentContainer[] getFcSuperComponents(
      final ComponentContainer component) {
    throw new UnsupportedOperationException("NOT IMPLEMENTED");
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /*
   * methods resembling {@link fractal.api.control.NameController}
   */
  /**
   * Return the value of the <code>name</code> for the <code>component</code>.
   * 
   * @return the value of the <code>name</code> for the <code>component</code>.
   */
  public static String getFcName(final ComponentContainer component) {

    if (component == null) {
      throw new IllegalArgumentException("Component argument can't be null");
    }

    if (component instanceof Definition) {
      return ((Definition) component).getName();
    } else if (component instanceof Component) {
      return ((Component) component).getName();
    } else {
      throw new IllegalArgumentException(
          "The given node is neither a Definition nor a Component");        
    }
  }

  /**
   * Set the name of the <code>component</code>. It takes care of checking it
   * the {@link ComponentContainer} passed as parameter is a {@link Definition}
   * or a {@link Component} node type.
   * 
   * @param component
   * @param name
   */
  public static void setFcName(final ComponentContainer component,
      final String name) {

    if (component == null) {
      throw new IllegalArgumentException("Component argument can't be null");
    }

    if (name == null || name.length() == 0) {
      throw new IllegalArgumentException(
          "Name argument String can't be null or empty");
    }

    if (component instanceof Definition) {
      ((Definition) component).setName(name);
    } else {
      ((Component) component).setName(name);
    }
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /*
   * methods resembling {@link fractal.api.control.BindingController}
   */

  /**
   * Returns the names of the <strong>client</strong> interfaces of the
   * component.
   * 
   * @return the names of the client interfaces of the component.
   */
  public static String[] listFc(final ComponentContainer component) {
    final TypeInterface[] allInterfaces = ASTFractalUtil
        .getFcInterfaces(component);

    final List<String> clientItfNames = new ArrayList<String>();
    for (final TypeInterface i : allInterfaces) {
      if (i.getRole().equals(TypeInterface.CLIENT_ROLE)) {
        clientItfNames.add(i.getName());
      }
    }

    return clientItfNames.toArray(new String[clientItfNames.size()]);
  }

  /**
   * XXX not implemented.
   */
  public static TypeInterface lookupFc(final ComponentContainer component,
      final String name) {
    throw new UnsupportedOperationException("NOT IMPLEMENTED");
  }

  /**
   * XXX not implemented.
   */
  public static void bindFc(final ComponentContainer component,
      final TypeInterface serverItf) {
    throw new UnsupportedOperationException("NOT IMPLEMENTED");
  }

  /**
   * XXX not implemented.
   */
  public static void unbindFc(final ComponentContainer component,
      final String name) {
    throw new UnsupportedOperationException("NOT IMPLEMENTED");
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /*
   * methods resembling {@link fractal.api.factory.Factory}
   */

  /**
   * Return the {@link Controller} AST node returned by
   * {@link ControllerContainer#getController()} on the <code>component</code>
   * passed as parameter, the result can be eventually <code>null</code>.
   * 
   * @return the {@link Controller} AST node returned by
   *         {@link ControllerContainer#getController()} on the
   *         <code>component</code> passed as parameter, the result can be
   *         eventually <code>null</code>.
   */
  public static Controller getFcControllerDesc(
      final ComponentContainer component) {

    if (component == null) {
      throw new IllegalArgumentException("Component argument can't be null");
    }

    return ((ControllerContainer) component).getController();
  }

  /**
   * Set the {@link Controller} AST node on the <code>component</code> passed
   * as parameter.
   * 
   * @param component the component node upon which the controller has to be
   *            set.
   * @param controllerDesc the Controller node to set.
   */
  public static void setFcControllerDesc(final ComponentContainer component,
      final Controller controllerDesc) {

    if (component == null) {
      throw new IllegalArgumentException("Component argument can't be null");
    }

    ((ControllerContainer) component).setController(controllerDesc);

  }

  /**
   * Return the {@link Implementation} node of the given component, or
   * <code>null</code> if none is present.
   * 
   * @param component a component AST node.
   * @return the {@link Implementation} node of the given component, or
   *         <code>null</code> if none is present.
   */
  public static Implementation getFcContentDesc(
      final ComponentContainer component) {

    if (component == null) {
      throw new IllegalArgumentException("Component argument can't be null");
    }

    return ((ImplementationContainer) component).getImplementation();
  }

  /**
   * Set the {@link Implementation} node of the given component, or
   * <code>null</code> if none is present, <strong>without checking if the
   * component is a composite or not</strong>.
   * 
   * @param component a component AST node.
   * @param content an Implementation AST node.
   */
  public static void setFcContentDesc(final ComponentContainer component,
      final Implementation content) {

    if (component == null) {
      throw new IllegalArgumentException("Component argument can't be null");
    }

    ((ImplementationContainer) component).setImplementation(content);

  }

  /**
   * XXX not implemented at the moment, it could be implemented as a
   * <em>clone</em> of the given <code>component</code> passed as parameter.
   * 
   * @param component
   * @return
   */
  public static ComponentContainer newFcInstance(
      final ComponentContainer component) {
    throw new UnsupportedOperationException("NOT IMPLEMENTED");
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /*
   * Other miscellaneous methods dealing with generic components manipulations.
   */

  /**
   * Return <code>true</code> if the given component has an
   * {@link Implementation} node not null, <code>false</code> otherwise.
   * 
   * @param component a component.
   * @retun <code>true</code> if the given component has an
   *        {@link Implementation} node not null, <code>false</code>
   *        otherwise.
   */
  public static boolean isPrimitive(final ComponentContainer component) {

    if (component == null) {
      throw new IllegalArgumentException("Component argument can't be null");
    }

    return component instanceof ImplementationContainer
        && ((ImplementationContainer) component).getImplementation() != null;
  }

  /**
   * Return an array (possibly emtpy) of the {@link Binding} of the given
   * component.
   * 
   * @param component a component.
   * @return an array (possibly emtpy) of the {@link Binding} of the given
   *         component.
   */
  public static Binding[] getBindings(final ComponentContainer component) {

    if (component == null) {
      throw new IllegalArgumentException("Component argument can't be null");
    }

    return ((BindingContainer) component).getBindings();
  }

  /**
   * Return the {@link Binding}s AST nodes in which the
   * {@link Binding#getTo()#equals(to)} == true , or an empty container is none
   * is found.
   * 
   * @param component
   * @param to
   * @return the {@link Binding}s AST nodes in which the
   *         {@link Binding#getTo()#equals(to)} == true , or an empty container
   *         is none is found.
   */
  public static Binding[] getBindingsByFullToAttrValue(
      final ComponentContainer component, final String to) {

    final List<Binding> result = new ArrayList<Binding>();

    for (final Binding b : ((BindingContainer) component).getBindings()) {
      if (b.getTo().equals(to)) {
        result.add(b);
      }
    }

    return result.toArray(new Binding[result.size()]);
  }

  /**
   * The {@link Binding} which has the {@link Binding#getFrom()} equals to the
   * <code>from</code> parameter, or <code>null</code> if none is found.
   * 
   * @param component
   * @param from
   * @return the {@link Binding} which has the {@link Binding#getFrom()} equals
   *         to the <code>from</code> parameter, or <code>null</code> if
   *         none is found.
   */
  public static Binding getBindingByFullFromAttrValue(
      final ComponentContainer component, final String from) {

    for (final Binding b : ((BindingContainer) component).getBindings()) {
      if (b.getFrom().equals(from)) {
        return b;
      }
    }

    return null;

  }

  // ---------------------------------------------------------------
  // Cecilia ADL specific AST utility methods

  /**
   * Adds the specified {@link Include} node in the given
   * {@link ExtendedImplementation} node (<strong>XXX: this method is Cecilia
   * specific</strong>).
   * 
   * @param implementation
   */
  public static void addInclude(final Implementation implementation,
      final Include include) {

    if (implementation == null) {
      throw new IllegalArgumentException("Implementation node can't be null");
    }

    if (include == null) {
      throw new IllegalArgumentException("Include node can't be null");
    }

    ((IncludeContainer) implementation).addInclude(include);
  }

  /**
   * Return an (eventually empty) array of the {@link Include} AST nodes
   * contained in the given {@link Implementation} AST node (<strong>XXX: this
   * method is Cecilia specific</strong>).
   * 
   * @param implementation an Implementation node.
   * @return an (eventually empty) array of the {@link Include} AST nodes
   *         contained in the given {@link Implementation} AST node.
   */
  public static Include[] getIncludes(final Implementation implementation) {
    return ((IncludeContainer) implementation).getIncludes();
  }

}
