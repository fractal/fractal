/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.adl.interfaces;

import static org.objectweb.fractal.cecilia.adl.ReservedWordsChecker.isReservedCWord;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.ChainedErrorLocator;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.error.NodeErrorLocator;
import org.objectweb.fractal.adl.interfaces.IDLLoader;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.merger.MergeException;
import org.objectweb.fractal.adl.merger.NodeMerger;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.InterfaceDefinition;
import org.xml.sax.SAXException;

/**
 * A {@link Loader} to check {@link Interface} nodes in definitions. This loader
 * checks:
 * <ul>
 * <li>that interface names are locally unique</li>
 * <li>loads the AST of the interface definition</li>
 * <li>merges the AST definition of the interface with the one of the ADL.</li>
 * </ul>
 */
public class InterfaceLoader extends AbstractLoader {

  /**
   * The URL of the DTD that defines AST node interfaces for bridging ADL and
   * IDL trees.
   */
  public static final String ADL_IDL_BRIGDE_DTD   = "classpath://org/objectweb/fractal/cecilia/adl/idl/adl-idl-bridge.dtd";

  // ---------------------------------------------------------------------------
  // Client interfaces
  // ---------------------------------------------------------------------------

  /** The name of the {@link #idlLoaderItf} client interface. */
  public static final String INTERFACE_LOADER_ITF = "interface-loader";

  /** The {@link IDLLoader} client interface used by this component. */
  public IDLLoader           idlLoaderItf;

  /** The {@link NodeMerger} client interface used by this component. */
  public NodeMerger          nodeMergerItf;

  /** The {@link XMLNodeFactory} client interface used by this component. */
  public XMLNodeFactory      nodeFactoryItf;

  // ---------------------------------------------------------------------------
  // Implementation of the Loader interface
  // ---------------------------------------------------------------------------

  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    final Definition d = clientLoader.load(name, context);
    checkNode(d, context);
    return d;
  }

  // ---------------------------------------------------------------------------
  // Checking methods
  // ---------------------------------------------------------------------------

  protected void checkNode(final Object node, final Map<Object, Object> context)
      throws ADLException {
    if (node instanceof InterfaceContainer) {
      checkInterfaceContainer((InterfaceContainer) node, context);
    }
    if (node instanceof ComponentContainer) {
      for (final Component comp : ((ComponentContainer) node).getComponents()) {
        checkNode(comp, context);
      }
    }
  }

  protected void checkInterfaceContainer(final InterfaceContainer container,
      final Map<Object, Object> context) throws ADLException {
    final Map<String, Interface> names = new HashMap<String, Interface>();
    for (Interface itf : container.getInterfaces()) {
      final String name = itf.getName();
      if (name == null) {
        throw new ADLException(
            org.objectweb.fractal.adl.interfaces.InterfaceErrors.INTERFACE_NAME_MISSING,
            itf);
      }
      if (isReservedCWord(name)) {
        throw new ADLException(InterfaceErrors.IDL_NAME_IS_RESERVED_WORD, name);
      }
      final Interface previousDefinition = names.put(name, itf);
      if (previousDefinition != null) {
        throw new ADLException(
            org.objectweb.fractal.adl.interfaces.InterfaceErrors.DUPLICATED_INTERFACE_NAME,
            itf, name, new NodeErrorLocator(previousDefinition));
      }
      if (!(itf instanceof IDLDefinitionContainer)) {
        // the interface node is not an IDLDefinitionContainer
        // - create a bridge node;
        // - merge it with the interface node.
        try {
          Node bridge = nodeFactoryItf.newXMLNode(ADL_IDL_BRIGDE_DTD,
              ((Node) itf).astGetType());
          bridge = nodeMergerItf.merge(itf, bridge,
              new HashMap<String, String>());
          if (bridge != itf) {
            // replace itf node
            container.removeInterface(itf);
            itf = (Interface) bridge;
            container.addInterface(itf);
          }
        } catch (final SAXException e) {
          throw new CompilerError(GenericErrors.INTERNAL_ERROR, e,
              "Unable to create AST node.");
        } catch (final MergeException e) {
          throw new CompilerError(GenericErrors.INTERNAL_ERROR, e,
              "Unable to merge interface node.");
        }
      }
      // Load the interface definition node
      Node interfaceDefinition = null;
      final String signature = ((TypeInterface) itf).getSignature();
      try {
        interfaceDefinition = (Node) idlLoaderItf.loadInterface(signature,
            context);
      } catch (final ADLException e) {
        ChainedErrorLocator.chainLocator(e, itf);
        throw e;
      }
      // Test if the node is an instance of InterfaceDefinition
      if (!(interfaceDefinition instanceof InterfaceDefinition)) {
        throw new ADLException(InterfaceErrors.INVALID_SIGNATURE, itf,
            signature);
      }
      // Add the interface definition node into the interface node
      ((IDLDefinitionContainer) itf)
          .setIDLDefinition((InterfaceDefinition) interfaceDefinition);
    }
  }

  // ---------------------------------------------------------------------------
  // Overridden BindingController methods
  // ---------------------------------------------------------------------------

  @Override
  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(INTERFACE_LOADER_ITF)) {
      idlLoaderItf = (IDLLoader) o;
    } else if (s.equals(NodeMerger.ITF_NAME)) {
      nodeMergerItf = (NodeMerger) o;
    } else if (s.equals(XMLNodeFactory.ITF_NAME)) {
      nodeFactoryItf = (XMLNodeFactory) o;
    } else {
      super.bindFc(s, o);
    }
  }

  @Override
  public String[] listFc() {
    final String[] superList = super.listFc();
    final String[] list = new String[superList.length + 3];
    list[0] = INTERFACE_LOADER_ITF;
    list[1] = NodeMerger.ITF_NAME;
    list[2] = XMLNodeFactory.ITF_NAME;
    System.arraycopy(superList, 0, list, 3, superList.length);
    return list;
  }

  @Override
  public Object lookupFc(final String s) throws NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(INTERFACE_LOADER_ITF)) {
      return idlLoaderItf;
    } else if (s.equals(NodeMerger.ITF_NAME)) {
      return nodeMergerItf;
    } else if (s.equals(XMLNodeFactory.ITF_NAME)) {
      return nodeFactoryItf;
    } else {
      return super.lookupFc(s);
    }
  }

  @Override
  public void unbindFc(final String s) throws IllegalBindingException,
      NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(INTERFACE_LOADER_ITF)) {
      idlLoaderItf = null;
    } else if (s.equals(NodeMerger.ITF_NAME)) {
      nodeMergerItf = null;
    } else if (s.equals(XMLNodeFactory.ITF_NAME)) {
      nodeFactoryItf = null;
    } else {
      super.unbindFc(s);
    }
  }
}
