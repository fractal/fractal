/***
 * Cecilia ADL Compiler
 * Copyright (C) 2009 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Lionel Debroux
 *
 */

package org.objectweb.fractal.cecilia.adl.interfaces;

import static org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil.setFlattenedCollectionCardinality;
import static org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil.setFlattenedCollectionName;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.NodeUtil;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;

/**
 * A {@link org.objectweb.fractal.adl.Loader} to handle {@link Interface} nodes
 * that contain fake collection interfaces (convenient one-line declaration for
 * n..m cardinality collection interfaces). These are flattened to m singleton
 * interfaces.
 */
public class BoundedCollectionInterfaceFlattenerLoader extends AbstractLoader {

  // --------------------------------------------------------------------------
  // Implementation of the Loader interface
  // --------------------------------------------------------------------------

  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    final Definition d = clientLoader.load(name, context);
    checkNode(d, context);
    return d;
  }

  private void checkNode(final Object node, final Map<Object, Object> context)
      throws ADLException {
    if (node instanceof InterfaceContainer) {
      checkInterfaceContainer((InterfaceContainer) node);
    }
    if (node instanceof ComponentContainer) {
      for (final Component comp : ((ComponentContainer) node).getComponents()) {
        checkNode(comp, context);
      }
    }
  }

  private void checkInterfaceContainer(final InterfaceContainer container)
      throws ADLException {
    final Interface[] itfs = container.getInterfaces();
    if (itfs != null) {
      for (final Interface itf : itfs) {
        NodeUtil.castNodeError(itf, TypeInterface.class);

        final String cardinalityStr = ((TypeInterface) itf).getCardinality();

        // if the cardinality is null, pass it.
        if (cardinalityStr == null) continue;
        try {
          final Integer cardinality = Integer.valueOf(cardinalityStr);
          if (cardinality > 0) {
            final String itfName = itf.getName();
            // We have a valid bounded "collection" interface.
            // Create new singleton nodes, add them to the container.
            for (int i = 1; i <= cardinality; i++) {
              final Node newItf = NodeUtil.cloneNode(itf);
              NodeUtil.castNodeError(newItf, TypeInterface.class);

              setFlattenedCollectionName((Interface) newItf, itfName);
              setFlattenedCollectionCardinality((Interface) newItf, cardinality);
              ((TypeInterface) newItf).setName(itfName + i);
              ((TypeInterface) newItf)
                  .setCardinality(TypeInterface.SINGLETON_CARDINALITY);

              container.addInterface((Interface) newItf);
            }

            // Remove the bounded "collection" interface.
            container.removeInterface(itf);
          }
        } catch (final NumberFormatException nfe) {
          // Do nothing, as in this Loader, we want to handle only integers.
        }
      }
    }
  }
}
