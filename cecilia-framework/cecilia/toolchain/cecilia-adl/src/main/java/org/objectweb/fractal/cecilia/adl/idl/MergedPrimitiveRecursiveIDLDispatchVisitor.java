/***
 * Cecilia ADL Compiler
 * Copyright (C) 2005-2008 STMicroelectronics
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.idl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.DefinitionVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.ast.ASTFractalUtil;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.bindings.MergedBindingResolverLoader;
import org.objectweb.fractal.cecilia.adl.components.merge.MergeUtil;
import org.objectweb.fractal.cecilia.adl.components.merge.MergedInterface;
import org.objectweb.fractal.cecilia.adl.components.merge.MergedInterfaceRole;
import org.objectweb.fractal.cecilia.adl.directives.Include;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.task.core.TaskException;

/**
 * Travels the component's interfaces and attribute definitions, dispatching
 * compilations also for Interfaces which are defined in fused components inside
 * the merged primitive.
 *
 * @author Alessio Pace
 */
public class MergedPrimitiveRecursiveIDLDispatchVisitor
    extends RecursiveIDLDispatchVisitor {

  @Override
  protected void visitImplementationContainer(final List<Node> path,
      final ImplementationContainer container, final List<Component> taskComponents,
      final Set<String> visitedIDL, final Map<Object, Object> context)
      throws ADLException, TaskException {
    /*
     * travel down the includes to get the IDL used by the included modules. The
     * container is a primitive which could be a singleton or not.
     */
    final Implementation implementation = container.getImplementation();
    if (implementation != null) {

      final Include[] includes = ASTFractalUtil.getIncludes(implementation);
      for (final Include include : includes) {
        /* iterate over the interfaces of the current Include */
        for (final Interface itf : ((InterfaceContainer) include).getInterfaces()) {

          final MergedInterface mergedItf = (MergedInterface) itf;

          final MergedInterfaceRole mergedItfRole = MergedBindingResolverLoader
              .getMergedInterfaceRole(mergedItf);

          /*
           * dispatch compilation ONLY if it is either a CLIENT_INNER or a
           * SERVER_INNER interface, because the IDL compilation has been already
           * dispatched during the enclosing merged primitive processing for the
           * CLIENT_OUTER and SERVER_OUTER IDL interface types.
           */
          if (mergedItfRole == MergedInterfaceRole.CLIENT_INNER
              || mergedItfRole == MergedInterfaceRole.SERVER_INNER) {

            final IDLDefinition idl = ((IDLDefinitionContainer) mergedItf)
                .getIDLDefinition();

            /* if this IDL interface has to be compiled as static or not */
            final boolean hasToBeCompiledAsStatic = MergeUtil
                .hasToBeCompiledAsStatic(mergedItf);

            final IDLCompilationDesc compilationDesc = new IDLCompilationDesc(
                idl, hasToBeCompiledAsStatic);

            /* dispatch compilation to the various compilers */
            invokeVisitors(path, compilationDesc, taskComponents, visitedIDL, context);
          }
        }
      }
    }
  }
}
