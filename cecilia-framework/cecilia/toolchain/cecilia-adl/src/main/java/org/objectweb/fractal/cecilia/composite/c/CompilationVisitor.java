/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.composite.c;

import static org.objectweb.fractal.cecilia.adl.directives.DirectiveHelper.getLdFlags;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.cecilia.adl.compiler.CompilationTaskFactory;
import org.objectweb.fractal.cecilia.adl.file.FutureFileCollectionProvider;
import org.objectweb.fractal.cecilia.adl.file.FutureFileProvider;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterfaceForEach;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

public class CompilationVisitor extends AbstractTaskFactoryUser
    implements ComponentVisitor {
  /**
   * The name of the task composition schema used by this visitor.
   */
  public static final String COMPILATION_COMPOSITION = "org.objectweb.fractal.cecilia.composite.c.Compilation";

  /** The name of the {@link #compilationTaskFactoryItf} client interface. */
  public static final String    COMPILATION_TASK_FACTORY_ITF_NAME = "compilation-task-factory";

  /** The client interface used to create compilation tasks. */
  public CompilationTaskFactory compilationTaskFactoryItf;

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link ComponentContainer} nodes and creates a compilation tasks.
   */
  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {

    final Collection<Component> tasks = new ArrayList<Component>();

    // create the task that aggregates every compiled files
    tasks.add(createCompositeCompiledFileAggregatorTask(container));

    // create the compilation task for the component source file
    tasks.add(createComponentCompilationTask(container, context));

    // If the visited node is the top level node, add a link task.
    final boolean doLink = path.isEmpty();
    if (doLink) {
      final List<String> ldFlags = new ArrayList<String>();
      ldFlags.addAll(getLdFlags(container));
      tasks.add(compilationTaskFactoryItf.newLinkTask(container, null,
          ldFlags, context));
    }
    return taskFactoryItf.newCompositeTask(tasks, COMPILATION_COMPOSITION,
        null, container, doLink);
  }

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  protected Component createComponentCompilationTask(
      final ComponentContainer container, final Map<Object, Object> context)
      throws TaskException {
    return compilationTaskFactoryItf.newCompileTask(container, null, context);
  }

  protected Component createCompositeCompiledFileAggregatorTask(
      final ComponentContainer container) throws TaskException {
    return taskFactoryItf.newPrimitiveTask(
        new CompositeCompiledFileAggregator(), container, Arrays
            .asList(container.getComponents()));
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Aggregate a file provided by the given composite component and its sub
   * components.
   */
  @TaskParameters({"componentNode", "subComponents"})
  @ServerInterfaces(@ServerInterface(name = "component-compiled-files-provider", signature = FutureFileCollectionProvider.class, record = "role:outputFiles, id:%", parameters = "componentNode"))
  public static class CompositeCompiledFileAggregator
      implements
        FutureFileCollectionProvider {

    protected Collection<Future<File>>                                                             providedFiles;

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /**
     * Client interface used to retrieve compiled runtime implementations. This
     * interface is used only if the component is the top-level component.
     */
    @ClientInterface(name = "runtime-compiled-files", signature = FutureFileProvider.class, record = "role:runtimeCompiledFile, id:%", parameters = "componentNode")
    public final Map<String, FutureFileProvider>                                                   runtimeProviderItfs           = new HashMap<String, FutureFileProvider>();

    /**
     * Client interface used to retrieve compiled runtime implementations. This
     * interface is used only if the component is the top-level component.
     */
    @ClientInterface(name = "runtime-compiled-file-collections", signature = FutureFileCollectionProvider.class, record = "role:runtimeCompiledFileCollection, id:%", parameters = "componentNode")
    public final Map<String, FutureFileCollectionProvider>                                         runtimeCollectionProviderItfs = new HashMap<String, FutureFileCollectionProvider>();

    /**
     * Client collection interface used to retrieve compiled files for the given
     * component.
     */
    @ClientInterface(name = "component-files", signature = FutureFileProvider.class, record = "role:componentCompiledFile, id:%", parameters = "componentNode")
    public final Map<String, FutureFileProvider>                                                   componentFileItfs             = new HashMap<String, FutureFileProvider>();

    /**
     * Client collection interface used to retrieve compiled files for the given
     * component.
     */
    @ClientInterfaceForEach(prefix = "sub-component-files", signature = FutureFileCollectionProvider.class, iterable = "subComponents", record = "role:subComponentCompiledFile, id:%", parameters = "subComponents.element")
    public final Map<org.objectweb.fractal.adl.components.Component, FutureFileCollectionProvider> subComponentFilesItfs         = new HashMap<org.objectweb.fractal.adl.components.Component, FutureFileCollectionProvider>();

    // -------------------------------------------------------------------------
    // Implementation of the FutureFileCollectionProvider interface
    // -------------------------------------------------------------------------

    public synchronized Collection<Future<File>> getFiles(
        final ExecutorService executorService) {
      if (providedFiles == null) {
        providedFiles = new HashSet<Future<File>>();

        for (final FutureFileProvider fileProvider : runtimeProviderItfs
            .values()) {
          providedFiles.add(fileProvider.getFile(executorService));
        }

        for (final FutureFileCollectionProvider fileCollectionProvider : runtimeCollectionProviderItfs
            .values()) {
          providedFiles
              .addAll(fileCollectionProvider.getFiles(executorService));
        }

        for (final FutureFileProvider fileProvider : componentFileItfs.values()) {
          providedFiles.add(fileProvider.getFile(executorService));
        }

        for (final FutureFileCollectionProvider fileCollectionProvider : subComponentFilesItfs
            .values()) {
          providedFiles
              .addAll(fileCollectionProvider.getFiles(executorService));
        }
      }
      return providedFiles;
    }
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  @Override
  public String[] listFc() {
    final String[] superItfs = super.listFc();
    final String[] itfs = new String[superItfs.length + 1];
    System.arraycopy(superItfs, 0, itfs, 0, superItfs.length);
    itfs[superItfs.length] = COMPILATION_TASK_FACTORY_ITF_NAME;
    return itfs;
  }

  @Override
  public void bindFc(final String clientItfName, final Object serverItf)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.equals(COMPILATION_TASK_FACTORY_ITF_NAME))
      compilationTaskFactoryItf = (CompilationTaskFactory) serverItf;
    else
      super.bindFc(clientItfName, serverItf);
  }

  @Override
  public Object lookupFc(final String clientItfName)
      throws NoSuchInterfaceException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.equals(COMPILATION_TASK_FACTORY_ITF_NAME))
      return compilationTaskFactoryItf;
    else
      return super.lookupFc(clientItfName);
  }

  @Override
  public void unbindFc(final String clientItfName)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.equals(COMPILATION_TASK_FACTORY_ITF_NAME))
      compilationTaskFactoryItf = null;
    else
      super.unbindFc(clientItfName);
  }
}
