/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.primitive.optC.types;

import static org.objectweb.fractal.api.type.TypeFactory.OPTIONAL;

import java.util.List;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.interfaces.AbstractInterfaceVisitor;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the definition of the interfaces of the
 * component for the <code>ThinkMC</code> dialect.
 */
public class InterfaceDefinitionVisitor extends AbstractInterfaceVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractInterfaceVisitor
  // ---------------------------------------------------------------------------

  /**
   * Creates a task that writes the source code containing the definition of the
   * interfaces of the component.
   */
  @Override
  protected Component createTask(final ComponentContainer container,
      final List<TypeInterface> itfs) throws ADLException, TaskException {
    return taskFactoryItf.newPrimitiveTask(new InterfaceDefinitonTask(),
        container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds source code containing the definition of the interfaces for the
   * given component node. This task provides the produced source code. It
   * requires the source code that contains for the definition of both client
   * and server interfaces.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "source-code-provider", signature = SourceCodeProvider.class, record = "role:interfaceDefinition, id:%", parameters = "componentNode"))
  public static class InterfaceDefinitonTask extends AbstractDefinitionTask {

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /** Source code to be included for server interface definitions. */
    @ClientInterface(name = "server-itf-def", contingency = OPTIONAL, record = "role:serverInterfaceDefinition, id:%", parameters = "componentNode")
    public SourceCodeProvider serverItfDefinitionsItf;

    /** Source code to be included for server interface definitions. */
    @ClientInterface(name = "meth-macros-def", contingency = OPTIONAL, record = "role:methMacrosDefinition, id:%", parameters = "componentNode")
    public SourceCodeProvider methMacrosDefinitionsItf;

    /** Source code to be included for client interface definitions. */
    @ClientInterface(name = "client-itf-def", contingency = OPTIONAL, record = "role:clientInterfaceDefinition, id:%", parameters = "componentNode")
    public SourceCodeProvider clientItfDefinitionsItf;

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      // generate the definition of the structure representing the component
      // type
      final CodeWriter cw = new CodeWriter("Primitive Interfaces Definition Builder");
      final String name = typeNameProviderItf.getCTypeName();

      if (serverItfDefinitionsItf != null) {
        cw.append(serverItfDefinitionsItf.getSourceCode()).endl();
      }
      if (methMacrosDefinitionsItf != null) {
        cw.append(methMacrosDefinitionsItf.getSourceCode()).endl();
      }
      if (clientItfDefinitionsItf != null) {
        cw.append(clientItfDefinitionsItf.getSourceCode()).endl();
      }

      // defines the structure <name>_type
      cw.append("struct ").append(name).append("_type {").endl();
      if (serverItfDefinitionsItf != null)
        cw.append("struct ").append(name).append("_exporteds exported;").endl();
      if (clientItfDefinitionsItf != null)
        cw.append("struct ").append(name).append("_importeds imported;").endl();
      cw.append("};").endl();

      return cw.toString();
    }
  }
}
