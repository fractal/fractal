/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.implementations;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.implementations.ImplementationCodeLoader;
import org.objectweb.fractal.adl.implementations.ImplementationErrors;
import org.objectweb.fractal.cecilia.adl.file.SourceFile;

/**
 * {@link ImplementationCodeLoader} implementation for C source code. The
 * {@link #loadImplementation(String, String, Map) loadImplementation} method
 * returns a {@link SourceFile} object of the corresponding file.
 */
public class CCodeLoader implements ImplementationCodeLoader {

  /*
   * Req #24905 Implementation code loader should be able to load ".h" file: in
   * case of multi module the content.getClassName refers to a .h file
   * containing the private data
   */
  private static final String[]  FILE_EXTENSIONS = {".c", ".S", ".s"};

  private static final boolean[] ASSEMBLY_FILE   = {false, true, true};

  static {
    assert FILE_EXTENSIONS.length == ASSEMBLY_FILE.length;
  }

  public Object loadImplementation(final String signature,
      final String language, final Map<Object, Object> context)
      throws ADLException {
    ClassLoader loader = null;
    if (context != null) {
      loader = (ClassLoader) context.get("classloader");
    }
    if (loader == null) {
      loader = getClass().getClassLoader();
    }
    // Search the implementation file in the classpath

    for (int i = 0; i < FILE_EXTENSIONS.length; i++) {
      final String extension = FILE_EXTENSIONS[i];
      String s = signature;
      String path;
      if (s.endsWith(extension)) {
        // the given signature ends with the file extension. Consider that it is
        // a path.
        path = s;
        // convert s to correct signature
        s = s.substring(0, s.length() - extension.length()).replace('/', '.');
      } else {
        path = s.replace('.', '/') + extension;
      }

      // Search all the resources that match this path and return the first one
      // that is a file (i.e. not a jar entry).
      Enumeration<URL> results;
      try {
        results = loader.getResources(path);
      } catch (final IOException e) {
        continue;
      }
      while (results.hasMoreElements()) {
        final URL res = results.nextElement();
        File f;

        if (!"file".equals(res.getProtocol())) continue;
        try {
          f = new File(res.toURI());
        } catch(URISyntaxException e) {
          // This URL is not a valid URI.
          // Fall back to new File(String), hoping that it will work properly...
          f = new File(res.getPath());
        }
        if (f.exists()) return new SourceFile(s, f, ASSEMBLY_FILE[i]);
      }
    }
    throw new ADLException(ImplementationErrors.IMPLEMENTATION_NOT_FOUND,
        signature);
  }
}
