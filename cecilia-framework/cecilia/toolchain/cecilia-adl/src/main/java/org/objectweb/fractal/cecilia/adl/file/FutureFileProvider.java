/***
 * Cecilia ADL Compiler
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.file;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * This task interface can be implemented by task that produces a file
 * asynchronously.
 */
public interface FutureFileProvider {

  /**
   * Returns a {@link Future} that provides the produced file.
   * 
   * @return a {@link Future} that provides the produced {@link File}.
   */
  Future<File> getFile(ExecutorService executorService);
}
