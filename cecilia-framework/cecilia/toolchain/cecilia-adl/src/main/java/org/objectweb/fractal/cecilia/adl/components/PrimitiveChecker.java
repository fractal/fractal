/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.components;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;

/**
 * A component definition checker. Implementation of this checker interface is
 * intended to check a single component at a time, i.e. it does not recursively
 * checks the definitions of the sub components of a component.
 */
public interface PrimitiveChecker {

  /**
   * Checks a single component. This method is called before the sub components
   * of the given <code>container</code> are visited.
   * 
   * @param path the path of the component.
   * @param container the component.
   * @param context additional parameters.
   * @throws ADLException if something wrong happen.
   */
  void beforeSubComponentCheck(List<ComponentContainer> path,
      ComponentContainer container, Map<Object, Object> context)
      throws ADLException;

  /**
   * Checks a single component. This method is called after the sub components
   * of the given <code>container</code> have been visited.
   * 
   * @param path the path of the component.
   * @param container the component.
   * @param context additional parameters.
   * @throws ADLException if something wrong happen.
   */
  void afterSubComponentCheck(List<ComponentContainer> path,
      ComponentContainer container, Map<Object, Object> context)
      throws ADLException;
}
