
package org.objectweb.fractal.cecilia.primitive.thinkMC.types;

import static org.objectweb.fractal.cecilia.adl.controllers.ControllerDecorationUtil.getDataSuffix;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerDecorationUtil.getVTable;

import java.util.List;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractInstantiationTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.interfaces.AbstractServerInterfaceVisitor;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the instantiation of the server interfaces of
 * the component for <code>ThinkMC</code> dialect.
 */
public class ServerInterfaceInstantiationVisitor
    extends
      AbstractServerInterfaceVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractServerInterfaceVisitor
  // ---------------------------------------------------------------------------

  @Override
  protected Component createTask(final ComponentContainer container,
      final List<TypeInterface> serverInterfaces) throws ADLException,
      TaskException {
    return taskFactoryItf.newPrimitiveTask(new ServerInterfaceInstanceTask(
        serverInterfaces), container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds instantiation source code for server interfaces for the given
   * component node. This task provides the produced source code.
   */
  @TaskParameters({"componentNode"})
  @ServerInterfaces(@ServerInterface(name = "source-code-provider", signature = SourceCodeProvider.class, record = "role:serverInterfaceInstantiation, id:%", parameters = "componentNode"))
  public static class ServerInterfaceInstanceTask
      extends
        AbstractInstantiationTask {

    protected final List<TypeInterface> exportedItfNodes;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param exportedItfNodes the list of exported interface nodes belonging to
     *            the component.
     */
    public ServerInterfaceInstanceTask(
        final List<TypeInterface> exportedItfNodes) {
      // The order of the interfaces is important, this is why an array of
      // exportedIrfNodes is used for iterating of the exported interfaces
      // rather than iterating directly on the set of idlHeaderItfs client
      // interfaces.
      this.exportedItfNodes = exportedItfNodes;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractInstantiationTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter("Primitive Server Interfaces Instantiation");
      cw.append("{").endl();

      for (final TypeInterface itf : exportedItfNodes) {
        cw.append("{&");

        // If there is a special close for the implementation vft, we apply it
        final String vTable = getVTable(itf);
        if (vTable != null) {
          // If the interface name is given by a controller (ex:
          // _bindingcontrollermeth)
          cw.append(vTable);
        } else {
          // If this is a normal interface (VFT interfaces)
          cw.append(typeNameProviderItf.getCTypeName()).append("_").append(
              itf.getName().replace('-', '_')).append("meth");
        }
        cw.append(", &(").append(instanceNameProviderItf.getCInstanceName());
        final String data = getDataSuffix(itf);
        if (data != null) cw.append('_').append(data);

        cw.append(")").endl();
        /* Add the signature */
        cw.append("#if defined(TYPEDINTERFACE)").endl();
        cw.append(", \"").append(itf.getSignature()).append("\"").endl();
        cw.append("#endif").endl();

        cw.append("},").endl();
      }
      cw.append("}").endl();

      return cw.toString();
    }
  }
}
