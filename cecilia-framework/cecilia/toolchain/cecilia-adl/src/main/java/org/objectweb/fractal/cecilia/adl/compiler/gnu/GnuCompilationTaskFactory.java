/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.compiler.gnu;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.compiler.AbstractCompilationTaskFactory;
import org.objectweb.fractal.task.core.TaskException;

public class GnuCompilationTaskFactory extends AbstractCompilationTaskFactory {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractCompilationTaskFactory
  // ---------------------------------------------------------------------------

  @Override
  protected Component createCompileTask(final Object id,
      final String compileCommand, final List<String> flags, final File outputDir,
      final Map<Object, Object> context) throws TaskException {
    return taskFactoryItf.newPrimitiveTask(new GnuDepCompilerTask(
        compileCommand, outputDir, flags), id);
  }

  @Override
  protected Component createLinkTask(final Object id, final String linkCommand,
      final String linkedFileName, List<String> flags, final File outputDir,
      final int nbJobs, final Map<Object, Object> context) throws TaskException {
    final String ldScript = tryGetContextStringArg("linker-script", context);
    if (ldScript != null) {
      flags.add("-T");
      flags.add(ldScript);
    }
    return taskFactoryItf.newPrimitiveTask(new GnuLinkerTask(linkCommand,
        linkedFileName, outputDir, flags, nbJobs), id);
  }

  @Override
  protected Component createArchiveTask(final Object id,
      final String archiveCommand, final String archiveFileName,
      final List<String> flags, final File outputDir,
      final Map<Object, Object> context) throws TaskException {
    return taskFactoryItf.newPrimitiveTask(new GnuArchiverTask(archiveCommand,
        archiveFileName, outputDir, flags), id);
  }
}
