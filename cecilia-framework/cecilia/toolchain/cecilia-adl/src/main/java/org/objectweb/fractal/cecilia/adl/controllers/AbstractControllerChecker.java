/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Ali Erdem Ozcan
 * Contributors: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.controllers;

import static org.objectweb.fractal.adl.types.TypeInterfaceUtil.isClient;
import static org.objectweb.fractal.adl.types.TypeInterfaceUtil.isMandatory;
import static org.objectweb.fractal.adl.types.TypeInterfaceUtil.isServer;
import static org.objectweb.fractal.adl.types.TypeInterfaceUtil.isSingleton;
import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationDecorationUtil.setCode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.NodeUtil;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.implementations.ImplementationCodeLoader;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.cecilia.adl.CeciliaADLConstants;
import org.objectweb.fractal.cecilia.adl.components.PrimitiveChecker;
import org.xml.sax.SAXException;

/**
 * Abstract implementation of a {@link PrimitiveChecker} component to check
 * controller part.
 */
public abstract class AbstractControllerChecker
    implements
      PrimitiveChecker,
      BindingController {

  private static final String     INTERFACE_AST_NODE_NAME        = "interface";

  // ---------------------------------------------------------------------------
  // Client interfaces
  // ---------------------------------------------------------------------------

  /** The name of the {@link #nodeFactoryItf} client interface. */
  public static final String      NODE_FACTORY_ITF               = "node-factory";

  /** The name of the {@link #implementationCodeLoaderItf} client interface. */
  public static final String      IMPLEMENTATION_CODE_LOADER_ITF = "implementation-code-loader";

  /** The {@link XMLNodeFactory} client interface. */
  public XMLNodeFactory           nodeFactoryItf;

  /** The {@link ImplementationCodeLoader} client interface. */
  public ImplementationCodeLoader implementationCodeLoaderItf;

  // ---------------------------------------------------------------------------
  // Implementation of the PrimitiveChecker interface
  // ---------------------------------------------------------------------------

  public void beforeSubComponentCheck(final List<ComponentContainer> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    // do nothing.
  }

  public void afterSubComponentCheck(final List<ComponentContainer> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final InterfaceContainer itfContainer = NodeUtil.castNodeError(container,
        InterfaceContainer.class);

    // check that the given component is valid
    checkComponent(container);

    // retrieve the list of control interface.
    final List<Interface> controllerInterfaces = getControllerInterfaces(
        container, context);
    final Map<String, Interface> controllerInterfacesMap =
        new HashMap<String, Interface>(controllerInterfaces.size());

    for (final Interface i : controllerInterfaces) {
      controllerInterfacesMap.put(i.getName(), i);
    }

    // remove all interfaces to add controllers at first
    final Interface[] itfs = itfContainer.getInterfaces();
    for (final Interface itf : itfs) {
      itfContainer.removeInterface(itf);
    }

    // add controller interfaces
    for (final Interface controllerItf : controllerInterfaces) {
      itfContainer.addInterface(controllerItf);
    }
    // re-add other interfaces
    for (final Interface itf : itfs) {
      /* if this it not a controller interface, add it */
      if (!controllerInterfacesMap.containsKey(itf.getName())) {
        itfContainer.addInterface(itf);
      }
    }
  }

  // ---------------------------------------------------------------------------
  // abstract methods
  // ---------------------------------------------------------------------------

  protected abstract void checkComponent(final ComponentContainer container)
      throws ADLException;

  /**
   * @param container
   * @param context
   * @return a sorted (by key) Map of the controller interfaces to be added to
   *         this component.
   * @throws ADLException
   */
  protected abstract List<Interface> getControllerInterfaces(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException;

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  protected TypeInterface getInterface(final String name,
      final InterfaceContainer container) {
    for (final Interface itf : container.getInterfaces()) {
      if (itf.getName().equals(name)) return (TypeInterface) itf;
    }
    return null;
  }

  /**
   * @param signature
   * @return the <em>server</em> <tt>TypeInterface</tt> with the given
   *         <tt>signature</tt>, or <tt>null</tt> if none is found.
   */
  protected List<TypeInterface> getServerInterfacesBySignature(
      final String signature, final InterfaceContainer container) {

    final List<TypeInterface> result = new ArrayList<TypeInterface>();

    for (final Interface itf : container.getInterfaces()) {
      final TypeInterface typedInterface = (TypeInterface) itf;
      if (typedInterface.getSignature().equals(signature)
          && typedInterface.getRole().equals("server")) {
        result.add(typedInterface);
      }
    }
    return result;

  }

  protected void checkServerInterface(final TypeInterface itf)
      throws ADLException {
    if (!isServer(itf)) {
      throw new ADLException(ControllerErrors.INVALID_CONTROLLER_ITF_ROLE, itf,
          itf.getName(), TypeInterface.SERVER_ROLE);
    }
    if (!isMandatory(itf)) {
      throw new ADLException(
          ControllerErrors.INVALID_CONTROLLER_ITF_CONTINGENCY, itf, itf
              .getName(), TypeInterface.MANDATORY_CONTINGENCY);
    }
    if (!isSingleton(itf)) {
      throw new ADLException(
          ControllerErrors.INVALID_CONTROLLER_ITF_CARDINALITY, itf, itf
              .getName(), TypeInterface.SINGLETON_CARDINALITY);
    }
  }

  protected void checkClientInterface(final TypeInterface itf)
      throws ADLException {
    if (!isClient(itf)) {
      throw new ADLException(ControllerErrors.INVALID_CONTROLLER_ITF_ROLE, itf,
          itf.getName(), TypeInterface.CLIENT_ROLE);
    }
    if (!isMandatory(itf)) {
      throw new ADLException(
          ControllerErrors.INVALID_CONTROLLER_ITF_CONTINGENCY, itf, itf
              .getName(), TypeInterface.MANDATORY_CONTINGENCY);
    }
    if (!isSingleton(itf)) {
      throw new ADLException(
          ControllerErrors.INVALID_CONTROLLER_ITF_CARDINALITY, itf, itf
              .getName(), TypeInterface.SINGLETON_CARDINALITY);
    }
  }

  protected TypeInterface newServerInterfaceNode(final String name,
      final String signature) {
    TypeInterface itf;
    try {
      itf = (TypeInterface) nodeFactoryItf.newXMLNode(
          CeciliaADLConstants.CECILIA_ADL_DTD,
          AbstractControllerChecker.INTERFACE_AST_NODE_NAME);
    } catch (final SAXException e) {
      throw new CompilerError(GenericErrors.INTERNAL_ERROR, e,
          "Unable to create interface node");
    }
    itf.setName(name);
    itf.setSignature(signature);
    itf.setCardinality(TypeInterface.SINGLETON_CARDINALITY);
    itf.setRole(TypeInterface.SERVER_ROLE);
    itf.setContingency(TypeInterface.MANDATORY_CONTINGENCY);
    return itf;
  }

  protected TypeInterface newClientInterfaceNode(final String name,
      final String signature) {
    TypeInterface itf;
    try {
      itf = (TypeInterface) nodeFactoryItf.newXMLNode(
          CeciliaADLConstants.CECILIA_ADL_DTD,
          AbstractControllerChecker.INTERFACE_AST_NODE_NAME);
    } catch (final SAXException e) {
      throw new CompilerError(GenericErrors.INTERNAL_ERROR, e,
          "Unable to create interface node");
    }
    itf.setName(name);
    itf.setSignature(signature);
    itf.setCardinality(TypeInterface.SINGLETON_CARDINALITY);
    itf.setRole(TypeInterface.CLIENT_ROLE);
    itf.setContingency(TypeInterface.MANDATORY_CONTINGENCY);
    return itf;
  }

  /**
   * Use {@link #setItfCode(Interface, Map, String...)} instead.
   * 
   * @param itf
   * @param signature
   * @param context
   * @throws ADLException
   */
  @Deprecated
  protected void setItfCode(final Interface itf, final String signature,
      final Map<Object, Object> context) throws ADLException {
    setItfCode(itf, context, signature);
  }

  protected void setItfCode(final Interface itf,
      final Map<Object, Object> context, final String... signatures)
      throws ADLException {
    final List<Object> codes = new ArrayList<Object>(signatures.length);
    for (final String signature : signatures) {
      codes.add(implementationCodeLoaderItf.loadImplementation(signature, null,
          context));
    }
    setCode(itf, codes);
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(NODE_FACTORY_ITF)) {
      this.nodeFactoryItf = (XMLNodeFactory) o;
    } else if (s.equals(IMPLEMENTATION_CODE_LOADER_ITF)) {
      this.implementationCodeLoaderItf = (ImplementationCodeLoader) o;
    } else {
      throw new NoSuchInterfaceException("There is no interface named '" + s
          + "'");
    }
  }

  public String[] listFc() {
    return new String[]{NODE_FACTORY_ITF, IMPLEMENTATION_CODE_LOADER_ITF};
  }

  public Object lookupFc(final String s) throws NoSuchInterfaceException {
    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(NODE_FACTORY_ITF)) {
      return this.nodeFactoryItf;
    } else if (s.equals(IMPLEMENTATION_CODE_LOADER_ITF)) {
      return this.implementationCodeLoaderItf;
    } else {
      throw new NoSuchInterfaceException("There is no interface named '" + s
          + "'");
    }
  }

  public void unbindFc(final String s) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(NODE_FACTORY_ITF)) {
      this.nodeFactoryItf = null;
    } else if (s.equals(IMPLEMENTATION_CODE_LOADER_ITF)) {
      this.implementationCodeLoaderItf = null;
    } else {
      throw new NoSuchInterfaceException("There is no interface named '" + s
          + "'");
    }
  }

}
