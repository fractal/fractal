/***
 * Cecilia ADL Compiler
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.implementations;

import static org.objectweb.fractal.cecilia.adl.SourceCodeHelper.appendSortedSourceCodes;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.DefinitionVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.compiler.CompilationTaskFactory;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.file.SourceFile;
import org.objectweb.fractal.cecilia.adl.file.SourceFileProvider;
import org.objectweb.fractal.cecilia.adl.file.SourceFileWriter;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.Executable;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;

public class GlobalInitVisitor extends AbstractTaskFactoryUser
    implements
      DefinitionVisitor {

  public static final String    COMPOSTION                        = "org.objectweb.fractal.cecilia.adl.implementations.GlobalInitCompilation";

  // ---------------------------------------------------------------------------
  // Client interface
  // ---------------------------------------------------------------------------

  /** The name of the {@link #compilationTaskFactoryItf} client interface. */
  public static final String    COMPILATION_TASK_FACTORY_ITF_NAME = "compilation-task-factory";

  /** The client interface used to create compilation tasks. */
  public CompilationTaskFactory compilationTaskFactoryItf;

  // ---------------------------------------------------------------------------
  // Implementation of the DefinitionVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link ComponentContainer} nodes and creates a task that writes the
   * source code containing the component data structure definitions.
   */
  public Component visit(final List<Node> path, final Definition container,
      final Map<Object, Object> context) throws ADLException, TaskException {
    final Collection<Component> tasks = new ArrayList<Component>();
    tasks.add(createGlobalInitialisationDeclarationTask(container, context));
    tasks.add(createGlobalInitialisationCompilationTask(container, context));
    return taskFactoryItf.newCompositeTask(tasks, COMPOSTION, null, container);
  }

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  protected Component createGlobalInitialisationDeclarationTask(
      final Definition container, final Map<Object, Object> context)
      throws TaskException {
    return taskFactoryItf
        .newPrimitiveTask(new GlobalInitialisationDeclatationTask(
            (File) context.get("adlBuildDirectory"), container.getName()));
  }

  protected Component createGlobalInitialisationCompilationTask(
      final Definition container, final Map<Object, Object> context)
      throws TaskException {
    return compilationTaskFactoryItf
        .newCompileTask("globalInit", null, context);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  @ServerInterfaces(@ServerInterface(name = "global-init-code", signature = SourceFileProvider.class, record = "role:globalInitFile"))
  public static class GlobalInitialisationDeclatationTask
      implements
        Executable,
        SourceFileProvider {

    /** The suffix of the generated file. */
    public static final String                   FILE_NAME_SUFFIX     = ".init.c";

    protected final File                         adlBuildDirectory;

    protected final String                       definitionName;

    // Produced source file
    protected SourceFile                         sourceFile;

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /**
     * The client interface bound to task providing declarations necessary for
     * the constructor/destructor code.
     */
    @ClientInterface(name = "constructor-declaration-provider", signature = SourceCodeProvider.class, record = "role:globalInit, type:declaration")
    public final Map<String, SourceCodeProvider> declatationsItfs     = new HashMap<String, SourceCodeProvider>();

    /** The client interface bound to task providing constructor code. */
    @ClientInterface(name = "constructor-call-provider", signature = SourceCodeProvider.class, record = "role:globalInit, type:call, kind:constructor")
    public final Map<String, SourceCodeProvider> constructorCallsItfs = new HashMap<String, SourceCodeProvider>();

    /** The client interface bound to task providing destructor code. */
    @ClientInterface(name = "destructor-call-provider", signature = SourceCodeProvider.class, record = "role:globalInit, type:call, kind:destructor")
    public final Map<String, SourceCodeProvider> destructorCallsItfs  = new HashMap<String, SourceCodeProvider>();

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param adlBuildDirectory The directory into which the generated file will
     *          be placed.
     * @param definitionName the name of the global definition.
     */
    public GlobalInitialisationDeclatationTask(final File adlBuildDirectory,
        final String definitionName) {
      this.adlBuildDirectory = adlBuildDirectory;
      this.definitionName = definitionName;
    }

    // -------------------------------------------------------------------------
    // Utility methods
    // -------------------------------------------------------------------------

    protected void prepareSourceCode(final CodeWriter cw) throws Exception {

      cw.appendln("// --------------------------------------------------------"
          + "---------------------");
      cw.appendln("// declaration of global constructor/destructor functions");
      cw.appendln("// --------------------------------------------------------"
          + "---------------------");
      cw.endl();

      appendSortedSourceCodes(cw, declatationsItfs.values());

      cw.appendln("void __cecilia_global_constructor__(void) {");
      appendSortedSourceCodes(cw, constructorCallsItfs.values());
      cw.appendln("}");

      cw.appendln("void __cecilia_global_destructor__(void) {");
      appendSortedSourceCodes(cw, destructorCallsItfs.values());
      cw.appendln("}");
    }

    protected String getFileName() {
      return definitionName.replace('.', '_') + FILE_NAME_SUFFIX;
    }

    protected String getSignature() {
      return definitionName + "_init";
    }

    // -------------------------------------------------------------------------
    // Implementation of the Executable interface
    // -------------------------------------------------------------------------

    public void execute() throws Exception {
      final String fileName = getFileName();
      final File outputFile = new File(adlBuildDirectory, fileName);

      final CodeWriter cw = new CodeWriter();
      cw.appendln("// THIS FILE HAS BEEN GENERATED BY THE CECILIA ADL "
          + "COMPILER.");
      cw.appendln("// DO NOT EDIT").endl();
      prepareSourceCode(cw);

      try {
        SourceFileWriter.writeToFile(outputFile, cw.toString());
      } catch (final IOException e) {
        throw new ADLException(GenericErrors.INTERNAL_ERROR, e,
            "An error occurs while writing to file '"
                + outputFile.getAbsolutePath() + "'");
      }

      sourceFile = new SourceFile(getSignature(), outputFile);
    }

    // -------------------------------------------------------------------------
    // Implementation of the SourceFileProvider interface
    // -------------------------------------------------------------------------

    public SourceFile getSourceFile() {
      return sourceFile;
    }
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  @Override
  public String[] listFc() {
    final String[] superItfs = super.listFc();
    final String[] itfs = new String[superItfs.length + 1];
    System.arraycopy(superItfs, 0, itfs, 0, superItfs.length);
    itfs[superItfs.length] = COMPILATION_TASK_FACTORY_ITF_NAME;
    return itfs;
  }

  @Override
  public void bindFc(final String clientItfName, final Object serverItf)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.equals(COMPILATION_TASK_FACTORY_ITF_NAME))
      compilationTaskFactoryItf = (CompilationTaskFactory) serverItf;
    else
      super.bindFc(clientItfName, serverItf);
  }

  @Override
  public Object lookupFc(final String clientItfName)
      throws NoSuchInterfaceException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.equals(COMPILATION_TASK_FACTORY_ITF_NAME))
      return compilationTaskFactoryItf;
    else
      return super.lookupFc(clientItfName);
  }

  @Override
  public void unbindFc(final String clientItfName)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (clientItfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (clientItfName.equals(COMPILATION_TASK_FACTORY_ITF_NAME))
      compilationTaskFactoryItf = null;
    else
      super.unbindFc(clientItfName);
  }
}