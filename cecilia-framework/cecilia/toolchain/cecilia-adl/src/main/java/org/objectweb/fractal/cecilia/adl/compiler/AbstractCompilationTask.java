/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.compiler;

import java.io.File;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.util.FractalADLLogManager;
import org.objectweb.fractal.cecilia.adl.file.FutureFileProvider;
import org.objectweb.fractal.cecilia.adl.file.SourceFile;
import org.objectweb.fractal.cecilia.adl.file.SourceFileProvider;
import org.objectweb.fractal.cecilia.adl.file.SourceFileWriter;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Abstract implementation of task returned by
 * {@link CompilationTaskFactory#newCompileTask(Object, String, java.util.Map)}.
 */
@TaskParameters("id")
@ServerInterfaces(@ServerInterface(name = "file-provider", signature = FutureFileProvider.class, record = "role:compiledFile, id:%", parameters = "id"))
public abstract class AbstractCompilationTask implements FutureFileProvider {

  /** The default file extension of compiled files. */
  protected static final String DEFAULT_EXTENSION = ".o";

  /** The compilation flags. */
  protected List<String>              flags;
  /** The output directory. */
  protected final File          outputDir;

  // Produced file
  protected Future<File>        compiledFile;

  // The dep logger
  protected static Logger       depLogger         = FractalADLLogManager
                                                      .getLogger("dep");
  // The io logger
  protected static Logger       ioLogger          = FractalADLLogManager
                                                      .getLogger("io");

  // ---------------------------------------------------------------------------
  // Task client interfaces
  // ---------------------------------------------------------------------------

  /** The client interface used to retrieve the source file to compile. */
  @ClientInterface(name = "source-file-provider", record = "role:sourceFile, id:%", parameters = "id")
  public SourceFileProvider     sourceFileProviderItf;

  // ---------------------------------------------------------------------------
  // Task constructor
  // ---------------------------------------------------------------------------

  /**
   * @param outputDir the directory into which the compiled file will be placed.
   * @param flags compilation flags.
   */
  public AbstractCompilationTask(final File outputDir, final List<String> flags) {
    this.outputDir = outputDir;
    this.flags = flags;
  }

  // ---------------------------------------------------------------------------
  // Abstract methods
  // ---------------------------------------------------------------------------

  /**
   * Compilation method.
   * 
   * @param sourceFile the source file to compile.
   * @param outputFile the compiled file that is provided by this task.
   * @throws ADLException if an error occurs.
   * @throws InterruptedException
   */
  protected abstract void compile(SourceFile sourceFile, File outputFile)
      throws ADLException, InterruptedException;

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  protected File getOutputFile(final SourceFile sourceFile) {
    final String fileName = sourceFile.getSignature().replace('.',
        File.separatorChar)
        + getExtension();
    return new File(outputDir, fileName);
  }

  protected String getExtension() {
    return DEFAULT_EXTENSION;
  }

  // ---------------------------------------------------------------------------
  // Implementation of the FutureFileProvider interface
  // ---------------------------------------------------------------------------

  public synchronized Future<File> getFile(final ExecutorService executorService) {
    if (compiledFile == null) {
      compiledFile = executorService.submit(new Callable<File>() {

        public File call() throws Exception {
          final SourceFile sourceFile = sourceFileProviderItf.getSourceFile();
          final File cFile = getOutputFile(sourceFile);
          SourceFileWriter.createOutputDir(cFile.getParentFile());
          compile(sourceFile, cFile);
          return cFile;
        }
      });
    }
    return compiledFile;
  }
}
