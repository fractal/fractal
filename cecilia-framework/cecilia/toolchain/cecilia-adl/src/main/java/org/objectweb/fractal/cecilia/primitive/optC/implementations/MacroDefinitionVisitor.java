/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 * Contributors: Alessio Pace (merged macro definition)
 *               Lionel Debroux (adaptation to Cecilia 2.x)
 */

package org.objectweb.fractal.cecilia.primitive.optC.implementations;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;
import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationDecorationUtil.getCode;
import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationHelper.hasConstructor;
import static org.objectweb.fractal.cecilia.adl.implementations.ImplementationHelper.hasDestructor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.components.merge.MergeUtil;
import org.objectweb.fractal.cecilia.adl.directives.Include;
import org.objectweb.fractal.cecilia.adl.directives.IncludeContainer;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.file.SourceFile;
import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;
import org.objectweb.fractal.cecilia.adl.types.TypeDecorationUtil;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.TaskFactory;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that provides the inclusion code of the implementation file
 * for <code>optC</code> dialect.
 */
public class MacroDefinitionVisitor extends AbstractTaskFactoryUser
    implements
      ComponentVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link ComponentContainer} nodes and creates a task that includes
   * the implementation file, and that defines some macros that are used in the
   * implementation file.
   */
  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    final Component implTask;
    final Implementation impl = castNodeError(container,
        ImplementationContainer.class).getImplementation();
    if (impl == null) {
      throw new CompilerError(GenericErrors.INTERNAL_ERROR,
          "This visitor is only applicable for primitive component.");
    }

    boolean isSingleton = TypeDecorationUtil.isSingletonComponentType(
        container);
    implTask = createImplementationTask(container, impl, isSingleton);
    

    if (impl instanceof IncludeContainer
        && ((IncludeContainer) impl).getIncludes().length != 0) {
      final Include[] includes = ((IncludeContainer) impl).getIncludes();

      final Collection<Component> tasks = new ArrayList<Component>(
          includes.length + 1);
      if (implTask != null) {
        tasks.add(implTask);
      }

      for (final Include include : includes) {
        // if the module is an assembly, file, it should not be included in
        // implementation.
        if (!((SourceFile) getCode((Node) include)).isAssemblyFile()) {
          final Component implTask2;
          implTask2 = createModuleImplementationTask(container, impl, include,
              isSingleton);
          if (implTask2 != null) {
            tasks.add(implTask2);
          }
        }
      }
      return taskFactoryItf.newCompositeTask(tasks, TaskFactory.EXPORT_ALL,
          null);
    } else {
      return implTask;
    }
  }

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  protected Component createImplementationTask(
      final ComponentContainer container, final Implementation impl,
      final boolean isSingleton) throws TaskException {
    if (!MergeUtil.isMergedPrimitive(container)) {
      return taskFactoryItf.newPrimitiveTask(new ImplementationTask(
          hasConstructor(container), hasDestructor(container), container,
          impl, isSingleton), container);
    }
    return null;
  }

  protected Component createModuleImplementationTask(
      final ComponentContainer container, final Implementation impl,
      final Include include, final boolean isSingleton) throws TaskException {
    if (!MergeUtil.isMergedPrimitive(container)) {
      return taskFactoryItf.newPrimitiveTask(new ModuleImplementationTask(
          hasConstructor(container), hasDestructor(container), container,
          include, isSingleton), container, include);
    }
    return null;
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  protected abstract static class AbstractImplementationTask
      extends AbstractDefinitionTask {

    protected final boolean hasConstructor;
    protected final boolean hasDestructor;
    protected ComponentContainer container;
    protected Implementation impl;
    protected Include include;
    protected boolean isSingleton;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param hasConstructor <code>true</code> if the component has a
     *          constructor.
     * @param hasDestructor <code>true</code> if the component has a destructor.
     */
    public AbstractImplementationTask(final boolean hasConstructor,
        final boolean hasDestructor) {
      this.hasConstructor = hasConstructor;
      this.hasDestructor = hasDestructor;
    }

    // -------------------------------------------------------------------------
    // Abstract methods
    // -------------------------------------------------------------------------

    protected abstract void appendImplementationCode(CodeWriter cw);

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    protected String commonProcessSourceCode(final CodeWriter cw) throws Exception {
      final String componentCName = typeNameProviderItf.getCTypeName();

      beforePrivateDataMacroDefinition(cw, componentCName);
        
      generatePrivateDataMacroDefinition(cw, componentCName);

      afterPrivateDataMacroDefinition(cw, componentCName);
        
      // Print the implementation code.
      appendImplementationCode(cw);
      
      return cw.toString();
    }
    
    protected void beforePrivateDataMacroDefinition(final CodeWriter cw,
        String componentCName) throws Exception {
      // Do nothing.
    }

    protected void generatePrivateDataMacroDefinition(final CodeWriter cw,
        String componentCName) throws Exception {
      /*
       * PRIVATE_DATA macro for component instance data. NOTE: it also includes
       * "cecilia_opt.h"
       */
      cw.appendln("#define PRIVATE_DATA \\");
      cw.appendln("  COMPONENT_PRIVATE_DATA_TYPE; \\");
      cw.appendln("  __CECILIA__DEFINE_COMPONENT_TYPE; \\");
      cw.appendln("  DECLARE_FIRST_INSTANCE;");
      cw.endl();
      /*
       * #define to avoid redundant redeclaration warnings when
       * -Wredundant-decls (for GCC) is turned on.
       */
      cw.append("#define ").append(componentCName).append("_DECLARED").endl();
    }

    protected void afterPrivateDataMacroDefinition(final CodeWriter cw,
        String componentCName) throws Exception {
      /*
       * Define the constructor and destructor, if any.
       */
      if (hasConstructor) {
        cw.append("#define CONSTRUCTOR \\").endl();
        cw.append(componentCName).append("_constructor").endl();
      }
      if (hasDestructor) {
        cw.append("#define DESTRUCTOR \\").endl();
        cw.append(componentCName).append("_destructor").endl();
      }

      /*
       * ATTRIBUTES macro to access attributes.
       */
      generateATTR(cw, componentCName);

      /*
       * THIS macro to access component instance data.
       */
      generateTHIS(cw, componentCName);
      /*
       * DATA macro as an alias for THIS.
       */
      cw.append("#define DATA THIS").endl();

      String thisIfAny = "";
      if (!System.getProperty(InterfaceDecorationUtil.CECILIA_ENABLE_THIS_OPTIMIZATION,
          "false").equals("true")) {
        /* Define the GETSELF macro according to above defined structures */
        cw.appendln("#define GETSELF \\");
        cw.append("  struct ").append(componentCName).append(
            "_t *self = (struct " + componentCName).appendln("_t *) _this ");

        // Define the GET_MY_OWNER macro
        cw.appendln("#define GET_MY_OWNER \\");
        cw.appendln("  ((Rfractal_api_Component *) _this)");

        thisIfAny = "void *_this";
      }

      if (hasConstructor) {
        cw.appendln("// declare constructor.");
        cw.appendln("void CONSTRUCTOR(" + thisIfAny + ");");
      }
      if (hasDestructor) {
        cw.appendln("// declare destructor.");
        cw.appendln("void DESTRUCTOR(" + thisIfAny + ");");
      }
    }

    protected void generateTHIS(final CodeWriter cw,
        String componentCName) throws Exception {
      if (!isSingleton) {
        cw.appendln("#define THIS \\");
        cw.append("  ((struct ").appendln(componentCName + "_t *)_this)->data");
      }
      else {
        cw.appendln("// The following THIS definition is optimized for a singleton component");
        cw.appendln("#define THIS \\");
        cw.appendln(componentCName + ".data");
      }
    }

    protected void generateATTR(final CodeWriter cw,
        String componentCName) throws Exception {
      if (!isSingleton) {
        cw.appendln("#define ATTR(attrName) \\");
        cw.append("  ( ( (struct ").append(componentCName)
            .appendln("_t*) _this)->attributes.attrName)");
      }
      else {
        cw.appendln("// The following ATTR definition is optimized for a singleton component");
        cw.appendln("#define ATTR(attrName) \\");
        cw.append(componentCName).appendln(".attributes.attrName");
      }
    }
  }

  /**
   * Builds definition of implementation source code for the given component
   * node. This task defines the macros that are used in the implementation file
   * and then includes the implementation file.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "implementation-provider", signature = SourceCodeProvider.class, record = "role:implementation, id:%", parameters = "componentNode"))
  public static class ImplementationTask extends AbstractImplementationTask {

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /** Client interface used to retrieve the implementation code. */
    @ClientInterface(name = "implementation-source-code", record = "role:implementationSourceCode, id:%", parameters = "componentNode")
    public SourceCodeProvider implementationSourceCodeProviderItf;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param hasConstructor <code>true</code> if the component has a
     *          constructor.
     * @param hasDestructor <code>true</code> if the component has a destructor.
     * @param container the component container
     * @param impl the component implementation
     * @param isSingleton <code>true</code> if the component is a singleton.
     */
    public ImplementationTask (final boolean hasConstructor,
        final boolean hasDestructor, final ComponentContainer container,
        final Implementation impl, final boolean isSingleton) {
      super(hasConstructor, hasDestructor);
      this.container = container;
      this.impl = impl;
      this.isSingleton = isSingleton;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractImplementationTask
    // -------------------------------------------------------------------------
    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter("Primitive Implementation Definition (Macro) Builder");
      if (isSingleton) {
        cw.appendln("#define IS_SINGLETON");
      }

      /* the cecilia header */
      cw.appendln("#include \"cecilia_opt2.h\"").endl();

      commonProcessSourceCode(cw);

      cw.appendln("#ifndef __CECILIA_OPT2_H__");
      cw.appendln("#error You forgot to add an empty `typedef struct { ... } PRIVATE_DATA`. This is required.");
      cw.appendln("#endif");

      if (isSingleton) {
        cw.appendln("#undef IS_SINGLETON");
      }

      return cw.toString();
    }

    @Override
    protected void appendImplementationCode(final CodeWriter cw) {
      cw.appendln(implementationSourceCodeProviderItf.getSourceCode());
    }
  }

  /**
   * Builds definition of implementation source code for the given module of the
   * given component node. This task defines the macros that are used in the
   * implementation file and then includes the implementation file.
   */
  @TaskParameters({"componentNode", "moduleNode"})
  @ServerInterfaces(@ServerInterface(name = "implementation-provider", signature = SourceCodeProvider.class, record = "role:implementation, id:%, module:%", parameters = {
      "componentNode", "moduleNode"}))
  public static class ModuleImplementationTask extends AbstractImplementationTask {

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /** Client interface used to retrieve the module of implementation code. */
    @ClientInterface(name = "implementation-source-code", record = "role:implementationSourceCode, id:%, module:%", parameters = {
        "componentNode", "moduleNode"})
    public SourceCodeProvider implementationModuleSourceCodeProviderItf;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param hasConstructor <code>true</code> if the component has a
     *          constructor.
     * @param hasDestructor <code>true</code> if the component has a destructor.
     * @param container the component container
     * @param include the include
     * @param isSingleton <code>true</code> if the component is a singleton.
     */
    public ModuleImplementationTask (final boolean hasConstructor,
        final boolean hasDestructor, final ComponentContainer container,
        final Include include, final boolean isSingleton) {
      super(hasConstructor, hasDestructor);
      this.container = container;
      this.include = include;
      this.isSingleton = isSingleton;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractImplementationTask
    // -------------------------------------------------------------------------
    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter("Primitive Implementation Definition (Macro) (Module) Builder");
      if (isSingleton) {
        cw.appendln("#define IS_SINGLETON");
      }

      appendImplementationCode(cw);

      if (isSingleton) {
        cw.appendln("#undef IS_SINGLETON");
      }

      return cw.toString();
    }
    
    @Override
    protected void appendImplementationCode(final CodeWriter cw) {
      cw.appendln(implementationModuleSourceCodeProviderItf.getSourceCode());
    }
  }
}
