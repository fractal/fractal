/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.controllers;

import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;

/**
 * Utility class to manipulate decorations of controller {@link Interface} node.
 */
public final class ControllerDecorationUtil {
  private ControllerDecorationUtil() {
  }

  /**
   * A decoration set on {@link Interface} node to indicate the symbol name of
   * the V-Table that provides the implementation of this interface.
   * 
   * @see #getVTable(Interface)
   * @see #setVTable(Interface, String)
   */
  public static final String V_TABLE_DECORATION = "vtbl";

  /**
   * Sets the value of the {@link #V_TABLE_DECORATION} decoration.
   * 
   * @param itf an interface node.
   * @param value the value of the decoration.
   * @see #V_TABLE_DECORATION
   */
  public static void setVTable(final Interface itf, final String value) {
    ((Node) itf).astSetDecoration(V_TABLE_DECORATION, value);
  }

  /**
   * Returns the value of the {@link #V_TABLE_DECORATION} decoration of the
   * given interface node.
   * 
   * @param itf an interface node.
   * @return The value of the {@link #V_TABLE_DECORATION} decoration or
   *         <code>null</code> if the given node has no such decoration.
   * @see #V_TABLE_DECORATION
   */
  public static String getVTable(final Interface itf) {
    return (String) ((Node) itf).astGetDecoration(V_TABLE_DECORATION);
  }

  /**
   * A decoration set on {@link Interface} node to indicate the suffix of the
   * symbol name of the <code>self-data</code> of the interface.
   * 
   * @see #getDataSuffix(Interface)
   * @see #setDataSuffix(Interface, String)
   */
  public static final String DATA_SUFFIX_DECORATION = "data";

  /**
   * Sets the value of the {@link #DATA_SUFFIX_DECORATION} decoration.
   * 
   * @param itf an interface node.
   * @param value the value of the decoration.
   * @see #DATA_SUFFIX_DECORATION
   */
  public static void setDataSuffix(final Interface itf, final String value) {
    ((Node) itf).astSetDecoration(DATA_SUFFIX_DECORATION, value);
  }

  /**
   * Returns the value of the {@link #DATA_SUFFIX_DECORATION} decoration of the
   * given interface node.
   * 
   * @param itf an interface node.
   * @return The value of the {@link #DATA_SUFFIX_DECORATION} decoration or
   *         <code>null</code> if the given node has no such decoration.
   * @see #DATA_SUFFIX_DECORATION
   */
  public static String getDataSuffix(final Interface itf) {
    return (String) ((Node) itf).astGetDecoration(DATA_SUFFIX_DECORATION);
  }

  /**
   * A decoration set on the <code>binding-controller</code> {@link Interface}
   * node to indicate if the binding controller is NOT implemented by the component
   * implementation.
   * 
   * @see #isDefaultBindingController(Interface)
   * @see #hasDefaultBindingController(InterfaceContainer)
   * @see #setDefaultBindingController(Interface, boolean)
   */
  public static final String DEFAULT_BC_DECORATION = "default-binding-controller";

  /**
   * Sets the value of the {@link #DEFAULT_BC_DECORATION} decoration to the given
   * value.
   * 
   * @param itf an interface node. This node is supposed to be the
   *            <code>binding-controller</code> interface.
   * @param value the value of the decoration.
   * @see #DEFAULT_BC_DECORATION
   */
  public static void setDefaultBindingController(final Interface itf,
      final boolean value) {
    ((Node) itf).astSetDecoration(DEFAULT_BC_DECORATION, value);
  }

  /**
   * Returns <code>true</code> if and only if the given {@link Interface} node
   * has a {@link #DEFAULT_BC_DECORATION} decoration with the <code>true</code>
   * value.
   * 
   * @param itf an interface node.
   * @return the value of the {@link #DEFAULT_BC_DECORATION}, or
   *         <code>false</code> if it is not set.
   * @see #DEFAULT_BC_DECORATION
   */
  public static boolean isDefaultBindingController(final Interface itf) {
    final Boolean b = (Boolean) ((Node) itf)
        .astGetDecoration(DEFAULT_BC_DECORATION);
    return (b == null) ? false : b;
  }

  /**
   * Helper method to retrieve the value of the {@link #DEFAULT_BC_DECORATION}
   * decoration of the <code>binding-controller</code> interface node
   * contained by the given container.
   * 
   * @param itfContainer an {@link Interface} container.
   * @return return the value of the {@link #DEFAULT_BC_DECORATION} decoration of
   *         the <code>"binding-controller"</code> interface node or
   *         <code>false</code> if the given interface container does not
   *         contain a <code>binding-controller</code> interface.
   * @see #DEFAULT_BC_DECORATION
   */
  public static boolean hasDefaultBindingController(
      final InterfaceContainer itfContainer) {
    for (final Interface itf : itfContainer.getInterfaces()) {
      if (itf.getName().equals("binding-controller"))
        return isDefaultBindingController(itf);
    }
    return false;
  }
}
