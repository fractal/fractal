/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;

/**
 * A very simple delegating loader which call a client {@link Checker} before
 * returning the loaded AST.
 */
public class CheckerLoader extends AbstractLoader implements BindingController {

  // ---------------------------------------------------------------------------
  // Client interface
  // ---------------------------------------------------------------------------

  /** The name of the {@link Checker} client interface of this component. */
  public static final String CHECKER_ITF = "checker";

  /** The {@link Checker} client interface. */
  public Checker             checkerItf;

  // ---------------------------------------------------------------------------
  // Implementation of the Loader interface
  // ---------------------------------------------------------------------------

  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    final Definition d = clientLoader.load(name, context);
    checkerItf.check(d, context);
    return d;
  }

  // ---------------------------------------------------------------------------
  // Overridden BindingController methods
  // ---------------------------------------------------------------------------

  @Override
  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    try {
      if (s.equals(LOADER_BINDING)) {
        this.clientLoader = (Loader) o;
      } else if (s.equals(CHECKER_ITF)) {
        this.checkerItf = (Checker) o;
      } else {
        throw new NoSuchInterfaceException("There is no interface named '" + s
            + "'");
      }
    } catch (final RuntimeException e) {
      e.printStackTrace();
      throw new IllegalBindingException(
          "Client interface type for interface named ' " + s
              + "' does not match reference type");
    }
  }

  @Override
  public String[] listFc() {
    return new String[]{LOADER_BINDING, CHECKER_ITF};
  }

  @Override
  public Object lookupFc(final String s) throws NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(LOADER_BINDING)) {
      return this.clientLoader;
    } else if (s.equals(CHECKER_ITF)) {
      return this.checkerItf;
    } else {
      throw new NoSuchInterfaceException("There is no interface named '" + s
          + "'");
    }
  }

  @Override
  public void unbindFc(final String s) throws NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (s.equals(LOADER_BINDING)) {
      this.clientLoader = null;
    } else if (s.equals(CHECKER_ITF)) {
      this.checkerItf = null;
    } else {
      throw new NoSuchInterfaceException("There is no interface named '" + s
          + "'");
    }
  }

}
