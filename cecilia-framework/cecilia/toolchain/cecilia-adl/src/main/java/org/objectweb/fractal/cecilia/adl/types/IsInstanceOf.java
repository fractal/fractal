/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.types;

import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.task.core.composition.Function;
import org.objectweb.fractal.task.core.composition.FunctionExecutionException;
import org.objectweb.fractal.task.core.composition.InvalidNumberOfParameterException;
import org.objectweb.fractal.task.core.composition.InvalidParameterTypeException;
import org.objectweb.fractal.task.core.composition.NullParameterException;

/**
 * Implements a Task composition function that can be used to test if a given
 * {@link ComponentContainer} defines the type of another one. <br>
 * This function takes two parameters of type {@link ComponentContainer}. It
 * returns <code>true</code> if the
 * {@link TypeDecorationUtil#getTypeDecoration(ComponentContainer) type} of the
 * first parameter is equals to the second parameter; or is the type of the
 * first parameter is null, returns <code>true</code> if the two parameter are
 * the same.
 * 
 * @see TypeDecorationUtil#getTypeDecoration(ComponentContainer)
 */
public class IsInstanceOf implements Function {

  // ---------------------------------------------------------------------------
  // Implementation of the Function interface
  // ---------------------------------------------------------------------------

  public Object invoke(final Object... params)
      throws FunctionExecutionException {
    if (params.length != 2) {
      throw new InvalidNumberOfParameterException(this, 2, params.length);
    }
    final Object comp = params[0];
    final Object type = params[1];
    if (comp == null) throw new NullParameterException(this, 0);
    if (type == null) throw new NullParameterException(this, 1);
    if (!(comp instanceof ComponentContainer))
      throw new InvalidParameterTypeException(this, 0,
          ComponentContainer.class, comp.getClass());
    if (!(type instanceof ComponentContainer))
      throw new InvalidParameterTypeException(this, 1,
          ComponentContainer.class, type.getClass());
    final Object t = TypeDecorationUtil
        .getTypeDecoration((ComponentContainer) comp);
    if (t == null) {
      return comp == type;
    } else {
      return t == type;
    }
  }
}
