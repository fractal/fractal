/***
 * Cecilia ADL Compiler
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl;

import java.util.Map;
import java.util.TreeMap;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.api.control.BindingController;

/**
 * An implementation of the {@link Checker} interface that delegates
 * {@link Definition} check requests to a set of {@link Checker checkers}
 * according to the alphabetical order of their client interface names.
 *
 * @author Alessio Pace
 */
public class CheckerDispatcher implements Checker, BindingController {

  /**
   * Name of the collection interface bound to the {@link Checker Checkers} used
   * by this checker.
   */
  public static final String  PRIMITIVE_CHECKERS = "client-checker";

  /**
   * The primitive checkers used by this checker.
   */
  public Map<String, Checker> checkersItf        = new TreeMap<String, Checker>();

  // --------------------------------------------------------------------------
  // Implementation of the Checker interface
  // --------------------------------------------------------------------------

  public void check(final Definition definition,
      final Map<Object, Object> context) throws ADLException {
    for (final Checker checker : checkersItf.values()) {
      checker.check(definition, context);
    }
  }

  // --------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // --------------------------------------------------------------------------

  public String[] listFc() {
    return checkersItf.keySet().toArray(new String[checkersItf.size()]);
  }

  public Object lookupFc(final String itf) {
    if (itf.startsWith(PRIMITIVE_CHECKERS)) {
      return checkersItf.get(itf);
    }
    return null;
  }

  public void bindFc(final String itf, final Object value) {
    if (itf.startsWith(PRIMITIVE_CHECKERS)) {
      checkersItf.put(itf, (Checker) value);
    }
  }

  public void unbindFc(final String itf) {
    if (itf.startsWith(PRIMITIVE_CHECKERS)) {
      checkersItf.remove(itf);
    }
  }
}
