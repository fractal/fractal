/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.primitive.thinkMC.types;

import static java.util.Arrays.asList;
import static org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil.getBoundTo;
import static org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil.getContainer;
import static org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil.isNoStaticBinding;
import static org.objectweb.fractal.task.core.TaskFactory.EXPORT_ALL;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractInstantiationTask;
import org.objectweb.fractal.cecilia.adl.InstanceNameProvider;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.TypeNameProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.interfaces.AbstractClientInterfaceVisitor;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterfaceForEach;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the instantiation of the imported interfaces of
 * the component for <code>ThinkMC</code> dialect.
 */
public class ClientInterfaceInstantiationVisitor
    extends
      AbstractClientInterfaceVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractClientInterfaceVisitor
  // ---------------------------------------------------------------------------

  /**
   * Creates a task that writes the source code containing the instantiation of
   * the client interfaces of the component.
   */
  @Override
  protected Component createTask(final ComponentContainer container,
      final List<TypeInterface> clientInterfaces) throws ADLException,
      TaskException {
    final LinkedHashMap<TypeInterface, Interface> clientInterfaceBindings = new LinkedHashMap<TypeInterface, Interface>();
    final Set<Object> serverComponents = new HashSet<Object>();
    for (final TypeInterface clientInterface : clientInterfaces) {
      final Interface serverItf = isNoStaticBinding(clientInterface)
          ? null
          : getBoundTo(clientInterface);
      clientInterfaceBindings.put(clientInterface, serverItf);
      if (serverItf != null) {
        serverComponents.add(getContainer(serverItf));
      }
    }
    final Component includeTask = createIncludeClientInstanceTask(container,
        clientInterfaceBindings, serverComponents);
    final Component initializationTask = createInitializationClientInstanceTask(
        container, clientInterfaceBindings, serverComponents);

    return taskFactoryItf.newCompositeTask(asList(includeTask,
        initializationTask), EXPORT_ALL, null);
  }

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  protected Component createIncludeClientInstanceTask(
      final ComponentContainer container,
      final LinkedHashMap<TypeInterface, Interface> clientInterfaceBindings,
      final Set<Object> serverComponents) throws TaskException {
    return taskFactoryItf.newPrimitiveTask(new IncludeClientInstanceTask(
        clientInterfaceBindings), container, serverComponents);
  }

  protected Component createInitializationClientInstanceTask(
      final ComponentContainer container,
      final LinkedHashMap<TypeInterface, Interface> clientInterfaceBindings,
      final Set<Object> serverComponents) throws TaskException {
    return taskFactoryItf.newPrimitiveTask(
        new InitializationClientInstanceTask(clientInterfaceBindings),
        container, serverComponents);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds part of the source code for imported interface instantiation for the
   * given component node. More precisely, this task builds the source codes
   * that includes the ".adl.h" files of the server components to which the
   * client interfaces are statically bound. This task provides the produced
   * source code. <br>
   * The <code>serverComponents</code> arguments must refer to the collection
   * of the server components to which the given component is statically bound
   * to.
   */
  @TaskParameters({"componentNode", "serverComponents"})
  @ServerInterfaces(@ServerInterface(name = "source-code-provider", signature = SourceCodeProvider.class, record = "role:clientInterfaceInstantiation, id:%, codePiece:include", parameters = "componentNode"))
  public static class IncludeClientInstanceTask
      extends
        AbstractInstantiationTask {

    protected final LinkedHashMap<TypeInterface, Interface> staticBindings;

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /**
     * The client interface used to retrieve the type name of the server
     * components to which client interfaces are bound to.
     */
    @ClientInterfaceForEach(iterable = "serverComponents", prefix = "server-type-name-provider", signature = TypeNameProvider.class, record = "role:staticBindingTypeNameProvider, id:%", parameters = "serverComponents.element")
    public final Map<Object, TypeNameProvider>              serverTypeNameProviderItf       = new HashMap<Object, TypeNameProvider>();

    /**
     * The client interfaces used to retrieve the code that defines the type of
     * the server components to which client interfaces are bound to.
     */
    @ClientInterfaceForEach(iterable = "serverComponents", prefix = "server-type-definition-provider", signature = SourceCodeProvider.class, record = "role:staticBindingTypeDefinitionProvider, id:%", parameters = "serverComponents.element")
    public final Map<Object, SourceCodeProvider>            serverTypeDefinitionProviderItf = new HashMap<Object, SourceCodeProvider>();

    /**
     * The client interfaces used to retrieve the instance name of the server
     * components to which client interfaces are bound to.
     */
    @ClientInterfaceForEach(iterable = "serverComponents", prefix = "server-instance-name-provider", signature = InstanceNameProvider.class, record = "role:staticBindingInstanceNameProvider, id:%", parameters = "serverComponents.element")
    public final Map<Object, InstanceNameProvider>          serverInstanceNameProviderItf   = new HashMap<Object, InstanceNameProvider>();

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param staticBindings Map associating client interface node to the server
     *            interface to which they are bound to (or null if the client
     *            interface is not statically bound). Use LinkedHashMap to
     *            ensure iteration order.
     */
    public IncludeClientInstanceTask(
        final LinkedHashMap<TypeInterface, Interface> staticBindings) {
      this.staticBindings = staticBindings;
    }

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter(
          "Client Interfaces Instantiation Builder (extern references to server components.)");
      for (final Map.Entry<TypeInterface, Interface> staticBinding : staticBindings
          .entrySet()) {
        // if no static binding, pass it.
        if (staticBinding.getValue() == null) continue;

        final Object serverComponent = getContainer(staticBinding.getValue());
        final String serverTypeName = serverTypeNameProviderItf.get(
            serverComponent).getCTypeName();
        final String serverTypeDefinition = serverTypeDefinitionProviderItf
            .get(serverComponent).getSourceCode();
        final String serverInstanceName = serverInstanceNameProviderItf.get(
            serverComponent).getCInstanceName();

        // Bug 307360: if the server component has the same type as this
        // component do not (re)append type definition
        cw.append("#ifndef ").append(serverInstanceName).append("_DECLARED")
            .endl();
        if (serverTypeName.equals(typeNameProviderItf.getCTypeName())) {
          cw.append("extern struct ").append(
              serverTypeName.toString().replace('.', '_')).append("_t ")
              .append(serverInstanceName.replace('.', '_')).append(";").endl();
        } else {
          cw.append(serverTypeDefinition).endl();
          cw.append("extern struct ").append(
              serverTypeName.toString().replace('.', '_')).append("_exporteds ")
              .append(serverInstanceName.replace('.', '_')).append(";").endl();
        }
        cw.append("#define ").append(serverInstanceName).append("_DECLARED")
            .endl();
        cw.appendln("#endif");

      }
      return cw.toString();
    }
  }

  /**
   * Builds part of the source code for imported interface instantiation for the
   * given component node. More precisely, this task builds the source codes
   * that initializes the client interfaces to point to the server interfaces
   * they are statically bound to. This task provides the produced source code.
   * <br>
   * The <code>serverComponents</code> arguments must refer to the collection
   * of the server components to which the given component is statically bound
   * to.
   */
  @ServerInterfaces(@ServerInterface(name = "source-code-provider", signature = SourceCodeProvider.class, record = "role:clientInterfaceInstantiation, id:%, codePiece:initialization", parameters = "componentNode"))
  public class InitializationClientInstanceTask
      extends
        IncludeClientInstanceTask {

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @see IncludeClientInstanceTask#IncludeClientInstanceTask(LinkedHashMap)
     */
    public InitializationClientInstanceTask(
        final LinkedHashMap<TypeInterface, Interface> clientItfNodes) {
      super(clientItfNodes);
    }

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter(
          "Client Interfaces Instantiation Builder (initialization)");
      cw.append("{").endl();
      for (final Map.Entry<TypeInterface, Interface> clientBinding : staticBindings
          .entrySet()) {

        final TypeInterface clientItf = clientBinding.getKey();
        final Interface serverItf = clientBinding.getValue();
        if (serverItf == null) {
          cw.append("0");
        } else {
          final Object serverComponent = getContainer(serverItf);
          final String serverTypeName = serverTypeNameProviderItf.get(
              serverComponent).getCTypeName();
          final String serverInstance = serverInstanceNameProviderItf.get(
              serverComponent).getCInstanceName();

          if (serverTypeName.equals(typeNameProviderItf.getCTypeName())) {
            // Bug 307360: if the server component has the same type as this
            // component, server interfaces are accessed through
            // ".type.exported"
            cw.append(
                "(R" + clientItf.getSignature().replace('.', '_') + "*)&(")
                .append(serverInstance.replace('.', '_')).append(
                    ".type.exported.").append(
                    serverItf.getName().replace('-', '_')).append(')');
          } else {
            // otherwise, server interfaces are accessed through ".exported"
            cw.append(
                "(R" + clientItf.getSignature().replace('.', '_') + "*)&(")
                .append(serverInstance.replace('.', '_')).append(".")
                .append(serverItf.getName().replace('-', '_')).append(')');
          }
        }
        cw.append(", // ").append(clientItf.getName()).endl();

      }
      cw.append("}").endl();

      return cw.toString();
    }
  }
}
