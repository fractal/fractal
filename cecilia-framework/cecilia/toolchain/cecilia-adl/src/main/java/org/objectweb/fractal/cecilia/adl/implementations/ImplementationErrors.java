/**
 * Cecilia ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.implementations;

import static org.objectweb.fractal.adl.error.ErrorTemplateValidator.validErrorTemplate;

import org.objectweb.fractal.adl.error.ErrorTemplate;
import org.objectweb.fractal.adl.implementations.ImplementationCodeLoader;

/**
 * {@link ErrorTemplate} group for the implementations package. This enumeration
 * complete the
 * {@link org.objectweb.fractal.adl.implementations.ImplementationErrors fractalADL ImplementationErrors enumeration}.
 * {@link #getErrorId() errorIds} defined in this enumeration starts at
 * <code>100</code>.
 */
public enum ImplementationErrors implements ErrorTemplate {

  /**
   * Template used when an {@link ImplementationCodeLoader} plugin can't be
   * found.
   */
  IMPL_LOADER_PLUGIN_NOT_FOUND(
      "Can't find ImplementationCodeLoader plugin for language \"%s\". Checks 'language' attribute.",
      "language"),

  /**
   * Template used when a language visitor plugin can't be found.
   */
  LANGUAGE_VISITOR_PLUGIN_NOT_FOUND(
      "Can't find visitory plugin for language \"%s\". Checks 'language' attribute.",
      "language"),

  /** Template used when the implementation language is missing. */
  MISSING_LANGUAGE("Implementation language missing");

  private static final int ORDINAL_OFFSET = 100;

  private int              id;
  private String           format;

  private ImplementationErrors(final String format, final Object... args) {
    this.id = ORDINAL_OFFSET + ordinal();
    this.format = format;

    assert validErrorTemplate(this, args);
  }

  public int getErrorId() {
    return id;
  }

  public String getGroupId() {
    return org.objectweb.fractal.adl.implementations.ImplementationErrors.GROUP_ID;
  }

  public String getFormatedMessage(final Object... args) {
    return String.format(format, args);
  }

  public String getFormat() {
    return format;
  }
}
