/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.compiler;

/**
 * AST interface for "output" node.
 */
public interface OutputFormat {

  /**
   * The value of the output format to be used to compile the component in an
   * archive.
   */
  String ARCHIVE_OUTPUT_FORMAT = "archive";

  /**
   * Returns the value of the <code>format</code> attribute of the
   * <code>output</code> node.
   * 
   * @return the value of the <code>format</code> attribute.
   */
  String getFormat();

  /**
   * Sets the value of the <code>format</code> attribute of the
   * <code>output</code> node.
   * 
   * @param format the value of the <code>format</code> attribute.
   */
  void setFormat(String format);
}
