/***
 * Cecilia ADL Compiler
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.components;

import org.objectweb.fractal.adl.components.Component;

/**
 * AST Node interface which adds a <code>startOrder</code> attribute to
 * <code>component</code> element.
 */
public interface ExtendedComponent extends Component {

  /**
   * Returns the value of the <code>startOrder</code> attribute.
   * 
   * @return the value of the <code>startOrder</code> attribute.
   */
  String getStartOrder();

  /**
   * Set the value of the <code>startOrder</code> attribute.
   * 
   * @param startOrder the value of the <code>startOrder</code> attribute. Must
   *          be a string representation of a positive or null integer.
   * @see #getStartOrder()
   */
  void setStartOrder(String startOrder);
}
