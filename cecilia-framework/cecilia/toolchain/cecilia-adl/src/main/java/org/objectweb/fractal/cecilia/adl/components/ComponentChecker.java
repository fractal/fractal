/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.components;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.cecilia.adl.Checker;

/**
 * Basic implementation of the {@link Checker} interface. This implementation
 * recurse in component architecture and delegates check for each component to a
 * {@link PrimitiveChecker}.
 */
public class ComponentChecker implements Checker, BindingController {

  // --------------------------------------------------------------------------
  // Client interface
  // --------------------------------------------------------------------------

  /**
   * The name of the {@link PrimitiveChecker} client interface used by this
   * compiler.
   */
  public static final String PRIMITIVE_CHECKER_ITF = "primitive-checker";

  /** The primitive checker used by this compiler. */
  protected PrimitiveChecker primitiveCheckerItf;

  // --------------------------------------------------------------------------
  // Implementation of the Compiler interface
  // --------------------------------------------------------------------------

  public void check(final Definition definition,
      final Map<Object, Object> context) throws ADLException {
    if (definition instanceof ComponentContainer) {
      check(new ArrayList<ComponentContainer>(),
          (ComponentContainer) definition, context);
    }
  }

  // --------------------------------------------------------------------------
  // Compilation methods
  // --------------------------------------------------------------------------

  void check(final List<ComponentContainer> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    primitiveCheckerItf.beforeSubComponentCheck(path, container, context);
    path.add(container);
    final Component[] comps = container.getComponents();
    for (final Component element : comps) {
      check(path, element, context);
    }
    path.remove(path.size() - 1);

    primitiveCheckerItf.afterSubComponentCheck(path, container, context);
  }

  public void bindFc(final String itfName, final Object value)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (itfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (itfName.equals(PRIMITIVE_CHECKER_ITF)) {
      try {
        this.primitiveCheckerItf = (PrimitiveChecker) value;
      } catch (final RuntimeException e) {
        e.printStackTrace();
        throw new IllegalBindingException("Unmatching types for interface '"
            + itfName + "'");
      }
    } else {
      throw new NoSuchInterfaceException("There is no interface named '"
          + itfName + "'");
    }

  }

  public String[] listFc() {
    return new String[]{PRIMITIVE_CHECKER_ITF};
  }

  public Object lookupFc(final String itfName) throws NoSuchInterfaceException {

    if (itfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (itfName.equals(PRIMITIVE_CHECKER_ITF)) {

      return this.primitiveCheckerItf;
    } else {
      throw new NoSuchInterfaceException("There is no interface named '"
          + itfName + "'");
    }
  }

  public void unbindFc(final String itfName) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException {

    if (itfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (itfName.equals(PRIMITIVE_CHECKER_ITF)) {

      this.primitiveCheckerItf = null;
    } else {
      throw new NoSuchInterfaceException("There is no interface named '"
          + itfName + "'");
    }
  }
}
