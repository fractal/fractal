/**
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Initial Developper : J.-P. Fassino.
 * Author: Matthieu Leclercq
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.idl;

import static org.objectweb.fractal.cecilia.adl.ReservedWordsChecker.isReservedCWord;
import static org.objectweb.fractal.cecilia.adl.idl.util.Util.getComplexType;
import static org.objectweb.fractal.cecilia.adl.idl.util.Util.getContainedType;
import static org.objectweb.fractal.cecilia.adl.idl.util.Util.isComplexType;
import static org.objectweb.fractal.cecilia.adl.idl.util.Util.setContainedDefinition;
import static org.objectweb.fractal.cecilia.adl.idl.util.Util.setImportedAST;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.ContextLocal;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.error.ChainedErrorLocator;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.error.NodeErrorLocator;
import org.objectweb.fractal.adl.interfaces.IDLLoader;
import org.objectweb.fractal.adl.timestamp.Timestamp;
import org.objectweb.fractal.adl.util.ClassLoaderHelper;
import org.objectweb.fractal.adl.xml.XMLNode;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.cecilia.adl.idl.ast.ComplexType;
import org.objectweb.fractal.cecilia.adl.idl.ast.EnumDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.ExtendingDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Field;
import org.objectweb.fractal.cecilia.adl.idl.ast.FieldContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Import;
import org.objectweb.fractal.cecilia.adl.idl.ast.ImportContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.InterfaceDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Method;
import org.objectweb.fractal.cecilia.adl.idl.ast.MethodContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.Parameter;
import org.objectweb.fractal.cecilia.adl.idl.ast.ParameterContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.RecordDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Type;
import org.objectweb.fractal.cecilia.adl.idl.ast.TypeContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.UnionDefinition;
import org.objectweb.fractal.cecilia.adl.idl.parser.IDLParser;
import org.objectweb.fractal.cecilia.adl.idl.parser.ParseException;
import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;
import org.xml.sax.SAXException;

/**
 * {@link IDLLoader} implementation for IDL source code. The
 * {@link #loadInterface(String, Map) loadCode} method returns the AST of the
 * IDL, (eventually) merging fields and methods in the above IDL inheritance
 * tree, and decorating it with an ordered sequence of IDL types from the bottom
 * to the top of inheritance tree.
 */
public class BasicIDLLoader implements IDLLoader, BindingController {

  /** the client interface name for the node factory. */
  public static final String                         NODE_FACTORY_ITF_NAME = "node-factory";

  /**
   * Client interface bound to the {@link XMLNodeFactory node factory}
   * component.
   */
  public XMLNodeFactory                              nodeFactoryItf;

  protected ContextLocal<Map<String, IDLDefinition>> loadedCode            = new ContextLocal<Map<String, IDLDefinition>>();

  // ---------------------------------------------------------------------------
  // Implementation of the IDLLoader interface
  // ---------------------------------------------------------------------------

  public Object loadInterface(final String signature,
      final Map<Object, Object> context) throws ADLException {
    if (signature == null || signature.equals("")) {
      throw new IllegalArgumentException(
          "Signature to be loaded can't be empty or null");
    }
    // first, search in cache
    Map<String, IDLDefinition> cache = loadedCode.get(context);
    if (cache == null) {
      cache = new HashMap<String, IDLDefinition>();
      loadedCode.set(context, cache);
    }
    final Object c = cache.get(signature);
    if (c != null) return c;

    final ClassLoader loader = ClassLoaderHelper.getClassLoader(this, context);
    final Map<String, DependantNode> toBeLoaded = new HashMap<String, DependantNode>();
    final IDLDefinition definition = loadIDL(signature, toBeLoaded,
        new ArrayList<String>(), loader, cache);

    // load IDLs that have to be loaded.
    // the while statement does not use an Iterator on toBeLoaded map since
    // iterations may modify the Map, which invalidate the iterator.
    while (!toBeLoaded.isEmpty()) {

      // remove the first element of the toBeLoaded map.
      final Iterator<Map.Entry<String, DependantNode>> iter = toBeLoaded
          .entrySet().iterator();
      final Map.Entry<String, DependantNode> elem = iter.next();
      iter.remove();

      final DependantNode dep = elem.getValue();
      // Load the definition
      try {
        final IDLDefinition def = loadIDL(elem.getKey(), toBeLoaded,
            new ArrayList<String>(), loader, cache);
        // set the definition as the complex type of each ComplexTypeContainer
        dep.setDefinition(def);
      } catch (final ADLException e) {
        if (dep.imports != null)
          ChainedErrorLocator.chainLocator(e, dep.imports.get(0));
        else if (dep.types != null)
          ChainedErrorLocator.chainLocator(e, dep.types.get(0));

        throw e;
      }
    }
    return definition;
  }

  // --------------------------------------------------------------------------
  // Utility methods
  // --------------------------------------------------------------------------

  protected IDLDefinition loadIDL(final String signature,
      final Map<String, DependantNode> toBeLoaded,
      final List<String> inheritancePath, final ClassLoader loader,
      final Map<String, IDLDefinition> cache) throws ADLException {

    // first, search in cache
    IDLDefinition node = cache.get(signature);
    if (node != null) return node;

    final URL srcFile = locate(signature, loader);

    InputStream inputStream;
    try {
      inputStream = srcFile.openStream();
    } catch (final IOException e) {
      throw new ADLException(IDLErrors.IO_ERROR, e, srcFile.getPath());
    }
    node = parseIDL(inputStream, signature, srcFile.getPath());

    // Set timestamp of the IDL file
    Timestamp.setTimestamp(node, srcFile);

    check(signature, node, toBeLoaded, inheritancePath, loader, cache);
    if (node instanceof ImportContainer) {
      for (final Import i : ((ImportContainer) node).getImports()) {
        DependantNode d = toBeLoaded.get(i.getName());
        if (d == null) {
          d = new DependantNode();
          toBeLoaded.put(i.getName(), d);
        }
        d.addImport(i);
      }
    }

    cache.put(signature, node);
    return node;
  }

  /**
   * Creates an {@link IDLParser} and uses it to parse the IDL.
   * 
   * @param inputStream
   * @param srcFile
   * @return
   * @throws IDLSyntaxErrorException
   */
  protected IDLDefinition parseIDL(final InputStream inputStream,
      final String signature, final String srcFile) throws ADLException {
    IDLDefinition node;
    final IDLParser parser = new IDLParser(inputStream);
    parser.init(nodeFactoryItf, CeciliaIDLConstants.CECILIA_IDL_DTD, srcFile);
    try {
      node = parser.getIDLNode();
    } catch (final ParseException e) {
      throw new ADLException(IDLErrors.SYNTAX_ERROR, signature, e.getMessage());
    }
    return node;
  }

  protected URL locate(final String signature, final ClassLoader loader)
      throws ADLException {
    final URL srcFile = loader
        .getResource(signature.replace('.', '/') + ".idl");
    if (srcFile == null) {
      throw new ADLException(IDLErrors.IDL_NOT_FOUND, signature);
    }
    return srcFile;
  }

  protected void resolv(final ComplexType typeNode,
      final IDLDefinition idlNode, final Map<String, DependantNode> toBeLoaded,
      final ClassLoader loader, final Map<String, IDLDefinition> cache)
      throws ADLException {
    final String signatureToResolv = typeNode.getName();
    final String resolvedSignature = resolveSignature(idlNode,
        signatureToResolv, typeNode, loader);
    addImport(resolvedSignature, (ImportContainer) idlNode);
    final IDLDefinition definition = cache.get(resolvedSignature);
    if (definition == null) {
      // the IDL is not loaded, add it to the toBeLoaded map
      DependantNode l = toBeLoaded.get(resolvedSignature);
      if (l == null) {
        l = new DependantNode();
        toBeLoaded.put(resolvedSignature, l);
      }
      l.addComplexType(typeNode);
    } else {
      setContainedDefinition(typeNode, definition);
    }
  }

  /**
   * Resolve the full qualified name of a signature.
   */
  protected String resolveSignature(final IDLDefinition idlNode,
      final String signatureToResolv, final Node referencingNode,
      final ClassLoader loader) throws ADLException {
    final String idlSignature = idlNode.getName();
    String resolvedSignature = null;
    if (signatureToResolv.equals(idlSignature)) {
      return idlSignature;
    } else if (signatureToResolv.indexOf('.') != -1) {
      // the signature contains a '.', it is a fully qualified name.
      resolvedSignature = signatureToResolv;
    } else {
      for (final Import imp : ((ImportContainer) idlNode).getImports()) {
        final String importName = imp.getName();
        final String interfaceName = importName.substring(importName
            .lastIndexOf('.') + 1);
        if (signatureToResolv.equals(interfaceName)) {
          resolvedSignature = importName;
          break;
        }
      }

      if (resolvedSignature == null) {
        // If the signature to resolv is used without package name and it is in
        // the same package with the userSignature
        final int i = idlSignature.lastIndexOf('.');
        if (i < 0) {
          resolvedSignature = signatureToResolv;
        } else {
          resolvedSignature = idlSignature.substring(0, i) + "."
              + signatureToResolv;
        }
      }
    }
    try {
      locate(resolvedSignature, loader);
    } catch (final ADLException e) {
      ChainedErrorLocator.chainLocator(e, referencingNode);
      throw e;
    }
    return resolvedSignature;
  }

  protected void check(final String signature, IDLDefinition node,
      final Map<String, DependantNode> toBeLoaded,
      final List<String> inheritancePath, final ClassLoader loader,
      final Map<String, IDLDefinition> cache) throws ADLException {

    checkPackageSrcCompatibility(signature, node);
    checkInterfaceNameSrcCompatibility(signature, node);
    normalizeIDL(node, loader);

    /*
     * insert the node signature as first element in the inheritance path at
     * this point
     */
    InterfaceDecorationUtil.updateInheritancePathDecoration(node, signature);

    if (node instanceof InterfaceDefinition) {
      final InterfaceDefinition interfaceDefinition = (InterfaceDefinition) node;
      inheritancePath.add(signature);
      String ext = null;
      if (interfaceDefinition instanceof ExtendingDefinition) {
        ext = ((ExtendingDefinition) interfaceDefinition).getExtends();
        ((ExtendingDefinition) interfaceDefinition).setExtends(null);
      }
      if (ext != null) {

        final String resolvedSuperSignature = resolveSignature(node, ext, node,
            loader);
        if (inheritancePath.contains(resolvedSuperSignature)) {
          throw new ADLException(IDLErrors.INHERITANCE_CYCLE,
              interfaceDefinition, signature, resolvedSuperSignature);
        }
        final IDLDefinition def;
        try {
          def = loadIDL(resolvedSuperSignature, toBeLoaded, inheritancePath,
              loader, cache);
        } catch (final ADLException e) {
          ChainedErrorLocator.chainLocator(e, interfaceDefinition);
          throw e;
        }
        if (!(def instanceof InterfaceDefinition)) {
          throw new ADLException(IDLErrors.INVALID_SUPER_INTERFACE,
              interfaceDefinition, resolvedSuperSignature);
        }
        addImport(resolvedSuperSignature, (ImportContainer) node);
        node = merge(def, node);
      }
      inheritancePath.remove(inheritancePath.size() - 1);

      for (final Field field : ((FieldContainer) interfaceDefinition)
          .getFields()) {
        final Type type = getContainedType(field);
        if (isComplexType(type))
          resolv(getComplexType(type), node, toBeLoaded, loader, cache);
      }
      for (final Method method : ((MethodContainer) interfaceDefinition)
          .getMethods()) {
        final Type returnType = getContainedType(method);
        if (isComplexType(returnType))
          resolv(getComplexType(returnType), node, toBeLoaded, loader, cache);

        for (final Parameter parameter : ((ParameterContainer) method)
            .getParameters()) {
          final Type parameterType = getContainedType(parameter);
          if (isComplexType(parameterType))
            resolv(getComplexType(parameterType), node, toBeLoaded, loader,
                cache);
        }
      }
    } else if (node instanceof RecordDefinition) {
      final RecordDefinition recordDefinition = (RecordDefinition) node;

      for (final Field field : ((FieldContainer) recordDefinition).getFields()) {
        final Type type = getContainedType(field);
        if (isComplexType(type))
          resolv(getComplexType(type), node, toBeLoaded, loader, cache);
      }
    } else if (node instanceof UnionDefinition) {
      final UnionDefinition unionDefinition = (UnionDefinition) node;

      for (final Field field : ((FieldContainer) unionDefinition).getFields()) {
        final Type type = getContainedType(field);
        if (isComplexType(type))
          resolv(getComplexType(type), node, toBeLoaded, loader, cache);
      }
    } else if (node instanceof EnumDefinition) {
      // DO NOTHING HERE
    } else {
      throw new CompilerError(GenericErrors.INTERNAL_ERROR,
          new NodeErrorLocator(node), "Unrecognized IDL construct in '"
              + signature + "'");
    }
  }

  protected IDLDefinition merge(final IDLDefinition superNode,
      final IDLDefinition node) throws ADLException {
    // add imports from superNode to node
    final Set<String> imports = new HashSet<String>();
    for (final Import i : ((ImportContainer) node).getImports())
      imports.add(i.getName());

    // add import of superNode only if it is not already present in Node
    for (final Import i : ((ImportContainer) superNode).getImports())
      if (imports.add(i.getName())) ((ImportContainer) node).addImport(i);

    /*
     * Add fields from 'superNode' to 'node'. Sub nodes of 'superNode' must be
     * placed before sub nodes of 'node' (see bug #24670). So we need to remove
     * fields and methods of 'node', then add those of 'superNode' and finally
     * re-add 'node' ones.
     */
    final FieldContainer nodeFieldCtn = (FieldContainer) node;
    final Field[] nodeFields = nodeFieldCtn.getFields();
    final MethodContainer nodeMethodCtn = (MethodContainer) node;
    final Method[] nodeMethods = nodeMethodCtn.getMethods();

    /*
     * create a map of the sub nodes of node. Keys will be used to detect name
     * clashes and values will be used to create DuplicatedNameException
     */
    final Map<String, Node> nodes = new HashMap<String, Node>();
    for (final Field f : nodeFieldCtn.getFields()) {
      nodes.put(f.getName(), f);
      nodeFieldCtn.removeField(f);
    }
    for (final Method m : (nodeMethodCtn).getMethods()) {
      nodes.put(m.getName(), m);
      nodeMethodCtn.removeMethod(m);
    }

    // add fields from superNode to node
    for (final Field f : ((FieldContainer) superNode).getFields()) {
      final Node n = nodes.put(f.getName(), f);
      if (n != null) {
        throw new ADLException(IDLErrors.DUPLICATED_FIELD_NAME, f, f.getName(),
            new NodeErrorLocator(n));
      }
      nodeFieldCtn.addField(f);
    }

    // add methods from superNode to node
    for (final Method m : ((MethodContainer) superNode).getMethods()) {
      final Node n = nodes.put(m.getName(), m);
      if (n != null) {
        throw new ADLException(IDLErrors.DUPLICATED_METHOD_NAME, m,
            m.getName(), new NodeErrorLocator(n));
      }
      nodeMethodCtn.addMethod(m);
    }

    // re-add fields from node
    for (final Field field : nodeFields)
      nodeFieldCtn.addField(field);

    // re-add methods from node
    for (final Method method : nodeMethods)
      nodeMethodCtn.addMethod(method);

    /* append the parent node signature in the inheritance path for this node */
    InterfaceDecorationUtil.updateInheritancePathDecoration(node,
        InterfaceDecorationUtil.getInheritancePathDecoration(superNode));

    // merge timestamps
    Timestamp.setTimestamp(node, Timestamp.getTimestamp(superNode));
    return node;
  }

  protected void normalizeIDL(final IDLDefinition idlNode,
      final ClassLoader loader) throws ADLException {
    if (idlNode instanceof ImportContainer) {
      for (final Import i : ((ImportContainer) idlNode).getImports()) {
        try {
          locate(i.getName(), loader);
        } catch (final ADLException e) {
          ChainedErrorLocator.chainLocator(e, i);
          throw e;
        }
      }
    }
    final Map<String, Node> nodes = new HashMap<String, Node>();
    // Check for incorrectly named interfaces.
    if (isReservedCWord(idlNode.getName())) {
      throw new ADLException(IDLErrors.IDL_NAME_IS_RESERVED_WORD, idlNode,
          idlNode.getName());
    }

    // Check for duplicate fields
    if (idlNode instanceof FieldContainer) {
      for (final Field f : ((FieldContainer) idlNode).getFields()) {
        if (isReservedCWord(f.getName())) {
          throw new ADLException(IDLErrors.IDL_NAME_IS_RESERVED_WORD, f, f
              .getName());
        }
        final Node n = nodes.put(f.getName(), f);
        if (n != null)
          throw new ADLException(IDLErrors.DUPLICATED_FIELD_NAME, f, f
              .getName(), new NodeErrorLocator(n));
      }
    }
    // Check for duplicate methods, or methods with duplicated parameter names.
    if (idlNode instanceof MethodContainer) {
      for (final Method m : ((MethodContainer) idlNode).getMethods()) {
        if (isReservedCWord(m.getName())) {
          throw new ADLException(IDLErrors.IDL_NAME_IS_RESERVED_WORD, m, m
              .getName());
        }
        final Node previousDeclaration = nodes.put(m.getName(), m);
        if (previousDeclaration != null)
          throw new ADLException(IDLErrors.DUPLICATED_METHOD_NAME, m, m
              .getName(), new NodeErrorLocator(previousDeclaration));
        final Map<String, Node> params = new HashMap<String, Node>();
        for (final Parameter p : m.getParameters()) {
          if (isReservedCWord(p.getName())) {
            throw new ADLException(IDLErrors.IDL_NAME_IS_RESERVED_WORD, p, p
                .getName());
          }
          final Node n1 = params.put(p.getName(), p);
          if (n1 != null) {
            throw new ADLException(IDLErrors.DUPLICATED_PARAMETER_NAME, p, p
                .getName(), new NodeErrorLocator(n1));
          }
        }
      }
    }
  }

  protected XMLNode newNode(final String name) {
    try {
      return nodeFactoryItf.newXMLNode(CeciliaIDLConstants.CECILIA_IDL_DTD,
          name);
    } catch (final SAXException e) {
      throw new CompilerError(GenericErrors.INTERNAL_ERROR, e,
          "Unable to create node \"" + name + "\"");
    }
  }

  protected void addImport(final String signature,
      final ImportContainer importContainer) {
    for (final Import i : importContainer.getImports()) {
      if (i.getName().equals(signature)) return;
    }
    final Import newImport = (Import) newNode("import");
    newImport.setName(signature);
    importContainer.addImport(newImport);
  }

  protected void addComplexType(final IDLDefinition node,
      final TypeContainer complexTypeContainer) {
    // Test if there is already a sub-element
    if (complexTypeContainer.getComplexType() == null) {
      throw new IllegalStateException("There is already a sub element");
    }
    final ComplexType ct = (ComplexType) newNode("complexType");
    ct.setIDLDefinition(node);
    complexTypeContainer.setComplexType(ct);
  }

  protected void checkPackageSrcCompatibility(final String signature,
      final IDLDefinition node) throws ADLException {
    final String nodeSignature = node.getName();
    final String nodePkgName = (nodeSignature.lastIndexOf('.') < 0)
        ? ""
        : nodeSignature.substring(0, nodeSignature.lastIndexOf('.'));
    final String expPkgName = (signature.lastIndexOf('.') < 0) ? "" : signature
        .substring(0, signature.lastIndexOf('.'));
    if (!expPkgName.equals(nodePkgName))
      throw new ADLException(IDLErrors.INVALID_PACKAGE_NAME, node, nodePkgName,
          expPkgName);
  }

  protected void checkInterfaceNameSrcCompatibility(final String signature,
      final IDLDefinition node) throws ADLException {
    final String nodeSignature = node.getName();
    final String nodeItfName = (nodeSignature.lastIndexOf('.') < 0)
        ? nodeSignature
        : nodeSignature.substring(nodeSignature.lastIndexOf('.') + 1);
    final String expItfName = (signature.lastIndexOf('.') < 0)
        ? signature
        : signature.substring(signature.lastIndexOf('.') + 1);
    if (!expItfName.equals(nodeItfName))
      throw new ADLException(IDLErrors.INVALID_IDL_NAME, node, nodeItfName,
          expItfName);
  }

  protected static final class DependantNode {
    private List<ComplexType> types;
    private List<Import>      imports;

    protected void addComplexType(final ComplexType type) {
      if (types == null) types = new ArrayList<ComplexType>();
      types.add(type);
    }

    protected void addImport(final Import i) {
      if (imports == null) imports = new ArrayList<Import>();
      imports.add(i);
    }

    protected void setDefinition(final IDLDefinition def) {

      if (types != null) {
        for (final ComplexType ct : types) {
          ct.setIDLDefinition(def);
        }
      }

      if (imports != null) {
        for (final Import i : imports)
          setImportedAST(i, def);
      }
    }
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  public void bindFc(final String itfName, final Object value)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (itfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (itfName.equals(NODE_FACTORY_ITF_NAME)) {
      this.nodeFactoryItf = (XMLNodeFactory) value;
    } else {
      throw new NoSuchInterfaceException("There is no interface named '"
          + itfName + "'");
    }

  }

  public String[] listFc() {
    return new String[]{NODE_FACTORY_ITF_NAME};
  }

  public Object lookupFc(final String itfName) throws NoSuchInterfaceException {

    if (itfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (itfName.equals(NODE_FACTORY_ITF_NAME)) {
      return this.nodeFactoryItf;
    } else {
      throw new NoSuchInterfaceException("There is no interface named '"
          + itfName + "'");
    }
  }

  public void unbindFc(final String itfName) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException {

    if (itfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (itfName.equals(NODE_FACTORY_ITF_NAME)) {
      this.nodeFactoryItf = null;
    } else {
      throw new NoSuchInterfaceException("There is no interface named '"
          + itfName + "'");
    }
  }
}
