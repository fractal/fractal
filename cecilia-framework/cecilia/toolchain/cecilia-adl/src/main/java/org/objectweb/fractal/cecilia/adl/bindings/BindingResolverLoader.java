/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.bindings;

import static org.objectweb.fractal.adl.types.TypeInterfaceUtil.isCollection;
import static org.objectweb.fractal.adl.types.TypeInterfaceUtil.isServer;
import static org.objectweb.fractal.adl.types.TypeInterfaceUtil.isSingleton;
import static org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil.getServerInterface;
import static org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil.isStaticallyBoundFalse;
import static org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil.setClientInterface;
import static org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil.setServerInterface;
import static org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil.setStaticallyBound;
import static org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil.setDynamic;
import static org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil.isDynamic;
import static org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil.getContainer;
import static org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil.isNoStaticBinding;
import static org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil.setBoundTo;
import static org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil.setContainer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.NodeUtil;
import org.objectweb.fractal.adl.ast.ASTFractalUtil;
import org.objectweb.fractal.adl.bindings.Binding;
import org.objectweb.fractal.adl.bindings.BindingContainer;
import org.objectweb.fractal.adl.bindings.TypeBindingLoader;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.NodeErrorLocator;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.adl.util.FractalADLLogManager;
import org.objectweb.fractal.cecilia.adl.Checker;
import org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;

/**
 * A Loader which "resolves" bindings. It adds to client interfaces of primitive
 * components the {@link InterfaceDecorationUtil#BOUND_TO_DECORATION}
 * decoration. The value of this decoration is the server interface of the
 * primitive component the client interface is bound to. Moreover, this
 * component adds the following decorations :
 * <ul>
 * <li> {@link BindingDecorationUtil#CLIENT_INTERFACE_DECORATION} on each binding
 * node. It is set to the client interface node designated by the
 * {@link Binding#getFrom() client} attribute.</li>
 * <li> {@link BindingDecorationUtil#SERVER_INTERFACE_DECORATION} on each binding
 * node. It is set to the server interface node designated by the
 * {@link Binding#getTo() server} attribute.</li>
 * <li> {@link BindingDecorationUtil#STATICALLY_BOUND_DECORATION} is set to
 * <code>true</code> on each binding node that has to be bound statically.</li>
 * <li> {@link BindingDecorationUtil#DYNAMIC_BINDING_ATTRIBUTE_DECORATION} is
 * set to <code>false</code> on each {@link Binding} node where there is
 * <code>dynamic="false"</code> AND on the client interface forming the
 * binding with this property.
 * <li> {@link InterfaceDecorationUtil#CONTAINER_DECORATION} on each interface
 * node. It is set to the interface container node (i.e. the component node that
 * contains this interface node).</li>
 * </ul>
 * <p>
 * Note: this component assumes that client and server interface Nodes have been
 * fused with the IDLDefinitions loaded from the interface signatures, because
 * it checks binding compatibility based on those and on their
 * {@link InterfaceDecorationUtil#INHERITANCE_PATH_DECORATION}.
 * </p>
 */
public class BindingResolverLoader extends TypeBindingLoader
    implements Checker {

  /**
   * Map associating client interface node to the list of binding nodes it is
   * the client.
   */
  protected Map<TypeInterface, List<Binding>> clientInterfaceBindings;

  // --------------------------------------------------------------------------
  // Implementation of the Loader interface
  // --------------------------------------------------------------------------

  @Override
  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    clientInterfaceBindings = new HashMap<TypeInterface, List<Binding>>();

    final Definition d = super.load(name, context);

    check(d, context);

    // set to null to allow GC
    clientInterfaceBindings = null;
    return d;
  }

  // ---------------------------------------------------------------------------
  // Implementation of the Checker interface
  // ---------------------------------------------------------------------------
  public void check(final Definition definition,
      final Map<Object, Object> context) throws ADLException {
    clientInterfaceBindings = new HashMap<TypeInterface, List<Binding>>();

    checkNode(definition, context);

    addDecoration(definition, context);
    resolvBinding(definition, definition, context);

    // set to null to allow GC
    clientInterfaceBindings = null;
  }

  /**
   * FIXME: this method is very strongly inspired from the method of same name
   * in superclass {@link BindingLoader}, which can't currently be used because
   * it was declared as private.
   */
  private void checkNode(final Object node, final Map<Object, Object> context)
      throws ADLException {
    if (node instanceof BindingContainer) {
      final Map<String, Map<String, Interface>> itfMap = new HashMap<String, Map<String, Interface>>();
      if (node instanceof InterfaceContainer) {
        final Map<String, Interface> containerItfs = new HashMap<String, Interface>();
        for (final Interface itf : ((InterfaceContainer) node).getInterfaces()) {
          containerItfs.put(itf.getName(), itf);
        }
        itfMap.put("this", containerItfs);
      }
      if (node instanceof ComponentContainer) {
        for (final Component comp : ((ComponentContainer) node).getComponents()) {
          if (comp instanceof InterfaceContainer) {
            final Map<String, Interface> compItfs = new HashMap<String, Interface>();
            for (final Interface itf : ((InterfaceContainer) comp)
                .getInterfaces()) {
              compItfs.put(itf.getName(), itf);
            }
            itfMap.put(comp.getName(), compItfs);
          }
        }
      }

      getAdditionalInterfaces(node, context, itfMap);

      for (final Binding binding : ((BindingContainer) node).getBindings()) {
        checkBinding(binding, itfMap, context);
      }
      final Map<String, Binding> fromItfs = new HashMap<String, Binding>();
      for (final Binding binding : ((BindingContainer) node).getBindings()) {
        final Binding previousDefinition = fromItfs.put(binding.getFrom(),
            binding);
        if (previousDefinition != null) {
          throw new ADLException(
              org.objectweb.fractal.adl.bindings.BindingErrors.DUPLICATED_BINDING,
              binding, binding.getFrom(), previousDefinition);
        }
      }
    }
    if (node instanceof ComponentContainer) {
      for (final Component comp : ((ComponentContainer) node).getComponents()) {
        checkNode(comp, context);
      }
    }
  }

  // --------------------------------------------------------------------------
  // Overridden BindingLoader methods
  // --------------------------------------------------------------------------

  @Override
  protected boolean ignoreBinding(final Binding binding,
      final String fromCompName, final String fromItfName,
      final String toCompName, final String toItfName) {
    return false;
  }

  // --------------------------------------------------------------------------
  // Overridden TypeBindingloader methods
  // --------------------------------------------------------------------------

  @Override
  protected void checkBinding(final Binding binding, final Interface fromItf,
      final String fromCompName, final String fromItfName,
      final Interface toItf, final String toCompName, final String toItfName,
      final Map<Object, Object> context) throws ADLException {

    NodeUtil.castNodeError(fromItf, TypeInterface.class);
    NodeUtil.castNodeError(toItf, TypeInterface.class);

    /* invoke super method */
    super.checkBinding(binding, fromItf, fromCompName, fromItfName, toItf,
        toCompName, toItfName, context);

    /* verifies that the binding is between compatible signatures */
    checkBindingSignaturesCompatibility(binding, fromItf, toItf);

    setClientInterface(binding, fromItf);
    setServerInterface(binding, toItf);

    if (toCompName.equals("this") && isCollection(toItf)) {
      // the given binding export a client collection interface through a
      // composite. Currently this is allowed only if interfaces have the same
      // name
      if (!fromItfName.equals(toItfName)) {
        throw new ADLException(BindingErrors.COMPOSITE_CLIENT_COLLECTION,
            binding);
      }
    }

    /*
     * by default a binding is considered to be "dynamic", which means it can be
     * reconfigured during the application lifetime. Check whether the ADL
     * specifies that the binding is not supposed to change (dynamic="false").
     */
    Boolean dynamicAttrValue = true; // default value
    final String disableDynamicFalse = System.getProperty(
        BindingDecorationUtil.CECILIA_DISABLE_DYNAMICFALSE_BINDINGS, "false");
    if (!disableDynamicFalse.equals("true")) {
      if (binding instanceof ExtendedBinding) {
        /* the dynamic attribute can be only "true" or "false", see the DTD */
        String dynStr = ((ExtendedBinding) binding).getDynamic();
        if (dynStr != null && dynStr.equals("false")) {
          dynamicAttrValue = false;
        }
      }
    }
    setDynamic(binding, dynamicAttrValue);

    List<Binding> bindings = clientInterfaceBindings.get(fromItf);
    if (bindings == null) {
      bindings = new ArrayList<Binding>();
      clientInterfaceBindings.put((TypeInterface) fromItf, bindings);
    }
    bindings.add(binding);
  }

  // --------------------------------------------------------------------------
  // Utility methods
  // --------------------------------------------------------------------------

  protected void getAdditionalInterfaces(final Object node,
      final Map<Object, Object> context,
      final Map<String, Map<String, Interface>> itfMap) throws ADLException {
    // Do nothing.
  }

  protected void checkBindingSignaturesCompatibility(final Binding binding,
      final Interface clientItf, final Interface serverItf) throws ADLException {

    if (!(clientItf instanceof IDLDefinitionContainer)
        || !(serverItf instanceof IDLDefinitionContainer)) return;

    final IDLDefinition clientIDLDefinition = ((IDLDefinitionContainer) clientItf)
        .getIDLDefinition();

    if (clientIDLDefinition == null) {
        throw new IllegalStateException(
            "The client interface does not contain an IDLDefinition");
    }

    final IDLDefinition serverIDLDefinition = ((IDLDefinitionContainer) serverItf)
        .getIDLDefinition();

    if (serverIDLDefinition == null) {
      throw new IllegalStateException(
          "The server interface does not contain an IDLDefinition");
    }

    if (!InterfaceDecorationUtil
        .isInheritancePathDecorationPresent(clientIDLDefinition)) {
      throw new IllegalStateException(
          "The client IDLDefinition has not been decorated with the inheritance path.");
    }

    if (!InterfaceDecorationUtil
        .isInheritancePathDecorationPresent(serverIDLDefinition)) {
      throw new IllegalStateException(
          "The server IDLDefinition has not been decorated with the inheritance path.");
    }

    final String clientItfSignature = InterfaceDecorationUtil
        .getInheritancePathDecoration(clientIDLDefinition).get(0);

    final List<String> serverItfInheritancePath = InterfaceDecorationUtil
        .getInheritancePathDecoration(serverIDLDefinition);

    final String serverItfSignature = serverItfInheritancePath.get(0);

    if (!serverItfInheritancePath.contains(clientItfSignature)) {
      final String message = "Client itf signature [" + clientItfSignature
          + "] is not compatible with server itf signature ["
          + serverItfSignature + "]";
      throw new ADLException(message, (Node) binding);
    }

  }

  protected void addDecoration(final Object node,
      final Map<Object, Object> context) {
    if (node instanceof InterfaceContainer) {
      final InterfaceContainer interfaceContainer = (InterfaceContainer) node;
      for (final Interface itf : interfaceContainer.getInterfaces())
        setContainer(itf, interfaceContainer);
    }
    if (node instanceof ComponentContainer) {
      for (final Component component : ((ComponentContainer) node)
          .getComponents())
        addDecoration(component, context);
    }
  }

  private void resolvBinding(final Object node, final Object topLevelNode,
      final Map<Object, Object> context) throws ADLException {
    if (ASTFractalUtil.isPrimitive((ComponentContainer)node)) {
      // primitive component
      if (!(node instanceof InterfaceContainer)) return;
      final InterfaceContainer itfContainer = (InterfaceContainer) node;
      for (final Interface itf : itfContainer.getInterfaces()) {
        if (isServer(itf)) continue;

        // for each client interface of primitive component.
        final TypeInterface clientItf = (TypeInterface) itf;

        // a statically initialized binding is allowed if:
        // 1) the NO_STATIC_BINDING_DECORATION is not set
        // 2) AND the client interface is a singleton.
        final boolean staticBindingAllowed = (!isNoStaticBinding(clientItf))
            && isSingleton(clientItf);

        final List<Binding> bindings = clientInterfaceBindings.get(clientItf);

        // if the client interface is not bound
        if (bindings == null) {
          continue;
        }

        // for each binding of this client interface
        for (final Binding binding : bindings) {
          // set client interface properties on binding node
          if (staticBindingAllowed) setStaticallyBound(binding, true);

          /* the server interface that is target of the current Binding node */
          TypeInterface serverItf = (TypeInterface) getServerInterface(binding);

          /* the component owning that server interface */
          InterfaceContainer serverComponent = getContainer(serverItf);

          /* get the value of the decoration */
          final Boolean isDynamicBindingSetToTrue = BindingDecorationUtil.isDynamic((Node)binding);
 
          /*
           * a dynamic="false" is of course not allowed if the binding is not
           * statically initializable.
           */
          if (!staticBindingAllowed) {
            if (!isDynamicBindingSetToTrue) {
              throw new ADLException(BindingErrors.INVALID_DYNAMIC_FALSE,
                  binding);
            }
          } else {
            if (!isDynamicBindingSetToTrue) {
              BindingDecorationUtil.setDynamic((Node) clientItf, false);
            }
          }

          walkBindingGraph(clientItf, binding, staticBindingAllowed);
        }
      }
    } else if (node instanceof ComponentContainer) {
      for (final Component comp : ((ComponentContainer) node).getComponents()) {
        resolvBinding(comp, topLevelNode, context);
      }
    }
  }

  protected void walkBindingGraph(final TypeInterface clientItf,
      final Binding binding, final boolean staticBindingAllowed)
      throws ADLException {
    final TypeInterface serverItf = (TypeInterface) getServerInterface(binding);
    final InterfaceContainer serverComponent = getContainer(serverItf);

    // if the serverItf belongs to a composite
    if (!ASTFractalUtil.isPrimitive((ComponentContainer)serverComponent)) {
      // if serverItf is a control interface stop here.
      if (isServer(serverItf)
          && (serverItf.getName().endsWith("controller")
              || serverItf.getName().equals("component") || serverItf.getName()
              .equals("factory"))) {
        setBoundTo(clientItf, serverItf);
        return;
      }

      final List<Binding> serverBindings = clientInterfaceBindings
          .get(serverItf);

      if (serverBindings == null) {
        return;
      }

      // If there is more than one binding
      if (serverBindings.size() > 1 && isServer(serverItf)) {
        throw new ADLException(BindingErrors.COMPOSITE_SERVER_COLLECTION,
            serverItf);
      }

      for (final Binding b : serverBindings) {

        // propagate the staticallybound property
        // the property is propagated only if the "statically-bound"
        // decoration is not already set to false.
        if (staticBindingAllowed && !isStaticallyBoundFalse(b))
          setStaticallyBound(b, true);
        else
          // if the static binding is not allowed, set the property to false
          // This avoid the property to be set latter for another client
          // interface exported through the same binding
          setStaticallyBound(b, false);

        // Since the serverItf node belongs to a composite, get the server
        // interface it is bound to and recheck if this new serverItf
        // belongs to a composite.
        walkBindingGraph(clientItf, b, staticBindingAllowed);
      }
    } else {
      // the serverItf node does not belong to a composite,
      // set the "bound-to" decoration.
      setBoundTo(clientItf, serverItf);
    }
  }
}
