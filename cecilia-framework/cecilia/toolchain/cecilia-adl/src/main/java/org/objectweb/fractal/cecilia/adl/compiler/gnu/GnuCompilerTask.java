/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.compiler.gnu;

import static org.objectweb.fractal.cecilia.adl.compiler.ExecutionHelper.exec;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.cecilia.adl.compiler.AbstractCompilationTask;
import org.objectweb.fractal.cecilia.adl.compiler.CompilerErrors;
import org.objectweb.fractal.cecilia.adl.file.SourceFile;

public class GnuCompilerTask extends AbstractCompilationTask {

  protected final String gccCommand;

  // ---------------------------------------------------------------------------
  // Task constructor
  // ---------------------------------------------------------------------------

  /**
   * @param gccCommand the command of the Gnu C compiler to be executed by this
   *            task.
   * @param outputDir the directory into which the compiled file will be placed.
   * @param flags compilation flags.
   */
  public GnuCompilerTask(final String gccCommand, final File outputDir,
      final List<String> flags) {
    super(outputDir, flags);
    this.gccCommand = gccCommand;
  }

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractCompilationTask
  // ---------------------------------------------------------------------------

  @Override
  protected void compile(final SourceFile sourceFile, final File outputFile)
      throws ADLException, InterruptedException {
    // forge command line.
    final List<String> cmd = new ArrayList<String>();
    cmd.add(gccCommand);
    cmd.add("-c");

    if (flags != null) {
      for (String flag : flags) {
        if (flag != null && !flag.equals("")) {
          cmd.add(flag);
        }
      }
    }

    cmd.add("-o");
    cmd.add(outputFile.getPath());
    cmd.add(sourceFile.getFile().getPath());

    // execute command
    final int rValue = exec("gcc: " + outputFile.getPath(), cmd);
    if (rValue != 0) {
      throw new ADLException(CompilerErrors.COMPILER_ERROR, sourceFile
          .getFile().getPath());
    }
  }
}
