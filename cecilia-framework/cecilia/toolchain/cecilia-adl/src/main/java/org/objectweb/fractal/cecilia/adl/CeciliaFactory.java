/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl;

import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;

import org.objectweb.fractal.adl.ADLErrors;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Compiler;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.DefinitionVisitor;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.util.FractalADLLogManager;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.cecilia.adl.file.FileCollectionProvider;
import org.objectweb.fractal.cecilia.adl.file.FileProvider;
import org.objectweb.fractal.task.core.Record;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.TaskInterface;
import org.objectweb.fractal.task.core.control.TaskExecutionException;

/**
 * Implementation of the {@link Factory} interface.
 * 
 * @see org.objectweb.fractal.adl.BasicFactory
 */
public class CeciliaFactory implements Factory, BindingController {

  // --------------------------------------------------------------------------
  // Client interfaces
  // --------------------------------------------------------------------------

  /** The name of the {@link Loader} client interface of this component. */
  public static final String LOADER_ITF_NAME  = "loader";

  /** The name of the {@link Compiler} client interface of this component. */
  public static final String VISITOR_ITF_NAME = "visitor";

  /** The {@link Loader} used by this factory. */
  private Loader             loaderItf;

  /** The {@link ComponentVisitor} used by this factory. */
  private DefinitionVisitor  visitorItf;

  // --------------------------------------------------------------------------
  // Implementation of the Factory interface
  // --------------------------------------------------------------------------

  // Suppress unchecked warning to avoid to change Factory interface
  @SuppressWarnings("unchecked")
  public Object newComponentType(final String name, final Map context)
      throws ADLException {
    throw new UnsupportedOperationException();
  }

  // Suppress unchecked warning to avoid to change Factory interface
  @SuppressWarnings("unchecked")
  public Object newComponent(final String name, final Map context)
      throws ADLException {

    if (FractalADLLogManager.STEP.isLoggable(Level.FINE))
      FractalADLLogManager.STEP.fine("[basic-factory] Load ADL \'" + name
          + "\'");

    // Try loading this definition.
    final Definition d = loaderItf.load(name, context);

    // Do we have to stop when we're done checking the ADL ?
    final Object checkADL = context.get("check-adl");
    if (checkADL != null && ((Boolean) checkADL)) {
      FractalADLLogManager.STEP.info("[basic-factory] ADL \'" + name
          + "\' loaded successfully, it looks correct");
      return d;
    }

    // No, we don't.
    FractalADLLogManager.STEP
        .fine("[basic-factory] ADL loaded successfully, build task graph");

    Component taskGraph;
    try {
      taskGraph = visitorItf.visit(new ArrayList<Node>(), d, context);
    } catch (final TaskException e1) {
      throw new CompilerError(GenericErrors.INTERNAL_ERROR, e1,
          "Task component instantiation problem during the creation of the task graph.");
    }

    FractalADLLogManager.STEP
        .fine("[basic-factory] Task graph built successfully, execute tasks");

    Object result = null;
    try {
      final Record outputRecord = new Record("role:outputFiles, id:%", d);
      for (final Object taskItf : taskGraph.getFcInterfaces()) {
        if (outputRecord.equals(((TaskInterface) taskItf).getFcRecord())) {
          if (taskItf instanceof FileProvider)
            result = ((FileProvider) taskItf).getFile();
          if (taskItf instanceof FileCollectionProvider)
            result = ((FileCollectionProvider) taskItf).getFiles();
        }
      }
    } catch (final TaskExecutionException e) {
      if (e.getCause() instanceof ADLException)
        throw (ADLException) e.getCause();
      else
        throw new ADLException(ADLErrors.TASK_EXECUTION_ERROR, e.getCause());
    } catch (final Exception e) {
      throw new ADLException(ADLErrors.TASK_EXECUTION_ERROR, e);
    }

    FractalADLLogManager.STEP
        .fine("[basic-factory] Tasks executed successfully.");
    return result;
  }

  public void bindFc(final String itfName, final Object value)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (itfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    // XXX check whether it is null or not !!

    try {
      if (itfName.equals(LOADER_ITF_NAME)) {
        this.loaderItf = (Loader) value;
      } else if (itfName.equals(VISITOR_ITF_NAME)) {
        this.visitorItf = (DefinitionVisitor) value;
      } else {
        throw new NoSuchInterfaceException(
            "Unable to find the interface named '" + itfName + "'");
      }
    } catch (final ClassCastException cce) {
      cce.printStackTrace();
      throw new IllegalBindingException("Unmatching interface types");
    }
  }

  public String[] listFc() {
    return new String[]{LOADER_ITF_NAME, VISITOR_ITF_NAME};
  }

  public Object lookupFc(final String itfName) throws NoSuchInterfaceException {

    if (itfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (itfName.equals(LOADER_ITF_NAME)) {
      return this.loaderItf;
    } else if (itfName.equals(VISITOR_ITF_NAME)) {
      return this.visitorItf;
    } else {
      throw new NoSuchInterfaceException("Unable to find the interface named '"
          + itfName + "'");
    }
  }

  public void unbindFc(final String itfName) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException {

    if (itfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (itfName.equals(LOADER_ITF_NAME)) {
      this.loaderItf = null;
    } else if (itfName.equals(VISITOR_ITF_NAME)) {
      this.visitorItf = null;
    } else {
      throw new NoSuchInterfaceException("Unable to find the interface named '"
          + itfName + "'");
    }

  }
}
