/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.composite.c.components;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractInstantiationTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the instantiation of a static component
 * instance for default composite component.
 */
public class ComponentInstantiationVisitor extends AbstractTaskFactoryUser
    implements
      ComponentVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link ComponentContainer} nodes and creates a task that writes the
   * source code containing the component data structure definitions.
   */
  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    return taskFactoryItf.newPrimitiveTask(new ComponentInstantiationTask(),
        container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds source code for the instantiation/initialization of a default
   * composite component. This task provides the produced source code.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "component-instantiation-provider", signature = SourceCodeProvider.class, record = "role:componentInstantiation, id:%", parameters = "componentNode"))
  public static class ComponentInstantiationTask
      extends
        AbstractInstantiationTask {

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /** Source code for binding-controller instantiation. */
    @ClientInterface(name = "bc-instantiation-provider", record = "role:controllerInstantiation, id:%, controller:BC", parameters = "componentNode")
    public SourceCodeProvider bcInstantiationProviderItf;

    /** Source code for binding-controller instantiation. */
    @ClientInterface(name = "cc-instantiation-provider", record = "role:controllerInstantiation, id:%, controller:CC", parameters = "componentNode")
    public SourceCodeProvider ccInstantiationProviderItf;

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractInstantiationTask
    // -------------------------------------------------------------------------
    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter("Composite Instantiation Builder");
      final String instanceName = instanceNameProviderItf.getCInstanceName();

      cw.appendln("// --------------------------------------------------------"
          + "---------------------");
      cw.append("// Declare instance ").append(
          instanceNameProviderItf.getInstanceName()).endl();
      cw.append("//        from type ").append(
          typeNameProviderItf.getTypeName()).endl();
      cw.appendln("// --------------------------------------------------------"
          + "---------------------");

      // Declare the component structure since it may be referenced by the
      // controller structures.
      // This declaration is extern since it is redeclared and initialized
      // latter.
      // This avoid warnings when compiling with -Wredundant-decls.
      cw.append("extern struct __cecilia_defaultCompositeData_t ").append(
          instanceName).append(";").endl();

      // Append the code for the initialization of the controller data
      // structures (content controller must be appended before binding
      // controller).
      cw.appendln(ccInstantiationProviderItf.getSourceCode()).endl();
      cw.appendln(bcInstantiationProviderItf.getSourceCode()).endl();

      // Initialize the component data structure.
      cw.append("struct __cecilia_defaultCompositeData_t ")
          .append(instanceName).append(" = {").endl();
      cw.appendln("// Server interfaces");
      cw.append("{ &__cecilia_defaultComposite_ComponentMeths, ").endl();
      cw.append("&").append(instanceName).append("}, // myreference").endl();
      cw.append("{ &__cecilia_defaultComposite_BindingControllerMeths, ")
          .endl();
      cw.append("&").append(instanceName).append("}, // bc").endl();
      cw.append("{ &__cecilia_defaultComposite_LifeCycleControllerMeths, ")
          .endl();
      cw.append("&").append(instanceName).append("}, // lcc").endl();
      cw.append("{ &__cecilia_defaultComposite_ContentControllerMeths, ")
          .endl();
      cw.append("&").append(instanceName).append("}, // cc").endl();
      cw.appendln("// private data");
      cw.append("sizeof(").append(instanceName).append(
          "_subComponents) / sizeof(").append(instanceName).append(
          "_subComponents[0]), // nbSubComponents").endl();
      cw.append("sizeof(").append(instanceName).append("_bindings) / sizeof(")
          .append(instanceName).append("_bindings[0]), // nbBindings").endl();
      cw.append("0, // started").endl();
      cw.append(instanceName).append("_subComponents,").endl();
      cw.append(instanceName).append("_bindings,").endl();
      cw.appendln("};");

      return cw.toString();
    }
  }

}
