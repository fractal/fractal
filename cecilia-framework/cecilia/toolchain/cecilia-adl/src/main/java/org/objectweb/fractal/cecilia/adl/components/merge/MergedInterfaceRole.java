/***
 * Cecilia
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.components.merge;

/**
 * Specify the role of an interface of a regular Cecilia component being fused
 * inside a <em>merged primitive</em>.
 *
 * @author Alessio Pace
 */
public enum MergedInterfaceRole {
  /**
   * A server interface of a subcomponent, which is exported also as a "server"
   * on the composite being merged.
   */
  SERVER_OUTER,
  /**
   * An interface "server" which is just called internally by other
   * subcomponents of the composite being merged.
   */
  SERVER_INNER,
  /**
   * A client interface of a subcomponent, which is exported also through a
   * client interface of the composite being merged.
   */
  CLIENT_OUTER,
  /**
   * A client interface of a subcomponent which just calls a server interface of
   * one of the subcomponents of the composite being merged.
   */
  CLIENT_INNER
}
