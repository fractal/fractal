/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.components;

import java.util.List;

import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;

/**
 * Utility class to manipulate decoration of {@link Component} node.
 */
public final class ComponentDecorationUtil {

  private ComponentDecorationUtil() {
  }

  /**
   * A decoration set on {@link ComponentContainer} nodes, its value is a
   * {@link List} of the sub component sorted in a correct start order.
   * 
   * @see #setStartOrder(ComponentContainer, List)
   * @see #getStartOrder(ComponentContainer)
   */
  public static final String SUB_COMPONENT_START_ORDER = "sub-component-start-order";

  /**
   * Sets the {@link #SUB_COMPONENT_START_ORDER} decoration to the given
   * {@link ComponentContainer} node.
   * 
   * @param container a composite node.
   * @param startOrder the list of the sub components sorted in a correct start
   *          order.
   * @throws IllegalArgumentException if the <code>startOrder</code> list does
   *           not contain the same number of component as the
   *           <code>container</code>.
   * @see #SUB_COMPONENT_START_ORDER
   */
  public static void setStartOrder(final ComponentContainer container,
      List<Component> startOrder) {
    if (startOrder.size() != container.getComponents().length) {
      throw new IllegalArgumentException("The startOrder list must contains "
          + "the same number of Component as the container");
    }
    ((Node) container).astSetDecoration(SUB_COMPONENT_START_ORDER, startOrder);
  }

  /**
   * Returns the value of the {@link #SUB_COMPONENT_START_ORDER} decoration of
   * the given {@link ComponentContainer} node.
   * 
   * @param container a composite node.
   * @return The value of the {@link #SUB_COMPONENT_START_ORDER} decoration or
   *         <code>null</code> if the given node has no such decoration.
   * @see #SUB_COMPONENT_START_ORDER
   */
  @SuppressWarnings("unchecked")
  public static List<Component> getStartOrder(final ComponentContainer container) {
    return (List<Component>) ((Node) container)
        .astGetDecoration(SUB_COMPONENT_START_ORDER);
  }
}
