/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.adl;

import org.objectweb.fractal.task.core.Executable;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;

/**
 * Abstract task that can be used for definitions. This class implements an
 * executable {@link SourceCodeProvider} interface that uses the code that is
 * prepared by the abstract prepareSourceCode method. This class gives access to
 * the typeName of the visited component.
 */
public abstract class AbstractDefinitionTask
    implements
      Executable,
      SourceCodeProvider {

  /**
   * The name of the component type.
   */
  @ClientInterface(name = "type-name-provider", record = "role:typeNameProvider, id:%", parameters = "componentNode")
  public TypeNameProvider typeNameProviderItf;

  /**
   * This method must prepare the source code during the execution of this task.
   * 
   * @return the prepared source code.
   * @throws Exception is any error occurs.
   */
  protected abstract String processSourceCode() throws Exception;

  // Produced source code.
  protected String sourceCode;

  public void execute() throws Exception {
    sourceCode = processSourceCode();
  }

  public String getSourceCode() {
    if (sourceCode == null) {
      System.err
          .println("Controller task is not executed normally. It will be executed manually.");
      try {
        execute();
      } catch (final Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    return sourceCode;
  }

}
