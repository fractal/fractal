/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.primitive.optC.controllers;

import static org.objectweb.fractal.cecilia.adl.controllers.ControllerDecorationUtil.hasDefaultBindingController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.interfaces.AbstractClientInterfaceVisitor;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.TaskFactory;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the factory implementation code pieces for
 * cloning binding controller data structure.
 */
public class BCFactoryVisitor extends AbstractClientInterfaceVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractClientInterfaceVisitor
  // ---------------------------------------------------------------------------

  @Override
  protected Component createTask(final ComponentContainer container,
      final List<TypeInterface> clientInterfaces) throws ADLException,
      TaskException {
    if (clientInterfaces.isEmpty()
        || !hasDefaultBindingController((InterfaceContainer) container)) {
       return null;
    }

    final Collection<Component> tasks = new ArrayList<Component>(3);
    tasks.add(taskFactoryItf.newPrimitiveTask(
        new BCFactoryInstantiateDeclarationTask(), container));
    tasks.add(taskFactoryItf.newPrimitiveTask(
        new BCFactoryInstantiateSizeofTask(), container));
    tasks.add(taskFactoryItf.newPrimitiveTask(new BCFactoryInstantiateTask(
        clientInterfaces), container));

    return taskFactoryItf.newCompositeTask(tasks, TaskFactory.EXPORT_ALL, null);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds the source code that declare the new binding controller data
   * structure.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "bc-factory-instantiate-declaration", signature = SourceCodeProvider.class, record = "role:factoryInstantiateDeclaration, id:%, codePiece:BC", parameters = "componentNode"))
  public static class BCFactoryInstantiateDeclarationTask
      extends
        AbstractDefinitionTask {

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final String definitionName = typeNameProviderItf.getCTypeName();
      final CodeWriter cw = new CodeWriter();
      cw.append("struct ").append(definitionName).appendln(
          "_BC_data_type * newComp_BC_data;");
      return cw.toString();
    }
  }

  /**
   * Builds the source code that declare the new binding controller data
   * structure.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "bc-factory-instantiate-sizeof", signature = SourceCodeProvider.class, record = "role:factoryInstantiateSizeof, id:%, codePiece:BC", parameters = "componentNode"))
  public static class BCFactoryInstantiateSizeofTask
      extends
        AbstractDefinitionTask {

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final String definitionName = typeNameProviderItf.getCTypeName();
      final CodeWriter cw = new CodeWriter();
      cw.append("+ sizeof(struct ").append(definitionName)
          .append("_BC_data_type)");
      return cw.toString();
    }
  }

  /**
   * Builds the source code that copy the binding controller data structure.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "bc-factory-instantiate", signature = SourceCodeProvider.class, record = "role:factoryInstantiatePiece, id:%, codePiece:BC", parameters = "componentNode"))
  public static class BCFactoryInstantiateTask extends AbstractDefinitionTask {

    // The client interfaces array
    final List<TypeInterface> clientInterfaces;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param clientInterfaces the client interface nodes.
     */
    public BCFactoryInstantiateTask(final List<TypeInterface> clientInterfaces) {
      this.clientInterfaces = clientInterfaces;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final String definitionName = typeNameProviderItf.getCTypeName();
      final CodeWriter cw = new CodeWriter("Primitive Binding Controller");
      cw.appendln("{");
      cw.append("newComp_BC_data = (struct ").append(
          definitionName).appendln("_BC_data_type *)_ptr;");
      cw.append("struct ").append(definitionName).append(
          "_BC_data_type * original_BC_data = ").append("(struct ").append(
          definitionName).append("_BC_data_type *) ").append(
          "originalComp->type.exported.binding_controller.selfdata;").endl();

      cw.append("memcpy(newComp_BC_data, original_BC_data, sizeof(struct ")
          .append(definitionName).append("_BC_data_type));").endl();

      for (final TypeInterface element : clientInterfaces) {
        final String escapedItfName = element.getName().replace('-', '_');
        cw.append("newComp_BC_data->imported[0].itf_").append(escapedItfName)
            .append(" = &(newComp->type.imported.").append(escapedItfName)
            .appendln(");");
      }
      cw.append("_ptr += sizeof(struct ").append(definitionName)
          .appendln("_BC_data_type);");
      cw.appendln("}");
      return cw.toString();
    }
  }
}
