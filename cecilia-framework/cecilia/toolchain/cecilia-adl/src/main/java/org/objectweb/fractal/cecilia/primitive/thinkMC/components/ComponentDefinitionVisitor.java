/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.primitive.thinkMC.components;

import static org.objectweb.fractal.api.type.TypeFactory.OPTIONAL;
import static org.objectweb.fractal.cecilia.adl.SourceCodeHelper.appendSortedSourceCodes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the definition of the component data structure
 * of the component for <code>ThinkMC</code> dialect.
 */
public class ComponentDefinitionVisitor extends AbstractTaskFactoryUser
    implements
      ComponentVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link ComponentContainer} nodes and creates a task that writes the
   * source code containing the definition of the component data structure.
   */
  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    return taskFactoryItf.newPrimitiveTask(new ComponentDefinitionTask(),
        container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds source code for the definition of the component data structure for
   * the given component node. This task provides the produced source code. It
   * uses the source code provided for client/server interfaces, controllers,
   * and the attributes of the component.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "component-definition-provider", signature = SourceCodeProvider.class, record = "role:componentDefinition, id:%", parameters = "componentNode"))
  public static class ComponentDefinitionTask extends AbstractDefinitionTask {

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /** Source code to be included for interface definitions (optional). */
    @ClientInterface(name = "interface-definition-provider", contingency = OPTIONAL, record = "role:interfaceDefinition, id:%", parameters = "componentNode")
    public SourceCodeProvider                    interfaceDefinitionsProviderItf;

    /** Source code to be included for attribute definitions (optional). */
    @ClientInterface(name = "attribute-definition-provider", contingency = OPTIONAL, record = "role:attributeDefinition, id:%", parameters = "componentNode")
    public SourceCodeProvider                    attributeDefintionsProviderItf;

    /** Source codes to be included for controllers definition. */
    @ClientInterface(name = "controller-definition-provider", signature = SourceCodeProvider.class, record = "role:controllerDefinition, id:%", parameters = "componentNode")
    public final Map<String, SourceCodeProvider> controllerDefintionsProviderItf = new HashMap<String, SourceCodeProvider>();

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------
    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter("Primitive Definition Builder");
      final String componentCName = typeNameProviderItf.getCTypeName();

      if (interfaceDefinitionsProviderItf != null)
        cw.appendln(interfaceDefinitionsProviderItf.getSourceCode()).endl();

      if (attributeDefintionsProviderItf != null)
        cw.appendln(attributeDefintionsProviderItf.getSourceCode()).endl();

      cw.append("#define __CECILIA__DEFINE_COMPONENT_TYPE \\").endl();

      cw.append("struct ").append(componentCName).append("_t { \\").endl();

      if (interfaceDefinitionsProviderItf != null)
        cw.append("struct ").append(componentCName).append("_type type; \\")
            .endl();

      if (attributeDefintionsProviderItf != null)
        cw.append(componentCName).append("_attributes_t attributes; \\").endl();

      cw.append("  struct ").append(componentCName).append(
          "_instancedata data ; \\").endl();
      cw.append("} ;").endl();

      cw.endl();

      // To ensure reproducible code generation, append controller definition
      // codes in their alphabetic order.
      appendSortedSourceCodes(cw, controllerDefintionsProviderItf.values());

      return cw.toString();
    }
  }

}
