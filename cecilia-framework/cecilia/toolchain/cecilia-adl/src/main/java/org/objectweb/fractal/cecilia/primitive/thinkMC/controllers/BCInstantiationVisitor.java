/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.primitive.thinkMC.controllers;

import static org.objectweb.fractal.cecilia.adl.controllers.ControllerDecorationUtil.hasDefaultBindingController;

import java.util.List;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractInstantiationTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.interfaces.AbstractClientInterfaceVisitor;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the instantiation of the binding controller
 * data structure of the component for <code>ThinkMC</code> dialect.
 */
public class BCInstantiationVisitor extends AbstractClientInterfaceVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractClientInterfaceVisitor
  // ---------------------------------------------------------------------------

  /**
   * Creates a task that writes the source code containing the binding
   * controller definition.
   */
  @Override
  protected Component createTask(final ComponentContainer container,
      final List<TypeInterface> clientInterfaces) throws ADLException,
      TaskException {
    if (clientInterfaces.isEmpty()
        || !hasDefaultBindingController((InterfaceContainer) container)) {
       return null;
    }

    return taskFactoryItf.newPrimitiveTask(new BCPrimitiveInstantiationTask(
        clientInterfaces), container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds source code for the instantiation of the binding controller data
   * structure for the given component node. This task provides the produced
   * source code.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "bc-instantiation-provider", signature = SourceCodeProvider.class, record = "role:controllerInstantiation, id:%, controller:BC", parameters = "componentNode"))
  public static class BCPrimitiveInstantiationTask
      extends
        AbstractInstantiationTask {
    // The client interfaces array
    final List<TypeInterface> clientInterfaces;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param clientInterfaces the client interface nodes.
     */
    public BCPrimitiveInstantiationTask(
        final List<TypeInterface> clientInterfaces) {
      this.clientInterfaces = clientInterfaces;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractInstantiationTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter(
          "Binding Controller Data Instantiation");
      cw.append("static struct ").append(typeNameProviderItf.getCTypeName())
          .append("_BC_data_type ").append(
              instanceNameProviderItf.getCInstanceName())
          .append("_BC_data = {").endl();
      cw.append(clientInterfaces.size()).append(", {").endl();
      for (final TypeInterface itf : clientInterfaces) {
        cw.append("{\"").append(itf.getName()).append("\", & ").append(
            instanceNameProviderItf.getCInstanceName()).append(
            ".type.imported.").append(itf.getName().replace('-', '_')).append(
            "},").endl();
      }
      cw.append("}};").endl();

      return cw.toString();
    }
  }
}
