/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.primitive.optC.attributes;

import org.objectweb.fractal.adl.attributes.Attributes;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.attributes.AbstractAttributeVisitor;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the definition of the attribute data structures
 * of the component for <code>ThinkMC</code> dialect.
 */
public class AttributeDefinitionVisitor extends AbstractAttributeVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractAttributeVisitor
  // ---------------------------------------------------------------------------

  @Override
  protected Component createTask(final ComponentContainer container,
      final Attributes attributes) throws TaskException {
    return taskFactoryItf.newPrimitiveTask(new AttributeDefinitionTask(
        attributes.getSignature()), container,
        ((IDLDefinitionContainer) attributes).getIDLDefinition());
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds definition source code for attribute data structure for the given
   * component node. This task provides the produced source code.
   */
  @TaskParameters({"componentNode", "recordDefinitionNode"})
  @ServerInterfaces(@ServerInterface(name = "source-code-provider", signature = SourceCodeProvider.class, record = "role:attributeDefinition, id:%", parameters = "componentNode"))
  public static class AttributeDefinitionTask extends AbstractDefinitionTask {

    protected final String    attributeSignature;

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /**
     * Source code to be included to access the record definition header files
     * (idl.h) for the attributes' signature of the componentNode.
     */
    @ClientInterface(name = "attribute-header-provider", record = "role:cRecordDefinition, id:%", parameters = "recordDefinitionNode")
    public SourceCodeProvider cInterfaceDefinitionItf;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param attributeSignature the signature of the attributes of the
     *            component.
     */
    public AttributeDefinitionTask(final String attributeSignature) {
      this.attributeSignature = attributeSignature;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter(
          "Attribute Definition Builder, include");
      cw.appendln(cInterfaceDefinitionItf.getSourceCode());

      cw.append("typedef struct ").append(attributeSignature.replace('.', '_'))
          .append(' ').append(typeNameProviderItf.getCTypeName()).append(
              "_attributes_t;");

      return cw.toString();
    }
  }
}
