/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.primitive.thinkMC.controllers;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.attributes.Attribute;
import org.objectweb.fractal.adl.attributes.Attributes;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractInstantiationTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.attributes.AbstractAttributeVisitor;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.idl.ast.Field;
import org.objectweb.fractal.cecilia.adl.idl.ast.FieldContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.Type;
import org.objectweb.fractal.cecilia.adl.idl.util.CUtil;
import org.objectweb.fractal.cecilia.adl.idl.util.Util;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the instantiation of the attribute controller
 * data structure of the component for <code>ThinkMC</code> dialect.
 */
public class ACInstantiationVisitor extends AbstractAttributeVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractAttributeVisitor
  // ---------------------------------------------------------------------------

  @Override
  protected Component createTask(final ComponentContainer container,
      final Attributes attributes) throws TaskException {

    return taskFactoryItf.newPrimitiveTask(new ACInstantiationTask(attributes),
        container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds source code for the instantiation of the attribute controller data
   * structure for the given component node. This task provides the produced
   * source code.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "ac-instance-provider", signature = SourceCodeProvider.class, record = "role:controllerInstantiation, id:%, controller:AC", parameters = "componentNode"))
  public static class ACInstantiationTask extends AbstractInstantiationTask {
    // Array of record fields
    final Field[]               recordFields;

    // Array of attribute fields
    protected final Attribute[] fields;

    // AST node representing the attributes of the component
    Attributes                  attributes;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param attributes the attributes to initialize.
     */
    public ACInstantiationTask(final Attributes attributes) {
      this.attributes = attributes;
      this.fields = attributes.getAttributes();
      this.recordFields = ((FieldContainer) ((IDLDefinitionContainer) attributes)
          .getIDLDefinition()).getFields();
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractInstantiationTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      boolean hasConstAttributes = false;
      for (final Field f : recordFields) {
        if (f.getQualifier().equals(Field.CONST)) {
          hasConstAttributes = true;
          break;
        }
      }

      final CodeWriter cw = new CodeWriter("Primitive Attribute Controller Instantiation");
      cw.append("struct ").append(typeNameProviderItf.getCTypeName()).append(
          "_AC_data_type ").append(instanceNameProviderItf.getCInstanceName())
          .append("_AC_data = {").endl();

      cw.append(fields.length).append(", {").endl();

      for (final Attribute attr : fields) {
        Field field = null;
        Type attType = null;
        for (final Field fieldDefinition : recordFields) {
          if (attr.getName().equals(fieldDefinition.getName())) {
            field = fieldDefinition;
            attType = Util.getContainedType(fieldDefinition);
            break;
          }
        }
        if (attType == null || field == null) {
          throw new ADLException(
              "The attribute record does not define a field called '"
                  + attr.getName() + "'", attributes);
        }

        cw.append("{\"").append(attr.getName()).append("\", &").append(
            instanceNameProviderItf.getCInstanceName()).append(".attributes.")
            .append(attr.getName()).append(", sizeof(").append(
                CUtil.buildDeclarationType(attType)).append(")");
        if (hasConstAttributes) {
          cw.append(", ");
          if (field.getQualifier().equals(Field.CONST))
            cw.append("1 ");
          else
            cw.append("0 ");
        }
        cw.append("},").endl();
      }
      cw.append("}};").endl(); // terminate attribute data static declaration

      return cw.toString();
    }

    protected List<Field> getConstFields(final Field[] fields) {
      final List<Field> result = new ArrayList<Field>();
      for (final Field f : fields) {
        if (f.getQualifier().equals(Field.CONST)) {
          result.add(f);
        }
      }
      return result;
    }

  }
}
