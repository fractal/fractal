/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.composite.c.controllers;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;
import static org.objectweb.fractal.adl.types.TypeInterfaceUtil.isCollection;
import static org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil.getServerInterface;
import static org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil.isStaticallyBound;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.bindings.Binding;
import org.objectweb.fractal.adl.bindings.BindingContainer;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractInstantiationTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the instantiation of the binding controller
 * data structure of a default composite component.
 */
public class BCInstantiationVisitor extends AbstractTaskFactoryUser
    implements
      ComponentVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link ComponentContainer} nodes and creates a task that writes the
   * source code containing the initialization of the binding controller data
   * structure.
   */
  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    final BindingContainer bindingContainer = castNodeError(container,
        BindingContainer.class);
    return createTask(container, bindingContainer.getBindings());
  }

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  protected Component createTask(final ComponentContainer container,
      final Binding[] bindings) throws TaskException {
    return taskFactoryItf.newPrimitiveTask(new BCInstantiationTask(bindings),
        container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds source code for the instantiation of the binding controller data
   * structure for the given component node. This task provides the produced
   * source code.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "bc-instantiation-provider", signature = SourceCodeProvider.class, record = "role:controllerInstantiation, id:%, controller:BC", parameters = "componentNode"))
  public static class BCInstantiationTask extends AbstractInstantiationTask {
    protected final Binding[] bindings;

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param bindings the binding nodes.
     */
    public BCInstantiationTask(final Binding[] bindings) {
      this.bindings = bindings;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractInstantiationTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter(
          "Binding Controller Data Instantiation");
      cw.append("struct BindingDesc_t ").append(
          instanceNameProviderItf.getCInstanceName()).append("_bindings[] = {")
          .endl();
      for (final Binding binding : bindings) {
        final String from = binding.getFrom();
        int index = from.indexOf('.');
        final String clientComp = from.substring(0, index);
        final String clientItf = from.substring(index + 1);

        final String to = binding.getTo();
        index = to.indexOf('.');
        final String serverComp = to.substring(0, index);
        final String serverItf = to.substring(index + 1);

        cw.append("{").endl();
        if (serverComp.equals("this"))
          cw.append("COMPOSITE_IDX");
        else
          cw.append("IDX_").append(serverComp.replace('-', '_').toUpperCase());
        cw.append(", // server").endl();

        if (clientComp.equals("this"))
          cw.append("COMPOSITE_IDX");
        else
          cw.append("IDX_").append(clientComp.replace('-', '_').toUpperCase());
        cw.append(", // client").endl();

        if (isStaticallyBound(binding)) {
          cw.append("BINDING_BOUND");
        } else {
          cw.append("BINDING_UNBOUND");
        }

        if (serverComp.equals("this")
            && isCollection(getServerInterface(binding)))
          cw.append(" | EXP_COL_BINDING");
        else
          cw.append(" | STANDARD_BINDING");

        cw.append(", // state").endl();

        cw.append("\"").append(serverItf).append("\", // serverName").endl();
        cw.append("\"").append(clientItf).append("\" // clientName").endl();
        cw.append("},").endl();
      }
      cw.append("};").endl();

      return cw.toString();
    }
  }
}
