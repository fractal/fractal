/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.controllers;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.error.NodeErrorLocator;
import org.objectweb.fractal.adl.implementations.Controller;
import org.objectweb.fractal.adl.implementations.ControllerContainer;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.cecilia.adl.components.PrimitiveChecker;
import org.objectweb.fractal.cecilia.adl.implementations.ExtendedImplementation;
import org.objectweb.fractal.cecilia.adl.plugin.AbstractPluginUser;
import org.objectweb.fractal.cecilia.adl.plugin.PluginErrors;
import org.objectweb.fractal.cecilia.adl.plugin.PluginException;

/**
 * Add the controllers description in the AST.
 */
public class ControllerCheckerSelector extends AbstractPluginUser
    implements
      PrimitiveChecker {

  // ---------------------------------------------------------------------------
  // Client interfaces
  // ---------------------------------------------------------------------------

  /** The name of the {@link PrimitiveChecker} client collection interface. */
  public static final String CONTROLLER_CHECKERS = "controller-checker";

  /** Default constructor. */
  public ControllerCheckerSelector() {
    super(CONTROLLER_CHECKERS);
  }

  // ---------------------------------------------------------------------------
  // Implementation of the PrimitiveChecker interface
  // ---------------------------------------------------------------------------

  public void beforeSubComponentCheck(final List<ComponentContainer> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    getCheckerPlugin(container, context).beforeSubComponentCheck(path,
        container, context);
  }

  public void afterSubComponentCheck(final List<ComponentContainer> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    getCheckerPlugin(container, context).afterSubComponentCheck(path,
        container, context);
  }

  protected PrimitiveChecker getCheckerPlugin(
      final ComponentContainer container, final Map<Object, Object> context)
      throws CompilerError, ADLException {
    final Implementation implementation = castNodeError(container,
        ImplementationContainer.class).getImplementation();

    final ExtendedImplementation extImplementation;
    if (implementation == null)
      extImplementation = null;
    else
      extImplementation = castNodeError(implementation,
          ExtendedImplementation.class);

    final Controller controller = castNodeError(container,
        ControllerContainer.class).getController();
    if (controller == null)
      throw new CompilerError(GenericErrors.INTERNAL_ERROR,
          new NodeErrorLocator(container), "Can't find controller node in AST");
    final ExtendedController extController = castNodeError(controller,
        ExtendedController.class);

    String pluginName;
    if (extImplementation == null)
      pluginName = "composite.";
    else
      pluginName = "primitive." + extImplementation.getLanguage() + ".";

    pluginName += extController.getLanguage().substring(0, 1).toUpperCase()
        + extController.getLanguage().substring(1)
        + extController.getDescriptor().substring(0, 1).toUpperCase()
        + extController.getDescriptor().substring(1);

    PrimitiveChecker controllerChecker;
    try {
      controllerChecker = getPlugin(pluginName, context, PrimitiveChecker.class);
    } catch (final PluginException e) {
      if (e.getError().getTemplate() == PluginErrors.PLUGIN_NOT_FOUND) {
        throw new ADLException(
            ControllerErrors.CONTROLLER_CHECKER_PLUGIN_NOT_FOUND, pluginName);
      } else {
        throw new CompilerError(GenericErrors.INTERNAL_ERROR, e,
            "Can't load checker plugin for controller \"" + pluginName + "\"");
      }
    }
    return controllerChecker;
  }
}
