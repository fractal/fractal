/***
 * Cecilia ADL Compiler
 * Copyright (C) 2005-2008 STMicroelectronics
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.components.merge;

import org.objectweb.fractal.adl.types.TypeInterface;

/**
 * An interface which is translatable to a new name at code generation time: for
 * example if a piece of functional code was coded to make use of the
 * CLIENT(clientItf, methName) macro, but due to fusion the CLIENT macro
 * expansion should be converted to something else, the Include should have the
 * correct redefinitions.
 *
 * @author Alessio Pace
 */
public interface MergedInterface extends TypeInterface {

// String getTranslatedName();
//
// void setTranslatedName(String translatedName);
//
// void setMergedInterfaceRole(MergedInterfaceRole role);
//
// MergedInterfaceRole getMergedInterfaceRole();
}
