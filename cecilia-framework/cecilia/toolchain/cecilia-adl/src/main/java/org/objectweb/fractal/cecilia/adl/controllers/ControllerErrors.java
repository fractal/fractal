/**
 * Cecilia ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.controllers;

import static org.objectweb.fractal.adl.error.ErrorTemplateValidator.validErrorTemplate;

import org.objectweb.fractal.adl.error.ErrorTemplate;

/** {@link ErrorTemplate} group for the controller package. */
public enum ControllerErrors implements ErrorTemplate {

  /** */
  INVALID_CONTROLLER_ITF_ROLE(
      "Invalid \"%s\" interface specified in the ADL. The role must by \"%s\"",
      "itfName", "expected"),

  /** */
  INVALID_CONTROLLER_ITF_CONTINGENCY(
      "Invalid \"%s\" interface specified in the ADL. The contingency must by \"%s\"",
      "itfName", "expected"),

  /** */
  INVALID_CONTROLLER_ITF_CARDINALITY(
      "Invalid \"%s\" interface specified in the ADL. The cardinality must by \"%s\"",
      "itfName", "expected"),

  /** */
  CONTROLLER_VISITOR_PLUGIN_NOT_FOUND(

      "Can't find visitor plugin for controller \"%s\". Checks controller 'desc' and 'language' attributes.",
      "controllerdesc"),

  /** */
  CONTROLLER_CHECKER_PLUGIN_NOT_FOUND(
      "Can't find checker plugin for controller \"%s\". Checks controller 'desc' and 'language' attributes.",
      "controllerdesc"),

  ;

  /** The groupId of ErrorTemplates defined in this enumeration. */
  public static final String GROUP_ID = "CTR";

  private int                id;
  private String             format;

  private ControllerErrors(final String format, final Object... args) {
    this.id = ordinal();
    this.format = format;

    assert validErrorTemplate(this, args);
  }

  public int getErrorId() {
    return id;
  }

  public String getGroupId() {
    return GROUP_ID;
  }

  public String getFormatedMessage(final Object... args) {
    return String.format(format, args);
  }

  public String getFormat() {
    return format;
  }
}
