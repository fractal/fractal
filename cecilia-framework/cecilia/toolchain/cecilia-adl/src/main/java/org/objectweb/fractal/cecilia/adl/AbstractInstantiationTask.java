/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.adl;

import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;

/**
 * Abstract task that can be used for instantiations. This class implements an
 * executable {@link SourceCodeProvider} interface that uses the code that is
 * prepared by the abstract prepareSourceCode method. This class gives access to
 * the typeName and to the instanceName of the visited component.
 */
public abstract class AbstractInstantiationTask extends AbstractDefinitionTask {

  /**
   * The client interface that can be used to retrieve the name of the component
   * instance.
   */
  @ClientInterface(name = "instance-name-provider", record = "role:instanceNameProvider, id:%", parameters = "componentNode")
  public InstanceNameProvider instanceNameProviderItf;

}
