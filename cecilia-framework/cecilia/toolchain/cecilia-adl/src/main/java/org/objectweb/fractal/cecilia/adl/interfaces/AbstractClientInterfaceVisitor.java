/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.interfaces;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;
import static org.objectweb.fractal.adl.types.TypeInterfaceUtil.isClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;

/**
 * Abstract visitor component that creates a task that depends on the client
 * interfaces of the component (if any).
 */
public abstract class AbstractClientInterfaceVisitor
    extends
      AbstractTaskFactoryUser implements ComponentVisitor {

  /**
   * Creates the task that is returned by this visitor.
   * 
   * @param container the component that is visited.
   * @param clientInterfaces the client interfaces of the component (contains at
   *            least one element).
   * @return the created task.
   * @throws InstantiationException if the instantiation of the task fails.
   * @throws ADLException if the instantiation of the task fails.
   */
  protected abstract Component createTask(ComponentContainer container,
      List<TypeInterface> clientInterfaces) throws ADLException, TaskException;

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link ComponentContainer} nodes and call
   * {@link #createTask(ComponentContainer, Interface[]) createTask} if the
   * given component contains client {@link Interface} sub-nodes.
   */
  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    final List<TypeInterface> clientInterfaces = new ArrayList<TypeInterface>();
    if (container instanceof InterfaceContainer) {
      for (final Interface itf : ((InterfaceContainer) container)
          .getInterfaces()) {
        if (isClient(itf))
          clientInterfaces.add(castNodeError(itf, TypeInterface.class));
      }
    }
    return createTask(container, clientInterfaces);
  }

}