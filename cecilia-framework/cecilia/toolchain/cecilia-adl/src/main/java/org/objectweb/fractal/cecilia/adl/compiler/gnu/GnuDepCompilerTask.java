/***
 * Cecilia ADL Compiler
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.compiler.gnu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.cecilia.adl.file.SourceFile;
import org.objectweb.fractal.cecilia.adl.directives.DirectiveHelper;

public class GnuDepCompilerTask extends GnuCompilerTask {

  /** The default file extension of dependency files. */
  protected static final String DEPENDENCY_EXTENSION = ".d";

  // ---------------------------------------------------------------------------
  // Task constructor
  // ---------------------------------------------------------------------------

  /**
   * @param gccCommand the command of the Gnu C compiler to be executed by this
   *            task.
   * @param outputDir the directory into which the compiled file will be placed.
   * @param flags compilation flags.
   */
  public GnuDepCompilerTask(final String gccCommand, final File outputDir,
      final List<String> flags) {
    super(gccCommand, outputDir, flags);
  }

  // ---------------------------------------------------------------------------
  // Overridden methods
  // ---------------------------------------------------------------------------

  @Override
  protected void compile(final SourceFile sourceFile, final File outputFile)
      throws ADLException, InterruptedException {
    final File depFile = getDependencyFile(outputFile);
    List<String> newFlags = new ArrayList<String>();

    if (needRecompile(outputFile, depFile)) {
      newFlags.add("-MMD");
      newFlags.add("-MF");
      newFlags.add(depFile.toString());
      newFlags.addAll(flags);
      flags = newFlags;
      super.compile(sourceFile, outputFile);
    }
  }

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  protected File getDependencyFile(final File outputFile) {
    String name = outputFile.getName();
    name = name.substring(0, name.lastIndexOf('.')) + DEPENDENCY_EXTENSION;
    return new File(outputFile.getParent(), name);
  }

  protected boolean needRecompile(final File outputFile,
      final File dependencyFile) {

    if (!outputFile.exists()) {
      if (depLogger.isLoggable(Level.FINE))
        depLogger.fine("Output file '" + outputFile
            + "' does not exist, compile it.");
      return true;
    }
    final long outputTimestamp = outputFile.lastModified();
    if (outputTimestamp == 0) {
      if (depLogger.isLoggable(Level.WARNING))
        depLogger.warning("Unable to determine timestamp of file '"
            + outputFile + "', recompile.");
      return true;
    }

    if (!dependencyFile.exists()) {
      if (depLogger.isLoggable(Level.FINE))
        depLogger.fine("Dependency file of '" + outputFile
            + "' does not exist, recompile.");
      return true;
    }

    final Collection<File> depFiles = getDependencies(dependencyFile,
        outputFile);

    // if depFiles is null (i.e. the dependencyFile is invalid), recompile.
    if (depFiles == null) return true;

    for (final File depfile : depFiles) {
      if (!depfile.exists()) {
        if (ioLogger.isLoggable(Level.FINE))
          ioLogger.fine("Warning input file '" + depfile + " does not exist.");
      } else if (depfile.lastModified() > outputTimestamp) {
        if (depLogger.isLoggable(Level.FINE))
          depLogger.fine("Input file '" + depfile
              + " is more recent than output file '" + outputFile
              + "', recompile.");
        return true;
      }
    }

    if (depLogger.isLoggable(Level.FINE))
      depLogger.fine("Output file '" + outputFile
          + "' is up to date, do not recompile.");
    return false;
  }

  protected Collection<File> getDependencies(final File dependencyFile,
      final File outputFile) {
    if (depLogger.isLoggable(Level.FINEST)) {
      depLogger.finest("Parsing dependency file \"" + dependencyFile + "\"...");
    }
    final List<Rule> rules = parseRules(dependencyFile);
    if (depLogger.isLoggable(Level.FINEST)) {
      depLogger.finest("Parsed rules=" + rules);
    }

    for (final Rule rule : rules) {
      final File targetFile = new File(rule.target);
      if (depLogger.isLoggable(Level.FINEST)) {
        depLogger.finest("rule target file is \"" + targetFile + "\".");
      }

      // Checks if the rule target correspond to the outputFile. Note that some
      // old compilers may specify only the name of the output file without its
      // path.
      if (!outputFile.equals(targetFile)
          && !new File(outputFile.getName()).equals(targetFile)) {
        if (depLogger.isLoggable(Level.FINEST)) {
          depLogger
              .finest("target file does not match \"" + outputFile + "\".");
        }
        continue;
      }

      final List<File> dependentFiles = new ArrayList<File>();
      for (final String dependency : rule.dependencies) {
        if (dependency.length() == 0) continue;
        dependentFiles.add(new File(dependency));
      }
      return dependentFiles;
    }

    depLogger.warning("Invalid dependency file \"" + dependencyFile
        + "\". Can't find a rule for file \"" + outputFile + "\"");
    return null;
  }

  protected List<Rule> parseRules(final File dependencyFile) {
    final List<Rule> rules = new ArrayList<Rule>();
    try {
      final BufferedReader reader = new BufferedReader(new FileReader(
          dependencyFile));
      String line = null, rule = "";
      while ((line = reader.readLine()) != null) {
        if (line.endsWith("\\")) {
          rule += line.substring(0, line.length() - 1);
        } else {
          rule += line.trim();

          // end of rule, start a new one
          if (rule.length() > 0) {
          rules.add(new Rule(rule));
            rule = "";
          }
        }
      }
      if (rule.length() > 0) rules.add(new Rule(rule));
    } catch (final IOException e) {
      depLogger.log(Level.WARNING, "An error occurs while reading \""
          + dependencyFile + "\".", e);
    }
    return rules;
  }

  static final class Rule {
    String       target;
    List<String> dependencies;

    Rule(final String rule) {
      final String[] parts = rule.split(":\\s+");
      if (parts.length != 2) {
        throw new IllegalArgumentException("Can't find rule target");
      }
      target = parts[0].trim();
      // Unescape spaces.
      target = DirectiveHelper.splitOptionString(target).get(0);
      dependencies = new ArrayList<String>();
      // Split dependency part.
      for (final String dependency : DirectiveHelper.splitOptionString(parts[1])) {
        if (dependency.length() > 0) dependencies.add(dependency);
      }
    }
  }
}
