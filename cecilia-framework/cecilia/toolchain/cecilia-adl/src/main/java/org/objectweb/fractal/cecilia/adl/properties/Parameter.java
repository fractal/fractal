/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006 FranceTelecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: J. Polakovic
 */

package org.objectweb.fractal.cecilia.adl.properties;

/**
 * AST node interface for <code>parameter</code> elements.
 */
public interface Parameter {

  /**
   * Sets the name of the parameter.
   * 
   * @param name the name of the parameter.
   */
  void setName(String name);

  /**
   * Returns the name of the parameter.
   * 
   * @return the name of the parameter.
   */
  String getName();

  /**
   * Sets the value of the parameter.
   * 
   * @param value the value of the parameter.
   */
  void setValue(String value);

  /**
   * Returns the value of the parameter.
   * 
   * @return the value of the parameter.
   */
  String getValue();
}