/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.implementations;

import org.objectweb.fractal.adl.Node;

/**
 * Utility class to manipulate decorations of node containing implementation.
 */
public final class ImplementationDecorationUtil {

  private ImplementationDecorationUtil() {
  }

  /**
   * A decoration set on node that refers to an implementation. The value of
   * this decoration depends on the
   * {@link org.objectweb.fractal.adl.implementations.ImplementationCodeLoader}
   * used to load the implementation.
   * 
   * @see #setCode(Node, Object)
   * @see #getCode(Node)
   */
  public static final String CODE_DECORATION = "code";

  /**
   * Sets the {@link #CODE_DECORATION} decoration to the given node with the
   * given value.
   * 
   * @param node the node to which the decoration is set.
   * @param code the value of the decoration.
   * @see #CODE_DECORATION
   */
  public static void setCode(final Node node, final Object code) {
    node.astSetDecoration(CODE_DECORATION, code);
  }

  /**
   * Returns the value of the {@link #CODE_DECORATION} decoration of the given
   * node.
   * 
   * @param node the node to which the decoration is set.
   * @return the value of the {@link #CODE_DECORATION} decoration of the given
   *         node.
   * @see #CODE_DECORATION
   */
  public static Object getCode(final Node node) {
    return node.astGetDecoration(CODE_DECORATION);
  }
}
