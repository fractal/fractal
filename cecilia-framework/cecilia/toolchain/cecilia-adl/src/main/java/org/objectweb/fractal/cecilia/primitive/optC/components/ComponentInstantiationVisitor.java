/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq, Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.primitive.optC.components;

import static org.objectweb.fractal.api.type.TypeFactory.OPTIONAL;
import static org.objectweb.fractal.cecilia.adl.SourceCodeHelper.appendSortedSourceCodes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.ComponentVisitor;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractInstantiationTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.task.core.AbstractTaskFactoryUser;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the instantiation of a static component
 * instance for component written in the <code>ThinkMC</code> dialect.
 */
public class ComponentInstantiationVisitor extends AbstractTaskFactoryUser
    implements
      ComponentVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of the ComponentVisitor interface
  // ---------------------------------------------------------------------------

  /**
   * Visits {@link ComponentContainer} nodes and creates a task that writes the
   * source code containing the component data structure definitions.
   */
  public Component visit(final List<Node> path,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException, TaskException {
    return taskFactoryItf.newPrimitiveTask(new ComponentInstantiationTask(),
        container);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds source code for the instantiation/initialization of the component
   * data structure for the given component node. This task provides the
   * produced source code. It uses the source code provided for
   * imported/exported interfaces, controllers, and the attributes of the
   * component.
   */
  @TaskParameters("componentNode")
  @ServerInterfaces(@ServerInterface(name = "component-instantiation-provider", signature = SourceCodeProvider.class, record = "role:componentInstantiation, id:%", parameters = "componentNode"))
  public static class ComponentInstantiationTask
      extends
        AbstractInstantiationTask {

    // -------------------------------------------------------------------------
    // Task client interfaces
    // -------------------------------------------------------------------------

    /** Source code for the initialization of interface instances. */
    @ClientInterface(name = "interface-instance-initialization-provider", contingency = OPTIONAL, record = "role:interfaceInstantiation, id:%, codePiece:initialization", parameters = "componentNode")
    public SourceCodeProvider                    interfaceInstancesProviderInitializationItf;

    /** Source code containing includes necessary for the interface instances. */
    @ClientInterface(name = "interface-instance-include-provider", contingency = OPTIONAL, record = "role:interfaceInstantiation, id:%, codePiece:include", parameters = "componentNode")
    public SourceCodeProvider                    interfaceInstancesProviderIncludeItf;

    /** Source code for attribute definitions. */
    @ClientInterface(name = "attribute-instantiation-provider", contingency = OPTIONAL, record = "role:attributeInstantiation, id:%", parameters = "componentNode")
    public SourceCodeProvider                    attributeInstantiationProviderItf;

    /** Source codes for each controller definition. */
    @ClientInterface(name = "controller-instantiation-provider", signature = SourceCodeProvider.class, record = "role:controllerInstantiation, id:%", parameters = "componentNode")
    public final Map<String, SourceCodeProvider> controllerInstantiationsProviderItf = new HashMap<String, SourceCodeProvider>();

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractInstantiationTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter("Primitive Instantiation Builder");
      final String definitionName = typeNameProviderItf.getCTypeName();
      final String instanceName = instanceNameProviderItf.getCInstanceName();

      // Declare the component structure since it may be referenced by the
      // controller structures.
      // This declaration is extern since it is redeclared and initialized
      // latter.
      // This avoid warnings when compiling with -Wredundant-decls.
      cw.append("#ifndef ").append(instanceName).append("_DECLARED").endl();
      cw.append("extern struct ").append(definitionName).append("_t ").append(
          instanceName).append(";").endl();
      cw.append("#define ").append(instanceName).append("_DECLARED").endl();
      cw.appendln("#endif");

      cw.endl();

      // Append the code for the initialization of the controller data
      // structures.
      // To ensure reproducible code generation, append controller definition
      // codes in their alphabetic order.
      appendSortedSourceCodes(cw, controllerInstantiationsProviderItf.values());

      if (interfaceInstancesProviderIncludeItf != null)
        cw.appendln(interfaceInstancesProviderIncludeItf.getSourceCode());

      // Initialize the component data structure.
      cw.append("struct ").append(definitionName).append("_t ").append(
          instanceName).append(" = {").endl();
      // append initialization of interfaces.
      cw.append(interfaceInstancesProviderInitializationItf.getSourceCode())
          .append(",").endl();

      // append initialization of attributes (if any)
      if (attributeInstantiationProviderItf != null)
        cw.append(attributeInstantiationProviderItf.getSourceCode())
            .append(",").endl();

      cw.append("};").endl();

      return cw.toString();
    }
  }
}
