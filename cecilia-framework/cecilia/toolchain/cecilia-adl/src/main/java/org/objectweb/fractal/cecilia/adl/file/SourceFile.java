/***
 * Fractal Task Codegen Framework
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.file;

import java.io.File;

/**
 * A <code>SourceFile</code> is identified by a signature and contains the
 * {@link File} object.
 */
public class SourceFile {

  private final String  signature;
  private final File    file;
  private final boolean assemblyFile;

  /**
   * Creates a {@link SourceFile}. The created source file is not an assembly
   * file.
   * 
   * @param signature the name of the implementation.
   * @param sourceFile the source file.
   */
  public SourceFile(final String signature, final File sourceFile) {
    this(signature, sourceFile, false);
  }

  /**
   * Creates a {@link SourceFile}.
   * 
   * @param signature the name of the implementation.
   * @param sourceFile the source file.
   * @param assemblyFile indicate if the source file is an assembly file.
   */
  public SourceFile(final String signature, final File sourceFile,
      final boolean assemblyFile) {
    this.signature = signature;
    this.file = sourceFile;
    this.assemblyFile = assemblyFile;
  }

  /**
   * @return the signature of this source file.
   */
  public String getSignature() {
    return signature;
  }

  /**
   * @return the file object of this source file.
   */
  public File getFile() {
    return file;
  }

  /**
   * @return <code>true</code> if this source file is an assembly file.
   */
  public boolean isAssemblyFile() {
    return assemblyFile;
  }

  @Override
  public int hashCode() {
    return signature.hashCode() * file.hashCode();
  }

  @Override
  public boolean equals(final Object obj) {
    if (!(obj instanceof SourceFile)) return false;
    return signature.equals(((SourceFile) obj).signature)
        && file.equals(((SourceFile) obj).file);
  }

}
