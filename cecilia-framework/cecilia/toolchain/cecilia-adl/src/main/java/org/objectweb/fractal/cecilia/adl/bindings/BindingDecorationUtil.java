/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.bindings;

import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.bindings.Binding;
import org.objectweb.fractal.adl.interfaces.Interface;

/**
 * Utility class to manipulate decorations of {@link Binding} nodes.
 */
public final class BindingDecorationUtil {

  /**
   * A decoration set on {@link Binding} nodes, its value is the client
   * interface node of the binding. The value of this decoration must be an
   * {@link Interface} node.
   * 
   * @see #getClientInterface(Binding)
   * @see #setClientInterface(Binding, Interface)
   */
  public static final String CLIENT_INTERFACE_DECORATION           = "client-interface";

  /**
   * A decoration set on {@link Binding} nodes, its value is the server
   * interface node of the binding. The value of this decoration must be an
   * {@link Interface} node.
   * 
   * @see #getServerInterface(Binding)
   * @see #setServerInterface(Binding, Interface)
   */
  public static final String SERVER_INTERFACE_DECORATION           = "server-interface";

  /**
   * A decoration set on {@link Binding} nodes to say that a the binding must be
   * established statically. The value of this decoration must be a
   * {@link Boolean}.
   * 
   * @see #setStaticallyBound(Binding, boolean)
   * @see #isStaticallyBound(Binding)
   * @see #isStaticallyBoundFalse(Binding)
   */
  public static final String STATICALLY_BOUND_DECORATION           = "statically-bound";

  /**
   * A decoration set on {@link Binding} nodes to say that a binding is not
   * reconfigurable. The value of this decoration must be a {@link Boolean}.
   * 
   * @see #setDynamic(Binding, boolean)
   * @see #isDynamic(Binding)
   */
  public static final String DYNAMIC_BINDING_ATTRIBUTE_DECORATION  = "dynamic-binding";

  /**
   * A decoration set on {@link Binding} nodes to indicate that a binding is
   * an inner binding of a merged primitive component. The value of this
   * decoration must be a {@link Boolean}.
   * XXX: check whether this decoration is not redundant with another decoration.
   * 
   * @see #setInnerBinding(Binding, boolean)
   * @see #isInnerBinding(Binding)
   */
  public static final String INNER_BINDING_DECORATION              = "inner-binding";

  /**
   *
   */
  public static final String CECILIA_DISABLE_DYNAMICFALSE_BINDINGS = "CECILIA_DISABLE_DYNAMICFALSE_BINDINGS";

  private BindingDecorationUtil() {
  }

  /**
   * Sets the {@link #CLIENT_INTERFACE_DECORATION} decoration to the given
   * {@link Binding} node.
   * 
   * @param binding the binding to which the decoration is set.
   * @param clientItf the value of the decoration.
   * @see #CLIENT_INTERFACE_DECORATION
   */
  public static void setClientInterface(final Binding binding,
      final Interface clientItf) {
    ((Node) binding).astSetDecoration(CLIENT_INTERFACE_DECORATION, clientItf);
  }

  /**
   * Returns the value of the {@link #CLIENT_INTERFACE_DECORATION} decoration of
   * the given binding node.
   * 
   * @param binding a binding node.
   * @return The value of the {@link #CLIENT_INTERFACE_DECORATION} decoration or
   *         <code>null</code> if the given node has no such decoration.
   */
  public static Interface getClientInterface(final Binding binding) {
    return (Interface) ((Node) binding)
        .astGetDecoration(CLIENT_INTERFACE_DECORATION);
  }

  /**
   * Sets the {@link #SERVER_INTERFACE_DECORATION} decoration to the given
   * {@link Binding} node.
   * 
   * @param binding the binding to which the decoration is set.
   * @param clientItf the value of the decoration.
   * @see #SERVER_INTERFACE_DECORATION
   */
  public static void setServerInterface(final Binding binding,
      final Interface clientItf) {
    ((Node) binding).astSetDecoration(SERVER_INTERFACE_DECORATION, clientItf);
  }

  /**
   * Returns the value of the {@link #SERVER_INTERFACE_DECORATION} decoration of
   * the given binding node.
   * 
   * @param binding a binding node.
   * @return The value of the {@link #SERVER_INTERFACE_DECORATION} decoration or
   *         <code>null</code> if the given node has no such decoration.
   */
  public static Interface getServerInterface(final Binding binding) {
    return (Interface) ((Node) binding)
        .astGetDecoration(SERVER_INTERFACE_DECORATION);
  }

  /**
   * Sets the {@link #STATICALLY_BOUND_DECORATION} decoration to the given node
   * with the given boolean value.
   * 
   * @param binding the binding to which the decoration is set.
   * @param b the value of the decoration.
   * @see #STATICALLY_BOUND_DECORATION
   */
  public static void setStaticallyBound(final Binding binding, final boolean b) {
    ((Node) binding).astSetDecoration(STATICALLY_BOUND_DECORATION, b);
  }

  /**
   * Returns <code>true</code> if the given binding node has a
   * {@link #STATICALLY_BOUND_DECORATION} decoration with the <code>true</code>
   * value.
   * 
   * @param binding a binding node.
   * @return <code>true</code> if the given binding node has a
   *         {@link #STATICALLY_BOUND_DECORATION} decoration with the
   *         <code>true</code> value.
   * @see #STATICALLY_BOUND_DECORATION
   */
  public static boolean isStaticallyBound(final Binding binding) {
    final Boolean b = (Boolean) ((Node) binding)
        .astGetDecoration(STATICALLY_BOUND_DECORATION);
    return b != null && b;
  }

  /**
   * Returns <code>true</code> if the given binding node has a
   * {@link #STATICALLY_BOUND_DECORATION} decoration with the <code>false</code>
   * value. <b>Warning</b>: this method is not the oposite of the
   * {@link #isStaticallyBound(Binding)} since if the decoration is not present,
   * both of them return <code>false</code>.
   * 
   * @param binding a binding node.
   * @return <code>true</code> if the given binding node has a
   *         {@link #STATICALLY_BOUND_DECORATION} decoration with the
   *         <code>false</code> value.
   * @see #STATICALLY_BOUND_DECORATION
   */
  public static boolean isStaticallyBoundFalse(final Binding binding) {
    final Boolean b = (Boolean) ((Node) binding)
        .astGetDecoration(STATICALLY_BOUND_DECORATION);
    return b != null && (!b);
  }

  /**
   * Sets the {@link #DYNAMIC_BINDING_ATTRIBUTE_DECORATION} decoration to the given
   * {@link Binding} node.
   * 
   * @param binding the binding to which the decoration is set.
   * @param b the value of the decoration.
   * @see #DYNAMIC_BINDING_ATTRIBUTE_DECORATION
   */
  public static void setDynamic(final Node node, final boolean b) {
    node.astSetDecoration(DYNAMIC_BINDING_ATTRIBUTE_DECORATION, b);
  }

  /**
   * Returns the value of the {@link #DYNAMIC_BINDING_ATTRIBUTE_DECORATION} decoration of
   * the given binding node.
   * 
   * @param binding a binding node.
   * @return The value of the {@link #DYNAMIC_BINDING_ATTRIBUTE_DECORATION} decoration or
   *         <code>null</code> if the given node has no such decoration.
   */
  public static boolean isDynamic(final Node node) {
    Boolean b = (Boolean) node
        .astGetDecoration(DYNAMIC_BINDING_ATTRIBUTE_DECORATION);
    final String disableDynamicFalse = System.getProperty(
        BindingDecorationUtil.CECILIA_DISABLE_DYNAMICFALSE_BINDINGS, "false");
    return    (b == null)
           || (disableDynamicFalse.equals("true"))
           || b;
  }

  /**
   * Sets the {@link #INNER_BINDING_DECORATION} decoration to the given
   * {@link Binding} node.
   * 
   * @param binding the binding to which the decoration is set.
   * @param b the value of the decoration.
   * @see #INNER_BINDING_DECORATION
   */
  public static void setInnerBinding(final Binding binding, final boolean b) {
    binding.astSetDecoration(INNER_BINDING_DECORATION, b);
  }

  /**
   * Returns the value of the {@link #INNER_BINDING_DECORATION} decoration of
   * the given binding node.
   * 
   * @param binding a binding node.
   * @return <code>true</code> if the given binding node has a
   *         {@link #INNER_BINDING_DECORATION} decoration with the
   *         <code>true</code> value.
   */
  public static boolean isInnerBinding(final Binding binding) {
    Boolean b = (Boolean) ((Node) binding)
        .astGetDecoration(INNER_BINDING_DECORATION);
    return b != null && b;
  }
}
