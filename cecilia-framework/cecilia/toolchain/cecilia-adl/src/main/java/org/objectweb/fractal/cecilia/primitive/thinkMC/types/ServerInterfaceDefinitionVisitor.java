/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.primitive.thinkMC.types;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;
import static org.objectweb.fractal.cecilia.adl.controllers.ControllerDecorationUtil.getVTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.AbstractDefinitionTask;
import org.objectweb.fractal.cecilia.adl.SourceCodeProvider;
import org.objectweb.fractal.cecilia.adl.file.CodeWriter;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.interfaces.AbstractServerInterfaceVisitor;
import org.objectweb.fractal.task.core.TaskException;
import org.objectweb.fractal.task.core.primitive.annotations.ClientInterfaceForEach;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterface;
import org.objectweb.fractal.task.core.primitive.annotations.ServerInterfaces;
import org.objectweb.fractal.task.core.primitive.annotations.TaskParameters;

/**
 * Visitor component that builds the definition of the server interfaces of the
 * component for the <code>ThinkMC</code> dialect.
 */
public class ServerInterfaceDefinitionVisitor
    extends
      AbstractServerInterfaceVisitor {

  // ---------------------------------------------------------------------------
  // Implementation of abstract methods of AbstractServerInterfaceVisitor
  // ---------------------------------------------------------------------------

  /**
   * Creates a task that writes the source code containing the definition of the
   * server interfaces of the component.
   */
  @Override
  protected Component createTask(final ComponentContainer container,
      final List<TypeInterface> serverInterfaces) throws ADLException,
      TaskException {
    final List<IDLDefinition> serverInterfaceDefinitions = new ArrayList<IDLDefinition>();
    for (final Interface itf : serverInterfaces) {
      serverInterfaceDefinitions.add(castNodeError(itf,
          IDLDefinitionContainer.class).getIDLDefinition());
    }

    return taskFactoryItf.newPrimitiveTask(new ServerInterfaceDefinitionTask(
        serverInterfaces), container, serverInterfaceDefinitions);
  }

  // ---------------------------------------------------------------------------
  // Task classes
  // ---------------------------------------------------------------------------

  /**
   * Builds definition source code for exported interface definitions for the
   * given component node. This task provides the produced source code.
   */
  @TaskParameters({"componentNode", "exportedInterfaceDefinitions"})
  @ServerInterfaces(@ServerInterface(name = "source-code-provider", signature = SourceCodeProvider.class, record = "role:serverInterfaceDefinition, id:%", parameters = "componentNode"))
  public static class ServerInterfaceDefinitionTask
      extends
        AbstractDefinitionTask {

    protected final List<TypeInterface>                 exportedItfNodes;

    // -------------------------------------------------------------------------
    // Client interfaces
    // -------------------------------------------------------------------------

    /**
     * Source code to be included to access the interface definition header
     * files (idl.h) for each exported interface of the componentNode.
     */
    @ClientInterfaceForEach(iterable = "exportedInterfaceDefinitions", prefix = "c-interface-header", signature = SourceCodeProvider.class, record = "role:cInterfaceDefinition, id:%", parameters = "exportedInterfaceDefinitions.element")
    public final Map<TypeInterface, SourceCodeProvider> cItfDefinitionItfs = new HashMap<TypeInterface, SourceCodeProvider>();

    /**
     * Source code to be included to access the VFT template header files for
     * each exported interface of the componentNode.
     */
    @ClientInterfaceForEach(iterable = "exportedInterfaceDefinitions", prefix = "c-vft-header", signature = SourceCodeProvider.class, record = "role:cVFTDefinition, id:%", parameters = "exportedInterfaceDefinitions.element")
    public final Map<TypeInterface, SourceCodeProvider> cVFTHeaderItfs     = new HashMap<TypeInterface, SourceCodeProvider>();

    // -------------------------------------------------------------------------
    // Task constructor
    // -------------------------------------------------------------------------

    /**
     * @param exportedItfNodes the list of exported interface nodes belonging to
     *          the component.
     */
    protected ServerInterfaceDefinitionTask(
        final List<TypeInterface> exportedItfNodes) {
      // The order of the interfaces is important, this is why an array of
      // exportedItfNodes is used for iterating of the exported interfaces
      // rather than iterating directly on the set of idlHeaderItfs client
      // interfaces.
      this.exportedItfNodes = exportedItfNodes;
    }

    // -------------------------------------------------------------------------
    // Implementation of abstract methods of AbstractDefinitionTask
    // -------------------------------------------------------------------------

    @Override
    protected String processSourceCode() throws Exception {
      final CodeWriter cw = new CodeWriter("Primitive Server Interfaces Definition");
      // include idl.h files
      for (final TypeInterface itf : exportedItfNodes) {
        final IDLDefinition itfDef = castNodeError(itf,
            IDLDefinitionContainer.class).getIDLDefinition();
        cw.append(cItfDefinitionItfs.get(itfDef).getSourceCode());
      }
      cw.endl();

      // Build the structure of provided interfaces
      cw.append("struct ").append(typeNameProviderItf.getCTypeName()).append(
          "_exporteds {").endl();
      for (final TypeInterface itf : exportedItfNodes) {
        cw.append("R").append(itf.getSignature().replace('.', '_')).append(" ")
            .append(itf.getName().replace('-', '_')).append(";").endl();
      }
      cw.append("};").endl();

      // build the definition of the methods implemented by the component.
      for (final TypeInterface itf : exportedItfNodes) {
        cw.endl();
        final IDLDefinition itfDef = castNodeError(itf,
            IDLDefinitionContainer.class).getIDLDefinition();
        if (getVTable(itf) == null) {
          final String signature = itf.getSignature().replace('.', '_');
          final String itfName = itf.getName();
          final String cItfName = itfName.replace('-', '_');

          // include VFTTemplate header file and use the <itf>_METHOD_DEFINITION
          // macro to define the methods implemented by the component.
          cw.append("// defines the method prototype implemented by this ")
              .append("component for the interface ").append(itfName).endl();
          cw.append(cVFTHeaderItfs.get(itfDef).getSourceCode()).endl();
          cw.append(signature.toUpperCase()).append("_METHOD_DEFINITION(")
              .append(typeNameProviderItf.getCTypeName()).append(",").append(
                  cItfName).append(") ;").endl();
        }
      }
      return cw.toString();
    }
  }
}
