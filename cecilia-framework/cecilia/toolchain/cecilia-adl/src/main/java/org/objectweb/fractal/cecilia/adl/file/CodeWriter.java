/***
 * Fractal Task Codegen Framework
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.file;

/**
 * Utility class for source code generation.
 */
public final class CodeWriter {

  private static final int    INDENTATION_SPACE = 2;
  private final String        producerName;
  private int                 indentation       = 0;
  private boolean             newLine           = true;
  private final StringBuilder buff              = new StringBuilder();

  // private int pendingChars = 0;

  /**
   * @param producerName the name of the producer of the code.
   */
  public CodeWriter(final String producerName) {
    this.producerName = producerName;
    append("//+ Beginning of code produced by ").append(producerName).endl();
  }

  /**
   * Default constructor.
   */
  public CodeWriter() {
    this.producerName = null;
  }

  /**
   * Appends a string.
   * 
   * @param s the code to appends.
   * @return <code>this</code>.
   */
  public CodeWriter append(final String s) {
    if (s == null) return this;
    for (int i = 0; i < s.length(); i++) {
      append(s.charAt(i));
    }
    return this;
  }

  /**
   * Appends a character.
   * 
   * @param c a character.
   * @return <code>this</code>.
   */
  public CodeWriter append(final char c) {
    if (newLine && (c == ' ' || c == '\t')) return this;

    if (c == '}') {
      if (newLine && indentation >= INDENTATION_SPACE)
        buff.delete(buff.length() - INDENTATION_SPACE, buff.length());
      removeIndentLevel();
    } else if (c == '{') addIndentLevel();
    newLine = false;
    buff.append(c);
    if (c == '\n') {
      newLine = true;
      for (int j = 0; j < indentation; j++)
        buff.append(' ');
    }
    return this;
  }

  /**
   * Appends a string and ends the current line.
   * 
   * @param s the code to appends.
   * @return <code>this</code>.
   */
  public CodeWriter appendln(final String s) {
    if (s == null) return this;
    append(s).append('\n');
    return this;
  }

  /**
   * Appends an integer.
   * 
   * @param i the integer to appends.
   * @return <code>this</code>.
   */
  public CodeWriter append(final int i) {
    buff.append(i);
    return this;
  }

  /**
   * Ends the current line.
   * 
   * @return <code>this</code>.
   */
  public CodeWriter endl() {
    append('\n');
    return this;
  }

  protected CodeWriter addIndentLevel() {
    indentation += INDENTATION_SPACE;
    return this;
  }

  protected CodeWriter removeIndentLevel() {
    if (indentation >= INDENTATION_SPACE) indentation -= INDENTATION_SPACE;
    return this;
  }

  @Override
  public String toString() {
    if (producerName != null) {
      if (!newLine) endl();
      append("//- End of code produced by ").append(producerName).endl();
    }
    return buff.toString();
  }

}
