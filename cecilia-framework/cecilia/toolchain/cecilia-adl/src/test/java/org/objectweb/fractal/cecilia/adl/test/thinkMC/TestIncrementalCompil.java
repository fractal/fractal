/***
 * Cecilia ADL Compiler
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.test.thinkMC;

import java.io.File;
import org.objectweb.fractal.cecilia.adl.test.CeciliaAdlTestCase;

public class TestIncrementalCompil extends CeciliaAdlTestCase {

  public void testRecompile() throws Exception {
    // compile a first time
    File outputFile = compileADL("test.thinkMC.simple.SimpleMC");
    assertTrue("The compiled file does not exist", outputFile.exists());
    final long timestamp = outputFile.lastModified();
    assertTrue("Unable to get timestamp of compiled file", timestamp > 0);

    // recompile a second time
    outputFile = compileADL("test.thinkMC.simple.SimpleMC");

    assertTrue("The recompilation modifies the output file.", outputFile
        .lastModified() == timestamp);
  }

}
