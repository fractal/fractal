/**
 * 
 */

package org.objectweb.fractal.cecilia.adl.test.optC.optimizations.merge;

import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;
import org.objectweb.fractal.cecilia.adl.CeciliaADLConstants;
import org.objectweb.fractal.cecilia.adl.components.MergeableComponent;
import org.objectweb.fractal.cecilia.adl.test.optC.AbstractCeciliaLoaderTest;

/**
 * Simple test just to verify that the AST types and the DTD are ok.
 * 
 * @author Alessio Pace
 */
public class MergeableComponentTest extends AbstractCeciliaLoaderTest {

  XMLNodeFactory nodeFactory = new XMLNodeFactoryImpl();

  public void testCreateDefinition() throws Exception {
    final Definition definition = (Definition) this.nodeFactory.newXMLNode(
        CeciliaADLConstants.CECILIA_ADL_DTD,
        CeciliaADLConstants.DEFINITION_AST_NODE_NAME);
    assertNull(((MergeableComponent) definition).getMerge());
  }

  public void testCreateComponent() throws Exception {
    final Component definition = (Component) this.nodeFactory.newXMLNode(
        CeciliaADLConstants.CECILIA_ADL_DTD,
        CeciliaADLConstants.COMPONENT_AST_NODE_NAME);
    assertNull(((MergeableComponent) definition).getMerge());
  }

}
