/**
 * 
 */

package org.objectweb.fractal.cecilia.adl.perf;

import static java.lang.System.currentTimeMillis;

import java.util.HashMap;

import junit.framework.TestCase;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.adl.JavaFactory;
import org.objectweb.fractal.adl.StaticJavaGenerator;

/**
 * @author leclercm
 */
public class CeciliaCompilerInstantiationPerf extends TestCase {

  public static final int ADL_CYCLES   = 50;
  public static final int CLASS_CYCLES = 5000;

  protected Factory       factory;
  protected ClassLoader   classLoader;

  @Override
  public void setUp() throws Exception {
    factory = FactoryFactory.getFactory();
    classLoader = getClass().getClassLoader();
  }

  protected Object instantiateFromADL(final String definition) {
    try {
      return factory.newComponent(definition, new HashMap<Object, Object>());
    } catch (final Exception e) {
      e.printStackTrace();
      fail();
      return null;
    }
  }

  protected Object instantiateFromClass(final String definition) {
    try {
      final Class<?> factoryFactoryClass = classLoader.loadClass(definition
          + StaticJavaGenerator.CLASSNAME_SUFFIX);
      final JavaFactory factoryFactory = (JavaFactory) factoryFactoryClass
          .newInstance();
      return factoryFactory.newComponent();
    } catch (final Exception e) {
      e.printStackTrace();
      fail();
      return null;
    }
  }

  protected void instantiate(final String adl) {
    // run garbage collector before test.
    Runtime.getRuntime().gc();

    long fromADL = currentTimeMillis();

    for (int i = 0; i < ADL_CYCLES; i++) {
      instantiateFromADL(adl);
    }
    fromADL = currentTimeMillis() - fromADL;
    // run garbage collector between the two runs.
    long fromClass = currentTimeMillis();

    for (int i = 0; i < CLASS_CYCLES; i++) {
      instantiateFromClass(adl);
    }
    fromClass = currentTimeMillis() - fromClass;
    System.out.println("loading the " + adl);
    System.out.println("ADL Factory(" + ADL_CYCLES + ")=" + fromADL
        + "ms\tgenerated class(" + CLASS_CYCLES + ")=" + fromClass
        + "ms\tratio="
        + ((float) (CLASS_CYCLES * fromADL) / (ADL_CYCLES * fromClass)));
    System.out.println();
  }

  public void testCeciliaBasicFactory() {
    instantiate("org.objectweb.fractal.cecilia.adl.CeciliaBasicFactory");
  }

  public void testThinkMCVisitor() {
    instantiate("org.objectweb.fractal.cecilia.primitive.thinkMC.Visitor");
  }

  public void testThinkMCControllerLoader() {
    instantiate("org.objectweb.fractal.cecilia.primitive.thinkMC.CPrimitiveControllerLoader");
  }

  public void testThinkMCImplementationCodeLoader() {
    instantiate("org.objectweb.fractal.cecilia.primitive.thinkMC.ImplementationCodeLoader");
  }

  public void testThinkMCDefinitionVisitor() {
    instantiate("org.objectweb.fractal.cecilia.primitive.thinkMC.CPrimitiveDefinitionVisitor");
  }

  public void testThinkMCInstantiationVisitor() {
    instantiate("org.objectweb.fractal.cecilia.primitive.thinkMC.CPrimitiveInstantiationVisitor");
  }

  public void testCompositeVisitor() {
    instantiate("org.objectweb.fractal.cecilia.composite.CCompositeVisitor");
  }

}
