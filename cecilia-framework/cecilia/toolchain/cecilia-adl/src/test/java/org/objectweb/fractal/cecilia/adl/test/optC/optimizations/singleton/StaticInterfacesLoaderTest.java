/***
 * Cecilia
 * Copyright (C) 2005-2008 STMicroelectronics
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.test.optC.optimizations.singleton;

import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.ast.ASTFractalUtil;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;
import org.objectweb.fractal.cecilia.adl.test.optC.AbstractCeciliaLoaderTest;
import org.objectweb.fractal.cecilia.adl.types.TypeDecorationUtil;

/**
 * @author Alessio Pace
 */
public class StaticInterfacesLoaderTest extends AbstractCeciliaLoaderTest {

  public static final String SINGLETON_FIXTURES_PREFIX = "test.optC.optimizations.singleton.";
  public static final String ALL_FIXTURES_PREFIX       = "test.optC.optimizations.all.";

  public void testStaticInterfaces() throws Exception {
    final Definition definition = this.loader.load(SINGLETON_FIXTURES_PREFIX
        + "StaticInterfaces", getContextMapToBeUsedByLoader());
    assertNotNull("Assert not null", definition);
    final ComponentContainer test = ASTFractalUtil.getFcSubComponentByName(
        (ComponentContainer) definition, "test");
    final ComponentContainer server = ASTFractalUtil.getFcSubComponentByName(
        test, "server");
    assertTrue(TypeDecorationUtil.isSingletonComponentType(server));
    final TypeInterface rItf = ASTFractalUtil.getFcInterface(server, "r");
    assertNotNull(rItf);
    Boolean b = InterfaceDecorationUtil.getStaticInterfaceDecoration(rItf);
    assertTrue(b);
  }

  /**
   * This test verifies that inner interfaces are also considered to be static
   * or non static.
   * 
   * @throws Exception
   */
  public void testStaticInterfacesWithMergedPrimitive() throws Exception {
    final Definition definition = this.loader.load(
        ALL_FIXTURES_PREFIX + "All1", getContextMapToBeUsedByLoader());
    assertNotNull("Assert not null", definition);
    final ComponentContainer test = ASTFractalUtil.getFcSubComponentByName(
        (ComponentContainer) definition, "test");
    assertNotNull(test);
    final ComponentContainer server = ASTFractalUtil.getFcSubComponentByName(
        test, "server");
    assertTrue(TypeDecorationUtil.isSingletonComponentType(server));
    final TypeInterface sItf = ASTFractalUtil.getFcInterface(server, "s");
    assertNotNull(sItf);
    Boolean b = InterfaceDecorationUtil.getStaticInterfaceDecoration(sItf);
    assertTrue(b);
  }
}
