/***
 * Cecilia ADL Compiler
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.perf;

import static java.lang.System.currentTimeMillis;

import java.io.File;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.adl.JavaFactory;
import org.objectweb.fractal.adl.StaticJavaGenerator;
import org.objectweb.fractal.cecilia.adl.Launcher;
import org.objectweb.fractal.cecilia.adl.plugin.JavaPluginManager;
import org.objectweb.fractal.cecilia.adl.test.CeciliaAdlTestCase;

public class CeciliaPhasesPerf extends CeciliaAdlTestCase {

  static final String INSTRUMENTED_FACTORY = "org.objectweb.fractal.cecilia.adl.InstrumentedCeciliaBasicFactory";

  static final int    CYCLES               = 20;

  String              adl;
  boolean             useADLFactory;
  File                outputDir;

  long                factoryTime          = 0;
  long                loaderTime           = 0;
  long                visitorTime          = 0;
  long                instantiationTime    = 0;

  @Override
  protected Map<String, String> getLauncherOptions() throws Exception {
    final Map<String, String> launcherOptions = super.getLauncherOptions();
    outputDir = new File(launcherOptions.get("-o"), "CeciliaPhasesPerf");
    outputDir.mkdirs();
    launcherOptions.put("-o", outputDir.getAbsolutePath());
    return launcherOptions;
  }

  public void testSimpleMCWithADLFactory() throws Exception {
    System.setProperty(
        JavaPluginManager.NO_STATIC_PLUGIN_FACTORY_PROPERTY_NAME, "true");
    adl = "test.thinkMC.simple.SimpleMC";
    useADLFactory = true;
    doTest();
  }

  public void testSimpleMCWithClass() throws Exception {
    System
        .clearProperty(JavaPluginManager.NO_STATIC_PLUGIN_FACTORY_PROPERTY_NAME);
    adl = "test.thinkMC.simple.SimpleMC";
    useADLFactory = false;
    doTest();
  }

  void doTest() throws Exception {
    if (useADLFactory)
      System.out.println("Compilation of \"" + adl + "\" using ADL factory");
    else
      System.out.println("Compilation of \"" + adl
          + "\" using generated classes");
    System.out
        .println("\t\tglobal time\tinstantiation time\tload time\tvisit time\texec time");

    // warm-up
    cycle();
    // clean output directory
    deleteDirectory(outputDir);

    System.out.print("Warmup: ");
    printResults(1);

    for (int i = 0; i < CYCLES; i++) {

      cycle();

      // clean output directory
      deleteDirectory(outputDir);
    }

    System.out.print("Perf (" + CYCLES + "): ");
    printResults(CYCLES);
    System.out.println();
  }

  void cycle() throws Exception, ADLException {
    final AbstractInstrumentedLauncher launcher = (useADLFactory)
        ? new InstrumentedLauncherByADL(getLauncherOptions())
        : new InstrumentedLauncherByClass(getLauncherOptions());

    // run the compilation of the SimpleMC definition
    launcher.compile("test.thinkMC.simple.SimpleMC");

    // get Times
    factoryTime += launcher.factoryProbe.getTime();
    loaderTime += launcher.loaderProbe.getTime();
    visitorTime += launcher.visitorProbe.getTime();
    instantiationTime += launcher.instantiationTime;

// System.out.printf("partial %d, %d, %d, %d\n", launcher.factoryProbe
// .getTime(), launcher.loaderProbe.getTime(), launcher.visitorProbe
// .getTime(), launcher.instantiationTime);
// System.out.printf("total %d, %d, %d, %d\n", factoryTime, loaderTime,
// visitorTime, instantiationTime);

    Runtime.getRuntime().gc();
  }

  void printResults(final int n) {
    final double globalTime = (factoryTime + instantiationTime) / (1000.0 * n);
    final double instTime = instantiationTime / (1000.0 * n);
    final double loadTime = loaderTime / (1000.0 * n);
    final double visitTime = visitorTime / (1000.0 * n);
    final double execTime = (factoryTime - loaderTime - visitorTime)
        / (1000.0 * n);
    System.out
        .printf(
            "\t%.2fs\t\t%.2fs (% 3d%%)\t\t%.2fs (% 3d%%)\t%.2fs (% 3d%%)\t%.2fs (% 3d%%)",
            globalTime, instTime, ((int) ((instTime / globalTime) * 100.0)),
            loadTime, ((int) ((loadTime / globalTime) * 100.0)), visitTime,
            ((int) ((visitTime / globalTime) * 100.0)), execTime,
            ((int) ((execTime / globalTime) * 100.0)));
    System.out.println();

    factoryTime = 0;
    loaderTime = 0;
    visitorTime = 0;
    instantiationTime = 0;
  }

  abstract static class AbstractInstrumentedLauncher extends Launcher {

    Probe factoryProbe;
    Probe loaderProbe;
    Probe visitorProbe;
    long  instantiationTime;

    AbstractInstrumentedLauncher(final Map<String, String> options)
        throws InvalidCommandLineException, CompilerInstantiationException {
      super(options);
    }

    protected abstract Map<?, ?> createCompilerComponent(
        final Map<Object, Object> compilerContext) throws Exception;

    @Override
    protected Factory createCompiler(final Map<Object, Object> compilerContext,
        final CommandLine cmdLine) {
      final Map<?, ?> compilerComponent;

      try {
        final long begin = currentTimeMillis();
        compilerComponent = createCompilerComponent(compilerContext);
        instantiationTime = currentTimeMillis() - begin;
      } catch (final Exception e) {
        e.printStackTrace();
        fail("Unable to instantiate Cecilia compiler");
        return null;
      }

      factoryProbe = (Probe) compilerComponent.get("factory-probe");
      loaderProbe = (Probe) compilerComponent.get("loader-probe");
      visitorProbe = (Probe) compilerComponent.get("visitor-probe");
      return (Factory) compilerComponent.get("factory");
    }
  }

  static class InstrumentedLauncherByADL extends AbstractInstrumentedLauncher {

    InstrumentedLauncherByADL(final Map<String, String> options)
        throws InvalidCommandLineException, CompilerInstantiationException {
      super(options);
    }

    @Override
    protected Map<?, ?> createCompilerComponent(
        final Map<Object, Object> compilerContext) throws Exception {
      final Factory factory = FactoryFactory.getFactory();
      return (Map<?, ?>) factory.newComponent(INSTRUMENTED_FACTORY,
          compilerContext);
    }
  }

  static class InstrumentedLauncherByClass extends AbstractInstrumentedLauncher {

    InstrumentedLauncherByClass(final Map<String, String> options)
        throws InvalidCommandLineException, CompilerInstantiationException {
      super(options);
    }

    @Override
    protected Map<?, ?> createCompilerComponent(
        final Map<Object, Object> compilerContext) throws Exception {
      final Class<?> factoryFactoryClass = getClass().getClassLoader()
          .loadClass(
              INSTRUMENTED_FACTORY + StaticJavaGenerator.CLASSNAME_SUFFIX);
      final JavaFactory factoryFactory = (JavaFactory) factoryFactoryClass
          .newInstance();
      return (Map<?, ?>) factoryFactory.newComponent();
    }
  }

}
