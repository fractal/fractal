/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.test.optC.optimizations.all;

import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;
import org.objectweb.fractal.cecilia.adl.test.CeciliaAdlTestCase;

/**
 * Compiles and execute ADL definitions which use all the optimizations.
 * 
 * @author Alessio Pace
 */
public class OptimizationsAllTest extends CeciliaAdlTestCase {

  /**
   * Common prefix for test definitions.
   */
  public static final String TEST_STATICBINDING_PREFIX = "test.optC.optimizations.all.";

  /**
   * The System property is required.
   * 
   * @throws Exception
   */
  public void testAll1() throws Exception {
    try {
      System.setProperty(InterfaceDecorationUtil.CECILIA_ENABLE_THIS_OPTIMIZATION,
          "true");

      super.compileAndRun(TEST_STATICBINDING_PREFIX + "All1");
    }
    finally {
      System.setProperty(InterfaceDecorationUtil.CECILIA_ENABLE_THIS_OPTIMIZATION,
          "false");
    }
  }

  public void testAll2() throws Exception {
    try {
      System.setProperty(InterfaceDecorationUtil.CECILIA_ENABLE_THIS_OPTIMIZATION,
          "true");

      super.compileAndRun(TEST_STATICBINDING_PREFIX + "All2");
    }
    finally {
      System.setProperty(InterfaceDecorationUtil.CECILIA_ENABLE_THIS_OPTIMIZATION,
          "false");
    }
  }

  public void testAll3() throws Exception {
    try {
      System.setProperty(InterfaceDecorationUtil.CECILIA_ENABLE_THIS_OPTIMIZATION,
          "true");

      super.compileAndRun(TEST_STATICBINDING_PREFIX + "All3");
    }
    finally {
      System.setProperty(InterfaceDecorationUtil.CECILIA_ENABLE_THIS_OPTIMIZATION,
          "false");
    }
  }
}
