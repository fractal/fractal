/***
 * Cecilia ADL Compiler
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.perf;

import static java.lang.System.currentTimeMillis;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

public class FactoryProbe implements Factory, Probe, BindingController {

  private long               time                    = 0;
  private long               count                   = 0;

  // ---------------------------------------------------------------------------
  // Client interfaces
  // ---------------------------------------------------------------------------

  public static final String CLIENT_FACTORY_ITF_NAME = "client-factory";
  public Factory             clientFactoryItf;

  // ---------------------------------------------------------------------------
  // Implementation of the Visitor interface
  // ---------------------------------------------------------------------------

  public Object newComponentType(final String name, final Map context)
      throws ADLException {
    final long begin = currentTimeMillis();
    final Object result = clientFactoryItf.newComponentType(name, context);
    time += currentTimeMillis() - begin;
    count++;
    return result;

  }

  public Object newComponent(final String name, final Map context)
      throws ADLException {
    final long begin = currentTimeMillis();
    final Object result = clientFactoryItf.newComponent(name, context);
    time += currentTimeMillis() - begin;
    count++;
    return result;
  }

  // ---------------------------------------------------------------------------
  // Implementation of the Probe interface
  // ---------------------------------------------------------------------------

  public long getCount() {
    return count;
  }

  public long getTime() {
    return time;
  }

  public void reset() {
    time = 0;
    count = 0;
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  public void bindFc(final String itfName, final Object value)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {

    if (itfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (itfName.equals(CLIENT_FACTORY_ITF_NAME)) {
      clientFactoryItf = (Factory) value;
    } else {
      throw new NoSuchInterfaceException("Unable to find the interface named '"
          + itfName + "'");
    }
  }

  public String[] listFc() {
    return new String[]{CLIENT_FACTORY_ITF_NAME};
  }

  public Object lookupFc(final String itfName) throws NoSuchInterfaceException {

    if (itfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (itfName.equals(CLIENT_FACTORY_ITF_NAME)) {
      return clientFactoryItf;
    } else {
      throw new NoSuchInterfaceException("Unable to find the interface named '"
          + itfName + "'");
    }
  }

  public void unbindFc(final String itfName) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException {

    if (itfName == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (itfName.equals(CLIENT_FACTORY_ITF_NAME)) {
      clientFactoryItf = null;
    } else {
      throw new NoSuchInterfaceException("Unable to find the interface named '"
          + itfName + "'");
    }
  }

}
