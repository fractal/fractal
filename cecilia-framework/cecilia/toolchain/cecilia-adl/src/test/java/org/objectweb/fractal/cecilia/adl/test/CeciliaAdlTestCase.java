/***
 * Cecilia ADL Compiler
 * Copyright (C) 2005 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.test;

import static java.lang.System.getProperty;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.cecilia.adl.Launcher;
import org.objectweb.fractal.cecilia.adl.plugin.AbstractPluginManager;
import org.objectweb.fractal.julia.Julia;

/**
 * Utility class for compiler tests. Defines methods to compile ADL and run
 * compiled application.<br>
 * Tests can be configured using the following system properties:
 * <ul>
 * <li>{@link #BUILD_DIR_PROPERTY_NAME cecilia.test.buildDir} specify the test
 * output directory.</li>
 * <li>{@link #SOURCE_PATH_PROPERTY_NAME cecilia.test.sourcePath} specify the
 * source path of the tests.</li>
 * <li>{@link #COMPILER_CMD_PROPERTY_NAME cecilia.test.compiler} specify the
 * command of the C-compiler.</li>
 * <li>{@link #LINKER_CMD_PROPERTY_NAME cecilia.test.linker} specify the
 * command of the linker.</li>
 * <li>{@link #C_FLAGS_PROPERTY_NAME cecilia.test.cFlags} specify the
 * compilation flags.</li>
 * <li>{@link #LD_FLAGS_PROPERTY_NAME cecilia.test.ldFlags} specify the
 * linker flags.</li>
 * </ul>
 */
public abstract class CeciliaAdlTestCase extends TestCase {

  /**
   * The name of the system property that specify the test output directory.
   * This property is mandatory.
   */
  public static final String BUILD_DIR_PROPERTY_NAME    = "cecilia.test.buildDir";

  /** The default value of the {@link #BUILD_DIR_PROPERTY_NAME} property. */
  public static final String DEFAULT_BUILD_DIR          = "target/test-build";

  /**
   * The name of the system property that specify the source path of the tests.
   * This property is mandatory.
   */
  public static final String SOURCE_PATH_PROPERTY_NAME  = "cecilia.test.sourcePath";

  public static final String DEFAULT_SOURCE_PATH        = "target/test-resources"
                                                            + File.pathSeparator
                                                            + "src/test/resources";

  /**
   * The name of the system property that specify the compiler command. This
   * property is optional (default value is <code>gcc</code>).
   */
  public static final String COMPILER_CMD_PROPERTY_NAME = "cecilia.test.compiler";

  /**
   * The name of the system property that specify the linker command. This
   * property is optional (default value is <code>gcc</code>).
   */
  public static final String LINKER_CMD_PROPERTY_NAME   = "cecilia.test.linker";

  /**
   * The name of the system property that specify the compilation flags. This
   * property is optional (default value is given by the
   * {@value #DEFAULT_C_FLAGS} constant).
   */
  public static final String C_FLAGS_PROPERTY_NAME      = "cecilia.test.cFlags";

  /** The value of the default compilation flags. */
  public static final String DEFAULT_C_FLAGS            = "-g -c -Wall -Werror -Wredundant-decls -Wunreachable-code -Wstrict-prototypes -Wwrite-strings";

  public static final String LD_FLAGS_PROPERTY_NAME     = "cecilia.test.ldFlags";

  public static final String DEFAULT_LD_FLAGS           = "";
  
  public static final String TEST_JOBS_PROPERTY_NAME    = "cecilia.test.jobs";

  /** static reference in order to cache it in the many tests. */
  private static Launcher    launcher                   = null;

  /**
   * This boolean can be set to false by sub class to avoid cleaning build
   * directory at setup phase.
   */
  protected boolean          clearBuildDir              = true;

  @Override
  public void setUp() throws Exception {

    if (launcher == null) {
      final Map<String, String> options = getLauncherOptions();

      // a pure Java CeciliaADL application
      launcher = new Launcher(options);
    }

    /* delete generated "idl" dir */
    if (clearBuildDir) deleteDirectory(new File(getBuildDir(), "idl"));
  }

  protected static boolean deleteDirectory(final File path) {
    if (path.exists()) {
      for (final File file : path.listFiles()) {
        if (file.isDirectory()) {
          deleteDirectory(file);
        } else {
          file.delete();
        }
      }
    }
    return path.delete();
  }

  /**
   * Return the cached Launcher that is used by instances of this class.
   * 
   * @return
   */
  public static Launcher getLauncher() {
    return launcher;
  }

  /**
   * Set the cached Launcher that will be used by instances of this class.
   * 
   * @param l
   */
  public static void setLauncher(final Launcher l) {
    launcher = l;
  }

  /**
   * Returns the test build directory.
   * 
   * @return the test build directory.
   * @throws Exception if the {@value #BUILD_DIR_PROPERTY_NAME} system property
   *             is not specified.
   */
  public static File getBuildDir() throws Exception {
    final String buildPath = getProperty(BUILD_DIR_PROPERTY_NAME,
        DEFAULT_BUILD_DIR);
    final File buildDir = new File(buildPath);
    if (!buildDir.exists()) {
      buildDir.mkdir();
    } else if (!buildDir.isDirectory()) {
      throw new Exception("Invalid  '" + BUILD_DIR_PROPERTY_NAME
          + "' system property: not a directory.");
    }
    return buildDir;
  }

  protected Map<String, String> getLauncherOptions() throws Exception {
    final String srcPath = getProperty(SOURCE_PATH_PROPERTY_NAME,
        DEFAULT_SOURCE_PATH);

    final Map<String, String> options = new HashMap<String, String>();
    options.put("-o", getBuildDir().getAbsolutePath());
    options.put("-src-path", srcPath);
    options
        .put("-c-flags", getProperty(C_FLAGS_PROPERTY_NAME, DEFAULT_C_FLAGS));
    options.put("-ld-flags", getProperty(LD_FLAGS_PROPERTY_NAME,
        DEFAULT_LD_FLAGS));

    final String compiler = getProperty(COMPILER_CMD_PROPERTY_NAME);
    if (compiler != null) {
      options.put("-compiler-command", compiler);
    }

    final String linker = getProperty(LINKER_CMD_PROPERTY_NAME);
    if (linker != null) {
      options.put("-linker-command", linker);
    }

    final String testJobs = getProperty(TEST_JOBS_PROPERTY_NAME);
    if (testJobs != null) {
      if (testJobs.equals("0")) {
        options.put("-j", Integer.toString(Runtime.getRuntime()
            .availableProcessors()));
      }
      else {
        options.put("-j", testJobs);
      }
    }
    
    return options;
  }

  /**
   * Compiles the given ADL using the CeciliaADL factory. If the compilation
   * fails, it catches the exception and prints an error message.
   * 
   * @param adlName the name of the ADL to compile
   * @return the File returned by the factory.
   */
  public File compileADL(final String adlName) {
    try {
      final Map<Object, Object> additionalParams = new HashMap<Object, Object>();
      return tryCompileADL(adlName, additionalParams);
    } catch (final Exception e) {
      e.printStackTrace();
      fail("Compilation fails: " + e.getMessage());
      return null;
    }
  }

    /**
   * Compiles the given ADL using the CeciliaADL factory. If the compilation
   * fails, it catches the exception and prints an error message.
   * 
   * @param adlName the name of the ADL to compile
   * @param additionalParams additional parameters for ADL build. Used by
   *          merge tests.
   * @return the File returned by the factory.
   */
  public File compileADL(final String adlName,
      final Map<Object, Object> additionalParams) {
    try {
      return tryCompileADL(adlName, additionalParams);
    } catch (final Exception e) {
      e.printStackTrace();
      fail("Compilation fails: " + e.getMessage());
      return null;
    }
  }

  /**
   * Compiles the given ADL using the CeciliaADL factory and throws Exception if
   * the compilation fails.
   * 
   * @param adlName the name of the ADL to compile
   * @return the File returned by the factory.
   * @throws Exception if something goes wrong.
   */
  public File tryCompileADL(final String adlName) throws Exception {
    final Object compilationResult = launcher.compile(adlName);
    if (compilationResult instanceof File)
      return (File) compilationResult;
    else if (compilationResult instanceof Collection)
      return (File) ((Collection<?>) compilationResult).iterator().next();
    else { // If the type of the result is not recognized.
      fail("The result of the compilation is not a File nor a Collection.");
      return null;
    }
  }

  /**
   * Compiles the given ADL using the CeciliaADL factory and throws Exception if
   * the compilation fails.
   * 
   * @param adlName the name of the ADL to compile
   * @param additionalParams additional parameters for ADL build. Used by
   *          merge tests.
   * @return the File returned by the factory.
   * @throws Exception if something goes wrong.
   */
  public File tryCompileADL(final String adlName,
      final Map<Object, Object> additionalParams) throws Exception {
    final Object compilationResult = launcher.compile(adlName, additionalParams);
    if (compilationResult instanceof File)
      return (File) compilationResult;
    else if (compilationResult instanceof Collection)
      return (File) ((Collection<?>) compilationResult).iterator().next();
    else { // If the type of the result is not recognized.
      fail("The result of the compilation is not a File nor a Collection.");
      return null;
    }
  }

  /**
   * Run a compiled ADL. A thread is created which will kill the process if it
   * does not ends in 1 second.
   * 
   * @param test the compiled ADL.
   * @param adlName the name of the ADL (used for error message).
   * @throws Exception if something goes wrong.
   */
  public void runTest(final File test, final String adlName) throws Exception {
    final int r = runTestAndReturnValue(test, adlName);
    if (r != 0) {
      fail("The test \'" + adlName
          + "\' does not return correctly. Return value=" + r);
    }
  }

  /**
   * Run a given executable and return its process exit value, or fail() if it
   * does finish in time.
   *
   * @param test
   * @param adlName
   * @return the executable process exit value
   * @throws Exception
   */
  public int runTestAndReturnValue(final File test, final String adlName)
      throws Exception {

    final Process p = new ProcessBuilder(test.getAbsolutePath()).start();
    final KillerThread t = new KillerThread(p);
    t.start();
    final int r = p.waitFor();
    synchronized (t) {
      if (t.processKilled) {
        fail("The test \'" + adlName + "\' did not finish in time.");
      } else {
        t.processOK = true;
        t.notify();
      }
    }
    return r;
  }

  /**
   * Compile and run the given ADL.
   * 
   * @param adlName the name of the adl to compile and run.
   * @throws Exception if something goes wrong.
   */
  public void compileAndRun(final String adlName) throws Exception {
    final File compileADL = compileADL(adlName);
    assertNotNull("File returned by compiler is null", compileADL);
    runTest(compileADL, adlName);
  }

  /**
   * Compile and run the given ADL.
   * 
   * @param adlName the name of the adl to compile and run.
   * @param additionalParams additional parameters for ADL build. Used by
   *          merge tests.
   * @throws Exception if something goes wrong.
   */
  public void compileAndRun(final String adlName,
      final Map<Object, Object> additionalParams) throws Exception {
    final File compileADL = compileADL(adlName, additionalParams);
    assertNotNull("File returned by compiler is null", compileADL);
    runTest(compileADL, adlName);
  }

  /**
   * Compile and run the given ADL, propagate the return value.
   * 
   * @param adlName the name of the adl to compile and run.
   * @return the executable process exit value
   * @throws Exception if something goes wrong.
   */
  public int compileAndRunReturnValue(final String adlName) throws Exception {
    final File compileADL = compileADL(adlName);
    assertNotNull("File returned by compiler is null", compileADL);
    return runTestAndReturnValue(compileADL, adlName);
  }
  /**
   * Compile and run the given ADL, propagate the return value.
   * 
   * @param adlName the name of the adl to compile and run.
   * @param additionalParams additional parameters for ADL build. Used by
   *          merge tests.
   * @return the executable process exit value
   * @throws Exception if something goes wrong.
   */
  public int compileAndRunReturnValue(final String adlName,
      final Map<Object, Object> additionalParams) throws Exception {
    final File compileADL = compileADL(adlName, additionalParams);
    assertNotNull("File returned by compiler is null", compileADL);
    return runTestAndReturnValue(compileADL, adlName);
  }

  private static class KillerThread extends Thread {
    final Process p;
    boolean       processKilled = false;
    boolean       processOK     = false;

    KillerThread(final Process p) {
      this.p = p;
    }

    @Override
    public void run() {
      synchronized (this) {
        try {
          this.wait(5000);
          if (!processOK) {
            p.destroy();
            processKilled = true;
          }
        } catch (final InterruptedException e) {
          return;
        }
      }
    }
  }

  public class StaticallyLoadedCeciliaAdlLauncher extends Launcher {

    protected Component compilerComponent;

    public StaticallyLoadedCeciliaAdlLauncher(final String outDir,
        final String sources) throws InvalidCommandLineException,
        CompilerInstantiationException {
      super(outDir, sources);

    }

    @Override
    protected Factory createCompiler(final Map<Object, Object> compilerContext,
        final CommandLine cmdLine) {

      System.setProperty("fractal.provider", Julia.class.getCanonicalName());

      Factory f = null;
      try {
        f = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
      } catch (final Exception e) {
        e.printStackTrace();
        throw new RuntimeException(
            "Unable to create FractalADL Factory with Fractal Backend");
      }

      this.compilerComponent = null;
      try {
        /* a Component, since it is is created by a Fractal backend Factory */
        compilerComponent = (Component) f.newComponent("CeciliaBasicFactory",
            compilerContext);
      } catch (final Exception e) {
        e.printStackTrace();
        throw new RuntimeException("Unable to create CeciliaADL compiler");
      }

      try {
        final LifeCycleController lifecycleController = (LifeCycleController) compilerComponent
            .getFcInterface("lifecycle-controller");

        lifecycleController.startFc();

        /* the Factory interface */
        final Factory compiler = (Factory) compilerComponent
            .getFcInterface("factory");

        return compiler;
      } catch (final Exception e) {
        e.printStackTrace();
        throw new RuntimeException("Unable to start CeciliaADL compiler");
      }

    }
  }

  /**
   * A subclass of the Launcher which uses a fully compliant Fractal compiler.
   * 
   * @author Alessio Pace
   */
  public class FractalBackendLauncher extends Launcher {

    protected Component compilerComponent;

    public FractalBackendLauncher(final Map<String, String> options)
        throws InvalidCommandLineException, CompilerInstantiationException {
      super(options);
    }

    public final Component getCompilerComponent() {
      return compilerComponent;
    }

    @Override
    protected Factory createCompiler(final Map<Object, Object> compilerContext,
        final CommandLine cmdLine) {

      System.setProperty("fractal.provider", Julia.class.getCanonicalName());

      Factory f = null;
      try {
        f = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
      } catch (final Exception e) {
        e.printStackTrace();
        throw new RuntimeException(
            "Unable to create FractalADL Factory with Fractal Backend");
      }

      /* to use the FractalPluginManager and related stuff */
      compilerContext.put(AbstractPluginManager.PLUGIN_FACTORY_BACKEND,
          "Fractal");

      this.compilerComponent = null;
      try {
        /* a Component, since it is is created by a Fractal backend Factory */
        compilerComponent = (Component) f.newComponent(
            Launcher.CECILIA_BASIC_COMPILER_ADL, compilerContext);
      } catch (final Exception e) {
        e.printStackTrace();
        throw new RuntimeException("Unable to create CeciliaADL compiler");
      }

      try {
        final LifeCycleController lifecycleController = (LifeCycleController) compilerComponent
            .getFcInterface("lifecycle-controller");

        lifecycleController.startFc();

        /* the Factory interface */
        final Factory compiler = (Factory) compilerComponent
            .getFcInterface("factory");

        return compiler;
      } catch (final Exception e) {
        e.printStackTrace();
        throw new RuntimeException("Unable to start CeciliaADL compiler");
      }

    }
  }
}
