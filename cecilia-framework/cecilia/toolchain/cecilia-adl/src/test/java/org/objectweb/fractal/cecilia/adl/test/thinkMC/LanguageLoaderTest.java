
package org.objectweb.fractal.cecilia.adl.test.thinkMC;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.cecilia.adl.implementations.LanguageLoader;

/**
 * Test that arguments and attributes in LanguageLoader definition are correctly
 * defined.
 * 
 * @author Alessio Pace
 */
public class LanguageLoaderTest extends TestCase {

  public static final String  DEFINITION = LanguageLoader.class
                                             .getCanonicalName();

  private Factory             factory;
  private Map<String, String> compilerContext;

  @Override
  public void setUp() throws Exception {
    /* get the bootstrap factory */
    this.factory = FactoryFactory.getFactory();

    compilerContext = new HashMap<String, String>();
  }

  public void testLoadingLanguageLoader() throws Exception {
    final LanguageLoader languageLoaderComponent = (LanguageLoader) this.factory
        .newComponent(DEFINITION, this.compilerContext);
    assertNotNull("Assert component not null", languageLoaderComponent);
  }

  public void testLoadingCeciliaLoader() throws Exception {
    final Object result = this.factory
        .newComponent("org.objectweb.fractal.cecilia.adl.CeciliaLoader",
            this.compilerContext);
    assertNotNull("Assert result not null", result);
  }

  public void testLoadingCeciliaBasicFactory() throws Exception {
    final Object result = this.factory.newComponent(
        "org.objectweb.fractal.cecilia.adl.CeciliaBasicFactory",
        this.compilerContext);
    assertNotNull("Assert result not null", result);
  }

}
