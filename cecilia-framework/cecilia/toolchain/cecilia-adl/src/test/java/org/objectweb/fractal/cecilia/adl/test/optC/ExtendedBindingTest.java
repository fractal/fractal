/***
 * Cecilia ADL Compiler
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.test.optC;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;
import org.objectweb.fractal.cecilia.adl.CeciliaADLConstants;
import org.objectweb.fractal.cecilia.adl.bindings.ExtendedBinding;

/**
 * Simple test case just to check that the {@link ExtendedBinding} interface and
 * the <code>DTD</code> grammar are coherent.
 * 
 * @author Alessio Pace
 */
public class ExtendedBindingTest {

  private XMLNodeFactory xmlNodeFactory;

  @Before
  public void setUp() throws Exception {
    this.xmlNodeFactory = new XMLNodeFactoryImpl();
  }

  @Test
  public void testCorrectDefinition() throws Exception {
    final ExtendedBinding binding = (ExtendedBinding) this.xmlNodeFactory
        .newXMLNode(CeciliaADLConstants.CECILIA_ADL_DTD,
            CeciliaADLConstants.BINDING_AST_NODE_NAME);

    assertNotNull("Assert not null", binding);
  }
}
