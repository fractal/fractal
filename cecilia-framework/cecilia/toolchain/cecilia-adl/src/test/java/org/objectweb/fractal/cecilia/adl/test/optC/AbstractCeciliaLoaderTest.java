/**
 * Cecilia
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.test.optC;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.cecilia.adl.Launcher;
import org.objectweb.fractal.cecilia.adl.plugin.AbstractPluginManager;
import org.objectweb.fractal.cecilia.adl.test.CeciliaAdlTestCase;

/**
 * This abstract class provides facilities to run integration tests on a loader
 * chain of Cecilia ADL.
 * 
 * @author Alessio Pace
 */
public abstract class AbstractCeciliaLoaderTest extends TestCase {

  /**
   * The Loader definition that contains the Loader component which will be used
   * for the tests, namely stored as reference into the {@link #loader}
   * variable.
   */
  public static final String CECILIAADL_IT_LOADER                                  = "ceciliaadl.it.loader";

  /**
   * Default Cecilia ADL Loader component definition file.
   */
  public static final String ORG_OBJECTWEB_FRACTAL_CECILIA_ADL_CECILIA_LOADER      = "org.objectweb.fractal.cecilia.adl.CeciliaLoader";

  /**
   * A definition which encloses the real Loader definition to be tested.
   */
  public static final String ORG_OBJECTWEB_FRACTAL_CECILIA_ADL_CECILIA_LOADER_TEST = "org.objectweb.fractal.cecilia.adl.CeciliaLoaderTest";

  /**
   * The {@link Loader} component to be tested.
   */
  protected Loader           loader                                                = null;

  @SuppressWarnings("unchecked")
  @Override
  protected final void setUp() throws Exception {
    final Factory factory = FactoryFactory.getFactory();
    final Map<String, Object> ctxt = new HashMap<String, Object>();
    ctxt.put(AbstractPluginManager.PLUGIN_FACTORY_BACKEND,
        Launcher.DEFAULT_PLUGIN_FACTORY_BACKEND);
    ctxt.put(CECILIAADL_IT_LOADER, getLoaderDefinition());

    /* add the content of the #getContextMap() method */
    ctxt.putAll(getContextMapToConstructLoader());

    final Map<String, Object> result = (Map<String, Object>) factory
        .newComponent(ORG_OBJECTWEB_FRACTAL_CECILIA_ADL_CECILIA_LOADER_TEST,
            ctxt);
    this.loader = (Loader) result.get("loader");
  }

  /**
   * Override this method in concrete test classes to specify a different
   * <code>Loader</code> definition file other than the default Cecilia one.
   * 
   * @return the default Cecilia <code>Loader</code> definition file.
   */
  public String getLoaderDefinition() {
    return ORG_OBJECTWEB_FRACTAL_CECILIA_ADL_CECILIA_LOADER;
  }

  /**
   * <em>Override</em> this method in concrete test classes to specify a non
   * empty context Map, for example to pass <em>arguments</em> values which
   * are needed in your specific {@link Loader} component.
   * 
   * @return the <em>context</em> <code>Map</code> that will be used when
   *         constructing the <code>Loader</code> component whose definition
   *         is the one given by {@link #getLoaderDefinition()};
   */
  public Map<String, Object> getContextMapToConstructLoader() {
    return new HashMap<String, Object>();
  }

  /**
   * @return a context Map with a minimalist set of parameters needed to use the
   *         Loader component to load some user supplied definitions.
   */
  public Map<Object, Object> getContextMapToBeUsedByLoader()
      throws RuntimeException {

    final Map<Object, Object> ctxt = new HashMap<Object, Object>();

    final String[] paths = System.getProperty(
        CeciliaAdlTestCase.SOURCE_PATH_PROPERTY_NAME,
        CeciliaAdlTestCase.DEFAULT_SOURCE_PATH).split(
        "" + File.pathSeparatorChar);

    final URL[] clUrls = new URL[paths.length + 1];
    try {
      clUrls[0] = new File(".").toURI().toURL();
      for (int i = 1; i < clUrls.length; i++) {
        clUrls[i] = new File(paths[i - 1]).toURI().toURL();
      }
    } catch (final Exception e) {
      e.printStackTrace();
      throw new RuntimeException("Unable to create URLs for source path", e);
    }

    final URLClassLoader cl = new URLClassLoader(clUrls, getClass()
        .getClassLoader());

    ctxt.put("classloader", cl);

    return ctxt;
  }

}
