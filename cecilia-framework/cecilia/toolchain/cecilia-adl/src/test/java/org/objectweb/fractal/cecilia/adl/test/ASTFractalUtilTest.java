/**
 * Cecilia
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.test;

import junit.framework.TestCase;

import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.ast.ASTFractalUtil;
import org.objectweb.fractal.adl.bindings.Binding;
import org.objectweb.fractal.adl.bindings.BindingContainer;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.implementations.ControllerContainer;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;
import org.objectweb.fractal.cecilia.adl.CeciliaADLConstants;
import org.objectweb.fractal.cecilia.adl.controllers.ExtendedController;

/**
 * Tests for {@link ASTFractalUtil}.
 * 
 * @author Alessio Pace
 */
public class ASTFractalUtilTest extends TestCase {

  private XMLNodeFactory xmlNodeFactory;

  private Definition     definition;

  @Override
  public void setUp() throws Exception {
    this.xmlNodeFactory = new XMLNodeFactoryImpl();

    definition = (Definition) this.xmlNodeFactory.newXMLNode(
        CeciliaADLConstants.CECILIA_ADL_DTD,
        CeciliaADLConstants.DEFINITION_AST_NODE_NAME);
  }

  public void testAddFcSubComponentIntoDefinition() throws Exception {

    final Component subComponent = (Component) this.xmlNodeFactory.newXMLNode(
        CeciliaADLConstants.CECILIA_ADL_DTD,
        CeciliaADLConstants.COMPONENT_AST_NODE_NAME);
    subComponent.setName("subCompName");

    ASTFractalUtil.addFcSubComponent((ComponentContainer) definition,
        (ComponentContainer) subComponent);
  }

  public void testAddFcSubComponentAlreadyPresentIntoDefinitionThrowsException()
      throws Exception {

    final Component subComponent = (Component) this.xmlNodeFactory.newXMLNode(
        CeciliaADLConstants.CECILIA_ADL_DTD,
        CeciliaADLConstants.COMPONENT_AST_NODE_NAME);
    subComponent.setName("subCompName");

    ASTFractalUtil.addFcSubComponent((ComponentContainer) definition,
        (ComponentContainer) subComponent);

    try {
      ASTFractalUtil.addFcSubComponent((ComponentContainer) definition,
          (ComponentContainer) subComponent);
      fail("Should raise an exception because the subcomponent was already added");
    } catch (final IllegalArgumentException e) {
      // OK
    }
  }

  public void testAddFcSubComponentIntoAPrimitiveComponentThrowsException()
      throws Exception {
    final Implementation impl = (Implementation) this.xmlNodeFactory
        .newXMLNode(CeciliaADLConstants.CECILIA_ADL_DTD,
            CeciliaADLConstants.IMPLEMENTATION_AST_NODE_NAME);

    ((ImplementationContainer) this.definition).setImplementation(impl);

    final Component comp = (Component) this.xmlNodeFactory.newXMLNode(
        CeciliaADLConstants.CECILIA_ADL_DTD,
        CeciliaADLConstants.COMPONENT_AST_NODE_NAME);

    try {
      ASTFractalUtil.addFcSubComponent((ComponentContainer) this.definition,
          comp);
      fail("Should raise exception when trying to add a sub component to a primitive component");
    } catch (final Exception e) {
      // OK
    }
  }

  public void testGetFcSubComponentsOfEmptyComponentContainer()
      throws Exception {

    final ComponentContainer[] components = ASTFractalUtil
        .getFcSubComponents((ComponentContainer) definition);

    assertNotNull(components);
    assertEquals(0, components.length);
  }

  public void testGetFcSubComponents() throws Exception {
    final Component subComponent = (Component) this.xmlNodeFactory.newXMLNode(
        CeciliaADLConstants.CECILIA_ADL_DTD,
        CeciliaADLConstants.COMPONENT_AST_NODE_NAME);

    ((ComponentContainer) definition).addComponent(subComponent);

    final ComponentContainer[] components = ASTFractalUtil
        .getFcSubComponents((ComponentContainer) definition);

    assertNotNull(components);
    assertEquals(1, components.length);
    assertSame(subComponent, components[0]);
  }

  public void testSetFcController() throws Exception {
    final ExtendedController controller = (ExtendedController) this.xmlNodeFactory
        .newXMLNode(CeciliaADLConstants.CECILIA_ADL_DTD,
            CeciliaADLConstants.CONTROLLER_AST_NODE_NAME);
    ASTFractalUtil.setFcControllerDesc((ComponentContainer) this.definition,
        controller);

    assertSame(controller, ((ControllerContainer) this.definition)
        .getController());
  }

  public void testListFc() throws Exception {

    // a client itf
    final TypeInterface itf1 = (TypeInterface) this.xmlNodeFactory.newXMLNode(
        CeciliaADLConstants.CECILIA_ADL_DTD,
        CeciliaADLConstants.INTERFACE_AST_NODE_NAME);
    itf1.setRole("client");
    itf1.setName("c");
    itf1.setSignature("x.y.Z");
    ((InterfaceContainer) this.definition).addInterface(itf1);

    // a server itf
    final TypeInterface itf2 = (TypeInterface) this.xmlNodeFactory.newXMLNode(
        CeciliaADLConstants.CECILIA_ADL_DTD,
        CeciliaADLConstants.INTERFACE_AST_NODE_NAME);
    itf2.setRole("server");
    itf2.setName("s");
    itf2.setSignature("x.y.Z");
    ((InterfaceContainer) this.definition).addInterface(itf2);

    final String[] result = ASTFractalUtil
        .listFc((ComponentContainer) this.definition);
    assertEquals(1, result.length);
    assertEquals("c", result[0]);
  }

  public void testFcInterfaceForNonPresentInterface() throws Exception {
    final TypeInterface result = ASTFractalUtil.getFcInterface(
        (ComponentContainer) this.definition, "nonExistingInterface");
    assertNull("Assert result is null", result);
  }

  public void testGetBindingsByToAttrValue() throws Exception {
    final Binding b1 = (Binding) this.xmlNodeFactory.newXMLNode(
        CeciliaADLConstants.CECILIA_ADL_DTD,
        CeciliaADLConstants.BINDING_AST_NODE_NAME);
    b1.setTo("subComp.r");
    ((BindingContainer) this.definition).addBinding(b1);

    final Binding b2 = (Binding) this.xmlNodeFactory.newXMLNode(
        CeciliaADLConstants.CECILIA_ADL_DTD,
        CeciliaADLConstants.BINDING_AST_NODE_NAME);
    b2.setTo("subComp.r");
    ((BindingContainer) this.definition).addBinding(b2);

    final Binding[] result = ASTFractalUtil.getBindingsByFullToAttrValue(
        (ComponentContainer) this.definition, "subComp.r");
    assertEquals(2, result.length);
    assertSame(b1, result[0]);
  }

  public void testGetBindingsByFromAttrValue() throws Exception {
    final Binding b1 = (Binding) this.xmlNodeFactory.newXMLNode(
        CeciliaADLConstants.CECILIA_ADL_DTD,
        CeciliaADLConstants.BINDING_AST_NODE_NAME);
    b1.setFrom("subComp.r");
    ((BindingContainer) this.definition).addBinding(b1);

    final Binding result = ASTFractalUtil.getBindingByFullFromAttrValue(
        (ComponentContainer) this.definition, "subComp.r");
    assertSame(b1, result);
  }
}
