/***
 * Cecilia ADL Compiler
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.perf;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.cecilia.adl.compiler.AbstractArchiveTask;
import org.objectweb.fractal.cecilia.adl.compiler.AbstractCompilationTask;
import org.objectweb.fractal.cecilia.adl.compiler.AbstractCompilationTaskFactory;
import org.objectweb.fractal.cecilia.adl.compiler.AbstractLinkTask;
import org.objectweb.fractal.cecilia.adl.file.SourceFile;
import org.objectweb.fractal.task.core.TaskException;

public class NullCompilationTaskFactory extends AbstractCompilationTaskFactory {

  @Override
  protected Component createArchiveTask(final Object id,
      final String archiveCommand, final String archiveFileName,
      final List<String> flags, final File outputDir,
      final Map<Object, Object> context) throws TaskException {
    return taskFactoryItf.newPrimitiveTask(new NullArchiveTask(archiveFileName,
        outputDir, flags), id);
  }

  @Override
  protected Component createCompileTask(final Object id,
      final String compileCommand, final List<String> flags, final File outputDir,
      final Map<Object, Object> context) throws TaskException {
    return taskFactoryItf.newPrimitiveTask(new NullCompilationTask(outputDir,
        flags), id);
  }

  @Override
  protected Component createLinkTask(final Object id, final String linkCommand,
      final String linkedFileName, final List<String> flags, final File outputDir,
      final int nbJobs, final Map<Object, Object> context) throws TaskException {
    return taskFactoryItf.newPrimitiveTask(new NullLinkTask(linkedFileName,
        outputDir, flags, nbJobs), id);
  }

  public static class NullArchiveTask extends AbstractArchiveTask {

    public NullArchiveTask(final String outputName, final File outputDir,
        final List<String> flags) {
      super(outputName, outputDir, flags);
    }

    @Override
    protected File archivate(final File outputFile,
        final Collection<File> inputFiles) {
      // do nothing
      return null;
    }
  }

  public static class NullCompilationTask extends AbstractCompilationTask {

    public NullCompilationTask(final File outputDir, final List<String> flags) {
      super(outputDir, flags);
    }

    @Override
    protected void compile(final SourceFile sourceFile, final File outputFile) {
      // do nothing
    }
  }

  public static class NullLinkTask extends AbstractLinkTask {

    public NullLinkTask(final String outputName, final File outputDir,
        final List<String> flags, final int threads) {
      super(outputName, outputDir, flags, threads);
    }

    @Override
    protected File link(final File outputFile, final Collection<File> inputFiles) {
      // do nothing.
      return null;
    }
  }
}
