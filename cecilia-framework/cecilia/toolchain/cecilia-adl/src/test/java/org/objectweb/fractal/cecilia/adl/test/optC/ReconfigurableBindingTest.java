/**
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.test.optC;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.cecilia.adl.test.CeciliaAdlTestCase;

/**
 * @author Alessio Pace
 */
public class ReconfigurableBindingTest extends CeciliaAdlTestCase {

  public void testSimpleReconfigurationOfClientItfs() throws Exception {
    this
        .compileAndRun("test.optC.bindings.reconfigurable.ClientServerReconfigurable");
  }

  public void testSimpleDynamicFalse() throws Exception {
    this.compileAndRun("test.optC.bindings.reconfigurable.ClientServerDynamicFalse");
  }

  public void testSimpleDynamicFalseInCaseOfCollectionInterface()
      throws Exception {
    try {
      tryCompileADL("test.optC.bindings.reconfigurable.ClientServerInvalidDynamicFalse2");
      fail("Should have thrown an " + ADLException.class
          + " because the dynamic='false' binding is invalid here");
    } catch (final ADLException e) {
      // OK
    }
  }

  public void testAttributes() throws Exception {
    this
        .compileAndRun("test.optC.bindings.reconfigurable.ClientServerWithAttributes");
  }
}
