/***
 * Cecilia ADL Compiler
 * Copyright (C) 2005 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.it;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

import org.objectweb.fractal.cecilia.adl.test.CeciliaAdlTestCase;

/**
 * Parent class for integration tests that compile ADL using the "ceciliac"
 * command. By convention sub classes of this class should ends with "IT" and
 * should not begin or end with "Test".
 */
public abstract class CeciliaCTestCase extends CeciliaAdlTestCase {

  protected static final String ASSEMBLY_BASEDIR_PROPERTY_NAME = "cecilia.assembly.basedir";

  protected File                ceciliac;
  protected File                assemblyBasedir;
  protected File                objdir;
  protected String[]            options;

  @Override
  public void setUp() throws Exception {
    assemblyBasedir = getAssemblyBasedir();
    ceciliac = new File(assemblyBasedir, "bin" + File.separator + "ceciliac");

    final Map<String, String> launcherOptions = getLauncherOptions();

    final File itBuilddir = new File(getBuildDir(), "it");
    if (!itBuilddir.exists()) {
      itBuilddir.mkdir();
    } else if (!itBuilddir.isDirectory()) {
      throw new Exception("Invalid  '" + BUILD_DIR_PROPERTY_NAME
          + "' system property: '" + itBuilddir + "' not a directory.");
    }
    launcherOptions.put("-o", itBuilddir.getAbsolutePath());

    objdir = new File(itBuilddir, "obj");

    options = new String[launcherOptions.size()];
    int i = 0;
    for (final Map.Entry<String, String> option : launcherOptions.entrySet()) {
      options[i] = option.getKey() + "=" + option.getValue();
      i++;
    }
  }

  @Override
  public File tryCompileADL(final String adlName,
      final Map<Object, Object> additionalParams) throws Exception {
    execCeciliaC(adlName);
    return toOutputFile(adlName);
  }

  protected int execCeciliaC(final String... args) throws Exception {
    final String[] fullArgs = new String[options.length + args.length + 2];
    fullArgs[0] = "sh";
    fullArgs[1] = ceciliac.getAbsolutePath();
    System.arraycopy(options, 0, fullArgs, 2, options.length);
    System.arraycopy(args, 0, fullArgs, 2 + options.length, args.length);

    final Process process = new ProcessBuilder(fullArgs).redirectErrorStream(
        true).start();
    new Thread() {
      @Override
      public void run() {
        // output error stream on the logger.
        final BufferedReader reader = new BufferedReader(new InputStreamReader(
            process.getInputStream()));
        try {
          String line;
          while ((line = reader.readLine()) != null) {
            System.out.println(line);
          }
          reader.close();
        } catch (final IOException e) {
          e.printStackTrace();
        }
      }
    }.start();

    return process.waitFor();
  }

  protected File toOutputFile(final String adlName) {
    return new File(objdir, adlName.replace('.', '_'));
  }

  protected File getAssemblyBasedir() throws Exception {
    String assemblyBasedirProp = System
        .getProperty(ASSEMBLY_BASEDIR_PROPERTY_NAME);
    if (assemblyBasedirProp == null) {
      throw new Exception("The \"" + ASSEMBLY_BASEDIR_PROPERTY_NAME
          + "\" property must be set.");
    }

    File file = new File(assemblyBasedirProp);
    if (!file.isDirectory()) {
      throw new Exception("Invalid \"" + ASSEMBLY_BASEDIR_PROPERTY_NAME
          + "\" property: " + file + " is not a directory.");
    }

    return file;
  }
}
