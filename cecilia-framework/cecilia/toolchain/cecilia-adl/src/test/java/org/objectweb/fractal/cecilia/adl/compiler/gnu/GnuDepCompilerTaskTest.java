/***
 * Cecilia ADL Compiler
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.compiler.gnu;

import java.io.File;
import java.net.URL;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import junit.framework.TestCase;

import org.objectweb.fractal.cecilia.adl.compiler.gnu.GnuDepCompilerTask.Rule;

public class GnuDepCompilerTaskTest extends TestCase {

  static final String DEP_FILE_PREFIX = "org/objectweb/fractal/cecilia/adl/compiler/gnu/";

  GnuDepCompilerTask  task;
  File                outDir;

  @Override
  protected void setUp() throws Exception {
    task = new GnuDepCompilerTask("gcc", outDir, null);
    outDir = new File("outdir");
  }

  protected File getDepFile(final String name) throws Exception {
    final URL url = getClass().getClassLoader().getResource(
        DEP_FILE_PREFIX + name);
    File f;

    if (url == null) {
      throw new Exception("Can't find dep file \"" + DEP_FILE_PREFIX + name
          + "\"");
    }
      
    try {
      return new File(url.toURI());
    } catch(URISyntaxException e) {
      // This URL is not a valid URI.
      // Fall back to new File(String), hoping that it will work properly...
      return new File(url.getPath());
    }
  }

  public void testDepParser1() throws Exception {
    final Collection<File> deps = task.getDependencies(getDepFile("dep1.d"),
        new File(outDir, "outfile.o"));

    assertNotNull(deps);
    final Iterator<File> iter = deps.iterator();
    assertEquals(new File("file1"), iter.next());
    assertEquals(new File("file2"), iter.next());
    assertEquals(new File("file3"), iter.next());
  }

  public void testDepParser2() throws Exception {
    final Collection<File> deps = task.getDependencies(getDepFile("dep2.d"),
        new File(outDir, "outfile.o"));

    assertNotNull(deps);
    final Iterator<File> iter = deps.iterator();
    assertEquals(new File("file1"), iter.next());
    assertEquals(new File("file2"), iter.next());
    assertEquals(new File("file3"), iter.next());
  }

  public void testDepParser3() throws Exception {
    final List<Rule> rules = task.parseRules(getDepFile("dep3.d"));

    assertNotNull(rules);
    assertEquals(1, rules.size());
    final Rule rule = rules.get(0);
    assertNotNull(rule);
    assertEquals(
        "C:\\work\\leclercm\\cecilia-framework\\cecilia\\toolchain\\cecilia-adl\\target\\test-build\\obj\\test\\thinkMC\\simple\\SimpleMC\\test.o",
        rule.target);

    assertNotNull(rule.dependencies);

    final Iterator<String> iter = rule.dependencies.iterator();

    assertEquals(
        "C:\\work\\leclercm\\cecilia-framework\\cecilia\\toolchain\\cecilia-adl\\target\\test-build\\adl\\test_thinkMC_simple_SimpleMC_test.adl.c",
        iter.next());
    assertEquals(
        "C:\\work\\leclercm\\cecilia-framework\\cecilia\\toolchain\\cecilia-adl\\target\\test-build\\adl\\/test_thinkMC_simple_SimpleMC_test.adl.h",
        iter.next());
    assertEquals(
        "C:/work/leclercm/cecilia-framework/cecilia/toolchain/cecilia-adl/target/test-build/idl/fractal\\api\\Component.idl.h",
        iter.next());
    assertEquals(
        "C:/work/leclercm/cecilia-framework/cecilia/toolchain/cecilia-adl/target/test-build/idl/fractal\\api\\ErrorConst.idl.h",
        iter.next());
    assertEquals(
        "C:/work/leclercm/cecilia-framework/cecilia/toolchain/cecilia-adl/target/test-resources/cecilia_types.h",
        iter.next());
    assertEquals(
        "C:/work/leclercm/cecilia-framework/cecilia/toolchain/cecilia-adl/target/test-build/idl/test\\thinkMC\\api\\Run.idl.h",
        iter.next());
    assertEquals(
        "C:/work/leclercm/cecilia-framework/cecilia/toolchain/cecilia-adl/target/test-resources/cecilia_types.h",
        iter.next());
    assertEquals(
        "C:/work/leclercm/cecilia-framework/cecilia/toolchain/cecilia-adl/target/test-build/idl/test\\thinkMC\\api\\RunVFTTemplate.h",
        iter.next());
    assertEquals(
        "C:/work/leclercm/cecilia-framework/cecilia/toolchain/cecilia-adl/target/test-build/idl/test/thinkMC/api/Run.idl.h",
        iter.next());
    assertEquals(
        "C:\\work\\leclercm\\cecilia-framework\\cecilia\\toolchain\\cecilia-adl\\src\\test\\resources\\test\\thinkMC\\simple\\SimpleMC.c",
        iter.next());
    assertEquals(
        "C:/work/leclercm/cecilia-framework/cecilia/toolchain/cecilia-adl/target/test-resources/cecilia.h",
        iter.next());
    assertEquals(
        "C:/work/leclercm/cecilia-framework/cecilia/toolchain/cecilia-adl/target/test-build/idl/test\\thinkMC\\api\\RunVFTTemplate.h",
        iter.next());
    assertFalse(iter.hasNext());
  }

}
