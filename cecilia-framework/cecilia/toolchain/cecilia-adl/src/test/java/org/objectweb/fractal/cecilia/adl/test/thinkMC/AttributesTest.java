/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.test.thinkMC;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.cecilia.adl.test.CeciliaAdlTestCase;

/**
 * Test ADL in test.thinkMC.attributes package.
 */
public class AttributesTest extends CeciliaAdlTestCase {

  public void testAttributeMC() throws Exception {
    compileAndRun("test.thinkMC.attributes.AttributeMC");
  }

  public void testUninitializedAttributeMC() throws Exception {
    compileAndRun("test.thinkMC.attributes.UninitializedAttributeMC");
  }

  public void testZeroValueAttributeUsingAttributesMacro() throws Exception {
    compileAndRun("test.thinkMC.attributes.ZeroAttributeMC");
  }

  public void testZeroValueAttributeUsingAttributeController() throws Exception {
    compileAndRun("test.thinkMC.attributes.ZeroAttributeUsingAttributeControllerMC");
  }

  public void testReadConstField() throws Exception {
    compileAndRun("test.thinkMC.attributes.ConstAttributeMC");
  }

  public void testTryToReassignIntConstField() throws Exception {
    try {
      tryCompileADL("test.thinkMC.attributes.InvalidReassignIntConstAttributeMC");
      fail("Should throw a compilation exception while reassigning int variable");
    } catch (final ADLException e) {
      //
    }
  }

  public void testTryToReassignIntConstFieldUsingAC() throws Exception {

    compileAndRun("test.thinkMC.attributes.InvalidReassignIntConstAttributeUsingAC");

  }

  public void testTryToReassignStringConstField() throws Exception {
    try {
      tryCompileADL("test.thinkMC.attributes.InvalidReassignStringConstAttributeMC");
      fail("Should throw a compilation exception while reassigning char* variable");
    } catch (final ADLException e) {
      // OK
    }
  }

  public void testTryToReassignRecordConstField() throws Exception {
    try {
      tryCompileADL("test.thinkMC.attributes.InvalidReassignRecordConstAttributeMC");
      fail("Should throw a compilation exception while reassigning record variable");
    } catch (final ADLException e) {
      // OK
    }
  }

  public void testNontrivialAttributeInitializations() throws Exception {
    compileAndRun("test.thinkMC.attributes.NontrivialAttributeInitializations");
  }
}
