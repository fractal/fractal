/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.test.optC;

import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.ast.ASTFractalUtil;
import org.objectweb.fractal.adl.bindings.Binding;
import org.objectweb.fractal.adl.bindings.BindingContainer;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil;
import org.objectweb.fractal.cecilia.adl.bindings.BindingResolverLoader;
import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;

/**
 * Tests to check the beahviour of the {@link BindingResolverLoader} component
 * as part of the whole Loader chain usage.
 * 
 * @author Alessio Pace
 */
public class BindingResolverLoaderTest extends AbstractCeciliaLoaderTest {

  /**
   */
  public static final String RUN_SERVER_ITF_NAME  = "r";
  /**
   */
  public static final String RUN_CLIENT_ITF_NAME  = "c";
  /**
   */
  public static final String PRIMITIVE1_COMP_NAME = "primitive1";

  /**
   * Test that the {@link InterfaceDecorationUtil#BOUND_TO_DECORATION}
   * references the server interface inside the composite, and not on the
   * composite itself.
   */
  public void testDefinitionWithTwoComposites() throws Exception {
    final Definition definition = this.loader.load(
        "test.optC.bindings.DefinitionWithTwoComposites",
        getContextMapToBeUsedByLoader());

    final ComponentContainer composite1 = ASTFractalUtil
        .getFcSubComponentByName((ComponentContainer) definition, "composite1");
    assertNotNull("composite1");

    final ComponentContainer primitive1 = ASTFractalUtil
        .getFcSubComponentByName(composite1, PRIMITIVE1_COMP_NAME);
    assertNotNull(primitive1);

    final TypeInterface primitive1RunClientItf = ASTFractalUtil.getFcInterface(
        primitive1, RUN_CLIENT_ITF_NAME);
    assertNotNull(primitive1RunClientItf);

    final TypeInterface primitive1RunClientItfBoundToItf = (TypeInterface) InterfaceDecorationUtil
        .getBoundTo(primitive1RunClientItf);
    assertNotNull(primitive1RunClientItfBoundToItf);

    final ComponentContainer composite2 = ASTFractalUtil
        .getFcSubComponentByName((ComponentContainer) definition, "composite2");
    assertNotNull(composite2);

    final ComponentContainer primitive2 = ASTFractalUtil
        .getFcSubComponentByName(composite2, "primitive2");
    assertNotNull(primitive2);

    final TypeInterface primitive2RunServerItf = ASTFractalUtil.getFcInterface(
        primitive2, RUN_SERVER_ITF_NAME);
    assertNotNull(primitive2RunServerItf);

    /*
     * assert the client interface of primitive inside the first composite is
     * bound to the server interface of the primitive inside the second
     * composite
     */
    assertEquals(primitive2RunServerItf, primitive1RunClientItfBoundToItf);
  }

  public void testDefinitionWithTwoPrimitivesAndTwoComposites()
      throws Exception {
    final Definition definition = this.loader.load(
        "test.optC.bindings.DefinitionWithTwoPrimitivesAndTwoComposites",
        getContextMapToBeUsedByLoader());
    assertNotNull(definition);

    /* top level primitive1 */
    final ComponentContainer topLevelPrimitive1 = ASTFractalUtil
        .getFcSubComponentByName((ComponentContainer) definition,
            PRIMITIVE1_COMP_NAME);
    assertNotNull(topLevelPrimitive1);
    final TypeInterface topLevelPrimitive1RunClientItf = ASTFractalUtil
        .getFcInterface(topLevelPrimitive1, RUN_CLIENT_ITF_NAME);
    assertNotNull(topLevelPrimitive1RunClientItf);
    final TypeInterface topLevelPrimitive1RunClientItfBoundedToItf = (TypeInterface) InterfaceDecorationUtil
        .getBoundTo(topLevelPrimitive1RunClientItf);
    assertNotNull(topLevelPrimitive1RunClientItfBoundedToItf);

    /* composite1 */
    final ComponentContainer composite1 = ASTFractalUtil
        .getFcSubComponentByName((ComponentContainer) definition, "composite1");
    assertNotNull(composite1);

    // --------------
// final TypeInterface[] composite1Itfs = ASTFractalUtil
// .getFcInterfaces(composite1);
// assertNotNull(composite1Itfs);
// for (final TypeInterface ti : composite1Itfs) {
// final Node n = ((Node) ti);
// System.out.println("\n\n" + n.astGetDecorations());
// }
    // -----------

    /* primitive1 inside composite1 */
    final ComponentContainer primitive1InsideComposite1 = ASTFractalUtil
        .getFcSubComponentByName(composite1, PRIMITIVE1_COMP_NAME);
    assertNotNull(primitive1InsideComposite1);
    final TypeInterface primitive1InsideComposite1RunServerItf = ASTFractalUtil
        .getFcInterface(primitive1InsideComposite1, RUN_SERVER_ITF_NAME);
    assertNotNull(primitive1InsideComposite1RunServerItf);

    /*
     * check that the top level primitive1 client interface is bound to the
     * server interface of the primitive1 inside the composite1
     */
    assertEquals(primitive1InsideComposite1RunServerItf,
        topLevelPrimitive1RunClientItfBoundedToItf);
    final TypeInterface primitive1InsideComposite1RunClientItf = ASTFractalUtil
        .getFcInterface(primitive1InsideComposite1, RUN_CLIENT_ITF_NAME);
    assertNotNull(primitive1InsideComposite1RunClientItf);

    final TypeInterface primitive1InsideComposite1ClientItfBoundedTo = (TypeInterface) InterfaceDecorationUtil
        .getBoundTo(primitive1InsideComposite1RunClientItf);
    assertNotNull(primitive1InsideComposite1ClientItfBoundedTo);

    // -------------
// final TypeInterface[] primitive1InsideComposite1Itfs = ASTFractalUtil
// .getFcInterfaces(primitive1InsideComposite1);
// assertNotNull(primitive1InsideComposite1Itfs);
// for (final TypeInterface ti : primitive1InsideComposite1Itfs) {
// final Node n = ((Node) ti);
// System.out.println("\n\n" + n.astGetDecorations());
// }
    // -------------

    /* composite 2 */
    final ComponentContainer composite2 = ASTFractalUtil
        .getFcSubComponentByName((ComponentContainer) definition, "composite2");
    assertNotNull(composite2);

    /* primitive1 inside composite2 */
    final ComponentContainer primitive1InsideComposite2 = ASTFractalUtil
        .getFcSubComponentByName(composite2, PRIMITIVE1_COMP_NAME);
    assertNotNull(primitive1InsideComposite2);

    final TypeInterface primitive1InsideComposite2RunServerItf = ASTFractalUtil
        .getFcInterface(primitive1InsideComposite2, RUN_SERVER_ITF_NAME);
    assertNotNull(primitive1InsideComposite2RunServerItf);

    /*
     * check that the primitive1 inside composite1 client interface is bound to
     * the server interface of the primitive1 inside composite2.
     */
    assertEquals(primitive1InsideComposite2RunServerItf,
        primitive1InsideComposite1ClientItfBoundedTo);

    // TODO complete checks of the boundTo

  }

  public void testTwoPrimitivesAndAComposite() throws Exception {
    final Definition definition = this.loader.load(
        "test.optC.bindings.DefinitionWithTwoPrimitivesAndAComposite",
        getContextMapToBeUsedByLoader());
    assertNotNull(definition);

    /* top level primitive1 */
    final ComponentContainer topLevelPrimitive1 = ASTFractalUtil
        .getFcSubComponentByName((ComponentContainer) definition,
            PRIMITIVE1_COMP_NAME);
    assertNotNull(topLevelPrimitive1);
    final TypeInterface topLevelPrimitive1RunClientItf = ASTFractalUtil
        .getFcInterface(topLevelPrimitive1, RUN_CLIENT_ITF_NAME);
    assertNotNull(topLevelPrimitive1RunClientItf);
    final TypeInterface topLevelPrimitive1RunClientItfBoundedToItf = (TypeInterface) InterfaceDecorationUtil
        .getBoundTo(topLevelPrimitive1RunClientItf);
    assertNotNull(topLevelPrimitive1RunClientItfBoundedToItf);

    /* composite0 */
    final ComponentContainer composite0 = ASTFractalUtil
        .getFcSubComponentByName((ComponentContainer) definition, "composite0");
    assertNotNull(composite0);

    /* composite1 */
    final ComponentContainer composite1 = ASTFractalUtil
        .getFcSubComponentByName(composite0, "composite1");
    assertNotNull(composite1);

    /* primitive1 inside composite1 */
    final ComponentContainer primitive1InsideComposite1 = ASTFractalUtil
        .getFcSubComponentByName(composite1, PRIMITIVE1_COMP_NAME);
    assertNotNull(primitive1InsideComposite1);
    final TypeInterface primitive1InsideComposite1RunServerItf = ASTFractalUtil
        .getFcInterface(primitive1InsideComposite1, RUN_SERVER_ITF_NAME);
    assertNotNull(primitive1InsideComposite1RunServerItf);

    /*
     * check that the top level primitive1 client interface is bound to the
     * server interface of the primitive1 inside the composite1
     */
    assertEquals(primitive1InsideComposite1RunServerItf,
        topLevelPrimitive1RunClientItfBoundedToItf);
    final TypeInterface primitive1InsideComposite1RunClientItf = ASTFractalUtil
        .getFcInterface(primitive1InsideComposite1, RUN_CLIENT_ITF_NAME);
    assertNotNull(primitive1InsideComposite1RunClientItf);

    final TypeInterface primitive1InsideComposite1ClientItfBoundedTo = (TypeInterface) InterfaceDecorationUtil
        .getBoundTo(primitive1InsideComposite1RunClientItf);
    assertNotNull(primitive1InsideComposite1ClientItfBoundedTo);

    /* composite 2 */
    final ComponentContainer composite2 = ASTFractalUtil
        .getFcSubComponentByName(composite0, "composite2");
    assertNotNull(composite2);

    /* primitive1 inside composite2 */
    final ComponentContainer primitive1InsideComposite2 = ASTFractalUtil
        .getFcSubComponentByName(composite2, PRIMITIVE1_COMP_NAME);
    assertNotNull(primitive1InsideComposite2);

    final TypeInterface primitive1InsideComposite2RunServerItf = ASTFractalUtil
        .getFcInterface(primitive1InsideComposite2, RUN_SERVER_ITF_NAME);
    assertNotNull(primitive1InsideComposite2RunServerItf);

    /*
     * check that the primitive1 inside composite1 client interface is bound to
     * the server interface of the primitive1 inside composite2.
     */
    assertEquals(primitive1InsideComposite2RunServerItf,
        primitive1InsideComposite1ClientItfBoundedTo);

    // TODO complete checks of the boundTo

    /* verify the decorations on Bindings */
    final Binding[] topLevelBindings = ((BindingContainer) definition)
        .getBindings();

    for (final Binding b : topLevelBindings) {

      /* it is the <binding client="this.r" server="primitive1.r" */
      if (b.getFrom().startsWith("this.")) {
        final TypeInterface thisR = (TypeInterface) BindingDecorationUtil
            .getClientInterface(b);
        assertNotNull(thisR);
        assertSame(definition, InterfaceDecorationUtil.getContainer(thisR));

        final TypeInterface primitive1R = (TypeInterface) BindingDecorationUtil
            .getServerInterface(b);
        assertNotNull(primitive1R);
        assertSame(topLevelPrimitive1, InterfaceDecorationUtil
            .getContainer(primitive1R));
      }
      // other bindings decorations could be tested as well
    }

  }
}
