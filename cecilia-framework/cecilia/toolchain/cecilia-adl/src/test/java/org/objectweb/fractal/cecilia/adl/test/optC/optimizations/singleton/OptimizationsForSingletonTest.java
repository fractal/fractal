/***
 * Cecilia ADL Compiler
 * Copyright (C) 2005-2008 STMicroelectronics
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.test.optC.optimizations.singleton;

import java.io.File;

import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;
import org.objectweb.fractal.cecilia.adl.test.CeciliaAdlTestCase;
import org.objectweb.fractal.cecilia.adl.types.TypeDecorationUtil;

/**
 * @author Alessio Pace
 */
public class OptimizationsForSingletonTest extends CeciliaAdlTestCase {

  /**
   * Prefix for singleton tests.
   */
  public static final String TEST_SINGLETON_PREFIX = "test.optC.optimizations.singleton.";

  public void testSingleton() throws Exception {
    this.compileAndRun(TEST_SINGLETON_PREFIX + "Singleton");
  }

  public void testNotSingleton() throws Exception {
    this.compileAndRun(TEST_SINGLETON_PREFIX + "NotSingleton");
  }

  /**
   * XXX this test may be refactored in the future due to the hack nature of the
   * {@link TypeDecorationUtil#CECILIA_DISABLE_SINGLETONS_OPTIMIZATION} property..
   * 
   * @throws Exception
   */
  public void testWithExplicitDisableSingletonSystemProperty() throws Exception {
    try {
      /* set the property to disable singletons */
      System.setProperty(TypeDecorationUtil.CECILIA_DISABLE_SINGLETONS_OPTIMIZATION, "true");

      final File executable = this.compileADL(TEST_SINGLETON_PREFIX
          + "Singleton2");
      final int r = runTestAndReturnValue(executable,
          "test.optC.optimizations.Singleton2");

      /* assert it exited with a not OK status code */
      assertTrue(r != 0);
    }
    finally {
      /* change the property */
      System.setProperty(TypeDecorationUtil.CECILIA_DISABLE_SINGLETONS_OPTIMIZATION, "false");
    }
  }

  /**
   * The "server" component has an "r" test.optC.api.Run interface which should be
   * generated as static, and the "client" component's interface bound to it
   * should use it accordingly in the same way.
   */
  public void testStaticInterfaces() throws Exception {
    try {
      System.setProperty(InterfaceDecorationUtil.CECILIA_ENABLE_THIS_OPTIMIZATION,
          "true");

      final String adl = TEST_SINGLETON_PREFIX + "StaticInterfaces";
      final File executable = this.compileADL(adl);
      final int r = runTestAndReturnValue(executable, adl);

      /* assert it exited with a not OK status code */
      assertTrue(r == 0);
    }
    finally {
      System.setProperty(InterfaceDecorationUtil.CECILIA_ENABLE_THIS_OPTIMIZATION,
          "false");
    }
  }
}
