/***
 * Cecilia ADL Compiler
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.perf;

import static java.lang.System.currentTimeMillis;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.Definition;

public class LoaderProbe extends AbstractLoader implements Probe {

  private long time  = 0;
  private long count = 0;

  public Definition load(final String definition,
      final Map<Object, Object> context) throws ADLException {
    final long begin = currentTimeMillis();
    final Definition result = clientLoader.load(definition, context);
    time += currentTimeMillis() - begin;
    count++;
    return result;
  }

  public long getCount() {
    return count;
  }

  public long getTime() {
    return time;
  }

  public void reset() {
    time = 0;
    count = 0;
  }
}
