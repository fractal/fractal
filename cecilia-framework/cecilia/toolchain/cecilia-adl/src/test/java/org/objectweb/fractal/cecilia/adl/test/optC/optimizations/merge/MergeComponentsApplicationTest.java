/***
 * Cecilia
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.test.optC.optimizations.merge;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.cecilia.adl.components.merge.MergeUtil;
import org.objectweb.fractal.cecilia.adl.test.CeciliaAdlTestCase;

/**
 * These tests have as input definitions using (somewhere) the
 * <code>merge</code> attribute, and verify that the compilation and the
 * execution work fine in case of merging.
 * 
 * @author Alessio Pace
 */
public class MergeComponentsApplicationTest extends CeciliaAdlTestCase {
  /**
   * prefix for fixture files.
   */
  public static final String MERGE_FIXTURES_PREFIX = "test.optC.optimizations.merge.";
  public static final String MERGE_ATTRIBUTES_FIXTURES_PREFIX = "test.optC.optimizations.merge.attributes.";

  public void testExplicitlyMerged1() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX
        + "Test_ExplicitlyMergedPrimitive1");
  }

  public void testExplicitlyMerged2() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX
        + "Test_ExplicitlyMergedPrimitive2");
  }

  public void testExplicitlyMerged3() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX
        + "Test_ExplicitlyMergedPrimitive3");
  }

  public void testExplicitlyMerged4() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX
        + "Test_ExplicitlyMergedPrimitive4");
  }

  public void testExplicitlyMerged5() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX
        + "Test_ExplicitlyMergedPrimitive5");
  }

  public void testComposite1() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite1");
  }

  public void testComposite2() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite2");
  }

  public void testComposite2bis() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite2bis");
  }

  public void testComposite3() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite3");
  }

  public void testComposite4() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite4");
  }

  public void testComposite5() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite5");
  }

  public void testComposite6() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite6");
  }

  public void testComposite7WithoutMerge() throws Exception {
    try {
      /* set the property to disable merge */
      System.setProperty(MergeUtil.CECILIA_DISABLE_MERGE_OPTIMIZATION, "true");

      super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite7MergeALL");
    }
    finally {
      /* change the property */
      System.setProperty(MergeUtil.CECILIA_DISABLE_MERGE_OPTIMIZATION, "false");
    }
  }

  public void testComposite7MergeCTWO() throws Exception {
    final Map<Object, Object> additionalParams = new HashMap<Object, Object>();
    additionalParams.put("composite7", MERGE_FIXTURES_PREFIX + "Composite7"
        + "MergeCTWO");
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite7",
        additionalParams);
  }

  public void testComposite7MergeCTHREE() throws Exception {
    final Map<Object, Object> additionalParams = new HashMap<Object, Object>();
    additionalParams.put("composite7", MERGE_FIXTURES_PREFIX + "Composite7"
        + "MergeCTHREE");
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite7",
        additionalParams);
  }

  public void testComposite7MergeCTHREEandCTWO() throws Exception {
    final Map<Object, Object> additionalParams = new HashMap<Object, Object>();
    additionalParams.put("composite7", MERGE_FIXTURES_PREFIX + "Composite7"
        + "MergeCTHREEandCTWO");
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite7",
        additionalParams);
  }

  public void testComposite8() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite8");
  }

  public void testComposite9() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite9");
  }

  public void testComposite10() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite10");
  }

  public void testComposite11() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite11");
  }

  // ================================
  // RECURSIVE MERGE TESTS
  // ================================

  public void testComposite7MergeCONE() throws Exception {
    final Map<Object, Object> additionalParams = new HashMap<Object, Object>();
    additionalParams.put("composite7", MERGE_FIXTURES_PREFIX + "Composite7"
        + "MergeCONE");
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite7",
        additionalParams);
  }

  public void testComposite7MergeALL() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite7MergeALL");
  }

  public void testComposite12() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite12");
  }

  public void testComposite13() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite13");
  }

  public void testComposite14() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite14");
  }

  public void testComposite15() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite15");
  }

  public void testComposite16() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite16");
  }

  public void testComposite17() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite17");
  }

  public void testComposite18() throws Exception {
    super.compileAndRun(MERGE_FIXTURES_PREFIX + "Test_Composite18");
  }

  // ================================
  // ATTRIBUTES MERGE TESTS
  // ================================
  public void testAttributeMerge1() throws Exception {
    super.compileAndRun(MERGE_ATTRIBUTES_FIXTURES_PREFIX
        + "Test_AttributeMerge1");
  }

  public void testAttributeRecursiveMerge1() throws Exception {
    super.compileAndRun(MERGE_ATTRIBUTES_FIXTURES_PREFIX
        + "Test_AttributeRecursiveMerge1");
  }
  
  public void testAttributeMergeWithNameClash() throws Exception {
    try {
      tryCompileADL(MERGE_ATTRIBUTES_FIXTURES_PREFIX
          + "Test_AttributeMergeWithNameClash");
      fail("Should have thrown an " + ADLException.class
          + " because two of the component's attributes are supposed to have the same name");
    } catch (final ADLException e) {
      // OK
    }
  }
}
