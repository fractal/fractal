/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 * Contributor: Alessio Pace (enums)
 * Contributor: Lionel Debroux (unions)
 */

package org.objectweb.fractal.cecilia.adl.test.optC;

import static org.objectweb.fractal.cecilia.adl.idl.util.Util.getComplexType;
import static org.objectweb.fractal.cecilia.adl.idl.util.Util.isComplexType;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.error.ErrorTemplate;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;
import org.objectweb.fractal.cecilia.adl.idl.BasicIDLLoader;
import org.objectweb.fractal.cecilia.adl.idl.IDLErrors;
import org.objectweb.fractal.cecilia.adl.idl.ast.ComplexType;
import org.objectweb.fractal.cecilia.adl.idl.ast.EnumDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Field;
import org.objectweb.fractal.cecilia.adl.idl.ast.FieldContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.Import;
import org.objectweb.fractal.cecilia.adl.idl.ast.ImportContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.InterfaceDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Method;
import org.objectweb.fractal.cecilia.adl.idl.ast.MethodContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.Parameter;
import org.objectweb.fractal.cecilia.adl.idl.ast.ParameterContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.RecordDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Type;
import org.objectweb.fractal.cecilia.adl.idl.ast.UnionDefinition;
import org.objectweb.fractal.cecilia.adl.idl.util.Util;
import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;

public class IDLLoaderTest extends TestCase {
  /**
   * 
   */
  public static final String USING_ENUM_SIGNATURE      = "test.optC.idl.enums.UsingEnum";
  /**
   * 
   */
  public static final String USING_UNION_SIGNATURE      = "test.optC.idl.unions.UsingUnion";
  /**
   * 
   */
  public static final String ENUM_E1_SIGNATURE         = "test.optC.idl.enums.E1";
  /**
   * 
   */
  public static final String UNION_U1_SIGNATURE         = "test.optC.idl.unions.U1";
  protected BasicIDLLoader   loader;
  public static final String INTERFACES_PACKAGE_PREFIX = "test.optC.idl.interfaces.";

  @Override
  protected void setUp() throws Exception {
    loader = new BasicIDLLoader();
    loader.nodeFactoryItf = new XMLNodeFactoryImpl();
  }

  public void testA1() throws Exception {
    final InterfaceDefinition itf = loadInterface("A1", null);
    final Import[] imports = ((ImportContainer) itf).getImports();
    assertNotNull("Assert imports not null", imports);
    /* just "R" among the imports */
    assertEquals("Assert number of imports", 1, imports.length);
  }

  public void testJ1() throws Exception {
    final InterfaceDefinition itf = loadInterface("J1", null);
    checkItfContent(itf, new String[]{"fieldI", "fieldJ"}, new String[]{
        "methodI", "methodJ"});
  }

  public void testJ2() throws Exception {
    try {
      loadInterface("J2", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(IDLErrors.DUPLICATED_FIELD_NAME, e.getError().getTemplate());
    }
  }

  public void testJ3() throws Exception {
    try {
      loadInterface("J3", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(IDLErrors.DUPLICATED_METHOD_NAME, e.getError().getTemplate());
    }
  }

  public void testK() throws Exception {
    final InterfaceDefinition itf = loadInterface("K", null);
    checkItfContent(itf, new String[]{"fieldI", "fieldK"}, new String[]{
        "methodI", "methodK"});
  }

  public void testN() throws Exception {
    try {
      loadInterface("N", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(IDLErrors.INHERITANCE_CYCLE, e.getError().getTemplate());
    }
  }

  public void testQ() throws Exception {
    final InterfaceDefinition itf = loadInterface("Q", null);
    checkItfContent(itf, new String[]{"fieldP", "fieldQ"}, new String[]{
        "methodP", "methodQ"});
  }

  public void testErroneousI() throws Exception {
    loadErroneousIDL("ErroneousI1", IDLErrors.SYNTAX_ERROR);
    loadErroneousIDL("ErroneousI2", IDLErrors.SYNTAX_ERROR);
    loadErroneousIDL("ErroneousI3", IDLErrors.SYNTAX_ERROR);
    loadErroneousIDL("ErroneousI4", IDLErrors.SYNTAX_ERROR);
    loadErroneousIDL("ErroneousI5", IDLErrors.SYNTAX_ERROR);
  }

  @SuppressWarnings("unchecked")
  public void testInheritancePathDecoration() throws Exception {
    final InterfaceDefinition itf = loadInterface("J4", null);
    final List<String> decoration = (List<String>) ((Node) itf)
        .astGetDecoration(InterfaceDecorationUtil.INHERITANCE_PATH_DECORATION);

    assertNotNull("Assert inheritance path decoration not null", decoration);
    assertEquals("Assert inheritance path size", 3, decoration.size());
    assertEquals("Assert first element", INTERFACES_PACKAGE_PREFIX + "J4",
        decoration.get(0));
    assertEquals("Assert second element", INTERFACES_PACKAGE_PREFIX + "J1",
        decoration.get(1));
    assertEquals("Assert third element", INTERFACES_PACKAGE_PREFIX + "I",
        decoration.get(2));

  }

  public void testInterfaceUsingEnumTypes() throws Exception {
    final InterfaceDefinition itf = (InterfaceDefinition) loader.loadInterface(
        USING_ENUM_SIGNATURE, new HashMap<Object, Object>());
    assertNotNull("Assert loader interface is not null", itf);

    final Import[] imports = ((ImportContainer) itf).getImports();
    assertNotNull("Assert imports not null", imports);

    assertEquals("Assert number of imports", 2, imports.length);
    assertEquals("Assert import name", ENUM_E1_SIGNATURE, imports[0].getName());
    assertEquals("Assert second import name", USING_ENUM_SIGNATURE, imports[1]
        .getName());

    final Method[] methods = ((MethodContainer) itf).getMethods();

    final Method firstMethod = methods[0];
    final ComplexType enumParameterType = (ComplexType) Util
        .getContainedType(firstMethod.getParameters()[0]);
    assertNotNull("Assert enum parameter type not null", enumParameterType);
    final EnumDefinition enumDefinition = (EnumDefinition) ((IDLDefinitionContainer) enumParameterType)
        .getIDLDefinition();
    assertNotNull(
        "Assert EnumDefinition contained by parameter type is not null",
        enumDefinition);

    final Method secondMethod = methods[1];
    final ComplexType enumReturnType = secondMethod.getComplexType();
    assertNotNull("Assert enum return type not null", enumReturnType);
    final EnumDefinition enumDefinitionContainedByReturnType = (EnumDefinition) ((IDLDefinitionContainer) enumReturnType)
        .getIDLDefinition();
    assertNotNull(
        "Assert EnumDefinition contained by parameter type is not null",
        enumDefinitionContainedByReturnType);

    assertSame(enumDefinition, enumDefinitionContainedByReturnType);

  }

  public void testInterfaceUsingUnionTypes() throws Exception {
    final InterfaceDefinition itf = (InterfaceDefinition) loader.loadInterface(
        USING_UNION_SIGNATURE, new HashMap<Object, Object>());
    assertNotNull("Assert loader interface is not null", itf);

    final Import[] imports = ((ImportContainer) itf).getImports();
    assertNotNull("Assert imports not null", imports);

    assertEquals("Assert number of imports", 2, imports.length);
    assertEquals("Assert import name", UNION_U1_SIGNATURE, imports[0].getName());
    assertEquals("Assert second import name", USING_UNION_SIGNATURE, imports[1]
        .getName());

    final Method[] methods = ((MethodContainer) itf).getMethods();

    final Method firstMethod = methods[0];
    final ComplexType unionParameterType = (ComplexType) Util
        .getContainedType(firstMethod.getParameters()[0]);
    assertNotNull("Assert union parameter type not null", unionParameterType);
    final UnionDefinition unionDefinition = (UnionDefinition) ((IDLDefinitionContainer) unionParameterType)
        .getIDLDefinition();
    assertNotNull(
        "Assert UnionDefinition contained by parameter type is not null",
        unionDefinition);

    final Method secondMethod = methods[1];
    final ComplexType unionReturnType = secondMethod.getComplexType();
    assertNotNull("Assert union return type not null", unionReturnType);
    final UnionDefinition unionDefinitionContainedByReturnType = (UnionDefinition) ((IDLDefinitionContainer) unionReturnType)
        .getIDLDefinition();
    assertNotNull(
        "Assert UnionDefinition contained by parameter type is not null",
        unionDefinitionContainedByReturnType);

    assertSame(unionDefinition, unionDefinitionContainedByReturnType);

  }

  protected InterfaceDefinition loadInterface(final String name,
      final Map<Object, Object> ctx) throws Exception {
    return checkInterface((IDLDefinition) loader.loadInterface(
        INTERFACES_PACKAGE_PREFIX + name, (ctx == null)
            ? new HashMap<Object, Object>()
            : ctx), new HashSet<String>());
  }

  protected void loadErroneousIDL(final String name,
      final ErrorTemplate expectedTemplate) throws Exception {
    try {
      loadInterface(name, null);
      fail("Loading of IDL '" + name + "' is suppose to fail");
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(expectedTemplate, e.getError().getTemplate());
    }
  }

  // =====================================================================
  // utility methods to assert results

  protected static void checkItfContent(final InterfaceDefinition itf,
      final String[] fields, final String[] methods) {
    final Field[] fieldNodes = ((FieldContainer) itf).getFields();
    if (fields == null) {
      if (fieldNodes.length != 0) fail();
    } else {
      if (fieldNodes.length != fields.length)
        fail("The InterfaceDefinition does not contain the expected number of fields ("
            + fieldNodes.length + " instead of " + fields.length + ")");

      final Set<String> presentFields = new HashSet<String>(fieldNodes.length);
      for (final Field f : fieldNodes) {
        presentFields.add(f.getName());
      }

      for (final String f : fields) {
        if (!presentFields.contains(f)) {
          fail("The InterfaceDefinition does not contain a field named '" + f
              + "'");
        }
      }
    }

    final Method[] methodnodes = ((MethodContainer) itf).getMethods();
    if (methods == null) {
      if (methodnodes.length != 0) fail();
    } else {
      if (methodnodes.length != methods.length)
        fail("The InterfaceDefinition does not contains the expected number of methods ("
            + methodnodes.length + " instead of " + methods.length + ")");
      final Set<String> presentMethods = new HashSet<String>(methodnodes.length);

      for (final Method m : methodnodes) {
        presentMethods.add(m.getName());
      }

      for (final String methodToCheck : methods) {
        if (!presentMethods.contains(methodToCheck)) {
          fail("The InterfaceDefinition does not provide a method name '"
              + methodToCheck + "'");
        }
      }
    }
  }

  protected static InterfaceDefinition checkInterface(final IDLDefinition idl,
      final Set<String> checkedType) {
    if (!(idl instanceof InterfaceDefinition)) fail();
    final InterfaceDefinition itf = (InterfaceDefinition) idl;

    // check if it has already been checked
    if (!checkedType.add(idl.getName())) return itf;

    checkImportContainer((ImportContainer) itf);

    for (final Field field : ((FieldContainer) itf).getFields()) {
      final Type type = Util.getContainedType(field);
      if (Util.isComplexType(type)) {
        checkComplexType(Util.getComplexType(type), checkedType);
      }
      if (field.getName() == null) fail();
      if (field.getValue() == null) fail();
    }

    for (final Method m : ((MethodContainer) itf).getMethods()) {
      final Type returnType = Util.getContainedType((m));
      if (isComplexType(returnType))
        checkComplexType(Util.getComplexType(returnType), checkedType);
      if (m.getName() == null) fail();
      for (final Parameter p : ((ParameterContainer) m).getParameters()) {
        final Type parameterType = Util.getContainedType((p));
        if (isComplexType(parameterType))
          checkComplexType(getComplexType(parameterType), checkedType);
        if (p.getName() == null) fail();
        if (p.getQualifier() == null) fail();
      }
    }
    return itf;
  }

  protected static RecordDefinition checkRecord(final IDLDefinition idl,
      final Set<String> checkedType) {
    if (!(idl instanceof RecordDefinition)) fail();
    final RecordDefinition rec = (RecordDefinition) idl;

    // check if it has already been checked
    if (!checkedType.add(idl.getName())) return rec;

    checkImportContainer((ImportContainer) rec);

    for (final Field field : ((FieldContainer) rec).getFields()) {
      final Type type = Util.getContainedType(field);
      if (Util.isComplexType(type)) {
        checkComplexType(Util.getComplexType(type), checkedType);
      }
      if (field.getName() == null) fail();
      if (field.getValue() != null) fail();
    }
    return rec;
  }

  protected static void checkImportContainer(
      final ImportContainer importContainer) {
    for (final Import i : importContainer.getImports()) {
      final IDLDefinition impIDL = (IDLDefinition) ((Node) i)
          .astGetDecoration("idl-definition");
      if (impIDL == null) fail();
      if (!i.getName().equals(impIDL.getName())) fail();
    }
  }

  protected static void checkComplexType(final ComplexType ct,
      final Set<String> checkedType) {
    if (ct != null) {
// final InterfaceDefinition i = ((InterfaceDefinitionContainer) ct)
// .getInterfaceDefinition();
// final RecordDefinition r = ((RecordDefinitionContainer) ct)
// .getRecordDefinition();
// if (i == null && r == null) fail();
// if (i != null && r != null) fail();

      final IDLDefinition i = ((IDLDefinitionContainer) ct).getIDLDefinition();

      if (i instanceof InterfaceDefinition) {
        if (!i.getName().endsWith(ct.getName())) fail();
        checkInterface(i, checkedType);
      } else if (i instanceof RecordDefinition) {
        if (!i.getName().endsWith(ct.getName())) fail();
        checkRecord(i, checkedType);
      }
    }
  }
}
