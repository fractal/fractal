
package org.objectweb.fractal.cecilia.adl.test.optC;

import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import org.objectweb.fractal.cecilia.adl.AbstractLauncher.CmdAppendOption;
import org.objectweb.fractal.cecilia.adl.AbstractLauncher.CmdArgument;
import org.objectweb.fractal.cecilia.adl.AbstractLauncher.CmdFlag;
import org.objectweb.fractal.cecilia.adl.AbstractLauncher.CmdPathOption;
import org.objectweb.fractal.cecilia.adl.AbstractLauncher.CmdProperties;
import org.objectweb.fractal.cecilia.adl.AbstractLauncher.CommandLine;
import org.objectweb.fractal.cecilia.adl.AbstractLauncher.Options;

public class CommandLineTest extends TestCase {

  CmdFlag         flag1   = new CmdFlag("f", "flag1", "flag1 desc");
  CmdFlag         flag2   = new CmdFlag("g", "flag2", "flag2 desc");

  CmdArgument     arg1    = new CmdArgument("a", "arg1", "arg1 desc", "ARG");
  CmdArgument     arg2    = new CmdArgument("b", "arg2", "arg2 desc", "ARG",
                              "default", false);

  CmdAppendOption app     = new CmdAppendOption("d", "app", "app desc", "ARG");

  CmdPathOption   path    = new CmdPathOption("p", "path", "path desc", "ARG");

  CmdProperties   prop    = new CmdProperties("D", "D desc", "PROP_NAME",
                              "PROP_VALUE");

  Options         options = new Options();

  @Override
  protected void setUp() throws Exception {
    options.addOptions(flag1, flag2, arg1, arg2, app, path, prop);
  }

  public void test1() throws Exception {
    final CommandLine commandLine = CommandLine.parseArgs(options, false, "-f",
        "-a=foo", "-d=abc", "-d=def", "-p=f1", "-p=f2", "-Dfoo=bar",
        "-Dtoto=titi");

    assertTrue(flag1.isPresent(commandLine));
    assertFalse(flag2.isPresent(commandLine));

    assertEquals("foo", arg1.getValue(commandLine));
    assertEquals("default", arg2.getValue(commandLine));

    assertEquals("abc def", app.getValue(commandLine));

    final List<String> paths = path.getPathValue(commandLine);
    assertNotNull(paths);
    assertEquals(2, paths.size());
    assertEquals("f1", paths.get(0));
    assertEquals("f2", paths.get(1));

    final Map<String, String> props = prop.getValue(commandLine);
    assertNotNull(props);
    assertEquals(2, props.size());
    assertEquals("bar", props.get("foo"));
    assertEquals("titi", props.get("toto"));
  }
}
