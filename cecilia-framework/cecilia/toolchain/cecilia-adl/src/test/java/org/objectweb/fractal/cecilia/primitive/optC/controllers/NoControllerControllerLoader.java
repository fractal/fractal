/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 * Contributors: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.primitive.optC.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.cecilia.adl.controllers.AbstractControllerChecker;

/**
 * Adds controllers interface for <code>"primitive"</code> components.
 */
public class NoControllerControllerLoader extends AbstractControllerChecker {

  // --------------------------------------------------------------------------
  // Implementation of abstract methods
  // --------------------------------------------------------------------------

  @Override
  protected void checkComponent(final ComponentContainer container)
      throws ADLException {
  }

  @Override
  protected List<Interface> getControllerInterfaces(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    return new ArrayList<Interface>();
  }
}
