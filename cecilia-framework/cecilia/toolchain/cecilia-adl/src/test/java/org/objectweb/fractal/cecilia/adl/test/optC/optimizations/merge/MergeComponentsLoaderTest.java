/***
 * Cecilia
 * Copyright (C) 2005-2008 STMicroelectronics
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.test.optC.optimizations.merge;

import java.util.Map;

import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.ast.ASTFractalUtil;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.implementations.Controller;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.cecilia.adl.components.MergeComponentsLoader;
import org.objectweb.fractal.cecilia.adl.controllers.ExtendedController;
import org.objectweb.fractal.cecilia.adl.directives.Include;
import org.objectweb.fractal.cecilia.adl.directives.IncludeContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.implementations.ImplementationDecorationUtil;
import org.objectweb.fractal.cecilia.adl.implementations.LanguageLoader;
import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;
import org.objectweb.fractal.cecilia.adl.test.optC.AbstractCeciliaLoaderTest;

/**
 * Tests for the complete Cecilia <code>Loader</code> component in order to
 * see if at the end the AST is formed by merged components definitions which
 * satisfy the requirements of the compilers/builders of the toolchain.
 * 
 * @author Alessio Pace
 */
public class MergeComponentsLoaderTest extends AbstractCeciliaLoaderTest {

  /**
   * prefix for fixture files.
   */
  public static final String MERGE_FIXTURES_PREFIX        = "test.optC.optimizations.merge.";
  public static final String XXX_INTERFACE_FULL_SIGNATURE = MERGE_FIXTURES_PREFIX
                                                              + "Xxx";

  public static final String COMPOSITE1                   = "Composite1";
  public static final String COMPOSITE3                   = "Composite3";
  public static final String COMPOSITE7                   = "Composite7";

  /**
   * A primitive remains the same primitive (no changes at all).
   */
  public void testPrimitiveProvidesX() throws Exception {

    final Definition definition = this.loader.load(MERGE_FIXTURES_PREFIX
        + "PrimitiveProvidesX", getContextMapToBeUsedByLoader());
    assertNotNull("Assert not null", definition);

    assertTrue("Assert the merged definition is *still* a primitive",
        ASTFractalUtil.isPrimitive((ComponentContainer) definition));

    final TypeInterface xItf = ASTFractalUtil.getFcInterface(
        (ComponentContainer) definition, "x");
    assertEquals("Assert itf name", "x", xItf.getName());
    assertEquals("Assert itf role", "server", xItf.getRole());
    assertEquals("Assert itf signature", XXX_INTERFACE_FULL_SIGNATURE, xItf
        .getSignature());
    final Implementation implementation = ASTFractalUtil
        .getFcContentDesc((ComponentContainer) definition);
    assertEquals("Assert impl class", MERGE_FIXTURES_PREFIX + "providesX",
        implementation.getClassName());
  }

  /**
   * Merging a composite containing just a primitive which has one server
   * interface that is exported through the composite would result in a
   * primitive component with the same features of the primitive contained in
   * the composite.
   */
  public void testComposite1() throws Exception {

    final ComponentContainer definition = (ComponentContainer) this.loader
        .load(MERGE_FIXTURES_PREFIX + COMPOSITE1,
            getContextMapToBeUsedByLoader());

    assertNotNull("Assert not null", definition);

    /*
     * Check the name of the merged component is like the one of the original
     * composite
     */
    assertEquals("Assert merged component name", MERGE_FIXTURES_PREFIX
        + COMPOSITE1, ASTFractalUtil.getFcName(definition));

    /*
     * because the definition is the result of a merge, it should not contain
     * sub components, but it has 1 inner binding.
     */
    assertEquals("Assert 0 sub components", 0, ASTFractalUtil
        .getFcSubComponents(definition).length);
    assertEquals("Assert 1 binding", 1,
        ASTFractalUtil.getBindings(definition).length);

    /* checks implementation */
    final Implementation implementation = ASTFractalUtil
        .getFcContentDesc(definition);
    assertNotNull("Assert the merged component's Implementation not null",
        implementation);
    assertEquals("Assert the implementation value",
        MergeComponentsLoader.FRACTAL_LIB_MERGED_PRIMITIVE, implementation
            .getClassName());

    /* checks controller AST node */
    final Controller controller = ASTFractalUtil
        .getFcControllerDesc(definition);
    assertNotNull("Assert controller AST node not null", controller);
    assertEquals("Assert controller desc is "
        + MergeComponentsLoader.MERGED_PRIMITIVE_CONTROLLER_DESC,
        MergeComponentsLoader.MERGED_PRIMITIVE_CONTROLLER_DESC, controller
            .getDescriptor());
    assertEquals("Assert controller language", LanguageLoader.DEFAULT_CPL,
        ((ExtendedController) controller).getLanguage());

    /* check server functional interfaces */
    final TypeInterface[] allInterfaces = ASTFractalUtil
        .getFcInterfaces(definition);
    /* it should have the "component" and "x" (both server) interfaces */
    assertEquals("Assert number of interfaces on merged comp", 2,
        allInterfaces.length);

    final TypeInterface xItf = ASTFractalUtil.getFcInterface(definition, "x");
    assertNotNull("Assert x itf not null", xItf);
    assertEquals("Assert x itf signature", XXX_INTERFACE_FULL_SIGNATURE, xItf
        .getSignature());
    // check that the interface contains the IDLDefinition of the Xxx interface
    final IDLDefinition xIdlDefinition = ((IDLDefinitionContainer) xItf)
        .getIDLDefinition();
    assertNotNull("Assert TypeInterface contains a not null IDLDefintion",
        xIdlDefinition);

    final TypeInterface componentItf = ASTFractalUtil.getFcInterface(
        definition, "component");
    assertNotNull(componentItf);

    /* check that it is decorated with the merged composite */
    final Object mergedCompositeDecoration = ((Node) definition)
        .astGetDecoration(MergeComponentsLoader.PREVIOUS_COMPOSITE_DECORATION);
    assertNotNull(mergedCompositeDecoration);
    assertTrue(mergedCompositeDecoration instanceof ComponentContainer);

    /* check client interfaces */
    final String[] clientItfNames = ASTFractalUtil.listFc(definition);
    assertEquals("Assert there are no client itf", 0, clientItfNames.length);

    /* checks includes */
    final Include[] includes = ((IncludeContainer) implementation)
        .getIncludes();
    assertEquals("Assert # include nodes", 1, includes.length);
    assertEquals("Assert 1st include file",
        MERGE_FIXTURES_PREFIX + "providesX", includes[0].getFile());
    assertNotNull("Assert 'code' decoration on 1st include is not null",
        ImplementationDecorationUtil.getCode((Node) includes[0]));

  }

  /**
   * Merging a composite containing two primitives: one has one server interface
   * that is exported through the composite (with the same name) and a client
   * interface which is bound to the other primitive component only server
   * interface (they both have the same name), would result in a primitive
   * having one server interface (the one of the original composite).
   */
  public void testComposite3() throws Exception {

    final ComponentContainer definition = (ComponentContainer) this.loader
        .load(MERGE_FIXTURES_PREFIX + COMPOSITE3,
            getContextMapToBeUsedByLoader());

    assertNotNull("Assert not null", definition);

    /*
     * Check the name of the merged component is like the one of the original
     * composite
     */
    assertEquals("Assert merged component name", MERGE_FIXTURES_PREFIX
        + COMPOSITE3, ASTFractalUtil.getFcName(definition));

    /*
     * because the definition is the result of a merge, it should not contain
     * sub components and 2 bindings
     */
    assertEquals("Assert 0 sub components", 0, ASTFractalUtil
        .getFcSubComponents(definition).length);
    assertEquals("Assert 2 bindings", 2,
        ASTFractalUtil.getBindings(definition).length);

    /* checks implementation */
    final Implementation implementation = ASTFractalUtil
        .getFcContentDesc(definition);
    assertNotNull("Assert the merged component's Implementation not null",
        implementation);
    assertEquals("Assert the implementation value",
        MergeComponentsLoader.FRACTAL_LIB_MERGED_PRIMITIVE, implementation
            .getClassName());

    /* checks controller AST node */
    final Controller controller = ASTFractalUtil
        .getFcControllerDesc(definition);
    assertNotNull("Assert controller AST node not null", controller);
    assertEquals("Assert controller desc is "
        + MergeComponentsLoader.MERGED_PRIMITIVE_CONTROLLER_DESC,
        MergeComponentsLoader.MERGED_PRIMITIVE_CONTROLLER_DESC, controller
            .getDescriptor());
    assertEquals("Assert controller language", LanguageLoader.DEFAULT_CPL,
        ((ExtendedController) controller).getLanguage());

    /* check server functional interfaces */
    /* check server functional interfaces */
    final TypeInterface[] allInterfaces = ASTFractalUtil
        .getFcInterfaces(definition);
    /* it should have the "component" and "x" (both server) interfaces */
    assertEquals("Assert number of interfaces on merged comp", 2,
        allInterfaces.length);

    final TypeInterface sItf = ASTFractalUtil.getFcInterface(definition, "s");
    assertNotNull("Assert s itf not null", sItf);
    assertEquals("Assert s itf signature", MERGE_FIXTURES_PREFIX + "Xxx", sItf
        .getSignature());
    // check that the interface contains the IDLDefinition of the Xxx interface
    final IDLDefinition xIdlDefinition = ((IDLDefinitionContainer) sItf)
        .getIDLDefinition();
    assertNotNull("Assert TypeInterface contains a not null IDLDefintion",
        xIdlDefinition);

    final TypeInterface componentItf = ASTFractalUtil.getFcInterface(
        definition, "component");
    assertNotNull("Assert the merged primitive has a 'component' interface",
        componentItf);

    /* check that it is decorated with the merged composite */
    final Object mergedCompositeDecoration = ((Node) definition)
        .astGetDecoration(MergeComponentsLoader.PREVIOUS_COMPOSITE_DECORATION);
    assertNotNull(mergedCompositeDecoration);
    assertTrue(mergedCompositeDecoration instanceof ComponentContainer);

    /* check client interfaces */
    final String[] clientItfNames = ASTFractalUtil.listFc(definition);
    assertEquals("Assert there are no client itf", 0, clientItfNames.length);

    /* checks includes */
    final Include[] includes = ((IncludeContainer) implementation)
        .getIncludes();
    assertEquals("Assert # include nodes", 2, includes.length);

    assertEquals("Assert 1st include file", MERGE_FIXTURES_PREFIX
        + "providesX_usesY", includes[0].getFile());
    assertNotNull("Assert 'code' decoration on 1st include is not null",
        ImplementationDecorationUtil.getCode((Node) includes[0]));
    // TODO assert id

    assertEquals("Assert 2nd include file",
        MERGE_FIXTURES_PREFIX + "providesY", includes[1].getFile());
    assertNotNull("Assert 'code' decoration on 2nd include is not null",
        ImplementationDecorationUtil.getCode((Node) includes[1]));
    // TODO assert id
  }

  /**
   * This test checks the AST after merging the "ctwo" composite component.
   */
  public void testComposite7MergeFirstLevelComposite() throws Exception {
    final Map<Object, Object> mapForLoader = getContextMapToBeUsedByLoader();
    mapForLoader.put("composite7", MERGE_FIXTURES_PREFIX + COMPOSITE7
        + "MergeCTWO");

    /* top level Definition */
    final ComponentContainer definition = (ComponentContainer) this.loader
        .load(MERGE_FIXTURES_PREFIX + "Test_" + COMPOSITE7, mapForLoader);

    final ComponentContainer testComponent = ASTFractalUtil
        .getFcSubComponentByName(definition, "test");
    assertNotNull(testComponent);

    final ComponentContainer providesUComponent = ASTFractalUtil
        .getFcSubComponentByName(testComponent, "providesU");
    assertNotNull(providesUComponent);
    final TypeInterface uItf = ASTFractalUtil.getFcInterface(
        providesUComponent, "u");
    assertNotNull(uItf);

    final ComponentContainer toBeMergedComponent = ASTFractalUtil
        .getFcSubComponentByName(testComponent, "toBeMerged");
    assertNotNull(toBeMergedComponent);

    final ComponentContainer cthreeComponent = ASTFractalUtil
        .getFcSubComponentByName(toBeMergedComponent, "cthree");
    assertNotNull(cthreeComponent);

    final ComponentContainer psevenComponent = ASTFractalUtil
        .getFcSubComponentByName(cthreeComponent, "pseven");
    assertNotNull(psevenComponent);
    final TypeInterface psevenXItf = ASTFractalUtil.getFcInterface(
        psevenComponent, "x");
    assertNotNull(psevenXItf);

    final ComponentContainer coneComponent = ASTFractalUtil
        .getFcSubComponentByName(toBeMergedComponent, "cone");
    assertNotNull(coneComponent);

    final ComponentContainer pfive = ASTFractalUtil.getFcSubComponentByName(
        coneComponent, "pfive");
    assertNotNull(pfive);
    final TypeInterface pfiveSItf = ASTFractalUtil.getFcInterface(pfive, "s");
    assertNotNull(pfiveSItf);

    // ====================================================
    /* this is the supposed to have been merged component */
    final ComponentContainer ctwoComponent = ASTFractalUtil
        .getFcSubComponentByName(coneComponent, "ctwo");
    assertNotNull(ctwoComponent);
    /* so assert it is a primitive */
    assertTrue(ASTFractalUtil.isPrimitive(ctwoComponent));

    /* verify it has the server interface of the former composite */
    final TypeInterface mItf = ASTFractalUtil
        .getFcInterface(ctwoComponent, "m");
    assertNotNull(mItf);

    /* verify it has the client interfaces of the former composite */
    final TypeInterface ctwoC1Itf = ASTFractalUtil.getFcInterface(
        ctwoComponent, "c1");
    assertNotNull(ctwoC1Itf);
    /*
     * the interface should have been rebound to the "pfive" component's
     * interface after rerunning the checkers
     */
    assertSame(pfiveSItf, InterfaceDecorationUtil.getBoundTo(ctwoC1Itf));

    final TypeInterface ctwoC2Itf = ASTFractalUtil.getFcInterface(
        ctwoComponent, "c2");
    assertNotNull(ctwoC2Itf);
    assertSame(uItf, InterfaceDecorationUtil.getBoundTo(ctwoC2Itf));

    /* the interface should have been rebound */
    assertSame(InterfaceDecorationUtil.getBoundTo(psevenXItf), mItf);

    // =====================================================

  }
}
