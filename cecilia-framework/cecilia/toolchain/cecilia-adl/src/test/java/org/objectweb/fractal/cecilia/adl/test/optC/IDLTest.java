/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.adl.test.optC;

import org.objectweb.fractal.cecilia.adl.test.CeciliaAdlTestCase;

public class IDLTest extends CeciliaAdlTestCase {

  public void testA1() throws Exception {
    compileAndRun("test.optC.idl.A1");
  }

  public void testRtest() throws Exception {
    compileAndRun("test.optC.idl.Rtest");
  }

  public void testIDLSimple() throws Exception {
    compileAndRun("test.optC.idl.Simple");
  }

  public void testIDLExtendsSimple() throws Exception {
    compileAndRun("test.optC.idl.ExtendsSimple");
  }

  public void testIDLOnlyDot() throws Exception {
    try {
      tryCompileADL("test.optC.idl.OnlyDot");
      fail("An interface containing a method with only one argument which is a dot type has compiled. This is permitted by the IDL language.");
    } catch (final Exception e) {
      return;
    }
  }

  public void testIDLInterfaceReferenceInSamePackage() throws Exception {
    compileAndRun("test.optC.idl.InterfaceReferenceInSamePackage");
  }

  public void testIDLInterfaceReferenceInSamePackageWithImport()
      throws Exception {
    compileAndRun("test.optC.idl.InterfaceReferenceInSamePackageWithImport");
  }

  public void testIDLAttributedServer() throws Exception {
    compileAndRun("test.optC.idl.SimpleAttributedServer");
  }

  public void testIDLAssignedAttributedServer() throws Exception {
    compileAndRun("test.optC.idl.SimpleAssignedAttributedServer");
  }

  public void testIDLErroneousAssignedAttributedServer() throws Exception {
    try {
      tryCompileADL("test.optC.idl.ErroneousAssignedAttributedServer");
      fail("An unxisting field of a record has been assigned.");
    } catch (final Exception e) {
      return;
    }
  }

  public void testIDLComplexAssignedAttributedServer() throws Exception {
    compileAndRun("test.optC.idl.ComplexAttributedServer");
  }

  public void testIDLInterfaceReferenceRecordsAndInterface() throws Exception {
    compileAndRun("test.optC.idl.InterfaceReferenceRecordsAndInterface");
  }

  public void testIDLInterfaceReferenceRecordsAndInterfaceWithoutImports()
      throws Exception {
    compileAndRun("test.optC.idl.InterfaceReferenceRecordsAndInterfaceWithoutImports");
  }

  public void testIDLErroneousInterface() throws Exception {
    try {
      tryCompileADL("test.optC.idl.ErroneousInterface");
      fail("An malformed interface name has been accepted by the compiler.");
    } catch (final Exception e) {
      return;
    }
  }

  public void testIDLInterfaceRecordOverloadsInterfaceError() throws Exception {
    try {
      tryCompileADL("test.optC.idl.InterfaceRecordOverloadsInterfaceError");
      fail("The interface 'InterfaceRecordOverloadsInterfaceError' contains a reference overload error but the compiler has not thrown any error.");
    } catch (final Exception e) {
      return;
    }
  }

  public void testInterfaceUsingEnum() throws Exception {

    compileAndRun("test.optC.idl.EnumTest");

  }

    public void testInterfaceUsingUnion() throws Exception {

    compileAndRun("test.optC.idl.UnionTest");

  }
}
