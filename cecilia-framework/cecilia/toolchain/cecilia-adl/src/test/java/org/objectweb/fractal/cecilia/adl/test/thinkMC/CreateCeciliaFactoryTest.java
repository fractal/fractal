/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.test.thinkMC;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.adl.util.ContentControllerHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.cecilia.adl.Launcher;
import org.objectweb.fractal.cecilia.adl.plugin.AbstractPluginManager;
import org.objectweb.fractal.julia.Julia;
import org.objectweb.fractal.util.Fractal;
import org.objectweb.fractal.cecilia.adl.test.CeciliaAdlTestCase;

/**
 * An integration test case to check that CeciliaADL factory can be indeed
 * instantiated using different FractalADL Factories.
 * 
 * @author Alessio Pace
 */
public class CreateCeciliaFactoryTest extends CeciliaAdlTestCase {

  private Factory factory;

  /**
   * Just creates a CeciliaADL factory using the default bootstrap factory. The
   * toolchain after is NOT used to compile an ADL, so plugins are not loaded.
   */
  @SuppressWarnings("unchecked")
  public void testCreateCeciliaFactoryUsingBootstrapFactory() throws Exception {

    /* the Bootstrap Factory (it has a Java Backend) */
    this.factory = FactoryFactory.getFactory();
    assertNotNull("Assert bootstrap Factory not null", this.factory);

    final HashMap<String, Object> context = new HashMap<String, Object>();

    final Map<String, Object> ceciliaAdlCompositeComponent = (Map<String, Object>) this.factory
        .newComponent(Launcher.CECILIA_BASIC_COMPILER_ADL, context);

    assertNotNull("Assert result not null", ceciliaAdlCompositeComponent);

    final Factory f = (Factory) ceciliaAdlCompositeComponent.get("factory");
    assertNotNull("Assert factory interface not null", f);
  }

  /**
   * Just creates a CeciliaADL factory as a Fractal application. The toolchain
   * after is NOT used to compile an ADL, so plugins are not loaded.
   */
  @SuppressWarnings("unchecked")
  public void testCreateCeciliaFactoryUsingJavaBackendFactory()
      throws Exception {

    /* Factory with a Java Backend that is created by the Bootstrap Factory */
    this.factory = FactoryFactory.getFactory(FactoryFactory.JAVA_BACKEND);
    assertNotNull("Assert bootstrap Factory not null", this.factory);

    final HashMap<String, Object> context = new HashMap<String, Object>();

    final Map<String, Object> ceciliaAdlCompositeComponent = (Map<String, Object>) this.factory
        .newComponent(Launcher.CECILIA_BASIC_COMPILER_ADL, context);

    assertNotNull("Assert result not null", ceciliaAdlCompositeComponent);

    final Factory f = (Factory) ceciliaAdlCompositeComponent.get("factory");
    assertNotNull("Assert factory interface not null", f);
  }

  public void testCreateCeciliaFactoryUsingFractalBackendFactory()
      throws Exception {

    System.setProperty("fractal.provider", Julia.class.getCanonicalName());

    /* Factory with a Fractal Backend that is created by the Bootstrap Factory */
    this.factory = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
    assertNotNull("Assert bootstrap Factory not null", this.factory);

    final HashMap<String, Object> context = new HashMap<String, Object>();

    /* to use the Fractal Plugin Factory */
    // TODO extract "Fractal" as a constant
    context.put(AbstractPluginManager.PLUGIN_FACTORY_BACKEND, "Fractal");

    /* the result this time will be an o.o.f.api.Component */
    final Component ceciliaAdlCompositeComponent = (Component) this.factory
        .newComponent(Launcher.CECILIA_BASIC_COMPILER_ADL, context);

    assertNotNull("Assert result not null", ceciliaAdlCompositeComponent);

    /* use Fractal APIs to access "factory" interface */
    final Factory f = (Factory) ceciliaAdlCompositeComponent
        .getFcInterface("factory");
    assertNotNull("Assert factory interface not null", f);

  }

  /**
   * This integration tests load a CeciliaADL toolchain as a Fractal
   * application, compiles with it the HelloworldShared example, and
   * additionally it performs few checks on the loaded components and the
   * bindings between them.
   * 
   * @throws Exception
   */
  public void testCompileWithAFractalCompiler() throws Exception {

    final FractalBackendLauncher launcher = new FractalBackendLauncher(
        getLauncherOptions());

    final Launcher oldL = CeciliaAdlTestCase.getLauncher();

    try {
      CeciliaAdlTestCase.setLauncher(launcher);
      compileAndRun("test.thinkMC.share.HelloworldShared");

      final Component ceciliaBasicFactoryComponent = launcher
          .getCompilerComponent();

      final Component loaderComponent = ContentControllerHelper
          .getSubComponentByName(ceciliaBasicFactoryComponent, "loader");

      // ---------------------------------------------------------------------
      /*
       * check plugins have been loaded into the implementation-code-loader
       * composite component
       */
      final Component implementationCodeLoaderComp = ContentControllerHelper
          .getSubComponentByName(loaderComponent, "implementation-code-loader");

      final Component implementationCodeLoaderCompPluginSelector = ContentControllerHelper
          .getSubComponentByName(implementationCodeLoaderComp,
              "plugin-selector");

      assertTrue("Assert pluginSelector contains the loaded plugin interface",
          containsExternalInterface(implementationCodeLoaderCompPluginSelector,
              "client-loader"));
      // ----------------------------------------------------------------------

      // ----------------------------------------------------------------------
      /*
       * check interfaces into the controller-loader composite component
       */
      final Component controllerLoaderComp = ContentControllerHelper
          .getSubComponentByName(loaderComponent, "controller-loader");

      assertTrue("Assert controller-loader has a loader external interface",
          containsExternalInterface(controllerLoaderComp, "loader"));

      final Interface controllerLoaderCompLoaderItf = (Interface) controllerLoaderComp
          .getFcInterface("loader");

      assertTrue("Assert loader interface is external",
          !controllerLoaderCompLoaderItf.isFcInternalItf());

      final InterfaceType loaderInterfaceType = (InterfaceType) controllerLoaderCompLoaderItf
          .getFcItfType();

      assertTrue("Assert loader interface is server", !loaderInterfaceType
          .isFcClientItf());

      final BindingController controllerLoaderBindingController = Fractal
          .getBindingController(controllerLoaderComp);

      final Interface controllerLoaderCompLoaderItfByLookup = (Interface) controllerLoaderBindingController
          .lookupFc("loader");

      final String ownerName = Fractal.getNameController(
          controllerLoaderCompLoaderItfByLookup.getFcItfOwner()).getFcName();
      assertEquals("Assert owner of loader interface", "main-loader", ownerName);
      // -----------------------------------------------------------------------

    } catch (final Exception e) {
      e.printStackTrace();
      fail("Unable to compile HelloworldShared");
    } finally {
      CeciliaAdlTestCase.setLauncher(oldL);
      assertNotSame(CeciliaAdlTestCase.getLauncher(), launcher);
    }

  }

  public boolean containsExternalInterface(final Component pluginSelector,
      final String itf) {

    final Object[] interfaces = pluginSelector.getFcInterfaces();
    for (final Object o : interfaces) {
      final Interface i = (Interface) o;
      if (i.getFcItfName().contains(itf)) {
        return true;
      }
    }

    return false;

  }
}
