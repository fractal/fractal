/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.test.optC;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.cecilia.adl.test.CeciliaAdlTestCase;

/**
 * Test ADL in test.optC.bindings package.
 */
public class BindingsTest extends CeciliaAdlTestCase {

  public void testNoBinding() throws Exception {
    compileAndRun("test.optC.bindings.NoBindingComposite");
  }

  public void testOptionalImportBinding() throws Exception {
    compileAndRun("test.optC.bindings.OptionalImportBinding");
  }

  public void testCustomBindingControllerMC() throws Exception {
    compileAndRun("test.optC.bindings.CustomBindingControllerMC");
  }

  public void testCustomImportedBindingControllerMC() throws Exception {
    compileAndRun("test.optC.bindings.CustomImportedBindingControllerMC");
  }

  public void testCollectionBindingControllerMC() throws Exception {
    compileAndRun("test.optC.bindings.CollectionBindingControllerMC");
  }

  public void testCollectionCompositeBindingControllerMC() throws Exception {
    compileAndRun("test.optC.bindings.CollectionCompositeBindingControllerMC");
  }

  public void testCollectionImportedBindingControllerMC() throws Exception {
    compileAndRun("test.optC.bindings.CollectionImportedBindingControllerMC");
  }

  // test for Bug 307360
  public void testMultipleServers() throws Exception {
    compileAndRun("test.optC.bindings.static.DelegatingServersMC");
  }

  /**
   * Test for Bug #307806 about binding incompatible signatures. Here we test
   * that the given ADL should not be compiled correctly by the toolchain.
   */
  public void testIncompatibleSignatureForBinding() throws Exception {
    try {
      tryCompileADL("test.optC.bindings.incompatible.IncompatibleBinding");
      fail("Should throw an ADLException because there are bindings with incompatible signatures");
    } catch (final ADLException e) {
      // OK
    }
  }
  
  // tests for feature request #313030
  public void testCollectionFlattenedMC() throws Exception {
    compileAndRun("test.optC.bindings.CollectionFlattenedMC");
  }

  public void testCollectionFlattenedCompositeMC() throws Exception {
    compileAndRun("test.optC.bindings.CollectionFlattenedCompositeMC");
  }

  public void testCollectionOptionalFlattenedCompositeMC() throws Exception {
    compileAndRun("test.optC.bindings.CollectionOptionalFlattenedCompositeMC");
  }

  public void testInvalidCollectionFlattenedCompositeMC() throws Exception {
    try {
      tryCompileADL("test.optC.bindings.InvalidCollectionFlattenedCompositeMC");
      fail("Should throw an ADLException because there is a binding without a corresponding interface");
    } catch (final ADLException e) {
      // OK
    }
  }

  public void testCollectionFlattenedImportedMC() throws Exception {
    compileAndRun("test.optC.bindings.CollectionFlattenedImportedMC");
  }
}
