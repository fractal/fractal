/**
 * Cecilia
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.primitive.myLanguage;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;

// START SNIPPET:Content
public class PrimitiveControllerLoader
    extends
      org.objectweb.fractal.cecilia.primitive.thinkMC.controllers.PrimitiveControllerLoader {

  @Override
  protected void checkComponent(final ComponentContainer container)
      throws ADLException {
    // TODO Auto-generated method stub
    System.out.println("[" + this.getClass().getName()
        + "::checkComponent()] Hello World");
    super.checkComponent(container);
  }

  @Override
  protected List<Interface> getControllerInterfaces(
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    // TODO Auto-generated method stub
    System.out.println("[" + this.getClass().getName()
        + "::getControllerInterfaces()] Hello World");
    return super.getControllerInterfaces(container, context);
  }

}
// END SNIPPET:Content
