/***
 * Cecilia ADL Compiler
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.test.optC;

import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.cecilia.adl.idl.ast.ComplexType;
import org.objectweb.fractal.cecilia.adl.idl.ast.EnumDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinitionContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.InterfaceDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Method;
import org.objectweb.fractal.cecilia.adl.idl.ast.MethodContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.Parameter;
import org.objectweb.fractal.cecilia.adl.idl.ast.ParameterContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.UnionDefinition;

/**
 * 
 */
public class CeciliaLoaderIntegrationTest extends AbstractCeciliaLoaderTest {

  /**
   * Test that after loading an Enum ADL, the loaded IDL are contained in the
   * {@link TypeInterface} and that {@link ComplexType}s presents in them
   * contain also the definitions.
   * 
   * @throws Exception
   */
  public void testContainedDefinitionsInLoadedEnumIDL() throws Exception {
    final Definition result = loader.load("test.optC.idl.EnumTest",
        getContextMapToBeUsedByLoader());
    assertNotNull("Assert Definition not null", result);

    final Component[] components = ((ComponentContainer) result)
        .getComponents();

    Component testComponent = null;
    for (final Component c : components) {
      if (c.getName().equals("test")) {
        testComponent = c;
        break;
      }
    }
    assertNotNull("Assert there is the 'test' component", testComponent);

    final Interface[] testComponentInterfaces = ((InterfaceContainer) testComponent)
        .getInterfaces();
    TypeInterface interfaceUsingEnum = null;
    for (final Interface i : testComponentInterfaces) {
      if (i.getName().equals("e")) {
        interfaceUsingEnum = (TypeInterface) i;
        break;
      }
    }
    assertNotNull("Assert interface using enum not null", interfaceUsingEnum);
    assertTrue(interfaceUsingEnum instanceof IDLDefinitionContainer);
    final InterfaceDefinition idlInterfaceUsingEnum = (InterfaceDefinition) ((IDLDefinitionContainer) interfaceUsingEnum)
        .getIDLDefinition();
    assertNotNull("Assert contained IDLDefinition not null",
        idlInterfaceUsingEnum);

    final Method[] methods = ((MethodContainer) idlInterfaceUsingEnum)
        .getMethods();
    final Method firstMethod = methods[0];
    final Parameter[] firstMethodParameters = ((ParameterContainer) firstMethod)
        .getParameters();
    final Parameter firstMethodFirstParameter = firstMethodParameters[0];
    assertEquals("Assert parameter name", "e", firstMethodFirstParameter
        .getName());
    final ComplexType firstMethodFirstParameterContainedType = firstMethodFirstParameter
        .getComplexType();
    assertEquals("Assert first method first parameter type name", "E1",
        firstMethodFirstParameterContainedType.getName());
    final EnumDefinition firstMethodFirstParameterContainedEnumDefinition = (EnumDefinition) ((IDLDefinitionContainer) firstMethodFirstParameterContainedType)
        .getIDLDefinition();
    assertNotNull("Assert enum definition",
        firstMethodFirstParameterContainedEnumDefinition);
  }

  /**
   * Test that after loading an Union ADL, the loaded IDL are contained in the
   * {@link TypeInterface} and that {@link ComplexType}s presents in them
   * contain also the definitions.
   * 
   * @throws Exception
   */
  public void testContainedDefinitionsInLoadedUnionIDL() throws Exception {
    final Definition result = loader.load("test.optC.idl.UnionTest",
        getContextMapToBeUsedByLoader());
    assertNotNull("Assert Definition not null", result);

    final Component[] components = ((ComponentContainer) result)
        .getComponents();

    Component testComponent = null;
    for (final Component c : components) {
      if (c.getName().equals("test")) {
        testComponent = c;
        break;
      }
    }
    assertNotNull("Assert there is the 'test' component", testComponent);

    final Interface[] testComponentInterfaces = ((InterfaceContainer) testComponent)
        .getInterfaces();
    TypeInterface interfaceUsingUnion = null;
    for (final Interface i : testComponentInterfaces) {
      if (i.getName().equals("u")) {
        interfaceUsingUnion = (TypeInterface) i;
        break;
      }
    }
    assertNotNull("Assert interface using union not null", interfaceUsingUnion);
    assertTrue(interfaceUsingUnion instanceof IDLDefinitionContainer);
    final InterfaceDefinition idlInterfaceUsingUnion = (InterfaceDefinition) ((IDLDefinitionContainer) interfaceUsingUnion)
        .getIDLDefinition();
    assertNotNull("Assert contained IDLDefinition not null",
        idlInterfaceUsingUnion);

    final Method[] methods = ((MethodContainer) idlInterfaceUsingUnion)
        .getMethods();
    final Method firstMethod = methods[0];
    final Parameter[] firstMethodParameters = ((ParameterContainer) firstMethod)
        .getParameters();
    final Parameter firstMethodFirstParameter = firstMethodParameters[0];
    assertEquals("Assert parameter name", "u", firstMethodFirstParameter
        .getName());
    final ComplexType firstMethodFirstParameterContainedType = firstMethodFirstParameter
        .getComplexType();
    assertEquals("Assert first method first parameter type name", "U1",
        firstMethodFirstParameterContainedType.getName());
    final UnionDefinition firstMethodFirstParameterContainedUnionDefinition = (UnionDefinition) ((IDLDefinitionContainer) firstMethodFirstParameterContainedType)
        .getIDLDefinition();
    assertNotNull("Assert union definition",
        firstMethodFirstParameterContainedUnionDefinition);
  }

  /**
   * Test that loading an IDL containing an interface whose name is a reserved
   * keyword of the C/C++ language (or several other Cecilia IDL keywords like
   * the C99 integer types) fails in the toolchain (feature request #308707). If
   * these keywords are not trapped by the toolchain, the compiler will complain
   * about many syntax errors.
   * 
   * @throws Exception
   */
  public void testIDLReservedWordInterface() throws Exception {
    final Definition result;
    try {
      result = loader.load("test.optC.idl.ReservedWordInterface",
          getContextMapToBeUsedByLoader());
    } catch (final Exception e) {
      System.err.println(e.getMessage());
      return;
    }
    fail("An malformed interface name has been accepted by the compiler.");
  }

  /**
   * Test that loading an IDL containing an method whose name is a reserved
   * keyword of the C/C++ language (or several other Cecilia IDL keywords like
   * the C99 integer types) fails in the toolchain (feature request #308707). If
   * these keywords are not trapped by the toolchain, the compiler will complain
   * about many syntax errors.
   * 
   * @throws Exception
   */
  public void testIDLReservedWordMethod() throws Exception {
    final Definition result;
    try {
      result = loader.load("test.optC.idl.ReservedWordMethod",
          getContextMapToBeUsedByLoader());
    } catch (final Exception e) {
      System.err.println(e.getMessage());
      return;
    }
    fail("An malformed method name has been accepted by the compiler.");
  }

  /**
   * Test that loading an IDL containing an method of which one parameter name
   * is a reserved keyword of the C/C++ language (or several other Cecilia IDL
   * keywords like the C99 integer types) fails in the toolchain (feature
   * request #308707). If these keywords are not trapped by the toolchain, the
   * compiler will complain about many syntax errors.
   * 
   * @throws Exception
   */
  public void testIDLReservedWordMethodParameter() throws Exception {
    final Definition result;
    try {
      result = loader.load("test.optC.idl.ReservedWordMethodParameter",
          getContextMapToBeUsedByLoader());
    } catch (final Exception e) {
      System.err.println(e.getMessage());
      return;
    }
    fail("An malformed method parameter name has been accepted by the compiler.");
  }

  /**
   * Test that loading an IDL containing an field whose name is a reserved
   * keyword of the C/C++ language (or several other Cecilia IDL keywords like
   * the C99 integer types) fails in the toolchain (feature request #308707). If
   * these keywords are not trapped by the toolchain, the compiler will complain
   * about many syntax errors.
   * 
   * @throws Exception
   */
  public void testIDLReservedWordField() throws Exception {
    final Definition result;
    try {
      result = loader.load("test.optC.idl.ReservedWordField",
          getContextMapToBeUsedByLoader());
    } catch (final Exception e) {
      System.err.println(e.getMessage());
      return;
    }
    fail("An malformed field name has been accepted by the compiler.");
  }

  /**
   * Test that loading an ADL containing an interface whose name is a reserved
   * keyword of the C/C++ language (or several other Cecilia IDL keywords like
   * the C99 integer types) fails in the toolchain (feature request #308707). If
   * these keywords are not trapped by the toolchain, the compiler will complain
   * about many syntax errors.
   * 
   * @throws Exception
   */
  public void testADLReservedWordInterface() throws Exception {
    final Definition result;
    try {
      result = loader.load("test.optC.idl.ADLReservedWordInterface",
          getContextMapToBeUsedByLoader());
    } catch (final Exception e) {
      System.err.println(e.getMessage());
      return;
    }
    fail("An malformed interface name has been accepted by the compiler.");
  }
}
