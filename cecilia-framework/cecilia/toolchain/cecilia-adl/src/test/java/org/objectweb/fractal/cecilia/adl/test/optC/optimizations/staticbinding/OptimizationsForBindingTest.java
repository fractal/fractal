/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2008 STMicroelectronics
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.test.optC.optimizations.staticbinding;

import java.io.File;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil;
import org.objectweb.fractal.cecilia.adl.test.CeciliaAdlTestCase;

/**
 * @author Alessio Pace
 */
public class OptimizationsForBindingTest extends CeciliaAdlTestCase {

  /**
   * Common prefix for static binding test definitions.
   */
  public static final String TEST_STATICBINDING_PREFIX = "test.optC.optimizations.staticbinding.";

  /**
   * This test is not related to optimizations but it just tests changing the
   * client interface of a component.
   */
  public void testSimpleReconfigurationOfClientItfs() throws Exception {
    this
        .compileAndRun(TEST_STATICBINDING_PREFIX + "ClientServerReconfigurable");
  }

  public void testSimpleDynamicFalse() throws Exception {
    this.compileAndRun(TEST_STATICBINDING_PREFIX + "ClientServerDynamicFalse");
  }

  public void testSimpleDynamicFalseInCaseOfCollectionInterface()
      throws Exception {
    try {
      tryCompileADL(TEST_STATICBINDING_PREFIX
          + "ClientServerInvalidDynamicFalse2");
      fail("Should have thrown an " + ADLException.class
          + " because the dynamic='false' binding is invalid here");
    } catch (final ADLException e) {
      // OK
    }
  }

  /**
   * Should verify that a component that has only dynamic="false" bindings
   * (no dynamic="true" bindings) exports no BC interface.
   */
  public void testInvalidRebindOfDynamicFalseUsingBC() throws Exception {
    try {
      tryCompileADL(TEST_STATICBINDING_PREFIX
          + "ClientServerInvalidRuntimeRebindOfDynamicFalseUsingBC");
      fail("Should have thrown an " + ADLException.class
          + " because the component does not have a BC interface");
    } catch (final ADLException e) {
      // OK
    }
  }

  
  /**
   * Should verify that using the BindingController to reconfigure a
   * dynamic="false" binding does not result in changing the client interface.
   */
  public void testInvalidRebindOfDynamicFalseUsingBC2() throws Exception {
    compileAndRun(TEST_STATICBINDING_PREFIX
        + "ClientServerInvalidRuntimeRebindOfDynamicFalseUsingBC2");
  }

  /**
   * Should verify that using the BindingController to reconfigure a
   * dynamic="false" binding results in changing the client interface,
   * because the dynamic="false" bindings are globally disabled.
   */
  public void testDisableDynamicFalseBindings() throws Exception {
    try {
      System.setProperty(BindingDecorationUtil.CECILIA_DISABLE_DYNAMICFALSE_BINDINGS,
          "true");

      final File executable = compileADL(TEST_STATICBINDING_PREFIX
          + "ClientServerInvalidRuntimeRebindOfDynamicFalseUsingBC");

      final int r = runTestAndReturnValue(executable, TEST_STATICBINDING_PREFIX
          + "ClientServerInvalidRuntimeRebindOfDynamicFalseUsingBC");

      // assert it exited with a not OK status code
      assertTrue(r != 0);
    }
    finally {
      System.setProperty(BindingDecorationUtil.CECILIA_DISABLE_DYNAMICFALSE_BINDINGS,
          "false");
    }
  }

  /**
   * XXX NOTE: this test makes sense if the component type having this
   * implementation maintains the *_this pointer in method signatures.
   */
  public void testSimpleDynamicFalseUsingPassedThisArgument() throws Exception {
    this.compileAndRun(TEST_STATICBINDING_PREFIX
        + "ClientServerDynamicFalseUsingPassedThisArgument");
  }
}
