/***
 * Cecilia ADL Compiler
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.test.optC;

import junit.framework.TestCase;

import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;
import org.objectweb.fractal.cecilia.adl.bindings.BindingDecorationUtil;
import org.objectweb.fractal.cecilia.adl.interfaces.InterfaceDecorationUtil;
import org.objectweb.fractal.cecilia.adl.types.ComponentTypeDesc;

/**
 * This class tests various scenarios in which two components may be designated
 * to belong or not to the same component type, according to what is implemented
 * in the {@link ComponentTypeDesc#equals} method.
 * 
 * @author Alessio Pace
 */
public class ComponentTypeDescTest extends TestCase {

  public static final String      CECILIA_DTD = "classpath://org/objectweb/fractal/cecilia/adl/parser/xml/cecilia.dtd";
  private ComponentContainer      firstComponentType;
  private ComponentContainer      secondComponentType;
  private ComponentTypeDesc firstComponentTypeDesc;
  private ComponentTypeDesc secondComponentTypeDesc;
  private XMLNodeFactory          xmlNodeFactory;

  private TypeInterface           aServerInterface;
  private TypeInterface           anotherServerInterface;

  @Override
  public void setUp() throws Exception {

    xmlNodeFactory = new XMLNodeFactoryImpl();

    firstComponentType = createComponent();
    secondComponentType = createComponent();

    firstComponentTypeDesc = new ComponentTypeDesc(
        firstComponentType);
    secondComponentTypeDesc = new ComponentTypeDesc(
        secondComponentType);

    aServerInterface = createTypeInterface("s", "server", "x.y.Z", "singleton",
        "mandatory");

    anotherServerInterface = createTypeInterface("s", "server", "x.y.Z",
        "singleton", "mandatory");
  }

  protected ComponentContainer createComponent() throws Exception {
    return (ComponentContainer) xmlNodeFactory.newXMLNode(CECILIA_DTD,
        "component");
  }

  protected TypeInterface createTypeInterface(final String name,
      final String role, final String signature, final String cardinality,
      final String contingency) throws Exception {
    final TypeInterface result = (TypeInterface) xmlNodeFactory.newXMLNode(
        CECILIA_DTD, "interface");

    result.setName(name);
    result.setRole(role);
    result.setSignature(signature);
    result.setCardinality(cardinality);
    result.setContingency(contingency);

    return result;
  }

  protected void addInterface(final ComponentContainer component,
      final TypeInterface interfase) {

    ((InterfaceContainer) component).addInterface(interfase);
  }

  // =====================================================================
  // test methods

  public void testComponentsWithoutInterfacesOrImplementation()
      throws Exception {

    assertEquals("Assert same component type", firstComponentTypeDesc,
        secondComponentTypeDesc);
  }

  public void testComponentsWithEquivalentTypeInterfaceBoundDynamicallyToSameServerInterface()
      throws Exception {

    final TypeInterface firstCompClientItf = createTypeInterface("c", "client",
        "x.y.Z", "singleton", "mandatory");
    addInterface(firstComponentType, firstCompClientItf);
/* bound as if it was dynamic="true" */
    BindingDecorationUtil.setDynamic(
        (Node) firstCompClientItf, true);
    InterfaceDecorationUtil.setBoundTo(firstCompClientItf, aServerInterface);

    final TypeInterface secondCompClientItf = createTypeInterface("c",
        "client", "x.y.Z", "singleton", "mandatory");
    addInterface(secondComponentType, secondCompClientItf);
/* bound as if it was dynamic="true" */
    BindingDecorationUtil.setDynamic(
        (Node) secondCompClientItf, true);
    InterfaceDecorationUtil.setBoundTo(secondCompClientItf, aServerInterface);

    assertEquals("Assert same component type", firstComponentTypeDesc,
        secondComponentTypeDesc);
  }

  public void testComponentsWithEquivalentTypeInterfaceBoundDynamicallyToDifferentServerInterfaces()
      throws Exception {

    final TypeInterface firstCompClientItf = createTypeInterface("c", "client",
        "x.y.Z", "singleton", "mandatory");
    addInterface(firstComponentType, firstCompClientItf);
    /* bound as if it was dynamic="true" */
    BindingDecorationUtil.setDynamic(
        (Node) firstCompClientItf, true);
    InterfaceDecorationUtil.setBoundTo(firstCompClientItf, aServerInterface);

    final TypeInterface secondCompClientItf = createTypeInterface("c",
        "client", "x.y.Z", "singleton", "mandatory");
    addInterface(secondComponentType, secondCompClientItf);
    /* bound as if it was dynamic="true" */
    BindingDecorationUtil.setDynamic(
        (Node) secondCompClientItf, true);
    InterfaceDecorationUtil.setBoundTo(secondCompClientItf,
        anotherServerInterface);

    assertEquals("Assert same component type", firstComponentTypeDesc,
        secondComponentTypeDesc);
  }

  public void testComponentsWithEquivalentTypeInterfaceButDifferentDynamicConfiguration()
      throws Exception {

    final TypeInterface firstCompClientItf = createTypeInterface("c", "client",
        "x.y.Z", "singleton", "mandatory");
    addInterface(firstComponentType, firstCompClientItf);
    /* bound as if it was dynamic="false" */
    BindingDecorationUtil.setDynamic(
        (Node) firstCompClientItf, false);
    InterfaceDecorationUtil.setBoundTo(firstCompClientItf, aServerInterface);

    final TypeInterface secondCompClientItf = createTypeInterface("c",
        "client", "x.y.Z", "singleton", "mandatory");
    addInterface(secondComponentType, secondCompClientItf);
    /* bound as if it was dynamic="true" */
    BindingDecorationUtil.setDynamic(
        (Node) secondCompClientItf, true);
    InterfaceDecorationUtil.setBoundTo(secondCompClientItf, aServerInterface);

    assertFalse("Assert different component type", firstComponentTypeDesc
        .equals(secondComponentTypeDesc));
  }

  public void testComponentsWithEquivalentTypeInterfaceBothDynamicFalseButBoundToDifferentServerInterface()
      throws Exception {

    final TypeInterface firstCompClientItf = createTypeInterface("c", "client",
        "x.y.Z", "singleton", "mandatory");
    addInterface(firstComponentType, firstCompClientItf);
    /* bound as if it was dynamic="false" */
    BindingDecorationUtil.setDynamic(
        (Node) firstCompClientItf, false);
    InterfaceDecorationUtil.setBoundTo(firstCompClientItf, aServerInterface);

    final TypeInterface secondCompClientItf = createTypeInterface("c",
        "client", "x.y.Z", "singleton", "mandatory");
    addInterface(secondComponentType, secondCompClientItf);
    /* bound as if it was dynamic="false" */
    BindingDecorationUtil.setDynamic(
        (Node) secondCompClientItf, false);
    InterfaceDecorationUtil.setBoundTo(secondCompClientItf,
        anotherServerInterface);

    assertFalse("Assert different component type", firstComponentTypeDesc
        .equals(secondComponentTypeDesc));
  }

  public void testComponentsWithEquivalentTypeInterfaceBothDynamicFalseAndBoundToSameServerInterface()
      throws Exception {

    final TypeInterface firstCompClientItf = createTypeInterface("c", "client",
        "x.y.Z", "singleton", "mandatory");
    addInterface(firstComponentType, firstCompClientItf);
    /* bound as if it was dynamic="false" */
    BindingDecorationUtil.setDynamic(
        (Node) firstCompClientItf, false);
    InterfaceDecorationUtil.setBoundTo(firstCompClientItf, aServerInterface);

    final TypeInterface secondCompClientItf = createTypeInterface("c",
        "client", "x.y.Z", "singleton", "mandatory");
    addInterface(secondComponentType, secondCompClientItf);
    /* bound as if it was dynamic="false" */
    BindingDecorationUtil.setDynamic(
        (Node) secondCompClientItf, false);
    InterfaceDecorationUtil.setBoundTo(secondCompClientItf, aServerInterface);

    assertTrue("Assert same component type", firstComponentTypeDesc
        .equals(secondComponentTypeDesc));
  }
}
