/***
 * Cecilia ADL Compiler
 * Copyright (C) 2009 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.test;

import java.util.List;

import junit.framework.TestCase;

import org.objectweb.fractal.cecilia.adl.directives.DirectiveHelper;

public class DirectiveHelperTest extends TestCase {

  public void testSplitOptionString() throws Exception {
    assertSplit("", new String[0]);
    assertSplit("a", "a");
    assertSplit("a b", "a", "b");
    assertSplit("a  b", "a", "b");
    assertSplit("a\\ b", "a b");
    assertSplit("a\\  b", "a ", "b");
    assertSplit("a \\ b", "a", " b");

    assertSplit("a\\", "a\\");

    assertSplit("a\tb", "a", "b");
    assertSplit("a\nb", "a", "b");

  }

  protected static void assertSplit(final String s, final String... expected) {
    final List<String> actual = DirectiveHelper.splitOptionString(s);
    assertEquals(expected.length, actual.size());
    for (int i = 0; i < expected.length; i++) {
      assertEquals(expected[i], actual.get(i));
    }
  }
}
