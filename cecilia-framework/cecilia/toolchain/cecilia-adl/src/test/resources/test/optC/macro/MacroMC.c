/**
 * Cecilia, an implementation of the Fractal component model in C.
 * Copyright (C) 2006 STMicroelectronics
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

typedef struct {
  int a;
} PRIVATE_DATA;

#include <stdio.h>

jint METHOD(r, run)() {
  int err;
  Rfractal_api_Component * ci = GET_MY_OWNER;
  void * runItf = SERVER_ITF(r);
  void * r;

  err = CALL(ci, getFcInterface) ("r", &r);
  if (r != runItf) {
    printf("SERVER_ITF(r) and getFcInterface('r') did not lead to same interface pointer\n");
    return -1;
  }

  // just try to assign a value using the macro
  THIS.a = 1;

  return 0;
}
