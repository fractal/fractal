/**
 * Cecilia, an implementation of the Fractal component model in C.
 * Copyright (C) 2006 STMicroelectronics
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

DECLARE_DATA{
  int a;
};

#include <cecilia.h>

#define MAGIC_NB 2165489

void CONSTRUCTOR(void* _this) {
  DATA.a = MAGIC_NB;
}

void DESTRUCTOR(void* _this) {
  DATA.a = 0;
}

jint METHOD(r, run)(void* _this) {
  if (DATA.a != MAGIC_NB) {
    return 1;
  }
  return 0;
}
