
#include <stdio.h>

typedef struct {
  int toBeInitialized;
} PRIVATE_DATA;

jint METHOD(r, run)() {
	
	if(DATA.toBeInitialized != 3)  {
		return 1;	// it has not been initialized
	}else {
		return 0;	// OK
	}
}

jint METHOD(lifecycle_controller, startFc)() {
	
	// initialize data 
	DATA.toBeInitialized = 3;

	return 0;
}

jint METHOD(lifecycle_controller, stopFc)(){
	return 0;
}

jint METHOD(lifecycle_controller, getFcState)() {
	return 0;
}
