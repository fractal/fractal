
/** all the includes *except* cecilia.h */
#include <stdio.h>

/** Declare component internal data, empty here. */
DECLARE_DATA {
} ;

/** Include cecilia.h. Must be included after the DECLARE_DATA */
#include <cecilia.h>

// -----------------------------------------------------------------------------
// Implementation of the 'main' boot interface.
// -----------------------------------------------------------------------------

// int main(int argc, string[] argv)
int METHOD(in, execute) (void *_this, int id){

  printf("Hello, you are in cc :-) \n");
 
 return 0; 
}
