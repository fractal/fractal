
#include <stdio.h>

DECLARE_DATA {
  int toBeInitialized;
};

#include <cecilia.h>

jint METHOD(r, run)(void *_this) {
	
	if(DATA.toBeInitialized != 3)  {
		return 1;	// it has not been initialized
	}else {
		return 0;	// OK
	}
}

jint METHOD(lc, startFc)(void* _this) {
	
	// initialize data 
	DATA.toBeInitialized = 3;

	return 0;
}

jint METHOD(lc, stopFc)(void* _this){
	return 0;
}

jint METHOD(lc, getFcState)(void* _this) {
	return 0;
}
