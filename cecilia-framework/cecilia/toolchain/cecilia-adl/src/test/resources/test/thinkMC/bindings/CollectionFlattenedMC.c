/**
 * Cecilia, a software framework for component-based operating system kernels.
 * Copyright (C) 2009 INRIA
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Lionel Debroux
 */
#include <stdint.h>
#include <string.h>

DECLARE_DATA {
};

#include <cecilia.h>

jint METHOD(r, run)(void *_this) {
  if (CALL(REQUIRED.c1, run) != 0) return 1;
  if (CALL(REQUIRED.c2, run) != 0) return 2;
  return 0;
}
