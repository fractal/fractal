DECLARE_DATA {};

#include <cecilia.h>

/**
 * Assert setting the 'a' attribute to 0, 
 * using the ATTRIBUTES macro.
 */
jint METHOD(r, run)(void* _this) {

	int initialValue = ATTRIBUTES.a;
	
	// check the initial value as defined in the .fractal file
	if(initialValue != 3) {
	    return 1;
	}
	
	// set the attribute to 0 using the macro ATTRIBUTES
	ATTRIBUTES.a = 0;

	// check that the value has indeed been set
	if(ATTRIBUTES.a !=0) {
		// if not, return an error code
		return 2;
	}
	else {
		return fractal_api_ErrorConst_OK;
	}
}

