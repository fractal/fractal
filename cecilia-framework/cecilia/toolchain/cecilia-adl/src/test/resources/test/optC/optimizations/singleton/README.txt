Test files in this package are meant to verify eventual optimizations in the case of singleton component types:
 * in which access to instance data structures (dates, attributes, interface pointers) can be accessed as global variables
   (the one of the singleton instance)
 * the functional interfaces of the singleton component are generated without the *_this pointer ("static interfaces")
 
The functions of an interface "s" server of a component "foo" can be generated in "static" fashion (singleton style, == without the _this) 
if and only if:

1) the component "foo" is a singleton component type
2) every client interface which is bound to the interface "s" of the component "foo" is bound as <binding dynamic="false />
