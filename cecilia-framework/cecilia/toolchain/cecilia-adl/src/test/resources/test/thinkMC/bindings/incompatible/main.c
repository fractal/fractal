
/** all the includes *except* cecilia.h */
#include <stdio.h>

/** Declare component internal data, empty here. */
DECLARE_DATA {
} ;

/** Include cecilia.h. Must be included after the DECLARE_DATA */
#include <cecilia.h>

// -----------------------------------------------------------------------------
// Implementation of the 'main' boot interface.
// -----------------------------------------------------------------------------

int METHOD(r, run) (void *_this){

  printf("calling m1 \n");
  CALL(REQUIRED.out, m1, 0, 0);
  
  printf("calling m2, will segfault \n");
  CALL(REQUIRED.out, m2, 0);
  
  //unreachable
  printf("calling m3... \n");
  CALL(REQUIRED.out, m3, 0, 0);
 
  
  return 0;
}
