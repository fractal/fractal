/**
 * Cecilia, an implementation of the Fractal component model in C.
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */
#include <stdio.h>

DECLARE_DATA {
  // empty
};


#include <cecilia.h>

jint METHOD(r, run)(void* _this) {
 
  /* check that it works in reading the value */
  if(ATTRIBUTES.y != 2) {
	  return 1;
  }
  
  Rfractal_api_AttributeController* ac = GET_MY_INTERFACE(attribute_controller);
  
  if(ac == NULL) {
	  return 2;
  }
  
  /* the "x" attribute should be constant */
  CALL(ac, setFcAttribute, "y", (void*) 10);
  
  int newValue = ATTRIBUTES.y;
  
  if(newValue != 2) {
	  printf("'y' attribute current value is: %d [expected wrong value is 10], but should not change and still be 2\n", newValue);
	  return 3;
  }
  
  return 0;
}
