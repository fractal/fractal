/**
 * Cecilia, an implementation of the Fractal component model in C.
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

typedef struct {
  // empty
} PRIVATE_DATA;

#include "fractal/api/LifeCycleController.idl.h"
#include "test/optC/api/Run.idl.h"


jint METHOD(r, run)() {
  Rtest_optC_factory_GetNext *fact_client;
  Rtest_optC_factory_GetNext *fact_server;
  Rtest_optC_api_Run *fact_run;
  
  Rfractal_api_Component *comp1;
  void *comp1_lcc;
  void *comp1_client;
  Rtest_optC_factory_GetNext *comp1_server;
  Rtest_optC_api_Run *comp1_run;
  
  Rfractal_api_Component *comp2;
  void *comp2_lcc;
  void *comp2_client;
  Rtest_optC_factory_GetNext *comp2_server;
  Rtest_optC_api_Run *comp2_run;
  
  
  fact_client = CLIENT_ITF(factory_get_next);
  fact_server = (Rtest_optC_factory_GetNext *) CALL(fact_client, getNext) ();
  fact_run = (Rtest_optC_api_Run *) CALL(fact_server, getNext) ();
  
  CALL(fact_client, initI) ();
  if (CALL(fact_client, getI) () != 0) return 1;
  
  CLIENT(cloneable_factory, newFcInstance) (&comp1);
  CALL(comp1, getFcInterface) ("lifecycle-controller", &comp1_lcc);
  CALL((Rfractal_api_LifeCycleController *)comp1_lcc, startFc) ();
  
  CALL(comp1, getFcInterface) ("get-next", &comp1_client); // HERE
  comp1_server = (Rtest_optC_factory_GetNext *) CALL((Rtest_optC_factory_GetNext *)comp1_client, getNext) ();
  comp1_run = (Rtest_optC_api_Run *) CALL(comp1_server, getNext) ();
  
  if (CALL((Rtest_optC_factory_GetNext *)comp1_client, getI) () != 1) return 2;
  if (CALL(fact_client, getI) () != 1) return 3;
  
  CLIENT(cloneable_factory, newFcInstance) (&comp2);
  CALL(comp2, getFcInterface) ("lifecycle-controller", &comp2_lcc);
  CALL((Rfractal_api_LifeCycleController *)comp2_lcc, startFc) ();
  
  CALL(comp2, getFcInterface) ("get-next", &comp2_client); // HERE
  comp2_server = (Rtest_optC_factory_GetNext *) CALL((Rtest_optC_factory_GetNext *)comp2_client, getNext) ();
  comp2_run = (Rtest_optC_api_Run *) CALL(comp2_server, getNext) ();
  
  
  if (CALL((Rtest_optC_factory_GetNext *)comp2_client, getI) () != 2) return 4;
  if (CALL((Rtest_optC_factory_GetNext *)comp1_client, getI) () != 2) return 5;
  if (CALL(fact_client, getI) () != 2) return 6;
  
  if (comp1 == comp2) return 7;

  if (fact_client == comp1_client) return 10;
  if (fact_server == comp1_server) return 20;
  if (fact_run != comp1_run) return 30;
  
  if (fact_client == comp2_client) return 40;
  if (fact_server == comp2_server) return 50;
  if (fact_run != comp2_run) return 60;
  
  if (comp1_client == comp2_client) return 70;
  if (comp1_server == comp2_server) return 80;
  if (comp1_run != comp2_run) return 90;

  CLIENT(cloneable_factory, destroyFcInstance) (comp1);
  CLIENT(cloneable_factory, destroyFcInstance) (comp2);
  return 0;
}
