/**
 * Cecilia, an implementation of the Fractal component model in C.
 * Copyright (C) 2006-2008 STMicroelectronics
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

/**
 * Include the generated idl.h file with the _static prefix
 */
#include <test/optimizations/Service_static.idl.h>

/* function prototype without *_this  */
extern char* compType_getImplementationName(void);

/* VFT called with a _static postfix */
struct Mtest_optimizations_Service_static methods = { &compType_getImplementationName };

/* Interface structure with the _static prefix */
Rtest_optimizations_Service_static itf = { &methods, (void*)0 };
