/**
 * Cecilia, an implementation of the Fractal component model in C.
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

#include <string.h>

typedef struct {
  // empty
} PRIVATE_DATA;

jint METHOD(r, run)() {
  if (ATTR(a) == 3) {
    if (ATTR(b.a) == 4) {
      if (   (ATTR(c)[0] == 0x00)
          && (ATTR(c)[1] == 0x55)
          && (ATTR(c)[2] == 0xAA)
          && (ATTR(c)[3] == 0xFF)
         ) {
        if (ATTR(d) == 1) {
          if (!strcmp(ATTR(e),"foo")) {
            if (ATTR(f) == (uint32_t *)0x12345678) {
              if (   (ATTR(g)[0][0] == 0x0000)
                  && (ATTR(g)[0][1] == 0x5555)
                  && (ATTR(g)[1][0] == 0xAAAA)
                  && (ATTR(g)[1][1] == 0xFFFF)
                 ) {
                if (ATTR(h) == (size_t)419) { // ((6 *34 + 2) << 1) ^ 077
                  return 0;
                }
                else
                  return 8;
              }
              else
                return 7;
            }
            else
              return 6;
          }
          else
            return 5;
        }
        else 
          return 4;
      }
      else
        return 3;
    }
    else
      return 2;
  }
  else
    return 1;
}
