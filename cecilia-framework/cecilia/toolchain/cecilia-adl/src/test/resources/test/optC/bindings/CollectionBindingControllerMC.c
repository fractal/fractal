/**
 * Cecilia, a software framework for component-based operating system kernels.
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */
#include <stdint.h>
#include <string.h>

#define MAGIC_NB UINT32_C(0xfedabc02)

typedef struct {
  uint32_t bound1;
  Rtest_optC_api_Run *c1;

  uint32_t bound2;
  Rtest_optC_api_Run *c2;
} PRIVATE_DATA;

jint METHOD(r, run)() {
  if (DATA.bound1 != MAGIC_NB || DATA.bound2 != MAGIC_NB) {
    return 1;
  }
  if (CALL(DATA.c1, run) () != 0) return 2;
  if (CALL(DATA.c2, run) () != 0) return 3;
  return 0;
}

// -----------------------------------------------------------------------------
// Implementation of the binding-controller interface.
// -----------------------------------------------------------------------------

// int listFc(in string[] clientItfNames);
int METHOD(binding_controller, listFc)(const char* clientItfNames[])
{
  if (clientItfNames != NULL) {
    clientItfNames[0] = "c";
  }
  return 1;
}

// int lookupFc(in string clientItfName, out any interfaceReference);
int METHOD(binding_controller, lookupFc)(const char* clientItfName, void** interfaceReference)
{
  if (interfaceReference == NULL || clientItfName == NULL) {
    return fractal_api_ErrorConst_INVALID_ARGUMENT;
  }

  if (strcmp(clientItfName, "c1") == 0) {
    *interfaceReference = DATA.c1;
    return fractal_api_ErrorConst_OK;
  } else if (strcmp(clientItfName, "c2") == 0) {
    *interfaceReference = DATA.c2;
    return fractal_api_ErrorConst_OK;
  } else {
    return fractal_api_ErrorConst_NO_SUCH_INTERFACE;
  }
}

// int bindFc(in string clientItfName, in any serverItf);
int METHOD(binding_controller, bindFc)(const char* clientItfName, void* serverItf)
{
  if (clientItfName == NULL) {
    return fractal_api_ErrorConst_INVALID_ARGUMENT;
  }

  if (strcmp(clientItfName, "c1") == 0) {
    DATA.bound1 = MAGIC_NB;
    DATA.c1 = (Rtest_optC_api_Run *) serverItf;
    return fractal_api_ErrorConst_OK;
  } else if (strcmp(clientItfName, "c2") == 0) {
    DATA.bound2 = MAGIC_NB;
    DATA.c2 = (Rtest_optC_api_Run *) serverItf;
    return fractal_api_ErrorConst_OK;
  } else {
    return fractal_api_ErrorConst_NO_SUCH_INTERFACE;
  }
}

// int unbindFc(in string clientItfName);
int METHOD(binding_controller, unbindFc)(const char* clientItfName)
{
  if (clientItfName == NULL) {
    return fractal_api_ErrorConst_INVALID_ARGUMENT;
  }

  if (strcmp(clientItfName, "c1") == 0) {
    DATA.c1 = NULL;
    return fractal_api_ErrorConst_OK;
  } else if (strcmp(clientItfName, "c2") == 0) {
    DATA.c2 = NULL;
    return fractal_api_ErrorConst_OK;
  } else {
    return fractal_api_ErrorConst_NO_SUCH_INTERFACE;
  }
}

void* METHOD(binding_controller, lookup)(const char* clientItfName) {
  if (strcmp(clientItfName, "c1") == 0) return DATA.c1;
  if (strcmp(clientItfName, "c2") == 0) return DATA.c2;
  return NULL;
}
