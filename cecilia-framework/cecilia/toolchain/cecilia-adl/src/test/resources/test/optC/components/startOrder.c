/**
 * Cecilia, a software framework for component-based operating system kernels.
 * Copyright (C) 2008 STMicroelectronics
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

typedef struct {
	int startValue;
} PRIVATE_DATA;

#include <cecilia_opt.h>


void CONSTRUCTOR(void *_this) {
	DATA.startValue = 0;
}

jint METHOD(r, run)() {
  if (DATA.startValue != ATTR(a))
    return 1;
  else if (CLIENT_ITF(c) != NULL)
    return CLIENT(c, run) ();
  else
    return 0;
}

jint METHOD(lifecycle_controller, getFcState)() {
  return 0;
}

jint METHOD(lifecycle_controller, startFc)() {
  DATA.startValue = CLIENT(startOrder, getStartOrder) ();
  return 0;
}

jint METHOD(lifecycle_controller, stopFc)() {
  return 0;
}
