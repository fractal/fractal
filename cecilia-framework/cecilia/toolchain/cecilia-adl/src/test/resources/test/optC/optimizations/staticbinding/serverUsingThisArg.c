/**
 * Cecilia, an implementation of the Fractal component model in C.
 * Copyrigth (C) 2006-2008 STMicroelectronics
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

typedef struct {
  // empty
} PRIVATE_DATA;


/**
 * NOTE: this test makes sense if the component type having this implementation 
 * mantains the *_this pointer in method signatures.
 */
char* METHOD(s, getImplementationName)() {

  if(_this != SERVER_ITF(component)) {
	  return (char *) "Error: *_this does not correspond to the component memory address";
  }
  	
  return (char *)"server1";
}
