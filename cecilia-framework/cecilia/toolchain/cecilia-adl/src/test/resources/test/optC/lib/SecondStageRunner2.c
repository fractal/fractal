/**
 * Cecilia, an implementation of the Fractal component model in C.
 * Copyright (C) 2006-2008 STMicroelectronics
 * Copyright (C) 2007-2008 INRIA
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 * Contributor: Alessio Pace
 */

// include generated header file from IDL fractal.api.LifeCycleController
#include "fractal/api/LifeCycleController.idl.h"

typedef struct {
  // empty
} PRIVATE_DATA;


int METHOD(boot, run) () {

  jint err, testResult;

  if(CLIENT_ITF(main) == NULL) {
    return 1;
  }
  
  // start the test component.
  if(CLIENT_ITF(lcc) != NULL) {
    err = CLIENT(lcc, startFc)();
    if (err != fractal_api_ErrorConst_OK) return err;
  }

  // call the "r" interface
  testResult = CLIENT(main, run)();

  // stop the test component.
  if(CLIENT_ITF(lcc) != NULL) {
    err = CLIENT(lcc, stopFc)();
    if (err != fractal_api_ErrorConst_OK) return err;
  }

  return testResult;
}
