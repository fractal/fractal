DECLARE_DATA{};

#include <cecilia.h>

#define P (sizeof(void *))
#define I (sizeof(int))

int isDiff(int a, int b) {
  return a != b;
}

jint METHOD(r, run)(void* _this) {
  Rtest_thinkMC_idl_interfaces_R r;
  if (isDiff(sizeof(r.a1), P)) return 1;
  if (isDiff(sizeof(r.a2), 10 * I)) return 2;
  if (isDiff(sizeof(r.a3), 100 * I)) return 3;

  if (isDiff(sizeof(r.b1), P)) return 4;
  if (isDiff(sizeof(r.b2), P)) return 5;
  if (isDiff(sizeof(*(r.b2)), P)) return 6;
  if (isDiff(sizeof(r.b3), 10 * P)) return 7;
  if (isDiff(sizeof(r.b4), 100 * P)) return 8;
  if (isDiff(sizeof(r.b5), 10 * P)) return 9;
  
  return 0;
}
