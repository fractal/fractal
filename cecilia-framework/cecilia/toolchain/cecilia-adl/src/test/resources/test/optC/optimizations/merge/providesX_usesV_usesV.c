/**
 * Cecilia
 * Copyright (C) 2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

#include <string.h>

typedef struct {
  char* mydata;
} PRIVATE_DATA;

/**
 * Implementation of Xxx#uuu() method
 */
char* METH(xxx) () {
	
	//	TODO uncomment when instance data merging is done.
	//	THIS.mydata = "Xxx";
	
	char* resultForC1 = CLIENT(c1, vvv)();
	if(strcmp("Vvv", resultForC1) != 0) {
	  return (char *)"WRONG RESULT FOR client(c1,vvv)()";
	}
	
	char* resultForC2 = CLIENT(c2, vvv)();
	if(strcmp("Vvv", resultForC2) != 0) {
	  return (char *)"WRONG RESULT FOR client(c2,vvv)()";
	}
	
	// return THIS.mydata;
	return (char *)"Xxx";
	
}
