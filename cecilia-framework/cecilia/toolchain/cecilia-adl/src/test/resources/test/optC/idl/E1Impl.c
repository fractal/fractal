
typedef struct {
  // empty
} PRIVATE_DATA;

#include "cecilia_opt.h"

jint METHOD (r, run) () {
	
	jint result = SERVER(e, useEnum) (FIRST);
	if(result != FIRST) {
		return 1;  // error
	}
	
	result = SERVER(e, returnEnum) ();
	if(result != SECOND) {
		return 2;  // error
	}
	
	return 0;
}

// jint (*useEnum)(Rtest_optC_idl_enums_E1 e);
jint METHOD(e, useEnum) (Rtest_optC_idl_enums_E1 e) {
	
	return e;
}

// Rtest_optC_idl_enums_E1 (*returnEnum)();
Rtest_optC_idl_enums_E1 METHOD(e, returnEnum) () {
	
	return SECOND;
}

//void (*useSelf)(Rtest_optC_idl_enums_UsingEnum* i);
void METHOD(e, useSelf) (Rtest_optC_idl_enums_UsingEnum* i) {
	//
}

