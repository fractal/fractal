/**
 * Cecilia, an implementation of the Fractal component model in C.
 * Copyright (C) 2006-2008 STMicroelectronics
 * Copyright (C) 2007-2008 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

typedef struct {
	//
} PRIVATE_DATA;

/**
 */
jint METH(run)() {
	
	/* Test that the "c" client interface is of this kind, accessing it through CLIENT_ITF() macro */
	Rtest_optC_api_Run_static *cItf = CLIENT_ITF(c);
	
	if(cItf == NULL) {
		return 1;
	}
	
	/* verify calling "run" on the "c" client interface will be expanded correctly */
	return CLIENT(c, run)();
}
