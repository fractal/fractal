/**
 * Cecilia, an implementation of the Fractal component model in C.
 *
 * Copyright (C) 2009 INRIA SARDES
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Lionel Debroux
 */

/** Declare component internal data */
typedef struct {
  // empty
} PRIVATE_DATA;

jint METH(run) () {
  jint retval;
  
  retval = CLIENT(s, getValue) ();
  if (retval != 12345) {
    return 1;
  }

  void * acItf = SERVER_ITF(attribute_controller);
  if (acItf == NULL) {
    return 2;
  }

  retval = CALL((Rfractal_api_AttributeController *)acItf, setFcAttribute) ("value", (void *)54321);
  if (retval != 0) {
    return 3;
  }

  retval = CLIENT(s, getValue) ();
  if (retval != 54321) {
    return 4;
  }

  return 0;
}
