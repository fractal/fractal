/**
 * Cecilia, a software framework for component-based operating system kernels.
 * Copyright (C) 2009 INRIA
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Lionel Debroux
 */

#include <stdio.h>

DECLARE_DATA {
};

#include <cecilia.h>

static const char * const expectedItfNames[] = {"c1", "c2", "c3"};
static const char * retrievedItfNames[5];

jint METHOD(r, run)(void *_this) {
  void * itfRef;
    
  if (CALL(REQUIRED.c1, run) != 0) return 1;
  if (CALL(REQUIRED.c2, run) != 0) return 2;

  if (CALL(GET_MY_INTERFACE(binding_controller), listFc, retrievedItfNames) != 3) return 3;

  if (CALL(GET_MY_INTERFACE(binding_controller), lookupFc, "c1", &itfRef) != fractal_api_ErrorConst_OK) return 4;
  if (itfRef != REQUIRED.c1) return 5;
  if (CALL(GET_MY_INTERFACE(binding_controller), lookupFc, "c2", &itfRef) != fractal_api_ErrorConst_OK) return 6;
  if (itfRef != REQUIRED.c2) return 7;
  if (CALL(GET_MY_INTERFACE(binding_controller), lookupFc, "c3", &itfRef) != fractal_api_ErrorConst_OK) return 8;
  if (itfRef != REQUIRED.c3) return 9;
  if (CALL(GET_MY_INTERFACE(binding_controller), lookupFc, "c4", &itfRef) != fractal_api_ErrorConst_NO_SUCH_INTERFACE) return 10;

  if (CALL(GET_MY_INTERFACE(binding_controller), bindFc, "c1", REQUIRED.c1) != fractal_api_ErrorConst_OK) return 11;
  if (CALL(GET_MY_INTERFACE(binding_controller), bindFc, "c2", REQUIRED.c2) != fractal_api_ErrorConst_OK) return 12;
  if (CALL(GET_MY_INTERFACE(binding_controller), bindFc, "c3", REQUIRED.c2) != fractal_api_ErrorConst_OK) return 13;
  if (CALL(REQUIRED.c3, run) != 0) return 14;
  if (CALL(GET_MY_INTERFACE(binding_controller), bindFc, "c4", REQUIRED.c2) != fractal_api_ErrorConst_NO_SUCH_INTERFACE) return 15;

  if (CALL(GET_MY_INTERFACE(binding_controller), unbindFc, "c3") != fractal_api_ErrorConst_OK) return 16;
  if (REQUIRED.c3 != NULL) return 17;

  return 0;
}
