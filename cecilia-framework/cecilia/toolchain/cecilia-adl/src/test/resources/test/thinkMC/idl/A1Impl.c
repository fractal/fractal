DECLARE_DATA {};

#include <cecilia.h>

void METHOD(itf, a1)(void *_this, int a[10]) {}
void METHOD(itf, a2)(void *_this, int a[10][10]) {}
void METHOD(itf, a3)(void *_this, int a[][10][10]) {}

void METHOD(itf, b1)(void *_this, int (*a)[10]) {}
void METHOD(itf, b2)(void *_this, int (*a)[10][10]) {}
void METHOD(itf, b3)(void *_this, int (*a)[][10][10]) {}

void METHOD(itf, c1)(void *_this, int* a) {}
void METHOD(itf, c2)(void *_this, int* a[10]) {}
void METHOD(itf, c3)(void *_this, int** a[10]) {}
void METHOD(itf, c4)(void *_this, int* a[][10][10]) {}
void METHOD(itf, c5)(void *_this, int** a[][10][10]) {}

void METHOD(itf, d1)(void *_this, int* (*a)) {}
void METHOD(itf, d2)(void *_this, int* (*a)[10]) {}
void METHOD(itf, d3)(void *_this, int** (*a)[10]) {}
void METHOD(itf, d4)(void *_this, int* (*a)[][10][10]) {}
void METHOD(itf, d5)(void *_this, int** (*a)[][10][10]) {}

int* METHOD(itf, e1)(void *_this) { return NULL; }
int* METHOD(itf, e2)(void *_this) { return NULL; }
int** METHOD(itf, e3)(void *_this) { return NULL; }
int*** METHOD(itf, e4)(void *_this) { return NULL; }

void METHOD(itf, g1)(void *_this, char* a) {}
void METHOD(itf, g2)(void *_this, char** a) {}
void METHOD(itf, g3)(void *_this, char* a[10]) {}
void METHOD(itf, g4)(void *_this, char* a[][10][10]) {}
void METHOD(itf, g5)(void *_this, char** a[10]) {}
void METHOD(itf, g6)(void *_this, char*** a[10]) {}
void METHOD(itf, g7)(void *_this, char*** a[][10]) {}

void METHOD(itf, h1)(void *_this, char* (*a)) {}
void METHOD(itf, h2)(void *_this, char** (*a)) {}
void METHOD(itf, h3)(void *_this, char* (*a)[10]) {}
void METHOD(itf, h4)(void *_this, char* (*a)[][10][10]) {}
void METHOD(itf, h5)(void *_this, char** (*a)[10]) {}
void METHOD(itf, h6)(void *_this, char*** (*a)[10]) {}
void METHOD(itf, h7)(void *_this, char*** (*a)[][10]) {}

char *METHOD(itf, i1)(void *_this) {return NULL;}
char **METHOD(itf, i2)(void *_this) {return NULL;}
char **METHOD(itf, i3)(void *_this) {return NULL;}
char ***METHOD(itf, i4)(void *_this) {return NULL;}

void METHOD(itf, j1)(void *_this, test_thinkMC_idl_interfaces_R a) {}
void METHOD(itf, j2)(void *_this, test_thinkMC_idl_interfaces_R *a) {}
void METHOD(itf, j3)(void *_this, test_thinkMC_idl_interfaces_R a[10]) {}
void METHOD(itf, j4)(void *_this, test_thinkMC_idl_interfaces_R a[][10]) {}
void METHOD(itf, j5)(void *_this, test_thinkMC_idl_interfaces_R a[10][10]) {}
void METHOD(itf, j6)(void *_this, test_thinkMC_idl_interfaces_R *a[10]) {}

void METHOD(itf, k1)(void *_this, test_thinkMC_idl_interfaces_R *a) {}
void METHOD(itf, k2)(void *_this, test_thinkMC_idl_interfaces_R **a) {}
void METHOD(itf, k3)(void *_this, test_thinkMC_idl_interfaces_R (*a)[10]) {}
void METHOD(itf, k4)(void *_this, test_thinkMC_idl_interfaces_R (*a)[][10]) {}
void METHOD(itf, k5)(void *_this, test_thinkMC_idl_interfaces_R (*a)[10][10]) {}
void METHOD(itf, k6)(void *_this, test_thinkMC_idl_interfaces_R *(*a)[10]) {}

test_thinkMC_idl_interfaces_R  METHOD(itf, l1)(void *_this) {
  test_thinkMC_idl_interfaces_R r;
  return r;
}
test_thinkMC_idl_interfaces_R * METHOD(itf, l2)(void *_this) {return NULL;}
test_thinkMC_idl_interfaces_R * METHOD(itf, l3)(void *_this) {return NULL;}
test_thinkMC_idl_interfaces_R ** METHOD(itf, l4)(void *_this) {return NULL;}
