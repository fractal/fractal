/**
 * Cecilia, an implementation of the Fractal component model in C.
 * Copyright (C) 2007 INRIA
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

typedef struct {
  // empty
} PRIVATE_DATA;


jint METHOD(r, run) () {
	
	Rfractal_api_BindingController* bc = SERVER_ITF(binding_controller);
	
	if(bc == NULL) {
		return 1;
	}
	
	/* 
	 * XXX Trying to rebind the "c" client interface which is declared as dynamic="false" 
	 * should raise a compilation problem or a runtime exception (int result indicating an exception) ?
	 * */
	void* configuredClientItf = NULL;
	CALL(bc, lookupFc) ("c",  &configuredClientItf);
	
	if(configuredClientItf == NULL) {
		return 2;
	}
	
	int result = CALL(bc, bindFc) ("c", NULL);
	
	/* 
	 * The bindFc() should "throw" an exception (result code != 0) because the "c" interface 
	 * was bound as dynamic="false" 
	 */
	if(result == 0) {
		return 3;
	}
	
	return 0;
}
