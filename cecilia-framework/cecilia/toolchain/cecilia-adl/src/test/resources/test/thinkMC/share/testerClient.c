/** Declare component internal data, empty here. */
DECLARE_DATA {
};

/** Include think.h. Must be included after the DECLARE_DATA */
#include <cecilia.h>

/**
 * Implementation of the 'main' method of the interface 'r' of type 
 * 'activity.api.Main'
 */
jint METHOD(r, run)(void* _this) {
  int err, sharedCols, sharedRows, unsharedCols, unsharedRows;
  // These four variables are necessary to get rid of strict aliasing warnings.
  // See bug #307668.
  int * pSharedCols = &sharedCols;
  int * pSharedRows = &sharedRows;
  int * pUnsharedCols = &unsharedCols;
  int * pUnsharedRows = &unsharedRows;
  // call the 'putcs' method of the 'console' client interface.
  err = (int)CALL(REQUIRED.sharedConsoleAttributes, getFcAttribute, "cols", 
      (void **) pSharedCols);
  if (err != fractal_api_ErrorConst_OK) return err;
  err = (int)CALL(REQUIRED.sharedConsoleAttributes, getFcAttribute, "rows", 
      (void **) pSharedRows);
  if (err != fractal_api_ErrorConst_OK) return err;
  err = (int)CALL(REQUIRED.unsharedConsoleAttributes, getFcAttribute, "cols", 
      (void **) pUnsharedCols);
  if (err != fractal_api_ErrorConst_OK) return err;
  err = (int)CALL(REQUIRED.unsharedConsoleAttributes, getFcAttribute, "rows", 
      (void **) pUnsharedRows);
  if (err != fractal_api_ErrorConst_OK) return err;

  // Test the attribute values
  if(sharedCols == 30 && sharedRows == 40 && unsharedCols == 80 && unsharedRows == 21)
      return 0;
  else
      return -1;
}
