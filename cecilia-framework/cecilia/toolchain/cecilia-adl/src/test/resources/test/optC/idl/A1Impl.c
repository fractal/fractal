typedef struct {
  // empty
} PRIVATE_DATA;

void METHOD(itf, a1)(int a[10]) {}
void METHOD(itf, a2)(int a[10][10]) {}
void METHOD(itf, a3)(int a[][10][10]) {}

void METHOD(itf, b1)(int (*a)[10]) {}
void METHOD(itf, b2)(int (*a)[10][10]) {}
void METHOD(itf, b3)(int (*a)[][10][10]) {}

void METHOD(itf, c1)(int* a) {}
void METHOD(itf, c2)(int* a[10]) {}
void METHOD(itf, c3)(int** a[10]) {}
void METHOD(itf, c4)(int* a[][10][10]) {}
void METHOD(itf, c5)(int** a[][10][10]) {}

void METHOD(itf, d1)(int* (*a)) {}
void METHOD(itf, d2)(int* (*a)[10]) {}
void METHOD(itf, d3)(int** (*a)[10]) {}
void METHOD(itf, d4)(int* (*a)[][10][10]) {}
void METHOD(itf, d5)(int** (*a)[][10][10]) {}

int* METHOD(itf, e1)() { return NULL; }
int* METHOD(itf, e2)() { return NULL; }
int** METHOD(itf, e3)() { return NULL; }
int*** METHOD(itf, e4)() { return NULL; }

void METHOD(itf, g1)(char* a) {}
void METHOD(itf, g2)(char** a) {}
void METHOD(itf, g3)(char* a[10]) {}
void METHOD(itf, g4)(char* a[][10][10]) {}
void METHOD(itf, g5)(char** a[10]) {}
void METHOD(itf, g6)(char*** a[10]) {}
void METHOD(itf, g7)(char*** a[][10]) {}

void METHOD(itf, h1)(char* (*a)) {}
void METHOD(itf, h2)(char** (*a)) {}
void METHOD(itf, h3)(char* (*a)[10]) {}
void METHOD(itf, h4)(char* (*a)[][10][10]) {}
void METHOD(itf, h5)(char** (*a)[10]) {}
void METHOD(itf, h6)(char*** (*a)[10]) {}
void METHOD(itf, h7)(char*** (*a)[][10]) {}

char *METHOD(itf, i1)() {return NULL;}
char **METHOD(itf, i2)() {return NULL;}
char **METHOD(itf, i3)() {return NULL;}
char ***METHOD(itf, i4)() {return NULL;}

void METHOD(itf, j1)(test_optC_idl_interfaces_R a) {}
void METHOD(itf, j2)(test_optC_idl_interfaces_R *a) {}
void METHOD(itf, j3)(test_optC_idl_interfaces_R a[10]) {}
void METHOD(itf, j4)(test_optC_idl_interfaces_R a[][10]) {}
void METHOD(itf, j5)(test_optC_idl_interfaces_R a[10][10]) {}
void METHOD(itf, j6)(test_optC_idl_interfaces_R *a[10]) {}

void METHOD(itf, k1)(test_optC_idl_interfaces_R *a) {}
void METHOD(itf, k2)(test_optC_idl_interfaces_R **a) {}
void METHOD(itf, k3)(test_optC_idl_interfaces_R (*a)[10]) {}
void METHOD(itf, k4)(test_optC_idl_interfaces_R (*a)[][10]) {}
void METHOD(itf, k5)(test_optC_idl_interfaces_R (*a)[10][10]) {}
void METHOD(itf, k6)(test_optC_idl_interfaces_R *(*a)[10]) {}

test_optC_idl_interfaces_R  METHOD(itf, l1)() {
  test_optC_idl_interfaces_R r;
  return r;
}
test_optC_idl_interfaces_R * METHOD(itf, l2)() {return NULL;}
test_optC_idl_interfaces_R * METHOD(itf, l3)() {return NULL;}
test_optC_idl_interfaces_R ** METHOD(itf, l4)() {return NULL;}
