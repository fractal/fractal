
/** all the includes *except* cecilia_opt.h */
#include <stdio.h>

/** Declare component internal data, empty here. */
typedef struct {
  // empty
} PRIVATE_DATA;

// -----------------------------------------------------------------------------
// Implementation of the 'main' boot interface.
// -----------------------------------------------------------------------------

// int main(int argc, string[] argv)
int METHOD(in, execute) (int id){

  printf("Hello, you are in cc :-) \n");
 
 return 0; 
}
