
DECLARE_DATA {
	
};

#include "cecilia.h"

jint METHOD (r, run) (void* _this) {
	
	jint result = CALLMINE(e, useEnum, FIRST);
	if(result != FIRST) {
		return 1;  // error
	}
	
	result = CALLMINE(e, returnEnum);
	if(result != SECOND) {
		return 2;  // error
	}
	
	return 0;
}

// jint (*useEnum)(void *_this, Rtest_thinkMC_idl_enums_E1 e);
jint METHOD(e, useEnum) (void* _this, Rtest_thinkMC_idl_enums_E1 e) {
	
	return e;
}

// Rtest_thinkMC_idl_enums_E1 (*returnEnum)(void *_this);
Rtest_thinkMC_idl_enums_E1 METHOD(e, returnEnum) (void* _this) {
	
	return SECOND;
}

//void (*useSelf)(void *_this, Rtest_thinkMC_idl_enums_UsingEnum* i);
void METHOD(e, useSelf) (void* _this, Rtest_thinkMC_idl_enums_UsingEnum* i) {
	//
}

