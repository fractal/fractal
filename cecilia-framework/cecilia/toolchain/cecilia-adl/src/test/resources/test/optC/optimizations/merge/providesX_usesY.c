/**
 * Cecilia
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

#include <string.h>
#include <stdio.h>

typedef struct {
  char* mydata;
} PRIVATE_DATA;

/**
 * Implementing Xxx#xxx() method.
 */
char* METH(xxx) () {
	
	// TODO uncomment when instance data merging is done.
	// THIS.mydata = "Xxx";
	
	char* result = CLIENT(y,yyy)();
	if(strcmp("Yyy", result) != 0) {
	  printf("WRONG RESULT FOR client(y,yyy)() : [%s] instead of [Yyy]", result);
	  return (char *)"WRONG RESULT FOR client(y,yyy)()";
	}else {
		//	return THIS.mydata;
		return (char *)"Xxx";
	}
}
