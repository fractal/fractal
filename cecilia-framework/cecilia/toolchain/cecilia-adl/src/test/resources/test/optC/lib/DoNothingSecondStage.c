/**
 * Cecilia, an implementation of the Fractal component model in C.
 * Copyright (C) 2006 STMicroelectronics
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

// include generated header file from IDL fractal.api.LifeCycleController
#include "fractal/api/LifeCycleController.idl.h"

/** 
 * This second stage runner does not call the 'run' interface of the test 
 * component, It simply start and stop it and return 0. This is usefull if the 
 * test only checks that a given ADL compiles without error.
 */
typedef struct {
  // empty
} PRIVATE_DATA;

int METHOD(boot, run) () {
  void* lcc;
  jint err;

  // get the "lifecycle-controller" interface of the test component.
  err = CLIENT(test, getFcInterface) ("lifecycle-controller", &lcc);
  if (err == fractal_api_ErrorConst_OK) {

    // start test component.
    err = CALL((Rfractal_api_LifeCycleController *)lcc, startFc) ();
    if (err != fractal_api_ErrorConst_OK) return err;

    // stop test component.
    err = CALL((Rfractal_api_LifeCycleController *)lcc, stopFc) ();
    if (err != fractal_api_ErrorConst_OK) return err;

  } else if (err != fractal_api_ErrorConst_NO_SUCH_INTERFACE) {
    // an error occurs while getting LCC, return error code.
    return err;
  }
  return 0;
}
