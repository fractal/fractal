/**
 * Cecilia, an implementation of the Fractal component model in C.
 * Copyright (C) 2007 INRIA
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */
#include <string.h>
#include <stdio.h>

typedef struct {
  int a;
} PRIVATE_DATA;

int METHOD(r, run)() {

  /* simply test THIS macro */
  THIS.a = 0;

  /* simply test CLIENT_ITF macro */
  void* cItf = CLIENT_ITF(c);
  if (cItf == NULL) {
    return 1;
  }

  char* c1ItfImplementation = CLIENT(c, getImplementationName)();
  if (strcmp("server1", c1ItfImplementation) != 0) {
    return 2;
  }
  
  return 0;
}
