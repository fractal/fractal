/**
 * Cecilia, an implementation of the Fractal component model in C.
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

typedef struct {
	//
} PRIVATE_DATA;

jint METH(run)() {
	
	/* Test that the "r" server interface is of this kind, accessing it through SERVER_ITF() macro */
	Rtest_optC_api_Run_static *rItf = SERVER_ITF(r);
	
	if(rItf == NULL) {
		return 1;
	}
	
	return 0;
}
