<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE definition PUBLIC "-//objectweb.org//DTD Fractal ADL 2.0//EN"
  "classpath://org/objectweb/fractal/cecilia/adl/parser/xml/cecilia.dtd">

<!-- 
  A composite component containing two primitive components.
  
  THINGS TO NOTE:
  (1) the "x" interface of the first primitive is exported as "s" through the composite, so
    the primitive method implementation macro expansion should reflect the 
    type name and interface name of the composite component, because external components
    bound to the merged primitive would expect to use that function prototype, ie:
    
      char* test_optimizations_merge_Composite2_s_xxx_method()
  
    where xxx is the name of the method on the Xxx IDL interface.
    [ SERVER_OUTER case ]
  
  (2) The second primitive has a server interface "y" which is just used internally in the 
      composite, so in some way: 
        * be discovered by the RecursiveIDLDispatcherForMergedComponents
        * its function prototypes should be declared in the top level definition
          (see point (3) on why it is needed this)
      [ SERVER_INNER case]
      
  (3) The first primitive has also a client interface which is bound to the server
      interface of the second primitive, so its CLIENT(y,yyy) should be equivalent
      to a SERVER(y,yyy) because the prototype of the server interface of the second 
      primitive are declared in the top level definition which is in common with 
      the various modules (= primitives being fused)
      [ CLIENT_INNER case ]
 -->
<definition name="test.optC.optimizations.merge.Composite3" merge="true">

  <interface name="s" role="server" signature="test.optC.optimizations.merge.Xxx" />

  <component name="comp_providesX_usesY" >
    <interface name="x" role="server" signature="test.optC.optimizations.merge.Xxx" />
    <interface name="y" role="client" signature="test.optC.optimizations.merge.Yyy" />
    <content class="test.optC.optimizations.merge.providesX_usesY" language="optC" />
  </component>
  
  <!-- The "Yyy" interface function prototypes should then be declared  -->
  <component name="comp_providesY" >
    <interface name="y" role="server" signature="test.optC.optimizations.merge.Yyy" />
    <content class="test.optC.optimizations.merge.providesY" language="optC" />
  </component>
  
  <binding client="this.s" server="comp_providesX_usesY.x" />
  <binding client="comp_providesX_usesY.y" server="comp_providesY.y"  /> 
  
</definition>
