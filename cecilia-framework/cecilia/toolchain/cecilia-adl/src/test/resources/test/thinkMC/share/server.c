
/** Declare component internal data. */
DECLARE_DATA {
  jint          x, y;           /* Cursor position */
};

/** Include think.h. Must be included after the DECLARE_DATA */
#include <cecilia.h>
extern int printf(const char * format, ...);

/**
 * Implementation of the 'putxycs' method of the interface 'console' of type 
 * 'video.api.Console'
 */
void METHOD(console,putxycs)(void* _this, jint x, jint y, const char *str) {
  printf("{%2d,%2d} %s\n", x, y, str);
}

/**
 * Implementation of the 'scrollup' method of the interface 'console' of type 
 * 'video.api.Console'
 */
void METHOD(console,scrollup)(void* _this) {
  //nop;
}

/**
 * Implementation of the 'putc' method of the interface 'console' of type 
 * 'video.api.Console'
 */
void METHOD(console,putc)(void* _this, jchar c) {
  char ch[] = {0, 0};
  switch(c) {
  case '\b':
    if (DATA.x > 0)
      DATA.x--;
    break;
  case '\t':
  {
    int space = 8 - (DATA.x & 7);
    while(space--) {
      CALLMINE(console, putxycs, DATA.x++, DATA.y, " ");
    }
  }; break;
  case '\n':
    DATA.x = 0;
    DATA.y++;
    break;
  case '\r':
    DATA.x = 0;
    break;
  default:
    ch[0] = c;
    CALLMINE(console, putxycs, DATA.x++, DATA.y, ch);
  }
  // Next line if need
  if (DATA.x >= ATTRIBUTES.cols) {
	DATA.x = 0;
	DATA.y++;
  }
  // Scroll screen if need
  if(DATA.y >= ATTRIBUTES.rows) {
    CALLMINE(console, scrollup);
	DATA.y--;
  }
}

/**
 * Implementation of the 'putcs' method of the interface 'console' of type 
 * 'video.api.Console'
 */
void METHOD(console, putcs)(void* _this, const char *str) {
  while(*str)
    CALLMINE(console, putc, *str++);
}

/**
 * Implementation of the 'cols' method of the interface 'console' of type 
 * 'video.api.Console'
 */
jint METHOD(console, cols)(void* _this) {
  return ATTRIBUTES.cols;
}

/**
 * Implementation of the 'rows' method of the interface 'console' of type 
 * 'video.api.Console'
 */
jint METHOD(console,rows)(void* _this) {
  return ATTRIBUTES.rows;
}
