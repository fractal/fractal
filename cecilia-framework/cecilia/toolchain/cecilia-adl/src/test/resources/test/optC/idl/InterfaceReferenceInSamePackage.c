/**
 * Cecilia, an implementation of the Fractal component model in C.
 * Copyright (C) 2006 STMicroelectronics
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Ali Erdem Ozcan
 */

typedef struct {
  // empty
} PRIVATE_DATA;

void METHOD(itf, meth1)(Rtest_optC_idl_interfaces_SimpleInterface* itf, jint a){}
Rtest_optC_idl_interfaces_SimpleInterface* METHOD(itf, meth2)(){ return NULL;}
Rtest_optC_idl_interfaces_InterfaceReferenceInSamePackage* METHOD(itf, meth3)(Rtest_optC_idl_interfaces_SimpleInterface* itf){ return NULL;}
