/**
 * Cecilia, an implementation of the Fractal component model in C.
 * Copyright (C) 2006 STMicroelectronics
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

// include generated header file from IDL fractal.api.LifeCycleController
#include "fractal/api/LifeCycleController.idl.h"

DECLARE_DATA {};

#include <cecilia.h>

// Declare the global init functions.
void __cecilia_global_constructor__(void);
void __cecilia_global_destructor__(void);

int METHOD(boot, run) (void * _this) {
  void* lcc;
  void* runItf;
  jint err, testResult;

  // Call the global constructor function.
  __cecilia_global_constructor__();

  // get the "lifecycle-controller" interface of the test component.
  err = CALL(REQUIRED.test, getFcInterface, "lifecycle-controller", &lcc);
  if (err == fractal_api_ErrorConst_NO_SUCH_INTERFACE) {
    lcc = NULL;
  } else if (err != fractal_api_ErrorConst_OK) {
    return err;
  }

  // start the test component.
  if(lcc != NULL) {
    err = CALL((Rfractal_api_LifeCycleController*)lcc, startFc);
    if (err != fractal_api_ErrorConst_OK) return err;
  }

  // get the "r" interface of the test component.
  err = CALL(REQUIRED.test, getFcInterface, "r", &runItf);
  if (err != fractal_api_ErrorConst_OK) return err;

  // call the "r" interface
  testResult = CALL((Rtest_thinkMC_api_Run *)runItf, run);

  // stop the test component.
  if(lcc != NULL) {
    err = CALL((Rfractal_api_LifeCycleController*)lcc, stopFc);
    if (err != fractal_api_ErrorConst_OK) return err;
  }

  // Call the global destructor function.
  __cecilia_global_destructor__();

  return testResult;
}
