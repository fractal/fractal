
/** Declare component internal data. */
typedef struct {
  jint          x, y;           /* Cursor position */
} PRIVATE_DATA;

extern int printf(const char * format, ...);

/**
 * Implementation of the 'putxycs' method of the interface 'console' of type 
 * 'video.api.Console'
 */
void METHOD(console,putxycs)(jint x, jint y, const char *str) {
  printf("{%2d,%2d} %s\n", x, y, str);
}

/**
 * Implementation of the 'scrollup' method of the interface 'console' of type 
 * 'video.api.Console'
 */
void METHOD(console,scrollup)() {
  //nop;
}

/**
 * Implementation of the 'putc' method of the interface 'console' of type 
 * 'video.api.Console'
 */
void METHOD(console,putc)(jchar c) {
  char ch[] = {0, 0};
  switch(c) {
  case '\b':
    if (DATA.x > 0)
      DATA.x--;
    break;
  case '\t':
  {
    int space = 8 - (DATA.x & 7);
    while(space--) {
      SERVER(console, putxycs) (DATA.x++, DATA.y, " ");
    }
  }; break;
  case '\n':
    DATA.x = 0;
    DATA.y++;
    break;
  case '\r':
    DATA.x = 0;
    break;
  default:
    ch[0] = c;
    SERVER(console, putxycs) (DATA.x++, DATA.y, ch);
  }
  // Next line if need
  if (DATA.x >= ATTR(cols)) {
	DATA.x = 0;
	DATA.y++;
  }
  // Scroll screen if need
  if(DATA.y >= ATTR(rows)) {
    SERVER(console, scrollup) ();
	DATA.y--;
  }
}

/**
 * Implementation of the 'putcs' method of the interface 'console' of type 
 * 'video.api.Console'
 */
void METHOD(console, putcs)(const char *str) {
  while(*str)
    SERVER(console, putc) (*str++);
}

/**
 * Implementation of the 'cols' method of the interface 'console' of type 
 * 'video.api.Console'
 */
jint METHOD(console, cols)() {
  return ATTR(cols);
}

/**
 * Implementation of the 'rows' method of the interface 'console' of type 
 * 'video.api.Console'
 */
jint METHOD(console,rows)() {
  return ATTR(rows);
}
