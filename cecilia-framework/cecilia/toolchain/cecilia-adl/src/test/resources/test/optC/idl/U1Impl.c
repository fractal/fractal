
typedef struct {
  // empty
} PRIVATE_DATA;

#include "cecilia_opt.h"

jint METHOD (r, run) () {
	Rtest_optC_idl_unions_U1 U1;
	U1.a = (intptr_t)0x1;
    
	uintptr_t resultu = SERVER(u, useUnion) (U1);
	if(resultu != (uintptr_t)0x2) {
		return 1;  // error
	}
	
	Rtest_optC_idl_unions_U1 result = SERVER(u, returnUnion) ();
	if(result.a != (intptr_t)0x3) {
		return 2;  // error
	}
	
	return 0;
}

uintptr_t METHOD(u, useUnion) (Rtest_optC_idl_unions_U1 u) {
	u.a++;
	return u.b;
}

Rtest_optC_idl_unions_U1 METHOD(u, returnUnion) () {
	Rtest_optC_idl_unions_U1 U1;
	U1.b = (uintptr_t)0x3;
	return U1;
}

void METHOD(u, useSelf) (Rtest_optC_idl_unions_UsingUnion* i) {
	// Do nothing, just check that the prototype is OK.
}

