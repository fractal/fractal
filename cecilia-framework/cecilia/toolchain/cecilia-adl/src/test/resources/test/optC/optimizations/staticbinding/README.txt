Test files in this package are meant to verify eventual optimizations in the case of static bindings, which means
binding in which the server interface reference is "fixed". In this case the component type owning the client interface
of the binding, can have client method calls as direct function calls to the server interface functions, instead
of passing through the VFT.