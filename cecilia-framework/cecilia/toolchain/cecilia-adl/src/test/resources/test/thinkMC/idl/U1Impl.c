
DECLARE_DATA {
	
};

#include "cecilia.h"

jint METHOD (r, run) (void* _this) {
	Rtest_thinkMC_idl_unions_U1 U1;
	U1.a = (intptr_t)0x1;
    
	uintptr_t resultu = CALLMINE(u, useUnion, U1);
	if(resultu != (uintptr_t)0x2) {
		return 1;  // error
	}
	
	Rtest_thinkMC_idl_unions_U1 result = CALLMINE(u, returnUnion);
	if(result.a != (intptr_t)0x3) {
		return 2;  // error
	}
	
	return 0;
}

uintptr_t METHOD(u, useUnion) (void* _this, Rtest_thinkMC_idl_unions_U1 u) {
	u.a++;
	return u.b;
}

Rtest_thinkMC_idl_unions_U1 METHOD(u, returnUnion) (void* _this) {
	Rtest_thinkMC_idl_unions_U1 U1;
	U1.b = (uintptr_t)0x3;
	return U1;
}

void METHOD(u, useSelf) (void* _this, Rtest_thinkMC_idl_unions_UsingUnion* i) {
	// Do nothing, just check that the prototype is OK.
}

