
/** all the includes *except* cecilia_opt.h */
#include <stdio.h>

/** Declare component internal data, empty here. */
typedef struct {
  // empty
} PRIVATE_DATA;

// -----------------------------------------------------------------------------
// Implementation of the 'main' boot interface.
// -----------------------------------------------------------------------------

int METHOD(r, run) (){

  printf("calling m1 \n");
  CLIENT(out, m1) (0, 0);
  
  printf("calling m2, will segfault \n");
  CLIENT(out, m2) (0);
  
  //unreachable
  printf("calling m3... \n");
  CLIENT(out, m3) (0, 0);
 
  
  return 0;
}
