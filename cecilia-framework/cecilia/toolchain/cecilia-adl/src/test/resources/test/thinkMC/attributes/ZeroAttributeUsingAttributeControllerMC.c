DECLARE_DATA{};

#include <cecilia.h>

#include <stdio.h>

/**
 * Assert setting the 'a' attribute to 0, using the 
 * AttributeController {get|set}FcInterface methods.
 */
jint METHOD(r, run)(void* _this) {
	
  Rfractal_api_Component * ci = GET_MY_OWNER;
  void * ac = NULL;
  
  int err = CALL(ci, getFcInterface, "attribute-controller", &ac);
  
  if(ac == NULL) {
	  printf("AttributeController is NULL");
	  return 1;
  }
  
  if(err != fractal_api_ErrorConst_OK) {
	  // error getting the attribute-controller interface
	  printf("Error getting attribute-controller\n");
	  return 2;
  }else{
	  
	  int i = 0;
	  err = CALL((Rfractal_api_AttributeController *)ac, getFcAttribute, "a", (void*)&i);
	  
	  // check the initial value of the attribute
	  if(i != 3){
		  return 3;
	  }
	  
	  err = CALL((Rfractal_api_AttributeController *)ac, setFcAttribute, "a", 0);
	  
	  // assert that the setFcAttribute call does not lead to errors...
	  if(err != fractal_api_ErrorConst_OK) {
		  return 4;
	  }
	  
	  // ...and that the value is indeed set
	  err = CALL((Rfractal_api_AttributeController *)ac, getFcAttribute, "a", (void*)&i);
	  if(i != 0){
		  return 5;
	  }
	  
	  return fractal_api_ErrorConst_OK;
  }
}
