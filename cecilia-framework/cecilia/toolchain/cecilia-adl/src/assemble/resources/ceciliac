#!/bin/sh

# ------------------------------------------------------------------------------
# Copyright (C) 2008 STMicroelectronics
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Contact: fractal@objectweb.org
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# Cecilia Compiler batch script ${project.version}
#
# Required ENV vars:
# ------------------
#   JAVA_HOME - location of a JDK home dir
#
# Optional ENV vars
# -----------------
#   CECILIA_HOME - location of cecilia's installed home dir
#   CECILIA_OPTS - parameters passed to the Java VM running the cecilia compiler
#     e.g. to specify logging levels, use
#       set CECILIA_OPTS=-Ddefault.console.level=FINE -Ddefault.file.level=FINER
#   See documentation for more detail on logging system.


# OS specific support.  $var _must_ be set to either true or false.
cygwin=false;
darwin=false;
mingw=false
case "`uname`" in
  CYGWIN*) cygwin=true ;;
  MINGW*) mingw=true;;
  Darwin*) darwin=true 
           if [ -z "$JAVA_VERSION" ] ; then
             JAVA_VERSION="CurrentJDK"
           else
             echo "Using Java version: $JAVA_VERSION"
           fi
           if [ -z "$JAVA_HOME" ] ; then
             JAVA_HOME=/System/Library/Frameworks/JavaVM.framework/Versions/${JAVA_VERSION}/Home
           fi
           ;;
esac
if [ -z "$JAVA_HOME" ] ; then
  if [ -r /etc/gentoo-release ] ; then
    JAVA_HOME=`java-config --jre-home`
  fi
fi
if [ -z "$CECILIA_HOME" ] ; then
  ## resolve links - $0 may be a link to cecilia's home
  PRG="$0"

  # need this for relative symlinks
  while [ -h "$PRG" ] ; do
    ls=`ls -ld "$PRG"`
    link=`expr "$ls" : '.*-> \(.*\)$'`
    if expr "$link" : '/.*' > /dev/null; then
      PRG="$link"
    else
      PRG="`dirname "$PRG"`/$link"
    fi
  done

  saveddir=`pwd`

  CECILIA_HOME=`dirname "$PRG"`/..

  # make it fully qualified
  CECILIA_HOME=`cd "$CECILIA_HOME" && pwd`

  cd "$saveddir"
  # echo Using cecilia at $CECILIA_HOME
fi

CECILIA_SOURCES_PATH="$CECILIA_HOME"/cecilia-sources
CECILIA_LIB="$CECILIA_HOME"/lib

# For Cygwin, ensure paths are in UNIX format before anything is touched
if $cygwin ; then
  [ -n "$CECILIA_HOME" ] &&
    CECILIA_HOME=`cygpath --unix "$CECILIA_HOME"`
  [ -n "$JAVA_HOME" ] &&
    JAVA_HOME=`cygpath --unix "$JAVA_HOME"`
  [ -n "$CECILIA_LIB" ] &&
    CECILIA_LIB=`cygpath --path --unix "$CECILIA_LIB"`
  [ -n "$CECILIA_SOURCES_PATH" ] &&
    CECILIA_SOURCES_PATH=`cygpath --path --unix "$CECILIA_SOURCES_PATH"`
fi

# For Migwn, ensure paths are in UNIX format before anything is touched
if $mingw ; then
  [ -n "$CECILIA_HOME" ] &&
    CECILIA_HOME="`(cd "$CECILIA_HOME"; pwd)`"
  [ -n "$JAVA_HOME" ] &&
    JAVA_HOME="`(cd "$JAVA_HOME"; pwd)`"
  # TODO classpath?
fi


## Launcher class name
LAUNCHER=org.objectweb.fractal.cecilia.adl.Launcher

## Preparation of the java command to be executed
if [ -z "$JAVACMD" ] ; then
  if [ -n "$JAVA_HOME"  ] ; then
    if [ -x "$JAVA_HOME/jre/sh/java" ] ; then
      # IBM's JDK on AIX uses strange locations for the executables
      JAVACMD="$JAVA_HOME/jre/sh/java"
    else
      JAVACMD="$JAVA_HOME/bin/java"
    fi
  else
    JAVACMD=java
  fi
fi

## Print error if the java command is not ready
if [ ! -x "$JAVACMD" ] ; then
  echo "Error: JAVA_HOME is not defined correctly."
  echo "  We cannot execute $JAVACMD"
  exit 1
fi
if [ -z "$JAVA_HOME" ] ; then
  echo "Warning: JAVA_HOME environment variable is not set."
fi

# For Cygwin, switch paths to Windows format before running java
if $cygwin; then
  [ -n "$CECILIA_HOME" ] &&
    CECILIA_HOME=`cygpath --path --windows "$CECILIA_HOME"`
  [ -n "$JAVA_HOME" ] &&
    JAVA_HOME=`cygpath --path --windows "$JAVA_HOME"`
  [ -n "$HOME" ] &&
    HOME=`cygpath --path --windows "$HOME"`
  [ -n "$CECILIA_SOURCES_PATH" ] &&
    CECILIA_SOURCES_PATH=`cygpath --path --windows "$CECILIA_SOURCES_PATH"`
  [ -n "$CECILIA_LIB" ] &&
    CECILIA_LIB=`cygpath --path --windows "$CECILIA_LIB"`
fi




JAR_SET=`ls $CECILIA_LIB`
for i in $JAR_SET; do
  if [ -z "$CECILIA_CLASSPATH" ] ; then
    if $cygwin; then
      CECILIA_CLASSPATH="${CECILIA_LIB}\\${i}"
    else
      CECILIA_CLASSPATH="${CECILIA_LIB}/${i}"
    fi
  else
    if $cygwin; then
      CECILIA_CLASSPATH="${CECILIA_CLASSPATH};${CECILIA_LIB}\\${i}"
    else
      CECILIA_CLASSPATH="${CECILIA_CLASSPATH}:${CECILIA_LIB}/${i}"
    fi
  fi
done


if [ -n "$CECILIA_VERBOSITY_LEVEL" ] ; then
  echo "Warning: CECILIA_VERBOSITY_LEVEL environment variable is deprecated."
  echo "Use CECILIA_OPTS instead."
  CECILIA_OPTS="$CECILIA_OPTS -Ddefault.console.level=${CECILIA_VERBOSITY_LEVEL}"
fi

##########################################
#echo $CECILIA_SOURCES_PATH
#echo $CECILIA_LIB
#echo $CECILIA_CLASSPATH
###########################################

exec "$JAVACMD" \
  -classpath "${CECILIA_CLASSPATH}" \
  ${CECILIA_OPTS} \
  -Dcecilia.launcher.name=ceciliac \
  ${LAUNCHER} \
  -src-path="${CECILIA_SOURCES_PATH}" \
  "$@"


