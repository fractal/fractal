Cecilia compiler ${project.version}

Installing Cecilia
==================

  The following instructions show how to install Cecilia:

  1) Unpack the archive where you would like to store the binaries, eg:

# tar zxvf cecilia-framework-${project.version}-bin.tar.gz

  2) A directory called "cecilia-framework-${project.version}" will be created.

  3) Add the bin directory to your PATH, eg:
  
# export PATH=/usr/local/cecilia-framework-${project.version}/bin:$PATH

  4) Make sure JAVA_HOME is set to the location of your JDK

  5) Run "ceciliac" to verify that it is correctly installed.

  For more information, please see http://fractal.objectweb.org/cecilia-site/current

Running the CeciliaC compiler
=============================

  The ceciliac compiler compiles a given architecture definition file.
  It is used with the following command-line arguments:

# ceciliac [OPTIONS] (<definition>[:<output-name>])+
  
  where <definition> is the name of the component to be compiled, 
  and <execname> is the name of the output file to be created.

  Available options are (from command line, options must be prefix with '-') :
    src-path (S)                : the search path of ADL,IDL and implementation 
                                  files
    out-path (out-dir, o)       : the path where generated files will be put
    compiler-adl                : Specify the name of the ADL of the compiler 
                                  itself
    compiler-command (cc)       : the command of the C compiler (default is 
                                  'gcc')
    defs (D)                    : the -D compiler directives
    c-flags (cf)                : the c-flags compiler directives
    inc-path (I)                : the -I compiler directives
    linker-command (ld)         : the command of the linker (default is 'gcc')
    ld-flags (lf)               : the ld-flags compiler directives
    ld-path (-L)                : the -L compiler directives
    executable-format           : the binary format associated to "executable" 
                                  output format (default is 'elf')
    archive-command (ar)        : the command of the archive tool, (default is 
                                  'ar')
    linker-script (T)           : linker script
    executable-name (exec-name) : the name of the target executable file (can be
                                  used with only one ADL to be compiled.)
    jobs (j)                    : The number of concurrent compilation jobs

Setting the verbosity level of the ceciliac compiler
====================================================

  Use the CECILIA_OPTS environment variable to specify verbosity level. 

  For instance 

# set CECILIA_OPTS=-Ddefault.console.level=FINE -Ddefault.file.level=FINER

  specifies the FINE level for console messages and FINER for messages dumped in 
  log file.

Addition of extension modules to the ceciliac compiler
========================================================

  Set the CECILIA_CLASSPATH environment variable to include the .jar files or 
  the classes of the extension module(s).

# export CECILIA_CLASSPATH="MyModule1.jar:MyModule2.jar"
 
