/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace, Lionel Debroux
 */

package org.objectweb.fractal.cecilia.adl.idl.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.InputStream;

import org.junit.Test;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;
import org.objectweb.fractal.cecilia.adl.idl.CeciliaIDLConstants;
import org.objectweb.fractal.cecilia.adl.idl.ast.ComplexType;
import org.objectweb.fractal.cecilia.adl.idl.ast.EnumDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.EnumNameValuePair;
import org.objectweb.fractal.cecilia.adl.idl.ast.Field;
import org.objectweb.fractal.cecilia.adl.idl.ast.FieldContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Import;
import org.objectweb.fractal.cecilia.adl.idl.ast.ImportContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.InterfaceDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Method;
import org.objectweb.fractal.cecilia.adl.idl.ast.MethodContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.Package;
import org.objectweb.fractal.cecilia.adl.idl.ast.PackageContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.Parameter;
import org.objectweb.fractal.cecilia.adl.idl.ast.ParameterContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.PointerOf;
import org.objectweb.fractal.cecilia.adl.idl.ast.PrimitiveType;
import org.objectweb.fractal.cecilia.adl.idl.ast.RecordDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Type;
import org.objectweb.fractal.cecilia.adl.idl.ast.TypeContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.UnionDefinition;
import org.objectweb.fractal.cecilia.adl.idl.util.Util;

/**
 * This test case must be run as part of a prior execution of
 * <code>javacc</code> which will generate the {@link IDLParser} class.
 * 
 * @author Alessio Pace
 * @contributor Lionel Debroux
 */
public class IDLParserTest {
  /**
   * 
   */
  public static final String   I1_USING_E1_IDL                = "/I1UsingE1.idl";

  private IDLParser            idlParser;

  public static final String   INTERFACE_WITH_PACKAGE_PATH    = "/org/objectweb/fractal/cecilia/adl/idl/parser/Foo.idl";
  public static final String   RECORD_WITH_PACKAGE_PATH       = "/org/objectweb/fractal/cecilia/adl/idl/parser/Bar.idl";
  public static final String   C99ITF_WITH_PACKAGE_PATH       = "/org/objectweb/fractal/cecilia/adl/idl/parser/Baz.idl";
  public static final String   INTERFACE_WITHOUT_PACKAGE_PATH = "/I1.idl";
  public static final String   RECORD_WITHOUT_PACKAGE_PATH    = "/R1.idl";
  public static final String   C99REC_WITHOUT_PACKAGE_PATH    = "/R2.idl";
  public static final String   E1                             = "/E1.idl";
  public static final String   E2                             = "/E2.idl";
  public static final String   U1                             = "/U1.idl";

  private final XMLNodeFactory nodeFactoryItf                 = new XMLNodeFactoryImpl();

  protected IDLDefinition parseIDLDefinition(final String path)
      throws Exception {
    final InputStream is = IDLParserTest.class.getResourceAsStream(path);

    if (is == null) {
      throw new RuntimeException("Unable to find the IDL file " + path);
    }

    // create the IDLParser
    this.idlParser = new IDLParser(is);
    this.idlParser.init(this.nodeFactoryItf,
        CeciliaIDLConstants.CECILIA_IDL_DTD, new File(path).getPath());

    // get the IDLDefinition node result which will be used in all the tests
    final IDLDefinition definition = this.idlParser.getIDLNode();

    return definition;
  }

  @Test
  public void testImports() throws Exception {
    final IDLDefinition definition = parseIDLDefinition(INTERFACE_WITH_PACKAGE_PATH);

    assertTrue("Assert it is an ImportContainer",
        definition instanceof ImportContainer);

    final Import[] imports = ((ImportContainer) definition).getImports();
    assertNotNull("Assert Import[] not null", imports);
    assertEquals("Assert number of imports", 0, imports.length);
  }

  @Test
  public void testInterfaceWithNotNullPackage() throws Exception {
    final IDLDefinition definition = parseIDLDefinition(INTERFACE_WITH_PACKAGE_PATH);

    assertTrue("Assert it is a PackageContainer",
        definition instanceof PackageContainer);

    final Package packageNode = ((PackageContainer) definition).getPackage();
    assertNotNull("Assert Package node not null", packageNode);
    // because the .idl file is in the same package of this test class
    assertEquals("Assert package value",
        this.getClass().getPackage().getName(), packageNode.getName());
  }

  @Test
  public void testInterfaceWithNullPackage() throws Exception {
    final IDLDefinition definition = parseIDLDefinition(INTERFACE_WITHOUT_PACKAGE_PATH);

    assertTrue("Assert it is a PackageContainer",
        definition instanceof PackageContainer);

    final Package packageNode = ((PackageContainer) definition).getPackage();
    // because the .idl file has no package declaration
    assertNull("Assert Package node not null", packageNode);
  }

  @Test
  public void testRecordWithNotNullPackage() throws Exception {
    final IDLDefinition definition = parseIDLDefinition(RECORD_WITH_PACKAGE_PATH);

    assertTrue("Assert it is a PackageContainer",
        definition instanceof PackageContainer);
    assertTrue(definition instanceof RecordDefinition);

    final Package packageNode = ((PackageContainer) definition).getPackage();
    assertNotNull("Assert Package node not null", packageNode);
    // because the .idl file is in the same package of this test class
    assertEquals("Assert package value",
        this.getClass().getPackage().getName(), packageNode.getName());
  }

  @Test
  public void testRecordWithNullPackage() throws Exception {
    final IDLDefinition definition = parseIDLDefinition(RECORD_WITHOUT_PACKAGE_PATH);

    assertTrue("Assert it is a PackageContainer",
        definition instanceof PackageContainer);
    assertTrue(definition instanceof RecordDefinition);

    final Package packageNode = ((PackageContainer) definition).getPackage();
    // because the .idl file has no package declaration
    assertNull("Assert Package node not null", packageNode);
  }

  @Test
  public void testC99ItfWithNotNullPackage() throws Exception {
    final IDLDefinition definition = parseIDLDefinition(C99ITF_WITH_PACKAGE_PATH);

    assertTrue("Assert it is a PackageContainer",
        definition instanceof PackageContainer);

    final Package packageNode = ((PackageContainer) definition).getPackage();
    assertNotNull("Assert Package node not null", packageNode);
    // because the .idl file is in the same package of this test class
    assertEquals("Assert package value",
        this.getClass().getPackage().getName(), packageNode.getName());
  }

  @Test
  public void testC99RecWithNullPackage() throws Exception {
    final IDLDefinition definition = parseIDLDefinition(C99REC_WITHOUT_PACKAGE_PATH);

    assertTrue("Assert it is a PackageContainer",
        definition instanceof PackageContainer);
    assertTrue(definition instanceof RecordDefinition);
    assertTrue(definition instanceof RecordDefinition);

    final Package packageNode = ((PackageContainer) definition).getPackage();
    // because the .idl file has no package declaration
    assertNull("Assert Package node not null", packageNode);
  }

  @Test
  public void testC99ItfMethods() throws Exception {
    final IDLDefinition definition = parseIDLDefinition(C99ITF_WITH_PACKAGE_PATH);

    assertTrue("Assert it is a PackageContainer",
        definition instanceof PackageContainer);

    final Package packageNode = ((PackageContainer) definition).getPackage();
    assertNotNull("Assert Package node not null", packageNode);
    // because the .idl file is in the same package of this test class
    assertEquals("Assert package value",
        this.getClass().getPackage().getName(), packageNode.getName());

    assertTrue("Assert it is a MethodContainer",
        definition instanceof MethodContainer);
    final Method[] methods = ((MethodContainer) definition).getMethods();
    assertNotNull("Assert methods not null", methods);

    testFooMethod(methods[0]);

    testBarMethod(methods[1]);

    testIntleast(methods[2]);

    testUintleast(methods[3]);

    testIntfast(methods[4]);

    testUintfast(methods[5]);

    testOthers(methods[6]);
  }

  /**
   * Test the "int8_t foo (int16_t arg1, int32_t * arg2, const int64_t arg3);"
   * method.
   */
  private void testFooMethod(final Method method) {
    assertEquals("foo", method.getName());

    assertEquals("Assert it has a variable number of parameters", "false",
        method.getHasVarParams());

    /* The "int8_t" return type */
    final Type returnType = ((TypeContainer) method).getPrimitiveType();
    assertTrue(returnType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INT8_T.name(),
        ((PrimitiveType) returnType).getName());

    final Parameter[] parameters = ((ParameterContainer) method)
        .getParameters();
    assertNotNull("Assert parameters not null", parameters);

    /* The "int16_t arg1" parameter */
    Parameter parameter = parameters[0];
    assertEquals("arg1", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    Type argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INT16_T.name(),
        ((PrimitiveType) argType).getName());

    /* The "int32_t * arg2" parameter */
    parameter = parameters[1];
    assertEquals("arg2", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PointerOf);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INT32_T.name(),
        ((TypeContainer) argType).getPrimitiveType().getName());

    /* The "const int64_t arg3" parameter */
    parameter = parameters[2];
    assertEquals("arg3", parameter.getName());
    assertEquals("Assert 'const' qualifier is set", Field.CONST, parameter
        .getQualifier());
    argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INT64_T.name(),
        ((PrimitiveType) argType).getName());
  }

  /**
   * Test the "uint8_t bar (out uint16_t arg1, uint32_t * arg2, uint64_t arg3);"
   * method.
   */
  private void testBarMethod(final Method method) {
    assertEquals("bar", method.getName());

    assertEquals("Assert it has a variable number of parameters", "false",
        method.getHasVarParams());

    /* The "uint8_t" return type */
    final Type returnType = ((TypeContainer) method).getPrimitiveType();
    assertTrue(returnType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.UINT8_T.name(),
        ((PrimitiveType) returnType).getName());

    final Parameter[] parameters = ((ParameterContainer) method)
        .getParameters();
    assertNotNull("Assert parameters not null", parameters);

    /* The "out uint16_t arg1" parameter */
    Parameter parameter = parameters[0];
    assertEquals("arg1", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "out", parameter
        .getQualifier());
    Type argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.UINT16_T.name(),
        ((PrimitiveType) argType).getName());

    /* The "uint32_t * arg2" parameter */
    parameter = parameters[1];
    assertEquals("arg2", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PointerOf);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.UINT32_T.name(),
        ((TypeContainer) argType).getPrimitiveType().getName());

    /* The "uint64_t arg3" parameter */
    parameter = parameters[2];
    assertEquals("arg3", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.UINT64_T.name(),
        ((PrimitiveType) argType).getName());
  }

  /**
   * Test the "int_least8_t intleast(int_least16_t arg1, int_least32_t * arg2,
   * int_least64_t arg3);" method.
   */
  private void testIntleast(final Method method) {
    assertEquals("intleast", method.getName());

    assertEquals("Assert it has a variable number of parameters", "false",
        method.getHasVarParams());

    /* The "int_least8_t" return type */
    final Type returnType = ((TypeContainer) method).getPrimitiveType();
    assertTrue(returnType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INT_LEAST8_T.name(),
        ((PrimitiveType) returnType).getName());

    final Parameter[] parameters = ((ParameterContainer) method)
        .getParameters();
    assertNotNull("Assert parameters not null", parameters);

    /* The "int_least16_t arg1" parameter */
    Parameter parameter = parameters[0];
    assertEquals("arg1", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    Type argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INT_LEAST16_T.name(),
        ((PrimitiveType) argType).getName());

    /* The "int_least32_t * arg2" parameter */
    parameter = parameters[1];
    assertEquals("arg2", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PointerOf);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INT_LEAST32_T.name(),
        ((TypeContainer) argType).getPrimitiveType().getName());

    /* The "int_least64_t arg3" parameter */
    parameter = parameters[2];
    assertEquals("arg3", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INT_LEAST64_T.name(),
        ((PrimitiveType) argType).getName());
  }

  /**
   * Test the "uint_least8_t uintleast(uint_least16_t arg1, uint_least32_t *
   * arg2, uint_least64_t arg3);" method.
   */
  private void testUintleast(final Method method) {
    assertEquals("uintleast", method.getName());

    assertEquals("Assert it has a variable number of parameters", "false",
        method.getHasVarParams());

    /* The "uint_least8_t" return type */
    final Type returnType = ((TypeContainer) method).getPrimitiveType();
    assertTrue(returnType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.UINT_LEAST8_T.name(),
        ((PrimitiveType) returnType).getName());

    final Parameter[] parameters = ((ParameterContainer) method)
        .getParameters();
    assertNotNull("Assert parameters not null", parameters);

    /* The "uint_least16_t arg1" parameter */
    Parameter parameter = parameters[0];
    assertEquals("arg1", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    Type argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.UINT_LEAST16_T.name(),
        ((PrimitiveType) argType).getName());

    /* The "uint_least32_t * arg2" parameter */
    parameter = parameters[1];
    assertEquals("arg2", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PointerOf);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.UINT_LEAST32_T.name(),
        ((TypeContainer) argType).getPrimitiveType().getName());

    /* The "uint_least64_t arg3" parameter */
    parameter = parameters[2];
    assertEquals("arg3", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.UINT_LEAST64_T.name(),
        ((PrimitiveType) argType).getName());
  }

  /**
   * Test the "int_fast8_t intfast(int_fast16_t arg1, int_fast32_t * arg2,
   * int_fast64_t arg3);" method.
   */
  private void testIntfast(final Method method) {
    assertEquals("intfast", method.getName());

    assertEquals("Assert it has a variable number of parameters", "false",
        method.getHasVarParams());

    /* The "int_fast8_t" return type */
    final Type returnType = ((TypeContainer) method).getPrimitiveType();
    assertTrue(returnType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INT_FAST8_T.name(),
        ((PrimitiveType) returnType).getName());

    final Parameter[] parameters = ((ParameterContainer) method)
        .getParameters();
    assertNotNull("Assert parameters not null", parameters);

    /* The "int_fast16_t arg1" parameter */
    Parameter parameter = parameters[0];
    assertEquals("arg1", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    Type argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INT_FAST16_T.name(),
        ((PrimitiveType) argType).getName());

    /* The "int_fast32_t * arg2" parameter */
    parameter = parameters[1];
    assertEquals("arg2", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PointerOf);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INT_FAST32_T.name(),
        ((TypeContainer) argType).getPrimitiveType().getName());

    /* The "int_fast64_t arg3" parameter */
    parameter = parameters[2];
    assertEquals("arg3", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INT_FAST64_T.name(),
        ((PrimitiveType) argType).getName());
  }

  /**
   * Test the "uint_fast8_t uintfast(uint_fast16_t arg1, uint_fast32_t * arg2,
   * uint_fast64_t arg3);" method.
   */
  private void testUintfast(final Method method) {
    assertEquals("uintfast", method.getName());

    assertEquals("Assert it has a variable number of parameters", "false",
        method.getHasVarParams());

    /* The "uint_fast8_t" return type */
    final Type returnType = ((TypeContainer) method).getPrimitiveType();
    assertTrue(returnType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.UINT_FAST8_T.name(),
        ((PrimitiveType) returnType).getName());

    final Parameter[] parameters = ((ParameterContainer) method)
        .getParameters();
    assertNotNull("Assert parameters not null", parameters);

    /* The "uint_fast16_t arg1" parameter */
    Parameter parameter = parameters[0];
    assertEquals("arg1", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    Type argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.UINT_FAST16_T.name(),
        ((PrimitiveType) argType).getName());

    /* The "uint_fast32_t * arg2" parameter */
    parameter = parameters[1];
    assertEquals("arg2", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PointerOf);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.UINT_FAST32_T.name(),
        ((TypeContainer) argType).getPrimitiveType().getName());

    /* The "uint_fast64_t arg3" parameter */
    parameter = parameters[2];
    assertEquals("arg3", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.UINT_FAST64_T.name(),
        ((PrimitiveType) argType).getName());
  }

  /**
   * Test the "void others(intptr_t arg1, uintptr_t arg2, intmax_t arg3,
   * uintmax_t arg4, size_t arg5, ptrdiff_t arg6, wchar_t arg7);" method.
   */
  private void testOthers(final Method method) {
    assertEquals("others", method.getName());

    assertEquals("Assert it has a variable number of parameters", "false",
        method.getHasVarParams());

    /* The "void" return type */
    final Type returnType = ((TypeContainer) method).getPrimitiveType();
    assertTrue(returnType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.VOID.name(),
        ((PrimitiveType) returnType).getName());

    final Parameter[] parameters = ((ParameterContainer) method)
        .getParameters();
    assertNotNull("Assert parameters not null", parameters);

    /* The "intptr_t arg1" parameter */
    Parameter parameter = parameters[0];
    assertEquals("arg1", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    Type argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INTPTR_T.name(),
        ((PrimitiveType) argType).getName());

    /* The "uintptr_t arg2" parameter */
    parameter = parameters[1];
    assertEquals("arg2", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.UINTPTR_T.name(),
        ((PrimitiveType) argType).getName());

    /* The "intmax_t arg3" parameter */
    parameter = parameters[2];
    assertEquals("arg3", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INTMAX_T.name(),
        ((PrimitiveType) argType).getName());

    /* The "uintmax_t arg4" parameter */
    parameter = parameters[3];
    assertEquals("arg4", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.UINTMAX_T.name(),
        ((PrimitiveType) argType).getName());

    /* The "size_t arg5" parameter */
    parameter = parameters[4];
    assertEquals("arg5", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.SIZE_T.name(),
        ((PrimitiveType) argType).getName());

    /* The "ptrdiff_t arg6" parameter */
    parameter = parameters[5];
    assertEquals("arg6", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.PTRDIFF_T.name(),
        ((PrimitiveType) argType).getName());

    /* The "wchar_t arg7" parameter */
    parameter = parameters[6];
    assertEquals("arg7", parameter.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "in", parameter
        .getQualifier());
    argType = Util.getContainedType(parameter);
    assertTrue(argType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.WCHAR_T.name(),
        ((PrimitiveType) argType).getName());
  }

  @Test
  public void testRecordField() throws Exception {
    final IDLDefinition definition = parseIDLDefinition(RECORD_WITH_PACKAGE_PATH);

    assertTrue(definition instanceof FieldContainer);
    assertTrue(definition instanceof RecordDefinition);

    final Field[] fields = ((FieldContainer) definition).getFields();
    assertNotNull("Assert fields not null", fields);

    /* the "int x" field */
    final Field xField = fields[0];
    assertEquals("x", xField.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "", xField
        .getQualifier());
    final Type xReturnType = Util.getContainedType(xField);
    assertTrue(xReturnType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INT.name(),
        ((PrimitiveType) xReturnType).getName());

    /* the "const int y" field */
    final Field yField = fields[1];
    assertEquals("y", yField.getName());
    assertEquals("Assert 'const' qualifier is set", Field.CONST, yField
        .getQualifier());
    final Type yReturnType = Util.getContainedType(yField);
    assertTrue(yReturnType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INT.name(),
        ((PrimitiveType) yReturnType).getName());
  }

  @Test
  public void testRecordC99Field() throws Exception {
    final IDLDefinition definition = parseIDLDefinition(C99REC_WITHOUT_PACKAGE_PATH);

    assertTrue(definition instanceof FieldContainer);
    assertTrue(definition instanceof RecordDefinition);

    final Field[] fields = ((FieldContainer) definition).getFields();
    assertNotNull("Assert fields not null", fields);

    /* the "int8_t a" field */
    Field field = fields[0];
    assertEquals("a", field.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "", field
        .getQualifier());
    Type returnType = Util.getContainedType(field);
    assertTrue(returnType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INT8_T.name(),
        ((PrimitiveType) returnType).getName());

    /* the "const int16_t b" field */
    field = fields[1];
    assertEquals("b", field.getName());
    assertEquals("Assert 'const' qualifier is set", Field.CONST, field
        .getQualifier());
    returnType = Util.getContainedType(field);
    assertTrue(returnType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INT16_T.name(),
        ((PrimitiveType) returnType).getName());

    /* the "int32_t * c" field */
    field = fields[2];
    assertEquals("c", field.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "", field
        .getQualifier());
    returnType = Util.getContainedType(field);
    assertTrue(returnType instanceof PointerOf);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INT32_T.name(),
        ((TypeContainer) returnType).getPrimitiveType().getName());

    /* the "int64_t d" field */
    field = fields[3];
    assertEquals("d", field.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "", field
        .getQualifier());
    returnType = Util.getContainedType(field);
    assertTrue(returnType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INT64_T.name(),
        ((PrimitiveType) returnType).getName());

    /* the "uint8_t e" field */
    field = fields[4];
    assertEquals("e", field.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "", field
        .getQualifier());
    returnType = Util.getContainedType(field);
    assertTrue(returnType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.UINT8_T.name(),
        ((PrimitiveType) returnType).getName());

    /* the "uint16_t f" field */
    field = fields[5];
    assertEquals("f", field.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "", field
        .getQualifier());
    returnType = Util.getContainedType(field);
    assertTrue(returnType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.UINT16_T.name(),
        ((PrimitiveType) returnType).getName());

    /* the "uint32_t * g" field */
    field = fields[6];
    assertEquals("g", field.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "", field
        .getQualifier());
    returnType = Util.getContainedType(field);
    assertTrue(returnType instanceof PointerOf);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.UINT32_T.name(),
        ((TypeContainer) returnType).getPrimitiveType().getName());

    /* the "const uint64_t h" field */
    field = fields[7];
    assertEquals("h", field.getName());
    assertEquals("Assert 'const' qualifier is set", Field.CONST, field
        .getQualifier());
    returnType = Util.getContainedType(field);
    assertTrue(returnType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.UINT64_T.name(),
        ((PrimitiveType) returnType).getName());
  }

  @Test
  public void testEnum() throws Exception {
    final IDLDefinition definition = parseIDLDefinition(E1);
    assertNotNull("Assert definition not null", definition);

    assertTrue(definition instanceof EnumDefinition);
    final EnumDefinition enumDefinition = (EnumDefinition) definition;

    final EnumNameValuePair[] pairs = enumDefinition.getEnumNameValuePairs();
    assertEquals("Assert number of enum pairs", 2, pairs.length);

    assertEquals("Assert first member name", "FOO", pairs[0].getName());
    assertEquals("Assert first member value", "1", pairs[0].getValue());
    assertEquals("Assert second member name", "BAR", pairs[1].getName());
    assertEquals("Assert second member value", "\"2\"", pairs[1].getValue());
  }

  @Test
  public void testInterfaceUsingEnum() throws Exception {
    final IDLDefinition definition = parseIDLDefinition(I1_USING_E1_IDL);
    assertNotNull("Assert definition not null", definition);

    final InterfaceDefinition interfaceDefinition = (InterfaceDefinition) definition;

    final Method[] methods = ((MethodContainer) interfaceDefinition)
        .getMethods();

    // assertions for enum usage in first method
    final Method useEnumMethod = methods[0];
    final Parameter useEnumParameter = ((ParameterContainer) useEnumMethod)
        .getParameters()[0];
    assertEquals("Assert parameter name", "e", useEnumParameter.getName());
    final ComplexType useEnumContainedType = useEnumParameter.getComplexType();
    assertNotNull("Assert enum parameter is a complex type and not null",
        useEnumContainedType);
    assertEquals("Assert enum parameter type name", "E1", useEnumContainedType
        .getName());

    // assertions for enum usage in second method
    final Method returnEnumMethod = methods[1];
    final ComplexType returnEnumType = returnEnumMethod.getComplexType();
    assertNotNull("Assert return type is an enum and not null", returnEnumType);
    assertEquals("Assert enum return type name", "E1", returnEnumType.getName());

  }

  @Test
  public void testEnumWithMemberFieldsNotDeclaringValue() throws Exception {
    final IDLDefinition definition = parseIDLDefinition(E2);
    assertNotNull("Assert definition not null", definition);
  }

  @Test
  public void testSimpleUnion() throws Exception {
    final IDLDefinition definition = parseIDLDefinition(U1);
    assertNotNull("Assert definition not null", definition);

    assertTrue(definition instanceof FieldContainer);
    assertTrue(definition instanceof UnionDefinition);

    final Field[] fields = ((FieldContainer) definition).getFields();
    assertNotNull("Assert fields not null", fields);

    /* the "intptr_t a" field */
    Field field = fields[0];
    assertEquals("a", field.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "", field
        .getQualifier());
    Type returnType = Util.getContainedType(field);
    assertTrue(returnType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.INTPTR_T.name(),
        ((PrimitiveType) returnType).getName());

    /* the "uint64_t b" field */
    field = fields[1];
    assertEquals("b", field.getName());
    assertEquals("Assert 'const' qualifier is NOT set", "", field
        .getQualifier());
    returnType = Util.getContainedType(field);
    assertTrue(returnType instanceof PrimitiveType);
    assertEquals(PrimitiveType.PrimitiveTypeEnum.UINT64_T.name(),
        ((PrimitiveType) returnType).getName());
  }
}
