/**
 * 
 */
package org.objectweb.fractal.cecilia.adl.idl.ast;

import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;

/**
 * @author Alessio Pace
 */
public abstract class AbstractASTNodesTest {

  protected XMLNodeFactory nodeFactory     = new XMLNodeFactoryImpl();

}
