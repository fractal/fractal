/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.idl.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;
import org.objectweb.fractal.cecilia.adl.idl.CeciliaIDLConstants;
import org.objectweb.fractal.cecilia.adl.idl.ast.Method;
import org.objectweb.fractal.cecilia.adl.idl.ast.MethodContainer;

/**
 * Test the methods of the {@link Util} utility class.
 * 
 * @author Alessio Pace
 */
public class UtilTest {

  private XMLNodeFactory     nodeFactory;

  @Before
  public void setUp() throws Exception {

    this.nodeFactory = new XMLNodeFactoryImpl();
    assertNotNull(this.nodeFactory);
  }

  /**
   * This method creates {@link Method}s and {@link MethodContainer} nodes
   * using directly a node factory.
   * 
   * @throws Exception
   */
  @Test
  public void testGetMethodByName() throws Exception {

    final String methodElementName = CeciliaIDLConstants.METHOD_AST_NODE_NAME;
    final Method foo = (Method) this.nodeFactory.newXMLNode(
        CeciliaIDLConstants.CECILIA_IDL_DTD, methodElementName);
    final String fooMethodName = "foo";
    foo.setName(fooMethodName);
    final Method bar = (Method) this.nodeFactory.newXMLNode(
        CeciliaIDLConstants.CECILIA_IDL_DTD, methodElementName);
    final String barMethodName = "bar";
    bar.setName(barMethodName);

    final MethodContainer itf = (MethodContainer) this.nodeFactory.newXMLNode(
        CeciliaIDLConstants.CECILIA_IDL_DTD,
        CeciliaIDLConstants.INTERFACE_DEFINITION_AST_NODE_NAME);
    itf.addMethod(foo);
    itf.addMethod(bar);

    assertEquals(foo, Util.getMethodByName(itf, fooMethodName));
    assertEquals(bar, Util.getMethodByName(itf, barMethodName));
    assertNull(Util.getMethodByName(itf, "nonPresentMethodName"));
  }
}
