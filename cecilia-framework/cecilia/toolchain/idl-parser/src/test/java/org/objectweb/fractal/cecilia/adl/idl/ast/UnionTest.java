/***
 * Cecilia ADL Compiler
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace, Lionel Debroux
 */

package org.objectweb.fractal.cecilia.adl.idl.ast;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.cecilia.adl.idl.CeciliaIDLConstants;

/**
 * Tests for Union.
 */
public class UnionTest extends AbstractASTNodesTest {

  @Test
  public void testCreateUnionDefinition() throws Exception {
    try {
      final Node n = this.nodeFactory.newXMLNode(
          CeciliaIDLConstants.CECILIA_IDL_DTD, "unionDefinition");
      assertTrue("Assert created node type", n instanceof UnionDefinition);
    } catch (final Exception e) {
      e.printStackTrace();
      fail("Unable to create 'unionDefinition' node type: check your DTD definition");
    }
  }

  @Test
  public void testComplexTypeIsAnUnionDefinitionContainer() throws Exception {
    try {
      final Node n = this.nodeFactory.newXMLNode(
          CeciliaIDLConstants.CECILIA_IDL_DTD, "complexType");
      assertTrue("Assert created node type", n instanceof ComplexType);
      assertTrue("Assert ComplexType instance of IDLDefinitionContainer",
          n instanceof IDLDefinitionContainer);
    } catch (final Exception e) {
      e.printStackTrace();
      fail("Node is not an " + IDLDefinitionContainer.class
          + ": check your interface hierarchy or DTD definition");
    }
  }
}
