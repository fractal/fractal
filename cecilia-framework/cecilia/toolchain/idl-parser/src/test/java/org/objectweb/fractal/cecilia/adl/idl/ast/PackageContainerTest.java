/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */
package org.objectweb.fractal.cecilia.adl.idl.ast;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.cecilia.adl.idl.CeciliaIDLConstants;

/**
 * @author Alessio Pace
 */
public class PackageContainerTest extends AbstractASTNodesTest {

  @Test
  public void testCreatePackageContainerNode() throws Exception {

    final Node n = this.nodeFactory.newXMLNode(CeciliaIDLConstants.CECILIA_IDL_DTD,
        CeciliaIDLConstants.PACKAGE_CONTAINER_AST_NODE_NAME);
    assertTrue("Assert is a PackageContainer", n instanceof PackageContainer);

  }

  @Test
  public void testInterfaceDefinitionIsAPackageContainer() throws Exception {

    final Node n = this.nodeFactory.newXMLNode(CeciliaIDLConstants.CECILIA_IDL_DTD,
        CeciliaIDLConstants.INTERFACE_DEFINITION_AST_NODE_NAME);
    assertTrue("Assert is a PackageContainer", n instanceof PackageContainer);

  }

  @Test
  public void testRecordDefinitionIsAPackageContainer() throws Exception {

    final Node n = this.nodeFactory.newXMLNode(CeciliaIDLConstants.CECILIA_IDL_DTD,
        CeciliaIDLConstants.RECORD_DEFINITION);
    assertTrue("Assert is a PackageContainer", n instanceof PackageContainer);

  }
}
