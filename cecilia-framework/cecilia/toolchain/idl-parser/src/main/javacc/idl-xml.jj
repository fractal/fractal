
options {
  JAVA_UNICODE_ESCAPE = true;
  STATIC=false;
}

PARSER_BEGIN(IDLParser)

package org.objectweb.fractal.cecilia.adl.idl.parser;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.adl.xml.XMLNode;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.cecilia.adl.idl.ast.ArrayOf;
import org.objectweb.fractal.cecilia.adl.idl.ast.ComplexType;
import org.objectweb.fractal.cecilia.adl.idl.ast.ExtendingDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Field;
import org.objectweb.fractal.cecilia.adl.idl.ast.FieldContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Import;
import org.objectweb.fractal.cecilia.adl.idl.ast.ImportContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.InterfaceDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Method;
import org.objectweb.fractal.cecilia.adl.idl.ast.MethodContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.Package;
import org.objectweb.fractal.cecilia.adl.idl.ast.PackageContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.Parameter;
import org.objectweb.fractal.cecilia.adl.idl.ast.ParameterContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.PointerOf;
import org.objectweb.fractal.cecilia.adl.idl.ast.PrimitiveType;
import org.objectweb.fractal.cecilia.adl.idl.ast.RecordDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Type;
import org.objectweb.fractal.cecilia.adl.idl.ast.TypeContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.PrimitiveType.PrimitiveTypeEnum;
import org.objectweb.fractal.cecilia.adl.idl.ast.EnumDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.EnumNameValuePair;
import org.objectweb.fractal.cecilia.adl.idl.ast.UnionDefinition;
import org.objectweb.fractal.cecilia.adl.idl.util.Util;
import org.xml.sax.SAXException;

@SuppressWarnings("all")
public class IDLParser
{
  static String lastjavadoc = null;
  private String filename;
  private XMLNodeFactory nodeFactory;
  private String dtd;

  public void init(XMLNodeFactory nodeFactory, String dtd, String filename) {
    this.nodeFactory = nodeFactory;
    this.dtd = dtd;
    this.filename = filename;
    try {
      nodeFactory.checkDTD(dtd) ;
    } catch (SAXException e) {
      // TODO use a more specific type of RuntimeException
      throw new RuntimeException("Error in dtd file '" + dtd + "'" , e);
    }
  }

  private XMLNode newNode(String name, Token t) {
    XMLNode node;
    try {
      node = nodeFactory.newXMLNode(dtd, name);
    } catch (SAXException e) {
      // TODO use a more specific type of RuntimeException
      throw new RuntimeException("Unable to create node", e);
    }
    if (t == null) node.astSetSource(filename);
    else node.astSetSource(filename + ":" + t.beginLine + ", " + t.beginColumn);
    return node;
  }
  
  private Field newField(Token name, String qualifier, Type type, String value) {
    Field field = (Field)newNode("field", name);
    field.setName(name.image);
    field.setValue(value);
    field.setQualifier(qualifier);
    Util.setContainedType((TypeContainer)field, type);
    return field;
  }
}

PARSER_END(IDLParser)


/* WHITE SPACE */
SKIP :
{
  " "
| "\t"
| "\n"
| "\r"
| "\f"
}

/* COMMENTS */

TOKEN_MGR_DECLS : {
	public String lastjavadoc = null;
}

MORE :
{
  "//" : IN_SINGLE_LINE_COMMENT
|
  <"/**" ~["/"]> : IN_FORMAL_COMMENT
|
  "/*" : IN_MULTI_LINE_COMMENT
}

<IN_SINGLE_LINE_COMMENT>
SPECIAL_TOKEN :
{
  <SINGLE_LINE_COMMENT: "\n" | "\r" | "\r\n" > : DEFAULT
}

<IN_FORMAL_COMMENT>
SPECIAL_TOKEN :
{
  <FORMAL_COMMENT: "*/" > { lastjavadoc = image.toString(); } : DEFAULT
}

<IN_MULTI_LINE_COMMENT>
SPECIAL_TOKEN :
{
  <MULTI_LINE_COMMENT: "*/" > : DEFAULT
}

<IN_SINGLE_LINE_COMMENT,IN_FORMAL_COMMENT,IN_MULTI_LINE_COMMENT>
MORE :
{
  < ~[] >
}

/* RESERVED WORDS AND LITERALS (in C/C++ or Cecilia IDL).
   C/C++ keywords that are not Cecilia IDL keywords are maintained in ReservedWordsChecker.
*/

TOKEN :
{
  < BOOLEAN: "boolean" >
| < BYTE: "byte" >
| < CHAR: "char" >
| < CONST: "const" >
| < DOUBLE: "double" >
| < EXTENDS: "extends" >
| < FALSE: "false" >
| < FLOAT: "float" >
| < IMPORT: "import" >
| < INT: "int" >
| < INTERFACE: "interface" >
| < LONG: "long" >
| < NULL: "null" >
| < PACKAGE: "package">
| < PUBLIC: "public" >
| < SHORT: "short" >
| < UNSIGNED: "unsigned" >
| < THROWS: "throws" >
| < TRUE: "true" >
| < VOID: "void" >
// Added by Jean-Philippe Fassino
| < STRING: "string" >
| < ANY: "any" >
// Added by Ali Erdem Ozcan
| < RECORD: "record" >
| < IN: "in" >
| < OUT: "out" >
// Added by Lionel Debroux
| < INT8_T: "int8_t" >
| < UINT8_T: "uint8_t" >
| < INT16_T: "int16_t" >
| < UINT16_T: "uint16_t" >
| < INT32_T: "int32_t" >
| < UINT32_T: "uint32_t" >
| < INT64_T: "int64_t" >
| < UINT64_T: "uint64_t" >
| < INT_LEAST8_T: "int_least8_t" >
| < UINT_LEAST8_T: "uint_least8_t" >
| < INT_LEAST16_T: "int_least16_t" >
| < UINT_LEAST16_T: "uint_least16_t" >
| < INT_LEAST32_T: "int_least32_t" >
| < UINT_LEAST32_T: "uint_least32_t" >
| < INT_LEAST64_T: "int_least64_t" >
| < UINT_LEAST64_T: "uint_least64_t" >
| < INT_FAST8_T: "int_fast8_t" >
| < UINT_FAST8_T: "uint_fast8_t" >
| < INT_FAST16_T: "int_fast16_t" >
| < UINT_FAST16_T: "uint_fast16_t" >
| < INT_FAST32_T: "int_fast32_t" >
| < UINT_FAST32_T: "uint_fast32_t" >
| < INT_FAST64_T: "int_fast64_t" >
| < UINT_FAST64_T: "uint_fast64_t" >
| < INTPTR_T: "intptr_t" >
| < UINTPTR_T: "uintptr_t" >
| < INTMAX_T: "intmax_t" >
| < UINTMAX_T: "uintmax_t" >
| < SIZE_T: "size_t" >
| < PTRDIFF_T: "ptrdiff_t" >
| < WCHAR_T: "wchar_t" >
// Added by Alessio Pace, Lionel Debroux
| < ENUM: "enum" >
// Added by Lionel Debroux
| < STRUCT: "struct" >
| < UNION: "union" >
}

/* LITERALS */

TOKEN :
{
  < INTEGER_LITERAL:
        <DECIMAL_LITERAL> (["l","L"])?
      | <HEX_LITERAL> (["l","L"])?
      | <OCTAL_LITERAL> (["l","L"])?
  >
|
  < #DECIMAL_LITERAL: ["1"-"9"] (["0"-"9"])* >
|
  < #HEX_LITERAL: "0" ["x","X"] (["0"-"9","a"-"f","A"-"F"])+ >
|
  < #OCTAL_LITERAL: "0" (["0"-"7"])* >
|
  < FLOATING_POINT_LITERAL:
        (["0"-"9"])+ "." (["0"-"9"])* (<EXPONENT>)? (["f","F","d","D"])?
      | "." (["0"-"9"])+ (<EXPONENT>)? (["f","F","d","D"])?
      | (["0"-"9"])+ <EXPONENT> (["f","F","d","D"])?
      | (["0"-"9"])+ (<EXPONENT>)? ["f","F","d","D"]
  >
|
  < #EXPONENT: ["e","E"] (["+","-"])? (["0"-"9"])+ >
|
  < CHARACTER_LITERAL:
      "'"
      (   (~["'","\\","\n","\r"])
        | ("\\"
            ( ["n","t","b","r","f","\\","'","\""]
            | ["0"-"7"] ( ["0"-"7"] )?
            | ["0"-"3"] ["0"-"7"] ["0"-"7"]
            )
          )
      )
      "'"
  >
|
  < STRING_LITERAL:
      "\""
      (   (~["\"","\\","\n","\r"])
        | ("\\"
            ( ["n","t","b","r","f","\\","'","\""]
            | ["0"-"7"] ( ["0"-"7"] )?
            | ["0"-"3"] ["0"-"7"] ["0"-"7"]
            )
          )
      )*
      "\""
  >
}

/* IDENTIFIERS */

TOKEN :
{
  < IDENTIFIER: <LETTER> (<LETTER>|<DIGIT>)* >
|
  < #LETTER:
      [
       "\u0024",
       "\u0041"-"\u005a",
       "\u005f",
       "\u0061"-"\u007a",
       "\u00c0"-"\u00d6",
       "\u00d8"-"\u00f6",
       "\u00f8"-"\u00ff",
       "\u0100"-"\u1fff",
       "\u3040"-"\u318f",
       "\u3300"-"\u337f",
       "\u3400"-"\u3d2d",
       "\u4e00"-"\u9fff",
       "\uf900"-"\ufaff"
      ]
  >
|
  < #DIGIT:
      [
       "\u0030"-"\u0039",
       "\u0660"-"\u0669",
       "\u06f0"-"\u06f9",
       "\u0966"-"\u096f",
       "\u09e6"-"\u09ef",
       "\u0a66"-"\u0a6f",
       "\u0ae6"-"\u0aef",
       "\u0b66"-"\u0b6f",
       "\u0be7"-"\u0bef",
       "\u0c66"-"\u0c6f",
       "\u0ce6"-"\u0cef",
       "\u0d66"-"\u0d6f",
       "\u0e50"-"\u0e59",
       "\u0ed0"-"\u0ed9",
       "\u1040"-"\u1049"
      ]
  >
}

/* SEPARATORS */

TOKEN :
{
  < LPAREN: "(" >
| < RPAREN: ")" >
| < LBRACE: "{" >
| < RBRACE: "}" >
| < LBRACKET: "[" >
| < RBRACKET: "]" >
| < SEMICOLON: ";" >
| < COMMA: "," >
| < DOT: "." >
}

/* OPERATORS */

TOKEN :
{
  < ASSIGN: "=" >
| < GT: ">" >
| < LT: "<" >
| < BANG: "!" >
| < TILDE: "~" >
| < HOOK: "?" >
| < COLON: ":" >
| < EQ: "==" >
| < LE: "<=" >
| < GE: ">=" >
| < NE: "!=" >
| < SC_OR: "||" >
| < SC_AND: "&&" >
| < INCR: "++" >
| < DECR: "--" >
| < PLUS: "+" >
| < MINUS: "-" >
| < STAR: "*" >
| < SLASH: "/" >
| < BIT_AND: "&" >
| < BIT_OR: "|" >
| < XOR: "^" >
| < REM: "%" >
| < LSHIFT: "<<" >
| < RSIGNEDSHIFT: ">>" >
| < PLUSASSIGN: "+=" >
| < MINUSASSIGN: "-=" >
| < STARASSIGN: "*=" >
| < SLASHASSIGN: "/=" >
| < ANDASSIGN: "&=" >
| < ORASSIGN: "|=" >
| < XORASSIGN: "^=" >
| < REMASSIGN: "%=" >
| < LSHIFTASSIGN: "<<=" >
| < RSIGNEDSHIFTASSIGN: ">>=" >
}


/*****************************************
 * THE LANGUAGE GRAMMAR STARTS HERE      *
 *****************************************/

/*
 * Program structuring syntax follows.
 */

IDLDefinition getIDLNode() :
{
  Package p = null;
  List<Import> imports = new ArrayList<Import>();
  Import importNode;
  IDLDefinition resultNode ;
  InterfaceDefinition itfNode = null;
  RecordDefinition recNode = null;
  UnionDefinition unionNode = null;
  EnumDefinition enumNode = null;
}
{
	/* take the Package node (or null if there is no one) */
  [ p = PackageDeclaration() {token_source.lastjavadoc = null;} ]
	/* create the Import nodes and register them in a Set */
  ( importNode = ImportDeclaration() { imports.add(importNode); } )*
	/* Create the resultNode instance according to the definition*/
  ( (itfNode = InterfaceDefinition(p) { resultNode = itfNode; }) |
    (recNode = RecordDefinition(p) { resultNode = recNode;}) |
    (unionNode = UnionDefinition(p) { resultNode = unionNode;}) |
    (enumNode = EnumDefinition(p) { resultNode = enumNode;}) 
  )
  <EOF>
  { 
    ((PackageContainer)resultNode).setPackage(p);
    // EnumDefinition nodes have no import. Should it raise an error or a warning ?
    if(resultNode != enumNode) {
      for (Import i : imports) 
        ((ImportContainer)resultNode).addImport(i);
    }
    return resultNode;
  }
}

Package PackageDeclaration() :
{
  Token t;
  Package p;
}
{
  <PACKAGE> t = Name() SemiList()
  { 
     p = (Package) newNode("package", t);
     p.setName(t.image);
     return p; 
  }
}

Import ImportDeclaration() :
{
  Token t, name;
  Import i;
}
{
  t = <IMPORT> name = Name() SemiList()
    { 
      i = (Import) newNode("import", t);
      i.setName(name.image);
      return i;
    }
}

InterfaceDefinition InterfaceDefinition(Package p) :
{
  Token t;
  InterfaceDefinition itfNode = null ;
  Token ext;
}
{
  ( <PUBLIC> ) *
  <INTERFACE> t = <IDENTIFIER> 
	{ itfNode = (InterfaceDefinition)newNode("interfaceDefinition", t);
	  itfNode.setName((p == null) ? t.image : p.getName() + "." + t.image);
	} 
  [ <EXTENDS> ext = Name() { ((ExtendingDefinition) itfNode).setExtends(ext.image);} ]
  <LBRACE> 
    ( InterfaceMemberDefinition(itfNode) )* 
  <RBRACE>
  { 
  	return itfNode;
   }
}

EnumDefinition EnumDefinition(Package p) :
{
  Token t;
  EnumDefinition enumNode = null ;
  Token ext;
}
{
  <ENUM> t = <IDENTIFIER> 
	{ enumNode = (EnumDefinition)newNode("enumDefinition", t);
	  enumNode.setName((p == null) ? t.image : p.getName() + "." + t.image);
	} 
  <LBRACE> 
    (EnumMemberDefinition(enumNode))+
  <RBRACE>
  { 
  	return enumNode;
   }
}

void EnumMemberDefinition(EnumDefinition enumDefinition) :
{
    EnumNameValuePair pair;
}
{
  pair = EnumNameValuePairDefinition() { enumDefinition.addEnumNameValuePair(pair); }
}

EnumNameValuePair EnumNameValuePairDefinition() :
{ 
  Token t = null;
  EnumNameValuePair field ;
  String value = null;
}
{
  /* the enum member value is optional */
  t = <IDENTIFIER> (<COLON> (value = Expression() | value = LiteralString()))? CommaList()
  { 
    field = (EnumNameValuePair)newNode("enumNameValuePair", t);
    field.setName(t.image);
    field.setValue(value);
    return field;
   }
}


RecordDefinition RecordDefinition(Package p) :
{
  Token t;
  RecordDefinition recNode = null ;
}
{
  <RECORD> t = <IDENTIFIER> 
	{   
	  recNode = (RecordDefinition)newNode("recordDefinition", t);
	  recNode.setName((p == null) ? t.image : p.getName() + "." + t.image);
	} 
  <LBRACE>
      (RecordMemberDefinition(recNode))*
  <RBRACE>
  { 
    return recNode;
   }
}

UnionDefinition UnionDefinition(Package p) :
{
  Token t;
  UnionDefinition unionNode = null ;
}
{
  <UNION> t = <IDENTIFIER> 
	{   
	  unionNode = (UnionDefinition)newNode("unionDefinition", t);
	  unionNode.setName((p == null) ? t.image : p.getName() + "." + t.image);
	} 
  <LBRACE>
      (UnionMemberDefinition(unionNode))*
  <RBRACE>
  { 
    return unionNode;
   }
}

void InterfaceMemberDefinition(InterfaceDefinition itf) :
{}
{
  LOOKAHEAD( ReturnType() <IDENTIFIER> <LPAREN> )
  MethodDefinition((MethodContainer)itf)
  | FieldDefinition((FieldContainer)itf)
}

void RecordMemberDefinition(RecordDefinition rec) :
{}
{
  RecordFieldDefinition((FieldContainer)rec) 
}

void UnionMemberDefinition(UnionDefinition rec) :
{}
{
  UnionFieldDefinition((FieldContainer)rec)
}

String RecordFieldQualifier() :
{
  String qualifier = "";
}
{
  [ 
    ( <CONST> {qualifier = Field.CONST;} ) 
  ]
  { return qualifier; }
}

String UnionFieldQualifier() :
{
  String qualifier = "";
}
{
  [ 
    ( <CONST> {qualifier = Field.CONST;} ) 
  ]
  { return qualifier; }
}

void RecordFieldDefinition(FieldContainer container) :
{
  Token t;
  Field field;
  Type type;
  String qualifier = "";
}
{
  qualifier = RecordFieldQualifier() type = RecordFieldType()

  t = <IDENTIFIER> { container.addField(newField(t, qualifier, type, null)); }
  (
    <COMMA> t = <IDENTIFIER>
    { container.addField(newField(t, qualifier, type, null)); }
  ) *
  SemiList()
}

void UnionFieldDefinition(FieldContainer container) :
{
  Token t;
  Field field;
  Type type;
  String qualifier = "";
}
{
  qualifier = UnionFieldQualifier() type = RecordFieldType()

  t = <IDENTIFIER> { container.addField(newField(t, qualifier, type, null)); }
  (
    <COMMA> t = <IDENTIFIER>
    { container.addField(newField(t, qualifier, type, null)); }
  ) *
  SemiList()
}

void FieldDefinition(FieldContainer container) :
{ 
  Token t;
  Field field ;
  PrimitiveType type;
  String value = null;
}
{
  type = PrimitiveType() 
  
  t = <IDENTIFIER> <ASSIGN> (value = Expression() | value = LiteralString() )
  { 
    container.addField(newField(t, null, type, value));
  }
  
  (
    <COMMA> t = <IDENTIFIER> 
    <ASSIGN> (value = Expression() | value = LiteralString() )
    { container.addField(newField(t, null, type, value)); }
  ) *
  
  SemiList()
}

void MethodDefinition(MethodContainer container) :
{
  Token t;
  Type type;
  Method method;
}
{
  type = ReturnType() t= <IDENTIFIER> 
  { 
  	method = (Method)newNode("method", t);
  	method.setName(t.image);
  	method.setHasVarParams("false");
    Util.setContainedType((TypeContainer)method, type);
  } 
  FormalParameters((ParameterContainer)method) 
  [ <THROWS> NameList() /* NOT handle yet */] SemiList()
  { container.addMethod(method); }
}

void FormalParameters(ParameterContainer method) :
{
  Parameter fp;
  PrimitiveType type ;
  Token t;
}
{
  <LPAREN> 
    [ fp = FormalParameter() { method.addParameter(fp) ; }
      ( LOOKAHEAD(2) <COMMA> fp = FormalParameter() { method.addParameter(fp); } )* ] 
    [ <COMMA> t = "..." 
      { 
        ((Method)method).setHasVarParams("true");
      }
    ]
  <RPAREN>
}

Parameter FormalParameter() :
{
  Token t;
  Type type;
  Parameter fp;
  String qualifier;
}
{
  qualifier = ParameterQualifier() type = ParameterType() t = <IDENTIFIER>
    {
      fp = (Parameter)newNode("parameter", t);
      fp.setName(t.image);
      fp.setQualifier(qualifier);
	  Util.setContainedType((TypeContainer)fp, type);
      return fp;
	}
}

String ParameterQualifier() :
{
  String qualifier = Parameter.IN;;
}
{
  [ 
    ( <CONST> {qualifier = Parameter.CONST;} [ <IN> ]) 
    | ( <OUT> {qualifier = Parameter.OUT; } ) 
    | ( <IN> [ <OUT> {qualifier = Parameter.IN_OUT; } ] )
  ]
  { return qualifier; }
}

/* ------------------------------------------------------------- */
/*
 * Type, name and expression syntax follows.
 */
 
Type ParameterType() :
{
  Type type;
  Token t ;
  Token size ;
  ArrayOf arrayOf ;
  PointerOf pointerOf ;
}
{
  ( 
    type = PrimitiveType()
    | type = ComplexType()
  ) 
  (
    t =  "*" 
      {
        pointerOf = (PointerOf)newNode("pointerOf", t) ;
        Util.setContainedType(pointerOf, type) ;
        type = pointerOf ;
      }
  )*
  [
    LOOKAHEAD(2)  t = "[" "]"  
      {
        arrayOf = (ArrayOf)newNode("arrayOf", t) ;
        Util.setContainedType(arrayOf, type) ;
        type = arrayOf ;
      }
   ]
  (
    t =  "[" size = <INTEGER_LITERAL> "]"  
      {
        arrayOf = (ArrayOf)newNode("arrayOf", t) ;
        arrayOf.setSize(size.image) ;
        Util.setContainedType(arrayOf, type) ;
        type = arrayOf ;
      }
  )*
  { return type ;}
}


Type RecordFieldType() :
{
  Type type;
  Token t ;
  Token size ;
  ArrayOf arrayOf ;
  PointerOf pointerOf ;
}
{
  ( 
    type = PrimitiveType()
    | type = ComplexType()
  ) 
  (
    t =  "*" 
      {
        pointerOf = (PointerOf)newNode("pointerOf", t) ;
        Util.setContainedType(pointerOf, type) ;
        type = pointerOf ;
      }
  )*
  (
    t =  "[" size = <INTEGER_LITERAL> "]"  
      {
        arrayOf = (ArrayOf)newNode("arrayOf", t) ;
        arrayOf.setSize(size.image) ;
        Util.setContainedType(arrayOf, type) ;
        type = arrayOf ;
      }
  )*
  { return type ;}
}


Type ReturnType() :
{
  Type type;
  Token t ;
  Token size ;
  PrimitiveType primitiveType;
  ArrayOf arrayOf ;
  PointerOf pointerOf ;
}
{
  ( 
    t = <VOID> 
      {
      	primitiveType =  (PrimitiveType) newNode("primitiveType", t); 
        primitiveType.setName(PrimitiveTypeEnum.VOID.toString()); 
        type = primitiveType;
      }
    | type = PrimitiveType()
    | type = ComplexType()
  ) 
  (
    t =  "*" 
      {
        pointerOf = (PointerOf)newNode("pointerOf", t) ;
        Util.setContainedType(pointerOf, type) ;
        type = pointerOf ;
      }
  )*
  [
    t = "[" "]"  
      {
        arrayOf = (ArrayOf)newNode("arrayOf", t) ;
        Util.setContainedType(arrayOf, type) ;
        type = arrayOf ;
      }
  ]
  { return type ;}
}


ComplexType ComplexType() :
{
  Token t;
  ComplexType type ;
}
{
  t = Name() 
    { 
      type = (ComplexType)newNode("complexType", t);
      type.setName(t.image); 
      return type;
    }
}

PrimitiveType PrimitiveType() :
{
  Token t;
  String typeName;
  PrimitiveType primitiveType;
}
{
  ( 
    ( 
      ( t = <BOOLEAN> | t = <CHAR> | t = <BYTE> | t = <SHORT> | t = <INT> 
        | t = <LONG> | t = <FLOAT> | t = <DOUBLE> | t = <STRING> | t = <ANY> 
        | t = <INT8_T> | t = <UINT8_T> | t = <INT16_T> | t = <UINT16_T> 
        | t = <INT32_T> | t = <UINT32_T> | t = <INT64_T> | t = <UINT64_T> 
        | t = <INT_LEAST8_T> | t = <UINT_LEAST8_T> | t = <INT_LEAST16_T> | t = <UINT_LEAST16_T> 
        | t = <INT_LEAST32_T> | t = <UINT_LEAST32_T> | t = <INT_LEAST64_T> | t = <UINT_LEAST64_T> 
        | t = <INT_FAST8_T> | t = <UINT_FAST8_T> | t = <INT_FAST16_T> | t = <UINT_FAST16_T> 
        | t = <INT_FAST32_T> | t = <UINT_FAST32_T> | t = <INT_FAST64_T> | t = <UINT_FAST64_T> 
        | t = <INTPTR_T> | t = <UINTPTR_T> | t = <INTMAX_T> | t = <UINTMAX_T> | t = <SIZE_T>
        | t = <PTRDIFF_T> | t = <WCHAR_T> )
      { typeName = t.image; } ) 
    | ( <UNSIGNED> { typeName = "unsigned "; }
        ( t = <BYTE> | t = <SHORT> | t = <INT> | t = <LONG> ) { typeName += t.image; } )
  )
  {
    primitiveType = (PrimitiveType) newNode ("primitiveType", t); 
    primitiveType.setName(PrimitiveTypeEnum.fromIDLTypeName(typeName).toString());
  	return primitiveType;
  }
}

Token Name() :
{
  Token t, t1;
}
{
  t = <IDENTIFIER>
  ( <DOT> t1 = <IDENTIFIER> { t.image += "." + t1.image; })*
  { return t; }
}

String[] NameList() :
{
  Token t;
  List<String> list = new ArrayList<String>();
}
{
  t = Name() { list.add(t.image); }
  ( "," t = Name() { list.add(t.image); } )*
  { return list.toArray(new String[list.size()]); }
}

String Expression() :
{
	String exp;
}
{
  exp = InclusiveOrExpression()
  { return exp; }
}

String InclusiveOrExpression() :
{
	String exp, exp2;
}
{
  exp = ExclusiveOrExpression() ( "|" exp2 = ExclusiveOrExpression() { exp.concat("|" + exp2);} )*
  { return exp; }
}

String ExclusiveOrExpression() :
{
	String exp, exp2;
}
{
  exp = AndExpression() ( "^" exp2 = AndExpression() { exp.concat("^" + exp2);} )*
  { return exp; }
}

String AndExpression() :
{
	String exp, exp2;
}
{
  exp = ShiftExpression() ( "&" exp2 = ShiftExpression() { exp.concat("&" + exp2);} )*
  { return exp; }
}

String ShiftExpression() :
{
	String exp, exp2;
	Token t;
}
{
  exp = AdditiveExpression() ( (  t = "<<" |  t = ">>") exp2 = AdditiveExpression() { exp = exp + t.image + exp2;} )*
  { return exp; }
}

String AdditiveExpression() :
{
	String exp, exp2;
	Token t;
}
{
  exp = MultiplicativeExpression() ( ( t = "+" | t = "-" ) exp2 = MultiplicativeExpression() { exp = exp + t.image + exp2;} )*
  { return exp; }
}

String MultiplicativeExpression() :
{
	String exp, exp2;
	Token t;
}
{
  exp = UnaryExpression() ( ( t = "*" | t = "/" | t = "%" ) exp2 = UnaryExpression() { exp = exp + t.image + exp2;} )*
  { return exp; }
}

String UnaryExpression() :
{
	String exp;
	Token t;
}
{
  (( t = "+" | t = "-") exp = UnaryExpression() {exp = t.image + exp;}
|
  exp = UnaryExpressionNotPlusMinus())
  { return exp; }
}

String UnaryExpressionNotPlusMinus() :
{
	String exp;
	Token t;
}
{
  (( t = "~" | t = "!" ) exp = UnaryExpression() {exp = t.image + exp;}
|
  exp = Primary())
  { return exp; }
}

String Primary() :
{
	String exp;
}
{
  ( exp = Literal()
  | "(" exp = Expression() ")" { exp = "(" + exp + ")"; })
  { return exp; }
}

String Literal() :
{
  Token t;
}
{
  ( t = <INTEGER_LITERAL> 
  | t = <FLOATING_POINT_LITERAL>
  | t = <CHARACTER_LITERAL> 
  | t = "true" 
  | t = "false" 
  | t = "null" 
  )
  { return t.image; }
}

String LiteralString() :
{
  Token t;
}
{
  t = <STRING_LITERAL> 
  { return t.image; }
}

void SemiList() :
{}
{
  ( <SEMICOLON> ) *
}

void CommaList() :
{}
{
  ( <COMMA> ) *
}
