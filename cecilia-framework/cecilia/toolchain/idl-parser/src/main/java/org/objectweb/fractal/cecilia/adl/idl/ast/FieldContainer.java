/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.adl.idl.ast;

import org.objectweb.fractal.adl.Node;

/**
 * AST node interface for <code>field</code> container elements.
 */

public interface FieldContainer extends Node {
  /**
   * Returns the <code>field</code> sub-elements.
   * 
   * @return the <code>field</code> sub-elements.
   */

  Field[] getFields();

  /**
   * Adds a <code>field</code> sub-element.
   * 
   * @param field a <code>field</code> sub-element to add.
   */

  void addField(Field field);

  /**
   * Removes a <code>field</code> sub-element.
   * 
   * @param field a <code>field</code> sub-element to remove.
   */

  void removeField(Field field);
}
