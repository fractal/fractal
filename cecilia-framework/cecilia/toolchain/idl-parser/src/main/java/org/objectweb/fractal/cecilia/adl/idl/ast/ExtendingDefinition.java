/***
 * Cecilia ADL Compiler
 * Copyright (C) 2005 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia.adl.idl.ast;

import org.objectweb.fractal.adl.Node;

/**
 * Generic AST node interface for definition node that extends another
 * definition.
 */
public interface ExtendingDefinition extends Node {

  /**
   * Returns the definition that is extended by this one.
   * 
   * @return the definition that is extended by this one.
   */
  String getExtends();

  /**
   * Set the definition that is extended by this one.
   * 
   * @param ext the definition that is extended by this one.
   */
  void setExtends(String ext);

}
