/***
 * Cecilia ADL Compiler
 * Copyright (C) 2005 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.adl.idl;

import org.objectweb.fractal.cecilia.adl.idl.ast.Field;
import org.objectweb.fractal.cecilia.adl.idl.ast.FieldContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.RecordDefinition;

/**
 * Abstract attribute/record definition visitor.
 */

public abstract class AbstractRecordVisitor implements RecordVisitor {

  public void visit(final IDLDefinition idlDefinition) throws Exception {
    if (!(idlDefinition instanceof RecordDefinition))
// throw new IDLParserException("Only record definition nodes are accepted.",
// idlDefinition);
      throw new IDLParserException("Only record definition nodes are accepted");
    final RecordDefinition record = (RecordDefinition) idlDefinition;

    enterRecord(record);

    for (final Field field : ((FieldContainer) record).getFields())
      visitField(field);

    leaveRecord(record);
  }
}
