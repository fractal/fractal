/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.adl.idl.ast;

/**
 * AST node interface for <code>primitiveType</code> elements.
 */
public interface PrimitiveType extends Type {

  /** Enumeration of the primitive types. */
  public static enum PrimitiveTypeEnum {
    /** The name of the any primitive type. */
    ANY("any", "void *"),

    /** The name of the void primitive type. */
    VOID("void", "void"),

    /** The name of the boolean primitive type. */
    BOOLEAN("boolean", "jboolean"),

    /** The name of the char primitive type. */
    CHAR("char", "jchar"),

    /** The name of the byte primitive type. */
    BYTE("byte", "jbyte"),

    /** The name of the short primitive type. */
    SHORT("short", "jshort"),

    /** The name of the int primitive type. */
    INT("int", "jint"),

    /** The name of the long primitive type. */
    LONG("long", "jlong"),

    /** The name of the float primitive type. */
    FLOAT("float", "jfloat"),

    /** The name of the double primitive type. */
    DOUBLE("double", "jdouble"),

    /** The name of the string primitive type. */
    STRING("string", "char*"),

    /** The name of the unsigned byte primitive type. */
    UNSIGNED_BYTE("unsigned byte", "jubyte"),

    /** The name of the unsigned short primitive type. */
    UNSIGNED_SHORT("unsigned short", "jushort"),

    /** The name of the unsigned int primitive type. */
    UNSIGNED_INT("unsigned int", "juint"),

    /** The name of the unsigned long primitive type. */
    UNSIGNED_LONG("unsigned long", "julong"),

    /** The name of the int8_t primitive type. */
    INT8_T("int8_t", "int8_t"),

    /** The name of the uint8_t primitive type. */
    UINT8_T("uint8_t", "uint8_t"),

    /** The name of the int16_t primitive type. */
    INT16_T("int16_t", "int16_t"),

    /** The name of the uint16_t primitive type. */
    UINT16_T("uint16_t", "uint16_t"),

    /** The name of the int32_t primitive type. */
    INT32_T("int32_t", "int32_t"),

    /** The name of the uint32_t primitive type. */
    UINT32_T("uint32_t", "uint32_t"),

    /** The name of the int64_t primitive type. */
    INT64_T("int64_t", "int64_t"),

    /** The name of the uint64_t primitive type. */
    UINT64_T("uint64_t", "uint64_t"),

    /** The name of the int_least8_t primitive type. */
    INT_LEAST8_T("int_least8_t", "int_least8_t"),

    /** The name of the uint_least8_t primitive type. */
    UINT_LEAST8_T("uint_least8_t", "uint_least8_t"),

    /** The name of the int_least16_t primitive type. */
    INT_LEAST16_T("int_least16_t", "int_least16_t"),

    /** The name of the uint_least16_t primitive type. */
    UINT_LEAST16_T("uint_least16_t", "uint_least16_t"),

    /** The name of the int_least32_t primitive type. */
    INT_LEAST32_T("int_least32_t", "int_least32_t"),

    /** The name of the uint_least32_t primitive type. */
    UINT_LEAST32_T("uint_least32_t", "uint_least32_t"),

    /** The name of the int_least64_t primitive type. */
    INT_LEAST64_T("int_least64_t", "int_least64_t"),

    /** The name of the uint_least64_t primitive type. */
    UINT_LEAST64_T("uint_least64_t", "uint_least64_t"),

    /** The name of the int_fast8_t primitive type. */
    INT_FAST8_T("int_fast8_t", "int_fast8_t"),

    /** The name of the uint_fast8_t primitive type. */
    UINT_FAST8_T("uint_fast8_t", "uint_fast8_t"),

    /** The name of the int_fast16_t primitive type. */
    INT_FAST16_T("int_fast16_t", "int_fast16_t"),

    /** The name of the uint_fast16_t primitive type. */
    UINT_FAST16_T("uint_fast16_t", "uint_fast16_t"),

    /** The name of the int_fast32_t primitive type. */
    INT_FAST32_T("int_fast32_t", "int_fast32_t"),

    /** The name of the uint_fast32_t primitive type. */
    UINT_FAST32_T("uint_fast32_t", "uint_fast32_t"),

    /** The name of the int_fast64_t primitive type. */
    INT_FAST64_T("int_fast64_t", "int_fast64_t"),

    /** The name of the uint_fast64_t primitive type. */
    UINT_FAST64_T("uint_fast64_t", "uint_fast64_t"),

    /** The name of the intptr_t primitive type. */
    INTPTR_T("intptr_t", "intptr_t"),

    /** The name of the uintptr_t primitive type. */
    UINTPTR_T("uintptr_t", "uintptr_t"),

    /** The name of the intmax_t primitive type. */
    INTMAX_T("intmax_t", "intmax_t"),

    /** The name of the uintmax_t primitive type. */
    UINTMAX_T("uintmax_t", "uintmax_t"),

    /** The name of the size_t primitive type. */
    SIZE_T("size_t", "size_t"),

    /** The name of the size_t primitive type. */
    PTRDIFF_T("ptrdiff_t", "ptrdiff_t"),

    /** The name of the size_t primitive type. */
    WCHAR_T("wchar_t", "wchar_t");

    private final String idlTypeName;
    private final String cType;

    PrimitiveTypeEnum(final String idlType, final String cType) {
      this.idlTypeName = idlType;
      this.cType = cType;
    }

    /**
     * @return the IDL representation of the primitive type.
     */
    public String getIdlTypeName() {
      return idlTypeName;
    }

    /**
     * @return the equivalent C type.
     */
    public String getCType() {
      return cType;
    }

    /**
     * Returns the enumeration constant that correspond to the given IDL type
     * name.
     * 
     * @param idlTypeName the name of the primitive type as it is found in the
     *            IDL.
     * @return the corresponding enumeration constant.
     * @throws IllegalArgumentException if the given IDL type name does not
     *             correspond to a primitive type.
     */
    public static PrimitiveTypeEnum fromIDLTypeName(final String idlTypeName) {
      for (final PrimitiveTypeEnum t : values()) {
        if (t.idlTypeName.equals(idlTypeName)) {
          return t;
        }
      }
      throw new IllegalArgumentException(
          "The given IDL type name does not correspond to a primitive type.");
    }
  };

  /**
   * Returns the name of the primitive type.
   * 
   * @return the name of the primitive type.
   */
  String getName();

  /**
   * Set the name of the primitive type.
   * 
   * @param name the name of the primitive type to be set.
   */
  void setName(String name);
}
