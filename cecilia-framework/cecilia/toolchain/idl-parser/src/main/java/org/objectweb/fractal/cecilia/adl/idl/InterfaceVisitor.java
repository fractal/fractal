/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.adl.idl;

import org.objectweb.fractal.cecilia.adl.idl.ast.Field;
import org.objectweb.fractal.cecilia.adl.idl.ast.InterfaceDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Method;

/**
 * The meta builder interface for Interface writers.
 */
public interface InterfaceVisitor extends IDLVisitor {

  /**
   * Builds the header part for an given interface definition.
   * 
   * @param itf the interface definition for which has to be built.
   * @throws Exception if any error occurs.
   */
  void enterInterface(InterfaceDefinition itf) throws Exception;

  /**
   * Builds the footer part for an given interface definition.
   * 
   * @param itf the interface definition for which has to be built.
   * @throws Exception if any error occurs.
   */
  void leaveInterface(InterfaceDefinition itf) throws Exception;

  /**
   * Visits a method definition.
   * 
   * @param method the field definition to be used.
   * @throws Exception if any error occurs.
   */

  void visitMethod(Method method) throws Exception;

  /**
   * Visits a field definition.
   * 
   * @param field the field definition to be used.
   * @throws Exception if any error occurs.
   */
  void visitField(Field field) throws Exception;

}
