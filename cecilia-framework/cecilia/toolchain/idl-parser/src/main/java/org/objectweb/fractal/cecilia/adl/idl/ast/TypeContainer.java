/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.adl.idl.ast;

import org.objectweb.fractal.adl.Node;

/**
 * AST node interface for <code>Type</code> container elements.
 */
public interface TypeContainer extends Node {

  /**
   * Returns the <code>primitiveType</code> sub-element.
   * 
   * @return the <code>primitiveType</code> sub-element.
   */
  PrimitiveType getPrimitiveType();

  /**
   * Sets a <code>primitiveType</code> sub-element.
   * 
   * @param type a <code>primitiveType</code> sub-element to set.
   */
  void setPrimitiveType(PrimitiveType type);

  /**
   * Returns the <code>complexType</code> sub-element.
   * 
   * @return the <code>complexType</code> sub-element.
   */
  ComplexType getComplexType();

  /**
   * Sets a <code>complexType</code> sub-element.
   * 
   * @param type a <code>complexType</code> sub-element to set.
   */
  void setComplexType(ComplexType type);

  /**
   * Returns the <code>arrayOf</code> sub-element.
   * 
   * @return the <code>arrayOf</code> sub-element.
   */
  ArrayOf getArrayOf();

  /**
   * Sets a <code>arrayOf</code> sub-element.
   * 
   * @param arrayOf a <code>arrayOf</code> sub-element to set.
   */
  void setArrayOf(ArrayOf arrayOf);

  /**
   * Returns the <code>pointerOf</code> sub-element.
   * 
   * @return the <code>pointerOf</code> sub-element.
   */
  PointerOf getPointerOf();

  /**
   * Sets a <code>pointerOf</code> sub-element.
   * 
   * @param pointerOf a <code>pointerOf</code> sub-element to set.
   */
  void setPointerOf(PointerOf pointerOf);
}
