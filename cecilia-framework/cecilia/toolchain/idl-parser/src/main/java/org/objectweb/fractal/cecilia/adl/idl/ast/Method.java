/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.adl.idl.ast;

/**
 * AST node interface for <code>method</code> elements.
 */
public interface Method extends TypeContainer, ParameterContainer {

  /**
   * Returns the name of the method.
   * 
   * @return the name of the method.
   */
  String getName();

  /**
   * Set the name of the method.
   * 
   * @param name the name of the method to be set.
   */
  void setName(String name);

  /**
   * Returns the fact whether the method accepts a variable number of parameters
   * or not.
   * 
   * @return the fact whether the method accepts a variable number of parameters
   *         or not.
   */
  String getHasVarParams();

  /**
   * Set the fact whether the method accepts a variable number of parameters or
   * not.
   * 
   * @param hasVarParams the fact whether the method accepts a variable number
   *            of parameters or not to be set.
   */
  void setHasVarParams(String hasVarParams);
}
