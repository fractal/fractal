/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.idl.ast;

/**
 * AST node interface for <code>field</code> elements.
 */
public interface Field extends TypeContainer {

  /**
   * The parameter qualifier for constant fields. How this value is translated
   * depends on the primitive programming language being used.
   */
  String CONST = "const";

  /**
   * @return the Field qualifier.
   */
  String getQualifier();

  /**
   * @param qualifier to be set.
   */
  void setQualifier(String qualifier);

  /**
   * Returns the name of the field.
   * 
   * @return the name of the field.
   */
  String getName();

  /**
   * Set the the name of the field.
   * 
   * @param name the name of the field.
   */
  void setName(String name);

  /**
   * Returns the value of the field.
   * 
   * @return the value of the field.
   */
  String getValue();

  /**
   * Set the value of the field.
   * 
   * @param value the value of the field.
   */
  void setValue(String value);
}
