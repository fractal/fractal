/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.idl.util;

import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.cecilia.adl.idl.ast.ArrayOf;
import org.objectweb.fractal.cecilia.adl.idl.ast.ComplexType;
import org.objectweb.fractal.cecilia.adl.idl.ast.Field;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Import;
import org.objectweb.fractal.cecilia.adl.idl.ast.Method;
import org.objectweb.fractal.cecilia.adl.idl.ast.MethodContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.Parameter;
import org.objectweb.fractal.cecilia.adl.idl.ast.PointerOf;
import org.objectweb.fractal.cecilia.adl.idl.ast.PrimitiveType;
import org.objectweb.fractal.cecilia.adl.idl.ast.Type;
import org.objectweb.fractal.cecilia.adl.idl.ast.TypeContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.PrimitiveType.PrimitiveTypeEnum;

/**
 * Provides utility methods to manipulate IDL AST nodes.
 */
public final class Util {

  private Util() {
  }

  // ---------------------------------------------------------------------------
  // Type utility methods
  // ---------------------------------------------------------------------------

  /**
   * Returns <code>true</code> if the given type node is a complex type or
   * either an array or pointer of complex type.
   * 
   * @param type a type node.
   * @return <code>true</code> if the given type node is a complex type or
   *         either an array or pointer of complex type.
   */
  public static boolean isComplexType(final Type type) {
    if (type instanceof ComplexType) {
      return true;
    } else if ((type instanceof ArrayOf) || (type instanceof PointerOf)) {
      return isComplexType(getContainedType((TypeContainer) type));
    } else {
      return false;
    }
  }

  /**
   * @param type
   * @return <code>true</code> is the given type node is a
   *         {@link PrimitiveType}, <code>false</code> otherwise.
   */
  public static boolean isPrimitiveType(final Type type) {
    if (type instanceof PrimitiveType) {
      return true;
    }
    return false;

  }

  /**
   * Returns the complex type node contained by the given type. More precisely,
   * if the given <code>type</code> is a {@link ComplexType} this method
   * simply return it; if the given <code>type</code> is an {@link ArrayOf} of
   * a {@link PointerOf}, returns the complex type it contains; else, throws an
   * {@link IllegalArgumentException}
   * 
   * @param type a {@link ComplexType} or a container of {@link ComplexType}
   * @return the complex type node contained by the given type.
   * @throws IllegalArgumentException if
   *             <code>{@link #isComplexType(Type)}(type)</code> is
   *             <code>false</code>.
   */
  public static ComplexType getComplexType(final Type type) {
    if (type instanceof ComplexType) {
      return (ComplexType) type;
    } else if ((type instanceof ArrayOf) || (type instanceof PointerOf)) {
      return getComplexType(getContainedType((TypeContainer) type));
    } else {
      throw new IllegalArgumentException(
          "The given type is not a complex type.");
    }
  }

  /**
   * Returns the {@link PrimitiveTypeEnum} enumeration element that represents
   * the given primitive type.
   * 
   * @param type a type node.
   * @return the {@link PrimitiveTypeEnum} enumeration element that represents
   *         the given primitive type.
   */
  public static PrimitiveTypeEnum getPrimitiveType(final PrimitiveType type) {
    return PrimitiveTypeEnum.valueOf(type.getName());
  }

  /**
   * @param container a type container.
   * @return the type node contained by the given container or <code>null</code>
   *         if the given container is empty.
   */
  public static Type getContainedType(final TypeContainer container) {
    if (container.getPrimitiveType() != null) {
      return container.getPrimitiveType();
    }
    if (container.getComplexType() != null) {
      return container.getComplexType();
    }
    if (container.getArrayOf() != null) {
      return container.getArrayOf();
    }
    if (container.getPointerOf() != null) {
      return container.getPointerOf();
    }
    return null;
  }

  /**
   * @param container a type container.
   * @param type the type node to be added in the container.
   * @throws IllegalArgumentException if the <code>type</code> is an unkown
   *             type.
   */
  public static void setContainedType(final TypeContainer container,
      final Type type) {
    if (type instanceof PrimitiveType) {
      container.setPrimitiveType((PrimitiveType) type);
    } else if (type instanceof ComplexType) {
      container.setComplexType((ComplexType) type);
    } else if (type instanceof ArrayOf) {
      container.setArrayOf((ArrayOf) type);
    } else if (type instanceof PointerOf) {
      container.setPointerOf((PointerOf) type);
    } else {
      throw new IllegalArgumentException("Unknown type");
    }
  }

  /**
   * Returns the {@link IDLDefinition} contained by the given
   * {@link ComplexType}.
   * 
   * @param type a complex type node.
   * @return the {@link IDLDefinition} contained by the given
   *         {@link ComplexType} or <code>null</code> if the given container
   *         is empty.
   */
  public static IDLDefinition getContainedDefinition(final ComplexType type) {
    return type.getIDLDefinition();
  }

  /**
   * Set the definition contained by the given {@link ComplexType} node.
   * 
   * @param type a {@link ComplexType} node.
   * @param definition the {@link IDLDefinition} to be contained in the
   *            {@link ComplexType} param.
   */
  public static void setContainedDefinition(final ComplexType type,
      final IDLDefinition definition) {

    type.setIDLDefinition(definition);
  }

  // ---------------------------------------------------------------------------
  // Parameter utility methods
  // ---------------------------------------------------------------------------

  /**
   * Returns <code>true</code> if the given parameter is "in" (ie.
   * <code>Parameter.IN.equals(parameter.getQualifier())</code>).
   * 
   * @param parameter a parameter node.
   * @return <code>true</code> if the given parameter is "in".
   */
  public static boolean isInParameter(final Parameter parameter) {
    return Parameter.IN.equals(parameter.getQualifier());
  }

  /**
   * Returns <code>true</code> if the given parameter is "out" (ie.
   * <code>Parameter.OUT.equals(parameter.getQualifier())</code>).
   * 
   * @param parameter a parameter node.
   * @return <code>true</code> if the given parameter is "out".
   */
  public static boolean isOutParameter(final Parameter parameter) {
    return Parameter.OUT.equals(parameter.getQualifier());
  }

  /**
   * Returns <code>true</code> if the given parameter is "in out" (ie.
   * <code>Parameter.IN_OUT.equals(parameter.getQualifier())</code>).
   * 
   * @param parameter a parameter node.
   * @return <code>true</code> if the given parameter is "in out".
   */
  public static boolean isInOutParameter(final Parameter parameter) {
    return Parameter.IN_OUT.equals(parameter.getQualifier());
  }

  /**
   * Returns <code>true</code> if the given parameter is "const" (ie.
   * <code>Parameter.CONST.equals(parameter.getQualifier())</code>).
   * 
   * @param parameter a parameter node.
   * @return <code>true</code> if the given parameter is "const".
   */
  public static boolean isConstParameter(final Parameter parameter) {
    return Parameter.CONST.equals(parameter.getQualifier());
  }

  /**
   * @param field
   * @return <code>true</code> if the given {@link Field} is meant to be
   *         constant (<em>the semantic depends on the primitive programming
   *         language</em>),
   *         <code>false</code> otherwise.
   */
  public static boolean isConstField(final Field field) {
    if (field.getQualifier().equals(Field.CONST)) {
      return true;
    }
    return false;
  }

  // ---------------------------------------------------------------------------
  // Method utility methods
  // ---------------------------------------------------------------------------

  /**
   * Returns <code>true</code> if
   * {@link Method#getHasVarParams() method.getHasVarParams()} is
   * <code>"true"</code>.
   * 
   * @param method a method.
   * @return <code>true</code> if
   *         {@link Method#getHasVarParams() method.getHasVarParams()} is
   *         <code>"true"</code>.
   */
  public static boolean hasVarParams(final Method method) {
    return Boolean.valueOf(method.getHasVarParams());
  }

  // ---------------------------------------------------------------------------
  // Import utility methods
  // ---------------------------------------------------------------------------

  /**
   * A decoration set on {@link Import} nodes that reference the imported
   * {@link Node AST}.
   * 
   * @see #setImportedAST(Import, Node)
   * @see #getImportedAST(Import)
   */
  public static final String IMPORTED_AST_DECORATION = "idl-definition";

  /**
   * Sets the {@link #IMPORTED_AST_DECORATION} decoration to the given node with
   * the {@link IDLDefinition}.
   * 
   * @param i the import node to which the decoration is set.
   * @param ast the value of the decoration.
   * @see #IMPORTED_AST_DECORATION
   */
  public static void setImportedAST(final Import i, final Node ast) {
    i.astSetDecoration(IMPORTED_AST_DECORATION, ast);
  }

  /**
   * Returns the value of the {@link #IMPORTED_AST_DECORATION} decoration of the
   * given import node.
   * 
   * @param i an import node.
   * @return The value of the {@link #IMPORTED_AST_DECORATION} decoration or
   *         <code>null</code> if the given node has no such decoration.
   * @see #IMPORTED_AST_DECORATION
   */
  public static Node getImportedAST(final Import i) {
    return (Node) i.astGetDecoration(IMPORTED_AST_DECORATION);
  }

  /**
   * Returns the Method with the given name or null if none is found.
   * 
   * @param methodContainer The method container node.
   * @param name the name of the {@link Method} to be found.
   * @return the {@link Method} method with the given <code>name</code>, or
   *         <code>null</code> if none is found.
   */
  public static Method getMethodByName(final MethodContainer methodContainer,
      final String name) {

    if (name == null || name.length() == 0) {
      throw new IllegalArgumentException("Method name can't be null or empty");
    }

    final Method[] methods = methodContainer.getMethods();
    for (final Method m : methods) {
      if (m.getName().equals(name)) {
        return m;
      }
    }

    /* no method with that name found, return null */
    return null;
  }
}
