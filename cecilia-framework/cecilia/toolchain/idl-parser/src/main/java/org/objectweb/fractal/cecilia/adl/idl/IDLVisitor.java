/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.idl;

import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;

/**
 * A base visitor interface to visit an {@link IDLDefinition}.
 * 
 * @author Alessio Pace
 */
public interface IDLVisitor {

  /**
   * Visits the given {@link IDLDefinition} node.
   * 
   * @param idlDefinition the node to visit.
   * @throws Exception if an error occurs.
   */
  void visit(IDLDefinition idlDefinition) throws Exception;
}
