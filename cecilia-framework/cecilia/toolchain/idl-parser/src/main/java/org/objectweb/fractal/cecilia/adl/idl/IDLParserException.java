/***
 * Cecilia ADL Compiler
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.cecilia.adl.idl;

/**
 * Exception thrown when a syntax error is found in an IDL file.
 */
public class IDLParserException extends Exception {

  private static final long serialVersionUID = -101332210779190020L;

  /**
   * @see Exception#Exception()
   */
  public IDLParserException() {
    super();
  }

  /**
   * @see Exception#Exception(String, Throwable)
   */
  public IDLParserException(final String message, final Throwable cause) {
    super(message, cause);

  }

  /**
   * @see Exception#Exception(String)
   */
  public IDLParserException(final String message) {
    super(message);

  }

  /**
   * @see Exception#Exception(Throwable)
   */
  public IDLParserException(final Throwable cause) {
    super(cause);

  }

}