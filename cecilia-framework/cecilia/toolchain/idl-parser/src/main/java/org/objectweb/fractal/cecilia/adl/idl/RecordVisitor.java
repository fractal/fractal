/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.adl.idl;

import org.objectweb.fractal.cecilia.adl.idl.ast.Field;
import org.objectweb.fractal.cecilia.adl.idl.ast.RecordDefinition;

/**
 * The meta builder interface for Record writers.
 */
public interface RecordVisitor extends IDLVisitor {

  /**
   * Builds the header part for an given record definition.
   * 
   * @param record the record definition.
   * @throws Exception if any error occurs.
   */
  void enterRecord(RecordDefinition record) throws Exception;

  /**
   * Builds the footer part for an given record definition.
   * 
   * @param record the record definition.
   * @throws Exception if any error occurs.
   */
  void leaveRecord(RecordDefinition record) throws Exception;

  /**
   * Visits a field definition.
   * 
   * @param field the field definition to be used.
   * @throws Exception if any error occurs.
   */
  void visitField(Field field) throws Exception;

}
