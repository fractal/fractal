/***
 * Cecilia ADL Compiler
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace, Lionel Debroux
 */

package org.objectweb.fractal.cecilia.adl.idl.ast;

/**
 * AST Node interface to represent one enumeration {name, value} pair.
 */
public interface EnumNameValuePair {

  /**
   * Returns the name of this enumeration entry.
   * 
   * @return the name of this enumeration entry.
   */
  String getName();

  /**
   * Set the name of this enumeration entry.
   * 
   * @param name the name of this enumeration entry to be set.
   */
  void setName(String name);

  /**
   * Returns the value of this enumeration entry.
   * 
   * @return the value of this enumeration entry.
   */
  String getValue();

  /**
   * Set the value of this enumeration entry.
   * 
   * @param value the value of this enumeration entry to be set.
   */
  void setValue(String value);
}
