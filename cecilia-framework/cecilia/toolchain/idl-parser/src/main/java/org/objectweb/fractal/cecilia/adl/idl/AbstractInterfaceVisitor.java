/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Ali Erdem Ozcan
 */

package org.objectweb.fractal.cecilia.adl.idl;

import static org.objectweb.fractal.adl.NodeUtil.castNodeError;

import org.objectweb.fractal.cecilia.adl.idl.ast.Field;
import org.objectweb.fractal.cecilia.adl.idl.ast.FieldContainer;
import org.objectweb.fractal.cecilia.adl.idl.ast.IDLDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.InterfaceDefinition;
import org.objectweb.fractal.cecilia.adl.idl.ast.Method;
import org.objectweb.fractal.cecilia.adl.idl.ast.MethodContainer;

/**
 * Abstract Interface Definition Visitor.
 */
public abstract class AbstractInterfaceVisitor implements InterfaceVisitor {

  public void visit(final IDLDefinition idlDefinition) throws Exception {
    final InterfaceDefinition itf = castNodeError(idlDefinition,
        InterfaceDefinition.class);

    enterInterface(itf);

    if (itf instanceof FieldContainer) {
      for (final Field field : ((FieldContainer) itf).getFields())
        visitField(field);
    }

    if (itf instanceof MethodContainer) {
      for (final Method method : ((MethodContainer) itf).getMethods())
        visitMethod(method);
    }

    leaveInterface(itf);
  }

}
