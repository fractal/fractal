###################################################
#                OVERVIEW
This module contains a Cecilia IDL parser.

As part of the build of the module, the IDLParser class is generated during the "generate-sources" phase.

You can check this behaviour with:
  $ mvn clean generate-sources
  
and going into the "generated-sources" directory.


###################################################
#                 RUNNING TESTS IN AN IDE
For what has been said before, in order to run tests in your IDE you 
have to take care of generating the sources and including them in the IDE
build path, otherwise the test classes depending on the IDLParser class
will give a compilation error. 