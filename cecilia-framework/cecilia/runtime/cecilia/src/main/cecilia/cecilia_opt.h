/**
 * Cecilia, an implementation of the Fractal component model in C.
 * 
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Matthieu Leclercq.
 */

#ifndef CECILIA_H
#define CECILIA_H

/**
 * Append macros.
 */
#define DO_APPEND(a, b) a##_##b
#define APPEND(a, b) DO_APPEND(a, b)

/* The `...' is a variable argument. In the invocation of such a macro, 
 * it represents the zero or more tokens until the closing parenthesis that 
 * ends the invocation, including any commas. This set of tokens replaces 
 * the identifier __VA_ARGS__ in the macro body wherever it appears.
 *  
 * See the CPP manual for more information.
 */
#define __CECILIA_PARAMS(...) ,##__VA_ARGS__)

#define __CECILIA_ARGS(...) __VA_ARGS__)

/**
 * The component C type name.
 */
#define COMPONENT_TYPE APPEND(COMPONENT_NAME, t)

/**
 * The component's instance data C type name.
 */
#define COMPONENT_PRIVATE_DATA_TYPE APPEND(COMPONENT_NAME, instancedata)

/**
 * To compose a method name function name.
 */
#define METHOD_NAME(itfName, methName) APPEND(COMPONENT_NAME, itfName##_##methName##_method)

/**
 * METHOD definition.
 *
 * XXX the *_this pointer is not typed in this version.
 */
#define METHOD(itfName, methName) \
  APPEND(COMPONENT_NAME, itfName##_##methName##_method) \
    (void *_this __CECILIA_PARAMS

/**
 * STATIC_METHOD definition.
 *
 * This macro should be used to define static methods.
 */
#define STATIC_METHOD(itfName, methName) \
	APPEND(COMPONENT_NAME, itfName##_##methName##_method) \
	  ( __CECILIA_ARGS

/**
 * METH definition in order to omit interface name when definining a method body.
 * The toolchain is responsible for actually defining the METH_m definition.
 */
#define METH(m) METH_##m

/**
 * CLIENT(itfName) definition.
 *
 * The toolchain has to generate the CLIENT_itfName actual definition,
 * which could eventually be subject to optimizations.
 */
#define CLIENT_ITF(itfName) CLIENT_##itfName

/**
 * CLIENT(itfName, methName) definition.
 *
 * The toolchain has to generate the CLIENT_itfname_methName actual definition,
 * which could eventually be subject to optimizations.
 */
#define CLIENT(itfName, methName) CLIENT_##itfName##_##methName

/**
 * SERVER(itfName) definition.
 *
 * The toolchain has to generate the SERVER_itfName actual definition,
 * which could eventually be subject to optimizations.
 */
#define SERVER_ITF(itfName) SERVER_##itfName

/**
 * SERVER(itfName, methName) definition.
 *
 * The toolchain has to generate the SERVER_itfname_methName actual definition,
 * according to the component type.
 */
#define SERVER(itfName, methName) SERVER_##itfName##_##methName

/*
 * This construction doesn't allow recursion, i.e. it can't be used the
 * following way: CALL(c1, m1, CALL(c2, m2)).
 * 
 * User code should not use this macro when invoking functional interfaces (server
 * or client), but should rely on SERVER and CLIENT macro which could benefit of
 * optimization.
 */
#define CALL(itfPtr, methName) \
  (itfPtr)->meth->methName((itfPtr)->selfdata __CECILIA_PARAMS

// define NULL if needed.
#ifndef NULL
#define NULL ((void *) 0)
#endif

#endif //CECILIA_H
