/**
 * Cecilia, an implementation of the Fractal component model in C.
 *
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Matthieu Leclercq.
 */

#define IN_CECILIA_MECHANISM

#include "fractal/api/cloneableComposite.h"
#include <cecilia.h>

#include <string.h>

extern int printf(const char * format, ...);

// -----------------------------------------------------------------------------
// Implementation of the component interface.
// -----------------------------------------------------------------------------

/**
 * Implementation of the getFcInterface method.
 */
// int getFcInterface(in string interfaceName, out any interfaceReference);
static int getFcInterface(void* _this, const char* interfaceName,
    void ** interfaceReference)
{
  struct __cecilia_cloneableCompositeData_t
      *self = (struct __cecilia_cloneableCompositeData_t *) _this;

  if (strcmp(interfaceName, "factory") == 0) {
    *interfaceReference = &self->factory;
    return fractal_api_ErrorConst_OK;
  }

  // call getFcInterface of defaultComposite.
  return __cecilia_defaultComposite_ComponentMeths.getFcInterface(
      & (self->defaultComposite), interfaceName, interfaceReference);
}

/**
 * Implementation of the getFcInterfaces method.
 */
// int getFcInterfaces(in any[] interfaceReferences);
static int getFcInterfaces(void* _this, void* interfaceReferences[])
{
  struct __cecilia_cloneableCompositeData_t
      *self = (struct __cecilia_cloneableCompositeData_t *) _this;
  int r;

  // call getFcInterface of defaultComposite.
  r = __cecilia_defaultComposite_ComponentMeths.getFcInterfaces(
      & (self->defaultComposite), interfaceReferences);

  if (r >= 0&& interfaceReferences != NULL) {
    interfaceReferences[r] = &self->factory;
    r++;
  }

  return r;
}

/** Declaration of the V-Table of this interface implementation. */
struct Mfractal_api_Component __cecilia_cloneableComposite_ComponentMeths = {
    getFcInterface, getFcInterfaces };

// -----------------------------------------------------------------------------
// Implementation of the binding-controller interface.
// -----------------------------------------------------------------------------

/**
 * Implementation of the listFc method.
 */
// int listFc(in string[] clientItfNames);
static int listFc(void* _this, const char* clientItfNames[])
{
  struct __cecilia_cloneableCompositeData_t
      *self = (struct __cecilia_cloneableCompositeData_t *) _this;
  int r;

  // Call the listFc of defaultComposite.
  r = __cecilia_defaultComposite_BindingControllerMeths.listFc(
      & (self->defaultComposite), clientItfNames);

  if (r >= 0&& clientItfNames != NULL) {
    clientItfNames[r] = "factory-allocator";
    r++;
  }

  return r;
}

/**
 * Implementation of the lookupFc method.
 */
// int lookupFc(in string clientItfName, out any interfaceReference);
static int lookupFc(void* _this, const char* clientItfName, void** interfaceReference)
{
  struct __cecilia_cloneableCompositeData_t
      *self = (struct __cecilia_cloneableCompositeData_t *) _this;

  if (strcmp(clientItfName, "factory-allocator") == 0) {
    *interfaceReference = &self->factory_allocator;
    return fractal_api_ErrorConst_OK;
  }

  // call lookupFc of defaultComposite.
  return __cecilia_defaultComposite_BindingControllerMeths.lookupFc(
                                                                    & (self->defaultComposite), clientItfName, interfaceReference);
}

/**
 * Implementation of the bindFc method.
 */
// int bindFc(in string clientItfName, in any serverItf);
static int bindFc(void* _this, const char* clientItfName, void* serverItf)
{
  struct __cecilia_cloneableCompositeData_t
      *self = (struct __cecilia_cloneableCompositeData_t *) _this;
  int err;

  if (strcmp(clientItfName, "factory-allocator") == 0) {
    self->factory_allocator = serverItf;
  }

  // call bindFc of defaultComposite (to bind sub components).
  err = __cecilia_defaultComposite_BindingControllerMeths.bindFc(
      & (self->defaultComposite), clientItfName, serverItf);

  if (err == fractal_api_ErrorConst_NO_SUCH_INTERFACE) {
    // "super.bindFc" may return NO_SUCH_INTERFACE if there is no sub component
    // bound to this interface. This is not an error.
    err = fractal_api_ErrorConst_OK;
  }

  return err;
}

/**
 * Implementation of the unbindFc method.
 */
// int unbindFc(in string clientItfName);
static int unbindFc(void* _this, const char* clientItfName)
{
  struct __cecilia_cloneableCompositeData_t
      *self = (struct __cecilia_cloneableCompositeData_t *) _this;
  int err;

  if (strcmp(clientItfName, "factory-allocator") == 0) {
    self->factory_allocator = NULL;
  }

  // call unbindFc of defaultComposite (to unbind sub components).
  err = __cecilia_defaultComposite_BindingControllerMeths.unbindFc(
      & (self->defaultComposite), clientItfName);

  if (err == fractal_api_ErrorConst_NO_SUCH_INTERFACE) {
    // "super.bindFc" may return NO_SUCH_INTERFACE if there is no sub component
    // bound to this interface. This is not an error.
    err = fractal_api_ErrorConst_OK;
  }

  return err;
}

/** Declaration of the V-Table of this interface implementation. */
struct Mfractal_api_BindingController
    __cecilia_cloneableComposite_BindingControllerMeths = { listFc, lookupFc,
        bindFc, unbindFc };

// -----------------------------------------------------------------------------
// Implementation of the factory interface.
// -----------------------------------------------------------------------------

/**
 * Implementation of the newFcInstance method.
 */
// int newFcInstance(out Component instance)
static int newFcInstance(void *_this, Rfractal_api_Component **instance)
{
  struct __cecilia_cloneableCompositeData_t
      *self = (struct __cecilia_cloneableCompositeData_t *) _this;

  // New composite data structure
  struct __cecilia_cloneableCompositeData_t *newComp;

  // New sub-components structure
  struct SubComponentDesc_t *newSubComponents;

  // New bindings structure
  struct BindingDesc_t *newBindings;

  // Temporary variables
  int i, err;
  // declare fItf as void* and cast it at usage to avoid strict-aliasing
  // warnings (see bug#307695)
  void *fItf;
  Rmemory_api_Allocator *allocator = self->factory_allocator;

  // Allocate memory for the new composite instance
  char *buff = (char *) CALL(allocator, alloc,
      sizeof(struct __cecilia_cloneableCompositeData_t)
      + self->defaultComposite.nbSubComponents * sizeof(struct SubComponentDesc_t)
      + self->defaultComposite.nbBindings * sizeof(struct BindingDesc_t));
  if (buff == NULL) {
#ifdef DEBUGCOMPOSITE
    printf("Unable to allocate new composite instance (this=%p).\n",
        _this);
#endif // DEBUGCOMPOSITE
    // TODO deallocate previously allocated memory.
    return fractal_api_ErrorConst_INSTANTIATION_ERROR;
  }

  newComp = (struct __cecilia_cloneableCompositeData_t *) buff;
  newSubComponents = (struct SubComponentDesc_t *) (buff
      + sizeof(struct __cecilia_cloneableCompositeData_t));
  newBindings = (struct BindingDesc_t *) (buff
      + sizeof(struct __cecilia_cloneableCompositeData_t)
      + self->defaultComposite.nbSubComponents * sizeof(struct SubComponentDesc_t));

  // Copy the content of my instance to the new one to have the default informations
  memcpy(newComp, self, sizeof(struct __cecilia_cloneableCompositeData_t));
  memcpy(newSubComponents, self->defaultComposite.subComponents,
      (self->defaultComposite.nbSubComponents)
          *sizeof(struct SubComponentDesc_t));
  memcpy(newBindings, self->defaultComposite.bindings,
      (self->defaultComposite.nbBindings)*sizeof(struct BindingDesc_t));

  // update selfdata field of server interfaces.
  newComp->defaultComposite.myreference.selfdata = &(newComp->defaultComposite);
  newComp->defaultComposite.bc.selfdata = &(newComp->defaultComposite);
  newComp->defaultComposite.lcc.selfdata = &(newComp->defaultComposite);
  newComp->defaultComposite.cc.selfdata = &(newComp->defaultComposite);
  newComp->factory.selfdata = newComp;

  newComp->defaultComposite.started = 0;
  newComp->defaultComposite.subComponents = newSubComponents;
  newComp->defaultComposite.bindings = newBindings;

  // for each sub component.
  for (i = 0; i < self->defaultComposite.nbSubComponents; i++) {
    // get the "factory" interface.
    err = CALL(self->defaultComposite.subComponents[i].instance,
        getFcInterface, "factory", &fItf);
    if (err != fractal_api_ErrorConst_OK) {
#ifdef DEBUGCOMPOSITE
      printf("Unable to get \"factory\" interface of sub component %d (this=%p).\n", i,
          _this);
#endif // DEBUGCOMPOSITE
      // TODO deallocate previously allocated memory.
      return fractal_api_ErrorConst_INSTANTIATION_ERROR;
    }

    // clone the sub component.
    err = CALL((Rfractal_api_Factory *)fItf, newFcInstance,
        &(newComp->defaultComposite.subComponents[i].instance));
    if (err != fractal_api_ErrorConst_OK) {
#ifdef DEBUGCOMPOSITE
      printf("Unable to instantiate sub component %d (this=%p).\n", i,
          _this);
#endif // DEBUGCOMPOSITE
      // TODO deallocate previously allocated memory.
      return fractal_api_ErrorConst_INSTANTIATION_ERROR;
    }

    // update state of the sub component
    newComp->defaultComposite.subComponents[i].state
        = SUB_COMPONENT_NOT_INITIALIZED;
  }

  // for each binding.
  for (i = 0; i < newComp->defaultComposite.nbBindings; i++) {
    newComp->defaultComposite.bindings[i].state
        = BINDING_UNBOUND| (newComp->defaultComposite.bindings[i].state
            & (~BINDING_STATUS_MASK));
  }

  *instance = &(newComp->defaultComposite.myreference);
  return fractal_api_ErrorConst_OK;
}

/**
 * Implementation of the destroyFcInstance method.
 */
// int destroyFcInstance(in Component instance)
static int destroyFcInstance(void *_this, Rfractal_api_Component* instance)
{
  struct __cecilia_cloneableCompositeData_t
      *self = (struct __cecilia_cloneableCompositeData_t *) _this;

  struct __cecilia_defaultCompositeData_t* victim = instance->selfdata;
  Rmemory_api_Allocator *allocator = self->factory_allocator;
  // declare fItf as void* and cast it at usage to avoid strict-aliasing
  // warnings (see bug#307695)
  void *fItf;
  int i, err;

  // for each sub component
  for (i = 0; i < victim->nbSubComponents; i++) {
    // get the "factory" interface.
    err = CALL(self->defaultComposite.subComponents[i].instance,
        getFcInterface, "factory", &fItf);
    if (err != fractal_api_ErrorConst_OK) {
#ifdef DEBUGCOMPOSITE
      printf("Unable to get \"factory\" interface of sub component %d (this=%p).\n", i,
          _this);
#endif // DEBUGCOMPOSITE
      return fractal_api_ErrorConst_INSTANTIATION_ERROR;
    }

    // destroy the sub component.
    err = CALL((Rfractal_api_Factory *)fItf, destroyFcInstance,
        victim->subComponents[i].instance);
    if (err != fractal_api_ErrorConst_OK) {
#ifdef DEBUGCOMPOSITE
      printf("Unable to destroy sub component %d (this=%p).\n", i,
          _this);
#endif // DEBUGCOMPOSITE
      return fractal_api_ErrorConst_INSTANTIATION_ERROR;
    }
  }
  // Then free the memory space that was allocated for this component
  CALL(allocator, free, (void *)instance);

  return fractal_api_ErrorConst_OK;
}

struct Mfractal_api_Factory __cecilia_cloneableComposite_FactoryMeths = {
    newFcInstance, destroyFcInstance };
