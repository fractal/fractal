/**
 * Cecilia, an implementation of the Fractal component model in C.
 *
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Matthieu Leclercq.
 */

#ifndef FRACTAL_API_CLONEABLECOMPOSITE_H
#define FRACTAL_API_CLONEABLECOMPOSITE_H

#ifndef IN_CECILIA_MECHANISM
#error You must not include this file in standard component
#endif

// include generated header file from IDL fractal.api.Factory
#include <fractal/api/Factory.idl.h>
// include generated header file from IDL memory.api.Allocator
#include <memory/api/Allocator.idl.h>

#include <fractal/api/defaultComposite.h>

struct __cecilia_cloneableCompositeData_t {
  struct __cecilia_defaultCompositeData_t defaultComposite;
  Rfractal_api_Factory factory;
  Rmemory_api_Allocator* factory_allocator;
};

extern struct Mfractal_api_Component
    __cecilia_cloneableComposite_ComponentMeths;
extern struct Mfractal_api_BindingController
    __cecilia_cloneableComposite_BindingControllerMeths;
extern struct Mfractal_api_Factory
    __cecilia_cloneableComposite_FactoryMeths;

/**
 * Macro used by generated code to initialize cloneable composite instance.
 * This macro supposes that the array containing descriptions of sub-component
 * is called <instanceName>_subComponents
 * Similarly, This macro supposes that the array containing descriptions of
 * binding is called <instanceName>_bindings
 */
#define CLONEABLE_COMPOSITE_INSTANCE(instanceName) \
  { \
	{ \
      { &__cecilia_cloneableComposite_ComponentMeths, &instanceName.defaultComposite}, \
      { &__cecilia_cloneableComposite_BindingControllerMeths, &instanceName.defaultComposite}, \
      { &__cecilia_defaultComposite_LifeCycleControllerMeths, &instanceName.defaultComposite}, \
      { &__cecilia_defaultComposite_ContentControllerMeths, &instanceName.defaultComposite}, \
      sizeof(instanceName##_subComponents) / sizeof(instanceName##_subComponents[0]), \
      sizeof(instanceName##_bindings) / sizeof(instanceName##_bindings[0]), \
      0, \
      instanceName##_subComponents, \
      instanceName##_bindings \
    }, \
    {&__cecilia_cloneableComposite_FactoryMeths, &instanceName}, \
    0 \
  }


#endif // FRACTAL_API_CLONEABLECOMPOSITE_H
