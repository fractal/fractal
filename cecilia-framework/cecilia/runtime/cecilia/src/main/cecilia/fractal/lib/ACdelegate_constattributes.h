#ifndef TEST_ATTRIBUTE_ACDELEGATE_CONSTATTRIBUTES_H
#define TEST_ATTRIBUTE_ACDELEGATE_CONSTATTRIBUTES_H

#include "fractal/api/AttributeController.idl.h"

/** Descriptor of an attribute that may be constant. */
struct ConstAttributeDesc_t {
  /** the name of the client interface. */
  const char *name;
  /** Pointer to the attribute. */
  const void *value;
  /** The size of the attribute. */
  char size;
  /** True if the attribute is constant. */
  jboolean isConst;
};

/** definition of the type of the '_this' parameter. */
struct ConstACData_t {
    
  /** Number of all the attributes. */
  unsigned int nbAttribute;

  /**
   * Array of 'ConstAttributeDesc_t'. The actual structure pointed by '_this'
   * contains 'nbAttribute' elements.
   */
  struct ConstAttributeDesc_t attributes[1];
};

/** Defines an ConstACData_t structure. */
#define DEFINE_CONST_AC_DATA_TYPE(comp_name, nbAttr) \
  struct comp_name##_AC_data_type { \
    unsigned int nbAttribute; \
    struct ConstAttributeDesc_t attributes[nbAttr]; \
  }

#endif // TEST_ATTRIBUTE_ACDELEGATE_CONSTATTRIBUTES_H
