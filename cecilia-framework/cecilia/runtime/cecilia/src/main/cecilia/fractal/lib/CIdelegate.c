/**
 * Cecilia, an implementation of the Fractal component model in C.
 * 
 * Copyright (C) 2004 France Telecom R&D
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: J.-P. Fassino, Matthieu Leclercq.
 */

// include generated header file from IDL fractal.api.Component
#include "fractal/api/Component.idl.h"

#include <string.h>
int printf(const char * format, ...);

/** Descriptor of a server interface. */
struct ServerInterfaceDesc_t {
  /** the name of the server interface. */
  const char *name;
  /** Pointer to the server interface. */
  void *itf;
};

/** Generic definition of the type of the '_this' parameter. */
struct GenericCIData_t {
  /** Number of server interfaces. */
  unsigned int nbServerInterface;

  /**
   * Array of 'ServerInterfaceDesc_t'. The actual structure pointed by '_this'
   * contains 'nbServerInterface' elements.
   */
  struct ServerInterfaceDesc_t serverInterfaces[1];
};

/**
 * Implementation of the getFcInterface method.
 */
// int getFcInterface(in string interfaceName, out any interfaceReference);
static int getFcInterface(void* _this, const char* interfaceName,
    void ** interfaceReference)
{
  struct GenericCIData_t *self =(struct GenericCIData_t *) _this;
  unsigned int i;

  // check argument
  if (interfaceReference == NULL) {
    return fractal_api_ErrorConst_INVALID_ARGUMENT;
  }

  // search an interface with the correct name.
  for (i = 0; i < self->nbServerInterface; i++) {
    if(strcmp(self->serverInterfaces[i].name, interfaceName) == 0) {
      *interfaceReference = self->serverInterfaces[i].itf;
      return fractal_api_ErrorConst_OK;
    }
  }
  return fractal_api_ErrorConst_NO_SUCH_INTERFACE;
};

/**
 * Implementation of the getFcInterfaces method.
 */
// int getFcInterfaces(in any[] interfaceReferences);
static int getFcInterfaces(void* _this, void* interfaceReferences[])
{
  struct GenericCIData_t *self =(struct GenericCIData_t *) _this;
  unsigned int i;

  if (interfaceReferences != NULL) {
    for (i = 0; i < self->nbServerInterface; i++) {
      interfaceReferences[i] = self->serverInterfaces[i].itf;
    }
  }
  return self->nbServerInterface;
}

/** Declaration of the V-Table of this interface implementation. */
struct Mfractal_api_Component __cecilia_default_ComponentMeths = {
    getFcInterface, getFcInterfaces };
