#ifndef FRACTAL_LIB_ACDELEGATE_H
#define FRACTAL_LIB_ACDELEGATE_H

#include "fractal/api/AttributeController.idl.h"

/** Descriptor of an attribute. */
struct AttributeDesc_t {
  /** the name of the client interface. */
  const char *name;
  /** Pointer to the attribute. */
  void *value;
  /** The size of the attribute. */
  char size;
};

/** Generic definition of the type of the '_this' parameter. */
struct GenericACData_t {
  /** Number of attribute. */
  unsigned int nbAttribute;

  /**
   * Array of 'AttributeDesc_t'. The actual structure pointed by '_this'
   * contains 'nbAttribute' elements.
   */
  struct AttributeDesc_t attributes[1];
};

/** Defines an ACData_t structure. */
#define DEFINE_AC_DATA_TYPE(comp_name, nbAttr) \
  struct comp_name##_AC_data_type { \
    unsigned int nbAttribute; \
    struct AttributeDesc_t attributes[nbAttr]; \
  }

#endif // TEST_ATTRIBUTE_ACDELEGATE_H
