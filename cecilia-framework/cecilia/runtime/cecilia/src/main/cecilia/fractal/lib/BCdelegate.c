/**
 * Cecilia, an implementation of the Fractal component model in C.
 * 
 * Copyright (C) 2004 France Telecom R&D
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: J.-P. Fassino, Matthieu Leclercq.
 */

// include generated header file from IDL fractal.api.BindingController
#include "fractal/lib/BCdelegate.h"

#include <string.h>

/**
 * Implementation of the listFc method.
 */
// int listFc(in string[] clientItfNames);
static int listFc(void* _this, const char* clientItfNames[])
{
  struct GenericBCData_t *self =(struct GenericBCData_t *) _this;
  unsigned int i;

  if (clientItfNames != NULL) {
    for(i = 0; i < self->nbClientInterface; i++) {
      clientItfNames[i] = self->clientInterfaces[i].name;
    }
  }
  return self->nbClientInterface;
}

/**
 * Implementation of the lookupFc method.
 */
// int lookupFc(in string clientItfName, out any interfaceReference);
static int lookupFc(void* _this, const char* clientItfName, void** interfaceReference)
{
  struct GenericBCData_t *self =(struct GenericBCData_t *) _this;
  unsigned int i;

  if (interfaceReference == NULL || clientItfName == NULL) {
    return fractal_api_ErrorConst_INVALID_ARGUMENT;
  }

  for(i = 0; i < self->nbClientInterface; i++) {
    if(strcmp(self->clientInterfaces[i].name, clientItfName) == 0) {
      *interfaceReference = *(self->clientInterfaces[i].itf);
      return fractal_api_ErrorConst_OK;
    }
  }
  return fractal_api_ErrorConst_NO_SUCH_INTERFACE;
}

/**
 * Implementation of the bindFc method.
 */
// int bindFc(in string clientItfName, in any serverItf);
static int bindFc(void* _this, const char* clientItfName, void* serverItf)
{
  struct GenericBCData_t *self =(struct GenericBCData_t *) _this;
  unsigned int i;

  if (clientItfName == NULL) {
    return fractal_api_ErrorConst_INVALID_ARGUMENT;
  }

  for(i = 0; i < self->nbClientInterface; i++) {
    if(strcmp(self->clientInterfaces[i].name, clientItfName) == 0) {
      *(self->clientInterfaces[i].itf) = serverItf;
      return fractal_api_ErrorConst_OK;
    }
  }
  return fractal_api_ErrorConst_NO_SUCH_INTERFACE;
}

/**
 * Implementation of the unbindFc method.
 */
// int unbindFc(in string clientItfName);
static int unbindFc(void* _this, const char* clientItfName)
{
  return bindFc(_this, clientItfName, NULL);
}

struct Mfractal_api_BindingController
    __cecilia_default_BindingControllerMeths = { listFc, lookupFc, bindFc,
        unbindFc };

