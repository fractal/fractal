#ifndef FRACTAL_LIB_BCDELEGATE_H
#define FRACTAL_LIB_BCDELEGATE_H

#include "fractal/api/BindingController.idl.h"

/** Descriptor of a server interface. */
struct ClientInterfaceDesc_t {
  /** the name of the client interface. */
  const char *name;
  /** Pointer to the client interface. */
  void **itf;
};

/** Generic definition of the type of the '_this' parameter. */
struct GenericBCData_t {
  /** Number of client interfaces. */
  unsigned int nbClientInterface;

  /**
   * Array of 'ClientInterfaceDesc_t'. The actual structure pointed by '_this'
   * contains 'nbClientInterface' elements.
   */
  struct ClientInterfaceDesc_t clientInterfaces[1];
};

#endif // FRACTAL_LIB_BCDELEGATE_H

