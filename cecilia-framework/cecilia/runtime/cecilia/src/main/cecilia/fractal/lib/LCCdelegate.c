/**
 * Cecilia, an implementation of the Fractal component model in C.
 * 
 * Copyright (C) 2004 France Telecom R&D
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: J.-P. Fassino, Matthieu Leclercq.
 */

// include generated header file from IDL fractal.api.LifeCycleController
#include "fractal/api/LifeCycleController.idl.h"
#include <cecilia.h>

#include <string.h>
int printf(const char * format, ...);

struct GenericLCCData_t {
  int state;
  Rfractal_api_LifeCycleController *underlyingLCC;
};

/**
 * Implementation of the getFcState method.
 */
// int getFcState();
static int getFcState(void* _this)
{
  struct GenericLCCData_t* self = (struct GenericLCCData_t *) _this;
  if (self->state == 0)
    return fractal_api_LifeCycleController_STOPPED;
  else
    return fractal_api_LifeCycleController_STARTED;
}

/**
 * Implementation of the startFc method.
 */
// int startFc();
static int startFc(void* _this)
{
  struct GenericLCCData_t* self = (struct GenericLCCData_t *) _this;
  if (self->state == 0) {
    int ret = CALL(self->underlyingLCC, startFc);
    if (ret != fractal_api_ErrorConst_OK) {
      return ret;
    } else {
      self->state = 1;
    }
  }
  return fractal_api_ErrorConst_OK;
}

/**
 * Implementation of the stopFc method.
 */
// int stopFc();
static int stopFc(void* _this)
{
  struct GenericLCCData_t* self = (struct GenericLCCData_t *) _this;
  if (self->state != 0) {
    int ret = CALL(self->underlyingLCC, stopFc);
    if (ret != fractal_api_ErrorConst_OK) {
      return ret;
    } else {
      self->state = 0;
    }
  }
  return fractal_api_ErrorConst_OK;
}

struct Mfractal_api_LifeCycleController
    __cecilia_default_LifeCycleControllerMeths = { getFcState, startFc, stopFc };
