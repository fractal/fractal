/**
 * Cecilia, an implementation of the Fractal component model in C.
 *
 * Copyright (C) 2004 France Telecom R&D
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: J.-P. Fassino, Matthieu Leclercq.
 */

#define IN_CECILIA_MECHANISM
#include "fractal/api/defaultComposite.h"
#include <cecilia.h>

// TODO add printf for DEBUG_COMPOSITE in composite start.

#include <string.h>

extern int printf(const char * format, ...);

// checks a binding descriptor
static int checkBinding(struct __cecilia_defaultCompositeData_t *self,
    struct BindingDesc_t* bindingDesc);

// starts the given sub component.
static int startSubComp(struct __cecilia_defaultCompositeData_t *self,
    int subCompIdx);

// stops the given sub component.
static int stopSubComp(struct __cecilia_defaultCompositeData_t *self,
    int subCompIdx);

// Instantiates the given binding descriptor
static int bind(struct __cecilia_defaultCompositeData_t *self,
    struct BindingDesc_t* bindingDesc);

// get a interface of a sub component.
static int getItf(struct __cecilia_defaultCompositeData_t *self, int idx,
    const char* name, void **itf)
{
  int err = CALL(self->subComponents[idx].instance, getFcInterface, name, itf);

#ifdef DEBUGCOMPOSITE
  if (err != 0)
    printf("An error occurs while retrieving interface \"%s\" of component \"%s\": err=%d\n",
        name, self->subComponents[idx].name, err);
#endif // DEBUGCOMPOSITE

  return err;
}

static int getItf_no_error(struct __cecilia_defaultCompositeData_t *self, int idx,
    const char* name, void **itf)
{
  return CALL(self->subComponents[idx].instance, getFcInterface, name, itf);
}

// -----------------------------------------------------------------------------
// Implementation of the component interface.
// -----------------------------------------------------------------------------

/**
 * Implementation of the getFcInterface method.
 */
// int getFcInterface(in string interfaceName, out any interfaceReference);
static int getFcInterface(void* _this, const char* interfaceName,
    void ** interfaceReference)
{
  struct __cecilia_defaultCompositeData_t
      *self = (struct __cecilia_defaultCompositeData_t *) _this;

  int i;
  if (strcmp(interfaceName, "component") == 0) {
    *interfaceReference = &(self->myreference);
    return fractal_api_ErrorConst_OK;
  } else if (strcmp(interfaceName, "binding-controller") == 0) {
    *interfaceReference = &(self->bc);
    return fractal_api_ErrorConst_OK;
  } else if (strcmp(interfaceName, "lifecycle-controller") == 0) {
    *interfaceReference = &(self->lcc);
    return fractal_api_ErrorConst_OK;
  } else if (strcmp(interfaceName, "content-controller") == 0) {
    *interfaceReference = &(self->cc);
    return fractal_api_ErrorConst_OK;
  }

  // Getting exported interfaces owned by sub-components
  for (i = 0; i < self->nbBindings; i++) {
    if ((self->bindings[i].client == COMPOSITE_IDX)&& (strcmp(interfaceName,
        self->bindings[i].clientName) == 0)) {
      return getItf(self, self->bindings[i].server,
          self->bindings[i].serverName, interfaceReference);
    }
  }

  return fractal_api_ErrorConst_NO_SUCH_INTERFACE;
}

/**
 * Implementation of the getFcInterfaces method.
 */
// int getFcInterfaces(in any[] interfaceReferences);
static int getFcInterfaces(void* _this, void* interfaceReferences[])
{
  struct __cecilia_defaultCompositeData_t
      *self = (struct __cecilia_defaultCompositeData_t *) _this;
  int i, err;
  int nbItf = 4; // there is at least 4 control interfaces.

  if (interfaceReferences != NULL) {
    interfaceReferences[0] = &self->myreference;
    interfaceReferences[1] = &self->bc;
    interfaceReferences[2] = &self->lcc;
    interfaceReferences[3] = &self->cc;

    for (i = 0; i < self->nbBindings; i++) {
      if (self->bindings[i].client == COMPOSITE_IDX) {
        err = getItf(self, self->bindings[i].server,
            self->bindings[i].serverName, &(interfaceReferences[nbItf]));
        if (err != fractal_api_ErrorConst_OK)
          return err;
        nbItf ++;
      }
    }
  } else { // interfaceReferences == NULL
    for (i = 0; i < self->nbBindings; i++) {
      if (self->bindings[i].client == COMPOSITE_IDX) {
        nbItf ++;
      }
    }
  }

  return nbItf;
}

/** Declaration of the V-Table of this interface implementation. */
struct Mfractal_api_Component __cecilia_defaultComposite_ComponentMeths = {
    getFcInterface, getFcInterfaces };

// -----------------------------------------------------------------------------
// Implementation of the binding-controller interface.
// -----------------------------------------------------------------------------

/**
 * Implementation of the listFc method.
 */
// int listFc(in string[] clientItfNames);
static int listFc(void* _this, const char* clientItfNames[])
{
  struct __cecilia_defaultCompositeData_t
      *self = (struct __cecilia_defaultCompositeData_t *) _this;
  int i;
  int nbItf = 0;

  if (clientItfNames != NULL) {
    for (i = 0; i < self->nbBindings; i++) {
      if (self->bindings[i].server == COMPOSITE_IDX) {
        clientItfNames[nbItf] = self->bindings[i].serverName;
        nbItf++;
      }
    }
  } else { // clientItfNames == NULL
    for (i = 0; i < self->nbBindings; i++) {
      if (self->bindings[i].server == COMPOSITE_IDX) {
        nbItf++;
      }
    }
  }

  return nbItf;
}

/**
 * Implementation of the lookupFc method.
 */
// int lookupFc(in string clientItfName, out any interfaceReference);
static int lookupFc(void* _this, const char* clientItfName, void** interfaceReference)
{
  struct __cecilia_defaultCompositeData_t
      *self = (struct __cecilia_defaultCompositeData_t *) _this;
  int i, err;

  if (interfaceReference == NULL || clientItfName == NULL) {
#ifdef DEBUGCOMPOSITE
    printf("Invalid arguments at %s:%d", __FILE__, __LINE__);
#endif // DEBUGCOMPOSITE
    return fractal_api_ErrorConst_INVALID_ARGUMENT;
  }

  for (i = 0; i < self->nbBindings; i++) {
    if (self->bindings[i].server == COMPOSITE_IDX) {
      if (GET_BINDING_KIND(self->bindings[i].state) == STANDARD_BINDING) {
        if (strcmp(clientItfName, self->bindings[i].serverName) == 0) {
          // declare subBC as void* and cast it at usage to avoid strict-aliasing
          // warnings (see bug#307695)
          void *subBC;
          err = getItf(self, self->bindings[i].client, "binding-controller", &subBC);
          if (err != fractal_api_ErrorConst_OK)
            return err;
          // TODO remove cast to char* (see bug #307668)
          return CALL((fractal_api_BindingController)subBC, lookupFc,
              (char *) self->bindings[i].clientName, interfaceReference);
        }
      } else { // GET_BINDING_KIND(self->bindings[i].state) == EXP_COL_BINDING
        if (strncmp(clientItfName, self->bindings[i].serverName,
                strlen(self->bindings[i].serverName)) == 0) {
          // declare subBC as void* and cast it at usage to avoid strict-aliasing
          // warnings (see bug#307695)
          void *subBC;
          err= getItf(self, self->bindings[i].client, "binding-controller", &subBC);
          if (err != fractal_api_ErrorConst_OK)
            return err;

          // assumes that the name of the client interface of the sub component
          // has the same name as the client interface of this composite
          // component.
          return CALL((fractal_api_BindingController)subBC, lookupFc,
        		  clientItfName, interfaceReference);
        }
      }
    }
  }

  return fractal_api_ErrorConst_NO_SUCH_INTERFACE;
}

/**
 * Implementation of the bindFc method.
 */
// int bindFc(in string clientItfName, in any serverItf);
static int bindFc(void* _this, const char* clientItfName, void* serverItf)
{
  struct __cecilia_defaultCompositeData_t
      *self = (struct __cecilia_defaultCompositeData_t *) _this;
  int i, err;
  int result = fractal_api_ErrorConst_NO_SUCH_INTERFACE;

  if (clientItfName == NULL) {
#ifdef DEBUGCOMPOSITE
    printf("Invalid arguments at %s:%d", __FILE__, __LINE__);
#endif // DEBUGCOMPOSITE
    return fractal_api_ErrorConst_INVALID_ARGUMENT;
  }

  for (i = 0; i < self->nbBindings; i++) {
    if (self->bindings[i].server == COMPOSITE_IDX) {
      if (GET_BINDING_KIND(self->bindings[i].state) == STANDARD_BINDING) {
        if (strcmp(clientItfName, self->bindings[i].serverName) == 0) {
          // declare subBC as void* and cast it at usage to avoid strict-aliasing
          // warnings (see bug#307695)
          void *subBC;
          err= getItf(self, self->bindings[i].client, "binding-controller", &subBC);
          if (err != fractal_api_ErrorConst_OK)
            return err;

          err = CALL((fractal_api_BindingController)subBC, bindFc,
              self->bindings[i].clientName, serverItf);
          if (err != fractal_api_ErrorConst_OK) {
#ifdef DEBUGCOMPOSITE
            printf("An error occurs while binding interface \"%s\" of sub-component \"%s\": err=%d",
                self->bindings[i].clientName,
                self->subComponents[self->bindings[i].client].name,
                err);
#endif // DEBUGCOMPOSITE
            return err;
          }

          result = fractal_api_ErrorConst_OK;
        }
      } else { // GET_BINDING_KIND(self->bindings[i].state) == EXP_COL_BINDING
        if (strncmp(clientItfName, self->bindings[i].serverName,
                strlen(self->bindings[i].serverName)) == 0) {
          // declare subBC as void* and cast it at usage to avoid strict-aliasing
          // warnings (see bug#307695)
          void *subBC;
          err= getItf(self, self->bindings[i].client, "binding-controller", &subBC);
          if (err != fractal_api_ErrorConst_OK)
            return err;

          // assumes that the name of the client interface of the sub component
          // has the same name as the client interface of this composite
          // component.
          err = CALL((fractal_api_BindingController)subBC, bindFc,
        		  clientItfName, serverItf);
          if (err != fractal_api_ErrorConst_OK) {
#ifdef DEBUGCOMPOSITE
            printf("An error occurs while binding interface \"%s\" of sub-component \"%s\": err=%d",
                clientItfName,
                self->subComponents[self->bindings[i].client].name,
                err);
#endif // DEBUGCOMPOSITE
            return err;
          }

          result = fractal_api_ErrorConst_OK;
        }
      }
    }
  }

  return result;
}

/**
 * Implementation of the unbindFc method.
 */
// int unbindFc(in string clientItfName);
static int unbindFc(void* _this, const char* clientItfName)
{
  return bindFc(_this, clientItfName, NULL);
}

struct Mfractal_api_BindingController
    __cecilia_defaultComposite_BindingControllerMeths = { listFc, lookupFc,
        bindFc, unbindFc };

// -----------------------------------------------------------------------------
// Implementation of the lifecycle-controller interface.
// -----------------------------------------------------------------------------

/**
 * Implementation of the getFcState method.
 */
// int getFcState();
static int getFcState(void* _this)
{
  struct __cecilia_defaultCompositeData_t
      *self = (struct __cecilia_defaultCompositeData_t *) _this;
  if (self->started == 0)
    return fractal_api_LifeCycleController_STOPPED;

  return fractal_api_LifeCycleController_STARTED;
}

// This bit is set on a component if has at least one "usedAtStart" client
// interface.
#define SUB_COMPONENT_HAS_UAS ((unsigned char) 0x10)

/**
 * Implementation of the startFc method.
 */
// int startFc();
static int startFc(void* _this)
{
  struct __cecilia_defaultCompositeData_t
      *self = (struct __cecilia_defaultCompositeData_t *) _this;
  int i, err;

  // if already started, simply return
  if (self->started != 0) {
    return fractal_api_ErrorConst_OK;
  }

  // first create each binding
  for (i = 0; i < self->nbBindings; i++) {

    // check binding
    err = checkBinding(self, &(self->bindings[i]));
    if (err != fractal_api_ErrorConst_OK) {
      return err;
    }

    // if it is an import or an export binding, pass it.
    if (self->bindings[i].client == COMPOSITE_IDX|| self->bindings[i].server
        == COMPOSITE_IDX)
      continue;

    // if the binding is already done, pass it.
    if (GET_BINDING_STATUS(self->bindings[i].state) == BINDING_BOUND) {
      continue;
    }

    // perform the actual binding.
    err = bind(self, &(self->bindings[i]));
    if (err != fractal_api_ErrorConst_OK)
      return err;

  } // end for each binding.

  // there is no "usedAtStart" bindings.
  for (i = 0; i < self->nbSubComponents; i++) {
    err = startSubComp(self, i);
    if (err != fractal_api_ErrorConst_OK)
      return err;
  }

  self->started = 1;
  return fractal_api_ErrorConst_OK;

} // end startFc method


/**
 * Implementation of the stopFc method.
 */
// int stopFc();
static int stopFc(void* _this)
{
  struct __cecilia_defaultCompositeData_t
      *self = (struct __cecilia_defaultCompositeData_t *) _this;
  int i, err;

  for (i = 0; i < self->nbSubComponents; i++) {
    err = stopSubComp(self, i);
    if (err != fractal_api_ErrorConst_OK)
      return err;
  }

  self->started = 0;
  return fractal_api_ErrorConst_OK;
}

struct Mfractal_api_LifeCycleController
    __cecilia_defaultComposite_LifeCycleControllerMeths = { getFcState,
        startFc, stopFc };

// -----------------------------------------------------------------------------
// Implementation of the content-controller interface.
// -----------------------------------------------------------------------------

/**
 * Implementation of the getFcSubComponents method.
 */
// int getFcSubComponents(in Component[] subComponents)
static int getFcSubComponents(void *_this,
    Rfractal_api_Component* subComponents[])
{
  struct __cecilia_defaultCompositeData_t
      *self = (struct __cecilia_defaultCompositeData_t *) _this;
  int i;

  if (subComponents != NULL) {
    for (i = 0; i < self->nbSubComponents; i++) {
      subComponents[i] = self->subComponents[i].instance;
    }
  }

  return self->nbSubComponents;
}

/**
 * Implementation of the addFcSubComponent method (not supported).
 */
// int addFcSubComponent(in Component subComponent)
static int addFcSubComponent(void *_this, Rfractal_api_Component* subComponent)
{
#ifdef DEBUGCOMPOSITE
    printf("Operation not supported at %s:%d", __FILE__, __LINE__);
#endif // DEBUGCOMPOSITE
  return fractal_api_ErrorConst_OPERATION_NOT_SUPPORTED;
}

/**
 * Implementation of the removeFcSubComponent method (not supported).
 */
// int removeFcSubComponent(in Component subComponent)
static int removeFcSubComponent(void *_this,
    Rfractal_api_Component* subComponent)
{
#ifdef DEBUGCOMPOSITE
    printf("Operation not supported at %s:%d", __FILE__, __LINE__);
#endif // DEBUGCOMPOSITE
  return fractal_api_ErrorConst_OPERATION_NOT_SUPPORTED;
}

/**
 * Implementation of the addFcSubBinding method (not supported).
 */
// int addFcSubBinding(in Component clientComponent, in string clientItfName,
//     in Component serverComponent, in string serverItfName)
static int addFcSubBinding(void *_this,
    Rfractal_api_Component* clientComponent, const char* clientItfName,
    Rfractal_api_Component* serverComponent, const char* serverItfName)
{
#ifdef DEBUGCOMPOSITE
    printf("Operation not supported at %s:%d", __FILE__, __LINE__);
#endif // DEBUGCOMPOSITE
  return fractal_api_ErrorConst_OPERATION_NOT_SUPPORTED;
}

/**
 * Implementation of the removeFcSubBinding method (not supported).
 */
// int removeFcSubBinding(in Component clientComponent, in string clientItfName)
static int removeFcSubBinding(void *_this,
    Rfractal_api_Component* clientComponent, const char* clientItfName)
{
#ifdef DEBUGCOMPOSITE
    printf("Operation not supported at %s:%d", __FILE__, __LINE__);
#endif // DEBUGCOMPOSITE
  return fractal_api_ErrorConst_OPERATION_NOT_SUPPORTED;
}

struct Mfractal_api_ContentController
    __cecilia_defaultComposite_ContentControllerMeths = { getFcSubComponents,
        addFcSubComponent, removeFcSubComponent, addFcSubBinding,
        removeFcSubBinding };

// -----------------------------------------------------------------------------
// [start/stop]Subcomp utility method implementation
// -----------------------------------------------------------------------------

#ifdef FULLDEBUGCOMPOSITE
// this static variable is used for pretty printing start/stop debug messages.
static int indent_level = 0;
#endif // FULLDEBUGCOMPOSITE

static int startSubComp(struct __cecilia_defaultCompositeData_t *self,
    int subCompIdx)
{
  int err;
  // declare lcc as void* and cast it at usage to avoid strict-aliasing
  // warnings (see bug#307695)
  void *lcc;

#ifdef FULLDEBUGCOMPOSITE
  {
    int i;
    for (i = 0; i < indent_level; i++) printf("  ");
    printf("Starting sub-component \"%s\"\n",
        self->subComponents[subCompIdx].name);
    indent_level ++;
  }
#endif // FULLDEBUGCOMPOSITE

  if (GET_SUB_COMPONENT_STATUS(self->subComponents[subCompIdx].state)
      != SUB_COMPONENT_STARTED) {
    err = getItf_no_error(self, subCompIdx, "lifecycle-controller", &lcc);
    if (err != fractal_api_ErrorConst_NO_SUCH_INTERFACE) {
      if (err != fractal_api_ErrorConst_OK) {
#ifdef DEBUGCOMPOSITE
        printf("Can't start sub component \"%s\": an error occurs while retrieving the \"lifecycle-controller\" interface err=%d\n",
            self->subComponents[subCompIdx].name, err);
#endif // DEBUGCOMPOSITE
        return err;
      }

      // call startFc on the sub component.
      err = CALL((Rfractal_api_LifeCycleController *)lcc, startFc);
      if (err != fractal_api_ErrorConst_OK) {
#ifdef DEBUGCOMPOSITE
        printf("An error occurs while starting sub component \"%s\": err=%d\n",
            self->subComponents[subCompIdx].name, err);
#endif // DEBUGCOMPOSITE
        return err;
      }
    }
    self->subComponents[subCompIdx].state = SUB_COMPONENT_STARTED
        |(self->subComponents[subCompIdx].state & (~SUB_COMPONENT_STATUS_MASK));
  }

#ifdef FULLDEBUGCOMPOSITE
  {
    int i;
    indent_level --;
    for (i = 0; i < indent_level; i++) printf("  ");
    printf("OK\n");
  }
#endif // FULLDEBUGCOMPOSITE

  return fractal_api_ErrorConst_OK;
}

static int stopSubComp(struct __cecilia_defaultCompositeData_t *self,
    int subCompIdx)
{
  int err;
  // declare lcc as void* and cast it at usage to avoid strict-aliasing
  // warnings (see bug#307695)
  void *lcc;

#ifdef FULLDEBUGCOMPOSITE
  {
    int i;
    for (i = 0; i < indent_level; i++) printf("  ");
    printf("Stopping sub-component \"%s\" ...\n",
        self->subComponents[subCompIdx].name);
  }
#endif // FULLDEBUGCOMPOSITE

  if (GET_SUB_COMPONENT_STATUS(self->subComponents[subCompIdx].state)
      != SUB_COMPONENT_STOPPED) {
    err = getItf_no_error(self, subCompIdx, "lifecycle-controller", &lcc);
    if (err != fractal_api_ErrorConst_NO_SUCH_INTERFACE) {
      if (err != fractal_api_ErrorConst_OK) {
#ifdef DEBUGCOMPOSITE
        printf("Can't stop sub component \"%s\": an error occurs while retrieving the \"lifecycle-controller\" interface err=%d\n",
            self->subComponents[subCompIdx].name, err);
#endif // DEBUGCOMPOSITE
        return err;
      }

      // call startFc on the sub component.
      err = CALL((Rfractal_api_LifeCycleController *)lcc, stopFc);
      if (err != fractal_api_ErrorConst_OK) {
#ifdef DEBUGCOMPOSITE
        printf("An error occurs while stopping sub component \"%s\": err=%d\n",
            self->subComponents[subCompIdx].name, err);
#endif // DEBUGCOMPOSITE
        return err;
      }
    }
    self->subComponents[subCompIdx].state = SUB_COMPONENT_STOPPED
        |(self->subComponents[subCompIdx].state & (~SUB_COMPONENT_STATUS_MASK));
  }

#ifdef FULLDEBUGCOMPOSITE
  {
    int i;
    for (i = 0; i < indent_level; i++) printf("  ");
    printf("OK\n");
  }
#endif // FULLDEBUGCOMPOSITE

  return fractal_api_ErrorConst_OK;
}

// -----------------------------------------------------------------------------
// bind utility method implementation
// -----------------------------------------------------------------------------

static int bind(struct __cecilia_defaultCompositeData_t *self,
    struct BindingDesc_t* bindingDesc)
{
  // declare clientBc as void* and cast it at usage to avoid strict-aliasing
  // warnings (see bug#307695)
  void* clientBc;
  void* serverItf;
  int err;

  // First step, get the server interface
  err = getItf(self, bindingDesc->server, bindingDesc->serverName, &serverItf);
  if (err != fractal_api_ErrorConst_OK)
    return err;

  // Next get the BindingController interface of the client component
  err = getItf(self, bindingDesc->client, "binding-controller", &clientBc);
  if (err != fractal_api_ErrorConst_OK)
    return err;

  // Effective binding
  // TODO remove cast to char* (see bug #307668)
  err = CALL((Rfractal_api_BindingController *)clientBc, bindFc,
      (char *) bindingDesc->clientName, serverItf);
  if (err != fractal_api_ErrorConst_OK) {
#ifdef DEBUGCOMPOSITE
    printf("An error occurs while binding client inteface \"%s\" of component \"%s\" to server interface \"%s\" of component \"%s\": err=%d\n",
        bindingDesc->clientName, self->subComponents[bindingDesc->client].name,
        bindingDesc->serverName, self->subComponents[bindingDesc->server].name,
        err);
#endif // DEBUGCOMPOSITE
    return err;
  }

  return fractal_api_ErrorConst_OK;
}

// -----------------------------------------------------------------------------
// checkBinding utility method implementation
// -----------------------------------------------------------------------------

static int checkBinding(struct __cecilia_defaultCompositeData_t *self,
    struct BindingDesc_t* bindingDesc)
{
#ifdef CHECKCOMPOSITE

  // check the server index
  if (bindingDesc->server != COMPOSITE_IDX
      && bindingDesc->server >= self->nbSubComponents) {
#ifdef DEBUGCOMPOSITE
    printf("Invalid binding descriptor: serverIdx=%d\n", bindingDesc->server);
#endif // DEBUGCOMPOSITE
    return fractal_api_ErrorConst_INVALID_ARGUMENT;
  }

  // check the client index
  if (bindingDesc->client != COMPOSITE_IDX
      && bindingDesc->client >= self->nbSubComponents) {
#ifdef DEBUGCOMPOSITE
    printf("Invalid binding descriptor: clientIdx=%d\n", bindingDesc->client);
#endif // DEBUGCOMPOSITE
    return fractal_api_ErrorConst_INVALID_ARGUMENT;
  }

  // check the serverName
  if (bindingDesc->serverName == NULL) {
#ifdef DEBUGCOMPOSITE
    printf("Invalid binding descriptor: serverName is NULL\n");
#endif // DEBUGCOMPOSITE
    return fractal_api_ErrorConst_INVALID_ARGUMENT;
  }

  // check the clientName
  if (bindingDesc->clientName == NULL) {
#ifdef DEBUGCOMPOSITE
    printf("Invalid binding descriptor: clientName is NULL\n");
#endif // DEBUGCOMPOSITE
    return fractal_api_ErrorConst_INVALID_ARGUMENT;
  }

#endif // CHECKCOMPOSITE
  return fractal_api_ErrorConst_OK;
}
