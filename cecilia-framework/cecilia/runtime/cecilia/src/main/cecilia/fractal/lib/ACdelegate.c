/**
 * Cecilia, an implementation of the Fractal component model in C.
 * 
 * Copyright (C) 2004 France Telecom R&D
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: J.-P. Fassino, Matthieu Leclercq.
 */

// include generated header file from IDL fractal.api.AttributeController
#include "fractal/api/AttributeController.idl.h"
#include "fractal/lib/ACdelegate.h"

#include <string.h>

/**
 * Implementation of the listFcAttributes method.
 */
// int listFcAttributes(in string[] attributeNames);
static int listFcAttributes(void* _this, const char* attributeNames[])
{
  struct GenericACData_t* self = (struct GenericACData_t*) _this;
  unsigned int i;

  if (attributeNames == NULL)
    return self->nbAttribute;
    
  for (i = 0; i < self->nbAttribute; i++) {
    attributeNames[i] = self->attributes[i].name;
  }
  return self->nbAttribute;
}

/**
 * Implementation of the getFcAttribute method.
 */
// int getFcAttribute(in string attributeName, out any value);
static int getFcAttribute(void* _this, const char* attributeName, void** value)
{
  struct GenericACData_t *self = (struct GenericACData_t*) _this;
  unsigned int i;

  if ((attributeName == NULL) || (value == NULL)) {
    return fractal_api_ErrorConst_INVALID_ARGUMENT;
  }

  for (i = 0; i < self->nbAttribute; i++) {
    if (strcmp(self->attributes[i].name, attributeName) == 0) {
      // copy attribute value into the out parameter.
      memcpy(value, self->attributes[i].value, self->attributes[i].size);
      return fractal_api_ErrorConst_OK;
    }
  }
  return fractal_api_ErrorConst_NO_SUCH_ATTRIBUTE;
}

/**
 * Implementation of the setFcAttribute method.
 */
// int setFcAttribute(in string attributeName, in any value);
static int setFcAttribute(void* _this, const char* attributeName, void* value)
{
  struct GenericACData_t *self = (struct GenericACData_t*) _this;
  unsigned int i;

  if (attributeName == NULL) {
    return fractal_api_ErrorConst_INVALID_ARGUMENT;
  }

  for (i = 0; i < self->nbAttribute; i++) {
    if (strcmp(self->attributes[i].name, attributeName) == 0) {
      // copy given value into attribute value. The value of the set
      // attribute is passed by value (not by reference) that why the "value" 
      // parameter is dereferenced (the "value" parameter does not point to the 
      // value to set, the "value" parameter contains the value to set).
      memcpy(self->attributes[i].value, &value, self->attributes[i].size);
      return fractal_api_ErrorConst_OK;
    }
  }
  return fractal_api_ErrorConst_NO_SUCH_ATTRIBUTE;
}

struct Mfractal_api_AttributeController
    __cecilia_default_AttributeControllerMeths = { listFcAttributes,
        getFcAttribute, setFcAttribute };
