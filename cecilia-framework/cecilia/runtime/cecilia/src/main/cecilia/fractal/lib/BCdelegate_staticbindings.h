#ifndef FRACTAL_LIB_BCDELEGATE_STATICBINDINGS_H
#define FRACTAL_LIB_BCDELEGATE_STATICBINDINGS_H

#include "fractal/api/BindingController.idl.h"

/** Descriptor of a server interface. */
struct StaticClientInterfaceDesc_t {
  /** the name of the client interface. */
  const char *name;
  /** Pointer to the client interface. */
  void **itf;
  /** True if the binding is static. */
  jboolean isStatic;
};

/** Generic definition of the type of the '_this' parameter. */
struct StaticBCData_t {
  /** Number of client interfaces. */
  unsigned int nbClientInterface;

  /**
   * Array of 'StaticClientInterfaceDesc_t'. The actual structure pointed by '_this'
   * contains 'nbClientInterface' elements.
   */
  struct StaticClientInterfaceDesc_t clientInterfaces[1];
};

#endif // FRACTAL_LIB_BCDELEGATE_STATICBINDINGS_H

