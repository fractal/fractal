/**
 * Cecilia, an implementation of the Fractal component model in C.
 *
 * Copyright (C) 2004 France Telecom R&D
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: J.-P. Fassino, Matthieu Leclercq.
 */

#ifndef FRACTAL_API_DEFAULTCOMPOSITE_H
#define FRACTAL_API_DEFAULTCOMPOSITE_H

#ifndef IN_CECILIA_MECHANISM
#error You must not include this file in standard component
#endif

// include generated header file from IDL fractal.api.Component
#include <fractal/api/Component.idl.h>

// include generated header file from IDL fractal.api.LifeCycleController
#include <fractal/api/LifeCycleController.idl.h>

// include generated header file from IDL fractal.api.BindingController
#include <fractal/api/BindingController.idl.h>

// include generated header file from IDL fractal.api.ContentController
#include <fractal/api/ContentController.idl.h>

// Three macros can be defined to enable checking and debug :
// if CHECKCOMPOSITE is defined, some sanity checks are performed.
// if DEBUGCOMPOSITE if defined, some debug information are printed.
// if FULLDEBUGCOMPOSITE is defined, many debug information are printed.

// It is not necessary to define more than one :
// if DEBUGCOMPOSITE is defined, then CHECKCOMPOSITE is also defined.
// if FULLDEBUGCOMPOSITE is defined, then DEBUGCOMPOSITE and CHECKCOMPOSITE are also defined.

#ifdef FULLDEBUGCOMPOSITE
#define DEBUGCOMPOSITE
#endif

#ifdef DEBUGCOMPOSITE
#define CHECKCOMPOSITE
#endif

/** The structure to hold sub components. */
struct SubComponentDesc_t {
  /** Pointer to the 'Component' interface of the sub component. */
  Rfractal_api_Component *instance;
  /** State of the sub component. */
  unsigned char state;

  // following fields are only defined if DEBUGCOMPOSITE is defined.
#ifdef DEBUGCOMPOSITE
  /** The name of the sub component. */
  const char* name;
  /** The implementation name of the sub component. */
  const char* implementationName;
#endif // DEBUGCOMPOSITE
};

// Bit field definition for the 'state' of subg components

#define SUB_COMPONENT_STATUS_MASK     ((unsigned char) 0x03)
#define SUB_COMPONENT_NOT_INITIALIZED ((unsigned char) 0x00)
#define SUB_COMPONENT_STOPPED         ((unsigned char) 0x01)
#define SUB_COMPONENT_STARTED         ((unsigned char) 0x02)
#define GET_SUB_COMPONENT_STATUS(state) ((unsigned char) (((unsigned char) (state)) & SUB_COMPONENT_STATUS_MASK))

/** The structure to hold bindings contained by the composite. */
struct BindingDesc_t {
  /** The index of the server component. */
  unsigned char server;
  /** The index of the client component. */
  unsigned char client;
  /** The state of the binding. */
  unsigned char state;
  /** The name of the server interface. */
  const char* serverName;
  /** The name of the client interface. */
  const char* clientName;
};

/**
 * If the client or the server of the binding is the composite component, the
 * index is set to COMPOSITE_IDX.
 */
#define COMPOSITE_IDX 0xff

// Bit field definition for the 'state' of bindings

#define BINDING_STATUS_MASK ((unsigned char) 0x01)
#define BINDING_BOUND       ((unsigned char) 0x01)
#define BINDING_UNBOUND     ((unsigned char) 0x00)
#define GET_BINDING_STATUS(state) ((unsigned char) (((unsigned char) (state)) & BINDING_STATUS_MASK))

#define BINDING_KIND_MASK    ((unsigned char) 0x02)
#define EXP_COL_BINDING      ((unsigned char) 0x02)
#define STANDARD_BINDING     ((unsigned char) 0x00)
#define GET_BINDING_KIND(state) ((unsigned char) (((unsigned char) (state)) & BINDING_KIND_MASK))

/**
 * The structure of a composite component.
 */
struct __cecilia_defaultCompositeData_t {
  // Exported interfaces
  Rfractal_api_Component myreference;
  Rfractal_api_BindingController bc;
  Rfractal_api_LifeCycleController lcc;
  Rfractal_api_ContentController cc;
  // Private data
  unsigned char nbSubComponents;
  unsigned char nbBindings;
  unsigned char started;
  struct SubComponentDesc_t *subComponents;
  struct BindingDesc_t *bindings;
};

/** The V-Table of the implementation of the 'Component' interface. */
extern struct Mfractal_api_Component __cecilia_defaultComposite_ComponentMeths;

/** The V-Table of the implementation of the 'BindingController' interface. */
extern struct Mfractal_api_BindingController
    __cecilia_defaultComposite_BindingControllerMeths;

/** The V-Table of the implementation of the 'LifeCycleController' interface. */
extern struct Mfractal_api_LifeCycleController
    __cecilia_defaultComposite_LifeCycleControllerMeths;

/** The V-Table of the implementation of the 'ContentController' interface. */
extern struct Mfractal_api_ContentController
    __cecilia_defaultComposite_ContentControllerMeths;

/**
 * Macro used by generated code to initialize composite instance.
 * This macro supposes that the array containing descriptions of sub-component
 * is called <instanceName>_subComponents
 * Similarly, This macro supposes that the array containing descriptions of
 * binding is called <instanceName>_bindings
 */
#define DEFAULT_COMPOSITE_INSTANCE(instanceName) \
  { \
    { &__cecilia_defaultComposite_ComponentMeths, &instanceName}, \
    { &__cecilia_defaultComposite_BindingControllerMeths, &instanceName}, \
    { &__cecilia_defaultComposite_LifeCycleControllerMeths, &instanceName}, \
    { &__cecilia_defaultComposite_ContentControllerMeths, &instanceName}, \
    sizeof(instanceName##_subComponents) / sizeof(instanceName##_subComponents[0]), \
    sizeof(instanceName##_bindings) / sizeof(instanceName##_bindings[0]), \
    0, \
    instanceName##_subComponents, \
    instanceName##_bindings \
  }

#endif // FRACTAL_API_DEFAULTCOMPOSITE_H
