/**
 * Cecilia, an implementation of the Fractal component model in C.
 * 
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Matthieu Leclercq.
 */

#ifndef CECILIA_H
#define CECILIA_H

/*
 * The '__CECILIA__DEFINE_COMPONENT_TYPE' macro is defined by generated code. 
 * The usage of this macro define the structure of the primitive component that
 * includes this file (i.e. the actual type of the '_this' parameter). Moreover,
 * this macro requires to be used after the 'DECLARE_DATA' statement since it 
 * references the type that is defined by this latter.
 */
#ifdef __CECILIA__DEFINE_COMPONENT_TYPE
__CECILIA__DEFINE_COMPONENT_TYPE;
#define __CECILIA__COMPONENT_TYPE_DEFINITION_DONE 1
#endif

/*
 * Definition of CALL macros 
 */

#define CALL0(itf, proc) \
  ((itf)->meth->proc((itf)->selfdata))
#define CALL1(itf, proc, arg1) \
  ((itf)->meth->proc((itf)->selfdata, arg1))
#define CALL2(itf, proc, arg1, arg2) \
  ((itf)->meth->proc((itf)->selfdata, arg1, arg2))
#define CALL3(itf, proc, arg1, arg2, arg3) \
  ((itf)->meth->proc((itf)->selfdata, arg1, arg2, arg3))
#define CALL4(itf, proc, arg1, arg2, arg3, arg4) \
  ((itf)->meth->proc((itf)->selfdata, arg1, arg2, arg3, arg4))
#define CALL5(itf, proc, arg1, arg2, arg3, arg4, arg5) \
  ((itf)->meth->proc((itf)->selfdata, arg1, arg2, arg3, arg4, arg5))
#define CALL6(itf, proc, arg1, arg2, arg3, arg4, arg5, arg6) \
  ((itf)->meth->proc((itf)->selfdata, arg1, arg2, arg3, arg4, arg5, arg6))
#define CALL7(itf, proc, arg1, arg2, arg3, arg4, arg5, arg6, arg7) \
  ((itf)->meth->proc((itf)->selfdata, arg1, arg2, arg3, arg4, arg5, arg6, arg7))
#define CALL8(itf, proc, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8) \
  ((itf)->meth->proc((itf)->selfdata, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8))

/*
 * This definition can replace the above CALLx macros.
 * Nevertheless, this construction doesn't allow recursion, i.e. it can't be
 * used the following way: CALL(c1, m1, CALL(c2, m2)).
 */
#ifndef CALL
#define CALL(itf, proc, args...) ((itf)->meth->proc((itf)->selfdata, ##args))
#endif

// define NULL if needed.
#ifndef NULL
#define NULL ((void *) 0)
#endif

#endif //CECILIA_H
