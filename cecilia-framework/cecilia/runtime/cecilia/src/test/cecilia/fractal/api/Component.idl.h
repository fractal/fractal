//+ Beginning of code produced by C Interface builder
/* Interface fractal.api.Component
Generated automatically, do not edit ! */

#ifndef Hfractal_api_Component
#define Hfractal_api_Component

// Interface descriptorstruct Mfractal_api_Component;
typedef struct {
  struct Mfractal_api_Component *meth;
  void *selfdata;
  #if defined(TYPEDINTERFACE)
  const char *type;
  #endif
}  *fractal_api_Component, Rfractal_api_Component;
#include "fractal/api/ErrorConst.idl.h"

/* some cecilia typedefs for predefinite C types */
#include "cecilia_types.h"


/* Virtual table */
struct Mfractal_api_Component {
  #define fractal_api_Component_OK ((jint) 0)
  
  #define fractal_api_Component_OPERATION_NOT_SUPPORTED ((jint) -1)
  
  #define fractal_api_Component_INVALID_ARGUMENT ((jint) -2)
  
  #define fractal_api_Component_NO_SUCH_INTERFACE ((jint) -102)
  
  #define fractal_api_Component_ILLEGAL_BINDING ((jint) -103)
  
  #define fractal_api_Component_ILLEGAL_LIFE_CYCLE ((jint) -104)
  
  #define fractal_api_Component_ILLEGAL_CONTENT ((jint) -105)
  
  #define fractal_api_Component_NO_SUCH_SUB_COMPONENT ((jint) -106)
  
  #define fractal_api_Component_NO_SUCH_ATTRIBUTE ((jint) -107)
  
  #define fractal_api_Component_ILLEGAL_ATTRIBUTE ((jint) -108)
  
  #define fractal_api_Component_INSTANTIATION_ERROR ((jint) -109)
  
  jint (*getFcInterface)(void *_this, const char* interfaceName, void * (*interfaceReference));
  jint (*getFcInterfaces)(void *_this, void * interfaceReferences[]);
};

#endif

//- End of code produced by C Interface builder
