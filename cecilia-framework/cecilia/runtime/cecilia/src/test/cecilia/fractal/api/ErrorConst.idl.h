//+ Beginning of code produced by C Interface builder
/* Interface fractal.api.ErrorConst
Generated automatically, do not edit ! */

#ifndef Hfractal_api_ErrorConst
#define Hfractal_api_ErrorConst

// Interface descriptorstruct Mfractal_api_ErrorConst;
typedef struct {
  struct Mfractal_api_ErrorConst *meth;
  void *selfdata;
  #if defined(TYPEDINTERFACE)
  const char *type;
  #endif
}  *fractal_api_ErrorConst, Rfractal_api_ErrorConst;

/* some cecilia typedefs for predefinite C types */
#include "cecilia_types.h"


/* Virtual table */
struct Mfractal_api_ErrorConst {
  #define fractal_api_ErrorConst_OK ((jint) 0)
  
  #define fractal_api_ErrorConst_OPERATION_NOT_SUPPORTED ((jint) -1)
  
  #define fractal_api_ErrorConst_INVALID_ARGUMENT ((jint) -2)
  
  #define fractal_api_ErrorConst_NO_SUCH_INTERFACE ((jint) -102)
  
  #define fractal_api_ErrorConst_ILLEGAL_BINDING ((jint) -103)
  
  #define fractal_api_ErrorConst_ILLEGAL_LIFE_CYCLE ((jint) -104)
  
  #define fractal_api_ErrorConst_ILLEGAL_CONTENT ((jint) -105)
  
  #define fractal_api_ErrorConst_NO_SUCH_SUB_COMPONENT ((jint) -106)
  
  #define fractal_api_ErrorConst_NO_SUCH_ATTRIBUTE ((jint) -107)
  
  #define fractal_api_ErrorConst_ILLEGAL_ATTRIBUTE ((jint) -108)
  
  #define fractal_api_ErrorConst_INSTANTIATION_ERROR ((jint) -109)
  
};

#endif

//- End of code produced by C Interface builder
