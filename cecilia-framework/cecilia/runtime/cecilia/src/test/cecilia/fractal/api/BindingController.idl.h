//+ Beginning of code produced by C Interface builder
/* Interface fractal.api.BindingController
Generated automatically, do not edit ! */

#ifndef Hfractal_api_BindingController
#define Hfractal_api_BindingController

// Interface descriptorstruct Mfractal_api_BindingController;
typedef struct {
  struct Mfractal_api_BindingController *meth;
  void *selfdata;
  #if defined(TYPEDINTERFACE)
  const char *type;
  #endif
}  *fractal_api_BindingController, Rfractal_api_BindingController;
#include "fractal/api/ErrorConst.idl.h"

/* some cecilia typedefs for predefinite C types */
#include "cecilia_types.h"


/* Virtual table */
struct Mfractal_api_BindingController {
  #define fractal_api_BindingController_OK ((jint) 0)
  
  #define fractal_api_BindingController_OPERATION_NOT_SUPPORTED ((jint) -1)
  
  #define fractal_api_BindingController_INVALID_ARGUMENT ((jint) -2)
  
  #define fractal_api_BindingController_NO_SUCH_INTERFACE ((jint) -102)
  
  #define fractal_api_BindingController_ILLEGAL_BINDING ((jint) -103)
  
  #define fractal_api_BindingController_ILLEGAL_LIFE_CYCLE ((jint) -104)
  
  #define fractal_api_BindingController_ILLEGAL_CONTENT ((jint) -105)
  
  #define fractal_api_BindingController_NO_SUCH_SUB_COMPONENT ((jint) -106)
  
  #define fractal_api_BindingController_NO_SUCH_ATTRIBUTE ((jint) -107)
  
  #define fractal_api_BindingController_ILLEGAL_ATTRIBUTE ((jint) -108)
  
  #define fractal_api_BindingController_INSTANTIATION_ERROR ((jint) -109)
  
  jint (*listFc)(void *_this, const char* clientItfNames[]);
  jint (*lookupFc)(void *_this, const char* clientItfName, void * (*interfaceReference));
  jint (*bindFc)(void *_this, const char* clientItfName, void * serverItf);
  jint (*unbindFc)(void *_this, const char* clientItfName);
};

#endif

//- End of code produced by C Interface builder
