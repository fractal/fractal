//+ Beginning of code produced by C Interface builder
/* Interface fractal.api.AttributeController
Generated automatically, do not edit ! */

#ifndef Hfractal_api_AttributeController
#define Hfractal_api_AttributeController

// Interface descriptorstruct Mfractal_api_AttributeController;
typedef struct {
  struct Mfractal_api_AttributeController *meth;
  void *selfdata;
  #if defined(TYPEDINTERFACE)
  const char *type;
  #endif
}  *fractal_api_AttributeController, Rfractal_api_AttributeController;
#include "fractal/api/ErrorConst.idl.h"

/* some cecilia typedefs for predefinite C types */
#include "cecilia_types.h"


/* Virtual table */
struct Mfractal_api_AttributeController {
  #define fractal_api_AttributeController_OK ((jint) 0)
  
  #define fractal_api_AttributeController_OPERATION_NOT_SUPPORTED ((jint) -1)
  
  #define fractal_api_AttributeController_INVALID_ARGUMENT ((jint) -2)
  
  #define fractal_api_AttributeController_NO_SUCH_INTERFACE ((jint) -102)
  
  #define fractal_api_AttributeController_ILLEGAL_BINDING ((jint) -103)
  
  #define fractal_api_AttributeController_ILLEGAL_LIFE_CYCLE ((jint) -104)
  
  #define fractal_api_AttributeController_ILLEGAL_CONTENT ((jint) -105)
  
  #define fractal_api_AttributeController_NO_SUCH_SUB_COMPONENT ((jint) -106)
  
  #define fractal_api_AttributeController_NO_SUCH_ATTRIBUTE ((jint) -107)
  
  #define fractal_api_AttributeController_ILLEGAL_ATTRIBUTE ((jint) -108)
  
  #define fractal_api_AttributeController_INSTANTIATION_ERROR ((jint) -109)
  
  jint (*listFcAttributes)(void *_this, const char* attributeNames[]);
  jint (*getFcAttribute)(void *_this, const char* attributeName, void * (*value));
  jint (*setFcAttribute)(void *_this, const char* attributeName, void * value);
};

#endif

//- End of code produced by C Interface builder
