//+ Beginning of code produced by C Interface builder
/* Interface fractal.api.LifeCycleController
Generated automatically, do not edit ! */

#ifndef Hfractal_api_LifeCycleController
#define Hfractal_api_LifeCycleController

// Interface descriptorstruct Mfractal_api_LifeCycleController;
typedef struct {
  struct Mfractal_api_LifeCycleController *meth;
  void *selfdata;
  #if defined(TYPEDINTERFACE)
  const char *type;
  #endif
}  *fractal_api_LifeCycleController, Rfractal_api_LifeCycleController;
#include "fractal/api/ErrorConst.idl.h"

/* some cecilia typedefs for predefinite C types */
#include "cecilia_types.h"


/* Virtual table */
struct Mfractal_api_LifeCycleController {
  #define fractal_api_LifeCycleController_OK ((jint) 0)
  
  #define fractal_api_LifeCycleController_OPERATION_NOT_SUPPORTED ((jint) -1)
  
  #define fractal_api_LifeCycleController_INVALID_ARGUMENT ((jint) -2)
  
  #define fractal_api_LifeCycleController_NO_SUCH_INTERFACE ((jint) -102)
  
  #define fractal_api_LifeCycleController_ILLEGAL_BINDING ((jint) -103)
  
  #define fractal_api_LifeCycleController_ILLEGAL_LIFE_CYCLE ((jint) -104)
  
  #define fractal_api_LifeCycleController_ILLEGAL_CONTENT ((jint) -105)
  
  #define fractal_api_LifeCycleController_NO_SUCH_SUB_COMPONENT ((jint) -106)
  
  #define fractal_api_LifeCycleController_NO_SUCH_ATTRIBUTE ((jint) -107)
  
  #define fractal_api_LifeCycleController_ILLEGAL_ATTRIBUTE ((jint) -108)
  
  #define fractal_api_LifeCycleController_INSTANTIATION_ERROR ((jint) -109)
  
  #define fractal_api_LifeCycleController_STOPPED ((jint) 0)
  
  #define fractal_api_LifeCycleController_STARTED ((jint) 1)
  
  jint (*getFcState)(void *_this);
  jint (*startFc)(void *_this);
  jint (*stopFc)(void *_this);
};

#endif

//- End of code produced by C Interface builder
