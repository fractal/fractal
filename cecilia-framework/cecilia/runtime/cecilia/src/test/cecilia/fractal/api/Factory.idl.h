//+ Beginning of code produced by C Interface builder
/* Interface fractal.api.Factory
Generated automatically, do not edit ! */

#ifndef Hfractal_api_Factory
#define Hfractal_api_Factory

// Interface descriptorstruct Mfractal_api_Factory;
typedef struct {
  struct Mfractal_api_Factory *meth;
  void *selfdata;
  #if defined(TYPEDINTERFACE)
  const char *type;
  #endif
}  *fractal_api_Factory, Rfractal_api_Factory;
#include "fractal/api/ErrorConst.idl.h"
#include "fractal/api/Component.idl.h"

/* some cecilia typedefs for predefinite C types */
#include "cecilia_types.h"


/* Virtual table */
struct Mfractal_api_Factory {
  #define fractal_api_Factory_OK ((jint) 0)
  
  #define fractal_api_Factory_OPERATION_NOT_SUPPORTED ((jint) -1)
  
  #define fractal_api_Factory_INVALID_ARGUMENT ((jint) -2)
  
  #define fractal_api_Factory_NO_SUCH_INTERFACE ((jint) -102)
  
  #define fractal_api_Factory_ILLEGAL_BINDING ((jint) -103)
  
  #define fractal_api_Factory_ILLEGAL_LIFE_CYCLE ((jint) -104)
  
  #define fractal_api_Factory_ILLEGAL_CONTENT ((jint) -105)
  
  #define fractal_api_Factory_NO_SUCH_SUB_COMPONENT ((jint) -106)
  
  #define fractal_api_Factory_NO_SUCH_ATTRIBUTE ((jint) -107)
  
  #define fractal_api_Factory_ILLEGAL_ATTRIBUTE ((jint) -108)
  
  #define fractal_api_Factory_INSTANTIATION_ERROR ((jint) -109)
  
  jint (*newFcInstance)(void *_this, fractal_api_Component (*instance));
  jint (*destroyFcInstance)(void *_this, fractal_api_Component instance);
};

#endif

//- End of code produced by C Interface builder
