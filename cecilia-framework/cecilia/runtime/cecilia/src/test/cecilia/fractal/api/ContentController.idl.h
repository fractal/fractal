//+ Beginning of code produced by C Interface builder
/* Interface fractal.api.ContentController
Generated automatically, do not edit ! */

#ifndef Hfractal_api_ContentController
#define Hfractal_api_ContentController

// Interface descriptorstruct Mfractal_api_ContentController;
typedef struct {
  struct Mfractal_api_ContentController *meth;
  void *selfdata;
  #if defined(TYPEDINTERFACE)
  const char *type;
  #endif
}  *fractal_api_ContentController, Rfractal_api_ContentController;
#include "fractal/api/Component.idl.h"
#include "fractal/api/ErrorConst.idl.h"

/* some cecilia typedefs for predefinite C types */
#include "cecilia_types.h"


/* Virtual table */
struct Mfractal_api_ContentController {
  #define fractal_api_ContentController_OK ((jint) 0)
  
  #define fractal_api_ContentController_OPERATION_NOT_SUPPORTED ((jint) -1)
  
  #define fractal_api_ContentController_INVALID_ARGUMENT ((jint) -2)
  
  #define fractal_api_ContentController_NO_SUCH_INTERFACE ((jint) -102)
  
  #define fractal_api_ContentController_ILLEGAL_BINDING ((jint) -103)
  
  #define fractal_api_ContentController_ILLEGAL_LIFE_CYCLE ((jint) -104)
  
  #define fractal_api_ContentController_ILLEGAL_CONTENT ((jint) -105)
  
  #define fractal_api_ContentController_NO_SUCH_SUB_COMPONENT ((jint) -106)
  
  #define fractal_api_ContentController_NO_SUCH_ATTRIBUTE ((jint) -107)
  
  #define fractal_api_ContentController_ILLEGAL_ATTRIBUTE ((jint) -108)
  
  #define fractal_api_ContentController_INSTANTIATION_ERROR ((jint) -109)
  
  jint (*getFcSubComponents)(void *_this, fractal_api_Component subComponents[]);
  jint (*addFcSubComponent)(void *_this, fractal_api_Component subComponent);
  jint (*removeFcSubComponent)(void *_this, fractal_api_Component subComponent);
  jint (*addFcSubBinding)(void *_this, fractal_api_Component clientComponent, const char* clientItfName, fractal_api_Component serverComponent, const char* serverItfName);
  jint (*removeFcSubBinding)(void *_this, fractal_api_Component clientComponent, const char* clientItfName);
};

#endif

//- End of code produced by C Interface builder
