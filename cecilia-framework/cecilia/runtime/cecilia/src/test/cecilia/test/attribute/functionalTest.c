#include <stdio.h>

#include "fractal/lib/ACdelegate.h"
#include "test/assert.h"

// the vTable of the attribute controller interface.
extern struct Mfractal_api_AttributeController
    __cecilia_default_AttributeControllerMeths;

#define CALL_AC(proc, self, args...) \
  (__cecilia_default_AttributeControllerMeths.proc((self), ##args))

#define NB_ATTR 5

typedef struct
{
  int a;
  const char *s;
  char c;
  short i;
  void *p;
} attributes_t;

attributes_t myAttributes = { 10, "foo", 'c', 5, NULL };

DEFINE_AC_DATA_TYPE(test, NB_ATTR);

struct test_AC_data_type attributes_desc = 
  { NB_ATTR, 
      { 
        { "a", &(myAttributes.a), sizeof(int) }, 
        { "s", &(myAttributes.s), sizeof(char *) },
        { "c", &(myAttributes.c), sizeof(char) },
        { "i", &(myAttributes.i), sizeof(short) },
        { "p", &(myAttributes.p), sizeof(void *) } 
      } 
  };

void test_listFcAttributes()
{
  int e;
  const char* attNames[NB_ATTR];

  e = CALL_AC(listFcAttributes, &attributes_desc, NULL);
  ASSERT_EQUALS_INT("test_listFcAttributes: number of attribute (1)", NB_ATTR,
      e);

  e = CALL_AC(listFcAttributes, &attributes_desc, attNames);
  ASSERT_EQUALS_INT("test_listFcAttributes: number of attribute (2)", NB_ATTR,
      e);

  ASSERT_EQUALS_STRING("test_listFcAttributes: name of first attribute", "a",
      attNames[0]);
  ASSERT_EQUALS_STRING("test_listFcAttributes: name of second attribute", "s",
      attNames[1]);
  ASSERT_EQUALS_STRING("test_listFcAttributes: name of second attribute", "c",
      attNames[2]);
  ASSERT_EQUALS_STRING("test_listFcAttributes: name of second attribute", "i",
      attNames[3]);
  ASSERT_EQUALS_STRING("test_listFcAttributes: name of third attribute", "p",
      attNames[4]);
}

void test_getFcAttribute()
{
  int e;
  int a;
  const char *s;
  char c;
  short i;
  void *p;

  e = CALL_AC(getFcAttribute, &attributes_desc, "a", (void **)&a);
  ASSERT_EQUALS_INT("test_getFcAttribute: getFcAttribute(a) return value",
      fractal_api_ErrorConst_OK, e);
  ASSERT_EQUALS_INT("value of a", myAttributes.a, a);

  e = CALL_AC(getFcAttribute, &attributes_desc, "s", (void **)&s);
  ASSERT_EQUALS_INT("test_getFcAttribute: getFcAttribute(s) return value",
      fractal_api_ErrorConst_OK, e);
  ASSERT_EQUALS_STRING("value of s", myAttributes.s, s);

  e = CALL_AC(getFcAttribute, &attributes_desc, "c", (void **)&c);
  ASSERT_EQUALS_INT("test_getFcAttribute: getFcAttribute(c) return value",
      fractal_api_ErrorConst_OK, e);
  ASSERT_EQUALS_INT("value of a", myAttributes.c, c);

  e = CALL_AC(getFcAttribute, &attributes_desc, "i", (void **)&i);
  ASSERT_EQUALS_INT("test_getFcAttribute: getFcAttribute(i) return value",
      fractal_api_ErrorConst_OK, e);
  ASSERT_EQUALS_INT("value of a", myAttributes.i, i);

  e = CALL_AC(getFcAttribute, &attributes_desc, "p", (void **)&p);
  ASSERT_EQUALS_INT("test_getFcAttribute: getFcAttribute(p) return value",
      fractal_api_ErrorConst_OK, e);
  ASSERT_EQUALS_POINTER("value of p", myAttributes.p, p);

  // checks if name is NULL
  e = CALL_AC(getFcAttribute, &attributes_desc, NULL, (void **)&a);
  ASSERT_EQUALS_INT("test_getFcAttribute: getFcAttribute(NULL) return value",
      fractal_api_ErrorConst_INVALID_ARGUMENT, e);

  // checks if value is NULL
  e = CALL_AC(getFcAttribute, &attributes_desc, "a", NULL);
  ASSERT_EQUALS_INT(
      "test_getFcAttribute: getFcAttribute(a, NULL) return value",
      fractal_api_ErrorConst_INVALID_ARGUMENT, e);

  // checks if attribute name is unknown
  e = CALL_AC(getFcAttribute, &attributes_desc, "unknown", (void **)&a);
  ASSERT_EQUALS_INT(
      "test_getFcAttribute: getFcAttribute(unknown) return value",
      fractal_api_ErrorConst_NO_SUCH_ATTRIBUTE, e);
}

void test_setFcAttribute()
{
  int e;

  e = CALL_AC(setFcAttribute, &attributes_desc, "a", (void *) 5);
  ASSERT_EQUALS_INT("test_getFcAttribute: setFcAttribute(a, 5) return value",
      fractal_api_ErrorConst_OK, e);
  ASSERT_EQUALS_INT("value of a", 5, myAttributes.a);

  e = CALL_AC(setFcAttribute, &attributes_desc, "s", (void *) "bar");
  ASSERT_EQUALS_INT(
      "test_getFcAttribute: setFcAttribute(s, \"bar\") return value",
      fractal_api_ErrorConst_OK, e);
  ASSERT_EQUALS_STRING("value of s", "bar", myAttributes.s);

  e = CALL_AC(setFcAttribute, &attributes_desc, "c", (void *) 'd');
  ASSERT_EQUALS_INT("test_getFcAttribute: setFcAttribute(c, 'd') return value",
      fractal_api_ErrorConst_OK, e);
  ASSERT_EQUALS_INT("value of c", 'd', myAttributes.c);

  e = CALL_AC(setFcAttribute, &attributes_desc, "i", (void *) -2);
  ASSERT_EQUALS_INT("test_getFcAttribute: setFcAttribute(i, -2) return value",
      fractal_api_ErrorConst_OK, e);
  ASSERT_EQUALS_INT("value of i", -2, myAttributes.i);

  e = CALL_AC(setFcAttribute, &attributes_desc, "p", (void *) &myAttributes);
  ASSERT_EQUALS_INT(
      "test_getFcAttribute: setFcAttribute(p, &myAttributes) return value",
      fractal_api_ErrorConst_OK, e);
  ASSERT_EQUALS_POINTER("value of p", &myAttributes, myAttributes.p);

  // checks if value is NULL (see bug#307506)
  e = CALL_AC(setFcAttribute, &attributes_desc, "p", NULL);
  ASSERT_EQUALS_INT(
      "test_getFcAttribute: setFcAttribute(p, NULL) return value",
      fractal_api_ErrorConst_OK, e);

  // checks if name is NULL
  e = CALL_AC(setFcAttribute, &attributes_desc, NULL, (void *) 0);
  ASSERT_EQUALS_INT("test_getFcAttribute: setFcAttribute(NULL) return value",
      fractal_api_ErrorConst_INVALID_ARGUMENT, e);

  // checks if attribute name is unknown
  e = CALL_AC(setFcAttribute, &attributes_desc, "unknown", (void *) 0);
  ASSERT_EQUALS_INT(
      "test_getFcAttribute: setFcAttribute(unknown) return value",
      fractal_api_ErrorConst_NO_SUCH_ATTRIBUTE, e);
}

int main()
{
  test_listFcAttributes();
  test_getFcAttribute();
  test_setFcAttribute();

  return 0;
}
