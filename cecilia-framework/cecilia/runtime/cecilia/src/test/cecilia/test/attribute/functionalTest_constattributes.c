#include <stdio.h>

#include "fractal/lib/ACdelegate_constattributes.h"
#include "test/assert.h"

// the vTable of the attribute controller interface.
extern struct Mfractal_api_AttributeController
    __cecilia_AttributeControllerConstAttributesMeths;

#define CALL_AC(proc, self, args...) \
  (__cecilia_AttributeControllerConstAttributesMeths.proc((self), ##args))

#define NB_ATTR 5

typedef struct
{
  const int a;
  const char *s;
  const char c;
  short i;
  void *p;
} attributes_t;

attributes_t myAttributes = { 10, "foo", 'c', 5, NULL };

DEFINE_CONST_AC_DATA_TYPE(test, NB_ATTR);

struct test_AC_data_type attributes_desc = 
  { NB_ATTR, 
    { 
      { "a", &(myAttributes.a), sizeof(int), 1 }, 
      { "s", &(myAttributes.s), sizeof(char *), 0 },
      { "c", &(myAttributes.c), sizeof(char), 1 },
      { "i", &(myAttributes.i), sizeof(short), 0 },
      { "p", &(myAttributes.p), sizeof(void *), 0 } 
    } 
  };

void test_setFcAttribute()
{
  int e;

  e = CALL_AC(setFcAttribute, &attributes_desc, "a", (void *) 5);
  /* assert that a constant field can't be set */
  ASSERT_EQUALS_INT("test_getFcAttribute: setFcAttribute(a, 5) return value",
      fractal_api_AttributeController_OPERATION_NOT_SUPPORTED, e);
  /* assert the value is still the original one */
  ASSERT_EQUALS_INT("value of a", 10, myAttributes.a);

  e = CALL_AC(setFcAttribute, &attributes_desc, "c", (void *) 'd');
  /* assert that a constant field can't be set */
  ASSERT_EQUALS_INT("test_getFcAttribute: setFcAttribute(c, 'd') return value",
	fractal_api_AttributeController_OPERATION_NOT_SUPPORTED, e);
  /* assert the value is still the original one */
  ASSERT_EQUALS_INT("value of c", 'c', myAttributes.c);
}

int main()
{
  test_setFcAttribute();

  return 0;
}
