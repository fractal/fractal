#ifndef TEST_ASSERT_H
#define TEST_ASSERT_H

#include <string.h>
#include <stdlib.h>

#define ASSERT_EQUALS_INT(msg, expected, actual) \
  if ((expected) != (actual)) { \
    printf("Assertion failed: %s. expected=%d, actual=%d", \
        (msg), (expected), (actual)); \
    exit(1); \
  } 

#define ASSERT_EQUALS_STRING(msg, expected, actual) \
  if (strcmp((expected), (actual)) != 0) { \
    printf("Assertion failed: %s. expected=\"%s\", actual=\"%s\"", \
        (msg), (expected), (actual)); \
    exit(1); \
  } 

#define ASSERT_EQUALS_POINTER(msg, expected, actual) \
  if ((expected) != (actual)) { \
    printf("Assertion failed: %s. expected=%p, actual=%p", \
        (msg), (expected), (actual)); \
    exit(1); \
  } 

#define FAIL(msg) \
  printf("Test fails: %s", (msg)); \
  exit(1);

#endif // TEST_ASSERT_H
