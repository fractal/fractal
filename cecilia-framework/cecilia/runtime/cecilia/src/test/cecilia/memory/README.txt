Files in this directory are supposed to be generated by the cecilia-toolchain.
But for dependency management reasons, tests performed in this modules cannot 
uses the cecilia-toolchain. So, currently these files are simply added as test 
resources for this module. This is a temporary solution, a better organization 
of tests in the cecilia (parent) module should be found.