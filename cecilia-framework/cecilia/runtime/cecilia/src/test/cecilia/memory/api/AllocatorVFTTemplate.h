//+ Beginning of code produced by C VFT definition builder
/* This file is generated automatically */
#ifndef _MEMORY_API_ALLOCATOR_VFT_TEMPLATE_H_
#define _MEMORY_API_ALLOCATOR_VFT_TEMPLATE_H_
#include "memory/api/Allocator.idl.h"

// The following macro can be used to define prototypes of the methods that a given component must implements.
#define MEMORY_API_ALLOCATOR_METHOD_DEFINITION(comp, itf) \
jbyte* comp##_##itf##_##alloc##_method(void *_this, jint size); \
void comp##_##itf##_##free##_method(void *_this, jbyte addr[]); \
// The following macro can be used to declare and initialize a VTF instance for a given component interface.
#define MEMORY_API_ALLOCATOR_VFT_DECLARATION(comp, itf) \
static struct Mmemory_api_Allocator comp##_##itf##meth = {\
  comp##_##itf##_##alloc##_method, \
  comp##_##itf##_##free##_method, \
};
#endif
//- End of code produced by C VFT definition builder
