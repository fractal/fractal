//+ Beginning of code produced by C Interface builder
/* Interface memory.api.Allocator
Generated automatically, do not edit ! */

#ifndef Hmemory_api_Allocator
#define Hmemory_api_Allocator

// Interface descriptorstruct Mmemory_api_Allocator;
typedef struct {
  struct Mmemory_api_Allocator *meth;
  void *selfdata;
  #if defined(TYPEDINTERFACE)
  char *type;
  #endif
} Rmemory_api_Allocator;
#ifndef HTYPES_JAVA
#define HTYPES_JAVA
typedef unsigned char    jboolean;
typedef unsigned short   jchar;
typedef signed char      jbyte;
typedef signed short     jshort;
typedef signed int       jint;
typedef signed long long jlong;
typedef unsigned char      jubyte;
typedef unsigned short     jushort;
typedef unsigned int       juint;
typedef unsigned long long julong;
typedef float            jfloat;
typedef double           jdouble;
#endif /* HTYPES_JAVA */


/* Virtual table */
struct Mmemory_api_Allocator {
  jbyte* (*alloc)(void *_this, jint size);
  void (*free)(void *_this, jbyte addr[]);
};

#endif
//- End of code produced by C Interface builder
