/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Test;

public class AttributeTest extends AbstractCeciliaTestCase {

  @Override
  protected String getBuildDir() throws Exception {
    return super.getBuildDir() + File.separatorChar + "AttributeTest";
  }

  @Test
  public void testFuntional() throws Exception {
    final File ac = compileToObj(AC_DELEGATE);
    final File f = compileToObj("test/attribute/functionalTest.c");

    final File exe = link("test/attribute/functionalTest", ac, f);
    assertEquals("test/attribute/functionalTest does not return correctly.", 0,
        exec(exe));
  }
  
  @Test
  public void testACwithConstAttributes() throws Exception {
    final File ac = compileToObj(AC_DELEGATE_CONST_ATTRIBUTES);
    final File f = compileToObj("test/attribute/functionalTest_constattributes.c");

    final File exe = link("test/attribute/functionalTest_constattributes", ac, f);
    assertEquals("test/attribute/functionalTest_constattributes does not return correctly.", 0,
        exec(exe));
  }

}
