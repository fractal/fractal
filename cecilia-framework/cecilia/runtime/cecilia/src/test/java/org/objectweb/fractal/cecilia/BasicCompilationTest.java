/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia;

import java.io.File;

import org.junit.Test;

/**
 * This test case simply checks that the cecilia source files compiles correctly
 * with {@value AbstractCeciliaTestCase#DEFAULT_C_FLAGS default cFlags}.
 */
public class BasicCompilationTest extends AbstractCeciliaTestCase {

  @Override
  protected String getBuildDir() throws Exception {
    return super.getBuildDir() + File.separatorChar + getSubBuildDirName();
  }

  protected String getSubBuildDirName() {
    return "BasicCompilationTest";
  }

  /**
   * Checks that the {@value AbstractCeciliaTestCase#AC_DELEGATE} file compiles
   * correctly.
   * 
   * @throws Exception if compilation fails.
   */
  @Test
  public void testACDelegate() throws Exception {
    compileToObj(AC_DELEGATE, true /* forced */);
  }

  /**
   * Checks that the {@value AbstractCeciliaTestCase#AC_DELEGATE_CONST_ATTRIBUTES} file compiles
   * correctly.
   * 
   * @throws Exception if compilation fails.
   */
  @Test
  public void testACDelegateConstAttr() throws Exception {
    compileToObj(AC_DELEGATE_CONST_ATTRIBUTES, true /* forced */);
  }

  /**
   * Checks that the {@value AbstractCeciliaTestCase#BC_DELEGATE} file compiles
   * correctly.
   * 
   * @throws Exception if compilation fails.
   */
  @Test
  public void testBCDelegate() throws Exception {
    compileToObj(BC_DELEGATE, true /* forced */);
  }

  /**
   * Checks that the {@value AbstractCeciliaTestCase#BC_DELEGATE_STATIC_BINDINGS} file compiles
   * correctly.
   * 
   * @throws Exception if compilation fails.
   */
  @Test
  public void testBCDelegateStaticBindings() throws Exception {
    compileToObj(BC_DELEGATE_STATIC_BINDINGS, true /* forced */);
  }

  /**
   * Checks that the {@value AbstractCeciliaTestCase#CI_DELEGATE} file compiles
   * correctly.
   * 
   * @throws Exception if compilation fails.
   */
  @Test
  public void testCIDelegate() throws Exception {
    compileToObj(CI_DELEGATE, true /* forced */);
  }

  /**
   * Checks that the {@value AbstractCeciliaTestCase#LCC_DELEGATE} file compiles
   * correctly.
   * 
   * @throws Exception if compilation fails.
   */
  @Test
  public void testLCCDelegate() throws Exception {
    compileToObj(LCC_DELEGATE, true /* forced */);
  }

  /**
   * Checks that the {@value AbstractCeciliaTestCase#DEFAULT_COMPOSITE} file
   * compiles correctly.
   * 
   * @throws Exception if compilation fails.
   */
  @Test
  public void testDefaultComposite() throws Exception {
    compileToObj(DEFAULT_COMPOSITE, true /* forced */);
  }

  /**
   * Checks that the {@value AbstractCeciliaTestCase#CLONEABLE_COMPOSITE} file
   * compiles correctly.
   * 
   * @throws Exception if compilation fails.
   */
  @Test
  public void testCloneableComposite() throws Exception {
    compileToObj(CLONEABLE_COMPOSITE, true /* forced */);
  }
}
