/***
 * Cecilia ADL Compiler
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author:Matthieu Leclercq
 */

package org.objectweb.fractal.cecilia;

import static org.junit.Assert.fail;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

/**
 * Utility class for tests. Defines methods to compile C source files and run
 * compiled applications.<br>
 * Tests can be configured using the following system properties:
 * <ul>
 * <li>{@link #BUILD_DIR_PROPERTY_NAME cecilia.test.buildDir} specify the test
 * output directory.</li>
 * <li>{@link #SOURCE_PATH_PROPERTY_NAME cecilia.test.sourcePath} specify the
 * source path of the tests.</li>
 * <li>{@link #COMPILER_CMD_PROPERTY_NAME cecilia.test.compiler} specify the
 * command of the C-compiler.</li>
 * <li>{@link #LINKER_CMD_PROPERTY_NAME cecilia.test.linker} specify the
 * command of the linker.</li>
 * <li>{@link #C_FLAGS_PROPERTY_NAME cecilia.test.cFlags} specify the
 * compilation flags.</li>
 * <li>{@link #LD_FLAGS_PROPERTY_NAME cecilia.test.ldFlags} specify the
 * compilation flags.</li>
 * </ul>
 */
public class AbstractCeciliaTestCase {

  /**
   * The name of the system property that specify the test output directory.
   * This property is mandatory.
   */
  public static final String    BUILD_DIR_PROPERTY_NAME       = "cecilia.test.buildDir";

  /**
   * The name of the system property that specify the source path of the tests.
   * This property is mandatory.
   */
  public static final String    SOURCE_PATH_PROPERTY_NAME     = "cecilia.test.sourcePath";

  /**
   * The name of the system property that specify the compiler command. This
   * property is optional (default value is <code>gcc</code>).
   */
  public static final String    COMPILER_CMD_PROPERTY_NAME    = "cecilia.test.compiler";

  /**
   * The name of the system property that specify the linker command. This
   * property is optional (default value is <code>gcc</code>).
   */
  public static final String    LINKER_CMD_PROPERTY_NAME      = "cecilia.test.linker";

  /**
   * The name of the system property that specify the compilation flags. This
   * property is optional (default value is given by the
   * {@value #DEFAULT_C_FLAGS} constant).
   */
  public static final String    C_FLAGS_PROPERTY_NAME         = "cecilia.test.cFlags";

  /** The value of the default compilation flags. */
  public static final String    DEFAULT_C_FLAGS               = "-g3 -W -Wall -Werror -Wwrite-strings -Wshadow -Wredundant-decls -Wpointer-arith -Wno-unused-parameter";

  /**
   * The name of the system property that specify the linker flags. This
   * property is optional (default value is given by the
   * {@value #DEFAULT_LD_FLAGS} constant).
   */
  public static final String    LD_FLAGS_PROPERTY_NAME        = "cecilia.test.ldFlags";

  /** The value of the default compilation flags. */
  public static final String    DEFAULT_LD_FLAGS              = "";
  
  protected static final String AC_DELEGATE                   = "fractal/lib/ACdelegate.c";
  protected static final String AC_DELEGATE_CONST_ATTRIBUTES  = "fractal/lib/ACdelegate_constattributes.c";
  protected static final String BC_DELEGATE                   = "fractal/lib/BCdelegate.c";
  protected static final String BC_DELEGATE_STATIC_BINDINGS   = "fractal/lib/BCdelegate_staticbindings.c";
  protected static final String CI_DELEGATE                   = "fractal/lib/CIdelegate.c";
  protected static final String LCC_DELEGATE                  = "fractal/lib/LCCdelegate.c";
  protected static final String DEFAULT_COMPOSITE             = "fractal/lib/defaultComposite.c";
  protected static final String CLONEABLE_COMPOSITE           = "fractal/lib/cloneableComposite.c";

  private ClassLoader           sourceClassLoader;

  /**
   * Compiles the given source file to an object file (i.e. compile with the
   * <code>-c</code> flag) and returns the corresponding {@link File}.
   * 
   * @param srcFileName the name of the file to compile. The actual compiled
   *            file is located using {@link #findSourceFile(String)}.
   * @param forced if <code>false</code>, checks if the output file is
   *            {@link #isUptodate(File, File...) up to date}
   * @return the compilation output file. This file is located in the
   *         {@link #getBuildDirectory() build directory}. Its name is based on
   *         the given <code>srcFileName</code> where the extension is
   *         replaced by <code>.o</code>.
   * @throws Exception is the compilation fails.
   * @see #compile(File, File, String, String)
   */
  protected File compileToObj(final String srcFileName, final boolean forced)
      throws Exception {
    final File objFile = new File(getBuildDirectory(), srcFileName.substring(0,
        srcFileName.lastIndexOf('.'))
        + ".o");
    compile(objFile, getAllCFlags() + " -c", getCompilerCommand(), forced,
        findSourceFile(srcFileName));
    return objFile;
  }

  /**
   * Shortcut of the {@link #compileToObj(String, boolean)} method where
   * <code>forced</code> parameter is <code>false</code>.
   */
  protected File compileToObj(final String srcFileName) throws Exception {
    return compileToObj(srcFileName, false);
  }

  /**
   * Compile the given source file to an executable application and returns the
   * corresponding {@link File}.
   * 
   * @param srcFileName the name of the file to compile. The actual compiled
   *            file is located using {@link #findSourceFile(String)}.
   * @param forced if <code>false</code>, checks if the output file is
   *            {@link #isUptodate(File, File...) up to date}
   * @return the compilation output file. This file is located in the
   *         {@link #getBuildDirectory() build directory}. Its name is based on
   *         the given <code>srcFileName</code> where the extension is
   *         removed.
   * @throws Exception is the compilation fails.
   * @see #compile(File, File, String, String)
   */
  protected File compile(final String srcFileName, final boolean forced)
      throws Exception {
    final File objFile = new File(getBuildDirectory(), srcFileName.substring(0,
        srcFileName.lastIndexOf('.')));
    compile(objFile, getAllCFlags(), getCompilerCommand(), forced,
        findSourceFile(srcFileName));
    return objFile;
  }

  /**
   * Shortcut of the {@link #compile(String, boolean)} method where
   * <code>forced</code> parameter is <code>false</code>.
   */
  protected File compile(final String srcFileName) throws Exception {
    return compile(srcFileName, false);
  }

  /**
   * Link the given list of source file and returns the corresponding executable
   * {@link File}.
   * 
   * @param fileName the name of the desired output file.
   * @param forced if <code>false</code>, checks if the output file is
   *            {@link #isUptodate(File, File...) up to date}
   * @param sourceFiles the list of file to be linked.
   * @return the compilation output file. This file is located in the
   *         {@link #getBuildDirectory() build directory}.
   * @throws Exception is the compilation fails.
   * @see #compile(File, File, String, String)
   */
  protected File link(final String fileName, final boolean forced,
      final File... sourceFiles) throws Exception {
    final File objFile = new File(getBuildDirectory(), fileName);
    compile(objFile, getAllCFlags(), getLinkerCommand(), forced, sourceFiles);
    return objFile;
  }

  /**
   * Shortcut of the {@link #link(String, boolean, File...)} method where
   * <code>forced</code> parameter is <code>false</code>.
   */
  protected File link(final String fileName, final File... sourceFiles)
      throws Exception {
    return link(fileName, false, sourceFiles);
  }

  /**
   * Compile the given source files.
   * 
   * @param sourceFiles the files to compile.
   * @param objFile the output file.
   * @param cFlags the compilation flags.
   * @param compilerCommand the compiler command to use.
   * @param forced if <code>false</code>, checks if the <code>objFile</code>
   *            is {@link #isUptodate(File, File...) up to date} regarding the
   *            given source files. <b>WARNING</b>: This checks doesn't take
   *            into account dependencies to other files, in particular included
   *            header files.
   * @throws Exception if the compilation fails.
   */
  protected void compile(final File objFile, final String cFlags,
      final String compilerCommand, boolean forced, final File... sourceFiles)
      throws Exception {
    if ((!forced) && isUptodate(objFile, sourceFiles)) return;

    final File outputDir = objFile.getParentFile();
    if (!outputDir.exists()) outputDir.mkdirs();

    String command = compilerCommand + " " + cFlags + " -o "
        + objFile.getAbsolutePath();
    for (final File sourceFile : sourceFiles) {
      command += " " + sourceFile.getAbsolutePath();
    }
    final int rValue = exec(command);
    if (rValue != 0) {
      fail("compilation fails");
    }
  }

  /**
   * Execute the given command.
   * 
   * @param command the command to execute.
   * @return the return value.
   * @throws Exception if something wrong happens.
   */
  protected int exec(final String command) throws Exception {
    final String executedCommand = command;
    System.out.println(executedCommand);

    final Process process = new ProcessBuilder(executedCommand.split("\\s"))
        .redirectErrorStream(true).start();

    // push output stream to std err
    new Thread(new Runnable() {

      public void run() {
        final LineNumberReader gccerr = new LineNumberReader(
            new InputStreamReader(new BufferedInputStream(process
                .getInputStream())));
        String line;
        boolean commandPrinted = false;
        try {
          while ((line = gccerr.readLine()) != null) {
            if (!commandPrinted) {
              System.err.println(executedCommand);
              commandPrinted = true;
            }
            System.err.println(line);
          }
          gccerr.close();
        } catch (final IOException e) {
          // ignore.
        }
      }
    }).start();

    return process.waitFor();
  }

  /**
   * Execute the given file.
   * 
   * @param executable the file to execute.
   * @return the return value.
   * @throws Exception if something wrong happens.
   */
  protected int exec(final File executable) throws Exception {
    return exec(executable.getAbsolutePath());
  }

  /**
   * Checks if the given <code>outputFile</code> is newer that the given list
   * of source file. More precisely returns <code>true</code> if and only if
   * the <code>outputFile</code> exists and the
   * {@link File#lastModified() timestamp} of each file in the
   * <code>sourceFiles</code> list is lesser than the timestamp of the output
   * file.
   * 
   * @param outputFile the output file to test.
   * @param sourceFiles the list of dependent files.
   * @return <code>true</code> if the output file is newer than the source
   *         files.
   * @throws Exception if one of the source files does not exist or if a
   *             timestamp can't be retrieved.
   */
  protected boolean isUptodate(final File outputFile, final File... sourceFiles)
      throws Exception {
    // if output file does not exist, return false.
    if (!outputFile.exists()) return false;
    final long outTimestamp = outputFile.lastModified();
    if (outTimestamp == 0)
      throw new Exception("Unable to get timestamp of file '"
          + outputFile.getAbsolutePath() + "'");

    for (final File sourceFile : sourceFiles) {
      if (!sourceFile.exists())
        throw new Exception("Source file '" + sourceFile.getAbsolutePath()
            + "' does not exist.");
      final long srcTimestamp = sourceFile.lastModified();
      if (srcTimestamp == 0)
        throw new Exception("Unable to get timestamp of file '"
            + sourceFile.getAbsolutePath() + "'");

      if (outTimestamp < srcTimestamp) {
        // source file is newer than output file
        return false;
      }
    }

    return true;
  }

  /**
   * Returns a {@link ClassLoader} that can be used to
   * {@link ClassLoader#getResource(String) locate} source files based on there
   * names. This class loader is configured using the value of
   * {@link #getSourcePath()}.
   * 
   * @return a {@link ClassLoader} that can be used to
   *         {@link ClassLoader#getResource(String) locate} source files based
   *         on there names.
   * @throws Exception if the {@link #SOURCE_PATH_PROPERTY_NAME} property is not
   *             defined or is invalid.
   * @see #findSourceFile(String)
   */
  protected ClassLoader getSourceClassLoader() throws Exception {
    if (sourceClassLoader == null) {
      final String paths = getSourcePath();
      final String[] sourcePaths = parsePath(paths);

      final URL[] urls = new URL[sourcePaths.length];
      for (int i = 0; i < urls.length; i++) {
        final String path = sourcePaths[i];
        final File f = new File(path);
        if (!f.exists()) {
          throw new Exception("Invalid source path: '" + f.getAbsolutePath()
              + "' does not exist.");
        }
        urls[i] = f.toURI().toURL();
      }

      sourceClassLoader = new URLClassLoader(urls, null);
    }
    return sourceClassLoader;
  }

  /**
   * Locates and returns a source file in the
   * {@link #getSourcePath() source path}.
   * 
   * @param srcFileName the name of the source file to locate.
   * @return the {@link File} object for the found source file.
   * @throws Exception if the file can't be found, or if the
   *             {@link #SOURCE_PATH_PROPERTY_NAME} property is not defined
   *             properly.
   * @see #getSourceClassLoader()
   * @see ClassLoader#getResource(String)
   */
  protected File findSourceFile(final String srcFileName) throws Exception {
    final URL sourceURL = getSourceClassLoader().getResource(srcFileName);
    if (sourceURL == null) {
      throw new Exception("Can't find source file '" + srcFileName + "'");
    }
    final File sourceFile = new File(sourceURL.getPath());
    if (!sourceFile.exists()) {
      throw new Exception("Unknown source file \"" + srcFileName + "\"");
    }
    return sourceFile;
  }

  /**
   * Returns the compilation flags. This method returns the
   * {@link #getCFlags() global cFlags} appended with a list of <code>-I</code>
   * directive for each path in the {@link #getSourcePath() source path}.
   * 
   * @return the compilation flags.
   * @throws Exception if the {@link #SOURCE_PATH_PROPERTY_NAME} property is not
   *             defined.
   */
  protected String getAllCFlags() throws Exception {
    String cFlags = getCFlags();
    for (final String srcPath : parsePath(getSourcePath())) {
      cFlags += " -I" + srcPath;
    }
    return cFlags;
  }

  /**
   * Returns the value of the {@link #COMPILER_CMD_PROPERTY_NAME} property. This
   * method is declared <code>non-static</code> in order to be overridden by
   * concrete test cases if needed.
   * 
   * @return the value of the {@link #COMPILER_CMD_PROPERTY_NAME} property or
   *         <code>"gcc"</code> if the property is not defined.
   */
  protected String getCompilerCommand() {
    return System.getProperty(COMPILER_CMD_PROPERTY_NAME, "gcc");
  }

  /**
   * Returns the value of the {@link #LINKER_CMD_PROPERTY_NAME} property. This
   * method is declared <code>non-static</code> in order to be overridden by
   * concrete test cases if needed.
   * 
   * @return the value of the {@link #LINKER_CMD_PROPERTY_NAME} property or
   *         <code>"gcc"</code> if the property is not defined.
   */
  protected String getLinkerCommand() {
    return System.getProperty(LINKER_CMD_PROPERTY_NAME, "gcc");
  }

  /**
   * Returns the value of the {@link #C_FLAGS_PROPERTY_NAME} property. This
   * method is declared <code>non-static</code> in order to be overridden by
   * concrete test cases if needed.
   * 
   * @return the value of the {@link #C_FLAGS_PROPERTY_NAME} property or
   *         {@value #DEFAULT_C_FLAGS} if the property is not defined.
   */
  protected String getCFlags() {
    return System.getProperty(C_FLAGS_PROPERTY_NAME, DEFAULT_C_FLAGS);
  }

  /**
   * Returns the value of the {@link #LD_FLAGS_PROPERTY_NAME} property. This
   * method is declared <code>non-static</code> in order to be overridden by
   * concrete test cases if needed.
   * 
   * @return the value of the {@link #LD_FLAGS_PROPERTY_NAME} property or
   *         {@value #DEFAULT_LD_FLAGS} if the property is not defined.
   */
  protected String getLDFlags() {
    return System.getProperty(LD_FLAGS_PROPERTY_NAME, DEFAULT_LD_FLAGS);
  }

  /**
   * Returns the value of the {@link #SOURCE_PATH_PROPERTY_NAME} property. This
   * method is declared <code>non-static</code> in order to be overridden by
   * concrete test cases if needed.
   * 
   * @return the value of the {@link #SOURCE_PATH_PROPERTY_NAME} property.
   * @throws Exception if the {@link #SOURCE_PATH_PROPERTY_NAME} property is not
   *             defined.
   */
  protected String getSourcePath() throws Exception {
    final String sourcePath = System.getProperty(SOURCE_PATH_PROPERTY_NAME);
    if (sourcePath == null) {
      throw new Exception("System property '" + SOURCE_PATH_PROPERTY_NAME
          + "' must be defined.");
    }
    return sourcePath;
  }

  /**
   * Returns the value of the {@link #BUILD_DIR_PROPERTY_NAME} property. This
   * method is declared <code>non-static</code> in order to be overridden by
   * concrete test cases if needed.
   * 
   * @return the value of the {@link #BUILD_DIR_PROPERTY_NAME} property.
   * @throws Exception if the {@link #BUILD_DIR_PROPERTY_NAME} property is not
   *             defined.
   */
  protected String getBuildDir() throws Exception {
    final String buildDir = System.getProperty(BUILD_DIR_PROPERTY_NAME);
    if (buildDir == null) {
      throw new Exception("System property '" + BUILD_DIR_PROPERTY_NAME
          + "' must be defined.");
    }
    return buildDir;
  }

  /**
   * Returns the {@link File} object corresponding to path returned by the
   * {@link #getBuildDir()} method, moreover this method creates the directory
   * if it doesn't exist. This method is declared <code>non-static</code> in
   * order to be overridden by concrete test cases if needed.
   * 
   * @return the {@link File} object corresponding to the folder designated by
   *         the {@link #BUILD_DIR_PROPERTY_NAME} property.
   * @throws Exception if the {@link #BUILD_DIR_PROPERTY_NAME} property is not
   *             defined or designate an existing file that is not a directory.
   */
  protected File getBuildDirectory() throws Exception {
    final File buildDir = new File(getBuildDir());
    if (!buildDir.exists()) {
      buildDir.mkdirs();
    } else if (!buildDir.isDirectory()) {
      throw new Exception("Invalid build directory '"
          + buildDir.getAbsolutePath() + "': not a directory.");
    }
    return buildDir;
  }

  private String[] parsePath(String paths) {
    final List<String> sourcePaths = new ArrayList<String>();
    int index = paths.indexOf(File.pathSeparatorChar);
    while (index != -1) {
      sourcePaths.add(paths.substring(0, index));
      paths = paths.substring(index + 1);
      index = paths.indexOf(File.pathSeparatorChar);
    }
    sourcePaths.add(paths);
    return sourcePaths.toArray(new String[0]);
  }
}
