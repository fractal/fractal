/**
 * Fractal API : fractal interfaces written in the Cecilia IDL language.
 *
 * Copyright (C) 2003 France Telecom R&D
 * Copyright (C) 2007 ST Microelectronics, INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: J.-P. Fassino, Matthieu Leclercq
 */

// START SNIPPET: Content
package fractal.api;

/**
 * A component interface to control the life-cycle of the component to which it
 * belongs. The life-cycle of a component is supposed to be an automaton, whose
 * states represent execution states of the component. This interface
 * corresponds to an automaton with two states called {@link #STARTED
 * STARTED} and {@link #STOPPED STOPPED}, where all the 4 four
 * possible transitions are allowed. It is however possible to define completely
 * different life-cycle controller interfaces to use completely different
 * automatons, or to define sub interfaces of this interface to define
 * automatons based on this one, but with more states and more transitions. <p>
 *
 * <b>Note:</b> the sub-interfaces of this interface should use the conventions
 * used in this interface, which are the following. The interface contains one
 * method per state in the life-cycle automaton. Each of these methods changes
 * the current state to the state corresponding to its name, if there is a
 * transition from the current state to this state. The interface also contains
 * one field per state. The names of these fields correspond to the names of the
 * methods.
 */
public interface LifeCycleController extends ErrorConst {

  /**
   * The state of a component just after {@link #stopFc stopFc} has been
   * executed. This state is also the initial state of a component, i.e., the
   * state of a component just after it has been created.
   */
  int STOPPED = 0;

  /**
   * The state of a component just after {@link #startFc startFc} has been
   * executed.
   */
  int STARTED = 1;

  /**
   * Returns the execution state of the component to which this interface
   * belongs.
   *
   * @return the execution state of the component to which this interface
   *      belongs.
   */
  int getFcState();

  /**
   * Starts the component to which this interface belongs.
   *
   * @return <code>0</code> if the operation succeed.
   *      {@link ErrorConst#ILLEGAL_LIFE_CYCLE} if it fails.
   */
  int startFc();

  /**
   * Stops the component to which this interface belongs. The result of a method
   * call on a stopped component is undefined, except on its control interfaces
   * (these calls are executed normally).
   *
   * @return <code>0</code> if the operation succeed.
   *      {@link ErrorConst#ILLEGAL_LIFE_CYCLE} if it fails.
   *      {@link ErrorConst#OPERATION_NOT_SUPPORTED} if this operation is not 
   *      supported.
   */
  int stopFc();
}
