/**
 * Fractal API : fractal interfaces written in the Cecilia IDL language.
 *
 * Copyright (C) 2003 France Telecom R&D
 * Copyright (C) 2007 ST Microelectronics, INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: J.-P. Fassino, Matthieu Leclercq
 */

// START SNIPPET: Content
package fractal.api;

/**
 * A component interface to control the attributes of the component to which it
 * belongs.
 */
public interface AttributeController extends ErrorConst {

  /**
   * Returns the names of the attributes of the component to which this
   * interface belongs. More precisely, if the given <code>attributeNames</code>
   * is <code>null</code>, this method returns the number of attributes. If it 
   * is not <code>null</code>, this method assumes that the given array is big
   * enough to contain all the attribute names.
   *
   * @param attributeNames an array into which attribute names are 
   *      copied. Can be <code>null</code>.
   * @return the number of attributes; or
   *      {@link ErrorConst#OPERATION_NOT_SUPPORTED} if this operation is not
   *      supported.
   */
  int listFcAttributes(const in string[] attributeNames);

  /**
   * Returns the value of an attribute.
   *
   * @param attributeName the name of the attribute to return.
   * @param value (out parameter) the value of the attribute.
   * @return <code>0</code> if the operation succeed.
   *      {@link ErrorConst#NO_SUCH_ATTRIBUTE} if there is no such attribute. 
   */
  int getFcAttribute(const in string attributeName, out any value);

  /**
   * Sets the value of an attribute.
   *
   * @param attributeName the name of the attribute to set.
   * @param value the value of the attribute to set.
   * @return <code>0</code> if the operation succeed.
   *      {@link ErrorConst#NO_SUCH_ATTRIBUTE} if there is no such attribute. 
   *      {@link ErrorConst#ILLEGAL_ATTRIBUTE} if the attribute is not settable.
   *      {@link ErrorConst#OPERATION_NOT_SUPPORTED} if this operation is not
   *      supported.
   */
  int setFcAttribute(const in string attributeName, in any value);
}
