This module contains Fractal APIs written in the Cecilia IDL Language.

For more informations on this Interface Definition Language you may
have a look at the following web page:

  http://fractal.objectweb.org/cecilia/cecilia-idl.html