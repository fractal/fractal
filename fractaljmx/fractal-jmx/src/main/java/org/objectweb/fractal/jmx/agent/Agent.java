/***
 * Fractal JMX
 * Copyright (C) 2008 Bull S.A.S.
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.jmx.agent;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.SuperController;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.util.Fractal;

import javax.management.ObjectName;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.NotificationListener;
import javax.management.NotificationFilter;
import javax.management.Notification;
import javax.management.JMException;
import javax.management.AttributeNotFoundException;
import javax.management.JMRuntimeException;
import javax.management.modelmbean.ModelMBean;
import javax.management.modelmbean.InvalidTargetObjectTypeException;
import javax.management.monitor.MonitorNotification;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;

import java.io.IOException;
import java.lang.management.ManagementFactory;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.RemoteException;
import java.util.*;
import java.net.MalformedURLException;

/**
 * A component representing the JMX agent level.
 *
 * <p>This component encapsulates a JMX {@link javax.management.MBeanServer MBeanServer}.
 * Specifically, since version 0.2, it encapsulates the PlatformMBeanServer which allows remote connexion (e.g. with JConsole)
 * as explained in the j2se documentation.
 * For example, the following command line arguments start the jvm with a JMX remote agent enabled, with no
 * authentication or ssl security and with a JMX connector listening on port 1234:
 * <UL>
 * <LI> -Dcom.sun.management.jmxremote.authenticate=false
 * <LI> -Dcom.sun.management.jmxremote.ssl=false
 * <LI> -Dcom.sun.management.jmxremote.port=1234
 * </UL>
 *
 * This component allows MBean manipulation by implementing the {@link Admin} interface for registering MBeans,
 * and the {@link AdminAttributes} interface for filtering MBeans.
 * Where the MBeans represent component server interfaces or JMX
 * {@link javax.management.monitor.MonitorMBean monitors} for observing component attributes.
 *
 * <p>The binding controller part of this component sets the [required] listener component
 * that handles JMX notifications sent out by the {@link javax.management.monitor.MonitorMBean monitors}.
 * The life-cycle controller part bootstraps this component by registering two MBeans representing
 * its {@link Admin} and {@link AdminAttributes} interfaces.
 * These two interfaces are identified respectively in the agent by the {@link ObjectName}:
 * <pre>
 *    Agent:itf=admin
 *    Agent:itf=attribute-controller
 * </pre>
 *
 * <p>The naming convention used for the MBeans representing component server interfaces is defined in {@link Admin}.
 * The {@link javax.management.monitor.MonitorMBean monitors} are identified by the {@link ObjectName}:
 * <pre>
 *    AgentService:type=<i>monitorType</i>,monitor=<i>index</i>[,attributeType=<i>attType</i>]
 * </pre>
 * where:
 * <UL>
 * <LI><I>monitorType</I>: is the type of the JMX monitor (counter, gauge or string).
 * <LI><I>index</I>: represents the position at which the monitor is specified, for a given filter of the
 * {@link AdminAttributes} interface.
 * <LI><I>attType</I>: is the type of the attribute being observed (for counter and gauge
 * monitors only, since they handle several integer or floating-point attribute types).
 * </UL>
 *
 * @version 0.2
 */
public class Agent implements BindingController, LifeCycleController, AdminAttributes, Admin {
    //private MBeanServer _server = MBeanServerFactory.newMBeanServer("Fractal");
    private MBeanServer _server = ManagementFactory.getPlatformMBeanServer();
    private int _depth = -1;
    private String _itfPatterns = "*:*";
    private String _stringMonitors = "";
    private String _counterMonitors = "";
    private String _gaugeMonitors = "";
    private Component _myId = null;
    private NotificationListener _listener = null;
    private AgentContext _ctx = new AgentContext();
    private Registry registry = null;

    private JMXConnectorServer rmiConnectorServer = null;

    private boolean _connectorStarted = false;

    private String _address = "localhost";

    private int _port = 2033;

    private String _adminId = "admin";

    // -------------------------------------------------------------------------
    // implements Binding controller
    // -------------------------------------------------------------------------
    public String[] listFc() {
        return new String[] { "listener" };
    }

    public Object lookupFc(final String itfName) {
        if (itfName.equals("listener")) {
            return _listener;
        } else
            return null;
    }

    public void bindFc(final String itfName, final Object itfValue) {
        if (itfName.equals("component")) {
            _myId = (Component) itfValue;
        } else if (itfName.equals("listener")) {
            _listener = (NotificationListener) itfValue;
        }
    }

    public void unbindFc(final String itfName) {
        if (itfName.equals("listener")) {
            _listener = null;
        }
    }

    // -------------------------------------------------------------------------
    // implements LifeCycle controller
    // -------------------------------------------------------------------------
    public String getFcState() {
        return null;
    }

    public void startFc() {
        try {
            ObjectName oName = new ObjectName(CONST.ADMIN + ":itf=admin");
            exposeFcItf(_myId, oName, "admin");
            oName = new ObjectName(CONST.ADMIN + ":itf=attribute-controller");
            exposeFcItf(_myId, oName, "attribute-controller");
        } catch (JMException e) {
            throw new JMRuntimeException(e.toString());
        }
        try {
            startConnector();
        } catch (IOException e) {
            throw new JMRuntimeException(e.toString());
        }
    }

    public void stopFc() {
        if(_connectorStarted && rmiConnectorServer != null) {
            try {
                rmiConnectorServer.stop();
            } catch (IOException e) {
                throw new JMRuntimeException(e.toString());
            }
        }
    }

    // -------------------------------------------------------------------------
    // implements Attribute controller
    // -------------------------------------------------------------------------
    /*public int getDepth() {
        return _depth;
    }

    public void setDepth(final int depth) {
        this._depth = depth;
    }*/

    public String getItfPatterns() {
        return _itfPatterns;
    }

    public void setItfPatterns(final String str) {
        this._itfPatterns = (str == null) ? "" : str;
    }

    public String getMonitorStringPatterns() {
        return _stringMonitors;
    }

    public void setMonitorStringPatterns(final String str) {
        this._stringMonitors = (str == null) ? "" : str;
    }

    public String getMonitorCounterPatterns() {
        return _counterMonitors;
    }

    public void setMonitorCounterPatterns(final String counters) {
        this._counterMonitors = (counters == null) ? "" : counters;
    }

    public String getMonitorGaugePatterns() {
        return _gaugeMonitors;
    }

    public void setMonitorGaugePatterns(final String gauges) {
        this._gaugeMonitors = (gauges == null) ? "" : gauges;
    }

    public String getCurrencyTimeLimit() {
        return Introspector.CURRENCY_TIME_LIMIT; // !!!
    }

    public void setCurrencyTimeLimit(final String currencyTimeLimit) {
        Introspector.CURRENCY_TIME_LIMIT = currencyTimeLimit; // !!!
    }

    public MBeanServer getRawMBeanServer() {
        return _server;
    }

    public void setRawMBeanServer(final MBeanServer srv) {
        throw new UnsupportedOperationException();
    }

        public String getAddress() {
        return _address;
    }

    public boolean getConnectorStarted() {
        return _connectorStarted;
    }

    public void setAddress(final String address) {
        _address = address;
    }

    public void setConnectorStarted(final boolean started) {
        _connectorStarted = started;
    }

    public int getPort() {
        return _port;
    }

    public void setPort(final int port) {
        _port = port;
    }

    public String getAdminId() {
        return _adminId;
    }

    public void setAdminId(final String adminId) {
        _adminId = adminId;
    }

    // -------------------------------------------------------------------------
    // implements Admin
    // -------------------------------------------------------------------------
    /*public MBeanServer rawAdmin() {
        return server;
    }*/

    public void expose() throws JMException {
        try {
            // clean
            unregisterMBean(CONST.FC + "*:*");
            unregisterMBean(CONST.MONITOR + "*:*");
            // reset agent context
            _ctx.reset(_itfPatterns, _stringMonitors, _counterMonitors, _gaugeMonitors);
            // expose components
            Component ids[] = (Component[]) exposeSuper(_myId, new HashSet()).toArray(new Component[0]);
            exposeSub(ids, 0);
            // starts monitors
            startMonitors(_ctx.getMonitors());
        } catch (RuntimeException e) {
            //e.printStackTrace();
            throw new JMException(e.toString());
        }
    }

    // -------------------------------------------------------------------------
    // expose methods
    // -------------------------------------------------------------------------
    private Set exposeSuper(Component id, Set set) {
        try {
            SuperController supCtrl = Fractal.getSuperController(id);
            Component ids[] = supCtrl.getFcSuperComponents();
            if (ids.length == 0){
                set.add(id);
            }
            else for (int j = 0; j < ids.length; j++) {
                try{
                    ids[j].getFcInterface("admin");
                    exposeSuper(ids[j], set);
                } catch (NoSuchInterfaceException e) { set.add(ids[j]);}
            }
        } catch (NoSuchInterfaceException e) { set.add(id);}
        return set;
    }

    private void exposeSub(Component ids[], int crtDepth) throws JMException {
        if (_depth >= 0 && crtDepth > _depth)
            return;
        for (int i = 0; i < ids.length; i++) {
            // register subcomponents
            try {
                ContentController cCtrl = Fractal.getContentController(ids[i]);
                exposeSub(cCtrl.getFcSubComponents(), crtDepth + 1);
            } catch (NoSuchInterfaceException ignored) {}
            // register component infos
            String[] names = getFullFCName(ids[i]);
            for (int j = 0; j < names.length; j++) {
                exposeFCinfos(ids[i], "/" + names[j] + '@' + Integer.toHexString(ids[i].hashCode()));
            }
        }
    }

    private void exposeFCinfos(Component id, String name) throws JMException {
        InterfaceType[] itfts = ((ComponentType) id.getFcType()).getFcInterfaceTypes();
        String domain = CONST.FC + name + (isShared(id) ? "-shared" : "");
        for (int j = 0; j < itfts.length; j++) {
            ObjectName oName = new ObjectName(domain + ":itf=" + itfts[j].getFcItfName());
            if (!itfts[j].isFcClientItf() && (_ctx.matchItfPatterns(oName) || _ctx.matchMonitorPatterns(oName))) {
                exposeFcItf(id, oName, itfts[j].getFcItfName());
                boolean ret = setObservers(oName, _ctx.getMonitors());
                if (!(_ctx.matchItfPatterns(oName) || ret))
                    _server.unregisterMBean(oName);
            }
        }
    }

    private boolean exposeFcItf(Component cid, ObjectName oname, String itfName) throws JMException {
        boolean ret = false;
        try {
            if (_server.isRegistered(oname))
                return true;
            // itf signature
            InterfaceType itfType = ((ComponentType) cid.getFcType()).getFcInterfaceType(itfName);
            String signature = itfType.getFcItfSignature();
            // introspect & register
            ModelMBean rmBean = Introspector.createMBean(cid.getFcInterface(itfName), Class.forName(signature));
            _server.registerMBean(rmBean, oname);
            ret = true;
        } catch (NoSuchInterfaceException ignored) {} catch (InvalidTargetObjectTypeException e) {
            throw new IllegalStateException(e.toString());
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException(e.toString());
        }
        return ret;
    }

    private boolean setObservers(ObjectName oName, AgentMonitorMBean[] monitors) throws JMException {
        boolean ret = false;
        for (int i = 0; i < monitors.length; i++) {
            try {
                Class c = _server.getAttribute(oName, monitors[i].getObservedAttribute()).getClass();
                //if (monitors[i].getPattern().apply(oName) && c.equals(monitors[i].getAttributeType())) {
                if (monitors[i].getPattern().apply(oName) && monitors[i].getAttributeType().isAssignableFrom(c)) {
                    monitors[i].addObservedObject(oName);
                    ret = true;
                }
            } catch (AttributeNotFoundException ignored) {}
        }
        return ret;
    }

    private void startMonitors(AgentMonitorMBean[] monitors) throws JMException {
        if (_listener == null)
            return;
        for (int i = 0; i < monitors.length; i++) {
            ObjectName[] oNames = monitors[i].getObservedObjects();
            if (oNames.length == 0)
                continue;
            ObjectName monitorName = _server.registerMBean(monitors[i], null).getObjectName();
            for (int j = 0; j < oNames.length; j++)
                _server.addNotificationListener(monitorName, _listener, new Filter(oNames[j]), oNames[j]);
            monitors[i].start();
        }
    }

    class Filter implements NotificationFilter {
        ObjectName observedObject;
        Filter(ObjectName observedObject) {
            this.observedObject = observedObject;
        }

        public boolean isNotificationEnabled(Notification n) {
            MonitorNotification notif = (MonitorNotification) n;
            if (observedObject.equals(notif.getObservedObject()))
                return true;
            return false;
        }
    }

    // -------------------------------------------------------------------------
    // JMX utils
    // -------------------------------------------------------------------------
    private void unregisterMBean(String pattern) throws JMException {
        ObjectName o = new ObjectName(pattern);
        Set s = _server.queryNames(o, null);
        for (Iterator i = s.iterator(); i.hasNext();)
            _server.unregisterMBean((ObjectName) i.next());
    }

    // -------------------------------------------------------------------------
    // Fractal utils
    // -------------------------------------------------------------------------
    private boolean isShared(Component id) {
        boolean shared = false;
        try {
            Component ids[] = Fractal.getSuperController(id).getFcSuperComponents();
            if (ids.length > 1)
                shared = true;
            else if (ids.length > 1)
                shared = isShared(ids[0]);
        } catch (NoSuchInterfaceException ignored) {}
        return shared;
    }

    private String[] getFullFCName(Component id) {
        String[] names = null;
        try {
            Component ids[] = Fractal.getSuperController(id).getFcSuperComponents();
            List l = new ArrayList();
            for (int i = 0; i < ids.length; i++) {
                String[] tmp = getFullFCName(ids[i]);
                for (int j = 0; j < tmp.length; j++)
                    tmp[j] += "/" + getFCName(id);
                l.addAll(Arrays.asList(tmp));
            }
            names = (String[]) l.toArray(new String[l.size()]);
        } catch (NoSuchInterfaceException ignored) {}
        if (names == null || names.length == 0) {
            names = new String[1];
            names[0] = getFCName(id);
        }
        return names;
    }

    private String getFCName(Component id) {
        try {
            NameController nc = Fractal.getNameController(id);
            return nc.getFcName();
        } catch (NoSuchInterfaceException e) {
            throw new IllegalStateException("NameController required");
        }
    }

    private void startConnector() throws IOException, RemoteException {
        // Create connector
        JMXServiceURL jmxServiceURL = null;
        String url = "service:jmx:rmi:///jndi/rmi://" + _address + ":" + _port + "/fractaljmx_connector";
        if(_connectorStarted && registry == null) {
            try {
                jmxServiceURL = new JMXServiceURL(url);
                registry = LocateRegistry.createRegistry(_port);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        if(_connectorStarted && rmiConnectorServer == null) {
            rmiConnectorServer =
                    JMXConnectorServerFactory.newJMXConnectorServer(jmxServiceURL, null, _server);
            rmiConnectorServer.start();
            System.out.println("[Agent JMX] connected to " + url);
        }
    }

}
