/***
 * Fractal JMX
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.jmx.agent;
/**
 * Specifies constants.
 * 
 * @version 0.1
 */
class CONST {
	static final public String ADMIN = "Agent";
	static final public String MONITOR = "AgentService";
	static final public String FC = "FC";
	static final public String PREFIX_SYNTAX =
		"\nSyntax [domainName]:observedAttribute=value,granularityPeriod=value";
	static final public String STRING_SYNTAX =
		PREFIX_SYNTAX + ",stringToCompare=value,notifyDiffer=value,notifyMatch=value[,otherProperty=value]*";
	static final public String COUNTER_SYNTAX =
		PREFIX_SYNTAX + ",initThreshold=value,modulus=value,offset=value,differenceMode=value[,otherProperty=value]*";
	static final public String GAUGE_SYNTAX =
		PREFIX_SYNTAX + ",lowThreshold=value,highThreshold=value,differenceMode=value[,otherProperty=value]*";
}
