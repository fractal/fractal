/***
 * Fractal JMX
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.jmx.julia.stat;

/** 
 * An interface for observing method calls.
 * More precisely, a controller implementing this interface  returns the number and the rate  
 * of method calls that have been made on the server interfaces of a component 
 * (control interfaces excluded).
 * 
 * @version 0.1
 */
public interface StatController {
 
	/**
	 * Gets the number of method calls.
	 * 
	 * @return the number of method calls that have been made on the server
	 * interfaces of the component (control interfaces excluded).
	 */
	int getNumberOfMethodCall();

	/**
	 * Gets the number of method calls that have "succeded".
	 * 
	 * @return the number of method calls that have been made on the server
	 * interfaces of the component (control interfaces excluded), and that have
	 * "succeded", i.e., that have not thrown an exception.
	 */
	int getNumberOfMethodSuccess();

	/**
	 * Gets the rate of method calls.
	 * 
	 * @return the rate of method calls (number / second) that have been made on the server
	 * interfaces of the component (control interfaces excluded).
	 */
	double getRateOfMethodCall();

	/**
	 * Gets the rate of method calls that have "succeded".
	 * 
	 * @return the rate of method calls (number / second) that have been made on the server
	 * interfaces of the component (control interfaces excluded), and that have
	 * "succeded", i.e., that have not thrown an exception.
	 */
	double getRateOfMethodSuccess();

	/**
	 * Resets (to zero) the values returned by this controller.
	 */
	void reset();
}

