/***
 * Fractal JMX
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Authors: Eric Bruneton
 */
package org.objectweb.fractal.jmx.julia.stat;

import org.objectweb.fractal.julia.asm.SimpleCodeGenerator;
import java.lang.reflect.Method;

/**
 * This code generator generates interception code. It uses the following form for
 * all methods:
 *
 * <p><pre>
 * <i>method-signature</i> {
 *   <i>return-type</i> result;
 *   delegate.statPreMethod(<i>method-name</i>);
 *   // original method code, where returns are replaced with gotos
 *   delegate.statPostMethod(<i>method-name</i>);
 *   return result;
 * }
 * </pre>
 *
 * where <tt>delegate</tt> is initialized with the following code: 
 *
 * <p><pre>
 * delegate = (...)ic.getInterface("stat-controller");
 * </pre>
 */
public class StatCodeGenerator extends SimpleCodeGenerator {
	protected String getControllerInterfaceName() {
		return "stat-controller";
	}

	protected String getPreMethodName() {
		return "statPreMethod";
	}

	protected String getPostMethodName() {
		return "statPostMethod";
	}

	protected Class getContextType() {
		return Void.TYPE;
	}

	protected String getMethodName(final Method m) {
		return m.getName();
	}
}
