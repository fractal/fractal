/***
 * Fractal JMX
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.jmx.listener;

import javax.management.Notification;
import javax.management.monitor.MonitorNotification;
import javax.management.NotificationListener;

import java.util.Date;

/** 
 * Provides a simple listener component.
 * An object of this class can be used to receive notifications sent out by standard monitoring services 
 * (counter, gauge and string) of the JMX specification. All notifications received will be write to user screen. 
 * 
 * @version 0.1
 */
public class BasicListener implements NotificationListener { 

	// -------------------------------------------------------------------------
	// Implementation of the NotificationListener interface
	// -------------------------------------------------------------------------
	/**
	 * Invoked when a JMX notification occurs. Currently handles {@link MonitorNotification monitor notifications} 
	 * sent out by standard monitoring services (counter, gauge and string) of the JMX specification. 
	 * This listener will print to user screen the following information (part of the received notification): 
	 * <ul>
	 * <li>the {@link Date} on which the Event initially occurred (initialized from the notification timestamp),</li> 
	 * <li>the notification type,</li> 
	 * <li>the monitor source on which the Event initially occurred,</li>
	 * <li>the observed object of this monitor notification,</li>
	 * <li>the observed attribute of this monitor notification,</li>
	 * <li>the threshold/string (depending on the monitor type) that triggered off this monitor notification,</li>
	 * <li>the derived gauge of this monitor notification.</li>
	 * </ul>
	 *
	 * @param n The notification (should be an instance of {@link MonitorNotification}).
	 * @param handback An opaque object, not used by this simple implementation.
	 * 
	 * @throws ClassCastException if the received notification is not instance of {@link MonitorNotification}. 
	 */
	public void handleNotification(Notification n, Object handback) {
		MonitorNotification notif = (MonitorNotification) n;
		StringBuffer msg = new StringBuffer(150);
		msg.append("\n");
		if (notif.getType().equals(MonitorNotification.STRING_TO_COMPARE_VALUE_MATCHED)
			|| notif.getType().equals(MonitorNotification.STRING_TO_COMPARE_VALUE_DIFFERED)) {
			msg.append("*****  String Monitor");
		} else if (notif.getType().equals(MonitorNotification.THRESHOLD_VALUE_EXCEEDED)) {
			msg.append("*****  Counter Monitor");
		} else if (notif.getType().equals(MonitorNotification.THRESHOLD_HIGH_VALUE_EXCEEDED)
			|| notif.getType().equals(MonitorNotification.THRESHOLD_LOW_VALUE_EXCEEDED)) {
				msg.append("*****  Gauge Monitor");
		} else
			msg.append("*****  Unexpected Monitor notication !!!");
		msg.append(" (" + new Date(notif.getTimeStamp()) + ")");
		msg.append("\n* NotificationType: " + notif.getType());
		msg.append("\n* Src: " + notif.getSource());
		msg.append("\n* ObservedObject: " + notif.getObservedObject());
		msg.append("\n* ObservedAttribute: " + notif.getObservedAttribute());
		msg.append("\n* Trigger: " + notif.getTrigger());
		msg.append("\n* DerivedGauge: " + notif.getDerivedGauge());
		//msg.append("\n* handback: " + handback);
		msg.append("\n*****");
		System.out.println(msg);
	}
}

