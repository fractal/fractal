/**
 *  The JMX 4 Distributed Open Processing (JMX4ODP)
 *
 *  Copyright (C) 2003 by Lucas McGregor <lmcgregor@users.sourceforge.net>
 *  All Rights Reserved.
 *
 *  This software is released under the terms of the following
 *  licenses:
 *
 *      - The BSD License (see LICENSE.BSD).
 *
 *  While not mandated by the BSD license, any patches you make to the
 *  JMX 4 Distributed Open Processing source code may be contributed back into the
 *  JMX 4 Distributed Open Processing project at your discretion. Contributions will
 *  benefit the Open Source community. Submissions may be made at:
 *
 *      http://jmx4odp.sourceforge.net
 *
 * As per the BSD liscence, credit must be given to Lucas McGregor
 * and the synerr dot org project.
 **/
package org.objectweb.fractal.jmx.agent;

import javax.management.Descriptor;
import javax.management.JMException;
import javax.management.modelmbean.RequiredModelMBean;
import javax.management.modelmbean.DescriptorSupport;
import javax.management.modelmbean.ModelMBeanInfo;
import javax.management.modelmbean.ModelMBeanInfoSupport;
import javax.management.modelmbean.ModelMBeanAttributeInfo;
import javax.management.modelmbean.ModelMBeanOperationInfo;
import javax.management.modelmbean.ModelMBeanConstructorInfo;
import javax.management.modelmbean.ModelMBeanNotificationInfo;
import javax.management.modelmbean.InvalidTargetObjectTypeException;

import java.util.Vector;
import java.lang.reflect.Method;

/**
 * Uses introspection to build ModelMBeans.
 * @author  Lucas McGregor, slightly modified for Fractal JMX
 */
public class Introspector {
	private final static boolean VERBOSE = true;
	private final static boolean VERBOSE_DEBUG = false;
	public static String CURRENCY_TIME_LIMIT = "1"; // !!!

	private Introspector() {}

	/**
	 * Uses introspection to create an MBean that can manage methods and attributes of an object.
	 *
	 * @return a RequiredModelMBean that can manage the object.
	 * 
	 * @param obj the object to introspect.
	 * 
	 * @throws JMException if a problem occurs during introspection.
	 */
	public static RequiredModelMBean createMBean(Object obj)
		throws JMException {
			try{
				return createMBean(obj, obj.getClass());
			}
			catch(InvalidTargetObjectTypeException e){
				throw new IllegalStateException(e.toString());
			}
		}

	/**
	 * Uses introspection to create an MBean that can manage some methods and attributes of an object.
	 * These methods and attributes are specified in a class assignable to the object class.
	 *
	 * @return a RequiredModelMBean that can manage the object.
	 * 
	 * @param obj the object to introspect.
	 * @param assign a class assignable to the object class.
	 * 
	 * @throws InvalidTargetObjectTypeException if assign is not assignable to the object class.
	 * @throws JMException if a problem occurs during introspection.
	 */
	public static RequiredModelMBean createMBean(Object obj, Class assign)
		throws JMException, InvalidTargetObjectTypeException {
		Class objClass = obj.getClass();
		if (!assign.isAssignableFrom(objClass))
			throw new InvalidTargetObjectTypeException(assign + " not a superclass or superinterface of " + objClass);
		Method[] methods = assign.getMethods();
		Vector attributes = new Vector();
		Vector operators = new Vector();

		// GET THE LIST OF METHODS IN THE OBJECT
		for (int i = 0; i < methods.length; i++) {
			// IF THE METHOD STARTS WITH "is," "get," or "set" THEN TRY TO ADD AN ATTRIBUTE TO THE MODELMBEAN
			if ((methods[i].getName().startsWith("get"))
				|| (methods[i].getName().startsWith("set"))
				|| (methods[i].getName().startsWith("is"))) {
				try {
					ModelMBeanAttributeInfo mmai = doAttribute(methods, i);
					if (mmai != null)
						attributes.add(mmai);
				} catch (Exception e) {
					if (VERBOSE)
						e.printStackTrace();
				}
			}

			// ADD THE METHOD AS AN OPERATION TO THE MODEL MBEAN
			try {
				ModelMBeanOperationInfo mmoi = doOperation(methods, i);
				if (mmoi != null)
					operators.add(mmoi);
			} catch (Exception e) {
				if (VERBOSE)
					e.printStackTrace();
			}
		}

		// ATTRIBUTES
		ModelMBeanAttributeInfo[] attributeArray = new ModelMBeanAttributeInfo[attributes.size()];
		for (int i_ = 0; i_ < attributeArray.length; i_++) {
			attributeArray[i_] = (ModelMBeanAttributeInfo) attributes.get(i_);
		}

		// METHODS
		ModelMBeanOperationInfo[] operatorArray = new ModelMBeanOperationInfo[operators.size()];
		for (int i_ = 0; i_ < operatorArray.length; i_++) {
			operatorArray[i_] = (ModelMBeanOperationInfo) operators.get(i_);
		}

		// Descriptor
		Descriptor desc = new DescriptorSupport();
		desc.setField("name", objClass.getName());
		desc.setField("descriptorType", "MBean");

		// CREATE MODELMBEAN
		ModelMBeanInfo mminfo =
			new ModelMBeanInfoSupport(
				objClass.getName(),
				objClass.getName(),
				attributeArray,
				new ModelMBeanConstructorInfo[0],
				operatorArray,
				new ModelMBeanNotificationInfo[0],
				desc);
		RequiredModelMBean model = new RequiredModelMBean(mminfo);
		model.setManagedResource(obj, "ObjectReference");
		return model;
	}

	/**
	 * Build the ModelMBeanOperationInfo for the method. Take the array
	 * of the objects methods and the index of the method to work on.
   * 
   * @param methods the array of the objects methods to work on.
   * @param i the index of the method to work on.
   * @return the ModelMBeanOperationInfo for the method.
   * @throws JMException if something goes wrong. 
	 */
	private static ModelMBeanOperationInfo doOperation(Method methods[], int i) throws JMException {
		if (VERBOSE_DEBUG)
			System.out.println("ObjectIntrospector: processing operation: " + methods[i].getName());

		Descriptor desc = new DescriptorSupport();
		desc.setField("name", methods[i].getName());
		desc.setField("descriptorType", "operation");
		// MAKE SURE THAT THE AGENT DOESN'T READ FROM CACHE (0 does not work with JMX RI)
		//desc.setField("currencyTimeLimit", "1"); //!!!
		desc.setField("currencyTimeLimit", CURRENCY_TIME_LIMIT);

		// IF THE METHOD STARTS WITH "get" or "is" TREAT IT AS A GETTER. 
		// IF IT STARTS WITH A "set" TREAT IT AS A SETTER.
		if ((methods[i].getName().startsWith("get") || methods[i].getName().startsWith("is"))
			&& (methods[i].getParameterTypes().length == 0))
			desc.setField("role", "getter");
		else if (methods[i].getName().startsWith("set") && methods[i].getParameterTypes().length == 1)
			desc.setField("role", "setter");
		else
			desc.setField("role", "operation");

		ModelMBeanOperationInfo mmoi = new ModelMBeanOperationInfo(methods[i].getName(), methods[i], desc);
		return mmoi;
	}

	/**
	 * Build the ModelMBeanAttributeInfo for the method. Take the array
	 * of the objects methods and the index of the method to work on. It
	 * needs the entire array of methods to determine matching sets of
	 * setters and getters.
   * 
   * @param methods the array of the objects methods to work on.
   * @param i the index of the method to work on.
   * @return the ModelMBeanOperationInfo for the method.
   * @throws JMException if something goes wrong. 
	 */
	private static ModelMBeanAttributeInfo doAttribute(Method methods[], int i) throws JMException {
		if (VERBOSE_DEBUG)
			System.out.println("ObjectIntrospector: processing attribute: " + methods[i].getName());

		String name = null;
		Method getter = null;
		Method setter = null;
		String type = null;

		// THIS METHOD IS A GETTER
		if ((methods[i].getName().startsWith("get")) && (methods[i].getParameterTypes().length == 0)) {
			String setName = "s" + methods[i].getName().substring(1);
			name = methods[i].getName().substring(3);
			getter = methods[i];
			type = methods[i].getReturnType().getName();
			// CHECK TO SEE IF THERE IS THE EQUVALENT WRITE OBJECT
			for (int i_ = 0; i_ < methods.length; i_++) {
				if (methods[i_].getName().equals(setName)) {
					setter = methods[i_];
					break;
				}
			}

		} // THIS OBJECT IS A SETTER
		else if ((methods[i].getName().startsWith("set")) && (methods[i].getParameterTypes().length == 1)) {
			String getName = "g" + methods[i].getName().substring(1);
			name = methods[i].getName().substring(3);
			// CHECK TO SEE IF THERE IS THE EQUVALENT READ OBJECT
			// IF SO, THEN THE GET EQUIVALENT WOULD HAVE RETURNED
			// A ModelMBeanAttributeInfo, SO RETURN A NULL
			// SO THIS ATTRIBUTE DOESN'T SHOW UP TWICE
			for (int i_ = 0; i_ < methods.length; i_++) {
				if (methods[i_].getName().equals(getName)) {
					return null;
				}
			}
			setter = methods[i];
			type = methods[i].getParameterTypes()[0].getName();

		} // THIS OBJECT IS A GETTER WITH NO SETTER
		else if ((methods[i].getName().startsWith("is")) && (methods[i].getParameterTypes() == null)) {
			name = methods[i].getName().substring(2);
			getter = methods[i];
			type = boolean.class.getName();
		} // NOT A COMPLIENT get, set, OR is ATTRIBUTE
		else {
			return null;
		}

		Descriptor desc = new DescriptorSupport();
		desc.setField("name", name); // THE NAME THAT THE ATTRIBUTE WILL HAVE
		desc.setField("descriptorType", "attribute"); // THIS IS MANDATORY FOR ANY ATTRIBUTES DESCRIPTION
		desc.setField("displayName", name);
		// MAKE SURE THAT THE AGENT DOESN'T READ FROM CACHE (0 does not work with JMX RI)
		//desc.setField("currencyTimeLimit", "1"); // !!!
		desc.setField("currencyTimeLimit", CURRENCY_TIME_LIMIT);

		if (getter != null)
			desc.setField("getMethod", getter.getName());
		if (setter != null)
			desc.setField("setMethod", setter.getName());

		ModelMBeanAttributeInfo mmai =
			new ModelMBeanAttributeInfo(name, type, name, (getter != null), (setter != null), false, desc);

		return mmai;
	}
}
