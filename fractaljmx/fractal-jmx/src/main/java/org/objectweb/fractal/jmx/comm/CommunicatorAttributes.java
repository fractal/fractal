/***
 * Fractal JMX
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.jmx.comm; 

import org.objectweb.fractal.api.control.AttributeController;

/**
 * A minimal attribute controller for protocol adaptor components.
 * 
 * @version 0.1
 */
public interface CommunicatorAttributes extends AttributeController {

	/**
	 * Gets the port number used by this component. 
	 *
	 * @return  the port number used by this component.
	 */
	int getPort ();

	/**
	 * Sets the port number used by this Component.
	 *
	 * @param port the port number used by this Component.
	 */
	void setPort (int port);
}

