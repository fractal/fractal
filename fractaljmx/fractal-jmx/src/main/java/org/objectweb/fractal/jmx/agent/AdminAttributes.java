/***
 * Fractal JMX
 * Copyright (C) 2008 Bull S.A.S.
 * Copyright (C) 2001-2002 France Telecom, INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.jmx.agent;

import org.objectweb.fractal.api.control.AttributeController;
import javax.management.MBeanServer;

/**
 * Attribute controller of the agent component.
 * This interface provides attributes for filtering MBeans in the scope of the agent.
 * Only the MBeans, whose {@link javax.management.ObjectName ObjectName} matches a pattern defined by these attributes,
 * are registered in the agent.  
 * 
 * <p>Available filters:
 * <ul>
 * <li><B>ItfPatterns</B>: for filtering component interface MBeans.
 * <li><B>MonitorStringPatterns</B>: for filtering  
 * {@link javax.management.monitor.StringMonitorMBean string monitor} MBeans.
 * <li><B>MonitorCounterPatterns</B>: for filtering  
 * {@link javax.management.monitor.CounterMonitorMBean counter monitor} MBeans.
 * <li><B>MonitorGaugePatterns</B>: for filtering  
 * {@link javax.management.monitor.GaugeMonitorMBean gauge monitor} MBeans.
 * </ul>
 * 
 * The syntax of these filters relies on the naming convention defined in {@link Admin}
 * for component interfaces,
 * except for some additional property-value pairs reserved for the configuration of JMX monitors.
 * 
 * 
 * @version 0.1
 */
public interface AdminAttributes extends AttributeController {
	/*int getDepth ();
	void setDepth (int depth);*/

	/**
	 * Returns the patterns for filtering components interface MBeans. 
	 *
	 * @return the patterns for filtering components interface MBeans.
	 *
	 * @see  #setItfPatterns
	 */
	String getItfPatterns();

	/**
	 * Sets the patterns for filtering components interface MBeans.
	 * Any component interface, whose {@link javax.management.ObjectName ObjectName} matches one of these patterns,
	 * can be registered in the agent. 
	 * 
	 * <P>The syntax of this filter is defined to equal a sequence of (";" separated) string representations of 
	 * {@link javax.management.ObjectName ObjectName} patterns.
	 * For example:
	 * <p><pre>
	 *    *:itf=attribute-controller,*;&#42;/Foo/*:*
	 * </pre>
	 * represents two patterns that can match respectively:
	 * <UL>
	 * <LI>the attribute controller interface of any component.</LI> 
	 * <LI>any server interface of any component, if a super-component (in the content hierarchy) 
	 * of this component is named "Foo".</LI>
	 * </UL>
	 *
	 * @param str the patterns for filtering components interface MBeans.
	 */
	void setItfPatterns(String str);

	/**
	 * Returns the patterns for filtering {@link javax.management.monitor.StringMonitorMBean string monitor} MBeans. 
	 *
	 * @return the patterns for filtering {@link javax.management.monitor.StringMonitorMBean string monitor} MBeans.
	 *
	 * @see  #setMonitorStringPatterns
	 */
	String getMonitorStringPatterns();

	/**
	 * Sets the patterns for filtering {@link javax.management.monitor.StringMonitorMBean string monitor} MBeans.
	 * Any getter of type string, specified in a component interface whose 
	 * {@link javax.management.ObjectName ObjectName} matches one of these patterns,
	 * can be observed by a {@link javax.management.monitor.StringMonitorMBean string monitor}.
	 * 
	 * <P>The syntax of this filter is defined to equal a sequence of (";" separated) string representations of 
	 * {@link javax.management.ObjectName ObjectName} patterns. Where the following properties are 
	 * reserved for the configuration of a {@link javax.management.monitor.StringMonitorMBean string monitor}
	 * (as defined in the JMX specification):
	 * <p><UL>
	 * <LI><B>observedAttribute</B>: the attribute being observed.</LI> 
	 * <LI><B>granularityPeriod</B>: the granularity period (in milliseconds).</LI> 
	 * <LI><B>stringToCompare</B>: the string to compare with the observed attribute.</LI> 
	 * <LI><B>notifyDiffer</B>: the differing notification's on/off switch value.</LI> 
	 * <LI><B>notifyMatch</B>: the matching notification's on/off switch value.</LI> 
	 * </UL>
	 * 
	 * <p>For example:
	 * <p><pre>
	 *    *:itf=attribute-controller,observedAttribute=Foo,granularityPeriod=5000,
	 *      stringToCompare=fooval,notifyDiffer=true,notifyMatch=true,*
	 * </pre>
	 * represents a pattern that can match the attribute controller interface of any component, 
	 * if this interface specifies a string attribute "Foo". 
	 * This attribute will be monitored every 5000 ms and compared with the value "fooval".
	 * As defined in the JMX specification:
	 * <p><UL>
	 * <LI>A {@link javax.management.monitor.MonitorNotification#STRING_TO_COMPARE_VALUE_MATCHED match notification} 
	 * is sent when the value of the attribute "Foo" first matches the value "fooval".
	 * Subsequent matchings of "fooval" do not cause further notifications unless the attribute value 
	 * differs from "fooval".</LI> 
	 * <LI>A {@link javax.management.monitor.MonitorNotification#STRING_TO_COMPARE_VALUE_DIFFERED differ notification} 
	 * is sent when the value of the attribute "Foo" first differs from the value "fooval".
	 * Subsequent differences from "fooval" do not cause further notifications unless the attribute value 
	 * matches "fooval".</LI> 
	 * </UL>
	 * 
	 *
	 * @param str the patterns for filtering {@link javax.management.monitor.StringMonitorMBean string monitor} MBeans.
	 */
	void setMonitorStringPatterns(String str);

	/**
	 * Returns the patterns for filtering {@link javax.management.monitor.CounterMonitorMBean counter monitor} MBeans. 
	 *
	 * @return the patterns for filtering {@link javax.management.monitor.CounterMonitorMBean counter monitor} MBeans.
	 *
	 * @see  #setMonitorCounterPatterns
	 */
	String getMonitorCounterPatterns();

	/**
	 * Sets the patterns for filtering {@link javax.management.monitor.CounterMonitorMBean counter monitor} MBeans.
	 * Any getter of type integer (Byte, Integer, Short, Long), 
	 * specified in a component interface whose 
	 * {@link javax.management.ObjectName ObjectName} matches one of these patterns,
	 * can be observed by a {@link javax.management.monitor.CounterMonitorMBean counter monitor}.
	 * 
	 * <P>The syntax of this filter is defined to equal a sequence of (";" separated) string representations of 
	 * {@link javax.management.ObjectName ObjectName} patterns. Where the following properties are 
	 * reserved for the configuration of a {@link javax.management.monitor.CounterMonitorMBean counter monitor}
	 * (as defined in the JMX specification):
	 * <p><UL>
	 * <LI><B>observedAttribute</B>: the attribute being observed.</LI> 
	 * <LI><B>granularityPeriod</B>: the granularity period (in milliseconds).</LI> 
	 * <LI><B>initThreshold</B>: the initial threshold value.</LI> 
	 * <LI><B>modulus</B>: the modulus value.</LI> 
	 * <LI><B>offset</B>: the offset value.</LI> 
	 * <LI><B>differenceMode</B>: the difference mode flag value.</LI> 
	 * </UL>
	 * 
	 * <p>For example:
	 * <p><pre>
	 *    *:itf=stat-controller,observedAttribute=NumberOfMethodCall,granularityPeriod=5000,
	 *      initThreshold=2,modulus=0,offset=10,differenceMode=false,*
	 * </pre>
	 * represents a pattern that can match the interface "stat-controller" of any component, 
	 * if this interface specifies an integer attribute named "NumberOfMethodCall". 
	 * This attribute will be monitored every 5000 ms.
	 * {@link javax.management.monitor.MonitorNotification#THRESHOLD_VALUE_EXCEEDED Threshold notifications}
	 * are sent, as defined in the JMX specification for a 
	 * {@link javax.management.monitor.CounterMonitorMBean counter monitor} configured with: 
	 * InitThreshold=2, Modulus=0, Offset=10 and DifferenceMode=false
	 * (by default Notify=true).
	 *
	 * @param str the patterns for filtering 
	 * {@link javax.management.monitor.CounterMonitorMBean counter monitor} MBeans.
	 */
	void setMonitorCounterPatterns(String str);

	/**
	 * Returns the patterns for filtering 
	 * {@link javax.management.monitor.GaugeMonitorMBean gauge monitor} MBeans. 
	 *
	 * @return the patterns for filtering 
	 * {@link javax.management.monitor.GaugeMonitorMBean gauge monitor} MBeans.
	 *
	 * @see  #setMonitorGaugePatterns
	 */
	String getMonitorGaugePatterns();

	/**
	 * Sets the patterns for filtering 
	 * {@link javax.management.monitor.GaugeMonitorMBean gauge monitor} MBeans.
	 * Any getter of type integer or floating-point (Byte, Integer, Short,
	 * Long, Float, Double), specified in a component interface whose 
	 * {@link javax.management.ObjectName ObjectName} matches one of these patterns,
	 * can be observed by a {@link javax.management.monitor.GaugeMonitorMBean gauge monitor}.
	 * 
	 * <P>The syntax of this filter is defined to equal a sequence of (";" separated) string representations of 
	 * {@link javax.management.ObjectName ObjectName} patterns. Where the following properties are 
	 * reserved for the configuration of a {@link javax.management.monitor.GaugeMonitorMBean gauge monitor}
	 * (as defined in the JMX specification):
	 * <p><UL>
	 * <LI><B>observedAttribute</B>: the attribute being observed.</LI> 
	 * <LI><B>granularityPeriod</B>: the granularity period (in milliseconds).</LI> 
	 * <LI><B>lowThreshold</B>: the low threshold value.</LI> 
	 * <LI><B>highThreshold</B>: the low threshold value.</LI> 
	 * <LI><B>differenceMode</B>: the difference mode flag value.</LI> 
	 * </UL>
	 * 
	 * <p>For example:
	 * <p><pre>
	 *    *:itf=stat-controller,observedAttribute=RateOfMethodCall,granularityPeriod=5000,
	 *      lowThreshold=0.05,highThreshold=0.15,differenceMode=false,*
	 * </pre>
	 * represents a pattern that can match the interface "stat-controller" of any component, 
	 * if this interface specifies an integer or floating-point attribute named "RateOfMethodCall". 
	 * This attribute will be monitored every 5000 ms.
	 * {@link javax.management.monitor.MonitorNotification#THRESHOLD_HIGH_VALUE_EXCEEDED Threshold high notifications}
	 * and 
	 * {@link javax.management.monitor.MonitorNotification#THRESHOLD_LOW_VALUE_EXCEEDED Threshold low notifications}
	 * are sent, as defined in the JMX specification for a 
	 * {@link javax.management.monitor.GaugeMonitorMBean gauge monitor} configured with: 
	 * LowThreshold=0.05, HighThreshold=0.15 and DifferenceMode=false
	 * (by default NotifyHigh=true and NotifyLow=true).
	 * </UL>
	 * 
	 *
	 * @param str the patterns for filtering 
	 * {@link javax.management.monitor.GaugeMonitorMBean gauge monitor} MBeans.
	 */
	void setMonitorGaugePatterns(String str);

	/**
	 * Returns the patterns for filtering {@link javax.management.monitor.PeriodicMonitorMBean periodic monitor} MBeans. 
	 *
	 * @return the patterns for filtering {@link javax.management.monitor.PeriodicMonitorMBean periodic monitor} MBeans.
	 *
	 * @see  #setMonitorPeriodicPatterns
	 */
	//String getMonitorPeriodicPatterns(); // !!!

	/**
	 * Sets the patterns for filtering {@link javax.management.monitor.PeriodicMonitorMBean periodic monitor} MBeans.
	 * Any getter, specified in a component interface whose 
	 * {@link javax.management.ObjectName ObjectName} matches one of these patterns,
	 * can be observed by a {@link javax.management.monitor.PeriodicMonitorMBean periodic monitor}.
	 * 
	 * <P>The syntax of this filter is defined to equal a sequence of (";" separated) string representations of 
	 * {@link javax.management.ObjectName ObjectName} patterns. Where the following properties are 
	 * reserved for the configuration of a {@link javax.management.monitor.PeriodicMonitorMBean periodic monitor}:
	 * <p><UL>
	 * <LI><B>observedAttribute</B>: the attribute being observed.</LI> 
	 * <LI><B>granularityPeriod</B>: the granularity period (in milliseconds).</LI> 
	 * </UL>
	 * 
	 * <p>For example:
	 * <p><pre>
	 *    *:itf=attribute-controller,observedAttribute=Foo,granularityPeriod=5000
	 * </pre>
	 * represents a pattern that can match the attribute controller interface of any component, 
	 * if this interface specifies a string attribute "Foo". 
	 * Every 5000 ms, this attribute will be monitored and a notification will be sent.
	 * 
	 *
	 * @param str the patterns for filtering {@link javax.management.monitor.PeriodicMonitorMBean periodic monitor} MBeans.
	 */
	//void setMonitorPeriodicPatterns(String str); //!!!

	/**
	 * Returns the underlying {@link javax.management.monitor.MBeanServer MBeanServer} used by the agent.
	 * 
	 * This method allows raw MBean manipulation on the Fractal JMX agent side.  
	 * 
	 * @return the underlying {@link javax.management.monitor.MBeanServer MBeanServer} used by the agent.
	 */
	MBeanServer getRawMBeanServer();

	/**
	 * Sets the underlying {@link javax.management.monitor.MBeanServer MBeanServer} used by the agent.
	 * 
	 * <p>This method is unsupported, but specified since the Fractal 
	 * {@link org.objectweb.fractal.api.control.AttributeController attribute controller} 
	 * API requires getter and setter methods for each attribute. 
	 *
	 * @param srv the underlying {@link javax.management.monitor.MBeanServer MBeanServer} used by the agent.
	 * 
	 * @throws UnsupportedOperationException if this operation is not supported.
	 */
	void setRawMBeanServer(MBeanServer srv) throws UnsupportedOperationException;

	/**
	 * Returns the CurrencyTimeLimit value (in second) used by the agent when creating/registering new MBean.
	 *
	 * <p>Note (from Jmx Spec.): Meaning of CurrencyTimeLimit when a method is called on a MBean:
	 * <p><UL>
	 * <LI>Positive value:  the return value is stored in a cache for the value
	 * <LI>Negative value: avoid any caching
	 * </UL>
	 * 
	 * @return a string represention of the CurrencyTimeLimit value (in second) used by the agent when creating/registering new model MBean.
	 */
	String getCurrencyTimeLimit();

	/**
	 * Sets the CurrencyTimeLimit value (in second) used by the agent when creating/registering new model MBean
	 *
	 * @param str a string represention of the CurrencyTimeLimit value.
	 * 
	 * @see  #getCurrencyTimeLimit
	 */
	void setCurrencyTimeLimit(String str);

    /**
     * Return if the RMI connector is enabled.
     * @return true if the RMI connector is enabled
     */
    boolean getConnectorStarted();

    /**
     * Set if the RMI connector is enabled.
     * @param started true if the RMI connector is enabled
     */
    void setConnectorStarted(boolean started);

    /**
     * Return the address of the RMI connector.
     * @return the address of the RMI connector
     */
    String getAddress();

    /**
     * Set the address of the RMI connector.
     * @param address of the RMI connector
     */
    void setAddress(String address);

    /**
     * Return the port of the RMI connector.
     * @return the port of the RMI connector
     */
    int getPort();

    /**
     * Set the port of the RMI connector.
     * @param port the port of the RMI connector
     */
    void setPort(int port);

    /**
     * Return the identifier of the agent component.
     * @return the identifier of the agent component
     */
    String getAdminId();

    /**
     * Set the identifier of the agent component.
     * @param adminId the identifier of the agent component
     */
    void setAdminId(final String adminId);

}
