/***
 * Fractal JMX
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.jmx.julia.stat; 

import org.objectweb.fractal.julia.loader.Tree;

/** 
 * Extends {@link BasicStatController} with an exponential smoothing method for the rate computation. 
 * The rate values returned by this implementation are computed as follows:
 * <pre>
 *            S<sub>T</sub> = (alpha * x<sub>T</sub>) + ((1 - alpha) * S<sub>T-1</sub>)
 * </pre>
 * 
 * where:  
 * <ul>
 * <li>S<sub>T</sub>: is the current smoothed rate value. It represents a weighted average of all past rate values 
 * returned by {@link BasicStatController}.
 * <li>alpha: is the {@link #_alpha smoothing constant}. 
 * Its value must be in the interval [0-1] and can be fixed by {@link #initialize initialize}. 
 * <br>If alpha equals 1, the rate values returned by this implementation are similar to the values returned by 
 * {@link BasicStatController}. 
 * <li>x<sub>T</sub>: is the current actual rate value returned by {@link BasicStatController}.
 * <li>S<sub>T-1</sub>: is the previous smoothed rate value.
 * </ul>
 * 
 * 
 * @version 0.1
 */
public class ExponentialSmoothingStatController extends BasicStatController {
	/**
	 * Smoothing constant (default value is 0.5). 
	 * <p>Its value must be in the interval [0-1] and can be fixed by {@link #initialize initialize}.
	 */
	protected double _alpha = 0.5;

	private double _lastRateOfMethodCall;

	private double _lastRateOfMethodSuccess;

	// -------------------------------------------------------------------------
	// overload BasicStatController
	// -------------------------------------------------------------------------
	/**
	 * Initializes this object with the given arguments.  
	 * The first subtree (if any) must represent the value of {@link #_period period}.
	 * The second subtree (if any) must represent the value of the {@link #_alpha smoothing constant}.
	 *
	 * @param args the arguments to be used to initialize this object. 
	 * @throws NumberFormatException 
	 *   if the first subtree is not a valid string representation of a long value,
	 *   or if the second subtree is not a valid string representation of a double value.
	 * @throws IllegalStateException
	 *   if the firts subtree does not represent a positive value 
	 *   or if the second subtree does not represent a value in the interval [0-1]. 
	 */
	public void initialize(final Tree args) { 
		//System.out.println("-- " + args.toString() + ", period=" + period + ", alpha=" + alpha);
		if (args.getSize() == 0)
			return;
		else if  (args.getSize() == 1){
			super.initialize(args);
			return;
		}
		super.initialize(args);
		_alpha = Double.valueOf(args.getSubTree(1).toString()).doubleValue();
		if ((_alpha < 0) || (_alpha > 1))
			throw new IllegalStateException("alpha=" + _alpha + " is invalid. The value must in the interval [0-1].");
		//System.out.println("-- " + args.toString() + ", period=" + period + ", alpha=" + alpha);
	}

	public double getRateOfMethodCall() {
		return _lastRateOfMethodCall = expSmoothing(super.getRateOfMethodCall(), _lastRateOfMethodCall);
	}

	public double getRateOfMethodSuccess() {
		return _lastRateOfMethodSuccess = expSmoothing(super.getRateOfMethodSuccess(), _lastRateOfMethodSuccess);
	}

	public void reset() {
		super.reset();
		_lastRateOfMethodCall = 0;
		_lastRateOfMethodSuccess = 0;
	}

	private double expSmoothing(double newRate, double lastRate) {
		return (_alpha * newRate) + (1 - _alpha) * lastRate;
	}
}

