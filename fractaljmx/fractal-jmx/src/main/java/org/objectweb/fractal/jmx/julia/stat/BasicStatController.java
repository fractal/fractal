/***
 * Fractal JMX
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.jmx.julia.stat;

import org.objectweb.fractal.julia.loader.Tree;
import org.objectweb.fractal.julia.loader.Initializable;

/** 
 * Simple implementation of {@link StatController}.
 * <UL>
 * <LI>The number of (intercepted) method calls, returned by this implementation, 
 * is incremented as usual each time a call is made on the server interfaces of the component 
 * (control interfaces excluded).
 * <LI>The rate of (intercepted) method calls, returned by this implementation, are re-computed every {@link #_period period}.
 * The value of this period must be positive and can be fixed by {@link #initialize initialize}.
 * </UL>
 * 
 * @version 0.1 
 */
public class BasicStatController implements StatController, Initializable {

	/**
	 * The minimal time interval used for rate re-computation, in milliseconds (default value is 5000).
	 * <p>Its value must be positive and can be fixed by {@link #initialize initialize}.
	 */
	protected long _period = 5000;

	/**
	 * Number of calls to preMethod, i.e., the total number of method calls.
	 */
	private int _calls;

	/**
	 * Number of calls to preMethod during the current sample.
	 */
	private int _callsCrtSample;

	/**
	 * Rate of calls to preMethod during the last sample.
	 */
	private double _rateOfMethodCall;

	/**
	 * Number of calls to postMethod, i.e., the total number of method success
	 * (since postMethod is not called inside a finally block - see
	 * StatCodeGenerator - it is not called if the intercepted method throws an
	 * exception).
	 */
	private int _success;

	/**
	 * Number of calls to postMethod during the current sample.
	 */
	private int _successCrtSample;

	/**
	 * Rate of calls to preMethod during the last sample.
	 */
	private double _rateOfMethodSuccess;

	/**
	 * Sample start time
	 */
	private long _startSample = System.currentTimeMillis();

	// -------------------------------------------------------------------------
	// implementation of the Initializable interface
	// -------------------------------------------------------------------------
	/**
	 * Initializes this object with the given arguments. 
	 * The first subtree (if any) must represent the value of {@link #_period period}.
	 *
	 * @param args the arguments to be used to initialize this object. 
	 * @throws NumberFormatException 
	 *   if the first subtree is not a valid string representation of a long value.
	 * @throws IllegalStateException if the firts subtree does not represent a positive value. 
	 */
	public void initialize(final Tree args) {
		//System.out.println("-- " + args.toString() + ", period=" + period);
		if (args.getSize() == 0)
			return;
		_period = Long.valueOf(args.getSubTree(0).toString()).longValue();
		if (_period < 1)
			throw new IllegalStateException("period=" + _period + " is invalid. The value must be positive.");
		//System.out.println("-- " + args.toString() + ", period=" + period);
	}

	// -------------------------------------------------------------------------
	// implementation of the StatController interface
	// -------------------------------------------------------------------------
	public synchronized void reset() {
		_calls = 0;
		_callsCrtSample = 0;
		_rateOfMethodCall = 0;
		_success = 0;
		_successCrtSample = 0;
		_rateOfMethodSuccess = 0;
		_startSample = System.currentTimeMillis();
	}

	public int getNumberOfMethodCall() {
		return _calls;
	}

	public int getNumberOfMethodSuccess() {
		return _success;
	}

	public double getRateOfMethodCall() {
		resetSample();
		return _rateOfMethodCall;
	}

	public double getRateOfMethodSuccess() {
		resetSample();
		return _rateOfMethodSuccess;
	}

	private synchronized void resetSample() {
		long t = System.currentTimeMillis();
		if (t < _startSample + _period)
			return;
		_rateOfMethodCall = (double)(_callsCrtSample * 1000) / (t - _startSample);
		_callsCrtSample = 0;
		_rateOfMethodSuccess = (double)(_successCrtSample * 1000) / (t - _startSample);
		_successCrtSample = 0;
		_startSample = t;
	}

	// -------------------------------------------------------------------------
	// methods called by the associated interceptor
	// -------------------------------------------------------------------------
	/**
	 * A preMethod called by the associated interceptor.
	 * This preMethod increments the number of method calls that have been made on the server
	 * interfaces of the component (control interfaces excluded).
	 * 
	 * @param method the method that is intercepted. 
	 */
	public void statPreMethod(final String method) {
		synchronized (this) {
			++_calls;
			++_callsCrtSample;
		}
	}

	/**
	 * A postMethod called by the associated interceptor.
	 * Since postMethod is not called inside a finally block - see
	 * StatCodeGenerator - it is not called if the intercepted method throws an
	 * exception.
	 * <P>This postMethod increments the number of method calls that have been made on the server
	 * interfaces of the component (control interfaces excluded), and that have
	 * "succeded", i.e., that have not thrown an exception.
	 * 
	 * @param method the method that is intercepted. 
	 */
	public void statPostMethod(final String method) {
		synchronized (this) {
			++_success;
			++_successCrtSample;
		}
	}
}

