/***
 * Fractal JMX
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.jmx.agent;

// JMX
import javax.management.ObjectName;
import javax.management.MBeanServer;
import javax.management.JMException;
import javax.management.MalformedObjectNameException;
import javax.management.monitor.MonitorMBean;
import javax.management.monitor.StringMonitor;
import javax.management.monitor.CounterMonitor;
import javax.management.monitor.GaugeMonitor;

import java.util.*;

/**
 * The context of the agent component (handles patterns and monitors).
 * 
 * @version 0.1
 */
public class AgentContext {
	private ObjectName[] _itfPatterns = new ObjectName[0];
	private ObjectName[] _monitorPatterns = new ObjectName[0];
	private AgentMonitorMBean[] _monitors = new AgentMonitorMBean[0];

	// -------------------------------------------------------------------------
	// public
	// -------------------------------------------------------------------------
	/**
	 * returns the current {@link AgentMonitorMBean monitors}.
	 * 
	 * @return the current {@link AgentMonitorMBean monitors}.
	 */
	public AgentMonitorMBean[] getMonitors() {
		return _monitors;
	}

	/**
	 * Checks if an {@link ObjectName} matches the current patterns for filtering component interface MBeans. 
	 * 
	 * @param oName the {@link ObjectName} to check.
	 * @return true if it matches, false otherwhise.
	 */
	public boolean matchItfPatterns(ObjectName oName) {
		if (matchPatterns(_itfPatterns, oName))
			return true;
		return false;
	}

	/**
	 * Checks if an {@link ObjectName} matches the current patterns for filtering monitor MBeans.
	 * 
	 * @param oName the {@link ObjectName} to check.
	 * @return true if it matches, false otherwhise.
	 */
	public boolean matchMonitorPatterns(ObjectName oName) {
		if (matchPatterns(_monitorPatterns, oName))
			return true;
		return false;
	}

	/**
	 * Resets this {@link Agent agent} context. 
	 * 
	 * <P>This method translates the {@link org.objectweb.fractal.jmx.agent.AdminAttributes agent attributes}, 
	 * in their corresponding {@link ObjectName} patterns and {@link AgentMonitorMBean monitors}.
	 *
	 * @param fc the agent attribute for filtering component interface MBeans.
	 * @param str the agent attribute for filtering {@link javax.management.monitor.StringMonitorMBean string monitor} MBeans.
	 * @param counter the agent attribute for filtering {@link javax.management.monitor.CounterMonitorMBean counter monitor} MBeans.
	 * @param gauge the agent attribute for filtering {@link javax.management.monitor.GaugeMonitorMBean gauge monitor} MBeans.
	 * 
	 * @throws JMException if a problem occurs during the translation.
	 */
	public void reset(String fc, String str, String counter, String gauge) throws JMException {
		cleanMonitors(_monitors);
		set(fc, str, counter, gauge);
	}

	// -------------------------------------------------------------------------
	// reset
	// -------------------------------------------------------------------------
	private void cleanMonitors(MonitorMBean[] monitors) {
		for (int i = 0; i < monitors.length; i++) {
			monitors[i].stop();
			ObjectName[] onames = monitors[i].getObservedObjects();
			for (int j = 0; j < onames.length; j++) {
				monitors[i].removeObservedObject(onames[j]);
			}
		}
	}

	private void set(String fc, String str, String counter, String gauge) throws JMException {
		// creates monitors
		AgentMonitorMBean[] stringMonitors = createStringMonitors(str);
		AgentMonitorMBean[] counterMonitors = createCounterMonitors(counter);
		AgentMonitorMBean[] gaugeMonitors = createGaugeMonitors(gauge);
		// merge monitors
		List l = new ArrayList(Arrays.asList(stringMonitors));
		l.addAll(Arrays.asList(counterMonitors));
		l.addAll(Arrays.asList(gaugeMonitors));
		_monitors = (AgentMonitorMBean[]) l.toArray(new AgentMonitorMBean[0]);
		// creates patterns
		_itfPatterns = createItfPatterns(fc);
		l = new ArrayList(Arrays.asList(getMonitorPatterns(_monitors)));
		_monitorPatterns = (ObjectName[]) l.toArray(new ObjectName[0]);
	}

	// -------------------------------------------------------------------------
	// patterns 
	// -------------------------------------------------------------------------
	private ObjectName[] createItfPatterns(String str) throws MalformedObjectNameException {
		String[] strTab = split(str, ";");
		ObjectName[] oNames = new ObjectName[strTab.length];
		for (int i = 0; i < strTab.length; i++) {
			try {
				oNames[i] = new ObjectName(strTab[i]);
			} catch (MalformedObjectNameException e) {
				throw new MalformedObjectNameException(
					"pattern \"" + strTab[i] + "\" is an invalid string representation of JMX object name");
			}
		}
		return oNames;
	}

	private boolean matchPatterns(ObjectName[] patterns, ObjectName oName) {
		for (int i = 0; i < patterns.length; i++) {
			if (patterns[i].apply(oName))
				return true;
		}
		return false;
	}

	private ObjectName[] getMonitorPatterns(AgentMonitorMBean[] monitors) {
		ObjectName[] oNames = new ObjectName[monitors.length];
		for (int i = 0; i < oNames.length; i++)
			oNames[i] = monitors[i].getPattern();
		return oNames;
	}


	// -------------------------------------------------------------------------
	// String monitor
	// -------------------------------------------------------------------------
	private AgentMonitorMBean[] createStringMonitors(String str) throws MalformedObjectNameException {
		String[] strTab = split(str, ";");
		//System.out.println("- " + Arrays.asList(strTab));
		MyStringMonitor[] gauges = new MyStringMonitor[strTab.length];
		for (int i = 0; i < strTab.length; i++) {
			try {
				ObjectName oName = new ObjectName(strTab[i]);
				Hashtable h = oName.getKeyPropertyList();
				gauges[i] = new MyStringMonitor((i + 1));
				// set String monitor params
				gauges[i].setObservedAttribute((String) remove(h, "observedAttribute"));
				gauges[i].setGranularityPeriod(Long.parseLong((String) remove(h, "granularityPeriod")));
				gauges[i].setStringToCompare((String) remove(h, "stringToCompare"));
				gauges[i].setNotifyDiffer(Boolean.valueOf((String) remove(h, "notifyDiffer")).booleanValue());
				gauges[i].setNotifyMatch(Boolean.valueOf((String) remove(h, "notifyMatch")).booleanValue());
				if (oName.isPropertyPattern())
					gauges[i].setPattern(new ObjectName(oName.getDomain() + ":" + asString(h) + ",*"));
				else
					gauges[i].setPattern(new ObjectName(oName.getDomain(), h));
			} catch (Exception e) {
				throw new IllegalStateException(
					"String monitor \"" + strTab[i] + "\" is invalid. " + e.toString() + ". " + CONST.STRING_SYNTAX);
			}
		}
		return gauges;
	}

	class MyStringMonitor extends StringMonitor implements AgentMonitorMBean {
		private ObjectName _pattern;
		private int _i;

		MyStringMonitor(int i) throws JMException {
			this._pattern = new ObjectName("*:*");
			this._i = i;
		}

		public ObjectName getPattern() {
			return _pattern;
		}

		public void setPattern(ObjectName pattern) {
			_pattern = pattern;
		}
		public Class getAttributeType() {
			return String.class;
		}

		public ObjectName preRegister(MBeanServer server, ObjectName name) throws Exception {
			super.preRegister(server, name);
			return new ObjectName(CONST.MONITOR + ":type=stringMonitor" + ",observer=" + _i);
		}
	}

	// -------------------------------------------------------------------------
	// Counter monitor
	// -------------------------------------------------------------------------
	private AgentMonitorMBean[] createCounterMonitors(String str) throws MalformedObjectNameException {
		String[] strTab = split(str, ";");
		MyCounterMonitor[] counters = new MyCounterMonitor[strTab.length * 4];
		for (int i = 0; i < strTab.length; i++) {
			try {
				ObjectName oName = new ObjectName(strTab[i]);
				Hashtable h = oName.getKeyPropertyList();
				int j = i * 4;
				counters[j] = new MyCounterMonitor((i + 1), Long.class);
				// set Counter monitor params
				counters[j].setObservedAttribute((String) remove(h, "observedAttribute"));
				counters[j].setGranularityPeriod(Long.parseLong((String) remove(h, "granularityPeriod")));
				counters[j].setInitThreshold(Long.valueOf((String) remove(h, "initThreshold")));
				counters[j].setNotify(true);
				counters[j].setModulus(Long.valueOf((String) remove(h, "modulus")));
				counters[j].setOffset(Long.valueOf((String) remove(h, "offset")));
				counters[j].setDifferenceMode(Boolean.valueOf((String) remove(h, "differenceMode")).booleanValue());
				if (oName.isPropertyPattern())
					counters[j].setPattern(new ObjectName(oName.getDomain() + ":" + asString(h) + ",*"));
				else
					counters[j].setPattern(new ObjectName(oName.getDomain(), h));
				counters[j + 1] = new MyCounterMonitor(counters[j], Integer.class);
				counters[j + 2] = new MyCounterMonitor(counters[j], Short.class);
				counters[j + 3] = new MyCounterMonitor(counters[j], Byte.class);
			} catch (Exception e) {
				throw new IllegalStateException(
					"Counter monitor \"" + strTab[i] + "\" is invalid. " + e.toString() + ". " + CONST.COUNTER_SYNTAX);
			}
		}
		return counters;
	}

	class MyCounterMonitor extends CounterMonitor implements AgentMonitorMBean {
		private ObjectName _pattern;
		private int _i;
		private Class _attType;

		MyCounterMonitor(int i, Class attType) throws JMException {
			this._pattern = new ObjectName("*:*");
			this._i = i;
			this._attType = attType;
		}

		MyCounterMonitor(MyCounterMonitor g, Class attType) throws MalformedObjectNameException {
			this._pattern = g.getPattern();
			this._i = g._i;
			this._attType = attType;
			setObservedAttribute(g.getObservedAttribute());
			setGranularityPeriod(g.getGranularityPeriod());
			setDifferenceMode(g.getDifferenceMode());
			setNotify(g.getNotify());
			if (attType.equals(Long.class)) {
				setInitThreshold(new Long(g.getInitThreshold().longValue()));
				setModulus(new Long(g.getModulus().longValue()));
				setOffset(new Long(g.getOffset().longValue()));
			} else if (attType.equals(Integer.class)) {
				setInitThreshold(new Integer(g.getInitThreshold().intValue()));
				setModulus(new Integer(g.getModulus().intValue()));
				setOffset(new Integer(g.getOffset().intValue()));
			} else if (attType.equals(Short.class)) {
				setInitThreshold(new Short(g.getInitThreshold().shortValue()));
				setModulus(new Short(g.getModulus().shortValue()));
				setOffset(new Short(g.getOffset().shortValue()));
			} else if (attType.equals(Byte.class)) {
				setInitThreshold(new Byte(g.getInitThreshold().byteValue()));
				setModulus(new Byte(g.getModulus().byteValue()));
				setOffset(new Byte(g.getOffset().byteValue()));
			} else
				throw new IllegalStateException(attType + " is an invalid counter threshold type");
		}

		public ObjectName getPattern() {
			return _pattern;
		}

		public void setPattern(ObjectName pattern) {
			_pattern = pattern;
		}

		public Class getAttributeType() {
			return _attType;
		}

		public ObjectName preRegister(MBeanServer server, ObjectName name) throws Exception {
			super.preRegister(server, name);
			return new ObjectName(
				CONST.MONITOR + ":type=counterMonitor" + ",observer=" + _i + ",attributeType=" + _attType.getName());
		}
	}

	// -------------------------------------------------------------------------
	// Gauge monitor
	// -------------------------------------------------------------------------
	private AgentMonitorMBean[] createGaugeMonitors(String str) throws MalformedObjectNameException {
		String[] strTab = split(str, ";");
		MyGaugeMonitor[] gauges = new MyGaugeMonitor[strTab.length * 6];
		for (int i = 0; i < strTab.length; i++) {
			try {
				ObjectName oName = new ObjectName(strTab[i]);
				Hashtable h = oName.getKeyPropertyList();
				int j = i * 4;
				gauges[j] = new MyGaugeMonitor((i + 1), Double.class);
				// set Gauge monitor params
				gauges[j].setObservedAttribute((String) remove(h, "observedAttribute"));
				gauges[j].setGranularityPeriod(Long.parseLong((String) remove(h, "granularityPeriod")));
				gauges[j].setNotifyLow(true);
				gauges[j].setNotifyHigh(true);
				gauges[j].setThresholds(
					Double.valueOf((String) remove(h, "highThreshold")),
					Double.valueOf((String) remove(h, "lowThreshold")));
				gauges[j].setDifferenceMode(Boolean.valueOf((String) remove(h, "differenceMode")).booleanValue());
				if (oName.isPropertyPattern())
					gauges[j].setPattern(new ObjectName(oName.getDomain() + ":" + asString(h) + ",*"));
				else
					gauges[j].setPattern(new ObjectName(oName.getDomain(), h));
				gauges[j + 1] = new MyGaugeMonitor(gauges[j], Float.class);
				gauges[j + 2] = new MyGaugeMonitor(gauges[j], Long.class);
				gauges[j + 3] = new MyGaugeMonitor(gauges[j], Integer.class);
				gauges[j + 4] = new MyGaugeMonitor(gauges[j], Short.class);
				gauges[j + 5] = new MyGaugeMonitor(gauges[j], Byte.class);
			} catch (Exception e) {
				throw new IllegalStateException(
					"Gauge monitor \"" + strTab[i] + "\" is invalid. " + e.toString() + ". " + CONST.GAUGE_SYNTAX);
			}
		}
		return gauges;
	}

	class MyGaugeMonitor extends GaugeMonitor implements AgentMonitorMBean {
		private ObjectName _pattern;
		private int _i;
		private Class _attType;

		MyGaugeMonitor(int i, Class attType) throws JMException {
			this._pattern = new ObjectName("*:*");
			this._i = i;
			this._attType = attType;
		}

		MyGaugeMonitor(MyGaugeMonitor g, Class attType) {
			this._pattern = g.getPattern();
			this._i = g._i;
			this._attType = attType;
			setObservedAttribute(g.getObservedAttribute());
			setGranularityPeriod(g.getGranularityPeriod());
			setDifferenceMode(g.getDifferenceMode());
			setNotifyLow(g.getNotifyLow());
			setNotifyHigh(g.getNotifyHigh());
			Number high = g.getHighThreshold();
			Number low = g.getLowThreshold();
			if (attType.equals(Double.class))
				setThresholds(new Double(high.doubleValue()), new Double(low.doubleValue()));
			else if (attType.equals(Float.class))
				setThresholds(new Float(high.floatValue()), new Float(low.floatValue()));
			else if (attType.equals(Long.class))
				setThresholds(new Long(high.longValue()), new Long(low.longValue()));
			else if (attType.equals(Integer.class))
				setThresholds(new Integer(high.intValue()), new Integer(low.intValue()));
			else if (attType.equals(Short.class))
				setThresholds(new Short(high.shortValue()), new Short(low.shortValue()));
			else if (attType.equals(Byte.class))
				setThresholds(new Byte(high.byteValue()), new Byte(low.byteValue()));
			else
				throw new IllegalStateException(attType + " is an invalid gauge threshold type");
		}

		public ObjectName getPattern() {
			return _pattern;
		}

		public void setPattern(ObjectName pattern) {
			_pattern = pattern;
		}

		public Class getAttributeType() {
			return _attType;
		}

		public ObjectName preRegister(MBeanServer server, ObjectName name) throws Exception {
			super.preRegister(server, name);
			return new ObjectName(
				CONST.MONITOR + ":type=gaugeMonitor" + ",observer=" + _i + ",attributeType=" + _attType.getName());
		}
	}

	// -------------------------------------------------------------------------
	// Java utils 
	// -------------------------------------------------------------------------
	private String[] split(String str, String regex) { // JDK1.4 !!!
		if (str == null || str.matches(" *"))
			return new String[0];
		return str.split(regex);
	}

	private Object remove(Hashtable h, String key) throws Exception {
		if (h.containsKey(key))
			return h.remove(key);
		throw new Exception("property \"" + key + "\" is missing");
	}

	private String asString(Hashtable h) {
		String str = "";
		for (Iterator i = h.entrySet().iterator(); i.hasNext();) {
			Map.Entry e = (Map.Entry) i.next();
			str += e.getKey() + "=" + e.getValue();
			if (i.hasNext())
				str += ",";
		}
		return str;
	}
}
