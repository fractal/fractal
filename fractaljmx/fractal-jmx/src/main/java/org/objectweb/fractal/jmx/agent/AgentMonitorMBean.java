/***
 * Fractal JMX
 * Copyright (C) 2003 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 */
package org.objectweb.fractal.jmx.agent;

import javax.management.ObjectName;
import javax.management.monitor.MonitorMBean;

/**
 * A customized {@link MonitorMBean} interface.
 * 
 * @version 0.1
 */
public interface AgentMonitorMBean extends MonitorMBean {

	/**
	 * Returns the type of attribute which can be observed by this {@link MonitorMBean}. 
	 * This is usefull when this monitor can handle several attribute types 
	 * (e.g. {@link javax.management.monitor.CounterMonitorMBean counter} and 
	 * {@link javax.management.monitor.GaugeMonitorMBean gauge} monitors handle several integer 
	 * or floating-point attribute types)
	 *
	 * @return the type of attribute which can be observed by this {@link MonitorMBean}.
	 */
	Class getAttributeType();

	/**
	 * Returns an {@link ObjectName} pattern which can be used for filtering MBeans. 
	 * Typically, only MBeans, whose {@link javax.management.ObjectName ObjectName} matches this pattern,
	 * must be added in the set of observed MBeans. 
	 *
	 * @return an {@link ObjectName} pattern.
	 */
	ObjectName getPattern();
}

