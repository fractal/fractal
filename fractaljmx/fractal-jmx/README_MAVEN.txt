**************************************************************************
* This fix is not required anymore
**************************************************************************

Some of the artifacts needed to build (and use) fractal-jmx are not, at the time
of the writing, on any public Maven repository (due to lincensing issues).

In order to fix things up those artifacts must be downloaded and installed
manually on the local maven repository:

1) Go to http://java.sun.com/products/JavaManagement/download.html and download the
reference implementation zip file whose version matches the javax.management:jmxri 
version indicated in the fractal-jmx project pom.xml file (i.e. 1.2.1)

2) unzip the downloaded zip archive

3) enter into the folder "jmx-1.2.1-bin"/lib . You should find 2 jars here:
jmxrmi and jmxtools

4) manually install only jmxtools : 
   
   mvn install:install-file -Dfile=jmxtools.jar -DgroupId=com.sun.jdmk -DartifactId=jmxtools -Dversion=1.2.1 -Dpackaging=jar

