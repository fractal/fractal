package comanche;

public class BasicLogger implements Logger, LoggerAttributes {

	private String header = "";

	// -------------------------------------------------------------------------
	// implements attribute controller   
	// -------------------------------------------------------------------------
	public String getHeader() {
		return header;
	}

	public void setHeader(final String header) {
		this.header = header;
	}

	// -------------------------------------------------------------------------
	// implements Logger interface   
	// -------------------------------------------------------------------------
	public void log(String msg) {
		System.out.println(header + " " + msg);
	}
}
