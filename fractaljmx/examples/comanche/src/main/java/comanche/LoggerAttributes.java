package comanche;

import org.objectweb.fractal.api.control.AttributeController;

public interface LoggerAttributes extends AttributeController {
	String getHeader ();
	void setHeader (String header);
}
