/**
 * Fractal Libraries
 * Copyright (C) 2009 Bull S.A.S.
 * Copyright (C) 2009 INRIA, Sardes
 * Contact: fractal@ow2.org

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * $Id$
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package org.ow2.fractal.julia.activator;

import java.util.Properties;

import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.julia.Julia;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * Register the Julia bootstrap factory as an OSGi service.
 * @author Loris Bouzonnet
 */
public class JuliaActivator implements BundleActivator {
	
	private ServiceRegistration registration;

	public void start(BundleContext context) throws Exception {
		Julia juliaFactory = new Julia();
		Properties props = new Properties();
		props.setProperty("fractal.provider.alias", "julia");
		props.setProperty("julia.config", "julia-osgi-context.cfg");
		registration = context.registerService(
				Factory.class.getName(), juliaFactory, props);
	}

	public void stop(BundleContext context) throws Exception {
		if (registration != null) {
			registration.unregister();
		}
	}

}
