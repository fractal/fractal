package org.ow2.fractal.rmi.osgi;

import java.net.URL;

import org.objectweb.fractal.rmi.registry.Registry;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class RegistryActivator implements BundleActivator {

	public void start(final BundleContext context) throws Exception {
		ClassLoader tccl = Thread.currentThread().getContextClassLoader();

        Thread.currentThread().setContextClassLoader(new ClassLoader(tccl) {
            @Override
            protected Class<?> findClass(String name) throws ClassNotFoundException {
                return context.getBundle().loadClass(name);
            }
            @Override
            protected URL findResource(String name) {
                return context.getBundle().getResource(name);
            }
        });
        try {
        	Registry.createRegistry(1234);
        } finally {
        	Thread.currentThread().setContextClassLoader(tccl);
        }
	}

	public void stop(BundleContext context) throws Exception {
		// TODO Auto-generated method stub

	}

}
