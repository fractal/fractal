/**
 * Fractal Libraries
 * Copyright (C) 2010 Bull S.A.S.
 * Copyright (C) 2010 INRIA, Sardes
 * Contact: fractal@ow2.org

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * $Id$
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */
package org.ow2.fractal.adl.osgi;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * @author Loris Bouzonnet
 */
public class ADLFactoryActivatator implements BundleActivator {

	private ServiceRegistration reg;
	
	public void start(BundleContext context) throws Exception {
		
		Factory adlFactory = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
		reg = context.registerService(Factory.class.getName(), adlFactory, null);
	}

	public void stop(BundleContext context) throws Exception {
		
		if (reg != null) {
			reg.unregister();
		}
	}

}
