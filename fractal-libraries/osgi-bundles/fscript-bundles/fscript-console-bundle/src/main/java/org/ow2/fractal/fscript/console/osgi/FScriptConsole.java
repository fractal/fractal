/**
 * Fractal Libraries
 * Copyright (C) 2010 Bull S.A.S.
 * Copyright (C) 2010 INRIA, Sardes
 * Contact: fractal@ow2.org

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * $Id$
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */
package org.ow2.fractal.fscript.console.osgi;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.fscript.FScript;
import org.objectweb.fractal.fscript.InvalidScriptException;
import org.osgi.framework.BundleContext;
import org.ow2.fractal.julia.osgi.api.control.OSGiContextController;

/**
 * @author Loris Bouzonnet
 */
public class FScriptConsole implements OSGiContextController, BindingController, LifeCycleController {
	
	private final static String FSCRIPT = "fscriptComp";
	
	private final static String[] ITFS = {FSCRIPT};
	
	private static Logger logger = Logger.getLogger(FScriptConsole.class.getName());
	
	private Component fscript;

	private BundleContext context;
	
	private OSGiConsole console;
    
	/**
     * The state of this Fractal component.
     */
    private String                 fcState = LifeCycleController.STOPPED;
    

    
	public String getFcState() {
		return fcState;
	}
	
	public void startFc() throws IllegalLifeCycleException {


        if (fcState.equals(LifeCycleController.STARTED)) {
            logger.log(Level.SEVERE,
                    "The FScript Console component is already started!");
            throw new IllegalLifeCycleException(
                    "The FScript Console component is already started!");
        }

        if (context == null) {
            logger.severe("Bundle context missing!!!");
            throw new IllegalStateException("Bundle context missing!!!");
        }

		try {
			FScript.loadStandardLibrary(fscript);
		} catch (InvalidScriptException e) {
			IllegalLifeCycleException ilce =
				new IllegalLifeCycleException("Error when loading the standard library.");
			ilce.initCause(e);
			throw ilce;
		} catch (NoSuchInterfaceException e) {
			IllegalLifeCycleException ilce =
				new IllegalLifeCycleException("Error when loading the standard library.");
			ilce.initCause(e);
			throw ilce;
		}
		console = new OSGiConsole(fscript, context);
        fcState = LifeCycleController.STARTED;
	}

	public void stopFc() throws IllegalLifeCycleException {
        if (fcState.equals(LifeCycleController.STOPPED)) {
            logger.log(Level.SEVERE,
                    "The FScript Console component is already stopped!");
            throw new IllegalLifeCycleException(
                    "The FScript Console component is already stopped!");
        }
        fcState = LifeCycleController.STOPPED;
		
	}

	public BundleContext getBundleContext() {
		return context;
	}

	public void setBundleContext(BundleContext context) {
		this.context = context;
	}

	public void bindFc(String clientItfName, Object serverItf)
			throws NoSuchInterfaceException, IllegalBindingException,
			IllegalLifeCycleException {
		
		if (clientItfName.equals(FSCRIPT)) {
			if (!(serverItf instanceof Component)) {
				throw new IllegalBindingException("Not a component: " + serverItf.toString());
			}
			fscript = (Component) serverItf;
		} else {
			throw new NoSuchInterfaceException(clientItfName);
		}
		
	}

	public String[] listFc() {
		return ITFS;
	}

	public Object lookupFc(String clientItfName)
			throws NoSuchInterfaceException {
		if (clientItfName.equals(FSCRIPT)) {
			return fscript;
		}
		throw new NoSuchInterfaceException(clientItfName);
	}

	public void unbindFc(String clientItfName) throws NoSuchInterfaceException,
			IllegalBindingException, IllegalLifeCycleException {
		
		if (clientItfName.equals(FSCRIPT)) {
			fscript = null;
		} else {
			throw new NoSuchInterfaceException(clientItfName);
		}
		
	}

}
