/**
 * Fractal Libraries
 * Copyright (C) 2010 Bull S.A.S.
 * Copyright (C) 2010 INRIA, Sardes
 * Contact: fractal@ow2.org

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * $Id$
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */
package org.ow2.fractal.fscript.console.osgi;

import java.util.ArrayList;
import java.util.List;

import jline.Completor;
import jline.MultiCompletor;

import org.objectweb.fractal.fscript.console.Command;
import org.osgi.service.command.CommandSession;
import org.osgi.service.command.Function;
import org.ow2.chameleon.shell.gogo.ICompletable;

/**
 * @author Loris Bouzonnet
 */
public class FScriptCommandWrapper implements Function, ICompletable {

	private final Command cmd;
	
	final List<Completor> completors = new ArrayList<Completor>();
	
	public FScriptCommandWrapper(Command cmd, final List<Completor> completors) {
		this.cmd = cmd;
		this.completors.add(new MultiCompletor(completors));
	}

	public Object execute(CommandSession session, List<Object> arguments)
			throws Exception {

		String args = "";
		for (Object arg : arguments) {
			args += arg.toString();
		}
		cmd.execute(args);
		return null;
	}

	public List<Completor> getCompletors() {
		return completors;
	}

}
