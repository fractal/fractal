/**
 * Fractal Libraries
 * Copyright (C) 2010-2011 Bull S.A.S.
 * Copyright (C) 2010-2011 INRIA, Sardes
 * Contact: fractal@ow2.org

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * $Id$
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */
package org.ow2.fractal.fscript.console.osgi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import jline.Completor;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.fscript.console.AbstractConsole;
import org.objectweb.fractal.fscript.console.Command;
import org.objectweb.fractal.fscript.console.PrefixCompletor;
import org.objectweb.fractal.fscript.model.Node;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.command.Function;
import com.google.common.base.Preconditions;

import static java.lang.Math.max;

/**
 * @author Loris Bouzonnet
 */
public class OSGiConsole extends AbstractConsole {

    private static final String                              OSGI_COMMAND_SCOPE    =
                                                                                           "osgi.command.scope";
    private static final String                              OSGI_COMMAND_FUNCTION =
                                                                                           "osgi.command.function";
	private final BundleContext bundleContext;

    private final List<Completor> completors = new ArrayList<Completor>();
	
	public OSGiConsole(Component fscript, BundleContext bundleContext) {
		
		super(fscript);
        this.completors.add(new AxisNameCompletor());
        this.completors.add(new VariableNameCompletor());
        this.completors.add(new ProcedureNameCompletor());
		this.bundleContext = bundleContext;
		for (Command cmd : commands.values()) {
			registerOSGiCmd(cmd);
		}
	}
	
	private void registerOSGiCmd(Command cmd) {
		Function function = new FScriptCommandWrapper(cmd, completors);
		
		// Prepare service properties
        final Hashtable<String, Object> props = new Hashtable<String, Object>();
        props.put(OSGI_COMMAND_SCOPE, "fscript");
        props.put(OSGI_COMMAND_FUNCTION, cmd.getName());

        final ServiceRegistration reg =
                bundleContext.registerService(Function.class.getName(), function,
                        props);
	}

    public void printString(String str) {
        System.out.print(str);
    }
    
	public void newline() {
		System.out.println();
	}
	
	public void printLine(String msg) {
		System.out.println(msg);
    }

	public void showMessage(String message) {
        printLine(message);
    }

    public void showWarning(String warning) {
        printLine("Warning: " + warning);
    }

    public void showError(String error) {
        printLine("Error: " + error);
    }

    public void showError(String error, Throwable cause) {
        printLine("Error: " + error);
        printLine("Error: " + cause.getMessage());
    }

    @SuppressWarnings("unchecked")
    public void showResult(Object result) {
        if (result instanceof Set<?>) {
            Set<Node> nodes = (Set<Node>) result;
            printLine("=> a node-set with " + nodes.size() + " element(s):");

            List<String> formattedNodes = new ArrayList<String>();
            for (Node node : nodes) {
                formattedNodes.add(node.toString());
            }
            Collections.sort(formattedNodes);
            for (String formattedNode : formattedNodes) {
            	printLine(formattedNode);
            }
        } else if (result instanceof String) {
            printLine("=> \"" + ((String) result).replaceAll("\\\"", "\\\\\\\"") + "\"");
        } else {
            printLine("=> " + String.valueOf(result));
        }
    }

    public void fill(char c, int width) {
        String str = Character.toString(c);
        for (int i = 0; i < width; i++) {
            printString(str);
        }
    }


    public void showTitle(String title) {
        newline();
        printLine(title);
        fill('=', title.length());
        newline();
        newline();
    }

    public void showTable(String[][] table) {
        new TableFormatter(this, table).print();
    }
    
    private static class TableFormatter {
        /**
         * How much space to put between columns of tabular results.
         */
        private static final int TABLE_PADDING = 4;

        private final OSGiConsole console;

        private final String[][] table;

        public TableFormatter(OSGiConsole console, String[][] table) {
            this.console = console;
            this.table = table;
            Preconditions.checkNotNull(table);
            Preconditions.checkArgument(table.length > 0, "No table header.");
        }
        
        public void print() {
            int[] columnsWidths = computeWidths(table, TABLE_PADDING);
            String[] headers = table[0];
            showTableRow(headers, columnsWidths);
            showSeparatorRow(columnsWidths);
            for (int i = 1; i < table.length; i++) {
                showTableRow(table[i], columnsWidths);
            }
        }
        
        private void showTableRow(String[] row, int[] columnsWidths) {
            assert row.length == columnsWidths.length;
            int i = 0;
            for (String cell : row) {
                String value = (cell == null) ? "null" : cell;
                console.printString(value);
                console.fill(' ', columnsWidths[i++] - value.length());
            }
            console.newline();
        }

        private void showSeparatorRow(int[] columnsWidths) {
            for (int width : columnsWidths) {
                console.fill('-', width);
            }
            console.newline();
        }

        private int[] computeWidths(String[][] table, int padding) {
            int[] widths = new int[table[0].length];
            // Compute the minimum width to hold the data
            for (String[] row : table) {
                int column = 0;
                for (String cell : row) {
                    int width = (cell != null) ? cell.length() : "null".length();
                    widths[column] = max(widths[column], width);
                    column++;
                }
            }
            // Add padding to all but the last column
            for (int i = 0; i < widths.length - 1; i++) {
                widths[i] += padding;
            }
            return widths;
        }
    }

    private class AxisNameCompletor extends PrefixCompletor {

        public AxisNameCompletor() {
            super('/');
        }

        @Override
        protected Set<String> getAllPossibleValues() {

            final Set<String> axes = getAxesNames();
            final Set<String> completions = new HashSet<String>();
            for (final String axis : axes) {
                completions.add(axis + "::");
            }
            return completions;
        }

    }

    private class VariableNameCompletor extends PrefixCompletor {

        public VariableNameCompletor() {
            super('$');
        }

        @Override
        protected Set<String> getAllPossibleValues() {
            return getGlobalVariablesNames();
        }

    }

    private class ProcedureNameCompletor extends PrefixCompletor {

        public ProcedureNameCompletor() {
            super('=','(');
        }

        @Override
        protected Set<String> getAllPossibleValues() {
        	
            Set<String> procNames = getModel().getAvailableProcedures();
            final Set<String> completions = new HashSet<String>();
            for (final String proc : procNames) {
                completions.add(proc + "(");
            }
            return completions;
        }

    }
    
}
