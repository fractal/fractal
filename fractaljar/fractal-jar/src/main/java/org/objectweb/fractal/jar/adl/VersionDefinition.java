/*
 * Copyright area
 */

package org.objectweb.fractal.jar.adl;

import org.objectweb.fractal.adl.Definition;

public interface VersionDefinition extends Definition {
  String getVersion ();
  void setVersion (String version);
}
