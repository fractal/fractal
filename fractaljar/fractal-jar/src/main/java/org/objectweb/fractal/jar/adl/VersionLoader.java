/*
 * Copyright area
 */

package org.objectweb.fractal.jar.adl;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.xml.XMLLoader;

public class VersionLoader extends XMLLoader {

  public Definition load (final String name, final Map context) 
    throws ADLException 
  {
    int i = name.indexOf('-');
    if (i > 0) {
      //return super.load(name.substring(i + 1) + '/' + name.substring(0, i), context);  
      return super.load(name.substring(0, i), context);
    } else {
      return super.load(name, context);
    }
  }
}
