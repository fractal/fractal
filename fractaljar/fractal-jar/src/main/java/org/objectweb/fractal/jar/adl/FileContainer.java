/*
 * Copyright area
 */

package org.objectweb.fractal.jar.adl;

public interface FileContainer {
  File[] getFiles();
  void addFile (File f);
  void removeFile (File f);
}
