/*
 * Copyright area
 */

package org.objectweb.fractal.jar;

import java.io.IOException;
import java.io.InputStream;

public interface PackageSource {
  InputStream getPackage (String pkg) throws IOException;
}
