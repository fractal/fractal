/*
 * Copyright area
 */

package org.objectweb.fractal.jar;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class PackageFile implements Serializable {

  String name;
  
  String version;
  
  Set packages; // list of Package that contain this file
  
  byte[] content;
  
  public PackageFile (String name, String version) {
    this.name = name;
    this.version = version;
    this.packages = new HashSet();
  }
  
  public String toString () {
    return name;
  }
}
