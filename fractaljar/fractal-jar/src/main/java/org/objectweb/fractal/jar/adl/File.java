/*
 * Copyright area
 */

package org.objectweb.fractal.jar.adl;

public interface File {
  String getName ();
  void setName (String name);
}
