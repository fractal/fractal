/*
 * Copyright area
 */

package org.objectweb.fractal.jar;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class LocalPackageSource implements PackageSource {

  private String src;
  
  public LocalPackageSource (String src) {
    this.src = src;
  }
  
  public InputStream getPackage (String pkg) throws IOException {
    return new FileInputStream(new File(src, pkg + ".far"));
  }
}
