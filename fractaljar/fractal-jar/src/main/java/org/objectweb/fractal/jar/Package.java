/*
 * Copyright area
 */

package org.objectweb.fractal.jar;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.components.ComponentDefinition;
import org.objectweb.fractal.adl.xml.XMLParser;

public class Package implements Serializable {

  String name;
  
  Map files; // associate name and version to PackageFile files
  
  List packageDependencies; // list of Package objects
  
  List dependentPackages; // list of Package objects
  
  public Package (String name) {
    this.name = name;
    this.files = new HashMap();
    this.packageDependencies = new ArrayList();
    this.dependentPackages = new ArrayList();
  }
  
  Set computeDependencies () throws Exception {
    int dash = name.indexOf('-');
    String n = name.substring(0, dash);
    String v = name.substring(dash + 1);
      
    Set s = new HashSet();
    getDependencies((PackageFile)files.get(
      v + '/' + n.replace('.', '/') + ".fractal"), s);

    return s;
  }
  
  private void getDependencies (PackageFile file, Set s) throws Exception {
    XMLParser p = new XMLParser();
    Node n = p.parse(new ByteArrayInputStream(file.content), file.name); 
    getDependencies(n, s);
  }
  
  private void getDependencies (Node n, Set s) throws Exception {
    if (n instanceof ComponentContainer) {
      Component[] components = ((ComponentContainer)n).getComponents();
      for (int i = 0; i < components.length; ++i) {
        getDependencies((Node)components[i], s);
      }
    }    
    if (n instanceof ComponentDefinition) {
      getDependencies(((ComponentDefinition)n).getExtends(), s);
    }
    if (n instanceof Component) {
      getDependencies(((Component)n).getDefinition(), s);
    }    
  }
  
  private void getDependencies (String definitions, Set s) throws Exception {
    if (definitions == null) {
      return;
    }
    int comma = definitions.indexOf(','); 
    if (comma != -1) {
      getDependencies(definitions.substring(0, comma), s);
      getDependencies(definitions.substring(comma + 1), s);
      return;
    }
    int dash = definitions.indexOf('-');
    String definition = definitions.substring(0, dash);
    String version = definitions.substring(dash + 1);

    PackageFile file = (PackageFile)files.get(
      version + '/' + definition.replace('.', '/') + ".fractal");
    
    if (file == null) {
      s.add(definitions);
    } else {
      getDependencies(file, s);
    }
  }
  
  public String toString () {
    return name;
  }
}
