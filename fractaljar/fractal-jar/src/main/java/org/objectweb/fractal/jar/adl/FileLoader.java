/*
 * Copyright area
 */

package org.objectweb.fractal.jar.adl;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.attributes.Attributes;
import org.objectweb.fractal.adl.attributes.AttributesContainer;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;

public class FileLoader extends AbstractLoader {
  
  // --------------------------------------------------------------------------
  // Implementation of the Loader interface
  // --------------------------------------------------------------------------
  
  public Definition load (final String name, final Map context) 
    throws ADLException 
  {
    Definition d = clientLoader.load(name, context);
    checkNode((Node)d);
    return d;
  }
  
  // --------------------------------------------------------------------------
  // Checking methods
  // --------------------------------------------------------------------------
 
  private void checkNode (Node node) throws ADLException {
    Set classes = new HashSet();
    Set files = new HashSet();
    getClasses(node, classes);
    getFiles(node, files);
    classes.removeAll(files);
    if (classes.size() > 0) {
      throw new ADLException(
        classes.iterator().next() + 
        " is missing in the component's files list", node);
    }    
    if (node instanceof ComponentContainer) {
      Component[] comps = ((ComponentContainer)node).getComponents();
      for (int i = 0; i < comps.length; i++) {
        checkNode((Node)comps[i]);
      }
    }
  }
  
  private void getClasses (Node n, Set s) {
    if (n instanceof InterfaceContainer) {
      Interface[] itfs = ((InterfaceContainer)n).getInterfaces();
      for (int i = 0; i < itfs.length; ++i) {
        if (itfs[i] instanceof TypeInterface) {
          addFile(((TypeInterface)itfs[i]).getSignature(), s);
        }
      }
    }
    if (n instanceof AttributesContainer) {
      Attributes a = ((AttributesContainer)n).getAttributes();
      if (a != null && a.getSignature() != null) {
        addFile(a.getSignature(), s);
      }
    }
    if (n instanceof ImplementationContainer) {
      Implementation i = ((ImplementationContainer)n).getImplementation();
      if (i != null) {
        addFile(i.getClassName(), s);
      }
    }
  }
  
  private static void addFile (String f, Set s) {
    if (!f.startsWith("java") && !f.startsWith("org/objectweb/fractal/api")) {
      s.add(f.replace('.', '/') + ".class");
    }
  }

  private void getFiles (Node n, Set s) {
    if (n instanceof FileContainer) {
      File[] files = ((FileContainer)n).getFiles();
      for (int i = 0; i < files.length; ++i) {
        s.add(files[i].getName());
      }
    }
  }
}
