/*
 * Copyright area
 */

package org.objectweb.fractal.jar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class PackageDatabase implements Serializable {
  
  private Map packages; // associates Package to package names

  private Map files; // associates PackageFile to file names and version 
  
  public PackageDatabase () {
    this.packages = new HashMap();
    this.files = new HashMap();
  }

  public List getPackages () throws Exception {
    return new ArrayList(packages.keySet());
  }

  private Package getPackage (String pkg) throws Exception {
    Package p = (Package)packages.get(pkg);
    if (p == null) {
      throw new Exception("No such package " + pkg);
    }
    return p;
  }
  
  public List getPackageFiles (String pkg) throws Exception {
    return new ArrayList(getPackage(pkg).files.values());
  }
  
  public List getPackageDependencies (String pkg) throws Exception {
    Set s = new HashSet();
    getPackageDependencies(getPackage(pkg), s);
    return new ArrayList(s);
  }
  
  private void getPackageDependencies (Package p, Set s) {
    Iterator i = p.packageDependencies.iterator();
    while (i.hasNext()) {
      Package q = (Package)i.next();
      s.add(q);
      getPackageDependencies(q, s);
    }
  }
  
  public List getDependentPackages (String pkg) throws Exception {
    Set s = new HashSet();
    getDependentPackages(getPackage(pkg), s);
    return new ArrayList(s);
  }
  
  private void getDependentPackages (Package p, Set s) {
    Iterator i = p.dependentPackages.iterator();
    while (i.hasNext()) {
      Package q = (Package)i.next();
      s.add(q);
      getDependentPackages(q, s);
    }
  }
  
  public void getPackage (String pkg, ZipOutputStream os) throws Exception {
    Iterator i = getPackage(pkg).files.values().iterator();
    while (i.hasNext()) {
      PackageFile file = (PackageFile)i.next();
      os.putNextEntry(new ZipEntry(file.version + '/' + file.name));
      os.write(file.content);
      os.closeEntry();
    }
  }
  
  public void getJAR (String pkg, ZipOutputStream os) throws Exception {
    Map m = new HashMap();
    getJARFiles(getPackage(pkg), m);
    
    Iterator i = m.values().iterator();
    while (i.hasNext()) {
      PackageFile file = (PackageFile)i.next();
      os.putNextEntry(new ZipEntry(file.name));
      os.write(file.content);
      os.closeEntry();      
    }
  }
  
  private void getJARFiles (Package p, Map m) throws Exception {
    Iterator i = p.files.values().iterator();
    while (i.hasNext()) {
      PackageFile file = (PackageFile)i.next();
      if (file.name.endsWith("MANIFEST.MF")) {
        continue;
      }
      if (m.get(file.name) != null) {
        PackageFile f = (PackageFile)m.get(file.name);
        if (!sameContent(file.content, f.content)) {
          throw new Exception(
            "Cannot create a JAR file because several versions of " + f.name + 
            " are needed");
        }
      } else {
        m.put(file.name, file);
      }
    }
    
    i = p.packageDependencies.iterator();
    while (i.hasNext()) {
      Package q = (Package)i.next();
      getJARFiles(q, m);
    }
  }
  
  private boolean sameContent (byte[] c, byte[] d) {
    if (c.length == d.length) {
      for (int i = 0; i < c.length; ++i) {
        if (c[i] != d[i]) {
          return false;
        }
      }
      return true;
    }
    return false;
  }
  
  public Package addPackage (String pkg, PackageSource source, Set s) throws Exception {
    if (packages.get(pkg) != null) {
      return getPackage(pkg);
    }
    
    Package p = new Package(pkg);
    packages.put(p.name, p);
    s.add(p);
    
    InputStream is = source.getPackage(pkg);
    ZipInputStream zis = new ZipInputStream(is);
    while (true) {
      ZipEntry e = zis.getNextEntry();
      if (e != null) {
        String n = e.getName();
        int dash = n.indexOf('/');
        String name;
        String version;
        if (dash == -1) {
          version = "";
          name = n;
        } else {
          version = n.substring(0, dash);
          name = n.substring(dash + 1);
        }
        PackageFile file = new PackageFile(name, version);
        file.content = readFile(zis);
        p.files.put(n, file);
        zis.closeEntry();

        if (!n.endsWith("MANIFEST.MF")) {
          file = addFile(file);
        }
        file.packages.add(p);
      } else {
        break;
      }
    }
    
    Iterator i = p.computeDependencies().iterator();
    while (i.hasNext()) {
      Package q = addPackage((String)i.next(), source, s);
      p.packageDependencies.add(q);
      q.dependentPackages.add(p);
    }
    
    return p;
  }
  
  private byte[] readFile (InputStream is) throws IOException {
    byte[] b = new byte[1000];
    int len = 0;
    while (true) {
      int n = is.read(b, len, b.length - len);
      if (n == -1) {
        if (len < b.length) {
          byte[] c = new byte[len];
          System.arraycopy(b, 0, c, 0, len);
          b = c;
        }
        return b;
      } else {
        len += n;
        if (len == b.length) {
          byte[] c = new byte[b.length + 1000];
          System.arraycopy(b, 0, c, 0, len);
          b = c;
        }
      }
    }
  }
  
  private PackageFile addFile (PackageFile file) throws Exception {
    String key = file.version + '/' + file.name;
    if (files.get(key) == null) {
      files.put(key, file);
      return file;
    } else {
      PackageFile f = (PackageFile)files.get(key);
      if (!sameContent(file.content, f.content)) {
        throw new Exception(
          "Cannot add package: the " + file.name + '-' + file.version + 
          " file content is not the same in the package and in the database");
      }
      return f;
    }
  }
  
  public void removePackage (String pkg) throws Exception {
    removePackage(getPackage(pkg));
  }
  
  public void removePackage (Package pkg) {
    // removes the files of the package
    Iterator i = pkg.files.values().iterator();
    while (i.hasNext()) {
      PackageFile file = (PackageFile)i.next();
      file.packages.remove(pkg);
      if (file.packages.size() == 0) {
        files.remove(file.version + '/' + file.name);
      }
    }

    // removes package dependencies
    i = pkg.packageDependencies.iterator();
    while (i.hasNext()) {
      Package q = (Package)i.next();
      q.dependentPackages.remove(pkg);
    }
    
    // recursively removes dependent packages
    i = new ArrayList(pkg.dependentPackages).iterator();
    while (i.hasNext()) {
      removePackage((Package)i.next());
    }
    
    packages.remove(pkg.name);
  }
  
  // -------------------------------------------------------------------------
  
  public static void main (String[] args) throws Exception {
    if (args.length < 3) {
      printUsage();
    }
    String database = args[0];
    String source = args[1];
    String op = args[2];
    
    PackageDatabase db;
    if (new File(database).exists()) {
      ObjectInputStream is = new ObjectInputStream(new FileInputStream(database));
      db = (PackageDatabase)is.readObject();
      is.close();
    } else {
      System.out.println("Database does not exist, a new one will be created");
      db = new PackageDatabase();
    }
    
    if (op.equals("-l")) {
      if (args.length > 3) {
        printUsage();
      }
      List l = db.getPackages();
      System.out.println("Available packages:");
      for (int i = 0; i < l.size(); ++i) {
        System.out.println("  " + l.get(i));
      }
    } else {
      if (args.length != 4) {
        printUsage();
      }
      String pkg = args[3];
      if (op.equals("-i")) {
        List l = db.getPackageFiles(pkg);
        System.out.println("Package files:");
        for (int i = 0; i < l.size(); ++i) {
          System.out.println("  " + l.get(i));
        }
        l = db.getPackageDependencies(pkg); 
        System.out.println("Package dependencies:");
        for (int i = 0; i < l.size(); ++i) {
          System.out.println("  " + l.get(i));
        }
        l = db.getDependentPackages(pkg); 
        System.out.println("Dependent packages:");
        for (int i = 0; i < l.size(); ++i) {
          System.out.println("  " + l.get(i));
        }
      } else if (op.equals("-x")) {
        FileOutputStream fos = new FileOutputStream(pkg + ".far");
        ZipOutputStream zos = new ZipOutputStream(fos);
        db.getPackage(pkg, zos);
        zos.close();
      } else if (op.equals("-j")) {
        FileOutputStream fos = new FileOutputStream(pkg + ".jar");
        ZipOutputStream zos = new ZipOutputStream(fos);
        db.getJAR(pkg, zos);
        zos.close();
      } else if (op.equals("-a")) {
        Set s = new HashSet();
        db.addPackage(pkg, new LocalPackageSource(source), s);
        System.out.println("The following packages need to be added:");
        Iterator i = s.iterator();
        while (i.hasNext()) {
          System.out.println("  " + i.next());
        }
        // System.out.println("Do you want to continue [y/N]?"); TODO        
        ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(database));
        os.writeObject(db);
        os.close();
      } else if (op.equals("-r")) {
        List l = db.getDependentPackages(pkg);
        l.add(db.getPackage(pkg));
        System.out.println("The following packages need to be removed:");
        for (int i = 0; i < l.size(); ++i) {
          System.out.println("  " + l.get(i));
        }
        // System.out.println("Do you want to continue [y/N]?"); TODO
        db.removePackage(pkg);
        ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(database));
        os.writeObject(db);
        os.close();
      } else {
        printUsage();
      }
    }
  }
  
  private static void printUsage () {
    System.err.println("Usage: <database> <package source> -l|-i|-x|-j|-a|-r [<pkg>]");
    System.exit(0);
  }
}
