/*
 * Copyright area
 */

package org.objectweb.fractal.jar;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.Attribute;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.Type;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.Parser;
import org.objectweb.fractal.adl.ParserException;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.components.ComponentDefinition;
import org.objectweb.fractal.adl.xml.XMLParser;

import org.objectweb.fractal.jar.adl.File;
import org.objectweb.fractal.jar.adl.FileContainer;
import org.objectweb.fractal.jar.adl.FileLoader;
import org.objectweb.fractal.jar.adl.VersionDefinition;

public class CreatePackage {

  String src;

  String dst;
  
  String definition;
    
  boolean recursive;
  
  Set excludes;
  
  Set content;

  Set classes;
  
  Set dependencies;
  
  Set libraryDependencies;
  
  Loader loader;
  
  public CreatePackage (
    String src,
    String dst,
    String definition,
    boolean recursive,
    Set excludes)
  {
    this.src = src;
    this.dst = dst;
    this.definition = definition;
    this.recursive = recursive;
    this.excludes = excludes;
    this.content = new HashSet();
    this.classes = new HashSet();
    this.dependencies = new HashSet();
    this.libraryDependencies = new HashSet();
  }
  
  public void create () throws Exception {
    FileLoader fl = new FileLoader();
    fl.clientLoader = new XMLLoader();
    loader = fl;

    Definition d = loader.load(definition, null);
    String version = ((VersionDefinition)d).getVersion();
    
    String f = definition + "-" + version + ".far";
    OutputStream os = new FileOutputStream(new java.io.File(dst, f));
    ZipOutputStream zos = new ZipOutputStream(os);
    
    zos.putNextEntry(new ZipEntry("META-INF/MANIFEST.MF"));
    zos.write("Manifest-Version: 1.0\n".getBytes());
    zos.write(("Component-Name: " + definition + "-" + version + "\n").getBytes());
    zos.closeEntry();

    addFile(definition.replace('.', '/') + ".fractal", version, zos, true);
    addFiles((Node)d, version, zos, true);
    
    zos.close();
    
    dependencies.removeAll(classes);
    if (dependencies.size() > 0) {
      Iterator i = dependencies.iterator();
      System.out.println(
        "WARNING: the following classes are needed by the classes " +
        "listed in the ADL files, but they are not listed in the ADL files");
      while (i.hasNext()) {
        System.out.println("  " + i.next());
      }
    }
  }
  
  private void addFiles (Node n, String version, ZipOutputStream z, boolean add) throws Exception {
    if (n instanceof FileContainer) {
      File[] files = ((FileContainer)n).getFiles();
      for (int i = 0; i < files.length; ++i) {
        try {
          addFile(files[i].getName(), version, z, add);
        } catch (Exception e) {
          throw new ADLException("Cannot add file", (Node)files[i], e);
        }
      }
    }
    if (n instanceof ComponentContainer) {
      Component[] components = ((ComponentContainer)n).getComponents();
      for (int i = 0; i < components.length; ++i) {
        addFiles((Node)components[i], version, z, add);
      }
    }
    
    if (n instanceof ComponentDefinition) {
      try {
        addDefinitions(((ComponentDefinition)n).getExtends(), z, recursive);
      } catch (Exception e) {
        throw new ADLException("Cannot read or add inherited files", n, e);
      }
    }
    if (n instanceof Component) {
      try {
        addDefinitions(((Component)n).getDefinition(), z, recursive);
      } catch (Exception e) {
        throw new ADLException("Cannot read or add referenced files", n, e);
      }
    }
  }

  private void addFile (String f, String version, ZipOutputStream z, boolean add) throws Exception {
    if (add) {
      String entry = version + '/' + f;
      if (content.contains(entry)) {
        return;
      } else {
        content.add(entry);
      }
      InputStream is = new FileInputStream(new java.io.File(src, f));
      z.putNextEntry(new ZipEntry(entry));
      
      byte[] b = new byte[Math.min(100000, is.available())];
      while (true) {
        int n = is.read(b, 0, b.length);
        if (n == -1) {
          break;
        } else {
          z.write(b, 0, n);
        }
      }
      
      z.closeEntry();
    }
    
    if (f.endsWith(".class")) {
      classes.add(f);
      InputStream is = new FileInputStream(new java.io.File(src, f));
      new ClassReader(is).accept(new DependencyClassVisitor(), ClassReader.SKIP_DEBUG);
    }
  }
  
  private void addDefinitions (String definitions, ZipOutputStream z, boolean add) 
    throws Exception 
  {
    if (definitions == null) {
      return;
    }
    int comma = definitions.indexOf(','); 
    if (comma != -1) {
      addDefinitions(definitions.substring(0, comma), z, add);
      addDefinitions(definitions.substring(comma + 1), z, add);
      return;
    }
    int dash = definitions.indexOf('-');
    String definition = definitions.substring(0, dash);
    String version = definitions.substring(dash + 1);
    
    if (!excludes.contains(definition)) {
      Definition d = loader.load(definition, null);
      if (!((VersionDefinition)d).getVersion().equals(version)) {
        throw new Exception("The declared version does not match the requested version");
      }
      addFile(definition.replace('.', '/') + ".fractal", version, z, add);
      addFiles((Node)d, version, z, add);
    }
  }
  
  // -------------------------------------------------------------------------

  class XMLLoader implements Loader {

    private Parser parser = new XMLParser(true);
    
    public Definition load (final String name, final Map context) 
      throws ADLException 
    {
      try {
        String file = name.replace('.', '/') + ".fractal";
        InputStream is;
        try {
          is = new FileInputStream(new java.io.File(src, file));
        } catch (IOException e) {
          throw new ADLException(
            "Cannot find file " + src + '/' + file, null, e);
        }
        Definition d = (Definition)parser.parse(is, file);
        if (d.getName() == null) {
          throw new ADLException("Definition name missing", (Node)d);
        }
        if (!d.getName().equals(name)) {
          throw new ADLException(
            "Wrong definition name ('" + name + 
            "' expected, instead of '" + d.getName() + "')", (Node)d);
        }
        return d;
      } catch (ParserException e) {
        throw new ADLException("Cannot load '" + name + "'", null, e);
      }
    }
  }
  
  // -------------------------------------------------------------------------
  
  void addClassDependency (String s) {
    if (s.startsWith("java") || s.startsWith("org/objectweb/fractal/api")) {
      libraryDependencies.add(s + ".class");
    } else {
      dependencies.add(s + ".class");
    }
  }

  void addTypeDependency (Type type) {
    if (type.getSort() == Type.ARRAY) {
      addTypeDependency(type.getElementType());
    } else if (type.getSort() == Type.OBJECT) {
      addClassDependency(type.getClassName().replace('.', '/'));
    }
  }

  void addTypeDependency (Type[] types) {
    for (int i = 0; i < types.length; ++i) {
      addTypeDependency(types[i]);
    }
  }

  class DependencyClassVisitor implements ClassVisitor, MethodVisitor {

    public void visit (
      final int version,
      final int access,
      final String name,
      final String signature,
      final String superName,
      final String[] interfaces)
    {
      addClassDependency(superName);
      if (interfaces != null) {
        for (int i = 0; i < interfaces.length; ++i) {
          addClassDependency(interfaces[i]);
        }
      }
    }

    public AnnotationVisitor visitAnnotation (
      final String desc,
      final boolean visible)
    {
      // does nothing
      return null;
    }

    public void visitSource (
      final String source,
      final String debug)
    {
      // does nothing
    }

    public void visitInnerClass (
      final String name,
      final String outerName,
      final String innerName,
      final int access)
    {
    }

    public void visitOuterClass (
      final String owner,
      final String name,
      final String desc)
    {
      // does nothing
    }

    public FieldVisitor visitField (
      final int access,
      final String name,
      final String desc,
      final String signature,
      final Object value)
    {
      addTypeDependency(Type.getType(desc));
      return null;
    }

    public MethodVisitor visitMethod (
      final int access,
      final String name,
      final String desc,
      final String signature,
      final String[] exceptions)
    {
      addTypeDependency(Type.getArgumentTypes(desc));
      addTypeDependency(Type.getReturnType(desc));
      if (exceptions != null) {
        for (int i = 0; i < exceptions.length; ++i) {
          addClassDependency(exceptions[i]);
        }
      }
      return this;
    }

    public void visitAttribute (final Attribute attr) {
    }

    public void visitEnd () {
    }
    
    public AnnotationVisitor visitAnnotationDefault () {
      return null;
    }

    public AnnotationVisitor visitParameterAnnotation (
      final int parameter,
      final String desc,
      final boolean visible) {
      return null;
    }

    public void visitCode () {
    }

    public void visitInsn (final int opcode) {
    }

    public void visitIntInsn (final int opcode, final int operand) {
    }

    public void visitVarInsn (final int opcode, final int var) {
    }

    public void visitTypeInsn (final int opcode, final String desc) {
      if (desc.charAt(0) == '[') {
        addTypeDependency(Type.getType(desc));
      } else {
        addClassDependency(desc);
      }
    }

    public void visitFieldInsn (
      final int opcode,
      final String owner,
      final String name,
      final String desc)
    {
      addClassDependency(owner);
      addTypeDependency(Type.getType(desc));
    }

    public void visitMethodInsn (
      final int opcode,
      final String owner,
      final String name,
      final String desc)
    {
      addClassDependency(owner);
      addTypeDependency(Type.getArgumentTypes(desc));
      addTypeDependency(Type.getReturnType(desc));
    }

    public void visitJumpInsn (final int opcode, final Label label) {
    }

    public void visitLabel (final Label label) {
    }

    public void visitLdcInsn (final Object cst) {
    }

    public void visitIincInsn (final int var, final int increment) {
    }

    public void visitTableSwitchInsn (
      final int min,
      final int max,
      final Label dflt,
      final Label[] labels)
    {
    }

    public void visitLookupSwitchInsn (
      final Label dflt,
      final int[] keys,
      final Label[] labels)
    {
    }

    public void visitMultiANewArrayInsn (final String desc, final int dims) {
      addTypeDependency(Type.getType(desc));
    }

    public void visitTryCatchBlock (
      final Label start,
      final Label end,
      final Label handler,
      final String type)
    {
      addClassDependency(type);
    }

    public void visitMaxs (final int maxStack, final int maxLocals) {
    }

    public void visitLocalVariable (
      final String name,
      final String desc,
      final String signature,
      final Label start,
      final Label end,
      final int index)
    {
    }

    public void visitLineNumber (final int line, final Label start) {
    }

	public void visitFrame(int arg0, int arg1, Object[] arg2, int arg3, Object[] arg4) {
	}
  }

  // -------------------------------------------------------------------------

  public static void main (String[] args) throws Exception {
    if (args.length < 3) {
      printUsage();
    }
    boolean recursive = false;
    Set excludes = new HashSet();
    for (int i = 3; i < args.length; ++i) {
      if (args[i].equals("-r")) {
        recursive = true;
      } else if (args[i].equals("-e")) {
        if (i < args.length - 1) {
          excludes.add(args[++i]);
        } else {
          printUsage();
        }
      } else {
        printUsage();
      }
    }
    new CreatePackage(args[0], args[1], args[2], recursive, excludes).create();
  }
  
  private static void printUsage () {
    System.err.println("Usage: <src> <dst> <definition> [-r] [-e xxx]*");
    System.exit(0);
  }
}
