/*
 * Copyright area
 */

import java.io.File;

import org.objectweb.fractal.jar.CreatePackage;
import org.objectweb.fractal.jar.PackageDatabase;

public class Helloworld {

  public static void main (String[] args) throws Exception {
    new File("database.ser").delete();
    
    System.out.println("CREATE lib.ClientServerImpl package");
    CreatePackage.main(new String[] {
//      "build",
//      ".",
      "lib.ClientServerImpl",
      "-r",
      "-e",
      "lib.ServerImpl"
    });

    System.out.println("CREATE api.ServerType package");
    CreatePackage.main(new String[] {
//      "build",
//      ".",
      "api.ServerType",
      "-r"
    });

    System.out.println("CREATE lib.ServerImpl package");
    CreatePackage.main(new String[] {
//      "build",
//      ".",
      "lib.ServerImpl"
    });
    
    System.out.println();
    System.out.println("ADD lib.ClientServerImpl-1.0 to the database");
    PackageDatabase.main(new String[] {
      "database.ser",
      ".",
      "-a",
      "lib.ClientServerImpl-1.0"
    });
    
    System.out.println();
    System.out.println("LIST the available packages in the database");
    PackageDatabase.main(new String[] {
      "database.ser",
      ".",
      "-l"
    });

    System.out.println();
    System.out.println("INFORMATION about the lib.ClientServerImpl-1.0 package");
    PackageDatabase.main(new String[] {
      "database.ser",
      ".",
      "-i",
      "lib.ClientServerImpl-1.0"
    });

    System.out.println();
    System.out.println("INFORMATION about the lib.ServerImpl-2.0 package");
    PackageDatabase.main(new String[] {
      "database.ser",
      ".",
      "-i",
      "lib.ServerImpl-2.0"
    });
    
    System.out.println();
    System.out.println("create a JAR file for the lib.ClientServerImpl-1.0 package");
    PackageDatabase.main(new String[] {
      "database.ser",
      ".",
      "-j",
      "lib.ClientServerImpl-1.0"
    });
    
    System.out.println();
    System.out.println("REMOVE the api.ServerType-1.0 package");
    PackageDatabase.main(new String[] {
      "database.ser",
      ".",
      "-r",
      "api.ServerType-1.0"
    });
    
    System.out.println();
    System.out.println("LIST the available packages in the database");
    PackageDatabase.main(new String[] {
      "database.ser",
      ".",
      "-l"
    });
  }
}
