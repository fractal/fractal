This module is the parent module of the Fractal ADL.

In includes among others:
* the "fractal-adl" module itself
* the "task-framework" module which contains interfaces and abstract classes
* the "examples" module, containing a sub module for each example (i.e. helloworld)
* the "maven-fractaladl-plugin" to easy running Fractal applications using the ADL

        

