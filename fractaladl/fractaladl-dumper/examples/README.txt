This module contains some examples of the Fractal ADL Dumper.

Each example can be run performing the following steps:
 1. mvn clean       (to clean up the code)
 
 2. mvn -Pgenerate  (to generate the ADL representation of the Fractal application)
   --> The generated .fractal file(s) will be under the "target/classes" dir        
   
 3. mvn -Prun       (to run the ADL generated at point [2] )