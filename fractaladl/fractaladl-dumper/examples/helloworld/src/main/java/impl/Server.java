package impl;

import itf.Service;
import itf.ServiceAttributes;


public class Server implements Service, ServiceAttributes
{

	private String header;
	
	private int count;
	
	// -----------------------------------------------------
	// Constructor without parameters
	// -----------------------------------------------------

    public Server()
    {
    }
        
	// -----------------------------------------------------
	// Implementation of the MyService interface 'service'
	// -----------------------------------------------------
		
	public void print(String arg0)  {
		System.out.println(header + " " + arg0);
		
	}
	
	// -----------------------------------------------------
	// Implementation of the AttributeController interface
	// -----------------------------------------------------

	public String getHeader(){
		return this.header;
	}

	public void setHeader(String value) {
		this.header = value;
	}

	public int getCount(){
		return this.count;
	}

	public void setCount(int value) {
		this.count = value;
	}

}

