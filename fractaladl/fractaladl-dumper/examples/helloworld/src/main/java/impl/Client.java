package impl;

import itf.Service;
import java.util.ArrayList;
import java.util.List;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;


public class Client implements BindingController, Runnable
{

	private Service service;
	
	// -----------------------------------------------------
	// Constructor without parameters
	// -----------------------------------------------------

    public Client()
    {
    }
        
	// -----------------------------------------------------
	// Implementation of the Runnable interface 'main'
	// -----------------------------------------------------
		
	public void run()  {
		service.print("Hello World !");
		
	}
	
	// -----------------------------------------------------
	// Implementation of the BindingController interface
	// -----------------------------------------------------

	public String[] listFc() {
		List<Object>  _interfaces_ = new ArrayList<Object>();
		_interfaces_.add("service");
        return (String[])_interfaces_.toArray(new String[]{});
	}

	public Object lookupFc(final String clientItfName) throws NoSuchInterfaceException {
		if (clientItfName.equals("service")) {
			return service;
		}
		throw new NoSuchInterfaceException("Client interface '" + clientItfName + "' is undefined.");
	}

	public void bindFc(final String clientItfName, final Object serverItf) throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException {
		if (clientItfName.equals("service")) {
			service = (Service) serverItf;
			return;
		}
		throw new NoSuchInterfaceException("Client interface '" + clientItfName + "' is undefined.");
	}

	public void unbindFc(final String clientItfName) throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException {
		if (clientItfName.equals("service")) {
			service = null;
			return;
		}
		throw new NoSuchInterfaceException("Client interface '" + clientItfName + "' is undefined.");
	}
}

