/***
 * Fractal ADL Dumper
 * Copyright (C) 2002-2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Marc L�ger
 */

package launch;

import org.objectweb.fractal.adl.dumper.SaxAdlGenerator;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.util.Fractal;

public class TestClientServer {

	public static void main(String[] args) {
		try {
			final Component cs = Create();
			SaxAdlGenerator generator = new SaxAdlGenerator(
					SaxAdlGenerator.STANDARD_DTD, args[0]);
			generator.generateDefinition(cs, Boolean.parseBoolean(args[2]));
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}
	
	public static Component Create() {
		
		System.setProperty("fractal.provider",
		"org.objectweb.fractal.julia.Julia");

		
		// NOT REQUIRED WITH JULIA >= 2.5.1
		//String configFile = "../etc/julia.cfg";
		//String loader = "org.objectweb.fractal.julia.loader.DynamicLoader";
		//System.setProperty("julia.loader", loader);
		//System.setProperty("julia.config", configFile);
		try {
			Component boot = Fractal.getBootstrapComponent();
			TypeFactory typeFactory = (TypeFactory) boot
					.getFcInterface("type-factory");
			GenericFactory genericFactory = (GenericFactory) boot
					.getFcInterface("generic-factory");
			InterfaceType IT0 = typeFactory.createFcItfType("main",
					"java.lang.Runnable", false, false, false);
			InterfaceType IT1 = typeFactory.createFcItfType("service",
					"itf.Service", true, false, false);
			ComponentType CT0 = typeFactory.createFcType(new InterfaceType[] {
					IT0, IT1 });
			Component C0 = genericFactory.newFcInstance(CT0, "primitive",
					"impl.Client");
			try {
				Fractal.getNameController(C0).setFcName("client");
			} catch (NoSuchInterfaceException ignored) {
			}
			InterfaceType IT2 = typeFactory.createFcItfType("service",
					"itf.Service", false, false, false);
			InterfaceType IT3 = typeFactory.createFcItfType(
					"attribute-controller", "itf.ServiceAttributes", false,
					false, false);
			ComponentType CT1 = typeFactory.createFcType(new InterfaceType[] {
					IT2, IT3 });
			Component C1 = genericFactory.newFcInstance(CT1, "primitive",
					"impl.Server");
			try {
				Fractal.getNameController(C1).setFcName("server");
			} catch (NoSuchInterfaceException ignored) {
			}
			((itf.ServiceAttributes) Fractal.getAttributeController(C1))
					.setHeader("start");
			((itf.ServiceAttributes) Fractal.getAttributeController(C1))
					.setCount(1);
			InterfaceType IT4 = typeFactory.createFcItfType("main",
					"java.lang.Runnable", false, false, false);
			ComponentType CT2 = typeFactory
					.createFcType(new InterfaceType[] { IT4 });
			Component C2 = genericFactory.newFcInstance(CT2, "composite", null);
			try {
				Fractal.getNameController(C2).setFcName("ClientServer");
			} catch (NoSuchInterfaceException ignored) {
			}
			Fractal.getContentController(C2).addFcSubComponent(C0);
			try {
				Fractal.getNameController(C0).setFcName("client");
			} catch (NoSuchInterfaceException ignored) {
			}
			Fractal.getContentController(C2).addFcSubComponent(C1);
			try {
				Fractal.getNameController(C1).setFcName("server");
			} catch (NoSuchInterfaceException ignored) {
			}
			Fractal.getBindingController(C2).bindFc("main",
					C0.getFcInterface("main"));
			Fractal.getBindingController(C0).bindFc("service",
					C1.getFcInterface("service"));
			return C2;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
