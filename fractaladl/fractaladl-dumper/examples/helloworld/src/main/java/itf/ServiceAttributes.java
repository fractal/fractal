package itf;


import org.objectweb.fractal.api.control.AttributeController;

public interface ServiceAttributes extends AttributeController {
  String getHeader ();
  void setHeader (String header);
  int getCount ();
  void setCount (int count);
}
