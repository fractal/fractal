package itf;

import impl.Request;

public interface RequestHandler {
  void handleRequest (Request r) throws java.io.IOException;
}
