package itf;

public interface Scheduler {
  void schedule (Runnable task);
}
