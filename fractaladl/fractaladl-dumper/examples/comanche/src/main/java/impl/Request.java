package impl;
import java.io.PrintStream;
import java.io.Reader;
import java.net.Socket;

public class Request {
  public Socket s;
  public Reader in;
  public PrintStream out;
  public String url;
  public Request (Socket s) { this.s = s; }
}
