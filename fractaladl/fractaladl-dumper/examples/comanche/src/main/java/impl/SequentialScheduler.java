package impl;

import itf.Scheduler;

public class SequentialScheduler implements Scheduler {
  public void schedule (Runnable task) { task.run(); }
}
