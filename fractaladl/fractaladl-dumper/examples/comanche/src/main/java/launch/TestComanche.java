/***
 * Fractal ADL Dumper
 * Copyright (C) 2002-2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Marc L�ger
 */

package launch;

import org.objectweb.fractal.adl.dumper.SaxAdlGenerator;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.util.Fractal;


public class TestComanche {
	
	public static void main(String[] args) {
		try {
			final Component cs = Create();
			SaxAdlGenerator generator = new SaxAdlGenerator(
					SaxAdlGenerator.STANDARD_DTD, args[0]);
			generator.generateDefinition(cs, Boolean.parseBoolean(args[2]));
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}

	public static Component Create() {
		
		// NO MORE NEEDED WITH Julia >= 2.5.1
		System.setProperty("fractal.provider",
		"org.objectweb.fractal.julia.Julia");

		//String configFile = "../etc/julia.cfg";
		//String loader = "org.objectweb.fractal.julia.loader.DynamicLoader";
		//System.setProperty("julia.loader", loader);
		//System.setProperty("julia.config", configFile);
		try {
			Component boot = Fractal.getBootstrapComponent();
			TypeFactory typeFactory = (TypeFactory) boot
					.getFcInterface("type-factory");
			GenericFactory genericFactory = (GenericFactory) boot
					.getFcInterface("generic-factory");
			InterfaceType IT0 = typeFactory.createFcItfType("r",
					"java.lang.Runnable", false, false, false);
			InterfaceType IT1 = typeFactory.createFcItfType("rh",
					"itf.RequestHandler", true, false, false);
			InterfaceType IT2 = typeFactory.createFcItfType("s",
					"itf.Scheduler", true, false, false);
			ComponentType CT0 = typeFactory.createFcType(new InterfaceType[] {
					IT0, IT1, IT2 });
			Component C0 = genericFactory.newFcInstance(CT0, "primitive",
					"impl.RequestReceiver");
			try {
				Fractal.getNameController(C0).setFcName("rr");
			} catch (NoSuchInterfaceException ignored) {
			}
			InterfaceType IT3 = typeFactory.createFcItfType("s",
					"itf.Scheduler", false, false, false);
			ComponentType CT1 = typeFactory
					.createFcType(new InterfaceType[] { IT3 });
			Component C1 = genericFactory.newFcInstance(CT1, "primitive",
					"impl.MultiThreadScheduler");
			try {
				Fractal.getNameController(C1).setFcName("s");
			} catch (NoSuchInterfaceException ignored) {
			}
			InterfaceType IT4 = typeFactory.createFcItfType("r",
					"java.lang.Runnable", false, false, false);
			InterfaceType IT5 = typeFactory.createFcItfType("rh",
					"itf.RequestHandler", true, false, false);
			ComponentType CT2 = typeFactory.createFcType(new InterfaceType[] {
					IT4, IT5 });
			Component C2 = genericFactory.newFcInstance(CT2, "composite", null);
			try {
				Fractal.getNameController(C2).setFcName("fe");
			} catch (NoSuchInterfaceException ignored) {
			}
			Fractal.getContentController(C2).addFcSubComponent(C0);
			try {
				Fractal.getNameController(C0).setFcName("rr");
			} catch (NoSuchInterfaceException ignored) {
			}
			Fractal.getContentController(C2).addFcSubComponent(C1);
			try {
				Fractal.getNameController(C1).setFcName("s");
			} catch (NoSuchInterfaceException ignored) {
			}
			Fractal.getBindingController(C2)
					.bindFc("r", C0.getFcInterface("r"));
			Fractal.getBindingController(C0)
					.bindFc("s", C1.getFcInterface("s"));
			Fractal.getBindingController(C0).bindFc(
					"rh",
					Fractal.getContentController(C2).getFcInternalInterface(
							"rh"));
			InterfaceType IT6 = typeFactory.createFcItfType("a",
					"itf.RequestHandler", false, false, false);
			InterfaceType IT7 = typeFactory.createFcItfType("rh",
					"itf.RequestHandler", true, false, false);
			InterfaceType IT8 = typeFactory.createFcItfType("l", "itf.Logger",
					true, false, false);
			ComponentType CT3 = typeFactory.createFcType(new InterfaceType[] {
					IT6, IT7, IT8 });
			Component C3 = genericFactory.newFcInstance(CT3, "primitive",
					"impl.RequestAnalyzer");
			try {
				Fractal.getNameController(C3).setFcName("ra");
			} catch (NoSuchInterfaceException ignored) {
			}
			InterfaceType IT9 = typeFactory.createFcItfType("h1",
					"itf.RequestHandler", true, false, true);
			InterfaceType IT10 = typeFactory.createFcItfType("rh",
					"itf.RequestHandler", false, false, false);
			InterfaceType IT11 = typeFactory.createFcItfType("h0",
					"itf.RequestHandler", true, false, true);
			ComponentType CT4 = typeFactory.createFcType(new InterfaceType[] {
					IT9, IT10, IT11 });
			Component C4 = genericFactory.newFcInstance(CT4, "primitive",
					"impl.RequestDispatcher");
			try {
				Fractal.getNameController(C4).setFcName("rd");
			} catch (NoSuchInterfaceException ignored) {
			}
			InterfaceType IT12 = typeFactory.createFcItfType("rh",
					"itf.RequestHandler", false, false, false);
			ComponentType CT5 = typeFactory
					.createFcType(new InterfaceType[] { IT12 });
			Component C5 = genericFactory.newFcInstance(CT5, "primitive",
					"impl.FileRequestHandler");
			try {
				Fractal.getNameController(C5).setFcName("frh");
			} catch (NoSuchInterfaceException ignored) {
			}
			InterfaceType IT13 = typeFactory.createFcItfType("rh",
					"itf.RequestHandler", false, false, false);
			ComponentType CT6 = typeFactory
					.createFcType(new InterfaceType[] { IT13 });
			Component C6 = genericFactory.newFcInstance(CT6, "primitive",
					"impl.ErrorRequestHandler");
			try {
				Fractal.getNameController(C6).setFcName("erh");
			} catch (NoSuchInterfaceException ignored) {
			}
			InterfaceType IT14 = typeFactory.createFcItfType("rh",
					"itf.RequestHandler", false, false, false);
			ComponentType CT7 = typeFactory
					.createFcType(new InterfaceType[] { IT14 });
			Component C7 = genericFactory.newFcInstance(CT7, "composite", null);
			try {
				Fractal.getNameController(C7).setFcName("rh");
			} catch (NoSuchInterfaceException ignored) {
			}
			Fractal.getContentController(C7).addFcSubComponent(C4);
			try {
				Fractal.getNameController(C4).setFcName("rd");
			} catch (NoSuchInterfaceException ignored) {
			}
			Fractal.getContentController(C7).addFcSubComponent(C5);
			try {
				Fractal.getNameController(C5).setFcName("frh");
			} catch (NoSuchInterfaceException ignored) {
			}
			Fractal.getContentController(C7).addFcSubComponent(C6);
			try {
				Fractal.getNameController(C6).setFcName("erh");
			} catch (NoSuchInterfaceException ignored) {
			}
			Fractal.getBindingController(C7).bindFc("rh",
					C4.getFcInterface("rh"));
			Fractal.getBindingController(C4).bindFc("h0",
					C5.getFcInterface("rh"));
			Fractal.getBindingController(C4).bindFc("h1",
					C6.getFcInterface("rh"));
			InterfaceType IT15 = typeFactory.createFcItfType("l", "itf.Logger",
					false, false, false);
			ComponentType CT8 = typeFactory
					.createFcType(new InterfaceType[] { IT15 });
			Component C8 = genericFactory.newFcInstance(CT8, "primitive",
					"impl.BasicLogger");
			try {
				Fractal.getNameController(C8).setFcName("l");
			} catch (NoSuchInterfaceException ignored) {
			}
			InterfaceType IT16 = typeFactory.createFcItfType("rh",
					"itf.RequestHandler", false, false, false);
			ComponentType CT9 = typeFactory
					.createFcType(new InterfaceType[] { IT16 });
			Component C9 = genericFactory.newFcInstance(CT9, "composite", null);
			try {
				Fractal.getNameController(C9).setFcName("be");
			} catch (NoSuchInterfaceException ignored) {
			}
			Fractal.getContentController(C9).addFcSubComponent(C3);
			try {
				Fractal.getNameController(C3).setFcName("ra");
			} catch (NoSuchInterfaceException ignored) {
			}
			Fractal.getContentController(C9).addFcSubComponent(C7);
			try {
				Fractal.getNameController(C7).setFcName("rh");
			} catch (NoSuchInterfaceException ignored) {
			}
			Fractal.getContentController(C9).addFcSubComponent(C8);
			try {
				Fractal.getNameController(C8).setFcName("l");
			} catch (NoSuchInterfaceException ignored) {
			}
			Fractal.getBindingController(C9).bindFc("rh",
					C3.getFcInterface("a"));
			Fractal.getBindingController(C3)
					.bindFc("l", C8.getFcInterface("l"));
			Fractal.getBindingController(C3).bindFc("rh",
					C7.getFcInterface("rh"));
			InterfaceType IT17 = typeFactory.createFcItfType("r",
					"java.lang.Runnable", false, false, false);
			ComponentType CT10 = typeFactory
					.createFcType(new InterfaceType[] { IT17 });
			Component C10 = genericFactory.newFcInstance(CT10, "composite",
					null);
			try {
				Fractal.getNameController(C10).setFcName("Comanche");
			} catch (NoSuchInterfaceException ignored) {
			}
			Fractal.getContentController(C10).addFcSubComponent(C2);
			try {
				Fractal.getNameController(C2).setFcName("fe");
			} catch (NoSuchInterfaceException ignored) {
			}
			Fractal.getContentController(C10).addFcSubComponent(C9);
			try {
				Fractal.getNameController(C9).setFcName("be");
			} catch (NoSuchInterfaceException ignored) {
			}
			Fractal.getBindingController(C10).bindFc("r",
					C2.getFcInterface("r"));
			Fractal.getBindingController(C2).bindFc("rh",
					C9.getFcInterface("rh"));
			return C10;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
