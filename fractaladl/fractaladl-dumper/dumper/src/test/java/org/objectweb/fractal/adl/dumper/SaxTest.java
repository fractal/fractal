package org.objectweb.fractal.adl.dumper;

import junit.framework.TestCase;

import org.objectweb.fractal.api.Component;

public class SaxTest extends TestCase {

	private static SaxAdlGenerator generator;

	protected void setUp() throws Exception {
		if (generator == null)
			generator = new SaxAdlGenerator(SaxAdlGenerator.STANDARD_DTD,
					GenUtil.GENERATED_DIR_NAME);
		GenUtil.copyFiles();
	}

	protected void tearDown() {
		GenUtil.clean();
	}

	public void testCS() throws Exception {
		String definition = "CS";
		Component component = (Component) GenUtil.factory.newComponent(
				definition, null);
		GenUtil.clean();
		generator.generateDefinition(component);
		GenUtil.factory.newComponent(definition, null);
	}

	public void testSeparatedCS() throws Exception {
		String definition = "CS";
		Component component = (Component) GenUtil.factory.newComponent(
				definition, null);
		GenUtil.clean();
		generator.generateDefinition(component, true);
		GenUtil.factory.newComponent(definition, null);
	}

	public void testCSColl() throws Exception {
		String definition = "CSColl";
		Component component = (Component) GenUtil.factory.newComponent(
				definition, null);
		GenUtil.clean();
		generator.generateDefinition(component);
		GenUtil.factory.newComponent(definition, null);
	}

	public void testCSOptional() throws Exception {
		String definition = "CSOptional";
		Component component = (Component) GenUtil.factory.newComponent(
				definition, null);
		GenUtil.clean();
		generator.generateDefinition(component);
		GenUtil.factory.newComponent(definition, null);
	}

	public void testSharedCS() throws Exception {
		String definition = "SharedCS";
		Component component = (Component) GenUtil.factory.newComponent(
				definition, null);
		GenUtil.clean();
		generator.generateDefinition(component);
		GenUtil.factory.newComponent(definition, null);
	}

	public void testCSParametric() throws Exception {
		String definition = "CSParametric";
		Component component = (Component) GenUtil.factory.newComponent(
				definition, null);
		GenUtil.clean();
		generator.generateDefinition(component);
		GenUtil.factory.newComponent(definition, null);
	}

}
