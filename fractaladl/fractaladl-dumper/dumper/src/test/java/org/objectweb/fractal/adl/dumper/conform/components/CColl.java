package org.objectweb.fractal.adl.dumper.conform.components;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.objectweb.fractal.api.control.BindingController;

public class CColl implements Runnable, BindingController {

	public final static String SERVICE_BINDING = "service";

	private Map<String, Service> service = new TreeMap<String, Service>();

	public CColl() {

	}

	// -----------------------------------------------------
	// Implementation of the BindingController interface
	// -----------------------------------------------------

	public String[] listFc() {
		return (String[]) service.keySet().toArray(new String[service.size()]);
	}

	public Object lookupFc(String itfName) {
		if (itfName.startsWith(SERVICE_BINDING)) {
			return service.get(itfName);
		} else
			return null;
	}

	public void bindFc(String itfName, Object itfValue) {
		if (itfName.startsWith(SERVICE_BINDING)) {
			service.put(itfName, (Service) itfValue);
		}
	}

	public void unbindFc(String itfName) {
		if (itfName.startsWith(SERVICE_BINDING)) {
			service.remove(itfName);
		}
	}

	// -----------------------------------------------------
	// Implementation of the Runnable interface
	// -----------------------------------------------------

	public void run() {
		Iterator i = service.values().iterator();
		while (i.hasNext()) {
			((Service) i.next()).print("RUN");
		}

	}
}
