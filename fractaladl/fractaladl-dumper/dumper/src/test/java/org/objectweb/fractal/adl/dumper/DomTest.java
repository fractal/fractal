package org.objectweb.fractal.adl.dumper;

import junit.framework.TestCase;

import org.objectweb.fractal.api.Component;

public class DomTest extends TestCase {

	private static DomAdlGenerator generator;

	protected void setUp() throws Exception {
		if (generator == null)
			generator = new DomAdlGenerator(DomAdlGenerator.STANDARD_DTD);
		GenUtil.copyFiles();
	}

	protected void tearDown() {
		GenUtil.clean();
	}

	public void testCS() throws Exception {
		try {
			String definition = "CS";
			Component component = (Component) GenUtil.factory.newComponent(
					definition, null);
			GenUtil.clean();
			generator.dump(component, GenUtil.GENERATED_DIR_NAME);
			GenUtil.factory.newComponent(definition, null);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void testCSColl() throws Exception {
		String definition = "CSColl";
		Component component = (Component) GenUtil.factory.newComponent(
				definition, null);
		GenUtil.clean();
		generator.dump(component, GenUtil.GENERATED_DIR_NAME);
		GenUtil.factory.newComponent(definition, null);
	}

	public void testCSOptional() throws Exception {
		String definition = "CSOptional";
		Component component = (Component) GenUtil.factory.newComponent(
				definition, null);
		GenUtil.clean();
		generator.dump(component, GenUtil.GENERATED_DIR_NAME);
		GenUtil.factory.newComponent(definition, null);
	}

	public void testSharedCS() throws Exception {
		String definition = "SharedCS";
		Component component = (Component) GenUtil.factory.newComponent(
				definition, null);
		GenUtil.clean();
		generator.dump(component, GenUtil.GENERATED_DIR_NAME);
		GenUtil.factory.newComponent(definition, null);
	}

	public void testCSParametric() throws Exception {
		String definition = "CSParametric";
		Component component = (Component) GenUtil.factory.newComponent(
				definition, null);
		GenUtil.clean();
		generator.dump(component, GenUtil.GENERATED_DIR_NAME);
		GenUtil.factory.newComponent(definition, null);
	}

}
