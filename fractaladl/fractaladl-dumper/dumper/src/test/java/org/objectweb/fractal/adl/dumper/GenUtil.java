package org.objectweb.fractal.adl.dumper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.nio.channels.FileChannel;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;

public class GenUtil {

	protected static FilenameFilter filter;

	protected final static String DEFINITION_DIR_NAME = "target/test-classes/definitions";

	protected final static String GENERATED_DIR_NAME = "target/test-classes";

	protected static Factory factory;

	static {
		try {
			System.setProperty("fractal.provider",
					"org.objectweb.fractal.julia.Julia");
			filter = new FilenameFilter() {
				public boolean accept(File dir, String name) {
					if (name.endsWith(".fractal"))
						return true;
					else
						return false;
				}
			};
			factory = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected static void clean() {
		File[] files = new File(GENERATED_DIR_NAME).listFiles(filter);
		for (int i = 0; i < files.length; i++) {
			files[i].delete();
		}
	}

	private static void copyFile(File in, File out) throws Exception {
		FileChannel sourceChannel = new FileInputStream(in).getChannel();
		FileChannel destinationChannel = new FileOutputStream(out).getChannel();
		sourceChannel.transferTo(0, sourceChannel.size(), destinationChannel);
		sourceChannel.close();
		destinationChannel.close();
	}

	protected static void copyFiles() throws Exception {
		File files[] = new File(DEFINITION_DIR_NAME).listFiles(filter);
		for (int i = 0; i < files.length; i++) {
			File src = files[i];
			File dst = new File(GENERATED_DIR_NAME + "/" + files[i].getName());
			copyFile(src, dst);
		}
	}

}
