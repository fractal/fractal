package org.objectweb.fractal.adl.dumper.conform.components;


import org.objectweb.fractal.api.control.AttributeController;

public interface CompositeAttributes extends AttributeController {
	String getTest ();
	void setTest (String test);
}
