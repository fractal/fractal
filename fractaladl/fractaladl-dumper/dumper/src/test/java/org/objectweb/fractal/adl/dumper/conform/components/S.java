package org.objectweb.fractal.adl.dumper.conform.components;


public class S implements Service, ServiceAttributes {

	private String header;

	private int count;

	public void print(String msg) {
		System.out.println(header + " " + msg);
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(final String header) {
		this.header = header;
	}

	public int getCount() {
		return count;
	}

	public void setCount(final int count) {
		this.count = count;
	}
	

}
