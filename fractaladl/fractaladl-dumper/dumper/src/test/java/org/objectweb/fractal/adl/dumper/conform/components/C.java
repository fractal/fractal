package org.objectweb.fractal.adl.dumper.conform.components;

import org.objectweb.fractal.api.control.BindingController;


public class C implements Runnable, BindingController {

	private final static String SERVICE_BINDING = "service";

	private Service service;

	
	public C() {

	}

	// -----------------------------------------------------
	// Implementation of the BindingController interface
	// -----------------------------------------------------

	public String[] listFc() {
		return new String[] { SERVICE_BINDING };
	}

	public Object lookupFc(final String clientItfName) {
		if (SERVICE_BINDING.equals(clientItfName)) {
			return service;
		}
		return null;
	}

	public void bindFc(final String clientItfName, final Object serverItf) {
		if (SERVICE_BINDING.equals(clientItfName)) {
			service = (Service) serverItf;
		}

	}

	public void unbindFc(final String clientItfName) {
		if (SERVICE_BINDING.equals(clientItfName)) {
			service = null;
		}
	}
	

	// -----------------------------------------------------
	// Implementation of the Runnable interface
	// -----------------------------------------------------

	public void run() {
		service.print("RUN");
	}

	
	

}
