/***
 * Fractal ADL Dumper
 * Copyright (C) 2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Marc L�ger
 */

package org.objectweb.fractal.adl.dumper.tasks.primitive;

import org.objectweb.fractal.adl.dumper.util.AttributeUtil;
import org.objectweb.fractal.api.Component;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;


public class AttributesTask extends PrimitiveTask {

	protected void generateAttribute(Component component, String attribute, ContentHandler hd) {
		try {
			AttributesImpl atts = new AttributesImpl();
			
			Object attributeObject = AttributeUtil
			.getAttribute(component, attribute);
			
			// if attribute value is not null
			if(attributeObject != null){
				atts.addAttribute("", "", "name", "CDATA", attribute);
				atts.addAttribute("", "", "value", "CDATA", attributeObject.toString());
				
				hd.startElement("", "", "attribute", atts);
				hd.endElement("", "", "attribute");
			}
			
			
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}

	public String getName() {
		return "attributes";
	}

	public void generate(Component comp, ContentHandler hd) {
		try {
			AttributesImpl atts = new AttributesImpl();
			Class[] attributesClasses = AttributeUtil
					.getAttributeControllerInterfaces(comp);
			for (int i = 0; i < attributesClasses.length; i++) {
				atts.clear();
				atts.addAttribute("", "", "signature", "CDATA",
						attributesClasses[i].getName());
				hd.startElement("", "", "attributes", atts);
				String[] attributes = AttributeUtil.getAttributesNames(comp,
						attributesClasses[i]);
				for (int j = 0; j < attributes.length; j++) {
					generateAttribute(comp, attributes[j], hd);
				}
				hd.endElement("", "", "attributes");
			}
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}

	public boolean isType() {
		return false;
	}

}
