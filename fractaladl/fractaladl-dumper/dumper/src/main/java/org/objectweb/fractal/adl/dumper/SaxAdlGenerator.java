/***
 * Fractal ADL Dumper
 * Copyright (C) 2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Marc Léger
 */

package org.objectweb.fractal.adl.dumper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import org.objectweb.fractal.adl.dumper.tasks.AdlTask;
import org.objectweb.fractal.adl.dumper.tasks.composite.ComponentTask;
import org.objectweb.fractal.adl.dumper.tasks.composite.CompositeTask;
import org.objectweb.fractal.adl.dumper.tasks.composite.ContentTask;
import org.objectweb.fractal.adl.dumper.tasks.composite.DefinitionTask;
import org.objectweb.fractal.adl.dumper.tasks.composite.ImplementationTask;
import org.objectweb.fractal.adl.dumper.tasks.composite.TypeTask;
import org.objectweb.fractal.adl.dumper.tasks.primitive.AttributesTask;
import org.objectweb.fractal.adl.dumper.tasks.primitive.BindingsTask;
import org.objectweb.fractal.adl.dumper.tasks.primitive.ComponentRefTask;
import org.objectweb.fractal.adl.dumper.tasks.primitive.ControllerTask;
import org.objectweb.fractal.adl.dumper.tasks.primitive.InterfacesTask;
import org.objectweb.fractal.adl.dumper.util.ComponentUtil;
import org.objectweb.fractal.adl.xml.XMLParser;
import org.objectweb.fractal.api.Component;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

public class SaxAdlGenerator {

	public final static String STANDARD_DTD = "org/objectweb/fractal/adl/xml/standard.dtd";

	private String baseDir;

	private String prefix;

	private Component defComp;

	private static SaxAdlGenerator generator = null;

	private Map<String, ContentHandler> handlers;

	private ContentHandler hd;

	private boolean separated = false;

	private String dtd;

	private AdlTask task;

	private OutputStreamWriter writer;

	public SaxAdlGenerator(String dtd, String path) {
		this(dtd);
		this.baseDir = path;
		if (!baseDir.endsWith("/"))
			this.baseDir += "/";
	}

	public static SaxAdlGenerator getGenerator() {
		return generator;
	}

	public SaxAdlGenerator(String dtd) {
		this.dtd = dtd;
		if (dtd == null)
			dtd = STANDARD_DTD;
		this.baseDir = "";
		task = defineComponentTasks();
		this.hd = initHandler();
		generator = this;
	}

	public SaxAdlGenerator(String dtd, ContentHandler hd) throws SAXException {
		this(dtd);
		this.hd = hd;
		if (hd instanceof XMLParser) {
			((XMLParser) hd).resolveEntity(
					"-//objectweb.org//DTD Fractal ADL 2.0//EN", "classpath://"
							+ this.dtd);
		}
	}

	public ContentHandler getContentHandler(String fileName) {
		ContentHandler handler = (ContentHandler) handlers.get(fileName);
		if (handler != null)
			return handler;
		handler = initHandler();
		handlers.put(fileName, handler);
		try {
			this.writer = new OutputStreamWriter(
					new FileOutputStream(fileName), "utf-8");
			((TransformerHandler) handler).setResult(new StreamResult(this.writer));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return handler;
	}

	protected ContentHandler initHandler() {
		try {
			SAXTransformerFactory tf = (SAXTransformerFactory) SAXTransformerFactory
					.newInstance();
			tf.setAttribute("indent-number", new Integer(2));
			ContentHandler handler = tf.newTransformerHandler();
			Transformer serializer = ((TransformerHandler) handler)
					.getTransformer();
			serializer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			serializer.setOutputProperty(OutputKeys.INDENT, "yes");
			serializer.setOutputProperty(OutputKeys.METHOD, "xml");
			serializer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,
					"-//objectweb.org//DTD Fractal ADL 2.0//EN");
			serializer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,
					"classpath://" + this.dtd);
			return handler;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	protected AdlTask defineComponentTasks() {
		CompositeTask componentTask = new ComponentTask();
		CompositeTask contentTask = new ContentTask();
		contentTask.addTask(componentTask);
		componentTask.addTask(new InterfacesTask());
		componentTask.addTask(contentTask);
		componentTask.addTask(new BindingsTask());
		componentTask.addTask(new AttributesTask());
		componentTask.addTask(new ControllerTask());
		return componentTask;
	}

	protected AdlTask defineSeparatedComponentTasks() {
		CompositeTask typeTask = new TypeTask();
		typeTask.addTask(new InterfacesTask());
		CompositeTask contentTask = new ContentTask();
		CompositeTask implementationTask = new ImplementationTask();
		implementationTask.addTask(contentTask);
		implementationTask.addTask(new BindingsTask());
		implementationTask.addTask(new AttributesTask());
		implementationTask.addTask(new ControllerTask());
		CompositeTask definitionTask = new DefinitionTask();
		contentTask.addTask(new ComponentRefTask());
		contentTask.addTask(definitionTask);
		definitionTask.addTask(typeTask);
		definitionTask.addTask(implementationTask);
		return definitionTask;
	}

	public void generateDefinition(Component comp) {
		generateDefinition(comp, "", false);
	}

	public void generateDefinition(Component comp, boolean separated) {
		generateDefinition(comp, "", separated);
	}

	public void generateDefinition(Component comp, String prefix,
			boolean separated) {
		if (this.separated != separated) {
			this.separated = separated;
			if (separated) {
				task = defineSeparatedComponentTasks();
			} else {
				task = defineComponentTasks();
			}
		}
		this.prefix = prefix;
		if (!prefix.equals(""))
			this.prefix += ".";
		this.defComp = comp;
		if (separated) {
			if (handlers == null)
				handlers = new HashMap<String, ContentHandler>();
		} else if (this.baseDir != null) {
			try {
				this.writer = new OutputStreamWriter(new FileOutputStream(
						this.baseDir + getAdlName(defComp)
								+ ".fractal"), "utf-8");
				((TransformerHandler) this.hd).setResult(new StreamResult(
						writer));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		task.generate(comp);
		if (separated)
			handlers.clear();
		finalize();
	}
	
	protected String getAdlName(Component component) {
	    return ComponentUtil.getName(component);
	}

	public ContentHandler getContentHandler() {
		return this.hd;
	}

	public String getDtd() {
		return this.dtd;
	}

	public boolean isSeparated() {
		return this.separated;
	}

	public String getBaseDir() {
		return this.baseDir;
	}

	public Component getDefComp() {
		return this.defComp;
	}

	public String getPrefix() {
		return this.prefix;
	}

	public void finalize() {
		try {
			this.writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
