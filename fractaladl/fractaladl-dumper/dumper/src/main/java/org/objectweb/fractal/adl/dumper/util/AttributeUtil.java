/***
 * Fractal ADL Dumper
 * Copyright (C) 2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Pierre-Charles David
 */

package org.objectweb.fractal.adl.dumper.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.fractal.util.Fractal;

public class AttributeUtil {

	private static String capitalize(String str) {
		if (str.length() == 0) {
			return str;
		} else {
			char chars[] = str.toCharArray();
			chars[0] = Character.toUpperCase(chars[0]);
			return new String(chars);
		}
	}

	private static Method findGetter(Class[] itfs, String propName) {
		for (int i = 0; i < itfs.length; i++) {
			Method getter = findGetter(itfs[i], propName);
			if (getter != null) {
				return getter;
			}
		}
		return null;
	}

	private static Method findGetter(Class klass, String propName) {
		Method getter = null;
		try {
			getter = ((Class<?>) klass).getMethod("get" + capitalize(propName),
					new Class[0]);
		} catch (NoSuchMethodException e) {
			try {
				getter = ((Class<?>) klass).getMethod("is"
						+ capitalize(propName), new Class[0]);
			} catch (NoSuchMethodException e1) {
			}
		}
		if (getter == null || getter.getReturnType().equals(Void.TYPE)) {
			return null;
		} else {
			return getter;
		}
	}

	private static Method findSetter(Class[] itfs, String propName, Object value) {
		for (int i = 0; i < itfs.length; i++) {
			Method setter = findSetter(itfs[i], propName, value);
			if (setter != null) {
				return setter;
			}
		}
		return null;
	}

	private static Method findSetter(Class klass, String propName, Object value) {
		Method[] allMethods = klass.getMethods();
		List<Method> candidates = new ArrayList<Method>();
		for (int i = 0; i < allMethods.length; i++) {
			if (isValidSetter(allMethods[i], propName, value)) {
				candidates.add(allMethods[i]);
			}
		}
		if (candidates.size() == 0) {
			return null;
		} else if (candidates.size() == 1) {
			return (Method) candidates.get(0);
		} else {
			return (Method) candidates.get(0);
		}
	}

	public static Class[] getAttributeControllerInterfaces(Component component) {
		try {
			Object ac = Fractal.getAttributeController(component);
			Class[] itfs = ac.getClass().getInterfaces();
			List<Class> attrItfs = new ArrayList<Class>();
			for (int i = 0; i < itfs.length; i++) {
				if (AttributeController.class.isAssignableFrom(itfs[i])) {
					attrItfs.add(itfs[i]);
				}
			}
			return (Class[]) attrItfs.toArray(new Class[attrItfs.size()]);
		} catch (NoSuchInterfaceException nsie) {
			return new Class[0];
		}
	}

	public static Map getAttributes(Component component) {
		String[] names = getAttributesNames(component);
		Map<String, Object> props = new HashMap<String, Object>();
		for (int i = 0; i < names.length; i++) {
			props.put(names[i], getAttribute(component, names[i]));
		}
		return props;
	}

	public static String[] getAttributesNames(Component component) {
		Class[] itfs = getAttributeControllerInterfaces(component);
		Set<String> names = new HashSet<String>();
		if (itfs != null) {
			for (int i = 0; i < itfs.length; i++) {
				Method[] allMethods = itfs[i].getMethods();
				for (int j = 0; j < allMethods.length; j++) {
					Method m = allMethods[j];
					if (m.getName().matches("^(get|is)[A-Z].*")
							&& m.getParameterTypes().length == 0
							&& (!m.getReturnType().equals(Void.TYPE))) {
						char[] chars = m.getName().substring(3).toCharArray();
						chars[0] = Character.toLowerCase(chars[0]);
						names.add(new String(chars));
					}
				}

			}
		}
		return (String[]) names.toArray(new String[names.size()]);
	}

	public static String[] getAttributesNames(Component component,
			Class attributeClass) {
		Set<String> names = new HashSet<String>();
		Method[] allMethods = attributeClass.getMethods();
		for (int j = 0; j < allMethods.length; j++) {
			Method m = allMethods[j];
			if (m.getName().matches("^(get|is)[A-Z].*")
					&& m.getParameterTypes().length == 0
					&& (!m.getReturnType().equals(Void.TYPE))) {
				char[] chars = m.getName().substring(3).toCharArray();
				chars[0] = Character.toLowerCase(chars[0]);
				names.add(new String(chars));
			}
		}
		return (String[]) names.toArray(new String[names.size()]);
	}

	public static Object getAttribute(Component component, String name) {
		Class[] itfs = getAttributeControllerInterfaces(component);
		if (itfs == null || itfs.length == 0) {
			return null;
		} else {
			Method getter = findGetter(itfs, name);
			if (getter != null) {
				try {
					return getter.invoke(Fractal
							.getAttributeController(component), new Object[0]);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			} else {
				return null;
			}
		}
	}

	public static Class getAttributeType(Component component, String name) {
		Class[] itfs = getAttributeControllerInterfaces(component);
		if (itfs == null || itfs.length == 0) {
			return null;
		} else {
			Method getter = findGetter(itfs, name);
			if (getter != null) {
				return getter.getReturnType();
			} else {
				return null;
			}
		}
	}

	public static boolean hasAttribute(Component component, String name) {
		Class[] itfs = getAttributeControllerInterfaces(component);
		if (itfs == null || itfs.length == 0) {
			return false;
		} else {
			return (findGetter(itfs, name) != null);
		}
	}

	private static boolean isValidSetter(Method m, String prop, Object value) {
		if (!m.getName().equals("set" + capitalize(prop))) {
			return false;
		}
		Class[] pTypes = m.getParameterTypes();
		if (pTypes.length != 1) {
			return false;
		} else if (value == null) {
			return true;
		} else {
			Class clazz = value.getClass();
			if (pTypes[0].isPrimitive() && !clazz.isPrimitive()) {
				if ((value instanceof Integer) && pTypes[0].equals(int.class))
					return true;
				if ((value instanceof Boolean)
						&& pTypes[0].equals(boolean.class))
					return true;
				if ((value instanceof Float) && pTypes[0].equals(float.class))
					return true;
				if ((value instanceof Long) && pTypes[0].equals(long.class))
					return true;
				if ((value instanceof Double) && pTypes[0].equals(double.class))
					return true;
			}
			return ((Class<?>) pTypes[0]).isAssignableFrom(value.getClass());
		}
	}

	public static void setAttribute(Component component, String name,
			Object value) throws IllegalArgumentException,
			NoSuchInterfaceException {
		if (!hasAttribute(component, name)) {
			throw new IllegalArgumentException("No such attribute.");
		}
		Class[] itfs = getAttributeControllerInterfaces(component);
		if (itfs != null) {
			Method setter = findSetter(itfs, name, value);
			if (setter != null) {
				try {
					setter.invoke(Fractal.getAttributeController(component),
							new Object[] { value });
				} catch (IllegalAccessException e) {
					throw new RuntimeException(e);
				} catch (InvocationTargetException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}
}