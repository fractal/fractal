/***
 * Fractal ADL Dumper
 * Copyright (C) 2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Marc L�ger
 */

package org.objectweb.fractal.adl.dumper.util;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;


public class Binding {
	private Interface clientItf;
	private Interface serverItf;
	
	
	public Binding(Interface clientItf, Interface serverItf) {
		this.clientItf = clientItf;
		this.serverItf = serverItf;
	}

	public Interface getClientItf() {
		return clientItf;
	}

	public Interface getServerItf() {
		return serverItf;
	}

	public String getClientItfName() {
		return  getClientItfOwnerName() + "." + clientItf.getFcItfName();
	}

	public String getServerItfName() {
		return getServerItfOwnerName() + "." + serverItf.getFcItfName();
	}
	
	public String getServerItfName(Component comp) {
		if (ComponentUtil.getName(comp).equals(ComponentUtil.getName(this.serverItf.getFcItfOwner()))) {
			return "this" + "." + serverItf.getFcItfName();
		}
		return getServerItfName();
	}
	
	public String getClientItfName(Component comp) {
		if (ComponentUtil.getName(comp).equals(ComponentUtil.getName(this.clientItf.getFcItfOwner()))) {
			return "this" + "." + clientItf.getFcItfName();
		}
		return getClientItfName();
	}
	
	public String getClientItfOwnerName() {
		return ComponentUtil.getName(this.clientItf.getFcItfOwner());
	}
	
	public String getServerItfOwnerName() {
		return ComponentUtil.getName(this.serverItf.getFcItfOwner());
	}
	
	public String toString() {
		return "Binding: client=" + getClientItfName() + ", server=" + getServerItfName();
	}
	
	
	
}
