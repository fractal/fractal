/***
 * Fractal ADL Dumper
 * Copyright (C) 2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Marc L�ger
 */

package org.objectweb.fractal.adl.dumper.tasks.composite;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.objectweb.fractal.adl.dumper.tasks.AdlTask;
import org.objectweb.fractal.adl.dumper.tasks.primitive.PrimitiveTask;
import org.objectweb.fractal.api.Component;
import org.xml.sax.ContentHandler;



public abstract class CompositeTask extends PrimitiveTask {

	protected List<AdlTask> subtasks;

	public CompositeTask() {
		subtasks = new ArrayList<AdlTask>();
	}

	public void setTasks(List<AdlTask> tasks) {
		this.subtasks = tasks;
	}

	public void addTask(AdlTask task) {
		subtasks.add(task);
	}

	public AdlTask getTask(String name) {
		try {
			Iterator it = subtasks.iterator();
			while (it.hasNext()) {
				AdlTask task = (AdlTask) it.next();
				if (task.getName().equals(name))
					return task;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void execute(Component comp) {
		try {
			Iterator it = subtasks.iterator();
			while (it.hasNext()) {
				((AdlTask) it.next()).generate(comp);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute(Component comp, ContentHandler hd) {
		try {
			Iterator it = subtasks.iterator();
			while (it.hasNext()) {
				((AdlTask) it.next()).generate(comp, hd);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
