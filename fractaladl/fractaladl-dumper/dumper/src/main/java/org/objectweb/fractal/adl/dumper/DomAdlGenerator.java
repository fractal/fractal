/***
 * Fractal ADL Dumper
 * Copyright (C) 2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Marc L�ger
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.adl.dumper;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.DocType;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.objectweb.fractal.adl.dumper.util.AttributeUtil;
import org.objectweb.fractal.adl.dumper.util.Binding;
import org.objectweb.fractal.adl.dumper.util.BindingUtil;
import org.objectweb.fractal.adl.dumper.util.ComponentUtil;
import org.objectweb.fractal.adl.dumper.util.ContentUtil;
import org.objectweb.fractal.adl.dumper.util.InterfaceUtil;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.type.InterfaceType;

/**
 * An ADL Generator for Fractal components using the JDOM API. When a FractalADL
 * node is generated, it can be dumped to an OutputStream.
 * 
 * @author Marc L�ger 
 * Contributor: Alessio Pace
 * 
 */
public class DomAdlGenerator {

	public final static String STANDARD_DTD = "org/objectweb/fractal/adl/xml/standard.dtd";

	private Component defComp;

	private String dtd;

	protected DomNodeFactory factory;

	public DomAdlGenerator(String dtd) {
		this.dtd = dtd;
		if (dtd == null)
			this.dtd = STANDARD_DTD;
		this.factory = DomNodeFactory.getNodeFactory();
	}

	public Document generateDefinition(Component component) {
		this.defComp = component;
		Document doc = new Document(generateComponent(component).setName(
				"definition"));
		DocType docType = new DocType("definition");
		docType.setPublicID("-//objectweb.org//DTD Fractal ADL 2.0//EN");
		docType.setSystemID("classpath://" + this.dtd);
		doc.setDocType(docType);
		return doc;
	}

	protected Element generateComponent(Component component) {
		Element componentElt = factory.createComponentNode(ComponentUtil
				.getName(component), null);
		componentElt.addContent(generateInterfaces(component));
		componentElt.addContent(generateContent(component));
		componentElt.addContent(generateBindings(component));
		componentElt.addContent(generateAttributes(component));
		componentElt.addContent(generateController(component));
		return componentElt;
	}

	protected List<Element> generateBindings(Component component) {
		List<Element> bindingsElt = new ArrayList<Element>();
		if (ContentUtil.isComposite(component)) {
			Binding[] bindings = BindingUtil.getBindingsIn(component);
			for (int i = 0; i < bindings.length; i++) {
				Element bindingNode = factory.createBindingNode(bindings[i]
						.getClientItfName(component), bindings[i]
						.getServerItfName(component));
				bindingsElt.add(bindingNode);
			}
		}
		return bindingsElt;
	}

	protected List<Element> generateAttributes(Component component) {
		List<Element> attributesElt = new ArrayList<Element>();
		Class[] attributesClasses = AttributeUtil
				.getAttributeControllerInterfaces(component);
		for (int i = 0; i < attributesClasses.length; i++) {
			attributesElt.add(factory.createAttributesNode(attributesClasses[i]
					.getName()));
			String[] attributes = AttributeUtil.getAttributesNames(component,
					attributesClasses[i]);
			for (int j = 0; j < attributes.length; j++) {
				Element attributeElt = generateAttribute(component,
						attributes[j]);
				if (attributeElt != null) {
					attributesElt.get(i).addContent(attributeElt);
				}

			}
		}
		return attributesElt;
	}

	protected Element generateAttribute(Component component, String attribute) {
		if (component == null) {
			throw new IllegalArgumentException(
					"Component parameter can't be null");
		}
		if (attribute == null) {
			throw new IllegalArgumentException(
					"Attribute parameter can't be null");
		}

		final Object attr = AttributeUtil.getAttribute(component, attribute);

		if (attr != null) {
			return factory.createAttributeNode(attribute, attr.toString());
		} else {
			return null;
		}

	}

	protected List<Element> generatePrimitiveContent(Component component) {
		List<Element> contentElt = new ArrayList<Element>();
		contentElt.add(factory.createContentNode(ContentUtil
				.getImplementation(component)));
		return contentElt;
	}

	protected List<Element> generateCompositeContent(Component component) {
		List<Element> contentElt = new ArrayList<Element>();
		Component[] children = ContentUtil.getSubComponents(component);
		for (int i = 0; i < children.length; i++) {
			if (ContentUtil.isFirstParentOfSharedComp(children[i], component)) {
				contentElt.add(factory.createComponentNode(ComponentUtil
						.getName(children[i]),
						ComponentUtil.getOneRelativeNameWithoutTop(children[i],
								this.defComp)));
			} else {
				contentElt.add(generateComponent(children[i]));
			}
		}
		return contentElt;
	}

	protected List<Element> generateContent(Component component) {
		if (ContentUtil.isComposite(component)) {
			return generateCompositeContent(component);
		} else {
			return generatePrimitiveContent(component);
		}
	}

	protected List<Element> generateInterfaces(Component component) {

		Map<String, Element> interfacesElt = new HashMap<String, Element>();
		Interface[] itfs = InterfaceUtil.getFunctionalItfs(component);
		for (int i = 0; i < itfs.length; i++) {
			Element interfaceElement = generateInterface(itfs[i]);
			String itfName = interfaceElement.getAttributeValue("name");
			if (!interfacesElt.containsKey(itfName)) {
				interfacesElt.put(itfName, interfaceElement);
			}
		}
		return new ArrayList<Element>(interfacesElt.values());
	}

	protected Element generateInterface(Interface itf) {
		InterfaceType itfType = (InterfaceType) itf.getFcItfType();
		return factory.createInterfaceNode(itfType.getFcItfName(), !itfType
				.isFcClientItf(), itfType.getFcItfSignature(), itfType
				.isFcOptionalItf(), itfType.isFcCollectionItf());
	}

	protected Element generateController(Component component) {
		String description = null;
		try {
			description = component.getFcInterface("/controllerDesc")
					.toString();
		} catch (NoSuchInterfaceException e) {
			description = ContentUtil.isComposite(component) ? "composite"
					: "primitive";
		}
		return factory.createControllerNode(description);
	}

	protected Element generateLifeCycle(Component component) {
		return factory.createLifeCycleNode(ComponentUtil
				.getLifeCycleState(component));
	}

	public void dump(Document doc, OutputStream os) {
		try {
			XMLOutputter outputter = new XMLOutputter();
			outputter.setFormat(Format.getPrettyFormat());
			outputter.output(doc, os);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void dump(Document doc, String path) {
		try {
			if (path.endsWith("/"))
				path = path.substring(0, path.length() - 1);
			String definition = doc.getRootElement().getAttribute("name")
					.getValue();
			dump(doc,
					new FileOutputStream(path + "/" + definition + ".fractal"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void dump(Component component, String directory) {
		Document document = generateDefinition(component);
		dump(document, directory);
	}

	public String getDtd() {
		return this.dtd;
	}

}
