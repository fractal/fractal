/***
 * Fractal ADL Dumper
 * Copyright (C) 2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Marc L�ger
 */

package org.objectweb.fractal.adl.dumper.tasks.primitive;

import java.util.HashSet;
import java.util.Set;

import org.objectweb.fractal.adl.dumper.util.InterfaceUtil;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.type.InterfaceType;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

public class InterfacesTask extends PrimitiveTask {

	protected void generateInterface(Interface itf, ContentHandler hd) {
		InterfaceType itfType = (InterfaceType) itf.getFcItfType();
		generateInterface(itfType.getFcItfName(), !itfType.isFcClientItf(),
				((InterfaceType) itf.getFcItfType()).getFcItfSignature(),
				itfType.isFcOptionalItf(), itfType.isFcCollectionItf(), hd);
	}

	protected void generateInterface(String name, boolean isServer,
			String signature, boolean isOptional, boolean isCollection,
			ContentHandler hd) {
		try {
			AttributesImpl atts = new AttributesImpl();
			atts.addAttribute("", "", "name", "CDATA", name);
			atts.addAttribute("", "", "role", "CDATA", isServer ? "server"
					: "client");
			atts.addAttribute("", "", "signature", "CDATA", signature);
			if (isCollection)
				atts.addAttribute("", "", "cardinality", "CDATA", "collection");
			if (isOptional)
				atts.addAttribute("", "", "contingency", "CDATA", "optional");
			hd.startElement("", "", "interface", atts);
			hd.endElement("", "", "interface");
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}

	public String getName() {
		return "interfaces";
	}

	public void generate(Component comp, ContentHandler hd) {
		Set<String> itfNames = new HashSet<String>();
		Interface[] itfs = InterfaceUtil.getFunctionalItfs(comp);
		for (int i = 0; i < itfs.length; i++) {
			String itfName = ((InterfaceType) itfs[i].getFcItfType())
					.getFcItfName();
			if (itfNames.add(itfName)) {
				generateInterface(itfs[i], hd);
			}
		}
	}

	public boolean isType() {
		return true;
	}

}
