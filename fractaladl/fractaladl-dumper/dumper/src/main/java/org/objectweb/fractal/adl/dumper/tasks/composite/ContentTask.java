/***
 * Fractal ADL Dumper
 * Copyright (C) 2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Marc L�ger
 */

package org.objectweb.fractal.adl.dumper.tasks.composite;

import org.objectweb.fractal.adl.dumper.SaxAdlGenerator;
import org.objectweb.fractal.adl.dumper.util.ComponentUtil;
import org.objectweb.fractal.adl.dumper.util.ContentUtil;
import org.objectweb.fractal.api.Component;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;



public class ContentTask extends CompositeTask {


	protected void generateCompositeContent(Component comp, ContentHandler hd) {
		Component[] children = ContentUtil.getSubComponents(comp);
		for (int i = 0; i < children.length; i++) {
			if (ContentUtil.isFirstParentOfSharedComp(children[i], comp)) {
				generateSharedContent(ComponentUtil.getName(children[i]),
						ComponentUtil.getOneRelativeNameWithoutTop(children[i],
								SaxAdlGenerator.getGenerator().getDefComp()), hd);
			} else {
				if (SaxAdlGenerator.getGenerator().isSeparated())
					execute(children[i], hd);
				else
					execute(children[i]);
			}
		}
	}

	protected void generatePrimitiveContent(Component comp, ContentHandler hd) {
		try {
			AttributesImpl atts = new AttributesImpl();
			atts.addAttribute("", "", "class", "CDATA", ContentUtil
					.getImplementation(comp));
			hd.startElement("", "", "content", atts);
			hd.endElement("", "", "content");
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}

	protected void generateSharedContent(String name, String definition, ContentHandler hd) {
		try {
			AttributesImpl atts = new AttributesImpl();
			atts.addAttribute("", "", "name", "CDATA", name);
			atts.addAttribute("", "", "definition", "CDATA", definition);
			hd.startElement("", "", "component", atts);
			hd.endElement("", "", "component");
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}

	public String getName() {
		return "content";
	}

	public void generate(Component comp, ContentHandler hd) {
		if (ContentUtil.isComposite(comp))
			generateCompositeContent(comp, hd);
		else
			generatePrimitiveContent(comp, hd);
	}

	public boolean isType() {
		return false;
	}
}
