/***
 * Fractal ADL Dumper
 * Copyright (C) 2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Marc L�ger
 */

package org.objectweb.fractal.adl.dumper.util;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.type.InterfaceType;

public class InterfaceUtil {

	public static String getRole(Interface itf) {
		if (((InterfaceType) itf.getFcItfType()).isFcClientItf())
			return "client";
		else
			return "server";
	}

	public static String getCardinality(Interface itf) {
		if (((InterfaceType) itf.getFcItfType()).isFcCollectionItf())
			return "collection";
		else
			return "singleton";
	}

	public static String getContingency(Interface itf) {
		if (((InterfaceType) itf.getFcItfType()).isFcOptionalItf())
			return "optional";
		else
			return "mandatory";
	}

	public static Interface[] getControllerItfs(Component comp) {
		Object[] itfs = comp.getFcInterfaces();
		List<Interface> controllers = new ArrayList<Interface>();
		for (int i = 0; i < itfs.length; i++) {
			String itfName = ((Interface) itfs[i]).getFcItfName();
			if ((itfName.endsWith("controller"))
					|| (itfName.equals("component"))) {
				controllers.add((Interface)itfs[i]);
			}
		}
		return (Interface[]) controllers.toArray(new Interface[controllers
				.size()]);
	}

	public static Interface[] getFunctionalItfs(Component comp) {
		Object[] itfs = comp.getFcInterfaces();
		List<Interface> functions = new ArrayList<Interface>();
		for (int i = 0; i < itfs.length; i++) {
			String itfName = ((Interface) itfs[i]).getFcItfName();
			if (!(itfName.endsWith("controller"))
					&& !(itfName.equals("component"))) {
				functions.add((Interface)itfs[i]);
			}
		}
		return (Interface[]) functions.toArray(new Interface[functions.size()]);
	}

	public static Interface[] getFunctionalServerItfs(Component comp) {
		Interface[] itfs = getFunctionalItfs(comp);
		List<Interface> funcItfs = new ArrayList<Interface>();
		for (int i = 0; i < itfs.length; i++) {
			if (!((InterfaceType) itfs[i].getFcItfType()).isFcClientItf()) {
				funcItfs.add(itfs[i]);
			}
		}
		return (Interface[]) funcItfs.toArray(new Interface[funcItfs.size()]);
	}

	public static Interface[] getFunctionalClientItfs(Component comp) {
		Interface[] itfs = getFunctionalItfs(comp);
		List<Interface> funcItfs = new ArrayList<Interface>();
		for (int i = 0; i < itfs.length; i++) {
			if (((InterfaceType) itfs[i].getFcItfType()).isFcClientItf()) {
				funcItfs.add(itfs[i]);
			}
		}
		return (Interface[]) funcItfs.toArray(new Interface[funcItfs.size()]);
	}

}
