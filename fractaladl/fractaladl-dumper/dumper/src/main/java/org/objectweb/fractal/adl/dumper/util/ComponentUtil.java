/***
 * Fractal ADL Dumper
 * Copyright (C) 2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Marc L�ger
 */

package org.objectweb.fractal.adl.dumper.util;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.util.Fractal;

public class ComponentUtil {

	public static String getName(Component comp) {
		try {
			return Fractal.getNameController(comp).getFcName();
		} catch (NoSuchInterfaceException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getOneRelativeName(Component child, Component parent) {
		String path = "";
		try {
			Component[] parents = Fractal.getContentController(parent)
					.getFcSubComponents();
			for (int i = 0; i < parents.length; i++) {
				if (getName(child).equals(getName(parent)))
					return getName(child);
				String result = getOneRelativeName(child, parents[i]);
				if (result != null) {
					path += result;
					break;
				}
			}
		} catch (NoSuchInterfaceException e) {
			if (getName(child).equals(getName(parent)))
				return getName(child);
			return null;
		}
		if (!path.equals(""))
			return getName(parent) + "/" + path;
		else
			return null;
	}

	public static String getOneRelativeNameWithoutTop(Component child,
			Component parent) {
		return getOneRelativeName(child, parent).substring(
				getName(parent).length() + 1);
	}

	public static String getDefinitionName(Component comp, Component parentComp) {
		if ((ContentUtil.isShared(comp))
				&& (!(ComponentUtil.getName(ContentUtil.getParents(comp)[0]))
						.equals(ComponentUtil.getName(parentComp)))) {
			return ComponentUtil.getName(ContentUtil.getParents(comp)[0]) + "/"
					+ ComponentUtil.getName(comp);
		} else {
			return getName(comp);
		}
	}

	public static String getLifeCycleState(Component component) {
		try {
			return Fractal.getLifeCycleController(component).getFcState();
		} catch (NoSuchInterfaceException e) {
			e.printStackTrace();
		}
		return null;
	}

}
