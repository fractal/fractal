/***
 * Fractal ADL Dumper
 * Copyright (C) 2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Marc L�ger
 */

package org.objectweb.fractal.adl.dumper.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.util.Fractal;

public class BindingUtil {

	public static List getClientBindings(Component comp) {
		BindingController bindingcontroller = null;
		List<Binding> bindings = new ArrayList<Binding>();
		try {
			bindingcontroller = Fractal.getBindingController(comp);
			String[] clientItfs = bindingcontroller.listFc();
			for (int i = 0; i < clientItfs.length; i++) {
				Interface clientItf = (Interface) comp
						.getFcInterface(clientItfs[i]);
				Interface serverItf = (Interface) bindingcontroller
						.lookupFc(clientItfs[i]);
				if (serverItf != null) {
					bindings.add(new Binding(clientItf, serverItf));
				}
			}
		} catch (NoSuchInterfaceException e) {
			return null;
		}
		return bindings;
	}

	public static Binding[] getBindingsIn(Component comp) {
		String compName = null;
		ContentController contentcontroller = null;
		List<Binding> bindings = new ArrayList<Binding>();
		try {
			compName = Fractal.getNameController(comp).getFcName();
			List parent = getClientBindings(comp);
			Iterator it = parent.listIterator();
			while (it.hasNext()) {
				Binding binding = ((Binding) it.next());
				Component serverOwner = binding.getServerItf().getFcItfOwner();
				if ((binding.getClientItfOwnerName().equals(compName))
						&& (ContentUtil.isSubcomponent(serverOwner, comp))) {
					bindings.add(binding);
				}
			}
			contentcontroller = Fractal.getContentController(comp);
			Component[] subcomps = contentcontroller.getFcSubComponents();
			for (int i = 0; i < subcomps.length; i++) {
				List children = getClientBindings(subcomps[i]);
				if (children != null) {
					it = children.iterator();
					while (it.hasNext()) {
						Binding binding = ((Binding) it.next());
						Component clientOwner = binding.getClientItf()
								.getFcItfOwner();
						Component serverOwner = binding.getServerItf()
								.getFcItfOwner();
						if ((binding.getServerItfOwnerName().equals(compName))) {
							if (ContentUtil.isSubcomponent(subcomps[i], comp)) {
								bindings.add(binding);
							}
						} else if (!ContentUtil.isSubcomponent(serverOwner,
								clientOwner)) {
							bindings.add(binding);
						} else {
						}
					}
				}
			}
		} catch (NoSuchInterfaceException e) {
			e.printStackTrace();
		}
		return (Binding[]) bindings.toArray(new Binding[bindings.size()]);
	}

}
