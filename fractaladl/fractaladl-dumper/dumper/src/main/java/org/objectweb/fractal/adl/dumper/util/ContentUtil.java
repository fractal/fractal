/***
 * Fractal ADL Dumper
 * Copyright (C) 2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Marc L�ger
 */

package org.objectweb.fractal.adl.dumper.util;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.util.Fractal;

public class ContentUtil {

	public static boolean isSubcomponent(Component child, Component parent) {
		try {
			Component[] children = Fractal.getContentController(parent)
					.getFcSubComponents();
			int i = 0;
			while (i < children.length && child != children[i]) {
				i++;
			}
			return (i < children.length);
		} catch (NoSuchInterfaceException e) {
			return false;
		}
	}

	public static Component[] getParents(Component comp) {
		try {
			return Fractal.getSuperController(comp).getFcSuperComponents();
		} catch (NoSuchInterfaceException e) {
			return null;
		}
	}

	public static boolean isShared(Component comp) {
		Component[] parents = getParents(comp);
		if (parents != null)
			return (parents.length > 1);
		else
			return false;
	}

	public static Component[] getSubComponents(Component comp) {
		try {
			return Fractal.getContentController(comp).getFcSubComponents();
		} catch (NoSuchInterfaceException e) {
			return null;
		}
	}

	public static boolean isComposite(Component comp) {
		try {
			Fractal.getContentController(comp);
			return true;
		} catch (NoSuchInterfaceException e1) {
			return false;
		}
	}

	public static String getImplementation(Component comp) {
		String content = null;
		try {
			content = comp.getFcInterface("/content").getClass().getName();
		} catch (NoSuchInterfaceException e) {
			return null;
		}
		return content;
	}

	public static boolean isFirstParentOfSharedComp(Component child,
			Component parent) {
		return (ContentUtil.isShared(child))
				&& ((ComponentUtil.getName(ContentUtil.getParents(child)[0]))
						.equals(ComponentUtil.getName(parent)));
	}

}
