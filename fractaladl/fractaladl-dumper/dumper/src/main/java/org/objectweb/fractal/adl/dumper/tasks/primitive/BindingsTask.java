/***
 * Fractal ADL Dumper
 * Copyright (C) 2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Marc L�ger
 */

package org.objectweb.fractal.adl.dumper.tasks.primitive;

import org.objectweb.fractal.adl.dumper.util.Binding;
import org.objectweb.fractal.adl.dumper.util.BindingUtil;
import org.objectweb.fractal.adl.dumper.util.ContentUtil;
import org.objectweb.fractal.api.Component;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;


public class BindingsTask extends PrimitiveTask {

	
	protected void generateBinding(String client, String server, ContentHandler hd) {
		try {
			AttributesImpl atts = new AttributesImpl();
			atts.addAttribute("", "", "client", "CDATA", client);
			atts.addAttribute("", "", "server", "CDATA", server);
			hd.startElement("", "", "binding", atts);
			hd.endElement("", "", "binding");
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}

	public String getName() {
		return "bindings";
	}

	public void generate(Component comp, ContentHandler hd) {
		if (ContentUtil.isComposite(comp)) {
			Binding[] bindings = BindingUtil.getBindingsIn(comp);
			for (int i = 0; i < bindings.length; i++) {
				generateBinding(bindings[i].getClientItfName(comp),
						bindings[i].getServerItfName(comp), hd);
			}
		}
	}

	public boolean isType() {
		return false;
	}

}
