/***
 * Fractal ADL Dumper
 * Copyright (C) 2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Marc L�ger
 */


package org.objectweb.fractal.adl.dumper;

import org.jdom.Attribute;
import org.jdom.Element;

/**
 * A JDOM factory for FractalADL nodes. 
 * 
 * @author Marc L�ger
 * 
 */
public class DomNodeFactory {

	private static DomNodeFactory factory = null;

	public static DomNodeFactory getNodeFactory() {
		if (factory == null) {
			return new DomNodeFactory();
		} else
			return factory;
	}

	public Element createDefinitionNode(String name) {
		Element elt = new Element("definition");
		Attribute nameAtt = new Attribute("name", name);
		elt.setAttribute(nameAtt);
		return elt;

	}

	public Element createBindingNode(String client, String server) {
		Element elt = new Element("binding");
		Attribute clientAtt = new Attribute("client", client);
		Attribute serverAtt = new Attribute("server", server);
		elt.setAttribute(clientAtt);
		elt.setAttribute(serverAtt);
		return elt;
	}

	public Element createAttributeNode(String name, String value) {
		Element elt = new Element("attribute");
		Attribute nameAtt = new Attribute("name", name);
		Attribute valueAtt = new Attribute("value", value);
		elt.setAttribute(nameAtt);
		elt.setAttribute(valueAtt);
		return elt;
	}

	public Element createComponentNode(String name, String definition) {
		Element elt = new Element("component");
		Attribute nameAtt = new Attribute("name", name);
		elt.setAttribute(nameAtt);
		if (definition != null) {
			Attribute definitionAtt = new Attribute("definition", definition);
			elt.setAttribute(definitionAtt);
		}
		return elt;
	}

	public Element createInterfaceNode(String name, boolean isServer,
			String signature, boolean isOptional, boolean isCollection) {
		Element elt = new Element("interface");
		Attribute nameAtt = new Attribute("name", name);
		elt.setAttribute(nameAtt);
		Attribute roleAtt = new Attribute("role", isServer ? "server"
				: "client");
		elt.setAttribute(roleAtt);
		Attribute signatureAtt = new Attribute("signature", signature);
		elt.setAttribute(signatureAtt);
		if (isOptional) {
			Attribute contingencyAtt = new Attribute("contingency", "optional");
			elt.setAttribute(contingencyAtt);
		}
		if (isCollection) {
			Attribute cardinalityAtt = new Attribute("cardinality",
					"collection");
			elt.setAttribute(cardinalityAtt);
		}
		return elt;
	}

	public Element createContentNode(String clazz) {
		Element elt = new Element("content");
		Attribute classAtt = new Attribute("class", clazz);
		elt.setAttribute(classAtt);
		return elt;
	}

	public Element createAttributesNode(String signature) {
		Element elt = new Element("attributes");
		Attribute signatureAtt = new Attribute("signature", signature);
		elt.setAttribute(signatureAtt);
		return elt;
	}

	public Element createControllerNode(String description) {
		Element elt = new Element("controller");
		Attribute descAtt = new Attribute("desc", description);
		elt.setAttribute(descAtt);
		return elt;
	}

	public Element createLifeCycleNode(String state) {
		Element elt = new Element("lifecycle");
		Attribute stateAtt = new Attribute("state", state);
		elt.setAttribute(stateAtt);
		return elt;
	}

}
