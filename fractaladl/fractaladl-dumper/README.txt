This is a Fractal ADL dumper for Fractal components. 

It allows to generate an architectural representation in ADL (Architectural Description Language) of 
a component-based application in Fractal during its execution. 

It is subdivided in the following modules:
 * dumper: the adl-dumper tool
 * examples: the parent modules for the dumper examples. Go there for more informations on them.
