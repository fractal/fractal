/*====================================================================

 ObjectWeb Deployment Framework
 Copyright (C) 2004-2006 INRIA - USTL - LIFL - GOAL
 Copyright (C) 2006-2007 STMicroelectronics

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA

 Initial developer(s): Philippe Merle, Fr�d�ric Briclet.
 Contributor(s): Eric Bruneton.

 ====================================================================
 $Id$
 ====================================================================*/

package org.objectweb.fractal.task.deployment.api;

/**
 * InitializationTask is the base interface of any deployment task which
 * initializes a component instance.
 * 
 * InitializationTask extends RequireInstanceProviderTask because a component
 * instance provider task is required.
 * 
 * @author <a href="mailto:Philippe.Merle@inria.fr">Philippe Merle</a>
 * @author <a href="mailto:Frederic.Briclet@lifl.fr">Frederic Briclet</a>
 * 
 * @version 0.2
 */

public interface InitializationTask extends RequireInstanceProviderTask {
	/**
	 * Return all the configuration tasks to execute before.
	 * 
	 * @return All the configuration tasks to execute before.
	 */
	public ConfigurationTask[] getConfigurationTasks();
}
