/*====================================================================

 ObjectWeb Deployment Framework
 Copyright (C) 2004-2006 INRIA - USTL - LIFL - GOAL
 Copyright (C) 2006-2007 STMicroelectronics

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA

 Initial developer(s): Philippe Merle, Frederic Briclet.
 Contributor(s): Eric Bruneton.

 ====================================================================
 $Id$
 ====================================================================*/

package org.objectweb.fractal.task.deployment.api;

import org.objectweb.fractal.task.core.Task;

/**
 * InstanceProviderTask is the base interface of any deployment tasks which
 * provide after execution a reference to a component instance.
 * 
 * @author <a href="mailto:Philippe.Merle@inria.fr">Philippe Merle</a>
 * @author <a href="mailto:Frederic.Briclet@lifl.fr">Frederic Briclet</a>
 * 
 * @version 0.2
 */

public interface InstanceProviderTask extends Task {
	/**
	 * Return the reference of a component instance.
	 * 
	 * @return The reference of a component instance.
	 */
	public Object getInstance();

	/**
	 * Set the reference of a component instance.
	 * 
	 * @param reference -
	 *            The reference of a component instance.
	 */
	public void setInstance(Object reference);
}
