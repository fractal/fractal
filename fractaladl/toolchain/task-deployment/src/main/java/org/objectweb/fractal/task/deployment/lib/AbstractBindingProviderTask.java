/*====================================================================

 ObjectWeb Deployment Framework
 Copyright (C) 2004-2006 INRIA - USTL - LIFL - GOAL
 Copyright (C) 2006-2007 STMicroelectronics

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA

 Initial developer(s): Philippe Merle, Fr�d�ric Briclet.
 Contributor(s): Eric Bruneton.

 ====================================================================
 $Id$
 ====================================================================*/

package org.objectweb.fractal.task.deployment.lib;

import org.objectweb.fractal.task.core.AbstractTask;
import org.objectweb.fractal.task.deployment.api.BindingProviderTask;

/**
 * AbstractBindingProviderTask implements BindingProviderTask.
 * 
 * Subclasses must implement: - public void execute(Object context) throws
 * Exception;
 * 
 * @author <a href="mailto:Philippe.Merle@inria.fr">Philippe Merle</a>
 * @author <a href="mailto:Frederic.Briclet@lifl.fr">Frederic Briclet</a>
 * 
 * @version 0.2
 */

public abstract class AbstractBindingProviderTask extends AbstractTask
		implements BindingProviderTask {
	// ==================================================================
	//
	// Internal state.
	//
	// ==================================================================

	/** Reference to the component binding. */
	private Object binding;

	// ==================================================================
	//
	// Constructor.
	//
	// ==================================================================

	/** The default constructor. */
	public AbstractBindingProviderTask() {
		// Call the default AbstractTask constructor.
		super();
	}

	// ==================================================================
	//
	// Public methods for
	// org.objectweb.deployment.scheduling.component.api.BindingProviderTask
	//
	// ==================================================================

	/**
	 * Return the reference of a component binding.
	 * 
	 * @return The reference of a component binding.
	 */
	public Object getBinding() {
		return binding;
	}

	/**
	 * Set the reference of a component binding.
	 * 
	 * @param reference -
	 *            The reference of a component binding.
	 */
	public void setBinding(Object reference) {
		binding = reference;
	}

	// ==================================================================
	//
	// Public methods for org.objectweb.deployment.scheduling.core.api.Task
	//
	// ==================================================================

	/**
	 * Return the result of the execution.
	 * 
	 * @return The result of the execution.
	 */
	public Object getResult() {
		// This task produces the binding as result.
		return getBinding();
	}

	/**
	 * Set the result of the execution.
	 * 
	 * @param result -
	 *            The result of the execution.
	 * 
	 * TODO: Is it really useful to provide this method?
	 */
	public void setResult(Object result) {
		// This task produces the binding as result.
		setBinding(result);
	}

}
