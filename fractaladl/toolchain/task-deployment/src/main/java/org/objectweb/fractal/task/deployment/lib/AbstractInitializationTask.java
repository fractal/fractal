/*====================================================================

 ObjectWeb Deployment Framework
 Copyright (C) 2004-2006 INRIA - USTL - LIFL - GOAL
 Copyright (C) 2006-2007 STMicroelectronics

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA

 Initial developer(s): Philippe Merle, Frederic Briclet.
 Contributor(s): Eric Bruneton.

 ====================================================================
 $Id$
 ====================================================================*/

package org.objectweb.fractal.task.deployment.lib;

import org.objectweb.fractal.task.core.TaskMap;
import org.objectweb.fractal.task.deployment.api.ConfigurationTask;
import org.objectweb.fractal.task.deployment.api.InitializationTask;

/**
 * AbstractInitializationTask implements InitializationTask.
 * 
 * Subclasses must implement: - public void execute(Object context) throws
 * Exception;
 * 
 * @author <a href="mailto:Philippe.Merle@inria.fr">Philippe Merle</a>
 * @author <a href="mailto:Frederic.Briclet@lifl.fr">Frederic Briclet</a>
 * 
 * @version 0.2
 */

public abstract class AbstractInitializationTask extends
		AbstractRequireInstanceProviderTask implements InitializationTask {


	// ==================================================================
	//
	// Constructor.
	//
	// ==================================================================

	/** The default constructor. */
	public AbstractInitializationTask() {
		// Call the default AbstractRequireInstanceProviderTask constructor.
		super();
	}


	// ==================================================================
	//
	// Public methods for
	// org.objectweb.deployment.scheduling.component.api.InitializationTask
	//
	// ==================================================================

	/**
	 * Return all the configuration tasks to execute before.
	 * 
	 * @return All the configuration tasks to execute before.
	 */
	public ConfigurationTask[] getConfigurationTasks() {

		TaskMap.TaskHole[] tasks = this.getPreviousTasks();
		ConfigurationTask[] result = new ConfigurationTask[tasks.length];
		int i = 0;
		for (TaskMap.TaskHole t : tasks) {
			result[i] = (ConfigurationTask) t.getTask();
			i++;
		}

		return result;
	}

	// ==================================================================
	//
	// Public methods for
	// org.objectweb.deployment.scheduling.component.api.RequireInstanceProviderTask
	//
	// ==================================================================

	// ==================================================================
	//
	// Public methods for org.objectweb.deployment.scheduling.core.api.Task
	//
	// ==================================================================

	/**
	 * Return the result of the execution.
	 * 
	 * @return The result of the execution.
	 */
	public Object getResult() {
		// This task produces no result.
		return null;
	}

	/**
	 * Set the result of the execution.
	 * 
	 * @param result -
	 *            The result of the execution.
	 * 
	 * TODO: Is it really useful to provide this method?
	 */
	public void setResult(Object result) {
		// Nothing to do as this task produces no result.
	}

}
