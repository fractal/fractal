/*====================================================================

 ObjectWeb Deployment Framework
 Copyright (C) 2004-2006 INRIA - USTL - LIFL - GOAL
 Copyright (C) 2006-2007 STMicroelectronics

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA

 Initial developer(s): Philippe Merle, Frederic Briclet.
 Contributor(s): Eric Bruneton.

 ====================================================================
 $Id$
 ====================================================================*/

package org.objectweb.fractal.task.deployment.lib;

import org.objectweb.fractal.task.core.AbstractTask;
import org.objectweb.fractal.task.core.TaskMap;
import org.objectweb.fractal.task.deployment.api.FactoryProviderTask;
import org.objectweb.fractal.task.deployment.api.RequireFactoryProviderTask;

/**
 * AbstractRequireFactoryProviderTask implements RequireFactoryProviderTask.
 * 
 * Subclasses must implement: - public void execute(Object context) throws
 * Exception; - public Object getResult(); - public void setResult(Object
 * result);
 * 
 * @author <a href="mailto:Philippe.Merle@inria.fr">Philippe Merle</a>
 * @author <a href="mailto:Frederic.Briclet@lifl.fr">Frederic Briclet</a>
 * 
 * @version 0.2
 */

public abstract class AbstractRequireFactoryProviderTask extends AbstractTask
		implements RequireFactoryProviderTask {
	// ==================================================================
	//
	// Internal state.
	//
	// ==================================================================

	/** Reference to the component factory provider task. */
	private TaskMap.TaskHole factoryProviderTask;

	// ==================================================================
	//
	// Constructor.
	//
	// ==================================================================

	/** The default constructor. */
	public AbstractRequireFactoryProviderTask() {
		// Call the default AbstractTask constructor.
		super();
	}

	// ==================================================================
	//
	// Public methods for
	// org.objectweb.deployment.scheduling.component.api.RequirFactoryProviderTask
	//
	// ==================================================================

	/**
	 * Return the component factory provider task.
	 * 
	 * @return The component factory provider task.
	 */
	public FactoryProviderTask getFactoryProviderTask() {
		return (this.factoryProviderTask) == null ? null
				: (FactoryProviderTask) factoryProviderTask.getTask();
	}

	/**
	 * Set the component factory provider task.
	 * 
	 * @param task -
	 *            The component factory provider task.
	 */
	public void setFactoryProviderTask(TaskMap.TaskHole task) {
		if (factoryProviderTask != null)
			removePreviousTask(factoryProviderTask);

		factoryProviderTask = task;

		if (factoryProviderTask != null)
			addPreviousTask(factoryProviderTask);
	}

}
