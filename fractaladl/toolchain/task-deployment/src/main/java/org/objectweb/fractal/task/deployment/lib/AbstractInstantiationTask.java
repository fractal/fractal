/*====================================================================

 ObjectWeb Deployment Framework
 Copyright (C) 2004-2006 INRIA - USTL - LIFL - GOAL
 Copyright (C) 2006-2007 STMicroelectronics

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA

 Initial developer(s): Philippe Merle, Frederic Briclet.
 Contributor(s): Eric Bruneton.

 ====================================================================
 $Id$
 ====================================================================*/

package org.objectweb.fractal.task.deployment.lib;

import org.objectweb.fractal.task.core.TaskMap;
import org.objectweb.fractal.task.deployment.api.FactoryProviderTask;
import org.objectweb.fractal.task.deployment.api.InstantiationTask;

/**
 * AbstractInstantiationTask implements InstantiationTask.
 * 
 * Subclasses must implement: - public void execute(Object context) throws
 * Exception;
 * 
 * @author <a href="mailto:Philippe.Merle@inria.fr">Philippe Merle</a>
 * @author <a href="mailto:Frederic.Briclet@lifl.fr">Frederic Briclet</a>
 * 
 * @version 0.2
 */

public abstract class AbstractInstantiationTask extends
		AbstractInstanceProviderTask implements InstantiationTask {
	// ==================================================================
	//
	// Internal state.
	//
	// ==================================================================

	/** Reference to the factory provider task. */
	private TaskMap.TaskHole factoryProviderTask_;

	// ==================================================================
	//
	// Constructor.
	//
	// ==================================================================

	/** The default constructor. */
	public AbstractInstantiationTask() {
		// Call the default AbstractInstanceProviderTask constructor.
		super();
	}

	// ==================================================================
	//
	// Public methods for
	// org.objectweb.deployment.scheduling.component.api.InstantiationTask
	//
	// ==================================================================

	// ==================================================================
	//
	// Public methods for
	// org.objectweb.deployment.scheduling.component.api.RequireFactoryProviderTask
	//
	// ==================================================================

	/**
	 * Return the component factory provider task.
	 * 
	 * @return The component factory provider task.
	 */
	public FactoryProviderTask getFactoryProviderTask() {
		return (factoryProviderTask_ == null) ? null
				: (FactoryProviderTask) factoryProviderTask_.getTask();
	}

	/**
	 * Set the component factory provider task.
	 * 
	 * @param task -
	 *            The component factory provider task.
	 */
	public void setFactoryProviderTask(TaskMap.TaskHole task) {
		if (factoryProviderTask_ != null)
			removePreviousTask(factoryProviderTask_);

		factoryProviderTask_ = task;

		if (factoryProviderTask_ != null)
			addPreviousTask(factoryProviderTask_);
	}

}
