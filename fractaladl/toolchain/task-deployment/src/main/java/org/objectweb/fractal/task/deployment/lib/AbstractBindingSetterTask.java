/*====================================================================

 ObjectWeb Deployment Framework
 Copyright (C) 2004-2006 INRIA - USTL - LIFL - GOAL
 Copyright (C) 2006-2007 STMicroelectronics

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA

 Initial developer(s): Philippe Merle, Frederic Briclet.
 Contributor(s): Eric Bruneton.

 ====================================================================
 $Id$
 ====================================================================*/

package org.objectweb.fractal.task.deployment.lib;

import org.objectweb.fractal.task.core.TaskMap;
import org.objectweb.fractal.task.deployment.api.BindingProviderTask;
import org.objectweb.fractal.task.deployment.api.BindingSetterTask;

/**
 * AbstractBindingSetterTask implements BindingSetterTask.
 * 
 * Subclasses must implement: - public void execute(Object context) throws
 * Exception;
 * 
 * @author <a href="mailto:Philippe.Merle@inria.fr">Philippe Merle</a>
 * @author <a href="mailto:Frederic.Briclet@lifl.fr">Frederic Briclet</a>
 * 
 * @version 0.2
 */

public abstract class AbstractBindingSetterTask extends
		AbstractConfigurationTask implements BindingSetterTask {
	// ==================================================================
	//
	// Internal state.
	//
	// ==================================================================

	/** Reference to the binding provider task. */
	private TaskMap.TaskHole bindingProviderTask;

	// ==================================================================
	//
	// Constructor.
	//
	// ==================================================================

	/** The default constructor. */
	public AbstractBindingSetterTask() {
		// Call the default AbstractConfigurationTask constructor.
		super();
	}

	// ==================================================================
	//
	// Internal methods.
	//
	// ==================================================================

	// ==================================================================
	//
	// Public methods for
	// org.objectweb.deployment.scheduling.component.api.BindingSetterTask
	//
	// ==================================================================

	// ==================================================================
	//
	// Public methods for
	// org.objectweb.deployment.scheduling.component.api.RequireBindingProviderTask
	//
	// ==================================================================

	/**
	 * Return the component binding provider task.
	 * 
	 * @return The component binding provider task.
	 */
	public BindingProviderTask getBindingProviderTask() {
		return (bindingProviderTask == null)
        ? null
        : (BindingProviderTask) bindingProviderTask.getTask();
	}

	/**
	 * Set the component binding provider task.
	 * 
	 * @param task -
	 *            The component binding provider task.
	 */
	public void setBindingProviderTask(TaskMap.TaskHole task) {
		if (bindingProviderTask != null)
			removePreviousTask(bindingProviderTask);

		bindingProviderTask = task;

		if (bindingProviderTask != null)
			addPreviousTask(bindingProviderTask);
	}

	// ==================================================================
	//
	// Public methods for
	// org.objectweb.deployment.scheduling.component.api.ConfigurationTask
	//
	// ==================================================================

	// ==================================================================
	//
	// Public methods for
	// org.objectweb.deployment.scheduling.component.api.RequireInstanceProviderTask
	//
	// ==================================================================

	// ==================================================================
	//
	// Public methods for org.objectweb.deployment.scheduling.core.api.Task
	//
	// ==================================================================

	/**
	 * Return the result of the execution.
	 * 
	 * @return The result of the execution.
	 */
	public Object getResult() {
		// This task produces no result.
		return null;
	}

	/**
	 * Set the result of the execution.
	 * 
	 * @param result -
	 *            The result of the execution.
	 * 
	 * TODO: Is it really useful to provide this method?
	 */
	public void setResult(Object result) {
		// Nothing to do.
	}

}
