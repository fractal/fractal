/**
 * ObjectWeb Task Framework
 * Copyright (C) 2002-2004 France Telecom R&D
 * Copyright (C) 2004-2006 INRIA - USTL - LIFL - GOAL
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 * Base on: org.objectweb.fractal.adl.TaskMap 
 *          from fractal.objectweb.org
 */

package org.objectweb.fractal.task.core;

import java.util.NoSuchElementException;

/**
 * A map associating {@link Task Tasks} to task identifiers and task types. Two
 * tasks of two distinct types may have the same identifier. <br>
 * Tasks are accessible using {@link TaskHole <i>task hole</i>}.
 * 
 * @see org.objectweb.fractal.task.core.TaskMap
 */
public interface TaskMap {

  /**
   * Returns the tasks contained in this task map.
   * 
   * @return the tasks contained in this task map.
   */
  Task[] getTasks();

  /**
   * Returns the task of the given type and identifier if it is already present
   * in the map, otherwise throws a NoSuchElementException.
   * 
   * @param type the type of the task that must be returned.
   * @param id the identifier of the task that must be returned.
   * @return the task whose type and identifier are given.
   * @throws java.util.NoSuchElementException if there is no such task.
   */
  Task getTask(String type, Object id) throws NoSuchElementException;

  /**
   * Returns the task hole of the given type and identifier. If there is no such
   * task, a new empty task hole is created, inserted in the task map and
   * returned. If a task hole already exist in the task map with the
   * {@link TaskHole#isOptional() optional} flag, this flag is cleared.
   * 
   * @param type the type of the task that must be returned.
   * @param id the identifier of the task that must be returned.
   * @return the task hole of task whose type and identifier are given.
   */
  TaskHole getTaskHole(String type, Object id);

  /**
   * Returns the task hole of the given type and identifier. If there is no such
   * task, a new empty task hole is created with the
   * {@link TaskHole#isOptional() optional} flag, inserted in the task map and
   * returned. Task depending on optional tasks should pay attention when they
   * try to access to these tasks. The task implementation should check if the
   * optional task hole is filled before retrieving the task. Note that if a
   * task hole is still {@link TaskHole#isOptional() optional} during the
   * execution of a task that depends on it, it means that the task hole has not
   * been filled; so the task that depends on it should not try to access the
   * task, the optional task hole contains.
   * 
   * @param type the type of the task that must be returned.
   * @param id the identifier of the task that must be returned.
   * @return the task hole of task whose type and identifier are given.
   * @see TaskHole#isOptional()
   */
  TaskHole getOptionalTaskHole(String type, Object id);

  /**
   * Adds the given task to this task map. If a task hole already exist in the
   * task map with the given type and identifier, it is filled with the given
   * task. Otherwise a new task hole is created and filled.
   * 
   * @param type the type of the task to be added.
   * @param id the identifier of the task to be added.
   * @param task the task itself.
   * @return the task hole of the added task.
   */
  TaskHole addTask(String type, Object id, Task task);

  /**
   * A task hole is a place holder for tasks. It may be empty which means that
   * someone needs a task for the associated type and identifier, but the task
   * has not yet been added. <br>
   * Task hole defines generic methods to {@link #addDependency add} or
   * {@link #removeDependency remove} task dependencies. If the task hole is
   * empty, these dependency are stored in an internal structure. If the task
   * hole is full, dependency modification are delegated to concrete task ({@link Task#addDependency}
   * and {@link Task#removeDependency}).
   * 
   * @see TaskMap#getTaskHole(String, Object)
   */
  interface TaskHole {

    /**
     * Adds the given task hole as a dependency of this task hole. The semantic
     * of this dependency is given by the <code>role</code> parameter.
     * 
     * @param task the task, this task hole depends on.
     * @param role the role of this dependency.
     * @param context additional parameter.
     * @throws IllegalArgumentException if the given <code>role</code> is
     *             incorrect.
     * @see Task#addDependency(TaskMap.TaskHole, Class, Object)
     */
    void addDependency(TaskHole task, Class<?> role, Object context);

    /**
     * Removes a previously added dependency.
     * 
     * @param task a dependency.
     * @param role the role of the dependency to remove.
     * @see Task#removeDependency(TaskMap.TaskHole, Class)
     */
    void removeDependency(TaskHole task, Class<?> role);

    /**
     * Returns the task that fills this hole or throws EmptyTaskHoleException if
     * the TaskHole is empty.
     * 
     * @return the task that fills this hole.
     * @throws EmptyTaskHoleException if the hole is empty.
     */
    Task getTask() throws EmptyTaskHoleException;

    /**
     * Returns <code>true</code> if the hole contains the Task, otherwise
     * returns false.
     * 
     * @return Returns <code>true</code> if the hole contains the Task,
     *         otherwise returns false.
     */
    boolean isFilled();

    /**
     * Returns <code>true</code> if the task that should fill this task hole
     * is optional. An optional task hole is created by the
     * {@link TaskMap#getOptionalTaskHole(String, Object)} method. The task hole
     * stays in an optional state until it is filled by a task, or someone calls
     * the {@link TaskMap#getTaskHole(String, Object)} with the same parameters.
     * 
     * @return <code>true</code> if the task that should fill this task hole
     *         is optional.
     */
    boolean isOptional();
  }
}
