/**
 * ObjectWeb Task Framework
 * Copyright (C) 2002-2004 France Telecom R&D
 * Copyright (C) 2004-2006 INRIA - USTL - LIFL - GOAL
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 *
 * Contributor(s): Matthieu Leclercq (allows null id and type)
 */

package org.objectweb.fractal.task.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Basic implementation of the {@link TaskMap} interface.
 */
// TODO BasicTaskMap should not override LinkdeHashMap, should use delegation
// instead.
public class BasicTaskMap
    extends
      LinkedHashMap<BasicTaskMap.Key, TaskMap.TaskHole> implements TaskMap {

  // --------------------------------------------------------------------------
  // Implementation of the TaskMap interface
  // --------------------------------------------------------------------------

  public Task[] getTasks() {
    final ArrayList<Task> tasks = new ArrayList<Task>();
    for (final TaskMap.TaskHole taskHole : values()) {
      if (!taskHole.isOptional()) {
        tasks.add(taskHole.getTask());
      }
    }
    return tasks.toArray(new Task[0]);
  }

  public Task getTask(final String type, final Object id) {
    final TaskMap.TaskHole taskHole = get(createTaskKey(type, id));
    if (taskHole == null || !taskHole.isFilled())
      throw new NoSuchElementException();
    return taskHole.getTask();
  }

  public TaskHole getTaskHole(final String type, final Object id) {
    final Key key = createTaskKey(type, id);
    TaskHoleImpl taskHole = (TaskHoleImpl) get(key);
    if (taskHole == null) {
      // create a new mandatory task hole
      taskHole = new TaskHoleImpl();
      put(key, taskHole);
    } else {
      // if the task hole already exist, ensure that it is mandatory.
      taskHole.setMandatory();
    }
    return taskHole;
  }

  public TaskHole getOptionalTaskHole(final String type, final Object id) {
    final Key key = createTaskKey(type, id);
    TaskHoleImpl taskHole = (TaskHoleImpl) get(key);
    if (taskHole == null) {
      // create a new optional task hole.
      taskHole = new TaskHoleImpl(true);
      put(key, taskHole);
    }
    return taskHole;
  }

  public TaskHole addTask(final String type, final Object id, final Task task) {
    final TaskHoleImpl taskHole = (TaskHoleImpl) getTaskHole(type, id);
    if (taskHole.task == null) taskHole.fill(task);
    return taskHole;
  }

  // --------------------------------------------------------------------------
  // Utility methods
  // --------------------------------------------------------------------------

  protected BasicTaskMap.Key createTaskKey(final String type, final Object id) {
    return new Key(type, id);
  }

  // --------------------------------------------------------------------------
  // Inner classes
  // --------------------------------------------------------------------------

  private static class TaskHoleImpl implements TaskMap.TaskHole {

    private Task                      task;
    private boolean                   optional;
    private final List<Dependency>    dependancies = new ArrayList<Dependency>();
    private final StackTraceElement[] holeCreation;

    TaskHoleImpl() {
      this(false);
    }

    TaskHoleImpl(final boolean optional) {
      holeCreation = new Throwable().getStackTrace();
      this.optional = optional;
    }

    // ------------------------------------------------------------------------
    // Implementation of the TaskHole interface
    // ------------------------------------------------------------------------

    public void addDependency(final TaskHole task, final Class<?> role,
        final Object context) {
      if (isFilled())
        this.task.addDependency(task, role, context);
      else
        dependancies.add(new Dependency(task, role, context));
    }

    public void removeDependency(final TaskHole task, final Class<?> role) {
      if (isFilled())
        this.task.removeDependency(task, role);
      else
        for (final Iterator<Dependency> iter = dependancies.iterator(); iter
            .hasNext();) {
          final Dependency dep = iter.next();
          if (dep.task.equals(task) && dep.role.equals(role)) {
            iter.remove();
          }
        }
    }

    public Task getTask() {
      if (task == null) throw new EmptyTaskHoleException(holeCreation);
      return task;
    }

    public boolean isFilled() {
      return task != null;
    }

    public boolean isOptional() {
      return optional;
    }

    // ------------------------------------------------------------------------
    // Utility methods and classes
    // ------------------------------------------------------------------------

    private void setMandatory() {
      optional = false;
    }

    private void fill(final Task task) {
      this.task = task;
      for (final Dependency dep : dependancies) {
        task.addDependency(dep.task, dep.role, dep.context);
      }
    }

    private static final class Dependency {
      private final TaskHole task;
      private final Class<?> role;
      private final Object   context;

      private Dependency(final TaskHole task, final Class<?> role,
          final Object context) {
        this.task = task;
        this.role = role;
        this.context = context;
      }

    }

  }

  /**
   * key class used by {@link BasicTaskMap this} implementation.
   */
  public static class Key {

    protected final String type;

    protected final Object id;

    private final int      hashcode;

    protected Key(final String type, final Object id) {
      this.type = type;
      this.id = id;
      hashcode = computeHashCode();
    }

    /**
     * @return the id of the task in the task map.
     */
    public Object getId() {
      return id;
    }

    /**
     * @return the type of the task in the task map.
     */
    public String getType() {
      return type;
    }

    @Override
    public boolean equals(final Object o) {
      if (o instanceof Key) {
        final Key k = (Key) o;
        return ((k.type == null) ? type == null : k.type.equals(type))
            && (k.id.equals(id));
      }
      return false;
    }

    @Override
    public int hashCode() {
      return hashcode;
    }

    protected int computeHashCode() {
      return ((type == null) ? 1 : type.hashCode()) * id.hashCode();

    }
  }
}
