/**
 * ObjectWeb Task Framework
 * Copyright (C) 2002-2004 France Telecom R&D
 * Copyright (C) 2004-2006 INRIA - USTL - LIFL - GOAL
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

import org.objectweb.fractal.task.core.TaskMap.TaskHole;

/**
 * Abstract {@link Task} implementation. Use a {@link Collection} to store
 * previous task.
 */
public abstract class AbstractTask implements Task {

  private final Collection<TaskHole> previousTasks = new LinkedHashSet<TaskHole>();

  protected final void addPreviousTask(final TaskHole task) {
    previousTasks.add(task);
  }

  protected final void removePreviousTask(final TaskHole task) {
    previousTasks.remove(task);
  }

  protected boolean doAddDependency(final TaskHole task, final Class<?> role,
      final Object context) {
    if (role == Task.PREVIOUS_TASK_ROLE) {
      addPreviousTask(task);
      return true;
    }
    return false;
  }

  protected boolean doRemoveDependency(final TaskHole task, final Class<?> role) {
    if (role == Task.PREVIOUS_TASK_ROLE) {
      removePreviousTask(task);
      return true;
    }
    return false;
  }

  // --------------------------------------------------------------------------
  // Implementation of the Task interface
  // --------------------------------------------------------------------------

  public void addDependency(final TaskHole task, final Class<?> role,
      final Object context) {
    if (!doAddDependency(task, role, context)) {
      throw new IllegalArgumentException("Unknown dependancy role: '" + role
          + "'");
    }
  }

  public void removeDependency(final TaskHole task, final Class<?> role) {
    if (!doRemoveDependency(task, role)) {
      throw new IllegalArgumentException("Unknown dependancy role: '" + role
          + "'");
    }
  }

  public TaskHole[] getPreviousTasks() {
    final List<TaskMap.TaskHole> tasks = new ArrayList<TaskHole>(previousTasks
        .size());
    for (final TaskMap.TaskHole task : previousTasks) {
      // if task is optional, pass it.
      if (task.isOptional()) continue;
      tasks.add(task);
    }
    return tasks.toArray(new TaskMap.TaskHole[0]);
  }
}
