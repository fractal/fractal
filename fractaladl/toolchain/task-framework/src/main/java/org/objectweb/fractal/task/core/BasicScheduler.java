/**
 * ObjectWeb Task Framework
 * Copyright (C) 2002-2004 France Telecom R&D
 * Copyright (C) 2004-2006 INRIA - USTL - LIFL - GOAL
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Initial developer(s): Philippe Merle, Frederic Briclet.
 * Contributor(s): Eric Bruneton, Matthieu Leclercq
 *
 * Based on: org.objectweb.deployment.scheduling.core.lib.BasicScheduler
 */

package org.objectweb.fractal.task.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * BasicScheduler implements Scheduler.
 */
public class BasicScheduler implements Scheduler {

  // --------------------------------------------------------------------------
  // Implementation of the Scheduler interface
  // --------------------------------------------------------------------------

  public void schedule(final Task[] tasks, final Map<Object, Object> context)
      throws TaskExecutionException {
    doSchedule(prepareScheduling(tasks), context);
  }

  // --------------------------------------------------------------------------
  // Utility methods
  // --------------------------------------------------------------------------

  /**
   * Prepare scheduling. TODO: Must be optimized and must detect cycle
   * dependencies between tasks.
   * 
   * @param tasks - The tasks to schedule.
   * @return The scheduled tasks.
   */
  protected Task[] prepareScheduling(final Task[] tasks) {
    final List<Task> result = new ArrayList<Task>();

    final Map<Task, List<Task>> allNextTasks = new HashMap<Task, List<Task>>();
    final Map<Task, List<Task>> allPreviousTasks = new HashMap<Task, List<Task>>();

    for (final Task currentTask : tasks) {
      allNextTasks.put(currentTask, new ArrayList<Task>());
      final List<Task> tl = new ArrayList<Task>();
      final TaskMap.TaskHole[] previousTaskHoles = currentTask
          .getPreviousTasks();
      for (final TaskMap.TaskHole element0 : previousTaskHoles) {
        tl.add(element0.getTask());
      }
      allPreviousTasks.put(currentTask, tl);
    }

    for (final Task currentTask : tasks) {
      final TaskMap.TaskHole[] previousTaskHoles = currentTask
          .getPreviousTasks();
      for (final TaskMap.TaskHole element0 : previousTaskHoles) {
        final List<Task> tl = allNextTasks.get(element0.getTask());
        tl.add(currentTask);
      }
    }

    while (allPreviousTasks.size() != 0) {
      for (final Task currentTask : tasks) {
        List<Task> previousTaskList = allPreviousTasks.get(currentTask);
        if (previousTaskList != null && previousTaskList.size() == 0) {
          result.add(currentTask);
          final List<Task> al = allNextTasks.get(currentTask);
          final Task[] nextTasks = al.toArray(new Task[al.size()]);
          for (final Task nextTask : nextTasks) {
            previousTaskList = allPreviousTasks.get(nextTask);
            previousTaskList.remove(currentTask);
          }
          allPreviousTasks.remove(currentTask);
        }
      }
    }

    return result.toArray(new Task[result.size()]);
  }

  protected void doSchedule(final Task[] tasks,
      final Map<Object, Object> context) throws TaskExecutionException {
    for (final Task currentTask : tasks) {
      try {
        currentTask.execute(context);
      } catch (final Exception e) {
        throw new TaskExecutionException(currentTask, e);
      }
    }
  }
}
