/**
 * ObjectWeb Task Framework
 * Copyright (C) 2002-2004 France Telecom R&D
 * Copyright (C) 2004-2006 INRIA - USTL - LIFL - GOAL
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Initial developer(s): Philippe Merle, Frederic Briclet.
 * Contributor(s): Eric Bruneton, Matthieu Leclercq
 * 
 * Based on: org.objectweb.deployment.scheduling.core.api.Scheduler
 */
// TODO fix header

package org.objectweb.fractal.task.core;

import java.util.Map;

/**
 * Scheduler is the base interface of any task scheduler.
 */
public interface Scheduler {

  /**
   * Schedule an array of tasks.
   * 
   * @param tasks The array of tasks to schedule.
   * @param context The context in which the task will be executed.
   * @throws TaskExecutionException if an exception occurs will executing a
   *             task.
   * @see Task#execute(Map)
   */
  void schedule(Task[] tasks, Map<Object, Object> context)
      throws TaskExecutionException;
}
