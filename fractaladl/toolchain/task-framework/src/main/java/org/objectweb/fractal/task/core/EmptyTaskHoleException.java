/***
 * ObjectWeb Task Framework
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.task.core;

import java.io.PrintStream;

/**
 * Exception thrown when trying to access to a task of an empty
 * {@link TaskMap.TaskHole task hole}.
 * 
 * @see TaskMap.TaskHole#getTask()
 */
public class EmptyTaskHoleException extends RuntimeException {

  private final StackTraceElement[] holeCreation;

  /**
   * Default constructor.
   * 
   * @param holeCreation the stack trace of the hole creation.
   */
  public EmptyTaskHoleException(final StackTraceElement[] holeCreation) {
    super("Task hole is empty");
    this.holeCreation = holeCreation;
  }

  @Override
  public void printStackTrace(final PrintStream s) {
    synchronized (s) {
      s.println(this);
      final StackTraceElement[] trace = getStackTrace();
      for (final StackTraceElement element : trace)
        s.println("\tat " + element);
      s.println("Hole created at:");
      // start at one to remove stack element of TaskHole creation.
      for (int i = 1; i < holeCreation.length; i++)
        s.println("\tat " + holeCreation[i]);
    }
  }

}
