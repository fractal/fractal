/**
 * ObjectWeb Task Framework
 * Copyright (C) 2002-2004 France Telecom R&D
 * Copyright (C) 2004-2006 INRIA - USTL - LIFL - GOAL
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Initial developer(s): Philippe Merle, Frederic Briclet.
 * Contributor(s): Eric Bruneton, Matthieu Leclercq
 *
 * Base on: org.objectweb.deployment.scheduling.core.api.Task
 */

package org.objectweb.fractal.task.core;

import java.util.Map;

import org.objectweb.fractal.task.core.TaskMap.TaskHole;

/**
 * Base interface of task. <br>
 * A task can be {@link #execute executed}. The execution of the task may
 * provides a {@link #getResult() result}.<br>
 * A task may depend on other tasks, the semantics of this dependency link is
 * not fixed (this interface defines a basic dependency link semantics for
 * {@link #PREVIOUS_TASK_ROLE previous} tasks: if a task must be executed before
 * this one). <br>
 * This interface defines generic methods to {@link #addDependency add} and
 * {@link #removeDependency remove} dependency. These methods take as parameter
 * a {@link TaskHole task hole} rather than a task; this allows to defines
 * dependency even if the task has not yet been added in the {@link TaskMap}.
 * 
 * @see TaskMap
 */
public interface Task {

  /**
   * The <code>role</code> used for previous tasks dependency.
   * 
   * @see #addDependency(TaskMap.TaskHole, Class, Object)
   */
  Class<?> PREVIOUS_TASK_ROLE = Task.class;

  /**
   * Adds the given task hole as a dependency of this task. The semantic of this
   * dependency is given by the <code>role</code> parameter.
   * 
   * @param task the task, this task hole depends on.
   * @param role the role of this dependency.
   * @param context additional parameter.
   * @throws IllegalArgumentException if the given <code>role</code> is
   *             incorrect.
   */
  void addDependency(TaskHole task, Class<?> role, Object context);

  /**
   * Removes a previously added dependency.
   * 
   * @param task a dependency.
   * @param role the role of the dependency to remove.
   */
  void removeDependency(TaskHole task, Class<?> role);

  /**
   * Return all the previous tasks.
   * 
   * @return All the previous tasks.
   */
  TaskHole[] getPreviousTasks();

  /**
   * Execute the task.
   * 
   * @param context The context in which the task will be executed.
   * @throws Exception Thrown if a problem occurs during execution.
   */
  void execute(Map<Object, Object> context) throws Exception;

  /**
   * Return the result of the execution.
   * 
   * @return The result of the execution.
   */
  Object getResult();

}
