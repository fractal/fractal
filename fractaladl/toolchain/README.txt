###################################################################################
#                OVERVIEW          
###################################################################################
This directory contains fractal ADL toolchain related modules:

################################################################################
#                 BUILD NOTES
################################################################################

To build FractalADL and install produced artifact in your local maven repository 
perform the following command:

$ mvn clean install


################################################################################

To deploy new artifacts on the OW2 maven repository perform the following 
command:

$ mvn clean deploy


################################################################################

To release a new stable version of FractalADL perform the two following commands:

$ mvn release:prepare -Dow.username=<your OW2 login>
$ mvn release:perform -Dow.username=<your OW2 login>

Where "<your OW2 login>" is your login on the OW2 forge. This is necessary only
if your local login does not match the OW2 one.
These commands will update version information in POMs, tag the repository, 
build and deploy new artifacts and documentations.
Notes: 
 - for some mysterious reasons, the first command may fail while creating the
SVN tag. In that case, simply re-run the same command again. (it seems that a 
race condition occurs when two SVN commands are executed one immediately after
the other)
 - the two previous commands can be shortened into the following one:

$ mvn release:prepare release:perform -Dow.username=<your OW2 login>

