/**
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA

  Contributor(s):
  - Philippe Merle (set the class loader used to obtain the Fractal ADL Factory)
 */

package org.objectweb.fractal.adl.mojo;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.model.FileSet;
import org.apache.maven.model.Resource;
import org.apache.maven.plugin.MojoExecutionException;

/**
 * Executes the fractaladl Java static generator with the enclosing project's
 * runtime dependencies as classpath + the mojo dependencies. This plugin lets
 * you specify:
 * <ul>
 * <li>the ADL definition to be compiled</li>
 * <li>the output directory</li>
 * <li>the factory to use to generate the component instantiation source code</li>
 * <li>the backend used by the factory</li>
 * </ul>
 * 
 * @goal generate
 * @phase generate-sources
 * @requiresDependencyResolution runtime
 * @author Matthieu Leclercq
 */
public class StaticJavaGeneratorMojo extends AbstractStaticJavaGeneratorMojo {

  /**
   * Directory where the output Java Files will be located.
   * 
   * @parameter expression="${project.build.directory}/generated-sources/fractaladl"
   * @required
   */
  protected File outputDirectory;

  public void execute() throws MojoExecutionException {

    generate();

    if (project != null) {
      project.addCompileSourceRoot(getOutputDirectory().getPath());
    }
  }

  @Override
  protected File getOutputDirectory() {
    return outputDirectory;
  }

  @Override
  protected ClassLoader getClassLoader() {
    try {
      // String outputDirectory = project.getBuild().getOutputDirectory();
      // URL targetClassesUrl = new File(outputDirectory).toURL();

      List<URL> classpathsUrls = new ArrayList<URL>();
      
      /* contains the enclosing project dependencies and sources */
      for (Object sourceRoots : project.getCompileSourceRoots()) {
        addToClasspath(classpathsUrls, (String) sourceRoots);
      }
      for (Object resource : project.getResources()) {
        addToClasspath(classpathsUrls, ((Resource) resource).getDirectory());
      }
      for (Object compileElem : project.getCompileClasspathElements()) {
        addToClasspath(classpathsUrls, (String) compileElem);
      }
      
      /*
       * the parent ClassLoader is needed to resolve the dependencies carried by
       * the MOJO itself
       */
      URLClassLoader result = new URLClassLoader(classpathsUrls
          .toArray(new URL[0]), Thread.currentThread()
          .getContextClassLoader());
      return result;
    } catch (Exception e) {
      e.printStackTrace();
      throw new RuntimeException(
          "Unable to adjust class loader to include compiled sources and runtime dependencies");
    }
  }
}
