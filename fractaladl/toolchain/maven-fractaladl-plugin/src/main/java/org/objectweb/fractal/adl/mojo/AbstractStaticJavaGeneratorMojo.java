/**
 * 
 */

package org.objectweb.fractal.adl.mojo;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.model.Resource;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.adl.Launcher;
import org.objectweb.fractal.adl.StaticJavaGenerator;
import org.objectweb.fractal.adl.StaticJavaGenerator.GenerationException;
import org.objectweb.fractal.adl.StaticJavaGenerator.InvalidCommandLineException;

/**
 * @author leclercm
 */
public abstract class AbstractStaticJavaGeneratorMojo extends AbstractMojo {

  /**
   * The fully qualified name of the factory to use (the corresponding .fractal
   * file must be in the classpath).
   * 
   * @parameter expression="org.objectweb.fractal.adl.StaticJavaGeneratorFactory"
   */
  protected String       factory;

  /**
   * The fully qualified name of the backend to use (the corresponding .fractal
   * file must be in the classpath).
   * 
   * @parameter
   */
  protected String       backend;

  /**
   * The fully qualified name of the component definitions to generate (must be
   * in the classpath).
   * 
   * @parameter
   */
  protected String[]     definitions;

  /**
   * The file that contains a list of component definition to generate (these
   * definitions must be in the classpath).
   * 
   * @parameter
   */
  protected File         adls;

  /**
   * The Maven project reference.
   * 
   * @parameter expression="${project}"
   * @required
   */
  protected MavenProject project;

  protected abstract File getOutputDirectory();

  protected void generate() throws MojoExecutionException {
    File outdir = getOutputDirectory();
    outdir.mkdirs();

    List<String> args = new ArrayList<String>();
    args.add(Launcher.PARAMETER_NAME_TRAILING_CHAR
        + StaticJavaGenerator.OUTPUT_PARAM_NAME
        + Launcher.PARAMETER_NAME_VALUE_SEPARATOR_CHAR + outdir.getPath());

    if (adls != null)
      args.add(Launcher.PARAMETER_NAME_TRAILING_CHAR
          + StaticJavaGenerator.INPUT_LIST_PARAM_NAME
          + Launcher.PARAMETER_NAME_VALUE_SEPARATOR_CHAR + adls.getPath());

    if (factory != null)
      args.add(Launcher.PARAMETER_NAME_TRAILING_CHAR
          + Launcher.FACTORY_PROPERTY_NAME
          + Launcher.PARAMETER_NAME_VALUE_SEPARATOR_CHAR + factory);

    if (backend != null)
      args.add(Launcher.PARAMETER_NAME_TRAILING_CHAR
          + FactoryFactory.BACKEND_PROPERTY_NAME
          + Launcher.PARAMETER_NAME_VALUE_SEPARATOR_CHAR + backend);

    if (definitions != null) {
      for (String definition : definitions) {
        args.add(definition);
      }
    }

    StaticJavaGenerator generator;
    try {
      generator = new StaticJavaGenerator(getClassLoader(), args.toArray(new String[0]));
    } catch (ADLException e) {
      throw new MojoExecutionException(
          "An error occurs while instantiating the FractalADL factory.", e);
    } catch (InvalidCommandLineException e) {
      throw new MojoExecutionException("Invalid configuration.", e);
    }

    try {
      generator.generate();
    } catch (GenerationException e) {
      throw new MojoExecutionException("", e);
    }
  }

  protected abstract ClassLoader getClassLoader();

  protected void addToClasspath(List<URL> classpathsUrls, String classpathEntry)
      throws MalformedURLException {
    classpathsUrls.add(new File(classpathEntry).toURI().toURL());
    getLog().debug(
        "Added classpath entry for mojo execution: " + classpathEntry);
  }
}