/**
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA

  Contributor(s):
  - Philippe Merle (set the class loader used to obtain the Fractal ADL Factory)
 */
package org.objectweb.fractal.adl;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;
import java.util.Map;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.julia.Julia;
import org.objectweb.fractal.util.Fractal;

/**
 * Executes the fractaladl Launcher in the current VM with the enclosing project's
 * runtime dependencies as classpath + the mojo dependencies.
 * 
 * This plugin lets you specify:
 * <ul>
 *   <li>the ADL file containing the top level component definition</li>
 *   <li>the name of the Runnable interface to run on the top level component</li>
 *   <li>the factory to use to instantiate the component</li>
 *   <li>the backend used by the factory</li>
 * </ul> 
 * 
 * 
 * Requires the enclosing project (runtime) dependencies
 * The compile phase must be / have been run before the MOJO
 * Associate the MOJO with the goal run
 * 
 * @author Alessio Pace
 * 
 * @execute phase="compile"
 * @requiresDependencyResolution runtime
 * @goal run
 */
public class JuliaLauncherMojo extends AbstractMojo {

	public final static String BASIC_FACTORY = "org.objectweb.fractal.adl.BasicFactory";

	/**
	 * The fully qualified name of the factory to use (the corresponding .fractal file must be in the classpath).
	 * 
	 * @parameter expression="org.objectweb.fractal.adl.BasicFactory"
	 */
	private String factory;

	/**
	 * The fully qualified name of the backend to use (the corresponding .fractal file must be in the classpath).
	 * 
	 * @parameter expression="org.objectweb.fractal.adl.FractalBackend"
	 */
	private String backend;

	/**
	 * The fully qualified name of the component definition to load (must be in the classpath).
	 * 
	 * @parameter
	 */
	private String definition;

	/**
	 * @parameter expression="r"
	 */
	private String runnableItf;

	/**
	 * @parameter expression="true"
	 */
	private boolean daemon;

	/**
	 * A comma separated value of Julia config files.
	 * 
	 * @parameter
	 */
	private String juliaConfig;

	/**
	 * The Maven project reference.
	 * 
	 * @parameter expression="${project}"
	 * @required
	 */
	private MavenProject project;

   /**
    * @parameter expression="${plugin.artifacts}"
    * @readonly
    */
   //private List pluginDependencies;

	public void execute() throws MojoExecutionException {

		/* this is the julia-launcher-plugin, so... */
		System.setProperty("fractal.provider", Julia.class.getCanonicalName());

		// Will be removed in Julia version 2.1.6
		//System.setProperty("julia.loader", DynamicLoader.class.getCanonicalName());

		if(juliaConfig!=null && juliaConfig.length() > 0)
			System.setProperty("julia.config", juliaConfig);

		/* get the current MOJO ClassLoader */
		ClassLoader initialClassLoader = Thread.currentThread().getContextClassLoader();
		getLog().debug("MOJO initial classloader: " + initialClassLoader);

		/* get the new ClassLoader */
		ClassLoader classLoader = getClassLoader(initialClassLoader);
		getLog().debug("MOJO new classloader: " + classLoader);

		/* 
		 * this is not technically needed because FractalADL tries to the ClassLoader 
		 * passed with the "classloader" key in the context Map, or falls back on 
		 * the XMLLoader's instance.class.getClassLoader() which is the same ClassLoader
		 * of the initialClassLoader variable (and which is not enough to load
		 * resources of the enclosing project)
		 */
		Thread.currentThread().setContextClassLoader(classLoader);

		try {
			new Launcher(classLoader, this.daemon).main(new String[]{factory, backend, definition, runnableItf});
		} catch(Exception e){
			e.printStackTrace();
			throw new MojoExecutionException("Unable to launch Fractal application", e);
		}

		/* set again the initial MOJO ClassLoader */
		Thread.currentThread().setContextClassLoader(initialClassLoader);
	}

	protected ClassLoader getClassLoader(final ClassLoader currentThreadClassLoader) {
		try {
			//String outputDirectory = project.getBuild().getOutputDirectory();
			//URL targetClassesUrl = new File(outputDirectory).toURL();

			/* contains the enclosing project dependencies and target/classes */
			List classpaths =  project.getRuntimeClasspathElements();

			/* copy the List of classpaths into an array of URL*/
			URL[] classpathsUrls = new URL[classpaths.size()];
			for(int i=0; i<classpaths.size(); i++){
				String classpathEntry = (String)classpaths.get(i);
				classpathsUrls[i] = new File(classpathEntry).toURL();
				getLog().debug("Added classpath entry for mojo execution: " + classpathEntry);
			}

			/* the parent ClassLoader is needed to resolve the dependencies carried by the MOJO itself */
			URLClassLoader result = new URLClassLoader(classpathsUrls, currentThreadClassLoader);
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to adjust class loader to include compiled sources and runtime dependencies");
		}
	}

	/**
	 * Authors: Eric Bruneton, Marc Léger
	 */
   class Launcher {

   	private ClassLoader classLoader;
   	private boolean daemon;

		private Launcher(ClassLoader classLoader, boolean daemon) {
			this.classLoader = classLoader;
		}

		public void main(final String[] args) throws Exception {

			Object comp = createComponent(args);

			if (comp instanceof Component) {
				LifeCycleController lc = null;
				try {
					lc = Fractal.getLifeCycleController((Component) comp);
				} catch (NoSuchInterfaceException ignored) {
				}
				if (lc != null) {
					getLog().debug("Starting component... ");
					lc.startFc();
				}
				Runnable r = null;
				try {
					r = (Runnable) ((Component) comp).getFcInterface(args[3]);
				} catch (NoSuchInterfaceException ignored) {
				}
				if (r != null) {
					getLog().debug("Executing component... ");
					r.run();
				}
				if ((lc != null) && !this.daemon) {
					getLog().debug("Stopping component... ");
					lc.stopFc();
				}
			} else {
				if (comp instanceof LifeCycleController) {
					getLog().debug("Starting component... ");
					((LifeCycleController) comp).startFc();
				}
				if (comp instanceof Runnable) {
					getLog().debug("Executing component... ");
					((Runnable) comp).run();
				}
               if ((comp instanceof LifeCycleController) && !this.daemon) {
					getLog().debug("Stopping component... ");
			        ((LifeCycleController) comp).stopFc();
               }
			}
		}

		private Object createComponent(String[] pargs) throws Exception {
			Map context = ContextMap.instance();
			context.putAll(System.getProperties());
			/* pass the current thread ClassLoader to the Factory in order to resolve resources inside "target/classes" */
			context.put("classloader", this.classLoader);

			getLog().debug("Obtaining Factory to instantiate the component definition");
			Factory f = FactoryFactory.getFactory(pargs[0], pargs[1], context);

			Map componentCreationParams = ContextMap.instance(); //new HashMap(System.getProperties());

			componentCreationParams.putAll(System.getProperties());
			/* pass the current thread ClassLoader to the Factory in order to resolve resources inside "target/classes" */
			componentCreationParams.put("classloader", this.classLoader);

			getLog().info("Loading component definition " + pargs[2]);
			Object component = f.newComponent(pargs[2], componentCreationParams);

			if (pargs[1].equals(FactoryFactory.JAVA_BACKEND))
				return ((Map) component).get(pargs[3]);
			return component;
		}
   }
}
