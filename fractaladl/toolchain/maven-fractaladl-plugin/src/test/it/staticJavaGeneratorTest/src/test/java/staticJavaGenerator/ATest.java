package staticJavaGenerator;

import org.objectweb.fractal.adl.JavaFactory;
import org.objectweb.fractal.adl.StaticJavaGenerator;

import junit.framework.TestCase;

public class ATest extends TestCase {
  
  public void test() throws Exception {
    final Class factoryFactoryClass = getClass().getClassLoader().loadClass("staticJavaGenerator.Definition2"
        + StaticJavaGenerator.CLASSNAME_SUFFIX);
    final JavaFactory factoryFactory = (JavaFactory) factoryFactoryClass
        .newInstance();
    assertNotNull(factoryFactory.newComponent());
  }
}