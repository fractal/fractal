/***
 * Cecilia
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl;

import java.io.File;
import java.util.Arrays;

import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.apache.maven.project.MavenProject;
import org.objectweb.fractal.adl.mojo.StaticJavaGeneratorTestMojo;

/**
 * Test the {@link CarMojo} mojo.
 */
public class StaticJavaGeneratorTestMojoTest extends AbstractMojoTestCase {

  private static final String POM = "/target/test-classes/unit/staticJavaGeneratorTest/plugin-config.xml";

  protected void setUp() throws Exception {
    // required for mojo lookups to work
    super.setUp();
  }

  /**
   * tests the proper discovery and configuration of the mojo
   * 
   * @throws Exception
   */
  public void testEnvironment() throws Exception {
    File testPom = new File(getBasedir(), POM);
    StaticJavaGeneratorTestMojo mojo =  (StaticJavaGeneratorTestMojo) lookupMojo(
        "generate-test", testPom);
    assertNotNull(mojo);

    assertEquals("factoryValue", (String) getVariableValueFromObject(mojo,
        "factory"));
    assertEquals("backendValue", (String) getVariableValueFromObject(mojo,
        "backend"));
    assertEquals(new File("adlsValue"), getVariableValueFromObject(
        mojo, "adls"));
    assertEquals(new File("outputDirectoryValue"), getVariableValueFromObject(
        mojo, "outputDirectory"));
    assertEquals(Arrays.asList(new String[]{"def1", "def2", "def3"}), Arrays
        .asList(((String[]) getVariableValueFromObject(mojo, "definitions"))));

    assertEquals("foo", ((MavenProject) getVariableValueFromObject(mojo,
        "project")).getGroupId());
    assertEquals("bar", ((MavenProject) getVariableValueFromObject(mojo,
        "project")).getArtifactId());
  }
}
