/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 *
 * Contributor(s): Philippe Merle
 *                 Matthieu Leclercq (externalize interface and implementation
 *   loading)
 */

package org.objectweb.fractal.adl.implementations;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.attributes.Attributes;
import org.objectweb.fractal.adl.attributes.AttributesContainer;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.ChainedErrorLocator;
import org.objectweb.fractal.adl.interfaces.IDLLoader;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;

/**
 * A {@link org.objectweb.fractal.adl.Loader} to check {@link Implementation}
 * nodes in definitions. This loader checks that the Java classes specified in
 * these nodes exist.
 */

public class ImplementationLoader extends AbstractLoader {

  // ---------------------------------------------------------------------------
  // Client interfaces
  // ---------------------------------------------------------------------------

  /** The name of the {@link #interfaceLoaderItf} client interface. */
  public static final String      INTERFACE_LOADER_CLIENT_ITF_NAME      = "interface-loader";

  /** The {@link IDLLoader} used by this loader to load interface code. */
  public IDLLoader                interfaceLoaderItf;

  /** The name of the {@link #implementationCodeLoaderItf} client interface. */
  public static final String      IMPLEMENTATION_LOADER_CLIENT_ITF_NAME = "implementation-code-loader";

  /**
   * The {@link ImplementationCodeLoader} used by this loader to load
   * implementation code.
   */
  public ImplementationCodeLoader implementationCodeLoaderItf;

  // ---------------------------------------------------------------------------
  // Implementation of the Loader interface
  // ---------------------------------------------------------------------------

  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    final Definition d = clientLoader.load(name, context);
    checkNode(d, context);
    return d;
  }

  // ---------------------------------------------------------------------------
  // Checking methods
  // ---------------------------------------------------------------------------

  protected void checkNode(final Object node, final Map<Object, Object> context)
      throws ADLException {
    if (node instanceof ImplementationContainer) {
      checkImplementationContainer((ImplementationContainer) node, context);
    }
    if (node instanceof ControllerContainer) {
      checkControllerContainer((ControllerContainer) node);
    }
    if (node instanceof ComponentContainer) {
      for (final Component comp : ((ComponentContainer) node).getComponents()) {
        checkNode(comp, context);
      }
    }
  }

  protected void checkImplementationContainer(
      final ImplementationContainer container, final Map<Object, Object> context)
      throws ADLException {
    final Implementation impl = container.getImplementation();
    if (impl == null) return;

    final String className = impl.getClassName();
    if (className == null) {
      throw new ADLException(ImplementationErrors.IMPLEMENTATION_MISSING, impl);
    }
    Object c;
    try {
      c = implementationCodeLoaderItf.loadImplementation(className, null,
          context);
    } catch (final ADLException e) {
      ChainedErrorLocator.chainLocator(e, impl);
      throw e;
    }
    impl.astSetDecoration("code", c);

    if (!(c instanceof Class)) return;

    final Class<?> implemClass = (Class<?>) c;
    if (container instanceof InterfaceContainer) {
      for (final Interface itf : ((InterfaceContainer) container)
          .getInterfaces()) {
        if (!(itf instanceof TypeInterface)) continue;

        final TypeInterface tItf = (TypeInterface) itf;
        if (tItf.getRole().equals(TypeInterface.SERVER_ROLE)) {
          final Object d = interfaceLoaderItf.loadInterface(
              tItf.getSignature(), context);
          if (d instanceof Class) {
            if (!((Class<?>) d).isAssignableFrom(implemClass)) {
              throw new ADLException(
                  ImplementationErrors.CLASS_DOES_NOT_IMPLEMENT_INTERFACE,
                  impl, implemClass.getName(), ((Class<?>) d).getName());
            }
          }
        }
      }
    }
    if (container instanceof AttributesContainer) {
      final Attributes attrs = ((AttributesContainer) container)
          .getAttributes();
      if (attrs != null) {
        final Object d = interfaceLoaderItf.loadInterface(attrs.getSignature(),
            context);
        if (d instanceof Class) {
          if (!((Class<?>) d).isAssignableFrom(implemClass)) {
            throw new ADLException(
                ImplementationErrors.CLASS_DOES_NOT_IMPLEMENT_ATTR_CTRL, impl,
                implemClass.getName(), ((Class<?>) d).getName());
          }
        }
      }
    }
  }

  protected void checkControllerContainer(final ControllerContainer container)
      throws ADLException {
    final Controller ctrl = container.getController();
    if (ctrl != null) {
      if (ctrl.getDescriptor() == null) {
        throw new ADLException(ImplementationErrors.MISSING_CONTROLLER_DESC,
            ctrl);
      }
    }
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  @Override
  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (INTERFACE_LOADER_CLIENT_ITF_NAME.equals(s)) {
      this.interfaceLoaderItf = (IDLLoader) o;
    } else if (IMPLEMENTATION_LOADER_CLIENT_ITF_NAME.equals(s)) {
      this.implementationCodeLoaderItf = (ImplementationCodeLoader) o;
    } else {
      super.bindFc(s, o);
    }
  }

  @Override
  public String[] listFc() {
    final String[] superList = super.listFc();
    final String[] list = new String[superList.length + 2];
    list[0] = INTERFACE_LOADER_CLIENT_ITF_NAME;
    list[1] = IMPLEMENTATION_LOADER_CLIENT_ITF_NAME;
    System.arraycopy(superList, 0, list, 2, superList.length);
    return list;
  }

  @Override
  public Object lookupFc(final String s) throws NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (INTERFACE_LOADER_CLIENT_ITF_NAME.equals(s)) {
      return this.interfaceLoaderItf;
    } else if (IMPLEMENTATION_LOADER_CLIENT_ITF_NAME.equals(s)) {
      return this.implementationCodeLoaderItf;
    } else {
      return super.lookupFc(s);
    }

  }

  @Override
  public void unbindFc(final String s) throws NoSuchInterfaceException,
      IllegalBindingException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (INTERFACE_LOADER_CLIENT_ITF_NAME.equals(s)) {
      this.interfaceLoaderItf = null;
    } else if (IMPLEMENTATION_LOADER_CLIENT_ITF_NAME.equals(s)) {
      this.implementationCodeLoaderItf = null;
    } else {
      super.unbindFc(s);
    }
  }
}
