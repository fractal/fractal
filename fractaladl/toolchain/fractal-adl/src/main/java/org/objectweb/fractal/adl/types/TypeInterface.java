/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 *
 * Contributor: Philippe Merle
 */

package org.objectweb.fractal.adl.types;

import org.objectweb.fractal.adl.interfaces.Interface;

/**
 * An AST node interface to define typed interfaces.
 */

public interface TypeInterface extends Interface {
  final String SINGLETON_CARDINALITY = "singleton";
  final String COLLECTION_CARDINALITY = "collection";
  final String MANDATORY_CONTINGENCY = "mandatory";
  final String OPTIONAL_CONTINGENCY = "optional";
  final String SERVER_ROLE = "server";
  final String CLIENT_ROLE = "client";
  String getCardinality ();
  void setCardinality (String arg);
  String getContingency ();
  void setContingency (String arg);
  String getSignature ();
  void setSignature (String arg);
  String getRole ();
  void setRole (String arg);
}
