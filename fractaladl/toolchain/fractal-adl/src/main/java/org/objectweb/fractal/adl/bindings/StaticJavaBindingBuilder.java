/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.bindings;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.ContextLocal;

/**
 * A Java based, static implementation of the {@link BindingBuilder} interface.
 * This implementation produces standard Java code that binds components.
 */

public class StaticJavaBindingBuilder implements BindingBuilder {

  private final ContextLocal<State> states = new ContextLocal<State>();

  // --------------------------------------------------------------------------
  // Implementation of the BindingBuilder interface
  // --------------------------------------------------------------------------

  public void bindComponent(final int type, final Object clientComp,
      final String clientItf, final Object serverComp, final String serverItf,
      final Object context) {

    // get the context local state.
    State state = states.get(context);
    if (state == null) {
      state = new State();
      states.set(context, state);
    }

    // get the output print writer.
    final PrintWriter pw = (PrintWriter) ((Map<?, ?>) context)
        .get("printwriter");

    // create clientSide and serverSide that identify this binding.
    BindingEnd clientSide = new BindingEnd();
    clientSide.comp = (String) clientComp;
    clientSide.itf = clientItf;
    BindingEnd serverSide = new BindingEnd();
    serverSide.comp = (String) serverComp;
    serverSide.itf = serverItf;

    /*
     * Since a single binding may resolve many pending ones, we add the new
     * binding in the map and re-evaluate the whole graph.
     */
    state.bindings.put(clientSide, serverSide);

    // for each pending bindings (use iterator since entry may be removed)
    for (final Iterator<Map.Entry<BindingEnd, BindingEnd>> iter = state.bindings
        .entrySet().iterator(); iter.hasNext();) {

      final Map.Entry<BindingEnd, BindingEnd> entry = iter.next();
      clientSide = entry.getKey();
      serverSide = entry.getValue();
      final boolean clientComposite = clientSide.isComposite();

      // try to find the primitive server component bound to the client...
      while (serverSide != null && serverSide.isComposite()) {
        if (!clientComposite) {
          // store reverse import binding information
          Set<BindingEnd> reverseBindings = state.reverseImportBindings
              .get(serverSide);
          StringBuffer buf = null;
          if (reverseBindings == null) {
            reverseBindings = new HashSet<BindingEnd>();
            state.reverseImportBindings.put(serverSide, reverseBindings);
            // declare the reverse binding set.
            buf = new StringBuffer();
            buf.append("java.util.Set<").append(JavaBindingEnd.class.getName())
                .append("> ").append(serverSide.comp).append(
                    serverSide.itf.replace('-', '_')).append(
                    "ReverseBindings = new java.util.HashSet<").append(
                    JavaBindingEnd.class.getName()).append(">();").append("\n");
            // put it in the composite map.
            buf.append(serverSide.comp).append(".put(\"").append(
                JavaBindingUtil.IMPORT_BINDING_PREFIX).append(serverSide.itf)
                .append("\", ").append(serverSide.comp).append(
                    serverSide.itf.replace('-', '_')).append(
                    "ReverseBindings);\n");
          }
          if (reverseBindings.add(clientSide)) {
            // store new reverseBinding
            if (buf == null) buf = new StringBuffer();
            buf.append(serverSide.comp)
                .append(serverSide.itf.replace('-', '_')).append(
                    "ReverseBindings.add(new ").append(
                    JavaBindingEnd.class.getName()).append("(").append(
                    clientSide.comp).append(", \"").append(clientSide.itf)
                .append("\"));\n");
            pw.print(buf.toString());
          }
        }
        // while the server is a composite
        // find a binding for which the client side is the current server.
        serverSide = state.bindings.get(serverSide);
      }

      // if a primitive server has been found.
      if (serverSide != null) {
        // bind client to server
        if (clientComposite) {
          if (state.exportBinding.add(clientSide)) {
            final StringBuffer buf = new StringBuffer();
            buf.append(clientSide.comp).append(".put(\"")
                .append(clientSide.itf).append("\", ").append(serverSide.comp)
                .append(");");
            pw.println(buf.toString());
          }
        } else {
          pw.print(clientSide.comp);
          pw.print(".bindFc(\"");
          pw.print(clientSide.itf);
          pw.print("\", ");
          pw.print(serverSide.comp);
          pw.println(");");

          // client is a primitive, this binding has been done correctly,
          // we can remove binding form list of pending bindings.
          iter.remove();
        }
      }
    }
  }

  // --------------------------------------------------------------------------
  // Inner classes
  // --------------------------------------------------------------------------

  static class BindingEnd {
    String comp;
    String itf;

    boolean isComposite() {
      return !comp.startsWith("P");
    }

    /**
     * @param serverSide
     * @param pw
     */
    void bind(final BindingEnd serverSide, final PrintWriter pw) {
      // TODO Auto-generated method stub

    }

    @Override
    public boolean equals(final Object obj) {
      if (obj == this) return true;
      if (obj instanceof BindingEnd) {
        return ((BindingEnd) obj).comp.equals(comp)
            && ((BindingEnd) obj).itf.equals(itf);
      }
      return false;
    }

    @Override
    public int hashCode() {
      return comp.hashCode() * itf.hashCode();
    }
  }

  static class State {
    Map<BindingEnd, BindingEnd>      bindings              = new HashMap<BindingEnd, BindingEnd>();
    Map<BindingEnd, Set<BindingEnd>> reverseImportBindings = new HashMap<BindingEnd, Set<BindingEnd>>();
    Set<BindingEnd>                  exportBinding         = new HashSet<BindingEnd>();
  }
}
