/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.arguments;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Node;

/**
 * A delegating loader that replace argument references by their actual values.
 */
public class ArgumentLoader extends AbstractLoader {

  /** The context key prefix used to retrieve argument values. */
  public static final String ARGUMENT_CONTEXT_PREFIX = "__arg__";

  /**
   * The separator used to separate prefix and argument name in a substitution
   * pattern.
   */
  public static final String PREFIX_SEPARATOR        = ":";
  /** The prefix to be used to access a Java system property. */
  public static final String JAVA_PROPERTY_PREFIX    = "java";
  /** The prefix to be used to access a context value. */
  public static final String CONTEXT_PROPERTY_PREFIX = "context";

  // ---------------------------------------------------------------------------
  // Implementation of the Loader interface
  // ---------------------------------------------------------------------------

  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    final Definition d = clientLoader.load(name, context);
    final Map<String, Object> values = new HashMap<String, Object>();
    if (d instanceof ArgumentDefinition) {
      final String args = ((ArgumentDefinition) d).getArguments();
      if (args != null) {
        int i = 0;
        for (final String arg : args.split(",")) {
          String argname, argdef;
          final int j = arg.indexOf('=');
          if (j != -1) {
            argname = arg.substring(0, j).trim();
            argdef = arg.substring(j + 1).trim();
          } else {
            argname = arg.trim();
            argdef = null;
          }
          if (argname.length() == 0) {
            throw new ADLException(ArgumentErrors.EMPTY_ARGUMENT_NAME, d, args);
          }

          if (argname.contains(":")) {
            throw new ADLException(ArgumentErrors.INVALID_ARGNAME, d, argname,
                ":");
          }

          if (argname.contains("$")) {
            throw new ADLException(ArgumentErrors.INVALID_ARGNAME, d, argname,
                "$");
          }

          // resolve argument value:
          // first look for (ARGUMENT_CONTEXT_PREFIX + argname) in the context;
          // this value is supposed to be put by the ArgumentComponentLoader
          // second look for (ARGUMENT_CONTEXT_PREFIX + index) in the context;
          // this value is supposed to be put by the ArgumentComponentLoader
          // third look for a default value
          Object argval = context.get(ARGUMENT_CONTEXT_PREFIX + argname);
          if (argval == null)
            argval = context.get(ARGUMENT_CONTEXT_PREFIX + i);
          if (argval == null) argval = evaluate(argdef, values, d, context);
          if (argval == null)
            throw new ADLException(ArgumentErrors.NO_VALUE, d, argname);
          values.put(argname, argval);
          ++i;
        }
        ((ArgumentDefinition) d).setArguments(null);
      }
    }

    // purge context: remove entries whose key starts with
    // ARGUMENT_CONTEXT_PREFIX
    if (context != null) {
      final Iterator<Object> contextKeys = context.keySet().iterator();
      while (contextKeys.hasNext()) {
        final Object contextKey = contextKeys.next();
        if ((contextKey instanceof String)
            && (((String) contextKey).startsWith(ARGUMENT_CONTEXT_PREFIX)))
          contextKeys.remove();
      }
    }

    evaluate(d, values, context);
    return d;
  }

  // ---------------------------------------------------------------------------
  // Argument evaluation methods
  // ---------------------------------------------------------------------------

  void evaluate(final Node node, final Map<String, Object> values,
      final Map<Object, Object> context) throws ADLException {
    final Map<String, String> attrs = node.astGetAttributes();
    for (final Map.Entry<String, String> attrEntry : attrs.entrySet()) {
      final String attr = attrEntry.getKey();
      String value = attrEntry.getValue();
      if (value != null) {
        value = evaluate(value, values, node, context);
        attrs.put(attr, value);
      }
    }
    node.astSetAttributes(attrs);

    for (final String nodeType : node.astGetNodeTypes()) {
      for (final Node n : node.astGetNodes(nodeType)) {
        if (n != null) {
          evaluate(n, values, context);
        }
      }
    }
  }

  // TODO created dedicated exception class.
  String evaluate(final String s, final Map<String, Object> values,
      final Node location, final Map<Object, Object> context)
      throws ADLException {
    if (s == null) return null;
    final String[] tokens = ArgumentHelper.splitNameRef(s);
    if (tokens == null) {
      throw new ADLException(ArgumentErrors.INVALID_PATTERN_SYNTAX_ERROR,
          location, s);
    }

    if (tokens.length == 1) return tokens[0];

    final String arg = tokens[1];
    String argname, argdef;
    Object value;

    int i = arg.indexOf('=');
    if (i != -1) {
      argname = arg.substring(0, i);
      argdef = arg.substring(i + 1);
    } else {
      argname = arg;
      argdef = null;
    }

    i = argname.indexOf(PREFIX_SEPARATOR);
    if (i == -1) {
      if (argdef != null)
        throw new ADLException(ArgumentErrors.INVALID_PATTERN_DEFAULT_VALUE,
            location, argname);
      value = values.get(argname);
      if (value == null)
        throw new ADLException(ArgumentErrors.UNDEFINED_ARGUMENT, location,
            argname);
    } else {
      // the argument name contains a prefix.
      final String prefix = argname.substring(0, i);
      argname = argname.substring(i + 1);
      if (prefix.equals(JAVA_PROPERTY_PREFIX)) {
        value = System.getProperty(argname);
      } else if (prefix.equals(CONTEXT_PROPERTY_PREFIX)) {
        value = context.get(argname);
      } else {
        throw new ADLException(ArgumentErrors.UNKNOWN_PREFIX, location, prefix,
            arg);
      }
      if (value == null) value = evaluate(argdef, values, location, context);
      if (value == null)
        throw new ADLException(ArgumentErrors.UNDEFINED_ARGUMENT, location,
            argname);
      if (!(value instanceof String))
        throw new ADLException(ArgumentErrors.INVALID_VALUE_NOT_A_STRING,
            location, argname);
    }
    return evaluate(tokens[0] + value + tokens[2], values, location, context);
  }
}
