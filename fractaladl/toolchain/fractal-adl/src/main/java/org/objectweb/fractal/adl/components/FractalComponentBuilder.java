/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributor: Philippe Merle
 *
 * $Id$
 */

package org.objectweb.fractal.adl.components;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.util.Fractal;

/**
 * A Fractal based implementation of the {@link ComponentBuilder} interface. 
 * This implementation uses the Fractal API to add and start components. 
 */

public class FractalComponentBuilder implements ComponentBuilder {
  
  // --------------------------------------------------------------------------
  // Implementation of the ComponentBuilder interface
  // --------------------------------------------------------------------------
  
  public void addComponent (
    final Object superComponent, 
    final Object subComponent,
    final String name,
    final Object context) throws Exception
  {
    Fractal.getContentController((Component)superComponent)
      .addFcSubComponent((Component)subComponent);

    // Next lines are commented as setFcName() is already called by
    // implementations.FractalImplementationBuilder.createComponent(...)
    //
    // try {
    //  Fractal.getNameController((Component)subComponent).setFcName(name);
    // } catch (NoSuchInterfaceException ignored) {
    // }
  }

  public void startComponent (final Object component, final Object context) 
    throws Exception 
  {
    /*
    try {
      Fractal.getLifeCycleController((Component)component).startFc();
    } catch (NoSuchInterfaceException ignored) {
    } catch (NullPointerException ignored) {
    }
    */
  }
}
