/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.bindings;

import java.io.PrintWriter;
import java.util.Map;

/**
 * A Fractal based, static implementation of the {@link BindingBuilder} 
 * interface. This implementation produces code that uses the Fractal API to 
 * bind components. 
 */

public class StaticFractalBindingBuilder implements BindingBuilder {
  
  // --------------------------------------------------------------------------
  // Implementation of the BindingBuilder interface
  // --------------------------------------------------------------------------
  
  public void bindComponent (
    final int type,
    final Object client, 
    final String clientItf, 
    final Object server, 
    final String serverItf, 
    final Object context) throws Exception
  {
    PrintWriter pw = (PrintWriter)((Map)context).get("printwriter");
    pw.print("Fractal.getBindingController(");
    pw.print(client);
    pw.print(").bindFc(\"");
    pw.print(clientItf);
    pw.print("\", ");
    if (type == IMPORT_BINDING) {
      pw.print("Fractal.getContentController(");
      pw.print(server);
      pw.print(").getFcInternalInterface(\"");
      pw.print(serverItf);
      pw.print("\")");
    } else {
      pw.print(server);
      pw.print(".getFcInterface(\"");
      pw.print(serverItf);
      pw.print("\")");
    }
    pw.println(");");
  }  
}

