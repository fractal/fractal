/***
 * Fractal ADL Parser
 * Copyright (C) 2006-2007 Universitetet i Oslo
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Romain Rouvoy
 */

package org.objectweb.fractal.adl;

import java.util.HashMap;
import java.util.Map;

/**
 * Context map used to fill the definition arguments. The map is automatically
 * filled with system properties tuples. System properties are also available
 * using the "java:" prefix in arguments.
 * 
 * @author Romain Rouvoy
 */
public class ContextMap extends HashMap<Object, Object> {

  private static final long  serialVersionUID      = -6036798588374546902L;
  // ************************ ContextMap factory *****************************
  /** System property specifying the implementation of the context map to use. */
  public static final String CONTEXT_MAP_PROPERTY  = "fractaladl.context-map";
  /** Default class loaded by the factory. */
  public static final String DEFAULT_CONTEXT_CLASS = "org.objectweb.fractal.adl.ContextMap";

  /**
   * Retrieve the class definition pointed by the name parameter.
   * 
   * @param name the name of the class to load.
   * @return the definition of the class.
   * @throws ClassNotFoundException thrown if the name points a class not
   *             available in the classloader
   */
  private static Class<?> _forName(final String name)
      throws ClassNotFoundException {
    return Thread.currentThread().getContextClassLoader().loadClass(name);
  }

  /**
   * Factory method creating a new instance of the context map pointed by the
   * "fractaladl.context-map" system property.<br>
   * Default class used by the factory is ContextMap.
   * 
   * @return instance of a context map.
   */
  @SuppressWarnings("unchecked")
  public static Map<Object, Object> instance() {
    final String cls = System.getProperty(CONTEXT_MAP_PROPERTY,
        DEFAULT_CONTEXT_CLASS);
    try {
      return (Map) _forName(cls).newInstance();
    } catch (final Exception e) {
      return new ContextMap();
    }
  }

  // ************************ ContextMap instance ****************************
  /** Prefix separator used in Fractal ADL definitions. */
  public static final String                 PREFIX_SEPARATOR  = ":";
  /** Specific prefix for accessing System properties. */
  public static final String                 JAVA_PROPS_PREFIX = "java";

  protected Map<String, Map<Object, Object>> prefix            = new HashMap<String, Map<Object, Object>>();

  /**
   * Default constructor loading the context map with system properties.<br>
   * System property values are made available from Fractal ADL definitions
   * using syntax ${java:property-name}.
   */
  public ContextMap() {
    this.prefix.put(JAVA_PROPS_PREFIX, System.getProperties());
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.HashMap#get(java.lang.Object)
   */
  @Override
  public Object get(final Object key) {
    final String skey = key.toString();

    final int i = skey.indexOf(PREFIX_SEPARATOR);
    if (i == -1) return super.get(key);

    final Map<Object, Object> m = this.prefix.get(skey.substring(0, i));

    if (m == null) return super.get(key);

    return m.get(skey.substring(i + 1));
  }
}
