/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2006 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributors: Philippe Merle, Alessio Pace
 *
 * $Id$
 */

package org.objectweb.fractal.adl.types;

import java.util.Map;

import org.objectweb.fractal.adl.ContextMap;
import org.objectweb.fractal.adl.util.ClassLoaderHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.util.Fractal;

/**
 * A Fractal based implementation of the {@link TypeBuilder} interface. 
 * This implementation uses the Fractal API to create types.
 */

public class FractalTypeBuilder implements TypeBuilder {
  
  // --------------------------------------------------------------------------
  // Implementation of the TypeBuilder interface
  // --------------------------------------------------------------------------
  
  public Object createInterfaceType (
    final String name,
    final String signature,
    final String role,
    final String contingency,
    final String cardinality,
    final Object context) throws Exception
  {
    ClassLoader loader = ClassLoaderHelper.getClassLoader(this, context);

    // TODO : cache already created types ?
    Component bootstrap = null;
    if (context != null) {
      bootstrap = (Component)((Map)context).get("bootstrap");
    }
    if (bootstrap == null) {
      Map ctxt = ContextMap.instance(); // new HashMap();
      ctxt.put("classloader", loader);
      bootstrap = Fractal.getBootstrapComponent(ctxt);
    }
    boolean client = TypeInterface.CLIENT_ROLE.equals(role);
    boolean optional = TypeInterface.OPTIONAL_CONTINGENCY.equals(contingency);
    boolean collection = TypeInterface.COLLECTION_CARDINALITY.equals(cardinality);
    
    return Fractal.getTypeFactory(bootstrap).createFcItfType(
      name, signature, client, optional, collection);
  }

  public Object createComponentType (
    final String name,
    final Object[] interfaceTypes, 
    final Object context) throws Exception 
  {
    ClassLoader loader = ClassLoaderHelper.getClassLoader(this, context);

    Component bootstrap = null;
    if (context != null) {
      bootstrap = (Component)((Map)context).get("bootstrap");
    }
    if (bootstrap == null) {
      Map ctxt = ContextMap.instance(); // new HashMap();
      ctxt.put("classloader", loader);
      bootstrap = Fractal.getBootstrapComponent(ctxt);
    }
    InterfaceType[] types = new InterfaceType[interfaceTypes.length];
    for (int i = 0; i < types.length; ++i) {
      types[i] = (InterfaceType)interfaceTypes[i];
    }
    return Fractal.getTypeFactory(bootstrap).createFcType(types);
  }
}
