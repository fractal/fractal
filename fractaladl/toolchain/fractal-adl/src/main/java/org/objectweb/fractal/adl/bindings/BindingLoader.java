/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 *
 * Contributor: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.bindings;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;

/**
 * A {@link org.objectweb.fractal.adl.Loader} to check {@link Binding} nodes in
 * definitions. This loader checks that the from and to interfaces specified in
 * these nodes exist.
 */
public class BindingLoader extends AbstractLoader {

  // --------------------------------------------------------------------------
  // Implementation of the Loader interface
  // --------------------------------------------------------------------------

  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    final Definition d = clientLoader.load(name, context);
    checkNode(d, context);
    return d;
  }

  // --------------------------------------------------------------------------
  // Checking methods
  // --------------------------------------------------------------------------

  protected void checkNode(final Object node, final Map<Object, Object> context)
      throws ADLException {
    if (node instanceof BindingContainer) {
      final Map<String, Map<String, Interface>> itfMap = new HashMap<String, Map<String, Interface>>();
      if (node instanceof InterfaceContainer) {
        final Map<String, Interface> containerItfs = new HashMap<String, Interface>();
        for (final Interface itf : ((InterfaceContainer) node).getInterfaces()) {
          containerItfs.put(itf.getName(), itf);
        }
        itfMap.put("this", containerItfs);
      }
      if (node instanceof ComponentContainer) {
        for (final Component comp : ((ComponentContainer) node).getComponents()) {
          if (comp instanceof InterfaceContainer) {
            final Map<String, Interface> compItfs = new HashMap<String, Interface>();
            for (final Interface itf : ((InterfaceContainer) comp)
                .getInterfaces()) {
              compItfs.put(itf.getName(), itf);
            }
            itfMap.put(comp.getName(), compItfs);
          }
        }
      }
      for (final Binding binding : ((BindingContainer) node).getBindings()) {
        checkBinding(binding, itfMap, context);
      }
      final Map<String, Binding> fromItfs = new HashMap<String, Binding>();
      for (final Binding binding : ((BindingContainer) node).getBindings()) {
        final Binding previousDefinition = fromItfs.put(binding.getFrom(),
            binding);
        if (previousDefinition != null) {
          throw new ADLException(BindingErrors.DUPLICATED_BINDING, binding,
              binding.getFrom(), previousDefinition);
        }
      }
    }
    if (node instanceof ComponentContainer) {
      for (final Component comp : ((ComponentContainer) node).getComponents()) {
        checkNode(comp, context);
      }
    }
  }

  protected void checkBinding(final Binding binding,
      final Map<String, Map<String, Interface>> itfMap,
      final Map<Object, Object> context) throws ADLException {
    final String from = binding.getFrom();
    final String to = binding.getTo();

    if (from == null) {
      throw new ADLException(BindingErrors.MISSING_FROM, binding);
    }
    if (to == null) {
      throw new ADLException(BindingErrors.MISSING_TO, binding);
    }

    int i = from.indexOf('.');
    if (i < 1 || i == from.length() - 1) {
      throw new ADLException(BindingErrors.INVALID_FROM_SYNTAX, binding, from);
    }
    final String fromCompName = from.substring(0, i);
    final String fromItfName = from.substring(i + 1);

    i = to.indexOf('.');
    if (i < 1 || i == to.length() - 1) {
      throw new ADLException(BindingErrors.INVALID_TO_SYNTAX, binding, to);
    }
    final String toCompName = to.substring(0, i);
    final String toItfName = to.substring(i + 1);

    if (!ignoreBinding(binding, fromCompName, fromItfName, toCompName,
        toItfName)) {

      final Interface fromItf = getInterface(fromCompName, fromItfName,
          binding, itfMap);
      final Interface toItf = getInterface(toCompName, toItfName, binding,
          itfMap);

      if (fromItf != null && toItf != null) {
        checkBinding(binding, fromItf, fromCompName, fromItfName, toItf,
            toCompName, toItfName, context);
      }
    }
  }

  protected void checkBinding(final Binding binding, final Interface fromItf,
      final String fromCompName, final String fromItfName,
      final Interface toItf, final String toCompName, final String toItfName,
      final Map<Object, Object> context) throws ADLException {
  }

  protected Interface getInterface(final String compName, final String itfName,
      final Node sourceNode, final Map<String, Map<String, Interface>> itfMap)
      throws ADLException {
    final Map<String, Interface> compItfs = itfMap.get(compName);
    if (compItfs == null) {
      throw new ADLException(BindingErrors.INVALID_ITF_NO_SUCH_COMPONENT,
          sourceNode, compName);
    }
    final Interface itf = compItfs.get(itfName);
    if (itf == null) {
      throw new ADLException(BindingErrors.INVALID_ITF_NO_SUCH_INTERFACE,
          sourceNode, compName, itfName);
    }

    return itf;
  }

  protected boolean ignoreBinding(final Binding binding,
      final String fromCompName, final String fromItfName,
      final String toCompName, final String toItfName) {
    return fromItfName.equals("component")
        || fromItfName.endsWith("-controller") || toItfName.equals("component")
        || toItfName.endsWith("-controller");
  }
}
