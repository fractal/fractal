
package org.objectweb.fractal.adl.util;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.util.Fractal;

/**
 * @author Alessio Pace
 */
public final class ContentControllerHelper {

  private ContentControllerHelper() {
    // private constructor to prevent instantiation.
  }

  /**
   * Helper methods to retrieve a sub component by name.
   * 
   * @param component
   * @param subComponentName
   * @return
   * @throws NoSuchInterfaceException
   * @throws NoSuchComponentException
   */
  public static Component getSubComponentByName(final Component component,
      final String subComponentName) throws NoSuchInterfaceException,
      NoSuchComponentException {

    if (component == null) {
      throw new IllegalArgumentException("Component parameter is null");
    }

    if (subComponentName == null || subComponentName.length() == 0) {
      throw new IllegalArgumentException(
          "Sub component name can't be null or empty");
    }

    final ContentController contentController = Fractal
        .getContentController(component);

    final Component[] subComponents = contentController.getFcSubComponents();
    for (final Component c : subComponents) {

      final NameController nc = Fractal.getNameController(c);

      if (subComponentName.equals(nc.getFcName())) {
        return c;
      }

    }
    throw new NoSuchComponentException("There is no subcomponent named '"
        + subComponentName + "'");
  }

}
