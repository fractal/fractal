/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributor: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.components;

import java.io.PrintWriter;
import java.util.Map;

import org.objectweb.fractal.api.control.LifeCycleController;

/**
 * A Java based, static implementation of the {@link ComponentBuilder}
 * interface. This implementation produces standard Java code that adds and
 * starts components.
 */

public class StaticJavaComponentBuilder implements ComponentBuilder {

  // --------------------------------------------------------------------------
  // Implementation of the ComponentBuilder interface
  // --------------------------------------------------------------------------

  public void addComponent(final Object superComponent,
      final Object subComponent, final String name, final Object context) {
    // does nothing
  }

  public void startComponent(final Object component, final Object context) {
    final String id = (String) component;
    if (id.startsWith("P")) {
      final PrintWriter pw = (PrintWriter) ((Map<?, ?>) context)
          .get("printwriter");
      pw.print("if (");
      pw.print(id);
      pw.println(" instanceof " + LifeCycleController.class.getName() + ") {");
      pw.print("  ((" + LifeCycleController.class.getName() + ")");
      pw.print(id);
      pw.println(").startFc();");
      pw.println("}");
    }
  }
}
