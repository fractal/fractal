/**
 * 
 */

package org.objectweb.fractal.adl.util;

/**
 * @author Alessio Pace
 */
public class NoSuchComponentException extends Exception {

  private static final long serialVersionUID = 4999493487247851047L;

  public NoSuchComponentException(final String message) {
    super(message);
  }

}
