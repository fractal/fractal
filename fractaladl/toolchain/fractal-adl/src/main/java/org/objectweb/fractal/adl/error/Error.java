/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.error;

/**
 * An error object contains all the informations about an error detected by the
 * tool-chain.
 */
public class Error {

  protected final ErrorTemplate template;
  protected ErrorLocator        locator;
  protected final Throwable     cause;
  protected final String        message;

  /**
   * Creates an error from the given template and message arguments.
   * 
   * @param template the error templates.
   * @param args the arguments for the formated message (see
   *            {@link ErrorTemplate#getFormatedMessage(Object...)}).
   */
  public Error(final ErrorTemplate template, final Object... args) {
    this(template, null, null, args);
  }

  /**
   * Creates an error from the given template, location and message arguments.
   * 
   * @param template the error templates.
   * @param locator the error location. May be <code>null</code>.
   * @param args the arguments for the formated message (see
   *            {@link ErrorTemplate#getFormatedMessage(Object...)}).
   */
  public Error(final ErrorTemplate template, final ErrorLocator locator,
      final Object... args) {
    this(template, locator, null, args);
  }

  /**
   * Creates an error from the given template, cause and message arguments.
   * 
   * @param template the error templates.
   * @param cause the cause of this error. May be <code>null</code>.
   * @param args the arguments for the formated message (see
   *            {@link ErrorTemplate#getFormatedMessage(Object...)}).
   */
  public Error(final ErrorTemplate template, final Throwable cause,
      final Object... args) {
    this(template, null, cause, args);
  }

  /**
   * Creates an error from the given template, location, cause and message
   * arguments.
   * 
   * @param template the error templates.
   * @param locator the error location. May be <code>null</code>.
   * @param cause the cause of this error. May be <code>null</code>.
   * @param args the arguments for the formated message (see
   *            {@link ErrorTemplate#getFormatedMessage(Object...)}).
   */
  public Error(final ErrorTemplate template, final ErrorLocator locator,
      final Throwable cause, final Object... args) {
    if (template == null)
      throw new IllegalArgumentException("Error can't be null");
    if (args == null) throw new IllegalArgumentException("args can't be null");
    this.template = template;
    this.locator = locator;
    this.cause = cause;
    this.message = template.getFormatedMessage(args);
  }

  /**
   * Returns the template of this error.
   * 
   * @return the template of this error.
   */
  public ErrorTemplate getTemplate() {
    return template;
  }

  /**
   * Returns the locator of this error. May be <code>null</code>.
   * 
   * @return the locator of this error.
   */
  public ErrorLocator getLocator() {
    return locator;
  }

  /**
   * Sets the locator of this error. May be <code>null</code>.
   * 
   * @param locator the locator of this error.
   */
  public void setLocator(final ErrorLocator locator) {
    this.locator = locator;
  }

  /**
   * Returns the cause of this error. May be <code>null</code>.
   * 
   * @return the cause of this error.
   */
  public Throwable getCause() {
    return cause;
  }

  /**
   * Returns the formated message of this error.
   * 
   * @return the formated message of this error.
   * @see ErrorTemplate#getFormatedMessage(Object...)
   */
  public String getMessage() {
    return message;
  }

  @Override
  public String toString() {
    String fullMessage = "";
    // append location info if any
    if (locator != null) fullMessage += locator.getLocation() + ": ";

    // append error Ids
    fullMessage += String.format("[%3s-%03d] ", template.getGroupId(), template
        .getErrorId());

    // append message
    fullMessage += message;

    // append cause if any
    if (cause != null) {
      fullMessage += " Caused by: ";
      if (cause.getMessage() == null)
        fullMessage += cause.getClass().getName();
      else
        fullMessage += cause.getMessage();
    }

    return fullMessage;
  }
}
