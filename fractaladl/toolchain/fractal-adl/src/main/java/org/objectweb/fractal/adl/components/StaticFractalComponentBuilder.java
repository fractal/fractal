/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributor: Philippe Merle
 *
 * $Id$
 */

package org.objectweb.fractal.adl.components;

import java.io.PrintWriter;
import java.util.Map;

/**
 * A Fractal based, static implementation of the {@link ComponentBuilder}
 * interface. This implementation produces Java code that uses the Fractal API
 * to add and start components.
 */

public class StaticFractalComponentBuilder implements ComponentBuilder {

  // --------------------------------------------------------------------------
  // Implementation of the ComponentBuilder interface
  // --------------------------------------------------------------------------

  public void addComponent(final Object superComponent,
      final Object subComponent, final String name, final Object context)
      throws Exception {
    final PrintWriter pw = (PrintWriter) ((Map) context).get("printwriter");
    pw.print("Fractal.getContentController(");
    pw.print(superComponent);
    pw.print(").addFcSubComponent(");
    pw.print(subComponent);
    pw.println(");");

    // Next lines are commented as setFcName() is already called by
    // implementations.StaticFractalImplementationBuilder.createComponent(...)
    //
    // pw.print("try { Fractal.getNameController(");
    // pw.print(subComponent);
    // pw.print(").setFcName(\"");
    // pw.print(name);
    // pw.println("\"); } catch (NoSuchInterfaceException ignored) { }");
  }

  public void startComponent(final Object component, final Object context)
      throws Exception {

    // Implement the same policy as for the dynamic FractalComponentBuilder
    // see http://mail.ow2.org/wws/arc/fractal/2008-10/msg00002.html

// PrintWriter pw = (PrintWriter)((Map)context).get("printwriter");
// pw.print("Fractal.getLifeCycleController(");
// pw.print(component);
// pw.println(").startFc();");
  }
}
