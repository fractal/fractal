/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.implementations;

import static org.objectweb.fractal.adl.error.ErrorTemplateValidator.validErrorTemplate;

import org.objectweb.fractal.adl.error.ErrorTemplate;

/** {@link ErrorTemplate} group for the implementations package. */
public enum ImplementationErrors implements ErrorTemplate {

  /** */
  IMPLEMENTATION_MISSING("Implementation class name missing"),

  /** */
  IMPLEMENTATION_NOT_FOUND("Can't find implementation \"%s\"", "impl"),

  /** */
  CLASS_DOES_NOT_IMPLEMENT_INTERFACE(
      "The implementation class \"%s\" does not implement the \"%s\" interface",
      "implclass", "itf"),

  /** */
  CLASS_DOES_NOT_IMPLEMENT_ATTR_CTRL(
      "The implementation class \"%s\" does not implement the \"%s\" attribute controller interface",
      "implclass", "itf"),

  /** */
  MISSING_CONTROLLER_DESC("Controller descriptor missing");

  /** The groupId of ErrorTemplates defined in this enumeration. */
  public static final String GROUP_ID = "IMP";

  private int                id;
  private String             format;

  private ImplementationErrors(final String format, final Object... args) {
    this.id = ordinal();
    this.format = format;

    assert validErrorTemplate(this, args);
  }

  public int getErrorId() {
    return id;
  }

  public String getGroupId() {
    return GROUP_ID;
  }

  public String getFormatedMessage(final Object... args) {
    return String.format(format, args);
  }

  public String getFormat() {
    return format;
  }
}
