/***
 * Fractal ADL Parser
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.types;

import org.objectweb.fractal.adl.interfaces.Interface;

/**
 * Utility class to test {@link Interface} node.
 */
public final class TypeInterfaceUtil {
  private TypeInterfaceUtil() {
  }

  /**
   * Returns <code>true</code> if the given interface is a server interface.
   * More precisely, it returns <code>true</code> if the interface
   * {@link TypeInterface#getRole() role} is
   * {@link TypeInterface#SERVER_ROLE "server"}.
   * 
   * @param itf an interface.
   * @return <code>true</code> if the given interface is a server interface.
   */
  public static boolean isServer(final Interface itf) {
    if (itf instanceof TypeInterface) {
      TypeInterface tItf = (TypeInterface) itf;
      return TypeInterface.SERVER_ROLE.equals(tItf.getRole());
    }
    return false;
  }

  /**
   * Returns <code>true</code> if the given interface is a client interface.
   * More precisely, it returns <code>true</code> if the interface
   * {@link TypeInterface#getRole() role} is
   * {@link TypeInterface#CLIENT_ROLE "client"}.
   * 
   * @param itf an interface.
   * @return <code>true</code> if the given interface is a client interface.
   */
  public static boolean isClient(final Interface itf) {
    if (itf instanceof TypeInterface) {
      TypeInterface tItf = (TypeInterface) itf;
      return TypeInterface.CLIENT_ROLE.equals(tItf.getRole());
    }
    return false;
  }

  /**
   * Returns <code>true</code> if the given interface is an optional
   * interface. More precisely, it returns <code>true</code> if the interface
   * {@link TypeInterface#getContingency() contingency} is
   * {@link TypeInterface#OPTIONAL_CONTINGENCY "optional"}.
   * 
   * @param itf an interface.
   * @return <code>true</code> if the given interface is an optional
   *         interface.
   */
  public static boolean isOptional(final Interface itf) {
    if (itf instanceof TypeInterface) {
      TypeInterface tItf = (TypeInterface) itf;
      return TypeInterface.OPTIONAL_CONTINGENCY.equals(tItf.getContingency());
    }
    return false;
  }

  /**
   * Returns <code>true</code> if the given interface is a mandatory
   * interface. More precisely, it returns <code>true</code> if the interface
   * {@link TypeInterface#getContingency() contingency} is <code>null</code>
   * or {@link TypeInterface#MANDATORY_CONTINGENCY "mandatory"}.
   * 
   * @param itf an interface.
   * @return <code>true</code> if the given interface is a mandatory
   *         interface.
   */
  public static boolean isMandatory(final Interface itf) {
    if (itf instanceof TypeInterface) {
      TypeInterface tItf = (TypeInterface) itf;
      return tItf.getContingency() == null
          || TypeInterface.MANDATORY_CONTINGENCY.equals(tItf.getContingency());
    }
    return false;
  }

  /**
   * Returns <code>true</code> if the given interface is a singleton
   * interface. More precisely, it returns <code>true</code> if the interface
   * {@link TypeInterface#getCardinality() cardinality} is <code>null</code>
   * or {@link TypeInterface#SINGLETON_CARDINALITY "singleton"}.
   * 
   * @param itf an interface.
   * @return <code>true</code> if the given interface is a singleton
   *         interface.
   */
  public static boolean isSingleton(final Interface itf) {
    if (itf instanceof TypeInterface) {
      TypeInterface tItf = (TypeInterface) itf;
      return tItf.getCardinality() == null
          || TypeInterface.SINGLETON_CARDINALITY.equals(tItf.getCardinality());
    }
    return false;
  }

  /**
   * Returns <code>true</code> if the given interface is a collection
   * interface. More precisely, it returns <code>true</code> if the interface
   * {@link TypeInterface#getCardinality() cardinality} is
   * {@link TypeInterface#SINGLETON_CARDINALITY "collection"}.
   * 
   * @param itf an interface.
   * @return <code>true</code> if the given interface is a collection
   *         interface.
   */
  public static boolean isCollection(final Interface itf) {
    if (itf instanceof TypeInterface) {
      TypeInterface tItf = (TypeInterface) itf;
      return TypeInterface.COLLECTION_CARDINALITY.equals(tItf.getCardinality());
    }
    return false;
  }
}
