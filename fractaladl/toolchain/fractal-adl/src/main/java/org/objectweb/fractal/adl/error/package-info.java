/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

//package org.objectweb.fractal.adl.error;
/**
 * Provides classes and interfaces for error handling in Fractal ADL. <br>
 * <br>
 * This package provides three main notions:
 * <ul>
 * <li><a href="#errorTemplateDesc">ErrorTemplate</a> defines an error that
 * can be reported by the tool-set.</li>
 * <li><a href="#errorLocatorDesc">ErrorLocator</a> allows to locate an error
 * giving file path and line and column informations.</li>
 * <li><a href="#errorDesc">Error</a> objects carry all informations
 * concerning a given error detected in an input file. In particular, it
 * contains an {@link org.objectweb.fractal.adl.error.ErrorTemplate} and an
 * {@link org.objectweb.fractal.adl.error.ErrorLocator}.
 * </ul>
 * When an error is detected (typically by a loader component) it is thrown in
 * an {@link org.objectweb.fractal.adl.ADLException} if the error is related to
 * an error in an input file or in a
 * {@link org.objectweb.fractal.adl.CompilerError} if the error is an internal
 * Fractal ADL error. <br>
 * <br>
 * <a name="errorTemplateDesc"/>
 * <h3>ErrorTemplate description</h3>
 * Each Fractal ADL module should provide its own set of
 * {@link org.objectweb.fractal.adl.error.ErrorTemplate} with its own
 * {@link org.objectweb.fractal.adl.error.ErrorTemplate#getGroupId() groupId}.
 * This set should be defined in an enumeration that may look like this:
 * 
 * <pre>
 * public enum MyErrorGroup implements ErrorTemplate {
 * 
 *   ERROR_1(1, &quot;Invalid interface name: %s&quot;), 
 *   ERROR_2(2, &quot;Invalid interface cardinality: %d&quot;);
 * 
 *   public static final String GROUP_ID = &quot;GID&quot;;
 * 
 *   private int    id;
 *   private String format;
 * 
 *   private MyErrorGroup(String format) {
 *     this.id = ordinal();
 *     this.format = format;
 * 
 *     assert ErrorTemplateValidator.validErrorTemplate(this);
 *   }
 * 
 *   public int getErrorId() {
 *     return id;
 *   }
 * 
 *   public String getGroupId() {
 *     return GROUP_ID;
 *   }
 * 
 *   public String getFormatedMessage(Object... args) {
 *     return String.format(format, args);
 *   }
 * 
 *   public String getFormat() {
 *     return format;
 *   }
 * }
 * </pre>
 * 
 * See {@link org.objectweb.fractal.adl.components.ComponentErrors} for a
 * concrete example of such enumeration class. <br>
 * This package provides a set of generic error template in the
 * {@link org.objectweb.fractal.adl.error.GenericErrors} enumeration. <br>
 * <br>
 * <a name="errorLocatorDesc"/>
 * <h3>ErrorLocator description</h3>
 * An error locator is used to locate an error in input files. This package
 * provides three different implementations of this interface:
 * <ul>
 * <li>{@link org.objectweb.fractal.adl.error.BasicErrorLocator} provides a
 * basic implementation of this interface.</li>
 * <li>{@link org.objectweb.fractal.adl.error.NodeErrorLocator} allows to
 * locate an error using the
 * {@link org.objectweb.fractal.adl.Node#astGetSource node source} info.</li>
 * <li>{@link org.objectweb.fractal.adl.error.ChainedErrorLocator} allows to
 * locate an error using a list of error locator. This is useful when an error
 * has been detected in an ADL that is referenced by another ADL.</li>
 * </ul>
 * <br>
 * <a name="errorDesc"/>
 * <h3>Error description</h3>
 * An {@link org.objectweb.fractal.adl.error.Error} object is an instantiation
 * of an error pattern with a locator and optionally a cause exception. Most of
 * the time, Error objects are not created directly but are created using the
 * constructors of {@link org.objectweb.fractal.adl.ADLException} or
 * {@link org.objectweb.fractal.adl.CompilerError}.<br>
 * <br>
 * <h3>Porting Fractal ADL extensions to the new Error reporting system</h3>
 * To reduce impact on existing Fractal ADL extensions, previous
 * {@link org.objectweb.fractal.adl.ADLException} constructors and sub classes
 * has not been removed but have been deprecated.<br>
 */

package org.objectweb.fractal.adl.error;

