/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 *
 * Contributor(s): Philippe Merle
 *                 Matthieu Leclercq (externalize interface loading)
 */

package org.objectweb.fractal.adl.types;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.ChainedErrorLocator;
import org.objectweb.fractal.adl.interfaces.IDLLoader;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;

/**
 * A {@link org.objectweb.fractal.adl.Loader} to check {@link TypeInterface}
 * nodes in definitions. This loader checks that the Java interfaces specified
 * in these nodes exist.
 */

public class TypeLoader extends AbstractLoader {

  // ---------------------------------------------------------------------------
  // Client interfaces
  // ---------------------------------------------------------------------------

  /** The name of the {@link #interfaceCodeLoaderItf} client interface. */
  public final static String INTERFACE_LOADER_CLIENT_ITF_NAME = "interface-loader";

  /** The client interface used to load interface signature. */
  public IDLLoader           interfaceCodeLoaderItf;

  // ---------------------------------------------------------------------------
  // Implementation of the Loader interface
  // ---------------------------------------------------------------------------

  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    final Definition d = clientLoader.load(name, context);
    checkNode(d, context);
    return d;
  }

  // ---------------------------------------------------------------------------
  // Checking methods
  // ---------------------------------------------------------------------------

  private void checkNode(final Object node, final Map<Object, Object> context)
      throws ADLException {
    if (node instanceof InterfaceContainer) {
      checkInterfaceContainer((InterfaceContainer) node, context);
    }
    if (node instanceof ComponentContainer) {
      for (final Component comp : ((ComponentContainer) node).getComponents()) {
        checkNode(comp, context);
      }
    }
  }

  protected void checkInterfaceContainer(final InterfaceContainer container,
      final Map<Object, Object> context) throws ADLException {
    for (final Interface itf : container.getInterfaces()) {
      if (itf instanceof TypeInterface) {
        final String signature = ((TypeInterface) itf).getSignature();
        if (signature == null) {
          throw new ADLException(TypeErrors.SIGNATURE_MISSING, itf);
        } else {
          try {
            interfaceCodeLoaderItf.loadInterface(signature, context);
          } catch (final ADLException e) {
            ChainedErrorLocator.chainLocator(e, itf);
            throw e;
          }
        }
        final String role = ((TypeInterface) itf).getRole();
        if (role == null) {
          throw new ADLException(TypeErrors.ROLE_MISSING, itf);
        } else {
          if (!role.equals(TypeInterface.CLIENT_ROLE)
              && !role.equals(TypeInterface.SERVER_ROLE)) {
            throw new ADLException(TypeErrors.INVALID_ROLE, itf, role);
          }
        }
        final String contingency = ((TypeInterface) itf).getContingency();
        if (contingency != null) {
          if (!contingency.equals(TypeInterface.MANDATORY_CONTINGENCY)
              && !contingency.equals(TypeInterface.OPTIONAL_CONTINGENCY)) {
            throw new ADLException(TypeErrors.INVALID_CONTINGENCY, itf,
                contingency);
          }
        }
        final String cardinality = ((TypeInterface) itf).getCardinality();
        if (cardinality != null) {
          if (!cardinality.equals(TypeInterface.SINGLETON_CARDINALITY)
              && !cardinality.equals(TypeInterface.COLLECTION_CARDINALITY)) {
            throw new ADLException(TypeErrors.INVALID_CARDINALITY, itf,
                cardinality);
          }
        }
      }
    }
  }

  // ---------------------------------------------------------------------------
  // Overridden methods of the BindingController interface
  // ---------------------------------------------------------------------------

  @Override
  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (INTERFACE_LOADER_CLIENT_ITF_NAME.equals(s)) {
      this.interfaceCodeLoaderItf = (IDLLoader) o;
    } else {
      super.bindFc(s, o);
    }
  }

  @Override
  public String[] listFc() {
    final String[] superList = super.listFc();
    final String[] list = new String[superList.length + 1];
    list[0] = INTERFACE_LOADER_CLIENT_ITF_NAME;
    System.arraycopy(superList, 0, list, 1, superList.length);
    return list;
  }

  @Override
  public Object lookupFc(final String s) throws NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (INTERFACE_LOADER_CLIENT_ITF_NAME.equals(s)) {
      return this.interfaceCodeLoaderItf;
    } else {
      return super.lookupFc(s);
    }
  }

  @Override
  public void unbindFc(final String s) throws IllegalBindingException,
      NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (INTERFACE_LOADER_CLIENT_ITF_NAME.equals(s)) {
      this.interfaceCodeLoaderItf = null;
    } else {
      super.unbindFc(s);
    }
  }

}
