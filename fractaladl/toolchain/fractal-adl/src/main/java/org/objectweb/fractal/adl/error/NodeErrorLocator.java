/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.error;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.objectweb.fractal.adl.Node;

/**
 * An error locator for locating error in an AST.
 */
public class NodeErrorLocator implements ErrorLocator {

  protected static final Pattern FULL_LOC                 = Pattern
                                                              .compile("(.*):\\[(\\d+),(\\d+)\\]-\\[(\\d+),(\\d+)\\]");
  protected static final int     FULL_LOC_PATH            = 1;
  protected static final int     FULL_LOC_BEGIN_LINE      = 2;
  protected static final int     FULL_LOC_BEGIN_COLUMN    = 3;
  protected static final int     FULL_LOC_END_LINE        = 4;
  protected static final int     FULL_LOC_END_COLUMN      = 5;

  protected static final Pattern LCBEGIN_LOC              = Pattern
                                                              .compile("(.*):(\\d+)-(\\d+)");
  protected static final int     LCBEGIN_LOC_PATH         = 1;
  protected static final int     LCBEGIN_LOC_BEGIN_LINE   = 2;
  protected static final int     LCBEGIN_LOC_BEGIN_COLUMN = 3;

  protected static final Pattern LINE_LOC                 = Pattern
                                                              .compile("(.*):(\\d+)");
  protected static final int     LINE_LOC_PATH            = 1;
  protected static final int     LINE_LOC_BEGIN_LINE      = 2;

  private final Node             node;
  private final int              beginLine;
  private final int              endLine;
  private final int              beginColumn;
  private final int              endColumn;
  private final String           inputFilePath;

  // ---------------------------------------------------------------------------
  // Static methods
  // ---------------------------------------------------------------------------

  /**
   * Returns a {@link Node#astGetSource() source} string that contains full
   * location informations.
   * 
   * @param inputFilePath the path of the input file
   * @param beginLine the begin line
   * @param endLine the end line
   * @param beginColumn the begin column
   * @param endColumn the end column
   * @return a {@link Node#astGetSource() source} string that contains full
   *         location informations.
   */
  public static final String fullLocation(final String inputFilePath,
      final int beginLine, final int endLine, final int beginColumn,
      final int endColumn) {
    return inputFilePath + ":[" + beginLine + "," + beginColumn + "]-["
        + endLine + "," + endColumn + "]";
  }

  /**
   * Returns a {@link Node#astGetSource() source} string that contains begin
   * line and begin column location informations.
   * 
   * @param inputFilePath the path of the input file
   * @param beginLine the begin line
   * @param beginColumn the begin column
   * @return a {@link Node#astGetSource() source} string that contains full
   *         location informations.
   */
  public static final String lineColumnLocation(final String inputFilePath,
      final int beginLine, final int beginColumn) {
    return inputFilePath + ":" + beginLine + "-" + beginColumn;
  }

  /**
   * Returns a {@link Node#astGetSource() source} string that contains begin
   * line and begin column location informations.
   * 
   * @param inputFilePath the path of the input file
   * @param beginLine the begin line
   * @return a {@link Node#astGetSource() source} string that contains full
   *         location informations.
   */
  public static final String lineLocation(final String inputFilePath,
      final int beginLine) {
    return inputFilePath + ":" + beginLine;
  }

  // ---------------------------------------------------------------------------
  // Constructor
  // ---------------------------------------------------------------------------

  /**
   * Creates a {@link NodeErrorLocator} for the given node.
   * 
   * @param node an AST node.
   */
  public NodeErrorLocator(final Node node) {
    if (node == null) throw new IllegalArgumentException("node can't be null");
    this.node = node;
    final String source = node.astGetSource();
    if (source == null) {
      inputFilePath = null;
      beginLine = -1;
      beginColumn = -1;
      endLine = -1;
      endColumn = -1;
      return;
    }

    Matcher matcher = FULL_LOC.matcher(source);
    if (matcher.matches()) {
      inputFilePath = matcher.group(FULL_LOC_PATH);
      beginLine = Integer.parseInt(matcher.group(FULL_LOC_BEGIN_LINE));
      beginColumn = Integer.parseInt(matcher.group(FULL_LOC_BEGIN_COLUMN));
      endLine = Integer.parseInt(matcher.group(FULL_LOC_END_LINE));
      endColumn = Integer.parseInt(matcher.group(FULL_LOC_END_COLUMN));
      return;
    }

    matcher = LCBEGIN_LOC.matcher(source);
    if (matcher.matches()) {
      inputFilePath = matcher.group(LCBEGIN_LOC_PATH);
      beginLine = Integer.parseInt(matcher.group(LCBEGIN_LOC_BEGIN_LINE));
      beginColumn = Integer.parseInt(matcher.group(LCBEGIN_LOC_BEGIN_COLUMN));
      endLine = -1;
      endColumn = -1;
      return;
    }

    matcher = LINE_LOC.matcher(source);
    if (matcher.matches()) {
      inputFilePath = matcher.group(LINE_LOC_PATH);
      beginLine = Integer.parseInt(matcher.group(LINE_LOC_BEGIN_LINE));
      beginColumn = -1;
      endLine = -1;
      endColumn = -1;
      return;
    }

    // No pattern matches
    inputFilePath = source;
    beginLine = -1;
    beginColumn = -1;
    endLine = -1;
    endColumn = -1;
  }

  /**
   * Returns the node located by this locator.
   * 
   * @return the node located by this locator.
   */
  public Node getNode() {
    return node;
  }

  // ---------------------------------------------------------------------------
  // Implementation of the ErrorLocator interface
  // ---------------------------------------------------------------------------

  public int getBeginColumn() {
    return beginColumn;
  }

  public int getBeginLine() {
    return beginLine;
  }

  public int getEndColumn() {
    return endColumn;
  }

  public int getEndLine() {
    return endLine;
  }

  public String getInputFilePath() {
    return inputFilePath;
  }

  public String getLocation() {
    if (inputFilePath == null) return "unknown";
    if (beginLine != -1) {
      if (beginColumn == -1) {
        return lineLocation(inputFilePath, beginLine);
      } else if (endLine == -1) {
        return lineColumnLocation(inputFilePath, beginLine, beginColumn);
      } else {
        return fullLocation(inputFilePath, beginLine, endLine, beginColumn,
            endColumn);
      }
    }
    return inputFilePath;
  }

  // ---------------------------------------------------------------------------
  // Overridden methods
  // ---------------------------------------------------------------------------

  @Override
  public String toString() {
    return getLocation();
  }
}
