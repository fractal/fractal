/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.attributes;

import static org.objectweb.fractal.adl.error.ErrorTemplateValidator.validErrorTemplate;

import org.objectweb.fractal.adl.error.ErrorTemplate;

/** {@link ErrorTemplate} group for the attributes package. */
public enum AttributeErrors implements ErrorTemplate {

  /** */
  SIGNATURE_MISSING("Attribute controller signature missing."),

  /** */
  NAME_MISSING("Attribute name missing."),

  /** */
  VALUE_MISSING("Attribute value missing."),

  /** */
  NO_SUCH_ATTRIBUTE("Undefined attribute \"%s\".", "attrname"),

  /** */
  UNSUPPORTED_ATTRIBUTE_TYPE(
      "Unsupported attribute type \"%s\" in initialization of attribute \"%s\".",
      "attrtype", "attrname"),

  /** */
  UNEXPECTED_ATTRIBUTE_TYPE(
      "Unexpected attribute type \"%s\" in initialization of attribute \"%s\".",
      "attrtype", "attrname"),

  /** */
  INVALID_INTEGER_VALUE(
      "Bad integer value \"%s\" in initialization of attribute \"%s\".",
      "value", "name"),

  /** */
  INVALID_LONG_VALUE(
      "Bad long value \"%s\" in initialization of attribute \"%s\".", "value",
      "name"),

  /** */
  INVALID_FLOAT_VALUE(
      "Bad float value \"%s\" in initialization of attribute \"%s\".", "value",
      "name"),

  /** */
  INVALID_DOUBLE_VALUE(
      "Bad double value \"%s\" in initialization of attribute \"%s\".",
      "value", "name"),

  /** */
  INVALID_BYTE_VALUE(
      "Bad byte value \"%s\" in initialization of attribute \"%s\".", "value",
      "name"),

  /** */
  INVALID_CHAR_VALUE(
      "Bad char value \"%s\" in initialization of attribute \"%s\".", "value",
      "name"),

  /** */
  INVALID_SHORT_VALUE(
      "Bad short value \"%s\" in initialization of attribute \"%s\".", "value",
      "name"),

  /** */
  INVALID_BOOLEAN_VALUE(
      "Bad boolean value \"%s\" in initialization of attribute \"%s\".",
      "value", "name");

  /** The groupId of ErrorTemplates defined in this enumeration. */
  public static final String GROUP_ID = "ATT";

  private int                id;
  private String             format;

  private AttributeErrors(final String format, final Object... args) {
    this.id = ordinal();
    this.format = format;

    assert validErrorTemplate(this, args);
  }

  public int getErrorId() {
    return id;
  }

  public String getGroupId() {
    return GROUP_ID;
  }

  public String getFormatedMessage(final Object... args) {
    return String.format(format, args);
  }

  public String getFormat() {
    return format;
  }
}
