/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.attributes;

import java.lang.reflect.Method;

/**
 * A Java based implementation of the {@link AttributeBuilder} interface. 
 */

public class JavaAttributeBuilder implements AttributeBuilder {

  // --------------------------------------------------------------------------
  // Implementation of the AttributeBuilder interface
  // --------------------------------------------------------------------------
  
  public void setAttribute (
    final Object component, 
    final String attributeController, 
    final String name, 
    final String value,
    final Object context) throws Exception 
  {    
    Class c = component.getClass();

    String attrName = Character.toUpperCase(name.charAt(0)) + name.substring(1);
    
    // sets the attribute's value
    String getterName = "get" + attrName;
    String setterName = "set" + attrName;
    Method getter = c.getMethod(getterName, new Class[0]);
    Method setter = c.getMethod(setterName, new Class[] {
        getter.getReturnType()
    });
    Class attrType = getter.getReturnType();
    Object attrValue;
    if (attrType.equals(String.class)) {
      attrValue = value;
    } else if (attrType.isPrimitive()) {
      if (attrType.equals(Integer.TYPE)) {
        attrValue = Integer.valueOf(value);
      } else if (attrType.equals(Long.TYPE)) {
        attrValue = Long.valueOf(value);
      } else if (attrType.equals(Float.TYPE)) {
        attrValue = Float.valueOf(value);
      } else if (attrType.equals(Double.TYPE)) {
        attrValue = Double.valueOf(value);
      } else if (attrType.equals(Byte.TYPE)) {
        attrValue = Byte.valueOf(value);
      } else if (attrType.equals(Character.TYPE)) {
        if (value.length() != 1) {
          throw new Exception("Bad char value: " + value);
        }
        attrValue = new Character(value.charAt(0));
      } else if (attrType.equals(Short.TYPE)) {
        attrValue = Short.valueOf(value);
      } else if (attrType.equals(Boolean.TYPE)) {
        if (!value.equals("true") &&
            !value.equals("false"))
        {
          throw new Exception("Bad boolean value: " + value);
        }
        attrValue = new Boolean(value.equals("true"));
      } else {
        throw new Exception("Unexpected case");
      }
    } else {
      throw new Exception("Unsupported attribute type: " + attrType);
    }
    setter.invoke(component, new Object[]{attrValue});
  }
}
