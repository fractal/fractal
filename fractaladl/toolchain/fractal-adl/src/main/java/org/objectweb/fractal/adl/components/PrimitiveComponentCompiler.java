/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.components;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.task.core.Task;
import org.objectweb.fractal.task.core.TaskMap;
import org.objectweb.fractal.task.deployment.api.InstanceProviderTask;
import org.objectweb.fractal.task.deployment.lib.AbstractInitializationTask;
import org.objectweb.fractal.task.deployment.lib.AbstractRequireInstanceProviderTask;

/**
 * A {@link PrimitiveCompiler} to compile {@link Component} nodes in
 * definitions.
 */

public class PrimitiveComponentCompiler implements BindingController,
		PrimitiveCompiler {

	/**
	 * Name of the mandatory interface bound to the {@link ComponentBuilder}
	 * used by this compiler.
	 */

	public final static String BUILDER_BINDING = "builder";

	/**
	 * The {@link ComponentBuilder} used by this compiler.
	 */

	public ComponentBuilder builder;

	// --------------------------------------------------------------------------
	// Implementation of the BindingController interface
	// --------------------------------------------------------------------------

	public String[] listFc() {
		return new String[] { BUILDER_BINDING };
	}

	public Object lookupFc(final String itf) {
		if (itf.equals(BUILDER_BINDING)) {
			return builder;
		}
		return null;
	}

	public void bindFc(final String itf, final Object value) {
		if (itf.equals(BUILDER_BINDING)) {
			builder = (ComponentBuilder) value;
		}
	}

	public void unbindFc(final String itf) {
		if (itf.equals(BUILDER_BINDING)) {
			builder = null;
		}
	}

	// --------------------------------------------------------------------------
	// Implementation of the PrimitiveCompiler interface
	// --------------------------------------------------------------------------

	public void compile(final List path, final ComponentContainer container,
			final TaskMap tasks, final Map context) throws ADLException {
		
		TaskMap.TaskHole createTaskHole = tasks
				.getTaskHole("create", container);

		StartTask startTask = new StartTask(builder);

		startTask.setInstanceProviderTask(createTaskHole);
		tasks.addTask("start", container, startTask);

		startTask.addDependency(createTaskHole, Task.PREVIOUS_TASK_ROLE,
				context);

		Component[] comps = container.getComponents();
		for (int i = 0; i < comps.length; i++) {
			
			TaskMap.TaskHole createSubComponentTaskHole = tasks.getTaskHole(
					"create", comps[i]);

			ComponentPair pair = new ComponentPair(container, comps[i]);
			try {
				// the task may already exist, in case of a shared component
				tasks.getTask("add", pair);
			} catch (NoSuchElementException e) {
				AddTask addTask = new AddTask(builder, comps[i].getName());
				
				addTask.setInstanceProviderTask(createTaskHole);
				addTask.setSubInstanceProviderTask(createSubComponentTaskHole);
				
				TaskMap.TaskHole addTaskHole = tasks.addTask("add", pair, addTask);
				
				addTask.addDependency(createTaskHole, Task.PREVIOUS_TASK_ROLE, context);
				addTask.addDependency(createSubComponentTaskHole, Task.PREVIOUS_TASK_ROLE, context);
				
				startTask.addDependency(addTaskHole, Task.PREVIOUS_TASK_ROLE, context);
			}
		
		}
	}

	// --------------------------------------------------------------------------
	// Inner classes
	// --------------------------------------------------------------------------

	/**
	 * 
	 */
	static class AddTask extends AbstractRequireInstanceProviderTask {

		private ComponentBuilder builder;

		private TaskMap.TaskHole subInstanceProviderTask;

		private String name;

		public AddTask(final ComponentBuilder builder, final String name) {
			this.builder = builder;
			this.name = name;
		}

		public InstanceProviderTask getSubInstanceProviderTask() {
			return (subInstanceProviderTask) == null ? null
					: (InstanceProviderTask) subInstanceProviderTask.getTask();
		}

		public void setSubInstanceProviderTask(final TaskMap.TaskHole task) {
			if (subInstanceProviderTask != null) {
				removePreviousTask(subInstanceProviderTask);
			}
			subInstanceProviderTask = task;
			if (subInstanceProviderTask != null) {
				addPreviousTask(subInstanceProviderTask);
			}
		}

		public void execute(final Map context) throws Exception {
			Object parent = getInstanceProviderTask().getInstance();
			Object child = getSubInstanceProviderTask().getInstance();
			builder.addComponent(parent, child, name, context);
		}

		public Object getResult() {
			return null;
		}

		public void setResult(Object result) {
		}

		public String toString() {
			return "T" + System.identityHashCode(this) + "[AddTask(" + name
					+ ")]";
		}
	}

	/**
	 * 
	 * 
	 */
	static class StartTask extends AbstractInitializationTask {

		private ComponentBuilder builder;

		public StartTask(final ComponentBuilder builder) {
			this.builder = builder;
		}

		public void execute(final Map context) throws Exception {
			builder.startComponent(getInstanceProviderTask().getInstance(),
					context);
		}

		public String toString() {
			return "T" + System.identityHashCode(this) + "[StartTask()]";
		}
	}
}
