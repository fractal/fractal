/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.components;

public class ComponentPair {
  
  public final ComponentContainer container;
  
  public final Component comp;
  
  public ComponentPair (
    final ComponentContainer container, 
    final Component comp) 
  {
    this.container = container;
    this.comp = comp;
  }
  
  public int hashCode () {
    return container.hashCode() + comp.hashCode();
  }
  
  public boolean equals (final Object o) {
    if (o instanceof ComponentPair) {
      ComponentPair other = (ComponentPair)o;
      return other.container == container && other.comp == comp;
    }
    return false;      
  }
}
