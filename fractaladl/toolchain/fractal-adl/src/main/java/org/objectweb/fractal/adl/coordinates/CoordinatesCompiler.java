/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.coordinates;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.components.ComponentPair;
import org.objectweb.fractal.adl.components.PrimitiveCompiler;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.task.core.Task;
import org.objectweb.fractal.task.core.TaskMap;
import org.objectweb.fractal.task.deployment.api.InstanceProviderTask;
import org.objectweb.fractal.task.deployment.lib.AbstractConfigurationTask;


/**
 * A {@link PrimitiveCompiler} to compile {@link Coordinates} nodes in 
 * definitions.  
 */

public class CoordinatesCompiler implements PrimitiveCompiler, BindingController {
  
  /**
   * Name of the mandatory interface bound to the {@link CoordinatesBuilder} 
   * used by this compiler.
   */
  
  public final static String BUILDER_BINDING = "builder";
  /**
   * The {@link CoordinatesBuilder} used by this compiler.
   */
  
  private CoordinatesBuilder builder;
  
  // --------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // --------------------------------------------------------------------------
  
  public String[] listFc () {
    return new String[] { BUILDER_BINDING };
  }

  public Object lookupFc (final String clientItfName) {
    if (BUILDER_BINDING.equals(clientItfName)) {
      return builder;
    }
    return null;
  }

  public void bindFc (
      final String clientItfName,
      final Object serverItf)
  {
    if (BUILDER_BINDING.equals(clientItfName)) {
      builder = (CoordinatesBuilder)serverItf;
    }
  }

  public void unbindFc (final String clientItfName) {
    if (BUILDER_BINDING.equals(clientItfName)) {
      builder = null;
    }
  }
  
  // --------------------------------------------------------------------------
  // Implementation of the Compiler interface
  // --------------------------------------------------------------------------
  
  public void compile (
    List path,
    ComponentContainer container, 
    TaskMap tasks, 
    Map context) throws ADLException 
  {
    if (path.size() > 0 || !(container instanceof CoordinatesContainer)) {
      return;
    }
    compile((CoordinatesContainer)container, container, tasks);
  }
  
  private void compile (CoordinatesContainer c, ComponentContainer cc, TaskMap tasks) {
    Coordinates[] coords = c.getCoordinatess();
    Component[] comps = cc.getComponents();
    for (int i = 0; i < coords.length; ++i) {
      String name = coords[i].getName();
      for (int j = 0; j < comps.length; ++j) {
        if (comps[j].getName().equals(name)) {
          double x0 = Double.parseDouble(coords[i].getX0());
          double y0 = Double.parseDouble(coords[i].getY0());
          double x1 = Double.parseDouble(coords[i].getX1());
          double y1 = Double.parseDouble(coords[i].getY1());
          int color = Integer.parseInt(coords[i].getColor());
          
          TaskMap.TaskHole parentCreateTaskHole = tasks.getTaskHole("create", cc);
          TaskMap.TaskHole createTaskHole = tasks.getTaskHole("create", comps[j]);
          
          SetCoordinatesTask coordinatesTask = new SetCoordinatesTask(builder, x0, y0, x1, y1, color);
          coordinatesTask.setParentInstanceProviderTask(parentCreateTaskHole);
          coordinatesTask.setInstanceProviderTask(createTaskHole);
          
          ComponentPair pair = new ComponentPair(cc, comps[j]);
          tasks.addTask("coordinates", pair, coordinatesTask);
          
          coordinatesTask.addDependency(tasks.getTaskHole("add", pair), Task.PREVIOUS_TASK_ROLE, new HashMap());
          
          compile(coords[i], comps[j], tasks);
        }
      }
    }
  }
  
  // --------------------------------------------------------------------------
  // Inner classes
  // --------------------------------------------------------------------------
  
  static class SetCoordinatesTask extends AbstractConfigurationTask {

    private TaskMap.TaskHole parentInstanceProviderTask;
    
    private CoordinatesBuilder builder;
    
    private double x0, y0, x1, y1;
    
    private int color;
    
    public SetCoordinatesTask (
      CoordinatesBuilder builder,
      double x0, 
      double y0,
      double x1,
      double y1,
      int color)
    {
      this.builder = builder;
      this.x0 = x0;
      this.y0 = y0;
      this.x1 = x1;
      this.y1 = y1;
      this.color = color;
    }
    
    public InstanceProviderTask getParentInstanceProviderTask () {
    	return (parentInstanceProviderTask) == null ? null
				: (InstanceProviderTask) parentInstanceProviderTask.getTask();
    }

    public void setParentInstanceProviderTask (TaskMap.TaskHole task) {
      if (parentInstanceProviderTask != null) {
        removePreviousTask(parentInstanceProviderTask);
      }
      parentInstanceProviderTask = task;
      if (parentInstanceProviderTask != null) {
        addPreviousTask(parentInstanceProviderTask);
      }
    }
    
    public void execute (final Map context) throws Exception {
      Object parent = getParentInstanceProviderTask().getResult();
      Object component = getInstanceProviderTask().getResult();
      builder.setCoordinates(parent, component, x0, y0, x1, y1, color, context);
    }

    public Object getResult() {
      return null;
    }

    public void setResult (Object result) {
    }
  }
}
