/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.timestamp;

import java.io.File;
import java.net.URL;

import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.merger.MergeException;
import org.objectweb.fractal.adl.merger.MergeableDecoration;

/**
 * Allows to attach timestamp information to AST nodes.
 */
public class Timestamp implements MergeableDecoration {

  /** the name of the decoration used to attache timestamp values. */
  public static final String TIMESTAMP_DECORATION = "timestamp";

  private long               timestamp;

  // ---------------------------------------------------------------------------
  // Implementation of the MergeableDecoration interface
  // ---------------------------------------------------------------------------

  public Object mergeDecoration(final Object overridingDecoration)
      throws MergeException {
    if (overridingDecoration == null) return this;

    if (!(overridingDecoration instanceof Timestamp))
      throw new MergeException("Invalid decoration, not a timestamp :"
          + overridingDecoration);

    if (((Timestamp) overridingDecoration).timestamp > this.timestamp)
      return overridingDecoration;

    return this;
  }

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  /**
   * Attaches the given timestamp to the given node. If the given node has
   * already an attached timestamp, the greatest timestamp value is attached.
   * 
   * @param node the node to which the timestamp will be attached.
   * @param timestamp the timestamp value;
   */
  public static void setTimestamp(final Node node, final long timestamp) {
    Timestamp ts = (Timestamp) node.astGetDecoration(TIMESTAMP_DECORATION);
    if (ts == null) {
      ts = new Timestamp();
      ts.timestamp = timestamp;
      node.astSetDecoration(TIMESTAMP_DECORATION, ts);
    } else if (ts.timestamp < timestamp) {
      ts.timestamp = timestamp;
    }
  }

  /**
   * Attaches the timestamp of the given file to the given node.
   * 
   * @param node the node to which the timestamp will be attached.
   * @param file a file
   * @see #setTimestamp(Node, long)
   */
  public static void setTimestamp(final Node node, final File file) {
    setTimestamp(node, file.lastModified());
  }

  /**
   * Attaches the timestamp of the file denoted by the given URL to the given
   * node. If the given URL does not denote a local file, the timestamp is set
   * to 0.
   * 
   * @param node the node to which the timestamp will be attached.
   * @param url a URL.
   * @see #setTimestamp(Node, long)
   */
  public static void setTimestamp(final Node node, final URL url) {
    if (url.getProtocol().equals("file")) {
      setTimestamp(node, new File(url.getPath()));
    } else {
      setTimestamp(node, 0);
    }
  }

  /**
   * Returns the value of the attached timestamp or <code>zero</code> if the
   * given node has no attached timestamp.
   * 
   * @param node a node.
   * @return the value of the attached timestamp or <code>zero</code>.
   */
  public static long getTimestamp(final Node node) {
    final Timestamp ts = (Timestamp) node
        .astGetDecoration(TIMESTAMP_DECORATION);
    if (ts == null) return 0;
    return ts.timestamp;
  }

  /**
   * Returns <code>true</code> if and only if the given node has an attached
   * timestamp and is greater than the given timestamp, or the node has no
   * attached timestamp, or the given timestamp is null.
   * 
   * @param node a node.
   * @param timestamp the timestamp to be compared.
   * @return <code>true</code> if and only if the given node has an attached
   *         timestamp and is greater than the given timestamp, or the node has
   *         no attached timestamp.
   */
  public static boolean isNodeMoreRecentThan(final Node node,
      final long timestamp) {
    final long nodeTS = getTimestamp(node);
    return (nodeTS == 0) || (timestamp == 0) || (nodeTS > timestamp);
  }

  /**
   * Returns <code>true</code> if and only if the given node has an attached
   * timestamp and is greater than the timestamp of the given file, or the node
   * has no attached timestamp, or the file does not exist.
   * 
   * @param node a node.
   * @param file a file.
   * @return <code>true</code> if and only if the given node has an attached
   *         timestamp and is greater than the given timestamp, or the node has
   *         no attached timestamp.
   */
  public static boolean isNodeMoreRecentThan(final Node node, final File file) {
    return isNodeMoreRecentThan(node, file.lastModified());
  }

  // ---------------------------------------------------------------------------
  // Overridden Object methods
  // ---------------------------------------------------------------------------

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof Timestamp)
      return timestamp == ((Timestamp) obj).timestamp;
    return false;
  }

  @Override
  public int hashCode() {
    return (int) timestamp;
  }

  @Override
  public String toString() {
    return Long.toString(timestamp);
  }
}
