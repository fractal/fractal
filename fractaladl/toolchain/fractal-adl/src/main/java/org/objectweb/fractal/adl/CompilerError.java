/***
 * Fractal ADL Parser
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl;

import org.objectweb.fractal.adl.error.Error;
import org.objectweb.fractal.adl.error.ErrorLocator;
import org.objectweb.fractal.adl.error.ErrorTemplate;
import org.objectweb.fractal.adl.error.GenericErrors;

/**
 * Error thrown when an internal error occurs in the compiler.
 */
public class CompilerError extends java.lang.Error {

  private final Error       error;

  private static final long serialVersionUID = 1600639157911278336L;

  /**
   * Constructs a new {@link CompilerError}.
   * 
   * @param template the error templates.
   * @param args the arguments for the formated message (see
   *            {@link ErrorTemplate#getFormatedMessage(Object...)}).
   */
  public CompilerError(final ErrorTemplate template, final Object... args) {
    this(template, null, null, args);
  }

  /**
   * Constructs a new {@link CompilerError}.
   * 
   * @param template the error templates.
   * @param locator the error location. May be <code>null</code>.
   * @param args the arguments for the formated message (see
   *            {@link ErrorTemplate#getFormatedMessage(Object...)}).
   */
  public CompilerError(final ErrorTemplate template,
      final ErrorLocator locator, final Object... args) {
    this(template, locator, null, args);
  }

  /**
   * Constructs a new {@link CompilerError}.
   * 
   * @param template the error templates.
   * @param cause the cause of this error. May be <code>null</code>.
   * @param args the arguments for the formated message (see
   *            {@link ErrorTemplate#getFormatedMessage(Object...)}).
   */
  public CompilerError(final ErrorTemplate template, final Throwable cause,
      final Object... args) {
    this(template, null, cause, args);
  }

  /**
   * Constructs a new {@link CompilerError}.
   * 
   * @param template the error templates.
   * @param locator the error location. May be <code>null</code>.
   * @param cause the cause of this error. May be <code>null</code>.
   * @param args the arguments for the formated message (see
   *            {@link ErrorTemplate#getFormatedMessage(Object...)}).
   */
  public CompilerError(final ErrorTemplate template,
      final ErrorLocator locator, final Throwable cause, final Object... args) {
    this(new Error(template, locator, cause, args));
  }

  /**
   * Constructs a new {@link CompilerError}.
   * 
   * @param error the error reported by this exception
   */
  public CompilerError(final Error error) {
    super(error.getCause());
    this.error = error;
  }

  /**
   * Constructs a new {@link CompilerError}. <br>
   * This constructor is deprecated, use {@link #CompilerError(Error)} or one of
   * its variant instead.
   * 
   * @param message a detail message.
   * @param cause the cause of this error.
   */
  @Deprecated
  public CompilerError(final String message, final Throwable cause) {
    this(new Error(GenericErrors.INTERNAL_ERROR, cause, message));
  }

  /**
   * Constructs a new {@link CompilerError}. <br>
   * This constructor is deprecated, use {@link #CompilerError(Error)} or one of
   * its variant instead.
   * 
   * @param message a detail message.
   */
  @Deprecated
  public CompilerError(final String message) {
    this(message, null);
  }

  /**
   * Constructs a new {@link CompilerError}. <br>
   * This constructor is deprecated, use {@link #CompilerError(Error)} or one of
   * its variant instead.
   * 
   * @param cause the cause of this error.
   */
  @Deprecated
  public CompilerError(final Throwable cause) {
    this(null, cause);
  }

  /**
   * Returns the {@link Error} object reported by this exception.
   * 
   * @return the {@link Error} object reported by this exception.
   */
  public Error getError() {
    return error;
  }

  @Override
  public String getMessage() {
    return error.toString();
  }
}
