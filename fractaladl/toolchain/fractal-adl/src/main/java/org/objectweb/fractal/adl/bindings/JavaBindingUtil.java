/***
 * Fractal ADL Parser
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.bindings;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;

/**
 * Utility class to bind component intantiated using a Java-Backend (i.e. when
 * components are plain Java object). This class can be used with plain Java
 * object implementing the {@link BindingController} interface.
 */
public final class JavaBindingUtil {

  static final String IMPORT_BINDING_PREFIX = "__IMPORT__";

  private JavaBindingUtil() {
  }

  /**
   * Utility method that can be used in replacement of the
   * {@link BindingController#listFc()} method for plain Java component.
   * 
   * @param component a plain Java object primitive component
   * @return the list of its client interface name.
   */
  public static String[] listFc(final Object component) {
    final Set<String> itfNames = new HashSet<String>();
    if (component instanceof BindingController) {
      final String[] s = ((BindingController) component).listFc();
      for (final String s1 : s)
        itfNames.add(s1);
    }

    return itfNames.toArray(new String[0]);
  }

  /**
   * Utility method that can be used in replacement of the
   * {@link BindingController#bindFc(String, Object)} method for plain Java
   * component. This method will first look for a {@link Field} with the correct
   * {@link ClientInterface} annotation. If this search fails, it will then call
   * the {@link BindingController#bindFc(String, Object) bindFc} method.
   * 
   * @param component a plain Java object primitive component
   * @param itfName the client interface name
   * @param serverItf the server interface
   * @throws IllegalBindingException if the binding cannot be created.
   */
  @SuppressWarnings("unchecked")
  public static void bindFc(final Object component, final String itfName,
      final Object serverItf) throws IllegalBindingException {
    // component is a primitive component.
    try {

      // field with a ClientInterface annotation not found, call bindFc method
      final Method m = component.getClass().getMethod("bindFc",
          new Class[]{String.class, Object.class});
      m.invoke(component, new Object[]{itfName, serverItf});
    } catch (final Exception e) {
      final IllegalBindingException ibe = new IllegalBindingException(
          "Can't bind client interface '" + itfName + "' of component '"
              + component.getClass().getName() + "'");
      ibe.initCause(e);
      throw ibe;
    }
  }

  /**
   * Utility method that can be used to bind client interface of composite
   * components instantiated with the Java backend.
   * <p>
   * <b>Warning</b>: This method is unable to check that the given
   * <code>itfName</code> refers to a client interface of the composite (and
   * not to a server one). If <code>itfName</code> refers to a server
   * interface, the execution of this method may cause unexpectable results.
   * 
   * @param composite a plain Java object composite component
   * @param itfName the client interface name
   * @param serverItf the server interface
   * @throws IllegalBindingException if the binding cannot be created.
   */
  @SuppressWarnings("unchecked")
  public static void bindComposite(final Map composite, final String itfName,
      final Object serverItf) throws IllegalBindingException {

    // lookfor import binding information
    final Set<JavaBindingEnd> importBindings = (Set<JavaBindingEnd>) composite
        .get(IMPORT_BINDING_PREFIX + itfName);
    if (importBindings != null) {
      // import binding info found. bind internal client to given server.
      for (final JavaBindingEnd client : importBindings) {
        bindFc(client.component, client.itfName, serverItf);
      }
    } else {
      throw new IllegalBindingException("Can't bind client interface '"
          + itfName + "' of composite component: no such interface");
    }
  }

  /**
   * Package private method used by {@link JavaBindingBuilder} to store
   * information concerning import bindings.
   * <p>
   * <b>Warning</b>: The {@link JavaBindingBuilder} can't distinguish import
   * binding (i.e. a binding from a client interface of a sub component to a
   * client interface of the composite) and normal binding (binding to a server
   * interface of the composite). This implies that this method will be called
   * for both of these bindings. So part of the information stored by this
   * method, in the composite {@link Map} is not relevant (hence the warning of
   * the {@link #bindComposite(Map, String, Object)} method.
   * 
   * @param compositeClientSide the composite binding side.
   * @param subComponentClientSide the sub component binding side.
   */
  @SuppressWarnings("unchecked")
  static void storeReverseImportBinding(
      final JavaBindingEnd compositeClientSide,
      final JavaBindingEnd subComponentClientSide) {
    // store import binding only if subComponentClientSide is a primitive.
    if (subComponentClientSide.isComposite()) return;

    final Map composite = (Map) compositeClientSide.component;
    Set<JavaBindingEnd> importBindings = (Set<JavaBindingEnd>) composite
        .get(IMPORT_BINDING_PREFIX + compositeClientSide.itfName);
    if (importBindings == null) {
      importBindings = new HashSet<JavaBindingEnd>();
      composite.put(IMPORT_BINDING_PREFIX + compositeClientSide.itfName,
          importBindings);
    }
    importBindings.add(subComponentClientSide);
  }
}
