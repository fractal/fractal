/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2006 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributor: Philippe Merle
 *
 * $Id$
 */

package org.objectweb.fractal.adl.implementations;

import java.util.HashMap;

import org.objectweb.fractal.adl.util.ClassLoaderHelper;

/**
 * A Java based implementation of the {@link ImplementationBuilder} interface. 
 */

public class JavaImplementationBuilder implements ImplementationBuilder {
  
  // --------------------------------------------------------------------------
  // Implementation of the ImplementationBuilder interface
  // --------------------------------------------------------------------------
  
  public Object createComponent (
    final Object type, 
    final String name,
    final String definition,
    final Object controllerDesc, 
    final Object contentDesc, 
    final Object context) throws Exception 
  {
    ClassLoader loader = ClassLoaderHelper.getClassLoader(this, context);
    
    if (contentDesc == null) {
      return new HashMap();
    }
    String c = (String)contentDesc;
    return loader.loadClass(c).newInstance();
  }
}
