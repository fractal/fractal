/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.arguments;

import static org.objectweb.fractal.adl.error.ErrorTemplateValidator.validErrorTemplate;

import org.objectweb.fractal.adl.error.ErrorTemplate;

/** {@link ErrorTemplate} group for the arguments package. */
public enum ArgumentErrors implements ErrorTemplate {

  /** */
  INVALID_ARGUMENT_VALUE_LIST("Syntax error in definition name."),

  /** */
  INVALID_ARGUMENT_VALUE_SPECIFICATION(
      "Invalid argument value list: cannot mix argument specified by name and argument specified by position."),

  /** */
  EMPTY_ARGUMENT_NAME("Invalid empty argument name in argument list \"%s\"",
      "arglist"),

  /** */
  INVALID_ARGNAME("Invalid argument name \"%s\"; can't contain a '%s'",
      "argname", "char"),

  /** */
  NO_VALUE("No value defined for argument \"%s\"", "argname"),

  /** */
  INVALID_PATTERN_SYNTAX_ERROR("Syntax error in expression \"%s\"", "expr"),

  /** */
  INVALID_PATTERN_DEFAULT_VALUE(
      "Syntax error in expression \"%s\": default values are not allowed for argument substitution",
      "expr"),

  /** */
  UNKNOWN_PREFIX("Unknown prefix \"%s\" in \"%s\"", "prefix", "argname"),

  /** */
  UNDEFINED_ARGUMENT("Undefined argument \"%s\"", "argname"),

  /** */
  INVALID_VALUE_NOT_A_STRING(
      "Invalid variable value for argument \"%s\": not a String", "argname");

  /** The groupId of ErrorTemplates defined in this enumeration. */
  public static final String GROUP_ID = "ARG";

  private int                id;
  private String             format;

  private ArgumentErrors(final String format, final Object... args) {
    this.id = ordinal();
    this.format = format;

    assert validErrorTemplate(this, args);
  }

  public int getErrorId() {
    return id;
  }

  public String getGroupId() {
    return GROUP_ID;
  }

  public String getFormatedMessage(final Object... args) {
    return String.format(format, args);
  }

  public String getFormat() {
    return format;
  }
}
