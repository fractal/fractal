/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.bindings;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.objectweb.fractal.adl.ContextLocal;
import org.objectweb.fractal.api.control.IllegalBindingException;

/**
 * A Java based implementation of the {@link BindingBuilder} interface. This
 * builder is able to bind plain Java objects.
 * 
 * @see JavaBindingUtil
 */
public class JavaBindingBuilder implements BindingBuilder {

  /**
   * Associates a list of bindings to each context. The contexts are the last
   * argument of the bindComponent method. The list of bindings for a context is
   * the list of the primitive bindings that have been build so far in this
   * context, as a {@link Map} associating client {@link JavaBindingEnd} to
   * server {@link JavaBindingEnd}.
   */
  private final ContextLocal<Map<JavaBindingEnd, JavaBindingEnd>> bindingLists = new ContextLocal<Map<JavaBindingEnd, JavaBindingEnd>>();

  // --------------------------------------------------------------------------
  // Implementation of the BindingBuilder interface
  // --------------------------------------------------------------------------

  public void bindComponent(final int type, final Object clientComp,
      final String clientItf, final Object serverComp, final String serverItf,
      final Object context) throws IllegalBindingException {

    JavaBindingEnd clientSide = new JavaBindingEnd(clientComp, clientItf);
    JavaBindingEnd serverSide = new JavaBindingEnd(serverComp, serverItf);

    Map<JavaBindingEnd, JavaBindingEnd> bindings = bindingLists.get(context);
    if (bindings == null) {
      bindings = new HashMap<JavaBindingEnd, JavaBindingEnd>();
      bindingLists.set(context, bindings);
    }

    /*
     * Since a single binding may resolve many pending ones, we add the new
     * binding in the map and re-evaluate the whole graph.
     */
    bindings.put(clientSide, serverSide);

    // for each pending bindings (use iterator since entry may be removed)
    for (final Iterator<Map.Entry<JavaBindingEnd, JavaBindingEnd>> iter = bindings
        .entrySet().iterator(); iter.hasNext();) {

      final Map.Entry<JavaBindingEnd, JavaBindingEnd> entry = iter.next();
      clientSide = entry.getKey();
      serverSide = entry.getValue();

      // try to find the primitive server component bound to the client...
      while (serverSide != null && serverSide.isComposite()) {
        // store reverse import binding information
        JavaBindingUtil.storeReverseImportBinding(serverSide, clientSide);
        // while the server is a composite
        // find a binding for which the client side is the current server.
        serverSide = bindings.get(serverSide);
      }

      // if a primitive server has been found.
      if (serverSide != null) {
        // bind client to server
        clientSide.bind(serverSide);

        // if client is a primitive, this binding has been done correctly,
        // we can remove binding form list of pending bindings.
        if (!clientSide.isComposite()) iter.remove();
      }
    }
  }
}
