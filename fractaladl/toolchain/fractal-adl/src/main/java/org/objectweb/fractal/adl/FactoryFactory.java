/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 *
 * Contributor(s): Matthieu Leclercq, Alessio Pace
 */

package org.objectweb.fractal.adl;

import java.util.Map;

import org.objectweb.fractal.adl.arguments.ArgumentComponentLoader;
import org.objectweb.fractal.adl.arguments.ArgumentLoader;
import org.objectweb.fractal.adl.attributes.AttributeCompiler;
import org.objectweb.fractal.adl.attributes.AttributeLoader;
import org.objectweb.fractal.adl.attributes.JavaAttributeBuilder;
import org.objectweb.fractal.adl.bindings.BindingCompiler;
import org.objectweb.fractal.adl.bindings.JavaBindingBuilder;
import org.objectweb.fractal.adl.bindings.TypeBindingLoader;
import org.objectweb.fractal.adl.bindings.UnboundInterfaceDetectorLoader;
import org.objectweb.fractal.adl.components.ComponentCompiler;
import org.objectweb.fractal.adl.components.JavaComponentBuilder;
import org.objectweb.fractal.adl.components.PrimitiveComponentCompiler;
import org.objectweb.fractal.adl.implementations.ImplementationCompiler;
import org.objectweb.fractal.adl.implementations.ImplementationLoader;
import org.objectweb.fractal.adl.implementations.JavaImplementationBuilder;
import org.objectweb.fractal.adl.interfaces.InterfaceLoader;
import org.objectweb.fractal.adl.merger.NodeMerger;
import org.objectweb.fractal.adl.merger.NodeMergerImpl;
import org.objectweb.fractal.adl.types.JavaTypeBuilder;
import org.objectweb.fractal.adl.types.TypeCompiler;
import org.objectweb.fractal.adl.types.TypeLoader;
import org.objectweb.fractal.adl.xml.XMLLoader;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;
import org.objectweb.fractal.task.core.BasicScheduler;

/**
 * Provides static methods to get a {@link Factory} component.
 */

public final class FactoryFactory {

  private FactoryFactory() {
  }

  /** The name of the ADL of the default factory. */
  public static final String DEFAULT_FACTORY             = "org.objectweb.fractal.adl.BasicFactory";

  /** The name of the ADL of the Fractal backend. */
  public static final String FRACTAL_BACKEND             = "org.objectweb.fractal.adl.FractalBackend";

  /** The name of the ADL of the static Fractal backend. */
  public static final String STATIC_FRACTAL_BACKEND      = "org.objectweb.fractal.adl.StaticFractalBackend";

  /** The name of the ADL of the Java backend. */
  public static final String JAVA_BACKEND                = "org.objectweb.fractal.adl.JavaBackend";

  /** The name of the ADL of the static Java backend. */
  public static final String STATIC_JAVA_BACKEND         = "org.objectweb.fractal.adl.StaticJavaBackend";

  /**
   * The prefix commonly used for FractalADL related parameter values.
   */
  public static final String FRACTALADL_PARAMETER_PREFIX = "fractaladl.";

  /**
   * The FractalADL backend used when compiling components.
   */
  public static final String BACKEND_PROPERTY_NAME       = FRACTALADL_PARAMETER_PREFIX
                                                             + "backend";

  private static Factory     factory;

  /**
   * Returns a bootstrap {@link Factory}, with a Java backend.
   * 
   * @return a bootstrap factory.
   */

  public static Factory getFactory() {
    if (factory != null) {
      return factory;
    }

    final BasicFactory r = new BasicFactory();

    final JavaCodeLoader cl = new JavaCodeLoader();

    final NodeMerger nm = new NodeMergerImpl();
    final XMLNodeFactory nFact = new XMLNodeFactoryImpl();
    final XMLLoader xmll = new XMLLoader();
    final ArgumentLoader argl = new ArgumentLoader();
    final ArgumentComponentLoader compl = new ArgumentComponentLoader();
    final InterfaceLoader itfl = new InterfaceLoader();
    final TypeLoader typl = new TypeLoader();
    final AttributeLoader attrl = new AttributeLoader();
    final ImplementationLoader impll = new ImplementationLoader();
    final TypeBindingLoader bindl = new TypeBindingLoader();
    // necessary to check inherited/overriden attributes
    final UnboundInterfaceDetectorLoader uidl = new UnboundInterfaceDetectorLoader();

    final TypeCompiler typc = new TypeCompiler();
    final ImplementationCompiler implc = new ImplementationCompiler();
    final PrimitiveComponentCompiler compc = new PrimitiveComponentCompiler();
    final BindingCompiler bindc = new BindingCompiler();
    final AttributeCompiler attrc = new AttributeCompiler();
    final ComponentCompiler allc = new ComponentCompiler();

    final JavaTypeBuilder typb = new JavaTypeBuilder();
    final JavaImplementationBuilder implb = new JavaImplementationBuilder();
    final JavaComponentBuilder compb = new JavaComponentBuilder();
    final JavaBindingBuilder bindb = new JavaBindingBuilder();
    final JavaAttributeBuilder attrb = new JavaAttributeBuilder();

    final BasicScheduler s = new BasicScheduler();

    typc.bindFc(TypeCompiler.BUILDER_BINDING, typb);
    implc.bindFc(ImplementationCompiler.BUILDER_BINDING, implb);
    compc.bindFc(PrimitiveComponentCompiler.BUILDER_BINDING, compb);
    bindc.bindFc(BindingCompiler.BUILDER_BINDING, bindb);
    attrc.bindFc(AttributeCompiler.BUILDER_BINDING, attrb);

    xmll.nodeFactoryItf = nFact;
    argl.clientLoader = xmll;
    compl.clientLoader = argl;
    itfl.clientLoader = compl;
    typl.clientLoader = itfl;
    attrl.clientLoader = typl;
    impll.clientLoader = attrl;
    bindl.clientLoader = impll;
    uidl.clientLoader = bindl;

    typl.interfaceCodeLoaderItf = cl;
    impll.interfaceLoaderItf = cl;
    impll.implementationCodeLoaderItf = cl;
    bindl.interfaceCodeLoaderItf = cl;
    attrl.interfaceLoaderItf = cl;

    compl.nodeMergerItf = nm;

    allc.bindFc(ComponentCompiler.PRIMITIVE_COMPILERS_BINDING + "0", typc);
    allc.bindFc(ComponentCompiler.PRIMITIVE_COMPILERS_BINDING + "1", implc);
    allc.bindFc(ComponentCompiler.PRIMITIVE_COMPILERS_BINDING + "2", compc);
    allc.bindFc(ComponentCompiler.PRIMITIVE_COMPILERS_BINDING + "3", bindc);
    allc.bindFc(ComponentCompiler.PRIMITIVE_COMPILERS_BINDING + "4", attrc);

    r.bindFc(BasicFactory.LOADER_BINDING, uidl);
    r.bindFc(BasicFactory.COMPILER_BINDING, allc);
    r.bindFc(BasicFactory.SCHEDULER_BINDING, s);

    factory = r;
    return r;
  }

  /**
   * Returns a {@link Factory} with the given backend.
   * 
   * @param backend the desired backend.
   * @return a {@link Factory} with the given backend.
   * @throws ADLException if the factory cannot be created.
   */

  public static Factory getFactory(final String backend) throws ADLException {
    return getFactory(backend, ContextMap.instance()); // new HashMap<Object,
    // Object>());
  }

  /**
   * Returns a {@link Factory} with the given backend.
   * 
   * @param backend the desired backend.
   * @param context additional optional information.
   * @return a {@link Factory} with the given backend.
   * @throws ADLException if the factory cannot be created.
   */

  public static Factory getFactory(final String backend,
      final Map<Object, Object> context) throws ADLException {
    return getFactory(DEFAULT_FACTORY, backend, context);
  }

  /**
   * Returns a {@link Factory} with the given implementation and backend.
   * 
   * @param factory the name of the desired factory.
   * @param backend the desired backend.
   * @param context additional optional information.
   * @return a {@link Factory} with the given backend.
   * @throws ADLException if the factory cannot be created.
   */

  public static Factory getFactory(final String factory, final String backend,
      final Map<Object, Object> context) throws ADLException {
    final Factory f = getFactory();
    context.put(FactoryFactory.BACKEND_PROPERTY_NAME, backend);
    final Map<?, ?> c = (Map<?, ?>) f.newComponent(factory, context);
    return (Factory) c.get("factory");
  }
}
