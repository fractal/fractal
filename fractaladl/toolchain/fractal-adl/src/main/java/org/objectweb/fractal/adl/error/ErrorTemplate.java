/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.error;

import java.util.Formatter;

/**
 * An error template defines a type of error that can be reported by the
 * FractalADL tool-chain. It is identified by a {@link #getGroupId() groupId}
 * which identifies a group of related error types and a
 * {@link #getErrorId() errorId} which must be unique in a given group. An error
 * template contains a {@link format #getFormat()} string that defines the
 * message to be issued for the type of error. This string is a "printf like"
 * string as defined in the {@link Formatter} class. <br>
 * It is recommended to implement this interface only in enumeration classes
 * that regroup every ErrorTemplate of the same group (see <a
 * href="package-summary.html#errorTemplateDesc">package description</a> for
 * details)
 */
public interface ErrorTemplate {

  /**
   * Returns the number that identify this ErrorTemplate in its
   * {@link #getGroupId() group}. This number must be unique in the error group
   * this ErrorTemplate belongs. It is recommended for this identifier not to be
   * greater that <code>999</code>.
   * 
   * @return the number that identify this ErrorTemplate in its
   *         {@link #getGroupId() group}.
   */
  int getErrorId();

  /**
   * Returns the identifier of the error group this ErrorTemplate belongs. It is
   * recommended for this identifier string to be upper case and as short as
   * possible ( <code>AC</code>, <code>BC</code> for instance) since it
   * will be issued in the error message as a prefix.
   * 
   * @return the identifier of the error group this ErrorTemplate belongs.
   */
  String getGroupId();

  /**
   * Returns the format of the error message that will be issued for errors of
   * that type. This string is a "printf like" string as defined in the
   * {@link Formatter} class.
   * 
   * @return the format of the error message that will be issued for errors of
   *         that type
   */
  String getFormat();

  /**
   * Returns a formated error message using the given arguments.
   * 
   * @param args the format arguments.
   * @return a formated error message.
   * @see String#format(String, Object...)
   */
  String getFormatedMessage(Object... args);
}
