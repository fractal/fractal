/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.components;

import static org.objectweb.fractal.adl.error.ErrorTemplateValidator.validErrorTemplate;

import org.objectweb.fractal.adl.error.ErrorTemplate;

/** {@link ErrorTemplate} group for the components package. */
public enum ComponentErrors implements ErrorTemplate {

  /** */
  DEFINITION_CYCLE("Cycle in definition references: %s.", "cycle"),

  /** */
  COMPONENT_NAME_MISSING("Component name must be specified."),

  /** */
  DUPLICATED_COMPONENT_NAME(
      "Redefinition of sub component \"%s\" (previously defined at \"%s\").",
      "name", "location"),

  /** */
  INVALID_PATH("Invalid path \"%s\": no such component.", "path"),

  /** */
  SHARED_WITH_DIFFERENT_NAME(
      "Shared components with distinct names not yet supported"),

  /** */
  MERGE_ERROR("An error occurs while merging definitions (def=\"%s\")", "def");

  /** The groupId of ErrorTemplates defined in this enumeration. */
  public static final String GROUP_ID = "CMP";

  private int                id;
  private String             format;

  private ComponentErrors(final String format, final Object... args) {
    this.id = ordinal();
    this.format = format;

    assert validErrorTemplate(this, args);
  }

  public int getErrorId() {
    return id;
  }

  public String getGroupId() {
    return GROUP_ID;
  }

  public String getFormatedMessage(final Object... args) {
    return String.format(format, args);
  }

  public String getFormat() {
    return format;
  }
}
