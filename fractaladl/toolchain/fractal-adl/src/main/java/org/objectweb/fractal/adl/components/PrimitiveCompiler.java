/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.components;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.task.core.TaskMap;

/**
 * A component definition compiler. This compiler compiles a single component at
 * a time, i.e. it does not recursively compiles the definitions of the sub
 * components of a component.
 */

public interface PrimitiveCompiler {

  /**
   * Compile a single component.
   * 
   * @param path the path of the component.
   * @param container the component.
   * @param tasks the task map.
   * @param context additional parameters.
   * @throws ADLException if something wrong happen.
   */
  void compile(List<ComponentContainer> path, ComponentContainer container,
      TaskMap tasks, Map<Object, Object> context) throws ADLException;
}
