/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2006 France Telecom R&D
 * Copyright (C) 2006-2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributor: Philippe Merle, Matthieu Leclercq
 *
 * $Id$
 */

package org.objectweb.fractal.adl.components;

import static org.objectweb.fractal.adl.error.ChainedErrorLocator.chainLocator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.error.NodeErrorLocator;
import org.objectweb.fractal.adl.merger.MergeException;
import org.objectweb.fractal.adl.merger.NodeMerger;
import org.objectweb.fractal.adl.util.FractalADLLogManager;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;

/**
 * A {@link org.objectweb.fractal.adl.Loader} to check {@link Component} nodes
 * in definitions. This loader checks sub component names, loads the definitions
 * referenced by the component nodes (and the "extends" attribute), and merges
 * all these definitions into a single one (by following inheritance rules).
 */
public class ComponentLoader extends AbstractLoader
    implements
      ComponentLoaderAttributes {

  protected static Logger             logger = FractalADLLogManager
                                                 .getLogger("load");

  // -------------------------------------------------------------------------
  // Client interfaces
  // -------------------------------------------------------------------------

  /** The node merger component used to merge AST. */
  public NodeMerger                   nodeMergerItf;

  // -------------------------------------------------------------------------
  // Attributes
  // -------------------------------------------------------------------------

  /**
   * The names of the "name" attribute for each AST node type. This map
   * associates the names of the "name" attribute, used to detect overridden
   * elements, to each AST node type that has such an attribute.
   */
  protected final Map<String, String> nameAttributes;

  // -------------------------------------------------------------------------
  // Constructor
  // -------------------------------------------------------------------------

  /**
   * Default constructor.
   */
  public ComponentLoader() {
    nameAttributes = new HashMap<String, String>();
    nameAttributes.put("component", "name");
    nameAttributes.put("interface", "name");
    nameAttributes.put("binding", "from");
    nameAttributes.put("attribute", "name");
    nameAttributes.put("coordinates", "name");
  }

  // -------------------------------------------------------------------------
  // Implementation of the ComponentLoaderAttributes interface
  // -------------------------------------------------------------------------

  public String getNameAttributes() {
    final StringBuffer b = new StringBuffer();
    for (final Map.Entry<String, String> e : nameAttributes.entrySet()) {
      b.append(e.getKey());
      b.append(' ');
      b.append(e.getValue());
      b.append(' ');
    }
    return b.toString();
  }

  public void setNameAttributes(String nameAttributes) {
    this.nameAttributes.clear();
    String key = null;
    int p = nameAttributes.indexOf(' ');
    while (p != -1) {
      final String s = nameAttributes.substring(0, p);
      if (key == null) {
        key = s;
      } else {
        this.nameAttributes.put(key, s);
        key = null;
      }
      nameAttributes = nameAttributes.substring(p + 1);
      p = nameAttributes.indexOf(' ');
    }
    if (key != null) {
      this.nameAttributes.put(key, nameAttributes);
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the Loader interface
  // -------------------------------------------------------------------------

  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    return load(new LinkedHashSet<String>(), name, context);
  }

  protected Definition load(final Set<String> loaded, final String name,
      final Map<Object, Object> context) throws ADLException {
    if (!loaded.add(name)) {
      throw new ADLException(ComponentErrors.DEFINITION_CYCLE, loaded
          .toString());
    }
    Definition d = clientLoader.load(name, context);
    if (d instanceof ComponentContainer) {
      final ComponentContainer container = (ComponentContainer) d;
      normalizeComponentContainer(container);
      resolveComponentContainer(loaded, container, container, context);
    }

    if (d instanceof ComponentDefinition) {
      d = resolveDefinitionExtension(loaded, (ComponentDefinition) d, context);
    }

    if (d instanceof ComponentContainer) {
      final ComponentContainer container = (ComponentContainer) d;
      resolveSharedComponentContainer(container, container, context);
    }
    loaded.remove(name);
    return d;
  }

  // -------------------------------------------------------------------------
  // First pass: normalization
  // -------------------------------------------------------------------------

  protected void normalizeComponentContainer(final ComponentContainer container)
      throws ADLException {
    final Map<String, Component> names = new HashMap<String, Component>();
    for (final Component comp : container.getComponents()) {
      final String name = comp.getName();
      if (name == null) {
        throw new ADLException(ComponentErrors.COMPONENT_NAME_MISSING, comp);
      }
      final Component previousDefinition = names.put(name, comp);
      if (previousDefinition != null) {
        throw new ADLException(ComponentErrors.DUPLICATED_COMPONENT_NAME, comp,
            name, new NodeErrorLocator(previousDefinition));
      }
      normalizeComponentContainer(comp);
    }
  }

  // -------------------------------------------------------------------------
  // Second pass: definition references resolution
  // -------------------------------------------------------------------------

  protected void resolveComponentContainer(final Set<String> loaded,
      final ComponentContainer topLevelDefinition,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    for (final Component comp : container.getComponents()) {
      resolveComponentContainer(loaded, topLevelDefinition, comp, context);
      final String definition = comp.getDefinition();
      if (definition != null) {
        final List<String> defs = parseDefinitions(definition, comp);
        // shared components will be resolved by the
        // resolveSharedComponentContainer method.
        if (defs.size() != 1 || !isShared(defs.get(0))) {
          comp.setDefinition(null);
          ((Node) comp).astSetDecoration("definition", definition);
          Definition d;
          try {
            d = resolveDefinitions(loaded, defs, context);
          } catch (final ADLException e) {
            logger.log(Level.FINE,
                "ADLException while loading referenced definition(s)", e);
            chainLocator(e, comp);
            throw e;
          }
          Node merged;
          try {
            merged = nodeMergerItf.merge(comp, d, nameAttributes);
          } catch (final MergeException e) {
            throw new CompilerError(ComponentErrors.MERGE_ERROR,
                new NodeErrorLocator(comp), e, definition);
          }
          if (merged != comp) {
            // the container AST is a pure tree (i.e. no sharing), so simply
            // replace the merged node in the container.
            container.removeComponent(comp);
            container.addComponent((Component) merged);
          }
        }
      }
    }
  }

  // -------------------------------------------------------------------------
  // Third pass: definition inheritance resolution
  // -------------------------------------------------------------------------

  protected ComponentDefinition resolveDefinitionExtension(
      final Set<String> loaded, ComponentDefinition def,
      final Map<Object, Object> context) throws ADLException {
    if (def.getExtends() != null) {
      final List<String> defs = parseDefinitions(def.getExtends(), def);
      try {
        def = (ComponentDefinition) nodeMergerItf.merge(def,
            resolveDefinitions(loaded, defs, context), nameAttributes);
      } catch (final ADLException e) {
        logger.log(Level.FINE,
            "ADLException while loading super definition(s)", e);
        chainLocator(e, def);
        throw e;
      } catch (final MergeException e) {
        throw new CompilerError(ComponentErrors.MERGE_ERROR,
            new NodeErrorLocator(def), e, def.getName());
      }
      def.setExtends(null);
    }
    return def;
  }

  protected void resolveSharedComponentContainer(
      final ComponentContainer topLevelDefinition,
      final ComponentContainer container, final Map<Object, Object> context)
      throws ADLException {
    final Component[] comps = container.getComponents();
    for (int i = 0; i < comps.length; i++) {
      final Component comp = comps[i];
      resolveSharedComponentContainer(topLevelDefinition, comp, context);
      String definition = comp.getDefinition();
      if (definition != null) {
        final List<String> defs = parseDefinitions(definition, comp);
        if (defs.size() == 1 && isShared(defs.get(0))) {
          // shared component
          comp.setDefinition(null);
          ((Node) comp).astSetDecoration("definition", definition);
          if (definition.startsWith("./")) {
            definition = definition.substring(2);
          }
          final Component c = getPathComponent(topLevelDefinition, definition);
          if (c == null) {
            throw new ADLException(ComponentErrors.INVALID_PATH, comp,
                definition);
          }
          if (!c.getName().equals(comps[i].getName())) {
            throw new ADLException(ComponentErrors.SHARED_WITH_DIFFERENT_NAME,
                comp);
          }
          final Map<Node, Node> replacements = new HashMap<Node, Node>();

          // comp and c are in fact the same component, merge their descriptions
          // and replace there references where needed.
          Node merged;
          try {
            merged = nodeMergerItf.merge(comp, c, nameAttributes);
          } catch (final MergeException e) {
            throw new CompilerError(ComponentErrors.MERGE_ERROR,
                new NodeErrorLocator(comp), e, definition);
          }
          if (comp != merged) {
            replacements.put(comp, merged);
          }
          replacements.put(c, merged);
          replaceComponents(topLevelDefinition, replacements);
        }
      }
    }
  }

  protected List<String> parseDefinitions(String nameList, final Node n)
      throws ADLException {
    final List<String> l = new ArrayList<String>();
    int p = nameList.indexOf(',');
    while (p != -1) {
      l.add(nameList.substring(0, p));
      nameList = nameList.substring(p + 1);
      p = nameList.indexOf(',');
    }
    l.add(nameList);
    return l;
  }

  protected boolean isShared(final String definition) {
    return definition.indexOf('/') != -1;
  }

  protected Definition resolveDefinitions(final Set<String> loaded,
      final List<String> nameList, final Map<Object, Object> context)
      throws ADLException {
    Definition d = load(loaded, nameList.get(0), context);
    for (int i = 1; i < nameList.size(); ++i) {
      Definition e;
      e = load(loaded, nameList.get(i), context);
      try {
        d = (Definition) nodeMergerItf.merge(e, d, nameAttributes);
      } catch (final MergeException me) {
        throw new CompilerError(ComponentErrors.MERGE_ERROR, me, nameList);
      }
    }
    return d;
  }

  protected Component getComponent(final ComponentContainer container,
      final String name) {
    for (final Component comp : container.getComponents()) {
      if (comp.getName().equals(name)) {
        return comp;
      }
    }
    return null;
  }

  protected Component getPathComponent(final ComponentContainer container,
      final String name) {
    final int p = name.indexOf('/');
    if (p == -1) {
      return getComponent(container, name);
    }
    final Component parent = getComponent(container, name.substring(0, p));
    if (parent == null) return null;
    return getPathComponent(parent, name.substring(p + 1));
  }

  protected ComponentContainer replaceComponents(
      final ComponentContainer container, final Map<Node, Node> replacements) {
    final Node node = replacements.get(container);
    if (node != null) {
      return (ComponentContainer) node;
    }
    final Component[] comps = container.getComponents();
    for (final Component element : comps) {
      container.removeComponent(element);
    }
    for (final Component element : comps) {
      container.addComponent((Component) replaceComponents(element,
          replacements));
    }
    return container;
  }

  // -------------------------------------------------------------------------
  // Overridden Binding Controller methods
  // -------------------------------------------------------------------------

  @Override
  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (NodeMerger.ITF_NAME.equals(s)) {
      nodeMergerItf = (NodeMerger) o;
    } else {
      super.bindFc(s, o);
    }
  }

  @Override
  public String[] listFc() {
    final String[] superList = super.listFc();
    final String[] list = new String[superList.length + 1];
    list[0] = NodeMerger.ITF_NAME;
    System.arraycopy(superList, 0, list, 1, superList.length);
    return list;
  }

  @Override
  public Object lookupFc(final String s) throws NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (NodeMerger.ITF_NAME.equals(s)) {
      return this.nodeMergerItf;
    } else {
      return super.lookupFc(s);
    }
  }

  @Override
  public void unbindFc(final String s) throws NoSuchInterfaceException,
      IllegalBindingException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (NodeMerger.ITF_NAME.equals(s)) {
      nodeMergerItf = null;
    } else {
      super.unbindFc(s);
    }
  }

}
