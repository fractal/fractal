/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 * 
 * Contributors: Alessio Pace
 */

package org.objectweb.fractal.adl.implementations;

import java.util.Map;

import org.objectweb.fractal.adl.ContextMap;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.util.Fractal;

/**
 * A Fractal based implementation of the {@link ImplementationBuilder} 
 * interface. This implementation uses the Fractal API to create components. 
 */

public class FractalImplementationBuilder implements ImplementationBuilder {
  
  // --------------------------------------------------------------------------
  // Implementation of the ImplementationBuilder interface
  // --------------------------------------------------------------------------
  
  public Object createComponent (
    final Object type, 
    final String name,
    final String definition,
    final Object controllerDesc, 
    final Object contentDesc, 
    final Object context) throws Exception
  {
    ClassLoader loader = null;
    if (context instanceof Map) {
      loader = (ClassLoader)((Map)context).get("classloader");
    }

    Component bootstrap = null;
    if (context != null) {
      bootstrap = (Component)((Map)context).get("bootstrap");
    }
    if (bootstrap == null) {
      if (loader != null) {
        Map ctxt = ContextMap.instance(); // new HashMap();
        ctxt.put("classloader", loader);
        bootstrap = Fractal.getBootstrapComponent(ctxt);
      } else {
        bootstrap = Fractal.getBootstrapComponent();
      }
    }
    Component result = Fractal.getGenericFactory(bootstrap).newFcInstance(
      (ComponentType)type, 
      loader == null ? controllerDesc : new Object[] {loader, controllerDesc}, 
      contentDesc);
    try {
      Fractal.getNameController(result).setFcName(name);
    } catch (NoSuchInterfaceException ignored) {
    }
    return result;
  }
}
