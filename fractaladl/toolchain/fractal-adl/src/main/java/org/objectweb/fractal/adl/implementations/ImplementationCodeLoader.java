/***
 * Fractal ADL Parser
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.implementations;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;

/**
 * Interface used to load component implementation code.
 */
public interface ImplementationCodeLoader {

  /**
   * Load an implementation code.
   * 
   * @param signature the signature of the implementation.
   * @param language the language of the implementation.
   * @param context optional additional information.
   * @return An Object representing the loaded implementation. The runtime type
   *         of this object depends on the language of the implementation (for
   *         Java the returned type is {@link java.lang.Class}).
   * @throws ADLException if the implementation can't be loaded.
   */
  Object loadImplementation(String signature, String language,
      Map<Object, Object> context) throws ADLException;
}
