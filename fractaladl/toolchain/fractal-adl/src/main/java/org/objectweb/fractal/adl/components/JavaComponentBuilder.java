/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.components;

import org.objectweb.fractal.api.control.LifeCycleController;

/**
 * A Java based implementation of the {@link ComponentBuilder} interface. 
 */

public class JavaComponentBuilder implements ComponentBuilder {

  // --------------------------------------------------------------------------
  // Implementation of the ComponentBuilder interface
  // --------------------------------------------------------------------------

  public void addComponent (
    final Object superComponent, 
    final Object subComponent, 
    final String name,
    final Object context) 
  {
    // does nothing
  }

  public void startComponent (final Object component, final Object context) 
    throws Exception
  {
    if (component instanceof LifeCycleController) {
      ((LifeCycleController)component).startFc();
    }
  }
}
