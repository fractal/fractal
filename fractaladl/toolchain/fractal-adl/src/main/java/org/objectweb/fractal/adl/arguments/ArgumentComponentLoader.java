/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.arguments;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.ContextMap;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.components.ComponentLoader;

/**
 * Extends {@link ComponentLoader} to take ADL arguments into account.
 */
public class ArgumentComponentLoader extends ComponentLoader {

  @Override
  protected Definition load(final Set<String> loaded, final String name,
      final Map<Object, Object> context) throws ADLException {
    String n = name;
    Map<Object, Object> c = context;

    // if the definition to be loaded is the top level definition
    if (context != null && loaded.isEmpty()) {

      // add String->String tuple of the context with the argument prefix
      // this allows to use the context to specify argument values.

      // it must be performed in two steps: first put new context entries in a
      // temporary map, second, add all entries of the temporary map in the
      // context map. This is to avoid concurrent modification of the context
      // map while iterating over it.
      final Map<Object, Object> addedContext = new HashMap<Object, Object>();
      for (final Map.Entry<Object, Object> entry : context.entrySet()) {
        if (!(entry.getKey() instanceof String)) continue;
        addedContext.put(ArgumentLoader.ARGUMENT_CONTEXT_PREFIX
            + entry.getKey(), entry.getValue());
      }

      context.putAll(addedContext);
    }

    int i = name.indexOf('(');
    if (i != -1) {
      if (!name.endsWith(")")) {
        throw new ADLException(ArgumentErrors.INVALID_ARGUMENT_VALUE_LIST);
      }

      n = name.substring(0, i);
      c = ContextMap.instance(); // new HashMap<Object, Object>();
      c.putAll(context);

      final List<String> args = ArgumentHelper.splitNameList(name.substring(
          i + 1, name.length() - 1));
      if (args == null) {
        throw new ADLException(ArgumentErrors.INVALID_ARGUMENT_VALUE_LIST);
      }
      i = 0;
      boolean argByName = false;
      for (final String arg : args) {
        String argname, argval;
        final int j = arg.indexOf("=>");
        final int k = arg.indexOf("(");
        if (j != -1 && (k == -1 || j < k)) { // if there is a "=>" before a "("
          if (i != 0) {
            // there are other arguments that are not specified by name.
            throw new ADLException(
                ArgumentErrors.INVALID_ARGUMENT_VALUE_SPECIFICATION);
          }
          argByName = true;
          argname = ArgumentLoader.ARGUMENT_CONTEXT_PREFIX
              + arg.substring(0, j);
          argval = ArgumentHelper.unquote(arg.substring(j + 2));
          if (argval == null) {
            throw new ADLException("Syntax error in definition name.");
          }
        } else {
          if (argByName) {
            // there are other arguments that are specified by name.
            throw new ADLException(
                ArgumentErrors.INVALID_ARGUMENT_VALUE_SPECIFICATION);
          }
          argname = ArgumentLoader.ARGUMENT_CONTEXT_PREFIX + (i++);
          argval = ArgumentHelper.unquote(arg);
          if (argval == null) {
            throw new ADLException("Syntax error in definition name.");
          }
        }
        c.put(argname, argval);
      }
    }
    return super.load(loaded, n, c);
  }

  @Override
  protected List<String> parseDefinitions(final String nameList, final Node n)
      throws ADLException {
    final List<String> l = ArgumentHelper.splitNameList(nameList);
    if (l == null) {
      throw new ADLException(ArgumentErrors.INVALID_ARGUMENT_VALUE_LIST, n);
    }
    return l;
  }

  @Override
  public boolean isShared(final String definition) {
    return definition.indexOf('/') != -1 && definition.indexOf('(') == -1;
  }
}
