/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl;

import java.util.Map;

/**
 * A component factory.
 */

public interface Factory {
  
  // TODO: javadoc
  Object newComponentType (String name, Map context) throws ADLException;
  
  /**
   * Creates a new instance of the component whose name is given. This can be
   * done by loading the ADL definition of this name, by compiling it into a
   * set of tasks and by executing these tasks in the proper order.
   *  
   * @param name the name of the component to be created. 
   * @param context optional additional information.
   * @return the component that has been created. The type of this result 
   *      depends on the implementation of this interface: it can be a Fractal
   *      component reference (if this factory creates Fractal components), it 
   *      can be an identifier (if this factory generates source code that will
   *      create components, instead of directly creating components), etc.   
   * @throws ADLException if a problem occurs during the creation of the 
   *      component.
   */
  
  Object newComponent (String name, Map context) throws ADLException;
}
