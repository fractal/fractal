/***
 * Fractal ADL Parser
 * Copyright (C) 2006 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Philippe Merle
 *
 * $Id$
 */

package org.objectweb.fractal.adl.util;

import java.util.Map;

/**
 * Helper methods related to {@link ClassLoader}.
 *
 * @author Philippe Merle
 */

public class ClassLoaderHelper {

  /**
   * Private constructor (uninstantiable class).
   */

  private ClassLoaderHelper () {
  }

  // --------------------------------------------------------------------------
  // Public static helper methods
  // --------------------------------------------------------------------------

  /**
   * Returns the class loader of the class of an object,
   * i.e., object.getClass().getClassLoader().
   *
   * If the class of the object was loaded by the bootstrap class loader
   * then its class loader may be null (@see java.lang.Class#getClassLoader())
   * then returns the system class loader (@see java.lang.ClassLoader#getSystemClassLoader()).
   *
   * @param object an object.
   * 
   * @return the class loader.
   */

  public static ClassLoader getClassLoader(final Object object)
  {
    // Gets the class loader of the class of the object.
    ClassLoader classLoader = object.getClass().getClassLoader();

    // classLoader could be null when object.getClass() was loaded
    // by the bootstrap class loader.
    //
    // Then returns the system class loader.
    //
    if(classLoader == null) {
      classLoader = ClassLoader.getSystemClassLoader();
    }

    return classLoader;
  }

  /**
   * Returns the class loader contained into the map
   * or the class loader of the class of the object.
   *
   * @param object an object.
   * @param map a map.
   * 
   * @return the class loader.
   */

  public static ClassLoader getClassLoader(final Object object, final Object map)
  {
    ClassLoader classLoader = null;

    // Gets the class loader from the map.
    if(map instanceof Map) {
      classLoader = (ClassLoader)((Map)map).get("classloader");
    }

    // If the map is not a Map or the class loader is not contained into the map
    // then gets the class loader of the class of the object.
    if(classLoader == null) {
      classLoader = getClassLoader(object);
    }

    return classLoader;
  }

}
