/***
 * Fractal ADL Parser
 * Copyright (C) 2005 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Ali Erdem Ozcan
 */

package org.objectweb.fractal.adl.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;

/**
 * <p>
 * Utility class for logging messages. Using this class, one can create new
 * loggers or access existing loggers. Each logger have two handlers; the first
 * one logs message on standard output, the other one in a file (this file is
 * shared by every logger). Handler level of each logger can be specified using
 * a system property. If the level of a handler of a logger is not specified, it
 * is inherited from the parent logger.
 * </p>
 * <h2>Configuration</h2>
 * <p>
 * Levels for each logger can be set separately with a
 * {@link System#getProperty(String) system property} whose name is
 * <code>"&lt;name&gt;.&lt;device&gt;.level"</code> where
 * <code>&lt;name&gt;</code> stands for the name of the logger, and
 * <code>&lt;device&gt;</code> stands for either <code>"file"</code> or
 * <code>"console"</code>.
 * </p>
 * <p>
 * Levels of the root logger can be defined using
 * <code>"default.&lt;device&gt;.level"</code> properties. Default level of the
 * root logger are {@link Level#INFO} for the console handler and
 * {@link Level#OFF} for the file handler. This imply that by default, nothing
 * is written in the log file and the log file is not created.
 * </p>
 * <p>
 * The name of the log file can be set with the <code>"loggers.outfile"</code>
 * {@link System#getProperty(String) system property}. Default value is
 * <code>"target/fractaladl-log.txt"</code>.
 * </p>
 */
public final class FractalADLLogManager {
  private FractalADLLogManager() {
  }

  /** Default output file for the logger. */
  public static final String                      OUTPUT_FILE_NAME                    = "target/fractaladl-log.txt";

  /**
   * The name of the {@link System#getProperty(String) system property} used to
   * select the name of the log file.
   */
  public static final String                      OUTPUT_FILE_PROPERTY_NAME           = "loggers.outfile";

  /**
   * The suffix of the {@link System#getProperty(String) system property} used
   * to set the level of the console logger.
   */
  public static final String                      CONSOLE_LEVEL_SUFFIX                = ".console.level";

  /**
   * The suffix of the {@link System#getProperty(String) system property} used
   * to set the level of the file logger.
   */
  public static final String                      FILE_LEVEL_SUFFIX                   = ".file.level";

  /**
   * The name of the {@link System#getProperty(String) system property} for
   * setting the default verbose level for the console handler.
   */
  public static final String                      DEFAULT_CONSOLE_LEVEL_PROPERTY_NAME = "default"
                                                                                          + CONSOLE_LEVEL_SUFFIX;
  /**
   * The name of the {@link System#getProperty(String) system property} for
   * setting the default verbose level for the file handler.
   */
  public static final String                      DEFAULT_FILE_LEVEL_PROPERTY_NAME    = "default"
                                                                                          + FILE_LEVEL_SUFFIX;
  /**
   * The name of logger which is the parent of all loggers created by this log
   * manager.
   */
  public static final String                      ROOT_LOGGER_NAME                    = "fractaladl";

  /** The default level of console handler. */
  public static final Level                       DEFAULT_CONSOLE_LEVEL               = Level.INFO;

  /** The default level of file handler. */
  public static final Level                       DEFAULT_FILE_LEVEL                  = Level.OFF;

  /** A cache containing already created loggers. */
  protected static final Map<String, Logger>      LOGGER_MAP                          = new HashMap<String, Logger>();
  protected static final Map<Logger, LoggerShelf> LOGGER_SHELF_MAP                    = new IdentityHashMap<Logger, LoggerShelf>(); ;

  /**
   * A default logger for logging the step-by-step continuation of the
   * compilation.
   */
  public static final Logger                      STEP;

  static {
    // Get the default console and file verbose levels
    final Level consoleLevel = getLevelFromProperty(
        DEFAULT_CONSOLE_LEVEL_PROPERTY_NAME, DEFAULT_CONSOLE_LEVEL);

    // Find the file verbose level of the logger
    final Level fileLevel = getLevelFromProperty(
        DEFAULT_FILE_LEVEL_PROPERTY_NAME, DEFAULT_FILE_LEVEL);

    // initialize root logger.
    LOGGER_MAP.put(ROOT_LOGGER_NAME, new LoggerShelf(ROOT_LOGGER_NAME,
        consoleLevel, fileLevel).logger);

    STEP = getLogger("step");
  }

  private static Level getLevelFromProperty(final String levelPropertyName,
      final Level defaultLevel) {
    Level level;
    final String levelString = System.getProperty(levelPropertyName);
    if (levelString != null) {
      try {
        level = Level.parse(levelString);
      } catch (final IllegalArgumentException e) {
        System.err.println("Warning: Invalid level value for ["
            + levelPropertyName + "]: \"" + levelString + "\". Defaulting to "
            + defaultLevel + " level");
        level = defaultLevel;
      }
    } else {
      level = defaultLevel;
    }
    return level;
  }

  /**
   * Find or create a logger for a named subsystem that prints its output to the
   * file system and to the console. The verbose level for those output devices
   * can be separately set with a {@link System#getProperty(String) system
   * property} whose name is <code>"&lt;name&gt;.&lt;device&gt;.level"</code>
   * where <code>&lt;name&gt;</code> stands for the name of the logger, and
   * <code>&lt;device&gt;</code> stands for either <code>"file"</code> or
   * <code>"console"</code>. The loggers with no level specified inherit from
   * their parent. The highest level loggers inherit from the default level.
   * 
   * @param name the name of the logger
   * @return a suitable {@link Logger}.
   */
  public static Logger getLogger(final String name) {
    // Look into the logger cache
    Logger logger = LOGGER_MAP.get(name);
    if (logger == null) {
      // If a logger for that name can't be found, create a new logger and
      // register it in the cache.
      logger = new LoggerShelf(name).logger;
      LOGGER_MAP.put(name, logger);
    }
    return logger;
  }

  static void closeLoggers() {
    for (final Logger logger : LOGGER_MAP.values()) {
      for (final Handler h : logger.getHandlers()) {
        h.close();
      }
    }
  }

  /**
   * A {@link Formatter} for printing very simple text messages on the console.
   */
  private static class SimpleConsoleFormatter extends Formatter {

    private final String prefix;

    SimpleConsoleFormatter(final String shortName) {
      prefix = "[" + shortName + "] ";
    }

    @Override
    public String format(final LogRecord record) {
      return prefix + record.getMessage() + "\n";
    }
  }

  static class StdOutConsoleHandler extends StreamHandler {
    StdOutConsoleHandler(final String shortName) {
      super(System.out, new SimpleConsoleFormatter(shortName));
    }

    @Override
    public synchronized void publish(final LogRecord record) {
      super.publish(record);
      flush();
    }
  }

  static class LazyFileOutputStream extends OutputStream {

    OutputStream os = null;
    final File   outputFile;

    LazyFileOutputStream(final File outputFile) {
      this.outputFile = outputFile;
    }

    @Override
    public void write(final int b) throws IOException {
      getOs().write(b);
    }

    @Override
    public synchronized void flush() throws IOException {
      if (os != null) os.flush();
    }

    @Override
    public synchronized void close() throws IOException {
      if (os != null) os.close();
    }

    synchronized OutputStream getOs() {
      if (os == null) {
        outputFile.getParentFile().mkdirs();
        try {
          os = new FileOutputStream(outputFile);
        } catch (final FileNotFoundException e) {
          System.err.println("Can't open log file " + outputFile + " : ");
          e.printStackTrace();
          // set a "do-nothing" output stream to avoid repetition of the
          // message.
          os = new OutputStream() {
            @Override
            public void write(final int b) throws IOException {
              // do nothing.
            }
          };
        }
      }
      return os;
    }
  }

  /**
   * A {@link StreamHandler} that prints on the file system..
   */
  static class SingletonFileHandler extends StreamHandler {
    static OutputStream outputStream;

    static synchronized OutputStream getOutputStream() throws IOException {
      if (outputStream == null) {
        final File outputFile = new File(System.getProperty(
            OUTPUT_FILE_PROPERTY_NAME, OUTPUT_FILE_NAME));
        outputStream = new LazyFileOutputStream(outputFile);
      }
      return outputStream;
    }

    SingletonFileHandler(final String shortName) throws IOException {
      setFormatter(new SimpleConsoleFormatter(shortName));
      setOutputStream(getOutputStream());
    }

    @Override
    public synchronized void publish(final LogRecord record) {
      super.publish(record);
      flush();
    }
  }

  private static class LoggerShelf {
    final Logger logger;

    Handler      consoleHandler;

    Handler      fileHandler;

    LoggerShelf(final String name) {
      final String loggerName = name.equals(ROOT_LOGGER_NAME)
          ? name
          : ROOT_LOGGER_NAME + "." + name;
      // Create the logger and register it in the map
      logger = Logger.getLogger(loggerName);
      LOGGER_SHELF_MAP.put(logger, this);

      // Find its parent
      final Logger parentLogger = logger.getParent();
      LoggerShelf parentLoggerShelf = LOGGER_SHELF_MAP.get(parentLogger);
      if (parentLoggerShelf == null) {
        parentLoggerShelf = new LoggerShelf(parentLogger.getName());
      }

      final Level consoleLevel = getLevelFromProperty(name
          + CONSOLE_LEVEL_SUFFIX, parentLoggerShelf.consoleHandler.getLevel());
      final Level fileLevel = getLevelFromProperty(name + FILE_LEVEL_SUFFIX,
          parentLoggerShelf.fileHandler.getLevel());

      initLogger(consoleLevel, fileLevel, name);
    }

    private void initLogger(final Level consoleLevel, final Level fileLevel,
        final String name) {
      // Default configuration of the logger
      logger.setUseParentHandlers(false);
      // Set the console handler
      consoleHandler = new StdOutConsoleHandler(name);
      consoleHandler.setLevel(consoleLevel);
      logger.addHandler(consoleHandler);
      // Set the file handler
      try {
        fileHandler = new SingletonFileHandler(name);
        fileHandler.setLevel(fileLevel);
        logger.addHandler(fileHandler);
      } catch (final IOException e) {
        System.err.println("Warning: Can't open log file: " + e.getMessage());
      }

      // Set the logger's verbose level to the most verbose one among the
      // console and the file
      logger.setLevel(consoleLevel.intValue() < fileLevel.intValue()
          ? consoleLevel
          : fileLevel);
    }

    LoggerShelf(final String name, final Level consoleLevel,
        final Level fileLevel) {
      final String loggerName = name.equals(ROOT_LOGGER_NAME)
          ? name
          : ROOT_LOGGER_NAME + "." + name;
      // Create the logger and register it in the map
      logger = Logger.getLogger(loggerName);
      LOGGER_SHELF_MAP.put(logger, this);

      initLogger(consoleLevel, fileLevel, name);
    }
  }
}
