/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.coordinates;

/**
 * An AST node interface to define component coordinates. 
 */

public interface Coordinates extends CoordinatesContainer {
  String getName ();
  void setName (String name);
  String getX0 ();
  void setX0 (String x0);
  String getX1 ();
  void setX1 (String x1);
  String getY0 ();
  void setY0 (String y0);
  String getY1 ();
  void setY1 (String y1);
  String getColor ();
  void setColor (String color);
}
