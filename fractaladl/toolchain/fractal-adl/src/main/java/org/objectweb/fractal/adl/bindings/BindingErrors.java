/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.bindings;

import static org.objectweb.fractal.adl.error.ErrorTemplateValidator.validErrorTemplate;

import org.objectweb.fractal.adl.error.ErrorTemplate;

/** {@link ErrorTemplate} group for the bindings package. */
public enum BindingErrors implements ErrorTemplate {

  /** */
  DUPLICATED_BINDING(
      "Redefintion of binding from the client interface \"%s\" (previously defined at \"%s\").",
      "citf", "location"),

  /** */
  MISSING_FROM("Missing client interface in binding."),

  /** */
  MISSING_TO("Missing server interface in binding."),

  /** */
  INVALID_FROM_SYNTAX(
      "Invalid client interface \"%s\" in binding: syntax error.", "itf"),

  /** */
  INVALID_TO_SYNTAX(
      "Invalid server interface \"%s\" in binding: syntax error.", "itf"),

  /** */
  INVALID_ITF_NO_SUCH_COMPONENT("Invalid binding: no such component \"%s\".",
      "compname"),

  /** */
  INVALID_ITF_NO_SUCH_INTERFACE(
      "Invalid binding: component \"%s\" has no interface named \"%s\".",
      "compname", "itfname"),

  /** */
  INVALID_FROM_INTERNAL(
      "Invalid binding: client interface \"%s\" of enclosing composite is not a server interface. Interface defined at %s.",
      "itfName", "location"),

  /** */
  INVALID_FROM_NOT_A_CLIENT(
      "Invalid binding: client interface \"%s\" is not a client interface. Interface defined at %s.",
      "fromItf", "location"),

  /** */
  INVALID_TO_INTERNAL(
      "Invalid binding: server interface \"%s\" of enclosing composite is not a client interface. Interface defined at %s.",
      "itfName", "location"),

  /** */
  INVALID_TO_NOT_A_SERVER(
      "Invalid binding: server interface \"%s\" is not a server interface. Interface defined at %s.",
      "fromItf", "location"),

  /** */
  INVALID_MANDATORY_TO_OPTIONAL(
      "Invalid binding: client interface \"%s\" is mandatory whereas server interface \"%s\" is optional.",
      "fromItf", "toItf"),

  /** */
  INVALID_SIGNATURE(
      "Invalid binding: client interface signature is not compatible with server interface signature.\nClient interface defined at \"%s\".\nServer interface defined at \"%s\".",
      "fromlocation", "tolocation"),

  /** */
  UNBOUND_CLIENT_INTERFACE(
      "Mandatory client interface \"%s\" of component \"%s\" is not bound",
      "itf", "comp"),

  /** */
  UNBOUND_COMPOSITE_SERVER_INTERFACE(
      "Server interface \"%s\" of composite \"%s\" is not bound inside the composite",
      "itf", "comp");

  /** The groupId of ErrorTemplates defined in this enumeration. */
  public static final String GROUP_ID = "BDG";

  private int                id;
  private String             format;

  private BindingErrors(final String format, final Object... args) {
    this.id = ordinal();
    this.format = format;

    assert validErrorTemplate(this, args);
  }

  public int getErrorId() {
    return id;
  }

  public String getGroupId() {
    return GROUP_ID;
  }

  public String getFormatedMessage(final Object... args) {
    return String.format(format, args);
  }

  public String getFormat() {
    return format;
  }
}
