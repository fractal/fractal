/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.error;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Node;

/**
 * An {@link ErrorLocator} that can be used when an error is located by a chain
 * of locator. This is useful to report an error that has been detected in a
 * file that is referenced by other files. The generated
 * {@link #getLocation() location} looks like:
 * 
 * <pre>
 * In definition referenced from location 1
 *                          from location 2
 *                          ...
 *                          from location N
 * location 0
 * </pre>
 * 
 * Instances of this classes should be created by using the
 * {@link #chainLocator(Error, ErrorLocator)} or
 * {@link #chainLocator(ADLException, Node)} static methods. A typical code that
 * uses this locator is:
 * 
 * <pre>
 * try {
 *   d = clientLoader.load(referencedDefinition, context);
 * } catch (final ADLException e) {
 *   chainLocator(e, referencingNode);
 *   throw e;
 * }
 * </pre>
 */
public class ChainedErrorLocator implements ErrorLocator {

  private final ErrorLocator       rootLocator;
  private final List<ErrorLocator> chainedLocations = new ArrayList<ErrorLocator>();

  // ---------------------------------------------------------------------------
  // Factory methods and constructor
  // ---------------------------------------------------------------------------

  /**
   * Update the locator information of the given {@link Error}. Adds the given
   * {@link Locator} in the chain of locator.
   * 
   * @param error an error.
   * @param locator the location that references the location of the given
   *            error.
   */
  public static void chainLocator(final Error error, final ErrorLocator locator) {
    if (error == null)
      throw new IllegalArgumentException("error can't be null");
    if (locator == null)
      throw new IllegalArgumentException("locator can't be null");

    final ErrorLocator l = error.getLocator();
    final ChainedErrorLocator chainedLocator;
    if (l instanceof ChainedErrorLocator) {
      chainedLocator = (ChainedErrorLocator) l;
    } else {
      chainedLocator = new ChainedErrorLocator(l);
      error.setLocator(chainedLocator);
    }
    chainedLocator.chainedLocations.add(locator);
  }

  /**
   * Updates the locator information of the given {@link ADLException}. Adds
   * the location of the given node in the chain of locator. <br>
   * This method is equivalent to
   * 
   * <pre>
   * updateLocator(e.getError(), new NodeErrorLocator(node))
   * </pre>
   * 
   * @param e an exception
   * @param node
   */
  public static void chainLocator(final ADLException e, final Node node) {
    if (e == null)
      throw new IllegalArgumentException("ADLException can't be null");
    if (node == null) throw new IllegalArgumentException("node can't be null");

    chainLocator(e.getError(), new NodeErrorLocator(node));
  }

  protected ChainedErrorLocator(final ErrorLocator rootLocator) {
    this.rootLocator = rootLocator;
  }

  // ---------------------------------------------------------------------------
  // Additional methods
  // ---------------------------------------------------------------------------

  /**
   * Returns the root locator of this chain of error locator (i.e. the actual
   * location of the error reported by this locator).
   * 
   * @return the root locator of this chain of error locator.
   */
  public ErrorLocator getRootLocator() {
    return rootLocator;
  }

  /**
   * Returns the chain of location that references the
   * {@link #getRootLocator() root locator}.
   * 
   * @return A list of error locator
   */
  public List<ErrorLocator> getChainedLocations() {
    return chainedLocations;
  }

  // ---------------------------------------------------------------------------
  // Implementation of the ErrorLocator interface
  // ---------------------------------------------------------------------------

  public String getLocation() {
    String result = "In definition referenced from ";
    String linePrefix = null;
    for (final ErrorLocator locator : chainedLocations) {
      if (linePrefix == null)
        linePrefix = "                         from ";
      else
        result += linePrefix;
      result += locator.getLocation() + "\n";
    }
    if (rootLocator != null) result += rootLocator.getLocation();
    return result;
  }

  public int getBeginColumn() {
    if (rootLocator == null) return chainedLocations.get(0).getBeginColumn();
    return rootLocator.getBeginColumn();
  }

  public int getBeginLine() {
    if (rootLocator == null) return chainedLocations.get(0).getBeginLine();
    return rootLocator.getBeginLine();
  }

  public int getEndColumn() {
    if (rootLocator == null) return chainedLocations.get(0).getEndColumn();
    return rootLocator.getEndColumn();
  }

  public int getEndLine() {
    if (rootLocator == null) return chainedLocations.get(0).getEndLine();
    return rootLocator.getEndLine();
  }

  public String getInputFilePath() {
    if (rootLocator == null) return chainedLocations.get(0).getInputFilePath();
    return rootLocator.getInputFilePath();
  }
}
