/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.timestamp;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.components.ComponentContainer;

/**
 * This loader propagate timestamp values down the AST. It is supposed to be
 * inserted in the loader chain between the component-loader and the parser.
 */
public class TimestampLoader extends AbstractLoader {

  // --------------------------------------------------------------------------
  // Implementation of the Loader interface
  // --------------------------------------------------------------------------

  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    final Definition d = clientLoader.load(name, context);
    if (d instanceof ComponentContainer)
      checkContainer((ComponentContainer) d);
    return d;
  }

  // --------------------------------------------------------------------------
  // Checking methods
  // --------------------------------------------------------------------------

  private void checkContainer(final ComponentContainer container)
      throws ADLException {
    final long ts = Timestamp.getTimestamp(container);
    if (ts == 0) return;

    for (final ComponentContainer subComp : container.getComponents()) {
      Timestamp.setTimestamp(subComp, ts);
      checkContainer(subComp);
    }
  }
}
