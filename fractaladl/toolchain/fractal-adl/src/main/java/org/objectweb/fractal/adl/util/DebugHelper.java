package org.objectweb.fractal.adl.util;

import java.io.IOException;
import java.io.PrintWriter;

import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.xml.XMLWriter;

public class DebugHelper {
  public static void printNode(String context, Node node) {
    PrintWriter pw = new PrintWriter(System.err);
    XMLWriter writer = new XMLWriter(pw);
    pw.println(">>>>>>>>>>> " + context + " (" + node + ")");
    try {
      writer.write(node);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    pw.println("<<<<<<<<<<< " + context + " (" + node + ")");
    pw.flush();
  }

}
