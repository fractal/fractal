/***
 * Fractal ADL Parser
 * Copyright (C) 2005 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.util;

/**
 * Utility class that defines methods to print messages on the standard output
 * at different level. The level of the printer is defined with the
 * <code>"printer.level"</code>
 * {@link System#getProperty(String) system property}.
 */
@Deprecated
public final class Printer {

  private Printer() {
  }

  /**
   * The name of the {@link System#getProperty(String) system property} used to
   * select the printer level.
   */
  public static final String LEVEL_PROPERTY_NAME = "verbose.level";

  /** The debug level value. */
  public static final String DEBUG_LEVEL         = "DEBUG";

  /** The info level value (default value). */
  public static final String INFO_LEVEL          = "INFO";

  /** The error level value. */
  public static final String WARNING_LEVEL       = "WARNING";

  /** The error level value. */
  public static final String ERROR_LEVEL         = "ERROR";

  private static final int   DEBUG               = 0;
  private static final int   INFO                = 1;
  private static final int   WARNING             = 2;
  private static final int   ERROR               = 3;

  private static int         level;

  static {
    final String l = System.getProperty(LEVEL_PROPERTY_NAME, INFO_LEVEL);
    if (DEBUG_LEVEL.equalsIgnoreCase(l))
      level = DEBUG;
    else if (INFO_LEVEL.equalsIgnoreCase(l))
      level = INFO;
    else if (WARNING_LEVEL.equalsIgnoreCase(l))
      level = WARNING;
    else if (ERROR_LEVEL.equalsIgnoreCase(l))
      level = ERROR;
    else
      System.err.println("Warning: invalid \'" + LEVEL_PROPERTY_NAME + "\'");
  }

  /**
   * Prints a debug message, if the printer level allows it.
   * 
   * @param message the message to print.
   */
  public static void debug(final String message) {
    log(DEBUG, message);
  }

  /**
   * Prints an info message, if the printer level allows it.
   * 
   * @param message the message to print.
   */
  public static void info(final String message) {
    log(INFO, message);
  }

  /**
   * Prints a warning message.
   * 
   * @param message the message to print.
   */
  public static void warning(final String message) {
    log(WARNING, message);
  }

  /**
   * Prints an error message.
   * 
   * @param message the message to print.
   */
  public static void error(final String message) {
    log(ERROR, message);
  }

  /**
   * Returns <code>true</code> if debug messages are enable.
   * 
   * @return <code>true</code> if debug messages are enable.
   */
  public static boolean isDebugEnable() {
    return (level <= DEBUG);
  }

  /**
   * Returns <code>true</code> if info messages are enable.
   * 
   * @return <code>true</code> if info messages are enable.
   */
  public static boolean isInfoEnable() {
    return (level <= INFO);
  }

  /**
   * Returns <code>true</code> if warning messages are enable.
   * 
   * @return <code>true</code> if warning messages are enable.
   */
  public static boolean isWarningEnable() {
    return (level <= WARNING);
  }

  private static void log(final int l, final String message) {
    if (level <= l) System.out.println(message);
  }
}
