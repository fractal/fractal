/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.implementations;

import java.io.PrintWriter;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * A Fractal based, static implementation of the {@link ImplementationBuilder} 
 * interface. This implementation produces Java code that uses the Fractal API 
 * to create components. 
 */

public class StaticFractalImplementationBuilder implements ImplementationBuilder {
  
  private Map counters = new WeakHashMap();
  
  // --------------------------------------------------------------------------
  // Implementation of the ImplementationBuilder interface
  // --------------------------------------------------------------------------
  
  public Object createComponent (
    final Object type, 
    final String name,
    final String definition,
    final Object controllerDesc, 
    final Object contentDesc, 
    final Object context) throws Exception
  {
    Integer i = (Integer)counters.get(context);
    if (i == null) {
      i = new Integer(0);
    }
    counters.put(context, new Integer(i.intValue() + 1));
    String id = "C" + i;
    
    PrintWriter pw = (PrintWriter)((Map)context).get("printwriter");
    pw.print("Component ");
    pw.print(id);
    pw.print(" = genericFactory.newFcInstance(");
    pw.print(type);
    pw.print(", \"");
    pw.print(controllerDesc);
    pw.print("\", ");
    if (contentDesc == null) {
      pw.print("null");
    } else if (contentDesc instanceof Object[]) {
      Object[] descs = (Object[])contentDesc;
      pw.print("new Object[] { \"");
      pw.print(descs[0]);
      pw.print("\", ");
      if (descs[1] == null) {
        pw.print("null");
      } else {
        pw.print("\"");
        pw.print(descs[1]);
        pw.print("\"");
      }
      pw.print(" }");
    } else {
      pw.print("\"");
      pw.print(contentDesc);
      pw.print("\"");
    }
    pw.println(");");

    pw.print("try { Fractal.getNameController(");
    pw.print(id);
    pw.print(").setFcName(\"");
    pw.print(name);
    pw.println("\"); } catch (NoSuchInterfaceException ignored) { }");
    
    return id;
  }
}
