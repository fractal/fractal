/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 *
 * Contributor: Matthieu Leclercq (externalize interface loading)
 */

package org.objectweb.fractal.adl.bindings;

import static org.objectweb.fractal.adl.types.TypeInterfaceUtil.isClient;
import static org.objectweb.fractal.adl.types.TypeInterfaceUtil.isMandatory;
import static org.objectweb.fractal.adl.types.TypeInterfaceUtil.isServer;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.error.NodeErrorLocator;
import org.objectweb.fractal.adl.interfaces.IDLLoader;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.types.TypeInterface;
import org.objectweb.fractal.adl.types.TypeInterfaceUtil;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;

/**
 * An extended {@link BindingLoader} for components with typed interfaces. In
 * addition to the checks performed by {@link BindingLoader}, this
 * implementation checks that from interfaces are client interfaces, that to
 * interfaces are server interfaces, and that the signatures and contingency of
 * client and server interfaces are compatible.
 */

public class TypeBindingLoader extends BindingLoader {

  // --------------------------------------------------------------------------
  // Client interfaces
  // --------------------------------------------------------------------------

  /** The name of the {@link #interfaceCodeLoaderItf} client interface. */
  public static final String INTERFACE_LOADER_BINDING = "interface-loader";

  /** The optional client interface used to load interface signatures. */
  public IDLLoader           interfaceCodeLoaderItf;

  // --------------------------------------------------------------------------
  // Overridden methods
  // --------------------------------------------------------------------------

  @Override
  protected void checkBinding(final Binding binding, final Interface fromItf,
      final String fromCompName, final String fromItfName,
      final Interface toItf, final String toCompName, final String toItfName,
      final Map<Object, Object> context) throws ADLException {
    if (fromItf instanceof TypeInterface && toItf instanceof TypeInterface) {
      final TypeInterface cItf = (TypeInterface) fromItf;
      final TypeInterface sItf = (TypeInterface) toItf;
      if (fromCompName.equals("this")) {
        if (!isServer(cItf)) {
          throw new ADLException(BindingErrors.INVALID_FROM_INTERNAL, binding,
              fromItfName, new NodeErrorLocator(cItf));
        }
      } else {
        if (!isClient(cItf)) {
          throw new ADLException(BindingErrors.INVALID_FROM_NOT_A_CLIENT,
              binding, binding.getFrom(), new NodeErrorLocator(cItf));
        }
      }
      if (toCompName.equals("this")) {
        if (!isClient(sItf)) {
          throw new ADLException(BindingErrors.INVALID_TO_INTERNAL, binding,
              toItfName, new NodeErrorLocator(cItf));
        }
      } else {
        if (!isServer(sItf)) {
          throw new ADLException(BindingErrors.INVALID_TO_NOT_A_SERVER,
              binding, binding.getTo(), new NodeErrorLocator(sItf));
        }
      }

      if (isMandatory(cItf) && !isMandatory(sItf)) {
        throw new ADLException(BindingErrors.INVALID_MANDATORY_TO_OPTIONAL,
            binding, binding.getFrom(), binding.getTo());
      }
      if (interfaceCodeLoaderItf != null) {
        Object cClass = null;
        Object sClass = null;
        try {
          cClass = interfaceCodeLoaderItf.loadInterface(cItf.getSignature(),
              context);
          sClass = interfaceCodeLoaderItf.loadInterface(sItf.getSignature(),
              context);
        } catch (final ADLException e) {
          // ignore
        }
        if ((cClass instanceof Class) && (sClass instanceof Class)
            && !((Class<?>) cClass).isAssignableFrom((Class<?>) sClass)) {
          throw new ADLException(BindingErrors.INVALID_SIGNATURE, binding,
              new NodeErrorLocator(fromItf), new NodeErrorLocator(toItf));
        }
      }
    }
  }

  @Override
  protected Interface getInterface(final String compName, final String itfName,
      final Node sourceNode, final Map<String, Map<String, Interface>> itfMap)
      throws ADLException {
    try {
      return super.getInterface(compName, itfName, sourceNode, itfMap);
    } catch (final ADLException e) {
      // The interface can't be found. Either it is a collection interface (not
      // supported by super implementation) or it is really an error.

      final Map<String, Interface> compItfs = itfMap.get(compName);
      if (compItfs == null) {
        // it is an error since the component does not exist. re-throw 'e'
        throw e;
      }

      for (final Map.Entry<String, Interface> entry : compItfs.entrySet()) {
        final String n = entry.getKey();
        final Interface itf = entry.getValue();
        if (TypeInterfaceUtil.isCollection(itf) && itfName.startsWith(n)) {
          return itf;
        }
      }

      // no collection interface can be found, re-throw 'e'
      throw e;
    }
  }

  // ------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ------------------------------------------------------------------------

  @Override
  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (INTERFACE_LOADER_BINDING.equals(s)) {
      interfaceCodeLoaderItf = (IDLLoader) o;
    } else {
      super.bindFc(s, o);
    }
  }

  @Override
  public String[] listFc() {
    final String[] superList = super.listFc();
    final String[] list = new String[superList.length + 1];
    list[0] = INTERFACE_LOADER_BINDING;
    System.arraycopy(superList, 0, list, 1, superList.length);
    return list;
  }

  @Override
  public Object lookupFc(final String s) throws NoSuchInterfaceException {
    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (INTERFACE_LOADER_BINDING.equals(s)) {
      return interfaceCodeLoaderItf;
    } else {
      return super.lookupFc(s);
    }
  }

  @Override
  public void unbindFc(final String s) throws IllegalBindingException,
      NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (INTERFACE_LOADER_BINDING.equals(s)) {
      interfaceCodeLoaderItf = null;
    } else {
      super.unbindFc(s);
    }
  }

}
