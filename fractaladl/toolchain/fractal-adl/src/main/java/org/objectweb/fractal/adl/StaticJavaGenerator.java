/***
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A class to generate java source code of instantiation of a set of component
 * definition.
 */
public class StaticJavaGenerator {

  /** The prefix that delimit comments in ADL list file. */
  public static final String    COMMENT_PREFFIX       = "#";

  /**
   * The suffix appended to ADL definition name to build the name of the
   * generated class.
   */
  public static final String    CLASSNAME_SUFFIX      = "JavaFactory";

  /** The option flag used to specify the output directory. */
  public static final String    OUTPUT_PARAM_NAME     = "o";

  /**
   * the option flag used to specify the name of the file containing a list of
   * ADL defintion to generate.
   */
  public static final String    INPUT_LIST_PARAM_NAME = "adls";

  protected Factory             factory;
  protected Map<Object, Object> factoryContext;
  protected File                outputDirectory;
  protected File                adlsFile              = null;
  protected List<String>        adlsList              = new ArrayList<String>();

  /**
   * Constructor.
   * 
   * @param args command line arguments
   * @throws ADLException if the FractalADL factory can't be instantiated.
   * @throws InvalidCommandLineException If the command line arguments are
   *             invalid.
   */
  public StaticJavaGenerator(final String... args) throws ADLException,
      InvalidCommandLineException {
    init(parseArgsAsMap(args));
  }

  /**
   * Constructor.
   * 
   * @param sourceClassLoader the class loader passed to the fractalADL factory.
   * @param args command line arguments
   * @throws ADLException if the FractalADL factory can't be instantiated.
   * @throws InvalidCommandLineException If the command line arguments are
   *             invalid.
   */
  public StaticJavaGenerator(final ClassLoader sourceClassLoader,
      final String... args) throws ADLException, InvalidCommandLineException {
    init(parseArgsAsMap(args));
    factoryContext.put("classloader", sourceClassLoader);
  }

  protected void init(final Map<String, String> context)
      throws InvalidCommandLineException, ADLException {
    final String outDirName = context.get(OUTPUT_PARAM_NAME);
    if (outDirName != null) {
      outputDirectory = new File(outDirName);
      if (!outputDirectory.exists()) {
        throw new InvalidCommandLineException("Invalid output directory \""
            + outputDirectory + "\" does not exist.");
      }
      if (!outputDirectory.isDirectory()) {
        throw new InvalidCommandLineException("Invalid output directory \""
            + outputDirectory + "\" is not a dirtectory.");
      }
    } else {
      outputDirectory = new File(".");
    }

    final String adlsFileName = context.get(INPUT_LIST_PARAM_NAME);
    if (adlsFileName != null) {
      adlsFile = new File(adlsFileName);
      if (!adlsFile.exists()) {
        throw new InvalidCommandLineException("Invalid ADL list \"" + adlsFile
            + "\" does not exist.");
      }
    }

    factoryContext = new HashMap<Object, Object>(context);

    String factoryDefinition = context.get(Launcher.FACTORY_PROPERTY_NAME);
    if (factoryDefinition == null)
      factoryDefinition = FactoryFactory.DEFAULT_FACTORY;

    String backend = context.get(FactoryFactory.BACKEND_PROPERTY_NAME);
    if (backend == null) backend = FactoryFactory.STATIC_JAVA_BACKEND;

    factory = FactoryFactory.getFactory(factoryDefinition, backend,
        new HashMap<Object, Object>(factoryContext));
  }

  /**
   * Run the generation of the ADL definitions.
   * 
   * @throws GenerationException if an error occurs.
   */
  public void generate() throws GenerationException {
    if (adlsFile != null) {
      try {
        final BufferedReader reader = new BufferedReader(new FileReader(
            adlsFile));
        String line;

        while ((line = reader.readLine()) != null) {
          if (line.startsWith(COMMENT_PREFFIX) || line.trim().length() == 0)
            continue;

          final int index = line.indexOf(COMMENT_PREFFIX);
          if (index != -1) line = line.substring(0, index).trim();

          generate(line);
        }
        reader.close();
      } catch (final IOException e) {
        throw new GenerationException("Can't read ADL list", e);
      }
    }

    for (final String adl : adlsList) {
      generate(adl);
    }
  }

  protected void generate(final String adlName) throws GenerationException {
    final File outputFile = getOutputFile(adlName);
    final PrintWriter pw;
    try {
      pw = new PrintWriter(new FileWriter(outputFile));
    } catch (final IOException e) {
      throw new GenerationException("Can't write to file \"" + outputFile
          + "\".", e);
    }

    final StringBuilder header = new StringBuilder();
    header.append("// THIS FILE HAS BEEN GENERATED BY THE FRACTAL ADL "
        + "COMPILER.\n");
    header.append("// DO NOT EDIT\n\n");
    generateHeader(adlName, header);
    pw.println(header);
    final Map<Object, Object> context = new HashMap<Object, Object>(
        factoryContext);
    context.put("printwriter", pw);
    final Object result;
    try {
      result = factory.newComponent(adlName, context);
    } catch (final ADLException e) {
      throw new GenerationException("An error occurs while compiling \""
          + adlName + "\".", e);
    }
    final StringBuilder footer = new StringBuilder();
    generateFooter(adlName, result, footer);
    pw.print(footer);
    pw.close();
  }

  protected void generateHeader(final String adlName, final StringBuilder header) {
    final String className = getClassName(adlName);
    header.append("// instantiation code for ").append(adlName).append("\n");

    // Class header (specify package if needed)
    final int index = className.lastIndexOf(".");
    if (index != -1) {
      header.append("package ").append(className.substring(0, index)).append(
          ";\n\n");
      header.append("public class ").append(className.substring(index + 1));
    } else {
      header.append("public class ").append(className);
    }
    header.append(" implements ").append(JavaFactory.class.getName()).append(
        "{\n\n");

    // newComponent method
    header.append("  public Object newComponent() throws Exception {\n");
  }

  protected void generateFooter(final String adlName, final Object result,
      final StringBuilder footer) {
    footer.append("    return ").append(result).append(";\n");
    footer.append("  }\n");
    footer.append("}\n");
  }

  protected String getClassName(String adlName) {
    final int index = adlName.indexOf('(');
    if (index != -1) adlName = adlName.substring(0, index);
    return adlName + CLASSNAME_SUFFIX;
  }

  protected File getOutputFile(final String adlName) {
    final File outputFile = new File(outputDirectory, getClassName(adlName)
        .replace('.', File.separatorChar)
        + ".java");
    outputFile.getParentFile().mkdirs();
    return outputFile;
  }

  /**
   * Main entry point.
   * 
   * @param args command line arguments.
   */
  public static void main(final String... args) {
    try {
      new StaticJavaGenerator(args).generate();
    } catch (final ADLException e) {
      System.err
          .println("An error occurs while instantiating the FractalADL factory.");
      e.printStackTrace();
      System.exit(1);
    } catch (final InvalidCommandLineException e) {
      System.err.println(e.getMessage());
      System.exit(1);
    } catch (final GenerationException e) {
      if (e.getCause() instanceof ADLException) {
        System.err.println(e.getMessage());
        System.err.println(((ADLException) e.getCause()).getError());
      } else {
        Throwable t = e;
        do {
          System.err.println(t.getLocalizedMessage());
          t = t.getCause();
        } while (t != null);
      }
      System.exit(1);
    }
  }

  protected Map<String, String> parseArgsAsMap(final String[] args)
      throws InvalidCommandLineException {
    final Map<String, String> result = new HashMap<String, String>();
    for (final String currentArg : args) {
      if (currentArg.startsWith(Launcher.PARAMETER_NAME_TRAILING_CHAR)) {
        /* if it is a "-name:value" parameter */
        final int index = currentArg
            .indexOf(Launcher.PARAMETER_NAME_VALUE_SEPARATOR_CHAR);
        if (index != -1) {
          final String paramName = currentArg.substring(
              Launcher.PARAMETER_NAME_TRAILING_CHAR.length(), index);
          final String paramValue = currentArg.substring(index
              + Launcher.PARAMETER_NAME_VALUE_SEPARATOR_CHAR.length());
          result.put(paramName, paramValue);
        } else {
          parseError();
        }
      } else {
        adlsList.add(currentArg);
      }
    }
    return result;
  }

  protected static void parseError() throws InvalidCommandLineException {
    final StringBuilder sb = new StringBuilder();
    sb.append("StaticJavaGenerator: generate Java source code that"
        + " instantiates ADL definitions\n");
    sb.append("Usage: StaticJavaGenerator [DEFINITION...]\n\n");
    sb.append("  where DEFINITION is a list of ADL definition to be"
        + " generated.\n\n");
    sb.append("Valid options:\n");
    sb.append("  -o=PATH     : Specify the directory into which generated"
        + " files are placed\n");
    sb.append("  -adls=PATH  : Specify a file that contains a list of ADL"
        + " definition to compile.\n");
    sb.append("                one definition per line");
    throw new InvalidCommandLineException(sb.toString());
  }

  /** Exception thrown when the command line arguments are invalid. */
  public static class InvalidCommandLineException extends Exception {

    InvalidCommandLineException(final String message) {
      super(message);
    }
  }

  /**
   * Exception thrown when the fractalADL factory reports an error while
   * generating a file.
   */
  public static class GenerationException extends Exception {

    GenerationException() {
      super();
    }

    GenerationException(final String message, final Throwable cause) {
      super(message, cause);
    }

    GenerationException(final Throwable cause) {
      super(cause);
    }

    GenerationException(final String message) {
      super(message);
    }
  }
}
