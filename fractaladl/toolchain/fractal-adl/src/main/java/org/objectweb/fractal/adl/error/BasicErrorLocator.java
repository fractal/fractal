/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.error;

import java.io.File;

/**
 * Basic {@link ErrorLocator} implementation.
 */
public class BasicErrorLocator implements ErrorLocator {

  private final int    beginLine;
  private final int    endLine;
  private final int    beginColumn;
  private final int    endColumn;
  private final String inputFilePath;

  // ---------------------------------------------------------------------------
  // Constructors
  // ---------------------------------------------------------------------------

  /**
   * Creates a {@link BasicErrorLocator}.
   * 
   * @param file a file
   * @param beginLine the begin line
   * @param beginColumn the begin column
   */
  public BasicErrorLocator(final File file, final int beginLine,
      final int beginColumn) {
    this(file.getPath(), beginLine, beginColumn);
  }

  /**
   * Creates a {@link BasicErrorLocator}.
   * 
   * @param inputFilePath the path of the input file
   * @param beginLine the begin line
   * @param beginColumn the begin column
   */
  public BasicErrorLocator(final String inputFilePath, final int beginLine,
      final int beginColumn) {
    this(inputFilePath, beginLine, -1, beginColumn, -1);
  }

  /**
   * Creates a {@link BasicErrorLocator}.
   * 
   * @param inputFilePath the path of the input file
   * @param beginLine the begin line
   * @param endLine the end line
   * @param beginColumn the begin column
   * @param endColumn the end column
   */
  public BasicErrorLocator(final String inputFilePath, final int beginLine,
      final int endLine, final int beginColumn, final int endColumn) {
    this.beginLine = beginLine;
    this.endLine = endLine;
    this.beginColumn = beginColumn;
    this.endColumn = endColumn;
    this.inputFilePath = inputFilePath;
  }

  // ---------------------------------------------------------------------------
  // Implementation of the ErrorLocator interface
  // ---------------------------------------------------------------------------

  public int getBeginLine() {
    return beginLine;
  }

  public int getEndLine() {
    return endLine;
  }

  public int getBeginColumn() {
    return beginColumn;
  }

  public int getEndColumn() {
    return endColumn;
  }

  public String getInputFilePath() {
    return inputFilePath;
  }

  public String getLocation() {
    String location = inputFilePath;
    if (beginLine != -1) {
      location += ":" + beginLine;
      if (beginColumn != -1) {
        location += "-" + beginColumn;
      }
    }

    return location;
  }

  // ---------------------------------------------------------------------------
  // Overridden methods
  // ---------------------------------------------------------------------------

  @Override
  public String toString() {
    return getLocation();
  }
}
