/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2006 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributor: Philippe Merle, Matthieu Leclercq
 *
 * ---------------------------------------------------------------------------
 * $Id$
 * ---------------------------------------------------------------------------
 */

package org.objectweb.fractal.adl;

import org.objectweb.fractal.adl.error.Error;
import org.objectweb.fractal.adl.error.ErrorLocator;
import org.objectweb.fractal.adl.error.ErrorTemplate;
import org.objectweb.fractal.adl.error.GenericErrors;
import org.objectweb.fractal.adl.error.NodeErrorLocator;

/**
 * Thrown when an error occurs during the analysis of an ADL definition.
 */
public class ADLException extends Exception {

  private final Error error;

  /**
   * Constructs a new {@link ADLException}.
   * 
   * @param template the error templates.
   * @param args the arguments for the formated message (see
   *            {@link ErrorTemplate#getFormatedMessage(Object...)}).
   */
  public ADLException(final ErrorTemplate template, final Object... args) {
    this(template, null, null, args);
  }

  /**
   * Constructs a new {@link ADLException}.
   * 
   * @param template the error templates.
   * @param node the error location.
   * @param args the arguments for the formated message (see
   *            {@link ErrorTemplate#getFormatedMessage(Object...)}).
   */
  public ADLException(final ErrorTemplate template, final Node node,
      final Object... args) {
    this(template, new NodeErrorLocator(node), null, args);
  }

  /**
   * Constructs a new {@link ADLException}.
   * 
   * @param template the error templates.
   * @param locator the error location. May be <code>null</code>.
   * @param args the arguments for the formated message (see
   *            {@link ErrorTemplate#getFormatedMessage(Object...)}).
   */
  public ADLException(final ErrorTemplate template, final ErrorLocator locator,
      final Object... args) {
    this(template, locator, null, args);
  }

  /**
   * Constructs a new {@link ADLException}.
   * 
   * @param template the error templates.
   * @param cause the cause of this error. May be <code>null</code>.
   * @param args the arguments for the formated message (see
   *            {@link ErrorTemplate#getFormatedMessage(Object...)}).
   */
  public ADLException(final ErrorTemplate template, final Throwable cause,
      final Object... args) {
    this(template, null, cause, args);
  }

  /**
   * Constructs a new {@link ADLException}.
   * 
   * @param template the error templates.
   * @param locator the error location. May be <code>null</code>.
   * @param cause the cause of this error. May be <code>null</code>.
   * @param args the arguments for the formated message (see
   *            {@link ErrorTemplate#getFormatedMessage(Object...)}).
   */
  public ADLException(final ErrorTemplate template, final ErrorLocator locator,
      final Throwable cause, final Object... args) {
    this(new Error(template, locator, cause, args));
  }

  /**
   * Constructs a new {@link ADLException}.
   * 
   * @param error the error reported by this exception
   */
  public ADLException(final Error error) {
    super(error.getCause());
    this.error = error;
  }

  /**
   * Constructs a new {@link ADLException}. <br>
   * This constructor is deprecated, use {@link #ADLException(Error)} or one of
   * its variant instead.
   * 
   * @param msg a detail message.
   */
  @Deprecated
  public ADLException(final String msg) {
    this(msg, null, null);
  }

  /**
   * Constructs a new {@link ADLException}. <br>
   * This constructor is deprecated, use {@link #ADLException(Error)} or one of
   * its variant instead.
   * 
   * @param msg a detail message.
   * @param src where the error is located.
   */
  @Deprecated
  public ADLException(final String msg, final Node src) {
    this(msg, src, null);
  }

  /**
   * Constructs a new {@link ADLException}. <br>
   * This constructor is deprecated, use {@link #ADLException(Error)} or one of
   * its variant instead.
   * 
   * @param msg a detail message.
   * @param e the exception that caused this exception.
   */
  @Deprecated
  public ADLException(final String msg, final Exception e) {
    this(msg, null, e);
  }

  /**
   * Constructs a new {@link ADLException}. <br>
   * This constructor is deprecated, use {@link #ADLException(Error)} or one of
   * its variant instead.
   * 
   * @param msg a detail message.
   * @param src where the error is located.
   * @param e the exception that caused this exception.
   */
  @Deprecated
  public ADLException(final String msg, final Node src, final Exception e) {
    super(e);
    error = new Error(GenericErrors.GENERIC_ERROR, (src == null)
        ? null
        : new NodeErrorLocator(src), e, msg);
  }

  /**
   * This method is deprecated, use {@link #getError()} and
   * {@link Error#getLocator()} instead.
   * 
   * @return the source node of the error. May be <code>null</code>.
   */
  @Deprecated
  public Node getSrc() {
    if (error.getLocator() instanceof NodeErrorLocator)
      return ((NodeErrorLocator) error.getLocator()).getNode();
    else
      return null;
  }

  /**
   * Returns the {@link Error} object reported by this exception.
   * 
   * @return the {@link Error} object reported by this exception.
   */
  public Error getError() {
    return error;
  }

  @Override
  public String getMessage() {
    return error.toString();
  }
}
