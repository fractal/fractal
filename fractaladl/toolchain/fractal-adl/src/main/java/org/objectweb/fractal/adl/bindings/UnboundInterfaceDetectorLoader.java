/***
 * Fractal ADL Parser
 * Copyright (C) 2005-2006 STMicroelectronics, INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Ali Erdem Ozcan
 *
 * Contributor: Philippe Merle, Matthieu Leclercq
 */

package org.objectweb.fractal.adl.bindings;

import static org.objectweb.fractal.adl.types.TypeInterfaceUtil.isClient;
import static org.objectweb.fractal.adl.types.TypeInterfaceUtil.isCollection;
import static org.objectweb.fractal.adl.types.TypeInterfaceUtil.isMandatory;
import static org.objectweb.fractal.adl.types.TypeInterfaceUtil.isServer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.NodeErrorLocator;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.adl.interfaces.Interface;
import org.objectweb.fractal.adl.interfaces.InterfaceContainer;

/**
 * A {@link org.objectweb.fractal.adl.Loader} to check
 * {@link org.objectweb.fractal.adl.bindings.Binding} nodes in definitions. This
 * loader checks that all mandatory client interfaces are bound.
 */
public class UnboundInterfaceDetectorLoader extends AbstractLoader {

  // ---------------------------------------------------------------------------
  // Implementation of the Loader interface
  // ---------------------------------------------------------------------------

  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    // Load the definition with the next loader.
    final Definition d = clientLoader.load(name, context);

    // Check the loaded definition.
    if (d instanceof ComponentContainer)
      checkDefinition((ComponentContainer) d);

    return d;
  }

  // ---------------------------------------------------------------------------
  // Checking methods
  // ---------------------------------------------------------------------------

  protected void checkDefinition(final ComponentContainer definition)
      throws ADLException {
    // This map will store all the components having some unbound mandatory
    // client interfaces.
    // The keys of the map are Component nodes.
    // The values of the map are List of there unbound interfaces.
    final Map<Component, ComponentBindingInfo> bindingInfos = new IdentityHashMap<Component, ComponentBindingInfo>();

    // Check the definition and feed the map of unbound mandatory client
    // interfaces.
    checkNode(definition, bindingInfos, new HashSet<Component>());

    // Iterate on all the components having some unbound mandatory client
    // interfaces.
    for (final Map.Entry<Component, ComponentBindingInfo> entry : bindingInfos
        .entrySet()) {
      if (entry.getValue().unboundInterfaces == null) continue;

      final Component comp = entry.getKey();
      for (final String itfNode : entry.getValue().unboundInterfaces) {
        // TODO should throw a list of error (containing every unbound
        // interfaces).
        throw new ADLException(BindingErrors.UNBOUND_CLIENT_INTERFACE, comp,
            itfNode, comp.getName());
      }
    }
  }

  protected void checkNode(final ComponentContainer node,
      final Map<Component, ComponentBindingInfo> bindingInfos,
      final Set<Component> visitedComponents) throws ADLException {
    // We do this only for composite components.
    if (isPrimitive(node)) return;

    final Map<String, Binding> bindings = new HashMap<String, Binding>();
    if (node instanceof BindingContainer) {
      for (final Binding binding : ((BindingContainer) node).getBindings()) {
        bindings.put(binding.getFrom(), binding);
      }
    }

    // for each sub component of the composite
    for (final Component comp : node.getComponents()) {
      if (!(comp instanceof InterfaceContainer)) continue;
      // retrieve or create bindingInfo object.
      ComponentBindingInfo bindingInfo = bindingInfos.get(comp);
      if (bindingInfo == null) {
        bindingInfo = new ComponentBindingInfo();
        bindingInfos.put(comp, bindingInfo);
      }

      checkSubComponent(comp, bindings, bindingInfo);
    }

    checkComposite(node, bindings);

    for (final Component comp : node.getComponents()) {
      checkNode(comp, bindingInfos, visitedComponents);
    }
  }

  protected void checkSubComponent(final Component component,
      final Map<String, Binding> bindings,
      final ComponentBindingInfo bindingInfo) throws ADLException {
    // for each interface of the sub-component.
    for (final Interface itf : ((InterfaceContainer) component).getInterfaces()) {
      // Only check mandatory client interfaces.
      if (!isClient(itf) || !isMandatory(itf)) continue;

      checkClientInterface(component, itf, itf.getName(), bindingInfo, bindings);
    }
  }

  protected void checkClientInterface(final Component component,
      final Interface itf, final String itfName,
      final ComponentBindingInfo bindingInfo,
      final Map<String, Binding> bindings) throws ADLException {
    final Binding binding = findBinding(bindings, itf, component.getName(),
        itfName);

    if (binding != null) {
      // if found, add it in bound interface.
      final Binding previousBinding = bindingInfo.addBoundInterface(
          getFromItfName(binding), binding);

      // and checks for duplication. (This checks is useful in case of
      // shared component where one of its client interface is bound twice
      // in two different composite.
      if (previousBinding != null) {
        throw new ADLException(BindingErrors.DUPLICATED_BINDING, binding,
            binding.getFrom(), new NodeErrorLocator(previousBinding));
      }
    } else {
      // binding not found, add itf in list of unbound interface.
      bindingInfo.addUnboundInterface(itfName);
    }
  }

  protected void checkComposite(final ComponentContainer composite,
      final Map<String, Binding> bindings) throws ADLException {
    // Verification of the unbound internal client interfaces of this composite
    if (composite instanceof InterfaceContainer) {
      for (final Interface itf : ((InterfaceContainer) composite)
          .getInterfaces()) {
        // Only check mandatory server interface but not a controller interface.
        if (!isServer(itf) || !isMandatory(itf)
            || itf.getName().endsWith("controller")
            || itf.getName().equals("component")
            || itf.getName().equals("factory")) continue;
        checkCompositeServerInterface(composite, itf, itf.getName(), bindings);
      }
    }
  }

  protected void checkCompositeServerInterface(
      final ComponentContainer composite, final Interface itf,
      final String itfName, final Map<String, Binding> bindings)
      throws ADLException {
    // In this case, this interface must be bounded
    if (findBinding(bindings, itf, "this", itfName) == null) {
      throw new ADLException(BindingErrors.UNBOUND_COMPOSITE_SERVER_INTERFACE,
          itf, itf.getName(), getName(composite));
    }
  }

  protected Binding findBinding(final Map<String, Binding> bindings,
      final Interface itf, final String compName, final String itfName) {
    final String bindingName = compName + "." + itfName;

    if (isCollection(itf)) {
      // This is a collection interface then it is bound if
      // there is at least one binding starting with the interface name.
      for (final Map.Entry<String, Binding> binding : bindings.entrySet()) {
        if (binding.getKey().startsWith(bindingName))
          return binding.getValue();
      }
      return null;
    } else {
      return bindings.get(bindingName);
    }
  }

  // TODO Use an helper class.
  protected String getFromItfName(final Binding binding) {
    final int i = binding.getFrom().indexOf('.');
    return binding.getFrom().substring(i + 1);
  }

  // TODO Use an helper class.
  protected boolean isPrimitive(final ComponentContainer node) {
    return (node instanceof ImplementationContainer)
        && ((ImplementationContainer) node).getImplementation() != null;
  }

  // TODO use helper class.
  protected String getName(final ComponentContainer composite) {
    if (composite instanceof Definition) {
      return ((Definition) composite).getName();
    }

    if (composite instanceof Component) {
      return ((Component) composite).getName();
    }

    return null;
  }

  protected static final class ComponentBindingInfo {
    Map<String, Binding> boundInterfaces   = null;
    Set<String>          unboundInterfaces = null;

    protected Binding addBoundInterface(final String itf,
        final Binding bindingNode) {
      // instantiate boundInterfaces Map lazily
      if (boundInterfaces == null)
        boundInterfaces = new HashMap<String, Binding>();

      // remove itf from set of unbound interfaces (if any)
      if (unboundInterfaces != null) {
        unboundInterfaces.remove(itf);
      }

      // add itf in Map of bound interfaces.
      // returns the previous binding node that was associated with the given
      // interface (if any). This is useful for error reporting.
      return boundInterfaces.put(itf, bindingNode);
    }

    protected void addUnboundInterface(final String itf) {
      // if itf is already bound, do not add is in unboundInterfaces set.
      if (boundInterfaces != null && boundInterfaces.containsKey(itf)) return;

      // instantiate unboundInterfaces Set lazily
      if (unboundInterfaces == null) unboundInterfaces = new HashSet<String>();

      // add interface in set.
      unboundInterfaces.add(itf);
    }
  }
}
