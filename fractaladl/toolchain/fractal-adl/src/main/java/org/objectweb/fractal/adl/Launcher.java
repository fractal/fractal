/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributors: Alessio Pace, Philippe Merle
 *
 * $Id$
 */

package org.objectweb.fractal.adl;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.util.Fractal;

/**
 * A class to launch a component from its definition from the command line.
 * Usage: Launcher [-java|-fractal] [ (-paramName=paramValue)* ]
 * &lt;definition&gt; [ &lt;itf&gt; ] where &lt;definition&gt; is the name of
 * the component to be instantiated and started,and &lt;itf&gt; is the name of
 * its Runnable interface, if it has one.
 * 
 * @author Eric Bruneton contributors: Alessio Pace, Philippe Merle
 */
public class Launcher {

  /** The prefix of parameter name. */
  public static final String    PARAMETER_NAME_TRAILING_CHAR        = "-";

  /** The separator between a parameter name and its value. */
  public static final String    PARAMETER_NAME_VALUE_SEPARATOR_CHAR = "=";

  /**
   * The Fractal provider. It is passed all along and used by
   * <tt>Fractal.getBootstrapComponent(Map hints);</tt> in
   * <tt>fractal-api</tt>.
   */
  public static final String    FRACTAL_PROVIDER_PROPERTY_NAME      = "fractal.provider";

  /**
   * If <strong>false</strong>, stopFc() will be eventually invoked (default
   * value is <em>true</em>).
   */
  public static final String    DAEMON_PROPERTY_NAME                = FactoryFactory.FRACTALADL_PARAMETER_PREFIX
                                                                        + "daemon";
  /** Default value of the {@link #DAEMON_PROPERTY_NAME} property. */
  public static final String    DAEMON_PROPERTY_DEFAULT_VALUE       = "true";

  /**
   * The FractalADL factory to compile components.
   */
  public static final String    FACTORY_PROPERTY_NAME               = FactoryFactory.FRACTALADL_PARAMETER_PREFIX
                                                                        + "factory";

  /**
   * The component definition to compile.
   */
  public static final String    COMPONENT_DEFINITION_PROPERTY_NAME  = FactoryFactory.FRACTALADL_PARAMETER_PREFIX
                                                                        + "definition";

  /**
   * The Runnable interface name (default is <tt>run</tt>).
   */
  public static final String    RUNNABLE_INTERFACE_PROPERTY_NAME    = FactoryFactory.FRACTALADL_PARAMETER_PREFIX
                                                                        + "itf";

  /**
   * The default Runnable interface name to look for ("r" or "run" to use?).
   */
  public static final String    RUNNABLE_INTERFACE_DEFAULT_VALUE    = "r";

  /**
   * The FractalADL Factory which will be used to compile component definitions.
   */
  protected Factory             factory;

  /**
   * The parameters that were used upon factory instantiation.
   */
  protected Map<Object, Object> compilerContext;

  /**
   * Instantiate a <tt>Launcher</tt> which will instantiate a FractalADL
   * Factory defined by the <tt>factoryDefinition</tt> and
   * <tt>backendDefinition</tt> parameters passed to this constructor.
   * 
   * @param factoryDefinition Factory definition
   * @param backendDefinition Backend definition
   * @param compilerCreationContext Map of parameters to create the Factory (can
   *            be an empty Map)
   * @throws ADLException
   */
  public Launcher(final String factoryDefinition,
      final String backendDefinition,
      final Map<Object, Object> compilerCreationContext) throws ADLException {

    this.factory = createFactory(factoryDefinition, backendDefinition,
        compilerCreationContext);

    this.compilerContext = new HashMap<Object, Object>(compilerCreationContext);
  }

  /**
   * Instantiate a <tt>Launcher</tt> which will instantiate a FractalADL
   * Factory. The <tt>factoryDefinition</tt> and <tt>backendDefinition</tt>
   * parameters must be passed in the given <code>params</code> map with the
   * key {@link #FACTORY_PROPERTY_NAME} and
   * {@link FactoryFactory#BACKEND_PROPERTY_NAME}.
   * 
   * @param params launcher parameters. Must contains at least the keys
   *            {@link #FACTORY_PROPERTY_NAME} and
   *            {@link FactoryFactory#BACKEND_PROPERTY_NAME} associated with
   *            <code>String</code> values.
   * @throws ADLException
   */
  public Launcher(final Map<Object, Object> params) throws ADLException {
    this((String) params.get(Launcher.FACTORY_PROPERTY_NAME), (String) params
        .get(FactoryFactory.BACKEND_PROPERTY_NAME), params);
  }

  /**
   * Return a copy of the <tt>Map</tt> used to instantiate the FractalADL
   * Factory that will be used by this <tt>Launcher</tt>.
   * 
   * @return a copy of the <tt>Map</tt> used to instantiate the FractalADL
   *         Factory that will be used by this <tt>Launcher</tt>.
   */
  public Map<Object, Object> getCompilerContext() {
    return new HashMap<Object, Object>(compilerContext);
  }

  /**
   * Returns the Factory which will be used by the Launcher to compile the
   * provided definition.
   * 
   * @return the Factory which will be used by the Launcher to compile the
   *         provided definition.
   */
  public Factory getFactory() {
    return factory;
  }

  /**
   * Calls the <tt>FactoryFactory</tt> to get a Factory.
   * 
   * @param factoryDefinition
   * @param backendDefinition
   * @param factoryCreationContext
   * @return
   * @throws RuntimeException
   * @throws ADLException
   */
  Factory createFactory(final String factoryDefinition,
      final String backendDefinition,
      final Map<Object, Object> factoryCreationContext) throws ADLException {

    return FactoryFactory.getFactory(factoryDefinition, backendDefinition,
        factoryCreationContext);

  }

  /**
   * Simply delegates to the other <tt>compile()</tt> method passing an empty
   * <tt>Map</tt>.
   * 
   * @param componentDefinition
   * @throws ADLException
   * @throws IllegalLifeCycleException
   */
  public void compile(final String componentDefinition) throws ADLException,
      IllegalLifeCycleException {
    this.compile(componentDefinition, ContextMap.instance()); // new HashMap<Object, Object>()
  }

  /**
   * Compile the given <tt>componentDefinition</tt> using the previously
   * instantiated Factory. This may result either in the component deployment or
   * in some code generation depending on the backend that have been used when
   * instantiating the Factory used by the Launcher. The compilation will make
   * use of the compilerContext + the context passed as parameter.
   * 
   * @param componentDefinition
   * @param context
   * @throws ADLException
   * @throws IllegalLifeCycleException
   */
  public void compile(final String componentDefinition,
      final Map<Object, Object> context) throws ADLException,
      IllegalLifeCycleException {

    final Map<Object, Object> actualParams = new HashMap<Object, Object>();
    actualParams.putAll(this.compilerContext);
    actualParams.putAll(context);

    /* this may result in a component creation of code generation */
    Object comp = this.factory.newComponent(componentDefinition, actualParams);

    final Object runnableInterface = actualParams
        .get(Launcher.RUNNABLE_INTERFACE_PROPERTY_NAME);

    /* if a Fractal Component has been deployed.. */
    if (comp instanceof Component) {
      LifeCycleController lc = null;
      try {
        lc = Fractal.getLifeCycleController((Component) comp);
      } catch (final NoSuchInterfaceException ignored) {
      }
      if (lc != null) {
        lc.startFc();
      }
      Runnable r = null;
      try {
        r = (Runnable) ((Component) comp)
            .getFcInterface((String) runnableInterface);
      } catch (final NoSuchInterfaceException ignored) {
        System.err.println("Impossible to find a Runnable interface named '"
            + runnableInterface + "'");
      }
      if (r != null) {
        r.run();
      }
      if ((lc != null) && !isDaemon(actualParams)) {
        lc.stopFc();
      }
    } else if (comp instanceof Map) { // else if a POJO has been deployed

      /* then the root component is a Java Map */
      comp = ((Map<?, ?>) comp).get(runnableInterface);

      if (comp instanceof LifeCycleController) {
        ((LifeCycleController) comp).startFc();
      }
      if (comp instanceof Runnable) {
        ((Runnable) comp).run();
      }
      if ((comp instanceof LifeCycleController) && !isDaemon(actualParams)) {
        ((LifeCycleController) comp).stopFc();
      }
    } // else there have been some code generation using a static backend
  }

  final boolean isDaemon(final Map<Object, Object> context) {
    final String daemon = (String) context.get(DAEMON_PROPERTY_NAME);
    return Boolean.valueOf(daemon).booleanValue();
  }

  /**
   * Command line method to instantiate a FractalADL Factory. The usage of this
   * main() method retains (up to this point) backward compatibility with the
   * old Launcher, so it is not possible to specify the FractalADL factory
   * architecture definition to use, and only which one of the two non static
   * backend to use (JavaBackend or FractalBackend). Alternatives to this
   * limitation:
   * <ul>
   * <li>instantiate the Launcher in your code by means of the constructor and
   * call the compile() method</li>
   * <li>modify the main() method and expecially the parameter format that it
   * expects</li>
   * </ul>
   * 
   * @param pargs
   * @throws Exception
   */
  public static void main(final String[] pargs) throws Exception {

    final Map<Object, Object> context = ContextMap.instance(); // new HashMap<Object, Object>();

    // init default values
    context.put(Launcher.FACTORY_PROPERTY_NAME, FactoryFactory.DEFAULT_FACTORY);
    context.put(FactoryFactory.BACKEND_PROPERTY_NAME,
        FactoryFactory.JAVA_BACKEND);
    context.put(Launcher.RUNNABLE_INTERFACE_PROPERTY_NAME,
        Launcher.RUNNABLE_INTERFACE_DEFAULT_VALUE);
    context.put(Launcher.DAEMON_PROPERTY_NAME,
        Launcher.DAEMON_PROPERTY_DEFAULT_VALUE);

    // set and override values with System properties
    final Map<Object, Object> systemPropertiesMap = new HashMap<Object, Object>(
        System.getProperties());
    context.putAll(systemPropertiesMap);

    // set and override values with command line properties
    final Map<String, String> commandLinePropertiesMap = parseCommandLineParameters(pargs);
    context.putAll(commandLinePropertiesMap);

    /* XXX set the current Thread context ClassLoader as "classloader" property */
    context.put("classloader", Thread.currentThread().getContextClassLoader());

    /* the Launcher instance that will be created */
    final Launcher launcher = new Launcher(context);

    /* compile the component definition */
    launcher.compile(commandLinePropertiesMap
        .get(Launcher.COMPONENT_DEFINITION_PROPERTY_NAME));
  }

  /**
   * Parse the command line parameters and create a {name, value} parameters
   * Map.
   * 
   * @param args
   * @return
   */
  private static Map<String, String> parseCommandLineParameters(
      final String[] args) {

    return parseArgsAsMap(args);
  }

  /**
   * Parse arguments in the "-name:value" style.
   * 
   * @param args
   * @return
   */
  static Map<String, String> parseArgsAsMap(final String[] args) {
    final Map<String, String> result = new HashMap<String, String>();
    String paramName = null;
    String paramValue = null;
    int index = -1;
    String definition = null;
    String runnableItf = null;
    for (final String currentArg : args) {
      if (currentArg.startsWith(PARAMETER_NAME_TRAILING_CHAR)) {
        /* if it is a "-name:value" parameter */
        if (currentArg.contains(PARAMETER_NAME_VALUE_SEPARATOR_CHAR)) {

          index = currentArg.indexOf(PARAMETER_NAME_VALUE_SEPARATOR_CHAR);
          paramName = currentArg.substring(1, index);
          paramValue = currentArg.substring(index + 1);
          result.put(paramName, paramValue);
        } else { // else if it is the legacy -java|-fractal backend parameter
          if (currentArg.equals("-java")) {
            result.put(FactoryFactory.BACKEND_PROPERTY_NAME,
                FactoryFactory.JAVA_BACKEND);
          } else {
            if (currentArg.equals("-fractal")) {
              result.put(FactoryFactory.BACKEND_PROPERTY_NAME,
                  FactoryFactory.FRACTAL_BACKEND);
            } else {
              parseError();
              throw new RuntimeException("The parameter '" + currentArg
                  + "' is unknown.");
            }
          }
        }
      } else {
        /*
         * else legacy way to specify component definition and its runnable
         * interface name
         */
        if (definition == null) {
          definition = currentArg;
          result.put(Launcher.COMPONENT_DEFINITION_PROPERTY_NAME, definition);
        } else if (runnableItf == null) {
          runnableItf = currentArg;
          result.put(Launcher.RUNNABLE_INTERFACE_PROPERTY_NAME, runnableItf);
        } else {
          parseError();
          throw new RuntimeException(
              "Too many args: you already specified the definition to load and its runnable interface name");
        }
      }
    }

    return result;
  }

  private static void parseError() {
    System.out
        .println("Usage: Launcher [-java|-fractal] [ (-<paramName=paramValue)* ] [<definition>] [ <itf> ] ");
    System.out.print("where <definition> is the name of the component to be ");
    System.out.print("instantiated and started,\nand <itf> is the name of ");
    System.out.println("its Runnable interface, if it has one");
    // commented because otherwise the Launcher could not be embedded safely in
    // a non-command line application
    // System.exit(1);
  }
}
