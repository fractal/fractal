/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 *
 * Contributor: Matthieu Leclercq (externalize interface loading)
 */

package org.objectweb.fractal.adl.attributes;

import java.lang.reflect.Method;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.CompilerError;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.ChainedErrorLocator;
import org.objectweb.fractal.adl.error.NodeErrorLocator;
import org.objectweb.fractal.adl.interfaces.IDLLoader;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;

/**
 * A {@link org.objectweb.fractal.adl.Loader} to check {@link Attributes} nodes
 * in definitions. This loader checks that the Java attribute controller
 * interfaces specified in these nodes exist, and that the attribute names and
 * values are consistent with the methods of theses interfaces.
 */

public class AttributeLoader extends AbstractLoader {

  // ---------------------------------------------------------------------------
  // Client interfaces
  // ---------------------------------------------------------------------------

  /** The name of the {@link #interfaceLoaderItf} client interface. */
  public static final String INTERFACE_LOADER_BINDING = "interface-loader";

  /** The {@link IDLLoader} used by this loader. */
  public IDLLoader           interfaceLoaderItf;

  // ---------------------------------------------------------------------------
  // Implementation of the Loader interface
  // ---------------------------------------------------------------------------

  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    final Definition d = clientLoader.load(name, context);
    checkNode(d, context);
    return d;
  }

  // ---------------------------------------------------------------------------
  // Checking methods
  // ---------------------------------------------------------------------------

  protected void checkNode(final Object node, final Map<Object, Object> context)
      throws ADLException {
    if (node instanceof AttributesContainer) {
      checkAttributesContainer((AttributesContainer) node, context);
    }
    if (node instanceof ComponentContainer) {
      for (final Component comp : ((ComponentContainer) node).getComponents()) {
        checkNode(comp, context);
      }
    }
  }

  protected void checkAttributesContainer(final AttributesContainer container,
      final Map<Object, Object> context) throws ADLException {
    final Attributes attrs = container.getAttributes();
    if (attrs != null) {
      final String signature = attrs.getSignature();
      if (signature == null) {
        throw new ADLException(AttributeErrors.SIGNATURE_MISSING, attrs);
      }
      Class<?> c;
      try {
        c = (Class<?>) interfaceLoaderItf.loadInterface(attrs.getSignature(),
            context);
      } catch (final ADLException e) {
        ChainedErrorLocator.chainLocator(e, attrs);
        throw e;
      }

      for (final Attribute attribute : attrs.getAttributes()) {
        final String attrName = attribute.getName();
        final String attrValue = attribute.getValue();
        if (attrName == null) {
          throw new ADLException(AttributeErrors.NAME_MISSING, attribute);
        }
        if (attrValue == null) {
          throw new ADLException(AttributeErrors.VALUE_MISSING, attribute);
        }
        final String getterName = "get"
            + Character.toUpperCase(attrName.charAt(0)) + attrName.substring(1);
        Method getter;
        try {
          getter = c.getMethod(getterName, new Class[0]);
        } catch (final Exception e) {
          throw new ADLException(AttributeErrors.NO_SUCH_ATTRIBUTE, attribute,
              e, attrName);
        }
        final Class<?> attrType = getter.getReturnType();
        if (attrType.isPrimitive()) {
          if (attrType.equals(Integer.TYPE)) {
            try {
              Integer.valueOf(attrValue);
            } catch (final NumberFormatException e) {
              throw new ADLException(AttributeErrors.INVALID_INTEGER_VALUE,
                  attribute, attrValue, attrName);
            }
          } else if (attrType.equals(Long.TYPE)) {
            try {
              Long.valueOf(attrValue);
            } catch (final NumberFormatException e) {
              throw new ADLException(AttributeErrors.INVALID_LONG_VALUE,
                  attribute, attrValue, attrName);
            }
          } else if (attrType.equals(Float.TYPE)) {
            try {
              Float.valueOf(attrValue);
            } catch (final NumberFormatException e) {
              throw new ADLException(AttributeErrors.INVALID_FLOAT_VALUE,
                  attribute, attrValue, attrName);
            }
          } else if (attrType.equals(Double.TYPE)) {
            try {
              Double.valueOf(attrValue);
            } catch (final NumberFormatException e) {
              throw new ADLException(AttributeErrors.INVALID_DOUBLE_VALUE,
                  attribute, attrValue, attrName);
            }
          } else if (attrType.equals(Byte.TYPE)) {
            try {
              Byte.valueOf(attrValue);
            } catch (final NumberFormatException e) {
              throw new ADLException(AttributeErrors.INVALID_BYTE_VALUE,
                  attribute, attrValue, attrName);
            }
          } else if (attrType.equals(Character.TYPE)) {
            if (attrValue.length() != 1) {
              throw new ADLException(AttributeErrors.INVALID_CHAR_VALUE,
                  attribute, attrValue, attrName);
            }
          } else if (attrType.equals(Short.TYPE)) {
            try {
              Short.valueOf(attrValue);
            } catch (final NumberFormatException e) {
              throw new ADLException(AttributeErrors.INVALID_SHORT_VALUE,
                  attribute, attrValue, attrName);
            }
          } else if (attrType.equals(Boolean.TYPE)) {
            if (!attrValue.equals("true") && !attrValue.equals("false")) {
              throw new ADLException(AttributeErrors.INVALID_BOOLEAN_VALUE,
                  attribute, attrValue, attrName);
            }
          } else {
            throw new CompilerError(AttributeErrors.UNEXPECTED_ATTRIBUTE_TYPE,
                new NodeErrorLocator(attribute), attrType, attrName);
          }
        } else if (attrType != String.class) {
          throw new ADLException(AttributeErrors.UNSUPPORTED_ATTRIBUTE_TYPE,
              attribute, attrType, attrName);
        }
      }
    }
  }

  // ---------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ---------------------------------------------------------------------------

  @Override
  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (INTERFACE_LOADER_BINDING.equals(s)) {
      interfaceLoaderItf = (IDLLoader) o;
    } else {
      super.bindFc(s, o);
    }
  }

  @Override
  public String[] listFc() {
    final String[] superList = super.listFc();
    final String[] list = new String[superList.length + 1];
    list[0] = INTERFACE_LOADER_BINDING;
    System.arraycopy(superList, 0, list, 1, superList.length);
    return list;
  }

  @Override
  public Object lookupFc(final String s) throws NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (INTERFACE_LOADER_BINDING.equals(s)) {
      return interfaceLoaderItf;
    } else {
      return super.lookupFc(s);
    }
  }

  @Override
  public void unbindFc(final String s) throws IllegalBindingException,
      NoSuchInterfaceException {

    if (s == null) {
      throw new IllegalArgumentException("Interface name can't be null");
    }

    if (INTERFACE_LOADER_BINDING.equals(s)) {
      interfaceLoaderItf = null;
    } else {
      super.unbindFc(s);
    }
  }

}
