/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author:      Eric Bruneton
 * Contributor: Alessio Pace
 */

package org.objectweb.fractal.adl.components;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Compiler;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.task.core.TaskMap;

/**
 * Basic implementation of the {@link Compiler} interface. This implementation
 * delegates definition compilation requests to a set of
 * {@link PrimitiveCompiler}s.
 */

public class ComponentCompiler implements BindingController, Compiler {

  /**
   * Name of the collection interface bound to the {@link PrimitiveCompiler}s
   * used by this compiler.
   */
  public static final String                   PRIMITIVE_COMPILERS_BINDING = "primitive-compilers";

  /**
   * The primitive compilers used by this compiler.
   */
  private final Map<String, PrimitiveCompiler> primitiveCompilers          = new TreeMap<String, PrimitiveCompiler>();

  // --------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // --------------------------------------------------------------------------

  public String[] listFc() {
    return primitiveCompilers.keySet().toArray(
        new String[primitiveCompilers.size()]);
  }

  public Object lookupFc(final String itf) {
    if (itf.startsWith(PRIMITIVE_COMPILERS_BINDING)) {
      return primitiveCompilers.get(itf);
    } else {
      return null;
    }
  }

  public void bindFc(final String itf, final Object value) {
    if (itf.startsWith(PRIMITIVE_COMPILERS_BINDING)) {
      primitiveCompilers.put(itf, (PrimitiveCompiler) value);
    }
  }

  public void unbindFc(final String itf) {
    if (itf.startsWith(PRIMITIVE_COMPILERS_BINDING)) {
      primitiveCompilers.remove(itf);
    }
  }

  // --------------------------------------------------------------------------
  // Implementation of the Compiler interface
  // --------------------------------------------------------------------------

  public void compile(final Definition definition, final TaskMap tasks,
      final Map<Object, Object> context) throws ADLException {
    if (definition instanceof ComponentContainer) {
      compile(new ArrayList<ComponentContainer>(),
          (ComponentContainer) definition, tasks, context);
    }
  }

  // --------------------------------------------------------------------------
  // Compilation methods
  // --------------------------------------------------------------------------

  void compile(final List<ComponentContainer> path,
      final ComponentContainer container, final TaskMap tasks,
      final Map<Object, Object> context) throws ADLException {
    path.add(container);
    final Component[] comps = container.getComponents();
    for (final Component element : comps) {
      compile(path, element, tasks, context);
    }
    path.remove(path.size() - 1);

    final Iterator<PrimitiveCompiler> it = primitiveCompilers.values()
        .iterator();
    while (it.hasNext()) {
      (it.next()).compile(path, container, tasks, context);
    }
  }
}
