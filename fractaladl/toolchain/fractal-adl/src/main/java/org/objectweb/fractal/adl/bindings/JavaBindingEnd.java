/***
 * Fractal ADL Parser
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.bindings;

import java.util.Map;

import org.objectweb.fractal.api.control.IllegalBindingException;

/**
 * Bindins end is a simple structure identifying one side of a bindind (client
 * or server). It is intended to be used to build binding between plain Java
 * object component (i.e. components instantiated with the Java Backend).
 */
public final class JavaBindingEnd {
  /** The component at this binding end. */
  final Object      component;
  /** The name of the interface at this binding end. */
  final String      itfName;
  // compute hashcode at creation since object is imutable.
  private final int hashcode;

  /**
   * Constructor.
   * 
   * @param component
   * @param itfName
   */
  public JavaBindingEnd(final Object component, final String itfName) {
    this.component = component;
    this.itfName = itfName;
    hashcode = System.identityHashCode(component) * itfName.hashCode();
  }

  /**
   * Returns <code>true</code> if the component at this binding end is a
   * composite (i.e. is a {@link Map}).
   * 
   * @return <code>true</code> if {@link #component} is a {@link Map}
   */
  boolean isComposite() {
    return component instanceof Map;
  }

  /**
   * Bind this binding end to the given server side.
   * 
   * @param serverSide the server side to bind to
   * @throws IllegalBindingException if the binding fails.
   * @see JavaBindingUtil#bindFc(Object, String, Object)
   */
  @SuppressWarnings("unchecked")
  void bind(final JavaBindingEnd serverSide) throws IllegalBindingException {
    if (isComposite()) {
      ((Map) component).put(itfName, serverSide.component);
    } else {
      JavaBindingUtil.bindFc(component, itfName, serverSide.component);
    }
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) return true;

    if (obj instanceof JavaBindingEnd) {
      final JavaBindingEnd be = (JavaBindingEnd) obj;
      return be.component == component && be.itfName.equals(itfName);
    }
    return false;
  }

  @Override
  public int hashCode() {
    return hashcode;
  }
}