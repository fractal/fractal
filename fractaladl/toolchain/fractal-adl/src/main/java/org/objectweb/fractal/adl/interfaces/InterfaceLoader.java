/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.interfaces;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.AbstractLoader;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.error.NodeErrorLocator;

/**
 * A {@link org.objectweb.fractal.adl.Loader} to check {@link Interface} nodes
 * in definitions. This loader checks that interface names are locally unique.
 */

public class InterfaceLoader extends AbstractLoader {

  // --------------------------------------------------------------------------
  // Implementation of the Loader interface
  // --------------------------------------------------------------------------

  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    final Definition d = clientLoader.load(name, context);
    checkNode(d);
    return d;
  }

  // --------------------------------------------------------------------------
  // Checking methods
  // --------------------------------------------------------------------------

  private void checkNode(final Object node) throws ADLException {
    if (node instanceof InterfaceContainer) {
      checkInterfaceContainer((InterfaceContainer) node);
    }
    if (node instanceof ComponentContainer) {
      for (final Component comp : ((ComponentContainer) node).getComponents()) {
        checkNode(comp);
      }
    }
  }

  private void checkInterfaceContainer(final InterfaceContainer container)
      throws ADLException {
    final Map<String, Interface> names = new HashMap<String, Interface>();
    for (final Interface itf : container.getInterfaces()) {
      if (itf.getName() == null) {
        throw new ADLException(InterfaceErrors.INTERFACE_NAME_MISSING, itf);
      }
      final Interface previousDefinition = names.put(itf.getName(), itf);
      if (previousDefinition != null) {
        throw new ADLException(InterfaceErrors.DUPLICATED_INTERFACE_NAME, itf,
            itf.getName(), new NodeErrorLocator(previousDefinition));
      }
    }
  }
}
