/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 *
 * Contributor(s): Matthieu Leclercq (allows null id and type)
 */

package org.objectweb.fractal.adl;

import org.objectweb.fractal.task.core.BasicTaskMap;
import org.objectweb.fractal.task.core.TaskMap;

/**
 * Basic implementation of the {@link TaskMap} interface.
 */
public class FractalADLTaskMap extends BasicTaskMap {

  @Override
  protected BasicTaskMap.Key createTaskKey(final String type, final Object id) {
    return new FractalADLKey(type, id);
  }

  public static class FractalADLKey extends BasicTaskMap.Key {

    FractalADLKey(final String type, final Object id) {
      super(type, id);
    }

    @Override
    public boolean equals(final Object o) {
      if (o instanceof Key) {
        final Key k = (Key) o;
        return ((k.getType() == null) ? type == null : k.getType().equals(type))
            && ((k.getId() instanceof Node) ? (k.getId() == id) : k.getId()
                .equals(id));
      }
      return false;
    }

    // add hashCode method to make checkstyle happy ;-)
    @Override
    public int hashCode() {
      return super.hashCode();
    }

    @Override
    protected int computeHashCode() {
      return ((type == null) ? 1 : type.hashCode())
          * ((id instanceof Node) ? System.identityHashCode(id) : id.hashCode());

    }
  }
}
