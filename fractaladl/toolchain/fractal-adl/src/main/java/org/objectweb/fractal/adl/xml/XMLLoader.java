/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributors:
 * - Matthieu Leclercq (externalize AST node factory)
 * - Philippe Merle (set the class loader to use during XML parsing)
 */

package org.objectweb.fractal.adl.xml;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

import org.objectweb.fractal.adl.ADLErrors;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.adl.Parser;
import org.objectweb.fractal.adl.ParserException;
import org.objectweb.fractal.adl.error.BasicErrorLocator;
import org.objectweb.fractal.adl.error.ErrorLocator;
import org.objectweb.fractal.adl.timestamp.Timestamp;
import org.objectweb.fractal.adl.util.ClassLoaderHelper;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.xml.sax.SAXParseException;

/**
 * A {@link Loader} that loads definitions from XML files.
 */

public class XMLLoader implements Loader, BindingController {

  private boolean            validate             = true;

  private Parser             parser;

  // --------------------------------------------------------------------------
  // Client interfaces
  // --------------------------------------------------------------------------

  /** The name of the {@link #nodeFactoryItf} client interface. */
  public static final String NODE_FACTORY_BINDING = "node-factory";

  /** The {@link XMLNodeFactory} client interface of this component. */
  public XMLNodeFactory      nodeFactoryItf;

  // --------------------------------------------------------------------------
  // Constructor
  // --------------------------------------------------------------------------

  /**
   * Creates a new {@link XMLLoader}.
   */
  public XMLLoader() {
  }

  /**
   * Creates a new {@link XMLLoader}.
   * 
   * @param validate if <code>true</code>, use a validating XML parser.
   */
  public XMLLoader(final boolean validate) {
    this.validate = validate;
  }

  // --------------------------------------------------------------------------
  // Implementation of the Loader interface
  // --------------------------------------------------------------------------

  public Definition load(final String name, final Map<Object, Object> context)
      throws ADLException {
    synchronized (this) {
      if (parser == null) {
        parser = new XMLParser(validate, nodeFactoryItf);
      }
    }

    final String file = name.replace('.', '/') + ".fractal";
    final ClassLoader cl = ClassLoaderHelper.getClassLoader(this, context);
    final URL url = cl.getResource(file);
    if (url == null) {
      throw new ADLException(ADLErrors.ADL_NOT_FOUND, file);
    }
    try {

      nodeFactoryItf.setClassLoader(cl);
      final Definition d;
      try {
        d = (Definition) parser.parse(url.openStream(), file);
      } catch (final IOException e) {
        throw new ADLException(ADLErrors.IO_ERROR, e, file);
      }
      nodeFactoryItf.setClassLoader(ClassLoaderHelper.getClassLoader(this));
      if (d.getName() == null) {
        throw new ADLException(XMLErrors.DEFINTION_NAME_MISSING, d);
      }
      if (!d.getName().equals(name)) {
        throw new ADLException(ADLErrors.WRONG_DEFINITION_NAME, d, name, d
            .getName());
      }

      // Set timestamp of the ADL file
      Timestamp.setTimestamp(d, url);

      return d;
    } catch (final ParserException e) {
      final Throwable cause = e.getCause();
      if (cause instanceof SAXParseException) {
        final SAXParseException spe = (SAXParseException) cause;
        final ErrorLocator locator = new BasicErrorLocator(file, spe
            .getLineNumber(), spe.getColumnNumber());
        throw new ADLException(ADLErrors.PARSE_ERROR, locator, spe.getMessage());
      }
      throw new ADLException(ADLErrors.PARSE_ERROR, cause, cause.getMessage());
    }
  }

  // --------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // --------------------------------------------------------------------------

  public void bindFc(final String s, final Object o)
      throws NoSuchInterfaceException, IllegalBindingException,
      IllegalLifeCycleException {
    if (NODE_FACTORY_BINDING.equals(s)) {
      this.nodeFactoryItf = (XMLNodeFactory) o;
    }

  }

  public String[] listFc() {
    return new String[]{NODE_FACTORY_BINDING};
  }

  public Object lookupFc(final String s) throws NoSuchInterfaceException {
    if (NODE_FACTORY_BINDING.equals(s)) {
      return this.nodeFactoryItf;
    }
    return null;
  }

  public void unbindFc(final String s) throws NoSuchInterfaceException,
      IllegalBindingException, IllegalLifeCycleException {
    if (NODE_FACTORY_BINDING.equals(s)) {
      this.nodeFactoryItf = null;
    }

  }
}
