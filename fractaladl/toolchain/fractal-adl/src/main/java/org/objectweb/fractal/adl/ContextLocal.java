/***
 * Fractal ADL Parser
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

/**
 * This class provides a "context local" variable. It can be used to define a
 * variable associated with a context Map without being put in the context.
 * <p>
 * This class stores context using {@link WeakReference}, so instances of this
 * class cannot be used to keep a strong reference to context object.
 * 
 * @param <T> The type of the context local variable.
 */
public class ContextLocal<T> {

  private final Map<ContextRef, T>     map   = new HashMap<ContextRef, T>();
  private final ReferenceQueue<Object> queue = new ReferenceQueue<Object>();

  /**
   * Set the value of the context local variable.
   * 
   * @param context the context.
   * @param value the value.
   */
  public void set(final Object context, final T value) {
    expungeStaleEntries();
    map.put(new ContextRef(context), value);
  }

  /**
   * Returns the value of the context local variable.
   * 
   * @param context the context.
   * @return the value of the context local variable.
   */
  public T get(final Object context) {
    expungeStaleEntries();
    return map.get(new ContextRef(context));
  }

  private void expungeStaleEntries() {
    ContextRef ref;
    while ((ref = (ContextRef) ((Reference<Object>) queue.poll())) != null) {
      map.remove(ref);
    }
  }

  private final class ContextRef extends WeakReference<Object> {

    private final int hash;

    ContextRef(final Object context) {
      super(context, queue);
      hash = System.identityHashCode(context);
    }

    @Override
    public boolean equals(final Object obj) {
      if (obj == this) return true;
      if (obj instanceof WeakReference)
        return get() == ((WeakReference<?>) obj).get();
      return false;
    }

    @Override
    public int hashCode() {
      return hash;
    }

  }
}
