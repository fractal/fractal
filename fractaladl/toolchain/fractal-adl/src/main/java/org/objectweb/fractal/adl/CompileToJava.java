/***
 * Fractal ADL Parser
 * Copyright (C) 2005 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Philippe.Merle@inria.fr
 *
 * Author: Philippe Merle
 */

package org.objectweb.fractal.adl;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Map;

/**
 * A class to compile a Fractal ADL definition to Java code. Usage:
 * CompileToJava [-java|-fractal] &lt;definition&gt; [ &lt;output&gt; ] where
 * &lt;definition&gt; is the name of the component to compile, and
 * &lt;output&gt; is the output file name.
 */
public final class CompileToJava {

  private CompileToJava() {
  }

  /**
   * Entry point.
   * 
   * @param args command line arguments.
   * @throws Exception If an error occurs.
   */
  public static void main(final String[] args) throws Exception {
    final String[] pargs = parseArgs(args);

    final String backend = (pargs[0].equals("-java"))
        ? FactoryFactory.STATIC_JAVA_BACKEND
        : FactoryFactory.STATIC_FRACTAL_BACKEND;

    final String definition = pargs[1];

    final PrintWriter printWriter = (pargs[2] == null) ? new PrintWriter(
        System.out, true) : new PrintWriter(new FileWriter(pargs[2]), true);

    final Map<Object, Object> context = ContextMap.instance(); // new HashMap<Object, Object>();
    context.put("printwriter", printWriter);
    final Factory f = FactoryFactory.getFactory(backend);
    f.newComponent(definition, context);
  }

  private static String[] parseArgs(final String[] args) {
    if (args.length < 1 || args.length > 3) {
      parseError();
    }
    final String[] result = new String[3];
    if (args[0].equals("-java") || args[0].equals("-fractal")) {
      if (args.length < 2) {
        parseError();
      }
      result[0] = args[0];
      result[1] = args[1];
      result[2] = args.length == 3 ? args[2] : null;
    } else {
      result[0] = "-java";
      result[1] = args[0];
      result[2] = args.length >= 2 ? args[1] : null;
    }
    return result;
  }

  private static void parseError() {
    System.out
        .println("Usage: CompileToJava [-java|-fractal] <definition> [ <output> ]");
    System.out
        .println("where <definition> is the name of the component to compile,");
    System.out.println("and <output> is the output file name");
    System.exit(1);
  }
}
