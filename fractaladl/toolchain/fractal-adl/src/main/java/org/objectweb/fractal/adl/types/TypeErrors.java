/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.types;

import static org.objectweb.fractal.adl.error.ErrorTemplateValidator.validErrorTemplate;

import org.objectweb.fractal.adl.error.ErrorTemplate;

/** {@link ErrorTemplate} group for the types package. */
public enum TypeErrors implements ErrorTemplate {

  /** */
  SIGNATURE_MISSING("Signature missing."),

  /** */
  ROLE_MISSING("Role missing"),

  /** */
  INVALID_ROLE("Invalid role \"%s\". Expecting one of \""
      + TypeInterface.CLIENT_ROLE + "\" or \"" + TypeInterface.SERVER_ROLE
      + "\".", "role"),

  /** */
  INVALID_CONTINGENCY("Invalid contengency \"%s\". Expecting one of \""
      + TypeInterface.MANDATORY_CONTINGENCY + "\" or \""
      + TypeInterface.OPTIONAL_CONTINGENCY + "\".", "contingency"),

  /** */
  INVALID_CARDINALITY("Invalid cardinality \"%s\". Expecting one of \""
      + TypeInterface.SINGLETON_CARDINALITY + "\" or \""
      + TypeInterface.COLLECTION_CARDINALITY + "\".", "cardinality");

  /** The groupId of ErrorTemplates defined in this enumeration. */
  public static final String GROUP_ID = "TYP";

  private int                id;
  private String             format;

  private TypeErrors(final String format, final Object... args) {
    this.id = ordinal();
    this.format = format;

    assert validErrorTemplate(this, args);
  }

  public int getErrorId() {
    return id;
  }

  public String getGroupId() {
    return GROUP_ID;
  }

  public String getFormatedMessage(final Object... args) {
    return String.format(format, args);
  }

  public String getFormat() {
    return format;
  }
}
