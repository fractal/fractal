/***
 * Fractal ADL Parser
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl;


/**
 * Exception thrown when a required ADL cannot be found. <br>
 * This exception class is deprecated, The {@link ADLException} with dedicated
 * {@link Error} object should be used instead.
 */
@Deprecated
public class ADLNotFoundException extends ADLException {

  /**
   * Constructs a new {@link ADLNotFoundException}.
   * 
   * @param msg a detail message.
   * @param e the exception that caused this exception.
   */
  public ADLNotFoundException(final String msg, final Exception e) {
    super(msg, e);
  }

  /**
   * Constructs a new {@link ADLNotFoundException}.
   * 
   * @param msg a detail message.
   */
  public ADLNotFoundException(final String msg) {
    super(msg);
  }

}
