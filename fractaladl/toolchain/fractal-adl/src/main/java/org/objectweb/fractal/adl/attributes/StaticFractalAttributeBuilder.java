/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2006 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributor: Philippe Merle
 *
 * $Id$
 */

package org.objectweb.fractal.adl.attributes;

import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.Map;

import org.objectweb.fractal.adl.util.ClassLoaderHelper;

/**
 * A Fractal based, static implementation of the {@link AttributeBuilder} 
 * interface. This implementation produces code that uses the Fractal API to 
 * set component attributes. 
 */

public class StaticFractalAttributeBuilder implements AttributeBuilder {

  // --------------------------------------------------------------------------
  // Implementation of the AttributeBuilder interface
  // --------------------------------------------------------------------------
  
  public void setAttribute (
    final Object component, 
    final String attributeController, 
    final String name, 
    final String value,
    final Object context) throws Exception 
  {
    ClassLoader loader = ClassLoaderHelper.getClassLoader(this, context);

    String v = value;
    Class c = loader.loadClass(attributeController);
    String attrName = Character.toUpperCase(name.charAt(0)) + name.substring(1);
    Method getter = c.getMethod("get" + attrName, new Class[0]);      
    if (getter.getReturnType() == String.class) {
      StringBuffer buf = new StringBuffer();
      buf.append("\"");
      for (int i = 0; i < v.length(); ++i) {
        char car = v.charAt(i);
        if (car == '\n') {
          buf.append("\\n");
        } else if (car == '\\') {
          buf.append("\\\\");
        } else if (car == '"') {
          buf.append("\\\"");
        } else {
          buf.append(car);
        }
      }
      buf.append("\"");
      v = buf.toString();
    }
    PrintWriter pw = (PrintWriter)((Map)context).get("printwriter");
    pw.print("((");
    pw.print(attributeController);
    pw.print(")Fractal.getAttributeController(");
    pw.print(component);
    pw.print(")).set");
    pw.print(attrName);
    pw.print("(");
    pw.print(v);
    pw.println(");");
  }
}
