/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.error;

import java.util.HashMap;
import java.util.IllegalFormatException;
import java.util.Map;

/**
 * Helper class that provides static methods to validate {@link ErrorTemplate}.
 * In particular it checks that each ErrorTemplate has a unique
 * {@link ErrorTemplate#getGroupId() groupId},
 * {@link ErrorTemplate#getErrorId() errorId} pair. These methods can be used in
 * {@link ErrorTemplate} constructors in an assertion statement.
 */
public final class ErrorTemplateValidator {
  private ErrorTemplateValidator() {
  }

  // map associating ErrorTemplateID and error template.
  private static Map<String, ErrorTemplate> idToTemplate = null;

  /**
   * Returns <code>true</code> if the given enumeration class is valid.
   * 
   * @param enumClass an enumeration class. This class must implement
   *            {@link ErrorTemplate}.
   * @return <code>true</code> if the given enumeration class is valid.
   * @see #validErrorTemplate(ErrorTemplate)
   */
  public static boolean validErrorEnum(final Class<? extends Enum<?>> enumClass) {
    for (final Enum<?> e : enumClass.getEnumConstants()) {
      if (!validErrorTemplate((ErrorTemplate) e)) return false;
    }
    return true;
  }

  /**
   * Validates the given {@link ErrorTemplate} and checks that itf
   * {@link ErrorTemplate#getFormat() format} is valid with the given
   * <code>args</code> (i.e. checks that
   * <code>template.getFormatedMessage(args)</code> do not throw a
   * {@link IllegalFormatException}).
   * 
   * @param template a template.
   * @param args the format arguments.
   * @return <code>true</code> if the given {@link ErrorTemplate} is valid.
   * @see #validErrorTemplate(ErrorTemplate)
   */
  public static boolean validErrorTemplate(final ErrorTemplate template,
      final Object... args) {
    return validMessage(template, args) && uniqueId(template);
  }

  /**
   * Validates that the given template has a unique
   * {@link ErrorTemplate#getGroupId() groupId},
   * {@link ErrorTemplate#getErrorId() errorId} pair.
   * 
   * @param template a template.
   * @return <code>true</code> if the given {@link ErrorTemplate} is valid.
   */
  public static boolean validErrorTemplate(final ErrorTemplate template) {
    return uniqueId(template);
  }

  private static boolean uniqueId(final ErrorTemplate template) {
    if (idToTemplate == null)
      idToTemplate = new HashMap<String, ErrorTemplate>();

    final String id = template.getGroupId() + template.getErrorId();
    final ErrorTemplate previousTemplate = idToTemplate.put(id, template);
    return previousTemplate == null || previousTemplate == template;
  }

  private static boolean validMessage(final ErrorTemplate template,
      final Object... args) {
    try {
      template.getFormatedMessage(args);
      return true;
    } catch (final IllegalFormatException e) {
      e.printStackTrace();
      return false;
    }
  }
}
