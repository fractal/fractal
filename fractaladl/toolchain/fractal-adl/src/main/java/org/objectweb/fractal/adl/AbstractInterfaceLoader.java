/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl;

import org.objectweb.fractal.adl.interfaces.IDLLoader;
import org.objectweb.fractal.api.control.BindingController;

/**
 * An abstract {@link IDLLoader} that delegates to another {@link IDLLoader}.
 */
public abstract class AbstractInterfaceLoader
    implements
      IDLLoader,
      BindingController {

  /**
   * Name of the mandatory interface bound to the {@link Loader} used by this
   * loader.
   */
  public static final String LOADER_BINDING = "client-loader";

  // TODO rename clientLoader to clientLoaderItf
  public IDLLoader           clientLoader;

  // ------------------------------------------------------------------------
  // Implementation of the BindingController interface
  // ------------------------------------------------------------------------

  public String[] listFc() {
    return new String[]{LOADER_BINDING};
  }

  public Object lookupFc(final String s) {
    if (LOADER_BINDING.equals(s)) {
      return clientLoader;
    }
    return null;
  }

  public void bindFc(final String s, final Object o) {
    if (LOADER_BINDING.equals(s)) {
      clientLoader = (IDLLoader) o;
    }
  }

  public void unbindFc(final String s) {
    if (LOADER_BINDING.equals(s)) {
      clientLoader = null;
    }
  }

}
