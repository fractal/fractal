/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.error;

/**
 * An error locator gives informations that allows to locate an error in an
 * input file.
 */
public interface ErrorLocator {

  /**
   * Returns location informations formated in a single string.
   * 
   * @return location informations formated in a single string.
   */
  String getLocation();

  /**
   * Returns the location of the input file that contains the error. May be
   * <code>null</code> if this information is not available.
   * 
   * @return the location of the input file that contains the error.
   */
  String getInputFilePath();

  /**
   * Return the line number on which the error begins in the input file. May be
   * <code>-1</code> if this information is not available.
   * 
   * @return the line number on which the error begins in the input file.
   */
  int getBeginLine();

  /**
   * Return the line number on which the error ends in the input file. May be
   * <code>-1</code> if this information is not available.
   * 
   * @return the line number on which the error ends in the input file.
   */
  int getEndLine();

  /**
   * Return the column number on which the error begins in the input file. May
   * be <code>-1</code> if this information is not available.
   * 
   * @return the line number on which the error begins in the input file.
   */
  int getBeginColumn();

  /**
   * Return the column number on which the error ends in the input file. May be
   * <code>-1</code> if this information is not available.
   * 
   * @return the line number on which the error ends in the input file.
   */
  int getEndColumn();
}
