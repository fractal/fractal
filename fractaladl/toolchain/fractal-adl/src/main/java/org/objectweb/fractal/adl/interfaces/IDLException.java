/***
 * Fractal ADL Parser
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.interfaces;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Node;

/**
 * Exception thrown when an error occurs during the analysis of an Interface
 * definition. <br>
 * This exception class is deprecated, The {@link ADLException} with dedicated
 * {@link InterfaceErrors} object should be used instead.
 */
@Deprecated
public class IDLException extends ADLException {

  private final String signature;

  /**
   * Constructs a new {@link IDLException}.
   * 
   * @param signature the signature of the faulting IDL.
   * @param msg a detail message.
   */
  public IDLException(final String signature, final String msg) {
    super(msg);
    this.signature = signature;
  }

  /**
   * Constructs a new {@link IDLException}.
   * 
   * @param signature the signature of the faulting IDL.
   * @param msg a detail message.
   * @param src where the error is located.
   */
  public IDLException(final String signature, final String msg, final Node src) {
    super(msg, src);
    this.signature = signature;
  }

  /**
   * Constructs a new {@link IDLException}.
   * 
   * @param signature the signature of the faulting IDL.
   * @param msg a detail message.
   * @param e the exception that caused this exception.
   */
  public IDLException(final String signature, final String msg,
      final Exception e) {
    super(msg, e);
    this.signature = signature;
  }

  /**
   * Constructs a new {@link IDLException}.
   * 
   * @param signature the signature of the faulting IDL.
   * @param msg a detail message.
   * @param src where the error is located.
   * @param e the exception that caused this exception.
   */
  public IDLException(final String signature, final String msg, final Node src,
      final Exception e) {
    super(msg, src, e);
    this.signature = signature;
  }

  @Override
  public String getMessage() {
    return (signature == null) ? super.getMessage() : "Error in IDL '"
        + signature + "': " + super.getMessage();
  }

  /**
   * @return the signature of the faulting IDL.
   */
  public String getSignature() {
    return signature;
  }
}
