/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributor: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.implementations;

import java.io.PrintWriter;
import java.util.Map;

import org.objectweb.fractal.adl.ContextLocal;

/**
 * A Java based, static implementation of the {@link ImplementationBuilder}
 * interface. This implementation produces standard Java code that creates
 * components.
 */

public class StaticJavaImplementationBuilder implements ImplementationBuilder {

  private final ContextLocal<Integer> primitiveCounters = new ContextLocal<Integer>();

  private final ContextLocal<Integer> compositeCounters = new ContextLocal<Integer>();

  // --------------------------------------------------------------------------
  // Implementation of the ImplementationBuilder interface
  // --------------------------------------------------------------------------

  public Object createComponent(final Object type, final String name,
      final String definition, final Object controllerDesc,
      final Object contentDesc, final Object context) {
    if (contentDesc == null) {
      Integer i = compositeCounters.get(context);
      if (i == null) {
        i = new Integer(0);
      }
      final String id = "C" + i;
      final PrintWriter pw = (PrintWriter) ((Map<?, ?>) context)
          .get("printwriter");
      pw.print("java.util.Map ");
      pw.print(id);
      pw.println(" = new java.util.HashMap();");
      compositeCounters.set(context, new Integer(i.intValue() + 1));
      return id;
    }
    Integer i = primitiveCounters.get(context);
    if (i == null) i = new Integer(0);
    final String id = "P" + i;
    final PrintWriter pw = (PrintWriter) ((Map<?, ?>) context)
        .get("printwriter");
    pw.print(contentDesc);
    pw.print(' ');
    pw.print(id);
    pw.print(" = new ");
    pw.print(contentDesc);
    pw.println("();");
    primitiveCounters.set(context, new Integer(i.intValue() + 1));
    return id;
  }
}
