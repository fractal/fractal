/***
 * Fractal ADL Parser
 * Copyright (C) 2007-2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.arguments;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ArgumentHelper {

  protected static final String LPARENT   = "(";
  protected static final String RPARENT   = ")";

  protected static final String COMMA     = ",";

  protected static final String LREF      = "${";
  protected static final String RREF      = "}";

  protected static final char   QUOTE     = '\'';
  protected static final char   BACKSLASH = '\\';

  private ArgumentHelper() {
  }

  public static List<String> splitNameList(final String nameList) {
    final List<String> result = new ArrayList<String>();
    String currentName = "";
    int openedDelimiter = 0;
    for (final String token : tokenize(nameList)) {
      if (LPARENT.equals(token)) {
        openedDelimiter++;
        currentName += token;
      } else if (RPARENT.equals(token)) {
        if (openedDelimiter == 0) {
          // parse error
          return null;
        }
        openedDelimiter--;
        currentName += token;
      } else if (COMMA.equals(token)) {
        if (openedDelimiter == 0) {
          result.add(currentName.trim());
          currentName = "";
        } else {
          currentName += token;
        }
      } else {
        currentName += token;
      }
    }
    result.add(currentName.trim());

    return result;
  }

  public static String[] splitNameRef(final String line) {
    String prefix = null;
    String refName = null;

    String currentName = "";
    int openedDelimiter = 0;
    for (final String token : tokenize(line)) {
      if (refName == null && LREF.equals(token)) {
        if (prefix == null) {
          prefix = currentName;
          currentName = "";
        } else {
          currentName += token;
        }
        openedDelimiter++;
      } else if (refName == null && RREF.equals(token)) {
        if (openedDelimiter == 0) {
          // parse error
          return null;
        }
        openedDelimiter--;
        if (openedDelimiter == 0) {
          refName = currentName;
          currentName = "";
        } else {
          currentName += token;
        }
      } else {
        currentName += token;
      }
    }

    if (prefix == null) {
      return new String[]{currentName};
    } else if (refName == null) {
      // parse error
      return null;
    } else {
      return new String[]{prefix, refName, currentName};
    }
  }

  protected static LinkedList<String> tokenize(final String line) {
    final LinkedList<String> result = new LinkedList<String>();
    final Matcher matcher = Pattern.compile(
        "('([^'\\\\]|\\\\')*')|,|=| |\\(|\\)|(\\$\\{)|\\}").matcher(line);
    int i = 0;
    while (matcher.find()) {
      if (i != matcher.start()) result.add(line.substring(i, matcher.start()));
      result.add(matcher.group());
      i = matcher.end();
    }
    if (i != line.length()) result.add(line.substring(i));
    return result;
  }

  public static String unquote(final String quoted) {

    final StringBuilder sb = new StringBuilder();
    boolean inQuote = false;
    final int length = quoted.length();
    for (int i = 0; i < length; i++) {
      final char c = quoted.charAt(i);
      if (c == QUOTE) {
        inQuote = !inQuote;
      } else if (inQuote && c == BACKSLASH) {
        if (i == length - 1) {
          // parse error
          return null;
        }
        final char n = quoted.charAt(i + 1);
        if (n == QUOTE) {
          sb.append(n);
          i++;
        } else {
          sb.append(c);
        }
      } else {
        sb.append(c);
      }
    }

    if (inQuote) {
      // parse error
      return null;
    }
    return sb.toString();
  }
}
