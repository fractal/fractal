/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl;

import static org.objectweb.fractal.adl.error.ErrorTemplateValidator.validErrorTemplate;

import org.objectweb.fractal.adl.error.ErrorTemplate;

/** {@link ErrorTemplate} group for the adl package. */
public enum ADLErrors implements ErrorTemplate {

  /** Template used when an ADL file can't be found. */
  ADL_NOT_FOUND("ADL not found \"%s\"", "adlname"),

  /** Error template used when an error occurs while executing task graph. */
  TASK_EXECUTION_ERROR("Unable to execute some of the tasks"),

  /** */
  WRONG_DEFINITION_NAME(
      "Wrong definition name \"%s\" was expected instead of \"%s\".",
      "expected", "actual"),

  /** */
  PARSE_ERROR("Parse error: %s", "detail"),

  /** */
  IO_ERROR("Can't read file \"%s\"", "filename"),

  ;

  /** The groupId of ErrorTemplates defined in this enumeration. */
  public static final String GROUP_ID = "ADL";

  private int                id;
  private String             format;

  private ADLErrors(final String format, final Object... args) {
    this.id = ordinal();
    this.format = format;

    assert validErrorTemplate(this, args);
  }

  public int getErrorId() {
    return id;
  }

  public String getGroupId() {
    return GROUP_ID;
  }

  public String getFormatedMessage(final Object... args) {
    return String.format(format, args);
  }

  public String getFormat() {
    return format;
  }
}
