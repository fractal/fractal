/***
 * Fractal ADL Parser
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl;

import java.util.Map;

import org.objectweb.fractal.adl.implementations.ImplementationCodeLoader;
import org.objectweb.fractal.adl.implementations.ImplementationErrors;
import org.objectweb.fractal.adl.interfaces.IDLLoader;
import org.objectweb.fractal.adl.interfaces.InterfaceErrors;

/**
 * Java implementation of {@link ImplementationCodeLoader} and {@link IDLLoader}
 * interfaces for Java component.
 */
public class JavaCodeLoader implements ImplementationCodeLoader, IDLLoader {

  // --------------------------------------------------------------------------
  // Implementation of the ImplementationCodeLoader interface
  // --------------------------------------------------------------------------

  public Object loadImplementation(final String signature,
      final String language, final Map<Object, Object> context)
      throws ADLException {
    ClassLoader loader = null;
    if (context != null) {
      loader = (ClassLoader) context.get("classloader");
    }
    if (loader == null) {
      loader = getClass().getClassLoader();
    }
    try {
      return loader.loadClass(signature);
    } catch (final ClassNotFoundException e) {
      throw new ADLException(ImplementationErrors.IMPLEMENTATION_NOT_FOUND, e,
          signature);
    }
  }

  // --------------------------------------------------------------------------
  // Implementation of the IDLLoader interface
  // --------------------------------------------------------------------------

  public Object loadInterface(final String signature,
      final Map<Object, Object> context) throws ADLException {
    ClassLoader loader = null;
    if (context != null) {
      loader = (ClassLoader) context.get("classloader");
    }
    if (loader == null) {
      loader = getClass().getClassLoader();
    }
    try {
      return loader.loadClass(signature);
    } catch (final ClassNotFoundException e) {
      throw new ADLException(InterfaceErrors.INTERFACE_NOT_FOUND, e, signature);
    }
  }

}
