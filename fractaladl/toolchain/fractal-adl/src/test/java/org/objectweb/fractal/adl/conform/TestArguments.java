/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.conform;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.ContextMap;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.adl.arguments.ArgumentComponentLoader;
import org.objectweb.fractal.adl.arguments.ArgumentErrors;
import org.objectweb.fractal.adl.arguments.ArgumentLoader;
import org.objectweb.fractal.adl.error.ChainedErrorLocator;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.adl.merger.NodeMergerImpl;
import org.objectweb.fractal.adl.xml.XMLLoader;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;

public class TestArguments extends Test {

  public Loader              l;

  public Map<Object, Object> context;

  @Override
  protected void setUp() {
    context = ContextMap.instance(); // new HashMap<Object, Object>();

    final ArgumentLoader pv = new ArgumentLoader();
    final ArgumentComponentLoader compv = new ArgumentComponentLoader();

    final XMLLoader xl = new XMLLoader(false);
    xl.nodeFactoryItf = new XMLNodeFactoryImpl();
    pv.clientLoader = xl;
    compv.clientLoader = pv;
    compv.nodeMergerItf = new NodeMergerImpl();
    l = compv;
  }

  public void test1() throws ADLException {
    context.put("p", "org.objectweb.fractal.adl.conform.components.IImpl");
    final Definition d1 = l.load("ARG1", context);
    final Definition d2 = l.load("ARG2", null);
    compare(d1, d2);
  }

  public void test2() {
    try {
      l.load("ARG1", context);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(ArgumentErrors.NO_VALUE, e.getError().getTemplate());
    }
  }

  public void test3() throws ADLException {
    context.put("p", "org.objectweb.fractal.adl.conform.components.IImpl");
    context.put("q", "ARG1");
    context.put("r", "primitive");
    final Definition d1 = l.load("ARG3", context);
    final Definition d2 = l.load("ARG4", null);
    compare(d1, d2);
  }

  public void test4() throws ADLException {
    context.put("p", "org.objectweb.fractal.adl.conform.components.IImpl");
    context.put("q", "org.objectweb.fractal.adl.conform.components.IImpl");
    final Definition d1 = l.load("ARG5", context);
    final Definition d2 = l.load("ARG6", null);
    compare(d1, d2);
  }

  public void test5() throws ADLException {
    context.put("u1", "org.objectweb.fractal.adl.conform.components.IImpl");
    context.put("v1", "org.objectweb.fractal.adl.conform.components.IImpl");
    context.put("u2", "org.objectweb.fractal.adl.conform.components.IImpl");
    context.put("v2", "org.objectweb.fractal.adl.conform.components.IImpl");
    final Definition d1 = l.load("ARG7", context);
    final Definition d2 = l.load("ARG8", null);
    compare(d1, d2);
  }

  public void test6() {
    try {
      context.put("u", "org.objectweb.fractal.adl.conform.components.IImpl");
      l.load("ARG9", context);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(ArgumentErrors.NO_VALUE, e.getError().getTemplate());
    }
  }

  public void test7() {
    try {
      context.put("u", "org.objectweb.fractal.adl.conform.components.IImpl");
      l.load("ARG10", context);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(ArgumentErrors.UNDEFINED_ARGUMENT, e.getError().getTemplate());
    }
  }

  public void test8() throws ADLException {
    context.put("impl", "org.objectweb.fractal.adl.conform.components.IImpl");
    final Definition d1 = l.load("ARG16", context);
    final Definition d2 = l.load("ARG17", null);
    compare(d1, d2);
  }

  public void test9() throws ADLException {
    context.put("impl", "org.objectweb.fractal.adl.conform.components.JImpl");
    final Definition d1 = l.load("ARG18", context);
    final Definition d2 = l.load("ARG17", null);
    compare(d1, d2);
  }

  public void test10() throws ADLException {
    final Definition d1 = l.load("ARG23", context);
    final Definition d2 = l.load("ARG24", null);
    compare(d1, d2);
  }

  public void testUsingDefaultArgumentValue() throws Exception {
    final Definition d = l.load("ARG11", context);
    assertEquals("org.objectweb.fractal.adl.conform.components.IImpl",
        ((ImplementationContainer) d).getImplementation().getClassName());
  }

  public void testOverridingDefaultArgumentValue() throws Exception {
    context.put("impl", "org.objectweb.fractal.adl.conform.components.JImpl");
    final Definition d = l.load("ARG11", context);
    assertEquals("org.objectweb.fractal.adl.conform.components.JImpl",
        ((ImplementationContainer) d).getImplementation().getClassName());
  }

  public void testOverridingNamedArgumentValueInAnExtendedDefinition()
      throws Exception {
    final Definition d = l.load("ARG12", context);
    assertEquals("org.objectweb.fractal.adl.conform.components.JImpl",
        ((ImplementationContainer) d).getImplementation().getClassName());
  }

  public void testOverridingDefaultArgumentValueInAnExtendedDefinition()
      throws Exception {
    final Definition d = l.load("ARG13", context);
    assertEquals("org.objectweb.fractal.adl.conform.components.JImpl",
        ((ImplementationContainer) d).getImplementation().getClassName());
  }

  public void testArgumentNameStartingWithJava() throws Exception {
    System.setProperty("p",
        "org.objectweb.fractal.adl.conform.components.JImpl");
    try {
      l.load("ARG15", context);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(ArgumentErrors.INVALID_ARGNAME, e.getError().getTemplate());
      assertTrue(e.getError().getLocator() instanceof ChainedErrorLocator);
      final ChainedErrorLocator locator = (ChainedErrorLocator) e.getError()
          .getLocator();
      assertEquals("ARG14.fractal", locator.getRootLocator().getInputFilePath());
      assertEquals("ARG15.fractal", locator.getChainedLocations().get(0)
          .getInputFilePath());
    }
  }

  public void testSystemProperty() throws Exception {
    System.setProperty("ARG19.impl",
        "org.objectweb.fractal.adl.conform.components.JImpl");
    final Definition d = l.load("ARG19", context);
    assertEquals("org.objectweb.fractal.adl.conform.components.JImpl",
        ((ImplementationContainer) d).getImplementation().getClassName());
  }

  public void testDefaultValueOfySystemProperty() throws Exception {
    final Definition d = l.load("ARG20", context);
    assertEquals("org.objectweb.fractal.adl.conform.components.IImpl",
        ((ImplementationContainer) d).getImplementation().getClassName());
  }

  public void testOverridingDefaultValueOfSystemProperty() throws Exception {
    System.setProperty("ARG20.impl",
        "org.objectweb.fractal.adl.conform.components.JImpl");
    final Definition d = l.load("ARG20", context);
    assertEquals("org.objectweb.fractal.adl.conform.components.JImpl",
        ((ImplementationContainer) d).getImplementation().getClassName());
  }

  public void testSystemPropertyAsDefaultValueOfSystemProperty()
      throws Exception {
    System.setProperty("ARG21.anotherImpl",
        "org.objectweb.fractal.adl.conform.components.JImpl");
    final Definition d = l.load("ARG21", context);
    assertEquals("org.objectweb.fractal.adl.conform.components.JImpl",
        ((ImplementationContainer) d).getImplementation().getClassName());
  }

  public void testQuotedArgumentValue() throws Exception {
    final Definition d = l.load("ARG30", context);
    assertEquals(" quoted impl ", ((ImplementationContainer) d)
        .getImplementation().getClassName());
  }

  public void testQuotedArgumentValue2() throws Exception {
    final Definition d = l.load("ARG31", context);
    assertEquals(" quoted impl ", ((ImplementationContainer) d)
        .getImplementation().getClassName());
  }

  public void testArgumentsWithWhiteSpaces() throws Exception {
    context.put("u1", "org.objectweb.fractal.adl.conform.components.IImpl");
    context.put("v1", "org.objectweb.fractal.adl.conform.components.IImpl");
    context.put("u2", "org.objectweb.fractal.adl.conform.components.IImpl");
    context.put("v2", "org.objectweb.fractal.adl.conform.components.IImpl");
    final Definition d1 = l.load("ARG7_WHITESPACES", context);
    final Definition d2 = l.load("ARG8", null);
    compare(d1, d2);
  }
}
