/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.error;

import static org.objectweb.fractal.adl.error.ErrorTemplateValidator.validErrorEnum;
import junit.framework.TestCase;

import org.objectweb.fractal.adl.ADLErrors;
import org.objectweb.fractal.adl.arguments.ArgumentErrors;
import org.objectweb.fractal.adl.attributes.AttributeErrors;
import org.objectweb.fractal.adl.bindings.BindingErrors;
import org.objectweb.fractal.adl.components.ComponentErrors;
import org.objectweb.fractal.adl.implementations.ImplementationErrors;
import org.objectweb.fractal.adl.interfaces.InterfaceErrors;
import org.objectweb.fractal.adl.types.TypeErrors;
import org.objectweb.fractal.adl.xml.XMLErrors;

public class ErrorTemplateTest extends TestCase {

  public void testADL() {
    assertTrue(validErrorEnum(ADLErrors.class));
  }

  public void testArgument() {
    assertTrue(validErrorEnum(ArgumentErrors.class));
  }

  public void testAttribute() {
    assertTrue(validErrorEnum(AttributeErrors.class));
  }

  public void testBinding() {
    assertTrue(validErrorEnum(BindingErrors.class));
  }

  public void testComponent() {
    assertTrue(validErrorEnum(ComponentErrors.class));
  }

  public void testError() {
    assertTrue(validErrorEnum(GenericErrors.class));
  }

  public void testImplementation() {
    assertTrue(validErrorEnum(ImplementationErrors.class));
  }

  public void testInterface() {
    assertTrue(validErrorEnum(InterfaceErrors.class));
  }

  public void testType() {
    assertTrue(validErrorEnum(TypeErrors.class));
  }

  public void testXML() {
    assertTrue(validErrorEnum(XMLErrors.class));
  }
}
