/**
 * 
 */
package org.objectweb.fractal.adl;

import java.util.Map;

import junit.framework.TestCase;

/**
 * @author Alessio Pace
 *
 */
public class LauncherTest extends TestCase {

	/**
	 * Test method for {@link org.objectweb.fractal.adl.Launcher#parseArgsAsMap(java.lang.String[])}.
	 */
	public void testParseArgsAsMap() {
		String factory = "BasicFactory";
		Map result = Launcher.parseArgsAsMap(new String[]{Launcher.PARAMETER_NAME_TRAILING_CHAR + Launcher.FACTORY_PROPERTY_NAME + Launcher.PARAMETER_NAME_VALUE_SEPARATOR_CHAR + factory});
		assertEquals(factory, result.get(Launcher.FACTORY_PROPERTY_NAME));
	}

}
