package org.objectweb.fractal.adl.conform;

import java.util.Map;

import junit.framework.TestCase;

import org.objectweb.fractal.adl.ContextMap;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.adl.Loader;

/**
 * A class to make integration tests of the BasicLoader composite component
 * containing all the primitive loader components.
 * 
 * @author Matthieu Leclercq, Alessio Pace
 *
 */
public class BasicLoaderTest extends TestCase {

	public final void testLoadExtendedDefinitionInheritingAttributesSignature() throws Exception {
		Factory bootstrapFactory = FactoryFactory.getFactory();
		
		Map contextMap = ContextMap.instance(); // new HashMap();
		Map basicLoaderComponent = (Map) bootstrapFactory.newComponent("org.objectweb.fractal.adl.BasicLoader", contextMap);
		Loader basicLoader = (Loader) basicLoaderComponent.get("loader");
		
		basicLoader.load("ATTR11", contextMap);
		
	}

}
