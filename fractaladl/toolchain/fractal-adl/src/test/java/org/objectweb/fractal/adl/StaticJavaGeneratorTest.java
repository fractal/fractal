/**
 * 
 */

package org.objectweb.fractal.adl;

import static org.objectweb.fractal.adl.StaticJavaBackendTest.instantiate;
import static org.objectweb.fractal.adl.StaticJavaBackendTest.load;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import java.util.Map;

import junit.framework.TestCase;

import org.eclipse.jdt.internal.compiler.CompilationResult;
import org.objectweb.fractal.adl.StaticJavaGenerator.GenerationException;
import org.objectweb.fractal.adl.StaticJavaGenerator.InvalidCommandLineException;
import org.objectweb.fractal.adl.conform.components.IImpl;

/**
 * @author leclercm
 */
public class StaticJavaGeneratorTest extends TestCase {

  File                  outputDir;
  protected JDTCompiler compiler;

  @Override
  protected void setUp() throws Exception {
    outputDir = createTempDir("StaticJavaGeneratorTest", "output");
    outputDir.deleteOnExit();
    compiler = new JDTCompiler();
  }

  public void test1() throws Exception {
    final String adlName = "IMPL2";
    final String className = adlName + StaticJavaGenerator.CLASSNAME_SUFFIX;

    final File outputFile = generate(adlName);
    assertEquals("The generated file do not have the expected name.", adlName
        + StaticJavaGenerator.CLASSNAME_SUFFIX + ".java", outputFile.getName());
    assertEquals("The generated file is not in the expected directory",
        outputDir, outputFile.getParentFile());

    final Object comp = instantiate(load(className, compile(outputFile,
        className)));

    assertTrue(comp instanceof IImpl);
  }

  public void testAutoGenerate() throws Exception {
    final String adlName = FactoryFactory.DEFAULT_FACTORY + '('
        + FactoryFactory.STATIC_JAVA_BACKEND + ')';
    final String className = FactoryFactory.DEFAULT_FACTORY
        + StaticJavaGenerator.CLASSNAME_SUFFIX;

    final File outputFile = generate(adlName);
    final Object comp = instantiate(load(className, compile(outputFile,
        className)));
    assertTrue(comp instanceof Map);
    assertNotNull(((Map<?, ?>) comp).get("factory"));
  }

  public void testList() throws Exception {
    final URL list = getClass().getClassLoader().getResource("adl-list.txt");
    final StaticJavaGenerator generator = new StaticJavaGenerator("-o="
        + outputDir, "-adls=" + list.getPath());
    generator.generate();
    final File outputFile1 = generator.getOutputFile("IMPL2");
    assertTrue("The generated file does not exist", outputFile1.exists());

    final File outputFile2 = generator
        .getOutputFile("org.objectweb.fractal.adl.BasicFactory(org.objectweb.fractal.adl.StaticJavaBackend)");
    assertTrue("The generated file does not exist", outputFile2.exists());
  }

  protected File generate(final String adlName) throws ADLException,
      InvalidCommandLineException, GenerationException {
    final StaticJavaGenerator generator = new StaticJavaGenerator("-o="
        + outputDir, adlName);
    generator.generate();
    final File outputFile = generator.getOutputFile(adlName);
    assertTrue("The generated file does not exist", outputFile.exists());
    return outputFile;
  }

  protected CompilationResult compile(final File sourceFile,
      final String className) throws Exception {
    final String sourceCode = getContent(sourceFile);
    final CompilationResult compilResult = compiler.compile(className,
        sourceCode.toCharArray());
    if (compilResult.hasErrors()) {
      System.out.println("Generated source file:");
      System.out.println(sourceFile);
      System.out.println("Compilation result:");
      System.out.println(compilResult);
      fail("The compilation of the generated class fails");
    }
    return compilResult;
  }

  protected static File createTempDir(final String prefix, final String suffix)
      throws IOException {
    final File tempFile = File.createTempFile(prefix, suffix);
    tempFile.delete();
    tempFile.mkdirs();
    return tempFile;
  }

  protected static String getContent(final File file) throws Exception {
    final StringWriter out = new StringWriter();
    final FileReader in = new FileReader(file);
    final char[] buf = new char[512];
    int read;
    while ((read = in.read(buf, 0, 512)) >= 0)
      out.write(buf, 0, read);
    return out.toString();
  }
}
