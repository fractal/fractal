/***
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.eclipse.jdt.internal.compiler.ClassFile;
import org.eclipse.jdt.internal.compiler.CompilationResult;
import org.objectweb.fractal.adl.conform.components.IImpl;

public class StaticJavaBackendTest extends TestCase {

  protected Factory     factory;
  protected JDTCompiler compiler;

  @Override
  protected void setUp() throws Exception {
    factory = FactoryFactory.getFactory(FactoryFactory.STATIC_JAVA_BACKEND);
    compiler = new JDTCompiler();
  }

  public void test1() throws Exception {
    final Object comp = generateCompileLoadAndInstantiate("IMPL2",
        new HashMap<Object, Object>());
    assertTrue(comp instanceof IImpl);
  }

  public void testAutoGenerate() throws Exception {
    final Map<Object, Object> context = new HashMap<Object, Object>();
    context.put(FactoryFactory.BACKEND_PROPERTY_NAME,
        FactoryFactory.STATIC_JAVA_BACKEND);
    final Object comp = generateCompileLoadAndInstantiate(
        FactoryFactory.DEFAULT_FACTORY, context);
    assertTrue(comp instanceof Map);
    assertNotNull(((Map<?, ?>) comp).get("factory"));
  }

  protected Object generateCompileLoadAndInstantiate(final String adlName,
      final Map<Object, Object> context) throws ADLException, Exception {
    final StringWriter writer = new StringWriter();
    final String className = generate(adlName, writer, context);

    final CompilationResult result = compile(writer.toString(), className);
    return instantiate(load(className, result));
  }

  protected String generate(final String adlName, final StringWriter writer,
      final Map<Object, Object> context) throws ADLException {
    final String className = generateHeader(adlName, writer);

    context.put("printwriter", new PrintWriter(writer));
    final Object result = factory.newComponent(adlName, context);

    generateFooter(result, writer);
    return className;
  }

  protected String generateHeader(final String adlName,
      final StringWriter writer) {
    final String className = adlName + "Factory";
    // Class header (specify package if needed)
    final int index = className.lastIndexOf(".");
    if (index != -1) {
      writer.append("package ").append(className.substring(0, index)).append(
          ";\n\n");
      writer.append("public class ").append(className.substring(index + 1));
    } else {
      writer.append("public class ").append(className);
    }

    writer.append(" implements ").append(JavaFactory.class.getName()).append(
        " {\n");
    writer.append("  public Object newComponent() throws Exception {\n");
    return className;
  }

  protected void generateFooter(final Object result, final StringWriter writer) {
    writer.append("  return ").append(result.toString()).append(";\n");
    writer.append("  }\n");
    writer.append("}\n");
  }

  protected CompilationResult compile(final String sourceCode,
      final String className) throws Exception {
    final CompilationResult compilResult = compiler.compile(className,
        sourceCode.toCharArray());
    if (compilResult.hasErrors()) {
      System.out.println("Generated source code:");
      System.out.println(sourceCode);
      System.out.println("Compilation result:");
      System.out.println(compilResult);
      fail("The compilation of the generated class fails");
    }
    return compilResult;
  }

  @SuppressWarnings("unchecked")
  protected static Class<? extends JavaFactory> load(final String className,
      final CompilationResult result) throws ClassNotFoundException {
    final ClassLoader cl = new CompiledClassLoader(result);
    return (Class<? extends JavaFactory>) cl.loadClass(className);
  }

  protected static Object instantiate(
      final Class<? extends JavaFactory> factoryClass) throws Exception {
    final JavaFactory javaFactory = factoryClass.newInstance();
    return javaFactory.newComponent();
  }

  protected static class CompiledClassLoader extends ClassLoader {

    protected CompiledClassLoader(final CompilationResult result) {
      for (final ClassFile classFile : result.getClassFiles()) {
        final StringBuilder name = new StringBuilder();
        final char[][] compoundName = classFile.getCompoundName();
        name.append(compoundName[0]);
        for (int i = 1; i < compoundName.length; i++) {
          name.append('.').append(compoundName[i]);
        }

        defineClass(name.toString(), classFile.getBytes(), 0, classFile
            .getBytes().length);
      }
    }
  }
}
