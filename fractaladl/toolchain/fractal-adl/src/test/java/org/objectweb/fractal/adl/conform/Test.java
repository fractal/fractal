/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.conform;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;

import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;
import org.objectweb.fractal.adl.xml.XMLWriter;

public abstract class Test extends TestCase {

  private static final String DTD_PREFIX   = "classpath://org/objectweb/fractal/adl/xml/";
  public static final String  BASIC_DTD    = DTD_PREFIX + "basic.dtd";
  public static final String  CORE_DTD     = DTD_PREFIX + "core.dtd";
  public static final String  STANDARD_DTD = DTD_PREFIX + "standard.dtd";

  public XMLNodeFactory newNodeFactory() {
    return new XMLNodeFactoryImpl();
  }

  public void print(final Node n) {
    try {
      final Writer w = new PrintWriter(System.err, true);
      new XMLWriter(w).write(n);
      w.flush();
    } catch (final Exception e) {
    }
  }

  public void compare(final Definition d, final Definition e) {
    compare((Node) d, (Node) e);
  }

  public void compare(final Node n, final Node m) {
    if (!n.astGetType().equals(m.astGetType())) {
      fail();
    }

    final Map nAttrs = n.astGetAttributes();
    final Map mAttrs = m.astGetAttributes();
    if (n.astGetType().equals("definition")) {
      mAttrs.put("name", nAttrs.get("name"));
    }
    Iterator it = nAttrs.keySet().iterator();
    while (it.hasNext()) {
      final Object key = it.next();
      if (nAttrs.get(key) == null) {
        it.remove();
      }
    }
    it = mAttrs.keySet().iterator();
    while (it.hasNext()) {
      final Object key = it.next();
      if (mAttrs.get(key) == null) {
        it.remove();
      }
    }
    if (!nAttrs.equals(mAttrs)) {
      fail();
    }

    final String[] nNames = n.astGetNodeTypes();
    final String[] mNames = m.astGetNodeTypes();
    final Set names = new HashSet();
    names.addAll(Arrays.asList(nNames));
    names.addAll(Arrays.asList(mNames));
    final Iterator i = names.iterator();
    while (i.hasNext()) {
      final String name = (String) i.next();
      Node[] nNodes = n.astGetNodes(name);
      if (nNodes == null) {
        nNodes = new Node[0];
      }
      Node[] mNodes = m.astGetNodes(name);
      if (mNodes == null) {
        mNodes = new Node[0];
      }
      if (nNodes.length == 0 && mNodes.length == 1 && mNodes[0] == null) {
        continue;
      }
      if (mNodes.length == 0 && nNodes.length == 1 && nNodes[0] == null) {
        continue;
      }
      if (nNodes.length != mNodes.length) {
        fail();
      }
      for (int j = 0; j < nNodes.length; ++j) {
        boolean ok = false;
        for (int k = 0; k < mNodes.length; ++k) {
          if (nNodes[j] == null) {
            if (mNodes[k] == null) {
              ok = true;
              break;
            }
          } else if (mNodes[k] != null) {
            try {
              compare(nNodes[j], mNodes[k]);
              ok = true;
              break;
            } catch (final Throwable t) {
            }
          }
        }
        if (!ok) {
          fail();
        }
      }
      for (int k = 0; k < mNodes.length; ++k) {
        boolean ok = false;
        for (int j = 0; j < nNodes.length; ++j) {
          if (mNodes[k] == null) {
            if (nNodes[j] == null) {
              ok = true;
              break;
            }
          } else if (nNodes[j] != null) {
            try {
              compare(nNodes[j], mNodes[k]);
              ok = true;
              break;
            } catch (final Throwable t) {
            }
          }
        }
        if (!ok) {
          fail();
        }
      }
    }
  }
}
