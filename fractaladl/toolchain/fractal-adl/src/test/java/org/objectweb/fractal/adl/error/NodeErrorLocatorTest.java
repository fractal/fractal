/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.error;

import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.conform.Test;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;

public class NodeErrorLocatorTest extends Test {

  XMLNodeFactory nodeFactory;

  @Override
  protected void setUp() throws Exception {
    nodeFactory = newNodeFactory();
  }

  public void testFullPath() throws Exception {
    final Node n = nodeFactory.newXMLNode(BASIC_DTD, "component");
    n.astSetSource("A.fractal:[1,2]-[3,4]");
    NodeErrorLocator locator = new NodeErrorLocator(n);
    assertEquals("A.fractal", locator.getInputFilePath());
    assertEquals(1, locator.getBeginLine());
    assertEquals(2, locator.getBeginColumn());
    assertEquals(3, locator.getEndLine());
    assertEquals(4, locator.getEndColumn());
    assertEquals("A.fractal:[1,2]-[3,4]", locator.getLocation());

    n.astSetSource("A.fractal:[11,22]-[33,44]");
    locator = new NodeErrorLocator(n);
    assertEquals("A.fractal", locator.getInputFilePath());
    assertEquals(11, locator.getBeginLine());
    assertEquals(22, locator.getBeginColumn());
    assertEquals(33, locator.getEndLine());
    assertEquals(44, locator.getEndColumn());
    assertEquals("A.fractal:[11,22]-[33,44]", locator.getLocation());
  }

  public void testPathLineAndColumn() throws Exception {
    final Node n = nodeFactory.newXMLNode(BASIC_DTD, "component");
    n.astSetSource("A.fractal:1-2");
    NodeErrorLocator locator = new NodeErrorLocator(n);
    assertEquals("A.fractal", locator.getInputFilePath());
    assertEquals(1, locator.getBeginLine());
    assertEquals(2, locator.getBeginColumn());
    assertEquals(-1, locator.getEndLine());
    assertEquals(-1, locator.getEndColumn());
    assertEquals("A.fractal:1-2", locator.getLocation());

    n.astSetSource("A.fractal:11-22");
    locator = new NodeErrorLocator(n);
    assertEquals("A.fractal", locator.getInputFilePath());
    assertEquals(11, locator.getBeginLine());
    assertEquals(22, locator.getBeginColumn());
    assertEquals(-1, locator.getEndLine());
    assertEquals(-1, locator.getEndColumn());
    assertEquals("A.fractal:11-22", locator.getLocation());
  }

  public void testPathAndLine() throws Exception {
    final Node n = nodeFactory.newXMLNode(BASIC_DTD, "component");
    n.astSetSource("A.fractal:1");
    final NodeErrorLocator locator = new NodeErrorLocator(n);
    assertEquals("A.fractal", locator.getInputFilePath());
    assertEquals(1, locator.getBeginLine());
    assertEquals(-1, locator.getBeginColumn());
    assertEquals(-1, locator.getEndLine());
    assertEquals(-1, locator.getEndColumn());
    assertEquals("A.fractal:1", locator.getLocation());
  }

  public void testPath() throws Exception {
    final Node n = nodeFactory.newXMLNode(BASIC_DTD, "component");
    n.astSetSource("A.fractal");
    final NodeErrorLocator locator = new NodeErrorLocator(n);
    assertEquals("A.fractal", locator.getInputFilePath());
    assertEquals(-1, locator.getBeginLine());
    assertEquals(-1, locator.getBeginColumn());
    assertEquals(-1, locator.getEndLine());
    assertEquals(-1, locator.getEndColumn());
    assertEquals("A.fractal", locator.getLocation());
  }

  public void testNullsource() throws Exception {
    final Node n = nodeFactory.newXMLNode(BASIC_DTD, "component");
    final NodeErrorLocator locator = new NodeErrorLocator(n);
    assertNull(locator.getInputFilePath());
    assertEquals(-1, locator.getBeginLine());
    assertEquals(-1, locator.getBeginColumn());
    assertEquals(-1, locator.getEndLine());
    assertEquals(-1, locator.getEndColumn());
    assertEquals("unknown", locator.getLocation());
  }

  public void testIllegalArgumentException() {
    try {
      new NodeErrorLocator(null);
      fail("An IllegalArgumentException was expected here");
    } catch (final IllegalArgumentException e) {
      // OK
    }
  }
}
