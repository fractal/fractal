/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.conform;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.components.ComponentLoader;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.adl.merger.NodeMergerImpl;
import org.objectweb.fractal.adl.xml.XMLLoader;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;

public class TestMix extends Test {

  public Loader l;

  protected void setUp () {
    ComponentLoader compLoader = new ComponentLoader();
    XMLLoader xl = new XMLLoader(false);
    xl.nodeFactoryItf = new XMLNodeFactoryImpl();
    compLoader.clientLoader = xl;
    compLoader.nodeMergerItf = new NodeMergerImpl();
    l = compLoader;
  }
  
  public void test1 () throws ADLException {
    l.load("MIX1", null);
    Definition d2 = l.load("MIX2", null);
    Definition d3 = l.load("MIX3", null);
    compare(d2, d3);
  }
 
  /**
   * Test case for bug #304723, see http://forge.objectweb.org/tracker/index.php?func=detail&aid=304723&group_id=22&atid=100022
   * 
   * The classes reported in the bug explanation have back introduced among the set of fixtures (A.fractal, B.fractal, CA.fractal, MainCA.fractal).
   */
  public void testSharingExtendingTopLevelDefinitionAndSubComponent() throws Exception {
	  ComponentContainer result = (ComponentContainer)l.load("MainCA", null);
	
	  Component[] subComponents = result.getComponents();
	  String className = ((ImplementationContainer)subComponents[0]).getImplementation().getClassName();
	  assertEquals("Assert sub component implementation comes from the B definition", "B", className);
  }
}
