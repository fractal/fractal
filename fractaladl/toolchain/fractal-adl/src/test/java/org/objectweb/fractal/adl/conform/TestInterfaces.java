/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.conform;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.adl.interfaces.InterfaceErrors;
import org.objectweb.fractal.adl.interfaces.InterfaceLoader;
import org.objectweb.fractal.adl.xml.XMLLoader;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;

public class TestInterfaces extends Test {

  public Loader l;

  @Override
  protected void setUp() {
    final InterfaceLoader itfLoader = new InterfaceLoader();
    final XMLLoader xl = new XMLLoader(false);
    xl.nodeFactoryItf = new XMLNodeFactoryImpl();
    itfLoader.clientLoader = xl;
    l = itfLoader;
  }

  public void test1() throws ADLException {
    assertNotNull(l.load("ITF1", null));
  }

  public void test2() {
    try {
      l.load("ITF2", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(InterfaceErrors.INTERFACE_NAME_MISSING, e.getError()
          .getTemplate());
    }
  }

  public void test3() {
    try {
      l.load("ITF3", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(InterfaceErrors.DUPLICATED_INTERFACE_NAME, e.getError()
          .getTemplate());
    }
  }
}
