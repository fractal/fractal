/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.conform;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.JavaCodeLoader;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.adl.implementations.ImplementationErrors;
import org.objectweb.fractal.adl.implementations.ImplementationLoader;
import org.objectweb.fractal.adl.xml.XMLLoader;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;

public class TestImplementations extends Test {

  public Loader l;

  @Override
  protected void setUp() {
    final ImplementationLoader implLoader = new ImplementationLoader();
    final JavaCodeLoader jcl = new JavaCodeLoader();
    final XMLLoader xl = new XMLLoader(false);
    xl.nodeFactoryItf = new XMLNodeFactoryImpl();
    implLoader.clientLoader = xl;
    implLoader.implementationCodeLoaderItf = jcl;
    implLoader.interfaceLoaderItf = jcl;
    l = implLoader;
  }

  public void test1() throws ADLException {
    assertNotNull(l.load("IMPL1", null));
  }

  public void test2() throws ADLException {
    assertNotNull(l.load("IMPL2", null));
  }

  public void test3() {
    try {
      l.load("IMPL3", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(ImplementationErrors.IMPLEMENTATION_NOT_FOUND, e.getError()
          .getTemplate());
    }
  }

  public void test4() {
    try {
      l.load("IMPL4", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(ImplementationErrors.CLASS_DOES_NOT_IMPLEMENT_INTERFACE, e
          .getError().getTemplate());
    }
  }
}
