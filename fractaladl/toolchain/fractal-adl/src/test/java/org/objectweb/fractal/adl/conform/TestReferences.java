/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.conform;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.adl.components.ComponentErrors;
import org.objectweb.fractal.adl.components.ComponentLoader;
import org.objectweb.fractal.adl.merger.NodeMergerImpl;
import org.objectweb.fractal.adl.xml.XMLLoader;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;

public class TestReferences extends Test {

  public Loader l;

  @Override
  protected void setUp() {
    final ComponentLoader compLoader = new ComponentLoader();
    final XMLLoader xl = new XMLLoader(false);
    xl.nodeFactoryItf = new XMLNodeFactoryImpl();
    compLoader.clientLoader = xl;
    compLoader.nodeMergerItf = new NodeMergerImpl();
    l = compLoader;
  }

  public void test1() throws ADLException {
    l.load("REF1", null);
    final Definition d2 = l.load("REF2", null);
    final Definition d3 = l.load("REF3", null);
    compare(d2, d3);
  }

  public void test2() throws ADLException {
    final Definition d1 = l.load("REF4", null);
    final Definition d2 = l.load("REF5", null);
    compare(d1, d2);
  }

  public void test3() throws ADLException {
    final Definition d1 = l.load("REF6", null);
    final Definition d2 = l.load("REF7", null);
    compare(d1, d2);
  }

  public void test4() throws ADLException {
    l.load("REF8", null);
    final Definition d2 = l.load("REF9", null);
    final Definition d3 = l.load("REF10", null);
    compare(d2, d3);
  }

  public void test5() throws ADLException {
    final Definition d1 = l.load("REF11", null);
    final Definition d2 = l.load("REF12", null);
    compare(d1, d2);
  }

  public void test6() throws ADLException {
    final Definition d1 = l.load("REF13", null);
    final Definition d2 = l.load("REF14", null);
    compare(d1, d2);
  }

  public void test7() throws ADLException {
    l.load("REF15", null);
    final Definition d2 = l.load("REF16", null);
    final Definition d3 = l.load("REF17", null);
    compare(d2, d3);
  }

  public void test8() throws ADLException {
    try {
      l.load("REF18", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(ComponentErrors.DEFINITION_CYCLE, e.getError().getTemplate());
    }
  }

  public void test9() throws ADLException {
    try {
      l.load("REF19", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(ComponentErrors.DEFINITION_CYCLE, e.getError().getTemplate());
    }
  }
}
