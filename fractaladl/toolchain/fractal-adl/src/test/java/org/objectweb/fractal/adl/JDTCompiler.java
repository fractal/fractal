/***
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.internal.compiler.CompilationResult;
import org.eclipse.jdt.internal.compiler.ICompilerRequestor;
import org.eclipse.jdt.internal.compiler.IErrorHandlingPolicy;
import org.eclipse.jdt.internal.compiler.batch.FileSystem;
import org.eclipse.jdt.internal.compiler.classfmt.ClassFileConstants;
import org.eclipse.jdt.internal.compiler.env.ICompilationUnit;
import org.eclipse.jdt.internal.compiler.impl.CompilerOptions;
import org.eclipse.jdt.internal.compiler.problem.DefaultProblemFactory;

import spoon.support.util.BasicCompilationUnit;

/**
 * Compiler utility class used by {@link StaticJavaBackendTest} to compile
 * generated source code.
 */
public class JDTCompiler implements ICompilerRequestor {

  CompilerOptions         compilerOption;
  List<CompilationResult> results;

  public static ICompilationUnit getUnit(final String name, final char[] source)
      throws Exception {

    final String[] tmp = name.split("[.]");
    final char[][] pack = new char[tmp.length - 1][];

    for (int i = 0; i < tmp.length - 1; i++) {
      pack[i] = tmp[i].toCharArray();
    }

    return new BasicCompilationUnit(source, pack, name.replace('.',
        File.separatorChar)
        + ".java");
  }

  public CompilationResult compile(final String name, final char[] source)
      throws Exception {
    final List<CompilationResult> r = compile(getUnit(name, source));
    return r.iterator().next();
  }

  public synchronized List<CompilationResult> compile(
      final ICompilationUnit... units) {
    results = new ArrayList<CompilationResult>();

    final org.eclipse.jdt.internal.compiler.Compiler compiler = new org.eclipse.jdt.internal.compiler.Compiler(
        getLibraryAccess(), getHandlingPolicy(), getCompilerOption(), this,
        new DefaultProblemFactory());
    compiler.compile(units);
    return results;
  }

  protected CompilerOptions getCompilerOption() {
    if (compilerOption == null) {
      compilerOption = new CompilerOptions();
      compilerOption.sourceLevel = ClassFileConstants.JDK1_5;
      compilerOption.suppressWarnings = true;
    }
    return compilerOption;
  }

  protected IErrorHandlingPolicy getHandlingPolicy() {
    return new IErrorHandlingPolicy() {
      public boolean proceedOnErrors() {
        return true; // stop if there are some errors
      }

      public boolean stopOnFirstError() {
        return false;
      }
    };
  }

  protected FileSystem getLibraryAccess() {
    final String bootpath = System.getProperty("sun.boot.class.path");
    final String classpath = System.getProperty("java.class.path");
    final String surefireClasspath = System
        .getProperty("surefire.test.class.path");
    final List<String> lst = new ArrayList<String>();
    for (final String s : bootpath.split(File.pathSeparator)) {
      final File f = new File(s);
      if (f.exists()) {
        lst.add(f.getAbsolutePath());
      }
    }
    for (final String s : classpath.split(File.pathSeparator)) {
      final File f = new File(s);
      if (f.exists()) {
        lst.add(f.getAbsolutePath());
      }
    }
    if (surefireClasspath != null)
      for (final String s : surefireClasspath.split(File.pathSeparator)) {
        final File f = new File(s);
        if (f.exists()) {
          lst.add(f.getAbsolutePath());
        }
      }
    return new FileSystem(lst.toArray(new String[0]), new String[0], System
        .getProperty("file.encoding"));
  }

  // ---------------------------------------------------------------------------
  // Implementation of the ICompilerRequestor interface
  // ---------------------------------------------------------------------------

  public void acceptResult(final CompilationResult result) {
    results.add(result);
  }

}
