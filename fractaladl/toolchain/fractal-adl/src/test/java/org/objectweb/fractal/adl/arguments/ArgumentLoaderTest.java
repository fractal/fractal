/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.arguments;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.adl.conform.Test;
import org.objectweb.fractal.adl.error.Error;
import org.objectweb.fractal.adl.error.ErrorTemplate;
import org.objectweb.fractal.adl.error.NodeErrorLocator;
import org.objectweb.fractal.adl.xml.XMLNodeFactory;

public class ArgumentLoaderTest extends Test {

  static final String PROP_NAME = ArgumentLoaderTest.class.getName() + ".prop";

  ArgumentLoader      al;
  XMLNodeFactory      nodeFactory;

  Map<Object, Object> context;
  Map<String, Object> values;
  ArgumentDefinition  node;

  @Override
  protected void setUp() throws Exception {
    al = new ArgumentLoader();
    nodeFactory = newNodeFactory();

    context = new HashMap<Object, Object>();
    context.put("ctx1", "ctx-value");
    context.put("ctx2", new Integer(1));
    context.put(ArgumentLoader.ARGUMENT_CONTEXT_PREFIX + "arg1", "value1");
    context.put(ArgumentLoader.ARGUMENT_CONTEXT_PREFIX + "arg2", "value2");
    context.put(ArgumentLoader.ARGUMENT_CONTEXT_PREFIX + "arg3", null);

    values = new HashMap<String, Object>();
    values.put("arg1", "value1");
    values.put("arg2", "value2");

    System.setProperty(PROP_NAME, "prop-value");

    node = (ArgumentDefinition) nodeFactory.newXMLNode(STANDARD_DTD,
        "definition");
    node.astSetSource("A.fractal:1-2");

    al.clientLoader = new Loader() {
      public Definition load(final String name,
          final Map<Object, Object> context) throws ADLException {
        return node;
      }
    };
  }

  void assertException(final ADLException e, final ErrorTemplate template,
      final Object... args) {
    final Error error = e.getError();
    assertNotNull(error);

    assertEquals(template, error.getTemplate());

    assertTrue(error.getLocator() instanceof NodeErrorLocator);
    final NodeErrorLocator locator = (NodeErrorLocator) error.getLocator();
    assertEquals("A.fractal", locator.getInputFilePath());
    assertEquals(1, locator.getBeginLine());
    assertEquals(2, locator.getBeginColumn());
    assertEquals(-1, locator.getEndLine());
    assertEquals(-1, locator.getEndColumn());

    assertEquals(error.getMessage(), template.getFormatedMessage(args));
  }

  // ---------------------------------------------------------------------------
  // "evaluate" validation tests
  // ---------------------------------------------------------------------------

  public void testEvaluateSimpleArg() throws Exception {
    final String s = al.evaluate("${arg1}", values, node, context);
    assertEquals("value1", s);
  }

  public void testEvaluateSimpleArgWithPrefixAndSuffix() throws Exception {
    final String s = al.evaluate("prefix${arg1}suffix", values, node, context);
    assertEquals("prefixvalue1suffix", s);
  }

  public void testEvaluateContextArg() throws Exception {
    final String s = al.evaluate("${context:ctx1}", values, node, context);
    assertEquals("ctx-value", s);
  }

  public void testEvaluateContextArgWithDefaultValue() throws Exception {
    final String s = al.evaluate("${context:notdefined=default}", values, node,
        context);
    assertEquals("default", s);
  }

  public void testEvaluateJavaPropertyArg() throws Exception {
    final String s = al.evaluate("${java:" + PROP_NAME + "}", values, node,
        context);
    assertEquals("prop-value", s);
  }

  public void testLoadSimpleArg() throws Exception {
    node.setArguments("arg1,arg2");
    node.setName("${arg1}");
    al.load("Foo", context);
    assertEquals("value1", node.getName());
  }

  public void testLoadSimpleArgWithDefaultValue1() throws Exception {
    node.setArguments("arg1=default,arg2");
    node.setName("${arg1}");
    al.load("Foo", context);
    assertEquals("value1", node.getName());
  }

  public void testLoadSimpleArgWithDefaultValue2() throws Exception {
    node.setArguments("arg3=default,arg2");
    node.setName("${arg3}");
    al.load("Foo", context);
    assertEquals("default", node.getName());
  }

  // ---------------------------------------------------------------------------
  // Error validation tests
  // ---------------------------------------------------------------------------

  public void testEvaluateUnkownPrefix() throws Exception {
    try {
      final String s = al.evaluate("${invalid:arg1}", values, node, context);
      fail("An ADLException was expected here");
    } catch (final ADLException e) {
      assertException(e, ArgumentErrors.UNKNOWN_PREFIX, "invalid",
          "invalid:arg1");
    }
  }

  public void testEvaluateUndefinedArgument1() throws Exception {
    try {
      final String s = al.evaluate("${context:invalid}", values, node, context);
      fail("An ADLException was expected here");
    } catch (final ADLException e) {
      assertException(e, ArgumentErrors.UNDEFINED_ARGUMENT, "invalid");
    }
  }

  public void testEvaluateInvalidNotAString() throws Exception {
    try {
      final String s = al.evaluate("${context:ctx2}", values, node, context);
      fail("An ADLException was expected here");
    } catch (final ADLException e) {
      assertException(e, ArgumentErrors.INVALID_VALUE_NOT_A_STRING, "ctx2");
    }
  }

  public void testEvaluateUndefinedArgument2() throws Exception {
    try {
      final String s = al.evaluate("${invalid}", values, node, context);
      fail("An ADLException was expected here");
    } catch (final ADLException e) {
      assertException(e, ArgumentErrors.UNDEFINED_ARGUMENT, "invalid");
    }
  }

  public void testEvaluateInvalidDefaultValue() throws Exception {
    try {
      final String s = al.evaluate("${arg1=default}", values, node, context);
      fail("An ADLException was expected here");
    } catch (final ADLException e) {
      assertException(e, ArgumentErrors.INVALID_PATTERN_DEFAULT_VALUE, "arg1");
    }
  }

  public void testLoadEmptyArgumentName() throws Exception {
    node.setArguments("arg1,=def");
    try {
      al.load("Foo", context);
      fail("An ADLException was expected here");
    } catch (final ADLException e) {
      assertException(e, ArgumentErrors.EMPTY_ARGUMENT_NAME, "arg1,=def");
    }
  }

  public void testLoadInvalidArgumentName1() throws Exception {
    node.setArguments("a:rg1");
    try {
      al.load("Foo", context);
      fail("An ADLException was expected here");
    } catch (final ADLException e) {
      assertException(e, ArgumentErrors.INVALID_ARGNAME, "a:rg1", ":");
    }
  }

  public void testLoadInvalidArgumentName2() throws Exception {
    node.setArguments("a$rg1");
    try {
      al.load("Foo", context);
      fail("An ADLException was expected here");
    } catch (final ADLException e) {
      assertException(e, ArgumentErrors.INVALID_ARGNAME, "a$rg1", "$");
    }
  }

  public void testLoadNoValue() throws Exception {
    node.setArguments("arg3");
    try {
      al.load("Foo", context);
      fail("An ADLException was expected here");
    } catch (final ADLException e) {
      assertException(e, ArgumentErrors.NO_VALUE, "arg3");
    }
  }
}
