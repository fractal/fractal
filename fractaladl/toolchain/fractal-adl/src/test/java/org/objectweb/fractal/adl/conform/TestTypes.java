/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.conform;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.JavaCodeLoader;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.adl.components.ComponentLoader;
import org.objectweb.fractal.adl.error.ChainedErrorLocator;
import org.objectweb.fractal.adl.interfaces.InterfaceErrors;
import org.objectweb.fractal.adl.merger.NodeMergerImpl;
import org.objectweb.fractal.adl.types.TypeErrors;
import org.objectweb.fractal.adl.types.TypeLoader;
import org.objectweb.fractal.adl.xml.XMLLoader;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;

public class TestTypes extends Test {

  public Loader l;

  @Override
  protected void setUp() {
    final TypeLoader typLoader = new TypeLoader();
    final ComponentLoader cl = new ComponentLoader();
    final JavaCodeLoader jcl = new JavaCodeLoader();
    final XMLLoader xl = new XMLLoader(false);
    xl.nodeFactoryItf = new XMLNodeFactoryImpl();
    cl.clientLoader = xl;
    cl.nodeMergerItf = new NodeMergerImpl();
    typLoader.clientLoader = cl;
    typLoader.interfaceCodeLoaderItf = jcl;
    l = typLoader;
  }

  public void test1() throws ADLException {
    assertNotNull(l.load("TYP1", null));
  }

  public void test2() throws ADLException {
    try {
      l.load("TYP2", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(TypeErrors.SIGNATURE_MISSING, e.getError().getTemplate());
    }
  }

  public void test3() {
    try {
      l.load("TYP3", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(InterfaceErrors.INTERFACE_NOT_FOUND, e.getError()
          .getTemplate());
      assertTrue(e.getError().getLocator() instanceof ChainedErrorLocator);
      final ChainedErrorLocator locator = (ChainedErrorLocator) e.getError()
          .getLocator();
      assertNull(locator.getRootLocator());
      assertEquals("TYP3.fractal", locator.getChainedLocations().get(0)
          .getInputFilePath());
    }
  }

  public void test4() {
    try {
      l.load("TYP4", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(TypeErrors.ROLE_MISSING, e.getError().getTemplate());
    }
  }

  public void test5() {
    try {
      l.load("TYP5", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(TypeErrors.INVALID_ROLE, e.getError().getTemplate());
    }
  }

  public void test6() {
    try {
      l.load("TYP6", null);
      fail();
    } catch (final ADLException e) {
    }
  }

  public void test7() {
    try {
      l.load("TYP7", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(TypeErrors.INVALID_CONTINGENCY, e.getError().getTemplate());
    }
  }

  public void test8() throws ADLException {
    assertNotNull(l.load("TYP8", null));
  }

}
