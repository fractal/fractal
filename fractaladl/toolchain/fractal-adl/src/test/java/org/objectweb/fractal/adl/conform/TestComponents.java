/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.conform;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.adl.components.ComponentErrors;
import org.objectweb.fractal.adl.components.ComponentLoader;
import org.objectweb.fractal.adl.merger.NodeMergerImpl;
import org.objectweb.fractal.adl.xml.XMLLoader;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;

public class TestComponents extends Test {

  public Loader l;

  @Override
  protected void setUp() {
    final ComponentLoader compLoader = new ComponentLoader();
    final XMLLoader xl = new XMLLoader(false);
    xl.nodeFactoryItf = new XMLNodeFactoryImpl();
    compLoader.clientLoader = xl;
    compLoader.nodeMergerItf = new NodeMergerImpl();
    l = compLoader;
  }

  public void test1() throws ADLException {
    l.load("COMP1", null);
  }

  /*
   * public void test2 () throws ADLException { // this feature has been removed
   * Definition d1 = l.load("COMP2", null); Definition d2 = l.load("COMP3",
   * null); compare(d1, d2); }
   */

  public void test3() throws ADLException {
    try {
      l.load("COMP4", null);
      fail();
    } catch (final ADLException e) {
      assertSame(ComponentErrors.DUPLICATED_COMPONENT_NAME, e.getError()
          .getTemplate());
    }
  }
}
