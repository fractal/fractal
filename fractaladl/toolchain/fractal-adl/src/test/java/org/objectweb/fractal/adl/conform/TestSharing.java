/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributors: Alessio Pace, Philippe Merle
 *
 * $Id$
 */

package org.objectweb.fractal.adl.conform;

import java.util.HashSet;
import java.util.Set;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.components.ComponentErrors;
import org.objectweb.fractal.adl.components.ComponentLoader;
import org.objectweb.fractal.adl.implementations.Controller;
import org.objectweb.fractal.adl.implementations.ControllerContainer;
import org.objectweb.fractal.adl.implementations.Implementation;
import org.objectweb.fractal.adl.implementations.ImplementationContainer;
import org.objectweb.fractal.adl.merger.NodeMergerImpl;
import org.objectweb.fractal.adl.xml.XMLLoader;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;

public class TestSharing extends Test {
  
  public Loader l;

  protected void setUp () {
    ComponentLoader compLoader = new ComponentLoader();
    XMLLoader xl = new XMLLoader(false);
    xl.nodeFactoryItf = new XMLNodeFactoryImpl();
    compLoader.clientLoader = xl;
    compLoader.nodeMergerItf = new NodeMergerImpl();
    l = compLoader;
  }
  
  public void test1 () throws ADLException {
    ComponentContainer d = (ComponentContainer)l.load("SHARING1", null);
    assertEquals("Asserting number of 'a' components", 1, getComponents(d, "a", new HashSet()).size());
    assertEquals("Asserting number of 'b' components", 1, getComponents(d, "b", new HashSet()).size());
    assertEquals("Asserting number of 'c' components", 1, getComponents(d, "c", new HashSet()).size());
    assertEquals("Asserting number of 'd' components", 1, getComponents(d, "d", new HashSet()).size());
  }
  
  public void test2 () throws ADLException {
    ComponentContainer d = (ComponentContainer)l.load("SHARING2", null);
    assertEquals("Asserting number of 'a' components", 1, getComponents(d, "a", new HashSet()).size());
    assertEquals("Asserting number of 'b' components", 1, getComponents(d, "b", new HashSet()).size());
    assertEquals("Asserting number of 'c' components", 1, getComponents(d, "c", new HashSet()).size());
    assertEquals("Asserting number of 'd' components", 1, getComponents(d, "d", new HashSet()).size());
    assertEquals("Asserting number of 'e' components", 1, getComponents(d, "e", new HashSet()).size());
  }
  
  public void test3 () throws ADLException {
    ComponentContainer d = (ComponentContainer)l.load("SHARING4", null);
    assertEquals("Asserting number of 'a' components", 1, getComponents(d, "a", new HashSet()).size());
    assertEquals("Asserting number of 'b' components", 1, getComponents(d, "b", new HashSet()).size());
  }

  public void test4 () throws ADLException {
    ComponentContainer d = (ComponentContainer)l.load("SHARING5", null);
    assertEquals("Asserting number of 'a' components", 1, getComponents(d, "a", new HashSet()).size());
    assertEquals("Asserting number of 'b' components", 1, getComponents(d, "b", new HashSet()).size());
  }

  public void test5 () throws ADLException {
    ComponentContainer d = (ComponentContainer)l.load("SHARING6", null);
    assertEquals("Asserting number of 'a' components", 1, getComponents(d, "a", new HashSet()).size());
    assertEquals("Asserting number of 'b' components", 1, getComponents(d, "b", new HashSet()).size());
  }

  public void test6 () throws ADLException {
    ComponentContainer d = (ComponentContainer)l.load("SHARING11", null);
    Set aComponents = getComponents(d, "a", new HashSet());
    assertEquals("Asserting number of 'a' components", 1, aComponents.size());
    Component a = (Component) aComponents.iterator().next();
    Controller aCtrl = ((ControllerContainer) a).getController();
    assertNotNull("Asserting component 'a' has a controller desc", aCtrl);
    assertEquals("Asserting 'a' controller desc", "primitive", aCtrl.getDescriptor());
  }

  public void test7 () throws ADLException {
    ComponentContainer d = (ComponentContainer)l.load("SHARING12", null);
    Set aComponents = getComponents(d, "a", new HashSet());
    assertEquals("Asserting number of 'a' components", 1, aComponents.size());
    Component a = (Component) aComponents.iterator().next();
    Controller aCtrl = ((ControllerContainer) a).getController();
    assertNotNull("Asserting component 'a' has a controller desc", aCtrl);
    assertEquals("Asserting 'a' controller desc", "primitive", aCtrl.getDescriptor());
  }

  public void test8 () throws ADLException {
    ComponentContainer d = (ComponentContainer)l.load("SHARING14", null);
    Set aComponents = getComponents(d, "a", new HashSet());
    assertEquals("Asserting number of 'a' components", 1, aComponents.size());
    Component a = (Component) aComponents.iterator().next();
    assertNotNull("Asserting component 'a' has a controller desc", ((ControllerContainer) a).getController());
    assertNotNull("Asserting component 'a' has an implementation", ((ImplementationContainer) a).getImplementation());
  }

  public void testSharingWithExtendedTopLevelDefinition() throws ADLException {
    ComponentContainer d = (ComponentContainer)l.load("SHARING7", null);
    assertEquals("Asserting number of 'a' components", 1, getComponents(d, "a", new HashSet()).size());
    assertEquals("Asserting number of 'b' components", 1, getComponents(d, "b", new HashSet()).size());
  }

  public void testSharingWithExtendedSubComponentDefinition() throws ADLException {
    ComponentContainer d = (ComponentContainer)l.load("SHARING9", null);
    assertEquals("Asserting number of 'a' components", 1, getComponents(d, "a", new HashSet()).size());
    assertEquals("Asserting number of 'b' components", 1, getComponents(d, "b", new HashSet()).size());
    assertEquals("Asserting number of 'foo' components", 1, getComponents(d, "foo", new HashSet()).size());
  }

  public void testSharingWithDifferentOrderOfSubComponents() throws ADLException {
    ComponentContainer d = (ComponentContainer)l.load("SHARING8", null);
    assertEquals("Asserting number of 'a' components", 1, getComponents(d, "a", new HashSet()).size());
    assertEquals("Asserting number of 'b' components", 1, getComponents(d, "b", new HashSet()).size());
    assertEquals("Asserting number of 'c' components", 1, getComponents(d, "c", new HashSet()).size());
  }

  public void testInvalidSharedComponentPath () {
    try {
      l.load("SHARING10", null);
      fail();
    } catch (ADLException e) {
      assertNotNull(e.getError());
      assertSame(ComponentErrors.INVALID_PATH, e.getError().getTemplate());
    } catch (NullPointerException e) {
      fail();
    }
  }
  private Set getComponents (final ComponentContainer c, final String n, final Set r) {
    if (c instanceof Component && ((Component)c).getName().equals(n)) {
      r.add(c);
    }
    Component[] comps = c.getComponents();
    for (int i = 0; i < comps.length; ++i) {
      getComponents(comps[i], n, r);
    }
    return r;
  }

}
