/***
 * Fractal ADL Parser
 * Copyright (C) 2005 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Ali Erdem Ozcan
 */

package org.objectweb.fractal.adl.util;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import junit.framework.TestCase;

public class FractalADLLogManagerTest extends TestCase {

  Properties            savedProperties;
  PrintStream           savedSysout;
  OutputStream          savedFileStream;
  ByteArrayOutputStream consoleStream;
  ByteArrayOutputStream fileStream;

  @Override
  protected void setUp() throws Exception {
    savedProperties = System.getProperties();
    final Properties newProperties = new Properties();
    for (final Map.Entry<Object, Object> entry : savedProperties.entrySet()) {
      if (entry.getKey() instanceof String) {
        final String propName = (String) entry.getKey();
        if (propName.endsWith(FractalADLLogManager.CONSOLE_LEVEL_SUFFIX)
            || propName.endsWith(FractalADLLogManager.FILE_LEVEL_SUFFIX))
          continue;
      }
      newProperties.put(entry.getKey(), entry.getValue());
    }
    System.setProperties(newProperties);

    savedSysout = System.out;
    consoleStream = new ByteArrayOutputStream();
    System.setOut(new PrintStream(consoleStream, /* autoFlush */true));

    savedFileStream = FractalADLLogManager.SingletonFileHandler.outputStream;
    fileStream = new ByteArrayOutputStream();
    FractalADLLogManager.SingletonFileHandler.outputStream = new PrintStream(
        fileStream, /* autoFlush */true);
  }

  @Override
  protected void tearDown() throws Exception {
    System.setProperties(savedProperties);
    System.setOut(savedSysout);
    FractalADLLogManager.SingletonFileHandler.outputStream = savedFileStream;
  }

  /**
   * Tests the level of verbose level of the
   * {@link FractalADLLogManager.ROOT_LOGGER_NAME} logger.
   */
  public void testLevelOfRootLogger() {
    final Logger logger = getRootLogger();

    final String consoleLevelProperty = System
        .getProperty(FractalADLLogManager.DEFAULT_CONSOLE_LEVEL_PROPERTY_NAME);
    final Level consoleLevel;
    if (consoleLevelProperty != null)
      consoleLevel = Level.parse(consoleLevelProperty);
    else
      consoleLevel = FractalADLLogManager.DEFAULT_CONSOLE_LEVEL;

    final String fileLevelProperty = System
        .getProperty(FractalADLLogManager.DEFAULT_FILE_LEVEL_PROPERTY_NAME);
    final Level fileLevel;
    if (fileLevelProperty != null)
      fileLevel = Level.parse(fileLevelProperty);
    else
      fileLevel = FractalADLLogManager.DEFAULT_FILE_LEVEL;

    assertLoggerLevels(logger, consoleLevel, fileLevel);
  }

  /**
   * Tests the name of a newly created logger. Indeed, the created logger name
   * must be prefixed by {@link FractalADLLogManager.ROOT_LOGGER_NAME} in order
   * to make the latter logger parent of each logger.
   */
  public void testLoggerName() {
    final String loggerName = "testLoggerName";
    final Logger logger = FractalADLLogManager.getLogger(loggerName);
    assertEquals(FractalADLLogManager.ROOT_LOGGER_NAME + "." + loggerName,
        logger.getName());
    assertSame(getRootLogger(), logger.getParent());
    assertLoggerLevels(logger, getConsoleLevel(getRootLogger()),
        getFileLevel(getRootLogger()));
  }

  /**
   * Tests the console handler verbose level of a created logger to check if it
   * correctly inherits the verbose level value coming from its parent.
   */
  public void testLoggerLevelWithInheritance() {
    final String loggerName = "testLoggerLevelWithInheritance";
    final String subLoggerName = loggerName + ".subLogger";
    System.setProperty(loggerName + FractalADLLogManager.CONSOLE_LEVEL_SUFFIX,
        "FINER");

    final Logger logger = FractalADLLogManager.getLogger(loggerName);
    assertLoggerLevels(logger, Level.FINER, getFileLevel(getRootLogger()));

    System.setProperty(subLoggerName + FractalADLLogManager.FILE_LEVEL_SUFFIX,
        "FINEST");
    final Logger subLogger = FractalADLLogManager.getLogger(subLoggerName);
    assertLoggerLevels(subLogger, Level.FINER, Level.FINEST);
  }

  public void testLoggerOutput() {
    final String loggerName = "testLoggerOutput";
    final String subLoggerName = loggerName + ".subLogger";

    System.setProperty(loggerName + FractalADLLogManager.CONSOLE_LEVEL_SUFFIX,
        "FINE");
    System.setProperty(loggerName + FractalADLLogManager.FILE_LEVEL_SUFFIX,
        "INFO");

    System.setProperty(subLoggerName
        + FractalADLLogManager.CONSOLE_LEVEL_SUFFIX, "FINER");
    System.setProperty(subLoggerName + FractalADLLogManager.FILE_LEVEL_SUFFIX,
        "WARNING");

    final Logger logger = FractalADLLogManager.getLogger(loggerName);
    final Logger subLogger = FractalADLLogManager.getLogger(subLoggerName);

    logger.log(Level.INFO, "msg1");
    String expectedOutput = "[" + loggerName + "] msg1\n";
    assertOutput(expectedOutput, expectedOutput);

    logger.log(Level.FINE, "msg2");
    expectedOutput = "[" + loggerName + "] msg2\n";
    assertOutput(expectedOutput, null);

    subLogger.log(Level.INFO, "msg3");
    expectedOutput = "[" + subLoggerName + "] msg3\n";
    assertOutput(expectedOutput, null);

    subLogger.log(Level.WARNING, "msg4");
    expectedOutput = "[" + subLoggerName + "] msg4\n";
    assertOutput(expectedOutput, expectedOutput);
  }

  private void assertOutput(final String consoleMessage,
      final String fileMessage) {
    if (consoleMessage != null) {
      assertEquals(consoleMessage, consoleStream.toString());
    } else {
      assertEquals(0, consoleStream.size());
    }
    if (fileMessage != null) {
      assertEquals(fileMessage, fileStream.toString());
    } else {
      assertEquals(0, fileStream.size());
    }
    consoleStream.reset();
    fileStream.reset();
  }

  protected Logger getRootLogger() {
    return FractalADLLogManager
        .getLogger(FractalADLLogManager.ROOT_LOGGER_NAME);
  }

  protected Level getConsoleLevel(final Logger logger) {
    return assertLoggerHasHandler(logger,
        FractalADLLogManager.StdOutConsoleHandler.class).getLevel();
  }

  protected Level getFileLevel(final Logger logger) {
    return assertLoggerHasHandler(logger,
        FractalADLLogManager.SingletonFileHandler.class).getLevel();
  }

  protected void assertLoggerLevels(final Logger logger,
      final Level consoleLevel, final Level fileLevel) {
    assertEquals(consoleLevel, assertLoggerHasHandler(logger,
        FractalADLLogManager.StdOutConsoleHandler.class).getLevel());
    assertEquals(fileLevel, assertLoggerHasHandler(logger,
        FractalADLLogManager.SingletonFileHandler.class).getLevel());
    if (consoleLevel.intValue() < fileLevel.intValue())
      assertEquals(consoleLevel, logger.getLevel());
    else
      assertEquals(fileLevel, logger.getLevel());
  }

  protected Handler assertLoggerHasHandler(final Logger logger,
      final Class<? extends Handler> handlerClass) {
    Handler handler = null;
    for (final Handler h : logger.getHandlers()) {
      if (h.getClass().equals(handlerClass)) {
        handler = h;
        break;
      }
    }
    assertNotNull("No hanndler of type " + handlerClass.getName()
        + " found on the '" + logger.getName() + "' logger.", handler);
    return handler;
  }
}
