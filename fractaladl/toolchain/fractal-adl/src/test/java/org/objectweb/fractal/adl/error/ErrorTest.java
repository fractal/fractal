/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.error;

import junit.framework.TestCase;

public class ErrorTest extends TestCase {

  /**
   * Test method for {@link Error#Error(ErrorTemplate, Object[])}.
   */
  public void testErrorErrorTemplateObjectArray() {
    final Error e = new Error(GenericErrors.GENERIC_ERROR, "foo");
    assertNotNull(e);
    assertNotNull(e.template);
    assertEquals("foo", e.message);
    assertNull(e.locator);
    assertNull(e.cause);
    assertSame(GenericErrors.GENERIC_ERROR, e.getTemplate());
    assertNotNull(e.toString());

    try {
      new Error(null, "foo");
      fail("An IllegalArgumentException was expected");
    } catch (final IllegalArgumentException iae) {
      // OK
    }

    try {
      new Error(GenericErrors.GENERIC_ERROR, (Object[]) null);
      fail("An IllegalArgumentException was expected");
    } catch (final IllegalArgumentException iae) {
      // OK
    }
  }

  /**
   * Test method for {@link Error#Error(ErrorTemplate, ErrorLocator, Object[])}.
   */
  public void testErrorErrorTemplateErrorLocatorObjectArray() {
    final Error e = new Error(GenericErrors.GENERIC_ERROR,
        new BasicErrorLocator("bar", 1, 2), "foo");
    assertNotNull(e);
    assertNotNull(e.template);
    assertEquals("foo", e.message);
    assertEquals("bar", e.locator.getInputFilePath());
    assertEquals(1, e.getLocator().getBeginLine());
    assertEquals(2, e.getLocator().getBeginColumn());
    assertEquals(-1, e.getLocator().getEndLine());
    assertEquals(-1, e.getLocator().getEndLine());
    assertNull(e.cause);
    assertSame(GenericErrors.GENERIC_ERROR, e.getTemplate());
    assertNotNull(e.toString());

    try {
      new Error(null, new BasicErrorLocator("bar", 1, 2), "foo");
      fail("An IllegalArgumentException was expected");
    } catch (final IllegalArgumentException iae) {
      // OK
    }

    try {
      new Error(GenericErrors.GENERIC_ERROR,
          new BasicErrorLocator("bar", 1, 2), (Object[]) null);
      fail("An IllegalArgumentException was expected");
    } catch (final IllegalArgumentException iae) {
      // OK
    }
  }

  /**
   * Test method for {@link Error#Error(ErrorTemplate, Throwable, Object[])}.
   */
  public void testErrorErrorTemplateThrowableObjectArray() {
    final Exception exception = new Exception();
    final Error e = new Error(GenericErrors.GENERIC_ERROR, exception, "foo");
    assertNotNull(e);
    assertNotNull(e.template);
    assertEquals("foo", e.message);
    assertSame(exception, e.cause);
    assertSame(GenericErrors.GENERIC_ERROR, e.getTemplate());
    assertNotNull(e.toString());

    try {
      new Error(null, exception, "foo");
      fail("An IllegalArgumentException was expected");
    } catch (final IllegalArgumentException iae) {
      // OK
    }

    try {
      new Error(GenericErrors.GENERIC_ERROR, exception, (Object[]) null);
      fail("An IllegalArgumentException was expected");
    } catch (final IllegalArgumentException iae) {
      // OK
    }
  }

  /**
   * Test method for
   * {@link Error#Error(ErrorTemplate, ErrorLocator, Throwable, Object[])}.
   */
  public void testErrorErrorTemplateErrorLocatorThrowableObjectArray() {
    final Exception exception = new Exception();
    final Error e = new Error(GenericErrors.GENERIC_ERROR,
        new BasicErrorLocator("bar", 1, 2), exception, "foo");
    assertNotNull(e);
    assertNotNull(e.template);
    assertEquals("foo", e.message);
    assertEquals("bar", e.locator.getInputFilePath());
    assertEquals(1, e.getLocator().getBeginLine());
    assertEquals(2, e.getLocator().getBeginColumn());
    assertEquals(-1, e.getLocator().getEndLine());
    assertEquals(-1, e.getLocator().getEndLine());
    assertSame(exception, e.cause);
    assertSame(GenericErrors.GENERIC_ERROR, e.getTemplate());
    assertNotNull(e.toString());

    new Error(GenericErrors.GENERIC_ERROR, (ErrorLocator) null,
        (Throwable) null, "foo");
    try {
      new Error(null, new BasicErrorLocator("bar", 1, 2), exception, "foo");
      fail("An IllegalArgumentException was expected");
    } catch (final IllegalArgumentException iae) {
      // OK
    }

    try {
      new Error(GenericErrors.GENERIC_ERROR,
          new BasicErrorLocator("bar", 1, 2), (Object[]) null);
      fail("An IllegalArgumentException was expected");
    } catch (final IllegalArgumentException iae) {
      // OK
    }
  }

  /**
   * Test method for {@link Error#toString()}.
   */
  public void testToString() {
    final Exception exception = new Exception("Cause");
    Error e = new Error(GenericErrors.GENERIC_ERROR, new BasicErrorLocator(
        "bar.fractal", 1, 2), exception, "foo");
    String s = e.toString();
    assertNotNull(s);
    assertTrue(s.contains(GenericErrors.GENERIC_ERROR.getGroupId()));
    assertTrue(s.contains(String.format("%03d", GenericErrors.GENERIC_ERROR
        .getErrorId())));
    assertTrue(s.contains("foo"));
    assertTrue(s.contains("bar.fractal"));
    assertTrue(s.contains("1"));
    assertTrue(s.contains("2"));
    assertTrue(s.contains("Cause"));

    e = new Error(GenericErrors.GENERIC_ERROR, (ErrorLocator) null, exception,
        "foo");
    s = e.toString();
    assertNotNull(s);
    assertTrue(s.contains(GenericErrors.GENERIC_ERROR.getGroupId()));
    assertTrue(s.contains(String.format("%03d", GenericErrors.GENERIC_ERROR
        .getErrorId())));
    assertTrue(s.contains("foo"));
    assertTrue(s.contains("Cause"));

    e = new Error(GenericErrors.GENERIC_ERROR, new BasicErrorLocator(
        "bar.fractal", 1, 2), (Throwable) null, "foo");
    s = e.toString();
    assertNotNull(s);
    assertTrue(s.contains(GenericErrors.GENERIC_ERROR.getGroupId()));
    assertTrue(s.contains(String.format("%03d", GenericErrors.GENERIC_ERROR
        .getErrorId())));
    assertTrue(s.contains("foo"));
    assertTrue(s.contains("bar.fractal"));
    assertTrue(s.contains("1"));
    assertTrue(s.contains("2"));
  }
}
