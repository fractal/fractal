/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.conform;

import java.util.Map;

import org.objectweb.fractal.adl.ContextMap;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;

public class TestAutoCreate extends Test {
  
  
  public void testBootstrapFactoryAndCreateJavaBackendFactory () throws Exception {
	/* get the bootstrap factory */  
    final Factory boostrapFactory = FactoryFactory.getFactory();
    assertNotNull("Assert bootstrap factory not null", boostrapFactory);
    Map ctxt = ContextMap.instance(); // new HashMap();
    ctxt.put(
      FactoryFactory.BACKEND_PROPERTY_NAME,
      FactoryFactory.JAVA_BACKEND);
    // creates a new parser component with the bootstrap one 
    final Map factoryWithJavaBackendComponent = (Map)boostrapFactory.newComponent(FactoryFactory.DEFAULT_FACTORY, ctxt);
    final Factory factoryWithJavaBackend = (Factory)factoryWithJavaBackendComponent.get("factory");
    assertNotNull("Assert first created 'factory' not null", boostrapFactory);
    // uses this new parser to check that it has been correctly created
    final Map resultMap = (Map)factoryWithJavaBackend.newComponent(FactoryFactory.DEFAULT_FACTORY, ctxt);
    final Factory resultFactory = (Factory)resultMap.get("factory");
    assertNotNull("Assert second created 'factory' not null", resultFactory);
  }
  
  public void testFractalBackend() throws Exception {
	  Factory f = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
	  assertNotNull("Assert Factory with Fractal backend is not null", f);
  }

}
