/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.conform;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Loader;
import org.objectweb.fractal.adl.bindings.BindingErrors;
import org.objectweb.fractal.adl.bindings.BindingLoader;
import org.objectweb.fractal.adl.xml.XMLLoader;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;

public class TestBindings extends Test {

  public Loader l;

  @Override
  protected void setUp() {
    final BindingLoader bindLoader = new BindingLoader();
    final XMLLoader xl = new XMLLoader(false);
    xl.nodeFactoryItf = new XMLNodeFactoryImpl();
    bindLoader.clientLoader = xl;
    l = bindLoader;
  }

  public void test1() throws ADLException {
    assertNotNull(l.load("BIND1", null));
  }

  public void test2() {
    try {
      l.load("BIND2", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(BindingErrors.INVALID_ITF_NO_SUCH_COMPONENT, e.getError()
          .getTemplate());
    }
  }

  public void test3() {
    try {
      l.load("BIND3", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(BindingErrors.INVALID_ITF_NO_SUCH_INTERFACE, e.getError()
          .getTemplate());
    }
  }

  public void test4() {
    try {
      l.load("BIND4", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(BindingErrors.INVALID_ITF_NO_SUCH_COMPONENT, e.getError()
          .getTemplate());
    }
  }

  public void test5() {
    try {
      l.load("BIND5", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(BindingErrors.INVALID_ITF_NO_SUCH_INTERFACE, e.getError()
          .getTemplate());
    }
  }

  public void test6() {
    try {
      l.load("BIND6", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(BindingErrors.DUPLICATED_BINDING, e.getError().getTemplate());
    }
  }

  public void test7() {
    try {
      l.load("BIND7", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(BindingErrors.MISSING_FROM, e.getError().getTemplate());
    }
  }

  public void test8() {
    try {
      l.load("BIND8", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(BindingErrors.MISSING_TO, e.getError().getTemplate());
    }
  }

  public void test9() {
    try {
      l.load("BIND9", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(BindingErrors.INVALID_FROM_SYNTAX, e.getError().getTemplate());
    }
  }

  public void test10() {
    try {
      l.load("BIND10", null);
      fail();
    } catch (final ADLException e) {
      assertNotNull(e.getError());
      assertSame(BindingErrors.INVALID_TO_SYNTAX, e.getError().getTemplate());
    }
  }
}
