/***
 * Fractal ADL Parser
 * Copyright (C) 2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.arguments;

import static org.objectweb.fractal.adl.arguments.ArgumentHelper.splitNameList;
import static org.objectweb.fractal.adl.arguments.ArgumentHelper.splitNameRef;
import static org.objectweb.fractal.adl.arguments.ArgumentHelper.tokenize;

import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;
import junit.framework.TestCase;

public class ArgumentHelperTest extends TestCase {

  // ---------------------------------------------------------------------------
  // test ArgumentHelper.splitNameList method
  // ---------------------------------------------------------------------------

  public void test01() {
    splitListAndCheck("a,b,c", "a", "b", "c");
  }

  public void test02() {
    splitListAndCheck("a(),b(),c()", "a()", "b()", "c()");
  }

  public void test03() {
    splitListAndCheck("a(1,2),b(),c()", "a(1,2)", "b()", "c()");
  }

  public void test04() {
    splitListAndCheck("a(1,2(foo,bar)),b(),c()", "a(1,2(foo,bar))", "b()",
        "c()");
  }

  public void test05() {
    splitListAndCheck("a(1,2(foo,bar)),b(1,2),c", "a(1,2(foo,bar))", "b(1,2)",
        "c");
  }

  public void test06() {
    assertNull(splitNameList("a,b),c"));
  }

  public void test07() {
    assertNull(splitNameList("a()),b,c"));
  }

  // ---------------------------------------------------------------------------
  // test ArgumentHelper.splitNameRef method
  // ---------------------------------------------------------------------------

  public void test10() {
    splitRefAndCheck("foo", "foo");
  }

  public void test11() {
    splitRefAndCheck("${a}", "", "a", "");
  }

  public void test12() {
    splitRefAndCheck("foo${a}bar", "foo", "a", "bar");
  }

  public void test13() {
    splitRefAndCheck("foo${a=${b}}bar", "foo", "a=${b}", "bar");
  }

  public void test14() {
    splitRefAndCheck("foo${a}bar${b}", "foo", "a", "bar${b}");
  }

  // ---------------------------------------------------------------------------
  // test ArgumentHelper.tokenize method
  // ---------------------------------------------------------------------------

  public void test20() {
    splitTokenAndCheck("a,b", "a", ",", "b");
  }

  public void test21() {
    splitTokenAndCheck("a, b", "a", ",", " ", "b");
  }

  public void test22() {
    splitTokenAndCheck(",a,b", ",", "a", ",", "b");
  }

  public void test23() {
    splitTokenAndCheck("${a}", "${", "a", "}");
  }

  public void test24() {
    splitTokenAndCheck("'a'", "'a'");
  }

  public void test25() {
    splitTokenAndCheck("'a','b'", "'a'", ",", "'b'");
  }

  public void test26() {
    splitTokenAndCheck("'a,b'", "'a,b'");
  }

  public void test27() {
    splitTokenAndCheck("a'b,c' d", "a", "'b,c'", " ", "d");
  }

  // ---------------------------------------------------------------------------
  // test ArgumentHelper.unquote method
  // ---------------------------------------------------------------------------

  public void test30() {
    assertEquals("a", ArgumentHelper.unquote("'a'"));
  }

  public void test31() {
    assertEquals("ab", ArgumentHelper.unquote("a'b'"));
  }

  public void test32() {
    assertEquals("ab", ArgumentHelper.unquote("'a'b"));
  }

  public void test33() {
    assertEquals("abc", ArgumentHelper.unquote("a'b'c"));
  }

  public void test34() {
    assertEquals("a'b", ArgumentHelper.unquote("a'\\''b"));
  }

  public void test35() {
    assertEquals("'\\n", ArgumentHelper.unquote("'\\'\\n'"));
  }

  private static List<String> splitListAndCheck(final String nameList,
      final String... expecteds) {
    final List<String> l = splitNameList(nameList);
    Assert.assertEquals(Arrays.asList(expecteds), l);
    return l;
  }

  private static String[] splitRefAndCheck(final String nameList,
      final String... expecteds) {
    final String[] s = splitNameRef(nameList);
    if (s == null && expecteds != null) fail();
    assertEquals(Arrays.asList(expecteds), Arrays.asList(s));
    return s;
  }

  private static List<String> splitTokenAndCheck(final String nameList,
      final String... expecteds) {
    final List<String> s = tokenize(nameList);
    if (s == null && expecteds != null) fail();
    assertEquals(Arrays.asList(expecteds), s);
    return s;
  }
}
