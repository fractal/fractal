This is the FractalADL toolchain project.

The project is built with Maven, here there are some useful commands
if you are new to it:

 * "mvn clean test"       , to clean, compile and launch tests.
   
   XXX NOTE: tests might not run if run directly from Eclipse or another IDE, because before the test phase
   Maven extract files and set some System properties.
 
 
 * "mvn clean site:site"  , to clean, compile, run tests and generate a local site
        under the directory target/site 
 
        
 * "mvn clean install"    , to clean, compile, make a jar and copy it into your 
        local Maven repository (on Unix, ${HOME}/.m2/repository)
 
        
 * "mvn clean assembly:assembly -DdescriptorId=jar-with-dependencies" , will make
        a jar containing FractalADL + all the classes from its dependencies.      