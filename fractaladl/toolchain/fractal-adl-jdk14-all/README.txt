This module serves the purpose of providing a nice way to include a JDK 1.4 binary distribution release of
FractalADL inside the global build process performed by Maven.

This binary distribution contains some, but not all, of the fractal-adl dependencies, specifically it
retrotranslates only ast-core, fractal-adl, task-framework and task-deployment. All the others dependencies
are not included, but are referenced from this project pom, so simply depending on it will be enough.

#####################################
        Lifecycle details

* The "mvn compile" is useless here, since there are no sources.

* The "mvn package" instead produces a jar which is made by the the FractalADL JDK 5.0 version classes, 
retrotranslated to 1.4 version. Additionally, the pom.xml declares a runtime dependency to
the retrotranslator-runtime, so if your Maven project depend on fractal-adl-jdk14 you will have
transitively also the retrotranslator runtime needed to use the retrofitted jar.



#####################################
         HOW TO DEPEND ON IT

<!-- your pom.xml -->
<project>
  <!-- ..... -->
  
  <dependencies>
  
    <dependency>
      <groupId>org.objectweb.fractal.fractaladl</groupId>
      <artifactId>fractal-adl-jdk14-all</artifactId</artifactId>
      <version>PUT_THE_VERSION</artifactId>
    </dependency>
    
    <!-- .... -->
  
  </dependencies>
</project>