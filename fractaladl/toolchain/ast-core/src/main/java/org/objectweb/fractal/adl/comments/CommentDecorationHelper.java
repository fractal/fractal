/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.comments;

import org.objectweb.fractal.adl.Node;

/**
 * Provides helper methods to manipulate {@link CommentDecoration}.
 */
public final class CommentDecorationHelper {

  private CommentDecorationHelper() {
  }

  /**
   * The decoration name used to attach {@link CommentDecoration} to nodes.
   */
  public static final String COMMENT_DECORATION_NAME = "comment";

  /**
   * Attach the given comment to the given node.
   * 
   * @param node a node.
   * @param comment the comment to attach.
   * @throws IllegalArgumentException if the given node already has a decoration
   *           called {@link #COMMENT_DECORATION_NAME}.
   */
  public static void setComment(final Node node, final String comment) {
    final Object o = node.astGetDecoration(COMMENT_DECORATION_NAME);
    if (o != null)
      throw new IllegalArgumentException("Given node already contains a \""
          + COMMENT_DECORATION_NAME + "\" decoration.");

    node.astSetDecoration(COMMENT_DECORATION_NAME, new CommentDecoration(
        comment, node));
  }

  /**
   * Return the comment that is attached to the given node.
   * 
   * @param node a node.
   * @return the comment that is attached to the given node, or
   *         <code>null</code> if the given node has no decoration called
   *         {@link #COMMENT_DECORATION_NAME}.
   * @throws IllegalArgumentException if the given node has a decoration called
   *           {@link #COMMENT_DECORATION_NAME} that is not a
   *           {@link CommentDecoration}.
   */
  public static String getComment(final Node node) {
    final Object deco = node.astGetDecoration(COMMENT_DECORATION_NAME);
    if (deco == null) return null;
    if (!(deco instanceof CommentDecoration))
      throw new IllegalArgumentException("Given node contains a \""
          + COMMENT_DECORATION_NAME
          + "\" decoration that is not a CommentDecoration.");
    return ((CommentDecoration) deco).getComment();
  }

  /**
   * Return the {@link CommentDecoration} object that is attached to the given
   * node.
   * 
   * @param node a node.
   * @return the {@link CommentDecoration} object that is attached to the given
   *         node, or <code>null</code> if the given node has no decoration
   *         called {@link #COMMENT_DECORATION_NAME}.
   * @throws IllegalArgumentException if the given node has a decoration called
   *           {@link #COMMENT_DECORATION_NAME} that is not a
   *           {@link CommentDecoration}.
   */
  public static CommentDecoration getCommentDecoration(final Node node) {
    final Object deco = node.astGetDecoration(COMMENT_DECORATION_NAME);
    if (deco == null) return null;
    if (!(deco instanceof CommentDecoration))
      throw new IllegalArgumentException("Given node contains a \""
          + COMMENT_DECORATION_NAME
          + "\" decoration that is not a CommentDecoration.");
    return (CommentDecoration) deco;
  }
}
