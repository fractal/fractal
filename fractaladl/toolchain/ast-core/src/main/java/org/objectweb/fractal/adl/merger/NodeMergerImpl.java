/***
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.merger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Type;
import org.objectweb.fractal.adl.AbstractNode;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.NodeClassLoader;

/**
 * Implementation of {@link NodeMerger} interface.
 */
public class NodeMergerImpl implements NodeMerger {

  /**
   * The parent class loader used by {@link #mergeClassLoader}.
   */
  protected ClassLoader                                    classLoader;

  /**
   * The class loader used to merge AST node classes. This class merging is
   * necessary when merging several ASTs with different sources (eg different
   * DTDs).
   */
  protected MergeClassLoader                               mergeClassLoader;

  /**
   * A map that associate a node class to arity informations.
   */
  protected final Map<Class<?>, Map<String, SubNodeArity>> subNodeArityCache = new IdentityHashMap<Class<?>, Map<String, SubNodeArity>>();

  // ---------------------------------------------------------------------------
  // Constructor
  // ---------------------------------------------------------------------------

  /**
   * Default constructor.
   */
  public NodeMergerImpl() {
    classLoader = getClass().getClassLoader();
    if (classLoader == null) {
      classLoader = ClassLoader.getSystemClassLoader();
    }

    mergeClassLoader = new MergeClassLoader(classLoader);
  }

  // ---------------------------------------------------------------------------
  // Implementation of the NodeMerger interface
  // ---------------------------------------------------------------------------

  public ClassLoader getClassLoader() {
    return classLoader;
  }

  public void setClassLoader(final ClassLoader loader) {
    if (loader != classLoader) {
      classLoader = loader;
      mergeClassLoader = new MergeClassLoader(classLoader);
    }
  }

  public Node merge(final Node elem, final Node superElem,
      final Map<String, String> idAttributes) throws MergeException {
    final Map<Node, MergeInfo> infos = new HashMap<Node, MergeInfo>();

    // compute merge info
    final MergeInfo elemInfo = computeMergeInfos(elem, superElem, infos,
        idAttributes);

    // create the merged nodes
    for (final MergeInfo info : infos.values()) {
      info.createResultNode(mergeClassLoader);
    }

    // initialize the merged nodes
    final Node n = initMergedNodes(elemInfo, infos);

    return n;
  }

  /*
   * May be overridden by sub classes to use another base class for merged
   * nodes.
   */
  protected Class<? extends AbstractNode> getBaseClass() {
    return AbstractNode.class;
  }

  protected MergeInfo computeMergeInfos(final Node elem, final Node superElem,
      final Map<Node, MergeInfo> infos, final Map<String, String> idAttributes)
      throws MergeException {
    MergeInfo info = infos.get(elem);
    if (info == null) {
      info = infos.get(superElem);
      if (info != null) {
        // 'elem' is not known but 'superElem' is (i.e. 'superElem' is shared)
        // this means that 'elem' is in fact shared with 'info.elem'
        info.resetElem(elem);
      } else { // info == null
        info = new MergeInfo(elem);
        info.addSuperNode(superElem);
        infos.put(superElem, info);
      }
      infos.put(elem, info);
    } else if (info.addSuperNode(superElem)) {
      // superNode added
      infos.put(superElem, info);
    } else {
      // superNode already in the set of the superNodes; it has already been
      // merged, simply return
      return info;
    }

    // build the set of the type of the sub nodes.
    final Set<String> nodeTypes = new HashSet<String>();
    nodeTypes.addAll(Arrays.asList(elem.astGetNodeTypes()));
    nodeTypes.addAll(Arrays.asList(superElem.astGetNodeTypes()));

    // for each type of sub nodes
    for (final String nodeType : nodeTypes) {
      // get the sub nodes of 'superNode' of type 'nodeType'
      Node[] superNodes = superElem.astGetNodes(nodeType);
      // get the sub nodes of 'elem' of type 'nodeType'
      Node[] nodes = elem.astGetNodes(nodeType);

      // if there is no node to merge, continue with the next 'nodeType'.
      if ((superNodes == null || superNodes.length == 0)
          && (nodes == null || nodes.length == 0)) continue;

      if (superNodes == null) superNodes = new Node[0];
      if (nodes == null) nodes = new Node[0];

      // check and get the arity of 'nodeType'
      final SubNodeArity arity = checkArityCompatibility(elem, superElem,
          nodeType);

      if (arity == SubNodeArity.ONE) {
        // arity of sub nodes is 'ONE'. check that there is not more that one
        // node in 'superNodes' and in 'Nodes'
        assert superNodes.length <= 1;
        assert nodes.length <= 1;

        final Node node = (nodes.length > 0) ? nodes[0] : null;
        final Node superNode = (superNodes.length > 0) ? superNodes[0] : null;

        if (superNode != null) {
          // there is a node in 'superNodes'
          if (node == null) {
            // there is no node in 'nodes'
            computeInhertedSubNodeMergeInfos(superNode, info, nodeType, infos);
          } else {
            // there is a node both in 'nodes' and 'superNodes'. merge them
            computeMergedSubNodesMergeInfos(node, superNode, info, infos,
                idAttributes, nodeType);
          }
        } else if (node != null) {
          computeSubNodeMergeInfos(node, info, nodeType, infos);
        }
      } else { // arity == SubNodeArity.MANY
        final Set<Node> nodeSet = new LinkedHashSet<Node>();
        for (final Node node : nodes)
          nodeSet.add(node);

        // For each superNode, find a node among 'nodes' that overrides it.
        for (final Node superNode : superNodes) {
          final Node overridingNode = findOverridingNode(superNode, nodes,
              nodeType, idAttributes);

          if (overridingNode != null) {
            computeMergedSubNodesMergeInfos(overridingNode, superNode, info,
                infos, idAttributes, nodeType);
            nodeSet.remove(overridingNode);
          } else {
            computeInhertedSubNodeMergeInfos(superNode, info, nodeType, infos);
          }
        }

        // for each nodes that are not merged with a supernode
        for (final Node node : nodeSet) {
          computeSubNodeMergeInfos(node, info, nodeType, infos);
        }
      }
    }
    return info;
  }

  protected Node findOverridingNode(final Node superNode, final Node[] nodes,
      final String nodeType, final Map<String, String> idAttributes) {
    // get the name of the attribute that identify the one sub node of
    // type 'nodeType' among others.
    final String nameAttr = idAttributes.get(nodeType);
    if (nameAttr != null) {
      // get the value of attribute 'nameAttr' of node 'superNode'
      final String superName = superNode.astGetAttributes().get(nameAttr);
      // Check that the value is not null
      if (superName != null) {
        // for each node in 'Nodes'
        for (final Node node : nodes) {
          // get the value of attribute 'nameAttr' of node 'node'
          final String name = node.astGetAttributes().get(nameAttr);
          // Check that the value is not null
          if (name == null) continue;

          if (name.equals(superName)) {
            return node;
          }
        }
      }
    }
    return null;
  }

  protected void computeSubNodeMergeInfos(final Node subNode,
      final MergeInfo parentInfo, final String subNodeType,
      final Map<Node, MergeInfo> infos) throws MergeException {
    parentInfo.addSubNodeInfo(computeMergeInfos(subNode, infos));
  }

  protected void computeInhertedSubNodeMergeInfos(final Node inheritedSubNode,
      final MergeInfo parentInfo, final String subNodeType,
      final Map<Node, MergeInfo> infos) throws MergeException {
    parentInfo.addSubNodeInfo(computeMergeInfos(inheritedSubNode, infos));
  }

  protected void computeMergedSubNodesMergeInfos(final Node subNode,
      final Node inheritedSubNode, final MergeInfo parentInfo,
      final Map<Node, MergeInfo> infos, final Map<String, String> idAttributes,
      final String subNodeType) throws MergeException {
    parentInfo.addSubNodeInfo(computeMergeInfos(subNode, inheritedSubNode,
        infos, idAttributes));
  }

  protected MergeInfo computeMergeInfos(final Node node,
      final Map<Node, MergeInfo> infos) throws MergeException {
    MergeInfo info = infos.get(node);
    if (info == null) {
      info = new MergeInfo(node);
      infos.put(node, info);
      for (final String type : node.astGetNodeTypes()) {
        for (final Node subNode : node.astGetNodes(type)) {
          if (subNode != null) {
            info.addSubNodeInfo(computeMergeInfos(subNode, infos));
          }
        }
      }
    }
    return info;
  }

  protected Node initMergedNodes(final MergeInfo info,
      final Map<Node, MergeInfo> infos) throws MergeException {
    if (info.done) {
      return info.result;
    }
    info.done = true;

    final Node elem = info.elem;
    final Node result = info.result;

    // merges source
    result.astSetSource(elem.astGetSource());

    // merges attributes
    final Map<String, String> resultAttrs = elem.astGetAttributes();

    // for each node that is overridden by 'elem'
    for (final Node superNode : info.getSuperNodes()) {
      // for each attribute of 'superNode'
      for (final Map.Entry<String, String> entry : superNode.astGetAttributes()
          .entrySet()) {
        final String name = entry.getKey();
        final String superValue = entry.getValue();
        final String value = resultAttrs.get(name);
        if (superValue != null && value == null) {
          // the attribute is defined for 'superNode' and not for 'elem'
          // put the value from 'superNode' as value of the attribute.
          resultAttrs.put(name, superValue);
        }
      }
    }
    result.astSetAttributes(resultAttrs);

    // merges decorations
    // decorations are merged from the "less significant" node to the most one.
    final Map<String, Object> resultDecors = elem.astGetDecorations();
    for (final Node superNode : info.getSuperNodes()) {
      // for each decoration of 'superNode'
      for (final Map.Entry<String, Object> entry : superNode
          .astGetDecorations().entrySet()) {
        final String name = entry.getKey();
        final Object superValue = entry.getValue();
        final Object value = resultDecors.get(name);
        if (superValue instanceof MergeableDecoration) {
          // the decoration is defined for 'superNode' and is a
          // MergeableDecoration put the merged decoration as value of
          // decoration.
          resultDecors.put(name, ((MergeableDecoration) superValue)
              .mergeDecoration(value));
        } else if (superValue != null && value == null) {
          // the decoration is defined for 'superNode' and not for 'elem'
          // put the value from 'superNode' as value of the decoration.
          resultDecors.put(name, superValue);
        }
      }
    }
    result.astSetDecorations(resultDecors);

    // merges sub nodes

    for (final MergeInfo subNodeInfo : info.getSubNodeInfos()) {
      result.astAddNode(initMergedNodes(subNodeInfo, infos));
    }
    return result;
  }

  /** The Arity of a sub node. */
  protected enum SubNodeArity {
    /** one sub node. */
    ONE,
    /** many sub nodes. */
    MANY,
    /** unknown. */
    NONE
  }

  private SubNodeArity getSubNodeArity(final Node node, final String subNodeType) {
    final Class<? extends Node> nodeClass = node.getClass();
    // try to get arities for the 'nodeClass' in the cache
    Map<String, SubNodeArity> arities = subNodeArityCache.get(nodeClass);
    if (arities == null) {
      // arities not found in cache for 'nodeClass'.
      // create a new Map and add it in cache.
      arities = new HashMap<String, SubNodeArity>();
      subNodeArityCache.put(nodeClass, arities);
    }

    // try to get arity if 'subNodeType' in map of arities of 'nodeClass'
    SubNodeArity arity = arities.get(subNodeType);
    if (arity == null) {
      // not found in cache.
      // try to find the getter method of 'subNodeType' as if the arity is 'ONE'
      // if the method is found, the arity is 'ONE'.
      try {
        nodeClass.getMethod("get"
            + Character.toUpperCase(subNodeType.charAt(0))
            + subNodeType.substring(1), new Class[0]);
        arity = SubNodeArity.ONE;
      } catch (final Exception e) {
        // method for arity ONE not found try for arity 'MANY,
        try {
          nodeClass.getMethod("get"
              + Character.toUpperCase(subNodeType.charAt(0))
              + subNodeType.substring(1) + 's', new Class[0]);
          arity = SubNodeArity.MANY;
        } catch (final Exception e1) {
          // not found, this node class does not provides method for sub node of
          // type 'subNodeType'
          arity = SubNodeArity.NONE;
        }
      }
      // put the arity in cache.
      arities.put(subNodeType, arity);
    }

    return arity;
  }

  private SubNodeArity checkArityCompatibility(final Node node,
      final Node superNode, final String subNodeType) throws MergeException {
    final SubNodeArity nodeArity = getSubNodeArity(node, subNodeType);

    // if node and superNode are instances of the same class, they have the same
    // arity for 'subNodeType'
    if (node.getClass() == superNode.getClass()) {
      assert nodeArity != SubNodeArity.NONE;
      return nodeArity;
    }

    final SubNodeArity superNodeArity = getSubNodeArity(superNode, subNodeType);

    if (nodeArity == SubNodeArity.NONE) {
      assert superNodeArity != SubNodeArity.NONE;
      return superNodeArity;
    }

    if (superNodeArity == SubNodeArity.NONE) {
      return nodeArity;
    }

    if (nodeArity != superNodeArity) {
      throw new MergeException(
          "Cannot merge AST, the arity of sub nodes of type '" + subNodeType
              + "' is not the same for node '" + node.astGetSource()
              + "' and node '" + superNode.astGetSource() + "'. Check DTDs");
    }
    return nodeArity;
  }

  protected static class MergeInfo {

    // the node that must be merged with the 'superNodes'.
    Node                       elem;

    // Most of the time, 'elem' node will be merged with only one node (there
    // are more that 1 node only in case of sharing). For optimization purpose,
    // add a field for storing the first node. Additional nodes (if any) will be
    // stored in 'additionalSuperElems'.
    Node                       superElem;
    // the set of the nodes that must be merged with 'elem' to obtain the
    // 'result' node.
    Set<Node>                  additionalSuperElems;

    // the Node that is the result of the merge of the 'nodes'.
    Node                       result;

    // the list of merge info of the sub nodes
    List<MergeInfo>            subNodeInfos;

    // true if the merge has been done
    boolean                    done;

    // null if every 'superNodes' are instances of the same class as 'elem'
    Set<Class<? extends Node>> nodeClasses;

    // state of the iterator.
    final SuperElemIterator    superElemIterator = new SuperElemIterator();

    MergeInfo(final Node elem) {
      assert elem != null;
      this.elem = elem;
    }

    // This method is used if 2 elements override the same superElement (i.e. in
    // case of shared component).
    void resetElem(final Node newElem) {
      superElemIterator.modification = true;
      assert newElem != null;
      assert elem != null;
      final Node oldElem = elem;
      elem = newElem;

      if (superElem == null) {
        assert additionalSuperElems == null;
        superElem = oldElem;
      } else {
        if (additionalSuperElems == null) {
          additionalSuperElems = new LinkedHashSet<Node>();
          additionalSuperElems.add(superElem);
          superElem = oldElem;
        } else {
          final Set<Node> oldSuperNodes = additionalSuperElems;
          additionalSuperElems = new LinkedHashSet<Node>();
          additionalSuperElems.add(superElem);
          additionalSuperElems.addAll(oldSuperNodes);
          superElem = oldElem;
        }
      }

      // check 'newElem' class
      if (newElem.getClass() != oldElem.getClass()) {
        if (nodeClasses == null) {
          nodeClasses = new HashSet<Class<? extends Node>>();
          nodeClasses.add(oldElem.getClass());
        }
        nodeClasses.add(newElem.getClass());
      }
    }

    boolean addSuperNode(final Node superNode) {
      superElemIterator.modification = true;
      assert superNode != null;
      final boolean r;
      if (superElem == null) {
        assert additionalSuperElems == null;
        superElem = superNode;
        r = true;
      } else {
        if (additionalSuperElems == null) {
          additionalSuperElems = new LinkedHashSet<Node>();
        }
        r = additionalSuperElems.add(superNode);
      }

      // check 'superNode' class
      if (superNode.getClass() != elem.getClass()) {
        if (nodeClasses == null) {
          nodeClasses = new HashSet<Class<? extends Node>>();
          nodeClasses.add(elem.getClass());
        }
        nodeClasses.add(superNode.getClass());
      }
      return r;
    }

    protected Iterable<Node> getSuperNodes() {
      return superElemIterator;
    }

    void addSubNodeInfo(final MergeInfo subNodeInfo) {
      assert subNodeInfo != null;
      if (subNodeInfos == null) subNodeInfos = new ArrayList<MergeInfo>();
      subNodeInfos.add(subNodeInfo);
    }

    @SuppressWarnings("unchecked")
    protected List<MergeInfo> getSubNodeInfos() {
      if (subNodeInfos == null) return Collections.EMPTY_LIST;
      return subNodeInfos;
    }

    void createResultNode(final MergeClassLoader mergeClassLoader)
        throws MergeException {
      if (nodeClasses == null) {
        try {
          result = elem.astNewInstance();
        } catch (final Exception e) {
          throw new MergeException("Cannot merge ASTs", elem, e);
        }
      } else {
        try {
          final Class<?> merged = mergeClassLoader.mergeNodeClasses(elem
              .astGetType(), nodeClasses);
          result = (Node) merged.newInstance();
        } catch (final Exception e) {
          throw new MergeException("Cannot merge AST classes", elem, e);
        }
      }
    }

    // Implementation of the Iterable interface
    private final class SuperElemIterator
        implements
          Iterable<Node>,
          Iterator<Node> {

      boolean        modification;
      boolean        superElemReturned;
      Iterator<Node> additionalSuperElemsIterator;

      public Iterator<Node> iterator() {
        // use this object has an iterator of superNodes (optimization)
        // WARNING: concurrent iterations are not supported.
        resetIterator();
        return this;
      }

      // Implementation of the Iterator interface
      public boolean hasNext() {
        if (modification) throw new ConcurrentModificationException();
        if (superElemReturned) {
          assert superElem != null;
          if (additionalSuperElemsIterator == null) {
            if (additionalSuperElems == null) {
              return false;
            } else {
              return additionalSuperElems.size() > 0;
            }
          } else {
            return additionalSuperElemsIterator.hasNext();
          }
        } else {
          return superElem != null;
        }
      }

      public Node next() {
        if (modification) throw new ConcurrentModificationException();
        if (superElemReturned) {
          assert superElem != null;
          if (additionalSuperElemsIterator == null) {
            if (additionalSuperElems == null) {
              throw new NoSuchElementException();
            } else {
              additionalSuperElemsIterator = additionalSuperElems.iterator();
            }
          }
          return additionalSuperElemsIterator.next();
        } else {
          if (superElem == null) {
            throw new NoSuchElementException();
          }
          superElemReturned = true;
          return superElem;
        }
      }

      public void remove() {
        throw new UnsupportedOperationException();
      }

      private void resetIterator() {
        superElemReturned = false;
        additionalSuperElemsIterator = null;
        modification = false;
      }
    }
  }

  private class MergeClassLoader extends NodeClassLoader {

    MergeClassLoader(final ClassLoader parent) {
      super(parent);
    }

    Class<?> mergeNodeClasses(final String astNodeName,
        final Set<Class<? extends Node>> nodeClasses)
        throws ClassNotFoundException {

      int hash = 0;
      for (final Class<? extends Node> nodeClass : nodeClasses) {
        hash = 17 * (hash + nodeClass.hashCode());
      }

      final String name = "org.objectweb.fractal.adl.merged.Merged"
          + Integer.toHexString(hash);

      try {
        return loadClass(name);
      } catch (final ClassNotFoundException e) {
      }

      final Set<String> itfs = new HashSet<String>();
      for (final Class<? extends Node> nodeClass : nodeClasses) {
        for (final Class<?> itf : nodeClass.getInterfaces()) {
          itfs.add(Type.getInternalName(itf));
        }
      }

      final ClassWriter cw = generateClass(name, astNodeName, Type
          .getInternalName(getBaseClass()), itfs
          .toArray(new String[itfs.size()]));

      return defineClass(name, cw.toByteArray());
    }
  }
}
