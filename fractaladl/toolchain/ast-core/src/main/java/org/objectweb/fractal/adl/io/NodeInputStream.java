/***
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Type;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.NodeClassId;
import org.objectweb.fractal.adl.NodeClassLoader;

/**
 * An {@link ObjectInputStream} that allows to read graphs of AST node that have
 * been serialized by a {@link NodeOutputStream}.
 */
public class NodeInputStream extends ObjectInputStream {

  final List<NamedNodeClassLoader> nodeClassLoaders = new ArrayList<NamedNodeClassLoader>();
  final ClassLoader                parentLoader;

  /**
   * Creates a new {@link NodeInputStream} that reads from the given
   * {@link InputStream}.
   * 
   * @param in the input stream from which serialized nodes are read.
   * @throws IOException if an error occurs.
   */
  public NodeInputStream(final InputStream in) throws IOException {
    this(in, null);
  }

  /**
   * Creates a new {@link NodeInputStream} that reads from the given
   * {@link InputStream}.
   * 
   * @param in the input stream from which serialized nodes are read.
   * @param parentLoader the class loader used by this node factory. If
   *          <code>null</code>, use the class loader that load this class.
   * @throws IOException if an error occurs.
   */
  public NodeInputStream(final InputStream in, final ClassLoader parentLoader)
      throws IOException {
    super(in);
    this.parentLoader = parentLoader == null
        ? getClass().getClassLoader()
        : parentLoader;
  }

  @Override
  protected Class<?> resolveClass(final ObjectStreamClass desc)
      throws IOException, ClassNotFoundException {
    final boolean isNode = readBoolean();
    if (isNode) {
      // read type and interfaces
      final String className = readUTF();
      final String superClassName = readUTF();
      final Class<? extends Node> superClass = Class.forName(superClassName)
          .asSubclass(Node.class);

      final String type = readUTF();
      // read node interfaces
      final int length = readByte();
      final String[] nodeInterfaces = new String[length];
      for (int i = 0; i < length; i++) {
        nodeInterfaces[i] = readUTF();
      }
      return findClass(className, type, superClass, nodeInterfaces);
    } else {
      return super.resolveClass(desc);
    }
  }

  protected Class<? extends Node> findClass(final String className,
      final String nodeType, final Class<? extends Node> baseClass,
      final String... itfs) throws ClassNotFoundException {
    final NodeClassId id = new NodeClassId(nodeType, baseClass, itfs);
    for (final NamedNodeClassLoader cl : nodeClassLoaders) {
      final NodeClassId clId = cl.classeIds.get(className);
      if (clId == null) {
        // This classloader never generates a class with that name.
        return cl.newNodeClass(className, id);
      }
      if (clId.equals(id)) {
        // This classloader has already generated a class with the same name and
        // the same id.
        return cl.classes.get(className);
      }
      // otherwise, this classloader has already generated a class with the same
      // name but for another classID. must use another classLoader.
    }

    final NamedNodeClassLoader cl = new NamedNodeClassLoader(parentLoader);
    nodeClassLoaders.add(cl);
    return cl.newNodeClass(className, id);
  }

  static class NamedNodeClassLoader extends NodeClassLoader {
    final Map<String, NodeClassId>           classeIds = new HashMap<String, NodeClassId>();
    final Map<String, Class<? extends Node>> classes   = new HashMap<String, Class<? extends Node>>();

    NamedNodeClassLoader(final ClassLoader parent) {
      super(parent);
    }

    Class<? extends Node> newNodeClass(final String className,
        final NodeClassId id) throws ClassNotFoundException {
      final String[] itfNames = new String[id.getItfs().length];
      for (int i = 0; i < itfNames.length; i++) {
        itfNames[i] = id.getItfs()[i].replace('.', '/');
      }
      final ClassWriter cw = generateClass(className, id.getType(), Type
          .getInternalName(id.getBaseClass()), itfNames);

      final Class<? extends Node> c = defineClass(className, cw.toByteArray())
          .asSubclass(Node.class);
      classeIds.put(className, id);
      classes.put(className, c);
      return c;
    }
  }
}
