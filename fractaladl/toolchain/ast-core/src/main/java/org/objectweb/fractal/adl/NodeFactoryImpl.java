/***
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Type;

/**
 * Default implementation of the {@link NodeFactory} interface.
 */
public class NodeFactoryImpl implements NodeFactory {

  protected ClassLoader classLoader = getClass().getClassLoader();
  SimpleNodeClassLoader nodeClassLoader;

  // ---------------------------------------------------------------------------
  // Implementation of the XMLNodeFactory interface
  // ---------------------------------------------------------------------------

  public Node newNode(final String nodeType, final String... interfaces)
      throws ClassNotFoundException {
    return newNode(nodeType, AbstractNode.class, interfaces);
  }

  public Node newNode(final String nodeType,
      final Class<? extends Node> baseNodeClass, final String... interfaces)
      throws ClassNotFoundException {
    if (nodeClassLoader == null)
      nodeClassLoader = new SimpleNodeClassLoader(classLoader);

    final Class<? extends Node> c = nodeClassLoader.getNodeClass(nodeType,
        baseNodeClass, interfaces);
    try {
      return c.newInstance();
    } catch (final Exception e) {
      throw new Error("An error occurs while instantiating Node", e);
    }
  }

  public ClassLoader getClassLoader() {
    return classLoader;
  }

  public void setClassLoader(final ClassLoader loader) {
    if (loader != classLoader) {
      classLoader = loader;
      nodeClassLoader = null;
    }
  }

  // ---------------------------------------------------------------------------
  // Inner classes
  // ---------------------------------------------------------------------------

  static class SimpleNodeClassLoader extends NodeClassLoader {

    final Map<NodeClassId, Class<? extends Node>> classes = new HashMap<NodeClassId, Class<? extends Node>>();

    SimpleNodeClassLoader(final ClassLoader parent) {
      super(parent);
    }

    Class<? extends Node> getNodeClass(final String nodeType,
        final Class<? extends Node> baseClass, final String... itfs)
        throws ClassNotFoundException {
      final NodeClassId key = new NodeClassId(nodeType, baseClass, itfs);
      Class<? extends Node> c = classes.get(key);
      if (c == null) {
        final String name = "org.objectweb.fractal.adl.Node" + classes.size();
        final String[] itfNames = new String[itfs.length];
        for (int i = 0; i < itfs.length; i++) {
          itfNames[i] = itfs[i].replace('.', '/');
        }
        final ClassWriter cw = generateClass(name, nodeType, Type
            .getInternalName(baseClass), itfNames);

        c = defineClass(name, cw.toByteArray()).asSubclass(Node.class);
        classes.put(key, c);
      }
      return c;
    }
  }
}
