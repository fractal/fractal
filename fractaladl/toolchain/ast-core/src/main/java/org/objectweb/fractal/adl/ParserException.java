/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton

 * Contributor: Matthieu Leclercq
 */

package org.objectweb.fractal.adl;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * Thrown when an error occurs during the parsing of a file.
 */

public class ParserException extends Exception {

  /**
  * 
  */
  private static final long serialVersionUID = 6743517195291088312L;
  
  /**
  * The exception that caused this exception. May be <tt>null</tt>.
  */
  private Exception exception;
  
  /**
   * Constructs a new {@link ParserException}.
   * 
   * @param msg a detail message.
   * @param e the exception that caused this exception.
   */
  
  public ParserException (final String msg, final Exception e) {
    super(msg);
    this.exception = e;
  }

  public void printStackTrace () {
    if (exception != null) {
      System.err.println(this);
      exception.printStackTrace();
    } else {
      super.printStackTrace();
    }
  }

  public void printStackTrace (final PrintStream s) {
    if (exception != null) {
      s.println(this);
      exception.printStackTrace(s);
    } else {
      super.printStackTrace(s);
    }
  }

  public void printStackTrace (final PrintWriter s) {
    if (exception != null) {
      s.write(this + "\n");
      exception.printStackTrace(s);
    } else {
      super.printStackTrace(s);
    }
  }
  
  public Throwable getCause() {
    return exception;
  }
  
}
