/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.adl;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * An abstract {@link Node}.
 */
public abstract class AbstractNode implements Node, Externalizable {

  private final String        type;

  private String              source;

  // On-demand allocation of the decoration map to minimize the memory
  // footprint.
  private Map<String, Object> decorations;

  protected AbstractNode(final String type) {
    this.type = type;
  }

  /**
   * Seamless on-demand allocation of the decoration map.
   * 
   * @return allocated decoration map.
   */
  private Map<String, Object> decorations() {
    if (decorations == null) decorations = new HashMap<String, Object>();
    return decorations;
  }

  // ---------------------------------------------------------------------------
  // Implementation of the Node interface
  // ---------------------------------------------------------------------------

  public String astGetType() {
    return type;
  }

  public String astGetSource() {
    return source;
  }

  public void astSetSource(final String source) {
    this.source = source;
  }

  public Object astGetDecoration(final String name) {
    if (decorations == null)
      return null;
    else
      return this.decorations().get(name);
  }

  public Map<String, Object> astGetDecorations() {
    if (decorations == null)
      return new HashMap<String, Object>();
    else
      return new HashMap<String, Object>(this.decorations());
  }

  public void astSetDecoration(final String name, final Object decoration) {
    this.decorations().put(name, decoration);
  }

  public void astSetDecorations(final Map<String, Object> decorations) {
    if (decorations.size() > 0) this.decorations().putAll(decorations);
  }

  // ---------------------------------------------------------------------------
  // Implementation of the Externalivable interface
  // ---------------------------------------------------------------------------

  protected static final byte NEW_DECO    = 0x20;
  protected static final byte END_OF_DECO = 0x22;

  public void readExternal(final ObjectInput in) throws IOException,
      ClassNotFoundException {
    // read node source
    astSetSource((String) in.readObject());

    // read attributes
    final int nbAttributes = in.readInt();
    final Map<String, String> attributes = new HashMap<String, String>(
        nbAttributes);
    for (int i = 0; i < nbAttributes; i++) {
      final String name = in.readUTF();
      final String value = (String) in.readObject();
      attributes.put(name, value);
    }
    astSetAttributes(attributes);

    // read decorations
    byte decoState;
    while ((decoState = in.readByte()) != END_OF_DECO) {
      final String name = in.readUTF();
      Object value;
      if (decoState == NEW_DECO) {
        value = in.readObject();
      } else {
        throw new IOException("Stream Error");
      }
      astSetDecoration(name, value);
    }

    // read sub nodes
    final int nbSubNodes = in.readInt();
    for (int i = 0; i < nbSubNodes; i++) {
      astAddNode((Node) in.readObject());
    }
  }

  public void writeExternal(final ObjectOutput out) throws IOException {
    // write node source
    out.writeObject(astGetSource());

    // write node attributes
    final Map<String, String> attributes = astGetAttributes();
    out.writeInt(attributes.size());
    for (final Map.Entry<String, String> attribute : attributes.entrySet()) {
      // attribute name can't be null, can use writeUTF safely.
      out.writeUTF(attribute.getKey());
      out.writeObject(attribute.getValue());
    }

    // write node decorations
    for (final Map.Entry<String, Object> decoration : astGetDecorations()
        .entrySet()) {
      final Object deco = decoration.getValue();
      if (deco == null || deco instanceof Serializable) {
        out.writeByte(NEW_DECO);
        // assert that decoration name is not null, use writeUTF .
        out.writeUTF(decoration.getKey());
        out.writeObject(deco);
      }
    }
    out.writeByte(END_OF_DECO);

    // write sub nodes

    // count number of sub node
    int nbSubNode = 0;
    for (final String nodeType : astGetNodeTypes()) {
      final Node[] subNodes = astGetNodes(nodeType);
      if (subNodes != null) {
        for (final Node subNode : subNodes) {
          if (subNode != null) nbSubNode++;
        }
      }
    }

    // write number of sub node
    out.writeInt(nbSubNode);

    // write sub nodes
    for (final String nodeType : astGetNodeTypes()) {
      final Node[] subNodes = astGetNodes(nodeType);
      if (subNodes != null) {
        for (final Node subNode : subNodes) {
          if (subNode != null) out.writeObject(subNode);
        }
      }
    }
  }

  // ---------------------------------------------------------------------------
  // Overridden methods
  // ---------------------------------------------------------------------------

  @Override
  public String toString() {
    // look for a "name" attribute
    final Map<String, String> attributes = astGetAttributes();
    final String name = attributes.get("name");
    if (name != null) {
      return type + '<' + name + '>';
    } else if (attributes.size() == 1) {
      // node contains only one attribute
      final Map.Entry<String, String> attr = attributes.entrySet().iterator()
          .next();
      return type + '<' + attr.getKey() + '=' + attr.getValue() + '>';
    } else {
      return type + '@' + System.identityHashCode(this);
    }
  }
}
