/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.comments;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.merger.MergeException;
import org.objectweb.fractal.adl.merger.MergeableDecoration;

/**
 * A decoration that can be used to attach "javadoc-like" comments on AST nodes.
 */
public class CommentDecoration implements MergeableDecoration {

  private final String            comment;
  private final Node              node;

  private List<CommentDecoration> overriddenComment;

  /**
   * Create a new <code>CommentDecoration</code>.
   * 
   * @param comment the comment contained by this decoration.
   * @param node the node to which this comment is attached.
   */
  public CommentDecoration(final String comment, final Node node) {
    this.comment = comment;
    this.node = node;
  }

  /**
   * Returns the comment contained by this decoration.
   * 
   * @return the comment contained by this decoration.
   */
  public String getComment() {
    return comment;
  }

  /**
   * Returns the node to which this comment is attached.
   * 
   * @return the node to which this comment is attached.
   */
  public Node getNode() {
    return node;
  }

  /**
   * Returns the comments that are overridden by this comment.
   * 
   * @return the comments that are overridden by this comment.
   */
  public List<CommentDecoration> getOverriddenComment() {
    return overriddenComment;
  }

  public Object mergeDecoration(final Object overridingDecoration)
      throws MergeException {
    if (!(overridingDecoration instanceof CommentDecoration)) {
      return this;
    }
    final CommentDecoration overridingComment = (CommentDecoration) overridingDecoration;
    if (overridingComment.overriddenComment == null)
      overridingComment.overriddenComment = new ArrayList<CommentDecoration>();

    overridingComment.overriddenComment.add(this);
    return overridingComment;
  }

}
