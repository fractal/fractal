/**
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.merger;

import org.objectweb.fractal.adl.Node;

/**
 * A <code>MergeableDecoration</code> can be used to
 * {@link Node#astSetDecoration(String, Object) decorate} a Node when a special
 * treatment is required when ASTs are merged.
 */
public interface MergeableDecoration {

  /**
   * Returns the merged decoration. The given <code>overridingDecoration</code>
   * is the decoration that overrides this decoration.
   * 
   * @param overridingDecoration the decoration that overrides this one. May be
   *            <code>null</code> if the node that is overriding the node from
   *            which this decoration belongs, does not have a decoration with
   *            the same name.
   * @return the merged decoration.
   * @throws MergeException if the given <code>overridingDecoration</code> is
   *             not valid and cannot be merge with this decoration.
   */
  Object mergeDecoration(Object overridingDecoration) throws MergeException;
}
