/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

/**
 * An abstract class loader to dynamically generate {@link Node} classes.
 */
public abstract class NodeClassLoader extends ClassLoader implements Opcodes {

  /**
   * Byte code of the dynamically generated classes.
   */
  private final Map<String, byte[]> bytecodes = new HashMap<String, byte[]>();

  /**
   * Constructor.
   * 
   * @param parent the parent class loader.
   */
  public NodeClassLoader(final ClassLoader parent) {
    super(parent);
  }

  /**
   * Returns the bytecode of the class of the given node.
   * 
   * @param node a node that has been instantiated from a class loaded by this
   *          ClassLoader.
   * @return the bytecode of the node class.
   */
  public byte[] getClassBytecode(final Node node) {
    return bytecodes
        .get(node.getClass().getName().replace('.', '/') + ".class");
  }

  protected ClassWriter generateClass(final String className,
      final String astNodeType, final String superClass, final String[] itfs)
      throws ClassNotFoundException {
    final String owner = className.replace('.', '/');
    final String node = Type.getInternalName(Node.class);

    final ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
    cw.visit(V1_1, ACC_PUBLIC, owner, null, superClass, itfs);

    cw.visitField(ACC_PRIVATE | ACC_STATIC | ACC_FINAL, "serialVersionUID",
        Type.LONG_TYPE.getDescriptor(), null, new Long(Arrays.hashCode(itfs)));

    final MethodVisitor icv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null,
        null);
    icv.visitVarInsn(ALOAD, 0);
    icv.visitLdcInsn(astNodeType);
    icv.visitMethodInsn(INVOKESPECIAL, superClass, "<init>",
        "(Ljava/lang/String;)V");

    final MethodVisitor ani = cw.visitMethod(ACC_PUBLIC, "astNewInstance",
        "()L" + node + ";", null, null);
    ani.visitTypeInsn(NEW, owner);
    ani.visitInsn(DUP);
    ani.visitMethodInsn(INVOKESPECIAL, owner, "<init>", "()V");
    ani.visitInsn(ARETURN);
    ani.visitMaxs(0, 0);

    final MethodVisitor aga = cw.visitMethod(ACC_PUBLIC, "astGetAttributes",
        "()Ljava/util/Map;", null, null);
    aga.visitTypeInsn(NEW, "java/util/HashMap");
    aga.visitInsn(DUP);
    aga.visitMethodInsn(INVOKESPECIAL, "java/util/HashMap", "<init>", "()V");
    aga.visitVarInsn(ASTORE, 1);

    final MethodVisitor asa = cw.visitMethod(ACC_PUBLIC, "astSetAttributes",
        "(Ljava/util/Map;)V", null, null);

    final MethodVisitor agn = cw.visitMethod(ACC_PUBLIC, "astGetNodeTypes",
        "()[Ljava/lang/String;", null, null);
    agn.visitIntInsn(SIPUSH, getNodeCount(itfs));
    agn.visitTypeInsn(ANEWARRAY, "java/lang/String");

    final MethodVisitor agns = cw.visitMethod(ACC_PUBLIC, "astGetNodes",
        "(Ljava/lang/String;)[L" + node + ";", null, null);
    final Label agnsEnd = new Label();
    agns.visitInsn(ACONST_NULL);
    agns.visitVarInsn(ASTORE, 2);

    final MethodVisitor aan = cw.visitMethod(ACC_PUBLIC, "astAddNode", "(L"
        + node + ";)V", null, null);
    aan.visitVarInsn(ALOAD, 1);
    aan.visitMethodInsn(INVOKEINTERFACE, "org/objectweb/fractal/adl/Node",
        "astGetType", "()Ljava/lang/String;");
    aan.visitVarInsn(ASTORE, 2);
    final Label aanEnd = new Label();

    final MethodVisitor arn = cw.visitMethod(ACC_PUBLIC, "astRemoveNode", "(L"
        + node + ";)V", null, null);
    arn.visitVarInsn(ALOAD, 1);
    arn.visitMethodInsn(INVOKEINTERFACE, "org/objectweb/fractal/adl/Node",
        "astGetType", "()Ljava/lang/String;");
    arn.visitVarInsn(ASTORE, 2);
    final Label arnEnd = new Label();

    int count = 0;
    final Set<String> methods = new HashSet<String>();
    for (int i = 0; i < itfs.length; ++i) {
      final Method[] meths = loadClass(itfs[i].replace('/', '.')).getMethods();
      for (int j = 0; j < meths.length; ++j) {
        final Method meth = meths[j];
        final String name = meth.getName();
        final String desc = Type.getMethodDescriptor(meth);

        if (!methods.add(name + desc)) {
          continue;
        }

        if (name.startsWith("ast")) {
          // ignore method prefixed by "ast", this allows AST interfaces to
          // extends the Node interface
          continue;
        }

        final MethodVisitor cv = cw.visitMethod(ACC_PUBLIC, name, desc, null,
            null);

        if (name.startsWith("get")) {
          final String field = getFieldName(name, 3);

          if (!desc.endsWith(")Ljava/lang/String;")) {
            final boolean single = desc.indexOf(")[") == -1;
            // generates code in "astGetNodes()"
            agn.visitInsn(DUP);
            agn.visitIntInsn(SIPUSH, count++);
            agn.visitLdcInsn(getASTName(name, single));
            agn.visitInsn(AASTORE);

            // generates code in "astGetNodes(String)"
            if (single) {
              generateGetNodeMethod(agns, owner, name, field,
                  desc.substring(2), agnsEnd);
            } else {
              generateGetNodesMethod(agns, owner, name, field, agnsEnd);
            }
          }

          if (desc.startsWith("()[")) {
            // case of a getter method for a sub node with arity * or +
            final String fieldDesc = "Ljava/util/List;";
            final String elemDesc = desc.substring(3);

            // generates the field
            cw.visitField(ACC_PRIVATE, field, fieldDesc, null, null);

            // generates the getter method
            cv.visitVarInsn(ALOAD, 0);
            cv.visitFieldInsn(GETFIELD, owner, field, fieldDesc);
            cv.visitVarInsn(ALOAD, 0);
            cv.visitFieldInsn(GETFIELD, owner, field, fieldDesc);
            cv
                .visitMethodInsn(INVOKEINTERFACE, "java/util/List", "size",
                    "()I");
            cv.visitTypeInsn(ANEWARRAY, elemDesc.substring(1,
                elemDesc.length() - 1));
            cv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "toArray",
                "([Ljava/lang/Object;)[Ljava/lang/Object;");
            cv.visitTypeInsn(CHECKCAST, "[" + elemDesc);
            cv.visitInsn(ARETURN);
            cv.visitMaxs(0, 0);

            // generates code to initialize the field in the constructor
            icv.visitVarInsn(ALOAD, 0);
            icv.visitTypeInsn(NEW, "java/util/ArrayList");
            icv.visitInsn(DUP);
            icv.visitMethodInsn(INVOKESPECIAL, "java/util/ArrayList", "<init>",
                "()V");
            icv.visitFieldInsn(PUTFIELD, owner, field, fieldDesc);

            generateAddNodesMethod(aan, owner, name, field, fieldDesc, aanEnd,
                true);
            generateAddNodesMethod(arn, owner, name, field, fieldDesc, arnEnd,
                false);
          } else {
            // case of a getter method for a sub node with arity 1 or ?
            final String fieldDesc = desc.substring(2);

            // generates the field
            cw.visitField(ACC_PRIVATE, field, fieldDesc, null, null);

            // generates the getter method
            cv.visitVarInsn(ALOAD, 0);
            cv.visitFieldInsn(GETFIELD, owner, field, fieldDesc);
            cv.visitInsn(ARETURN);
            cv.visitMaxs(0, 0);

            if (fieldDesc.equals("Ljava/lang/String;")) {
              // case of an attribute:

              // generates code in "astGetAttributes"
              aga.visitVarInsn(ALOAD, 1);
              aga.visitLdcInsn(getASTName(name, true));
              aga.visitVarInsn(ALOAD, 0);
              aga.visitFieldInsn(GETFIELD, owner, field, fieldDesc);
              aga.visitMethodInsn(INVOKEVIRTUAL, "java/util/HashMap", "put",
                  "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");
              aga.visitInsn(POP);

              // generates code in "astSetAttributes"
              asa.visitVarInsn(ALOAD, 0);
              asa.visitVarInsn(ALOAD, 1);
              asa.visitLdcInsn(getASTName(name, true));
              asa.visitMethodInsn(INVOKEINTERFACE, "java/util/Map", "get",
                  "(Ljava/lang/Object;)Ljava/lang/Object;");
              asa.visitTypeInsn(CHECKCAST, "java/lang/String");
              asa.visitFieldInsn(PUTFIELD, owner, field, fieldDesc);
            } else {
              // case of a sub node:
              generateAddNodeMethod(aan, owner, name, field, fieldDesc, aanEnd,
                  true);
              generateAddNodeMethod(arn, owner, name, field, fieldDesc, arnEnd,
                  false);
            }
          }
        } else if (name.startsWith("set")) {
          final String field = getFieldName(name, 3);
          final String fieldDesc = desc.substring(1, desc.length() - 2);
          cv.visitVarInsn(ALOAD, 0);
          cv.visitVarInsn(ALOAD, 1);
          cv.visitFieldInsn(PUTFIELD, owner, field, fieldDesc);
          cv.visitInsn(RETURN);
          cv.visitMaxs(0, 0);
        } else if (name.startsWith("add")) {
          final String field = getFieldName(name, 3) + "s";
          final String fieldDesc = "Ljava/util/List;";
          cv.visitVarInsn(ALOAD, 0);
          cv.visitFieldInsn(GETFIELD, owner, field, fieldDesc);
          cv.visitVarInsn(ALOAD, 1);
          cv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "add",
              "(Ljava/lang/Object;)Z");
          cv.visitInsn(POP);
          cv.visitInsn(RETURN);
          cv.visitMaxs(0, 0);
        } else if (name.startsWith("remove")) {
          final String field = getFieldName(name, 6) + "s";
          final String fieldDesc = "Ljava/util/List;";
          cv.visitVarInsn(ALOAD, 0);
          cv.visitFieldInsn(GETFIELD, owner, field, fieldDesc);
          cv.visitVarInsn(ALOAD, 1);
          cv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "remove",
              "(Ljava/lang/Object;)Z");
          cv.visitInsn(POP);
          cv.visitInsn(RETURN);
          cv.visitMaxs(0, 0);
        }
      }
    }

    icv.visitInsn(RETURN);
    icv.visitMaxs(0, 0);

    aga.visitVarInsn(ALOAD, 1);
    aga.visitInsn(ARETURN);
    aga.visitMaxs(0, 0);

    asa.visitInsn(RETURN);
    asa.visitMaxs(0, 0);

    agn.visitInsn(ARETURN);
    agn.visitMaxs(0, 0);

    agns.visitLabel(agnsEnd);
    agns.visitVarInsn(ALOAD, 2);
    agns.visitInsn(ARETURN);
    agns.visitMaxs(0, 0);

    aan.visitLabel(aanEnd);
    aan.visitInsn(RETURN);
    aan.visitMaxs(0, 0);

    arn.visitLabel(arnEnd);
    arn.visitInsn(RETURN);
    arn.visitMaxs(0, 0);

    return cw;
  }

  protected Class<?> defineClass(final String name, final byte[] b) {
    bytecodes.put(name.replace('.', '/') + ".class", b);
    return defineClass(name, b, 0, b.length);
  }

  @Override
  public InputStream getResourceAsStream(final String name) {
    InputStream is = super.getResourceAsStream(name);
    if (is == null) {
      final byte[] b = bytecodes.get(name);
      if (b != null) {
        is = new ByteArrayInputStream(b);
      }
    }
    return is;
  }

  private int getNodeCount(final String[] itfs) throws ClassNotFoundException {
    int count = 0;
    final Set<String> methods = new HashSet<String>();
    for (int i = 0; i < itfs.length; ++i) {
      final Method[] meths = loadClass(itfs[i].replace('/', '.')).getMethods();
      for (int j = 0; j < meths.length; ++j) {
        final Method meth = meths[j];
        final String name = meth.getName();
        final String desc = Type.getMethodDescriptor(meth);
        if (!methods.add(name + desc)) {
          continue;
        }
        if (name.startsWith("get") && !desc.endsWith(")Ljava/lang/String;")) {
          ++count;
        }
      }
    }
    return count;
  }

  private void generateGetNodeMethod(final MethodVisitor cv,
      final String owner, final String name, final String field,
      final String fieldDesc, final Label end) {
    final Label l = generateIf(cv, 1, getASTName(name, true));
    cv.visitInsn(ICONST_1);
    cv.visitTypeInsn(ANEWARRAY, Type.getInternalName(Node.class));
    cv.visitInsn(DUP);
    cv.visitInsn(ICONST_0);
    cv.visitVarInsn(ALOAD, 0);
    cv.visitFieldInsn(GETFIELD, owner, field, fieldDesc);
    cv.visitInsn(AASTORE);
    cv.visitVarInsn(ASTORE, 2);
    cv.visitJumpInsn(GOTO, end);
    cv.visitLabel(l);
  }

  private void generateGetNodesMethod(final MethodVisitor cv,
      final String owner, final String name, final String field, final Label end) {
    final Label l = generateIf(cv, 1, getASTName(name, false));
    cv.visitVarInsn(ALOAD, 0);
    cv.visitFieldInsn(GETFIELD, owner, field, "Ljava/util/List;");
    cv.visitVarInsn(ALOAD, 0);
    cv.visitFieldInsn(GETFIELD, owner, field, "Ljava/util/List;");
    cv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "size", "()I");
    cv.visitTypeInsn(ANEWARRAY, Type.getInternalName(Node.class));
    cv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "toArray",
        "([Ljava/lang/Object;)[Ljava/lang/Object;");
    cv.visitTypeInsn(CHECKCAST, "[" + Type.getDescriptor(Node.class));
    cv.visitVarInsn(ASTORE, 2);
    cv.visitJumpInsn(GOTO, end);
    cv.visitLabel(l);
  }

  private void generateAddNodeMethod(final MethodVisitor cv,
      final String owner, final String name, final String field,
      final String fieldDesc, final Label end, final boolean add) {
    final String s = getASTName(name, true);
    final Label l = generateIf(cv, 2, s);
    cv.visitVarInsn(ALOAD, 0);
    if (add) {
      cv.visitVarInsn(ALOAD, 1);
      cv.visitTypeInsn(CHECKCAST, fieldDesc
          .substring(1, fieldDesc.length() - 1));
    } else {
      cv.visitInsn(ACONST_NULL);
    }
    cv.visitFieldInsn(PUTFIELD, owner, field, fieldDesc);
    cv.visitJumpInsn(GOTO, end);
    cv.visitLabel(l);
  }

  private void generateAddNodesMethod(final MethodVisitor cv,
      final String owner, final String name, final String field,
      final String fieldDesc, final Label end, final boolean add) {
    final String s = getASTName(name, false);
    final Label l = generateIf(cv, 2, s);
    cv.visitVarInsn(ALOAD, 0);
    cv.visitFieldInsn(GETFIELD, owner, field, fieldDesc);
    cv.visitVarInsn(ALOAD, 1);
    cv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", add
        ? "add"
        : "remove", add ? "(Ljava/lang/Object;)Z" : "(Ljava/lang/Object;)Z");
    cv.visitInsn(POP);
    cv.visitJumpInsn(GOTO, end);
    cv.visitLabel(l);
  }

  private Label generateIf(final MethodVisitor cv, final int var,
      final String name) {
    final Label l = new Label();
    cv.visitVarInsn(ALOAD, var);
    cv.visitLdcInsn(name);
    cv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "equals",
        "(Ljava/lang/Object;)Z");
    cv.visitJumpInsn(IFEQ, l);
    return l;
  }

  protected static String getASTName(final String name, final boolean single) {
    final char c = Character.toLowerCase(name.charAt(3));
    if (single) {
      return c + name.substring(4);
    }
    return c + name.substring(4, name.length() - 1);
  }

  protected static String getFieldName(final String name, final int prefix) {
    final char c = Character.toLowerCase(name.charAt(prefix));
    return "_" + c + name.substring(prefix + 1);
  }
}
