/***
 * Fractal ADL Parser
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 *
 * Contributor(s):
 * - Philippe Merle (set the class loader to use during XML parsing)
 */

package org.objectweb.fractal.adl.xml;

import org.xml.sax.SAXException;

/**
 * Interface to create AST node instance. AST Node structures are described in a
 * DTD file.
 */
public interface XMLNodeFactory {

  /** The default name of this interface. */
  String ITF_NAME = "node-factory";

  /**
   * Parses and checks the DTD pointed by the given <code>systemId</code>.
   * 
   * @param systemId the URL of the DTD to check.
   * @throws SAXException if something goes wrong.
   */
  void checkDTD(String systemId) throws SAXException;

  /**
   * Creates a new AST node instance for the given qualifiedName as described in
   * the DTD pointed by the systemId URL.
   * 
   * @param systemId the URL of the DTD that describes the AST node.
   * @param qualifiedName the AST node name.
   * @return a new AST node instance.
   * @throws SAXException if something goes wrong.
   */
  XMLNode newXMLNode(String systemId, String qualifiedName) throws SAXException;

  /**
   * Creates a new AST node instance for the given qualifiedName as described in
   * the DTD pointed by the systemId URL.
   * 
   * @param systemId the URL of the DTD that describes the AST node.
   * @param qualifiedName the AST node name.
   * @param baseNodeClass the base class that is extended by the generated
   *          class.
   * @return a new AST node instance.
   * @throws SAXException if something goes wrong.
   */
  XMLNode newXMLNode(String systemId, String qualifiedName,
      Class<? extends XMLNode> baseNodeClass) throws SAXException;

  /**
   * Gets the class loader used by the XML node factory.
   * 
   * @return the class loader used by the XML node factory.
   */
  ClassLoader getClassLoader();

  /**
   * Sets the class loader used by the XML node factory.
   * 
   * @param loader the class loader used by the XML node factory.
   */
  void setClassLoader(ClassLoader loader);
}
