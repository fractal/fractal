/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributors:
 * - Matthieu Leclercq (externalize AST node factory)
 * - Philippe Merle (set the class loader to use during XML parsing)
 */

package org.objectweb.fractal.adl.xml;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.Parser;
import org.objectweb.fractal.adl.ParserException;
import org.objectweb.fractal.adl.comments.CommentDecorationHelper;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.DefaultHandler;

/**
 * An extensible XML parser that constructs strongly typed abstract syntax
 * trees.
 */

public class XMLParser extends DefaultHandler implements Parser, LexicalHandler {

  /** The parser factory used to create new parsers. */
  SAXParserFactory spf;

  /** Name of the file that is currently being parsed. */
  String           file;

  /** The URL of the DTD of the file that is currently being parsed. */
  String           systemId;

  /** Stack of the elements that are currently being parsed. */
  List<XMLNode>    stack;

  /** Top level element of the elements that are currently being parsed. */
  XMLNode          result;

  /** Current element. */
  XMLNode          current;

  /** Content of characters. */
  StringBuilder    characters;

  /** The comment */
  String           comment;

  /** The locator used to keep track of line numbers. */
  Locator          locator;

  int              line;

  XMLNodeFactory   nodeFactory;

  // ---------------------------------------------------------------------------
  // Constructor
  // ---------------------------------------------------------------------------

  /**
   * Creates a new {@link XMLParser}.
   * 
   * @param nodeFactory the {@link XMLNodeFactory} used to create AST nodes.
   */
  public XMLParser(final XMLNodeFactory nodeFactory) {
    this(true, nodeFactory);
  }

  /**
   * Creates a new {@link XMLParser}.
   * 
   * @param validate
   * @param nodeFactory the {@link XMLNodeFactory} used to create AST nodes.
   */
  public XMLParser(final boolean validate, final XMLNodeFactory nodeFactory) {
    spf = SAXParserFactory.newInstance();
    spf.setValidating(validate);
    this.nodeFactory = nodeFactory;
  }

  // ---------------------------------------------------------------------------
  // Implementation of the Parser interface
  // ---------------------------------------------------------------------------

  public Node parse(final InputStream is, final String name)
      throws ParserException {
    file = name;
    try {
      stack = new ArrayList<XMLNode>();
      final SAXParser sp = spf.newSAXParser();
      sp.setProperty("http://xml.org/sax/properties/lexical-handler", this);
      sp.parse(new InputSource(is), this);
      return result;
    } catch (final IOException e) {
      throw new ParserException("Parser error (" + file + ":" + line + ")", e);
    } catch (final ParserConfigurationException e) {
      throw new ParserException("Parser error (" + file + ":" + line + ")", e);
    } catch (final SAXException e) {
      throw new ParserException("Parser error (" + file + ":" + line + ")", e);
    }
  }

  // ---------------------------------------------------------------------------
  // Overriden DefaultHandler methods
  // ---------------------------------------------------------------------------

  @Override
  public void setDocumentLocator(final Locator locator) {
    super.setDocumentLocator(locator);
    this.locator = locator;
  }

  @Override
  public InputSource resolveEntity(final String publicId, final String systemId)
      throws SAXException {

    this.systemId = systemId;
    nodeFactory.checkDTD(systemId);

    if (systemId.startsWith("classpath://")) {
      return new InputSource(nodeFactory.getClassLoader().getResourceAsStream(
          systemId.substring("classpath://".length())));
    } else {
      return null;
    }
  }

  @Override
  public void startElement(final String uri, final String localName,
      final String qualifiedName, final Attributes attributes)
      throws SAXException {
    if (current != null && characters != null) {
      current.xmlSetContent(characters.toString());
      characters = null;
    }
    final XMLNode o = nodeFactory.newXMLNode(systemId, qualifiedName);
    o.astSetSource(file + ":" + locator.getLineNumber());
    o.xmlSetAttributes(attributes);
    if (comment != null) {
      CommentDecorationHelper.setComment(o, comment);
      comment = null;
    }

    if (stack.size() == 0) {
      result = o;
    } else {
      stack.get(stack.size() - 1).xmlAddNode(qualifiedName, o);
    }
    stack.add(o);
    current = o;
  }

  @Override
  public void endElement(final String uri, final String localName,
      final String qualifiedName) throws SAXException {
    if (characters != null) {
      current.xmlSetContent(characters.toString());
      characters = null;
    }

    stack.remove(stack.size() - 1);
    if (stack.size() == 0)
      current = null;
    else
      current = stack.get(stack.size() - 1);
  }

  @Override
  public void characters(final char[] ch, final int start, final int length)
      throws SAXException {
    if (characters == null) {
      final String previousContent = current.xmlGetContent();
      if (previousContent == null)
        characters = new StringBuilder();
      else
        characters = new StringBuilder(previousContent);
    }

    characters.append(ch, start, length);
  }

  @Override
  public void error(final SAXParseException e) throws SAXException {
    line = locator.getLineNumber();
    throw e;
  }

  @Override
  public void fatalError(final SAXParseException e) throws SAXException {
    line = locator.getLineNumber();
    throw e;
  }

  // ---------------------------------------------------------------------------
  // implementation of LexicalHandler interface
  // ---------------------------------------------------------------------------

  /*
   * Assumes that every comments are received completely in a single call to
   * this method.
   */
  public void comment(final char[] ch, final int start, final int length)
      throws SAXException {
    final int end = start + length;
    int i = start;

    // search first non-whitespace character.
    while (i < end && Character.isWhitespace(ch[i]))
      i++;
    if (i == end) return;

    if (ch[i] != '-') return;
    i++;

    String s = new String(ch, i, end - i).trim();
    s = s.replaceAll("\\n(\\s)*(-)*(\\s)+", " ");
    comment = (comment == null) ? s : comment + " " + s;
  }

  public void endCDATA() throws SAXException {
  }

  public void endDTD() throws SAXException {
  }

  public void endEntity(final String name) throws SAXException {
  }

  public void startCDATA() throws SAXException {
  }

  public void startDTD(final String name, final String publicId,
      final String systemId) throws SAXException {
  }

  public void startEntity(final String name) throws SAXException {
  }
}
