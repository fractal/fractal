/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.xml;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.objectweb.fractal.adl.Node;

/**
 * A class to write {@link Node} based abstract syntax trees in XML form.
 */

public class XMLWriter {

  private Writer w;

  /**
   * Creates a new {@link XMLWriter}.
   * 
   * @param writer the output writer used by this {@link XMLWriter}.
   */
  public XMLWriter(final Writer writer) {
    this.w = writer;
  }

  /**
   * Writes the given node.
   * 
   * @param node a node to write.
   * @throws IOException if an error occurs while writing on the output writer.
   */
  public void write(final Node node) throws IOException {
    write("", node, new Stack<Node>());
  }

  private void write(final String indent, final Node node,
      final Stack<Node> writtenNodes) throws IOException {
    w.write(indent);
    w.write('<');
    w.write(node.astGetType());
    final Map attrs = node.astGetAttributes();
    final Iterator i = attrs.keySet().iterator();
    while (i.hasNext()) {
      final String attr = (String) i.next();
      final String value = (String) attrs.get(attr);
      if (value != null) {
        w.write(' ');
        w.write(attr);
        w.write("=\"");
        w.write(value);
        w.write("\"");
      }
    }
    if (writtenNodes.contains(node)) {
      w.write(" (...) />\n");
    } else {
      final List<Node> subNodes = new ArrayList<Node>();
      final String[] subNodeTypes = node.astGetNodeTypes();
      for (final String subNode : subNodeTypes) {
        final Node[] nodes = node.astGetNodes(subNode);
        for (final Node n : nodes) {
          if (n != null) {
            subNodes.add(n);
          }
        }
      }
      if (subNodes.size() > 0) {
        w.write(">\n");

        final String subIndent = indent + "  ";
        writtenNodes.push(node);
        for (final Node subNode : subNodes) {
          write(subIndent, subNode, writtenNodes);
        }
        writtenNodes.pop();
        w.write(indent);
        w.write("</");
        w.write(node.astGetType());
        w.write(">\n");
      } else {
        w.write("/>\n");
      }
    }
  }
}
