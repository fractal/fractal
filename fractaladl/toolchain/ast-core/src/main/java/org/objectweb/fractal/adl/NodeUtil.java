/***
 * Fractal ADL Parser
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl;

import java.util.IdentityHashMap;
import java.util.Map;

/**
 * An helper class that provides a method to easily clone and cast AST node.
 */
public final class NodeUtil {

  private NodeUtil() {
  }

  /**
   * Clone the given {@link Node}. Sub nodes of the given node are not
   * recursively cloned. This imply that the returned node has the same sub
   * nodes as the given node.
   * 
   * @param <T> The type of the node.
   * @param node the node to clone.
   * @return a clone of the given node.
   */
  public static <T extends Node> T cloneNode(final T node) {
    final T newNode = cloneNodeState(node);

    // add sub nodes
    for (final String subNodeType : node.astGetNodeTypes()) {
      for (final Node subNode : node.astGetNodes(subNodeType)) {
        if (subNode != null) newNode.astAddNode(subNode);
      }
    }

    return newNode;
  }

  /**
   * Clone the given {@link Node} and its sub nodes recursively. <br>
   * <b>Warning</b>: this method consider that the given node is the root of a
   * <b>tree graph</b> (i.e. any transitive sub node of the given node is the
   * sub node of one and only one node). Use {@link #cloneGraph(Node)} method if
   * this condition is not true.
   * 
   * @param <T> The type of the node.
   * @param node the root of the tree to clone.
   * @return a clone of the given tree of node.
   * @see #cloneGraph(Node)
   */
  public static <T extends Node> T cloneTree(final T node) {
    final T newNode = cloneNodeState(node);

    // add sub nodes
    for (final String subNodeType : node.astGetNodeTypes()) {
      for (final Node subNode : node.astGetNodes(subNodeType)) {
        if (subNode != null) newNode.astAddNode(cloneTree(subNode));
      }
    }

    return newNode;
  }

  /**
   * Clone the given {@link Node} and its sub nodes recursively. This method
   * handles shared nodes correctly.
   * 
   * @param <T> The type of the node.
   * @param node the root of the graph to clone.
   * @return a clone of the given graph of node.
   */
  public static <T extends Node> T cloneGraph(final T node) {
    return cloneGraph(node, new IdentityHashMap<Node, Node>());
  }

  @SuppressWarnings("unchecked")
  private static <T extends Node> T cloneGraph(final T node,
      final Map<Node, Node> clonedNodes) {
    T clone = (T) clonedNodes.get(node);

    if (clone == null) {
      clone = cloneNodeState(node);

      // add sub nodes
      for (final String subNodeType : node.astGetNodeTypes()) {
        for (final Node subNode : node.astGetNodes(subNodeType)) {
          if (subNode != null)
            clone.astAddNode(cloneGraph(subNode, clonedNodes));
        }
      }

      clonedNodes.put(node, clone);
    }

    return clone;
  }

  @SuppressWarnings("unchecked")
  private static <T extends Node> T cloneNodeState(final T node) {
    // first create a new node instance
    final T newNode = (T) node.astNewInstance();

    // copy node attributes
    newNode.astSetAttributes(node.astGetAttributes());

    // copy node decoration
    newNode.astSetDecorations(node.astGetDecorations());

    // copy source
    newNode.astSetSource(node.astGetSource());
    return newNode;
  }

  /**
   * Cast the given node to the given class. If the given <code>node</code> is
   * not an instance of the given <code>nodeClass</code>, throws an
   * {@link InvalidNodeTypeException}.
   * 
   * @param <T> the type to which the node is casted to.
   * @param node a node.
   * @param nodeClass a node class
   * @return the given node casted to the given class.
   * @throws InvalidNodeTypeException if the given node is not an
   *             {@link Class#isInstance(Object) instance of} the given class.
   */
  public static <T> T castNode(final Object node, final Class<T> nodeClass) {
    try {
      return nodeClass.cast(node);
    } catch (final ClassCastException e) {
      throw new InvalidNodeTypeException(node, nodeClass);
    }
  }

  /**
   * Cast the given node to the given class. If the given <code>node</code> is
   * not an instance of the given <code>nodeClass</code>, throws an
   * {@link InvalidNodeTypeError}.
   * <p>
   * This method is intended to be used if it is a
   * {@link CompilerError unrecoverable error} if the given <code>node</code>
   * is not an instance of the given <code>nodeClass</code>.
   * 
   * @param <T> the type to which the node is casted to.
   * @param node a node.
   * @param nodeClass a node class
   * @return the given node casted to the given class.
   * @throws InvalidNodeTypeError if the given node is not an
   *             {@link Class#isInstance(Object) instance of} the given class.
   */
  public static <T> T castNodeError(final Object node, final Class<T> nodeClass) {
    try {
      return nodeClass.cast(node);
    } catch (final ClassCastException e) {
      throw new InvalidNodeTypeError(node, nodeClass);
    }
  }

  /**
   * The exception thrown by the {@link NodeUtil#castNode} method.
   */
  public static class InvalidNodeTypeException extends ClassCastException {

    InvalidNodeTypeException(final Object node, final Class<?> nodeClass) {
      super(getErrorMessage(node, nodeClass));
    }
  }

  /**
   * The exception thrown by the {@link NodeUtil#castNodeError} method.
   */
  public static class InvalidNodeTypeError extends Error {

    InvalidNodeTypeError(final Object node, final Class<?> nodeClass) {
      super(getErrorMessage(node, nodeClass));
    }
  }

  private static String getErrorMessage(final Object node,
      final Class<?> nodeClass) {
    final StringBuilder sb = new StringBuilder(
        "The given node does not implement the expected type.");
    sb.append(" Expected type: '").append(nodeClass.getName()).append("'.");
    if (node instanceof Node) {
      sb.append(" Node location: '").append(((Node) node).astGetSource())
          .append("'.");
    }
    return sb.toString();
  }
}
