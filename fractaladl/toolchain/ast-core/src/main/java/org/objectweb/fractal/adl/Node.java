/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2006 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributor: Philippe Merle
 *
 * ---------------------------------------------------------------------------
 * $Id$
 * ---------------------------------------------------------------------------
 */

package org.objectweb.fractal.adl;

import java.util.Map;

/**
 * An Abstract Syntax Tree (AST) node. Each node has a type (which can be though
 * of as the tag of an XML element), and can have attributes as well as sub
 * nodes (which can be though of as XML attributes and XML sub elements).
 */

public interface Node {

  /**
   * Returns the type of this node.
   * 
   * @return the type of this node.
   */
  String astGetType();

  /**
   * Returns the source of this node.
   * 
   * @return the source of this node (such as a file name).
   */
  String astGetSource();

  /**
   * Sets the source of this node.
   * 
   * @param source the source of this node (such as a file name).
   */
  void astSetSource(String source);

  /**
   * Returns the attributes of this node.
   * 
   * @return the attributes of this node.
   */
  Map<String, String> astGetAttributes();

  /**
   * Sets the attributes of this node.
   * 
   * @param attributes the attributes of this node that must be changed
   *            (attributes that are not defined in this argument are left
   *            unchanged).
   */
  void astSetAttributes(Map<String, String> attributes);

  /**
   * Returns a decoration of this node.
   * 
   * @param name the decoration's name.
   * @return a decoration of this node. May be <code>null</code> if this node
   *         does not have a decoration with the specified name.
   */
  Object astGetDecoration(String name);

  /**
   * Sets a decoration of this node.
   * 
   * @param name the decoration's name.
   * @param decoration a decoration.
   */
  void astSetDecoration(String name, Object decoration);

  /**
   * Returns the decorations of this node.
   * 
   * @return the decorations of this node.
   */
  Map<String, Object> astGetDecorations();

  /**
   * Sets the decorations of this node.
   * 
   * @param decorations the decorations of this node that must be changed
   *            (decorations that are not defined in this argument are left
   *            unchanged).
   */
  void astSetDecorations(Map<String, Object> decorations);

  /**
   * Returns the types of the sub nodes that this node can have.
   * 
   * @return the types of the sub nodes that this node can have.
   */
  String[] astGetNodeTypes();

  /**
   * Returns the sub nodes of this node that are of the given type.
   * 
   * @param type a node type.
   * @return the sub nodes of this node that are of the given type.
   */
  Node[] astGetNodes(String type);

  /**
   * Adds a sub node to this node.
   * 
   * @param node the sub node to be added to this node.
   */
  void astAddNode(Node node);

  /**
   * Removes a sub node from this node.
   * 
   * @param node the sub node to be removed from this node.
   */
  void astRemoveNode(Node node);

  /**
   * Creates a new, empty AST node of the same type as this node.
   * 
   * @return a new, empty AST node of the same type as this node.
   */
  Node astNewInstance();
}
