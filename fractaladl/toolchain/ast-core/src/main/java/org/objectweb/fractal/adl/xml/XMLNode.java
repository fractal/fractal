/***
 * Fractal ADL Parser
 * Copyright (C) 2002-2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.adl.xml;

import org.objectweb.fractal.adl.AbstractNode;
import org.xml.sax.Attributes;

/**
 * An {@link AbstractNode} with initialization methods for SAX parsers.
 */

public abstract class XMLNode extends AbstractNode {

  private String xmlContent;

  public XMLNode(final String type) {
    super(type);
  }

  public abstract void xmlSetAttributes(Attributes xmlAttrs);

  public abstract void xmlAddNode(String xmlName, XMLNode node);

  public String xmlGetContent() {
    return xmlContent;
  }

  public void xmlSetContent(final String xmlContent) {
    this.xmlContent = xmlContent;
  }
}
