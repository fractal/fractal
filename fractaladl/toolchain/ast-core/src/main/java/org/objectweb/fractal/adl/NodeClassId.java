/**
 * Copyright (C) 2010 STMicroelectronics
 *
 * This file is part of "Mind Compiler" is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact: mind@ow2.org
 *
 * Authors: Matthieu Leclercq
 * Contributors: 
 */

package org.objectweb.fractal.adl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public final class NodeClassId {

  private final String[]              itfs;
  private final String                type;
  private final Class<? extends Node> baseClass;

  private final int                   hashcode;
  private Set<String>                 itfSet = null;

  public NodeClassId(final String type, final Class<? extends Node> baseClass,
      final String[] itfs) {
    this.itfs = itfs;
    this.type = type;
    this.baseClass = baseClass;
    hashcode = (Arrays.deepHashCode(itfs) * 13 + baseClass.hashCode()) * 13
        + type.hashCode();
  }

  public String[] getItfs() {
    return itfs;
  }

  public String getType() {
    return type;
  }

  public Class<? extends Node> getBaseClass() {
    return baseClass;
  }

  private void initSet() {
    itfSet = new HashSet<String>();
    for (final String itf : itfs) {
      itfSet.add(itf);
    }
  }

  @Override
  public int hashCode() {
    return hashcode;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == this) return true;
    if (!(obj instanceof NodeClassId)) return false;

    final NodeClassId id = (NodeClassId) obj;
    if (!id.type.equals(type)) return false;
    if (!id.baseClass.equals(baseClass)) return false;

    if (itfSet != null) {
      if (id.itfSet != null) {
        return itfSet.equals(id.itfSet);
      } else {
        if (itfSet.size() != id.itfs.length) return false;
        for (final String itf : id.itfs) {
          if (!itfSet.contains(itf)) return false;
        }
        return true;
      }
    } else {
      if (id.itfSet == null) id.initSet();
      if (id.itfSet.size() != itfs.length) return false;
      for (final String itf : itfs) {
        if (!id.itfSet.contains(itf)) return false;
      }
      return true;
    }
  }
}