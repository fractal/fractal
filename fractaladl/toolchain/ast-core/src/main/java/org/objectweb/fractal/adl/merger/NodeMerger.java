/***
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.merger;

import java.util.Map;

import org.objectweb.fractal.adl.Node;

/**
 * Interface to merge two ASTs.
 */
public interface NodeMerger {

  /** The default name of this interface. */
  String ITF_NAME = "node-merger";

  /**
   * Merges the two given ASTs. The first one is overridden by the second one.
   * More precisely, the {@link Node#astGetAttributes() attributes} of nodes of
   * the first AST are replaced by attributes of the related nodes in the second
   * AST. Nodes of the two AST are interrelated using the
   * <code>idAttributes</code> map. This map associates a
   * {@link Node#astGetType() node type} to the name of its attribute that
   * identify a given node of this type among others. This is used when a node
   * may contains many sub-nodes of the same type. <br>
   * Note: the given ASTs may be acyclic graphs (i.e. a sub node may be shared
   * by two or more nodes).
   * 
   * @param elem an AST.
   * @param superElem an AST that overrides the first one.
   * @param idAttributes a map associating a {@link Node#astGetType() node type}
   *          to the name of its attribute that identify a given node of this
   *          type among others.
   * @return the merged node.
   * @throws MergeException when an error occurs. TODO detail error cases.
   */
  Node merge(Node elem, Node superElem, Map<String, String> idAttributes)
      throws MergeException;

  /**
   * Gets the class loader used by this node merger.
   * 
   * @return the class loader used by this node merger.
   */
  ClassLoader getClassLoader();

  /**
   * Sets the class loader used by this node merger.
   * 
   * @param loader the class loader used by this node merger.
   */
  void setClassLoader(ClassLoader loader);

}
