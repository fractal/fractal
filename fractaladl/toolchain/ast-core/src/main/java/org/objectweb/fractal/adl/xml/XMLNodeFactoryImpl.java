/***
 * Fractal ADL Parser
 * Copyright (C) 2006-2007 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 *
 * Contributors:
 * - Alessio Pace
 * - Philippe Merle (set the class loader to use during XML parsing)
 */

package org.objectweb.fractal.adl.xml;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.xml.sax.SAXException;

/**
 * Default implementation of the {@link XMLNodeFactory} interface.
 */
public class XMLNodeFactoryImpl implements XMLNodeFactory {
  /**
   * Cache of already read DTDs. Keys are DTD names, values are ASTClassLoader
   * objects.
   */
  protected Map<Class<? extends XMLNode>, Map<String, XMLNodeClassLoader>> loaders     = new HashMap<Class<? extends XMLNode>, Map<String, XMLNodeClassLoader>>();

  /**
   * Class loader to use during XML parsing.
   */
  public ClassLoader                                                       classLoader = getClass()
                                                                                           .getClassLoader();

  // --------------------------------------------------------------------------
  // Implementation of the XMLNodeFactory interface
  // --------------------------------------------------------------------------

  public void checkDTD(final String systemId) throws SAXException {
    XMLNodeClassLoader loader = getLoader(systemId, XMLNode.class);
    if (loader == null) {
      loader = newLoader(systemId, XMLNode.class);
      setLoader(systemId, XMLNode.class, loader);
    }
  }

  public XMLNode newXMLNode(final String systemId, final String qualifiedName)
      throws SAXException {
    return newXMLNode(systemId, qualifiedName, XMLNode.class);
  }

  public XMLNode newXMLNode(final String systemId, final String qualifiedName,
      final Class<? extends XMLNode> baseNodeClass) throws SAXException {
    XMLNodeClassLoader loader = getLoader(systemId, baseNodeClass);
    if (loader == null) {
      loader = newLoader(systemId, baseNodeClass);
      setLoader(systemId, baseNodeClass, loader);
    }
    final String xmlNodeClassName = loader.getASTClassName(qualifiedName);

    try {
      final Class<? extends XMLNode> loadClass = loader.loadClass(
          xmlNodeClassName).asSubclass(XMLNode.class);
      return loadClass.newInstance();
    } catch (final ClassNotFoundException cnfe) {
      throw new SAXException(
          "The element <"
              + qualifiedName
              + "> can't be mapped to any AST node type. Check that you spell it correctly, or that the '"
              + xmlNodeClassName + "' exists, or that your DTD configuration ["
              + systemId + "] is correct", cnfe);
    } catch (final Exception e) {
      e.printStackTrace();
      throw new SAXException("Internal error", e);
    }
  }

  public ClassLoader getClassLoader() {
    return classLoader;
  }

  public void setClassLoader(final ClassLoader loader) {
    classLoader = loader;
  }

  private XMLNodeClassLoader newLoader(final String systemId,
      final Class<? extends XMLNode> baseNodeClass) throws SAXException {
    XMLNodeClassLoader loader;
    try {
      loader = new XMLNodeClassLoader(classLoader, baseNodeClass);

      InputStream is;
      // gets an InputStream to read the DTD
      if (systemId.startsWith("classpath://")) {
        is = classLoader.getResourceAsStream(systemId.substring("classpath://"
            .length()));
        if (is == null)
          throw new SAXException("Unable to find DTD '" + systemId + "'");
      } else if (systemId.startsWith("file:")) {
        is = new URL(systemId).openStream();
      } else {
        throw new MalformedURLException("Unrecognized system identifier: "
            + systemId);
      }
      // verifies the DTD and initializes the AST class loader
      new DTDHandler().checkDTD(is, loader);
    } catch (final MalformedURLException e) {
      throw new SAXException("Cannot find the DTD", e);
    } catch (final IOException e) {
      throw new SAXException("Cannot read the DTD", e);
    } catch (final ClassNotFoundException e) {
      throw new SAXException("Cannot check the DTD", e);
    }
    return loader;
  }

  protected XMLNodeClassLoader getLoader(final String systemId,
      final Class<? extends XMLNode> baseNodeClass) {
    final Map<String, XMLNodeClassLoader> map = loaders.get(baseNodeClass);
    if (map == null) return null;
    return map.get(systemId);
  }

  protected void setLoader(final String systemId,
      final Class<? extends XMLNode> baseNodeClass,
      final XMLNodeClassLoader loader) {
    Map<String, XMLNodeClassLoader> map = loaders.get(baseNodeClass);
    if (map == null) {
      map = new HashMap<String, XMLNodeClassLoader>();
      loaders.put(baseNodeClass, map);
    }
    map.put(systemId, loader);
  }
}
