/***
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl;

/**
 * Interface to create AST node instance from a set of AST Node interface.
 */
public interface NodeFactory {

  /** The default name of this interface. */
  String ITF_NAME = "node-factory";

  /**
   * Creates a new AST Node instance that implements the given interfaces and
   * that have the given {@link Node#astGetType() nodeType}.
   * 
   * @param nodeType the node type.
   * @param interfaces the AST interfaces that must be implemented by the
   *          created node.
   * @return a node instance.
   * @throws ClassNotFoundException if one of the interfaces can't be found.
   */
  Node newNode(String nodeType, String... interfaces)
      throws ClassNotFoundException;

  /**
   * Creates a new AST Node instance that implements the given interfaces and
   * that have the given {@link Node#astGetType() nodeType}.
   * 
   * @param nodeType the node type.
   * @param baseNodeClass the base class that is extended by the generated
   *          class.
   * @param interfaces the AST interfaces that must be implemented by the
   *          created node.
   * @return a node instance.
   * @throws ClassNotFoundException if one of the interfaces can't be found.
   */
  Node newNode(String nodeType, Class<? extends Node> baseNodeClass,
      String... interfaces) throws ClassNotFoundException;

  /**
   * Gets the class loader used by this node factory.
   * 
   * @return the class loader used by this node factory.
   */
  ClassLoader getClassLoader();

  /**
   * Sets the class loader used by this node factory.
   * 
   * @param loader the class loader used by this node factory.
   */
  void setClassLoader(ClassLoader loader);

}
