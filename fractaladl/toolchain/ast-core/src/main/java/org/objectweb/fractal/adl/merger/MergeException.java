/***
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.merger;

import org.objectweb.fractal.adl.Node;

/**
 * Exception thrown when two AST can't be merged.
 */
public class MergeException extends Exception {

  private final Node src;

  /**
   * Constructs a new {@link MergeException}.
   * 
   * @param msg a detail message.
   */

  public MergeException(final String msg) {
    super(msg);
    src = null;
  }

  /**
   * Constructs a new {@link MergeException}.
   * 
   * @param msg a detail message.
   * @param src where the error is located.
   */

  public MergeException(final String msg, final Node src) {
    super(msg);
    this.src = src;
  }

  /**
   * Constructs a new {@link MergeException}.
   * 
   * @param e the exception that caused this exception.
   */

  public MergeException(final Throwable e) {
    super(e);
    src = null;
  }

  /**
   * Constructs a new {@link MergeException}.
   * 
   * @param msg a detail message.
   * @param e the exception that caused this exception.
   */

  public MergeException(final String msg, final Throwable e) {
    super(msg, e);
    src = null;
  }

  /**
   * Constructs a new {@link MergeException}.
   * 
   * @param msg a detail message.
   * @param src where the error is located.
   * @param e the exception that caused this exception.
   */

  public MergeException(final String msg, final Node src, final Throwable e) {
    super(msg, e);
    this.src = src;
  }

  /**
   * @return the source node of the error. May be <code>null</code>.
   */
  public Node getSrc() {
    return src;
  }

  @Override
  public String getMessage() {
    return (src == null) ? super.getMessage() : super.getMessage() + " ("
        + src.astGetSource() + ")";
  }

}
