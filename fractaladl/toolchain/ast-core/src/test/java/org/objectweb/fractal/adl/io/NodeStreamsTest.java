/***
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.io;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;

import junit.framework.TestCase;

import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.NodeFactory;
import org.objectweb.fractal.adl.NodeFactoryImpl;
import org.objectweb.fractal.adl.comments.Comment;
import org.objectweb.fractal.adl.comments.CommentContainer;
import org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl;

public class NodeStreamsTest extends TestCase {

  ByteArrayOutputStream baos;
  NodeOutputStream      nos;
  NodeInputStream       nis;

  @Override
  protected void setUp() throws Exception {
    baos = new ByteArrayOutputStream();
    nos = new NodeOutputStream(baos);
  }

  protected void initNodeInputStream() throws IOException {
    nos.flush();
    final ByteArrayInputStream bios = new ByteArrayInputStream(baos
        .toByteArray());
    baos.reset();
    nis = new NodeInputStream(bios);
  }

  protected Comment newCommentNode(final NodeFactory nodeFactory)
      throws Exception {
    return (Comment) nodeFactory.newNode("comment", Comment.class.getName());
  }

  protected CommentContainer newCommentContainerNode(
      final NodeFactory nodeFactory) throws Exception {
    return (CommentContainer) nodeFactory.newNode("comment",
        CommentContainer.class.getName());
  }

  public void testBasic() throws Exception {
    final NodeFactory nodeFactory = new NodeFactoryImpl();
    final Node n1 = newCommentNode(nodeFactory);
    nos.writeObject(n1);

    initNodeInputStream();
    final Node n2 = (Node) nis.readObject();
    assertNotNull(n2);
    assertEquals("comment", n2.astGetType());
  }

  public void testAttributes() throws Exception {
    final NodeFactory nodeFactory = new NodeFactoryImpl();
    final Comment n1 = newCommentNode(nodeFactory);
    n1.setText("foo");
    n1.setLanguage("bar");
    nos.writeObject(n1);

    initNodeInputStream();
    final Comment n2 = (Comment) nis.readObject();
    assertNotNull(n2);
    assertEquals("foo", n2.getText());
    assertEquals("bar", n2.getLanguage());
  }

  public void testDecoration() throws Exception {
    final NodeFactory nodeFactory = new NodeFactoryImpl();
    final Node n1 = newCommentNode(nodeFactory);
    n1.astSetDecoration("foo", "bar");
    nos.writeObject(n1);

    initNodeInputStream();
    final Node n2 = (Node) nis.readObject();
    assertNotNull(n2);
    assertEquals("bar", n2.astGetDecoration("foo"));
  }

  public void testComplexDecoration() throws Exception {
    final NodeFactory nodeFactory = new NodeFactoryImpl();
    CommentContainer container = newCommentContainerNode(nodeFactory);
    final Comment n1 = newCommentNode(nodeFactory);
    container.addComment(n1);

    Decoration d = new Decoration();
    d.c = n1;
    container.astSetDecoration("foo", d);
    nos.writeObject(container);

    initNodeInputStream();
    container = (CommentContainer) nis.readObject();
    assertNotNull(container);
    final Comment[] comments = container.getComments();
    assertEquals(1, comments.length);
    d = (Decoration) container.astGetDecoration("foo");
    assertNotNull(d);
    assertSame(d.c, comments[0]);
  }

  private static final class Decoration implements Serializable {
    Comment c;
  }

  public void testWriteTwiceReadSame() throws Exception {
    final NodeFactory nodeFactory = new NodeFactoryImpl();
    final Node n1 = newCommentNode(nodeFactory);
    nos.writeObject(n1);
    nos.writeObject(n1);

    initNodeInputStream();
    final Node n2 = (Node) nis.readObject();
    final Node n3 = (Node) nis.readObject();
    assertSame(n2, n3);
  }

  public void testGraph() throws Exception {
    final NodeFactory nodeFactory = new NodeFactoryImpl();
    CommentContainer container = newCommentContainerNode(nodeFactory);
    final Comment n1 = newCommentNode(nodeFactory);
    // add a same sub node twice (i.e create an a-cyclic graph)
    container.addComment(n1);
    container.addComment(n1);

    assertTrue(container.getComments().length == 2);

    nos.writeObject(container);

    initNodeInputStream();
    container = (CommentContainer) nis.readObject();
    assertNotNull(container);
    final Comment[] comments = container.getComments();
    assertEquals(2, comments.length);
    assertSame(comments[0], comments[1]);
  }

  public void testBaseClass() throws Exception {
    final XMLNodeFactoryImpl xmlNodeFactory = new XMLNodeFactoryImpl();
    final Node n1 = xmlNodeFactory.newXMLNode(
        "classpath://org/objectweb/fractal/adl/xml/comment.dtd", "comment");
    final Node n2 = newCommentNode(new NodeFactoryImpl());

    assertFalse(n1.getClass().getSuperclass().equals(
        n2.getClass().getSuperclass()));

    nos.writeObject(n1);
    nos.writeObject(n2);

    initNodeInputStream();
    final Node n3 = (Node) nis.readObject();
    final Node n4 = (Node) nis.readObject();

    assertFalse(n3.getClass().getSuperclass().equals(
        n4.getClass().getSuperclass()));
    assertTrue(n1.getClass().getSuperclass().equals(
        n3.getClass().getSuperclass()));
    assertTrue(n2.getClass().getSuperclass().equals(
        n4.getClass().getSuperclass()));
  }

  public void testClassNameClash1() throws Exception {
    final Node n1 = newCommentNode(new NodeFactoryImpl());

    // create another node with another node factory.
    final Node n2 = newCommentContainerNode(new NodeFactoryImpl());

    // the two node classes are different but should have the same name.
    assertEquals(n1.getClass().getName(), n2.getClass().getName());

    nos.writeObject(n1);
    nos.writeObject(n2);

    initNodeInputStream();
    final Node n3 = (Node) nis.readObject();
    final Node n4 = (Node) nis.readObject();
    assertNotSame(n3, n4);
    assertFalse(n3.getClass().equals(n4.getClass()));
    assertEquals(n3.getClass().getName(), n4.getClass().getName());
  }

  public void testClassNameClash2() throws Exception {
    final NodeFactoryImpl nodeFactory = new NodeFactoryImpl();
    final Node n0 = newCommentContainerNode(nodeFactory);
    final Node n1 = newCommentNode(nodeFactory);

    // create another node with another node factory.
    final Node n2 = newCommentNode(new NodeFactoryImpl());

    // the two node classes are the same but should have different names.
    assertFalse(n1.getClass().getName().equals(n2.getClass().getName()));

    nos.writeObject(n1);
    nos.writeObject(n2);

    initNodeInputStream();
    final Node n3 = (Node) nis.readObject();
    final Node n4 = (Node) nis.readObject();
    assertNotSame(n3, n4);
    assertFalse(n3.getClass().equals(n4.getClass()));
    assertFalse(n3.getClass().getName().equals(n4.getClass().getName()));
  }
}
