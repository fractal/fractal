/***
 * Fractal ADL Parser
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Alessio Pace
 */

package org.objectweb.fractal.adl.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.comments.Comment;
import org.objectweb.fractal.adl.comments.CommentContainer;
import org.objectweb.fractal.adl.comments.ExtendedComment;
import org.xml.sax.SAXException;

/**
 * @author Alessio Pace
 */
public class XMLNodeFactoryImplTest {

  private static final String FIXTURE_DTD = "classpath://org/objectweb/fractal/adl/xml/comment.dtd";
  private XMLNodeFactory      nodeFactory;

  @Before
  public void setUp() throws Exception {
    this.nodeFactory = new XMLNodeFactoryImpl();
  }

  /**
   * Test method for
   * {@link org.objectweb.fractal.adl.xml.XMLNodeFactoryImpl#newXMLNode(java.lang.String, java.lang.String)}.
   * 
   * @throws Exception
   */
  @Test
  public final void testNewXMLNode() throws Exception {
    final Node n1 = nodeFactory.newXMLNode(FIXTURE_DTD, "comment");
    Assert.assertTrue(n1 instanceof Comment);
    final Comment c1 = (Comment) n1;

    final Node n2 = nodeFactory.newXMLNode(FIXTURE_DTD, "extcomment");
    assertTrue(n2 instanceof ExtendedComment);
    final ExtendedComment c2 = (ExtendedComment) n2;

    final Node n3 = nodeFactory.newXMLNode(FIXTURE_DTD, "container");
    assertTrue(n3 instanceof CommentContainer);
    final CommentContainer container = (CommentContainer) n3;

    container.addComment(c1);
    Comment[] comments = container.getComments();
    assertEquals(1, comments.length);
    assertSame(c1, comments[0]);

    container.addComment(c2);
    comments = container.getComments();
    assertEquals(2, comments.length);
    assertSame(c1, comments[0]);
    assertSame(c2, comments[1]);

    container.removeComment(c1);
    comments = container.getComments();
    assertEquals(1, comments.length);
    assertSame(c2, comments[0]);

    container.removeComment(c2);
    comments = container.getComments();
    assertEquals(0, comments.length);

    n3.astAddNode(n1);
    comments = container.getComments();
    assertEquals(1, comments.length);
    assertSame(c1, comments[0]);

    n3.astAddNode(n2);
    comments = container.getComments();
    assertEquals(2, comments.length);
    assertSame(c1, comments[0]);
    assertSame(c2, comments[1]);

    n3.astRemoveNode(n1);
    comments = container.getComments();
    assertEquals(1, comments.length);
    assertSame(c2, comments[0]);

    n3.astRemoveNode(n2);
    comments = container.getComments();
    assertEquals(0, comments.length);
  }

  /**
   * Check that a {@link SAXException} is thrown when trying to create a node
   * whose definition does not exist.
   * 
   * @throws Exception
   */
  @Test
  public final void testNewXMLNodeNotPresentInDtd() throws Exception {

    try {
      this.nodeFactory.newXMLNode(FIXTURE_DTD, "invalidname");
      fail("Should throw " + SAXException.class);
    } catch (final SAXException expected) {
      // OK
    }
  }

}
