/**
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.comments.Comment;
import org.objectweb.fractal.adl.comments.CommentContainer;

public class NodeFactoryImplTest {

  private NodeFactory nodeFactory;

  @Before
  public void setUp() throws Exception {
    this.nodeFactory = new NodeFactoryImpl();
  }

  @Test
  public void testNewNode() throws Exception {
    final Node n1 = nodeFactory.newNode("comment", Comment.class.getName());

    assertTrue(n1 instanceof Comment);
    assertEquals("comment", n1.astGetType());

    final Node n11 = n1.astNewInstance();
    assertEquals("comment", n11.astGetType());

    final Node n2 = nodeFactory.newNode("another-comment", Comment.class
        .getName());

    assertTrue(n2 instanceof Comment);
    assertEquals("another-comment", n2.astGetType());

    final Node n3 = nodeFactory.newNode("comment-container", Comment.class
        .getName(), CommentContainer.class.getName());

    assertTrue(n3 instanceof Comment);
    assertTrue(n3 instanceof CommentContainer);
    assertEquals("comment-container", n3.astGetType());

  }
}
