/***
 * Fractal ADL Parser
 * Copyright (C) 2008 STMicroelectronics
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Matthieu Leclercq
 */

package org.objectweb.fractal.adl.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.adl.Node;
import org.objectweb.fractal.adl.comments.Comment;
import org.objectweb.fractal.adl.comments.CommentContainer;
import org.objectweb.fractal.adl.comments.CommentDecorationHelper;
import org.objectweb.fractal.adl.comments.ExtendedComment;

public class XMLParserTest {

  private static final String  FIXTURE_XML      = "org/objectweb/fractal/adl/xml/comment.xml";
  private static final String  EXPECTED_COMMENT = "This is the documentation of the container top level node. This comment is continuing on this new line";

  protected XMLParser          parser;
  protected XMLNodeFactoryImpl nodeFactory;

  @Before
  public void setUp() throws Exception {
    nodeFactory = new XMLNodeFactoryImpl();
    parser = new XMLParser(nodeFactory);
  }

  @Test
  public void testXMLParser() throws Exception {
    final InputStream is = getClass().getClassLoader().getResourceAsStream(
        FIXTURE_XML);
    final Node n = parser.parse(is, FIXTURE_XML);

    Assert.assertTrue(n instanceof CommentContainer);
    assertEquals(EXPECTED_COMMENT, CommentDecorationHelper.getComment(n));

    final Comment[] comments = ((CommentContainer) n).getComments();
    Assert.assertEquals(3, comments.length);

    final Comment c0 = comments[0];
    Assert.assertFalse(c0 instanceof ExtendedComment);
    assertEquals("foo", c0.getText());

    final Comment c1 = comments[1];
    assertTrue(c1 instanceof ExtendedComment);
    assertEquals("bar", c1.getText());
    assertEquals("toto.com", ((ExtendedComment) c1).getHref());
    assertEquals("fiifuu", ((XMLNode) c1).xmlGetContent());

    final Comment c2 = comments[2];
    Assert.assertFalse(c2 instanceof ExtendedComment);
    assertEquals("boo", c2.getText());
  }
}
