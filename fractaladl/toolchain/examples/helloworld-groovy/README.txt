This example shows how to program a simple Fractal based application using the 
Fractal ADL extensible XML language to describe the architecture of the application 
(see the Fractal ADL tutorials for details). 

Additionally, is shows how primitive components can be written in another language than Java,
in this example it has been used the Groovy programming language.

The application can be launched in two ways:
  1. using the exec-maven-plugin  (--> this is the preferred choice)
  2. using the maven-fractaladl-plugin
  
   
1) Using the exec-maven-plugin  
Three profiles have been configured, in each one the exec-maven-plugin invokes the Fractal ADL Launcher with a 
different backend. The possible commands which can be issued are:
  * "mvn -Prun.julia"    --> run as a Fractal application with Julia 
  * "mvn -Prun.koch"     --> run as a Fractal application with Koch
  * "mvn -Prun.java"     --> run as a pure Java application


2) Using the maven-fractaladl-plugin
Assuming Maven2 is already installed on your system, to launch the helloworld example just execute:

  "mvn fractaladl:run"
  
which will result in launching the application with Julia
