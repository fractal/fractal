/**
 * Fractal ADL Parser
 * Copyright (C) 2009 INRIA Lille - Nord Europe / LIFL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Romain Rouvoy
 */
class ServerImpl extends Service with ServiceAttributes {
    println("SERVER created")

    private var header: String = _
    def getHeader = header
    def setHeader(h: String) { header = h }

    private var count: Int = _
    def getCount = count
    def setCount(c: Int) { count = c }
    
    def print(msg: String) {
        new Exception {
            override def toString = "Server: print method called"
        }.printStackTrace
        println("Server: begin printing...")
        for (i <- List.range(0,count))
            println(header + msg)
        println("Server: print done.")
    }        
}
