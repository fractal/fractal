This Maven module is the parent for the various Fractal ADL examples.

Each Fractal ADL example can be checkout and can be run on its own 
(look at each example's README.txt file).

Additionally, the pom.xml of this parent module contains a set of Maven <profiles>
meant to be inherited by the concrete sub modules (i.e. "HelloWorld", 
"Comanche", ..) in order to share the same running set of profiles among all and
reduce pom.xml length. 

The set of running profiles includes among others:
 * Julia
 * Koch
 * Pure Java

in order to be usable, each sub example must define the Maven property <fractaladl.definition>
which is used internally by FractalADL to compile a component definition.
