<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>org.objectweb.fractal.fractaladl</groupId>
		<artifactId>parent</artifactId>
		<version>2.1.8</version>
	</parent>

	<!-- 
		XXX groupId is *NOT* o.o.f.f.toolchain, in order mantain the same name
		of the old "fractal-adl" sub-module release.
	-->
	<artifactId>toolchain-parent</artifactId>
	<name>Fractal ADL Toolchain parent project</name>
	<version>2.7-SNAPSHOT</version>
	<packaging>pom</packaging>

	<description>
		Fractal ADL is an extensible toolset for supporting heterogenous
		architecture descriptions.
	</description>

	<!-- FractalADL web page -->
	<url>
		http://fractal.objectweb.org/fractaladl-site/${project.version}
	</url>

	<modules>
		<module>ast-core</module>
		<module>fractal-adl</module>
		<module>fractal-adl-jdk14-all</module>
		<module>task-framework</module>
		<module>task-deployment</module>
		<module>maven-fractaladl-plugin</module>
		<module>examples</module>
	</modules>

	<properties>
		<!-- The julia version used by sub modules -->
		<julia.version>2.5.2</julia.version>

		<!-- The login name of the OW2 forge. By default use the local user 
			name. -->
		<ow.username>${user.name}</ow.username>

		<!-- The OW2 forge hostname -->
		<ow.hostname>forge.objectweb.org</ow.hostname>

		<!-- The directory into which the generated site will be deployed -->
		<ow.site.deploy.dir>
			/var/lib/gforge/chroot/home/groups/fractal/htdocs/fractaladl-site
		</ow.site.deploy.dir>

		<!-- The distributionManagement.site.url. It is defined as a property
			so that it can be overridden on the command line which helps site 
			development -->
		<deploy.site.url>
			scp://${ow.hostname}${ow.site.deploy.dir}/${project.version}
		</deploy.site.url>
		<!-- Documentation properties -->
		<site.resources>
			${project.basedir}/src/site/resources
		</site.resources>
		<docbook.source>
			${project.basedir}/src/site/docbook
		</docbook.source>
		<docbook.target>${project.basedir}/target/doc</docbook.target>
		<docbook.xsl>
			${project.build.directory}/docbook-xsl
		</docbook.xsl>
		<dir.site>${project.basedir}/src/site</dir.site>
		<docbook-xsl-version>1.72.0</docbook-xsl-version>
		<docbook-clean-plugin-version>1.0</docbook-clean-plugin-version>
	</properties>

	<scm>
		<connection>scm:svn:svn://svn.forge.objectweb.org/svnroot/fractal/trunk/fractaladl/toolchain</connection>
		<developerConnection>scm:svn:svn+ssh://svn.forge.objectweb.org/svnroot/fractal/trunk/fractaladl/toolchain</developerConnection>
		<url>http://svn.forge.objectweb.org/cgi-bin/viewcvs.cgi/fractal/trunk/fractaladl/toolchain</url>
	</scm>

	<distributionManagement>
		<site>
			<id>objectweb-site</id>
			<name>ObjectWeb's site deployment</name>
			<url>${deploy.site.url}</url>
		</site>
	</distributionManagement>

	<!-- Gather reporting definitions in the parent POM -->
	<reporting>
	</reporting>

	<build>
		<resources>
			<resource>
				<directory>${basedir}/src/main/resources</directory>
				<filtering>true</filtering>
			</resource>
			<resource>
				<directory>${basedir}/src/site/apt</directory>
				<targetPath>../filtered-site/apt</targetPath>
				<filtering>true</filtering>
			</resource>
			<resource>
				<directory>${basedir}/src/site</directory>
				<targetPath>../filtered-site</targetPath>
				<filtering>false</filtering>
				<excludes>
					<exclude>apt/**/*</exclude>
				</excludes>
			</resource>
		</resources>

		<plugins>
			<!-- Configure the maven-release-plugin -->
			<plugin>
				<artifactId>maven-release-plugin</artifactId>
				<version>2.0-beta-7</version>
				<configuration>
					<!-- Configure the tagBase property of the release plugin -->
					<tagBase>
						svn+ssh://svn.forge.objectweb.org/svnroot/fractal/tags
					</tagBase>

					<!-- The SCM username if the OW2 username. -->
					<username>${ow.username}</username>

					<!-- Sub modules have the same version as this one. -->
					<autoVersionSubmodules>true</autoVersionSubmodules>

					<!-- At preparation phase, new artifacts must be installed 
						in the local repository. In particular, maven plugins 
						must be available when the release is performed because
						of the maven-source-pluging that fork a maven lifecycle
					-->
					<preparationGoals>clean install</preparationGoals>

					<!-- release goals contain "antrun:run" to update 
						documentation link on the OW2 forge. -->
					<!-- TODO running the "antrun:run" goal while performing 
						release is not very clean since sub modules may defines 
						there own configuration of this plugin which may produce
						unexpected result. Should use a	more specific MOJO. -->
					<goals>clean deploy site-deploy antrun:run</goals>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<version>2.0.4</version>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!-- maven-dependency-plugin is be declared before 
				xml-maven-plugin. It is important as we must unpack archive before use files
				that it contains. -->

			<!-- In order to transform docbook
				to xhtml usable by maven, we need to unpack the docbook-xsl archive
				which contains xslt files docbook->xhtml -->
			<plugin>
				<artifactId>maven-dependency-plugin</artifactId>
				<executions>
					<execution>
						<id>unpack</id>
						<phase>pre-site</phase>
						<goals>
							<goal>unpack</goal>
						</goals>
						<configuration>
							<artifactItems>
								<artifactItem>
									<groupId>org.docbook</groupId>
									<artifactId>docbook-xsl</artifactId>
									<version>1.72.0</version>
									<type>jar</type>
									<outputDirectory>
										${project.build.directory}/docbook-xsl
									</outputDirectory>
								</artifactItem>
							</artifactItems>
							<overWriteReleases>true</overWriteReleases>
							<overWriteSnapshots>
								true
							</overWriteSnapshots>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-site-plugin</artifactId>
				<version>2.0-beta-7</version>
				<configuration>
					<siteDirectory>
						${basedir}/target/filtered-site
					</siteDirectory>
				</configuration>
			</plugin>
		</plugins>
		<pluginManagement>
			<plugins>
				<!-- Apply the xsl file to transform all docbook files in
					xhtml that will be use by maven to construct the website.
					The docbook file must be located in the src/site/docbook
					directory. And are generated in the target/filtered-site/xdoc directory -->
				<plugin>
					<groupId>org.codehaus.mojo</groupId>
					<artifactId>xml-maven-plugin</artifactId>
					<executions>
						<execution>
							<phase>pre-site</phase>
							<goals>
								<goal>transform</goal>
							</goals>
						</execution>
					</executions>
					<dependencies>
						<dependency>
							<groupId>net.sf.saxon</groupId>
							<artifactId>saxon</artifactId>
							<version>8.7</version>
						</dependency>
					</dependencies>
					<configuration>

						<transformationSets>
							<transformationSet>
								<dir>${docbook.source}</dir>
								<includes>
									<include>**/*.dbk</include>
								</includes>
								<stylesheet>
									${project.build.directory}/docbook-xsl/xhtml/docbook.xsl
								</stylesheet>
								<outputDir>
									${project.basedir}/target/filtered-site/xdoc
								</outputDir>
								<fileMappers>
									<fileMapper implementation="org.codehaus.plexus.components.io.filemappers.FileExtensionMapper">
										<targetExtension>
											.xml
										</targetExtension>
									</fileMapper>
								</fileMappers>
							</transformationSet>

						</transformationSets>

						<catalogs>
							<catalog>
								${docbook.source}/catalog.xml
							</catalog>

						</catalogs>

					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<profiles>
		<profile>
			<id>local-site-deploy</id>
			
			<properties>
				<local.deploy.dir>/tmp/fractaladl-site/${project.version}</local.deploy.dir>
			</properties>
			<distributionManagement>
				<site>
					<name>local site deployment</name>
					<url>file://${local.deploy.dir}</url>
				</site>
			</distributionManagement>
		</profile>

		<!-- Use this profile to generate the every maven reports. These reports
			have been extracted in a separated profile since they are pretty 
			long to generate; which may be painful when developing site pages.
			Note that this profile is automatically activated when a release is
			performed -->
		<profile>
			<id>full-reporting</id>

			<activation>
				<property>
					<name>performRelease</name>
					<value>true</value>
				</property>
			</activation>

			<reporting>
				<plugins>
					<!-- javadoc -->
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-javadoc-plugin</artifactId>
						<configuration>
							<windowtitle>
								${project.name} Documentation
							</windowtitle>
							<doctitle>
								${project.name} ${project.version} Documentation
							</doctitle>
							<docfilessubdirs>true</docfilessubdirs>
							<excludedocfilessubdir>.svn</excludedocfilessubdir>
							<links>
								<link>
									http://java.sun.com/j2se/1.5/docs/api
								</link>
							</links>
							<quiet>true</quiet>
							<use>false</use>
						</configuration>
					</plugin>

					<!-- source cross reference reports -->
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-jxr-plugin</artifactId>
					</plugin>

					<!-- project reports -->
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>
							maven-project-info-reports-plugin
						</artifactId>
					</plugin>

					<!-- test coverage -->
					<plugin>
						<groupId>org.codehaus.mojo</groupId>
						<artifactId>cobertura-maven-plugin</artifactId>
						<version>2.2</version>
					</plugin>

					<!-- quality metrics -->
					<plugin>
						<groupId>org.codehaus.mojo</groupId>
						<artifactId>jdepend-maven-plugin</artifactId>
					</plugin>

					<!-- source metrics -->
					<plugin>
						<groupId>org.codehaus.mojo</groupId>
						<artifactId>javancss-maven-plugin</artifactId>
						<version>2.0-beta-2</version>
					</plugin>

					<!-- PMD reports -->
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-pmd-plugin</artifactId>
						<configuration>
							<targetJdk>1.5</targetJdk>
						</configuration>
					</plugin>
				</plugins>
			</reporting>
		</profile>

		<profile>
			<id>release-profile</id>

			<activation>
				<property>
					<name>performRelease</name>
					<value>true</value>
				</property>
			</activation>

			<build>
				<plugins>
					<plugin>
						<!-- This Ant task will update the link to the current 
							documentation of fractaladl. -->
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-antrun-plugin</artifactId>
						<inherited>false</inherited>
						<configuration>
							<tasks>
								<sshexec host="${ow.hostname}" username="${ow.username}" passphrase="" keyfile="${user.home}/.ssh/id_rsa" command="cd ${ow.site.deploy.dir}; ln -nfs ${project.version} current" />
							</tasks>
						</configuration>
						<dependencies>
							<dependency>
								<groupId>org.apache.ant</groupId>
								<artifactId>ant-jsch</artifactId>
								<version>1.7.0</version>
							</dependency>
						</dependencies>
					</plugin>
				</plugins>
			</build>
		</profile>

	</profiles>
</project>