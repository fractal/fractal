<?xml version="1.0"?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN" "http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">
<!-- 
	* This library is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2 of the License, or (at your option) any later version.
	*
	* This library is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	*
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
-->

<book>
	<title>Fractal Concepts</title>
	
	<chapter id="introduction">
		<title>Introduction</title>	
			
		<para>This document presents the concepts of the Fractal component model.</para>
	
		<para>The Fractal model has been created in order to build, deploy and manage complex 
		systems like applications, middleware platform or operating systems. The model is based on the following principles:
		</para>

		<itemizedlist>
		<listitem>
		<para>
		composite components (component which contains sub-components) allowing to have a homogeneous view of the
		 software architecture at different abstraction levels.
		</para>
		</listitem>

		<listitem>
		<para>
		shared components (sub-components encapsulated by many different composite components) 
		allowing the modeling of shared ressources, without losing encapsulation properties.
		</para>
		</listitem>

		<listitem>
		<para>
		introspection capabilities allowing to explore system during the runtime execution.
		</para>
		</listitem>
		
		<listitem>
		<para>
		reconfiguration capabilities allowing to deploy and reconfigure systems at runtime execution.
		</para>
		</listitem>

		</itemizedlist>
		
		<para>The Fractal component model has been implemented in various programming languages,
		 Java, C, C++, Smalltalk...A Fractal implementation allows you to manipulate Fractal concepts in 
		 the target language. </para>
		 
		 <para>Implementations don't always implement all concepts for various reasons,there is often 
		 more than one way to map concepts.</para>
		 
		 <para>
		 The Fractal specification provides conformance levels. These conformance 
		 levels describe the number of Fractal concepts that are implemented by the implementation. 
		 It allows you to know that kind of concepts you  will be able to manipulate in the target 
		 implementation.
		 </para>
		 
		 <para>The reference implementation of the Fractal component model is named Julia, 
		 it is written in Java for Java programmers. We will use this implementation along this tutorial,
		  but in the first section we'll try just to talk about the Fractal concepts without care about 
		  limitations, or specificities created by implementations.
		 </para>
		 
		 <para>Fractal is a component model.
		 	With object model like Java you think problems as a set of objects.
			With a component model approach you think problems as a set of components.
		 </para>
		 
		 <para>But what is a component ? and more precisely what is the fractal definition of a component ?
			  And what is the utility of a such approach ?
		 </para>

	</chapter>
	
	<chapter id="fractal-concepts">
		<title>Fractal Concepts</title>
		
		
		<section>
		<title>Fractal Component Definition</title>
		<para>There is a lot of component models, EJB, OSGi...and of course a lot of component definitions.</para>
		
		<para>In the Fractal world, a component is an runtime entity with recursive structure and reflexive 
		capabilities. A component is composed of a content and a membrane and has access points 
		named interfaces.</para>
		
		<mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/component-definition.png"/>
				</imageobject>
		</mediaobject>
		</section>
		
		
		<section>
			<title>Interfaces</title>
			<para>Interfaces allow you to emit and receive calls. As you can see, there are different kinds of interfaces :</para>
			
			<mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/interfaces-definitions.png"/>
				</imageobject>
		</mediaobject>
		
		<itemizedlist>
			<listitem>
			<para>The provided (or server) interfaces are  access points to the functional services provided by
			 the component.  By convention, they are at graphically drawn at the left side of the component.
			 </para>
			</listitem>
			
			<listitem>
			<para>The required (or client) interfaces are the access points to receive services from others components.
			By convention, they are graphically drawn at the left side of the component.
			</para>
			</listitem>
			
			<listitem>
			<para>The control interfaces are access points to add non-business functionalities like 
			reflexivity, life-cycle or recursive properties of the component. We will see that control interfaces 
			are just provided interfaces with special names. By convention they are graphically drawn at
			the top side of the component.
			</para>
			</listitem>
		</itemizedlist>
		</section>
		
		<section>
			<title>Membrane and Content</title>
			
			<para>A Fractal component has two parts :</para>
			
			<itemizedlist>
			<listitem>
			<para>one membrane which have functional interfaces and control interfaces allowing
			 the introspection and the configuration of the component.
			</para>
			</listitem>
			<listitem>
			<para>one content which is composed by a finite set of sub-components.
			</para>
			</listitem>

			</itemizedlist>
			
			
			<para>The membrane is composed by a set of controllers. Controllers implement control 
			interfaces. Each controller has a special function : for example, to control the life 
			cycle of a component or control the behaviours of sub-components...</para>
		</section>
		
		<section>
			<title>Subcomponents</title>
			
			<para>A component can contain recursively other components. The subcomponents are stored 
			in the content of the component. A component that contains other components is named 
			composite component and has a control interface named "content-controller" that allows to add,
			 list and remove subcomponents from its content. A component without ContentController 
			 interface is named a primitive component because we can not inspect its content, it is a black 
			 box.
			</para>
			
			<mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/component-subcomponents.png"/>
				</imageobject>
		</mediaobject>
		
		<para>The same component can be added in different components, it becomes shared by different
		 parents (also named super-components). A component has a SuperController control interface that allows to access to the list 
		 of its parents.</para>
		 
		 <mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/shared-component.png"/>
				</imageobject>
		</mediaobject>
		</section>
		
		<section>
			<title>Component Name</title>
			
			<para>A component can have a name. Its name is controlled by the NameController control interface.</para>
			
			<mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/component-name.png"/>
				</imageobject>
		</mediaobject>
		</section>
		
		<section>
		<title>Life-Cycle</title>
		
		<para>A component has a life-cycle. This life-cycle can be controlled via the LifeCycle Controller
		 control interface of the component. A component can be in the state STARTED or STOPPED.
		</para>
		 
		 <mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/component-lifecycle.png"/>
				</imageobject>
		</mediaobject>
		
		</section>
		
		<section>
		<title>Component Attributes</title>
		
		<para>A component can also have attributes. These attributes are generally not represented 
		graphically, they can be controller via an AttributeController control interface.
		</para>
		
		
		</section>
		
		
		<section>
		<title>Bindings</title>
		
		<para>Sub components that are in the same component can be connected via bindings.</para>
		
		<mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/component-bindings.png"/>
				</imageobject>
		</mediaobject>
		
		<para>A binding is an oriented communication path between a required interface and a provided 
		interface.</para>
		
		<mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/binding-zoom.png"/>
				</imageobject>
		</mediaobject>
		
		<para>A binding belongs to the content of the component of the required interface. </para>
		
		<mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/binding-owner.png"/>
				</imageobject>
		</mediaobject>
		
		<para>All bindings of a component are controlled by the control interface named BindingController
		 (BC). This BindingController interface allows to bind and unbind the required interfaces of the 
		 component.
		</para>
		 
		 <mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/binding-owner-controller.png"/>
				</imageobject>
		</mediaobject>
		
		
		</section>
	</chapter>
	
	<chapter>
	<title>Concepts Details</title>
	
	<section>
	<title>Bindings Rules</title>
	<itemizedlist>
		<listitem>
			<para>A binding is a connection from a required interface to a provided interface.</para>
		</listitem>
		<listitem>
			<para>A binding can't pass across a membrane.</para>
		</listitem>
		<listitem>
			<para>The components related to the bindings must be in a common super-component.</para>
		</listitem>
	</itemizedlist>
	
	<para>Some examples :</para>
	
	<mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/binding-rules-membrane.png"/>
				</imageobject>
		</mediaobject>
		
		<mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/binding-rules-ok.png"/>
				</imageobject>
		</mediaobject>
		
	<mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/binding-rules-cross-membrane.png"/>
				</imageobject>
		</mediaobject>
		
		<para>Often we want what our composite provides services implemented infact by its subcomponents
		. So we would like to make a binding between the provided interface of a composite component and
		a provided interface of its sub component. But it is not possible to do this, because it does
		 not repect  the binding rules at all !</para>
		 
		 <mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/binding-rules-cross-2.png"/>
				</imageobject>
		</mediaobject>
		
		<para>To do this, a Fractal interface has the notion of visibility. A visibility of a 
		interface indicates if the interface is on the internal face of the membrane or at the 
		external face. For the moment all our interfaces were external interfaces. In practice 
		you'll only see external interfaces, the internal interfaces are automatically created 
		by the Fractal implementation. The connexion between the internal and external interfaces 
		of the membrane one is also created automatically.
		</para>
		
		<mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/binding-rules-internal.png"/>
				</imageobject>
		</mediaobject>
		
		<para>Note : As control interfaces are specials provided interfaces but are provided interfaces,
		 you can connect a required interface to a control interface.</para>
		 
		 <mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/binding-rules-control.png"/>
				</imageobject>
		</mediaobject>
		
		<para>It is also possible to bind a required interface of a component to one of
		its own provided interface.</para>
		
		<mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/binding-rules-own.png"/>
				</imageobject>
		</mediaobject>
	</section>
	
	<section>
	<title>Fractal Interface Definition</title>
	
	<para>We have already seen three kinds of Fractal interfaces : provided, required and
	 control interfaces. We have also seen that they have a visibility that can be external or internal.</para>
	
	<para>To be exhaustive a Fractal interface is described by : a name, a visibility and a type.</para>
	
	<para>The name is just a string that lets you have access to the interface. In practice component has a control interface
	that provides a <code>getFcInterface(in string interfaceName)</code> method that returns the Fractal interface identified by the name 'interfaceName'.
	</para>
	
	<mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/interface-name.png"/>
				</imageobject>
		</mediaobject>
	
	<para>The visibility, we have already seen it, can be external or internal. 
	In practice they are only used for composite component and are automatically created. </para>
	
	
	<title>Interface and Component Type </title>
	<para>Finally, the type of a Fractal interface is described by a name, a role, a cardinality, 
	a contingency and a signature.</para>
	<itemizedlist>
		<listitem>
		<para>role : required or provided (control interface is just a provided 
		interface with special name)
		</para>
		</listitem>
		<listitem>
		<para>contingency : it is a property of an interface that indicates if the functionality 
		of this interface is guaranteed to be available or not, while its component is running. 
		The contingency is either optional or mandatory. 
		</para>
		</listitem>
		
		<listitem>
		<para>cardinality : it is a property of an interface type that indicates how many
		 interfaces of this type a given component may have. The cardinality is either 
		 singleton or collection.
		</para>
		</listitem>
		
		<listitem>
		<para>signature :is the name of the language interface type corresponding to this component 
		interface.. In Julia which is a Java implementation of Fractal, the signature is the name of a 
		Java interface's class.</para>
		<mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/interface-signature.png"/>
				</imageobject>
		</mediaobject>
			
		</listitem>
	</itemizedlist>
	
	
	<para>Note: In practice Fractal interface is not a Java interface. 
	In Julia for example, a Fractal interface is a java object which implements 
	the java signature and other specific java interface allowing you to get the name, 
	type, and other attributes of a Fractal interface.
	</para>
	</section>
	
	
	<para>The type of a component is just the set of its interfaces.
	</para>
	<section>
	<title>Interfaces Access</title>
	
	<para>There are different kinds of interfaces and we can't access 
	to all of them by the same way.</para>
	
	<itemizedlist>
		<listitem>
		<para>required interfaces are controlled by the binding controller interface.</para>
		<mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/interface-access-binding.png"/>
				</imageobject>
			</mediaobject>
		</listitem>
		<listitem>
		<para>external interfaces are controlled by the component controller interface.</para>
		<mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/interface-access-component.png"/>
				</imageobject>
		</mediaobject>
		</listitem>
		<listitem>
		<para>internal interfaces are controlled by the content controller interface.</para>
		<mediaobject>
				<imageobject>
					<imagedata align="center" fileref="./images/interface-access-content.png"/>
				</imageobject>
		</mediaobject>
		</listitem>
	</itemizedlist>
	</section>
	
	<section>
	<title>Bootstrap Component and Factories</title>
	
	<para>In order to create components, and types, Fractal has the notion of factories.</para>
	<para>There is two important factories :</para>
	<itemizedlist>
	<listitem><para>the type factory: allowing the creation of interface types, and component types.</para></listitem>
	<listitem><para>the generic factory (component factory) : allowing the creation of components.</para></listitem>
	</itemizedlist>
	
	<para>These factories are accessible from the bootstrap component. The bootstrap component is 
	just a component which does not need to be created explicitly and which is directly accessible.
	 Note that the bootstrap component creates components but not need to contain them. 
	 The created components are not subcomponents of the bootstrap component.</para>
	</section>
	</chapter>
	
	<chapter>
		<title>Standard Fractal API</title>
		
		<para>To manipulate Fractal concepts we need APIs. 
		And you will see that the set of standard Fractal APIs is small. There is a mapping of the Fractal API for the IDL, C and Java language. 
		 Here we will describe the Java API.
		</para>
		
		<section>
		<title>Fractal Types</title>
		
		<programlisting>
public interface Type {
	boolean isFcSubTypeOf (Type type);
} 

public interface ComponentType extends org.objectweb.fractal.api.Type {
	InterfaceType[] getFcInterfaceTypes ();
	InterfaceType getFcInterfaceType (String name) throws NoSuchInterfaceException;
}

public interface InterfaceType extends org.objectweb.fractal.api.Type {
	String getFcItfName ();
	String getFcItfSignature ();
	boolean isFcClientItf ();
	boolean isFcOptionalItf ();
	boolean isFcCollectionItf ();
} 

public interface TypeFactory {
	InterfaceType createFcItfType (
		String name, String signature, boolean isClient, boolean isOptional, boolean isCollection)
 		throws org.objectweb.fractal.api.factory.InstantiationException;
	ComponentType createFcType (InterfaceType[] interfaceTypes)
		throws org.objectweb.fractal.api.factory.InstantiationException;
} 

public interface Interface {
	Component getFcItfOwner ();
	String getFcItfName ();
	Type getFcItfType ();
	boolean isFcInternalItf ();
} 
		</programlisting>
		
		<para>In Fractal all types are subtypes of Type. This Type interface just has one method :
		 boolean isFcSubTypeOf (in Type type); which allows to know if types are compatible. 
		 In practive you don't have to use it often. But for a Fractal implementation it means that 
		 your target language has to provide or simulate type checking. For example, in a Java world,
		  when we try to compare to Fractal interfaces, the Fractal implementation checks the role, 
		  the cardinality, and the contingency of the Fractal interfaces,
		   but it has also to check the compatibility of the signatures which is Java interface 
		   casting verification.
		  </para>
		  
		  <para>The InterfaceType describes the type of Fractal interfaces which are described by :</para>
		  <itemizedlist>
		  <listitem>
		  <para>a name</para>
		  </listitem>
		  <listitem>
		  <para>a signature</para>
		  </listitem>
		  <listitem>
		  <para>a role : CLIENT (true) or SERVER (false)</para>
		  </listitem>
		  <listitem>
		  <para>a contingency :OPTIONAL (true) or MANDATORY (false)</para>
		  </listitem>
		  <listitem>
		  <para>a cardinality : COLLECTION (true) or SINGLETON (false)</para>
		  </listitem>
		  </itemizedlist>
		  
		  <para>A  ComponentType is just a set of  InterfaceType.</para>
		  
		  <para>The <programlisting>InterfaceType[] getFcInterfaceTypes ();</programlisting> 
		  method returns all interface types that are part of the component type.</para>
		  
		  <para>The <programlisting>InterfaceType getFcInterfaceType (String name) throws NoSuchInterfaceException;</programlisting> 
		  returns the type of a specific Fractal interface identified by its name.</para>
		  
		  <para>Type factories have also a type which defines the methods that a type factorie 
		  must provide. No suprises, we find  <code>createFcItfType</code> and a <code>createFcType</code> methods to 
		  create respectively interface and component types.</para>
		  
		  <para>Finally the <programlisting>Interface</programlisting> java interface which is implemented by all Fractal interfaces. 
		  In practice julia creates a java object for each Fractal interface that implement the Interface
		   java interface and the java interface of the signature. The implementation of java interface of the signature 
		    is delegate to the java class associated to the component that holds the Fractal interface.
		  </para>
		  
		</section>
		
		<section>
		<title>Fractal Control</title>
		<para>In the Fractal specification, seven controllers have been identified to introspect and 
		manipulate the Fractal architecture. We have seen most of them.</para>
		
		<section>
		<title>Component control interface</title>
		<para>A component interface to introspect the external interfaces of the component to which it belongs.</para>
		<programlisting>
public interface Component {
	Type getFcType ();
	Object[] getFcInterfaces ();
	Object getFcInterface (String interfaceName) throws NoSuchInterfaceException;
} 
		</programlisting>
		
		<para>Note that the return type of each method has not a strong type. For example 
		<code>getFcType</code> returns Type and not <code>ComponentType</code>, for 
		<code>getFcInterface</code> a <code>Object</code> is returned 
		not a Interface.</para>
		
		<para>The Java object returned by <code>getFcInterface</code> can be cast in 
		a <code>Interface</code> object, or in the</para>
		</section>
		
		<section>
		<title>AttributeController control interface</title>
		<para>A component interface to control the attributes 
		of the component to which it belongs. </para>
		
		<programlisting>interface AttributeController { }; </programlisting>
		
		<para>It doesn't have methods because the name of the attributes are unknown.
		 The user will have to provide a control interface that extends this one and declare
		  custom getters and setters in order to control attributes.</para>
		</section>
		
		<section>
		<title>BindingController control interface</title>
		<para>A component interface to control the bindings of the component to which it belongs.</para>
		<programlisting>
public interface BindingController {
	String[] listFc ();
	Object lookupFc (String clientItfName) throws NoSuchInterfaceException;
	void bindFc (String clientItfName, Object serverItf) throws
    	NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException;
	void unbindFc (String clientItfName) throws
    	NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException;
} 
		</programlisting>
		</section>
		
		<section>
		<title>ContentController control interface</title>
		<para>A component interface to control the content of the component to which it belongs.</para>
		<programlisting>
public interface ContentController {
	Object[] getFcInternalInterfaces ();
	Object getFcInternalInterface (String interfaceName) throws NoSuchInterfaceException;
	Component[] getFcSubComponents ();
	void addFcSubComponent (Component subComponent)
		throws IllegalContentException, IllegalLifeCycleException;
	void removeFcSubComponent (Component subComponent)
    	throws IllegalContentException, IllegalLifeCycleException;
} 
		</programlisting>
		</section>
		
		<section>
		<title>LifeCycleController control interface</title>
		<para>A component interface to control the lifecycle of the component to which it belongs.</para>
		<programlisting>
public interface LifeCycleController {
	String getFcState ();
	void startFc () throws IllegalLifeCycleException;
	void stopFc () throws IllegalLifeCycleException;
} 
		</programlisting>
		</section>
		
		<section>
		<title>NameController control interface</title>
		<para> A component interface to control the name 
		of the component to which it belongs.</para>
		
		<programlisting>
public interface NameController {
	String getFcName ();
	void setFcName (String name);
} 
		</programlisting>
		</section>
		
		<section>
		<title>SuperController control interface</title>
		<para>A component interface to control the super components of the component to which 
		it belongs.</para>
		
		<programlisting>
public interface SuperController {
  Component[] getFcSuperComponents ();
} 
		</programlisting>
		</section>
		
		<para>If you want news controller you have to create them. In practice with Julia 
		implementation it is not easy.</para>
		
		</section>
		
		<section>
		<title>Fractal Factory APIs</title>
		
		<programlisting>
public interface GenericFactory {
	Component newFcInstance (Type type, Object controllerDesc, Object contentDesc)
    	throws InstantiationException;
}  
		</programlisting>
		
		<programlisting>
public interface Factory {
	Type getFcInstanceType ();
	Object getFcControllerDesc ();
	Object getFcContentDesc ();
	Component newFcInstance () throws InstantiationException;
} 
		</programlisting>
		
		</section>
	
	</chapter>
</book>