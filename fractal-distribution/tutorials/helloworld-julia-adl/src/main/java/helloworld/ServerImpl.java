package helloworld;

/** 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


public class ServerImpl implements Service, ServiceAttributes {

  private String header;

  public ServerImpl () {
		System.err.println("SERVER created");
	}
  
  public void print (final String msg) {
    System.out.println(header + msg);
  }

  public String getHeader () {
    return header;
  }

  public void setHeader (final String header) {
    this.header = header;
  }
}
