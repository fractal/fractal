Open three console :
-In the first one "mvn -Prun.registry"
-In the second one "mvn -Prun.server"
-In the third one "mvn -Prun.client"

For linux/mac users: 
Don't forget to add, if it is not present, the hostname and 
the ip in the /etc/hosts file else you'll have a "java.net.UnknownHostException".
Todo this:
-get the hostname of the machine with the "hostname" command
-get the inet address of the machine with a ifconfig
-add in the /etc/hosts file a line with the ip followed by the hostname:
 e.g "193.49.212.38 proutproutII".