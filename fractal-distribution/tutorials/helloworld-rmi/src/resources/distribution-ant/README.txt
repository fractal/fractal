Open three console :
-In the first one "ant registry"
-In the second one "ant server"
-In the third one "ant client"

For linux/mac users: 
Don't forget to add, if it is not present, the hostname and 
the ip in the /etc/hosts file else you'll have a "java.net.UnknownHostException".
Todo this:
-get the hostname of the machine with the "hostname" command
-get the inet address of the machine with a ifconfig
-add in the /etc/hosts file a line with the ip followed by the hostname:
 e.g "192.168.0.1 mycomputer".
 