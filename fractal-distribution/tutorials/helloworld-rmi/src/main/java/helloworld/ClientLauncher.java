/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributor: Philippe Merle
 *
 * $Id: ClientLauncher.java 3499 2007-07-17 23:51:39Z merle $
 */

package helloworld;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;

import org.objectweb.fractal.api.Component;

import org.objectweb.fractal.rmi.registry.NamingService;
import org.objectweb.fractal.rmi.registry.Registry;

import org.objectweb.fractal.util.Fractal;

public class ClientLauncher {

  public static void main (final String[] args) throws Exception {
    boolean useWrapper = false;
    for (int i = 0; i < args.length; ++i) {
      useWrapper |= args[i].equals("wrapper");
    }

    NamingService ns = Registry.getRegistry();

    String name = useWrapper ? "helloworld.WrappedClientServer" : "helloworld.ClientServer";
    
    Map context = new HashMap();
    context.put("remote-node", ns.lookup("server-host"));
    
    Factory f = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
    Component c = (Component)f.newComponent(name, context);

    // start root component
    Fractal.getLifeCycleController(c).startFc();

    // call main method
    ((Main)c.getFcInterface("m")).main(new String[0]);
  }
}
