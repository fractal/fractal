from org.objectweb.fractal.adl import *
from java.util import *
from helloworld import *
from org.objectweb.fractal.api import *
from org.objectweb.fractal.util import *

# Creation of the Fractal ADL factory
factory = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);

# The creation of the BasicFractalExplorer component need 2 arguments
# the name of the component to explore "name"
# and the adl definition "definition"
context = HashMap()
context.put("name", "ClientServerImpl")
context.put("definition", "helloworld.ClientServerImpl")

# Create BasicFractalExplorer component to launch fractal explorer
# on our ClientServerImpl component
root = factory.newComponent("org.objectweb.fractal.explorer.BasicFractalExplorer",context)

# get the subcomponents of the root component via the ContentController
components = Fractal.getContentController(root).getFcSubComponents()

# count the number of sub components normally 2
len(components)

# get the name of the component 
# explorer
Fractal.getNameController(components[0]).getFcName()

# ClientServerImpl
Fractal.getNameController(components[1]).getFcName()

cs = components[1]

# list the interfaces
Fractal.getBindingController(cs).listFc()

# by default components have a lifecycle controller,
# we can't call the interfaces of the component if it's stopped
Fractal.getLifeCycleController(cs).getFcState()

# start the component
Fractal.getLifeCycleController(cs).startFc()

#call the main method of the interface named "m"
Fractal.getBindingController(cs).lookupFc("m").main()

#RECONFIGUTATION
# start the component
Fractal.getLifeCycleController(cs).stopFc()

#unbind the binding between ClientServerImpl.m and client.m
Fractal.getBindingController(cs).unbindFc("m")

#get the list of the components in the ClientServerImpl component
subcomps = Fractal.getContentController(cs).getFcSubComponents()

#introspect to find the client
Fractal.getNameController(subcomps[0]).getFcName()
client = subcomps[0]
server = subcomps[1]

#unbind the binding between client.s and server.s
Fractal.getBindingController(cs).unbindFc("s")

#Add a new client
context = HashMap()
client2 = factory.newComponent("helloworld.ClientImpl2",context)

#Add client2 to the ClientServerImpl
Fractal.getContentController(cs).addFcSubComponent(client2)

#rename the component 
Fractal.getNameController(client2).setFcName("client2")

#bind the client2 with the server
Fractal.getBindingController(client2).bindFc("s",server.getFcInterface("s"))

#bind the ClientServerImpl with the client2
Fractal.getBindingController(cs).bindFc("m",client2.getFcInterface("m"))

#start the ClientServerImpl component
Fractal.getLifeCycleController(cs).startFc()

#call the main method of the interface named "m"
# so we now have the main method called from the client2
Fractal.getBindingController(cs).lookupFc("m").main()
