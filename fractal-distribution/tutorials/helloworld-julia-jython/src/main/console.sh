#!/bin/sh

DIR=$(pwd)
cd ../../..

LIB_DIR=$(pwd)/lib/
CP=
for i in lib/*.jar
do
  CP="${CP}:$(pwd)${i}"
done

EXTERNALS_DIR=$(pwd)/externals/
for i in externals/*.jar
do
  CP="${CP}:$(pwd)/${i}"
done

cd $DIR
DIST_DIR=$(pwd)/dist/
CP="$CP:dist/helloworld.jar"
 
java -Dfractal.provider="org.objectweb.fractal.julia.Julia" -classpath "$CP" org.python.util.jython "$@"  