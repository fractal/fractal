This is a simple example to show how to develop application 
with the fractal component model.

to run the example :
ant
sh console.sh

On FScript console type : 
FScript> froot = adl-new("helloworld.ClientServerExplorer");
FScript> :load helloworld.fscript
FScript> reconfigure($froot);