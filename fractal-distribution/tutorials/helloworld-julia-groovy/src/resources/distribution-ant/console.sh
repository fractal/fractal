#!/bin/sh

DIR=$(pwd)

CP=
for i in lib/*.jar
do
  CP="${CP}:$(pwd)/${i}"
done


CP="$CP:$(pwd)/dist/helloworld.jar"

java -Dfractal.provider="org.objectweb.fractal.julia.Julia" -classpath "$CP" groovy.ui.Console "$@"  
