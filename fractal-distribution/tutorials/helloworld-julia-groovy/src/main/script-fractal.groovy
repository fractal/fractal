import org.objectweb.fractal.adl.*;
import java.util.*;
import helloworld.*;
import org.objectweb.fractal.api.*;
import org.objectweb.fractal.util.*;

// Creation of the Fractal ADL factory
factoryfactory =  org.objectweb.fractal.adl.FactoryFactory;

factory = factoryfactory.getFactory(factoryfactory.FRACTAL_BACKEND);

// The creation of the BasicFractalExplorer component need 2 arguments
// the name of the component to explore "name"
// and the adl definition "definition"
context = new HashMap();
context.put("name", "ClientServerImpl");
context.put("definition", "helloworld.ClientServerImpl");

// Create BasicFractalExplorer component to launch fractal explorer
// on our ClientServerImpl component
root = factory.newComponent("org.objectweb.fractal.explorer.BasicFractalExplorer",context);

fractal = org.objectweb.fractal.util.Fractal;

// get the subcomponents of the root component via the ContentController
components = fractal.getContentController(root).getFcSubComponents();

// get the name of the first subcomponent : explorer
fractal.getNameController(components[0]).getFcName();

// get the name of the second subcomponent : ClientServerImpl
fractal.getNameController(components[1]).getFcName();

// we create a reference to our root component ClientServerImpl
cs = components[1];

//Explore the fractal architecture and play with fractal API
// display the name list of the component interfaces
// name-controller
// super-controller
// component
// r
// binding-controller
// lifecycle-controller
// content-controller

for(int i=0; i<cs.getFcInterfaces().length;i++){
   System.out.println((cs.getFcInterfaces()[i]).getFcItfName());
}

// display the list of the client interfaces :
// m
for(int i=0; i<fractal.getBindingController(cs).listFc().length;i++){
   System.out.println(fractal.getBindingController(cs).listFc()[i]);
}

// by default components have a lifecycle controller,
// we can't call the interfaces of the component if it's stopped
fractal.getLifeCycleController(cs).getFcState();
//Enf of the fractal architecture exploration

// Start the exploration of the Julia implementation (for curious).
// we can see that Julia create java object for fractal interface
// with a special class name org.objectweb.fractal.julia.generated.C1bf3a58b_0 for example
System.out.println(fractal.getBindingController(cs).lookupFc("r").getClass());

//we can use java reflection to try to understand how it is implemented :
// List the interfaces implemented by the java object that represents the fractal interface.
// We see that the helloworld.Main interface is implemented by the fractal interface java object
// But the code is delegates to our ClientImpl class.
//We can see that the
for(int i=0; i<fractal.getBindingController(cs).lookupFc("r").getClass().getInterfaces().length;i++){
   System.out.println(fractal.getBindingController(cs).lookupFc("r").getClass().getInterfaces()[i]);
}

// Display the list of the methods available on the fractal interface java object
for(int i=0; i<fractal.getBindingController(cs).lookupFc("r").getClass().getMethods().length;i++){
   System.out.println(fractal.getBindingController(cs).lookupFc("r").getClass().getMethods()[i]);
}
//End of the Julia exploration


//Come back to the code
// start the component
fractal.getLifeCycleController(cs).startFc();

//call the main method of the interface named "r"
fractal.getBindingController(cs).lookupFc("r").run();

//RECONFIGUTATION
// stop the component
fractal.getLifeCycleController(cs).stopFc();

//unbind the binding between ClientServerImpl.m and client.m
fractal.getBindingController(cs).unbindFc("r");

//get the list of the components in the ClientServerImpl component
subcomps = fractal.getContentController(cs).getFcSubComponents();

//introspect to find the client
fractal.getNameController(subcomps[0]).getFcName();
client = subcomps[0];
server = subcomps[1];

//unbind the binding between client.s and server.s
fractal.getBindingController(client).unbindFc("s");

//Add a new client
client2 = factory.newComponent("helloworld.ClientImpl2",null);

//Add client2 to the ClientServerImpl
fractal.getContentController(cs).addFcSubComponent(client2);

//Remove the old client
fractal.getContentController(cs).removeFcSubComponent(client);

//rename the component 
fractal.getNameController(client2).setFcName("client2");

//bind the client2 with the server
fractal.getBindingController(client2).bindFc("s",server.getFcInterface("s"));

//bind the ClientServerImpl with the client2
fractal.getBindingController(cs).bindFc("r",client2.getFcInterface("r"));

//start the ClientServerImpl component
fractal.getLifeCycleController(cs).startFc();

//call the main method of the interface named "r"
// so we now have the main method called from the client2
fractal.getBindingController(cs).lookupFc("r").run();

