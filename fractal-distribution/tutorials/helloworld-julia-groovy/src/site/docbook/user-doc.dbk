<?xml version="1.0"?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN" "http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">
<!-- 
	* This library is free software; you can redistribute it and/or
	* modify it under the terms of the GNU Lesser General Public
	* License as published by the Free Software Foundation; either
	* version 2 of the License, or (at your option) any later version.
	*
	* This library is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	* Lesser General Public License for more details.
	*
	* You should have received a copy of the GNU Lesser General Public
	* License along with this library; if not, write to the Free Software
	* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
-->

<book>
	<title>Fractal Tutorial : Groovy + Fractal</title>
	
	<chapter id="introduction">
		
		<title>Introduction</title>
		
		<para>In this tutorial we will show how we can manipulate easily our Fractal architecture at 
		the runtime with Groovy. We'll be able to see graphically the evolution of our architecture
		 with a tool named Fractal Explorer.
		</para>
		
		<para>We will not describe the source code of the example it is quite the same than the one used in previous tutorials.
		</para>
	</chapter>
	
	<chapter id="groovy-shell">
	<title>The Groovy shell</title>
	<para>The first thing we need to do to run and reconfigure our helloworld application in groovy,
	is to launch the groovy shell.</para>
	
	<para>With ant, we first need to package the helloworld application in a jar :</para>
	<programlisting>
ant dist
	</programlisting>
	<para>We can now run the groovy shell</para>
	
	<para>Linux : </para>
	<programlisting>
bash $FRACTAL_HOME/bin/fractal.sh -cp dist/helloworld.jar -groovy
	</programlisting>

	<para>Windows : </para>
	<programlisting>
%FRACTAL_HOME%\bin\fractal.bat -cp dist/helloworld.jar -groovy
	</programlisting>
	
	<para>With Maven, just type :</para>
	<programlisting>
mvn -Prun
	</programlisting>
	
	<para>Once the groovy shell is launched, we can instanciate our helloworld architecture by calling the Fractal 
	Launcher on the ClientServerExplorer component.</para>
	
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>import org.objectweb.fractal.adl.*;</userinput>
<computeroutput>groovy>  </computeroutput><userinput>import java.util.*;</userinput>
<computeroutput>groovy>  </computeroutput><userinput>import helloworld.*;</userinput>
<computeroutput>groovy>  </computeroutput><userinput>import org.objectweb.fractal.api.*;</userinput>
<computeroutput>groovy>  </computeroutput><userinput>import org.objectweb.fractal.util.*;</userinput>
</programlisting>

	 <para>Creation of the Fractal ADL factory.</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>factoryfactory =  org.objectweb.fractal.adl.FactoryFactory;</userinput>
<computeroutput>groovy>  </computeroutput><userinput>factory = factoryfactory.getFactory(factoryfactory.FRACTAL_BACKEND);</userinput>
</programlisting>

	 <para>Fractal Explorer provides a Fractal component named 'BasicFractalExplorer' that
	 allows to explore its subcomponents.</para>
	 
	 <para>
	 The creation of the BasicFractalExplorer component need 2 arguments
	 the name of the component to explore "name"
	 and the adl definition "definition".</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>context = new HashMap();</userinput>
<computeroutput>groovy>  </computeroutput><userinput>context.put("name", "ClientServerImpl");</userinput>
<computeroutput>groovy>  </computeroutput><userinput>context.put("definition", "helloworld.ClientServerImpl");</userinput>
</programlisting>

<para>We create a BasicFractalExplorer.</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>root = factory.newComponent("org.objectweb.fractal.explorer.BasicFractalExplorer",context);</userinput>
</programlisting>
	
	<para>Once this code is entered or copied in the console, type :</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>execute</userinput>
</programlisting>
	
	<para> If you've made a mistake during the copying execute may fails with a
'statement not complete' message. You can type <userinput>discard</userinput> on the console.</para>

	<para>Normally the Fractal Explorer window will appear.</para>
	
	
	<mediaobject>
			<imageobject>
					<imagedata align="center" fileref="./images/fractal-explorer-window-1.png"/>
			</imageobject>
	</mediaobject>
	<literallayout>
	</literallayout>
	
	<para>Now we can explore our architecture. Fractal Explorer provides also a graphical view, 
	you can launch it with the "graph view" item of the "Roles" menu.</para>
	
	<mediaobject>
			<imageobject>
					<imagedata align="center" fileref="./images/fractal-explorer-window-2.png"/>
			</imageobject>
	</mediaobject>
	<literallayout>
	</literallayout>
	
	<para>Now click on our root component ClientServerImpl, the architecture of the component
	 is displayed.</para>
	 
	 <mediaobject>
			<imageobject>
					<imagedata align="center" fileref="./images/fractal-explorer-window-3.png"/>
			</imageobject>
	</mediaobject>
	<literallayout>
	</literallayout>

	<section>
	<title>Exploration</title>	
	
	<para>Scripting is really useful to introspect,  explore the Fractal architecture, 
	and play with the Fractal API.</para>
	
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>fractal = org.objectweb.fractal.util.Fractal;</userinput>
</programlisting>

	<para>get the subcomponents of the root component via the ContentController interface</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>components = fractal.getContentController(root).getFcSubComponents();</userinput>
</programlisting>

	<para>get the name of the first subcomponent : explorer</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>fractal.getNameController(components[0]).getFcName();</userinput>
</programlisting>

	<para>get the name of the second subcomponent : ClientServerImpl</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>fractal.getNameController(components[1]).getFcName();</userinput>
</programlisting>

	<para>we create a reference to our root component ClientServerImpl</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>cs = components[1];</userinput>
</programlisting>

	<para>start the component</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>fractal.getLifeCycleController(cs).startFc();</userinput>
</programlisting>

	<para>call the run method of the interface named "r"</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>cs.getFcInterface("r").run();</userinput>
</programlisting>
	</section>

	<section>
	<title>Reconfiguration</title>
	<para>start the component</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>fractal.getLifeCycleController(cs).stopFc();</userinput>
</programlisting>

	<para>unbind the binding between ClientServerImpl.m and client.m</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>fractal.getBindingController(cs).unbindFc("m");</userinput>
</programlisting>

	<para>get the list of the components in the ClientServerImpl component</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>subcomps = fractal.getContentController(cs).getFcSubComponents();</userinput>
</programlisting>

	<para>introspect to find the client</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>fractal.getNameController(subcomps[0]).getFcName();</userinput>
<computeroutput>groovy>  </computeroutput><userinput>client = subcomps[0];</userinput>
<computeroutput>groovy>  </computeroutput><userinput>server = subcomps[1];</userinput>
</programlisting>

	<para>unbind the binding between client.s and server.s</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>fractal.getBindingController(client).unbindFc("s");</userinput>
</programlisting>
	
	<para>We can update Fractal Explorer and see that the binding has been correctly removed:
	</para>
	
	 <mediaobject>
			<imageobject>
					<imagedata align="center" fileref="./images/fractal-explorer-window-4.png"/>
			</imageobject>
	</mediaobject>
	<literallayout>
	</literallayout>
	
		<para>Create a new client</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>client2 = factory.newComponent("helloworld.ClientImpl2",null);</userinput>
</programlisting>

	<para>Add client2 to the ClientServerImpl</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>fractal.getContentController(cs).addFcSubComponent(client2);</userinput>
</programlisting>
	
	<para>By default the new component is displayed at the top left corner you have to move it to display it correctly.</para>
	<mediaobject>
			<imageobject>
					<imagedata align="center" fileref="./images/fractal-explorer-window-5.png"/>
			</imageobject>
	</mediaobject>
	<literallayout>
	</literallayout>
	
	<para>Remove the old client</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>fractal.getContentController(cs).removeFcSubComponent(client);</userinput>
</programlisting>

	<para>rename the component</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>fractal.getNameController(client2).setFcName("client2");</userinput>
</programlisting>

	<para>bind the client2 with the server</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>fractal.getBindingController(client2).bindFc("s",server.getFcInterface("s"));</userinput>
</programlisting>

	<para>bind the ClientServerImpl with the client2</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>fractal.getBindingController(cs).bindFc("r",client2.getFcInterface("r"));</userinput>
</programlisting>
	</section>

	<para>start the ClientServerImpl component</para>
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>fractal.getLifeCycleController(cs).startFc();</userinput>
</programlisting>
	
	<mediaobject>
			<imageobject>
					<imagedata align="center" fileref="./images/fractal-explorer-window-6.png"/>
			</imageobject>
	</mediaobject>
	<literallayout>
	</literallayout>

	<para>call the run method of the interface named "r"
 	so we now have the main method called from the client2
	and a new message <computeroutput>->Hello I'm another Client</computeroutput>
	</para>
		
<programlisting>
<computeroutput>groovy>  </computeroutput><userinput>cs.getFcInterface("r").run();</userinput>
</programlisting>
	
	<para>As you can see scripting languages like groovy is very interesting for prototyping 
	and exploration of the recursive and reflexives capabilities of the fractal component model.</para>
	
	</chapter>
</book>