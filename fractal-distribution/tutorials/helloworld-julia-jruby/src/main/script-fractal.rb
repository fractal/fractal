require 'ast-core-2.3-20070906.075052-2.jar'     
require 'fractal-explorer-1.1.3-SNAPSHOT.jar'      
require 'fscript-1.0-SNAPSHOT.jar'          
require 'julia-mixins-2.5.2-SNAPSHOT.jar'
require 'dumper-0.1.jar'                         
require 'fractal-rmi-0.3.6-20070906.073945-2.jar'  
require 'fscript-console-1.0-SNAPSHOT.jar'  
require 'julia-runtime-2.5.2-SNAPSHOT.jar'
require 'fractal-adl-2.3-20070906.075052-3.jar'  
require 'fractal-spoonlet-2.0.3-SNAPSHOT.jar'      
require 'jgraph-fractal.jar'                
require 'task-deployment-2.3-20070906.075052-2.jar'
require 'fractal-api-2.0.2.jar'                  
require 'fractal-util-1.0.jar'                     
require 'julia-asm-2.5.2-SNAPSHOT.jar'      
require 'task-framework-2.3-20070906.075052-2.jar'
require 'antlr-2.7.7.jar'         
require 'bsh-2.0b4.jar'        
require 'explorer-1.0.jar'                   
require 'jruby-complete-1.0.1.jar'  
require 'log4j-1.2.9.jar'      
require 'ow-trace-1.0.jar'
require 'apollon-runtime-1.0.jar'  
require 'commandline-1.0.jar'  
require 'explorer-reflector-plugin-1.0.jar'  
require 'jsap-2.1.jar'              
require 'monolog-1.8.jar'      
require 'spoon-core-1.2.jar'
require 'asm-3.0.jar'              
require 'core-3.2.0.666.jar'   
require 'jdom-1.0.jar'                       
require 'junit-3.8.1.jar'           
require 'monolog-api-1.8.jar'  
require 'xercesImpl-2.4.0.jar'
require 'aval-0.7.1.jar'           
require 'dtdparser-1.21.jar'   
require 'jline-0.9.9.jar'                    
require 'jython-2.2-alpha1.jar'     
require 'ow-misc-1.0.jar'
require 'helloworld.jar'

require 'java'

include Java

factoryfactory = org.objectweb.fractal.adl.FactoryFactory
factory = factoryfactory.getFactory(factoryfactory::FRACTAL_BACKEND)

context = java.util.HashMap.new
context.put("name", "ClientServerImpl")
context.put("definition", "helloworld.ClientServerImpl")

#obscur error happends
#root = factory.newComponent("org.objectweb.fractal.explorer.BasicFractalExplorer",context)

root = factory.newComponent("helloworld.ClientServerImpl",context)

fractal = org.objectweb.fractal.util.Fractal
fractal.getLifeCycleController(root).startFc()

fractal.getBindingController(root).lookupFc("m").main()
