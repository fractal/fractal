package helloworld;
/***
 * Fractal Hello World Example
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Eric Bruneton
 * Author: Lionel Seinturier
 */
import org.objectweb.fractal.fraclet.annotation.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotation.annotations.FractalComponent;
import org.objectweb.fractal.fraclet.annotation.annotations.Interface;
import org.objectweb.fractal.fraclet.annotation.annotations.Provides;


@FractalComponent
@Provides(interfaces=@Interface(name="s",signature=Service.class))
public class ServerImpl implements Service {

  @Attribute(value="hello")
  private String header;
  
  @Attribute(value="1")
  private int count;

  public ServerImpl () {
    System.err.println("SERVER created");
  }
  
  public void print (final String msg) {
    new Exception() {
        private static final long serialVersionUID = 3881199107085394940L;
        public String toString () {
            return "Server: print method called";
        }
    }.printStackTrace();
    System.err.println("Server: begin printing...");
    for (int i = 0; i < count; ++i) {
      System.err.println(header + msg);
    }
    System.err.println("Server: print done.");
  }

}
