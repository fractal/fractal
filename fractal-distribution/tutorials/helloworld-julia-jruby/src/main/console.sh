#!/bin/sh

DIR=$(pwd)
cd ../../..

LIB_DIR=$(pwd)/lib/
CP=
for i in lib/*.jar
do
  CP="${CP} -I$(pwd)${i}"
done

EXTERNALS_DIR=$(pwd)/externals/
for i in externals/*.jar
do
  CP="${CP} -I$(pwd)/${i}"
done

cd $DIR
DIST_DIR=$(pwd)/dist/
CP="$CP -Idist/helloworld.jar"

LIBS="-I$LIB_DIR -I$EXTERNALS_DIR -I$DIST_DIR"
java -Dfractal.provider="org.objectweb.fractal.julia.Julia" -jar ../externals/jruby-complete-1.0.1.jar $LIBS --command irb
