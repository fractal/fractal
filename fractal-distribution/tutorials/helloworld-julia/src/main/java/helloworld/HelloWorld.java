package helloworld;

/** 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;

import org.objectweb.fractal.util.Fractal;

public class HelloWorld {
	
	public static void main (final String[] args) throws Exception {

		

		Component boot = Fractal.getBootstrapComponent();
		TypeFactory tf = Fractal.getTypeFactory(boot);
		
		// type of root component
		ComponentType rType = tf.createFcType(new InterfaceType[] {
				tf.createFcItfType("r", "java.lang.Runnable", false, false, false)
		});
		// type of client component
		ComponentType cType = tf.createFcType(new InterfaceType[] {
				tf.createFcItfType("r", "java.lang.Runnable", false, false, false),
				tf.createFcItfType("s", "helloworld.Service", true, false, false)
		});
		// type of server component
		ComponentType sType = tf.createFcType(new InterfaceType[] {
				tf.createFcItfType("s", "helloworld.Service", false, false, false),
				tf.createFcItfType("attribute-controller", "helloworld.ServiceAttributes", false, false, false)
		});

		GenericFactory cf = Fractal.getGenericFactory(boot);

		// create root component
		Component rComp = cf.newFcInstance(rType, "composite", null);
		// create client component
		Component cComp = cf.newFcInstance(cType, "primitive", "helloworld.ClientImpl");
		// create server component
		Component sComp = cf.newFcInstance(sType, "primitive", "helloworld.ServerImpl");
	
		// component assembly
		Fractal.getContentController(rComp).addFcSubComponent(cComp);
		Fractal.getContentController(rComp).addFcSubComponent(sComp);
		Fractal.getBindingController(rComp).bindFc("r", cComp.getFcInterface("r"));
		Fractal.getBindingController(cComp).bindFc("s", sComp.getFcInterface("s"));

		ServiceAttributes sa = (ServiceAttributes)sComp.getFcInterface("attribute-controller");
		sa.setHeader("->");
		
		// start root component
		Fractal.getLifeCycleController(rComp).startFc();
		
		// call main method
		((java.lang.Runnable)rComp.getFcInterface("r")).run();
	}
}
