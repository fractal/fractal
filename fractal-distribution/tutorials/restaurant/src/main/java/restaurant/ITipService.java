package restaurant;

public interface ITipService {
	double getPriceWithTip(double price);
}
