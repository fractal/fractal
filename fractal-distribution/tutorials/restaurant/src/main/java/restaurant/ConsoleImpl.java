package restaurant;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Interface;
import org.objectweb.fractal.fraclet.annotations.Requires;

@Component(provides= {@Interface(name="r",signature=java.lang.Runnable.class)})
public class ConsoleImpl implements java.lang.Runnable{
	@Requires(name = "s")
	public IRestaurantService restaurantService;
	
	public void run(){
		IMenu[] menus=restaurantService.getMenus();
		System.out.println("--Menu--");
		int i=0;
		for(IMenu m : menus){
			System.out.println(++i + " - " + m.printMenu());
			
		}
		System.out.println("--------");
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int choice=-1;
		try{
		while(choice==-1){
			System.out.println("? ");
			String c = br.readLine();
			try{
				choice=new Integer(c).intValue();
				if(choice<1 || choice>menus.length){choice = -1;}
			}catch(NumberFormatException e){}	
		}
		}catch(IOException e){}
		System.out.println();
		IMenu menu=menus[choice-1];
		System.out.println("Your choice: " + menu.printMenu());
		System.out.println("Price: " + restaurantService.getBill(menu));
	}
}
