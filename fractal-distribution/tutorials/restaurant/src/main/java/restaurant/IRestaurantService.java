package restaurant;

public interface IRestaurantService {
	IMenu[] getMenus();
	double getBill(IMenu menu);
}
