package restaurant;

import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Interface;
import org.objectweb.fractal.fraclet.annotations.Requires;

@Component(provides= {@Interface(name="s",
		signature=restaurant.IRestaurantService.class)})
public class RestaurantServiceImpl implements IRestaurantService{
	private IMenu[] menus;
	private double[] prices;
	
	@Requires(name = "vs")
	private IVatService vs;
	
	@Requires(name = "ts")
	private ITipService ts;
	
	public RestaurantServiceImpl(){
		menus=new IMenu[]{
				new MenuImpl(0,"Grilled hamburger with French fries"),
				new MenuImpl(1,"Roasted chicken with vegetables"),
				new MenuImpl(2,"Duck breast in an orange sauce"),
				new MenuImpl(3,"Duck fois gras & mango chutney")
		};
		prices = new double[]{10,15,35,50};
	}
	
	public double getBill(IMenu menu){
		double menuPrice=prices[((MenuImpl)menu).getId()];
		double priceWithTaxRate=vs.getPriceWithVat(menuPrice);
		double priceWithTipRate=ts.getPriceWithTip(priceWithTaxRate);
		return priceWithTipRate;
	}
	
	public IMenu[] getMenus(){return menus;}
}
