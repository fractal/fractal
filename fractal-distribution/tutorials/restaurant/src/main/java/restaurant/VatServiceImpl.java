package restaurant;

import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Interface;

@Component(provides = @Interface(name = "v", 
		signature = restaurant.IVatService.class))
public class VatServiceImpl implements IVatService{
	@Attribute(value="19.6")
	private double vatRate;
	
	public double getPriceWithVat(double price) {
		return price*vatRate/100+price;
	}
}
