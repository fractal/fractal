package restaurant;

import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Interface;

@Component(provides = @Interface(name = "t", 
		signature = restaurant.ITipService.class))
public class TipServiceImpl implements ITipService{
	@Attribute(value="10")
	private double tipRate;
	
	public double getPriceWithTip(double price){
		return price*tipRate/100+price;
	}
}
