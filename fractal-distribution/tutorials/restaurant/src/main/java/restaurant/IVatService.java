package restaurant;

public interface IVatService {
	double getPriceWithVat(double price);
}
