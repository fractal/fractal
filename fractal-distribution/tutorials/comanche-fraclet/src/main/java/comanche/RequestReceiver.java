/*==============================================================================
 The Fraclet Programming Model - Fractal Component Model (fractal@objectweb.org)
 Copyright (C) 2002-2006 INRIA Futurs / LIFL
 Copyright (C) 2006-2007 Universitetet i Oslo

 This library is free software; you can redistribute it and/or modify it under 
 the terms of the GNU Lesser General Public License as published by the Free 
 Software Foundation; either version 2.1 of the License, or any later version.

 This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License along 
 with this library; if not, write to the Free Software Foundation, Inc., 
 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

 Initial developer(s): Romain Rouvoy
 Contributor(s): 
 =============================================================================*/
package comanche;

import static java.util.logging.Level.SEVERE;
import static java.util.logging.Logger.getLogger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotations.Component;
import org.objectweb.fractal.fraclet.annotations.Interface;
import org.objectweb.fractal.fraclet.annotations.Requires;

/**
 * Comanche request receiver component.
 * 
 * @author Romain Rouvoy (Initial Developer)
 * @version $Revision$
 */
@Component(provides = @Interface(name = "r", signature = Runnable.class))
public class RequestReceiver implements Runnable {
    private final Logger log = getLogger("comanche");
    @Attribute(value = "8080")
    private int port;
    @Requires
    private Scheduler s;
    @Requires
    private RequestHandler rh;

    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    public void run() {
        try {
            ServerSocket ss = new ServerSocket(this.port);
            log.info("Comanche HTTP Server ready on port " + this.port);
            log.info("Load http://localhost:" + this.port + "/gnu.jpg");
            while (true) {
                final Socket socket = ss.accept();
                s.schedule(new Runnable() {
                    public void run() {
                        try {
                            rh.handleRequest(new Request(socket));
                        } catch (IOException _) {
                        }
                    }
                });
            }
        } catch (IOException e) {
            log.log(SEVERE, "Comanche failure: ", e);
        }
    }
}
