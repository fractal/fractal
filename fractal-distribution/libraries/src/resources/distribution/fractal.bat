rem Script to facilitate Fractal application launching
rem TODO: check arguments compatibility, check that the FRACTAL_LIBRARIES exists

@echo off

set CP=.

rem get the path of the batch script
set BATCH_PATH=%~dp0

rem get the path of fractal libraries
rem "~0,-5" to remove "\bin" from the BATCH_PATH
set FRACTAL_LIBRARIES=%BATCH_PATH:~0,-5%\lib

goto Start


:AppendClasspath
rem echo CP:%CP% - arg:%1
set CP=%CP%;%1
GOTO :eof

:Start
for %%i in (%FRACTAL_LIBRARIES%\*.jar) do (call :AppendClasspath %%i)

if [%1]==[-h] goto Help
if not [%1]==[-h] goto ParseArgs

:Help 
echo fractal.bat usage :
echo		-cp [class search path of directories and zip/jar files]
echo			A : separated list of directories, JAR archives,
echo			and ZIP archives to search for class files.
echo		-jvmargs \"[-D<name>=<value>]*\" arguments passed to the JVM
echo		-fscript a option to start the Fscript console
echo		-groovy a option to start the groovy console
echo		-adl [adlfile.fractal] option to start a Fractal ADL file
echo		-itf [interface name] option to specify which fractal interface must be launch
echo		-class [class name] option to start a java class
echo		-args extra arguments
echo.
echo		examples :
echo		To use the script you sould create a jar with your Java classes and Fractal files add 
echo		it to the classpath with the -cp option.
echo		By default, all jars contained in the lib folder %FRACTAL_LIBRARIES%.
echo.	
echo		fractal.bat  -cp dist/helloworld-julia-adl.jar -fscript
echo		Fscript is started in console mode with a custom jar added to the classpath.
echo. 	
echo		fractal.bat  -cp dist/helloworld-julia-adl.jar -groovy
echo		Groovy is started in console mode with a custom jar added to the classpath.
echo. 	
echo		fractal.bat  -cp dist/helloworld-julia-adl.jar -adl helloworld.ClientServerImpl
echo		Fractal ADL Launcher is called with the helloworld.ClientServerImpl Fractal definition 
echo		as argument. If the Fractal definition has a Fractal interface named 'r' with a 
echo		java.lang.Runnable as signature, the interface will be launched.
echo.
echo		fractal.bat  -cp dist/helloworld-julia-adl.jar -adl helloworld.ClientServerImpl -fe
echo		Launch Fractal Explorer on the helloworld.ClientServerImpl Fractal definition. 
echo		The name associated to the Fractal definition, needed by Fractal Explorer is set to "RootComposite"		
echo. 
echo		fractal.bat  -cp dist/helloworld-julia-adl.jar -class helloworld.HelloWorld
echo		Launch the helloworld.HelloWorld java class.
goto End


:ParseArgs
@echo off
SET opt_jvmargs=
SET opt_fexplorer="False"
SET opt_fscript="False"
SET opt_groovy="False"
SET opt_cp=
SET opt_adl=
SET opt_itf="r"
SET opt_class=
SET opt_args=

SET LAST_COMMAND="";
SET IS_COMMAND="False";

SET /a i=0
:Loop
	if [%1]==[] goto EndLoop
		if [%1]==[-jvmargs] SET LAST_COMMAND=%1
		if [%1]==[-groovy] SET LAST_COMMAND=%1
		if [%1]==[-fscript] SET LAST_COMMAND=%1
		if [%1]==[-fe] SET LAST_COMMAND=%1
		if [%1]==[-cp] SET LAST_COMMAND=%1
		if [%1]==[-adl] SET LAST_COMMAND=%1
		if [%1]==[-itf] SET LAST_COMMAND=%1
		if [%1]==[-class] SET LAST_COMMAND=%1
		if [%1]==[-args] SET LAST_COMMAND=%1
		
		if [%1]==[-jvmargs] SHIFT
		if [%1]==[-groovy] SHIFT
		if [%1]==[-fscript] SHIFT
		if [%1]==[-fe] SHIFT
		if [%1]==[-cp] SHIFT
		if [%1]==[-adl] SHIFT
		if [%1]==[-itf] SHIFT
		if [%1]==[-class] SHIFT
		if [%1]==[-args] SHIFT
		
		if [%1]==[-jvmargs] goto Loop
		if [%1]==[-groovy] goto Loop
		if [%1]==[-fscript] goto Loop
		
		if [%LAST_COMMAND%]==[-jvmargs] SET opt_jvmargs=%1
		if [%LAST_COMMAND%]==[-groovy] SET opt_groovy="True"
		if [%LAST_COMMAND%]==[-fscript] SET opt_fscript="True"
		if [%LAST_COMMAND%]==[-fe] SET opt_fexplorer="True"
		if [%LAST_COMMAND%]==[-cp] SET opt_cp=%1
		if [%LAST_COMMAND%]==[-adl] SET opt_adl=%1
		if [%LAST_COMMAND%]==[-itf] SET opt_itf=%1
		if [%LAST_COMMAND%]==[-class] SET opt_class=%1
		if [%LAST_COMMAND%]==[-args] SET opt_args=%opt_args% %1
	SHIFT
	goto Loop
:EndLoop

rem We check what the user want to launch
rem All 'start' variables are exclusive, only one can be launched
set start_java="False"
set start_fexplorer="False"
set start_fscript="False"
set start_groovy="False"
set start_adl="False"

if not [%opt_class%]==[] set start_java="True"
if [%opt_groovy%]==["True"] set start_groovy="True"
if [%opt_fscript%]==["True"] set start_fscript="True"
if not [%opt_adl%]==[] if [%opt_fexplorer%]==["False"] set start_adl="True"
if not [%opt_adl%]==[] if [%opt_fexplorer%]==["True"] set start_fexplorer="True"

rem We launch the appropriate program
if [%start_java%]==["True"] java %opt_jvmargs% -Dfractal.provider="org.objectweb.fractal.julia.Julia" -classpath "%CP%;%opt_cp%" "%opt_class%" "%opt_args%"
if [%start_adl%]==["True"] java %opt_jvmargs% -Dfractal.provider="org.objectweb.fractal.julia.Julia" -classpath "%CP%;%opt_cp%" org.objectweb.fractal.adl.Launcher -fractal "%opt_adl%" "%opt_itf%" 
if [%start_fexplorer%]==["True"] java %opt_jvmargs% -Dfractal.provider="org.objectweb.fractal.julia.Julia" -classpath "%CP%;%opt_cp%" org.objectweb.fractal.adl.Launcher -name="RootComposite" -definition="%opt_adl%" -fractal org.objectweb.fractal.explorer.BasicFractalExplorer
if [%start_fscript%]==["True"] java %opt_jvmargs% -Dfractal.provider="org.objectweb.fractal.julia.Julia" -classpath "%CP%:%opt_cp%" org.objectweb.fractal.fscript.console.Main
if [%start_groovy%]==["True"] java %opt_jvmargs% -Dfractal.provider="org.objectweb.fractal.julia.Julia" -classpath "%CP%:%opt_cp%" groovy.ui.Console
goto End


:End
rem echo ------------------------------------
rem echo arguments : 
rem echo opt_jvmargs %opt_jvmargs%
rem echo opt_cp %opt_cp%
rem echo opt_adl %opt_adl%
rem echo opt_itf %opt_itf%
rem echo opt_class %opt_class%
rem echo opt_args %opt_args%