#!/usr/bin/perl -w

# $Id: compskel.pl 491 2006-11-13 07:54:08Z kofron $
#
# Behavior Protocols extensions for static and runtime checking
# developed for the Julia implementation of Fractal.
#
# Copyright (C) 2006
#    Formal Methods In Software Engineering Group
#    Institute of Computer Science
#    Academy of Sciences of the Czech Republic
#
# Copyright (C) 2006 France Telecom
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# Contact: ft@nenya.ms.mff.cuni.cz
# Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>


my $className="";

my @clientItfs;
my @serverItfs;

# ----------------------------------------------------------------------------------------------------------
sub varName
{
my ($itfName)=@_;
  return lcfirst($itfName);
}

# ----------------------------------------------------------------------------------------------------------

while (<>) {
  my ($itfName,$itfSig,$clsName);

  if (($itfName,$itfSig) = /<interface name="([^"]*)" role="client" signature="[^"]*\.([^"]*)"/) {
    push @clientItfs, {"name"=>$itfName, "signature"=>$itfSig};

  } elsif (($itfSig) = /<interface name="[^"]*" role="server" signature="[^"]*\.([^"]*)"/) {
    push @serverItfs, $itfSig;

  } elsif (($clsName) = /<content class="([^"]*)"\/>/) {
    $className=$clsName;
  }
}

print "package org.objectweb.dsrg.bpc.demo;\n";

if (@clientItfs) {
  print "import org.objectweb.fractal.api.NoSuchInterfaceException;\n";
  print "import org.objectweb.fractal.api.control.BindingController;\n";
}

print "\n";
print "public class $className";

if (@clientItfs or @serverItfs) {
  my @implItfs=@serverItfs;
  unshift @implItfs, "BindingController" if @clientItfs;

  print " implements ".join(", ",@implItfs);
}

print " {\n\n";

if (@clientItfs) {
  for my $itf (@clientItfs) {
    my $itfName=$itf->{"name"};
    my $itfSig=$itf->{"signature"};
    my $varName=varName($itfName);
    
    print "\tprotected $itfSig $varName;\n";
  }

  print "\n";
}

print "\tpublic $className() {\n";
print "\t}\n";

if (@clientItfs) {
  my $firstItf;

	print "\n\n";
  print "\t// Binding controller methods\n";
	print "\t// --------------------------\n";

  print "\tpublic String[] listFc() {\n";
	print "\t\treturn new String[] { " . join(", ", map { "\"" . $_->{"name"} . "\"" } @clientItfs) . " };\n";
  print "\t}\n\n";

  print "\tpublic Object lookupFc(String cItf) throws NoSuchInterfaceException {\n";

  for my $itf (@clientItfs) {
    my $itfName=$itf->{"name"};
    my $varName=varName($itfName);
    
    print "\t\tif (cItf.equals(\"$itfName\")) {\n";
		print "\t\t\treturn $varName;\n";
		print "\t\t}\n";
  }
  
	print "\t\tthrow new NoSuchInterfaceException(cItf);\n";
  print "\t}\n\n";

  print "\tpublic void bindFc(String cItf, Object sItf) throws NoSuchInterfaceException {\n";
  
  $firstItf=1;
  for my $itf (@clientItfs) {
    my $itfName=$itf->{"name"};
    my $itfSig=$itf->{"signature"};
    my $varName=varName($itfName);
    
		print "\t\t" . ($firstItf ? "" : "} else ") . "if (cItf.equals(\"$itfName\")) {\n";
		print "\t\t\t$varName = ($itfSig)sItf;\n";
    $firstItf=0;
  }
  
	print "\t\t} else {\n";
  print "\t\t\tthrow new NoSuchInterfaceException(cItf);\n";
	print "\t\t}\n";
	print "\t}\n\n";
		
	print "\tpublic void unbindFc(String cItf) throws NoSuchInterfaceException {\n";

  $firstItf=1;
  for my $itf (@clientItfs) {
    my $itfName=$itf->{"name"};
    my $varName=varName($itfName);
    
		print "\t\t" . ($firstItf ? "" : "} else ") . "if (cItf.equals(\"$itfName\")) {\n";
		print "\t\t\t$varName=null;\n";
    $firstItf=0;
  }
  
	print "\t\t} else {\n";
	print "\t\t\tthrow new NoSuchInterfaceException(cItf);\n";
	print "\t\t}\n";
	print "\t}\n";
}

print "\n\n";
print "\t// Business methods\n";
print "\t// ------------------------\n\n";

print "}\n";
