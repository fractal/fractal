/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.dsrg.bpc.demo.env;

import java.util.Date;

import java.math.BigDecimal;

import java.net.InetAddress;

import org.objectweb.dsrg.bpc.demo.*;


public class DemoEnvValues extends cz.cuni.mff.envgen.EnvValueSets
{
	public DemoEnvValues()
	{
		super();

		// CardCenter
		putStringSet("CardCenter", "", "", new String[]{"1111-2222-3333-4405"});
		putObjectSet("java.util.Date", "CardCenter", "", "", new Date[]{new Date(Simulator.CARD_EXPDATE_MS)});
		putObjectSet("java.math.BigDecimal", "CardCenter", "", "", new BigDecimal[]{new BigDecimal("500.00")});

		// AccountDatabase
		putStringSet("AccountDatabase", "", "", new String[]{"1001", "Password1"});
		putBooleanSet("AccountDatabase", "", "", new boolean[]{false, true});
		putLongSet("AccountDatabase", "", "", new long[]{1000*3600});
		putObjectSet("java.util.Date", "AccountDatabase", "", "", new Date[]{new Date(Simulator.CARD_EXPDATE_MS)});
		putObjectSet("org.objectweb.dsrg.bpc.demo.IToken", "AccountDatabase", "", "", new IToken[]{Simulator.createAccountToken()});

		// Firewall
		try { putObjectSet("java.net.InetAddress", "Firewall", "", "", new java.net.InetAddress[]{InetAddress.getByAddress(new byte[]{-64,-88,0,1})}); } catch (Exception ex) {}

		// Arbitrator
		putStringSet("Arbitrator", "", "", new String[]{"1001", "Password1"});
		try { putObjectSet("java.net.InetAddress", "Arbitrator", "", "", new java.net.InetAddress[]{InetAddress.getByAddress(new byte[]{-64,-88,0,1})}); } catch (Exception ex) {}
		IToken arbToken1 = Simulator.createAccountToken();
		arbToken1.SetValidity(new Date(System.currentTimeMillis()+30*60*1000));
		putObjectSet("org.objectweb.dsrg.bpc.demo.IToken", "Arbitrator", "", "", new IToken[]{arbToken1});

		// FlyTicketClassifier
		putStringSet("FlyTicketClassifier", "", "", new String[]{"AFR1001B", "CSA1002E"});
		putObjectSet("java.lang.String[]", "FlyTicketClassifier", "", "", new Object[]{new String[]{"CSA1001B"}, new String[]{"AFR1003E"}});
		putObjectSet("java.util.Date", "FlyTicketClassifier", "", "", new Date[]{new Date(System.currentTimeMillis() + 120*1000)});
		putBooleanSet("FlyTicketClassifier", "", "", new boolean[]{false, true});

		// AfDbConnection
		putStringSet("AfDbConnection", "", "", new String[]{"SKY1001", "AFR1002E"});

		// CsaDbConnection
		putStringSet("CsaDbConnection", "", "", new String[]{"SKY1002", "CSA1001B"});

		// FrequentFlyerDatabase
		putStringSet("FrequentFlyerDatabase", "", "", new String[]{"SKY1003"});
		putObjectSet("java.lang.String[]", "FrequentFlyerDatabase", "", "", new Object[]{new String[]{"CSA1003B"}, new String[]{"AFR1004E"}});
		putObjectSet("java.util.Date", "FrequentFlyerDatabase", "", "", new Date[]{new Date(System.currentTimeMillis() - 180*1000), new Date(System.currentTimeMillis() + 180*1000)});

		// Timer (in DhcpServer)
		putObjectSet("java.util.Date", "Timer", "", "", new Date[]{new Date(System.currentTimeMillis() + 5*1000)});
	
		// DhcpListener
		putBooleanSet("DhcpListener", "", "", new boolean[]{false, true});
		try { putObjectSet("java.net.InetAddress", "DhcpListener", "", "", new java.net.InetAddress[]{InetAddress.getByAddress(new byte[]{-64,-88,0,1})}); } catch (Exception ex) {}

		// TransientIpDb
		putObjectSet("byte[]", "TransientIpDb", "", "", new Object[]{new byte[]{0, 0, 0, 0, 0, 1}});
		try { putObjectSet("java.net.InetAddress", "TransientIpDb", "", "", new java.net.InetAddress[]{InetAddress.getByAddress(new byte[]{-64,-88,0,1})}); } catch (Exception ex) {}
		putObjectSet("java.util.Date", "TransientIpDb", "", "", new Date[]{new Date(System.currentTimeMillis() + 60*1000)});

		// IpAddressManager
		putObjectSet("java.util.Date", "IpAddressManager", "", "", new Date[]{new Date(System.currentTimeMillis() + 60*1000)});
		putObjectSet("byte[]", "IpAddressManager", "", "", new Object[]{new byte[]{0, 0, 0, 0, 0, 1}});
		try { putObjectSet("java.net.InetAddress", "IpAddressManager", "", "", new java.net.InetAddress[]{null, InetAddress.getByAddress(new byte[]{-64,-88,0,1})}); } catch (Exception ex) {}

		// ValidityChecker
		putObjectSet("java.util.Date", "ValidityChecker", "", "", new Date[]{new Date(System.currentTimeMillis() + 30*1000)});
		putStringSet("ValidityChecker", "", "", new String[]{"1002", "++SecretCookie++"});
		IToken vcToken1 = Simulator.createToken();
		vcToken1.SetValidity(new Date(System.currentTimeMillis()+30*60*1000));
		putObjectSet("java.lang.Object", "ValidityChecker", "", "", new Object[]{vcToken1});

		// Timer (in Token)
		putObjectSet("java.util.Date", "Timer", "", "", new Date[]{new Date(System.currentTimeMillis() + 10*1000)});

		// CustomToken
		putStringSet("CustomToken", "", "", new String[]{"1001", "Password1"});
		putLongSet("CustomToken", "", "", new long[]{1000*3600});
	}
}
