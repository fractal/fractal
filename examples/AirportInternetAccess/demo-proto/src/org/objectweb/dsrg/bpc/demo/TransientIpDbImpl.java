/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.dsrg.bpc.demo;

import java.net.InetAddress;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TransientIpDbImpl implements IIpMacDb {

	protected Map ipMac = new HashMap();
	protected Map macIp = new HashMap();
	protected Map expTimes = new HashMap();
    
	public TransientIpDbImpl() {
	}


	//
	// Business methods
	//
	
	//
	// IIpMacDb interface
	//
	
	public void Add(byte[] MacAddress, InetAddress IpAddress, Date ExpirationTime) {
		ipMac.put(IpAddress, MacAddress);
		macIp.put(MacAddress, IpAddress);
		expTimes.put(IpAddress, ExpirationTime);
	}

	public Date GetExpirationTime(InetAddress IpAddress) {
		Date expTime = (Date) expTimes.get(IpAddress);
      
		return expTime;
	}

	public InetAddress GetIpAddress(byte[] MacAddress) {
		InetAddress ip = (InetAddress) macIp.get(MacAddress);
		
		return ip;
	}
    
	public byte[] GetMacAddress(InetAddress IpAddress) {
		byte[] mac = (byte[]) ipMac.get(IpAddress);
        
        	return mac;
	}
    
	public void Remove(InetAddress IpAddress) {
		byte[] mac = (byte[]) ipMac.get(IpAddress);
	
		if (mac != null) {
			ipMac.remove(IpAddress);
			macIp.remove(mac);
			expTimes.remove(IpAddress);
		}
	}

	public void SetExpirationTime(InetAddress IpAddress, Date ExpirationTime) {
		if (expTimes.containsKey(IpAddress)) {
			expTimes.put(IpAddress, ExpirationTime);
		}
	}
}
