/*
 * $Id: SimulatorJpf.source 447 2006-04-11 09:58:05Z kofron $
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.dsrg.bpc.demo;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Date;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.util.Fractal;

import org.objectweb.fractal.bpc.ProtocolController;

import org.objectweb.dsrg.bpc.demo.env.TokenStubImpl;
import org.objectweb.dsrg.bpc.demo.env.TokenAccountStubImpl;
import org.objectweb.dsrg.bpc.demo.env.AccountDatabaseStubImpl;
import org.objectweb.dsrg.bpc.demo.env.ArbitratorStubImpl;


public class Simulator implements BindingController, Runnable, IArbitratorCallback {

	protected ILife iArbitratorLifetimeController;
	protected ILogin iLogin;
	protected IAccount iAccount;

	//public static Component demo;
	//public static Component arbitrator;
	public static DhcpListenerImpl dhcpListener;
	//public static Component accountDatabase;
 

	public static final long CARD_EXPDATE_MS = System.currentTimeMillis() + 30*24*3600*1000;


	public Simulator() {
	}


	//
	// Binding controller methods
	//
	
	public String[] listFc() {
		return new String[] { "IArbitratorLifetimeController", "ILogin" };
	}

	public Object lookupFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("IArbitratorLifetimeController")) {
			return iArbitratorLifetimeController;
		}
		if (cItf.equals("ILogin")) {
			return iLogin;
		}
		if (cItf.equals("IAccount")) {
			return iAccount;
		}
		throw new NoSuchInterfaceException(cItf);
	}

	public void bindFc(String cItf, Object sItf) throws NoSuchInterfaceException {
		if (cItf.equals("IArbitratorLifetimeController")) {
			iArbitratorLifetimeController = (ILife)sItf;
		} else if (cItf.equals("ILogin")) {
			iLogin = (ILogin)sItf;
			
			//arbitrator = ((Interface)iLogin).getFcItfOwner();
			//demo = Fractal.getSuperController(arbitrator).getFcSuperComponents()[0];
		} else if (cItf.equals("IAccount")) {
			iAccount = (IAccount)sItf;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}

	public void unbindFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("IArbitratorLifetimeController")) {
			iArbitratorLifetimeController=null;
		} else if (cItf.equals("ILogin")) {
			iLogin=null;
		} else if (cItf.equals("IAccount")) {
			iAccount=null;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}


	//
	// Business methods
	//
	
	public static IToken createToken() {
		try {
			TokenStubImpl token = new TokenStubImpl();
			
			TimerImpl timer = new TimerImpl();
			token.bindFc("ITimer", (ITimer) timer);
			timer.bindFc("ITimerCallback", (ITimerCallback) token);
            
			token.bindFc("ITokenCallback", new ArbitratorStubImpl());
			
			return token;

		} catch (NoSuchInterfaceException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
        
		return null;
	}

	public static IToken createAccountToken() {
		try {
			TokenAccountStubImpl token = new TokenAccountStubImpl();
			
			TimerImpl timer = new TimerImpl();
			token.bindFc("ITimer", (ITimer) timer);
			timer.bindFc("ITimerCallback", (ITimerCallback) token);
            
			CustomTokenImpl customToken = new CustomTokenImpl();
			token.bindFc("ICustomCallback", (ICustomCallback) customToken);
		
			customToken.bindFc("IAccount", new AccountDatabaseStubImpl());
			
			token.bindFc("ITokenCallback", new ArbitratorStubImpl());
		
			return token;

		} catch (NoSuchInterfaceException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
        
		return null;
	}

    
	public static void startToken(IToken token) {
		try {
			((ILife) token).Start();
		} catch (ClassCastException e) {
			e.printStackTrace();
		}        
	}
    
	public void run()
	{
		// empty
	}
	

	//
	// IArbitratorCallback
	//
	public void Notify() {
		// nothing to be done...
	}

}
