/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>,
 *			Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.dsrg.bpc.demo;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.Interface;

public class ArbitratorImpl implements BindingController, ILife, ILogin, IDhcpCallback, ITokenCallback {

	protected IFirewall iFirewall;
	protected IAccountAuth iAccountAuth;
	protected IFlyTicketAuth iFlyTicketAuth;
	protected IFreqFlyerAuth iFreqFlyerAuth;
	protected ILife iDhcpServerLifetimeController;
	protected IArbitratorCallback iArbitratorCallback;

	protected IToken iToken;
	protected ILife iTokenLifetimeController;
	
	private Map tokenAddressMap=new HashMap();
	private Map addressTokenMap=new HashMap();

	public ArbitratorImpl() {
	}

	//
	// Binding controller methods
	//
	
	public String[] listFc() {
		return new String[] { "IFirewall", "IAccountAuth", "IFlyTicketAuth", "IFreqFlyerAuth", "IToken", "ITokenLifetimeController", "IDhcpServerLifetimeController", "IArbitratorCallback" };
	}

	public Object lookupFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("IFirewall")) {
			return iFirewall;
		}
		if (cItf.equals("IAccountAuth")) {
			return iAccountAuth;
		}
		if (cItf.equals("IFlyTicketAuth")) {
			return iFlyTicketAuth;
		}
		if (cItf.equals("IFreqFlyerAuth")) {
			return iFreqFlyerAuth;
		}
		if (cItf.equals("IToken")) {
		    return iToken;
		}
		if (cItf.equals("ITokenLifetimeController")) {
		    return iTokenLifetimeController;
		}
		if (cItf.equals("IDhcpServerLifetimeController")) {
		    return iDhcpServerLifetimeController;
		}
		if (cItf.equals("IArbitratorCallback")) {
		    return iArbitratorCallback;
		}
		throw new NoSuchInterfaceException(cItf);
	}

	public void bindFc(String cItf, Object sItf) throws NoSuchInterfaceException {
		if (cItf.equals("IFirewall")) {
			iFirewall = (IFirewall)sItf;
		} else if (cItf.equals("IAccountAuth")) {
			iAccountAuth = (IAccountAuth)sItf;
      
	  		if (iAccountAuth instanceof Interface)
			{
				// the following class cast does not work for code checking because stub implementation of the IAccountAuth interface is employed
				Simulator.accountDatabase = ((Interface)iAccountAuth).getFcItfOwner();
			}
		} else if (cItf.equals("IFlyTicketAuth")) {
			iFlyTicketAuth = (IFlyTicketAuth)sItf;
		} else if (cItf.equals("IFreqFlyerAuth")) {
			iFreqFlyerAuth = (IFreqFlyerAuth)sItf;
		} else if (cItf.equals("IToken")) {
		    iToken = (IToken)sItf;
		} else if (cItf.equals("ITokenLifetimeController")) {
		    iTokenLifetimeController = (ILife)sItf;
		} else if (cItf.equals("IDhcpServerLifetimeController")) {
		    iDhcpServerLifetimeController = (ILife)sItf;
		} else if (cItf.equals("IArbitratorCallback")) {
		    iArbitratorCallback = (IArbitratorCallback)sItf;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}

	public void unbindFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("IFirewall")) {
			iFirewall = null;
		} else if (cItf.equals("IAccountAuth")) {
			iAccountAuth = null;
		} else if (cItf.equals("IFlyTicketAuth")) {
			iFlyTicketAuth = null;
		} else if (cItf.equals("IFreqFlyerAuth")) {
			iFreqFlyerAuth = null;
		} else if (cItf.equals("IToken")) {
		    iToken = null;
		} else if (cItf.equals("ITokenLifetimeController")) {
		    iTokenLifetimeController = null;
		} else if (cItf.equals("IDhcpServerLifetimeController")) {
		    iDhcpServerLifetimeController = null;
		} else if (cItf.equals("IArbitratorCallback")) {
		    iArbitratorCallback = null;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}

	//
	// Business methods
	//

	//
	// ILifeTimeController interface
	//
	
	public void Start() {
	        iDhcpServerLifetimeController.Start();
	}
	
	//
	// ILogin interface
	//

	public boolean LoginWithAccountId(InetAddress IpAddress, String AccountId, String Password) {
		IToken token = iAccountAuth.CreateToken(AccountId, Password);
		if (token == null) {
			return false;
		}
		
		return registerToken(IpAddress, token);
	}

	public boolean LoginWithFlyTicketId(InetAddress IpAddress, String FlyTicketId) {
		IToken token = iFlyTicketAuth.CreateToken(FlyTicketId, true);
		if (token == null) {
			return false;
		}

		return registerToken(IpAddress, token);
	}
    
	public boolean LoginWithFrequentFlyerId(InetAddress IpAddress, String FrequentFlyerId) {
		IToken token = iFreqFlyerAuth.CreateToken(FrequentFlyerId);
		if (token == null) {
			return false;
		}

		return registerToken(IpAddress, token);
	}
    
	public boolean Logout(InetAddress IpAddress) {
		System.out.println("----- Arbitrator.Logout: " + IpAddress + " -----");

		IToken token = getTokenFromIpAddress(IpAddress);

		if (token == null) {
			return false;
		}
		
		token.InvalidateAndSave();
		return true;
	}
    
	public String GetTokenIdFromIpAddress(InetAddress IpAddress) {
		IToken token = getTokenFromIpAddress(IpAddress);
	
		if (token == null) {
			return null;
		} else {
			return token.GetUniqueId();
		}
	}

	//
	// IDhcpCallback interface
	//

	public void IpAddressInvalidated(InetAddress IpAddress) {
		System.out.println("----- Arbitrator.IpAddressInvalidated: " + IpAddress + " -----");
	
		IToken token = getTokenFromIpAddress(IpAddress);
	
		if (token != null) {
			token.InvalidateAndSave();
		}
	}
	
	//
	// ITokenCallback interface
	//

	public void TokenInvalidated(Object TokenEvidence) {
		InetAddress addr = unregisterToken((IToken) TokenEvidence);
        
		iFirewall.EnablePortBlock(addr);
		
		System.out.println("===== Arbitrator.TokenInvalidated: " + addr + " =====");
	}
	
	
	public void Notify() {
		iArbitratorCallback.Notify();
	}
    
	//
	// Private methods
	//
	
	private boolean registerToken(InetAddress addr, IToken token) {
		System.out.println("----- Arbitrator: registered token for " + addr + " -----");
	
		token.SetEvidence(token);
		
		Simulator.startToken(token);
	
		addressTokenMap.put(addr, token);
		tokenAddressMap.put(token, addr);
	
		iFirewall.DisablePortBlock(addr);
		
		return true;
	}
    
	private InetAddress unregisterToken(IToken token) {
		InetAddress addr = (InetAddress) tokenAddressMap.remove(token);
	
		System.out.println("----- Arbitrator: unregistered token for " + addr + " -----");

		addressTokenMap.remove(addr);
        
		return addr;
	}
    
	private IToken getTokenFromIpAddress(InetAddress addr) {
		System.out.println("----- Arbitrator: retrieved token for " + addr + " -----");
		return (IToken) addressTokenMap.get(addr);
	}
}
