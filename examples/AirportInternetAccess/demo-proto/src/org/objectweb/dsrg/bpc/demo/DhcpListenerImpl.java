/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.dsrg.bpc.demo;

import java.net.InetAddress;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;

public class DhcpListenerImpl implements BindingController, IDhcpListenerCallback, ILife {

	protected IDhcpListenerCallback iDhcpListenerCallback;

	public DhcpListenerImpl() {
	    Simulator.dhcpListener=this;
	}

	//
	// Binding controller methods
	//
	
	public String[] listFc() {
		return new String[] { "IDhcpListenerCallback" };
	}

	public Object lookupFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("IDhcpListenerCallback")) {
			return iDhcpListenerCallback;
		}
		throw new NoSuchInterfaceException(cItf);
	}

	public void bindFc(String cItf, Object sItf) throws NoSuchInterfaceException {
		if (cItf.equals("IDhcpListenerCallback")) {
			iDhcpListenerCallback = (IDhcpListenerCallback)sItf;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}

	public void unbindFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("IDhcpListenerCallback")) {
			iDhcpListenerCallback=null;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}

	//	
	// Business methods
	//
	
	//
	// ILife interface
	//

	public void Start() {
	}
	
	//
	// IDhcpListenerCallback interface (needed only for the Simulator class)
	//
	
	public boolean ReleaseIpAddress(byte[] MacAddress, InetAddress IpAddress) {
		return iDhcpListenerCallback.ReleaseIpAddress(MacAddress, IpAddress);
	}

	public boolean RenewIpAddress(byte[] MacAddress, InetAddress IpAddress) {
		return iDhcpListenerCallback.RenewIpAddress(MacAddress, IpAddress);
	}

	public InetAddress RequestNewIpAddress(byte[] MacAddress) {
		return iDhcpListenerCallback.RequestNewIpAddress(MacAddress);
	}
}
