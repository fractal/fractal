<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE definition PUBLIC "-//objectweb.org//DTD Fractal ADL 2.0//EN" "classpath://org/objectweb/fractal/bpc/adl/basic-proto.dtd">

<!--
 * $Id: TokenAccount.fractal 479 2006-10-05 11:15:10Z kofron $
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>
 *
-->

<definition name="org.objectweb.dsrg.bpc.demo.TokenAccount">

	<interface name="ITokenCallback" role="client" signature="org.objectweb.dsrg.bpc.demo.ITokenCallback"/>
	<interface name="IToken" role="server" signature="org.objectweb.dsrg.bpc.demo.IToken"/>
	<interface name="IAccount" role="client" signature="org.objectweb.dsrg.bpc.demo.IAccount"/>
	<interface name="ILifetimeController" role="server" signature="org.objectweb.dsrg.bpc.demo.ILife"/>
	
	<component name="ValidityChecker">
		<interface name="ITokenCallback" role="client" signature="org.objectweb.dsrg.bpc.demo.ITokenCallback"/>
		<interface name="IToken" role="server" signature="org.objectweb.dsrg.bpc.demo.IToken"/>
		<interface name="ITimerCallback" role="server" signature="org.objectweb.dsrg.bpc.demo.ITimerCallback"/>
		<interface name="ITimer" role="client" signature="org.objectweb.dsrg.bpc.demo.ITimer"/>
		<interface name="ICustomCallback" role="client" signature="org.objectweb.dsrg.bpc.demo.ICustomCallback" contingency="optional"/>
		<interface name="ILifetimeController" role="server" signature="org.objectweb.dsrg.bpc.demo.ILife"/>

		<content class="org.objectweb.dsrg.bpc.demo.ValidityCheckerImpl"/>
		<controller desc="primitive-rtcheck"/>
		<protocol file="runtime/validitychecker.bp"/>

		<environment>
			<valuesets classname="org.objectweb.dsrg.bpc.demo.env.DemoEnvValues"/>
			<!--<userstub file="stubs/SimulatorStubs.code"/>-->
			<protocol file="envbp/env-validitychecker.bp"/>
		</environment>
	</component>
	
	<component name="Timer">
		<interface name="ITimerCallback" role="client" signature="org.objectweb.dsrg.bpc.demo.ITimerCallback"/>
		<interface name="ITimer" role="server" signature="org.objectweb.dsrg.bpc.demo.ITimer"/>

		<content class="org.objectweb.dsrg.bpc.demo.TimerImpl"/>
		<controller desc="primitive-rtcheck"/>
		<protocol file="runtime/timer.bp"/>

		<environment>
			<valuesets classname="org.objectweb.dsrg.bpc.demo.env.DemoEnvValues"/>
			<!--<userstub file="stubs/SimulatorStubs.code"/>-->
			<protocol file="envbp/env-timer.bp"/>
		</environment>
	</component>	
		
	<component name="CustomToken">
		<interface name="ICustomCallback" role="server" signature="org.objectweb.dsrg.bpc.demo.ICustomCallback"/>
		<interface name="IAccount" role="client" signature="org.objectweb.dsrg.bpc.demo.IAccount"/>

		<content class="org.objectweb.dsrg.bpc.demo.CustomTokenImpl"/>

		<protocol file="runtime/customtoken.bp"/>

		<environment>
			<valuesets classname="org.objectweb.dsrg.bpc.demo.env.DemoEnvValues"/>
			<!--<userstub file="stubs/SimulatorStubs.code"/>-->
			<protocol file="envbp/env-customtoken.bp"/>
		</environment>
		
	</component>

	<binding client="Timer.ITimerCallback" server="ValidityChecker.ITimerCallback"/>
	<binding client="ValidityChecker.ITimer" server="Timer.ITimer"/>
	<binding client="ValidityChecker.ICustomCallback" server="CustomToken.ICustomCallback"/>
	
	<binding client="CustomToken.IAccount" server="this.IAccount"/>
	<binding client="this.IToken" server="ValidityChecker.IToken"/>
	<binding client="ValidityChecker.ITokenCallback" server="this.ITokenCallback"/>
	<binding client="this.ILifetimeController" server="ValidityChecker.ILifetimeController"/>

	<controller desc="composite-rtcheck"/>
	<protocol file="runtime/token.bp"/>

</definition>
