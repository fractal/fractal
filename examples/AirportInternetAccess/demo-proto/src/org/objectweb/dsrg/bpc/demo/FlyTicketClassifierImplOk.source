/*
 * $Id: FlyTicketClassifierImplOk.source 431 2006-02-28 14:57:23Z mencl $
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.dsrg.bpc.demo;
import java.util.Arrays;
import java.util.Date;
import java.util.Vector;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;

public class FlyTicketClassifierImpl implements BindingController, IFlyTicketDb, IFlyTicketAuth {

	public static final String AF_TICKET_PREFIX = "AFR";
	public static final String CSA_TICKET_PREFIX = "CSA";
	private static final long TIME_UNITS_AFTER_ARRIVAL = 30 * 60;

	protected IFlyTicketDb iAfFlyTicketDb;
	protected IFlyTicketDb iCsaFlyTicketDb;

	public FlyTicketClassifierImpl() {
	}

	//
	// Binding controller methods
	//
	
	public String[] listFc() {
		return new String[] { "IAfFlyTicketDb", "ICsaFlyTicketDb" };
	}

	public Object lookupFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("IAfFlyTicketDb")) {
			return iAfFlyTicketDb;
		}
		if (cItf.equals("ICsaFlyTicketDb")) {
			return iCsaFlyTicketDb;
		}
		throw new NoSuchInterfaceException(cItf);
	}

	public void bindFc(String cItf, Object sItf) throws NoSuchInterfaceException {
		if (cItf.equals("IAfFlyTicketDb")) {
			iAfFlyTicketDb = (IFlyTicketDb)sItf;
		} else if (cItf.equals("ICsaFlyTicketDb")) {
			iCsaFlyTicketDb = (IFlyTicketDb)sItf;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}

	public void unbindFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("IAfFlyTicketDb")) {
			iAfFlyTicketDb=null;
		} else if (cItf.equals("ICsaFlyTicketDb")) {
			iCsaFlyTicketDb=null;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}

	//
	// Business methods
	//
	
	//
	// IFlyTicketDb
	//
	
	public String[] GetFlyTicketsByFrequentFlyerId(String FrequentFlyerId) {
		if (FrequentFlyerId == null) return null;
	
		Vector result = new Vector();
        
		// JPF can't handle the Vector.addAll method
		String[] array1 = iAfFlyTicketDb.GetFlyTicketsByFrequentFlyerId(FrequentFlyerId);
		for (int i = 0; i < array1.length; i++) result.add(array1[i]);
	
		String[] array2 = iCsaFlyTicketDb.GetFlyTicketsByFrequentFlyerId(FrequentFlyerId);
		for (int i = 0; i < array2.length; i++) result.add(array2[i]);
    
		return (String[]) result.toArray(new String[result.size()]);
	}
    
	public Date GetFlyTicketValidity(String FlyTicketId) {
		IFlyTicketDb db = getTicketDbByTicketId(FlyTicketId);
		if (db == null) return null;
		
		return db.GetFlyTicketValidity(FlyTicketId);
	}

	public boolean IsEconomyFlyTicket(String FlyTicketId) {
		IFlyTicketDb db = getTicketDbByTicketId(FlyTicketId);
		if (db == null) return true;
		
		return db.IsEconomyFlyTicket(FlyTicketId);
	}
	
	//
	// IFlyTicketAuth
	//
    
	public IToken CreateToken(String FlyTicketId, boolean RestrictValidity) {
		System.out.println("..... FlyTicketClassifier.CreateToken(" + FlyTicketId + ") .....");

		if (FlyTicketId == null) return null;
		
		IFlyTicketDb db = getTicketDbByTicketId(FlyTicketId);
		if (db == null) return null;
		
		Date ArrivalTime = null;
		if (RestrictValidity) {
			ArrivalTime = db.GetFlyTicketValidity(FlyTicketId);
			if (ArrivalTime == null || ArrivalTime.before(new Date())) return null;
		
			if (db.IsEconomyFlyTicket(FlyTicketId)) return null;
		}
		
		IToken token = Simulator.createToken();
		
		if (RestrictValidity) {
			token.SetValidity(new Date(ArrivalTime.getTime() + TIME_UNITS_AFTER_ARRIVAL * 1000));
		}
		
		return token;
	}
	
	//
	// Private methods
	//
	
	protected IFlyTicketDb getTicketDbByTicketId(String FlyTicketId) {
		if (FlyTicketId == null) return null;
		
		if (FlyTicketId.startsWith(AF_TICKET_PREFIX)) {
			return iAfFlyTicketDb;
		} else if (FlyTicketId.startsWith(CSA_TICKET_PREFIX)) {
			return iCsaFlyTicketDb;
		} else {
			return null;
		}
	}
}
