<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE definition PUBLIC "-//objectweb.org//DTD Fractal ADL 2.0//EN" "classpath://org/objectweb/fractal/bpc/adl/basic-proto.dtd">

<!--
 * $Id: StaticDemo.fractal 447 2006-04-11 09:58:05Z kofron $
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>
 *			Jan Kofron <kofron@nenya.ms.mff.cuni.cz>
 *
-->

<definition name="org.objectweb.dsrg.bpc.demo.StaticDemo">
	<interface name="ILifetimeController" role="server" signature="org.objectweb.dsrg.bpc.demo.ILife"/>
	<interface name="ILogin" role="server" signature="org.objectweb.dsrg.bpc.demo.ILogin"/>
	<interface name="IAccount" role="server" signature="org.objectweb.dsrg.bpc.demo.IAccount"/>
	<interface name="IArbitratorCallback" role="client" signature="org.objectweb.dsrg.bpc.demo.IArbitratorCallback"/>


	<component name="DhcpServer">
		<interface name="IManagement" role="server" signature="org.objectweb.dsrg.bpc.demo.IManagement"/>
		<interface name="IIpMacPermanentDb" role="client" signature="org.objectweb.dsrg.bpc.demo.IIpMacDb" contingency="optional"/>
		<interface name="IDhcpCallback" role="client" signature="org.objectweb.dsrg.bpc.demo.IDhcpCallback"/>
		<interface name="IDhcpServerLifetimeController" role="server" signature="org.objectweb.dsrg.bpc.demo.ILife"/>
		
		<component name="Timer">
			<interface name="ITimerCallback" role="client" signature="org.objectweb.dsrg.bpc.demo.ITimerCallback"/>
			<interface name="ITimer" role="server" signature="org.objectweb.dsrg.bpc.demo.ITimer"/>
	
			<content class="org.objectweb.dsrg.bpc.demo.TimerImpl"/>
			<protocol file="timer.bp"/>
		</component>
		
		<component name="DhcpListener">
			<interface name="IDhcpListenerCallback" role="client" signature="org.objectweb.dsrg.bpc.demo.IDhcpListenerCallback"/>
			<interface name="IListenerLifetimeController" role="server" signature="org.objectweb.dsrg.bpc.demo.ILife"/>
	
			<content class="org.objectweb.dsrg.bpc.demo.DhcpListenerImpl"/>
			<protocol file="dhcplistener.bp"/>
		</component>

		<component name="TransientIpDb">
			<interface name="IIpMacDb" role="server" signature="org.objectweb.dsrg.bpc.demo.IIpMacDb"/>
	
			<content class="org.objectweb.dsrg.bpc.demo.TransientIpDbImpl"/>
			<protocol file="transientdb.bp"/>
		</component>

		<component name="IpAddressManager">
			<interface name="ITimerCallback" role="server" signature="org.objectweb.dsrg.bpc.demo.ITimerCallback"/>
			<interface name="ITimer" role="client" signature="org.objectweb.dsrg.bpc.demo.ITimer"/>
			<interface name="IDhcpListenerCallback" role="server" signature="org.objectweb.dsrg.bpc.demo.IDhcpListenerCallback"/>
			<interface name="IManagement" role="server" signature="org.objectweb.dsrg.bpc.demo.IManagement"/>
			<interface name="IIpMacTransientDb" role="client" signature="org.objectweb.dsrg.bpc.demo.IIpMacDb"/>
			<interface name="IDhcpCallback" role="client" signature="org.objectweb.dsrg.bpc.demo.IDhcpCallback"/>
			<interface name="IIpMacPermanentDb" role="client" signature="org.objectweb.dsrg.bpc.demo.IIpMacDb" contingency="optional"/>
			<interface name="IDhcpServerLifetimeController" role="server" signature="org.objectweb.dsrg.bpc.demo.ILife"/>
			<interface name="IListenerLifetimeController" role="client" signature="org.objectweb.dsrg.bpc.demo.ILife"/>
	
			<content class="org.objectweb.dsrg.bpc.demo.IpAddressManagerImpl"/>
			<protocol file="ipaddressmanager.bp"/>
		</component>

		<binding client="Timer.ITimerCallback" server="IpAddressManager.ITimerCallback"/>
		<binding client="IpAddressManager.ITimer" server="Timer.ITimer"/>
		<binding client="DhcpListener.IDhcpListenerCallback" server="IpAddressManager.IDhcpListenerCallback"/>
		<binding client="IpAddressManager.IIpMacTransientDb" server="TransientIpDb.IIpMacDb"/>
		<binding client="IpAddressManager.IListenerLifetimeController" server="DhcpListener.IListenerLifetimeController"/>
		
		<binding client="IpAddressManager.IIpMacPermanentDb" server="this.IIpMacPermanentDb"/>
		<binding client="IpAddressManager.IDhcpCallback" server="this.IDhcpCallback"/>
		<binding client="this.IManagement" server="IpAddressManager.IManagement"/>
		<binding client="this.IDhcpServerLifetimeController" server="IpAddressManager.IDhcpServerLifetimeController"/>

		<protocol file="dhcpserver.bp"/>
	</component>
	
	<component name="Arbitrator_Others">
		<interface name="IDhcpCallback" role="server" signature="org.objectweb.dsrg.bpc.demo.IDhcpCallback"/>
		<interface name="IDhcpServerLifetimeController" role="client" signature="org.objectweb.dsrg.bpc.demo.ILife"/>
		<interface name="ILogin" role="server" signature="org.objectweb.dsrg.bpc.demo.ILogin"/>
		<interface name="IAccount" role="server" signature="org.objectweb.dsrg.bpc.demo.IAccount"/>
		<interface name="IArbitratorLifetimeController" role="server" signature="org.objectweb.dsrg.bpc.demo.ILife"/>
		<interface name="IArbitratorCallback" role="client" signature="org.objectweb.dsrg.bpc.demo.IArbitratorCallback"/>
		<interface name="ITokenLifetimeController" role="client" signature="org.objectweb.dsrg.bpc.demo.ILife"/>
		
		<component name="Firewall">
			<interface name="IFirewall" role="server" signature="org.objectweb.dsrg.bpc.demo.IFirewall"/>
			
			<content class="org.objectweb.dsrg.bpc.demo.FirewallImpl"/>
			<protocol file="firewall.bp"/>
		</component>
		
		<component name="Accountdb_Cardcenter_Token">
			<interface name="IAccount" role="server" signature="org.objectweb.dsrg.bpc.demo.IAccount"/>
			<interface name="IAccountAuth" role="server" signature="org.objectweb.dsrg.bpc.demo.IAccountAuth"/>
			<interface name="IToken" role="server" signature="org.objectweb.dsrg.bpc.demo.IToken"/>
			<interface name="ITokenCallback" role="client" signature="org.objectweb.dsrg.bpc.demo.ITokenCallback"/>
			<interface name="ITokenLifetimeController" role="server" signature="org.objectweb.dsrg.bpc.demo.ILife"/>
			
			<component name="CardCenter">
				<interface name="ICardCenter" role="server" signature="org.objectweb.dsrg.bpc.demo.ICardCenter"/>
		
				<content class="org.objectweb.dsrg.bpc.demo.CardCenterImpl"/>
				<protocol file="cardcenter.bp"/>
			</component>
			
			<component name="AccountDatabase">
				<interface name="ICardCenter" role="client" signature="org.objectweb.dsrg.bpc.demo.ICardCenter"/>
				<interface name="IAccountAuth" role="server" signature="org.objectweb.dsrg.bpc.demo.IAccountAuth"/>
				<interface name="IAccount" role="server" signature="org.objectweb.dsrg.bpc.demo.IAccount"/>
				
				<content class="org.objectweb.dsrg.bpc.demo.AccountDatabaseImpl"/>
				<protocol file="accountdatabase.bp"/>
			</component>
		
			<component name="Token">
				<interface name="ITokenCallback" role="client" signature="org.objectweb.dsrg.bpc.demo.ITokenCallback"/>
				<interface name="IToken" role="server" signature="org.objectweb.dsrg.bpc.demo.IToken"/>
				<interface name="IAccount" role="client" signature="org.objectweb.dsrg.bpc.demo.IAccount"/>
				<interface name="ITokenLifetimeController" role="server" signature="org.objectweb.dsrg.bpc.demo.ILife"/>
				
				<component name="Timer">
					<interface name="ITimerCallback" role="client" signature="org.objectweb.dsrg.bpc.demo.ITimerCallback"/>
					<interface name="ITimer" role="server" signature="org.objectweb.dsrg.bpc.demo.ITimer"/>
			
					<content class="org.objectweb.dsrg.bpc.demo.TimerImpl"/>
					<protocol file="timer.bp"/>
				</component>
				
				<component name="CustomToken">
					<interface name="ICustomCallback" role="server" signature="org.objectweb.dsrg.bpc.demo.ICustomCallback"/>
					<interface name="IAccount" role="client" signature="org.objectweb.dsrg.bpc.demo.IAccount"/>
			
					<content class="org.objectweb.dsrg.bpc.demo.CustomTokenImpl"/>
					<protocol file="customtoken.bp"/>
				</component>
		
				<component name="ValidityChecker">
					<interface name="ITokenCallback" role="client" signature="org.objectweb.dsrg.bpc.demo.ITokenCallback"/>
					<interface name="IToken" role="server" signature="org.objectweb.dsrg.bpc.demo.IToken"/>
					<interface name="ITimerCallback" role="server" signature="org.objectweb.dsrg.bpc.demo.ITimerCallback"/>
					<interface name="ITimer" role="client" signature="org.objectweb.dsrg.bpc.demo.ITimer"/>
					<interface name="ICustomCallback" role="client" signature="org.objectweb.dsrg.bpc.demo.ICustomCallback" contingency="optional"/>
					<interface name="ILifetimeController" role="server" signature="org.objectweb.dsrg.bpc.demo.ILife"/>
			
					<content class="org.objectweb.dsrg.bpc.demo.ValidityCheckerImpl"/>
					<protocol file="validitychecker.bp"/>
				</component>
				
				<binding client="Timer.ITimerCallback" server="ValidityChecker.ITimerCallback"/>
				<binding client="ValidityChecker.ITimer" server="Timer.ITimer"/>
				<binding client="ValidityChecker.ICustomCallback" server="CustomToken.ICustomCallback"/>
				
				<binding client="CustomToken.IAccount" server="this.IAccount"/>
				<binding client="this.IToken" server="ValidityChecker.IToken"/>
				<binding client="ValidityChecker.ITokenCallback" server="this.ITokenCallback"/>
				<binding client="this.ITokenLifetimeController" server="ValidityChecker.ILifetimeController"/>
		
				<protocol file="token.bp"/>
			</component>
			
	
			<binding client="AccountDatabase.ICardCenter" server="CardCenter.ICardCenter"/>
			<binding client="Token.IAccount" server="AccountDatabase.IAccount"/>
			
			<binding client="this.IAccount" server="AccountDatabase.IAccount"/>
			<binding client="this.IAccountAuth" server="AccountDatabase.IAccountAuth"/>
			<binding client="this.IToken" server="Token.IToken"/>
			<binding client="Token.ITokenCallback" server="this.ITokenCallback"/>
			<binding client="this.ITokenLifetimeController" server="Token.ITokenLifetimeController"/>
			
			
			<protocol file="accountdb-cardcenter-token.bp"/>
		</component>
		
		<component name="Flyticketdb_Frequentflyerdb">
			<interface name="IFreqFlyerAuth" role="server" signature="org.objectweb.dsrg.bpc.demo.IFreqFlyerAuth"/>
			<interface name="IFlyTicketAuth" role="server" signature="org.objectweb.dsrg.bpc.demo.IFlyTicketAuth"/>
		
			<component name="FrequentFlyerDatabase">	
				<interface name="IFlyTicketDb" role="client" signature="org.objectweb.dsrg.bpc.demo.IFlyTicketDb"/>
				<interface name="IFlyTicketAuth" role="client" signature="org.objectweb.dsrg.bpc.demo.IFlyTicketAuth"/>
				<interface name="IFreqFlyerAuth" role="server" signature="org.objectweb.dsrg.bpc.demo.IFreqFlyerAuth"/>
				
				<content class="org.objectweb.dsrg.bpc.demo.FrequentFlyerDatabaseImpl"/>
				<protocol file="frequentFlyerDatabase.bp"/>
			</component>
			
			<component name="FlyTicketDatabase">
				<interface name="IFlyTicketDb" role="server" signature="org.objectweb.dsrg.bpc.demo.IFlyTicketDb"/>
				<interface name="IFlyTicketAuth" role="server" signature="org.objectweb.dsrg.bpc.demo.IFlyTicketAuth"/>
				
				<component name="AfDbConnection">
					<interface name="IFlyTicketDb" role="server" signature="org.objectweb.dsrg.bpc.demo.IFlyTicketDb"/>
				
					<content class="org.objectweb.dsrg.bpc.demo.AfDbConnectionImpl"/>
					<protocol file="afdbconnection.bp"/>
				</component>
		
				<component name="CsaDbConnection">
					<interface name="IFlyTicketDb" role="server" signature="org.objectweb.dsrg.bpc.demo.IFlyTicketDb"/>
				
					<content class="org.objectweb.dsrg.bpc.demo.CsaDbConnectionImpl"/>
					<protocol file="csadbconnection.bp"/>
				</component>
		
				<component name="FlyTicketClassifier">
					<interface name="IFlyTicketDb" role="server" signature="org.objectweb.dsrg.bpc.demo.IFlyTicketDb"/>
					<interface name="IFlyTicketAuth" role="server" signature="org.objectweb.dsrg.bpc.demo.IFlyTicketAuth"/>
					<interface name="IAfFlyTicketDb" role="client" signature="org.objectweb.dsrg.bpc.demo.IFlyTicketDb"/>
					<interface name="ICsaFlyTicketDb" role="client" signature="org.objectweb.dsrg.bpc.demo.IFlyTicketDb"/>
				
					<content class="org.objectweb.dsrg.bpc.demo.FlyTicketClassifierImpl"/>
					<protocol file="flyticketclasifier.bp"/>
				</component>
		
				<binding client="FlyTicketClassifier.IAfFlyTicketDb" server="AfDbConnection.IFlyTicketDb"/>
				<binding client="FlyTicketClassifier.ICsaFlyTicketDb" server="CsaDbConnection.IFlyTicketDb"/>
		
				<binding client="this.IFlyTicketDb" server="FlyTicketClassifier.IFlyTicketDb"/>
				<binding client="this.IFlyTicketAuth" server="FlyTicketClassifier.IFlyTicketAuth"/>
		
				<protocol file="flyticketdatabase.bp"/>
			</component>
			
			
			<binding client="FrequentFlyerDatabase.IFlyTicketAuth" server="FlyTicketDatabase.IFlyTicketAuth"/>
			<binding client="FrequentFlyerDatabase.IFlyTicketDb" server="FlyTicketDatabase.IFlyTicketDb"/>
			
			
			<binding client="this.IFreqFlyerAuth" server="FrequentFlyerDatabase.IFreqFlyerAuth"/>
			<binding client="this.IFlyTicketAuth" server="FlyTicketDatabase.IFlyTicketAuth"/>
			
			<protocol file="flyticketdb-frequentflyerdb.bp"/>
		</component>
	
		<component name="Arbitrator">	
			<interface name="IArbitratorLifetimeController" role="server" signature="org.objectweb.dsrg.bpc.demo.ILife"/>
			<interface name="ITokenLifetimeController" role="client" signature="org.objectweb.dsrg.bpc.demo.ILife"/>
			<interface name="IDhcpServerLifetimeController" role="client" signature="org.objectweb.dsrg.bpc.demo.ILife"/>
			<interface name="ILogin" role="server" signature="org.objectweb.dsrg.bpc.demo.ILogin"/>
			<interface name="IFirewall" role="client" signature="org.objectweb.dsrg.bpc.demo.IFirewall"/>
			<interface name="IDhcpCallback" role="server" signature="org.objectweb.dsrg.bpc.demo.IDhcpCallback"/>
			<interface name="IAccountAuth" role="client" signature="org.objectweb.dsrg.bpc.demo.IAccountAuth"/>
			<interface name="IToken" role="client" signature="org.objectweb.dsrg.bpc.demo.IToken"/>
			<interface name="ITokenCallback" role="server" signature="org.objectweb.dsrg.bpc.demo.ITokenCallback"/>
			<interface name="IFlyTicketAuth" role="client" signature="org.objectweb.dsrg.bpc.demo.IFlyTicketAuth"/>
			<interface name="IFreqFlyerAuth" role="client" signature="org.objectweb.dsrg.bpc.demo.IFreqFlyerAuth"/>
			<interface name="IArbitratorCallback" role="client" signature="org.objectweb.dsrg.bpc.demo.IArbitratorCallback"/>
			
			<content class="org.objectweb.dsrg.bpc.demo.ArbitratorImpl"/>
			<protocol file="arbitrator.bp"/>
		</component>


		<binding client="Arbitrator.IFirewall" server="Firewall.IFirewall"/>
		<binding client="Arbitrator.IAccountAuth" server="Accountdb_Cardcenter_Token.IAccountAuth"/>
		<binding client="Arbitrator.IFreqFlyerAuth" server="Flyticketdb_Frequentflyerdb.IFreqFlyerAuth"/>
		<binding client="Arbitrator.IFlyTicketAuth" server="Flyticketdb_Frequentflyerdb.IFlyTicketAuth"/>
		<binding client="Arbitrator.IToken" server="Accountdb_Cardcenter_Token.IToken"/>
		<binding client="Accountdb_Cardcenter_Token.ITokenCallback" server="Arbitrator.ITokenCallback"/>
		<binding client="Arbitrator.ITokenLifetimeController" server="Accountdb_Cardcenter_Token.ITokenLifetimeController"/>
		
		<binding client="this.IAccount" server="Accountdb_Cardcenter_Token.IAccount"/>
		<binding client="this.IDhcpCallback" server="Arbitrator.IDhcpCallback"/>
		<binding client="Arbitrator.IDhcpServerLifetimeController" server="this.IDhcpServerLifetimeController"/>
		<binding client="this.ILogin" server="Arbitrator.ILogin"/>
		<binding client="Arbitrator.IArbitratorCallback" server="this.IArbitratorCallback"/>
		<binding client="this.IArbitratorLifetimeController" server="Arbitrator.IArbitratorLifetimeController"/>
		
		<protocol file="arbitrator-others.bp"/>
		
	</component>

	<binding client="DhcpServer.IDhcpCallback" server="Arbitrator_Others.IDhcpCallback"/>
	<binding client="Arbitrator_Others.IDhcpServerLifetimeController" server="DhcpServer.IDhcpServerLifetimeController"/>

	<binding client="this.ILifetimeController" server="Arbitrator_Others.IArbitratorLifetimeController"/>
	<binding client="this.ILogin" server="Arbitrator_Others.ILogin"/>
	<binding client="this.IAccount" server="Arbitrator_Others.IAccount"/>
	<binding client="Arbitrator_Others.IArbitratorCallback" server="this.IArbitratorCallback"/>

	<protocol file="demoframe.bp"/>
</definition>
