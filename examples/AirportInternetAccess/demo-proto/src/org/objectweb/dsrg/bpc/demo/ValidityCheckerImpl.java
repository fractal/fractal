/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.dsrg.bpc.demo;

import java.util.Date;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;

public class ValidityCheckerImpl implements BindingController, IToken, ITimerCallback, ILife {

	protected ITokenCallback iTokenCallback;
	protected ITimer iTimer;
	protected ICustomCallback iCustomCallback = null;
	
	protected Object evidence;
	protected Date validUntil;

	public ValidityCheckerImpl() {
	}

	//
	// Binding controller methods
	//
	
	public String[] listFc() {
		return new String[] { "ITokenCallback", "ITimer", "ICustomCallback" };
	}

	public Object lookupFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("ITokenCallback")) {
			return iTokenCallback;
		}
		if (cItf.equals("ITimer")) {
			return iTimer;
		}
		if (cItf.equals("ICustomCallback")) {
			return iCustomCallback;
		}
		throw new NoSuchInterfaceException(cItf);
	}

	public void bindFc(String cItf, Object sItf) throws NoSuchInterfaceException {
		if (cItf.equals("ITokenCallback")) {
			iTokenCallback = (ITokenCallback)sItf;
		} else if (cItf.equals("ITimer")) {
			iTimer = (ITimer)sItf;
		} else if (cItf.equals("ICustomCallback")) {
			iCustomCallback = (ICustomCallback)sItf;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}

	public void unbindFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("ITokenCallback")) {
			iTokenCallback=null;
		} else if (cItf.equals("ITimer")) {
			iTimer=null;
		} else if (cItf.equals("ICustomCallback")) {
			iCustomCallback=null;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}

	//
	// Business methods
	//
	
	//
	// IToken interface
	//

	public boolean InvalidateAndSave() {
		System.out.println(" ---- ValidityChecker.InvalidateAndSave -----");

		iTimer.CancelTimeouts();
		callCustomCallback();		
		iTokenCallback.TokenInvalidated(evidence);
		
		return true;
	}
	
	public void SetValidity(Date ValidUntil) {
		validUntil = ValidUntil;
	}
    
	public void SetAccountCredentials(String AccountId, String SecurityCookie) {
		if (iCustomCallback != null) {
			iCustomCallback.SetAccountCredentials(AccountId, SecurityCookie);
		}
	}
	
	public void SetEvidence(Object TokenEvidence) {
		evidence = TokenEvidence;
	}

    
	public String GetUniqueId() {
		return "ID";
	}
    
	//
	// ITimerCallback interface
	//
    
	public void Timeout() {
		System.out.println(" ---- ValidityChecker.Timeout -----");

		callCustomCallback();				
		iTokenCallback.TokenInvalidated(evidence);
	}
	
	//
	// ILife interface
	//

	public void Start() {
		iTimer.SetTimeout(validUntil);
	}

	//
	// Private methods
	//
	
	protected void callCustomCallback() {
		if (iCustomCallback != null) {
			long SecondsLeft = Math.max((validUntil.getTime() - System.currentTimeMillis()) / 1000, 0);
			iCustomCallback.InvalidatingToken(SecondsLeft);
		}
	}
			
}
