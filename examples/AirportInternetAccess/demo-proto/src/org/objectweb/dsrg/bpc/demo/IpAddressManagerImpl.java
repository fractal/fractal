/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.dsrg.bpc.demo;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;

public class IpAddressManagerImpl implements BindingController, ITimerCallback, IDhcpListenerCallback, IManagement, ILife {

	private static final long INITIAL_LEASE_TIME = 5;
	private static final long RENEW_LEASE_TIME = 5;

	protected ITimer iTimer;
	protected IIpMacDb iIpMacTransientDb;
	protected IDhcpCallback iDhcpCallback;
	protected IIpMacDb iIpMacPermanentDb;
	protected ILife iListenerLifetimeController;

	private LinkedList availableAddresses = new LinkedList();
	private Set assignedAddresses = new HashSet();
	protected boolean usePermanentDb = false;
	protected boolean renewPermanentIps = false; 
	
	public IpAddressManagerImpl() {
		byte[] byteAddr=new byte[4];
		byteAddr[0] = (byte) 192;
		byteAddr[1] = (byte) 168;
		byteAddr[2] = 0;
	    
		for (int lowestByte=1; lowestByte<256; lowestByte++) {
			byteAddr[3]=(byte)lowestByte;
			try {
				availableAddresses.add(InetAddress.getByAddress(byteAddr));
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
		}
	}

	//
	// Binding controller methods
	//
	
	public String[] listFc() {
		return new String[] { "ITimer", "IIpMacTransientDb", "IDhcpCallback", "IIpMacPermanentDb", "IListenerLifetimeController" };
	}

	public Object lookupFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("ITimer")) {
			return iTimer;
		}
		if (cItf.equals("IIpMacTransientDb")) {
			return iIpMacTransientDb;
		}
		if (cItf.equals("IDhcpCallback")) {
			return iDhcpCallback;
		}
		if (cItf.equals("IIpMacPermanentDb")) {
			return iIpMacPermanentDb;
		}
		if (cItf.equals("IListenerLifetimeController")) {
			return iListenerLifetimeController;
		}
		
		throw new NoSuchInterfaceException(cItf);
	}

	public void bindFc(String cItf, Object sItf) throws NoSuchInterfaceException {
		if (cItf.equals("ITimer")) {
			iTimer = (ITimer)sItf;
		} else if (cItf.equals("IIpMacTransientDb")) {
			iIpMacTransientDb = (IIpMacDb)sItf;
		} else if (cItf.equals("IDhcpCallback")) {
			iDhcpCallback = (IDhcpCallback)sItf;
		} else if (cItf.equals("IIpMacPermanentDb")) {
			iIpMacPermanentDb = (IIpMacDb)sItf;
		} else if (cItf.equals("IListenerLifetimeController")) {
			iListenerLifetimeController = (ILife)sItf;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}		
	}

	public void unbindFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("ITimer")) {
			iTimer=null;
		} else if (cItf.equals("IIpMacTransientDb")) {
			iIpMacTransientDb=null;
		} else if (cItf.equals("IDhcpCallback")) {
			iDhcpCallback=null;
		} else if (cItf.equals("IIpMacPermanentDb")) {
			iIpMacPermanentDb=null;
		} else if (cItf.equals("IListenerLifetimeController")) {
			iListenerLifetimeController=null;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}


	//
	// Business methods
	//
	
	//
	// ILife interface
	//
	
	public void Start() {
		iListenerLifetimeController.Start();
	}
	
	//
	// ITimerCallback interface
	//

	public void Timeout() {
		Date now = new Date();
		
		synchronized (this)
		{
			// JPF can't handle Date.toString
			System.out.println("+++++ IpAddressManager.Timeout() @ " + now.getTime() + " , " + assignedAddresses.size() + " +++++");
        
			Iterator it = assignedAddresses.iterator();
			while (it.hasNext()) {
				InetAddress addr = (InetAddress) it.next();
			
				Date expTime = iIpMacTransientDb.GetExpirationTime(addr);         
			
				// JPF can't handle Date.toString
				System.out.println("   -- probing " + addr + " @ " + ((expTime != null) ? String.valueOf(expTime.getTime()) : "null") + ", now = " + ((now != null) ? String.valueOf(now.getTime()) : "null"));
				     
				if ((expTime != null) && (expTime.getTime() <= now.getTime() + 500)) {
					System.out.println("   ++ lease expired for " + addr);
			
					iIpMacTransientDb.Remove(addr);
					assignedAddresses.remove(it);
					availableAddresses.addLast(addr);
					iDhcpCallback.IpAddressInvalidated(addr);                    
				}
			}
		}               
	}
	
	//
	// IDhcpListenerCallback interface
	//
    
	public boolean ReleaseIpAddress(byte[] MacAddress, InetAddress IpAddress) {
		System.out.println("+++++ IpAddressManager.ReleaseIpAddress " + IpAddress + " +++++");
	
		InetAddress storedIp = iIpMacTransientDb.GetIpAddress(MacAddress);

		//if (storedIp == null || !storedIp.equals(IpAddress)) {
		if (storedIp == null) return false;
		if (IpAddress == null) return false;
		if (!storedIp.toString().equals(IpAddress.toString())) return false;

		iIpMacTransientDb.Remove(IpAddress);
        
		releaseIpAddress(IpAddress);
		iDhcpCallback.IpAddressInvalidated(IpAddress);

		if (!haveAssignedAddresses()) {
			iTimer.CancelTimeouts();
		}
		
		return true;
	}
    
	public boolean RenewIpAddress(byte[] MacAddress, InetAddress IpAddress) {
		System.out.println("+++++ IpAddressManager.RenewIpAddress " + IpAddress + " +++++");
		
		InetAddress storedIp = iIpMacTransientDb.GetIpAddress(MacAddress);

		//if (storedIp == null || !storedIp.equals(IpAddress)) {
		if (storedIp == null) return false;
		if (IpAddress == null) return false;
		if (!storedIp.toString().equals(IpAddress.toString())) return false;
		
		synchronized (this)
		{
			if (usePermanentDb && !renewPermanentIps) {
				InetAddress permIp = iIpMacPermanentDb.GetIpAddress(MacAddress);
				if (permIp != null && permIp.equals(IpAddress)) {
					return false;
				}
			}
		}
		
		Date expTime = new Date(System.currentTimeMillis() + RENEW_LEASE_TIME * 1000);
        
		iIpMacTransientDb.SetExpirationTime(IpAddress, expTime);       
		iTimer.SetTimeout(expTime);

		return true;
	}
    
	public InetAddress RequestNewIpAddress(byte[] MacAddress) {
		System.out.println("+++++ IpAddressManager.RequestIpAddress" + MacAddress + " +++++");

		InetAddress ipAddr = iIpMacTransientDb.GetIpAddress(MacAddress);
		if (ipAddr == null) {
			synchronized (this) // because of usePermanentDb field
			{
				if (usePermanentDb) {
					ipAddr = iIpMacPermanentDb.GetIpAddress(MacAddress);
				} else {
					ipAddr = allocNewIpAddress();
				}
			}
		}
		if (ipAddr == null) {
			return null;
		}
        
		Date expTime = new Date(System.currentTimeMillis() + INITIAL_LEASE_TIME * 1000);

		iIpMacTransientDb.Add(MacAddress, ipAddr, expTime);
		iTimer.SetTimeout(expTime);

		return ipAddr;
	}
    
	//
	// IManagement interface
	//
    
	public synchronized void StopRenewingPermanentIpAddresses() {
		renewPermanentIps = false;
	}
    
	public synchronized void StopUsingPermanentIpDatabase() {
		usePermanentDb = false;
	}
    
	public synchronized void UsePermanentIpDatabase() {
		usePermanentDb = true;
		renewPermanentIps = true;
	}
    
	//
	// Private methods
	//
	
	private synchronized InetAddress allocNewIpAddress() {
		InetAddress ipAddr = (InetAddress) availableAddresses.removeFirst();
		
		if (ipAddr != null) {
			assignedAddresses.add(ipAddr);
		}
    
		return ipAddr;
	}
    
	private synchronized void releaseIpAddress(InetAddress addr) {
		assignedAddresses.remove(addr);
		availableAddresses.addLast(addr);
	}
    
	private synchronized boolean haveAssignedAddresses() {
		return !assignedAddresses.isEmpty();
	}
}
