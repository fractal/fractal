/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.dsrg.bpc.demo;

import java.util.*;

public class AfDbConnectionImpl implements IFlyTicketDb {

	private static final String FIRST_CLASS_SUFFIX = "F";
	private static final String BUSINESS_CLASS_SUFFIX = "B";
	private static final String ECONOMY_CLASS_SUFFIX = "E";

	protected Hashtable database;

	public AfDbConnectionImpl() {
		database = Demo_DatabaseGenerator.loadDatabase(DataRow_AfDbConnection.DATABASE_NAME);
		
		// System.out.println(" ---- AfDbConnection.AfDbConnection(): loaded database -----");
		// for (Iterator it = database.values().iterator(); it.hasNext(); ) {
		// 	DataRow_AfDbConnection row = (DataRow_AfDbConnection) it.next();
		// 	System.out.println("   -- " + row.TicketId + ", " + row.ArrivalTime + ", " + row.FrequentFlyerId);
		// }
	}

	//
	// Business methods
	//
	
	//
	// IFlyTicketDb interface
	//
	
	public String[] GetFlyTicketsByFrequentFlyerId(String FrequentFlyerId) {
		System.out.println(" #### AfDbConnection.GetFlyTicketsByFrequentFlyerId ####");
		
		if (FrequentFlyerId == null) return null;
	
		ArrayList result = new ArrayList();
		
		for (Iterator it = database.values().iterator(); it.hasNext(); ) {
			DataRow_AfDbConnection row = (DataRow_AfDbConnection) it.next();
			
			if (row.FrequentFlyerId != null && FrequentFlyerId.equals(row.FrequentFlyerId)) {
				result.add(row.TicketId);
			}
		}
		
		return (String []) result.toArray(new String[result.size()]);
	}
    
	public Date GetFlyTicketValidity(String FlyTicketId) {
		if (FlyTicketId == null) return null;
		
		if (!database.containsKey(FlyTicketId)) {
			return null;
		}
		
		return ((DataRow_AfDbConnection) database.get(FlyTicketId)).ArrivalTime;
	}
	
	public boolean IsEconomyFlyTicket(String FlyTicketId) {
		if (FlyTicketId == null) return true;
		
		if (!database.containsKey(FlyTicketId)) {
			return true;
		}
		
		return FlyTicketId.endsWith(ECONOMY_CLASS_SUFFIX);	
	}
}
