/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.dsrg.bpc.demo;

import java.util.*;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;

public class FrequentFlyerDatabaseImpl implements BindingController, IFreqFlyerAuth {

	private static final long TIME_UNITS_AFTER_ARRIVAL = 30 * 60;

	protected IFlyTicketDb iFlyTicketDb;
	protected IFlyTicketAuth iFlyTicketAuth;
	
	protected Hashtable database;

	public FrequentFlyerDatabaseImpl() {
		database = Demo_DatabaseGenerator.loadDatabase(DataRow_FrequentFlyerDatabase.DATABASE_NAME);
	}

	//
	// Binding controller methods
	//
	
	public String[] listFc() {
		return new String[] { "IFlyTicketDb", "IFlyTicketAuth" };
	}

	public Object lookupFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("IFlyTicketDb")) {
			return iFlyTicketDb;
		}
		if (cItf.equals("IFlyTicketAuth")) {
			return iFlyTicketAuth;
		}
		throw new NoSuchInterfaceException(cItf);
	}

	public void bindFc(String cItf, Object sItf) throws NoSuchInterfaceException {
		if (cItf.equals("IFlyTicketDb")) {
			iFlyTicketDb = (IFlyTicketDb)sItf;
		} else if (cItf.equals("IFlyTicketAuth")) {
			iFlyTicketAuth = (IFlyTicketAuth)sItf;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}

	public void unbindFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("IFlyTicketDb")) {
			iFlyTicketDb=null;
		} else if (cItf.equals("IFlyTicketAuth")) {
			iFlyTicketAuth=null;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}

	//
	// Business methods
	//
	
	//
	// IFreqFlyerAuth interface
	//
	
	public IToken CreateToken(String FrequentFlyerId) {
		System.out.println("..... FrequentFlyerDatabase.CreateToken(" + FrequentFlyerId + ") .....");
		
		if (FrequentFlyerId == null) return null;
		
		if (!database.containsKey(FrequentFlyerId)) {
			return null;
		}
	
		String[] ticketIds = iFlyTicketDb.GetFlyTicketsByFrequentFlyerId(FrequentFlyerId);
		
		System.out.println("   .. tickets = " + ticketIds.length + ":");
		
		if (ticketIds == null || ticketIds.length == 0) {
			return null;
		}
		
		System.out.println("   .. " + ticketIds[0] + " ..");
		
		int bestId = 0;
		Date bestValidity = iFlyTicketDb.GetFlyTicketValidity(ticketIds[0]);
		for (int i = 1; i < ticketIds.length; i++) {
		
			System.out.println("   .. " + ticketIds[i] + " ..");
		
			Date validity = iFlyTicketDb.GetFlyTicketValidity(ticketIds[i]);
			if (validity.after(bestValidity)) {
				bestId = i;
				bestValidity = validity;
			}
		}
		
		if (bestValidity.before(new Date())) {
			return null;
		} else {
			IToken token = iFlyTicketAuth.CreateToken(ticketIds[bestId], false);
			
			
			if (token != null) {
				token.SetValidity(new Date(bestValidity.getTime() + TIME_UNITS_AFTER_ARRIVAL * 1000));
	
				// JPF does not like Date.toString
				System.out.println("   .. chosen " + ticketIds[bestId] + " (" + (bestValidity.getTime() - System.currentTimeMillis())+ ") ..");
				//System.out.println("   .. chosen " + ticketIds[bestId] + " (" + bestValidity.toString() + ") ..");
			}
	
			return token;
		}
	}

}
