/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.dsrg.bpc.demo;

import java.math.BigDecimal;
import java.util.*;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;

public class AccountDatabaseImpl implements BindingController, IAccountAuth, IAccount {
	private static final double TIME_UNIT_PRICE = 0.5;
	private static final long TOKEN_VALIDITY = 30 * 60;
	private static final String SECURITY_COOKIE = "++SecretCookie++";

	protected ICardCenter iCardCenter;

	private int nextAccountId = 2000;
	private Hashtable database;
	
	public AccountDatabaseImpl() {
		database = Demo_DatabaseGenerator.loadDatabase(DataRow_AccountDatabase.DATABASE_NAME);
		
		// System.out.println(" ---- AccountDatabase.AccountDatabase(): loaded database -----");
		// for (Iterator it = database.values().iterator(); it.hasNext(); ) {
		// 	DataRow_AccountDatabase row = (DataRow_AccountDatabase) it.next();
		// 	System.out.println("   -- " + row.AccountId + ", " + row.PasswordHash);
		// }
	}

	//
	// Binding controller methods
	//
	
	public String[] listFc() {
		return new String[] { "ICardCenter" };
	}

	public Object lookupFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("ICardCenter")) {
			return iCardCenter;
		}
		throw new NoSuchInterfaceException(cItf);
	}

	public void bindFc(String cItf, Object sItf) throws NoSuchInterfaceException {
		if (cItf.equals("ICardCenter")) {
			iCardCenter = (ICardCenter)sItf;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}

	public void unbindFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("ICardCenter")) {
			iCardCenter=null;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}

	//
	// Business methods
	//
	
	//
	// IAccountAuth interface
	//
	
	public IToken CreateToken(String AccountId, String Password) {
		System.out.println(" ---- AccountDatabase.CreateToken(" + AccountId + ", " + Password + ") -----");
		
		if (AccountId == null || Password == null) return null;
		
		if (!database.containsKey(AccountId)) {
			System.out.println("   ** Invalid AccountId **");
			return null;
		}
		
		DataRow_AccountDatabase row = (DataRow_AccountDatabase) database.get(AccountId);
		if ((!row.PasswordHash.equals(Password)) || row.PrepaidTime == 0) {
			System.out.println("   ** Invalid Password or PrepaidTime is 0 **");
			return null;
		}
	
		IToken token = Simulator.createAccountToken();
		
		long TokenValidity = Math.min(row.PrepaidTime, TOKEN_VALIDITY);
		row.PrepaidTime -= TokenValidity;
		
		token.SetValidity(new Date(System.currentTimeMillis() + TokenValidity * 1000));
		token.SetAccountCredentials(AccountId, SECURITY_COOKIE);
		
		System.out.println(
			"   -- AccountId " + AccountId + ": " + Long.toString(row.PrepaidTime + TokenValidity) +
			" - " + Long.toString(TokenValidity) + " => " + Long.toString(row.PrepaidTime)
		);
        
		return token;
	}
	
	//
	// IAccount interface
	//
    
	public void AdjustAccountPrepaidTime(String AccountId, String SecurityCookie, long PrepaidTime) {
		System.out.print("  --- AccountDatabase.AdjustAccountPrepaidTime: ");
		System.out.println(AccountId + ", " + SecurityCookie + ", " + Long.toString(PrepaidTime) + " -----");
	
		if (AccountId == null || SecurityCookie == null) return;	
		if (database.containsKey(AccountId) && SecurityCookie.equals(SECURITY_COOKIE)) {
			DataRow_AccountDatabase row = (DataRow_AccountDatabase) database.get(AccountId);
			row.PrepaidTime += PrepaidTime;
			
			System.out.println(
				"   -- AccountId " + AccountId + ": " + Long.toString(row.PrepaidTime - PrepaidTime) +
				" + " + Long.toString(PrepaidTime) + " => " + Long.toString(row.PrepaidTime)
			);
		}
	}

	public boolean CreateAccount(String AccountId, String Password) {
		if (AccountId == null || Password == null) return false;
	
		if (database.containsKey(AccountId)) {
			return false;
		}
		
		DataRow_AccountDatabase row = new DataRow_AccountDatabase();
		row.AccountId = AccountId;
		row.PasswordHash = Password;
		row.PrepaidTime = 0;
		
		return true;
	}
    
	public String GenerateRandomAccountId() {    
		String AccountId;
		
		do {
			AccountId = Integer.toString(nextAccountId++);
		} while (database.containsKey(AccountId));
		
		return AccountId;
	}
    
	public boolean RechargeAccount(String AccountId, String CreditCardId, Date CreditCardExpirationDate, long PrepaidTime) {
		if (AccountId == null || CreditCardId == null || CreditCardExpirationDate == null) return false;
		
		if (!database.containsKey(AccountId)) {
			return false;
		}
		
		boolean result = iCardCenter.Withdraw(
			CreditCardId, CreditCardExpirationDate, new BigDecimal(TIME_UNIT_PRICE * PrepaidTime)
		);
       
       		if (!result) {
			return false;
		} else {
			DataRow_AccountDatabase row = (DataRow_AccountDatabase) database.get(AccountId);
			row.PrepaidTime += PrepaidTime;
			
			System.out.println(
				"   -- AccountId " + AccountId + ": " + Long.toString(row.PrepaidTime - PrepaidTime) +
				" + " + Long.toString(PrepaidTime) + " => " + Long.toString(row.PrepaidTime)
			);
			
			return true;
		}
	}
}
