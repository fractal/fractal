/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.dsrg.bpc.demo;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Date;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.util.Fractal;

import org.objectweb.fractal.bpc.ProtocolController;


public class Simulator implements BindingController, Runnable, IArbitratorCallback {

	protected ILife iArbitratorLifetimeController;
	protected ILogin iLogin;
	protected IAccount iAccount;

	public static Component demo;
	public static Component arbitrator;
	public static DhcpListenerImpl dhcpListener;
	public static Component accountDatabase;

 

	public static final long CARD_EXPDATE_MS = System.currentTimeMillis() + 30*24*3600*1000;


	public Simulator() {
	}

	//
	// Binding controller methods
	//
	
	public String[] listFc() {
		return new String[] { "IArbitratorLifetimeController", "ILogin" };
	}

	public Object lookupFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("IArbitratorLifetimeController")) {
			return iArbitratorLifetimeController;
		}
		if (cItf.equals("ILogin")) {
			return iLogin;
		}
		if (cItf.equals("IAccount")) {
			return iAccount;
		}
		throw new NoSuchInterfaceException(cItf);
	}

	public void bindFc(String cItf, Object sItf) throws NoSuchInterfaceException {
		if (cItf.equals("IArbitratorLifetimeController")) {
			iArbitratorLifetimeController = (ILife)sItf;
		} else if (cItf.equals("ILogin")) {
			iLogin = (ILogin)sItf;
			
			arbitrator = ((Interface)iLogin).getFcItfOwner();
			demo = Fractal.getSuperController(arbitrator).getFcSuperComponents()[0];
		} else if (cItf.equals("IAccount")) {
			iAccount = (IAccount)sItf;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}

	public void unbindFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("IArbitratorLifetimeController")) {
			iArbitratorLifetimeController=null;
		} else if (cItf.equals("ILogin")) {
			iLogin=null;
		} else if (cItf.equals("IAccount")) {
			iAccount=null;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}

	//
	// Business methods
	//
	
	public static IToken createToken() {
		try {
			
		
			// following code corresponds to the Token.fractal file
           
			Component bootComp = Fractal.getBootstrapComponent();
			TypeFactory tf = Fractal.getTypeFactory(bootComp);
            
			ComponentType tokenType = tf.createFcType(new InterfaceType[]{
				tf.createFcItfType("ITokenCallback", "org.objectweb.dsrg.bpc.demo.ITokenCallback", TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE),
				tf.createFcItfType("IToken", "org.objectweb.dsrg.bpc.demo.IToken", TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE),
				tf.createFcItfType("IAccount", "org.objectweb.dsrg.bpc.demo.IAccount", TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE),
				tf.createFcItfType("ILifetimeController", "org.objectweb.dsrg.bpc.demo.ILife", TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE)
			});
     
			ComponentType validityCheckerType = tf.createFcType(new InterfaceType[]{
				tf.createFcItfType("ITokenCallback", "org.objectweb.dsrg.bpc.demo.ITokenCallback", TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE),
				tf.createFcItfType("IToken", "org.objectweb.dsrg.bpc.demo.IToken", TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE),
				tf.createFcItfType("ITimerCallback", "org.objectweb.dsrg.bpc.demo.ITimerCallback", TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE),
				tf.createFcItfType("ITimer", "org.objectweb.dsrg.bpc.demo.ITimer", TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE),
				tf.createFcItfType("ICustomCallback", "org.objectweb.dsrg.bpc.demo.ICustomCallback", TypeFactory.CLIENT, TypeFactory.OPTIONAL, TypeFactory.SINGLE),
				tf.createFcItfType("ILifetimeController", "org.objectweb.dsrg.bpc.demo.ILife", TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE)
			});
            
			ComponentType timerType = tf.createFcType(new InterfaceType[]{
				tf.createFcItfType("ITimerCallback", "org.objectweb.dsrg.bpc.demo.ITimerCallback", TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE),
				tf.createFcItfType("ITimer", "org.objectweb.dsrg.bpc.demo.ITimer", TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE)
			});

			GenericFactory gf = Fractal.getGenericFactory(bootComp);

			Component tokenComp = gf.newFcInstance(tokenType, "composite-rtcheck", null);
			String tokenBP = loadProtocolFromClasspath("runtime/token.bp"); 
			((ProtocolController) tokenComp.getFcInterface("protocol-controller")).setFcProtocol(tokenBP);
			((NameController) tokenComp.getFcInterface("name-controller")).setFcName("Token");
            
			Component validityCheckerComp = gf.newFcInstance(validityCheckerType, "primitive-rtcheck", "org.objectweb.dsrg.bpc.demo.ValidityCheckerImpl");
			String validityCheckerBP = loadProtocolFromClasspath("runtime/validitychecker.bp");
			((ProtocolController) validityCheckerComp.getFcInterface("protocol-controller")).setFcProtocol(validityCheckerBP);
			((NameController) validityCheckerComp.getFcInterface("name-controller")).setFcName("ValidityChecker");
            
			Component timerComp = gf.newFcInstance(timerType, "primitive-rtcheck", "org.objectweb.dsrg.bpc.demo.TimerImpl");
			String timerBP = loadProtocolFromClasspath("runtime/timer.bp");
			((ProtocolController) timerComp.getFcInterface("protocol-controller")).setFcProtocol(timerBP);
			((NameController) timerComp.getFcInterface("name-controller")).setFcName("Timer");
			          
			Fractal.getContentController(tokenComp).addFcSubComponent(validityCheckerComp);
			Fractal.getContentController(tokenComp).addFcSubComponent(timerComp);

			Fractal.getBindingController(timerComp).bindFc("ITimerCallback", validityCheckerComp.getFcInterface("ITimerCallback"));
			Fractal.getBindingController(validityCheckerComp).bindFc("ITimer", timerComp.getFcInterface("ITimer"));
            
			Fractal.getBindingController(tokenComp).bindFc("IToken", validityCheckerComp.getFcInterface("IToken"));
			Fractal.getBindingController(validityCheckerComp).bindFc("ITokenCallback", Fractal.getContentController(tokenComp).getFcInternalInterface("ITokenCallback"));
			Fractal.getBindingController(tokenComp).bindFc("ILifetimeController", validityCheckerComp.getFcInterface("ILifetimeController"));
            		
			
			/*
			
			Factory f = FactoryFactory.getFactory("org.objectweb.fractal.bpc.adl.Factory","org.objectweb.fractal.bpc.adl.FractalBackend", new HashMap());
			Component tokenComp=(Component)f.newComponent("org.objectweb.dsrg.bpc.demo.Token", new HashMap());
			
			*/

			Fractal.getContentController(demo).addFcSubComponent(tokenComp);
			Fractal.getBindingController(tokenComp).bindFc("ITokenCallback", arbitrator.getFcInterface("ITokenCallback"));
			Fractal.getBindingController(tokenComp).bindFc("IAccount", accountDatabase.getFcInterface("IAccount"));
           
			Fractal.getLifeCycleController(tokenComp).startFc();
            
			IToken token = (IToken) tokenComp.getFcInterface("IToken");
            
			return token;
		} catch (IllegalContentException e) {
			e.printStackTrace();
		} catch (IllegalLifeCycleException e) {
			e.printStackTrace();
		} catch (NoSuchInterfaceException e) {
			e.printStackTrace();
		} catch (IllegalBindingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
        
		return null;
	}

	public static IToken createAccountToken() {
		try {
			
			
			// following code corresponds to the TokenAccount.fractal file
           
			Component bootComp = Fractal.getBootstrapComponent();
			TypeFactory tf = Fractal.getTypeFactory(bootComp);
            
			ComponentType tokenType = tf.createFcType(new InterfaceType[]{
				tf.createFcItfType("ITokenCallback", "org.objectweb.dsrg.bpc.demo.ITokenCallback", TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE),
				tf.createFcItfType("IToken", "org.objectweb.dsrg.bpc.demo.IToken", TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE),
				tf.createFcItfType("IAccount", "org.objectweb.dsrg.bpc.demo.IAccount", TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE),
				tf.createFcItfType("ILifetimeController", "org.objectweb.dsrg.bpc.demo.ILife", TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE)
			});
     
			ComponentType validityCheckerType = tf.createFcType(new InterfaceType[]{
				tf.createFcItfType("ITokenCallback", "org.objectweb.dsrg.bpc.demo.ITokenCallback", TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE),
				tf.createFcItfType("IToken", "org.objectweb.dsrg.bpc.demo.IToken", TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE),
				tf.createFcItfType("ITimerCallback", "org.objectweb.dsrg.bpc.demo.ITimerCallback", TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE),
				tf.createFcItfType("ITimer", "org.objectweb.dsrg.bpc.demo.ITimer", TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE),
				tf.createFcItfType("ICustomCallback", "org.objectweb.dsrg.bpc.demo.ICustomCallback", TypeFactory.CLIENT, TypeFactory.OPTIONAL, TypeFactory.SINGLE),
				tf.createFcItfType("ILifetimeController", "org.objectweb.dsrg.bpc.demo.ILife", TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE)
			});
            
			ComponentType timerType = tf.createFcType(new InterfaceType[]{
				tf.createFcItfType("ITimerCallback", "org.objectweb.dsrg.bpc.demo.ITimerCallback", TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE),
				tf.createFcItfType("ITimer", "org.objectweb.dsrg.bpc.demo.ITimer", TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE)
			});

			ComponentType customTokenType = tf.createFcType(new InterfaceType[]{
				tf.createFcItfType("ICustomCallback", "org.objectweb.dsrg.bpc.demo.ICustomCallback", TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE),
				tf.createFcItfType("IAccount", "org.objectweb.dsrg.bpc.demo.IAccount", TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE)
			});
            
			GenericFactory gf = Fractal.getGenericFactory(bootComp);

			Component tokenComp = gf.newFcInstance(tokenType, "composite-rtcheck", null);
			String tokenBP = loadProtocolFromClasspath("runtime/token.bp"); 
			((ProtocolController) tokenComp.getFcInterface("protocol-controller")).setFcProtocol(tokenBP);
			((NameController) tokenComp.getFcInterface("name-controller")).setFcName("Token");
            
			Component validityCheckerComp = gf.newFcInstance(validityCheckerType, "primitive-rtcheck", "org.objectweb.dsrg.bpc.demo.ValidityCheckerImpl");
			String validityCheckerBP = loadProtocolFromClasspath("runtime/validitychecker.bp");
			((ProtocolController) validityCheckerComp.getFcInterface("protocol-controller")).setFcProtocol(validityCheckerBP);
			((NameController) validityCheckerComp.getFcInterface("name-controller")).setFcName("ValidityChecker");
            
			Component timerComp = gf.newFcInstance(timerType, "primitive-rtcheck", "org.objectweb.dsrg.bpc.demo.TimerImpl");
			String timerBP = loadProtocolFromClasspath("runtime/timer.bp");
			((ProtocolController) timerComp.getFcInterface("protocol-controller")).setFcProtocol(timerBP);
			((NameController) timerComp.getFcInterface("name-controller")).setFcName("Timer");
			          
			Component customTokenComp = gf.newFcInstance(customTokenType, "primitive-rtcheck", "org.objectweb.dsrg.bpc.demo.CustomTokenImpl");
			String customTokenBP = loadProtocolFromClasspath("runtime/customtoken.bp");
			((ProtocolController) customTokenComp.getFcInterface("protocol-controller")).setFcProtocol(customTokenBP);
			((NameController) customTokenComp.getFcInterface("name-controller")).setFcName("CustomToken");
            
			Fractal.getContentController(tokenComp).addFcSubComponent(validityCheckerComp);
			Fractal.getContentController(tokenComp).addFcSubComponent(timerComp);
			Fractal.getContentController(tokenComp).addFcSubComponent(customTokenComp);

			Fractal.getBindingController(timerComp).bindFc("ITimerCallback", validityCheckerComp.getFcInterface("ITimerCallback"));
			Fractal.getBindingController(validityCheckerComp).bindFc("ITimer", timerComp.getFcInterface("ITimer"));
			Fractal.getBindingController(validityCheckerComp).bindFc("ICustomCallback", customTokenComp.getFcInterface("ICustomCallback"));
            
			Fractal.getBindingController(customTokenComp).bindFc("IAccount", Fractal.getContentController(tokenComp).getFcInternalInterface("IAccount"));
			Fractal.getBindingController(tokenComp).bindFc("IToken", validityCheckerComp.getFcInterface("IToken"));
			Fractal.getBindingController(validityCheckerComp).bindFc("ITokenCallback", Fractal.getContentController(tokenComp).getFcInternalInterface("ITokenCallback"));
			Fractal.getBindingController(tokenComp).bindFc("ILifetimeController", validityCheckerComp.getFcInterface("ILifetimeController"));
			
			/*
			            
			Factory f = FactoryFactory.getFactory("org.objectweb.fractal.bpc.adl.Factory","org.objectweb.fractal.bpc.adl.FractalBackend", new HashMap());
			Component tokenComp=(Component)f.newComponent("org.objectweb.dsrg.bpc.demo.TokenAccount", new HashMap());

			*/
			
			Fractal.getContentController(demo).addFcSubComponent(tokenComp);
			Fractal.getBindingController(tokenComp).bindFc("ITokenCallback", arbitrator.getFcInterface("ITokenCallback"));
			Fractal.getBindingController(tokenComp).bindFc("IAccount", accountDatabase.getFcInterface("IAccount"));
           
			Fractal.getLifeCycleController(tokenComp).startFc();
            
			IToken token = (IToken) tokenComp.getFcInterface("IToken");
            
			return token;
		} catch (IllegalContentException e) {
			e.printStackTrace();
		} catch (IllegalLifeCycleException e) {
			e.printStackTrace();
		} catch (NoSuchInterfaceException e) {
			e.printStackTrace();
		} catch (IllegalBindingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
        
		return null;
	}

    
	public static void startToken(IToken token) {
		try {
			((ILife)((Interface)token).getFcItfOwner().getFcInterface("ILifetimeController")).Start();
		} catch (NoSuchInterfaceException e) {
			e.printStackTrace();
		}        
	}
    
	public void run() {
		try {
			iArbitratorLifetimeController.Start();
            
			byte[] mac1=new byte[] { 0, 0, 0, 0, 0, 0 };
			byte[] mac2=new byte[] { 0, 0, 0, 0, 0, 1 };
			byte[] mac3=new byte[] { 0, 0, 0, 0, 0, 2 };
			byte[] mac4=new byte[] { 0, 0, 0, 0, 0, 3 };
				    
			InetAddress addr1=dhcpListener.RequestNewIpAddress(mac1);
			            
			iLogin.LoginWithAccountId(addr1,"1005","Password5");
				    
			iAccount.RechargeAccount("1005", "1111-2222-3333-4401", new Date(CARD_EXPDATE_MS), 60);
			iLogin.LoginWithAccountId(addr1,"1005","Password5");
			
			InetAddress addr2=dhcpListener.RequestNewIpAddress(mac2);
			InetAddress addr3=dhcpListener.RequestNewIpAddress(mac3);
				    	    
			iLogin.LoginWithFlyTicketId(addr3,"AFR1002B");
			iLogin.LoginWithFrequentFlyerId(addr2,"SKY1001");
				    
			InetAddress addr4=dhcpListener.RequestNewIpAddress(mac4);
			iAccount.RechargeAccount("1007", "1111-2222-3333-4402", new Date(CARD_EXPDATE_MS), 1);
			iLogin.LoginWithAccountId(addr4,"1007","Password7");
			            
			Thread.sleep(1000);
			dhcpListener.RenewIpAddress(mac1,addr1);
			            
			Thread.sleep(1000);
			dhcpListener.RenewIpAddress(mac1,addr1);
			
			Thread.sleep(1000);
			dhcpListener.ReleaseIpAddress(mac1,addr1);
			
			Thread.sleep(1000);
			iLogin.Logout(addr3);
			dhcpListener.ReleaseIpAddress(mac3,addr3);
				    
			Thread.sleep(3000);
			// iLogin.Logout(addr2);
			// dhcpListener.ReleaseIpAddress(mac2,addr2);
				    
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static String loadProtocolFromClasspath(String classpathFile) {
		if (classpathFile == null) return null;
        
		String content = null;
        
		try {
			// We can't use InputStreamReader here because that class works with charsets and
			// we have no MJI peers for charset-related classes
			// therefore, the file can contain only ASCII characters with codes in range 0-127
            
			java.io.InputStream in = ClassLoader.getSystemClassLoader().getResourceAsStream(classpathFile);
			if (in == null) return null; // File does not exist
            
			int fileSize = in.available();
			byte[] fileBytes = new byte[fileSize];
			in.read(fileBytes);
			in.close();
            
			char[] fileChars = new char[fileSize];
			for (int i = 0; i < fileSize; i++) {
				if (fileBytes[i] < 0) {
					fileChars[i] = (char) 32;
				} else {
					fileChars[i] = (char) fileBytes[i];
				}
			}
             
			content = new String(fileChars);
		} catch (Exception ex) {
			ex.printStackTrace();
			content = null;
		}
		
		return content;
	}
	
	//
	// IArbitratorCallback
	//
	public void Notify() {
		// nothing to be done...
	}

}


