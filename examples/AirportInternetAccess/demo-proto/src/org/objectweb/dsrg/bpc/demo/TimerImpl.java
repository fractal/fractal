/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.dsrg.bpc.demo;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;

public class TimerImpl implements BindingController, ITimer {

	protected ITimerCallback iTimerCallback;

	protected Timer timer = null;
	
	public TimerImpl() {
	}
	
	//
	// Custom TimerTask
	//
	
	public class TimerImplTask extends TimerTask {
		public TimerImplTask() {
			super();
		}
	    
		public void run() {
			iTimerCallback.Timeout();

			synchronized (TimerImpl.this)
			{
				if (timer != null) 
				{
					timer.cancel();
					timer = null;
				}
			}
		}
	}

	//
	// Binding controller methods
	//
	
	public String[] listFc() {
		return new String[] { "ITimerCallback" };
	}

	public Object lookupFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("ITimerCallback")) {
			return iTimerCallback;
		}
		throw new NoSuchInterfaceException(cItf);
	}

	public void bindFc(String cItf, Object sItf) throws NoSuchInterfaceException {
		if (cItf.equals("ITimerCallback")) {
			iTimerCallback = (ITimerCallback)sItf;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}

	public void unbindFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("ITimerCallback")) {
			iTimerCallback=null;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}


	//
	// Business methods
	//
	
	//
	// ITimer interface
	//
	
	public void CancelTimeouts() {
		synchronized (this)
		{
			if (timer != null) {
				timer.cancel();
				timer = null;
			}
		}
	}

	public void SetTimeout(Date Timeout) {
		synchronized (this)
		{
			if (timer == null) {
				timer = new Timer(true);
			}
		}
		
		timer.schedule(new TimerImplTask(), Timeout);
	}

}
