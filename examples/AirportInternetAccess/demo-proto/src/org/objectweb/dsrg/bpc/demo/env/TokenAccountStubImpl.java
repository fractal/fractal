/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.dsrg.bpc.demo.env;

import java.util.Date;

import org.objectweb.fractal.api.NoSuchInterfaceException;

import org.objectweb.dsrg.bpc.demo.ILife;
import org.objectweb.dsrg.bpc.demo.ITimer;
import org.objectweb.dsrg.bpc.demo.ITimerCallback;
import org.objectweb.dsrg.bpc.demo.IToken;
import org.objectweb.dsrg.bpc.demo.ITokenCallback;
import org.objectweb.dsrg.bpc.demo.ICustomCallback;


public class TokenAccountStubImpl implements IToken, ILife, ITimerCallback 
{
	protected ITokenCallback iTokenCallback;
	protected ITimer iTimer;
	protected ICustomCallback iCustomCallback;
	
	protected Object evidence;
	protected Date validUntil;


	public TokenAccountStubImpl() 
	{
	}

	// Binding controller methods
	// --------------------------
	public String[] listFc() 
	{
		return new String[] {"ITokenCallback", "ITimer", "ICustomCallback"};
	}

	public Object lookupFc(String itf) throws NoSuchInterfaceException 
	{
		if (itf.equals("ITokenCallback")) return iTokenCallback;
		else if (itf.equals("ITimer")) return iTimer;
		else if (itf.equals("ICustomCallback")) return iCustomCallback;
		throw new NoSuchInterfaceException(itf);
	}

	public void bindFc(String itf, Object obj) throws NoSuchInterfaceException 
	{
		if (itf.equals("ITokenCallback")) iTokenCallback = (ITokenCallback) obj;
		else if (itf.equals("ITimer")) iTimer = (ITimer) obj;
		else if (itf.equals("ICustomCallback")) iCustomCallback = (ICustomCallback) obj;
		else throw new NoSuchInterfaceException(itf);
	}

	public void unbindFc(String itf) throws NoSuchInterfaceException 
	{
		if (itf.equals("ITokenCallback")) iTokenCallback = null;
		else if (itf.equals("ITimer")) iTimer = null;
		else if (itf.equals("ICustomCallback")) iCustomCallback = null;
		else throw new NoSuchInterfaceException(itf);
	}


	// Business methods
	// ------------------------
    public void Start() 
	{
		iTimer.SetTimeout(validUntil);
    }

	public boolean InvalidateAndSave()
	{
		iTimer.CancelTimeouts();
		callCustomCallback();
		iTokenCallback.TokenInvalidated(evidence);

		return true;
	}

	public void SetValidity(Date tokenValidity)
	{
		this.validUntil = tokenValidity;
	}

	public void SetAccountCredentials(String accountId, String securCookie)
	{
		iCustomCallback.SetAccountCredentials(accountId, securCookie);
	}

	public void SetEvidence(Object tokenEvidence)
	{
		this.evidence = tokenEvidence;
	}

	public String GetUniqueId()
	{
		return "ID";
	}

	public void Timeout()
	{
		callCustomCallback();
		iTokenCallback.TokenInvalidated(evidence);
	}

	protected void callCustomCallback()
	{
		long secondsLeft = Math.max((validUntil.getTime() - System.currentTimeMillis()) / 1000, 0);
		iCustomCallback.InvalidatingToken(secondsLeft);
	}
}
