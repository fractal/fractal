/*
 * $Id$
 *
 * Behavior Protocols extensions for static and runtime checking
 * developed for the Julia implementation of Fractal.
 *
 * Copyright (C) 2006
 *    Formal Methods In Software Engineering Group
 *    Institute of Computer Science
 *    Academy of Sciences of the Czech Republic
 *
 * Copyright (C) 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Contact: ft@nenya.ms.mff.cuni.cz
 * Authors: Tomas Bures <bures@nenya.ms.mff.cuni.cz>
 *
 */

package org.objectweb.dsrg.bpc.demo;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;

public class CustomTokenImpl implements BindingController, ICustomCallback {

	protected IAccount iAccount;
	
	private String accountId = null;
	private String securityCookie = null;

	public CustomTokenImpl() {
	}


	//
	// Binding controller methods
	//
	
	public String[] listFc() {
		return new String[] { "IAccount" };
	}

	public Object lookupFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("IAccount")) {
			return iAccount;
		}
		throw new NoSuchInterfaceException(cItf);
	}

	public void bindFc(String cItf, Object sItf) throws NoSuchInterfaceException {
		if (cItf.equals("IAccount")) {
			iAccount = (IAccount)sItf;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}

	public void unbindFc(String cItf) throws NoSuchInterfaceException {
		if (cItf.equals("IAccount")) {
			iAccount=null;
		} else {
			throw new NoSuchInterfaceException(cItf);
		}
	}


	//
	// Business methods
	//
	
	//
	// ICustomCallback interface
	//

	public void SetAccountCredentials(String AccountId, String SecurityCookie) {
		accountId = AccountId;
		securityCookie = SecurityCookie;
	}

	public boolean InvalidatingToken(long TimeLeft) {
		iAccount.AdjustAccountPrepaidTime(accountId, securityCookie, TimeLeft);
		return true;
	}
}
