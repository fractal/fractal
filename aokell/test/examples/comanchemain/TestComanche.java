/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package comanchemain;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import junit.framework.TestCase;

/**
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class TestComanche extends TestCase {

    public void testComanche() throws Exception {
        System.out.println("=================== comanche ===================");
        
        ThreadMain comanche = new ThreadMain();
        comanche.start();
        
            // Waiting for comanche to start up.
            Thread.sleep(3000);
            
            // Load the root page. Should be empty.
            try {
                load( "http://localhost:8080", 0, new int[]{} );
                fail();
            }
            catch(FileNotFoundException fne) {}
            
            // Load a well-known logo.
            load( "http://localhost:8080/gnu.jpg",
                  0x14a6,
                  new int[]{255,216,255,224,0}
                  );
            
            // Stop comanche.
            comanche.interrupt();
            
            if( comanche.e != null ) {
                // comande thread ended with an exception
                throw new RuntimeException(comanche.e);
            }
    }
    
    private void load(
            String urlStr, int expectedLength, int[] expectedBytes )
    throws IOException {
        
        System.out.println("URL            : "+urlStr);
        System.out.println("Expected length: "+expectedLength);
        System.out.print("Expected bytes : ");
        for (int i = 0; i < expectedBytes.length; i++) {
            System.out.print(expectedBytes[i]+" ");
        }
        System.out.println();
        
        URL url = new URL(urlStr);
        URLConnection uc = url.openConnection();
        InputStream is = uc.getInputStream();
        
        System.out.print("Bytes          : ");
        int i,length=0;
        boolean differs = false;
        while( (i=is.read()) != -1 ) {
            if( length < expectedBytes.length ) {
                System.out.print(i+" ");
                if( i != expectedBytes[length] ) {
                    differs = true;
                }
            }
            length++;
        }
        System.out.println();
        System.out.println("Length         : "+length);
        
        assertFalse(differs);
        assertEquals(length,expectedLength);
    }
    
    /**
     * Call main in a thread and store potential exception in a field. 
     */
    private static class ThreadMain extends Thread {
        public Exception e;
        public void run() {
            try {
                comanchemain.Main.main(null);
            }
            catch(Exception e) {
                this.e = e;
            }
        }
    }
    
}
