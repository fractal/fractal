/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.perf;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;

import org.objectweb.fractal.julia.perf.components.CObject;
import org.objectweb.fractal.julia.perf.components.I;
import org.objectweb.fractal.julia.perf.components.C;

import org.objectweb.fractal.util.Fractal;

/**
 * An application to test the performances of the framework. This application
 * computes the size of empty components and compares it to the size of an empty
 * Java object. It also computes the duration of an empty method call on a
 * component, and compares it to the duration of an empty method call on a Java
 * object (via a Java interface, i.e., through an opc_invokeinterface bytecode
 * instruction). These measurements are done both with compiled and generated
 * containers.
 */

public class TestPerf {

  /**
   * Launches the performance measurement tests.
   * 
   * @param args the command line arguments.
   * @throws Exception if a problem occurs.
   */

  public static void main (final String[] args) throws Exception {

    Component boot = Fractal.getBootstrapComponent();

    TypeFactory tf = Fractal.getTypeFactory(boot);
    ComponentType type = tf.createFcType(new InterfaceType[] {
      tf.createFcItfType(
        "s",
        I.class.getName(),
        false,
        false,
        false)
    });
    GenericFactory cf = Fractal.getGenericFactory(boot);
    Component tmpl = cf.newFcInstance(
      type, "primitiveTemplate", new Object[] {
        "primitive", C.class.getName()
      });

    Component comp = Fractal.getFactory(tmpl).newFcInstance();
    try {
      Fractal.getLifeCycleController(comp).startFc();
    } catch (NoSuchInterfaceException _) {
    }
    I i = (I)comp.getFcInterface("s");

    System.out.println("Julia performances, " + args[0]);
    computeObjectCallDuration();
    computeComponentCallDuration(i);
    computeEmptyObjectSize();
    
    // TODO Unresolved issue: the following test seems to enter an endless loop with componentized membranes (may be related to the way the memory footprint is computed)
    // computeEmptyComponentSize(Fractal.getFactory(tmpl));

    computeComponentTypeSize((ComponentType)comp.getFcType());
    computeComponentCreationDuration();
    computeTemplateCreationDuration();
    computeTemplateInstantiationDuration(Fractal.getFactory(tmpl));
    System.out.println();
  }

  // -------------------------------------------------------------------------
  // Time measurements
  // -------------------------------------------------------------------------

  /**
   * Number of method calls to execute to estimate method call durations.
   */

  private final static int NB_CALLS = 10000000;

  /**
   * Number of estimations needed to compute min, max and mean values.
   */

  private final static int LOOPS = 7;

  /**
   * Mean duration of an empty method call on an object.
   */

  private static double objectCallMean;

  /**
   * Estimates the duration of an empty method call on an object.
   */

  private static void computeObjectCallDuration () {
    // initializes the min, max and mean value
    double min = Long.MAX_VALUE;
    double max = Long.MIN_VALUE;
    double mean = 0;
    I itf = new CObject();

    // makes several estimations in order to compute min, max and mean
    for (int j = 0; j < LOOPS; ++j) {
      int i = NB_CALLS;
      double dt = System.currentTimeMillis();
      while (i > 0) {
        i = itf.m(i);
      }
      dt = 1000 * (System.currentTimeMillis() - dt) / NB_CALLS;
      // the first iterations are not taken into account (warm up)
      if (j > 1) {
        // updates the min, max and mean values
        min = Math.min(dt, min);
        max = Math.max(dt, max);
        mean += dt;
      }
    }
    // computes the mean value in micro-seconds
    mean = mean / (LOOPS - 2);
    // computes the variation in nano-seconds
    double delta = 1000 * Math.max(max - mean, mean - min);
    // prints the results
    System.out.print("method call     = ");
    System.out.println(format(mean) + " �s (+/- " + format(delta) + " ns)");
    // stores the min, max and mean value to compute overhead of components
    // (see computeComponentCallDuration)
    objectCallMean = mean;
  }

  /**
   * Estimates the duration of an empty method call on a component.
   * 
   * @param itf the interface to be used for measurements.
   */

  private static void computeComponentCallDuration (I itf) {
    // initializes the min, max and mean value
    double min = Long.MAX_VALUE;
    double max = Long.MIN_VALUE;
    double mean = 0;

    // makes several estimations in order to compute min, max and mean
    for (int j = 0; j < LOOPS; ++j) {
      int i = NB_CALLS;
      double dt = System.currentTimeMillis();
      while (i > 0) {
        i = itf.m(i);
      }
      dt = 1000 * (System.currentTimeMillis() - dt) / NB_CALLS;
      // the first iterations are not taken into account (warm up)
      if (j > 1) {
        // updates the min, max and mean values
        min = Math.min(dt, min);
        max = Math.max(dt, max);
        mean += dt;
      }
    }
    // computes the mean value in micro-seconds
    mean = mean / (LOOPS - 2);
    // computes the variation in nano-seconds
    double delta = 1000 * Math.max(max - mean, mean - min);
    // compute the mean overhead, due to the interception code
    double overhead = mean - objectCallMean;
    // prints the results
    System.out.print("component call  = ");
    System.out.print(format(mean) + " �s (+/- " + format(delta) + " ns)");
    System.out.print(" => +" + format(overhead) + " �s");
    System.out.println(" (" + format(overhead / objectCallMean) + " calls)");
  }

  /**
   * Estimates the time needed to create a component.
   * 
   * @throws Exception if a problem occurs.
   */

  private static void computeComponentCreationDuration () throws Exception {
    Component boot = Fractal.getBootstrapComponent();
    // initializes the min, max and mean value
    double min = Long.MAX_VALUE;
    double max = Long.MIN_VALUE;
    double mean = 0;
    // makes several estimations in order to compute min, max and mean
    for (int j = 0; j < LOOPS; ++j) {
      int i = 0;
      double dt = System.currentTimeMillis();
      while (i < NB_INSTANCES_2) {
        TypeFactory tf = Fractal.getTypeFactory(boot);
        GenericFactory cf = Fractal.getGenericFactory(boot);
        ComponentType type = tf.createFcType(new InterfaceType[] {
          tf.createFcItfType(
            "s",
            I.class.getName(),
            false,
            false,
            false)
        });
        cf.newFcInstance(type, "primitive", C.class.getName());
        ++i;
      }
      dt = (System.currentTimeMillis() - dt) / NB_INSTANCES_2;
      // the first iterations are not taken into account (warm up)
      if (j > 1) {
        // updates the min, max and mean values
        min = Math.min(dt, min);
        max = Math.max(dt, max);
        mean += dt;
      }
    }
    // computes the mean value in milli-seconds
    mean = mean / (LOOPS - 2);
    // computes the variation in micro-seconds
    double delta = 1000 * Math.max(max - mean, mean - min);
    // prints the results
    System.out.print("component creation  = ");
    System.out.println(format(mean) + " ms (+/- " + format(delta) + " �s)");
  }

  /**
   * Estimates the time needed to create a component template.
   * 
   * @throws Exception if a problem occurs.
   */

  private static void computeTemplateCreationDuration () throws Exception {
    Component boot = Fractal.getBootstrapComponent();
    // initializes the min, max and mean value
    double min = Long.MAX_VALUE;
    double max = Long.MIN_VALUE;
    double mean = 0;
    // makes several estimations in order to compute min, max and mean
    for (int j = 0; j < LOOPS; ++j) {
      int i = 0;
      double dt = System.currentTimeMillis();
      while (i < NB_INSTANCES_2) {
        TypeFactory tf = Fractal.getTypeFactory(boot);
        ComponentType type = tf.createFcType(new InterfaceType[] {
          tf.createFcItfType(
            "s",
            I.class.getName(),
            false,
            false,
            false)
        });
        GenericFactory cf = Fractal.getGenericFactory(boot);
        cf.newFcInstance(
          type, "primitiveTemplate", new Object[] {
            "primitive", C.class.getName()
          });
        ++i;
      }
      dt = (System.currentTimeMillis() - dt) / NB_INSTANCES_2;
      // the first iterations are not taken into account (warm up)
      if (j > 1) {
        // updates the min, max and mean values
        min = Math.min(dt, min);
        max = Math.max(dt, max);
        mean += dt;
      }
    }
    // computes the mean value in milli-seconds
    mean = mean / (LOOPS - 2);
    // computes the variation in micro-seconds
    double delta = 1000 * Math.max(max - mean, mean - min);
    // prints the results
    System.out.print("template creation  = ");
    System.out.println(format(mean) + " ms (+/- " + format(delta) + " �s)");
  }

  /**
   * Estimates the time needed to instantiate a component template.
   * 
   * @param tmpl the component template to be used for the measurements.
   * @throws Exception if a problem occurs.
   */

  private static void computeTemplateInstantiationDuration (Factory tmpl)
    throws Exception
  {
    // initializes the min, max and mean value
    double min = Long.MAX_VALUE;
    double max = Long.MIN_VALUE;
    double mean = 0;
    // makes several estimations in order to compute min, max and mean
    for (int j = 0; j < LOOPS; ++j) {
      int i = 0;
      double dt = System.currentTimeMillis();
      while (i < NB_INSTANCES_2) {
        tmpl.newFcInstance();
        ++i;
      }
      dt = (System.currentTimeMillis() - dt) / NB_INSTANCES_2;
      // the first iterations are not taken into account (warm up)
      if (j > 1) {
        // updates the min, max and mean values
        min = Math.min(dt, min);
        max = Math.max(dt, max);
        mean += dt;
      }
    }
    // computes the mean value in milli-seconds
    mean = mean / (LOOPS - 2);
    // computes the variation in micro-seconds
    double delta = 1000 * Math.max(max - mean, mean - min);
    // prints the results
    System.out.print("template instantiation  = ");
    System.out.println(format(mean) + " ms (+/- " + format(delta) + " �s)");
  }

  // -------------------------------------------------------------------------
  // Memory measurements
  // -------------------------------------------------------------------------

  /**
   * Number of instances to instantiate to estimate object and component sizes.
   */

  private final static int NB_INSTANCES_1 = 100;

  /**
   * Number of instances to estimate object and component creation durations.
   */

  private final static int NB_INSTANCES_2 = 1000;

  /**
   * Estimated size of an empty object.
   */

  private static long objectSize;

  /**
   * Computes the size of an empty object.
   */

  private static void computeEmptyObjectSize () {
    // makes a first estimate
    long oldSize;
    long newSize = estimateEmptyObjectSize();
    //System.err.println("SIZE ESTIMATE " + newSize);
    do {
      // makes a new estimate
      oldSize = newSize;
      newSize = estimateEmptyObjectSize();
      //System.err.println("SIZE ESTIMATE " + newSize);
      // loop until two consecutive estimates are equal
    } while (newSize != oldSize);
    // prints the results
    System.out.print("empty object    = ");
    System.out.println(newSize + " bytes (" + (newSize / 4) + " words)");
    // stores the result to compute the overhead of components
    // (see computeEmptyComponentSize)
    objectSize = newSize;
  }

  /**
   * Estimates the size of an empty object.
   * 
   * @return the estimated size.
   */

  private static long estimateEmptyObjectSize () {
    long min = 0;
    while (true) {
      min = freeMem(min);
      Object[] instances = new Object[NB_INSTANCES_1];
      long start = freeMem(0);
      for (int i = 0; i < instances.length; ++i) {
        instances[i] = new CObject();
      }
      long freeMem = freeMem(0);
      for (int i = 0; i < instances.length; ++i) {
        instances[i] = null;
      }
      long end = freeMem(freeMem);
      /*
      System.err.println("FREE MEM BEFORE INSTANTIATE " + start);
      System.err.println("FREE MEM AFTER INSTANTIATE " + freeMem);
      System.err.println("FREE MEM AFTER GC " + end);
      */
      if (Math.abs(end - start) < NB_INSTANCES_1) {
        return (end - freeMem) / NB_INSTANCES_1;
      }
    }
  }

  /**
   * Computes the size of an empty component.
   * 
   * @param tmpl the template to be used for the measurements.
   * @throws Exception if a problem occurs.
   */

  private static void computeEmptyComponentSize (Factory tmpl)
    throws Exception
  {
    // makes a first estimate
    long oldSize;
    long newSize = estimateEmptyComponentSize(tmpl);
    //System.err.println("SIZE ESTIMATE " + newSize);
    do {
      // makes a new estimate
      oldSize = newSize;
      newSize = estimateEmptyComponentSize(tmpl);
      //System.err.println("SIZE ESTIMATE " + newSize);
      // loop until two consecutive estimates are equal
    } while (newSize != oldSize);
    // prints the results
    System.out.print("empty component = ");
    System.out.print(newSize + " bytes (" + (newSize / 4) + " words)");
    System.out.println(" => + " + (newSize - objectSize) / 4 + " words");
  }

  /**
   * Estimates the size of an empty component.
   * 
   * @param tmpl the template to be used for the measurements.
   * @return the estimated size.
   * @throws Exception if a problem occurs. 
   */

  private static long estimateEmptyComponentSize (Factory tmpl)
    throws Exception
  {
    long min = 0;
    while (true) {
      min = freeMem(min);
      Object[] instances = new Object[NB_INSTANCES_1];
      long start = freeMem(0);
      for (int i = 0; i < instances.length; ++i) {
        instances[i] = tmpl.newFcInstance();
      }
      long freeMem = freeMem(0);
      for (int i = 0; i < instances.length; ++i) {
        instances[i] = null;
      }
      long end = freeMem(freeMem);
      /*
      System.err.println("FREE MEM BEFORE INSTANTIATE " + start);
      System.err.println("FREE MEM AFTER INSTANTIATE " + freeMem);
      System.err.println("FREE MEM AFTER GC " + end);
      */
      if (Math.abs(end - start) < NB_INSTANCES_1) {
        return (end - freeMem) / NB_INSTANCES_1;
      }
    }
  }

  /**
   * Computes the size of a component type.
   * 
   * @param type the component type whose size must be computed.
   * @throws Exception if a problem occurs. 
   */

  private static void computeComponentTypeSize (ComponentType type)
    throws Exception
  {
    // makes a first estimate
    long oldSize;
    long newSize = estimateComponentTypeSize(type);
    //System.err.println("SIZE ESTIMATE " + newSize);
    do {
      // makes a new estimate
      oldSize = newSize;
      newSize = estimateComponentTypeSize(type);
      //System.err.println("SIZE ESTIMATE " + newSize);
      // loop until two consecutive estimates are equal
    } while (newSize != oldSize);
    // prints the results
    int n = type.getFcInterfaceTypes().length;
    System.out.print("component type (" + n + " interfaces) = ");
    System.out.println(newSize + " bytes (" + (newSize / 4) + " words)");
  }

  /**
   * Estimates the size of a component type.
   * 
   * @param type the component type whose size must be estimated.
   * @return the estimated size.
   * @throws Exception if a problem occurs.
   */

  private static long estimateComponentTypeSize (ComponentType type)
    throws Exception
  {
    TypeFactory tf = Fractal.getTypeFactory(Fractal.getBootstrapComponent());
    long min = 0;
    while (true) {
      min = freeMem(min);
      Object[] instances = new Object[NB_INSTANCES_1];
      long start = freeMem(0);
      for (int i = 0; i < instances.length; ++i) {
        instances[i] = clone(tf, type);
      }
      long freeMem = freeMem(0);
      for (int i = 0; i < instances.length; ++i) {
        instances[i] = null;
      }
      long end = freeMem(freeMem);
      /*
      System.err.println("FREE MEM BEFORE INSTANTIATE " + start);
      System.err.println("FREE MEM AFTER INSTANTIATE " + freeMem);
      System.err.println("FREE MEM AFTER GC " + end);
      */
      if (Math.abs(end - start) < NB_INSTANCES_1) {
        return (end - freeMem) / NB_INSTANCES_1;
      }
    }
  }

  /**
   * Estimates the amount of free memory.
   *
   * @param min the expected minimal amount of free memory
   * @return the estimated amount of free memory.
   */

  private static long freeMem (long min) {
    Runtime rt = Runtime.getRuntime();
    // gets the amount of free memory, after a garbage collection
    rt.gc();
    long oldFreeMem;
    long newFreeMem = rt.freeMemory();
    while (true) {
      // gets the amount of free memory again, after another garbage collection
      rt.gc();
      oldFreeMem = newFreeMem;
      newFreeMem = rt.freeMemory();
      // loop until two consecutive results are equal above 'min'
      // if (newFreeMem == oldFreeMem && newFreeMem >= min) {
      if (newFreeMem == oldFreeMem) {
        return newFreeMem;
      }
    }
  }

  // -------------------------------------------------------------------------
  // Utility methods
  // -------------------------------------------------------------------------

  /**
   * Converts the given double to a String.
   * 
   * @param d a double.
   * @return the given double as a String.
   */

  private static String format (final double d) {
    return Float.toString((float)d);
  }

  /**
   * Clones a component type.
   *
   * @param tf the type factory to be used to create the clone.
   * @param type the component type to be cloned.
   * @return a clone of the given component type.
   * @throws Exception if a problem occurs.
   */

  private static ComponentType clone (
    final TypeFactory tf,
    final ComponentType type) throws Exception
  {
    InterfaceType[] itfTypes = type.getFcInterfaceTypes();
    InterfaceType[] cloneItfTypes = new InterfaceType[itfTypes.length];
    for (int i = 0; i < itfTypes.length; ++i) {
      InterfaceType itfType = itfTypes[i];
      cloneItfTypes[i] = tf.createFcItfType(
        new String(itfType.getFcItfName().toCharArray()),
        new String(itfType.getFcItfSignature().toCharArray()),
        itfType.isFcClientItf(),
        itfType.isFcOptionalItf(),
        itfType.isFcCollectionItf());
    }
    return tf.createFcType(cloneItfTypes);
  }
}
