/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.conform;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import junit.framework.TestCase;

import org.objectweb.fractal.aokell.lib.control.binding.BindingControllerDef;
import org.objectweb.fractal.aokell.lib.control.component.ComponentItf;
import org.objectweb.fractal.aokell.lib.control.content.ContentControllerItf;
import org.objectweb.fractal.aokell.lib.control.factory.FactoryDef;
import org.objectweb.fractal.aokell.lib.control.lifecycle.LifeCycleControllerDef;
import org.objectweb.fractal.aokell.lib.control.name.NameControllerItf;
import org.objectweb.fractal.aokell.lib.control.superc.SuperControllerDef;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.conform.components.I;
import org.objectweb.fractal.util.Fractal;


/*
 * public removed to prevent the Eclipse 3.1 JUnit plugin from complaining that
 * this class does not contain any test (which is true.)
 */
class Test extends TestCase {

  protected final static String COMP = "component/"+ComponentItf.TYPE.getFcItfSignature()+"/false,false,false";
  protected final static String BC = "binding-controller/"+BindingControllerDef.TYPE.getFcItfSignature()+"/false,false,false";
  protected final static String CC = "content-controller/"+ContentControllerItf.TYPE.getFcItfSignature()+"/false,false,false";
  protected final static String NC = "name-controller/"+NameControllerItf.TYPE.getFcItfSignature()+"/false,false,false";
  protected final static String PLC = "lifecycle-controller/"+LifeCycleControllerDef.PRIMITIVE_TYPE.getFcItfSignature()+"/false,false,false";
  protected final static String CLC = "lifecycle-controller/"+LifeCycleControllerDef.COMPOSITE_TYPE.getFcItfSignature()+"/false,false,false";
  protected final static String SC = "super-controller/"+SuperControllerDef.TYPE.getFcItfSignature()+"/false,false,false";
  protected final static String F = "factory/"+FactoryDef.TYPE.getFcItfSignature()+"/false,false,false";

//  protected final static String COMP = "component/org.objectweb.fractal.api.Component/false,false,false";
//  protected final static String BC = "binding-controller/org.objectweb.fractal.api.control.BindingController/false,false,false";
//  protected final static String CC = "content-controller/org.objectweb.fractal.api.control.ContentController/false,false,false";
//  protected final static String NC = "name-controller/org.objectweb.fractal.api.control.NameController/false,false,false";
//  protected final static String LC = "lifecycle-controller/org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator/false,false,false";
//  protected final static String SC = "super-controller/org.objectweb.fractal.julia.control.content.SuperControllerNotifier/false,false,false";
//  protected final static String F = "factory/org.objectweb.fractal.julia.factory.Template/false,false,false";
  
  protected final static String PKG = "org.objectweb.fractal.julia.conform.components";

  public Test (final String name) {
    super(name);
  }

  protected void checkInterface (I i) {
    i.m(true);
    i.m((byte)1);
    i.m((char)1);
    i.m((short)1);
    i.m((int)1);
    i.m((long)1);
    i.m((float)1);
    i.m((double)1);
    i.m("1");
    i.m(new String[] { "1" });

    assertEquals(true, i.n(true, null));
    assertEquals((byte)1, i.n((byte)1, null));
    assertEquals((char)1, i.n((char)1, (double)0));
    assertEquals((short)1, i.n((short)1, (float)0));
    assertEquals((int)1, i.n((int)1, (long)0));
    assertEquals((long)1, i.n((long)1, (int)0));
    assertEquals((float)1, i.n((float)1, (short)0), 0);
    assertEquals((double)1, i.n((double)1, (char)0), 0);
    assertEquals("1", i.n("1", (byte)0));
  }

  protected void checkComponent (Component c, Set itfs) throws Exception {
    Set extItfs = getExternalItfs(c);
    assertEquals("Wrong external interface list", itfs, extItfs);
    Iterator i = itfs.iterator();
    while (i.hasNext()) {
      String itf = (String)i.next();
      String compItf = null;
      try {
          compItf = getItf((Interface)c.getFcInterface(getItfName(itf)), false);
      } catch (NoSuchInterfaceException e) {
        fail("Missing external interface: " + itf);
      }
      assertEquals("Wrong external interface", itf, compItf);
    }

    ContentController cc;
    try {
      cc = Fractal.getContentController(c);
    } catch (NoSuchInterfaceException e) {
      return;
    }

    itfs = new HashSet(itfs);
    i = itfs.iterator();
    while (i.hasNext()) {
      String itf = (String)i.next();
      if (itf.startsWith("component/") || itf.indexOf("-controller/") != -1) {
        i.remove();
      }
    }

    Set intItfs = getInternalItfs(cc);
    assertEquals("Wrong internal interface list", itfs, intItfs);
    i = itfs.iterator();
    while (i.hasNext()) {
      String itf = (String)i.next();
      String compItf = null;
      try {
        compItf = getItf((Interface)cc.getFcInternalInterface(getItfName(itf)), true);
      } catch (NoSuchInterfaceException e) {
        fail("Missing internal interface: " + itf);
      }
      assertEquals("Wrong internal interface", itf, compItf);
    }
  }

  protected Set getExternalItfs (Component c) {
    HashSet result = new HashSet();
    Object[] extItfs = c.getFcInterfaces();
    for (int i = 0; i < extItfs.length; ++i) {
      String itf = getItf((Interface)extItfs[i], false);
      if (!result.add(itf)) {
        fail("Duplicated interface: " + itf);
      }
    }
    return result;
  }

  protected Set getInternalItfs (ContentController cc) {
    HashSet result = new HashSet();
    Object[] extItfs = cc.getFcInternalInterfaces();
    for (int i = 0; i < extItfs.length; ++i) {
      String itf = getItf((Interface)extItfs[i], true);
      if (!result.add(itf)) {
        fail("Duplicated interface: " + itf);
      }
    }
    return result;
  }

  protected static String getItf (Interface itf, boolean internal) {
    InterfaceType itfType = (InterfaceType)itf.getFcItfType();
    return getItf(
      itf.getFcItfName(),
      itfType.getFcItfSignature(),
      itfType.isFcClientItf() ^ internal,
      itfType.isFcOptionalItf(),
      itfType.isFcCollectionItf());
  }

  private static String getItf (
    String name,
    String signature,
    boolean isClient,
    boolean isOptional,
    boolean isCollection)
  {
    return name+'/'+signature+'/'+isClient+','+isOptional+','+isCollection;
  }

  private static String getItfName (String itf) {
    return itf.substring(0, itf.indexOf('/'));
  }
}
