/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 * Contributor: Lionel Seinturier
 */

package org.objectweb.fractal.julia.conform;

import java.util.Arrays;
import java.util.HashSet;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.julia.conform.components.C;
import org.objectweb.fractal.julia.conform.components.I;
import org.objectweb.fractal.util.Fractal;

public class TestAutoBinding extends TestBinding {

  protected Component boot;
  protected TypeFactory tf;
  protected GenericFactory gf;

  protected ComponentType aType, bType, sType;

  // -------------------------------------------------------------------------
  // Constructor and setup
  // -------------------------------------------------------------------------

  public TestAutoBinding (final String name) {
    super(name);
  }

  protected void setUp () throws Exception {
    boot = Fractal.getBootstrapComponent();
    tf = Fractal.getTypeFactory(boot);
    gf = Fractal.getGenericFactory(boot);
    
    aType = tf.createFcType(new InterfaceType[0]);
    bType = tf.createFcType(new InterfaceType[] {
      tf.createFcItfType("client", I.class.getName(), true, true, false),
      tf.createFcItfType("clients", I.class.getName(), true, true, true)
    });
    sType = tf.createFcType(new InterfaceType[] {
      tf.createFcItfType("server", I.class.getName(), false, false, false) 
    });
  }

  // -------------------------------------------------------------------------
  // Test component instantiation
  // -------------------------------------------------------------------------

  public void testAutoBindingPrimitive () throws Exception {
    Component c = gf.newFcInstance(bType, "autoBindingPrimitive", C.class.getName());
    checkComponent(c, new HashSet(Arrays.asList(new Object[] {
      COMP, BC, PLC, SC, NC, TestCollection.clientI
    })));
  }

  public void testAutoBindingComposite () throws Exception {
    Component b = gf.newFcInstance(bType, "autoBindingComposite", null);
    checkComponent(b, new HashSet(Arrays.asList(new Object[] {
      COMP, BC, CC, CLC, SC, NC, TestCollection.clientI
    })));
  }


  // -------------------------------------------------------------------------
  // Test propagation of bindings (auto-bindings)
  // -------------------------------------------------------------------------

  public void testAutoBinding () throws Exception {

      Component aComp = gf.newFcInstance(aType, "composite", null);
      Component bComp = gf.newFcInstance(bType, "autoBindingComposite", null);
      Component cComp = gf.newFcInstance(bType, "autoBindingPrimitive", C.class.getName());
      Component s1Comp = gf.newFcInstance(sType, "primitive", C.class.getName());
      Component s2Comp = gf.newFcInstance(sType, "primitive", C.class.getName());
      Component s3Comp = gf.newFcInstance(sType, "primitive", C.class.getName());
      
      Fractal.getContentController(aComp).addFcSubComponent(bComp);
      Fractal.getContentController(aComp).addFcSubComponent(s1Comp);
      Fractal.getContentController(aComp).addFcSubComponent(s2Comp);
      Fractal.getContentController(aComp).addFcSubComponent(s3Comp);
      Fractal.getContentController(bComp).addFcSubComponent(cComp);
      
      // creates a binding between b and s1
      Fractal.getBindingController(bComp).bindFc(
        "clients-I", s1Comp.getFcInterface("server"));

      // creates a "model" binding between c and b
      // this automatically creates similar bindings bewteen c and b, 
      // for each interface in the collection in b
      Fractal.getBindingController(cComp).bindFc(
        "clients", 
        Fractal.getContentController(bComp).getFcInternalInterface("clients"));
      
      // we can check this with the following code:
      BindingController bc = Fractal.getBindingController(cComp);
      checkList( bc, new String[]{"client","clients","clients-I"} );
    
      // during the following bindings between b and si,
      // similar bindings are created between c and b (following the model binding)
      Fractal.getBindingController(bComp).bindFc(
        "clients-II", s2Comp.getFcInterface("server"));
      Fractal.getBindingController(bComp).bindFc(
        "clients-III", s3Comp.getFcInterface("server"));
       
      // we can check this with the following code:
      checkList( bc, new String[]{"client","clients","clients-I","clients-II","clients-III"} );
      
      // removing bindings between b and si also removes bindings between c and b
      Fractal.getBindingController(bComp).unbindFc("clients-I");
      Fractal.getBindingController(bComp).unbindFc("clients-II");
      Fractal.getBindingController(bComp).unbindFc("clients-III");
      
      // we can check this with the following code:
      checkList( bc, new String[]{"client","clients"} );
  }

}
