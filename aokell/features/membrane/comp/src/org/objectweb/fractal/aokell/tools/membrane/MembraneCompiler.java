/***
 * AOKell
 * Copyright (C) 2005 INRIA, France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.tools.membrane;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.aokell.AOKellMembranes;
import org.objectweb.fractal.aokell.lib.factory.GenericFactoryImpl;
import org.objectweb.fractal.aokell.lib.factory.TypeFactoryImpl;
import org.objectweb.fractal.aokell.lib.membrane.MembraneDef;
import org.objectweb.fractal.aokell.lib.membrane.StaticFactoryItf;
import org.objectweb.fractal.aokell.lib.util.FractalHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;


/**
 * Source code generator for Java classes implementing membranes types and
 * component parts. The Java classes are obtained with a modified version of the
 * Fractal ADL Java backend.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class MembraneCompiler {
    
    /** The directory where generated files are stored. */
    private static String generatedDirname = "generated";
    
    /** The Membranes (like AOKellMembranes). */
    private static MembraneDef[] Membrane = AOKellMembranes.getMembranes();
    
    private static void usage() {
        System.out.println("java "+MembraneCompiler.class.getName()+" [-d dir] com.example.Membranes");
        System.out.print("Compile into Java source files ");
        System.out.println("the Fractal ADL definitions of control membranes");
        System.out.println("Option:");
        System.out.print("  -d dir : the directory where files will be generated");
        System.out.println("(default generated/)");
        System.out.println("  com.example.Membranes : the membrane class file");
        
    }
    
    public static void main(String[] args) throws Exception {
        if( args.length != 0 ) {
            if((! args[0].equals("-d")) || args.length < 2 || args.length > 3 ) {
                usage();
                return;
            }
            generatedDirname = args[1];
            System.out.println(Membrane.length);
            if( args.length == 3 ){
            	System.out.println("Membrane = "+args[2]);
            	Object instance = MembraneCompiler.class.getClassLoader().loadClass(args[2]).newInstance();
            	Membrane = (MembraneDef[])instance.getClass().getMethod("getMembranes", null).invoke(instance, null);
            }
        }
        System.out.println(Membrane.length);
        new MembraneCompiler(Membrane).compile();
    }
    
    /**
     * Contruct a new membrane compiler.
     * 
     * @param adls     the membrane ADL descriptions
     * @param markers  the marker types for generating component parts
     */
    public MembraneCompiler( Map adls, Map markers ) {
        this.adls = adls;
        this.markers = markers;
    }
    
    /**
     * Contruct a new membrane compiler.
     * 
     * @param mdefs  an array of membrane definitions
     */
    public MembraneCompiler( MembraneDef[] mdefs ) {
        
        adls = new HashMap();
        markers = new HashMap();
        
        for (int i = 0; i < mdefs.length; i++) {
            adls.put( mdefs[i].getControllerDesc(), mdefs[i].getAdl() );
            markers.put( mdefs[i].getControllerDesc(), mdefs[i].getType() );
        }
    }
    
    private Map adls;
    private Map markers;
    
    public void compile() throws ADLException, IOException {
        
        /*
         * Generate the membranes.
         */
        for (Iterator iter = adls.values().iterator(); iter.hasNext();) {
            String adl = (String) iter.next();
            
            if( adl != null ) {
                int lastdot = adl.lastIndexOf('.');
                String packagename = adl.substring(0,lastdot);
                String classname = adl.substring(lastdot+1);
                compileMembrane(packagename,classname);
            }
        }
        System.out.println();
        
        System.out.println("Done.");
    }
    
    protected void compileMembrane( String packagename, String classname )
    throws ADLException, IOException {
        
        String dirname = generatedDirname+"/"+packagename.replace('.','/');
        String filename = dirname+"/"+classname+".java";
        new File(dirname).mkdirs();
        System.out.println("Compiling into: "+filename);
        
        FileWriter fw = new FileWriter(filename);
        PrintWriter pw = new PrintWriter(fw);
        prologue(pw,packagename);
        prologueMembrane(pw,classname);
        
        pw.println("    // --------------------------------------------------");        
        Map context = new HashMap();
        context.put("printwriter",pw);
        // Create the Root component
        Factory f = FactoryFactory.getFactory("org.objectweb.fractal.aokell.tools.membrane.Backend");
        Object root = f.newComponent(packagename+"."+classname,context);
        pw.println("    // --------------------------------------------------");
        
        epilogueMembrane(pw,root);
        pw.close();
        fw.close();
    }
    
    protected void prologue( PrintWriter pw, String packagename )
    throws IOException {
        
        pw.println("/*");
        pw.print(" * Generated by AOKell Membrane Compiler on: ");
        pw.println(new Date().toString());
        pw.println(" */");
        pw.println("package "+packagename+";");
        pw.println();
    }
    
    protected void prologueMembrane( PrintWriter pw, String classname )
    throws IOException {
        
        pw.println("import "+Component.class.getName()+";");
        pw.println("import "+GenericFactory.class.getName()+";");
        pw.println("import "+ComponentType.class.getName()+";");
        pw.println("import "+InterfaceType.class.getName()+";");
        pw.println("import "+TypeFactory.class.getName()+";");
        pw.println("import "+GenericFactoryImpl.class.getName()+";");
        pw.println("import "+TypeFactoryImpl.class.getName()+";");
        pw.println("import "+FractalHelper.class.getName()+";");        
        pw.println();
        
        pw.print("public class "+classname);
        pw.println(" implements "+StaticFactoryItf.class.getName()+" {");
        pw.println("  public Component newFcStaticMembrane() throws Exception {");
        pw.println("    TypeFactory tf = TypeFactoryImpl.get();");
        pw.println("    GenericFactory gf = GenericFactoryImpl.get();");
    }

    protected void epilogueMembrane( PrintWriter pw, Object root )
    throws IOException {
        pw.println("    return "+root+";");
        pw.println("  }");
        pw.println("}");
    }
}