/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.tools.membrane;

import java.io.PrintWriter;
import java.util.Map;

/**
 * Implementation of the ComponentBuilder for generating AOKell membranes.
 * This implementation is based on the original one written by E. Bruneton for
 * Fractal ADL.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class ComponentBuilder
    implements org.objectweb.fractal.adl.components.ComponentBuilder {

  // --------------------------------------------------------------------------
  // Implementation of the ComponentBuilder interface
  // --------------------------------------------------------------------------

  public void addComponent (
    final Object superComponent, 
    final Object subComponent, 
    final String name,
    final Object context) 
  {
      PrintWriter pw = (PrintWriter)((Map)context).get("printwriter");
      pw.print("FractalHelper.getContentController(");
      pw.print(superComponent);
      pw.print(").addFcSubComponent(");
      pw.print(subComponent);
      pw.println(");");
      
      pw.print("FractalHelper.getSuperController(");
      pw.print(subComponent);
      pw.print(").addedToFc(");
      pw.print(superComponent);
      pw.println(");");
  }

  public void startComponent (final Object component, final Object context) {
      // does nothing
  }
}
