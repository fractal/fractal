/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.tools.membrane;

import java.io.PrintWriter;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Implementation of the TypeBuilder for generating AOKell membranes.
 * This implementation is based on the original one written by E. Bruneton for
 * Fractal ADL.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class TypeBuilder
    implements org.objectweb.fractal.adl.types.TypeBuilder {

  // --------------------------------------------------------------------------
  // Implementation of the TypeBuilder interface
  // --------------------------------------------------------------------------
  
    private Map itfCounters = new WeakHashMap();
    
    private Map compCounters = new WeakHashMap();
    
    // --------------------------------------------------------------------------
    // Implementation of the TypeBuilder interface
    // --------------------------------------------------------------------------
    
    public Object createInterfaceType (
      final String name,
      final String signature,
      final String role,
      final String contingency,
      final String cardinality, 
      final Object context) throws Exception
    {
      Integer i = (Integer)itfCounters.get(context);
      if (i == null) {
        i = new Integer(0);
      }
      itfCounters.put(context, new Integer(i.intValue() + 1));
      String id = "IT" + i;
      
      PrintWriter pw = (PrintWriter)((Map)context).get("printwriter");
      pw.print("InterfaceType ");
      pw.print(id);
      pw.print(" = tf.createFcItfType(\"");
      pw.print(name);
      pw.print("\", \"");
      pw.print(signature);
      pw.print("\", ");
      pw.print("client".equals(role));
      pw.print(", ");
      pw.print("optional".equals(contingency));
      pw.print(", ");
      pw.print("collection".equals(cardinality));
      pw.println(");");
      
      return id;
    }

    public Object createComponentType (
      final String name,
      final Object[] interfaceTypes, 
      final Object context) throws Exception 
    {
      Integer i = (Integer)compCounters.get(context);
      if (i == null) {
        i = new Integer(0);
      }
      compCounters.put(context, new Integer(i.intValue() + 1));
      String id = "CT" + i;
      
      PrintWriter pw = (PrintWriter)((Map)context).get("printwriter");  
      pw.print("ComponentType ");
      pw.print(id);
      pw.print(" = tf.createFcType(new InterfaceType [] { ");
      for (int j = 0; j < interfaceTypes.length; ++j) {
        if (j > 0) {
          pw.print(", ");
        }
        pw.print(interfaceTypes[j]);
      }
      pw.println(" });");
      return id;
    }
}
