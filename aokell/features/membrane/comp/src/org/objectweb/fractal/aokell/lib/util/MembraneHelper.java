/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.util;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.aokell.lib.control.component.ComponentItf;
import org.objectweb.fractal.aokell.lib.control.content.ContentControllerItf;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.julia.control.content.SuperControllerNotifier;


/**
 * This class some feature-dependant helper methods.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class MembraneHelper {

    /**
     * Return the component controller implementation associated to the given
     * component.
     */
    public static Component getFcCompCtrlImpl( Component comp ) {
        
        Interface itf =
            (Interface) FractalHelper.getFcInterface(comp,"//component");
        Interface last = FractalHelper.followBindingsUpToLastInterface(itf);
        Component c = last.getFcItfOwner();
        Object content = FractalHelper.getContent(c);
        
        return (Component) content;
    }

    /**
     * Return the controller implementations associated to the given component.
     */
    public static Object[] getFcControllerImpls( Component comp ) {
        
        Object[] ctrls = ((ComponentItf)comp).getFcControllers();
        List impls = new ArrayList();
        for (int i = 0; i < ctrls.length; i++) {
            if( ctrls[i] instanceof Interface ) {
                Interface itf =
                    FractalHelper.followBindingsUpToLastInterface(
                            (Interface) ctrls[i] );
                Component c = itf.getFcItfOwner();
                Object content = FractalHelper.getContent(c);
                impls.add(content);
            }
        }
        
        return impls.toArray();       
    }

    /**
     * Return the component controller reference associated to the given
     * component.
     */
    public static ComponentItf getFcCompCtrl( Component comp ) {
        return (ComponentItf) FractalHelper.getFcInterface(comp,"//component");
    }

    /**
     * Return the binding controller reference associated to the given
     * component.
     */
    public static BindingController getFcBindingCtrl( Component comp ) {
        return (BindingController) FractalHelper.getFcInterface(comp,"//binding-controller");
    }

    /**
     * Return the content controller reference associated to the given
     * component.
     */
    public static ContentControllerItf getFcContentCtrl( Component comp ) {
        return (ContentControllerItf) FractalHelper.getFcInterface(comp,"//content-controller");
    }

    /**
     * Return the lifecycle controller reference associated to the given
     * component.
     */
    public static LifeCycleController getFcLifeCycleCtrl( Component comp ) {
        return (LifeCycleController) FractalHelper.getFcInterface(comp,"//lifecycle-controller");
    }

    /**
     * Return the super controller reference associated to the given component.
     */
    public static SuperControllerNotifier getFcSuperCtrl( Component comp ) {
        return (SuperControllerNotifier) FractalHelper.getFcInterface(comp,"//super-controller");
    }
    
    /**
     * Return the content associated to the given component.
     * With componentized membranes, the given component is a meta-component
     * (i.e. component instance associated to a control-component). The return
     * value is the content (M0 instance) controlled by the control-component.
     */
    public static Object getFcContent( Component comp ) {
        return FractalHelper.getFcInterface(comp,"///content");
    }
}
