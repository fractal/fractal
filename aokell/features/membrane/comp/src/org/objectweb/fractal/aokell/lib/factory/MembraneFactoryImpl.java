/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.factory;

import org.objectweb.fractal.aokell.Membranes;
import org.objectweb.fractal.aokell.lib.control.Controller;
import org.objectweb.fractal.aokell.lib.control.component.ComponentItf;
import org.objectweb.fractal.aokell.lib.control.content.ContentControllerItf;
import org.objectweb.fractal.aokell.lib.membrane.MCompositeImpl;
import org.objectweb.fractal.aokell.lib.membrane.MPrimitiveImpl;
import org.objectweb.fractal.aokell.lib.membrane.MembraneDef;
import org.objectweb.fractal.aokell.lib.type.InterfaceTypeImpl;
import org.objectweb.fractal.aokell.lib.util.FractalHelper;
import org.objectweb.fractal.aokell.lib.util.MembraneHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.InterfaceType;


/**
 * The singleton implementation of the membrane factory.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class MembraneFactoryImpl implements MembraneFactoryItf {
    
    private MembraneFactoryImpl() {}
    private static MembraneFactoryItf singleton = new MembraneFactoryImpl();
    public static MembraneFactoryItf get() { return singleton; }
    

    /**
     * Create a new control membrane.
     */
    public Component newFcMembrane(
            Type type, Object controllerDesc, Object contentDesc,
            Object content )
    throws InstantiationException {
        
        /*
         * Control membrane for controllers, i.e. components associated to the
         * mPrimitive and mComposite controller descriptions.
         */
        if( controllerDesc.equals("mPrimitive") ) {
            Component ctrlctrl = new MPrimitiveImpl(type,content);
            
            if( !(content instanceof Controller) ) {
                throw new InstantiationException(
                        "Component-controller class "+
                        content.getClass().getName()+" should implement "+
                        Controller.class.getName() );
            }
            
            // Set the reference to the component-controller
            Controller ctrl = (Controller)content;
            ctrl.setFcCompCtrl(ctrlctrl);
            
            return ctrlctrl;
        }
        if( controllerDesc.equals("mComposite") ) {
            return new MCompositeImpl(type,content);
        }
        
        /*
         * Control membrane for regular components.
         * 
         * Instantiate the control membrane which is a composite associated to
         * the given controller description.
         */
        MembraneDef mdef = Membranes.get().getMembraneDef(controllerDesc);
        Component membrane = mdef.newFcStaticMembrane();
        
        /*
         * Retrieve the component controller from the control membrane and
         * initialize it. The purpose of the initFc method is to initialize the
         * management of the component external interfaces.
         * 
         * In the case of composite component, the content controller is in
         * charge of the internal interfaces. Initialize it also. 
         */
        ComponentItf compctrlimpl =
            (ComponentItf) MembraneHelper.getFcCompCtrlImpl(membrane);
        compctrlimpl.initFc(type,controllerDesc,contentDesc,content);
        
        ContentControllerItf cc = null;
        try {
            cc = MembraneHelper.getFcContentCtrl(membrane);
            cc.initFc(type,controllerDesc,contentDesc,compctrlimpl,content);
        }
        catch( RuntimeException re ) {
            // No content controller in this membrane. This is not a composite.
        }
        
        /*
         * Initialize controllers.
         */
        Component[] subs = FractalHelper.getAllSubComponents(membrane);
        for (int i = 0; i < subs.length; i++) {
            Object subcontent = FractalHelper.getContent(subs[i]);
            if( subcontent != null ) {
                // subcontent may be null for composites
                if( subcontent instanceof Controller ) {
                    Controller ctrl = (Controller)subcontent;
                    ctrl.initFcCtrl();
                }                
            }
        }
        
        /*
         * Initialize the list of control interfaces implemented by this
         * membrane.
         */
        Object[] itfs = membrane.getFcInterfaces();
        for (int i = 0; i < itfs.length; i++) {
            Interface itf = (Interface) itfs[i];
            
            if( itf.getFcItfName().startsWith("//") ) {
                
                InterfaceType it = (InterfaceType) itf.getFcItfType();
                InterfaceTypeImpl itused =
                    new InterfaceTypeImpl(
                            it.getFcItfName().substring(2),   // remove //
                            it.getFcItfSignature(),
                            it.isFcClientItf(),
                            it.isFcOptionalItf(),
                            it.isFcCollectionItf() );
                
                /*
                 * Register the interfaces exported by the membrane:
                 * - with the component controller,
                 * - with the content controller (for internal interfaces).
                 * 
                 * These interfaces are register with a type where the heading
                 * // has been removed from the name. For example,
                 * //binding-controller is registered under binding-controller
                 * which is the usual name used at level 0.
                 * 
                 * Caution: itf.getFcItfName() == "//"+itused.getFcItfName()
                 */
                compctrlimpl.addFcController(new InterfaceType[]{itused},itf);
                if( cc != null ) {
                    cc.addFcController(new InterfaceType[]{itused},itf);
                }
            }
        }
        
        return (Component) compctrlimpl;
    }
    
    /**
     * Return true if the content class associated to the given controller
     * description must implement the BindingController interface when its type
     * defines at least one client interface. 
     */
    public boolean checkFcContentForBC( Object controllerDesc ) {
        if( controllerDesc.equals("mPrimitive") ||
            controllerDesc.equals("mComposite") )
            return false;
        
        return true;
    }

}
