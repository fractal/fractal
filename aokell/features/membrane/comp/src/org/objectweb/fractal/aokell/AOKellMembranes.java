/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell;

import org.objectweb.fractal.aokell.lib.membrane.MembraneDef;
import org.objectweb.fractal.aokell.lib.membrane.composite.CompositeTemplateType;
import org.objectweb.fractal.aokell.lib.membrane.composite.CompositeType;
import org.objectweb.fractal.aokell.lib.membrane.composite.ParametricCompositeTemplateType;
import org.objectweb.fractal.aokell.lib.membrane.composite.ParametricCompositeType;
import org.objectweb.fractal.aokell.lib.membrane.flat.FlatTemplateType;
import org.objectweb.fractal.aokell.lib.membrane.flat.FlatType;
import org.objectweb.fractal.aokell.lib.membrane.flat.ParametricFlatTemplateType;
import org.objectweb.fractal.aokell.lib.membrane.flat.ParametricFlatType;
import org.objectweb.fractal.aokell.lib.membrane.marker.BaseType;
import org.objectweb.fractal.aokell.lib.membrane.primitive.ParametricPrimitiveTemplateType;
import org.objectweb.fractal.aokell.lib.membrane.primitive.ParametricPrimitiveType;
import org.objectweb.fractal.aokell.lib.membrane.primitive.PrimitiveTemplateType;
import org.objectweb.fractal.aokell.lib.membrane.primitive.PrimitiveType;


/**
 * The class defines the membranes supported by AOKell.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class AOKellMembranes {

    /**
     * Return the membranes supported by AOKell.
     */
    public static MembraneDef[] getMembranes() {
        return MEMBRANES;
    }
    
    /** AOKell membranes. */
    final public static MembraneDef[] MEMBRANES =
        new MembraneDef[]{
            new MembraneDef(
                    "bootstrap",
                    "org.objectweb.fractal.aokell.lib.membrane.Base",
                    BaseType.class),
            
            new MembraneDef(
                    "flatPrimitive",
                    "org.objectweb.fractal.aokell.lib.membrane.flat.Flat",
                    FlatType.class),
            new MembraneDef(
                    "primitive",
                    "org.objectweb.fractal.aokell.lib.membrane.primitive.Primitive",
                    PrimitiveType.class),
            new MembraneDef(
                    "composite",
                    "org.objectweb.fractal.aokell.lib.membrane.composite.Composite",
                    CompositeType.class),

            // ----------
            // Parametric
            // ----------
            new MembraneDef(
                    "flatParametricPrimitive",
                    "org.objectweb.fractal.aokell.lib.membrane.flat.Flat",
                    ParametricFlatType.class),
            new MembraneDef(
                    "parametricPrimitive",
                    "org.objectweb.fractal.aokell.lib.membrane.primitive.Primitive",
                    ParametricPrimitiveType.class),
            new MembraneDef(
                    "parametricComposite",
                    "org.objectweb.fractal.aokell.lib.membrane.composite.Composite",
                    ParametricCompositeType.class),

            // --------
            // Template
            // --------
            new MembraneDef(
                    "flatPrimitiveTemplate",
                    "org.objectweb.fractal.aokell.lib.membrane.flat.FlatTemplate",
                    FlatTemplateType.class),
            new MembraneDef(
                    "primitiveTemplate",
                    "org.objectweb.fractal.aokell.lib.membrane.primitive.PrimitiveTemplate",
                    PrimitiveTemplateType.class),
            new MembraneDef(
                    "compositeTemplate",
                    "org.objectweb.fractal.aokell.lib.membrane.composite.CompositeTemplate",
                    CompositeTemplateType.class),

            // -------------------
            // Parametric template
            // -------------------
            new MembraneDef(
                    "flatParametricPrimitiveTemplate",
                    "org.objectweb.fractal.aokell.lib.membrane.flat.ParametricFlatTemplate",
                    ParametricFlatTemplateType.class),
            new MembraneDef(
                    "parametricPrimitiveTemplate",
                    "org.objectweb.fractal.aokell.lib.membrane.primitive.ParametricPrimitiveTemplate",
                    ParametricPrimitiveTemplateType.class),
            new MembraneDef(
                    "parametricCompositeTemplate",
                    "org.objectweb.fractal.aokell.lib.membrane.composite.ParametricCompositeTemplate",
                    ParametricCompositeTemplateType.class),
            
            // ------------
            // Auto-binding
            // ------------
            new MembraneDef(
                    "autoBindingPrimitive",
                    "org.objectweb.fractal.aokell.lib.membrane.primitive.AutoBindingPrimitive",
                    PrimitiveType.class),
            new MembraneDef(
                    "autoBindingComposite",
                    "org.objectweb.fractal.aokell.lib.membrane.composite.AutoBindingComposite",
                    CompositeType.class),
                            
            // -------------------------------------------
            // Membrane definitions for control components
            // -------------------------------------------
            new MembraneDef("mPrimitive",null,null),
            new MembraneDef("mComposite",null,null)
        };
    
}
