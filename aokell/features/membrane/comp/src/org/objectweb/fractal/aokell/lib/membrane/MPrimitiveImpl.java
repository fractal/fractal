/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.membrane;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.aokell.lib.interf.Delegator;
import org.objectweb.fractal.aokell.lib.type.InterfaceTypeImpl;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.control.content.SuperControllerNotifier;


/**
 * Implementation of the control membrane for mprimitive components which are
 * primitive control components. MPrimitiveImpl implements thus a level of
 * control for controllers.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class MPrimitiveImpl
    implements
        Component, BindingController, NameController, SuperControllerNotifier {
    
    private Type type;
    private Object content;
    
    public MPrimitiveImpl( Type type, Object content ) {
        
        this.type = type;
        this.content = content;
                
        ComponentType ct = (ComponentType) type;
        InterfaceType[] its = ct.getFcInterfaceTypes();
        for (int i = 0; i < its.length; i++) {
            Object itf =
                Delegator.generate(its[i],this,content,false,isBoundable(its[i]));
            fcInterfaces.add(itf);
        }
        
        fcInterfaces.add( Delegator.generate(COMPONENT_TYPE,this,this,false,false) );
        fcInterfaces.add( Delegator.generate(BINDING_TYPE,this,this,false,false) );
        fcInterfaces.add( Delegator.generate(NAME_TYPE,this,this,false,false) );
        fcInterfaces.add( Delegator.generate(SUPER_TYPE,this,this,false,false) );
    }
    
    protected boolean isBoundable( InterfaceType it ) {
        return it.isFcClientItf();
    }
    
    
    // -----------------------------------------------------------------
    // Implementation of the Component interface
    // -----------------------------------------------------------------
    
    final public static InterfaceType COMPONENT_TYPE =
        new InterfaceTypeImpl(
                "component",
                Component.class.getName(),
                false, false, false);
    
    public Type getFcType() {
        return type;
    }

    public Object getFcInterface(String interfaceName)
    throws NoSuchInterfaceException {
        
        for (Iterator iter = fcInterfaces.iterator(); iter.hasNext();) {
            Interface itf = (Interface) iter.next();
            if( itf.getFcItfName().equals(interfaceName) ) {
                return itf;
            }
        }
        
        // TODO support dynamically created collection interface instances
        
        if( interfaceName.equals("/content") ) {
            return content;
        }
        
        /*
         * Delegate the query to the component controller.
         * Needed for hidden interfaces (e.g. ///content ou ///desc).
         */
        if( ! interfaceName.equals("//component") ) {
            Component compctrl = (Component) getFcInterface("//component");
            return compctrl.getFcInterface(interfaceName.substring(2));
        }
        
        throw new NoSuchInterfaceException(interfaceName);
    }
    
    public Object[] getFcInterfaces() {        
        return fcInterfaces.toArray();
    }
    
    protected List fcInterfaces = new ArrayList();
    
    private Set getFcSingletonClientItfTypeNames() {
        if( singletonClientItfTypeNames == null ) {
            singletonClientItfTypeNames = new HashSet();
            InterfaceType[] its = ((ComponentType)type).getFcInterfaceTypes();
            for (int i = 0; i < its.length; i++) {
                if( its[i].isFcClientItf() && ! its[i].isFcCollectionItf() ) {
                    singletonClientItfTypeNames.add(its[i].getFcItfName());
                }
            }
        }
        return singletonClientItfTypeNames;
    }
    
    private Set getFcCollectionClientItfTypeNames() {
        if( collectionClientItfTypeNames == null ) {
            collectionClientItfTypeNames = new HashSet();
            InterfaceType[] its = ((ComponentType)type).getFcInterfaceTypes();
            for (int i = 0; i < its.length; i++) {
                if( its[i].isFcClientItf() && its[i].isFcCollectionItf() ) {
                    collectionClientItfTypeNames.add(its[i].getFcItfName());
                }
            }
        }
        return collectionClientItfTypeNames;
    }
    
    private Set singletonClientItfTypeNames;
    private Set collectionClientItfTypeNames;
    
    
    // -----------------------------------------------------------------
    // Implementation of the BindingController interface
    // -----------------------------------------------------------------
    
    final public static InterfaceType BINDING_TYPE =
        new InterfaceTypeImpl(
                "binding-controller",
                BindingController.class.getName(),
                false, false, false);
    
    // key: String interfaceName / value: Object bound interface
    private Map fcBindings = new HashMap();
    
    public String[] listFc() {
        
        // Singleton client interfaces
        Set names = new HashSet();
        names.addAll( getFcSingletonClientItfTypeNames() );
        
        // Collection client interfaces
        Set col = getFcCollectionClientItfTypeNames();
        for (Iterator iter = col.iterator(); iter.hasNext();) {
            String arg0 = (String) iter.next();
            for (Iterator iter2 = fcBindings.entrySet().iterator(); iter2.hasNext();) {
                Map.Entry entry = (Map.Entry) iter2.next();
                String itfName = (String) entry.getKey();
                if( itfName.startsWith(arg0) ) {
                    names.add( itfName );
                }
            }
        }

        return (String[]) names.toArray( new String[names.size()] );
    }
    
    public Object lookupFc(String arg0) throws NoSuchInterfaceException {
        
        // Singleton client interfaces
        if( fcBindings.containsKey(arg0) ) {
            return fcBindings.get(arg0);
        }
        
        // Collection client interfaces
        Set col = getFcCollectionClientItfTypeNames();
        if( col.contains(arg0) ) {
            List bound = new ArrayList();
            for (Iterator iter = fcBindings.entrySet().iterator(); iter.hasNext();) {
                Map.Entry entry = (Map.Entry) iter.next();
                String itfName = (String) entry.getKey();
                if( itfName.startsWith(arg0) ) {
                    bound.add( entry.getValue() );
                }
            }
            return bound;
        }
        
        throw new NoSuchInterfaceException(arg0);
    }
    
    public void bindFc(String arg0, Object arg1)
    throws NoSuchInterfaceException, IllegalBindingException,
    IllegalLifeCycleException {
        fcBindings.put(arg0,arg1);
    }
    
    public void unbindFc(String arg0)
    throws NoSuchInterfaceException, IllegalBindingException,
    IllegalLifeCycleException {
        fcBindings.remove(arg0);
    }
    
    
    // -----------------------------------------------------------------
    // Implementation of the NameController interface
    // -----------------------------------------------------------------
    
    final public static InterfaceType NAME_TYPE =
        new InterfaceTypeImpl(
                "name-controller",
                NameController.class.getName(),
                false, false, false);
    
    private String fcName;
    
    public String getFcName() {
        return fcName;
    }

    public void setFcName(String arg0) {
        fcName = arg0;
    }
    
    
    // -----------------------------------------------------------------
    // Implementation of the SuperControllerNotifier interface
    // -----------------------------------------------------------------
    
    final public static InterfaceType SUPER_TYPE =
        new InterfaceTypeImpl(
                "super-controller",
                SuperControllerNotifier.class.getName(),
                false, false, false);
    
    protected List fcSupers = new ArrayList();
    
    public Component[] getFcSuperComponents() {
        return (Component[]) fcSupers.toArray(new Component[fcSupers.size()]);
    }

    public void addedToFc( Component c ) {
        fcSupers.add(c);
    }
    
    public void removedFromFc( Component c ) {
        fcSupers.remove(c);
    }
}
