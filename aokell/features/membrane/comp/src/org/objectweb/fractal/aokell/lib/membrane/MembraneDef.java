/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.membrane;

import org.objectweb.fractal.aokell.lib.util.PlatformHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.InstantiationException;


/**
 * Class holding data for defining a membrane.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class MembraneDef {
    
    /** The controller description (e.g. primitive, composite). */
    private Object controllerdesc;
    
    /** The fully qualified name of the membrane ADL definition. */
    private String adl;
    
    /** The Java marker type for the membrane (must be an Java interface). */
    private Class type;
    
    public MembraneDef(Object controllerdesc, String adl, Class type) {
        this.controllerdesc = controllerdesc;
        this.adl = adl;
        this.type = type;
    }
    
    public Object getControllerDesc() {
        return controllerdesc;
    }
    
    public String getAdl() {
        return adl;
    }
    
    public Class getType() {
        return type;
    }
    
    private StaticFactoryItf adlsf;

    /**
     * Return a new instance of this membrane, i.e. create an instance of the
     * composite reference by the field adl.
     */
    public Component newFcStaticMembrane() throws InstantiationException {
        
        try {
            if( adlsf == null ) {
                Class adlcl = PlatformHelper.loadClass(adl);
                if( ! StaticFactoryItf.class.isAssignableFrom(adlcl) ) {
                    throw new Exception(
                            adlcl.getName()+" should implement "+
                            StaticFactoryItf.class.getName());
                }
                adlsf = (StaticFactoryItf) adlcl.newInstance();
            }
        
            return adlsf.newFcStaticMembrane();
        }
        catch( Exception e ) {
            e.printStackTrace();
            throw new InstantiationException(e.getMessage());
        }
    }
    
}
