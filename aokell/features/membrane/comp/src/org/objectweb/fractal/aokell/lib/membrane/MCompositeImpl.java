/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.membrane;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.aokell.lib.interf.Delegator;
import org.objectweb.fractal.aokell.lib.type.InterfaceTypeImpl;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.type.InterfaceType;


/**
 * Implementation of the control membrane for mcomposite components which are
 * composite control components. MCompositeImpl implements thus a level of
 * control for controllers.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class MCompositeImpl
    extends MPrimitiveImpl
    implements ContentController {
    
    public MCompositeImpl( Type type, Object content ) {
        
        super(type,content);

        fcInterfaces.add( Delegator.generate(CONTENT_TYPE,this,this,false,false) );
    }
    
    protected boolean isBoundable( InterfaceType it ) {
        // TODO manage composite with a content
        return true;
    }
    
    
    // -----------------------------------------------------------------
    // Implementation of the ContentController interface
    // -----------------------------------------------------------------

    final public static InterfaceType CONTENT_TYPE =
        new InterfaceTypeImpl(
                "content-controller",
                ContentController.class.getName(),
                false, false, false);
    
    private List fcContent = new ArrayList();
    
    public Object[] getFcInternalInterfaces() {
        throw new UnsupportedOperationException();
    }
    
    public Object getFcInternalInterface(String interfaceName)
    throws NoSuchInterfaceException {
        throw new UnsupportedOperationException();
    }

    public Component[] getFcSubComponents() {
        return (Component[]) fcContent.toArray(new Component[fcContent.size()]);
    }

    public void addFcSubComponent(Component arg0)
    throws IllegalContentException, IllegalLifeCycleException {
        fcContent.add(arg0);
        
        if( arg0 instanceof MPrimitiveImpl ) {
            ((MPrimitiveImpl)arg0).fcSupers.add(this);
        }
    }
    
    public void removeFcSubComponent(Component arg0)
    throws IllegalContentException, IllegalLifeCycleException {
        fcContent.remove(arg0);
    }
}
