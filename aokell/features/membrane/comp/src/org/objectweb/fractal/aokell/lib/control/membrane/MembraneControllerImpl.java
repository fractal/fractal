/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.control.membrane;

import org.objectweb.fractal.aokell.lib.control.CloneCtrlException;
import org.objectweb.fractal.aokell.lib.control.Controller;
import org.objectweb.fractal.aokell.lib.util.FractalHelper;
import org.objectweb.fractal.api.Component;


/**
 * Implementation of the name controller.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class MembraneControllerImpl
    implements MembraneControllerItf, Controller {

    public MembraneControllerImpl() {}
    
    // --------------------------------------------------------------
    // MembraneControllerItf interface
    // --------------------------------------------------------------
    
    public Component getFcMembrane() {
        return FractalHelper.getTopMostComponent(compctrl);
    }


    // --------------------------------------------------------------
    // Controller implementation
    // --------------------------------------------------------------
    
    private Component compctrl;
    
    /**
     * Set the reference towards the component controller associated to this
     * controller.
     */
    public void setFcCompCtrl( Component compctrl ) {
        this.compctrl = compctrl;
    }
    
    /**
     * Initialize the controller.
     */
    public void initFcCtrl() {
        // Indeed nothing
    }

    /**
     * Clone the controller state from the current component to another one.
     * This method may receive some hints on how to do this, or provide some
     * hints on how this has been done. For instance, the hints may be a map
     * that is read and/or written by the controller. The raison d'etre of
     * these hints is that when its state is cloned, a controller may produce
     * results that are needed by other controllers.
     * 
     * @param dst    the destination component
     * @param hints  hints for performing the operation
     */
    public void cloneFcCtrl( Component dst, Object hints )
    throws CloneCtrlException {
        // Indeed nothing
    }
}
