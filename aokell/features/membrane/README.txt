This directory contains the implementation of the membrane feature. This
feature deals with the way membrane are implemented.

Two versions exist:
- oo (default): membranes are implemented with Java objects
- comp: membranes are implemented with Fractal components