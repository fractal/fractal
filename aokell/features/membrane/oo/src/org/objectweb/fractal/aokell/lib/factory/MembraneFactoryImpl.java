/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.factory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.objectweb.fractal.aokell.Membranes;
import org.objectweb.fractal.aokell.lib.control.Controller;
import org.objectweb.fractal.aokell.lib.control.component.ComponentItf;
import org.objectweb.fractal.aokell.lib.control.content.ContentControllerItf;
import org.objectweb.fractal.aokell.lib.membrane.ControllerDef;
import org.objectweb.fractal.aokell.lib.membrane.MembraneDef;
import org.objectweb.fractal.aokell.lib.type.ComponentTypeImpl;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;


/**
 * The singleton implementation of the membrane factory.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class MembraneFactoryImpl implements MembraneFactoryItf {
    
    private MembraneFactoryImpl() {}
    private static MembraneFactoryItf singleton = new MembraneFactoryImpl();
    public static MembraneFactoryItf get() { return singleton; }
    

    /**
     * Create a new control membrane.
     */
    public Component newFcMembrane(
            Type type, Object controllerDesc, Object contentDesc,
            Object content )
    throws InstantiationException {
        
        /*
         * Instantiate controllers.
         */
        Controller[] ctrls = newFcControllers(controllerDesc);
        
        /*
         * Retrieve the component controller and initialize it.
         * The purpose of the initFc method is to initialize the management of
         * the component external interfaces.
         * 
         * In the case of composite component, the content controller is in
         * charge of the internal interfaces. Initialize it also. 

         */
        ComponentItf compctrl = null;
        ContentControllerItf cc = null;
        
        for (int i = 0; i < ctrls.length; i++) {
            if( ctrls[i] instanceof ComponentItf ) {
                compctrl = (ComponentItf) ctrls[i];
            }
            else if( ctrls[i] instanceof ContentControllerItf ) {
                cc = (ContentControllerItf) ctrls[i];
            }
        }
        
        if( compctrl == null ) {
            throw new InstantiationException(
                    "Unexpected error. compctrl shouldn't be null.");
        }
        
        /*
         * The type of the component is obtained by concatening business and
         * control interface types.
         */
        ComponentType busct = (ComponentType) type;
        InterfaceType[] busits = busct.getFcInterfaceTypes();
        InterfaceType[] ctrlits = getFcControllerTypes(controllerDesc);
        List all = new ArrayList();
        all.addAll( Arrays.asList(busits) );
        for (int i = 0; i < ctrlits.length; i++) {
            String ctrlitname = ctrlits[i].getFcItfName();
            if( ctrlitname.charAt(0) != '/' ) {
                try {
                    busct.getFcInterfaceType(ctrlitname);
                }
                catch( NoSuchInterfaceException nsie ) {
                    // Only add if the interface type does not already exist
                    all.add(ctrlits[i]);
                }
            }
        }
        InterfaceType[] its = (InterfaceType[])
            all.toArray( new InterfaceType[all.size()] );
        ComponentType ct = new ComponentTypeImpl(its);
        
        /*
         * If cc is null, the component is composite and this is not an error.
         */
        compctrl.initFc(ct,controllerDesc,contentDesc,content);
        if( cc != null ) {
            cc.initFc(ct,controllerDesc,contentDesc,compctrl,content);
        }
        
        /*
         * Initialize controllers.
         */
        initFcControllers(controllerDesc,ctrls,compctrl,cc);
        
        return compctrl;
    }

    
    /**
     * Return true if the content class associated to the given controller
     * description must implement the BindingController interface when its type
     * defines at least one client interface. 
     */
    public boolean checkFcContentForBC( Object controllerDesc ) {
        // Always true for AOKell with pure object-oriented membranes
        return true;
    }

    
    // -----------------------------------------------------------------
    // Implementation specific
    // -----------------------------------------------------------------
    
    private Controller[] newFcControllers( Object controllerDesc )
    throws InstantiationException {
        
        Membranes mr = Membranes.get();
        MembraneDef mdef = mr.getMembraneDef(controllerDesc);
        ControllerDef[] ctrldefs = mdef.getCtrls();
        Controller[] ctrls = new Controller[ ctrldefs.length ];
        
        for (int i = 0; i < ctrldefs.length; i++) {
            Class ctrlcl = ctrldefs[i].getImpl();
            try {
                ctrls[i] = (Controller) ctrlcl.newInstance();
            }
            catch( Exception e ) {
                String msg = e.getClass().getName()+": "+e.getMessage();
                throw new InstantiationException(msg);
            }
        }
        
        return ctrls;
    }
    
    private InterfaceType[] getFcControllerTypes( Object controllerDesc )
    throws InstantiationException {
        
        Membranes mr = Membranes.get();
        MembraneDef mdef = mr.getMembraneDef(controllerDesc);
        ControllerDef[] ctrldefs = mdef.getCtrls();
        List types = new ArrayList();
        
        for (int i = 0; i < ctrldefs.length; i++) {
            types.addAll( Arrays.asList(ctrldefs[i].getTypes()) );
        }
        
        InterfaceType[] res = (InterfaceType[])
            types.toArray( new InterfaceType[types.size()] );
        return res;
    }
    
    /**
     * Initialize the controllers.
     */
    private void initFcControllers(
            Object controllerDesc, Controller[] ctrls,
            ComponentItf compctrl, ContentControllerItf cc )
    throws InstantiationException {
        
        Membranes mr = Membranes.get();
        MembraneDef mdef = mr.getMembraneDef(controllerDesc);
        ControllerDef[] ctrldefs = mdef.getCtrls();
        
        // Registering the controller interfaces with the component controller
        // Note: ctrls.length = ctrldefs.length
        for (int i = 0; i < ctrls.length; i++) {
            compctrl.addFcController(ctrldefs[i].getTypes(),ctrls[i]);
            if( cc != null ) {
                cc.addFcController(ctrldefs[i].getTypes(),ctrls[i]);
            }
        }
        
        // Initialize the controllers
        for (int i = 0; i < ctrls.length; i++) {
            ctrls[i].setFcCompCtrl(compctrl);
            ctrls[i].initFcCtrl();
        }
    }
    
}
