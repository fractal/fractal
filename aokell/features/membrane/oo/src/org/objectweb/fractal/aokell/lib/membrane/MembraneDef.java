/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.membrane;

import java.util.HashSet;
import java.util.Set;

import org.objectweb.fractal.api.type.InterfaceType;


/**
 * Class holding data for defining a membrane.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class MembraneDef {
    
    /** The controller description (e.g. primitive, composite). */
    private Object controllerdesc;
    
    /** The array of controller definitions implemented by this membrane. */
    private ControllerDef[] ctrls;
    
    /** The Java marker type for the membrane (must be an Java interface). */
    private Class type;

    public MembraneDef(
            Object controllerdesc, ControllerDef[] ctrls, Class type ) {
        
        this.controllerdesc = controllerdesc;
        this.ctrls = ctrls;
        this.type = type;
    }
    
    public Object getControllerDesc() {
        return controllerdesc;
    }
    
    public ControllerDef[] getCtrls() {
        return ctrls;
    }
    
    public Class getType() {
        return type;
    }
    
    /**
     * Register a new controller definition with this membrane.
     * 
     * @param ctrl  the controller definition to add
     * @exception IllegalArgumentException
     *      if a definition with the same controller name already exists
     */
    public void registerControllerDef( ControllerDef ctrl ) {
        
        /*
         * This shouldn't be too frequent.
         * It is bearable to deal with an array instead of a collection.
         */
        
        InterfaceType[] its = ctrl.getTypes();
        Set names = new HashSet();
        for (int i = 0; i < its.length; i++) {
            names.add( its[i].getFcItfName() );
        }

        for (int i = 0; i < ctrls.length; i++) {
            InterfaceType[] it2s = ctrls[i].getTypes();
            for (int j = 0; j < it2s.length; j++) {
                String ctrlname = it2s[j].getFcItfName();
                if( names.contains(ctrlname) ) {
                    throw new IllegalArgumentException(
                            "A definition for a "+ctrlname+
                            " controller already exist in membrane "+
                            controllerdesc );
                }
            }
        }
        
        int length = this.ctrls.length;
        ControllerDef[] ctrls = new ControllerDef[ length + 1 ];
        System.arraycopy(this.ctrls,0,ctrls,0,length);
        ctrls[length] = ctrl;
        
        this.ctrls = ctrls;
    }
    
}
