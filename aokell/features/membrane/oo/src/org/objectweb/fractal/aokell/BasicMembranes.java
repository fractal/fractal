/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell;

import org.objectweb.fractal.aokell.lib.control.attribute.AttributeControllerImpl;
import org.objectweb.fractal.aokell.lib.control.attribute.AttributeControllerItf;
import org.objectweb.fractal.aokell.lib.control.binding.BindingControllerDef;
import org.objectweb.fractal.aokell.lib.control.binding.CompositeAutoBindingControllerImpl;
import org.objectweb.fractal.aokell.lib.control.binding.CompositeBindingControllerImpl;
import org.objectweb.fractal.aokell.lib.control.binding.CompositeTemplateBindingControllerImpl;
import org.objectweb.fractal.aokell.lib.control.binding.FlatBindingControllerImpl;
import org.objectweb.fractal.aokell.lib.control.binding.FlatTemplateBindingControllerImpl;
import org.objectweb.fractal.aokell.lib.control.binding.PrimitiveAutoBindingControllerImpl;
import org.objectweb.fractal.aokell.lib.control.binding.PrimitiveBindingControllerImpl;
import org.objectweb.fractal.aokell.lib.control.binding.PrimitiveTemplateBindingControllerImpl;
import org.objectweb.fractal.aokell.lib.control.component.ComponentImpl;
import org.objectweb.fractal.aokell.lib.control.component.ComponentItf;
import org.objectweb.fractal.aokell.lib.control.component.CompositeComponentImpl;
import org.objectweb.fractal.aokell.lib.control.content.ContentControllerImpl;
import org.objectweb.fractal.aokell.lib.control.content.ContentControllerItf;
import org.objectweb.fractal.aokell.lib.control.content.TemplateContentControllerImpl;
import org.objectweb.fractal.aokell.lib.control.factory.FactoryControllerImpl;
import org.objectweb.fractal.aokell.lib.control.factory.FactoryDef;
import org.objectweb.fractal.aokell.lib.control.lifecycle.CompositeLifeCycleControllerImpl;
import org.objectweb.fractal.aokell.lib.control.lifecycle.LifeCycleControllerImpl;
import org.objectweb.fractal.aokell.lib.control.lifecycle.LifeCycleControllerDef;
import org.objectweb.fractal.aokell.lib.control.name.NameControllerImpl;
import org.objectweb.fractal.aokell.lib.control.name.NameControllerItf;
import org.objectweb.fractal.aokell.lib.control.superc.SuperControllerDef;
import org.objectweb.fractal.aokell.lib.control.superc.SuperControllerImpl;
import org.objectweb.fractal.aokell.lib.membrane.ControllerDef;
import org.objectweb.fractal.aokell.lib.membrane.MembraneDef;
import org.objectweb.fractal.aokell.lib.membrane.composite.CompositeTemplateType;
import org.objectweb.fractal.aokell.lib.membrane.composite.CompositeType;
import org.objectweb.fractal.aokell.lib.membrane.composite.ParametricCompositeTemplateType;
import org.objectweb.fractal.aokell.lib.membrane.composite.ParametricCompositeType;
import org.objectweb.fractal.aokell.lib.membrane.flat.FlatTemplateType;
import org.objectweb.fractal.aokell.lib.membrane.flat.FlatType;
import org.objectweb.fractal.aokell.lib.membrane.flat.ParametricFlatTemplateType;
import org.objectweb.fractal.aokell.lib.membrane.flat.ParametricFlatType;
import org.objectweb.fractal.aokell.lib.membrane.marker.BaseType;
import org.objectweb.fractal.aokell.lib.membrane.primitive.ParametricPrimitiveTemplateType;
import org.objectweb.fractal.aokell.lib.membrane.primitive.ParametricPrimitiveType;
import org.objectweb.fractal.aokell.lib.membrane.primitive.PrimitiveTemplateType;
import org.objectweb.fractal.aokell.lib.membrane.primitive.PrimitiveType;
import org.objectweb.fractal.api.type.InterfaceType;


/**
 * This class defines the basic membranes which are defined by Julia.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class BasicMembranes {

    /**
     * Return the definitions of membranes supported by AOKell.
     */
    public static MembraneDef[] getMembranes() {
        return MEMBRANES;
    }

    
    // -------------------------------------------------
    // Component controller definition
    // -------------------------------------------------
    
    final public static ControllerDef componentControllerDef = 
        new ControllerDef(
            ComponentItf.TYPE,
            ComponentImpl.class
        );

    final public static ControllerDef compositeComponentControllerDef = 
        new ControllerDef(
            ComponentItf.TYPE,
            CompositeComponentImpl.class
        );
    
    
    // -------------------------------------------------
    // Binding controller definition
    // -------------------------------------------------
    
    final public static ControllerDef flatBindingControllerDef = 
        new ControllerDef(
                BindingControllerDef.TYPE,
                FlatBindingControllerImpl.class
        );
    
    final public static ControllerDef primitiveBindingControllerDef = 
        new ControllerDef(
                BindingControllerDef.TYPE,
                PrimitiveBindingControllerImpl.class
        );
    
    final public static ControllerDef compositeBindingControllerDef = 
        new ControllerDef(
                BindingControllerDef.TYPE,
                CompositeBindingControllerImpl.class
        );
    
    final public static ControllerDef flatTemplateBindingControllerDef = 
        new ControllerDef(
                BindingControllerDef.TYPE,
                FlatTemplateBindingControllerImpl.class
        );
    
    final public static ControllerDef primitiveTemplateBindingControllerDef = 
        new ControllerDef(
                BindingControllerDef.TYPE,
                PrimitiveTemplateBindingControllerImpl.class
        );
    
    final public static ControllerDef compositeTemplateBindingControllerDef = 
        new ControllerDef(
                BindingControllerDef.TYPE,
                CompositeTemplateBindingControllerImpl.class
        );
    
    final public static ControllerDef primitiveAutoBindingControllerDef = 
        new ControllerDef(
                BindingControllerDef.TYPE,
                PrimitiveAutoBindingControllerImpl.class
        );
    
    final public static ControllerDef compositeAutoBindingControllerDef = 
        new ControllerDef(
                BindingControllerDef.TYPE,
                CompositeAutoBindingControllerImpl.class
        );

    
    // -------------------------------------------------
    // Lifecycle controller definition
    // -------------------------------------------------
    
    final public static ControllerDef nonCompositeLifeCycleControllerDef =
        new ControllerDef(
                LifeCycleControllerDef.PRIMITIVE_TYPE,
                LifeCycleControllerImpl.class
        );
    
    final public static ControllerDef compositeLifeCycleControllerDef =
        new ControllerDef(
                LifeCycleControllerDef.COMPOSITE_TYPE,
                CompositeLifeCycleControllerImpl.class
        );

    
    // -------------------------------------------------
    // Name controller definition
    // -------------------------------------------------
    
    final public static ControllerDef nameControllerDef =
        new ControllerDef(
                NameControllerItf.TYPE,
                NameControllerImpl.class
        );
        
    
    // -------------------------------------------------
    // Super controller definition
    // -------------------------------------------------
    
    final public static ControllerDef superControllerDef =
        new ControllerDef(
                SuperControllerDef.TYPE,
                SuperControllerImpl.class
        );      
        
    
    // -------------------------------------------------
    // Content controller definition
    // -------------------------------------------------
    
    final public static ControllerDef contentControllerDef =
        new ControllerDef(
                ContentControllerItf.TYPE,
                ContentControllerImpl.class
        );
        
    final public static ControllerDef templateContentControllerDef =
        new ControllerDef(
                ContentControllerItf.TYPE,
                TemplateContentControllerImpl.class
        );
        
    
    // -------------------------------------------------
    // Factory controller definition
    // -------------------------------------------------
    
    final public static ControllerDef factoryControllerDef =
        new ControllerDef(
                new InterfaceType[]{
                        FactoryDef.TYPE,
                        FactoryDef.HIDDEN_TYPE
                },
                FactoryControllerImpl.class
        );
    
    
    // -------------------------------------------------
    // Attribute controller definition
    // -------------------------------------------------
    
    final public static ControllerDef attributeControllerDef =
        new ControllerDef(
                AttributeControllerItf.TYPE,
                AttributeControllerImpl.class
        );

    
    // -------------------------------------------------
    // AOKell membrane definitions
    // -------------------------------------------------
    
    final public static MembraneDef[] MEMBRANES =
        new MembraneDef[]{
            new MembraneDef(
                    "bootstrap",
                    new ControllerDef[] {
                            componentControllerDef
                    },
                    BaseType.class),
            
            new MembraneDef(
                    "flatPrimitive",
                    new ControllerDef[] {
                            flatBindingControllerDef,
                            componentControllerDef,
                            nonCompositeLifeCycleControllerDef,
                            nameControllerDef
                    },
                    FlatType.class),
            new MembraneDef(
                    "primitive",
                    new ControllerDef[] {
                            primitiveBindingControllerDef,
                            componentControllerDef,
                            nonCompositeLifeCycleControllerDef,
                            nameControllerDef,
                            superControllerDef
                    },
                    PrimitiveType.class),
            new MembraneDef(
                    "composite",
                    new ControllerDef[] {
                            compositeBindingControllerDef,
                            compositeComponentControllerDef,
                            contentControllerDef,
                            compositeLifeCycleControllerDef,
                            nameControllerDef,
                            superControllerDef,
                    },
                    CompositeType.class),
    
            // ----------
            // Parametric
            // ----------
            new MembraneDef(
                    "flatParametricPrimitive",
                    new ControllerDef[] {
                            flatBindingControllerDef,
                            componentControllerDef,
                            nonCompositeLifeCycleControllerDef,
                            nameControllerDef
                    },
                    ParametricFlatType.class),
            new MembraneDef(
                    "parametricPrimitive",
                    new ControllerDef[] {
                            primitiveBindingControllerDef,
                            componentControllerDef,
                            nonCompositeLifeCycleControllerDef,
                            nameControllerDef,
                            superControllerDef
                    },
                    ParametricPrimitiveType.class),
            new MembraneDef(
                    "parametricComposite",
                    new ControllerDef[] {
                            compositeBindingControllerDef,
                            compositeComponentControllerDef,
                            contentControllerDef,
                            compositeLifeCycleControllerDef,
                            nameControllerDef,
                            superControllerDef
                    },
                    ParametricCompositeType.class),
    
            // --------
            // Template
            // --------
            new MembraneDef(
                    "flatPrimitiveTemplate",
                    new ControllerDef[] {
                            flatTemplateBindingControllerDef,
                            componentControllerDef,
                            factoryControllerDef,
                            nameControllerDef
                    },
                    FlatTemplateType.class),
            new MembraneDef(
                    "primitiveTemplate",
                    new ControllerDef[] {
                            primitiveTemplateBindingControllerDef,
                            componentControllerDef,
                            factoryControllerDef,
                            nameControllerDef,
                            superControllerDef
                    },
                    PrimitiveTemplateType.class),
            new MembraneDef(
                    "compositeTemplate",
                    new ControllerDef[] {
                            compositeTemplateBindingControllerDef,
                            compositeComponentControllerDef,
                            templateContentControllerDef,
                            factoryControllerDef,
                            nameControllerDef,
                            superControllerDef
                    },
                    CompositeTemplateType.class),
    
            // -------------------
            // Parametric template
            // -------------------
            new MembraneDef(
                    "flatParametricPrimitiveTemplate",
                    new ControllerDef[] {
                            attributeControllerDef,
                            flatTemplateBindingControllerDef,
                            componentControllerDef,
                            factoryControllerDef,
                            nameControllerDef
                    },
                    ParametricFlatTemplateType.class),
            new MembraneDef(
                    "parametricPrimitiveTemplate",
                    new ControllerDef[] {
                            attributeControllerDef,
                            primitiveTemplateBindingControllerDef,
                            componentControllerDef,
                            factoryControllerDef,
                            nameControllerDef,
                            superControllerDef
                    },
                    ParametricPrimitiveTemplateType.class),
            new MembraneDef(
                    "parametricCompositeTemplate",
                    new ControllerDef[] {
                            attributeControllerDef,
                            compositeTemplateBindingControllerDef,
                            compositeComponentControllerDef,
                            templateContentControllerDef,
                            factoryControllerDef,
                            nameControllerDef,
                            superControllerDef
                    },
                    ParametricCompositeTemplateType.class),
            
            // ------------
            // Auto-binding
            // ------------
            new MembraneDef(
                    "autoBindingPrimitive",
                    new ControllerDef[] {
                            primitiveAutoBindingControllerDef,
                            componentControllerDef,
                            nonCompositeLifeCycleControllerDef,
                            nameControllerDef,
                            superControllerDef
                    },
                    PrimitiveType.class),
            new MembraneDef(
                    "autoBindingComposite",
                    new ControllerDef[] {
                            compositeAutoBindingControllerDef,
                            compositeComponentControllerDef,
                            contentControllerDef,
                            compositeLifeCycleControllerDef,
                            nameControllerDef,
                            superControllerDef
                    },
                    CompositeType.class),
        };
    
}
