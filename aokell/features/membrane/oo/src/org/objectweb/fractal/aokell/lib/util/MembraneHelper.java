/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.util;

import org.objectweb.fractal.aokell.Membranes;
import org.objectweb.fractal.aokell.lib.control.component.ComponentItf;
import org.objectweb.fractal.aokell.lib.control.content.ContentControllerItf;
import org.objectweb.fractal.aokell.lib.membrane.MembraneDef;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.julia.control.content.SuperControllerNotifier;



/**
 * This class contains helper methods with an implementation which differs
 * depending on the choosen version of the membrane feature.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class MembraneHelper {

    /**
     * Return the component controller implementation associated to the given
     * component. This method returns the given parameter with object-oriented
     * membranes.
     */
    public static Component getFcCompCtrlImpl( Component comp ) {
        return comp;
    }
    
    /**
     * Return the controller implementations associated to the given component.
     */
    public static Object[] getFcControllerImpls( Component comp ) {
        return ((ComponentItf)comp).getFcControllers();
    }
    
    /**
     * Return the component controller reference associated to the given
     * component. This method returns the given parameter with object-oriented
     * membranes.
     */
    public static ComponentItf getFcCompCtrl( Component comp ) {
        return (ComponentItf) comp;
    }

    /**
     * Return the binding controller reference associated to the given
     * component.
     */
    public static BindingController getFcBindingCtrl( Component comp ) {
        return FractalHelper.getBindingController(comp);
    }

    /**
     * Return the content controller reference associated to the given
     * component.
     */
    public static ContentControllerItf getFcContentCtrl( Component comp ) {
        return FractalHelper.getContentControllerItf(comp);
    }

    /**
     * Return the lifecycle controller reference associated to the given
     * component.
     */
    public static LifeCycleController getFcLifeCycleCtrl( Component comp ) {
        return FractalHelper.getLifeCycleController(comp);
    }

    /**
     * Return the super controller reference associated to the given component.
     */
    public static SuperControllerNotifier getFcSuperCtrl( Component comp ) {
        return FractalHelper.getSuperController(comp);
    }

    /**
     * Return the content associated to the given component.
     */
    public static Object getFcContent( Component comp ) {
        return FractalHelper.getContent(comp);
    }
    
    /**
     * Duplicate an existing membrane definition.
     * The purpose of this method is to facilitate the creation of new membrane
     * definitions based on existing ones. The duplicated membrane definition is
     * registered with the membrane repository.
     * 
     * @param srcControllerDesc  the membrane to duplicate
     * @param newControllerDesc  the controller description of the new membrane
     * @param newType            the type of the new membrane
     * @return                   the defintion of the new membrane
     * 
     * @exception IllegalArgumentException
     *      if the source membrane does not exists
     */
    public static MembraneDef duplicateMembraneDef(
            Object srcControllerDesc, Object newControllerDesc, Class newType ) {
        
        Membranes mrepository = Membranes.get();
        MembraneDef md = mrepository.getMembraneDef(srcControllerDesc);
        if( md == null ) {
            throw new IllegalArgumentException(
                    "Membrane "+srcControllerDesc+" does not exist");
        }
        
        MembraneDef newmd =
            new MembraneDef( newControllerDesc, md.getCtrls(), newType );
        mrepository.register(newmd);
        return newmd;
    }
}
