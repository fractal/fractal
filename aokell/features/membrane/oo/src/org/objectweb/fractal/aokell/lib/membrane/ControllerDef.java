/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.membrane;

import org.objectweb.fractal.api.type.InterfaceType;


/**
 * Class holding data for defining a controller.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class ControllerDef {
    
    /**
     * The Fractal interface types implemented by the controller.
     * 
     * Most of the time there is only one interface type.
     * One exception is the factory controller which implements two interfaces:
     * one associated with {@link org.objectweb.fractal.api.Factory} and
     * one associated with {@link org.objectweb.fractal.julia.factory.Template}.
     */
    private InterfaceType[] types;
    
    /** The class implementating the controller. */
    private Class impl;
    
    public ControllerDef( InterfaceType type, Class impl ) {
        this.types = new InterfaceType[]{type};
        this.impl = impl;
    }
    
    public ControllerDef( InterfaceType[] types, Class impl ) {
        this.types = types;
        this.impl = impl;
    }
    
    public InterfaceType[] getTypes() {
        return types;
    }
    
    public Class getImpl() {
        return impl;
    }
}
