This directory contains the implementation of some additional features of
AOKell. The features contained in this directory differ from the ones found in
the core source tree (src/), because they are:
- either optional,
- or comes in several versions.

Five features exist:
- fcinterface: deals with the way Fractal interfaces are implemented,
- glue: deals with the way the control and the content are glued together,
- loggable: deals with whether primitive components can be logged,
- membrane: deals with the way control membranes are implemented,
- platform: deals with the targeted Java platform (J2SE or J2ME).

See the README.txt file in the corresponding directories for further
explanations on these features.
