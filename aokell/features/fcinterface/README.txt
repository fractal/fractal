This directory contains the implementation of the fcinterface feature. This
feature deals with the way Fractal interfaces are implemented.

Two versions exist:
- rt (default): Fractal interface implementations are generated at runtime with
  ASM
- ct: Fractal interface implementations can be precompiled with the
  org.objectweb.fractal.aokell.tools.interf tool.