/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.asm;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.Attribute;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.fractal.aokell.lib.InterfaceImpl;
import org.objectweb.fractal.aokell.lib.interf.AOKellGeneratedItf;
import org.objectweb.fractal.aokell.lib.interf.ClassDefinition;
import org.objectweb.fractal.aokell.lib.interf.GeneratorItf;
import org.objectweb.fractal.aokell.lib.util.PlatformHelper;


/**
 * This class generates {@link org.objectweb.fractal.api.Interface}
 * implementations for interfaces which are bound to a target interface.
 * 
 * This is typically the case of:
 * <ul>
 * <li>interfaces own by composite components,</li>
 * <li>client interfaces associated to primitive components.</li>
 * </ul>
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class ASMGeneratorForBoundableInterface implements GeneratorItf {

    /**
     * Generate an Interface implementation by extending
     * {@link InterfaceImpl}.
     * 
     * @param targetClassname    the name of the generated class
     * @param delegateClassname  the name of the class where calls will be delegated
     * @return  the generated class definition (Class and bytecode)
     */
    public ClassDefinition generate(
            String targetClassname, String delegateClassname ) {
        
        final String targetSignature = targetClassname.replace('.','/');
        final String delegateSignature = delegateClassname.replace('.','/');

        // Class definition
        // Name: targetClassname
        // Extends: InterfaceImpl
        ClassWriter cw = new ClassWriter(false);
        cw.visit(
        		Opcodes.V1_1,
                Opcodes.ACC_PUBLIC,
                targetSignature,
                null,
                getInterfaceImpl(),
                new String[] {
                        delegateSignature,
                        AOKellGeneratedItf.class.getName().replace('.','/') });
        
        // Empty constructor generation
        MethodVisitor mv = cw.visitMethod(
                Opcodes.ACC_PUBLIC,
                "<init>", "()V",
                null,
                null);
        
        // Call the super contructor inherited from InterfaceImpl
        mv.visitVarInsn(Opcodes.ALOAD, 0);
        mv.visitMethodInsn(
                Opcodes.INVOKESPECIAL,
                getInterfaceImpl(),
                "<init>", "()V" );
        
        mv.visitInsn(Opcodes.RETURN);
        mv.visitMaxs(5, 5);
        
        // For each method of delegateClassname generate a delegator method
        ClassVisitor clv =
            new BoundableInterfaceGenerator(cw,targetSignature,delegateSignature);
        ClassReader cr;
        try {
            InputStream is =
                PlatformHelper.getResourceAsStream(
                        delegateClassname.replace('.','/')+".class" );
            cr = new ClassReader(is);
        }
        catch( IOException e ) {
            throw new RuntimeException("IOException: "+ e.getMessage());
        }
        cr.accept(clv,true);
        
        cw.visitEnd();
        
        // Load the proxy class
        byte[] bytecode = cw.toByteArray();
        Class cl = PlatformHelper.defineClass(targetClassname,bytecode);
        return new ClassDefinition(cl,bytecode);
    }
    
    /**
     * Return the name of the Interface implementation to be used as
     * super class for generated implementations.
     */
    protected String getInterfaceImpl() {
        return InterfaceImpl.class.getName().replace('.','/');
    }
}

/**
 * ASM class visitor for generating delegation methods.
 * 
 * For each visited method (typically defined in a Java interface), this class
 * generates a method containing delegation code. This code delegates the call
 * to the instance bound to the current interface (retrieved by calling lookupFc
 * on the current binding controller). 
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
class BoundableInterfaceGenerator implements ClassVisitor {
    
    private ClassWriter cw;
    private String targetClassname;
    private String delegateSignature;
    
    /** The set of methods already generated. */
    private Set methods = new HashSet();    // Set<String>
    
    
    /**
     * @param cw  the class writer where generated code must be written
     * @param targetClassname
     *      the name of the generated class that will hold the delegation
     *      methods
     * @param delegateSignature
     *      the signature of Java interface for which delegation code is to be
     *      generated
     */
    public BoundableInterfaceGenerator(
            ClassWriter cw, String targetClassname, String delegateSignature ) {        
        this.cw = cw;
        this.targetClassname = targetClassname;
        this.delegateSignature = delegateSignature;
    }
    
	public MethodVisitor visitMethod(
			int access, String name, String desc, String arg3,
			String[] exceptions ) {
        
        /*
         * Exclude static methods. Interfaces with final static fields may
         * contain a static block (which translates as a static method in the 
         * bytecode structure) to initialize these fields.
         */
        if( (access & Opcodes.ACC_STATIC) == Opcodes.ACC_STATIC ) {
            return null;
        }
        
        String key = name+desc;
        if( methods.contains(key) ) {
            /*
             * The signature has already been encountered.
             * The situation occurs when the interface for which we are
             * generating a proxy redefines (needlessly ?) a method with the
             * same signature and already defined in a super-interface.
             */
            return null;
        }
        methods.add(key);
        
        MethodVisitor cv =
            cw.visitMethod(
                    Opcodes.ACC_PUBLIC, name, desc, arg3, exceptions );
        
        // Call the lookupFc() method inherited from InterfaceImpl
        cv.visitVarInsn(Opcodes.ALOAD, 0);
        cv.visitMethodInsn(
                Opcodes.INVOKEVIRTUAL,
                targetClassname,
                "lookupFc",
                "()Ljava/lang/Object;");
        
        // Cast the return value to the business interface
        cv.visitTypeInsn(
                Opcodes.CHECKCAST,
                delegateSignature);
        
        // Generate a LOAD opcode for each parameter
        SignatureStringAnalyzer ssa;
        try {
            ssa = new SignatureStringAnalyzer(desc);
        }
        catch( InvalidSignatureException ise ) {
            throw new RuntimeException(
                    "InvalidSignatureException: "+ise.getMessage());
        }
        
        ISignatureParameter[] params = ssa.getParameters();
        ISignatureParameter ret = ssa.getReturnType();
        
        int maxStack = 1;
        for (int i = 0; i < params.length; i++) {
            ISignatureParameter parameter = params[i];
            int opcode = parameter.getLoadOpCode();
            cv.visitVarInsn(opcode,maxStack);
            maxStack += parameter.stackSize();
        }
        
        // Invoke the delegation method
        cv.visitMethodInsn(
                Opcodes.INVOKEINTERFACE, delegateSignature, name, desc);
        
        // Generate the RETURN opcode
        int opcode = ret.getReturnOpCode();
        cv.visitInsn(opcode);
        maxStack += ret.stackSize() - 1;
        
        // Compute maxs
        cv.visitMaxs(maxStack, maxStack);
        
        return cv;
    }

    public void visit(
            int arg0, int access, String name, String supername, String arg4,
            String[] interfaces ) {
        
        /*
         * Recursively visit implemented interfaces to generated delegation
         * method for all methods, including inherited ones. When an interface
         * extends another one, the super-interface appears in the implemented
         * interfaces (array interfaces), and not in the supername field.
         */
        for (int i = 0; i < interfaces.length; i++) {
            String delegateClassname = interfaces[i].replace('/','.');
            ClassReader cr;
            try {
                cr = new ClassReader(delegateClassname);
            }
            catch( IOException e ) {
                throw new RuntimeException("IOException: "+e.getMessage());
            }
            cr.accept(this,true);
        }
    }

    public void visitInnerClass(String arg0, String arg1, String arg2, int arg3) {
    }

    public void visitAttribute(Attribute arg0) {
    }

    public void visitEnd() {
    }

	public void visitSource(String arg0, String arg1) {
	}

	public void visitOuterClass(String arg0, String arg1, String arg2) {
	}

	public AnnotationVisitor visitAnnotation(String arg0, boolean arg1) {
        /*
         * Propagate annotations. Each generated method is associated to the
         * same annotations as its corresponding source method.
         */
		return cw.visitAnnotation(arg0,arg1);
	}

	public FieldVisitor visitField(int arg0, String arg1, String arg2, String arg3, Object arg4) {
        /*
         * Skip fields, i.e. fields encountered in the visited source code are
         * not inserted in the generated class.
         * 
         * Note: in the case of AOKell, the visited source code is a Java
         * interface. Java interfaces may contain constants. These constants are
         * stored as public final fields.
         */
        return null;
	}

}
