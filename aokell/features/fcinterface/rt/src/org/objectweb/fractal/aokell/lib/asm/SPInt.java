/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.asm;

import org.objectweb.asm.Opcodes;


/**
 * This class represents a int type in a signature string.
 * All instances of this class are equivalent. Hence this class can not be
 * instantiated and provides only a singleton instance of itself.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class SPInt implements ISignatureParameter {
    
    final public static SPInt singleton = new SPInt();

    private SPInt() {}
    
    public int getLoadOpCode() {
        return Opcodes.ILOAD;
    }

    public int getReturnOpCode() {
        return Opcodes.IRETURN;
    }
    
    public int stackSize() {
        return 1;
    }
    
    public String toString() {
        return "int";
    }
}
