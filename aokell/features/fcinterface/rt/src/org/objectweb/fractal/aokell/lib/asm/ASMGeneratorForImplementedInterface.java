/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.asm;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.Attribute;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.fractal.aokell.lib.ComponentInterfaceImpl;
import org.objectweb.fractal.aokell.lib.control.CloneCtrlException;
import org.objectweb.fractal.aokell.lib.control.attribute.AttributeControllerCloneableItf;
import org.objectweb.fractal.aokell.lib.interf.AOKellGeneratedItf;
import org.objectweb.fractal.aokell.lib.interf.ClassDefinition;
import org.objectweb.fractal.aokell.lib.interf.FcItfImplHelper;
import org.objectweb.fractal.aokell.lib.interf.GeneratorItf;
import org.objectweb.fractal.aokell.lib.util.PlatformHelper;
import org.objectweb.fractal.api.control.AttributeController;


/**
 * This class generates with ASM a class implementing
 * {@link org.objectweb.fractal.api.Interface}. This implementation corresponds
 * to a Fractal server interface associated to a primitive component. Calls to
 * the generated implementation are delegated to the component.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class ASMGeneratorForImplementedInterface implements GeneratorItf {
    
    /**
     * Generate an {@link org.objectweb.fractal.api.Interface} implementation by
     * extending {@link ComponentInterfaceImpl}. The implementation delegates
     * calls for a given Java interface.
     * 
     * @param targetClassname    the name of the class to generate
     * @param delegateJavaItfname
     *          the Java interface name for which delegation code must be
     *          generated
     * @return  the definition (class and bytecode) of the generated class
     */
    public ClassDefinition generate(
            String targetClassname, String delegateJavaItfname ) {
        
        final String targetSignature = targetClassname.replace('.','/');
        final String delegateSignature = delegateJavaItfname.replace('.','/');
        
        /*
         * Check whether the source Java interface is an attribute control
         * interface.
         */
        Class delegateJavaItf;
        try {
            delegateJavaItf = PlatformHelper.loadClass(delegateJavaItfname);
        }
        catch( ClassNotFoundException cnfe ) {
            final String msg = "Class not found exception: "+delegateJavaItfname;
            throw new RuntimeException(msg);
        }
        boolean isAttrCtrlItf =
            AttributeController.class.isAssignableFrom(delegateJavaItf);
        
        // Interfaces implemented by the generated class
        String[] impls =
            (isAttrCtrlItf) ?
                new String[] {
                    delegateSignature,
                    AttributeControllerCloneableItf.class.getName().replace('.','/'),
                    AOKellGeneratedItf.class.getName().replace('.','/') } :
                new String[] {
                    delegateSignature,
                    AOKellGeneratedItf.class.getName().replace('.','/') };            

        
        // Class definition
        // Name: targetClassname
        // Extends: ComponentInterfaceImpl
        ClassWriter cw = new ClassWriter(true);
        cw.visit(
        		Opcodes.V1_1,
        		Opcodes.ACC_PUBLIC,
                targetSignature,
                null,
                ComponentInterfaceImpl.class.getName().replace('.','/'),
                impls );
        
        // Generate field: private delegateClassname _delegate
        cw.visitField(
        		Opcodes.ACC_PRIVATE,
                "_delegate", "L"+delegateSignature+";",
                null,
                null);
        
        // Method visitor for generating the cloneFcCtrl method (if needed)
        MethodVisitor cloneFcCtrlMV = null;
        if( isAttrCtrlItf ) {
            cloneFcCtrlMV = cw.visitMethod(
                    Opcodes.ACC_PUBLIC,
                    "cloneFcCtrl",
                    "(L"+AttributeControllerCloneableItf.class.getName().replace('.','/')+";)V",
                    null,
                    new String[] {
                            CloneCtrlException.class.getName().replace('.','/') });
        }

        // Empty constructor generation
        MethodVisitor mv = cw.visitMethod(
        		Opcodes.ACC_PUBLIC,
                "<init>", "()V",
                null,
                null);
        
        // Call the super contructor inherited from ComponentInterfaceImpl
        mv.visitVarInsn(Opcodes.ALOAD, 0);
        mv.visitMethodInsn(
        		Opcodes.INVOKESPECIAL,
                ComponentInterfaceImpl.class.getName().replace('.','/'),
                "<init>", "()V" );
        
        mv.visitInsn(Opcodes.RETURN);
        mv.visitMaxs(1, 1);
        
        // Generate method: public void setFcItfImpl(Object fcContent)
        // This method overrides ComponentInterfaceImpl.setFcItfImpl(Object)
        mv = cw.visitMethod(
                Opcodes.ACC_PUBLIC,
                "setFcItfImpl", "(Ljava/lang/Object;)V",
                null, null );
        
        // Generate this.fcContent = fcContent
        mv.visitVarInsn(Opcodes.ALOAD, 0);
        mv.visitVarInsn(Opcodes.ALOAD, 1);
        mv.visitFieldInsn(
                Opcodes.PUTFIELD,
                ComponentInterfaceImpl.class.getName().replace('.','/'),
                "fcContent", "Ljava/lang/Object;" );
        
        // Generate this._delegate = (delegateSignature) fcContent
        mv.visitVarInsn(Opcodes.ALOAD, 0);
        mv.visitVarInsn(Opcodes.ALOAD, 1);
        mv.visitTypeInsn( Opcodes.CHECKCAST, delegateSignature );
        mv.visitFieldInsn(
                Opcodes.PUTFIELD,
                targetSignature,
                "_delegate", "L"+delegateSignature+";" );
        
        // Generate return
        mv.visitInsn(Opcodes.RETURN);
        mv.visitMaxs(2, 2);
        
        /*
         * For each method of delegateSignature generate a delegation method.
         */
        ImplementedInterfaceGenerator iig =
            new ImplementedInterfaceGenerator(
                    cw, targetSignature, delegateSignature,
                    isAttrCtrlItf, cloneFcCtrlMV );
        ClassReader cr;
        try {
            InputStream is =
                PlatformHelper.getResourceAsStream( delegateSignature+".class" );
            cr = new ClassReader(is);
        }
        catch( IOException e ) {
            throw new RuntimeException(delegateJavaItfname+": "+e);
        }
        cr.accept(iig,true);
        
        cw.visitEnd();
        
        /*
         * Terminate the definition of the cloneFcCtrl method.
         */
        if( isAttrCtrlItf ) {
            cloneFcCtrlMV.visitInsn(Opcodes.RETURN);
            cloneFcCtrlMV.visitMaxs(2,2);
            cloneFcCtrlMV.visitEnd();
        }
        
        /*
         * Load the proxy class.
         */
        byte[] bytecode = cw.toByteArray();        
        Class cl = PlatformHelper.defineClass(targetClassname,bytecode);
        return new ClassDefinition(cl,bytecode);
    }  
}

/**
 * ASM class visitor for generating delegation methods.
 * 
 * For each visited method (typically defined in a Java interface), this class
 * generates a method containing delegation code. This code delegates the call
 * for the current method to an instance referenced by the _delegate field. 
 *  
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
class ImplementedInterfaceGenerator implements ClassVisitor {
    
    private ClassWriter cw;    
    private String targetClassname;
    private String delegateSignature;
    private boolean isAttrCtrlItf;
    private MethodVisitor cloneFcCtrlMV;
    
    /** The set of methods already generated. */
    private Set methods = new HashSet();    // Set<String>
    
    
    /**
     * @param cw  the class writer where generated code must be written
     * @param targetClassname
     *      the name of the generated class that will hold the delegation
     *      methods
     * @param delegateSignature
     *      the signature of Java interface for which delegation code is to be
     *      generated
     */
    public ImplementedInterfaceGenerator(
            ClassWriter cw, String targetClassname, String delegateSignature,
            boolean isAttrCtrlItf, MethodVisitor cloneFcCtrlMV ) {
        
        this.cw = cw;
        this.targetClassname = targetClassname;
        this.delegateSignature = delegateSignature;
        this.isAttrCtrlItf = isAttrCtrlItf;
        this.cloneFcCtrlMV = cloneFcCtrlMV;
    }
    
    public MethodVisitor visitMethod(
            int access, String name, String desc, String arg3,
            String[] exceptions ) {
        
        /*
         * Exclude static methods. Interfaces with final static fields may
         * contain a static block (which translates as a static method in the 
         * bytecode structure) to initialize these fields.
         */
        if( (access & Opcodes.ACC_STATIC) == Opcodes.ACC_STATIC ) {
            return null;
        }
        
        /*
         * Check whether the signature has already been encountered.
         * The situation occurs when the interface for which we are
         * generating a proxy redefines (needlessly ?) a method with the
         * same signature and already defined in a super-interface.
         */
        String key = name+desc;
        if( methods.contains(key) ) {
            return null;
        }
        methods.add(key);
        
        /*
         * Analyse the method parameters.
         */
        SignatureStringAnalyzer ssa;
        try {
            ssa = new SignatureStringAnalyzer(desc);
        }
        catch( InvalidSignatureException ise ) {
            final String msg = "InvalidSignatureException: "+ise.getMessage();
            throw new RuntimeException(msg);
        }
        ISignatureParameter[] params = ssa.getParameters();
                
        /*
         * Generate the method containing the delegation code.
         */
        MethodVisitor mv =
            cw.visitMethod( Opcodes.ACC_PUBLIC, name, desc, arg3, exceptions );
        
        /*
         * Handle attribute control interfaces.
         */
        if( isAttrCtrlItf && name.startsWith("set") ) {
            
            // Get the attribute signature
            if( params.length != 1 ) {
                final String msg =
                    delegateSignature+"#"+name+
                    " does not follow Java Bean conventions for a setter name";
                throw new IllegalArgumentException(msg);
            }
            String fieldName = FcItfImplHelper.getFieldNameFromSetter(name);
            int p = desc.indexOf(')');
            if( p < 1 ) {
                final String msg = desc+" should contain a closing parenthesis";
                throw new RuntimeException(msg);
            }
            String fieldSignature = desc.substring(1,p);

            // Generate this.field = name
            mv.visitVarInsn(Opcodes.ALOAD, 0);
            ISignatureParameter parameter = params[0];
            int opcode = parameter.getLoadOpCode();
            mv.visitVarInsn(opcode,1);
            mv.visitFieldInsn(
                    Opcodes.PUTFIELD,
                    targetClassname,
                    fieldName, fieldSignature );
            
            // Generate the declaration of the field
            cw.visitField(
                    Opcodes.ACC_PRIVATE,
                    fieldName, fieldSignature,
                    null, null );
            
            // Generate an instruction in cloneFcCtrl(dst) to clone the attribute            
            // 1. Load and cast the first and unique parameter (dst)
            // 2. Load the attribute
            // 3. Call dst.setAttrName(value)
            cloneFcCtrlMV.visitVarInsn(Opcodes.ALOAD, 1);
            cloneFcCtrlMV.visitTypeInsn(Opcodes.CHECKCAST, delegateSignature);
            cloneFcCtrlMV.visitVarInsn(Opcodes.ALOAD, 0);
            cloneFcCtrlMV.visitFieldInsn(
                  Opcodes.GETFIELD,
                  targetClassname,
                  fieldName, fieldSignature );
            cloneFcCtrlMV.visitMethodInsn(
                  Opcodes.INVOKEINTERFACE,
                  delegateSignature,
                  name, desc);
        }
        
        // Get the _delegate field
        mv.visitVarInsn(Opcodes.ALOAD, 0);
        mv.visitFieldInsn(
                Opcodes.GETFIELD,
                targetClassname,
                "_delegate", "L"+delegateSignature+";" );
        
        // Generate a LOAD opcode for each parameter
        int maxStack = 1;
        for (int i = 0; i < params.length; i++) {
            ISignatureParameter parameter = params[i];
            int opcode = parameter.getLoadOpCode();
            mv.visitVarInsn(opcode,maxStack);
            maxStack += parameter.stackSize();
        }
        
        // Generate the invocation of the method
        mv.visitMethodInsn(
                Opcodes.INVOKEINTERFACE, delegateSignature, name, desc);
        
        // Generate the RETURN opcode
        ISignatureParameter ret = ssa.getReturnType();
        int opcode = ret.getReturnOpCode();
        mv.visitInsn(opcode);
        maxStack += ret.stackSize() - 1;
        
        // Compute maxs
        mv.visitMaxs(maxStack, maxStack);
        
        return mv;
    }

    public void visit(
            int arg0, int access, String name, String supername, String arg4,
            String[] interfaces ) {
        
        /*
         * Recursively visit implemented interfaces to generate delegation
         * method for all methods, including inherited ones. When an interface
         * extends another one, the super-interface appears in the implemented
         * interfaces (array interfaces), and not in the supername field.
         */
        for (int i = 0; i < interfaces.length; i++) {
            String delegateClassname = interfaces[i].replace('/','.');
            ClassReader cr;
            try {
                cr = new ClassReader(delegateClassname);
            }
            catch( IOException e ) {
                throw new RuntimeException("IOException: "+ e.getMessage());
            }
            cr.accept(this,true);
        }
    }

    public void visitInnerClass(String arg0, String arg1, String arg2, int arg3) {
    }

    public void visitAttribute(Attribute arg0) {
    }

    public void visitEnd() {
    }

	public void visitSource(String arg0, String arg1) {
	}

	public void visitOuterClass(String arg0, String arg1, String arg2) {
	}

	public AnnotationVisitor visitAnnotation(String arg0, boolean arg1) {
        /*
         * Propagate annotations. Each generated method is associated to the
         * same annotations as its corresponding source method.
         */
        return cw.visitAnnotation(arg0,arg1);
	}

	public FieldVisitor visitField(int arg0, String arg1, String arg2, String arg3, Object arg4) {
        /*
         * Skip fields, i.e. fields encountered in the visited source code are
         * not inserted in the generated class.
         * 
         * Note: in the case of AOKell, the visited source code is a Java
         * interface. Java interfaces may contain constants. These constants are
         * stored as public final fields.
         */
        return null;
	}
}
