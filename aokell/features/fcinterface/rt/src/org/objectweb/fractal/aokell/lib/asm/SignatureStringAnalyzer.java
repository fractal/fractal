/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.asm;

import java.util.ArrayList;
import java.util.List;

/**
 * This class analyzes a signature string (e.g. a string such as
 * "(DI[Ljava/lang/String;[F[Z[C[LItf;[Ljava/lang/Object;)Ljava/lang/Object;")
 * and extract its parameters.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class SignatureStringAnalyzer {
    
    private String desc;
    
    /** Indice giving the current position in the analyze of the string. */
    private int idx = 0;
    
    private ISignatureParameter[] parameters;
    private ISignatureParameter returnType;
    
    public SignatureStringAnalyzer(String desc)
    throws InvalidSignatureException {
        
        this.desc = desc;
        analyze();
    }
    
    public ISignatureParameter[] getParameters() {
        return parameters;
    }
    
    public ISignatureParameter getReturnType() {
        return returnType;
    }
    
    /**
     * Analyze the signature string.
     * 
     * @throws InvalidSignatureException 
     *      if an unexpected character is encountered
     */
    private void analyze() throws InvalidSignatureException {
        
        List params = new ArrayList();
        
        // Description strings start with a parenthesis
        if( desc.charAt(0) != '(' )
            throw new InvalidSignatureException(desc,desc.substring(0,1),0);
        
        // Skip the parenthesis
        idx = 1;
        
        for( ; idx<desc.length() && desc.charAt(idx)!=')' ; idx++ ) {
            params.add( analyzeParameter() );
        }
        
        // Skip the end parenthesis
        idx++;
        if( idx >= desc.length() )
            throw new InvalidSignatureException(desc,"EOF",idx);
        
        // The return type
        returnType = analyzeParameter();
        
        // The parameters
        parameters = (ISignatureParameter[])
            params.toArray( new ISignatureParameter[params.size()] );
    }
    
    /**
     * Analyze the current parameter in the string.
     * 
     * @return  the current parameter
     */
    private ISignatureParameter analyzeParameter()
    throws InvalidSignatureException {

        switch( desc.charAt(idx) ) {
            case 'B' : return SPInt.singleton;      // byte
            case 'C' : return SPInt.singleton;      // char
            case 'D' : return SPDouble.singleton;   // double
            case 'F' : return SPFloat.singleton;    // float
            case 'I' : return SPInt.singleton;      // int
            case 'J' : return SPLong.singleton;     // long
            case 'S' : return SPInt.singleton;      // short
            case 'V' : return SPVoid.singleton;     // void
            case 'Z' : return SPInt.singleton;      // boolean
            
            case '[' :  // array
                // The type of the array elements follows. Skip it. 
                idx++;
                analyzeParameter();
                return SPObject.singleton;
            
            case 'L' :  // object
                int end = desc.indexOf(';',idx);
                if( end == idx )
                    // The object type is missing
                    throw new InvalidSignatureException(desc,";",idx);
                idx = end;
                return SPObject.singleton;                
        }
        
        throw new InvalidSignatureException(desc,desc.substring(idx,idx+1),idx);
    }
}