/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.asm;

import org.objectweb.fractal.aokell.lib.asm.ISignatureParameter;
import org.objectweb.fractal.aokell.lib.asm.InvalidSignatureException;
import org.objectweb.fractal.aokell.lib.asm.SPInt;
import org.objectweb.fractal.aokell.lib.asm.SPObject;
import org.objectweb.fractal.aokell.lib.asm.SPVoid;
import org.objectweb.fractal.aokell.lib.asm.SignatureStringAnalyzer;

import junit.framework.TestCase;

/**
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class TestSignatureStringAnalyzer extends TestCase {
    
    private SignatureStringAnalyzer analyze( String desc ) {
        
        System.out.println("Desc: "+desc);
        
        SignatureStringAnalyzer ssa;
        try {
            ssa = new SignatureStringAnalyzer(desc);
        }
        catch(InvalidSignatureException ise) {
            throw new RuntimeException(ise);
        }
        
        ISignatureParameter[] params = ssa.getParameters();
        ISignatureParameter ret = ssa.getReturnType();
        
        System.out.print("Params  : ");
        for (int i = 0; i < params.length; i++) {
            System.out.print(params[i]+" ");
        }
        System.out.println();
        System.out.println("Return  : "+ret);
        
        return ssa;
    }
    
    public void test1() {
        System.out.println("=============== test 1 ===============");
        
        SignatureStringAnalyzer ssa = analyze("()V");
        
        ISignatureParameter[] params = ssa.getParameters();
        ISignatureParameter ret = ssa.getReturnType();
        
        if( params.length != 0 )
            throw new RuntimeException("There shouldn't have any parameter");
        if( !(ret instanceof SPVoid) )
            throw new RuntimeException("Return type should be void");
    }
    
    public void test2() {
        System.out.println("=============== test 2 ===============");
        
        SignatureStringAnalyzer ssa = analyze("()I");
        
        ISignatureParameter[] params = ssa.getParameters();
        ISignatureParameter ret = ssa.getReturnType();
        
        if( params.length != 0 )
            throw new RuntimeException("There shouldn't have any parameter");
        if( !(ret instanceof SPInt) )
            throw new RuntimeException("Return type should be int");
    }
    
    public void test3() {
        System.out.println("=============== test 3 ===============");
        
        SignatureStringAnalyzer ssa =
            analyze("(DILjava/lang/String;FZCSLItf;Ljava/lang/Object;)Ljava/lang/Object;");
        
        ISignatureParameter[] params = ssa.getParameters();
        ISignatureParameter ret = ssa.getReturnType();
        
        final String expected =
            "double int object float int int int object object ";
        
        String result = "";
        for (int i = 0; i < params.length; i++) {
            result += params[i]+" ";
        }
        
        if( ! result.equals(expected) ) {
            System.out.println("Expected: "+expected);
            throw new RuntimeException("Unexpected parameters");
        }
        if( !(ret instanceof SPObject) )
            throw new RuntimeException("Return type should be object");
    }
    
    public void test4() {
        System.out.println("=============== test 4 ===============");
        
        SignatureStringAnalyzer ssa =
            analyze("([D[I[Ljava/lang/String;[F[Z[C[LItf;[Ljava/lang/Object;)[Ljava/lang/Object;");
        
        ISignatureParameter[] params = ssa.getParameters();
        ISignatureParameter ret = ssa.getReturnType();
        
        final String expected =
            "object object object object object object object object ";
        
        String result = "";
        for (int i = 0; i < params.length; i++) {
            result += params[i]+" ";
        }
        
        if( ! result.equals(expected) ) {
            System.out.println("Expected: "+expected);
            throw new RuntimeException("Unexpected parameters");
        }
        if( !(ret instanceof SPObject) )
            throw new RuntimeException("Return type should be object");
    }
    
}
