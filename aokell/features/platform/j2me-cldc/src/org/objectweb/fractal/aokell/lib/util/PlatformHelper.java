/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.util;

import java.io.IOException;


/**
 * This class contains helper methods with an implementation which differs
 * depending on the choosen version of the platform feature.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class PlatformHelper {

    /**
     * Set the loader to be used by AOKell for loading classes.
     * 
     * @param cl  the loader.
     *      This parameter must implement ClassLoader. It is declared as an
     *      Object in order to be compliant with the J2ME version of AOKell.
     */
    public static void setLoader( Object cl ) {
        /*
         * To simplify the writing of the AOKell code which is common to all
         * features, I have kept a setLoader method even though it can not be
         * implemented for J2ME.
         * 
         * UnsupportedOperationException is not in the CLDC API.
         */
        throw new RuntimeException("Unsupported operation with J2ME");
    }

    /**
     * Return the loader used by AOKell for loading classes.
     */
    public static Class loadClass(String name) throws ClassNotFoundException {
        return Class.forName(name);
    }

    
    // ------------------------------------------------------------------
    // Filesystem related functions (no filesystem available with the
    // platform.j2me-cldc feature.
    // ------------------------------------------------------------------
    
    /**
     * Utility method to write the bytecode from a class to a file. This method
     * creates the directory structure corresponding to package names.
     * 
     * @param dirname    the root directory name
     * @param classname  the name of the class
     * @param b          the bytecode
     */
    public static void dumpClassToFile(
            String dirname, String classname, byte[] b ) throws IOException {
        // No filesystem, we can not dump the bytecode into a file.
        // Simply ignore the command.
    }
    
}
