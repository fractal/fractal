This directory contains the implementation of the platform feature. This feature
allows generating versions of AOKell for different types of Java platforms.

Three versions exist:
- j2se (default):
    with this version, AOKell runs on any standard J2SE virtual machine.
- j2me-cdc:
    the target platform is J2ME CDC <http://java.sun.com/products/cdc/>.
    With this version, AOKell can be used on virtual machines for PDA such as
    IBM J9 VM <http://www-306.ibm.com/software/wireless/weme/>.
- j2me-cldc:
    the target platform is J2ME CLDC <http://java.sun.com/products/cldc/>.
    With this version, AOKell can be used on virtual machines for smart phones
    and embedded devices such as Sun KVM
    <http://java.sun.com/javame/downloads/index.html>.
