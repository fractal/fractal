/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * This class contains helper methods with an implementation which differs
 * depending on the choosen version of the platform feature.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class PlatformHelper {

    // ------------------------------------------------------------------
    // Class and ressource loading primitives
    // ------------------------------------------------------------------
    
    /*
     * Thread.currentThread().setContextClassLoader() is needed to provide a
     * correct behavior with the RMIClassLoader used by fractalrmi.
     * 
     * RMIClassLoader is needed when one remotely downloads code (e.g.
     * communication stubs) with the java.rmi.server.codebase property.
     */

    /**
     * Set the loader to be used by AOKell for loading classes.
     * 
     * @param cl  the loader.
     *      This parameter must implement ClassLoader. It is declared as an
     *      Object in order to be compliant with the J2ME version of AOKell.
     */
    public static void setLoader( Object cl ) {
        Thread.currentThread().setContextClassLoader( (ClassLoader) cl );
    }

    /**
     * Load a class.
     */
    public static Class loadClass(String name) throws ClassNotFoundException {
        return AOKellClassLoader.get().loadClass(name);
    }
    
    /**
     * Load a ressource.
     */
    public static InputStream getResourceAsStream( String name ) {
        return AOKellClassLoader.get().getResourceAsStream(name);
    }

    
    // ------------------------------------------------------------------
    // AOKell class loader for registering classes generated on the fly
    // by the fcinterface.rt or the glue.spoon-rt features.
    // ------------------------------------------------------------------
    
    /**
     * Define a new class.
     */
    public static Class defineClass( String name, byte[] b ) {
        return AOKellClassLoader.get().defineClass(name,b);
    }
        
    
    /**
     * AOKell class loader.
     * 
     * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
     */
    private static class AOKellClassLoader extends ClassLoader {
        
        /*
         * Thread.currentThread().getContextClassLoader() is needed as a parent
         * of this class loader to provide a correct behavior with the
         * RMIClassLoader used by fractalrmi.
         * 
         * RMIClassLoader is needed when one remotely downloads code (e.g. 
         * communication stubs) with the java.rmi.server.codebase property.
         */

        private AOKellClassLoader() {
            super(Thread.currentThread().getContextClassLoader());
        }
        
        /** Return the singleton instance of this class. */
        public static AOKellClassLoader get() {
            if( singleton == null ||
                singleton.getParent() !=
                    Thread.currentThread().getContextClassLoader() ) {
                
                /*
                 * The context class loader may change, for example, when
                 * fractalrmi uses the java.rmi.server.codebase property.
                 */
                
                singleton = new AOKellClassLoader();
            }
            return singleton;
        }
        
        private static AOKellClassLoader singleton;;

        public Class defineClass(String name, byte[] b) {
            return defineClass(name,b,0,b.length);
        }
    }

    
    // ------------------------------------------------------------------
    // Filesystem related functions (no filesystem available with the
    // platform.j2me-cldc feature.
    // ------------------------------------------------------------------
    
    /**
     * Utility method to write the bytecode from a class to a file. This method
     * creates the directory structure corresponding to package names.
     * 
     * @param dirname    the root directory name
     * @param classname  the name of the class
     * @param b          the bytecode
     */
    public static void dumpClassToFile(
            String dirname, String classname, byte[] b ) throws IOException {
        
        File dir = new File(dirname);
        if( !dir.exists() || !dir.isDirectory() ) {
            throw new IOException(dir+" must exist and be a directory");
        }
        
        // Create the directory structure
        int lastdot = classname.lastIndexOf('.');
        String filedirname = classname.substring(0,lastdot);
        String filename = classname.substring(lastdot+1);
        File filedir = new File(dir,filedirname.replace('.','/'));
        filedir.mkdirs();
        
        // Write the file
        File classfile = new File(filedir,filename+".class");
        FileOutputStream fos = new FileOutputStream(classfile);
        fos.write(b);
        fos.close();
    }
}
