This directory contains the implementation of the loggable feature. When this
feature is enabled, each primitive component is bound to a logger and a monolog
factory instance. These instances can be retrieved by calling respectively,
lookupFc("logger") and lookupFc("monolog-factory").

Two versions exist:
- off (default): the feature is disabled, 
- on: the feature is enabled. 
