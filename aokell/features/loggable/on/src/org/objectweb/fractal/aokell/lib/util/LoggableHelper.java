/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, UTSL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.util;

import org.objectweb.fractal.aokell.lib.util.MembraneHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.util.ComponentHelper;
import org.objectweb.util.monolog.Monolog;
import org.objectweb.util.monolog.api.Loggable;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.MonologFactory;


/**
 * This class contains helper methods with an implementation which differs
 * depending on the choosen version of the loggable feature.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class LoggableHelper {
    
    /**
     * Initialize the logging mechanism.
     */
    public static void init() {
        String monologConfFile = System.getProperty("monolog-conf-file");
        if( monologConfFile != null ) {
            if( Monolog.monologFactory == Monolog.getDefaultMonologFactory() ) {
                Monolog.getMonologFactory(monologConfFile);
              }
        }        
    }
    
    /**
     * Bind the logger and monolog-factory interfaces.
     */
    public static void bind( Component compctrl ) {
        
        compctrl = MembraneHelper.getFcCompCtrlImpl(compctrl);
        
        try {
            if( Monolog.monologFactory == Monolog.getDefaultMonologFactory() ) {
                Monolog.initialize();
            }
            StringBuffer path = new StringBuffer();
            ComponentHelper.toString(compctrl, path);
            String s = path.toString().substring(1).replace('/', '.');
            MonologFactory mf = Monolog.monologFactory;
            Logger logger = Monolog.monologFactory.getLogger(s);
            Object content = compctrl.getFcInterface("/content");
            if (content instanceof Loggable) {
              ((Loggable)content).setLogger(logger);
              ((Loggable)content).setLoggerFactory(mf);
            }
            if (content instanceof BindingController) {
              BindingController bc = (BindingController)content;
              // final static constants must be avoided in mixins
              bc.bindFc("logger", logger);
              bc.bindFc("monolog-factory", mf);
            }
        } catch (Exception ignored) {
        }        
    }

}
