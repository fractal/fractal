This directory contains the implementation of the glue feature. This feature
deals with the way the control part and the content part of a component are
glued together.

Two versions exist:
- spoon (default): the glue is implemented with Spoon processors
- aspectj: the glue is implemented with AspectJ aspects
