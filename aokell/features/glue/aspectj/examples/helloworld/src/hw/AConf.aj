/***
 * AOKell Fractal Hello World Example
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package hw;

import org.objectweb.fractal.aokell.lib.control.component.ComponentSetterItf;
import org.objectweb.fractal.aokell.lib.control.lifecycle.LifeCycleInterceptorItf;
import org.objectweb.fractal.julia.control.content.SuperControllerNotifier;
import org.objectweb.fractal.aokell.lib.membrane.primitive.ParametricPrimitiveType;
import org.objectweb.fractal.aokell.lib.membrane.primitive.PrimitiveType;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.NameController;

/**
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public aspect AConf {
    
    declare parents: cs.impl.ClientImpl implements PrimitiveType;
    declare parents: cs.impl.ClientImpl implements Component;
    declare parents: cs.impl.ClientImpl implements BindingController;
    declare parents: cs.impl.ClientImpl implements LifeCycleController;
    declare parents: cs.impl.ClientImpl implements NameController;
    declare parents: cs.impl.ClientImpl implements SuperControllerNotifier;
    declare parents: cs.impl.ClientImpl implements LifeCycleInterceptorItf;
    declare parents: cs.impl.ClientImpl implements ComponentSetterItf;

    declare parents: cs.impl.ServerImpl implements ParametricPrimitiveType;
    declare parents: cs.impl.ServerImpl implements Component;
    declare parents: cs.impl.ServerImpl implements BindingController;
    declare parents: cs.impl.ServerImpl implements LifeCycleController;
    declare parents: cs.impl.ServerImpl implements NameController;
    declare parents: cs.impl.ServerImpl implements SuperControllerNotifier;
    declare parents: cs.impl.ServerImpl implements LifeCycleInterceptorItf;
    declare parents: cs.impl.ServerImpl implements ComponentSetterItf;

    declare parents: csadl.impl.ClientImpl implements PrimitiveType;
    declare parents: csadl.impl.ClientImpl implements Component;
    declare parents: csadl.impl.ClientImpl implements BindingController;
    declare parents: csadl.impl.ClientImpl implements LifeCycleController;
    declare parents: csadl.impl.ClientImpl implements NameController;
    declare parents: csadl.impl.ClientImpl implements SuperControllerNotifier;
    declare parents: csadl.impl.ClientImpl implements LifeCycleInterceptorItf;
    declare parents: csadl.impl.ClientImpl implements ComponentSetterItf;

    declare parents: csadl.impl.ServerImpl implements ParametricPrimitiveType;
    declare parents: csadl.impl.ServerImpl implements Component;
    declare parents: csadl.impl.ServerImpl implements BindingController;
    declare parents: csadl.impl.ServerImpl implements LifeCycleController;
    declare parents: csadl.impl.ServerImpl implements NameController;
    declare parents: csadl.impl.ServerImpl implements SuperControllerNotifier;
    declare parents: csadl.impl.ServerImpl implements LifeCycleInterceptorItf;
    declare parents: csadl.impl.ServerImpl implements ComponentSetterItf;

    declare parents: cstemplate.impl.ClientImpl implements PrimitiveType;
    declare parents: cstemplate.impl.ClientImpl implements Component;
    declare parents: cstemplate.impl.ClientImpl implements BindingController;
    declare parents: cstemplate.impl.ClientImpl implements LifeCycleController;
    declare parents: cstemplate.impl.ClientImpl implements NameController;
    declare parents: cstemplate.impl.ClientImpl implements SuperControllerNotifier;
    declare parents: cstemplate.impl.ClientImpl implements LifeCycleInterceptorItf;
    declare parents: cstemplate.impl.ClientImpl implements ComponentSetterItf;

    declare parents: cstemplate.impl.ServerImpl implements ParametricPrimitiveType;
    declare parents: cstemplate.impl.ServerImpl implements Component;
    declare parents: cstemplate.impl.ServerImpl implements BindingController;
    declare parents: cstemplate.impl.ServerImpl implements LifeCycleController;
    declare parents: cstemplate.impl.ServerImpl implements NameController;
    declare parents: cstemplate.impl.ServerImpl implements SuperControllerNotifier;
    declare parents: cstemplate.impl.ServerImpl implements LifeCycleInterceptorItf;
    declare parents: cstemplate.impl.ServerImpl implements ComponentSetterItf;

    declare parents: cstemplateadl.impl.ClientImpl implements PrimitiveType;
    declare parents: cstemplateadl.impl.ClientImpl implements Component;
    declare parents: cstemplateadl.impl.ClientImpl implements BindingController;
    declare parents: cstemplateadl.impl.ClientImpl implements LifeCycleController;
    declare parents: cstemplateadl.impl.ClientImpl implements NameController;
    declare parents: cstemplateadl.impl.ClientImpl implements SuperControllerNotifier;
    declare parents: cstemplateadl.impl.ClientImpl implements LifeCycleInterceptorItf;
    declare parents: cstemplateadl.impl.ClientImpl implements ComponentSetterItf;

    declare parents: cstemplateadl.impl.ServerImpl implements ParametricPrimitiveType;
    declare parents: cstemplateadl.impl.ServerImpl implements Component;
    declare parents: cstemplateadl.impl.ServerImpl implements BindingController;
    declare parents: cstemplateadl.impl.ServerImpl implements LifeCycleController;
    declare parents: cstemplateadl.impl.ServerImpl implements NameController;
    declare parents: cstemplateadl.impl.ServerImpl implements SuperControllerNotifier;
    declare parents: cstemplateadl.impl.ServerImpl implements LifeCycleInterceptorItf;
    declare parents: cstemplateadl.impl.ServerImpl implements ComponentSetterItf;
}
