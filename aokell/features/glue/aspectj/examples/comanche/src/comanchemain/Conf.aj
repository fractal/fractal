/***
 * AOKell Comanche web server
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package comanchemain;

import org.objectweb.fractal.aokell.lib.control.component.ComponentSetterItf;
import org.objectweb.fractal.aokell.lib.control.lifecycle.LifeCycleInterceptorItf;
import org.objectweb.fractal.julia.control.content.SuperControllerNotifier;
import org.objectweb.fractal.aokell.lib.membrane.primitive.PrimitiveType;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.NameController;

/**
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public aspect Conf {
    
    // Not all classes in comanche.* are Fractal components 
    
    declare parents: comanche.BasicLogger implements PrimitiveType;
    /*
     * Workaround for replacing:
     *     declare parents: BaseType+ implements Component;
     *     ...
     * which would require to expose BaseType to the weaver.
     */
    declare parents: comanche.BasicLogger implements Component;
    declare parents: comanche.BasicLogger implements BindingController;
    declare parents: comanche.BasicLogger implements LifeCycleController;
    declare parents: comanche.BasicLogger implements NameController;
    declare parents: comanche.BasicLogger implements SuperControllerNotifier;
    declare parents: comanche.BasicLogger implements LifeCycleInterceptorItf;
    declare parents: comanche.BasicLogger implements ComponentSetterItf;

    declare parents: comanche.ErrorRequestHandler implements PrimitiveType;
    declare parents: comanche.ErrorRequestHandler implements Component;
    declare parents: comanche.ErrorRequestHandler implements BindingController;
    declare parents: comanche.ErrorRequestHandler implements LifeCycleController;
    declare parents: comanche.ErrorRequestHandler implements NameController;
    declare parents: comanche.ErrorRequestHandler implements SuperControllerNotifier;
    declare parents: comanche.ErrorRequestHandler implements LifeCycleInterceptorItf;
    declare parents: comanche.ErrorRequestHandler implements ComponentSetterItf;

    declare parents: comanche.FileRequestHandler implements PrimitiveType;
    declare parents: comanche.FileRequestHandler implements Component;
    declare parents: comanche.FileRequestHandler implements BindingController;
    declare parents: comanche.FileRequestHandler implements LifeCycleController;
    declare parents: comanche.FileRequestHandler implements NameController;
    declare parents: comanche.FileRequestHandler implements SuperControllerNotifier;
    declare parents: comanche.FileRequestHandler implements LifeCycleInterceptorItf;
    declare parents: comanche.FileRequestHandler implements ComponentSetterItf;

    declare parents: comanche.MultiThreadScheduler implements PrimitiveType;
    declare parents: comanche.MultiThreadScheduler implements Component;
    declare parents: comanche.MultiThreadScheduler implements BindingController;
    declare parents: comanche.MultiThreadScheduler implements LifeCycleController;
    declare parents: comanche.MultiThreadScheduler implements NameController;
    declare parents: comanche.MultiThreadScheduler implements SuperControllerNotifier;
    declare parents: comanche.MultiThreadScheduler implements LifeCycleInterceptorItf;
    declare parents: comanche.MultiThreadScheduler implements ComponentSetterItf;

    declare parents: comanche.RequestAnalyzer implements PrimitiveType;
    declare parents: comanche.RequestAnalyzer implements Component;
    declare parents: comanche.RequestAnalyzer implements BindingController;
    declare parents: comanche.RequestAnalyzer implements LifeCycleController;
    declare parents: comanche.RequestAnalyzer implements NameController;
    declare parents: comanche.RequestAnalyzer implements SuperControllerNotifier;
    declare parents: comanche.RequestAnalyzer implements LifeCycleInterceptorItf;
    declare parents: comanche.RequestAnalyzer implements ComponentSetterItf;

    declare parents: comanche.RequestDispatcher implements PrimitiveType;
    declare parents: comanche.RequestDispatcher implements Component;
    declare parents: comanche.RequestDispatcher implements BindingController;
    declare parents: comanche.RequestDispatcher implements LifeCycleController;
    declare parents: comanche.RequestDispatcher implements NameController;
    declare parents: comanche.RequestDispatcher implements SuperControllerNotifier;
    declare parents: comanche.RequestDispatcher implements LifeCycleInterceptorItf;
    declare parents: comanche.RequestDispatcher implements ComponentSetterItf;

    declare parents: comanche.RequestReceiver implements PrimitiveType;
    declare parents: comanche.RequestReceiver implements Component;
    declare parents: comanche.RequestReceiver implements BindingController;
    declare parents: comanche.RequestReceiver implements LifeCycleController;
    declare parents: comanche.RequestReceiver implements NameController;
    declare parents: comanche.RequestReceiver implements SuperControllerNotifier;
    declare parents: comanche.RequestReceiver implements LifeCycleInterceptorItf;
    declare parents: comanche.RequestReceiver implements ComponentSetterItf;

    declare parents: comanche.SequentialScheduler implements PrimitiveType;
    declare parents: comanche.SequentialScheduler implements Component;
    declare parents: comanche.SequentialScheduler implements BindingController;
    declare parents: comanche.SequentialScheduler implements LifeCycleController;
    declare parents: comanche.SequentialScheduler implements NameController;
    declare parents: comanche.SequentialScheduler implements SuperControllerNotifier;
    declare parents: comanche.SequentialScheduler implements LifeCycleInterceptorItf;
    declare parents: comanche.SequentialScheduler implements ComponentSetterItf;
}
