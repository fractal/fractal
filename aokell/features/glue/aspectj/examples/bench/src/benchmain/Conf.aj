/***
 * AOKell JACBenchmark
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package benchmain;

import org.objectweb.fractal.aokell.lib.control.component.ComponentSetterItf;
import org.objectweb.fractal.aokell.lib.control.lifecycle.LifeCycleInterceptorItf;
import org.objectweb.fractal.julia.control.content.SuperControllerNotifier;
import org.objectweb.fractal.aokell.lib.membrane.primitive.PrimitiveType;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.NameController;


/**
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public aspect Conf {
    
    declare parents: bench.ClientImpl implements PrimitiveType;
    
    /*
     * Workaround for replacing:
     *     declare parents: BaseType+ implements Component;
     *     ...
     * which would require to expose BaseType to the weaver.
     */
    declare parents: bench.ClientImpl implements Component;
    declare parents: bench.ClientImpl implements BindingController;
    declare parents: bench.ClientImpl implements LifeCycleController;
    declare parents: bench.ClientImpl implements NameController;
    declare parents: bench.ClientImpl implements SuperControllerNotifier;
    declare parents: bench.ClientImpl implements LifeCycleInterceptorItf;
    declare parents: bench.ClientImpl implements ComponentSetterItf;
    
    declare parents: bench.Bench implements PrimitiveType;
    
    declare parents: bench.Bench implements Component;
    declare parents: bench.Bench implements BindingController;
    declare parents: bench.Bench implements LifeCycleController;
    declare parents: bench.Bench implements NameController;
    declare parents: bench.Bench implements SuperControllerNotifier;
    declare parents: bench.Bench implements LifeCycleInterceptorItf;
    declare parents: bench.Bench implements ComponentSetterItf;
}
