/***
 * AOKell E-Store application
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package estore;

import org.objectweb.fractal.aokell.lib.control.component.ComponentSetterItf;
import org.objectweb.fractal.aokell.lib.control.lifecycle.LifeCycleInterceptorItf;
import org.objectweb.fractal.julia.control.content.SuperControllerNotifier;
import org.objectweb.fractal.aokell.lib.membrane.primitive.PrimitiveType;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.NameController;

/**
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public aspect Conf {
    
    declare parents: estore.bank.account.AccountImpl implements PrimitiveType;
    declare parents: estore.bank.account.AccountImpl implements Component;
    declare parents: estore.bank.account.AccountImpl implements BindingController;
    declare parents: estore.bank.account.AccountImpl implements LifeCycleController;
    declare parents: estore.bank.account.AccountImpl implements NameController;
    declare parents: estore.bank.account.AccountImpl implements SuperControllerNotifier;
    declare parents: estore.bank.account.AccountImpl implements LifeCycleInterceptorItf;
    declare parents: estore.bank.account.AccountImpl implements ComponentSetterItf;
    
    declare parents: estore.bank.desk.DeskImpl implements PrimitiveType;
    declare parents: estore.bank.desk.DeskImpl implements Component;
    declare parents: estore.bank.desk.DeskImpl implements BindingController;
    declare parents: estore.bank.desk.DeskImpl implements LifeCycleController;
    declare parents: estore.bank.desk.DeskImpl implements NameController;
    declare parents: estore.bank.desk.DeskImpl implements SuperControllerNotifier;
    declare parents: estore.bank.desk.DeskImpl implements LifeCycleInterceptorItf;
    declare parents: estore.bank.desk.DeskImpl implements ComponentSetterItf;
    
    declare parents: estore.client.ClientImpl implements PrimitiveType;
    declare parents: estore.client.ClientImpl implements Component;
    declare parents: estore.client.ClientImpl implements BindingController;
    declare parents: estore.client.ClientImpl implements LifeCycleController;
    declare parents: estore.client.ClientImpl implements NameController;
    declare parents: estore.client.ClientImpl implements SuperControllerNotifier;
    declare parents: estore.client.ClientImpl implements LifeCycleInterceptorItf;
    declare parents: estore.client.ClientImpl implements ComponentSetterItf;
    
    declare parents: estore.provider.ProviderImpl implements PrimitiveType;
    declare parents: estore.provider.ProviderImpl implements Component;
    declare parents: estore.provider.ProviderImpl implements BindingController;
    declare parents: estore.provider.ProviderImpl implements LifeCycleController;
    declare parents: estore.provider.ProviderImpl implements NameController;
    declare parents: estore.provider.ProviderImpl implements SuperControllerNotifier;
    declare parents: estore.provider.ProviderImpl implements LifeCycleInterceptorItf;
    declare parents: estore.provider.ProviderImpl implements ComponentSetterItf;
    
    declare parents: estore.store.StoreImpl implements PrimitiveType;
    declare parents: estore.store.StoreImpl implements Component;
    declare parents: estore.store.StoreImpl implements BindingController;
    declare parents: estore.store.StoreImpl implements LifeCycleController;
    declare parents: estore.store.StoreImpl implements NameController;
    declare parents: estore.store.StoreImpl implements SuperControllerNotifier;
    declare parents: estore.store.StoreImpl implements LifeCycleInterceptorItf;
    declare parents: estore.store.StoreImpl implements ComponentSetterItf;    
}
