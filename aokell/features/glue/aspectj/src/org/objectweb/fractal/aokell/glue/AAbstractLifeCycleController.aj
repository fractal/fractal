/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;

import org.objectweb.fractal.aokell.lib.control.component.ComponentItf;
import org.objectweb.fractal.aokell.lib.control.lifecycle.LifeCycleCoordinatorItf;
import org.objectweb.fractal.aokell.lib.membrane.marker.LifeCycleType;
import org.objectweb.fractal.aokell.lib.membrane.marker.LifeCycleInterceptorType;
import org.objectweb.fractal.aokell.lib.util.FractalHelper;


/**
 * This aspect introduces the features provided by the lifecycle controller.
 * These features are introduced for all classes that implement the
 * LifeCycleType or the LifeCycleInterceptorType marker interfaces.
 * 
 * This aspect is abstract because control methods usually need to be excluded
 * from the lifecycle. By leaving the pointcut capturing these methods abstract,
 * new control methods can be added by extending this aspect.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public abstract aspect AAbstractLifeCycleController {
        
    // -----------------------------------------------------------
    // Introduction of the LifeCyleController interface
    // -----------------------------------------------------------
    
    public String LifeCycleType.getFcState() {
        LifeCycleController _lc = FractalHelper.getLifeCycleController(_fcComp);
        return _lc.getFcState();
    }

    public void LifeCycleType.startFc() throws IllegalLifeCycleException {
        LifeCycleController _lc = FractalHelper.getLifeCycleController(_fcComp);
        _lc.startFc();
    }

    public void LifeCycleType.stopFc() throws IllegalLifeCycleException {
        LifeCycleController _lc = FractalHelper.getLifeCycleController(_fcComp);
        _lc.stopFc();
    }
    

    // -----------------------------------------------------------
    // Introduction of the LifeCyleInterceptorItf interface
    // -----------------------------------------------------------
    
    /**
     * Introduced field to store the state of the lifecycle controller.
     * This field caches the state which is managed by the lifecycle controller.
     * This field is used to provide a more performant test on the lifecycle
     * controller state (saves a method call).
     */
    private boolean LifeCycleInterceptorType.isFcStarted = false;
    
    public void LifeCycleInterceptorType.setFcState( boolean isFcStarted ) {
        this.isFcStarted = isFcStarted;
    }
     
    /**
     * Advice all business methods of a component (i.e. any method that is not
     * a controller method or a method of the java.lang.Object interface).
     */
    pointcut contentMethodsUnderLifecycleControl( LifeCycleInterceptorType adviced ):
        execution( * LifeCycleInterceptorType+.*(..) ) &&
        !controllerMethodsExecution() && !jlObjectMethodsExecution() &&
        target(adviced);
    
    before(LifeCycleInterceptorType adviced) :
        contentMethodsUnderLifecycleControl(adviced) {
        
        if( ! adviced.isFcStarted ) {
            throw new RuntimeException(
                    "Component "+adviced+
                    " must be started before accepting method calls");
        }
    }
    
    /**
     * Pointcut for method executions of controllers.
     * This pointcut is left abstract to allow extending the set of control
     * methods.
     */
    abstract pointcut controllerMethodsExecution();
    
    /** Pointcut for method executions of the java.lang.Object interface. */
    pointcut jlObjectMethodsExecution() :
        execution( Object *.clone() ) ||
        execution( boolean *.equals(Object) ) ||
        execution( void *.finalize() ) ||
        execution( Class *.getClass() ) ||
        execution( int *.hashCode() ) ||
        execution( void *.notify() ) ||
        execution( void *.notifyAll() ) ||
        execution( String *.toString() ) ||
        execution( void *.wait() ) ||
        execution( void *.wait(long) ) ||
        execution( void *.wait(long,int) );    
}
