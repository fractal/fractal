/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue;

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.control.SuperController;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.Factory;

import org.objectweb.fractal.aokell.lib.control.lifecycle.LifeCycleCoordinatorItf;


/**
 * This aspect defines the methods which are control methods and which must be
 * excluded from the lifecycle control.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public aspect ALifeCycleController extends AAbstractLifeCycleController {
        
    /** Pointcut for method executions of controllers. */
    pointcut controllerMethodsExecution():
        attributeControllerExecution() ||
        bindingControllerExecution() ||
        contentControllerExecution() ||
        componentInterfaceExecution() ||
        factoryControllerExecution() ||
        lifecycleControllerExecution() ||
        nameControllerExecution() ||
        superControllerExecution();
        
    /** Pointcut for method executions of the attribute controller. */
    pointcut attributeControllerExecution():
        execution(void *.set*(..)) ||
        execution(* *.get*());
    
    /** Pointcut for method executions of the binding controller. */
    pointcut bindingControllerExecution():
        execution( * BindingController.*(..) );
//        execution( String[] *.listFc() ) ||
//        execution( Object *.getFc(String) ) ||
//        execution( void *.bindFc(String,Object) ) ||
//        execution( void *.unbindFc(String) ) ||
//        execution( BindingController *.getFcBindingController() ) ||
//        execution( BindingController *.createFcBindingController() );
    
    /** Pointcut for method executions of the Component interface. */
    pointcut componentInterfaceExecution():
        execution( * Component.*(..) );
//        execution( void *.initFc(Type,Object,Object) ) ||
//        execution( Type *.getFcType() ) ||
//        execution( Object *.getFcInterface(String) ) ||
//        execution( Object[] *.getFcInterfaces() ) ||
//        execution( Object *.getControllerDesc() ) ||
//        execution( Object *.getContentDesc() );

    /** CPointcut for method executions of the content controller. */
    pointcut contentControllerExecution():
        execution( * ContentController.*(..) );
//        execution( Object[] *.getFcInternalInterfaces() ) ||
//        execution( Object *.getFcInternalInterface(String) ) ||
//        execution( Component[] *.getFcSubComponents() ) ||
//        execution( void *.addFcSubComponent(Component) ) ||
//        execution( void *.removeFcSubComponent(Component) ) || 
//        execution( ContentController *.getFcContentController() );

    /** Pointcut for method executions of the Factory interface. */
    pointcut factoryControllerExecution():
        execution( * Factory.*(..) );
//        execution( Object *.getFcContentDesc() ) ||
//        execution( Object *.getFcControllerDesc() ) ||
//        execution( Type *.getFcInstanceType() ) ||
//        execution( Component *.newFcInstance() ) ||
//        execution( Factory *.getFcFactory() );
    
    /** Pointcut for method executions of the life cycle controller. */
    pointcut lifecycleControllerExecution():
        execution( * LifeCycleCoordinatorItf.*(..) );
//        execution( String *.getFcState() ) ||
//        execution( void *.startFc() ) ||
//        execution( void *.stopFc() ) ||
//        execution( boolean *.isFcStarted() ) ||
//        execution( LifeCycleCoordinatorItf *.getFcLifeCycleController() );
    
    /** Pointcut for method executions of the name controller. */
    pointcut nameControllerExecution():
        execution( * NameController.*(..) );
//        execution( String *.getFcName() ) ||
//        execution( void *.setFcName(String) ) ||
//        execution( NameController *.getFcNameController() );
    
    /** Pointcut for method executions of the super controller. */
    pointcut superControllerExecution():
        execution( * SuperController.*(..) );
//        execution( Component[] *.getFcSuperComponents() ) ||
//        execution( void *.addFcSuperComponent(Component) ) ||
//        execution( void *.removeFcSuperComponent(Component) ) ||
//        execution( SuperController *.getFcSuperController() );
    
}
