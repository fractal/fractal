/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

import org.objectweb.fractal.aokell.lib.control.component.ComponentItf;
import org.objectweb.fractal.aokell.lib.membrane.marker.BindingType;
import org.objectweb.fractal.aokell.lib.util.FractalHelper;


/**
 * This aspect introduces the features provided by the binding controller.
 * These features are introduced for all classes that implement the BindingType
 * marker interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public aspect ABindingController {
        
    // -----------------------------------------------------------
    // Introduction of the BindingController interface
    // -----------------------------------------------------------
    
    declare parents: BindingType+ implements BindingController;
    
    public String[] BindingType.listFc() {
        BindingController _bc = FractalHelper.getBindingController(_fcComp);
        return _bc.listFc();
    }
    
    public Object BindingType.lookupFc(final String cItf)
    throws NoSuchInterfaceException {
        BindingController _bc = FractalHelper.getBindingController(_fcComp);
        return _bc.lookupFc(cItf);
    }

    public void BindingType.bindFc(final String cItf, final Object sItf)
    throws NoSuchInterfaceException, IllegalBindingException,
    IllegalLifeCycleException {
        BindingController _bc = FractalHelper.getBindingController(_fcComp);
        _bc.bindFc(cItf,sItf);
    }

    public void BindingType.unbindFc(final String cItf)
    throws NoSuchInterfaceException, IllegalBindingException,
    IllegalLifeCycleException {
        BindingController _bc = FractalHelper.getBindingController(_fcComp);
        _bc.unbindFc(cItf);
    }

}
