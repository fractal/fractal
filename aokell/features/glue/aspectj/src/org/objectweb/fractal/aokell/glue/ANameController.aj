/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue;

import org.objectweb.fractal.api.control.NameController;

import org.objectweb.fractal.aokell.lib.control.component.ComponentItf;
import org.objectweb.fractal.aokell.lib.membrane.marker.BaseType;
import org.objectweb.fractal.aokell.lib.membrane.marker.NameType;
import org.objectweb.fractal.aokell.lib.util.FractalHelper;


/**
 * This aspect introduces the features provided by the name controller.
 * These features are introduced for all classes that implement the NameType
 * marker interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public aspect ANameController {
        
    // -----------------------------------------------------------
    // Introduction of the NameController interface
    // -----------------------------------------------------------
    
    declare parents: NameType+ implements NameController;
    
    public String NameType.getFcName() {
        NameController _nc = FractalHelper.getNameController(_fcComp);
        return _nc.getFcName();
    }

    public void NameType.setFcName(String arg0) {
        NameController _nc = FractalHelper.getNameController(_fcComp);
        _nc.setFcName(arg0);
    }
    
}
