/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

import org.objectweb.fractal.aokell.lib.control.component.ComponentItf;
import org.objectweb.fractal.aokell.lib.membrane.marker.ContentType;
import org.objectweb.fractal.aokell.lib.util.FractalHelper;


/**
 * This aspect introduces the features provided by the content controller.
 * These features are introduced for all classes that implement the
 * ContentType marker interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public aspect AContentController {
        
    // -----------------------------------------------------------
    // Introduction of the ContentController interface
    // -----------------------------------------------------------
    
    declare parents: ContentType+ implements ContentController;
    
    public Object[] ContentType.getFcInternalInterfaces() {
        ContentController _cc = FractalHelper.getContentController(_fcComp);
        return _cc.getFcInternalInterfaces();
    }

    public Object ContentType.getFcInternalInterface(String arg0)
    throws NoSuchInterfaceException {
        ContentController _cc = FractalHelper.getContentController(_fcComp);
        return _cc.getFcInternalInterface(arg0);
    }

    public Component[] ContentType.getFcSubComponents() {
        ContentController _cc = FractalHelper.getContentController(_fcComp);
        return _cc.getFcSubComponents();
    }

    public void ContentType.addFcSubComponent(Component arg0)
    throws IllegalContentException, IllegalLifeCycleException {
        ContentController _cc = FractalHelper.getContentController(_fcComp);
        _cc.addFcSubComponent(arg0);
    }

    public void ContentType.removeFcSubComponent(Component arg0)
    throws IllegalContentException, IllegalLifeCycleException {
        ContentController _cc = FractalHelper.getContentController(_fcComp);
        _cc.removeFcSubComponent(arg0);
    }
    
}
