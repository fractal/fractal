/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.InstantiationException;

import org.objectweb.fractal.aokell.lib.control.component.ComponentItf;
import org.objectweb.fractal.aokell.lib.membrane.marker.TemplateType;
import org.objectweb.fractal.aokell.lib.util.FractalHelper;


/**
 * This aspect introduces the features provided by the Factory interface.
 * These features are introduced for all classes that implement the TemplateType
 * marker interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public aspect ATemplate {
        
    // -----------------------------------------------------------
    // Introduction of the Factory interface
    // -----------------------------------------------------------
    
    public Object TemplateType.getFcContentDesc() {
        Factory _fc = FractalHelper.getFactory(_fcComp);
        return _fc.getFcContentDesc();
    }
    
    public Object TemplateType.getFcControllerDesc() {
        Factory _fc = FractalHelper.getFactory(_fcComp);
        return _fc.getFcControllerDesc();
    }
    
    public Type TemplateType.getFcInstanceType() {
        Factory _fc = FractalHelper.getFactory(_fcComp);
        return _fc.getFcInstanceType();
    }
    
    public Component TemplateType.newFcInstance()
    throws InstantiationException {
        Factory _fc = FractalHelper.getFactory(_fcComp);
        return _fc.newFcInstance();
    }
    
}
