/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue;

import org.objectweb.fractal.api.Component;

import org.objectweb.fractal.aokell.lib.control.component.ComponentItf;
import org.objectweb.fractal.julia.control.content.SuperControllerNotifier;
import org.objectweb.fractal.aokell.lib.membrane.marker.SuperType;
import org.objectweb.fractal.aokell.lib.util.FractalHelper;


/**
 * This aspect introduces the features provided by the super controller.
 * These features are introduced for all classes that implement the
 * SuperType marker interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public aspect ASuperController {
        
    // -----------------------------------------------------------
    // Introduction of the SuperController interface
    // -----------------------------------------------------------
    
    public Component[] SuperType.getFcSuperComponents() {
        SuperControllerNotifier _sc = FractalHelper.getSuperController(_fcComp);
        return _sc.getFcSuperComponents();
    }
    
    // -----------------------------------------------------------
    // Introduction of the SuperControllerNotifier interface
    // -----------------------------------------------------------

    public void SuperType.addedToFc( Component c ) {
        SuperControllerNotifier _sc = FractalHelper.getSuperController(_fcComp);
        _sc.addedToFc(c);
    }
    
    public void SuperType.removedFromFc( Component c ) {
        SuperControllerNotifier _sc = FractalHelper.getSuperController(_fcComp);
        _sc.removedFromFc(c);
    }
    
}
