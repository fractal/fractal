/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;

import org.objectweb.fractal.aokell.lib.control.component.ComponentItf;
import org.objectweb.fractal.aokell.lib.membrane.marker.BaseType;


/**
 * This aspect introduces the features provided by the Component interface.
 * These features are introduced for all classes that implement the BaseType
 * marker interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public aspect AComponent {
    
    /**
     * The reference towards the component controller.
     * This reference is public to be visible by other aspects.
     */
    public ComponentItf BaseType._fcComp;
    
    
    // -----------------------------------------------------------
    // Introduction of the Component interface
    // -----------------------------------------------------------
    
    declare parents: BaseType+ implements Component;
    
    public Type BaseType.getFcType() {
        return _fcComp.getFcType();
    }

    public Object BaseType.getFcInterface(String interfaceName)
    throws NoSuchInterfaceException {
        return _fcComp.getFcInterface(interfaceName);
    }
    
    public Object[] BaseType.getFcInterfaces() {
        return _fcComp.getFcInterfaces();
    }

    
    // ------------------------------------------------------------------
    // Introduction of the ComponentSetterItf interface
    // ------------------------------------------------------------------

    public void BaseType.setFcCompCtrl( ComponentItf _fcComp ) {
        this._fcComp = _fcComp;
    }
}
