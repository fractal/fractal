/***
 * AOKell
 * Copyright (C) 2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package comanchemain;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.aokell.glue.processor.ItfImplProcessor;
import org.objectweb.fractal.aokell.lib.membrane.primitive.PrimitiveType;


/**
 * This processor introduces the component marker types into the component
 * implementations of the comanche example.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class ConfProcessor extends ItfImplProcessor {
    
    public ConfProcessor() {
        super();
        setMarkers(markers);
    }
    
    /**
     * Store association between component implementations and their component
     * marker type.
     */
    private static Map<String,Class> markers = new HashMap<String,Class>();
    
    static {
        markers.put( comanche.BasicLogger.class.getName(), PrimitiveType.class );
        markers.put( comanche.ErrorRequestHandler.class.getName(), PrimitiveType.class );
        markers.put( comanche.FileRequestHandler.class.getName(), PrimitiveType.class );
        markers.put( comanche.MultiThreadScheduler.class.getName(), PrimitiveType.class );
        markers.put( comanche.RequestAnalyzer.class.getName(), PrimitiveType.class );
        markers.put( comanche.RequestDispatcher.class.getName(), PrimitiveType.class );
        markers.put( comanche.RequestReceiver.class.getName(), PrimitiveType.class );
        markers.put( comanche.SequentialScheduler.class.getName(), PrimitiveType.class );
    }

}
