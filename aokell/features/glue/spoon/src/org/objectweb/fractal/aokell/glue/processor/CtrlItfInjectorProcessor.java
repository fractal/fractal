/***
 * AOKell
 * Copyright (C) 2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue.processor;


import org.objectweb.fractal.aokell.glue.SpoonHelper;
import org.objectweb.fractal.aokell.glue.template.BindingControllerTemplate;
import org.objectweb.fractal.aokell.glue.template.ComponentControllerTemplate;
import org.objectweb.fractal.aokell.glue.template.ContentControllerTemplate;
import org.objectweb.fractal.aokell.glue.template.LifeCycleControllerTemplate;
import org.objectweb.fractal.aokell.glue.template.NameControllerTemplate;
import org.objectweb.fractal.aokell.glue.template.SuperControllerTemplate;
import org.objectweb.fractal.aokell.glue.template.TemplateControllerTemplate;
import org.objectweb.fractal.aokell.lib.membrane.marker.BaseType;
import org.objectweb.fractal.aokell.lib.membrane.marker.BindingType;
import org.objectweb.fractal.aokell.lib.membrane.marker.ContentType;
import org.objectweb.fractal.aokell.lib.membrane.marker.LifeCycleType;
import org.objectweb.fractal.aokell.lib.membrane.marker.NameType;
import org.objectweb.fractal.aokell.lib.membrane.marker.SuperType;
import org.objectweb.fractal.aokell.lib.membrane.marker.TemplateType;

import spoon.processing.AbstractProcessor;
import spoon.reflect.declaration.CtClass;
import spoon.template.Template;

/**
 * This processor injects control interfaces into component content classes
 * based on the marker types implemented by these classes.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class CtrlItfInjectorProcessor extends AbstractProcessor<CtClass<?>> {
    
    public CtrlItfInjectorProcessor() {}
    
    final private static Template lifecycleControllerTemplate = new LifeCycleControllerTemplate();
    final private static Template componentControllerTemplate = new ComponentControllerTemplate();
    final private static Template bindingControllerTemplate = new BindingControllerTemplate();
    final private static Template contentControllerTemplate = new ContentControllerTemplate();
    final private static Template nameControllerTemplate = new NameControllerTemplate();
    final private static Template superControllerTemplate = new SuperControllerTemplate();
    final private static Template templateControllerTemplate = new TemplateControllerTemplate();

    public void process(CtClass<?> ct) {        
        SpoonHelper.insert(ct,LifeCycleType.class,lifecycleControllerTemplate);
        SpoonHelper.insert(ct,BaseType.class,componentControllerTemplate);
        SpoonHelper.insert(ct,BindingType.class,bindingControllerTemplate);
        SpoonHelper.insert(ct,ContentType.class,contentControllerTemplate);
        SpoonHelper.insert(ct,NameType.class,nameControllerTemplate);
        SpoonHelper.insert(ct,SuperType.class,superControllerTemplate);
        SpoonHelper.insert(ct,TemplateType.class,templateControllerTemplate);
    }
    
}
