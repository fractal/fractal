/***
 * AOKell
 * Copyright (C) 2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue;

import org.objectweb.fractal.aokell.glue.processor.CtrlItfInjectorProcessor;
import org.objectweb.fractal.aokell.glue.processor.LifeCycleInterceptorProcessor;

import spoon.processing.AbstractProcessor;
import spoon.processing.ProcessingManager;
import spoon.reflect.declaration.CtClass;

/**
 * This is the main Spoon processor for AOKell.
 * This processor simply registers needed processors.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class AOKellProcessor extends AbstractProcessor<CtClass> {
    
    public AOKellProcessor() {
    }
    
    public boolean isToBeProcessed(CtClass ct) {
        return first;
    }

    private boolean first = true;

    public void process(CtClass ct) {
        
        ProcessingManager pm = getManager();
        pm.addProcessor( new LifeCycleInterceptorProcessor() );
        pm.addProcessor( new CtrlItfInjectorProcessor() );
        
        first = false;
    }
    
}
