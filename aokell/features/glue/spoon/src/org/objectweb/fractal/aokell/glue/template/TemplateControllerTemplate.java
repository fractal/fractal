/***
 * AOKell
 * Copyright (C) 2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue.template;

import org.objectweb.fractal.aokell.lib.control.component.ComponentItf;
import org.objectweb.fractal.aokell.lib.util.FractalHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.InstantiationException;

import spoon.template.Local;
import spoon.template.Template;

/**
 * This template defines the elements which are introduced in all classes
 * which are processed by TemplateControllerProcessor.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class TemplateControllerTemplate implements Template, Factory {
    
    @Local
    public TemplateControllerTemplate() {}
    
    /**
     * The reference towards the component controller.
     * This field is injected by the ComponentControllerTemplate.
     */
    @Local
    private ComponentItf _fcComp;

    
    // -----------------------------------------------------------
    // TemplateController interface implementation
    // -----------------------------------------------------------
    
    public Object getFcContentDesc() {
        Factory _fc = FractalHelper.getFactory(_fcComp);
        return _fc.getFcContentDesc();
    }
    
    public Object getFcControllerDesc() {
        Factory _fc = FractalHelper.getFactory(_fcComp);
        return _fc.getFcControllerDesc();
    }
    
    public Type getFcInstanceType() {
        Factory _fc = FractalHelper.getFactory(_fcComp);
        return _fc.getFcInstanceType();
    }
    
    public Component newFcInstance()
    throws InstantiationException {
        Factory _fc = FractalHelper.getFactory(_fcComp);
        return _fc.newFcInstance();
    }
}