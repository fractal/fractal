/***
 * AOKell
 * Copyright (C) 2006 INRIA, France Telecom, USTL, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue.processor;

import java.util.Map;
import java.util.Set;

import spoon.processing.AbstractProcessor;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.reference.CtTypeReference;

/**
 * This processor adds an interface in the list of interfaces implemented by a
 * class. This processor is typically used to introduce marker interfaces.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class ItfImplProcessor extends AbstractProcessor<CtClass<?>> {
    
    public ItfImplProcessor() {
        super();
    }
    
    /**
     * Store association between fully-qualified names of classes and the
     * interface which must be introduced.
     */
    private Map<String,Class> markers;
    
    public void setMarkers( Map<String,Class> markers ) {
        this.markers = markers;        
    }
    
    public void process(CtClass<?> ct) {
        /*
         * Add the marker type to the list of super interfaces implemented by
         * the processed class.
         */
        Set<CtTypeReference<?>> supers = ct.getSuperInterfaces();
        Class<?> marker = markers.get(ct.getQualifiedName());
        supers.add(getFactory().Type().createReference(marker));
    }
    
    public boolean isToBeProcessed(CtClass<?> ct) {        
        /*
         * Only classes which are registered with markers need to be processed.
         */
        return markers.containsKey(ct.getQualifiedName());
    }
}
