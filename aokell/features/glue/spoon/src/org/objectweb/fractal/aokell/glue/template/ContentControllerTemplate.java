/***
 * AOKell
 * Copyright (C) 2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue.template;

import org.objectweb.fractal.aokell.lib.control.component.ComponentItf;
import org.objectweb.fractal.aokell.lib.util.FractalHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

import spoon.template.Local;
import spoon.template.Template;

/**
 * This template defines the elements which are introduced in all classes
 * which are processed by LifeCycleControllerProcessor.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class ContentControllerTemplate implements Template, ContentController {
    
    @Local
    public ContentControllerTemplate() {}
    
    /**
     * The reference towards the component controller.
     * This field is injected by the ComponentControllerTemplate.
     */
    @Local
    private ComponentItf _fcComp;

    
    // -----------------------------------------------------------
    // ContentController interface implementation
    // -----------------------------------------------------------
    
    public Object[] getFcInternalInterfaces() {
        ContentController _cc = FractalHelper.getContentController(_fcComp);
        return _cc.getFcInternalInterfaces();
    }

    public Object getFcInternalInterface(String arg0)
    throws NoSuchInterfaceException {
        ContentController _cc = FractalHelper.getContentController(_fcComp);
        return _cc.getFcInternalInterface(arg0);
    }

    public Component[] getFcSubComponents() {
        ContentController _cc = FractalHelper.getContentController(_fcComp);
        return _cc.getFcSubComponents();
    }

    public void addFcSubComponent(Component arg0)
    throws IllegalContentException, IllegalLifeCycleException {
        ContentController _cc = FractalHelper.getContentController(_fcComp);
        _cc.addFcSubComponent(arg0);
    }

    public void removeFcSubComponent(Component arg0)
    throws IllegalContentException, IllegalLifeCycleException {
        ContentController _cc = FractalHelper.getContentController(_fcComp);
        _cc.removeFcSubComponent(arg0);
    }
    
}