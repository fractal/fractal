/***
 * AOKell
 * Copyright (C) 2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue.processor;

import java.util.Set;

import org.objectweb.fractal.aokell.glue.SpoonHelper;
import org.objectweb.fractal.aokell.glue.template.LifeCycleInterceptorBeforeTemplate;
import org.objectweb.fractal.aokell.glue.template.LifeCycleInterceptorTemplate;
import org.objectweb.fractal.aokell.lib.membrane.marker.LifeCycleInterceptorType;
import org.objectweb.fractal.aokell.lib.util.FractalHelper;

import spoon.processing.AbstractProcessor;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;
import spoon.template.Template;

/**
 * This processor introduces the features releted to the interception performed
 * by the lifecycle controller.
 * These features are introduced for all classes that implement the
 * LifeCycleInterceptorType marker interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class LifeCycleInterceptorProcessor extends AbstractProcessor<CtClass<?>> {

    public boolean isToBeProcessed(CtClass<?> ct) {
        return SpoonHelper.impls(ct,LifeCycleInterceptorType.class);
    }
    
    private static Template lifecycleInterceptorTemplate =
        new LifeCycleInterceptorTemplate();

    public void process(CtClass<?> ct) {
        
        /*
         * Insert the implementation of the LifeCycleInterceptorItf interface.
         * The 2nd parameter is really needed and is not redundant with
         * isTobeProcessed(). Read the code the insert method. 
         */
        SpoonHelper.insert(
                ct, LifeCycleInterceptorType.class,
                lifecycleInterceptorTemplate);
        
        /*
         * Insert before each method of the processed class, a if statement to
         * test whether the component is started or not.
         */
        Set<CtMethod<?>> methods = ct.getMethods();
        for( CtMethod<?> method : methods ) {
            String methodName = method.getSimpleName();
            
            // Skip control methods and methods not defined in an interface
            if( ! FractalHelper.isFcControllerMethodName(methodName) &&
                SpoonHelper.isInterfaceMethodImplementation(ct,method) ) {
                method.getBody().insertBegin(
                        new LifeCycleInterceptorBeforeTemplate().
                        getSubstitution(method.getDeclaringType())
                );
            }
        }
    }
    
}
