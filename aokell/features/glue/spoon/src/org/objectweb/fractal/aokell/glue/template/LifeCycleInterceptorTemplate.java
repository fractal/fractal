/***
 * AOKell
 * Copyright (C) 2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue.template;

import org.objectweb.fractal.aokell.lib.control.lifecycle.LifeCycleInterceptorItf;

import spoon.template.Local;
import spoon.template.Template;

/**
 * Template applied to all classes which are processed by
 * LifeCycleControllerProcessor. This template defines the _isFcStarted
 * field which will be introduced and the statements which will be inserted
 * at the beginning of all methods.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class LifeCycleInterceptorTemplate
    implements Template, LifeCycleInterceptorItf {
    
    @Local
    public LifeCycleInterceptorTemplate() {}

    /**
     * Introduced field to store the state of the lifecycle controller. This
     * field caches the state which is managed by the lifecycle controller.
     * This field is used to provide a more performant test on the lifecycle
     * controller state (saves a method call).
     * 
     * Visibility set to protected to let the field be visible by subclasses.
     */
    protected boolean isFcStarted = false;
    
    /**
     * Set the state of the component.
     * 
     * @param isFcStarted  true if the component is started
     */
    public void setFcState( boolean isFcStarted ) {
        this.isFcStarted = isFcStarted;
    }    
}
