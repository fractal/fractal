/***
 * AOKell
 * Copyright (C) 2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue.template;

import spoon.template.Local;
import spoon.template.StatementListTemplateParameter;
import spoon.template.Template;

/**
 * Template applied to all classes which are processed by
 * LifeCycleControllerProcessor. This template defines some before code inserted
 * at the beginning of all processed methods.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class LifeCycleInterceptorBeforeTemplate
    extends StatementListTemplateParameter implements Template {
    
    /**
     * Define the code which will be introduced before each method of the
     * processed class.
     */
    public void statements() {
        if( ! isFcStarted ) {
            throw new RuntimeException(
                    "Component "+this+
                    " must be started before accepting method calls");
        }
        
    }
    
    @Local
    private boolean isFcStarted = false;    
}