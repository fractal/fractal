/***
 * AOKell
 * Copyright (C) 2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue;

import java.util.List;
import java.util.Set;

import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtInterface;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtParameter;
import spoon.reflect.factory.TypeFactory;
import spoon.reflect.reference.CtTypeReference;
import spoon.template.Substitution;
import spoon.template.Template;

/**
 * Helper class for working with Spoon.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class SpoonHelper {

    /**
     * If the given method implements a method declared in an interface of the
     * given class, return true. Else return false.
     */
    public static boolean isInterfaceMethodImplementation(
            CtClass<?> cl, CtMethod<?> meth ) {
        
        String name = meth.getSimpleName();
        List<CtParameter<?>> params = meth.getParameters();
        Class[] paramClassTypes = getClassParameterTypes(params);
        CtTypeReference<?>[] paramCtRefTypes = getTypeRefParameterTypes(params);
        CtTypeReference<?> retType = meth.getType();
        
        Set<CtTypeReference<?>> impls = cl.getSuperInterfaces();
        for (CtTypeReference<?> impl : impls) {
            String qname = impl.getQualifiedName();
            try {
                Class itf = Class.forName(qname);
                try {
                    itf.getMethod(name,paramClassTypes);
                    return true;
                }
                catch( NoSuchMethodException nsme ) {}
                // The method is not defined in the current interface
            }
            catch( ClassNotFoundException cnfe ) {
                
                /*
                 * The interface can not be loaded with the Java reflection API.
                 * Try to check whether it is available in the Spoon model.
                 * 
                 * Note: conversely some interfaces can be loaded with the
                 * reflection API but are not in the Spoon model. This is the
                 * case for any interface (e.g. java.lang.Runnable) which is not
                 * in the Spoon source set. 
                 */
                
                CtInterface<?> ctitf = cl.getFactory().Interface().get(qname);
                if( ctitf == null ) {
                    throw new RuntimeException(qname+" not found");                    
                }
                CtMethod<?> m = ctitf.getMethod(retType,name,paramCtRefTypes);
                if( m != null ) {
                    return true;
                }                
            }
            
        }
        
        return false;
    }
    
       
    /**
     * Return the type references of the given list of parameters.
     */
    public static CtTypeReference<?>[] getTypeRefParameterTypes( List<CtParameter<?>> params ) {
        
        CtTypeReference[] result = new CtTypeReference[params.size()];
        int i = 0;
        for (CtParameter<?> param : params) {
            CtTypeReference<?> type = param.getType();
            TypeFactory tf = param.getFactory().Type();
            String qname = type.getQualifiedName();
            result[i] = tf.createReference(qname);
            i++;
        }
        
        return result;
    }

    /**
     * Return the classes of the given list of parameters.
     */
    public static Class<?>[] getClassParameterTypes( List<CtParameter<?>> params ) {
        
        Class[] result = new Class[params.size()];
        int i = 0;
        for (CtParameter<?> param : params) {
            CtTypeReference<?> type = param.getType();
            result[i] = type.getActualClass();
            if( result[i] == null ) {
                final String qname = type.getQualifiedName();
                throw new RuntimeException(qname+" not found");   
            }
            i++;
        }
        
        return result;
    }

    /**
     * Return true if one of the subclasses of the given CtClass implements the
     * given interface.
     */
    public static boolean deepImpls( CtClass<?> ct, Class<?> itf ) {
        
        /*
         * Recurse in super classes.
         */
        CtTypeReference<?> suptr = ct.getSuperclass();
        if( suptr == null ) {
            // No super class
            return false;
        }
        
        Class<?> supcl = suptr.getActualClass();
        CtClass<?> supctcl = ct.getFactory().Class().get(supcl);
        if( supctcl == null ) {
            // The super class is not in the Spoon model and will not be spooned.
            // Use Java reflection to check whether the interface is implemented.
            return itf.isAssignableFrom(supcl);
        }
        
        // Check whether the super class implements directly the interface
        if( impls(supctcl,itf) ) {
            return true;
        }
        
        return deepImpls(supctcl,itf);
    }
    
    /**
     * Return true if the given CtClass implements directly the given interface.
     * This method does not recurse in super classes and super interfaces and
     * simply checks the presence of the given interface in the list of
     * interfaces of the implements clause.
     */
    public static boolean impls( CtClass ct, Class<?> itf ) {
        
        Set<CtTypeReference<?>> supers = ct.getSuperInterfaces();
        CtTypeReference itfctr = ct.getFactory().Type().createReference(itf);
        
        /*
         * Check whether the interface is a super-type of one of the interfaces
         * declared in the implements clause of the CtClass.
         */
        for (CtTypeReference<?> sup : supers) {
            if( sup.isSubtypeOf(itfctr) ) {
                return true;
            }
        }
        
        return false;
    }

    /**
     * Insert a template in a CtClass
     * if the CtClass implements the given type and
     * if none of the super classes of the CtClass already implements the given
     * type (to avoid duplicate insertion of the template).
     * 
     * @param ct           the CtClass where the insertion must be performed
     * @param typeToMatch  the type to match (a class or an interface)
     * @param template     the Spoon template containing the code to insert
     */
    public static void insert( CtClass<?> ct, Class<?> typeToMatch, Template template ) {
        
        if( ! impls(ct,typeToMatch) ) {
            return;
        }
        if( deepImpls(ct,typeToMatch) ) {
            return;
        }
        
        insert(ct,template);
    }

    /**
     * Insert a template in a CtClass.
     * 
     * @param ct           the CtClass where the insertion must be performed
     * @param template     the Spoon template containing the code to insert
     */
    public static void insert( CtClass<?> ct, Template template ) {
        
        // Insert the template
        Substitution.insertAll(ct,template);
        
        // Add implemented interfaces (except Template)
        CtTypeReference<?> templtr = ct.getFactory().Type().createReference(Template.class);
        Class<? extends Template> c = template.getClass();
        CtClass<?> t = ct.getFactory().Template().get(c);
        Set<CtTypeReference<?>> supers = t.getSuperInterfaces();
        for (CtTypeReference<?> sup : supers) {
            if( ! templtr.isAssignableFrom(sup) ) {
                ct.getSuperInterfaces().add(sup);
            }
        }
    }
}
