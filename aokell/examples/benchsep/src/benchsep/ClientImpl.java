/***
 * JACBenchmark
 * Copyright (C) 2002 AOPSYS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: laurent@aopsys.com
 * Contact: Nicolas.Pessemier@lifl.fr
 *
 * Author: Laurent Martelli
 * Author: Nicolas Pessemier
 */

package benchsep;

import org.objectweb.fractal.api.control.BindingController;

/**
 * @author Nicolas Pessemier <Nicolas.Pessemier@lifl.fr>
 */
public class ClientImpl implements Runnable, BindingController {

    private BenchInterface b;

    boolean rCalls;



    private int n;
    String s;
    
    // from Main interface

    public void run() {
        
        
      this.n = 1000000;
      this.rCalls=false;
           
      // Let the VM optimize
      System.out.print("Warming up ");
      doBench(); System.out.print(".");
      doBench(); System.out.print(".");
      doBench(); System.out.print(".");
      doBench(); System.out.println(".");
      //Thread.currentThread().sleep(5000);

      System.out.println("Starting bench");
      long total = 0;
      int num_runs = 5;
      for (int i=0; i<num_runs; i++) {
         long start = System.currentTimeMillis();
         doBench();
         long end = System.currentTimeMillis();
         System.out.print(i+": "+(end-start)+"ms; ");
         total += end-start;
      }
      System.out.println("	average="+(total/num_runs)+"ms; ");
    }

    void doBench() {
           sbench1();
           sbench2();
           sbench3();
           sbench4();
           sbench5();
           sbench6();
           sbench7();
           sbench8();
     }



     void sbench1() {
         for(int i=0; i<n; i++) {
            b.m1();
         }
      }
     
     void sbench2() {
         //System.out.print("bench 2: ");
         for(int i=0; i<n; i++) {
            b.m2(new Integer(0));
         }
      }

     void sbench3() {
         for(int i=0; i<n; i++) {
            b.m3(new Integer(0),new Integer(0));
         }
      }
     
     void sbench4() {
         for(int i=0; i<n; i++) {
            b.m4();
         }
      }
     
     void sbench5() {
         for(int i=0; i<n; i++) {
            b.m5(new Integer(0));
         }
      }

     void sbench6() {
         for(int i=0; i<n; i++) {
            b.m6(new Integer(0),new Integer(0));
         }
      }
     
     void sbench7() {
         for(int i=0; i<n; i++) {
            b.m7(null);
         }
      }
     
     void sbench8() {
         for(int i=0; i<n; i++) {
            b.m8(null,null);
         }
      }
     
     public String[] listFc () {
         return new String[] { "bench" };
       }

       public Object lookupFc (final String cItf) {
         if (cItf.equals("bench")) {
           return b;
         }
         return null;
       }

       public void bindFc (final String cItf, final Object sItf) {
        if (cItf.equals("bench")) {
           b = (BenchInterface)sItf;
         }
       }

       public void unbindFc (final String cItf) {
         if (cItf.equals("bench")) {
           b = null;
         }
       } 
}
