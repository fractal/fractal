/***
 * Comanche web server
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Eric Bruneton
 * Author: Lionel Seinturier
 */

package comanche;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;

public class RequestDispatcher implements RequestHandler /*, BindingController */ {

//  private Map handlers = new TreeMap();
//  // configuration aspect
//  public String[] listFc () {
//    return (String[])handlers.keySet().toArray(new String[handlers.size()]);
//  }
//  public Object lookupFc (String itfName) {
//    if (itfName.startsWith("h")) { return handlers.get(itfName); }
//    else return null;
//  }
//  public void bindFc (String itfName, Object itfValue) {
//    if (itfName.startsWith("h")) { handlers.put(itfName, itfValue); }
//  }
//  public void unbindFc (String itfName) {
//    if (itfName.startsWith("h")) { handlers.remove(itfName); }
//  }

  // functional aspect
  public void handleRequest (Request r) throws IOException {
//      Iterator i = handlers.values().iterator();
      
      List handlers = null;
      try {
          handlers = (List) ((BindingController)this).lookupFc("h");
      }
      catch(NoSuchInterfaceException nsie) {
          throw new RuntimeException(nsie.getMessage());
      }   
      
      Iterator i = handlers.iterator();
    
      while (i.hasNext()) {
      try {
          RequestHandler rh = (RequestHandler) i.next();
          rh.handleRequest(r);
          return;
      } catch (IOException _) { }
    }
  }
}
