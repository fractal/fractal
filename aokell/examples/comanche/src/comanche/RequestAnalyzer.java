/***
 * Comanche web server
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Eric Bruneton
 * Author: Lionel Seinturier
 */

package comanche;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.PrintStream;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;

public class RequestAnalyzer implements RequestHandler /*, BindingController*/ {
    
  private RequestHandler rh;
  private Logger l;
  
//  // configuration aspect
//  public String[] listFc () { return new String[] { "l", "rh" }; }
//  public Object lookupFc (String itfName) {
//    if (itfName.equals("l")) { return l; }
//    else if (itfName.equals("rh")) { return rh; }
//    else return null;
//  }
//  public void bindFc (String itfName, Object itfValue) {
//    if (itfName.equals("l")) { l = (Logger)itfValue; }
//    else if (itfName.equals("rh")) { rh = (RequestHandler)itfValue; }
//  }
//  public void unbindFc (String itfName) {
//    if (itfName.equals("l")) { l = null; }
//    else if (itfName.equals("rh")) { rh = null; }
//  }

  // functional aspect
  public void handleRequest (Request r) throws IOException {
    r.in = new InputStreamReader(r.s.getInputStream());
    r.out = new PrintStream(r.s.getOutputStream());
    String rq = new LineNumberReader(r.in).readLine();    
    
    try {
        rh = (RequestHandler) ((BindingController)this).lookupFc("rh");
        l = (Logger) ((BindingController)this).lookupFc("l");
    }
    catch(NoSuchInterfaceException nsie) {
        throw new RuntimeException(nsie.getMessage());
    }   

    l.log(rq);
    if (rq.startsWith("GET ")) {
      r.url = rq.substring(5, rq.indexOf(' ', 4));
      rh.handleRequest(r);
    }
    r.out.close();
    r.s.close();
  }
}
