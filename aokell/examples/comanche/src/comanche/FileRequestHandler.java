/***
 * Comanche web server
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Eric Bruneton
 * Author: Lionel Seinturier
 */

package comanche;

import java.io.IOException;
import java.io.InputStream;


/**
 * Handle HTTP request for files.
 * 
 * @author Eric Bruneton
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class FileRequestHandler implements RequestHandler {
    
    public void handleRequest (Request r) throws IOException {
        
        if( r.url.length() == 0 ) {
            // Default to index.html
            r.url = "index.html";
        }
        
        InputStream is = ClassLoader.getSystemResourceAsStream(r.url);
        if( is != null ) {
            byte[] data = new byte[is.available()];
            is.read(data);
            is.close();
            r.out.print("HTTP/1.0 200 OK\n\n");
            r.out.write(data);
            return;
        }
        
        throw new IOException("File not found");
    }
}
