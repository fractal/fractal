/***
 * Comanche web server
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Eric Bruneton
 * Author: Lionel Seinturier
 */

package comanche;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.objectweb.fractal.api.control.BindingController;

public class RequestReceiver implements Runnable /*, BindingController*/ {

  public void run () {
    try {
      ServerSocket ss = new ServerSocket(8080);
      System.out.println("Comanche HTTP Server ready on port 8080.");
      System.out.println("Load http://localhost:8080/gnu.jpg");
      while (true) {
        final Socket socket = ss.accept();
        
        final Scheduler s = (Scheduler) ((BindingController)this).lookupFc("s");
        final RequestHandler rh = (RequestHandler) ((BindingController)this).lookupFc("rh");
        
        s.schedule(new  Runnable () {
          public void run () {
            try {
              rh.handleRequest(new Request(socket));
            } catch (IOException _) { }
          }
        });
      }
    } catch (Exception e) { e.printStackTrace(); }
  }
}
