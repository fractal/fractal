/***
 * Comanche web server
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Eric Bruneton
 * Author: Lionel Seinturier
 */

package comanchemain;

import java.util.HashMap;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.util.Fractal;


/**
 * Main class for the comanche web server.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class Main {
    
    public static void main(String[] args) throws Exception {
        
        // ----------------------------------------------------------        
        // Load the ADL definition
        // ----------------------------------------------------------        
        Factory factory = 
            FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
        Component root = (Component)
            factory.newComponent("comanche.Comanche",new HashMap());
        
        // ----------------------------------------------------------
        // Start the Root component
        // ----------------------------------------------------------
        Fractal.getLifeCycleController(root).startFc();

        // ----------------------------------------------------------
        // Call the entry point of the application
        // ----------------------------------------------------------
        ((Runnable)root.getFcInterface("r")).run();
    }
}
