This is the comanche web server. This program was implemented with
Julia and runs, unmodified, with AOKell.

To execute the example type:
  ant execute

The available Ant targets are:
 compile: compile the example
 execute: execute the example
 help: display this help message
 clean: clean the generated files
 
See "Developing with Fractal" in the Documentation section of the
Fractal web site <http://fractal.objectweb.org> for a description
of the comanche architecture.

When run, comanche waits for HTTP request on port 8080. A typical
run of the program should return something like:

Comanche HTTP Server ready on port 8080.

Open the <http://localhost:8080/src/gnu.jpg> URL in your favorite
web browser. A well-known head should appear! To stop the server,
type Ctrl-C in the launching shell.

You can add any file you like in the comanche/ directory to have
them served by comanche.
