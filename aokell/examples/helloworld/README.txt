This example shows how to program a simple Fractal application with AOKell.
This is the Hello World of Fractal.

To execute the example type:
  ant execute

The available Ant targets are:
 compile: compile the example
 execute: execute the example
 help: display this help message
 clean: clean the generated files
 
When run, the example creates the following components
architecture:
- 1 root composite component
- 2 primitive components included in the root composite

  |--------------------------------------|
  |                                      |
  |        |--------|        |--------|  |
|-|-|--->|-| Client |-|--->|-| Server |  |
  |        |--------|        |--------|  |
  |                                      |
  |--------------------------------------|

The server owns an interface with a method for displaying a message
and an attribute controller exporting two attributes:
- header: text to be printed before the message
- count: how many time the message must be printed

In addition to printing the message, the server throws an exception
and print the stack trace to show which are the method visited by
the program.

This program is run 5 times with 5 different configurations:
- the component assembly is defined with the API
- the component assembly is defined with Fractal ADL
- the component assembly is defined with the API and component implementations
  follow the JL style (see below the section on the JL vs AO style)
- the component assembly is defined with template components and the API
- the component assembly is defined with template components and Fractal ADL

A typical run of the program should return something like:

===================================================
==== Hello World with the API                  ====
===================================================
CLIENT created
SERVER created
Server: print method called
	at cs.impl.ServerImpl.print(ServerImpl.java:38)
	at aokell.generated.cs.impl.ServiceImplementedInterface.print(Unknown Source)
	at cs.impl.ClientImpl.run(ClientImpl.java:46)
	at aokell.generated.java.lang.RunnableImplementedInterface.run(Unknown Source)
	at aokell.generated.java.lang.RunnableBoundableInterface.run(Unknown Source)
	at cs.Main.main(Main.java:124)
	at hw.Main.main(Main.java:39)
Server: begin printing...
->hello world
Server: print done.

===================================================
==== Hello World with the ADL                  ====
===================================================
CLIENT created
SERVER created
Server: print method called
	at csadl.impl.ServerImpl.print(ServerImpl.java:39)
	at aokell.generated.csadl.impl.ServiceImplementedInterface.print(Unknown Source)
	at csadl.impl.ClientImpl.run(ClientImpl.java:48)
	at aokell.generated.java.lang.RunnableImplementedInterface.run(Unknown Source)
	at aokell.generated.java.lang.RunnableBoundableInterface.run(Unknown Source)
	at csadl.Main.main(Main.java:61)
	at hw.Main.main(Main.java:45)
Server: begin printing...
->hello world
Server: print done.

===================================================
==== Hello World with the API and the JL style ====
===================================================
CLIENT created
SERVER created
Server: print method called
	at cssep.impl.ServerImpl.print(ServerImpl.java:39)
	at aokell.generated.cssep.impl.ServiceImplementedInterface.print(Unknown Source)
	at cssep.impl.ClientImpl.run(ClientImpl.java:39)
	at aokell.generated.java.lang.RunnableImplementedInterface.run(Unknown Source)
	at aokell.generated.java.lang.RunnableBoundableInterface.run(Unknown Source)
	at cssep.Main.main(Main.java:124)
	at hw.Main.main(Main.java:51)
Server: begin printing...
->hello world
Server: print done.

===================================================
==== Hello World with the API and templates    ====
===================================================
CLIENT created
SERVER created
CLIENT created
SERVER created
Server: print method called
	at cstemplate.impl.ServerImpl.print(ServerImpl.java:39)
	at aokell.generated.cstemplate.impl.ServiceImplementedInterface.print(Unknown Source)
	at cstemplate.impl.ClientImpl.run(ClientImpl.java:47)
	at aokell.generated.java.lang.RunnableImplementedInterface.run(Unknown Source)
	at aokell.generated.java.lang.RunnableBoundableInterface.run(Unknown Source)
	at cstemplate.Main.main(Main.java:126)
	at hw.Main.main(Main.java:57)
Server: begin printing...
->hello world
->hello world
->hello world
->hello world
Server: print done.

===================================================
==== Hello World with the ADL and templates    ====
===================================================
CLIENT created
SERVER created
CLIENT created
SERVER created
Server: print method called
	at cstemplateadl.impl.ServerImpl.print(ServerImpl.java:39)
	at aokell.generated.cstemplateadl.impl.ServiceImplementedInterface.print(Unknown Source)
	at cstemplateadl.impl.ClientImpl.run(ClientImpl.java:48)
	at aokell.generated.java.lang.RunnableImplementedInterface.run(Unknown Source)
	at aokell.generated.java.lang.RunnableBoundableInterface.run(Unknown Source)
	at cstemplateadl.Main.main(Main.java:65)
	at hw.Main.main(Main.java:63)
Server: begin printing...
->hello world
->hello world
->hello world
Server: print done.


AO style vs JL style for implementing components
------------------------------------------------

With the AO style, component content classes implement one of the
AOKell provided marker interfaces (for instance,
aokell.lib.membrane.primitive.PrimitiveType for primitive
components.) Based on these markers, AOKell aspects extend the
component content classes with the control logic. The control
interfaces are then directly available to the component programmer.
For instance, s/he can call this.bindFc even though the method has
not been implemented (the method is injected by the binding
controller aspect.)

With the JL style, component content classes are just like what
they use to be with Julia. Content classes may implement control
interfaces, and if so, the membrane notifies the content whenever a
call to a control interfaces is issued. With the JL style,
programmers can then seamlessly run Fractal applications written for
Julia with AOKell.
