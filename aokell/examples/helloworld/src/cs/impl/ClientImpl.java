/***
 * Fractal Hello World Example
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Eric Bruneton
 * Author: Lionel Seinturier
 */

package cs.impl;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;


public class ClientImpl implements Runnable {

  public ClientImpl () {
    System.err.println("CLIENT created");
  }
  
  public void run () {
      Service service = null;
      try {
          service = (Service) ((BindingController)this).lookupFc("s");
      }
      catch( NoSuchInterfaceException nsie ) {
          throw new RuntimeException(nsie.getMessage());
      }
      
      service.print("hello world");
  }

}
