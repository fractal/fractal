/***
 * Fractal Hello World Example
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Eric Bruneton
 * Author: Lionel Seinturier
 */

package cstemplate;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.util.Fractal;

import cstemplate.impl.ClientImpl;
import cstemplate.impl.ServerImpl;
import cstemplate.impl.Service;
import cstemplate.impl.ServiceAttributes;


public class Main {

    public static void main(String[] args) throws Exception {
        
        // ----------------------------------------------------------
        // Get the bootstrap component and factories
        // ----------------------------------------------------------
        Component boot = Fractal.getBootstrapComponent();
        TypeFactory tf = Fractal.getTypeFactory(boot);
        GenericFactory cf = Fractal.getGenericFactory(boot);
        
        // ----------------------------------------------------------
        // Create the component types
        // ----------------------------------------------------------
        ComponentType rType = tf.createFcType(new InterfaceType[] {
                tf.createFcItfType(
                   "m",
                   "java.lang.Runnable",
                   false,   // server interface
                   false,   // mandatory
                   false)   // singleton
              });
        
        ComponentType cType = tf.createFcType(new InterfaceType[] {
                tf.createFcItfType("m", "java.lang.Runnable", false, false, false),
                tf.createFcItfType("s", Service.class.getName(), true, false, false)
        });
        ComponentType sType = tf.createFcType(new InterfaceType[] {
                tf.createFcItfType("s", Service.class.getName(), false, false, false),
                tf.createFcItfType(
                        "attribute-controller",
                        ServiceAttributes.class.getName(),
                        false,
                        false,
                        false)
        });
        
        // ----------------------------------------------------------
        // Instantiate and initialize the components
        // ----------------------------------------------------------
        Component rTemplate = cf.newFcInstance(
                rType,"compositeTemplate",new Object[]{"composite",null});
        
        Component cTemplate = cf.newFcInstance(
                cType,"primitiveTemplate",
                new Object[]{"primitive",ClientImpl.class.getName()} );
        
        Component sTemplate = cf.newFcInstance(
                sType,"parametricPrimitiveTemplate",
                new Object[]{"parametricPrimitive",ServerImpl.class.getName()} );
        
        ServiceAttributes sa = (ServiceAttributes)
            Fractal.getAttributeController(sTemplate);
        sa.setHeader("->");
        sa.setCount(4);
    
        // ----------------------------------------------------------
        // Add the Client and Server components in the Root composite
        // ----------------------------------------------------------
        Fractal.getContentController(rTemplate).addFcSubComponent(cTemplate);
        Fractal.getContentController(rTemplate).addFcSubComponent(sTemplate);
        
        // ----------------------------------------------------------
        // Create bindings
        // ----------------------------------------------------------
        Fractal.getBindingController(rTemplate).bindFc("m", cTemplate.getFcInterface("m"));
        Fractal.getBindingController(cTemplate).bindFc("s", sTemplate.getFcInterface("s"));
        
        // ----------------------------------------------------------
        // Instantiate the template
        // ----------------------------------------------------------
        Factory f = Fractal.getFactory(rTemplate);
        Component rComp = f.newFcInstance();

        // ----------------------------------------------------------
        // Start the component
        // ----------------------------------------------------------
        Fractal.getLifeCycleController(rComp).startFc();
        
        // ----------------------------------------------------------
        // Call the entry point of the application
        // ----------------------------------------------------------
        ((Runnable)rComp.getFcInterface("m")).run();

    }
}
