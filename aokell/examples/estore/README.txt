This example implements an electronic store selling CDs and DVDs. This
example demonstrates the use of collection interfaces.

To execute the example type:
  ant execute

The available Ant targets are:
 compile: compile the example
 execute: execute the example
 help: display this help message
 clean: clean the generated files
 
When run, the example creates the following components
architecture:
- 1 root composite component
- 4 components included in the root composite

  |------------------------------------------------------|
  |                                                      |
  |       |--------|       |-------|       |------|      |
|-|-|-->|-| Client |-|-->|-|       |-|-->|-| Bank |      |
  |       |--------|       |       |       |------|      |
  |                        | Store |                     |
  |                        |       |       |----------|  |
  |                        |       |-|-->|-| Provider |  |
  |                        |-------|       |----------|  |
  |                                                      |
  |------------------------------------------------------|

Client, Store and Provider are primitive components. Bank is a
composite with a Desk primitive sub-component, and some Account
primitive sub-components. The account components (3 in our case, 1
for the store, and 2 for customers Anne and Bob) are bound to a
collection client interfaces provided by the Desk component.

  |------------------------------------|
  | Bank                               |
  |                       |---------|  |                   
  |       |------|    |-->| E-Store |  |
|-|-|-->|-| Desk |-|--|   |---------|  |
  |       |------|    |                |
  |                   |   |------|     |
  |                   |-->| Anne |     |
  |                   |   |------|     |
  |                   |                |
  |                   |   |-----|      |
  |                   |-->| Bob |      |
  |                       |-----|      |
  |                                    |
  |------------------------------------|


The program performs two selling scenario. In the first one, both
Bob and Anne place an order for CDs. In the second one, Bob places
an order for DVDs and CDs. The following messages are displayed:

Scenario 1
Ordering 2 CD for Bob...
Order #0 amount: 30.0 delay: 71h issued on: Tue Jul 26 02:23:24 CEST 2005
Ordering 1 CD for Anne...
Order #1 amount: 15.0 delay: 2h issued on: Tue Jul 26 02:23:24 CEST 2005

Scenario 2
Ordering for Bob...
Item: DVD, quantity: 2
Item: CD, quantity: 1
Order #2 amount: 55.0 delay: 62h issued on: Tue Jul 26 02:23:24 CEST 2005
