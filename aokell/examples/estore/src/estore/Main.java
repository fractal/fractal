/***
 * AOKell E-Store application
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package estore;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.util.Fractal;

import estore.bank.desk.DeskHelper;


/**
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class Main {
    
    public static void main(String[] args) throws Exception {
                
        // Create the Root component
        Factory f = getFactory();
        root = (Component) f.newComponent("estore.Root",null);
        
        // Start the Root component
        LifeCycleController lc = Fractal.getLifeCycleController(root);
        lc.startFc();
        
        Component desk = DeskHelper.getDeskComponent();
        Runnable idc = (Runnable)
            desk.getFcInterface("IDeskConfiguration");;
        idc.run();
        
        // Call the run() method of the IRun interface of the Root component
        Runnable r = (Runnable) root.getFcInterface("IRun");
        r.run();
    }

    
    private static Factory factory;
    
    /**
     * Return the singleton instance of a FRACTAL_BACKEND factory.
     * FactoryFactory.getFactory always return a new instance.
     * For dynamic creation of component instances,
     * we need to access the same factory, and thus retain its reference.
     *  
     * @return  the singleton instance of a FRACTAL_BACKEND factory
     */
    public static Factory getFactory() {
        if ( factory == null ) {
            try {
                factory =
                    FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
            } catch (ADLException e) {
                e.printStackTrace();
            }
        }
        return factory;
    }
    
    /**
     * Get a named direct sub-component contained inside a composite.
     * This is not a recursive search.
     * 
     * @param c     the composite 
     * @param name  the name of the sub-component
     * @return      the sub-component or null
     */
    public static Component getSubComponent( Component c, String name )
    throws NoSuchInterfaceException {
        
        ContentController cc = Fractal.getContentController(c);
        Component[] comps = cc.getFcSubComponents();
        for (int i = 0; i < comps.length; i++) {
            NameController nc = Fractal.getNameController(comps[i]);
            if( nc.getFcName().equals(name) )
                return comps[i];
        }
        
        return null;
        
    }
    
    /** The root component of this application. */
    protected static Component root;
    public static Component getRootComponent() {
        return root;
    }
 
}
