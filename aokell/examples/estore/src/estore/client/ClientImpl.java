/***
 * AOKell E-Store application
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package estore.client;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;

import estore.bank.InsufficientBalanceException;
import estore.bank.UnknownAccountException;
import estore.provider.UnknownItemException;
import estore.store.Cart;
import estore.store.IFastLane;
import estore.store.ILane;
import estore.store.InvalidCartException;
import estore.store.Order;

public class ClientImpl implements Runnable, IClient {

    private ILane iLane;
    private IFastLane iFastLane;

    // -----------------------------------------------------
    // Implementation of the Runnable interface
    // -----------------------------------------------------

    public void run() {
        
        // Scenario 1
        // Direct ordering of an item
        // The scenario is run twice
        System.out.println("Scenario 1");
        scenario1("CD",2,"Lille","Bob");
        scenario1("CD",1,"Lille","Anne");
        System.out.println();
        
        // Scenario 2
        // Ordering of several items by using a cart
        System.out.println("Scenario 2");
        scenario2(new String[]{"DVD","CD"},new int[]{2,1},"Lille","Bob");
        System.out.println();
    }
    
    private void scenario1(
            String item, int qty, String address, String account ) {
        
        try {
            _scenario1(item,qty,address,account);
        }
        catch (Exception e) {
            System.err.println("Exception: "+e.getMessage());
            e.printStackTrace();
        }        
    }
    
    private void _scenario1(
            String item, int qty, String address, String account )
    throws
    UnknownItemException,
    InsufficientBalanceException, UnknownAccountException,
    NoSuchInterfaceException {
        
        iFastLane = (IFastLane) ((BindingController)this).lookupFc("IFastLane");
        
        System.out.println("Ordering "+qty+" "+item+" for "+account+"...");
        Order order = iFastLane.oneShotOrder(this,item,qty,address,account);
        System.out.println(order);
    }

    private void scenario2(
            String[] items, int[] qties, String address, String account ) {
        
        try {
            _scenario2(items,qties,address,account);
        }
        catch (Exception e) {
            System.err.println("Exception: "+e.getMessage());
            e.printStackTrace();
        }        
    }
    
    private void _scenario2(
            String[] items, int[] qties, String address, String account )
    throws
    InsufficientBalanceException, UnknownAccountException,
    UnknownItemException, InvalidCartException,
    NoSuchInterfaceException {

        iLane = (ILane) ((BindingController)this).lookupFc("ILane");
        
        System.out.println("Ordering for "+account+"...");
        Cart cart = null;
        for (int i = 0; i < items.length; i++) {
            System.out.println("Item: "+items[i]+", quantity: "+qties[i]);
            cart = iLane.addItemToCart(cart,this,items[i],qties[i]);
        }
        Order order = iLane.pay(cart,address,account);
        System.out.println(order);
    }

    // -----------------------------------------------------
    // Implementation of the IClient interface
    // -----------------------------------------------------

}