/***
 * AOKell E-Store application
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package estore.store;

import estore.bank.InsufficientBalanceException;
import estore.bank.UnknownAccountException;
import estore.client.IClient;
import estore.provider.UnknownItemException;

/**
 * @author Lionel Seinturier
 */
public interface IFastLane extends IStore {
    
    /**
     * Used by a client to order an item.
     * The whole process of ordering is encapsulated by this method.
     * If several items need to be ordered, this method needs to be
     * called several times, but the items will appear in separate orders.
     * 
     * @param client
     * @param item
     * @param quantity
     * @param address
     * @param bankAccount
     * @return  the order
     */
    Order oneShotOrder(
            IClient client,
            Object item,
            int quantity,
            Object address,
            Object bankAccount
    )
    throws
    UnknownItemException,
    InsufficientBalanceException, UnknownAccountException;

}
