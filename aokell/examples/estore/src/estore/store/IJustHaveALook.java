/***
 * AOKell E-Store application
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package estore.store;

import estore.provider.UnknownItemException;

/**
 * @author Lionel Seinturier
 */
public interface IJustHaveALook extends IStore {
    
    /**
     * @param item  a given item
     * @return         the price of a given item
     * @throws UnknownItemException
     */
    double getPrice( Object item ) throws UnknownItemException;
    
    /**
     * @param item  a given item
     * @param qty   a given quantity
     * @return
     *      true if the given quantity of the given item is available
     *      directly from the store
     *      i.e. without having to re-order it from the provider
     * @throws UnknownItemException
     */
    boolean isAvailable( Object item, int qty ) throws UnknownItemException;

}
