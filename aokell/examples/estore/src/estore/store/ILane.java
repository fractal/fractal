/***
 * AOKell E-Store application
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package estore.store;

import estore.bank.InsufficientBalanceException;
import estore.bank.UnknownAccountException;
import estore.client.IClient;
import estore.provider.UnknownItemException;

/**
 * @author Lionel Seinturier
 */
public interface ILane extends IStore {
    
    /**
     * Add an item to a cart.
     * If the cart does not exist yet, create a new one.
     * This method is called for each item one wants to add to the cart.
     * 
     * @param cart    a previously created cart or null
     * @param client
     * @param item
     * @param qty
     * @return
     *      Implementation dependant.
     *      Either a new cart at each call or the same cart updated.
     * 
     * @throws UnknownItemException
     * @throws InvalidCartException
     *      if the given client does not own the given cart
     */
    Cart addItemToCart(
            Cart cart,
            IClient client,
            Object item,
            int qty )
    throws UnknownItemException, InvalidCartException;

    /**
     * Once all the items have been added to the cart,
     * this method finish make the payment.
     *  
     * @param cart
     * @param address
     * @param bankAccount
     * @return  the order
     */
    Order pay( Cart cart, Object address, Object bankAccount )
    throws
    InvalidCartException, UnknownItemException,
    InsufficientBalanceException, UnknownAccountException;

}
