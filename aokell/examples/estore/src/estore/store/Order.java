/***
 * AOKell E-Store application
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package estore.store;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import estore.client.IClient;
import estore.provider.UnknownItemException;

/**
 * @author Lionel Seinturier
 */
public class Order {
    
    /** The total number of orders emitted so far. */
    private static int numOrders;
    
    /** The index of this order. */
    private int num;
    
    public IClient client;
    public Object item;
    public Object address;
    public Object bankAccount;
    
    /** The date at which the ordered is issued. */
    public Date date;
    
    /** The delay for delivering the items in the order. */
    private int delay;
    
    /** The items currently in the order. */
    private Set items = new HashSet();
    
    /** The quantities of each item ordered. key=item, value=quantity. */
    private Map itemQuantities = new HashMap();
    
    /** The individual prices of each item ordered. key=item, value=price. */
    private Map itemPrices = new HashMap();

    
    private Order() {
        num = numOrders++;
        date = new Date();
    }
    
    public Order(IClient client, Object address, Object bankAccount) {
        this();
        this.client = client;
        this.address = address;
        this.bankAccount = bankAccount;
    }
    
    /**
     * Add an item to the order.
     * 
     * @param item
     * @param qty
     * @param price
     * @throws UnknownItemException
     */
    public void addItem( Object item, int qty, double price )
    throws UnknownItemException {
        
        if ( itemPrices.containsKey(item) ) {
            double oldPrice = ((Double) itemPrices.get(item)).doubleValue();
            if ( oldPrice != price )
                throw new UnknownItemException(
                        "Item "+item+" price ("+price+
                        ") added to cart is different from the price ("+
                        oldPrice+") of the same item already in the cart" );
        }
        
        items.add(item);
        itemPrices.put( item, new Double(price) );
        
        int newQty = qty;
        if ( itemQuantities.containsKey(item) ) {
            newQty += ((Integer) itemQuantities.get(item)).intValue();
        }
        itemQuantities.put( item, new Integer(newQty) );
    }
    
    /**
     * Compute the total amount of the order
     */
    public double computeAmount() {
        
        double amount = 0;
        
        for (Iterator iter = items.iterator(); iter.hasNext();) {
            Object item = iter.next();
            int qty = ((Integer)itemQuantities.get(item)).intValue();
            double price = ((Double)itemPrices.get(item)).doubleValue();
            
            amount += qty*price;
        }
        
        return amount;
    }
    
    /**
     * @return Returns the delay for delivering this order.
     */
    public int getDelay() {
        return delay;
    }
    
    /**
     * Set the delay for this order.
     * The delay is the highest delay for delivering all the items of an order.
     */
    public void setDelay(int delay) {
        if ( delay > this.delay )
            this.delay = delay;
    }
    
    public int getKey() {
        return num;
    }
    
    public String toString() {
        String msg = "Order #"+num+" ";
        msg += "amount: "+computeAmount()+" ";            
        msg += "delay: "+getDelay()+"h ";
        msg += "issued on: "+date;
        return msg;
    }
}
