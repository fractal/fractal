/***
 * AOKell E-Store application
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package estore.store;

import estore.provider.IProvider;

/**
 * @author Lionel Seinturier
 */
public class ItemInStock {
    
    /** The total number of ItemInStock instances created. */
    private static int numItems;
    
    /** The index of this item. */
    private int num;
    
    public Object item;
    public int quantity;
    public double price;
    public IProvider provider;

    private ItemInStock() {
        num = numItems++;        
    }
    
    public ItemInStock(Object item, int quantity, double price,
            IProvider provider) {
        this();
        this.item = item;
        this.quantity = quantity;
        this.price = price;
        this.provider = provider;
    }
    
    public String toString() {
        return "Item #"+num;
    }

}
