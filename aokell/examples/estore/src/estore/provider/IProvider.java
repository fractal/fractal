/***
 * AOKell E-Store application
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package estore.provider;

import estore.store.IStore;

/**
 * @author Lionel Seinturier
 */
public interface IProvider {
    
    /**
     * Get the price of an item provided by this provider.
     * 
     * @param item
     * @return
     */
    double getPrice( Object item ) throws UnknownItemException;
    
    /**
     * Emit an order for items.
     * The provider returns the delay for delivering the items.
     * 
     * @param store  the store that emits the order
     * @param item   the item ordered
     * @param qty    the quantity ordered
     * @return       the delay (in hours)
     */
    int order( IStore store, Object item, int qty ) throws UnknownItemException;

}
