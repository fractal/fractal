/***
 * AOKell E-Store application
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package estore.bank;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;

import estore.Main;

/**
 * Helper class for retrieving the Bank component.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class BankHelper {
    
    /** The name of the Bank component defined in Root.fractal. */
    final private static String BANK_NAME = "Bank";
    
    private static Component bank;
    
    /**
     * Return the Bank component.
     */
    public static Component getBankComponent() {
        
        if( bank == null ) {
            try {
                Component root = Main.getRootComponent();
                bank = Main.getSubComponent(root,BANK_NAME);
            }
            catch (NoSuchInterfaceException e) {
                e.printStackTrace();
            }
            
        }
        return bank;
    }
    
}
