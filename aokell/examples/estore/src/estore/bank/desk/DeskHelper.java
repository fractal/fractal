/***
 * AOKell E-Store application
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package estore.bank.desk;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;

import estore.Main;
import estore.bank.BankHelper;

/**
 * Helper class for retrieving the Desk component.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class DeskHelper {
    
    /** The name of the Desk component defined in Bank.fractal. */
    final private static String DESK_NAME = "Desk";
    
    private static Component desk;
    
    /**
     * Return the Desk component.
     */
    public static Component getDeskComponent() {
        
        if ( desk == null ) {
            try {
                Component bank = BankHelper.getBankComponent();
                desk = Main.getSubComponent(bank,DESK_NAME);
            }
            catch (NoSuchInterfaceException e) {
                e.printStackTrace();
            }
        }        
        
        return desk;
    }
    
}
