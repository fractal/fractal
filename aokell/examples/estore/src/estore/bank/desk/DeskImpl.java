/***
 * AOKell E-Store application
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package estore.bank.desk;


import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.control.SuperController;
import org.objectweb.fractal.util.Fractal;

import estore.Main;
import estore.bank.IBank;
import estore.bank.InsufficientBalanceException;
import estore.bank.UnknownAccountException;
import estore.bank.account.IAccountProperties;

public class DeskImpl implements IBank, Runnable {

    // -----------------------------------------------------
    // Implementation of the IBank interface
    // -----------------------------------------------------

    public void transfert(Object from, Object to, double amount)
        throws InsufficientBalanceException, UnknownAccountException {
                
        try {
            // Get the Account components
            String fromAccountItfName = IACCOUNT_BINDING+from;
            
            Interface fromAccountItf = (Interface) ((BindingController)this).lookupFc(fromAccountItfName);
            Component fromAccount = fromAccountItf.getFcItfOwner();

            String toAccountItfName = IACCOUNT_BINDING+to;
            Interface toAccountItf = (Interface) ((BindingController)this).lookupFc(toAccountItfName);
            Component toAccount = toAccountItf.getFcItfOwner();
        
            // Get the balance of the account to widthdraw
            IAccountProperties acFrom = (IAccountProperties)
                    Fractal.getAttributeController(fromAccount);
            double fromBalance = acFrom.getAmount();
            
            // Check whether the account is sufficiently balanced
            if ( fromBalance < amount )
                throw new InsufficientBalanceException(from.toString());
            
            // Get the balance of the account to credit
            IAccountProperties acTo = (IAccountProperties)
                Fractal.getAttributeController(toAccount);
            double toBalance = acTo.getAmount();
            
            // Perform the transfert
            acFrom.setAmount( fromBalance - amount );
            acTo.setAmount( toBalance + amount );
        }
        catch (NoSuchInterfaceException e) {
            // We do not want this exception to appear in the business
            // interface, so we wrap it in a runtime exception
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
    
    // -----------------------------------------------------
    // Implementation of the Runnable interface
    // -----------------------------------------------------
    
    public void run() {
        Component desk = DeskHelper.getDeskComponent();
        createAndBindAccount(desk,"Bob",100);
        createAndBindAccount(desk,"Anne",30);
        createAndBindAccount(desk,"E-Store",0);        
    }

    /**
     * Create a Account component with the given initial values,
     * insert it in the Bank component that contains the given Desk component,
     * bind it to the current Desk component, and
     * start it.
     * 
     * @param desk    the Desk component that perform the creation
     * @param owner   the owner of the account
     * @param amount  the initial balance of the account
     */
    private void createAndBindAccount(
            Component desk,
            String owner,
            double amount ) {
        
        Factory f = Main.getFactory();
        
        try {            
            // Create a new Account component
            Component c = (Component)
                f.newComponent("estore.bank.account.Account",null);
            
            // Give a name (owner) to the Account component
            NameController nc = Fractal.getNameController(c);
            nc.setFcName(owner);
            
            // Initialize the Account component properties
            IAccountProperties props =
                (IAccountProperties) Fractal.getAttributeController(c);
            props.setOwner(owner);
            props.setAmount(amount);
            
            // Get the Bank composite that contains the Desk component
            // Nota: we "know" that Desk in not shared and that supers[0]
            // is the only composite containing Desk, and is Bank
            SuperController sc = Fractal.getSuperController(desk);
            Component[] supers = sc.getFcSuperComponents();
            Component bank = supers[0];
            
            // Add the Account component to the Bank composite
            ContentController cc = Fractal.getContentController(bank);
            cc.addFcSubComponent(c);
            
            // Bind the Account component to the current Desk component
            // bindFc(IACCOUNT_BINDING,c);
            BindingController bc = Fractal.getBindingController(desk);
            String clientItfName = IACCOUNT_BINDING+owner;
            Object itf = c.getFcInterface("IAccount");
            bc.bindFc(clientItfName,itf);
            
            // Start the Account component
            LifeCycleController lc = Fractal.getLifeCycleController(c);
            lc.startFc();
        }
        catch (Exception e) {
            throw new RuntimeException(
                    "Failed to create account for "+owner, e );
        }
        
    }

    public final static String IACCOUNT_BINDING = "IAccount";

}
