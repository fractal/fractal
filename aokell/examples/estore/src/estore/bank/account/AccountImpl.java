/***
 * AOKell E-Store application
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package estore.bank.account;

import estore.bank.InsufficientBalanceException;

public class AccountImpl implements IAccount, IAccountProperties {

    /**
     * Constructs a new AccountImpl
     */
    public AccountImpl() {
    }

    // -----------------------------------------------------
    // Implementation of the IAccountProperties interface
    // -----------------------------------------------------

    private double amount;
    private String owner;

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
    
    // -----------------------------------------------------
    // Implementation of the IAccount interface
    // -----------------------------------------------------

    public void credit(double amount) {
        this.amount += amount;        
    }

    public void withdraw(double amount) throws InsufficientBalanceException {
        if ( this.amount < amount )
            throw new InsufficientBalanceException(owner);
        this.amount -= amount;
    }
    
    // -----------------------------------------------------
    // Implementation of the Object interface
    // -----------------------------------------------------
    
    /**
     * Two AccountImpl instances are considered equals
     * if they share the same owner.
     * Of course, in a more realistic implementation,
     * we should have a account number.
     */
    public boolean equals( Object other ) {
        if( ! (other instanceof AccountImpl) )
            return false;
        AccountImpl otherAccount = (AccountImpl) other;
        return ( otherAccount.owner == owner);
    }
    
    public int hashCode() {
        return owner.hashCode();
    }

}
