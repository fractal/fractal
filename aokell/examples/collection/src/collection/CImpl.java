/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package collection;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.control.BindingController;

public class CImpl implements BindingController {

  private Map bindings = new HashMap();
  
  public String[] listFc () {
    return (String[])bindings.keySet().toArray(new String[bindings.size()]);
  }

  public Object lookupFc (String itf) {
    return bindings.get(itf);
  }

  public void bindFc (String itf, Object value) {
    if (!itf.equals("component")) {
      bindings.put(itf, value);
    }
  }

  public void unbindFc (String itf) {
    bindings.remove(itf);
  }
}
