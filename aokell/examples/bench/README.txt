This example implements a benchmark suite to measure the cost of
programming components with AOKell.

To execute the example type:
  ant execute

The available Ant targets are:
 compile: compile the example
 execute: execute the example
 help: display this help message
 clean: clean the generated files
 
When run, the example creates the following components
architecture:
- 1 root composite component
- 2 primitive components included in the root composite

  |--------------------------------------|
  |                                      |
  |        |--------|        |--------|  |
|-|-|--->|-| Client |-|--->|-| Server |  |
  |        |--------|        |--------|  |
  |                                      |
  |--------------------------------------|

The server component exports an interface with 8 different methods,
each of them being trivial but non empty to bypass VM
optimizations.

The bench consists in calling the 8 methods of the server 1,000,000
times. The bench is performed 4 times in a warmup phase, and then
measures are taken on 5 different executions of the bench. The
result is the average (in milliseconds) of these last 5 executions.

A typical run of the program should return something like:

BENCH CREATED
Warming up ....
Starting bench
0: 741ms; 1: 751ms; 2: 751ms; 3: 751ms; 4: 742ms;   average=747ms
