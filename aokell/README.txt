============================================================================
AOKell
Copyright (C) 2005-2006 INRIA, France Telecom, USTL

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Contact: Lionel.Seinturier@lifl.fr

Author: Lionel Seinturier
============================================================================


AOKell 2.0 Source Distribution
------------------------------

AOKell is the INRIA and France Telecom implementation of the Fractal
specifications. AOKell is a free software distributed under the terms of the
GNU Lesser General Public license. AOKell is written in Java.


Table of content
----------------
  1. Requirements
  2. Compiling AOKell
  3. Running the examples distributed with AOKell
  4. Software included with AOKell
  5. The feature mechanism
  6. Note on AspectJ


1. Requirements
---------------
The following software is required to compile, run and use AOKell:
	- J2SE >= 1.5 (1.4 if AspectJ is used instead of Spoon - see below)
	- Ant 1.6.x (1.7.x is not supported)


2. Compiling AOKell
-------------------
When compiling AOKell, the following directories come into play:

	src/        the main source files of AOKell
	config/     some .jar files needed to compile AOKell
	externals/  some .jar files needed to compile and run AOKell
	features/   some additional source modules
	
	output/build/	the directory where .class files are stored

To compile AOKell, from the root directory where AOKell has been installed,
type:
	ant compile


3. Running the examples distributed with AOKell
-----------------------------------------------
Several examples are distributed with AOKell. The source code of these examples
is located under the src/examples/ directory. The list of available examples
follows.

	- helloworld: Hello world
	- comanche: The comanche web server

	- bench: JACBenchmark application
	- benchsep: a second version of the JACBenchmark application
	- collection: A simple example with auto-binding components
	- estore: A simple example with collection interfaces


To run the examples, type (xxx is the example name):
	ant examples
	cd output/dist/examples/xxx
	ant execute

Typying simply "ant" in the example directory displays a short description of
the example.


4. Software included with AOKell
--------------------------------
AOKell includes the following software:

	- ASM 2.2           <http://asm.objectweb.org>
	- AspectJ 1.2.1     <http://www.eclipse.org/aspectj/>
	- Fractal API 2     <http://fractal.objectweb.org>
	- Fractal ADL 2     <http://fractal.objectweb.org>
	- Spoon 1.0         <http://spoon.gforge.inria.fr>


5. The feature mechanism
------------------------
The AOKell framework is decomposed in modules called features.
The purpose of this decomposition is to introduce some degrees of variability in
the framework and to let developers change the implementation of these features.

Different implementations of these features are provided with AOKell. While a
default implementation is provided for each feature, the developer can still
choose an alternative version or provide her/his own version by configuring the
properties defined in the build.properties file and recompiling AOKell.

The list of available features and their values follows:

- fcinterface: generation of Fractal interfaces
  - rt (default): Fractal interfaces are generated at run-time with ASM
  - ct: Fractal interfaces are precompiled
  
- glue: gluing the control and the content
  - spoon (default): the glue is implemented with Spoon processors
  - aspectj: the glue is implemented with AspectJ aspects

- loggable: logging Fractal components
  - off (default)
  - on
  
- membrane: control membrane implementation
  - oo (default): membranes are sets of objects
  - comp: control membranes are componentized
  
- platform: deals with the targeted Java platform (J2SE or J2ME)
  - j2se (default): a standard J2SE platform
  - j2me-cdc: a J2ME CDC compliant platform
  - j2me-cldc: a J2ME CLDC compliant platform such as the KVM


6. Note on AspectJ
------------------
When the glue feature is configured with the aspectj value, the aspect-oriented
language AspectJ is used. For this purpose, the AOKell distribution includes
version 1.2.1 of AspectJ. See http://www.eclipse.org/aspectj for further
details on AspectJ.

When programming new applications, if you intend to use another version of
AspectJ (especially a newer one), you will have to recompile AOKell with *your*
version of AspectJ. This is due to versioning incompatibilities between AspectJ
releases, i.e. your code compiled with another version of AspectJ won't work
with the .jar files of AOKell compiled with AspectJ 1.2.1.

This case may also occur if you are using a plug-in such as the AJDT (see
http://www.eclipse.org/ajdt) AspectJ plug-in for Eclipse which embeds its own
version of the AspectJ compiler. It is recommended to recompile AOKell with the
version of AspectJ provided by the plug-in.


For any question concerning AOKell, please contact Lionel.Seinturier@lifl.fr
