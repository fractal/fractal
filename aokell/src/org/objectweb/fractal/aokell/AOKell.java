/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell;

import org.objectweb.fractal.aokell.lib.factory.GenericFactoryImpl;
import org.objectweb.fractal.aokell.lib.type.ComponentTypeImpl;
import org.objectweb.fractal.aokell.lib.type.InterfaceTypeImpl;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;


/**
 * The AOKell factory component.
 * This component returns the reference of the Fractal bootstrap component.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class AOKell implements Factory {
    
    public AOKell() {}
    
    
    /**
     * Property name for dumping run-time generated code.
     * The value associated to this property contains the directory into which
     * run-time generated code such as Fractal interface implementations are
     * dumped. No dumping if the value is null.
     * This property is used in org.objectweb.fractal.aokell.lib.asm.
     */
    final public static String DUMP_GENERATED_PROP_NAME =
        "org.objectweb.fractal.aokell.dumpgenerated";
    
    
    /** Return the Fractal type of this component. */
    public Type getFcInstanceType() {
        return TYPE;
    }

    /** Return the Fractal controller description of this component. */
    public Object getFcControllerDesc() {
        return CONTROLLER_DESC;
    }

    /** Return the Fractal content description of this component. */
    public Object getFcContentDesc() {
        /*
         * Hard-coding the name of the bootstrap component to avoid a compile
         * dependency from this class to org.objectweb.fractal.aokell.component which would hinder the
         * modularity of the build process.
         */
        return "org.objectweb.fractal.aokell.component.BootstrapComponentImpl";
    }

    /** The singleton instance of the bootstrap component. */
    private static Component bootstrap;
    
    /**
     * Return the reference of the bootstrap component.
     * This method is called by Fractal.getBootstrapComponent().
     * The bootstrap component provides the following interfaces:
     * <ul>
     * <li> org.objectweb.fractal.api.type.TypeFactory </li>
     * <li> org.objectweb.fractal.api.factory.GenericFactory </li>
     * </ul>
     */
    public Component newFcInstance() throws InstantiationException {
        
        if( bootstrap != null )
            return bootstrap;
                
        // Create the bootstrap component
        GenericFactory gf = GenericFactoryImpl.get();
        bootstrap =
            gf.newFcInstance(
                    getFcInstanceType(),
                    getFcControllerDesc(),
                    getFcContentDesc() );
        
        return bootstrap;
    }

    final private static ComponentType TYPE =
        new ComponentTypeImpl(
                new InterfaceType[] {
                        new InterfaceTypeImpl(
                                "type-factory",
                                TypeFactory.class.getName(),
                                false, false, false),
                        new InterfaceTypeImpl(
                                "generic-factory",
                                GenericFactory.class.getName(),
                                false, false, false)
                });
    
    final private static Object CONTROLLER_DESC = "bootstrap";
}
