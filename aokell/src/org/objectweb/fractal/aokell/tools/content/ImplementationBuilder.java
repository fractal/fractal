/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.tools.content;

import java.util.Map;


/**
 * Implementation of the ImplementationBuilder for generating AOKell membranes.
 * This implementation is based on the original one written by E. Bruneton for
 * Fractal ADL.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class ImplementationBuilder
    implements org.objectweb.fractal.adl.implementations.ImplementationBuilder {

  // --------------------------------------------------------------------------
  // Implementation of the ImplementationBuilder interface
  // --------------------------------------------------------------------------
  
  public Object createComponent (
    final Object type, 
    final String name,
    final String definition,
    final Object controllerDesc, 
    final Object contentDesc, 
    final Object context) 
  {
      if( contentDesc != null ) {
          Map contents = (Map)((Map)context).get("contents");
          
          // contents is of type Map<Object,Object>
          // < content desc , controller desc >
          
          if( contents.containsKey(contentDesc) ) {
              throw new RuntimeException(
                      contentDesc+
                      " is associated with more than one controller description: "+
                      contents.get(contentDesc)+" and "+controllerDesc );
          }
          else {
              contents.put( contentDesc, controllerDesc );
          }
      }
      
      return null;
  }
}
