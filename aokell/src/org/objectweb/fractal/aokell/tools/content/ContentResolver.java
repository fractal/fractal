/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.tools.content;


import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.aokell.Membranes;
import org.objectweb.fractal.aokell.lib.membrane.MembraneDef;


/**
 * Starting from an ADL description, this tool determines component content
 * classes and generates the AspectJ aspect which introduces the
 * org.objectweb.fractal.aokell.lib.Content marker interface into those classes.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class ContentResolver {
    
    /** The directory where the generated file is stored. */
    final private static String generatedDirname = "generated";
    
    private static void usage() {
        System.out.println(
                "java "+ContentResolver.class.getName()+" .fractal file[s]");
        System.out.println(
                "Description: parse ADL architectures and extract Java content classes");
    }
    
    public static void main(String[] args) throws Exception {
        
        if( args.length == 0 ) {
            usage();
            return;
        }
        
        new ContentResolver(args).compile();
    }
    
    public ContentResolver( String[] adls ) {
        this.adls = adls;
    }
    
    private String[] adls;
    
    
    public void compile() throws ADLException, IOException {        
        for (int i = 0; i < adls.length; i++) {
            int lastdot = adls[i].lastIndexOf('.');
            String packagename =
                (lastdot==-1) ? "" : adls[i].substring(0,lastdot);
            String classname = adls[i].substring(lastdot+1);
            resolve(packagename,classname);
        }        
    }
    
    protected void resolve( String packagename, String classname )
    throws ADLException, IOException {
                
        ContentResolverHelper crh = new ContentResolverHelper();
        crh.newFile(generatedDirname,packagename,classname);
        
        // contents is of type Map<Object,Object>
        // < content desc , controller desc >
        Map contents = new HashMap();
        
        Map context = new HashMap();
        context.put("contents",contents);

        // Backend.fractal must be stored in the same package as ContentResolver 
        Factory f = FactoryFactory.getFactory(
                ContentResolver.class.getPackage().getName()+".Backend");
        f.newComponent(packagename+"."+classname,context);
        
        Set set = contents.entrySet();
        for (Iterator iter = set.iterator(); iter.hasNext();) {
            Map.Entry entry = (Map.Entry) iter.next();
            Object contentDesc = entry.getKey();
            Object ctrlDesc = entry.getValue();
            
            MembraneDef mdef = Membranes.get().getMembraneDef(ctrlDesc);
            if( mdef == null ) {
                throw new RuntimeException(
                        "Unknown controller description: "+ctrlDesc);
            }
            
            crh.generateClause(contentDesc,ctrlDesc);
        }

        crh.epilogue();
        
        System.out.println("Done.");
    }
        
}
