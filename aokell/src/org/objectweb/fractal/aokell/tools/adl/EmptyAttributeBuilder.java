/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.tools.adl;

import org.objectweb.fractal.adl.attributes.AttributeBuilder;


/**
 * This class implements an empty Fractal ADL AttributeBuilder component.
 * This class is used by tools which take advantage of Fractal ADL to process
 * .fractal files. This is an empty implementation which ignores
 * &lt;attribute&gt; tags.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class EmptyAttributeBuilder implements AttributeBuilder {

    // ------------------------------------------------------------------------
    // Implementation of the AttributeBuilder interface
    // ------------------------------------------------------------------------
    
    public void setAttribute (
      final Object component, 
      final String attributeController, 
      final String name, 
      final String value,
      final Object context) throws Exception 
    {
    }
}
