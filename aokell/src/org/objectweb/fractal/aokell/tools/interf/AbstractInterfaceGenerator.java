/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.tools.interf;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.objectweb.fractal.aokell.lib.ComponentInterfaceImpl;
import org.objectweb.fractal.aokell.lib.InterfaceImpl;
import org.objectweb.fractal.aokell.lib.control.CloneCtrlException;
import org.objectweb.fractal.aokell.lib.control.attribute.AttributeControllerCloneableItf;
import org.objectweb.fractal.aokell.lib.interf.AOKellGeneratedItf;
import org.objectweb.fractal.aokell.lib.util.PlatformHelper;
import org.objectweb.fractal.api.control.AttributeController;

/**
 * This class contains code elements shared by the Fractal interface generator
 * tools.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class AbstractInterfaceGenerator {
    
    /** The directory where the generated files are stored. */
    protected static String generatedDirname = "generated";
    
    /** Root package for generated files. */
    final protected static String packageprefix = "aokell.generated";
    
    public AbstractInterfaceGenerator( String[] args ) {
        this.args = args;
    }
    
    protected String[] args;
    
    /**
     * Generate the Java implementation class for a Fractal interface.
     * For attribute-controller interfaces, generate also the fields to store a
     * copy of the component state and a cloneFcCtrl method to clone this state.
     * 
     * @param boundable  true if a boundable interface must be generated
     *                   false if an implemented interface must be generated
     */
    protected void generateInterface( String originterfacename, boolean boundable )
    throws IOException, ClassNotFoundException {
        
        String interfacename = packageprefix + "." + originterfacename;
        String classname =
            interfacename +
            (String) ((boundable) ?
                        "BoundableInterface" : "ImplementedInterface");
        int lastdot = interfacename.lastIndexOf('.');
        String packagename = interfacename.substring(0,lastdot);
        String shortclassname = classname.substring(lastdot+1);
        
        String dirname = generatedDirname+"/"+packagename.replace('.','/');
        String filename = dirname+"/"+shortclassname+".java";
        new File(dirname).mkdirs();
        System.out.println("Writing into: "+filename);
        
        FileWriter fw = new FileWriter(filename);
        PrintWriter pw = new PrintWriter(fw);
        
        /*
         * Determine whether the interface extends AttributeController.
         * State cloning is only generated for ImplementedInterfaces.
         */
        Class originterface = PlatformHelper.loadClass(originterfacename);
        boolean isAttributeControllerItf =
            (!boundable) &&
            AttributeController.class.isAssignableFrom(originterface);
        
        prologue(
                pw,packagename,classname,originterfacename,
                boundable,
                isAttributeControllerItf);
        
        // Delegate field and setter/getter
        if( ! boundable ) {
            pw.println("    private "+originterfacename+" _delegate;");
            pw.println();
            pw.println("    public void setFcItfImpl( Object fcContent ) {");
            pw.println("        this.fcContent = fcContent;");
            pw.println("        this._delegate = ("+originterfacename+") fcContent;");
            pw.println("    }");
            pw.println();
        }
        
        /*
         * State variables set by a setter.
         * Used for attribute controller interfaces to determine the state of
         * the attribute controller.
         * 
         * Map<String setterMethod,String fieldType>
         */
        Map state = new HashMap();
        
        Class cl = PlatformHelper.loadClass(originterfacename);
        Method[] methods = cl.getMethods();
        for (int i = 0; i < methods.length; i++) {
            
            Method m = methods[i];
            Class rtype = m.getReturnType();
            String methName = m.getName();
            
            // Method name
            pw.print("    public ");
            pw.print(getClassName(rtype));
            pw.print(" ");
            pw.print(m.getName());
            
            // Method parameters
            pw.print("( ");
            Class[] ptypes = m.getParameterTypes();
            for (int j = 0; j < ptypes.length; j++) {
                if(j!=0)  pw.print(", ");
                Class ptype = ptypes[j];
                pw.print(getClassName(ptype)+" arg"+j);
                
                if( isAttributeControllerItf && methName.startsWith("set") ) {
                    // Setters with more than one field are not supported
                    if( j != 0 ) {
                        throw new IllegalArgumentException(
                                "Setter "+methName+
                                " should have only one parameter");
                    }
                    
                    // Record the type set by the setter
                    state.put(methName,getClassName(ptype));
                }
            }
            pw.print(" ) ");
            
            // Method exceptions
            Class[] etypes = m.getExceptionTypes();
            if( etypes.length != 0 ) {
                pw.print("throws ");
            }
            for (int j = 0; j < etypes.length; j++) {
                Class etype = etypes[j];
                if(j!=0)  pw.print(", ");
                pw.print(etype.getName());
            }
            
            pw.println(" {");
            
            
            // State recording for setters
            if( isAttributeControllerItf && methName.startsWith("set") ) {
                pw.print("        ");
                pw.println("this."+getFieldName(methName)+" = arg0;");
            }
            
            // Proceed the call to the delegate
            pw.print("        ");
            if( ! rtype.getName().equals("void") ) {
                pw.print("return ");
            }
            if( boundable ) {
                pw.print("((");
                pw.print(originterfacename);
                pw.print(")");
                pw.print("lookupFc()).");
                pw.print(m.getName());
                pw.print("(");
                for (int j = 0; j < ptypes.length; j++) {
                    if(j!=0)  pw.print(", ");
                    pw.print("arg");
                    pw.print(j);
                }
                pw.println(");");
            }
            else {
                pw.print("_delegate.");
                pw.print(m.getName());
                pw.print("(");
                for (int j = 0; j < ptypes.length; j++) {
                    if(j!=0)  pw.print(", ");
                    pw.print("arg");
                    pw.print(j);
                }
                pw.println(");");
            }
            
            pw.println("    }");
        }
        
        // Generate the fields to store a copy of the component state
        if( isAttributeControllerItf ) {
            
            // Field
            pw.println();
            for (Iterator iter = state.keySet().iterator(); iter.hasNext();) {
                String methName = (String) iter.next();
                String fieldType = (String) state.get(methName);
                pw.print("    ");
                pw.println("private "+fieldType+" "+getFieldName(methName)+";");
            }
            
            // AttributeControllerCloneableItf.cloneFc() method
            pw.println();
            pw.println("    public void cloneFcCtrl( AttributeControllerCloneableItf dst )");
            pw.println("    throws CloneCtrlException {");
            for (Iterator iter = state.keySet().iterator(); iter.hasNext();) {
                String methName = (String) iter.next();
                pw.print("      ");
                pw.print("(("+originterfacename+")dst).");
                pw.println(methName+"(this."+getFieldName(methName)+");");
            }
            pw.println("    }");
        }
        
        
        epilogue(pw);
        
        pw.close();
        fw.close();
    }
    
    /**
     * Return the Java Bean compliant field name associated to a given setter
     * method name. E.g. getFieldName("setFooBar") returns "fooBar".
     */
    private String getFieldName( String setter ) {
        
        if( setter.length() < 4 || ! setter.startsWith("set") ) {
            throw new IllegalArgumentException(
                    setter+
                    " does not follow the Java Bean conventions for a setter name");
        }
        
        return
            setter.substring(3,4).toLowerCase() + setter.substring(4);
    }
    
    protected void prologue(
            PrintWriter pw,
            String packagename, String classname, String interfacename,
            boolean boundable, boolean isAttributeControllerItf )
    throws IOException {
        
        pw.println("/*");
        pw.print(" * Generated by AOKell InterfaceGenerator on: ");
        pw.println(new Date().toString());
        pw.println(" */");
        pw.println();
        
        final int lastdot = classname.lastIndexOf('.');
        final String shortclassname = classname.substring(lastdot+1);
        final String extclassname =
            (boundable) ? "InterfaceImpl" : "ComponentInterfaceImpl";
        
        pw.println("package "+packagename+";");
        pw.println();
        pw.println("import "+AOKellGeneratedItf.class.getName()+";");
        pw.println("import "+AttributeControllerCloneableItf.class.getName()+";");
        pw.println("import "+CloneCtrlException.class.getName()+";");
        if(boundable) {
            pw.println("import "+InterfaceImpl.class.getName()+";");
        }
        else {
            pw.println("import "+ComponentInterfaceImpl.class.getName()+";");
        }
        pw.println();
        pw.println("public class "+shortclassname+" extends "+extclassname);
        pw.print("    implements AOKellGeneratedItf, "+interfacename);
        if( isAttributeControllerItf ) {
            pw.print(", AttributeControllerCloneableItf");
        }
        pw.println(" {");
        pw.println();
    }

    protected void epilogue( PrintWriter pw ) throws IOException {
        pw.println("}");
    }
    
    /**
     * Workaround for Class.getName() which returns something like
     * Laokell.lib.control.Controller; for array types.
     */
    protected String getClassName( Class cl ) {
        
        if( cl.isArray() ) {
            return getClassName(cl.getComponentType())+"[]";
        }
        
        return cl.getName();
    }
}
