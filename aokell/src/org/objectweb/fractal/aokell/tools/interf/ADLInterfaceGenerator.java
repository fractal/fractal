/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.tools.interf;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;

/**
 * This tool generates the implementation classes of Fractal interfaces.
 * The interfaces are extracted from a given ADL definition.
 * 
 * For each interface two classes are generated: one for server interfaces of
 * primitive component (so called ImplementedInterface) and one for client
 * interfaces and server interfaces exported by composite components (so called
 * BoundableInterface).
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class ADLInterfaceGenerator extends AbstractInterfaceGenerator {
    
    private static void usage() {
        System.out.println(
                "java "+ADLInterfaceGenerator.class.getName()+
                " [-d dir] ADL type[s]");
        System.out.println("Description: ");
        System.out.println("  Generate component interface implementations referenced by the given ADL types");
        System.out.println("Option:");
        System.out.println("  -d dir : the directory where files will be generated (default generated/)");
    }
    
    public static void main(String[] args) throws Exception {
        
        if( args.length == 0 ) {
            usage();
            return;
        }
        
        new ADLInterfaceGenerator(args).compile();
    }
    
    public ADLInterfaceGenerator( String[] args ) {
        super(args);
    }
        
    public void compile() throws IOException, ClassNotFoundException {        
        
        for (int i = 0; i < args.length; i++) {
            if( args[i].equals("-d") ) {
                if( i == args.length-1 ) {
                    usage();
                    return;
                }
                i++;
                generatedDirname = args[i];
            }
            else {
                try {
                    generateFromADL(args[i]);
                }
                catch( ADLException ae ) {
                    String msg = ae.getClass().getName()+": "+ae.getMessage();
                    throw new IllegalArgumentException(msg);
                }
            }
        }
        
        System.out.println("Done.");
    }
    
    /**
     * Generate from the given ADL file.
     */
    protected void generateFromADL( String adl )
    throws ADLException, IOException, ClassNotFoundException {
                
        /*
         * Retrieve interface signatures.
         */
        
        // interfaces is of type Set<String> where String is an interface signature
        Set interfaces = new HashSet();
        
        Map context = new HashMap();
        context.put("interfaces",interfaces);

        // Backend.fractal must be stored in the same package as ADLInterfaceGenerator 
        final String backend =
            ADLInterfaceGenerator.class.getPackage().getName() + ".Backend";
        Factory f = FactoryFactory.getFactory(backend);
        f.newComponent(adl,context);
        
        /*
         * Generate classes implementing Fractal interfaces.
         */
        for (Iterator iter = interfaces.iterator(); iter.hasNext();) {
            String interf = (String) iter.next();
            generateInterface(interf, false);
            generateInterface(interf, true);
        }
    }
}
