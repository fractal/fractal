/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.tools.interf;

import java.io.IOException;

import org.objectweb.fractal.aokell.lib.util.PlatformHelper;

/**
 * This tool generates the implementation classes of Fractal interfaces.
 * 
 * For each interface two classes are generated: one for server interfaces of
 * primitive component (so called ImplementedInterface) and one for client
 * interfaces and server interfaces exported by composite components (so called
 * BoundableInterface).
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class InterfaceGenerator extends AbstractInterfaceGenerator {
    
    private static void usage() {
        System.out.println(
                "java "+InterfaceGenerator.class.getName()+
                " [-d dir] type[s]");
        System.out.println("Description: ");
        System.out.println("  Generate component interface implementations for the given interface types");
        System.out.println("Option:");
        System.out.println("  -d dir : the directory where files will be generated (default generated/)");
    }
    
    public static void main(String[] args) throws Exception {
        
        if( args.length == 0 ) {
            usage();
            return;
        }
        
        new InterfaceGenerator(args).compile();
    }
    
    public InterfaceGenerator( String[] args ) {
        super(args);
    }
    
    
    public void compile() throws IOException, ClassNotFoundException {        
        
        for (int i = 0; i < args.length; i++) {
            if( args[i].equals("-d") ) {
                if( i == args.length-1 ) {
                    usage();
                    return;
                }
                i++;
                generatedDirname = args[i];
            }
            else {
                try {
                    generateFromJava(args[i]);
                }
                catch( IllegalArgumentException iae ) {
                    // Not a Java interface
                    String msg = args[i]+" is not a Java interface.";
                    throw new IllegalArgumentException(msg);
                }
            }
        }
        
        System.out.println("Done.");
    }
    
    /**
     * Generate from the given Java file.
     */
    protected void generateFromJava( String java )
    throws IOException, ClassNotFoundException {
        
        Class cl = PlatformHelper.loadClass(java);
        if( ! cl.isInterface() ) {
            throw new IllegalArgumentException(java+" is not an interface");
        }

        generateInterface(java, false);
        generateInterface(java, true);
    }
    
}
