/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.tools.interf;

import java.util.Map;
import java.util.Set;


/**
 * Type builder component for generating classes implementing Fractal
 * interfaces.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class TypeBuilder
    implements org.objectweb.fractal.adl.types.TypeBuilder {

    // -----------------------------------------------------------------------
    // Implementation of the TypeBuilder interface
    // -----------------------------------------------------------------------
    
    public Object createInterfaceType (
      final String name,
      final String signature,
      final String role,
      final String contingency,
      final String cardinality, 
      final Object context) throws Exception
    {
        if( signature != null ) {
            Set interfaces = (Set)((Map)context).get("interfaces");
            
            // interfaces is of type Set<String>
            // Set<interface signatures>

            interfaces.add(signature);
        }
        
        return null;
    }

    public Object createComponentType (
      final String name,
      final Object[] interfaceTypes, 
      final Object context) throws Exception 
    {
        return null;
    }
}
