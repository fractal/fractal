/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.aokell.lib.membrane.MembraneDef;


/**
 * Membrane repository.
 * This class is a singleton.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class Membranes {

    /** The singleton instance of itself. */
    private static Membranes singleton = new Membranes();
    private Membranes() {
        /*
         * Register AOKell default membranes.
         */
        register(AOKellMembranes.getMembranes());
    }
    
    /**
     * Return the singleton instance of this class.
     */
    public static Membranes get() { return singleton; }
    
    
    // Map<Object,MembraneDef>
    // <controller desc, membrane def>
    /** Membrane definitions. */
    private Map mdefs = new HashMap();
    
    
    /**
     * Register a new membrane.
     * 
     * @param mdef  the membrane definition to register
     */
    public void register( MembraneDef mdef ) {
        
        Object controllerdesc = mdef.getControllerDesc();
        if( mdefs.containsKey(controllerdesc) ) {
            throw new IllegalArgumentException(
                    "Membrane "+controllerdesc+" is already registered" );
        }
        mdefs.put(controllerdesc,mdef);
    }
    
    /**
     * Register new membranes.
     */
    public void register( MembraneDef[] mdefs ) {
       for (int i = 0; i < mdefs.length; i++) {
           register(mdefs[i]);
       }
    }
    
    /**
     * Return true if the given controller description is registered with the
     * repository.
     */
    public boolean containsControllerDesc( Object controllerDesc ) {
        return mdefs.containsKey(controllerDesc);
    }
    
    /**
     * Return the MembraneDef instance associated to the given controller
     * description.
     */
    public MembraneDef getMembraneDef( Object controllerDesc ) {
        return (MembraneDef) mdefs.get(controllerDesc);
    }
    
    /**
     * Erase all registered membrane definitions.
     * 
     * This method is needed to be able to launch tests or to launch several
     * times an application within the same JVM. As Membranes is a singleton,
     * reseting the repository of membrane definitions allows re-registering
     * the same definition without causing an IllegalArgumentException in
     * register().
     * 
     * This method should be used cautiously. This method is declared with the
     * "package public" modifier to limit any possible misuse.
     */
    void reset() {
        mdefs = new HashMap();
    }
}
