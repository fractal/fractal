/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.component;

import org.objectweb.fractal.aokell.lib.factory.GenericFactoryImpl;
import org.objectweb.fractal.aokell.lib.factory.TypeFactoryImpl;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;


/**
 * Bootstrap component providing a
 * <a href="http://www.objectweb.org/fractal/current/doc/javadoc/fractal/org/objectweb/fractal/api/type/TypeFactory.html">TypeFactory</a>
 * and a
 * <a href="http://www.objectweb.org/fractal/current/doc/javadoc/fractal/org/objectweb/fractal/api/factory/GenericFactory.html">GenericFactory</a>.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class BootstrapComponentImpl implements TypeFactory, GenericFactory {
    
    // -----------------------------------------------------------------
    // Implementation of the TypeFactory interface
    // -----------------------------------------------------------------
    
    private TypeFactory tf = TypeFactoryImpl.get();
    
    public InterfaceType createFcItfType(
            String name, String signature,
            boolean isClient, boolean isOptional, boolean isCollection)
    throws InstantiationException {
        
        return tf.createFcItfType(
                name,signature,isClient,isOptional,isCollection);        
    }

    public ComponentType createFcType(InterfaceType[] interfaceTypes)
    throws InstantiationException {
        
        return tf.createFcType(interfaceTypes);
    }

    // -----------------------------------------------------------------
    // Implementation of the GenericFactory interface
    // -----------------------------------------------------------------
    
    private GenericFactory gf = GenericFactoryImpl.get();
    
    public Component newFcInstance(
            Type type, Object controllerDesc, Object contentDesc)
    throws InstantiationException {
        
        return gf.newFcInstance(type,controllerDesc,contentDesc);        
    }
    
}
