/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.control.binding;

import org.objectweb.fractal.aokell.lib.control.content.ContentControllerImpl;
import org.objectweb.fractal.aokell.lib.control.content.ContentControllerItf;
import org.objectweb.fractal.aokell.lib.util.MembraneHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.type.InterfaceType;


/**
 * Implementation of the binding controller for composite components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class CompositeBindingControllerImpl
    extends PrimitiveBindingControllerImpl
    implements BindingController {  // Needed for fractalrmi

    public CompositeBindingControllerImpl() {
        super();
    }
    
    
    // --------------------------------------------------------------
    // Implementation specific
    // --------------------------------------------------------------
    
    /**
     * Test whether the given source interface can be bound to the given target
     * interface.
     * 
     * @throws IllegalBindingException  if the binding is illegal
     */
    protected void testLegalBinding( Interface srcItf, Object arg1 )
    throws IllegalBindingException, NoSuchInterfaceException {
        
        super.testLegalBinding(srcItf,arg1);

        Component compctrl = MembraneHelper.getFcCompCtrlImpl(this.compctrl);
        Interface targetItf = (Interface) arg1;
        Component target = targetItf.getFcItfOwner();
        
        if( ! srcItf.isFcInternalItf() &&
            ContentControllerImpl.isFcContainedIn(true,target,compctrl) ) {

            throw new IllegalBindingException(
                    "Trying to bind a client interface owned by a composite "+
                    "to a server interface of one of its sub-components: "+
                    toString(srcItf,targetItf) );
        }
    }
    
    protected Object[] getFcInterfaces() {
        
        ContentController cc = MembraneHelper.getFcContentCtrl(this.compctrl);
        Object[] i1 = super.getFcInterfaces();
        Object[] i2 = cc.getFcInternalInterfaces();
        
        Object[] all = new Object[ i1.length + i2.length ];
        System.arraycopy(i1,0,all,0,i1.length);
        System.arraycopy(i2,0,all,i1.length,i2.length);
        
        return all;
    }

    /**
     * Register a new interface with the component controller. This method is
     * used for registering new instances of a collection interface.
     * 
     * @throws NoSuchInterfaceException
     *      if the interface is not a collection interface
     */
    protected Interface registerFcInterface( String interfaceName )
    throws NoSuchInterfaceException {
        
        Interface extitf = super.registerFcInterface(interfaceName);
        
        ContentControllerItf cc = MembraneHelper.getFcContentCtrl(this.compctrl);
        Interface intitf = cc.registerFcInterface(interfaceName);
        
        return
            ( ((InterfaceType)extitf.getFcItfType()).isFcClientItf() ) ?
            extitf : intitf ;
    }
    
}
