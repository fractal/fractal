/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.aokell.lib.control.lifecycle;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.aokell.lib.control.CloneCtrlException;
import org.objectweb.fractal.aokell.lib.control.Controller;
import org.objectweb.fractal.aokell.lib.membrane.marker.LifeCycleType;
import org.objectweb.fractal.aokell.lib.util.MembraneHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator;
import org.objectweb.fractal.util.ContentControllerHelper;


/**
 * Implementation of the lifecycle controller for non composite components.
 * 
 * This class originates from the Julia source code. Although this class is not
 * present at this in the Julia source code, it has been generated from the
 * existing mixin chunks. 
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class LifeCycleControllerImpl
    implements LifeCycleCoordinatorItf, Controller {
    
    public LifeCycleControllerImpl() {
        super();
    }
    
    // --------------------------------------------------------------
    // LifeCycleController interface
    // --------------------------------------------------------------
    
    /** 
     * The life cycle state of this component. Zero means stopped, one means
     * stopping and two means started.
     */
    private int fcState;
    
    /** 
     * Checks the mandatory client interfaces of the component (and of all its sub
     * components) and then calls the overriden method.
     * 
     * @throws IllegalLifeCycleException if a problem occurs.
     */
    public void startFc() throws IllegalLifeCycleException {
        Component compctrl = MembraneHelper.getFcCompCtrlImpl(this.compctrl);
        List allSubComponents = ContentControllerHelper.getAllSubComponents(compctrl);
        for (int i = 0 ; i < (allSubComponents.size()) ; ++i) {
            Component subComponent = ((Component)(allSubComponents.get(i)));
            try {
                checkFcMandatoryInterfaces(subComponent);
            } catch (IllegalBindingException e) {
                final String msg = "Cannot start the component";
                throw new IllegalLifeCycleException(msg);
            }
        }
        startFc$0();
    }
    
    public String getFcState() {
        return (fcState) == 0 ? LifeCycleController.STOPPED : LifeCycleController.STARTED;
    }
    
    public void stopFc() throws IllegalLifeCycleException {
        if ((fcState) == 2) {
            stopFc(new LifeCycleCoordinator[]{ getFcCoordinator() });
            setFcState(false);
        }
    }
    
    
    // --------------------------------------------------------------
    // LifeCycleCoordinator interface
    // --------------------------------------------------------------
    
    /** 
     * The components that are currently active.
     */
    private List fcActive;
    
    public boolean fcActivated(final LifeCycleCoordinator component) {
        synchronized(fcActive) {
            if ((fcActive.size()) > 0) {
                if (!fcActive.contains(component)) {
                    fcActive.add(component);
                }
                return true;
            }
            return false;
        }
    }
    
    public void fcInactivated(final LifeCycleCoordinator component) {
        synchronized(fcActive) {
            fcActive.remove(component);
            fcActive.notifyAll();
        }
    }
    
    /** 
     * Calls the overriden method and then calls the {@link #setFcContentState
     * setFcContentState} method.
     * 
     * @return <tt>true</tt> if the execution state has changed, or <tt>false</tt>
     *      if it had already the {@link #STARTED STARTED} value.
     * @throws IllegalLifeCycleException if a problem occurs.
     */
    public boolean setFcStarted() throws IllegalLifeCycleException {
        synchronized(this) {
            if (setFcStarted$0()) {
                setFcContentState(true);
                return true;
            }
            return false;
        }
    }
    
    /** 
     * Calls the overriden method and then calls the {@link #setFcContentState
     * setFcContentState} method.
     *
     * @return <tt>true</tt> if the execution state has changed, or <tt>false</tt>
     *      if it had already the {@link #STOPPED STOPPED} value.
     * @throws IllegalLifeCycleException if a problem occurs.
     */
    public boolean setFcStopped() throws IllegalLifeCycleException {
        synchronized(this) {
            if (setFcStopped$0()) {
                setFcContentState(false);
                return true;
            }
            return false;
        }
    }
    
    public void setFcStopping(final LifeCycleCoordinator coordinator) throws IllegalLifeCycleException {
        synchronized(this) {
            fcState = 1;
            fcCoordinator = coordinator;
            if ((fcInvocationCounter) == 0) {
                fcCoordinator.fcInactivated(getFcCoordinator());
            }
        }
    }
    

    // --------------------------------------------------------------
    // InvocationCounter interface
    // --------------------------------------------------------------
    
    /** 
     * The number of currently executing method calls in this component.
     */
    private int fcInvocationCounter;
    
    /** 
     * Increments the number of currently executing method calls in this
     * component. If the component is started this method just increments this
     * counter. If the component is stopped, it waits until the component is
     * restarted, and then increments the counter. Finally, if the component is
     * stopping, there are two cases. If the counter is not null, it is just
     * incremented. If it is null, this method asks the coordinator if the method
     * call can be executed, or if it should be delayed until the component is
     * restarted. The method then blocks or not, depending on the coordinator's
     * response, before incrementing the counter.
     */
    public void incrementFcInvocationCounter() {
        boolean ok;
        do {
            if ((fcState) == 0) {
                ok = false;
            } else
            if ((fcState) == 1) {
                    if ((fcInvocationCounter) == 0) {
                        ok = fcCoordinator.fcActivated(getFcCoordinator());
                    } else {
                        ok = true;
                    }
                } else {
                    ok = true;
                }
            if (!ok) {
                try {
                    wait();
                } catch (final InterruptedException e) {
                }
            }
        } while (!ok );
        ++fcInvocationCounter;
    }
    
    /** 
     * Decrements the number of currently executing method calls in this
     * component. If the component is stopping, and if the counter of currently
     * executing method calls becomes null after being decremented, this method
     * notifies the coordinator that the component has become inactive.
     */
    public void decrementFcInvocationCounter() {
        --fcInvocationCounter;
        if ((fcInvocationCounter) == 0) {
            fcCoordinator.fcInactivated(getFcCoordinator());
        }
    }
    

    // --------------------------------------------------------------
    // Controller interface
    // --------------------------------------------------------------
    
    private Component compctrl;
    
    /** 
     * Set the reference towards the component controller associated to this
     * controller.
     */
    public void setFcCompCtrl(Component compctrl) {
        this.compctrl = compctrl;
    }
    
    /** 
     * Initialize the controller.
     */
    public void initFcCtrl() {
        // Indeed empty
    }

    /** 
     * Clone the controller state from the current component to another one.
     * This method may receive some hints on how to do this, or provide some
     * hints on how this has been done. For instance, the hints may be a map
     * that is read and/or written by the controller. The raison d'�tre of
     * these hints is that when its state is cloned, a controller may produce
     * results that are needed by other controllers.
     * 
     * @param dst    the destination component
     * @param hints  hints for performing the operation
     */
    public void cloneFcCtrl(Component dst, Object hints) throws CloneCtrlException {
        // Indeed empty
    }
    
    
    // --------------------------------------------------------------
    // Implementation specific
    // --------------------------------------------------------------
    
    /** 
     * The coordinator used to stop this component with other components
     * simultaneously.
     */
    private LifeCycleCoordinator fcCoordinator;
    
    /** 
     * Sets the lifecycle state of this component and of all its direct and
     * indirect sub components that have a {@link LifeCycleCoordinator} interface.
     *
     * @param started <tt>true</tt> to set the lifecycle state of the components
     *      to {@link #STARTED STARTED}, or <tt>false</tt> to set this state to
     *      {@link #STOPPED STOPPED}.
     * @throws IllegalLifeCycleException if a problem occurs.
     */
    private void setFcState(final boolean started) throws IllegalLifeCycleException {
        Component compctrl = MembraneHelper.getFcCompCtrlImpl(this.compctrl);
        List allSubComponents = ContentControllerHelper.getAllSubComponents(compctrl);
        for (int i = 0 ; i < (allSubComponents.size()) ; ++i) {
            Component c = ((Component)(allSubComponents.get(i)));
            LifeCycleCoordinator lc;
            try {
                lc = ((LifeCycleCoordinator)(c.getFcInterface("lifecycle-controller")));
            } catch (Exception e) {
                try {
                    lc = ((LifeCycleCoordinator)(c.getFcInterface("/lifecycle-coordinator")));
                } catch (NoSuchInterfaceException f) {
                    continue;
                }
            }
            if (started) {
                lc.setFcStarted();
            } else {
                lc.setFcStopped();
            }
        }
    }
    
    /** 
     * Calls the {@link LifeCycleController#startFc startFc} or {@link
     * LifeCycleController#stopFc stopFc} method on the encapsulated component.
     * This method does nothing if there is no encapsulated component, or if it
     * does not implement the {@link LifeCycleController} interface.
     *
     * @param started <tt>true</tt> to call the {@link LifeCycleController#startFc
     *      startFc}, or <tt>false</tt> to call the {@link
     *      LifeCycleController#stopFc stopFc} method.
     * @throws IllegalLifeCycleException if a problem occurs.
     */
    private void setFcContentState(final boolean started) throws IllegalLifeCycleException {
        Component compctrl = MembraneHelper.getFcCompCtrlImpl(this.compctrl);
        Object content;
        try {
            content = compctrl.getFcInterface("/content");
        } catch (NullPointerException e) {
            return ;
        } catch (NoSuchInterfaceException e) {
            return ;
        }
        
        /*
         * Optimization: set the value of the introduced isFcStarted field.
         * This field caches the lifecycle controller state.
         */
        if( content instanceof LifeCycleInterceptorItf ) {
            ((LifeCycleInterceptorItf)content).setFcState(started);
        }
        
        /*
         * For Julia-like components, notify the content. AO components can not
         * be notified: the control interface is directly introduced into the
         * content.
         */
        if( !(content instanceof LifeCycleType) &&
              content instanceof LifeCycleController ) {
                if (started) {
                    ((LifeCycleController)(content)).startFc();
                } else {
                    ((LifeCycleController)(content)).stopFc();
                }
            }
    }
    
    /** 
     * Checks that all the mandatory client interface of the given component are
     * bound.
     *
     * @param c a component.
     * @throws IllegalBindingException if a mandatory client interface of the
     *      given component is not bound.
     */
    private void checkFcMandatoryInterfaces(final Component c) throws IllegalBindingException {
        BindingController bc;
        try {
            bc = ((BindingController)(c.getFcInterface("binding-controller")));
        } catch (NoSuchInterfaceException e) {
            return ;
        }
        ComponentType compType = ((ComponentType)(c.getFcType()));
        String[] names = bc.listFc();
        for (int i = 0 ; i < (names.length) ; ++i) {
            InterfaceType itfType;
            try {
                itfType = compType.getFcInterfaceType(names[i]);
            } catch (NoSuchInterfaceException e) {
                continue;
            }
            if ((itfType.isFcClientItf()) && !itfType.isFcOptionalItf()) {
                Object sItf;
                try {
                    sItf = bc.lookupFc(names[i]);
                } catch (NoSuchInterfaceException e) {
                    continue;
                }
                if (sItf == null) {
                    final String msg = "Mandatory client interface unbound";
                    throw new IllegalBindingException(msg);
                }
            }
        }
    }
    
    private void startFc$0() throws IllegalLifeCycleException {
        if (fcState != 2) {
            setFcState(true);
        }
    }
    
    private boolean setFcStarted$0() throws IllegalLifeCycleException {
        synchronized(this) {
            if (fcState == 2) {
                return false;
            }
            fcState = 2;
            notifyAll();
            return true;
        }
    }
    
    /** 
     * Stops the given components simultaneously. This method sets the state of
     * the components to "<tt>STOPPING</tt>", waits until all the components
     * are simultaneoulsy inactive (their state is known thanks to the {@link
     * #fcActivated fcActivated} and {@link #fcInactivated fcInactivated} callback
     * methods), and then sets the state of the components to {@link #STOPPED
     * STOPPED}.
     *
     * @param components the {@link LifeCycleCoordinator} interface of the
     *      components to be stopped.
     * @throws IllegalLifeCycleException if a problem occurs.
     */
    private void stopFc(final LifeCycleCoordinator[] components) throws IllegalLifeCycleException {
        fcActive = new ArrayList();
        for (int i = 0 ; i < (components.length) ; ++i) {
            if (components[i].getFcState().equals(LifeCycleController.STARTED)) {
                fcActive.add(components[i]);
            }
        }
        Component compctrl = MembraneHelper.getFcCompCtrlImpl(this.compctrl);
        LifeCycleCoordinator c;
        try {
            c = ((LifeCycleCoordinator)(compctrl.getFcInterface("lifecycle-controller")));
        } catch (Exception e) {
            try {
                c = ((LifeCycleCoordinator)(compctrl.getFcInterface("/lifecycle-coordinator")));
            } catch (NoSuchInterfaceException f) {
                final String msg = "Cannot stop components";
                throw new IllegalLifeCycleException(msg);
            }
        }
        for (int i = 0 ; i < (components.length) ; ++i) {
            if (components[i].getFcState().equals(LifeCycleController.STARTED)) {
                components[i].setFcStopping(c);
            }
        }
        synchronized(fcActive) {
            while ((fcActive.size()) > 0) {
                try {
                    fcActive.wait();
                } catch (InterruptedException e) {
                }
            }
        }
        for (int i = 0 ; i < (components.length) ; ++i) {
            if (components[i].getFcState().equals(LifeCycleController.STARTED)) {
                components[i].setFcStopped();
            }
        }
        fcActive = null;
    }
    
    private boolean setFcStopped$0() throws IllegalLifeCycleException {
        synchronized(this) {
            if ((fcState) == 0) {
                return false;
            }
            fcState = 0;
            fcCoordinator = null;
            return true;
        }
    }
    
    /** 
     * Returns the {@link LifeCycleCoordinator} interface of this component.
     *
     * @return the {@link LifeCycleCoordinator} interface of the component to
     *      which this controller object belongs.
     */
    private LifeCycleCoordinator getFcCoordinator() {
        Component compctrl = MembraneHelper.getFcCompCtrlImpl(this.compctrl);
        try {
            return ((LifeCycleCoordinator)(compctrl.getFcInterface("lifecycle-controller")));
        } catch (Exception e) {
            try {
                return ((LifeCycleCoordinator)(compctrl.getFcInterface("/lifecycle-coordinator")));
            } catch (NoSuchInterfaceException f) {
                throw new Error("Internal error");
            }
        }
    }
    
}
