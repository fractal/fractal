/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.control.binding;

import org.objectweb.fractal.aokell.lib.util.MembraneHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.util.BindingControllerHelper;

/**
 * Implementation of the binding controller for primitive components. This
 * controller also implements the auto-binding feature for collection interfaces
 * imported by a composite.
 * 
 * @author Eric Bruneton
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class PrimitiveAutoBindingControllerImpl
    extends PrimitiveBindingControllerImpl
    implements BindingController {  // Needed for fractalrmi

    public PrimitiveAutoBindingControllerImpl() {
        super();
    }
    
    
    // --------------------------------------------------------------
    // BindingController interface
    // --------------------------------------------------------------
    
    /**
     * Binds the client interface whose name is given to a server interface.
     * In addition, auto bind collection interfaces imported by a composite to
     * the nested component bound to the collection interface.
     */
    public void bindFc(String clientItfName, Object serverItf)
    throws NoSuchInterfaceException, IllegalBindingException,
    IllegalLifeCycleException {
        
        super.bindFc(clientItfName,serverItf);
        
        if( clientItfName.equals("component") ) {
            // Do not perform any check for the interface "component"
            return;
        }
        
        Interface clientItf = (Interface) getFcBoundableInterface(clientItfName);
        InterfaceType clientItfType = (InterfaceType) clientItf.getFcItfType();
        
        if (clientItfType.isFcCollectionItf()) {
            String sItfName = clientItfType.getFcItfName();
            ContentController cc;
            try {
              cc = MembraneHelper.getFcContentCtrl(compctrl);
            } catch (RuntimeException e) {
              cc = null;
            }
            if (cc != null && !clientItfName.equals(sItfName)) {
              Object sItf = cc.getFcInternalInterface(sItfName);
              Object itf = cc.getFcInternalInterface(clientItfName);
              try {
                Object[] itfs =
                    BindingControllerHelper.getFcClientItfsBoundTo((Interface)sItf).toArray();
                for (int i = 0; i < itfs.length; ++i) {
                  Interface cItf = (Interface)itfs[i];
                  InterfaceType cItfType = (InterfaceType)cItf.getFcItfType();
                  if (cItfType.isFcCollectionItf()) {
                    Component c = cItf.getFcItfOwner();
                    BindingController bc = (BindingController)
                        c.getFcInterface("binding-controller");
                    String s =
                      cItfType.getFcItfName() +
                      clientItfName.substring(sItfName.length()); 
                    bc.bindFc(s, itf);
                  }
                }
              } catch (Exception e) {
                throw new IllegalBindingException(
                  "Error during automatic creation of bindings from the model binding");
              }
            }
            if (clientItfName.equals(sItfName)) {
              Interface sItf = (Interface)serverItf;
              InterfaceType sItfType = (InterfaceType)sItf.getFcItfType();
              if (sItf.isFcInternalItf() && 
                  sItfType.isFcCollectionItf() && 
                  sItf.getFcItfName().equals(sItfType.getFcItfName()))
              {
                Component sComp = sItf.getFcItfOwner();
                try {
                  BindingController bc = 
                    (BindingController)sComp.getFcInterface("binding-controller");
                  cc =
                    (ContentController)sComp.getFcInterface("content-controller");
                  String s = sItf.getFcItfName();
                  String[] itfs = bc.listFc();
                  for (int i = 0; i < itfs.length; ++i) {
                    if (itfs[i].length() > s.length() && itfs[i].startsWith(s)) {
                      super.bindFc(
                        clientItfName + itfs[i].substring(s.length()),
                        cc.getFcInternalInterface(itfs[i]));
                    }
                  }
                } catch (Exception e) {
                  throw new IllegalBindingException(
                    "Error during automatic creation of bindings from the model binding");
                }
              }        
            }
          }
    }

    /**
     * Unbinds the client interface whose name is given.
     * In addition, auto unbind collection interfaces imported by a composite to
     * the nested component bound to the collection interface.
     */
    public void unbindFc(String clientItfName)
    throws NoSuchInterfaceException, IllegalBindingException,
    IllegalLifeCycleException {
        
        Interface clientItf = (Interface) getFcBoundableInterface(clientItfName);
        InterfaceType clientItfType = (InterfaceType) clientItf.getFcItfType();
        
        super.unbindFc(clientItfName);
        
        if (clientItfType.isFcCollectionItf()) {
          ContentController cc;
          try {
            cc = MembraneHelper.getFcContentCtrl(compctrl);
          } catch (RuntimeException e) {
            return;
          }
          String sItfName = clientItfType.getFcItfName();
          Object sItf = cc.getFcInternalInterface(sItfName);
          try {
            Object[] itfs =
                BindingControllerHelper.getFcClientItfsBoundTo((Interface)sItf).toArray();
            for (int i = 0; i < itfs.length; ++i) {
              Interface cItf = (Interface)itfs[i];
              InterfaceType cItfType = (InterfaceType)cItf.getFcItfType();
              if (cItfType.isFcCollectionItf()) {
                Component c = cItf.getFcItfOwner();
                BindingController bc = 
                  (BindingController)c.getFcInterface("binding-controller");
                String s =
                  cItfType.getFcItfName() +
                  clientItfName.substring(sItfName.length()); 
                bc.unbindFc(s);
              }
            }
          } catch (Exception e) {
            throw new IllegalBindingException(
              "Error during automatic destruction of bindings from the model binding");
          }
        }
    }
}
