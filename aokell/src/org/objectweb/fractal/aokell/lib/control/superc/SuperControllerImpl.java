/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.control.superc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.aokell.lib.control.CloneCtrlException;
import org.objectweb.fractal.aokell.lib.control.Controller;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.julia.control.content.SuperControllerNotifier;
import org.objectweb.fractal.util.Fractal;


/**
 * Implementation of the super controller.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class SuperControllerImpl
    implements SuperControllerNotifier, Controller {
    
    private List supers = new ArrayList();    

    public SuperControllerImpl() {}
    
    
    // -------------------------------------------------------
    // SuperController interface
    // -------------------------------------------------------
    
    public Component[] getFcSuperComponents() {
        return (Component[]) supers.toArray(new Component[supers.size()]);
    }

    // -------------------------------------------------------
    // SuperControllerNotifier interface
    // -------------------------------------------------------
    
    public void addedToFc( Component c ) {
        supers.add(c);
    }
    
    public void removedFromFc( Component c ) {
        supers.remove(c);
    }
    
    // --------------------------------------------------------------
    // Controller implementation
    // --------------------------------------------------------------
    
    private Component compctrl;
    
    /**
     * Set the reference towards the component controller associated to this
     * controller.
     */
    public void setFcCompCtrl( Component compctrl ) {
        this.compctrl = compctrl;
    }
    
    /**
     * Initialize the controller.
     */
    public void initFcCtrl() {
        // Indeed nothing
    }

    /**
     * Clone the controller state from the current component to another one.
     * This method may receive some hints on how to do this, or provide some
     * hints on how this has been done. For instance, the hints may be a map
     * that is read and/or written by the controller. The raison d'etre of
     * these hints is that when its state is cloned, a controller may produce
     * results that are needed by other controllers.
     * 
     * @param dst    the destination component
     * @param hints  hints for performing the operation
     */
    public void cloneFcCtrl( Component dst, Object hints )
    throws CloneCtrlException {
        
        Map newContent = (Map) hints;
        SuperControllerNotifier sc;
        try {
            sc = (SuperControllerNotifier) Fractal.getSuperController(dst);
        }
        catch( NoSuchInterfaceException nsie ) {
            String mess = "No such interface: "+nsie.getMessage();
            throw new CloneCtrlException(mess);
        }
        
        for (Iterator iter = supers.iterator(); iter.hasNext();) {
            Object sup = iter.next();
            Component newSup = (Component) newContent.get(sup);
            if( newSup != null ) {
                /*
                 * newSup may be null if the super component of the cloned one
                 * is not within the scope of the cloned components.
                 */
                sc.addedToFc(newSup);                
            }
        }
    }
}
