/***
 * AOKell
 * Copyright (C) 2005-06 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.control.lifecycle;

/**
 * This interface is implemented by all component content classes which are
 * under the supervision of the lifecyle interceptor.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public interface LifeCycleInterceptorItf {

    /**
     * Set the state of the component.
     * 
     * @param isFcStarted  true if the component is started
     */
    public void setFcState( boolean isFcStarted );
}
