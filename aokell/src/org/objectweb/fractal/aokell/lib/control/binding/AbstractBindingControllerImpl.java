/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.control.binding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.objectweb.fractal.aokell.lib.control.CloneCtrlException;
import org.objectweb.fractal.aokell.lib.control.Controller;
import org.objectweb.fractal.aokell.lib.control.component.ComponentItf;
import org.objectweb.fractal.aokell.lib.membrane.marker.BindingType;
import org.objectweb.fractal.aokell.lib.util.FractalHelper;
import org.objectweb.fractal.aokell.lib.util.MembraneHelper;
import org.objectweb.fractal.aokell.lib.util.PlatformHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;


/**
 * Root class for binding controllers implementations.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public abstract class AbstractBindingControllerImpl
    implements BindingController, Controller {
    
    /**
     * The bindings managed by this component.
     * key=client interface name.
     * value=bound server interface.
     */
    private Map bindings = new HashMap();
    
    /** The object bound to the "component" client interface. */
    private Object componentBinding;
    
    
    public AbstractBindingControllerImpl() {}
    
    
    // --------------------------------------------------------------
    // BindingController interface
    // --------------------------------------------------------------
    
    /**
     * Returns the names of the client interfaces of the component to which this
     * interface belongs.
     *
     * @return the names of the client interfaces of the component to which this
     *      interface belongs.
     */
    public String[] listFc() {
        
        /*
         * For Julia-like components, notify the content. AO components can not
         * be notified: the control interface is directly introduced into the
         * content.
         */
        Object contentPart = MembraneHelper.getFcContent(compctrl);
        if( ! (contentPart instanceof BindingType) &&
            contentPart instanceof BindingController ) {
            return ((BindingController)contentPart).listFc();
        }
        
        // Get currently bound interface names
        Set names = new HashSet(bindings.keySet());
        
        // Add boundable interfaces not currently bound
        Object[] itfs = getFcInterfaces();
        for (int i = 0; i < itfs.length; i++) {
            Interface itf = (Interface) itfs[i];
            InterfaceType it = (InterfaceType) itf.getFcItfType();
            if( it.isFcClientItf() &&
                ! FractalHelper.isFcControllerInterfaceName(itf.getFcItfName()) ) {
                names.add( itf.getFcItfName() );
            }
        }
        
        return (String[]) names.toArray(new String[names.size()]);        
    }

    /**
     * Returns the interface to which the given client interface is bound. More
     * precisely, returns the server interface to which the client interface
     * whose name is given is bound. This server interface is necessarily in
     * the same address space as the client interface.
     *
     * @param arg0 the name of a client interface of the component to
     *      which this interface belongs.
     * @return the server interface to which the given interface is bound, or
     *      <tt>null</tt> if it is not bound.
     * @throws NoSuchInterfaceException if the component to which this interface
     *      belongs does not have a client interface whose name is equal to the
     *      given name.
     */
    public Object lookupFc(String arg0) throws NoSuchInterfaceException {
        
        Interface itf = null;
        try {
            // throw NoSuchInterfaceException if the interface does not exist
            itf = (Interface) getFcBoundableInterface(arg0);            
        }
        catch( NoSuchInterfaceException nsie ) {
            /*
             * Check whether the request is for a collection interface not
             * currently bound. If so and if the content provides an
             * implementation of the binding controller interface, delegate the
             * request. Else, propagate the exception.
             */
            Component compctrl = MembraneHelper.getFcCompCtrlImpl(this.compctrl);
            ComponentType ct = (ComponentType) compctrl.getFcType();
            InterfaceType[] its = ct.getFcInterfaceTypes();
            for (int i = 0; i < its.length; i++) {
                if( arg0.startsWith(its[i].getFcItfName()) ) {
                    Object contentPart = MembraneHelper.getFcContent(this.compctrl);
                    if( ! (contentPart instanceof BindingType) &&
                        contentPart instanceof BindingController ) {
                        return ((BindingController)contentPart).lookupFc(arg0);
                    }                    
                }
            }
            
            throw nsie;
        }
        
        /*
         * For Julia-like components, notify the content. AO components can not
         * be notified: the control interface is directly introduced into the
         * content.
         */
        Object contentPart = MembraneHelper.getFcContent(compctrl);
        if( ! (contentPart instanceof BindingType) &&
            contentPart instanceof BindingController ) {
            return ((BindingController)contentPart).lookupFc(arg0);
        }
        
        if( arg0.equals("component") ) {
            // Do not perform any check for the interface "component"
            return componentBinding;
        }
        
        if( bindings.containsKey(arg0) ) {
            return bindings.get(arg0);
        }

        // Bindings for collection interfaces are registered with the name of
        // the interface followed by a suffix (0, 1, 2, ...). Hence looking up
        // only the interface name returns null. We then return as a List
        // all the interfaces whose binding name starts with the prefix.
        
        InterfaceType it = (InterfaceType) itf.getFcItfType();
        
        if( it.isFcCollectionItf() ) {
            Set sorted = new TreeSet(bindings.keySet());
            List bounds = null;
            for (Iterator iter = sorted.iterator(); iter.hasNext();) {
                String element = (String) iter.next();
                if( element.startsWith(arg0) ) {
                    if( bounds == null )
                        bounds = new ArrayList();
                    bounds.add( bindings.get(element) );
                }
            }
            return bounds;
        }
        
        /*
         * When this point is reached, the interface exists but this is not a
         * collection interface (hence this is a singleton interface), and it is
         * not bound.
         */
        
        return null;
    }

    /**
     * Binds the client interface whose name is given to a server interface.
     * More precisely, binds the client interface of the component to which this
     * interface belongs, and whose name is equal to the given name, to the
     * given server interface. The given server interface must be in the same
     * address space as the client interface.
     *
     * @param arg0 the name of a client interface of the component to
     *      which this interface belongs.
     * @param arg1 a server interface.
     * @throws NoSuchInterfaceException if there is no such client interface.
     * @throws IllegalBindingException if the binding cannot be created.
     * @throws IllegalLifeCycleException if this component has a {@link
     *      LifeCycleController} interface, but it is not in an appropriate
     *      state to perform this operation.
     */
    public void bindFc(String arg0, Object arg1)
    throws NoSuchInterfaceException, IllegalBindingException,
    IllegalLifeCycleException {
        
        if( arg0.equals("component") ) {
            // Do not perform any check for the interface "component"
            componentBinding = arg1;
            bindFcContent(arg0,arg1);
            return;
        }
        
        Interface itf = null;
        try {
            itf = (Interface) getFcBoundableInterface(arg0);
        }
        catch( NoSuchInterfaceException nsie ) {
            /*
             * Try to register the interface with the component controller or
             * the content controller (for composites).
             * May throw NoSuchInterfaceException again.
             */
            itf = registerFcInterface(arg0);
        }
        
        // Test whether the binding is legal
        testLegalBinding(itf,arg1);
        
        bindFcContent(arg0,arg1);

        // Test whether the interface is not already bound
        if( bindings.containsKey(arg0) )
            throw new IllegalBindingException(
                    "Interface "+arg0+" already bound");
        
        // All tests cleared. Record the binding.
        bindings.put(arg0,arg1);
    }
    
    /**
     * If the content and the component parts are separated, and if the
     * content part implements the BindingController interface, invoke it.
     */
    protected void bindFcContent(String arg0, Object arg1) {
        
        /*
         * For Julia-like components, notify the content. AO components can not
         * be notified: the control interface is directly introduced into the
         * content.
         */
        Object contentPart = MembraneHelper.getFcContent(compctrl);
        if( ! (contentPart instanceof BindingType) &&
            contentPart instanceof BindingController ) {
            try {
                ((BindingController)contentPart).bindFc(arg0,arg1);
            }
            // Ignore exceptions which may be thrown by the content
            catch( NoSuchInterfaceException nsie ) {}
            catch( IllegalBindingException ibe ) {}
            catch( IllegalLifeCycleException ilce ) {}
        }
    }
    
    /**
     * Unbinds the given client interface. More precisely, unbinds the client
     * interface of the component to which this interface belongs, and whose
     * name is equal to the given name.
     *
     * @param arg0 the name of a client interface of the component to
     *      which this interface belongs.
     * @throws NoSuchInterfaceException if there is no such client interface.
     * @throws IllegalBindingException if the binding cannot be removed.
     * @throws IllegalLifeCycleException if this component has a {@link
     *      LifeCycleController} interface, but it is not in an appropriate
     *      state to perform this operation.
     */
    public void unbindFc(String arg0)
    throws NoSuchInterfaceException, IllegalBindingException,
    IllegalLifeCycleException {
        
        /*
         * For Julia-like components, notify the content. AO components can not
         * be notified: the control interface is directly introduced into the
         * content.
         */
        Object contentPart = MembraneHelper.getFcContent(compctrl);
        if( ! (contentPart instanceof BindingType) &&
            contentPart instanceof BindingController ) {
            try {
                ((BindingController)contentPart).unbindFc(arg0);
            }
            // Ignore exceptions which may be thrown by the content
            catch( NoSuchInterfaceException nsie ) {}
            catch( IllegalBindingException ibe ) {}
            catch( IllegalLifeCycleException ilce ) {}
        }

        // throw NoSuchInterfaceException if the interface does not exist
        Interface itf = (Interface) getFcBoundableInterface(arg0);
        
        // Test whether the interface is bound
        if( !bindings.containsKey(arg0) ) {
            throw new IllegalBindingException("Interface "+arg0+" not bound");
        }
        
        // Test whether the component is stopped
        testLifeCyleBeforeUnbinding(compctrl);
        
        // Remove the binding
        bindings.remove(arg0);
        
        /*
         * Unregister the source interface from its component controllers
         * (needed for collection).
         */
        ComponentItf compctrl = MembraneHelper.getFcCompCtrl(this.compctrl);
        compctrl.unregisterFcInterface(arg0);
    }

    
    // --------------------------------------------------------------
    // Implementation specific
    // --------------------------------------------------------------
    
    /**
     * Test whether the component is stopped before performing an unbind
     * operation. Implementations of the binding controller for templates
     * override this method with an empty implementation (template membranes do
     * not have a lifecycle controller.)
     */
    protected void testLifeCyleBeforeUnbinding( Component arg0 )
    throws IllegalLifeCycleException {
        
        LifeCycleController lc = MembraneHelper.getFcLifeCycleCtrl(arg0);
        if( lc.getFcState().equals(LifeCycleController.STARTED) ) {
            throw new IllegalLifeCycleException(
                    "Component must be stopped before performing an operation "+
                    "on the binding controller" );
        }
    }
    
    /**
     * @return    return true if it1 is assignable from it2
     */
    private static boolean isAssignableFrom(
            InterfaceType it1, InterfaceType it2 ) {
        
        Class i1,i2;
        try {
            i1 = PlatformHelper.loadClass( it1.getFcItfSignature() );
            i2 = PlatformHelper.loadClass( it2.getFcItfSignature() );
        }
        catch(ClassNotFoundException cnfe) {
            return false;
        }

        return i1.isAssignableFrom(i2);
    }
    
    /**
     * Test whether the given source interface can be bound to the given target
     * interface.
     * 
     * @throws IllegalBindingException  if the binding is illegal
     */
    protected void testLegalBinding( Interface srcItf, Object arg1 )
    throws IllegalBindingException, NoSuchInterfaceException {
        
        InterfaceType it = (InterfaceType) srcItf.getFcItfType();
        Interface targetItf = (Interface) arg1;
        InterfaceType targetItfType = (InterfaceType) targetItf.getFcItfType();
        
        if( (! it.isFcOptionalItf()) && targetItfType.isFcOptionalItf() ) {
            throw new IllegalBindingException(
                    "Mandatory to optional bindings are forbidden: "+
                    toString(srcItf,targetItf) );
        }

        if( ! isAssignableFrom(it,targetItfType) ) {
            throw new IllegalBindingException(
                    "The target interface is not a sub-type of the source interface: "+
                    toString(srcItf,targetItf) );
        }
        
        if( ! it.isFcClientItf() ) {
            throw new IllegalBindingException(
                    "The source interface is of type server:"+
                    toString(srcItf,targetItf) );
        }
        
        if( targetItfType.isFcClientItf() ) {
            throw new IllegalBindingException(
                    "Target interface is of type client"+
                    toString(srcItf,targetItf) );
        }                
    }
    
    /**
     * Return a string of the form
     * source_component_name.source_interface_name -> target_component_name.target_interface_name
     * describing the binding from a source interface to a target interface.
     */
    protected static String toString( Interface src, Interface target ) {
        
        return
            FractalHelper.getFcFullyQualifiedName(src.getFcItfOwner()) + "." +
            src.getFcItfName() + " -> " +
            FractalHelper.getFcFullyQualifiedName(target.getFcItfOwner()) + "." +
            target.getFcItfName();

    }
    
    protected Object[] getFcInterfaces() {
        Component compctrl = MembraneHelper.getFcCompCtrlImpl(this.compctrl);
        return compctrl.getFcInterfaces();
    }
    
    protected Object getFcBoundableInterface( String interfaceName )
    throws NoSuchInterfaceException {
        
        Object[] itfs = getFcInterfaces();
        for (int i = 0; i < itfs.length; i++) {
            
            Interface itf = (Interface) itfs[i];
            String itfName = itf.getFcItfName();
            InterfaceType it = (InterfaceType) itf.getFcItfType();
            
            if( ! it.isFcClientItf() )
                continue;
            if( FractalHelper.isFcControllerInterfaceName(itfName) )
                continue;
            
            if( itfName.equals(interfaceName) )
                return itf;
            if( it.isFcCollectionItf() && it.getFcItfName().equals(interfaceName) ) {
                /*
                 * Given instances of a collection interface (e.g. h0, h1), the
                 * prefix for the collection (here h) may be requested. In this
                 * case, the prefix does not correspond to the interface name
                 * (h0 or h1) but to the name registered in the interface type
                 * (h).
                 */
                return itf;
            }
        }
        
        throw new NoSuchInterfaceException(interfaceName);
    }
    
    /**
     * Register a new interface with the component controller. This method is
     * used for registering new instances of a collection interface.
     * 
     * @throws NoSuchInterfaceException
     *      if the interface is not a collection interface
     */
    protected Interface registerFcInterface( String interfaceName )
    throws NoSuchInterfaceException {
        
        ComponentItf compctrl = MembraneHelper.getFcCompCtrl(this.compctrl);
        Interface itf = compctrl.registerFcInterface(interfaceName);
        return itf;
    }
    
    
    // --------------------------------------------------------------
    // Controller implementation
    // --------------------------------------------------------------
    
    protected Component compctrl;
    
    /**
     * Set the reference towards the component controller associated to this
     * controller.
     */
    public void setFcCompCtrl( Component compctrl ) {
        this.compctrl = compctrl;
    }
    
    /**
     * Initialize the controller.
     */
    public void initFcCtrl() {
        
        Component compctrl = MembraneHelper.getFcCompCtrlImpl(this.compctrl);
        
        /*
         * Initialize the binding towards the component interface.
         */
        try {
            bindFc("component",compctrl);
        }
        catch( Exception e ) {
            throw new RuntimeException(
                    "Can not bind hidden client interface component");
        }
    }
    
    /**
     * Clone the controller state from the current component to another one.
     * This method may receive some hints on how to do this, or provide some
     * hints on how this has been done. For instance, the hints may be a map
     * that is read and/or written by the controller. The raison d'etre of
     * these hints is that when its state is cloned, a controller may produce
     * results that are needed by other controllers.
     * 
     * @param dst    the destination component
     * @param hints  hints for performing the operation
     */
    public void cloneFcCtrl( Component dst, Object hints )
    throws CloneCtrlException {
        
        /*
         * Map storing associations between the current component (plus its
         * content if this is a composite component) and their associated newly
         * created component.
         */
        Map newContent = (Map) hints;
        BindingController newBc = FractalHelper.getBindingController(dst);
        
        for (Iterator iter = bindings.entrySet().iterator(); iter.hasNext();) {
            Map.Entry entry = (Map.Entry) iter.next();
            String srcItfName = (String) entry.getKey();
            Interface dstItf = (Interface) entry.getValue();
            
            try {
                cloneBinding(srcItfName,dstItf,newContent,newBc);
            }
            catch(Exception e) {
                e.printStackTrace();
                throw new CloneCtrlException(e.getMessage());
            }
        }
    }
    
    /**
     * Clone a binding.
     * 
     * @param itfName     the source interface name
     * @param boundItf    the interface bound to the source
     * @param newContent  the map associating old components and new ones
     * @param newBc       the binding controller of the new component
     */
    private void cloneBinding(
            String itfName, Interface boundItf,
            Map newContent, BindingController newBc)
    throws Exception {
        
        if( boundItf == null )
            return;
        
        String boundItfName = boundItf.getFcItfName();        
        Component boundCompCtrl = boundItf.getFcItfOwner();
        
        Component dstBound = (Component) newContent.get(boundCompCtrl);
        
        if( dstBound == null ) {
            /*
             * Trying to clone a binding which starts from one of the cloned
             * components and which targets a component outside of the scope of
             * the cloned components.
             */
            return;
        }
        
        Interface dstBoundItf =
            (Interface) dstBound.getFcInterface(boundItfName);
        
        ContentController cc = null;
        try {
            cc = FractalHelper.getContentControllerItf(dstBound);
            
            /*
             * The target component is composite.
             * 
             * The server interface may be:
             * - external, if the target component is located at the same
             *   hierarchical level as the source component in the containment
             *   hierarchy,
             * - internal, if the target component encloses the source
             *   component.
             * Always return the server interface. 
             */
            InterfaceType it = (InterfaceType) dstBoundItf.getFcItfType();
            if( it.isFcClientItf() ) {
                dstBoundItf =
                    (Interface) cc.getFcInternalInterface(boundItfName);                
            }
        }
        catch( RuntimeException re ) {
            /*
             * The target component is primitive.
             * Nothing else to do.
             */
        }
        
        // Record the new binding
        newBc.bindFc(itfName,dstBoundItf);        
    }
}
