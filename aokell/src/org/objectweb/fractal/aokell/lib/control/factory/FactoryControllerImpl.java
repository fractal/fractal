/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.control.factory;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.aokell.lib.control.CloneCtrlException;
import org.objectweb.fractal.aokell.lib.control.Controller;
import org.objectweb.fractal.aokell.lib.control.component.ComponentDescItf;
import org.objectweb.fractal.aokell.lib.control.component.ComponentItf;
import org.objectweb.fractal.aokell.lib.factory.GenericFactoryImpl;
import org.objectweb.fractal.aokell.lib.util.FractalHelper;
import org.objectweb.fractal.aokell.lib.util.MembraneHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.julia.factory.Template;
import org.objectweb.fractal.util.ContentControllerHelper;


/**
 * Implementation of the {@link Factory} controller.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class FactoryControllerImpl implements Template, Controller {

    public FactoryControllerImpl() {
    }
    

    // --------------------------------------------------------------
    // Factory interface
    // --------------------------------------------------------------
    
    public Object getFcControllerDesc() {
        ComponentDescItf cdi =
            (ComponentDescItf) FractalHelper.getFcInterface(compctrl,"/desc");
        return cdi.getControllerDesc();
    }
    
    public Object getFcContentDesc() {
        ComponentDescItf cdi =
            (ComponentDescItf) FractalHelper.getFcInterface(compctrl,"/desc");
        return cdi.getContentDesc();
    }
    
    public Type getFcInstanceType() {
        return compctrl.getFcType();
    }
    
    public Component newFcInstance() throws InstantiationException {
    
        /*
         * Map storing instanciated components.
         * index: component in the template,
         * value: associated new component.
         * <Component,Component>
         */
        Map newContent = new HashMap();
        
        /*
         * Get all sub-components including the current one.
         * The list is a singleton if the current component is primitive.
         * Each shared component appears only once in the list.
         * The current component is stored at the beginning of the list.
         */
        Component compctrl = MembraneHelper.getFcCompCtrlImpl(this.compctrl);        
        List subs = ContentControllerHelper.getAllSubComponents(compctrl);
        
        /*
         * Instanciate the template.
         * 
         * Check whether all instantiated components implements the
         * AOKell-specific {@link ComponentItf} interface. This is the case when
         * one deals with homogeneous AOKell component assemblies. This is not
         * the case when one mixes for example AOKell and Julia components.
         */
        boolean homogeneous = true;
        for (Iterator iter = subs.iterator(); iter.hasNext();) {
            Component sub = (Component) iter.next();
            Template t = getTemplateRef(sub);
            Component newSub = t.newFcControllerInstance();
            newContent.put(sub,newSub);
            
            if( !(newSub instanceof ComponentItf) ) {
                homogeneous = false;
            }
        }
        
        /*
         * If the assembly contains only AOKell components, use the cloneFc
         * method implemented by AOKell controllers to clone the state of the
         * membrane for each created component.
         * 
         * If the assembly is heterogeneous, reconstruct the containment
         * hierarchy and the bindings using the Fractal API.
         */
        if( homogeneous ) {
            cloneFcMembrane(newContent,new Class[]{BindingController.class});
        }
        else {
            reconstruct(compctrl,newContent);
        }
        
        /*
         * The clone of the current component is stored at the beginning of the
         * list. See above.
         */
        Component newComponent = (Component) newContent.get(subs.get(0));
        return newComponent;
    }
    
    
    // --------------------------------------------------------------
    // Template interface
    // --------------------------------------------------------------    
    
    /**
     * Instantiates only the controller part of the component normally
     * instantiated by this template. Because of component sharing, the {@link
     * #newFcInstance newFcInstance} method cannot be implemented by just calling
     * itself recursively on the sub templates of this template (otherwise some
     * sub templates may be instantiated several times). Hence this method.
     *
     * @return the instantiated component.
     * @throws InstantiationException if the component controller cannot be
     *      instantiated.
     */
    public Component newFcControllerInstance () throws InstantiationException {
        
        // Templates are created with something like:
        //
        // Component cTmpl = cf.newFcInstance(
        //      cType, "primitiveTemplate",
        //      new Object[] { "primitive", "ClientImpl" });
        //
        // Component cTmpl = cf.newFcInstance(
        //      cType, "compositeTemplate",
        //      new Object[] { "composite", null });
        
        /*
         * Retrieve the description of the current component.
         */
        Component compctrl = MembraneHelper.getFcCompCtrlImpl(this.compctrl);        
        ComponentDescItf cdi =
            (ComponentDescItf) FractalHelper.getFcInterface(compctrl,"/desc");
        Type type = compctrl.getFcType();
        Object controllerDesc = ((Object[])cdi.getContentDesc())[0];
        Object contentDesc = ((Object[])cdi.getContentDesc())[1];
        
        /*
         * Instantiate the current component.
         */
        GenericFactoryImpl gf = GenericFactoryImpl.get();        
        Component newComponent =
            gf.newFcInstance(type,controllerDesc,contentDesc);
        
        return newComponent;
    }


    // --------------------------------------------------------------
    // Implementation specific 
    // --------------------------------------------------------------
    
    /**
     * Return the reference of the template control interface associated to the
     * given component. This control interface is implemented by the same
     * instance as the one implementing the factory controller.
     */
    private static Template getTemplateRef( Component comp ) {
        
        /*
         * Try to get the factory control interface and checks whether it
         * implements the {@link Template} interface.
         * The idea is that it is not mandatory to register a hidden interface
         * name "/template" and that the factory controller can implement both
         * the {@link Factory} and the {@link Template} interfaces.
         */
        try {
            Object ret = comp.getFcInterface(FactoryDef.NAME);
            return (Template) ret;
        }
        catch(NoSuchInterfaceException nsie) {}
        catch(ClassCastException cce) {}
        
        Object ret = FractalHelper.getFcInterface(comp,FactoryDef.HIDDEN_NAME);        
        return (Template) ret;
    }

    /**
     * Clone the membrane of newly created components. Controllers contained in
     * the membrane are cloned according to the order specified in the second
     * parameter which is an array of controller types.
     * 
     * First, all controllers are cloned, except the ones specified in the array
     * of controller types. Next, the controllers are cloned in the order in
     * which they appear in the array.
     * 
     * The rationale for this method lies in the fact that with composite
     * templates, the binding controllers must be cloned last.
     * 
     * @param newContent  the map associating old and new components
     * @param order       the order for cloning controllers. This parameter is
     *                    an array of controller types.
     */
    protected void cloneFcMembrane( Map newContent, Class[] order )
    throws InstantiationException {
        
        /*
         * First, clone all controllers execept the ones specified in order.
         * Next, clone the controllers contained in order.
         */
        for( int rank = -1; rank < order.length; rank++ ) {
            cloneFcMembrane(newContent,order,rank);
        }
    }
    
    private void cloneFcMembrane( Map newContent, Class[] order, int rank )
    throws InstantiationException {

        for( Iterator iter = newContent.entrySet().iterator(); iter.hasNext(); ) {
            
            Map.Entry entry = (Map.Entry) iter.next();
            ComponentItf c = (ComponentItf) entry.getKey();
            Component newC = (Component) entry.getValue();
            
            Object[] ctrls = MembraneHelper.getFcControllerImpls(c);
            
            for( int i = 0; i < ctrls.length; i++ ) {
                if( ctrls[i] instanceof Controller ) {
                    Controller ctrl = (Controller) ctrls[i];
                    cloneFcCtrl(newContent,order,rank,newC,ctrl);
                }
            }
        }
    }
    
    private void cloneFcCtrl(
            Map newContent, Class[] order, int rank,
            Component newC, Controller ctrl )
    throws InstantiationException {
        
        boolean clone = true;
        if( rank == -1 ) {
            for( int o = 0; o < order.length; o++ ) {
                if( order[o].isAssignableFrom(ctrl.getClass()) ) {
                    clone = false;
                }
            }
        }
        else {
            clone = order[rank].isAssignableFrom(ctrl.getClass()) ;
        }
        
        if(clone) {
            try {
                ctrl.cloneFcCtrl(newC,newContent);
            }
            catch( CloneCtrlException cce ) {
                cce.printStackTrace();
                throw new InstantiationException(cce.getMessage());
            }                    
        }
    }
    
    /**
     * Given a source template, reconstruct the containment hierarchy and the
     * bindings of cloned components using Fractal API.
     * 
     * This method has been extracted from the Julia source code.
     * See o.o.f.julia.factory.BasicTemplateMixin#newFcInstance().
     * 
     * @param src  the source template
     * @param instances
     *      a map storing associations between component in the template and
     *      their corresponding clone
     */
    private void reconstruct( Component src, Map instances ) {
        
        List allSubTemplates = ContentControllerHelper.getAllSubComponents(src);
        
        // adds the instances into each other
        for (int i = 0; i < allSubTemplates.size(); ++i) {
          Component tmpl = (Component)allSubTemplates.get(i);
          Component instance = (Component)instances.get(tmpl);

          ContentController tmplCC, instanceCC;
          try {
            tmplCC = (ContentController)tmpl.getFcInterface("content-controller");
          } catch (NoSuchInterfaceException e) {
            continue;
          }
          try {
            instanceCC =
              (ContentController)instance.getFcInterface("content-controller");
          } catch (NoSuchInterfaceException e) {
            throw new RuntimeException(
              "A component instantiated from a template with a ContentController " +
              "interface must provide the ContentController interface");
          }
          Component[] subTemplates = tmplCC.getFcSubComponents();
          Component[] subInstances = instanceCC.getFcSubComponents();
          for (int j = 0; j < subTemplates.length; ++j) {
            Component subInstance = (Component)instances.get(subTemplates[j]);
            boolean add = true;
            for (int k = 0; k < subInstances.length; ++k) {
              if (subInstances[k].equals(subInstance)) {
                // if sunInstance is already a sub component of instance,
                // do not add it again (this can happen with singleton templates)
                add = false;
              }
            }
            if (add) {
              try {
                instanceCC.addFcSubComponent(subInstance);
              } catch (IllegalContentException e) {
                throw new RuntimeException(
                  "Cannot set the component hierarchy from the template hierarchy");
              } catch (IllegalLifeCycleException e) {
                throw new RuntimeException(
                  "Cannot set the component hierarchy from the template hierarchy");
              }
            }
          }
        }

        // binds the instances to each other, as the templates are bound
        for (int i = 0; i < allSubTemplates.size(); ++i) {
          Component tmpl = (Component)allSubTemplates.get(i);
          Component instance = (Component)instances.get(tmpl);

          BindingController tmplBC, instanceBC;
          try {
            tmplBC = (BindingController)tmpl.getFcInterface("binding-controller");
          } catch (NoSuchInterfaceException e) {
            continue;
          }
          try {
            instanceBC =
              (BindingController)instance.getFcInterface("binding-controller");
          } catch (NoSuchInterfaceException e) {
            throw new RuntimeException(
              "A component instantiated from a template with a BindingController " +
              "interface must provide the BindingController interface");
          }
          String[] itfNames = tmplBC.listFc();
          for (int j = 0; j < itfNames.length; ++j) {
            String itfName = itfNames[j];
            Interface serverItf;
            try {
              serverItf = (Interface)tmplBC.lookupFc(itfName);
            } catch (ClassCastException e) {
              throw new RuntimeException(
                "The server interface of each binding between templates " +
                "must implement the Interface interface");
            } catch (NoSuchInterfaceException e) {
              throw new RuntimeException(
                "The '" + itfName +
                "' interface returned by the listFc method does not exist");
            }
            if (serverItf == null) {
              continue;
            }

            Component serverTmpl = serverItf.getFcItfOwner();
            Component serverInstance = (Component)instances.get(serverTmpl);
            if (serverInstance == null) {
              // 'tmpl' bound to a component that does not belong to the root
              // instantiated template: do not create this binding for 'instance'
              continue;
            }

            Object itfValue;
            try {
              if (serverItf.isFcInternalItf()) {
                ContentController cc = (ContentController)serverInstance.
                  getFcInterface("content-controller");
                itfValue = cc.getFcInternalInterface(serverItf.getFcItfName());
              } else {
                itfValue = serverInstance.getFcInterface(serverItf.getFcItfName());
              }
            } catch (NoSuchInterfaceException e) {
              throw new RuntimeException(
                "The server interface '" + serverItf.getFcItfName() +
                "'is missing in the component instantiated from the template");
            }

            try {
              if (instanceBC.lookupFc(itfName).equals(itfValue)) {
                // if the binding already exists, do not create again
                // (this can happen with singleton templates)
                continue;
              }
            } catch (Exception e) {
            }

            try {
              instanceBC.bindFc(itfName, itfValue);
            } catch (NoSuchInterfaceException e) {
              throw new RuntimeException(
                "Cannot set the component bindings from the template bindings");
            } catch (IllegalBindingException e) {
              throw new RuntimeException(
                "Cannot set the component bindings from the template bindings");
            } catch (IllegalLifeCycleException e) {
              throw new RuntimeException(
                "Cannot set the component bindings from the template bindings");
            }
          }
        }
    }
    
    // --------------------------------------------------------------
    // Controller implementation
    // --------------------------------------------------------------
    
    protected Component compctrl;
    
    /**
     * Set the reference towards the component controller associated to this
     * controller.
     */
    public void setFcCompCtrl( Component compctrl ) {
        this.compctrl = compctrl;
    }
    
    /**
     * Initialize the controller.
     */
    public void initFcCtrl() {
        // Indeed nothing
    }

    /**
     * Clone the controller state from the current component to another one.
     * This method may receive some hints on how to do this, or provide some
     * hints on how this has been done. For instance, the hints may be a map
     * that is read and/or written by the controller. The raison d'etre of
     * these hints is that when its state is cloned, a controller may produce
     * results that are needed by other controllers.
     * 
     * @param dst    the destination component
     * @param hints  hints for performing the operation
     */
    public void cloneFcCtrl( Component dst, Object hints )
    throws CloneCtrlException {
        // Indeed nothing
    }
}