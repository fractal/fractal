/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.control.component;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.aokell.lib.InterfaceImpl;
import org.objectweb.fractal.aokell.lib.control.CloneCtrlException;
import org.objectweb.fractal.aokell.lib.control.Controller;
import org.objectweb.fractal.aokell.lib.interf.InterfaceManager;
import org.objectweb.fractal.aokell.lib.interf.PrimitiveExtItfManager;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.type.InterfaceType;


/**
 * Root class for implementations of the Component interface.
 * This class contains common features shared by all classes implementing this
 * interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class ComponentImpl
    
    /*
     * Do not directly implements Interface as this would lead to a behavior
     * under fractal-explorer which is different from the one obtained with
     * Julia. Extending InterfaceImpl is better.
     */
    extends InterfaceImpl
    implements ComponentItf, ComponentDescItf, Controller {
    
    public ComponentImpl() {
        /*
         * Can not call super as the 1st argument should be this (and this is
         * forbidden when calling super). See below: Interface is overriden.
         */
        im = new PrimitiveExtItfManager();
    }
    
    
    // ------------------------------------------------------------------
    // Implementation of the Component interface
    // ------------------------------------------------------------------
    
    /** The interfaces managed by this component. */
    protected InterfaceManager im;
    
    /**
     * Return the instance implementing the {@link Interface} corresponding to
     * the given name. Whereas this is the case most of the time, it is not
     * mandatory for the returned value to be of type {@link Interface}.
     * 
     */
    public Object getFcInterface(String interfaceName)
    throws NoSuchInterfaceException {
        
        try {
            return im.getFcInterface(interfaceName);
        }
        catch( NoSuchInterfaceException nsie ) {}
        
        /*
         * Components with a content (all primitives have a content, composites
         * may have a content) implements the /content interface.
         * The /content interface is not defined in the Fractal specifications
         * but is Julia-specific.
         */
        if( interfaceName.equals("/content") ) {
            return contentPart;
        }
        
        if( interfaceName.equals("/desc") ) {
            return this;
        }
        
        /*
         * This was our last chance. If the interface has still not been found,
         * throw NoSuchInterfaceException.
         */
        throw new NoSuchInterfaceException(interfaceName);
    }
    
    /**
     * Return the array of {@link Interface} instances implemented by this
     * component. Whereas this is the case most of the time, it is not
     * mandatory for the returned values to be of type {@link Interface}.
     */
    public Object[] getFcInterfaces() {
        return im.getFcInterfaces();
    }
    
    public Type getFcType() {
        return type;
    }

    
    // ------------------------------------------------------------------
    // Implementation of the ComponentDescItf interface
    // ------------------------------------------------------------------
        
    public Object getControllerDesc() {
        return controllerDesc;
    }
    
    public Object getContentDesc() {
        return contentDesc;
    }
 
    
    // ------------------------------------------------------------------
    // Implementation of the ComponentItf interface
    // ------------------------------------------------------------------
        
    private Type type;
    private Object controllerDesc;
    private Object contentDesc;
    private Object contentPart;
        
    public void initFc(
            Type type, Object controllerDesc, Object contentDesc,
            Object contentPart ) {
        
        this.type = type;
        this.controllerDesc = controllerDesc;
        this.contentDesc = contentDesc;
        this.contentPart = contentPart;

        im.initFc(type,this,contentPart);
        
        /*
         * Link the content with the current component controller.
         * The content uses this reference in injected controller methods.
         */
        if( contentPart instanceof ComponentSetterItf ) {
            ((ComponentSetterItf)contentPart).setFcCompCtrl(this);
        }
    }

    
    // <InterfaceType[],Object>
    // Not all controllers implements Controller (hence Object)
    private Map ctrls = new HashMap();
    
    /**
     * Return the array of control interfaces implemented by the component.
     */
    public Object[] getFcControllers() {
        Collection values = ctrls.values();
        return values.toArray();
    }

    /**
     * Declare a new controller implemented by the component.
     */
    public void addFcController( InterfaceType[] it, Object ctrl ) {
        ctrls.put(it,ctrl);
        im.registerFcControlInterface(it,ctrl);
    }

    /**
     * Register a new collection {@link Interface} instance.
     * The given name must start with an existing collection interface name
     * decalred in the component type.
     * 
     * @param interfaceName  the interface name (e.g. foo004)
     * @throws NoSuchInterfaceException
     *      if there is no such collection interface
     */
    public Interface registerFcInterface( String interfaceName )
    throws NoSuchInterfaceException {
        return im.registerFcInterface(interfaceName);
    }

    /**
     * Unregister a collection {@link Interface} instance.
     * The given name must start with an existing collection interface name
     * decalred in the component type.
     * 
     * @param interfaceName  the interface name (e.g. foo004)
     * @throws NoSuchInterfaceException
     *      if there is no such collection interface
     */
    public void unregisterFcInterface( String interfaceName )
    throws NoSuchInterfaceException {
        im.unregisterFcInterface(interfaceName);
    }

    
    // --------------------------------------------------------------
    // Interface interface
    // --------------------------------------------------------------
    
    public String getFcItfName() {
        return ComponentItf.NAME;
    }
    
    public Component getFcItfOwner() {
        return this;
    }
    
    public Type getFcItfType() {
        return ComponentItf.TYPE;
    }
    
    public boolean isFcInternalItf() {
        return false;
    }

    
    // --------------------------------------------------------------
    // Controller interface
    // --------------------------------------------------------------
    
    protected Component compctrl;
    
    /**
     * Set the reference towards the component controller associated to this
     * controller.
     * 
     * @param compctrl  the component controller associated with this controller
     * For instances of this class, compctrl and this are equals (the component
     * controller is associated with itself).
     */
    public void setFcCompCtrl( Component compctrl ) {
        this.compctrl = compctrl;
    }
    
    /**
     * Initialize the controller.
     */
    public void initFcCtrl() {        
        // Indeed nothing
    }
    
    /**
     * Clone the controller state from the current component to another one.
     * This method may receive some hints on how to do this, or provide some
     * hints on how this has been done. For instance, the hints may be a map
     * that is read and/or written by the controller. The raison d'etre of
     * these hints is that when its state is cloned, a controller may produce
     * results that are needed by other controllers.
     * 
     * @param dst    the destination component
     * @param hints  hints for performing the operation
     */
    public void cloneFcCtrl( Component dst, Object hints )
    throws CloneCtrlException {
        // Indeed nothing for the component controller
    }

}
