/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.control.content;

import org.objectweb.fractal.aokell.lib.type.InterfaceTypeImpl;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.type.InterfaceType;


/**
 * This interface extends the Fractal ContentController interface with methods
 * to manage internal client interfaces.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public interface ContentControllerItf extends ContentController {

    final public static String NAME = "content-controller";
    
    final public static InterfaceType TYPE =
        new InterfaceTypeImpl(
                NAME,
                ContentControllerItf.class.getName(),
                false, false, false );

    /**
     * Declare a new controller implemented by the component.
     */
    public void addFcController( InterfaceType[] it, Object ctrl );

    /**
     * Register a new collection {@link Interface} instance.
     * The given name must start with an existing collection interface name
     * declared in the component type.
     * 
     * @param interfaceName  the interface name (e.g. foo004)
     * @throws NoSuchInterfaceException
     *      if there is no such collection interface
     */
    public Interface registerFcInterface( String interfaceName )
    throws NoSuchInterfaceException;

    /**
     * Unregister a collection {@link Interface} instance.
     * The given name must start with an existing collection interface name
     * declared in the component type.
     * 
     * @param interfaceName  the interface name (e.g. foo004)
     * @throws NoSuchInterfaceException
     *      if there is no such collection interface
     */
    public void unregisterFcInterface( String interfaceName )
    throws NoSuchInterfaceException;

    /**
     * Initialize parameters required for managing internal interfaces
     * associated with this content controller.
     */
    public void initFc(
            Type type, Object controllerDesc, Object contentDesc,
            Component compctrlimpl, Object contentPart );
}
