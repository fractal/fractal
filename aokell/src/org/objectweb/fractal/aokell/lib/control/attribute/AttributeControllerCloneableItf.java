/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.control.attribute;

import org.objectweb.fractal.aokell.lib.control.CloneCtrlException;

/**
 * This interface is implemented by Fractal interface implementations.
 * 
 * The cloneFcCtrl method is called whenever a template component is
 * instantiated to clone the state of the attribute controller. The
 * implementation of this method depends on the attributes defined in the
 * attribute controller. 
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public interface AttributeControllerCloneableItf {

    /**
     * Clone the attribute controller state from the current component to
     * another one.
     * 
     * @param dst    the destination component
     */
    public void cloneFcCtrl( AttributeControllerCloneableItf dst )
    throws CloneCtrlException;
    
}
