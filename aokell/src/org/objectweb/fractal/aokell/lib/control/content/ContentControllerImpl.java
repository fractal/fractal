/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.control.content;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.aokell.lib.control.CloneCtrlException;
import org.objectweb.fractal.aokell.lib.control.Controller;
import org.objectweb.fractal.aokell.lib.interf.CompositeIntItfManager;
import org.objectweb.fractal.aokell.lib.interf.InterfaceManager;
import org.objectweb.fractal.aokell.lib.util.FractalHelper;
import org.objectweb.fractal.aokell.lib.util.MembraneHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.control.content.SuperControllerNotifier;
import org.objectweb.fractal.util.BindingControllerHelper;
import org.objectweb.fractal.util.Fractal;


/**
 * Implementation for the {@link ContentController}.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class ContentControllerImpl
    implements ContentControllerItf, Controller {
    
    /** The list of components under the control of this controller. */
    // List<Component>
    private List content = new ArrayList();
    
    /** The manager for internal interfaces associated with this controller. */
    private InterfaceManager im = new CompositeIntItfManager();

    
    public ContentControllerImpl() {}
    
    
    // --------------------------------------------------------------
    // ContentController interface
    // --------------------------------------------------------------
    
    /**
     * Return the array of internal Interface instances implemented by this
     * component.
     */
    public Object[] getFcInternalInterfaces() {
        return im.getFcInterfaces();
    }

    public Object getFcInternalInterface(String interfaceName)
    throws NoSuchInterfaceException {
        return im.getFcInterface(interfaceName);
    }

    public Component[] getFcSubComponents() {
        return (Component[]) content.toArray(new Component[content.size()]);
    }

    public void addFcSubComponent(Component arg0)
    throws IllegalContentException, IllegalLifeCycleException {
        
        Component compctrl = MembraneHelper.getFcCompCtrlImpl(this.compctrl);
        
        // Test on the containment hierarchy
        
        // 1st test: is the added one (arg0) already contained in the comp?
        // Not recursive because of a problem with shared when a component is
        // shared by the root and a component that is contained in the root
        // (somewhat tricky but seems to be permitted by Julia and Nicolas uses
        // it)
        if( isFcContainedIn(false,arg0,compctrl) ) {
            throw new IllegalContentException(
                    arg0+" is already a sub-component of "+compctrl);
        }
        
        // 2nd test: is the component already contained in the added one?
        // Recursive to detect non trivial cycles.
        if( arg0==compctrl || isFcContainedIn(true,compctrl,arg0) ) {
            throw new IllegalContentException(
                    "Adding "+arg0+" in "+compctrl+" would create a cycle");
        }
        
        // Add the sub-component
        content.add(arg0);
        
        // Update the super controller state of the sub-component
        SuperControllerNotifier sc = FractalHelper.getSuperController(arg0);
        sc.addedToFc(compctrl);
    }

    public void removeFcSubComponent(Component arg0)
    throws IllegalContentException, IllegalLifeCycleException {
        
        Component compctrl = MembraneHelper.getFcCompCtrlImpl(this.compctrl);
        
        // Test on the containment hierarchy
        if( ! content.contains(arg0) ) {
            throw new IllegalContentException(
                    arg0+" is not a sub-component of "+compctrl);
        }
        
        // Test on bindings
        Set srvItfs = BindingControllerHelper.getFcServerItfsBoundTo(arg0);
        Set cltItfs = BindingControllerHelper.getFcClientItfsBoundTo(arg0);
        if( srvItfs.size()!=0 || cltItfs.size()!=0 ) {
            throw new IllegalContentException(
                    "Removing "+arg0+" from "+compctrl+
                    " would create illegal bindings");
        }
        
        // Test whether the removed component is stopped
        testLifeCyleBeforeRemoving(arg0);
        
        // Remove the sub-component
        content.remove(arg0);
        
        // Update the super controller state of the sub-component
        SuperControllerNotifier sc = FractalHelper.getSuperController(arg0);
        sc.removedFromFc(compctrl);        
    }

    
    // --------------------------------------------------------------
    // ContentControllerItf interface
    // --------------------------------------------------------------
    
    /**
     * Declare a new controller implemented by the component.
     */
    public void addFcController( InterfaceType[] it, Object ctrl ) {
        im.registerFcControlInterface(it,ctrl);
    }
    
    /**
     * Initialize parameters required for managing internal interfaces
     * associated with this content controller. This method is invoked by the
     * membrane factory.
     */
    public void initFc(
            Type type, Object controllerDesc, Object contentDesc,
            Component compctrlimpl, Object contentPart ) {
        im.initFc(type,compctrlimpl,contentPart);
    }
    
    /**
     * Register a new collection {@link Interface} instance.
     * The given name must start with an existing collection interface name
     * decalred in the component type.
     * 
     * @param interfaceName  the interface name (e.g. foo004)
     * @throws NoSuchInterfaceException
     *      if there is no such collection interface
     */
    public Interface registerFcInterface( String interfaceName )
    throws NoSuchInterfaceException {
        return im.registerFcInterface(interfaceName);
    }

    /**
     * Unregister a collection {@link Interface} instance.
     * The given name must start with an existing collection interface name
     * decalred in the component type.
     * 
     * @param interfaceName  the interface name (e.g. foo004)
     * @throws NoSuchInterfaceException
     *      if there is no such collection interface
     */
    public void unregisterFcInterface( String interfaceName )
    throws NoSuchInterfaceException {
        im.unregisterFcInterface(interfaceName);
    }

    
    // --------------------------------------------------------------
    // Implementation specific
    // --------------------------------------------------------------
    
    protected void testLifeCyleBeforeRemoving( Component arg0 )
    throws IllegalLifeCycleException {
        
        LifeCycleController lc = FractalHelper.getLifeCycleController(arg0);
        if( lc.getFcState().equals(LifeCycleController.STARTED) )
            throw new IllegalLifeCycleException(
                    "Component "+arg0+" must be stopped before removed");
    }
    
    /**
     * Return true if the given src component is contained in the given dst
     * component.
     * 
     * @param recurse  true if the search must be recursive
     * @param src      the source component
     * @param dst      the destination component
     * @return         true if src is contained in dst
     */
    public static boolean isFcContainedIn(
            boolean recurse, Component src, Component dst ) {
        
        ContentController cc = null;
        try {
            cc = Fractal.getContentController(dst);
        }
        catch(NoSuchInterfaceException nsie) {
            // dst is not a composite
            // Should we propagate NoSuchInterfaceException instead?
            return false;
        }
        
        Component[] contained = cc.getFcSubComponents();
        for (int i = 0; i < contained.length; i++) {
            if( contained[i].equals(src) ) {
                return true;
            }
            // Recursive search if needed
            if( recurse ) {
                if( isFcContainedIn(recurse,src,contained[i]) ) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    
    // --------------------------------------------------------------
    // Controller implementation
    // --------------------------------------------------------------
    
    private Component compctrl;
    
    /**
     * Set the reference towards the component controller associated to this
     * controller.
     */
    public void setFcCompCtrl( Component compctrl ) {
        this.compctrl = compctrl;        
    }
    
    /**
     * Initialize the controller.
     */
    public void initFcCtrl() {
        // Indeed nothing
    }

    /**
     * Clone the controller state from the current component to another one.
     * This method may receive some hints on how to do this, or provide some
     * hints on how this has been done. For instance, the hints may be a map
     * that is read and/or written by the controller. The raison d'etre of
     * these hints is that when its state is cloned, a controller may produce
     * results that are needed by other controllers.
     * 
     * @param dst    the destination component
     * @param hints  hints for performing the operation
     */
    public void cloneFcCtrl( Component dst, Object hints )
    throws CloneCtrlException {
        
        // Get the content of the composite.
        // Recursively instantiate contained component and add them to dst.
        ContentController newCc = FractalHelper.getContentControllerItf(dst);
        
        // Map storing associations between components in contained in the
        // current composite and their associated newly created component.
        Map newContent = (Map) hints;
        
        Component[] subs = getFcSubComponents();
        
        for (int i = 0; i < subs.length; i++) {
            Component sub = (Component) newContent.get(subs[i]);
            if( sub == null ) {
                throw new CloneCtrlException(
                        "Contained component "+subs[i]+
                        " is associated to a null instance");
            }
            try {
                newCc.addFcSubComponent(sub);
            }
            catch( Exception e ) {
                throw new CloneCtrlException(e.getMessage());
            }
        }
    }
}
