/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.aokell.lib.control.lifecycle;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.objectweb.fractal.aokell.lib.control.CloneCtrlException;
import org.objectweb.fractal.aokell.lib.control.Controller;
import org.objectweb.fractal.aokell.lib.util.MembraneHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator;
import org.objectweb.fractal.util.BindingControllerHelper;
import org.objectweb.fractal.util.ContentControllerHelper;


/**
 * Implementation of the lifecycle controller for non composite components.
 * 
 * This class originates from the Julia source code. Although this class is not
 * present at this in the Julia source code, it has been generated from the
 * existing mixin chunks. 
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class CompositeLifeCycleControllerImpl
    implements LifeCycleCoordinator, Controller {
    
    public CompositeLifeCycleControllerImpl() {
        super();
    }
    
    // --------------------------------------------------------------
    // LifeCycleController interface
    // --------------------------------------------------------------
    
    /** 
     * Indicates if this component is started or not.
     */
    private boolean fcStarted;
    
    /** 
     * Checks the mandatory client interfaces of the component (and of all its sub
     * components) and then calls the overriden method.
     * 
     * @throws IllegalLifeCycleException if a problem occurs.
     */
    public void startFc() throws IllegalLifeCycleException {
        Component compctrl = MembraneHelper.getFcCompCtrlImpl(this.compctrl);
        Component thisComponent;
        try {
            thisComponent = ((Component)(compctrl.getFcInterface("component")));
        } catch (NoSuchInterfaceException e) {
            final String msg = "Cannot start the component";
            throw new IllegalLifeCycleException(msg);
        }
        List allSubComponents = ContentControllerHelper.getAllSubComponents(thisComponent);
        for (int i = 0 ; i < (allSubComponents.size()) ; ++i) {
            Component subComponent = ((Component)(allSubComponents.get(i)));
            try {
                checkFcMandatoryInterfaces(subComponent);
            } catch (IllegalBindingException e) {
                final String msg = "Cannot start the component";
                throw new IllegalLifeCycleException(msg);
            }
        }
        startFc$0();
    }
    
    public String getFcState() {
        return fcStarted ? LifeCycleController.STARTED : LifeCycleController.STOPPED;
    }
    
    public void stopFc() throws IllegalLifeCycleException {
        Component compctrl = MembraneHelper.getFcCompCtrlImpl(this.compctrl);
        LifeCycleCoordinator[] clccs = getFcLifeCycleControllers(compctrl);
        /*
         * In a mail exchange (2007/11/15), Judicael Ribault noticed that
         * setFcState(boolean) was unnecessarily called twice on components.
         * Commenting the following statement solves the issue.
         */
//        stopFc(clccs);
        setFcState(false);
    }
    
    
    // --------------------------------------------------------------
    // LifeCycleCoordinator interface
    // --------------------------------------------------------------
    
    /** 
     * The components that are currently active.
     */
    private List fcActive;
    
    public boolean fcActivated(final LifeCycleCoordinator component) {
        synchronized(fcActive) {
            if ((fcActive.size()) > 0) {
                if (!fcActive.contains(component)) {
                    fcActive.add(component);
                }
                return true;
            }
            return false;
        }
    }
    
    public boolean setFcStarted() {
        if (!fcStarted) {
            fcStarted = true;
            return true;
        }
        return false;
    }
    
    public void fcInactivated(final LifeCycleCoordinator component) {
        synchronized(fcActive) {
            fcActive.remove(component);
            fcActive.notifyAll();
        }
    }
    
    public void setFcStopping(final LifeCycleCoordinator coordinator) throws IllegalLifeCycleException {
        throw new Error("Internal error");
    }
    
    public boolean setFcStopped() {
        if (fcStarted) {
            fcStarted = false;
            return true;
        }
        return false;
    }
    
    
    // --------------------------------------------------------------
    // Controller interface
    // --------------------------------------------------------------
    
    private Component compctrl;
    
    /** 
     * Set the reference towards the component controller associated to this
     * controller.
     */
    public void setFcCompCtrl(Component compctrl) {
        this.compctrl = compctrl;
    }
    
    /** 
     * Initialize the controller.
     */
    public void initFcCtrl() {
    }

    /** 
     * Clone the controller state from the current component to another one.
     * This method may receive some hints on how to do this, or provide some
     * hints on how this has been done. For instance, the hints may be a map
     * that is read and/or written by the controller. The raison d'�tre of
     * these hints is that when its state is cloned, a controller may produce
     * results that are needed by other controllers.
     * 
     * @param dst    the destination component
     * @param hints  hints for performing the operation
     */
    public void cloneFcCtrl(Component dst, Object hints) throws CloneCtrlException {
        // Indeed empty
    }
    
    
    // --------------------------------------------------------------
    // Implementation specific
    // --------------------------------------------------------------
    
    /** 
     * Sets the lifecycle state of this component and of all its direct and
     * indirect sub components that have a {@link LifeCycleCoordinator} interface.
     *
     * @param started <tt>true</tt> to set the lifecycle state of the components
     *      to {@link #STARTED STARTED}, or <tt>false</tt> to set this state to
     *      {@link #STOPPED STOPPED}.
     * @throws IllegalLifeCycleException if a problem occurs.
     */
    private void setFcState(final boolean started) throws IllegalLifeCycleException {
        Component compctrl = MembraneHelper.getFcCompCtrlImpl(this.compctrl);
        Component thisComponent;
        try {
            thisComponent = ((Component)(compctrl.getFcInterface("component")));
        } catch (NoSuchInterfaceException e) {
            final String msg = "Cannot set the lifecycle state";
            throw new IllegalLifeCycleException(msg);
        }
        List allSubComponents = ContentControllerHelper.getAllSubComponents(thisComponent);
        for (int i = 0 ; i < (allSubComponents.size()) ; ++i) {
            Component c = ((Component)(allSubComponents.get(i)));
            LifeCycleCoordinator lc;
            try {
                lc = ((LifeCycleCoordinator)(c.getFcInterface("lifecycle-controller")));
            } catch (Exception e) {
                try {
                    lc = ((LifeCycleCoordinator)(c.getFcInterface("/lifecycle-coordinator")));
                } catch (NoSuchInterfaceException f) {
                    continue;
                }
            }
            if (started) {
                lc.setFcStarted();
            } else {
                lc.setFcStopped();
            }
        }
    }
    
    private void startFc$0() throws IllegalLifeCycleException {
        /*
         * In a mail exchange (2007/11/15), Judicael Ribault noticed that
         * setFcState(boolean) was unnecessarily called twice on components.
         * Commenting the following statements solves the issue.
         */
//        Component compctrl = MembraneHelper.getFcCompCtrlImpl(this.compctrl);
//        LifeCycleCoordinator[] clccs = getFcLifeCycleControllers(compctrl);
//        for (int i = 0 ; i < (clccs.length) ; ++i) {
//            clccs[i].setFcStarted();
//        }
        setFcState(true);
    }
    
    /** 
     * Checks that all the mandatory client interface of the given component are
     * bound.
     *
     * @param c a component.
     * @throws IllegalBindingException if a mandatory client interface of the
     *      given component is not bound.
     */
    private void checkFcMandatoryInterfaces(final Component c) throws IllegalBindingException {
        BindingController bc;
        try {
            bc = ((BindingController)(c.getFcInterface("binding-controller")));
        } catch (NoSuchInterfaceException e) {
            return ;
        }
        ComponentType compType = ((ComponentType)(c.getFcType()));
        String[] names = bc.listFc();
        for (int i = 0 ; i < (names.length) ; ++i) {
            InterfaceType itfType;
            try {
                itfType = compType.getFcInterfaceType(names[i]);
            } catch (NoSuchInterfaceException e) {
                continue;
            }
            if ((itfType.isFcClientItf()) && !itfType.isFcOptionalItf()) {
                Object sItf;
                try {
                    sItf = bc.lookupFc(names[i]);
                } catch (NoSuchInterfaceException e) {
                    continue;
                }
                if (sItf == null) {
                    final String msg = "Mandatory client interface unbound";
                    throw new IllegalBindingException(msg);
                }
            }
        }
    }
    
    /** 
     * Stops the given components simultaneously. This method sets the state of
     * the components to "<tt>STOPPING</tt>", waits until all the components
     * are simultaneoulsy inactive (their state is known thanks to the {@link
     * #fcActivated fcActivated} and {@link #fcInactivated fcInactivated} callback
     * methods), and then sets the state of the components to {@link #STOPPED
     * STOPPED}.
     *
     * @param components the {@link LifeCycleCoordinator} interface of the
     *      components to be stopped.
     * @throws IllegalLifeCycleException if a problem occurs.
     */
    private void stopFc(final LifeCycleCoordinator[] components) throws IllegalLifeCycleException {
        fcActive = new ArrayList();
        for (int i = 0 ; i < (components.length) ; ++i) {
            if (components[i].getFcState().equals(LifeCycleController.STARTED)) {
                fcActive.add(components[i]);
            }
        }
        Component compctrl = MembraneHelper.getFcCompCtrlImpl(this.compctrl);
        LifeCycleCoordinator c;
        try {
            c = ((LifeCycleCoordinator)(compctrl.getFcInterface("lifecycle-controller")));
        } catch (Exception e) {
            try {
                c = ((LifeCycleCoordinator)(compctrl.getFcInterface("/lifecycle-coordinator")));
            } catch (NoSuchInterfaceException f) {
                final String msg = "Cannot stop components";
                throw new IllegalLifeCycleException(msg);
            }
        }
        for (int i = 0 ; i < (components.length) ; ++i) {
            if (components[i].getFcState().equals(LifeCycleController.STARTED)) {
                components[i].setFcStopping(c);
            }
        }
        synchronized(fcActive) {
            while ((fcActive.size()) > 0) {
                try {
                    fcActive.wait();
                } catch (InterruptedException e) {
                }
            }
        }
        for (int i = 0 ; i < (components.length) ; ++i) {
            if (components[i].getFcState().equals(LifeCycleController.STARTED)) {
                components[i].setFcStopped();
            }
        }
        fcActive = null;
    }
    
    /** 
     * Returns the components that must be stopped in order to stop the given
     * component. These components are the direct or indirect primitive sub
     * components of this component that provide a {@link LifeCycleCoordinator}
     * interface, as well as the primitive client components of these components.
     *
     * @param id a composite component.
     * @return the components that must be stopped in order to stop the given
     *      component.
     * @throws IllegalLifeCycleException if a problem occurs.
     */
    private LifeCycleCoordinator[] getFcLifeCycleControllers(final Component id) throws IllegalLifeCycleException {
        List clccList = getFcInternalLifeCycleControllers();
        Object[] sItfs = id.getFcInterfaces();
        Set visited = new HashSet();
        for (int i = 0 ; i < (sItfs.length) ; ++i) {
            Interface sItf = ((Interface)(sItfs[i]));
            if (!((InterfaceType)(sItf.getFcItfType())).isFcClientItf()) {
                getSExtLifeCycleControllers(sItf ,clccList ,visited);
            }
        }
        LifeCycleCoordinator[] clccs;
        clccs = new LifeCycleCoordinator[clccList.size()];
        return ((LifeCycleCoordinator[])(clccList.toArray(clccs)));
    }
    
    /** 
     * Finds the primitive client components that are bound to the given server
     * interface.
     *
     * @param serverItf a server interface.
     * @param clccList where to put the {@link LifeCycleCoordinator}
     *      interfaces of the primitive client components that are found.
     * @param visited the already visited interfaces.
     * @throws IllegalLifeCycleException if a problem occurs.
     */
    private void getSExtLifeCycleControllers(final Interface serverItf, final List clccList, final Set visited) throws IllegalLifeCycleException {
        Object[] comps;
        try {
            comps = BindingControllerHelper.getFcPotentialClientsOf(serverItf).toArray();
        } catch (Exception e) {
            final String msg = "Cannot get the LifeCycleCoordinator interfaces";
            throw new IllegalLifeCycleException(msg);
        }
        for (int i = 0 ; i < (comps.length) ; ++i) {
            Component comp = ((Component)(comps[i]));
            Interface[] clientItfs;
            try {
                List l = BindingControllerHelper.getFcClientItfsBoundTo(comp ,serverItf);
                clientItfs = ((Interface[])(l.toArray(new Interface[l.size()])));
            } catch (Exception e) {
                final String msg = "Cannot get the LifeCycleCoordinator interfaces";
                throw new IllegalLifeCycleException(msg);
            }
            for (int j = 0 ; j < (clientItfs.length) ; ++j) {
                getCExtLifeCycleControllers(clientItfs[j] ,clccList ,visited);
            }
        }
    }
    
    /** 
     * Finds the primitive client components that are bound to the given client
     * interface.
     *
     * @param clientItf a client interface.
     * @param clccList where to put the {@link LifeCycleCoordinator} interfaces of
     *      the primitive client components that are found.
     * @param visited the already visited interfaces.
     * @throws IllegalLifeCycleException if a problem occurs.
     */
    private void getCExtLifeCycleControllers(final Interface clientItf, final List clccList, final Set visited) throws IllegalLifeCycleException {
        Component component = clientItf.getFcItfOwner();
        ContentController cc = null;
        try {
            cc = ((ContentController)(component.getFcInterface("content-controller")));
        } catch (NoSuchInterfaceException e) {
        }
        if (cc != null) {
            Interface itf;
            String name = clientItf.getFcItfName();
            try {
                if (!clientItf.isFcInternalItf()) {
                    itf = ((Interface)(cc.getFcInternalInterface(name)));
                } else {
                    itf = ((Interface)(component.getFcInterface(name)));
                }
            } catch (NoSuchInterfaceException e) {
                final String msg = "Cannot find the LifeCycleCoordinator interfaces";
                throw new IllegalLifeCycleException(msg);
            }
            if (!visited.contains(itf)) {
                visited.add(itf);
                getSExtLifeCycleControllers(itf ,clccList ,visited);
            }
        } else
        if (!visited.contains(clientItf)) {
                visited.add(clientItf);
                Component c = clientItf.getFcItfOwner();
                LifeCycleCoordinator lcc;
                try {
                    lcc = ((LifeCycleCoordinator)(c.getFcInterface("lifecycle-controller")));
                } catch (Exception e) {
                    try {
                        lcc = ((LifeCycleCoordinator)(c.getFcInterface("/lifecycle-coordinator")));
                    } catch (NoSuchInterfaceException f) {
                        final String msg = "Primitive client without a LifeCycleCoordinator";
                        throw new IllegalLifeCycleException(msg);
                    }
                }
                if (!clccList.contains(lcc)) {
                    clccList.add(lcc);
                }
            }
    }
    
    private List getFcInternalLifeCycleControllers() throws IllegalLifeCycleException {
        Component compctrl = MembraneHelper.getFcCompCtrlImpl(this.compctrl);
        List allSubComponents = ContentControllerHelper.getAllSubComponents(compctrl);
        List result = new ArrayList();
        for (int i = 0 ; i < (allSubComponents.size()) ; ++i) {
            Component c = ((Component)(allSubComponents.get(i)));
            try {
                c.getFcInterface("content-controller");
            } catch (NoSuchInterfaceException e) {
                try {
                    result.add(((LifeCycleCoordinator)(c.getFcInterface("lifecycle-controller"))));
                } catch (Exception f) {
                    try {
                        result.add(c.getFcInterface("/lifecycle-coordinator"));
                    } catch (NoSuchInterfaceException ignored) {
                    }
                }
            }
        }
        return result;
    }
    
}
