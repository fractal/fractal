/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.control.attribute;

import org.objectweb.fractal.aokell.lib.control.CloneCtrlException;
import org.objectweb.fractal.aokell.lib.control.Controller;
import org.objectweb.fractal.aokell.lib.util.FractalHelper;
import org.objectweb.fractal.aokell.lib.util.MembraneHelper;
import org.objectweb.fractal.api.Component;


/**
 * Implementation of the attribute controller.
 * 
 * The role played by this class is very special in sense that when the
 * attribute controller is looked up on a component
 * (e.g. with <code>aComp.getFcInterface("attribute-controller")</code>)
 * this is not this instance that is returned. Indeed the attribute controller
 * is defined in the component type with something like:
 * <code>
 *         ComponentType sType = tf.createFcType(new InterfaceType[] {
 *               tf.createFcItfType("s", "cs.impl.Service", false, false, false),
 *               tf.createFcItfType(
 *                       "attribute-controller",
 *                       "cs.impl.ServiceAttributes",
 *                       false,
 *                       false,
 *                       false)
 *       });
 * </code>
 * Hence the attribute controller is seen as a business server interface.
 * The goal of the current class is to provide an implementation for
 * cloneCtrl(). This method is called when a template is instantiated to
 * transfert the state of the attribute controller to the new instance.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class AttributeControllerImpl
    implements AttributeControllerItf, Controller {
    
    public AttributeControllerImpl() {}
    
    
    // --------------------------------------------------------------
    // AttributeController interface
    // --------------------------------------------------------------
    
    // Empty
    
    // --------------------------------------------------------------
    // Controller implementation
    // --------------------------------------------------------------
    
    private Component compctrl;
    
    /**
     * Set the reference towards the component controller associated to this
     * controller.
     */
    public void setFcCompCtrl( Component compctrl ) {
        this.compctrl = compctrl;
    }
    
    /**
     * Initialize the controller.
     */
    public void initFcCtrl() {
        // Indeed nothing
    }
    
    /**
     * Clone the controller state from the current component to another one.
     * This method may receive some hints on how to do this, or provide some
     * hints on how this has been done. For instance, the hints may be a map
     * that is read and/or written by the controller. The raison d'etre of
     * these hints is that when its state is cloned, a controller may produce
     * results that are needed by other controllers.
     * 
     * @param dst    the destination component
     * @param hints  hints for performing the operation
     */
    public void cloneFcCtrl( Component dst, Object hints )
    throws CloneCtrlException {
        
        Component compctrl = MembraneHelper.getFcCompCtrl(this.compctrl);
        AttributeControllerCloneableItf src =
            (AttributeControllerCloneableItf)
                FractalHelper.getAttributeController(compctrl);
        
        AttributeControllerCloneableItf target =
            (AttributeControllerCloneableItf)
                FractalHelper.getAttributeController(dst);
        
        src.cloneFcCtrl(target);
    }

}
