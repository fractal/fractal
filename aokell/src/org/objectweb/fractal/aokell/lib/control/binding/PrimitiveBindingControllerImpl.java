/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.control.binding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.objectweb.fractal.aokell.lib.util.FractalHelper;
import org.objectweb.fractal.aokell.lib.util.MembraneHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.SuperController;


/**
 * Implementation of the binding controller for primitive components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class PrimitiveBindingControllerImpl
    extends AbstractBindingControllerImpl
    implements BindingController {  // Needed for fractalrmi

    public PrimitiveBindingControllerImpl() {
        super();
    }
    
    
    // --------------------------------------------------------------
    // Implementation specific
    // --------------------------------------------------------------
    
    /**
     * Test whether the given source interface can be bound to the given target
     * interface.
     * 
     * @throws IllegalBindingException  if the binding is illegal
     */
    protected void testLegalBinding( Interface srcItf, Object arg1 )
    throws IllegalBindingException, NoSuchInterfaceException {
        
        super.testLegalBinding(srcItf,arg1);
        
        Interface targetItf = (Interface) arg1;
        Component target = targetItf.getFcItfOwner();
        
        // Test whether component are in a common super component
        SuperController sc = MembraneHelper.getFcSuperCtrl(compctrl);
        Component[] supers = sc.getFcSuperComponents();
        List l = new ArrayList();
        l.add( MembraneHelper.getFcCompCtrlImpl(compctrl) );
        l.addAll( Arrays.asList(supers) );
        
        SuperController tsc = FractalHelper.getSuperController(target);
        Component[] tsupers = tsc.getFcSuperComponents();
        List t = new ArrayList();
        t.add(target);
        t.addAll( Arrays.asList(tsupers) );
        
        for (Iterator iter = l.iterator(); iter.hasNext();) {
            if( t.contains(iter.next()) ) {
                return;
            }
        }        
        
        throw new IllegalBindingException(
                "No common super component for binding: "+
                toString(srcItf,targetItf) );
    }
    
}
