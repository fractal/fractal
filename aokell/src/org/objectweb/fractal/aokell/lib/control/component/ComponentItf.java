/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.control.component;

import org.objectweb.fractal.aokell.lib.type.InterfaceTypeImpl;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.type.InterfaceType;


/**
 * Interface for component implementations.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public interface ComponentItf extends Component {

    final public static String NAME = "component";
    
    final public static InterfaceType TYPE =
        new InterfaceTypeImpl(
                NAME,
                ComponentItf.class.getName(),
                false, false, false);

    /**
     * Return the list of controllers implemented by the component.
     */
    public Object[] getFcControllers();
    
    /**
     * Declare a new controller implemented by the component.
     * 
     * @param it   the control interface types implemented by this controller
     * @param ctrl  the controller.
     *      The controller (i.e. the instance of
     *      {@link org.objectweb.fractal.aokell.lib.control.Controller})
     *      with object-oriented membranes.
     *      The proxy interface with componentized membranes. 
     */
    public void addFcController( InterfaceType[] it, Object ctrl );

    /**
     * Register a new collection {@link Interface} instance.
     * The given name must start with an existing collection interface name
     * declared in the component type.
     * 
     * @param interfaceName  the interface name (e.g. foo004)
     * @throws NoSuchInterfaceException
     *      if there is no such collection interface
     */
    public Interface registerFcInterface( String interfaceName )
    throws NoSuchInterfaceException;

    /**
     * Unregister a collection {@link Interface} instance.
     * The given name must start with an existing collection interface name
     * declared in the component type.
     * 
     * @param interfaceName  the interface name (e.g. foo004)
     * @throws NoSuchInterfaceException
     *      if there is no such collection interface
     */
    public void unregisterFcInterface( String interfaceName )
    throws NoSuchInterfaceException;

    public void initFc(
            Type type, Object controllerDesc, Object contentDesc,
            Object contentPart );
}
