/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.interf;

import org.objectweb.fractal.aokell.lib.util.PlatformHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.type.InterfaceType;

/**
 * A manager for external interfaces of composite components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class CompositeExtItfManager extends PrimitiveExtItfManager {
    
    // --------------------------------------------------------------
    // Implementation specific
    // --------------------------------------------------------------
    
    /**
     * Get the {@link Interface} instance associated to the given interface type
     * for the given component.
     */
    protected Interface getFcInterface(
            InterfaceType it, Component itfOwner, Object content,
            boolean isFcInternalInterface ) {
        
        if( content != null ) {
            /*
             * For composites which define a content, determine whether the
             * content implements the interface.
             * Very unlikely case, but seen once in Julia JUnit tests.
             */
            Class jit = null;
            try {
                jit = PlatformHelper.loadClass(it.getFcItfSignature());
            }
            catch( ClassNotFoundException cnfe ) {
                /*
                 * Shouldn't occur. Arrived at this point, the existence of the
                 * Java signature associated to the interface should have
                 * already been checked.
                 */
                throw new RuntimeException(cnfe.getMessage());
            }
            if( jit.isAssignableFrom(content.getClass()) ) {
                return
                    getProxyInterface(
                            it,itfOwner,content,
                            isFcInternalInterface,
                            false   // implemented
                    );
            }
        }
                
        return
            getProxyInterface(
                    it,itfOwner,content,
                    false,  // external
                    true    // boundable
            );
    }
    
}
