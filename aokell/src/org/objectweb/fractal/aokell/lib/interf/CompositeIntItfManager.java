/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.interf;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.aokell.lib.type.InterfaceTypeImpl;
import org.objectweb.fractal.aokell.lib.util.FractalHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.type.InterfaceType;

/**
 * A manager for internal interfaces of composite components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class CompositeIntItfManager extends CompositeExtItfManager {
    
    public Object[] getFcInterfaces() {
        
        // Remove internal control interfaces
        List l = new ArrayList();
        Object[] itfs = super.getFcInterfaces();
        for (int i = 0; i < itfs.length; i++) {
            Interface itf = (Interface) itfs[i];
            if( ! FractalHelper.isFcControllerInterfaceName(itf.getFcItfName()) ) {
                l.add(itf);
            }
        }
        
        return l.toArray();
    }
    
    // --------------------------------------------------------------
    // Implementation specific
    // --------------------------------------------------------------
    
    /**
     * Register new control interfaces for a given controller.
     * 
     * Most of the time a controller implements only one control interface.
     * One exception is the factory controller which implements the interface
     * type corresponding to {@link org.objectweb.fractal.api.factory.Factory}
     * and the interface type corresponding to
     * {@link org.objectweb.fractal.julia.factory.Template}.
     * 
     * @param its   the interface types
     * @param ctrl  the controller
     * @return      the interface instances
     */
    public Interface[] registerFcControlInterface(
            InterfaceType[] its, Object ctrl ) {
        
        InterfaceType[] newits = new InterfaceType[its.length];
        
        for (int i = 0; i < its.length; i++) {
            newits[i] = newSymetricInterfaceType(its[i]);
        }
        
        return super.registerFcControlInterface(newits,ctrl);
    }

    /**
     * Get the {@link Interface} instance associated to the given interface type
     * for the given component.
     */
    protected Interface getFcInterface(
            InterfaceType it, Component itfOwner, Object content,
            boolean isFcInternalInterface ) {
        
        InterfaceType newit = newSymetricInterfaceType(it);
        return
            getProxyInterface(
                newit,itfOwner,content,
                true,   // internal
                true    // boundable
            );
    }
    
    /**
     * Return a copy of the given interface type where the client role is
     * transformed into server and the server role is transformed into client.
     */
    private static InterfaceType newSymetricInterfaceType( InterfaceType it ) {
        return
            new InterfaceTypeImpl(
                    it.getFcItfName(),
                    it.getFcItfSignature(),
                    ! it.isFcClientItf(),   // client <-> server
                    it.isFcOptionalItf(),
                    it.isFcCollectionItf()
            );        
    }
    
}
