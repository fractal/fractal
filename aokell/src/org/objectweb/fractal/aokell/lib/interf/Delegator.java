/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.interf;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.objectweb.fractal.aokell.AOKell;
import org.objectweb.fractal.aokell.lib.InterfaceImpl;
import org.objectweb.fractal.aokell.lib.util.PlatformHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.type.InterfaceType;


/**
 * This class manages instances implementing Fractal interfaces.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class Delegator {
    
    /**
     * The factory for proxy generators. 
     */
    private static GeneratorFactoryItf genFact;
    
    /**
     * The map of already generated proxy classes.
     * Map<String,ClassDefinition>. key=target class name, value=class.
     */
    private static Map proxies = new HashMap();

    /**
     * Return an instance implementing a given Fractal interface type.
     * 
     * @param it        the interface type
     * @param itfOwner  the component associated to the interface
     * @param content   the content to which the call must be delegated
     * @param isFcInternalInterface  true is the interface is internal
     * @param boundable
     *     true if the interface can be bound (e.g. client for a primitive)
     *     false otherwise (e.g. server for a primitive)
     * @return  the proxy
     */
    public static Object generate(
            InterfaceType it, Component itfOwner, 
            Object content, boolean isFcInternalInterface,
            boolean boundable ) {
        
        // Compute the name of the class containing the Fractal interface
        // implementation
        final String javaItfSignature = it.getFcItfSignature();
        final int lastdot = javaItfSignature.lastIndexOf('.');
        String name, pkgName;
        if( lastdot == -1 ) {
            name = javaItfSignature;
            pkgName = "";
        }
        else {
            name = javaItfSignature.substring(lastdot+1);
            pkgName = javaItfSignature.substring(0,lastdot);
        }
        final String suffix =
            ( boundable ? "Boundable" : "Implemented" ) + "Interface";
        final String targetClassname =
            FcItfImplHelper.getGenPkgName(pkgName) + "." + name + suffix;
        
        // Get and instanciate the Fractal interface implementation
        try {
            Class gen =
                getFcItfImplClass(
                        targetClassname, javaItfSignature, boundable );
            Object o = gen.newInstance();
            InterfaceImpl i = (InterfaceImpl) o;
            i.init(itfOwner,it.getFcItfName(),it,isFcInternalInterface);
            i.setFcItfImpl(content);
            return o;
        }
        catch( Throwable t ) {
            dumpProxies();
            final String msg = t.getClass().getName()+" "+t.getMessage();
            throw new RuntimeException(msg);
        }
    }

    /**
     * Dump the generated classes.
     */
    private static void dumpProxies() {
        
        String dirname = System.getProperty("java.io.tmpdir");
        
        for (Iterator iter = proxies.entrySet().iterator(); iter.hasNext();) {
            Map.Entry entry = (Map.Entry) iter.next();
            String targetClassname = (String) entry.getKey();
            ClassDefinition cd = (ClassDefinition) entry.getValue();
            
            try {
                PlatformHelper.dumpClassToFile(dirname,targetClassname,cd.bytecode);
            }
            catch( IOException ioe ) {
                throw new RuntimeException("IOException: "+ ioe.getMessage());
            }
        }
    }

    /**
     * Return the class implementing a given Fractal interface.
     * 
     * @param targetClassname
     *     the name for the class implementing the Fractal interface
     * @param javaItfSignature
     *     the Java signature associated to the Fractal interface
     * @param boundable
     *     true if the interface can be bound (e.g. client for a primitive)
     *     false otherwise (e.g. server for a primitive)
     */
    private static Class getFcItfImplClass(
            String targetClassname, String javaItfSignature, boolean boundable )
    throws ClassNotFoundException {
        
        // Check whether the class has already been requested
        if( proxies.containsKey(targetClassname) ) {
            ClassDefinition cd = (ClassDefinition) proxies.get(targetClassname);
            return cd.cl;
        }
        
        // Try to load the class from the classpath
        try {
            return PlatformHelper.loadClass(targetClassname);
        }
        catch( ClassNotFoundException cnfe ) {}
        
        // The class can not be loaded. Check whether ASM can be loaded.
        try {
            PlatformHelper.loadClass("org.objectweb.asm.Opcodes");
        }
        catch( ClassNotFoundException cnfe ) {
            throw new ClassNotFoundException(targetClassname);
        }
        
        // Dynamically instantiate ASMGeneratorFactory to avoid introducing a
        // static dependency towards ASM.
        if( genFact == null ) {
            try {
                Class cl =
                    PlatformHelper.loadClass(
                        "org.objectweb.fractal.aokell.lib.asm.ASMGeneratorFactory");
                genFact = (GeneratorFactoryItf) cl.newInstance();
            }
            catch( Exception e ) {
                final String msg =
                    "Unexpected "+e.getClass().getName()+
                    " when loading org.objectweb.fractal.aokell.lib.asm.ASMGeneratorFactory";
                throw new RuntimeException(msg);
            }
        }
    
        // Generate the class.
        GeneratorItf gen =
            (boundable) ?
                genFact.getBoundableInterfaceGenerator() :
                genFact.getImplementedInterfaceGenerator();
                
        ClassDefinition cd = gen.generate(targetClassname,javaItfSignature);
        
        // Defensive programming. Check a frequently encountered problem.
        Class itf = PlatformHelper.loadClass(javaItfSignature);
        if( ! itf.isAssignableFrom(cd.cl) ) {
            throw new RuntimeException(
                    "Content class "+cd.cl.getName()+
                    " should implement "+itf.getName()+
                    ". Likely cause: trouble with the component controller."
                    );
        }
        
        registerProxy(targetClassname,cd);
        return cd.cl;
    }

    /**
     * Store a generated class into the memory cache.
     */
    private static void registerProxy( String targetClassname, ClassDefinition cd ) {
        
        proxies.put(targetClassname,cd);
        
        String cachedir = System.getProperty(AOKell.DUMP_GENERATED_PROP_NAME);
        if( cachedir != null ) {
            // If applies, dump the generated class to the cache
            try {
                PlatformHelper.dumpClassToFile(cachedir,targetClassname,cd.bytecode);
            }
            catch(IOException ioe) {
                throw new RuntimeException("IOException: "+ ioe.getMessage());
            }
        }
    }
    
}
