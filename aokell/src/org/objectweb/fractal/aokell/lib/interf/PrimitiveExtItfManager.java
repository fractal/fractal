/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.interf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.aokell.lib.InterfaceImpl;
import org.objectweb.fractal.aokell.lib.util.FractalHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;

/**
 * A manager for external interfaces of primitive components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class PrimitiveExtItfManager implements InterfaceManager {
    
    /** The interfaces managed by this component. */
    // Map<String,Object> <interface name,interface>
    private Map itfs;
    
    protected Component compctrlimpl;
    protected ComponentType type;
    protected Object content;

    
    public void initFc( Type type, Component compctrlimpl, Object content ) {
        
        this.type = (ComponentType) type;
        this.compctrlimpl = compctrlimpl;
        this.content = content;
        
        /*
         * Discard old {@link Interface} instances if there are some.
         */
        itfs = new HashMap();
        
        /*
         * Create {@link Interface} instances corresponding to the component
         * type.
         */
        ComponentType ct = (ComponentType) type;
        InterfaceType[] its = ct.getFcInterfaceTypes();
        for (int i = 0; i < its.length; i++) {
            /*
             * Collection interfaces defined in the type do not automatically
             * exist as {@link Interface} instances.
             * An instance for a collection is created:
             * - when an instance is requested with getFcInterface() or,
             * - when the interface is bound with bindFc().
             */
            if( ! its[i].isFcCollectionItf() ) {
                String name = its[i].getFcItfName();
                if( FractalHelper.isFcImplementableInterface(name) ) {
                    Interface itf =
                        getFcInterface(its[i],compctrlimpl,content,false);
                    itfs.put(name,itf);
                }
            }
        }
    }
    
    /**
     * Return the instance implementing the {@link Interface} corresponding to
     * the given name. The returned value is of type Object to be compatible
     * with the {@link org.objectweb.fractal.api.Component} interface.
     */
    public Object getFcInterface(String interfaceName)
    throws NoSuchInterfaceException {
        
        if( itfs.containsKey(interfaceName) ) {
            return itfs.get(interfaceName);
        }
        
        /*
         * The interface name has not been found.
         * Try to register it as a collection interface.
         */
        return registerFcInterface(interfaceName);
    }
    
    /**
     * Return the array of {@link Interface} instances implemented by this
     * component. The returned values are of type Object to be compatible with
     * the {@link org.objectweb.fractal.api.Component} interface.
     */
    public Object[] getFcInterfaces() {
        
        // Remove hidden interfaces
        List l = new ArrayList();
        for (Iterator iter = itfs.entrySet().iterator(); iter.hasNext();) {
            Map.Entry entry = (Map.Entry) iter.next();
            String itfName = (String) entry.getKey();
            if( itfName.charAt(0) != '/' ) {
                l.add(entry.getValue());
            }
        }
        
        return l.toArray();
    }

    /**
     * Register a new collection {@link Interface} instance.
     * The given name must start with an existing collection interface name
     * decalred in the component type.
     * 
     * @param interfaceName  the interface name (e.g. foo004)
     * @throws NoSuchInterfaceException
     *      if there is no such collection interface
     */
    public Interface registerFcInterface( String interfaceName )
    throws NoSuchInterfaceException {
        
        /*
         * Check whether the Interface is already registered or not.
         */
        if( itfs.containsKey(interfaceName) ) {
            Interface itf = (Interface) itfs.get(interfaceName);
            return itf;
        }
        
        /*
         * Check that the given name corresponds to the name of an existing
         * interface.
         */
        InterfaceType[] its = type.getFcInterfaceTypes();
        InterfaceType it = null;
        for (int i = 0; i < its.length; i++) {
            if( interfaceName.equals(its[i].getFcItfName()) ||
                ( interfaceName.startsWith(its[i].getFcItfName()) &&
                  its[i].isFcCollectionItf()) ) {
                it = its[i];
            }
        }
        
        // No such interface type has been found
        if( it == null ) {
            throw new NoSuchInterfaceException(interfaceName);
        }
        
        Interface itf = getFcInterface(it,compctrlimpl,content,false);
        ((InterfaceImpl)itf).setFcItfName(interfaceName);
        itfs.put(interfaceName,itf);
        
        return itf;
    }

    /**
     * Unregister a collection {@link Interface} instance.
     * The given name must start with an existing collection interface name
     * decalred in the component type.
     * 
     * @param interfaceName  the interface name (e.g. foo004)
     * @throws NoSuchInterfaceException
     *      if there is no such collection interface
     */
    public void unregisterFcInterface( String interfaceName )
    throws NoSuchInterfaceException {
        
        if( ! itfs.containsKey(interfaceName) ) {
            throw new NoSuchInterfaceException(interfaceName);
        }
        
        /*
         * If the interface is of type singleton, keep it in the list.
         */
        Interface itf = (Interface) itfs.get(interfaceName);
        InterfaceType it = (InterfaceType) itf.getFcItfType();
        if( it.isFcCollectionItf() ) {
            itfs.remove(interfaceName);
        }
    }

    /**
     * Register new control interfaces for a given controller.
     * 
     * Most of the time a controller implements only one control interface.
     * One exception is the factory controller which implements the interface
     * type corresponding to {@link org.objectweb.fractal.api.factory.Factory}
     * and the interface type corresponding to
     * {@link org.objectweb.fractal.julia.factory.Template}.
     * 
     * @param its   the interface types
     * @param ctrl  the controller
     * @return      the interface instances
     */
    public Interface[] registerFcControlInterface(
            InterfaceType[] its, Object ctrl ) {
        
        Interface[] result = new Interface[its.length];
        
        for (int i = 0; i < its.length; i++) {
            // Do not use getFcInterface which is overidden by sub-classes
            result[i] =
                getProxyInterface(
                        its[i],compctrlimpl,ctrl,
                        false,  // external
                        false   // implemented
                    );
            itfs.put(its[i].getFcItfName(),result[i]);
        }
        
        return result;
    }

    
    // --------------------------------------------------------------
    // Implementation specific
    // --------------------------------------------------------------
    
    /**
     * Get the {@link Interface} instance associated to the given interface type
     * for the given component.
     */
    protected Interface getFcInterface(
            InterfaceType it, Component itfOwner, Object content,
            boolean isFcInternalInterface ) {
        return
            getProxyInterface(
                it,itfOwner,content,
                isFcInternalInterface,
                it.isFcClientItf()      // boundable when client
            );                          // implemented when server
    }
    
    /**
     * Return an {@link Interface} instance for the given interface type.
     * The returned instance implements both the Interface interface and the
     * Java interface specified in the given interface type. The instance is a
     * proxy generated with ASM when the fcinterface feature is set to asm.
     * 
     * @param it         the interface type
     * @param itfOwner   the component owning this interface
     * @param content    the instance implementing the content
     * @param boundable
     *     true if the interface can be bound (e.g. client for a primitive)
     *     false otherwise (e.g. server for a primitive)
     * @return           the interface instance
     */
    final protected Interface getProxyInterface(
            InterfaceType it, Component itfOwner, Object content,
            boolean isFcInternalInterface, boolean boundable ) {
        
        Object proxy =
            Delegator.generate(
                it,                     // interface type              
                itfOwner,               // component owning the interface
                content,                // the instance holding the content
                isFcInternalInterface,  // external interface
                boundable               // boundable or implemented
            );          
        
        return (Interface) proxy;
    }
}
