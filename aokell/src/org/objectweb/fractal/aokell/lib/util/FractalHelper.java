/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.util;


import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.aokell.lib.control.content.ContentControllerItf;
import org.objectweb.fractal.aokell.lib.control.factory.FactoryDef;
import org.objectweb.fractal.aokell.lib.control.superc.SuperControllerDef;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.control.SuperController;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.julia.control.content.SuperControllerNotifier;
import org.objectweb.fractal.util.Fractal;


/**
 * Helper class that wraps calls to o.o.f.a.Fractal and that throws runtime
 * exception instead of checked exceptions.
 * Various other utility methods.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class FractalHelper {

    public static Object getFcInterface( Component c, String itfName ) {
        try {
            return c.getFcInterface(itfName);
        }
        catch( NoSuchInterfaceException nsie ) {
            throw new RuntimeException("No such interface: "+nsie.getMessage());
        }        
    }
    
    /**
     * Returns the {@link AttributeController} interface of the given component.
     *
     * @param c  a component.
     * @return the {@link AttributeController} interface of the given component.
     */
    public static AttributeController getAttributeController(Component c) {
        return (AttributeController) getFcInterface(c,"attribute-controller");
    }
    
    /**
     * Returns the {@link BindingController} interface of the given component.
     * 
     * @param c  a component.
     * @return the {@link BindingController} interface of the given component.
     */
    public static BindingController getBindingController(Component c) {
        return (BindingController) getFcInterface(c,"binding-controller");
    }

    /**
     * Returns the {@link ContentController} interface of the given component.
     * 
     * @param c a component.
     * @return the {@link ContentController} interface of the given component.
     */
    public static ContentController getContentController(Component c) {
        return (ContentController) getFcInterface(c,"content-controller");
    }

    /**
     * Returns the {@link ContentControllerItf} interface of the given component.
     * 
     * @param c a component.
     * @return the {@link ContentControllerItf} interface of the given component.
     */
    public static ContentControllerItf getContentControllerItf(Component c) {
        return (ContentControllerItf) getFcInterface(c,"content-controller");
    }

    /**
     * Returns the {@link SuperControllerNotifier} interface of the given
     * component.
     * 
     * @param c  a component.
     * @return the {@link SuperControllerNotifier} interface of the given component.
     */
    public static SuperControllerNotifier getSuperController(Component c) {
        return (SuperControllerNotifier) getFcInterface(c,"super-controller");
    }

    /**
     * Returns the {@link NameController} interface of the given component.
     * 
     * @param c a component.
     * @return the {@link NameController} interface of the given component.
     */
    public static NameController getNameController(Component c) {
        return (NameController) getFcInterface(c,"name-controller");
    }

    /**
     * Returns the {@link LifeCycleController} interface of the given component.
     * 
     * @param c a component.
     * @return the {@link LifeCycleController} interface of the given component.
     * @throws NoSuchInterfaceException
     *             if there is no such interface.
     */
    public static LifeCycleController getLifeCycleController(Component c) {
        return (LifeCycleController) getFcInterface(c,"lifecycle-controller");
    }

    /**
     * Returns the {@link FactoryDef} interface of the given component.
     * 
     * @param c a component.
     * @return the {@link FactoryDef} interface of the given component.
     */
    public static FactoryDef getFactory(Component c) {
        return (FactoryDef) getFcInterface(c,"factory");
    }

    /**
     * Returns the {@link GenericFactory} interface of the given component.
     * 
     * @param c a component.
     * @return the {@link GenericFactory} interface of the given component.
     */
    public static GenericFactory getGenericFactory(Component c) {
        return (GenericFactory) getFcInterface(c,"generic-factory");
    }

    /**
     * Returns the {@link TypeFactory} interface of the given component.
     * 
     * @param c a component.
     * @return the {@link TypeFactory} interface of the given component.
     */
    public static TypeFactory getTypeFactory(Component c) {
        return (TypeFactory) getFcInterface(c,"type-factory");
    }
    
    /**
     * Bind the given destination interface to the given client interface name.
     * 
     * @param bc       the binding controller
     * @param itfName  the client interface name
     * @param dstItf   the destination interface
     */
    public static void bindFc(
            BindingController bc, String itfName, Object dstItf ) {
        try {
            bc.bindFc(itfName,dstItf);
        }
        catch( NoSuchInterfaceException nsie ) {
            throw new RuntimeException("No such interface: "+nsie.getMessage());
        }
        catch( IllegalBindingException ibe ) {
            throw new RuntimeException(
                    "IllegalBindingException: "+ibe.getMessage());
        }
        catch( IllegalLifeCycleException ilce) {
            throw new RuntimeException(
                    "IllegalLifeCycleException: "+ilce.getMessage());
        }
    }
    
    public static Object lookupFc( BindingController bc, String itfName ) {
        try {
            return bc.lookupFc(itfName);
        }
        catch( NoSuchInterfaceException nsie ) {
            throw new RuntimeException("No such interface: "+nsie.getMessage());
        }
    }

    /**
     * Return all sub-components of the given component.
     */
    public static Component[] getAllSubComponents( Component c ) {
        List l = new ArrayList();
        addAllSubComponents(c,l);
        return (Component[]) l.toArray( new Component[l.size()] );
    }

    /**
     * Add all sub-components of the given component to the list.
     */
    public static void addAllSubComponents( Component c, List l ) {
        
        if( l == null ) {
            throw new IllegalArgumentException(
                    "The given list (2nd parameter) must be non null");
        }
        
        l.add(c);
        
        try {
            ContentController cc =
                (ContentController) c.getFcInterface("content-controller");
            Component[] subs = cc.getFcSubComponents();
            for (int i = 0; i < subs.length; i++) {
                addAllSubComponents(subs[i],l);
            }
        }
        catch( NoSuchInterfaceException nsie ) {
            // A non composite component has been reached
        }
    }

    /**
     * Follow bindings and return the interface associated to the component
     * located at the end of the binding chain.
     */
    public static Interface followBindingsUpToLastInterface( Interface start ) {
        
        Object next = null;
        Component comp = null;
        
        do {
            comp = start.getFcItfOwner();            
            try {
                /*
                 * Fractal.getBindingController() is more general than than
                 * getBindingController() defined in this class. It works also
                 * for componentized membrane which "simply" implements
                 * BindingController (and not BindingControllerItf).
                 */
                BindingController bc = Fractal.getBindingController(comp);
                next = bc.lookupFc(start.getFcItfName());
                if( next != null ) {
                    start = (Interface) next;
                }
            }
            catch( NoSuchInterfaceException nsie ) {
                next = null;
            }
        }
        while( next != null );
        
        return start;
    }

    public static Object getContent(Component c) {
        return getFcInterface(c,"/content");
    }

    /**
     * Return true if the specified name corresponds to the name of a Fractal
     * controller interface.
     * Note: factory is not considered as a controller interface but as a
     * business one.
     */
    public static boolean isFcControllerInterfaceName(String itfName) {
        if( itfName.equals("component") ||
            itfName.endsWith("-controller") ) {
            return true;
        }
        return false;
    }

    /**
     * Return true if the specified name corresponds to the name of a Fractal
     * control method.
     */
    public static boolean isFcControllerMethodName(String methName) {
        if( methName.indexOf("Fc") != -1 ||
            methName.startsWith("set") ||
            methName.startsWith("get") ) {
            return true;
        }
        return false;
    }
    
    /**
     * Return true if the specified name corresponds to the name of an interface
     * which should be implemented by a content class.
     */
    public static boolean isFcImplementableInterface( String itfName ) {
        if( itfName.equals("attribute-controller") ) {
            return true;
        }
        if( itfName.equals("factory") ) {
            return false;
        }
        return ! isFcControllerInterfaceName(itfName);
    }

    /**
     * Return the fully-qualified name of a component, i.e. a slash-separated
     * string of names reflecting the hierarchy of nested components.
     */
    public static String getFcFullyQualifiedName( Component comp ) {
        
        SuperController sc = null;
        try {
            sc = (SuperController) comp.getFcInterface(SuperControllerDef.NAME);
        }
        catch( NoSuchInterfaceException nsie ) {
            return getNameController(comp).getFcName();            
        }
        
        Component[] supers = sc.getFcSuperComponents();
        if( supers.length == 0 ) {
            return "/"+getNameController(comp).getFcName();
        }
        
        return
            getFcFullyQualifiedName(supers[0])+"/"+
            getNameController(comp).getFcName();
    }

    /**
     * Traverse the super-hierarchy to return the root component.
     */
    public static Component getTopMostComponent( Component c ) {
        
        SuperController sc = getSuperController(c);
        Component[] sups = sc.getFcSuperComponents();
        if( sups.length == 0 ) {
            // No more supers. We have reached the top.
            return c;
        }
        
        return getTopMostComponent(sups[0]);
    }

}
