/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.type;

import java.io.Serializable;

import org.objectweb.fractal.aokell.lib.util.PlatformHelper;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.type.InterfaceType;


/**
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class InterfaceTypeImpl implements InterfaceType, Serializable {

    private static final long serialVersionUID = -8689269253196796636L;
    
    private String name;
    private String signature;
    private boolean isClient;
    private boolean isOptional;
    private boolean isCollection;
    
    public InterfaceTypeImpl(String name, String signature, boolean isClient,
            boolean isOptional, boolean isCollection) {
        
        this.name = name;
        this.signature = signature;
        this.isClient = isClient;
        this.isOptional = isOptional;
        this.isCollection = isCollection;
    }
    
    // --------------------------------------------------------------
    // Type implementation
    // --------------------------------------------------------------
    
    /**
     * Tell whether the current type is a sub type of the given type.
     * Definition extracted from the Fractal specification 2.0.3 (2004/2/5).
     */
    public boolean isFcSubTypeOf(Type type) {
        
        if( !(type instanceof InterfaceType) )
            return false;
        
        InterfaceTypeImpl it = (InterfaceTypeImpl) type;
        
        // Interface types must have the same name and same role
        if( !it.getFcItfName().equals(getFcItfName()) ||
             it.isFcClientItf() != isFcClientItf() )
             return false;
        
        Class i1,i2;
        try {
            i1 = getFcItfClass();
            i2 = it.getFcItfClass();
        }
        catch(ClassNotFoundException cnfe) {
            return false;
        }
        
        if( it.isFcClientItf() ) {
            // I1 (this) is a sub type of client interface type I2 (it) if:
            
            // the language interface corresponding to I1 is a super interface
            // of the language interface corresponding to I2
            if( !i1.isAssignableFrom(i2) )
                return false;
            
            // if the contingency of I2 is optional,
            // then the contingency of I1 is optional too.
            if( it.isFcOptionalItf() ) {
                if( !isFcOptionalItf() )
                    return false;
            }
            
            // if the cardinality of I2 is collection,
            // then the cardinality of I1 is collection too.
            if( it.isFcCollectionItf() ) {
                if( !isFcCollectionItf() )
                    return false;
            }
        }
        else {
            // Server interface
            
            // I1 (this) is a sub type of server interface type I2 (it) if:
            
            // the language interface corresponding to I1 is a sub interface
            // of the language interface corresponding to I2
            if( !i2.isAssignableFrom(i1) )
                return false;
            
            // if the contingency of I2 is mandatory,
            // then the contingency of I1 is mandatory too.
            if( !it.isFcOptionalItf() ) {
                if( isFcOptionalItf() )
                    return false;
            }
            
            // if the cardinality of I2 is collection,
            // then the cardinality of I2 is collection too.
            if( it.isFcCollectionItf() ) {
                if( !isFcCollectionItf() )
                    return false;
            }
        }
        
        return false;
    }
    
    // --------------------------------------------------------------
    // InterfaceType implementation
    // --------------------------------------------------------------
    
    public String getFcItfName() {
        return name;
    }

    public String getFcItfSignature() {
        return signature;
    }

    public boolean isFcClientItf() {
        return isClient;
    }

    public boolean isFcOptionalItf() {
        return isOptional;
    }

    public boolean isFcCollectionItf() {
        return isCollection;
    }
    
    // --------------------------------------------------------------
    // Implementation specific methods
    // --------------------------------------------------------------
    
    private Class fcItfClass;
    
    /**
     * Return the class associated to the Java signature of this interface. 
     */
    public Class getFcItfClass() throws ClassNotFoundException {
        if( fcItfClass != null )
            return fcItfClass;
        
        fcItfClass = PlatformHelper.loadClass(getFcItfSignature());
        return fcItfClass;
    }

    // --------------------------------------------------------------
    // j.l.Object methods
    // --------------------------------------------------------------
    
    public String toString() {
        return name+"/"+signature+"/"+isClient+"/"+isOptional+"/"+isCollection;
    }
}
