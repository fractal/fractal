/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.type;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;

/**
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class ComponentTypeImpl implements ComponentType, Serializable {

    private static final long serialVersionUID = 6834581054002737468L;
    
    private InterfaceType[] interfaces;
        
    public ComponentTypeImpl(InterfaceType[] interfaces) {
        setFcInterfaceTypes(interfaces);
    }
    
    // --------------------------------------------------------------
    // Type implementation
    // --------------------------------------------------------------
    
    public boolean isFcSubTypeOf(Type type) {
        
        if( !(type instanceof ComponentType) )
            return false;
        
        ComponentType ct = (ComponentType) type;
        InterfaceType[] i2 = ct.getFcInterfaceTypes();
        
        for (int i = 0; i < interfaces.length; i++) {
            int j;
            for (j = 0; j < i2.length; j++) {
                if( interfaces[i].isFcClientItf() ) {
                    // There must exist an interface type that is a sub-type
                    if( i2[j].isFcSubTypeOf(interfaces[i]) )
                        break;
                }
                else {
                    // This is a server interface type.
                    // There must exist an interface type that is a super-type.
                    if( interfaces[i].isFcSubTypeOf(i2[j]) )
                        break;
                }
            }
            
            if( j == i2.length ) {
                // Nothing found
                return false;
            }
        }
        
        return true;
    }
    
    // --------------------------------------------------------------
    // ComponentType implementation
    // --------------------------------------------------------------
    
    public InterfaceType[] getFcInterfaceTypes() {
        return interfaces;
    }

    public InterfaceType getFcInterfaceType(String name)
    throws NoSuchInterfaceException {
        
        for (int i = 0; i < interfaces.length; i++) {
            if( ! interfaces[i].isFcCollectionItf() ) {
                // Singleton interface
                if( interfaces[i].getFcItfName().equals(name) )
                    return interfaces[i];
            }
            else {
                // Collection interface
                if( name.startsWith(interfaces[i].getFcItfName()) )
                    return interfaces[i];
            }
        }
        throw new NoSuchInterfaceException(name);
    }

    // --------------------------------------------------------------
    // Implementation specific methods
    // --------------------------------------------------------------
    
    private void setFcInterfaceTypes( InterfaceType[] interfaces ) {
        this.interfaces = interfaces;
        
        clientInterfaces = new ArrayList();
        
        // Skip the case where the (dumb) component type does not content any
        // interface
        if( interfaces == null )
            return;
        
        for (int i = 0; i < interfaces.length; i++) {            
            InterfaceType it = interfaces[i];
            if( it.isFcClientItf() && ! it.isFcCollectionItf() ) {
                clientInterfaces.add(it);
            }
        }
    }
    
    /** The List of client external interfaces implemented by this type. */
    private List clientInterfaces;
    
    public List getFcClientInterfaces() {
        return clientInterfaces;
    }
}
