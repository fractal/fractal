/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib;

import org.objectweb.fractal.aokell.lib.util.PlatformHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.util.Fractal;

/**
 * Default implementation of the {@link Interface} interface.
 * This class is extended by classes which implement component interfaces.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class InterfaceImpl implements Interface {
    
    private Component fcItfOwner;
    private String fcItfName;
    private Type fcItfType;
    private boolean fcInternalItf;
    

    public InterfaceImpl(Component fcItfOwner, String fcItfName,
            Type fcItfType, boolean fcInternalItf) {
        init(fcItfOwner,fcItfName,fcItfType,fcInternalItf);
    }
    
    public InterfaceImpl() {
    }
    
    public void init(Component fcItfOwner, String fcItfName,
            Type fcItfType, boolean fcInternalItf) {

        setFcItfOwner(fcItfOwner);
        setFcItfName(fcItfName);
        setFcItfType(fcItfType);
        setFcInternalItf(fcInternalItf);
    }
        

    // ------------------------------------------------------------------
    // Implementation of the Interface interface
    // ------------------------------------------------------------------
    
    public Component getFcItfOwner() {
        return fcItfOwner;
    }

    public String getFcItfName() {
        return fcItfName;
    }

    public Type getFcItfType() {
        return fcItfType;
    }

    public boolean isFcInternalItf() {
        return fcInternalItf;
    }

    
    // ------------------------------------------------------------------
    // Content instance associated to this interface
    // ------------------------------------------------------------------
    
    // Accessed by dynamically generated sub-classes
    protected Object fcContent;
    
    public void setFcItfImpl( Object fcContent ) {
        this.fcContent = fcContent;
    }
    
    // ------------------------------------------------------------------
    // Implementation specific methods
    // ------------------------------------------------------------------
    
    /**
     * If this interface can play the role of the source interface in a binding,
     * return the target interface currently bound to it (if there is one.)
     * Client interfaces or server interfaces owned by a composite can play the
     * role of the source interface in a binding.
     */
    public Object lookupFc() {
        try {
            return bc.lookupFc(fcItfName);
        }
        catch( NullPointerException npe ) {
            setBC();
            if( bc == null ) {
                String mess = fcItfOwner+" should have a binding controller";
                throw new RuntimeException(mess);
            }
            return lookupFc();
        }
        catch( NoSuchInterfaceException nsie ) {
            String mess = "No such interface exception: "+nsie.getMessage();
            throw new RuntimeException(mess);
        }
    }
    
    /** The binding controller associated to the owner of this interface. */
    protected BindingController bc;
    
    protected void setBC() {
        try {
            bc = Fractal.getBindingController(fcItfOwner);
        }
        catch( NoSuchInterfaceException nsie ) {
            String mess = "No such interface exception: "+nsie.getMessage();
            throw new RuntimeException(mess);
        }
    }
    
    public void setFcItfOwner( Component fcItfOwner ) {
        this.fcItfOwner = fcItfOwner;
        bc = null;
    }
    
    public void setFcItfName( String fcItfName ) {
        this.fcItfName = fcItfName;
    }
    
    public void setFcItfType( Type fcItfType ) {
        this.fcItfType = fcItfType;
    }
    
    public void setFcInternalItf(boolean fcInternalItf) {
        this.fcInternalItf = fcInternalItf;
    }

    
    // ------------------------------------------------------------------
    // Implementation of a mechanism for cloning InterfaceImpl instances
    // ------------------------------------------------------------------
    
    /*
     * The following method could not be replaced by a constructor.
     * We need in ComponentImpl to be able to create instances of sub-classes
     * of InterfaceImpl generated on the fly in
     * org.objectweb.fractal.aokell.lib.asm.
     * 
     *  The following method could be replaced by redefining clone() inherited
     *  from Object. However this would not work for the J2ME compliant version
     *  of AOKell as clone() is not part of the CLDC API. To avoid having two
     *  different versions of the cloning mechanism, the following scheme, which
     *  fits both cases, has been implemented. 
     */
    
    /**
     * Return a clone of the given InterfaceImpl.
     */
    public static InterfaceImpl clone( InterfaceImpl src ) {
        
        String clname = src.getClass().getName();
        
        InterfaceImpl target = null;
        try {
            Class cl = PlatformHelper.loadClass(clname);
            target = (InterfaceImpl) cl.newInstance();
        }
        catch( Exception e ) {
            throw new RuntimeException(e.getMessage());
        }
        
        target.setFcItfOwner( src.getFcItfOwner() );
        target.setFcItfName( src.getFcItfName() );
        target.setFcItfType( src.getFcItfType() );
        target.setFcInternalItf( src.isFcInternalItf() );
        
        return target;
    }
    
}
