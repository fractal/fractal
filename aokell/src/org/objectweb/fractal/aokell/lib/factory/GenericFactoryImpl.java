/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib.factory;

import org.objectweb.fractal.aokell.Membranes;
import org.objectweb.fractal.aokell.lib.util.FractalHelper;
import org.objectweb.fractal.aokell.lib.util.PlatformHelper;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;


/**
 * The implementation of the generic factory.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class GenericFactoryImpl implements GenericFactory {
    
    /**
     * The property name which defines the implementation class for the generic
     * factory. The value must be a sub-class of the GenericFactoryImpl. The
     * default value is GenericFactoryImpl.
     */ 
    public static final String GEN_FACT_IMPL = "org.objectweb.fractal.aokell.genericfactory";
    
    /** The singleton instance of itself. */
    private static GenericFactoryImpl singleton;
    
    /*
     * Initialize the singleton instance of itself.
     */
    static {
        String gfClassName = System.getProperty(GEN_FACT_IMPL);
        if( gfClassName==null || gfClassName.length()==0 ) {
            singleton = new GenericFactoryImpl();
        }
        else {
            try {
                Class cl = PlatformHelper.loadClass(gfClassName);
                singleton = (GenericFactoryImpl) cl.newInstance();
            }
            catch( Exception e ) {
                throw new RuntimeException(e.getMessage());
            }
        }
    }
    
    public static GenericFactoryImpl get() {
        return singleton;
    }

    
    // -----------------------------------------------------------------
    // Implementation of the GenericFactory interface
    // -----------------------------------------------------------------
    
    public Component newFcInstance(
            Type type, Object controllerDesc, Object contentDesc )
    throws InstantiationException {
        
        /*
         * When called from Fractal ADL, the component creation process may be
         * accompagnied with some hints. One of the them is the class loader to
         * be used. In this case, the controllerDesc parameter is an array where
         * the 1st element contains the class loader.
         */
        if( controllerDesc.getClass().isArray() ) {
            Object[] array = (Object[]) controllerDesc;
            PlatformHelper.setLoader( array[0] );
            controllerDesc = array[1];
        }
        
        checkFcType(type,controllerDesc);
        checkFcControllerDesc(controllerDesc);
        checkFcContentDesc(type,controllerDesc,contentDesc);
                
        /*
         * Create the content.
         */
        Object content = newFcContent(type,controllerDesc,contentDesc);
        
        /*
         * Create the membrane.
         */
        MembraneFactoryItf mf = MembraneFactoryImpl.get();
        Component compctrl =
            mf.newFcMembrane(type,controllerDesc,contentDesc,content);
        
        return compctrl;
    }
    
    
    // -----------------------------------------------------------------
    // Implementation specific
    // -----------------------------------------------------------------
    
    /**
     * Instantiate the content part of a component.
     */
    protected Object newFcContent(
            Type type, Object controllerDesc, Object contentDesc)
    throws InstantiationException {
        
        /*
         * Adaptation for templates components.
         * Templates are created with something like:
         * Component cTmpl = cf.newFcInstance(
         *      cType, "flatPrimitiveTemplate",
         *      new Object[] { "flatPrimitive", "ClientImpl" });
         */
        if( controllerDesc instanceof String ) {
            if( ((String)controllerDesc).endsWith("Template") ) {
                Object[] cont = (Object[]) contentDesc;
                contentDesc = cont[1];
            }
        }
        
        if( contentDesc == null ) {
            /*
             * contentDesc is null for composite components (this is not
             * mandatory, a composite can have a content, but this is the case
             * most of the time).
             */
            return null;
        }
        
        // Safe: ckecked before in checkFcContentDesc
        String contentDescStr = (String) contentDesc;
        
        /*
         * Load the content class.
         */
        Class contentClass = null;
        try {
            contentClass = PlatformHelper.loadClass(contentDescStr);
        }
        catch(ClassNotFoundException cnfe) {
            throw new InstantiationException(
                    "Content class "+contentDescStr+" not found");
        }
        
        /*
         * Perform checkings on the content class.
         */
        checkFcContentClassImplementsServerInterfaces(type,contentClass);
        MembraneFactoryItf mf = MembraneFactoryImpl.get();
        if( mf.checkFcContentForBC(controllerDesc) ) {
            checkFcContentClassforBC(type,contentClass);
        }

        /*
         * Instantiate the content class.
         */
        Object content = null;
        try {
            content = contentClass.newInstance();
        }
        catch(Exception e) {
            throw new InstantiationException(
                    e.getClass().getName()+" when instantiating content class "+
                    contentDescStr+". Message: "+e.getMessage() );
        }
        
        return content;
    }
    
    
    // -----------------------------------------------------------------
    // Checks
    // -----------------------------------------------------------------
    
    /**
     * Check that the value given for the type is legal.
     */
    protected void checkFcType( Type type, Object controllerDesc )
        throws InstantiationException {
        
        /** type must be a component type. */
        if( ! (type instanceof ComponentType) )
            throw new InstantiationException(
                    "Argument type must be an instance of ComponentType");
        
        /** The Java interfaces referenced in the type must exist. */
        ComponentType ct = (ComponentType) type;
        InterfaceType[] its = ct.getFcInterfaceTypes();
        for (int i = 0; i < its.length; i++) {
            String itSignature = its[i].getFcItfSignature();
            try {
                PlatformHelper.loadClass(itSignature);
            }
            catch(ClassNotFoundException cnfe) {
                throw new InstantiationException(
                        "Interface "+itSignature+
                        " referenced in Fractal interface type "+
                        its[i].getFcItfName()+" not found");
            }
        }        
    }
        
    /**
     * Check that the given value is a legal controller description.
     */
    protected void checkFcControllerDesc( Object controllerDesc )
    throws InstantiationException {
        
        if( controllerDesc==null || (!(controllerDesc instanceof String)) ) {
            throw new InstantiationException(
                    "Argument controllerDesc must be a non null String");        
        }
        
        Membranes mr = Membranes.get();
        if( ! mr.containsControllerDesc(controllerDesc) ) {
            throw new InstantiationException(
                    "Unsupported controllerDesc: "+controllerDesc);
        }
    }
    
    /**
     * Check that the value given for the content description is legal.
     */
    protected void checkFcContentDesc(
            Type type, Object controllerDesc, Object contentDesc )
    throws InstantiationException {
        
        /** Check for primitive components. */
        if( controllerDesc.equals("flatPrimitive") ||
            controllerDesc.equals("primitive") ||
            controllerDesc.equals("flatParametricPrimitive") ||
            controllerDesc.equals("parametricPrimitive") ) {
            if( contentDesc==null || !(contentDesc instanceof String) )
                throw new InstantiationException(
                        "Argument contentDesc must be a non null String");        
        }
        
        /** Check for template components. */
        if( controllerDesc instanceof String ) {
            if( ((String)controllerDesc).endsWith("Template") ) {
                
                // Templates are created with something like:
                // Component cTmpl = cf.newFcInstance(
                //      cType, "primitiveTemplate",
                //      new Object[] { "primitive", "ClientImpl" });
                
                if( ! contentDesc.getClass().isArray() ) {
                    throw new InstantiationException(
                            "When instantiating a "+controllerDesc+
                            " contentDesc should be an array");
                }
                
                Object[] tab = (Object[]) contentDesc;
                if( tab.length != 2 ) {
                    throw new InstantiationException(
                            "When instantiating a "+controllerDesc+
                            " contentDesc should be an array of size 2");
                }
                
                checkFcControllerDesc(tab[0]);
                checkFcContentDesc(type,tab[0],tab[1]);
            }
        }
    }
    
    /**
     * Check that the content class implements the server interfaces defined in
     * the type.
     */
    protected void checkFcContentClassImplementsServerInterfaces(
            Type type, Class contentClass )
    throws InstantiationException {
        
        ComponentType ct = (ComponentType) type;
        InterfaceType[] its = ct.getFcInterfaceTypes();
        for (int i = 0; i < its.length; i++) {
            String name = its[i].getFcItfName();
            if( ! its[i].isFcClientItf() &&
                FractalHelper.isFcImplementableInterface(name) ) {
                
                String itSignature = its[i].getFcItfSignature();
                
                // Checked before by checkFcType
                // but we need the class here anyway
                Class itClass;
                try {
                    itClass = PlatformHelper.loadClass(itSignature);
                }
                catch(ClassNotFoundException cnfe) {
                    throw new InstantiationException(
                            "Interface "+itSignature+
                            " referenced in Fractal interface type "+
                            its[i].getFcItfName()+" not found");
                }
                
                if( ! itClass.isAssignableFrom(contentClass) ) {
                    throw new InstantiationException(
                            "Component content class "+contentClass.getName()+
                            " should implement the server interface "+
                            itSignature);
                }
            }
        }
    }

    /**
     * Check that the content class implements the BindingController interface
     * if at least one client interface is defined in its type.
     */
    protected void checkFcContentClassforBC( Type type, Class contentClass )
    throws InstantiationException {
        
        ComponentType ct = (ComponentType) type;
        InterfaceType[] its = ct.getFcInterfaceTypes();
        for (int i = 0; i < its.length; i++) {
            if( its[i].isFcClientItf() ) {
                if( ! BindingController.class.isAssignableFrom(contentClass) ) {
                    throw new InstantiationException(
                            "Content class "+contentClass.getName()+
                            " is associated to the client interface type "+
                            its[i].getFcItfName()+
                            " and must then implement the "+
                            BindingController.class.getName()+" interface");
                }
            }
        }
    }
}
