/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.lib;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.julia.ComponentInterface;

/**
 * Default implementation of the {@link ComponentInterface} interface.
 * This class is extended by classes which implement server interfaces
 * associated to primitive components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class ComponentInterfaceImpl
    extends InterfaceImpl
    implements ComponentInterface {
    
    public ComponentInterfaceImpl() {
    }
    
    public ComponentInterfaceImpl(
            Component fcItfOwner, String fcItfName,
            Type fcItfType, boolean fcInternalItf ) {
        super(fcItfOwner,fcItfName,fcItfType,fcInternalItf);
    }

    
    // ------------------------------------------------------------------
    // Implementation of the ComponentInterface interface
    // ------------------------------------------------------------------
    
    public Object getFcItfImpl() {
        return fcContent;
    }

    public boolean hasFcInterceptor() {
        return false;
    }

    public Object clone() {
        return InterfaceImpl.clone(this);
    }    
}
