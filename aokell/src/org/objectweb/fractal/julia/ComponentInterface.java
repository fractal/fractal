/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia;

import org.objectweb.fractal.api.Interface;

/**
 * An extended {@link Interface} interface. This extended interface provides
 * access to the object that implements a component interface.
 */

public interface ComponentInterface extends Interface {

  /**
   * Sets the name of this component interface.
   *
   * @param name the new name of this component interface.
   */

  void setFcItfName (String name);

  /**
   * Returns the object that implements this component interface.
   *
   * @return the object that implements this component interface.
   * @see #setFcItfImpl setFcItfImpl
   */

  Object getFcItfImpl ();

  /**
   * Sets the object that implements this component interface.
   *
   * @param impl the object that implements this component interface.
   * @see #getFcItfImpl getFcItfImpl
   */

  void setFcItfImpl (Object impl);

  /**
   * Returns <tt>true</tt> if this interface has a permanently associated
   * interceptor. Note that, due to the algorithm to compute shortcut links
   * between components, the object that implements this component interface may
   * temporarily be an interceptor object, even if this interface has no
   * permanently associated interceptor. This method is therefore NOT equivalent
   * to {@link #getFcItfImpl getFcItfImpl} <tt>instanceof Interceptor</tt> (but
   * it implies it).
   *
   * @return <tt>true</tt> if this interface has a permanently associated
   *      interceptor.
   */

  boolean hasFcInterceptor ();

  /**
   * Creates and returns a copy of this component interface.
   *
   * @return a copy of this component interface.
   */

  Object clone ();
}
