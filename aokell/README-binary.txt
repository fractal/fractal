============================================================================
AOKell
Copyright (C) 2005-2006 INRIA, France Telecom, USTL

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Contact: Lionel.Seinturier@lifl.fr

Author: Lionel Seinturier
============================================================================


AOKell 2.0 Binary Distribution
-------------------------------

AOKell is the INRIA and France Telecom implementation of the Fractal
specifications. AOKell is a free software distributed under the terms of the
GNU Lesser General Public license. AOKell is written in Java.


Table of content
----------------
  1. Requirements
  2. Running the examples distributed with AOKell


1. Requirements
---------------
The following software is required to run and use AOKell:
	- J2SE >= 1.5
	- Ant >= 1.6

If you want to customize the AOKell framework, please download the source
distribution.


2. Running the examples distributed with AOKell
-----------------------------------------------
Several examples are distributed with AOKell. The source code of these examples
is located under the examples/ directory. The list of available examples
follows.

	- helloworld: Hello world
	- comanche: The comanche web server

	- bench: JACBenchmark application
	- benchsep: a second version of the JACBenchmark application
	- collection: A simple example with auto-binding components
	- estore: A simple example with collection interfaces


To run the examples, type (xxx is the example name):
	cd examples/xxx
	ant execute

Typying simply "ant" in the example directory displays a short description of
the example.


For any question concerning AOKell, please contact Lionel.Seinturier@lifl.fr
