/***
 * AOKell
 * Copyright (C) 2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue;


import org.objectweb.fractal.aokell.lib.membrane.marker.NameType;

import spoon.processing.AbstractProcessor;
import spoon.reflect.declaration.CtClass;
import spoon.template.Substitution;
import spoon.template.Template;

/**
 * This processor introduces the features provided by the name controller.
 * These features are introduced for all classes that implement the NameType
 * marker interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class LoggerControllerProcessor extends AbstractProcessor<CtClass> {

    public boolean isToBeProcessed(CtClass ct) {
        return SpoonHelper.impls(ct,DreamLogType.class);
    }

    public void process(CtClass ct) {
        Template t = new LoggerControllerTemplate();
        Substitution.insertAll(ct,t);
    }
    
}

