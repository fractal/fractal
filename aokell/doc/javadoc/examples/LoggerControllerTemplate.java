/***
 * AOKell
 * Copyright (C) 2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue;

import org.objectweb.fractal.aokell.lib.util.FractalHelper;
import org.objectweb.fractal.api.Component;
import aokell.dream.lib.control.logger.LoggerControllerItf;

import spoon.template.Local;
import spoon.template.Template;

/**
 * This template defines the elements which are introduced in all classes
 * which are processed by LoggerControllerProcessor.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class LoggerControllerTemplate implements Template, LoggerControllerItf {
    
    @Local
    public LoggerControllerTemplate() {}
    
    @Local
    private Component getFcComponent() {
        return null;
    }
    
    // -----------------------------------------------------------
    // LoggerController interface implementation
    // -----------------------------------------------------------
    
    public void setBaseName(String name) {
        LoggerControllerItf _loggerc = (LoggerControllerItf)
			FractalHelper.getFcInterface(getFcComponent(),"logger-controller");
        _loggerc.setBaseName(name);
    }

    public String getBaseName() {
        LoggerControllerItf _loggerc = (LoggerControllerItf)
			FractalHelper.getFcInterface(getFcComponent(),"logger-controller");
        return _loggerc.getBaseName();
    }

    public int getLoggerLevel(String loggerName) {
        LoggerControllerItf _loggerc = (LoggerControllerItf)
			FractalHelper.getFcInterface(getFcComponent(),"logger-controller");
        return _loggerc.getLoggerLevel(loggerName);
    }

    public void setLoggerLevel(String loggerName, int level) {
        LoggerControllerItf _loggerc = (LoggerControllerItf)
			FractalHelper.getFcInterface(getFcComponent(),"logger-controller");
        _loggerc.setLoggerLevel(loggerName,level);
    }

    public String[] getLoggerNames() {
        LoggerControllerItf _loggerc = (LoggerControllerItf)
			FractalHelper.getFcInterface(getFcComponent(),"logger-controller");
        return _loggerc.getLoggerNames();
    }
    
    // -----------------------------------------------------------
    // LoggerControllerRegister interface implementation
    // -----------------------------------------------------------
    
    public void register(String loggerName, Loggable loggable) {
        LoggerControllerItf _loggerc = (LoggerControllerItf)
			FractalHelper.getFcInterface(getFcComponent(),"logger-controller");
        _loggerc.register(loggerName,loggable);
    }

    public void unregiser(String loggerName, Loggable loggable) {
        LoggerControllerItf _loggerc = (LoggerControllerItf)
			FractalHelper.getFcInterface(getFcComponent(),"logger-controller");
        _loggerc.unregiser(loggerName,loggable);
    }

}
