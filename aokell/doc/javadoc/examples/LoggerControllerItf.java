/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.dream.lib.control.logger;

import org.objectweb.dream.control.logger.LoggerController;
import org.objectweb.dream.control.logger.LoggerControllerRegister;
import org.objectweb.fractal.aokell.lib.type.InterfaceTypeImpl;
import org.objectweb.fractal.api.type.InterfaceType;

/**
 * Unified interface for LoggerControllerRegister and LoggerController.
 * Both interfaces are implemented by BasicLoggerControllerImpl.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public interface LoggerControllerItf
    extends
        LoggerController,
        LoggerControllerRegister {

    final public static String NAME = "logger-controller";
    
    final public static InterfaceType TYPE =
      new InterfaceTypeImpl(
              NAME,
              LoggerControllerItf.class.getName(),
              false, false, false);

}
