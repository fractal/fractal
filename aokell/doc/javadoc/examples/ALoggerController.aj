/***
 * AOKell
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.aokell.glue;

/**
 * This aspect introduces the features provided by the logger controller.
 * These features are introduced for all classes that implement the
 * LoggerType marker interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */

import aokell.dream.lib.control.logger.LoggerControllerItf;
import aokell.dream.lib.membrane.marker.LoggerType;
import aokell.lib.util.MFractalHelper;

import org.objectweb.dream.control.logger.Loggable;

public aspect ALoggerController {
        
    // -----------------------------------------------------------
    // Introduction of the LoggerController interface
    // Delegate calls to an instance
    // -----------------------------------------------------------
    
    public void LoggerType.setBaseName(String name) {
        LoggerControllerItf _loggerc = (LoggerControllerItf) MFractalHelper.lookupFcController(getFcMembrane(),"logger-controller");
        _loggerc.setBaseName(name);
    }

    public String LoggerType.getBaseName() {
        LoggerControllerItf _loggerc = (LoggerControllerItf) MFractalHelper.lookupFcController(getFcMembrane(),"logger-controller");
        return _loggerc.getBaseName();
    }

    public int LoggerType.getLoggerLevel(String loggerName) {
        LoggerControllerItf _loggerc = (LoggerControllerItf) MFractalHelper.lookupFcController(getFcMembrane(),"logger-controller");
        return _loggerc.getLoggerLevel(loggerName);
    }

    public void LoggerType.setLoggerLevel(String loggerName, int level) {
        LoggerControllerItf _loggerc = (LoggerControllerItf) MFractalHelper.lookupFcController(getFcMembrane(),"logger-controller");
        _loggerc.setLoggerLevel(loggerName,level);
    }

    public String[] LoggerType.getLoggerNames() {
        LoggerControllerItf _loggerc = (LoggerControllerItf) MFractalHelper.lookupFcController(getFcMembrane(),"logger-controller");
        return _loggerc.getLoggerNames();
    }
    
    // -----------------------------------------------------------
    // Introduction of the LoggerControllerRegister interface
    // -----------------------------------------------------------
    
    public void LoggerType.register(String loggerName, Loggable loggable) {
        LoggerControllerItf _loggerc = (LoggerControllerItf) MFractalHelper.lookupFcController(getFcMembrane(),"logger-controller");
        _loggerc.register(loggerName,loggable);
    }

    public void LoggerType.unregiser(String loggerName, Loggable loggable) {
        LoggerControllerItf _loggerc = (LoggerControllerItf) MFractalHelper.lookupFcController(getFcMembrane(),"logger-controller");
        _loggerc.unregiser(loggerName,loggable);
    }

}
