This example shows how to program a simple Fractal application
with AOKell for the J2ME CLDC configuration.

This example requires the KVM which can be downloaded on the Sun
site at: http://java.sun.com/javame/downloads/index.html

To execute the example type:
  ant compile
  preverify.sh  (.bat on Windows)
  kvm.sh        (.bat on Windows)

The available Ant targets are:
 compile: compile the example
 help: display this help message
 clean: clean the generated files
 
When run, the example creates the following architecture:
- 1 root composite component
- 2 primitive components included in the root composite

  |--------------------------------------|
  |                                      |
  |        |--------|        |--------|  |
|-|-|--->|-| Client |-|--->|-| Server |  |
  |        |--------|        |--------|  |
  |                                      |
  |--------------------------------------|

The server owns an interface with a method for displaying a message
and an attribute controller exporting two attributes:
- header: text to be printed before the message
- count: how many time the message must be printed

When run, the program displays the following messages:

CLIENT created
SERVER created
Server: begin printing...
->hello world
Server: print done.
