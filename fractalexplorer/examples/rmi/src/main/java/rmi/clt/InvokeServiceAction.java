package rmi.clt;
/*====================================================================
 
 Objectweb Explorer Framework
 Copyright (C) 2000-2005 INRIA - USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

/** The console's imports */
import javax.swing.Box;
import javax.swing.JOptionPane;

import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.swing.gui.lib.LabelBox;

import rmi.clt.Service;

/**
 * This action allows to invoke the main method of the demo.
 *
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class InvokeServiceAction 
    implements MenuItem 
{

    protected LabelBox id_ = null;

    /**
     * Create a box containing all the box to specify all the params
     */
    protected Box createBox() {
        Box box = Box.createVerticalBox();
        box.add(Box.createVerticalGlue());
        id_ = new LabelBox("Value");
        box.add(id_);
        box.add(Box.createVerticalGlue());
        return box;
    }

    public void actionPerformed(MenuItemTreeView e) {
        Service s = (Service)e.getSelectedObject();
        InvokeThread t = new InvokeThread(s);
        t.start();
    }

    /**
     * @see org.objectweb.util.browser.api.MenuItem#isActive(org.objectweb.util.browser.gui.common.TreeView)
     */
    public int getStatus(TreeView treeView) {
        return MenuItem.ENABLED_STATUS;
    }

    public class InvokeThread extends Thread {
         
        protected Service s_ = null;
         
        public InvokeThread(Service s) {
            s_ = s;
        }
 
        public void run() {
            int result = JOptionPane.showOptionDialog(null, createBox(), "Invokes the print method", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, null, null);
            if (result == 0) {
                if(id_!=null) {
                    String message = id_.getLabel();
                    s_.print(message);      
                }
            }
        }
     }
 
}