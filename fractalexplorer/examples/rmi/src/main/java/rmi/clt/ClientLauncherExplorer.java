/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributors: Jerome Moroy, Philippe Merle
 *
 * $Id$
 */

package rmi.clt;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;

import org.objectweb.fractal.api.Component;

import org.objectweb.fractal.rmi.registry.NamingService;
import org.objectweb.fractal.rmi.registry.Registry;

import org.objectweb.fractal.util.Fractal;

public class ClientLauncherExplorer {

  public static void main (final String[] args) throws Exception {
    boolean useWrapper = false;
    for (int i = 0; i < args.length; ++i)
      useWrapper |= args[i].equals("wrapper");

    NamingService ns = Registry.getRegistry();

    Map context = new HashMap();
    context.put("remote-node", ns.lookup("server-host"));
    
    Factory f = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);

    String name = useWrapper ? "rmi.clt.WrappedClientServer" : "rmi.clt.ClientServer";
    Component c = (Component)f.newComponent(name, context);

    // Bind the component into the Fractal RMI Registry
    // in order to access it from Fractal Explorer.
    ns.rebind("Helloworld", c);

    // start root component
    Fractal.getLifeCycleController(c).startFc();

    // call main method
    ((Main)c.getFcInterface("m")).main(new String[0]);
  }
}
