The example is composed by 4 processes:
1) a Registry
2) a Server
3) a Client
4) an Explorer
 
which must be launched in that order. 
To do so, open 4 terminals, and invoke in i-th terminal the i-th command below:

1) mvn -Prun.registry
2) mvn -Prun.server
3) mvn -Prun.client
4) mvn -Prun.explorer

After it, you can kill manually each of the 4 processes.
