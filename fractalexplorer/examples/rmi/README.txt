This example shows how the Fractal RMI binder can be used to deploy
Fractal applications in a distributed way.

The ServerLauncher class constructs an instance of the Fractal RMI binder,
by using the Fractal ADL parser, and then uses this binder to export the
component identity interface of the Fractal bootstrap component. The
remote reference of this interface is then stored on disk.

The ClientLauncher class also constructs an instance of the Fractal RMI
binder, and uses it to create a remote binding to the previously exported
interface. It then constructs the composite template of the application
normally, by using the Fractal API. The only difference with the hello
world example is that two bootstrap components are used (a local one and
a remote one, which is used exactly as if it were local - distribution is
transparent). When the template factory interface of the remote bootstrap
component is used to create a template, this template is created in the
same JVM as the bootstrap component, i.e., remotely. The resulting
composite template therefore contains a local sub template, for the
client, and a remote sub template, for the server. When this template is
instantiated, each sub template creates an instance it is own JVM, which
results in a composite component with a local client sub component, and
a remote server sub component.

It also shows how to browse your Fractal based application using the 
Fractal Explorer.

Note: this example also shows the dynamic class loading feature of the
Fractal RMI binder. For example, the ServerImpl class, which is not
included in the classpath of the ServerLauncher, is dynamically
downloaded from the deployer host when the server template is remotely
created. This dynamic loading feature reuses the Java RMI mechanisms,
which use the java.rmi.server.codebase system property, and which
require a SecurityManager.
