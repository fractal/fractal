/*====================================================================

ObjectWeb Fractal Explorer
Copyright (C) 2000-2005 INRIA - USTL - LIFL - GOAL
Contact: fractal@objectweb.org

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Initial developer(s): Alexandre Vandekerkhove.
Contributor(s): Simon Leli�vre, Philippe Merle.

====================================================================
$Id$
====================================================================*/

package org.objectweb.fractal.explorer.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.SuperController;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.util.explorer.swing.graph.PortType;
import org.objectweb.util.explorer.swing.graph.VertexType;

/**
 * Utilities class for graphic representation
 * @author Alexandre Vandekerkhove
 * @version September 2004
 */
public class GraphInformations
{
    /**
     * Provides the first component (root) of an application
     * @param nameAppli the name of the application
     * @return the "root" component associated
     */
    public static Component getRootComponent (final String nameAppli){
        try {
			Factory f = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
            Object o = f.newComponent(nameAppli, new HashMap());
            return (Component)o;
		} catch (ADLException e) {
            System.out.println("Application " + nameAppli + " not found");
			return null;
		}
    }
    
    /**
     * @param component a component
     * @return true if the component is a primitive component (no sub-components)
     */
    public static boolean isPrimitiveComponent(final Component component) {
    	ContentController cc;
		try {
			cc = (ContentController)component.getFcInterface("content-controller");
			Component[] subComponents = cc.getFcSubComponents();
            return (subComponents.length == 0);
        } catch (NoSuchInterfaceException e) {
			return true;
		}
    }
    
    /**
     * Provides the type of a component (primitive/composite)
     * @param component the component
     * @return its type
     */
    public static String getComponentType (final Component component){
    	ContentController cc;
        try{
        	cc = (ContentController)component.getFcInterface("content-controller");
            Component[] subComponents = cc.getFcSubComponents();
            if (subComponents.length==0) return VertexType.PRIMITIF_VERTEX;
            return VertexType.COMPOSITE_VERTEX;
        } catch (NoSuchInterfaceException e){
        	return VertexType.PRIMITIF_VERTEX;
        }
    }
    
    /**
     * @param component a component
     * @return true if the component is a shared component
     */
    public static boolean isSharedComponent (final Component component){
    	SuperController sc;
        try{
            sc = (SuperController)component.getFcInterface("super-controller");
            Component[] superComponents = sc.getFcSuperComponents();
            return (superComponents.length > 1);
        } catch (NoSuchInterfaceException e){
        	return false;
        }
    }
    
    /**
     * Provides an array of the sub-components of a composite component
     * @param component the component
     * @return the array of its sub-components
     */
    public static Component[] getSubComponents (final Component component){     
        try {
            if (!isPrimitiveComponent(component)){
                ContentController cc = (ContentController)component.getFcInterface("content-controller");
                return cc.getFcSubComponents();
            }
        } catch (NoSuchInterfaceException e) {
            return null;
        }
        return null;
    }
    
    /**
     * Provides the execution state of a component
     * @param component the component
     * @return its execution state ("STARTED" / "STOPPED")
     */
    public static String getComponentState (final Component component){
        try {
            LifeCycleController lc = (LifeCycleController)component.getFcInterface("lifecycle-controller");
            return lc.getFcState();
		} catch (NoSuchInterfaceException e) {
			return "unknown state";
		}
    }
    
    /**
     * Returns true if the status of the component is "STARTED"
     * @param component the component
     */
    public static boolean isStarted (final Component component){
    	return getComponentState(component).equals("STARTED");
    }
    
    /**
     * Provides the array of the external interfaces of a component
     * The array is sorted by alphabetical order of the names of the interfaces
     * @param component the component
     * @return the array of the external interfaces
     */
    public static Interface[] getSortExtItf(final Component component){
        Object[] listExtItfObj = component.getFcInterfaces();
        Interface[] listItfExtItf = new Interface[listExtItfObj.length];
        List listExtItf = new ArrayList();
        for(int i=0 ; i<listExtItfObj.length ; i++){
        	listExtItf.add(i, listExtItfObj[i]);
        }
        Collections.sort(listExtItf, new SortInterface());
        for (int j=0 ; j<listExtItf.size() ; j++){
        	listItfExtItf[j] = (Interface)listExtItf.get(j);
        }
        return listItfExtItf;
    }

    /**
     * Provides the array of the internal interfaces of a component
     * @param component the component to introspect
     * @return the array of its internal interfaces
     */
    public static Interface[] getIntItf (final Component component){
    	try {
			ContentController cc = (ContentController)component.getFcInterface("content-controller");
            Object[] listIntItfObj = cc.getFcInternalInterfaces();
            Interface[] listIntItf = new Interface[listIntItfObj.length];
            for (int i=0 ; i<listIntItfObj.length ; i++){
            	listIntItf[i] = (Interface)listIntItfObj[i];
            }
            return listIntItf;
		} catch (NoSuchInterfaceException e) {
			System.out.println("No internal interfaces");
            return null;
		}
    }
    
    /**
     * Provides the name of an interface, summary name if it's a control interface ("content-controller" ==> "CC")
     * @param itf the interface 
     * @return the name of the interface
     */
    public static String getInterfaceName(final Interface itf){
        if (itf==null) return "unknown";
        else if (isControlInterface(itf)) {
            if (itf.getFcItfName().equals("component")) return "C";
            else if (itf.getFcItfName().equals("factory")) return "F";
            return (itf.getFcItfName().substring(0,1).toUpperCase() + "C");
        }
        return itf.getFcItfName();
    }
    
    /**
     * @param itf an interface
     * @return true if the interface is a  client interface
     */
    public static boolean isClientInterface (final Interface itf){
        InterfaceType itType = (InterfaceType)itf.getFcItfType();
        return itType.isFcClientItf();
    }
    
    /**
     * @param itf an interface
     * @return true if itf is a control interface
     */
    public static boolean isControlInterface (final Interface itf){
    	return (itf.getFcItfName().endsWith("-controller"))||(itf.getFcItfName().equals("component"))||(itf.getFcItfName().equals("factory"));
    }
    
    /**
     * @param itf a client interface
     * @return true if the client interface is a collection interface 
     */
    public static boolean isCollectionInterface(final Interface itf){
        InterfaceType itType = (InterfaceType)itf.getFcItfType();
        return itType.isFcCollectionItf();
    }
    
    /**
     * @param itf a client interface
     * @return true if the client interface is an optional interface
     */
    public static boolean isOptional(final Interface itf){
    	InterfaceType itType = (InterfaceType)itf.getFcItfType();
        return itType.isFcOptionalItf();
    }
    
    /**
     * Provides an ArrayList of the (InterfaceType) of the collection interfaces
     * Each collection will appear only one time in the ArrayList in order to represent a port per collection
     * and not a port per client interface
     * @param component the component to introspect
     * @return the ArrayList of the (InterfaceType) of its collection interfaces
     */
    public static List getItfCollection (final Component component){
    	Interface[] listExtItf = getSortExtItf(component);
        ArrayList listItfCollection = new ArrayList();
        ArrayList listNameCollection = new ArrayList();
        InterfaceType itType;
        String nameItf;
        for(int i=0 ; i<listExtItf.length ; i++){
        	if (isCollectionInterface(listExtItf[i])){
        		itType = (InterfaceType)listExtItf[i].getFcItfType();
                nameItf = itType.getFcItfName();
                //check if the collection already exists - it must appear only once
                if(!listNameCollection.contains(nameItf)){
                    listNameCollection.add(nameItf);
                    listItfCollection.add(itType);
                }
            }
        }
        return listItfCollection;
    }
    
    /**
     * Provides the type of the port associated to an interface : client/collection-client/server/control
     * @param itf the interface
     * @return the type of the port associated
     */
    public static String getPortType (final Interface itf){
    	if (isClientInterface(itf)) {
            if (isCollectionInterface(itf)) return PortType.COLLECTION_PORT;
            else return PortType.CLIENT_PORT;
        }
        else if (isControlInterface(itf)) return PortType.CONTROLLER_PORT;
        else return PortType.SERVER_PORT;
    }
    
    /**
     * Provides the signature of the interface
     * @param itf the interface 
     * @return the signature of the interface
     */
    public static String getSignature(final Interface itf){
        InterfaceType itType = (InterfaceType)itf.getFcItfType();
        return itType.getFcItfSignature();
    }
    
    /**
     *  Provides the destination component
     *  @param component the component "server" of the bind
     *  @param itf the client interface which is bound
     *  @return the component "client" of the bind
     */
    public static Component getTargetComponent (final Component component, final Interface itf){
        try {
            BindingController bc = (BindingController)component.getFcInterface("binding-controller");
            Interface destItf = (Interface)bc.lookupFc(itf.getFcItfName());
            if (destItf!=null)
                return destItf.getFcItfOwner();
            else return null;
        } catch (NoSuchInterfaceException e) {
            return null;
        }
    }
    
    /**
     *  Provides the the destination interface
     *  @param component the component "server" of the bind
     *  @param itf the client interface which is bound
     *  @return the interface "client" of the bind
     */
    public static Interface getTargetInterface (final Component component, final Interface itf){
        try {
            BindingController bc = (BindingController)component.getFcInterface("binding-controller");
            return (Interface)bc.lookupFc(itf.getFcItfName());
        } catch (NoSuchInterfaceException e) {
            return null;
        }
    }
    
    /**
     * Provides a List of the neighbors 'server' components of a primitive component 
     * @param primitiveComponent the primitive component
     * @return the list of its server neighbors
     */
    public static List getServerNeighbors(final Component primitiveComponent){
    	
        //we don't draw the neighbors of a shared component because it appears in several composite components
        if (isSharedComponent(primitiveComponent)) return new ArrayList();
            
        try {
    		SuperController sc = (SuperController)primitiveComponent.getFcInterface("super-controller");
            Component[] listSuperComponents = sc.getFcSuperComponents();
            Component superComponent = listSuperComponents[0];
            Component[] listSubComponents = getSubComponents(superComponent);
            Component subComponent;
            Interface[] listExtItf;
            Component targetComponent;
            ArrayList neighborsList = new ArrayList();
            //we put in the list the neighbors 'servers' of the primitiveComponent :
            //we have to introspect each sub-component and look its client interfaces
            for (int i=0 ; i<listSubComponents.length ; i++){
                subComponent = listSubComponents[i];
                listExtItf = getSortExtItf(subComponent);
                for (int j=0 ; j<listExtItf.length ; j++){
                    if(isClientInterface(listExtItf[j])){
                    	targetComponent = getTargetComponent(subComponent, listExtItf[j]);
                        if ( (targetComponent!=null) && 
                        	 (targetComponent.equals(primitiveComponent))&&
                        	 (!neighborsList.contains(subComponent)))
                            neighborsList.add(subComponent);
                    }
                }
            }
            return neighborsList;
    	} catch (NoSuchInterfaceException e) {
    		return new ArrayList();
    	}
    }
    
    /**
     * Provides a List of the neighbors 'client' components of a primitive component 
     * @param primitiveComponent the primitive component
     * @return the list of its client neighbors
     */
    public static List getClientNeighbors(final Component primitiveComponent){
        
        //we don't draw the neighbors of a shared component because it appears in several composite components
        if (isSharedComponent(primitiveComponent)) return new ArrayList();
    	
        Interface[] listExtItf;
        Component targetComponent;
        ArrayList neighborsList = new ArrayList();
        listExtItf = getSortExtItf(primitiveComponent);
        for (int i=0 ; i<listExtItf.length ; i++){
            if(isClientInterface(listExtItf[i])){
                targetComponent = getTargetComponent(primitiveComponent, listExtItf[i]);
                if((!neighborsList.contains(targetComponent))) {
                	Interface tgtItf = GraphInformations.getTargetInterface(primitiveComponent, listExtItf[i]);
                	if ((tgtItf != null) && (!tgtItf.isFcInternalItf()))
                        neighborsList.add(targetComponent);
                }
            }
        }
        return neighborsList;
    }
    
    /**
     * Sorts two interfaces by alphabetic order using their names.
     *  
     * @version 0.1
     */
    public static class SortInterface implements Comparator {

        protected int compare(Interface itf1, Interface itf2){
            return itf1.getFcItfName().compareTo(itf2.getFcItfName());
        }
        
        public int compare(Object o1, Object o2){
            return compare((Interface)o1,(Interface)o2);
        }

    }
}
