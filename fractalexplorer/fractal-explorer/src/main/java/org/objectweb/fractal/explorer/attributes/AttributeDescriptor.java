/**
 * Dream
 * Copyright (C) 2003-2004 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Jakub Kornas
 * Contributor(s): 
 */

package org.objectweb.fractal.explorer.attributes;

/**
 * A class to represent attributes.
 */
public class AttributeDescriptor {
    private String attributeName = null;

    private Object attributeValue = null;

    private boolean isMutable = false;

    private Thread monitor = null;

    /**
     * @param attributeName
     * @param attributeValue
     * @param isMutable
     */
    public AttributeDescriptor(String attributeName, Object attributeValue,
            boolean isMutable) {
        this.attributeName = attributeName;
        this.attributeValue = attributeValue;
        this.isMutable = isMutable;
    }

    /**
     * Returns the name of the attribute represented by this descriptor.
     * 
     * @return attributeName the name of the attribute.
     */
    public String getName() {
        return this.attributeName;
    }

    /**
     * Returns the value of the attribute represented by this descriptor.
     * 
     * @return attributeValue the value of the attribute.
     */
    public Object getValue() {
        return this.attributeValue;
    }

    /**
     * Tells whether a value of the attribute represented by this descriptor is
     * mutable (can be modified).
     * 
     * @return isMutable <code>true</code> if the attribute's value can be
     *         modified, <code>false</code> otherwise.
     */
    public boolean isMutable() {
        return this.isMutable;
    }

    /**
     * This method allows for the modification of the attribute's value.
     * 
     * @param newValue
     *            new value of the attribute represented by this descriptor.
     */
    public void setValue(Object newValue) {
        this.attributeValue = newValue;
    }

    /**
     * Tells whether the value of the attribute represented by this descriptor
     * is subject to monitoring, that is whether it is tested periodically for
     * modifications.
     * 
     * @return <code>true</code> if the attribute represented by this
     *         descriptor is subject to monitoring, <code>false</code>
     *         otherwise.
     */
    public boolean hasMonitor() {
        return monitor != null;
    }

    /**
     * Sets the monitor of the attribute represented by this descriptor to the
     * <code>Thread</code> passed as this method's parameter. The monitor is
     * responsible for periodically testing the value of the attribute
     * represented by this descriptor. The monitor has to be started externally
     * (its start method is not called in this method's body). If a monitor for
     * this descriptor already exists when this method is called, the "old"
     * monitor is stopped and destroyed.
     * 
     * @param monitor
     */
    public void setMonitor(Thread monitor) {
        if (hasMonitor()) {
            ((AttributeMonitoringThread) this.monitor).running = false;
            this.monitor = null;
        }
        this.monitor = monitor;
    }

    /**
     * Returns the <code>Thread</code> responsible for periodically testing
     * the value of the attribute corresponding to this descriptor.
     * 
     * @return monitor the thread responsible for periodically testing the value
     *         of the attribute corresponding to this descriptor.
     *         <code>null</code> if the monitor does not exist.
     */
    public Thread getMonitor() {
        return this.monitor;
    }
}
