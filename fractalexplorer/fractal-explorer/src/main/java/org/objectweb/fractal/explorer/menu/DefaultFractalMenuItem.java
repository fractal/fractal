/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.menu;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.fractal.explorer.api.FractalMenuItem;
import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.TreeView;

/**
 * Specialization of the menu item for entities having life cycle properties.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public abstract class DefaultFractalMenuItem 
           implements MenuItem, 
                      FractalMenuItem 
{
    //==================================================================
    //
    // No internal state.
    //
    //==================================================================
    
    //==================================================================
    //
    // No constructor.
    // 
    //==================================================================
    
    //==================================================================
    //
    // Internal methods.
    //
    //==================================================================
    
    //==================================================================
    //
    // No public method.
    //
    //==================================================================
    
    /**
     * Compute if the component is started or not.
     */
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#getStatus(org.objectweb.util.explorer.api.TreeView)
     */
    public int getStatus(TreeView arg0) {
        LifeCycleController lcc = null;
        try{
            lcc = FcExplorer.getLifeCycleController(FcExplorer.getComponent(arg0.getSelectedObject()));
        } catch(NoSuchInterfaceException e) {
            try {
                return getStartedStatus(arg0);
            } catch (Exception e1) {
                return getStoppedStatus(arg0);
            }                
        }
        if(lcc.getFcState().equals(LifeCycleController.STARTED))
            return getStartedStatus(arg0);
        return getStoppedStatus(arg0);
    }
    
    /**
     * Call the appropriate <code>actionPerformed</code> depending on the status of the component (STARTED or STOPPED). 
     */
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#actionPerformed(org.objectweb.util.explorer.api.MenuItemTreeView)
     */
    public void actionPerformed(MenuItemTreeView arg0) throws Exception {
        LifeCycleController lcc = null;
        try {
            lcc = FcExplorer.getLifeCycleController(FcExplorer.getComponent(arg0.getSelectedObject()));
        } catch(NoSuchInterfaceException e) {
            try {
                actionStartedPerformed(arg0);
            } catch (Exception e1) {
                actionStoppedPerformed(arg0);
            }
        }
        if(lcc.getFcState().equals(LifeCycleController.STARTED))
            actionStartedPerformed(arg0);
        else
            actionStoppedPerformed(arg0);
    }
    
    public int getStartedStatus(TreeView arg0) {
        return MenuItem.ENABLED_STATUS;
    }
    
    public int getStoppedStatus(TreeView arg0) {
        return MenuItem.NOT_VISIBLE_STATUS;
    }
    
    public void actionStoppedPerformed(MenuItemTreeView arg0) throws Exception {
        // Nothing to do
    }
    
    public abstract void actionStartedPerformed(MenuItemTreeView arg0) throws Exception;
}