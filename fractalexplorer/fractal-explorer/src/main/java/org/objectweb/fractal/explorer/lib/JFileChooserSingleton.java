/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.lib;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import org.objectweb.fractal.explorer.api.FractalBrowserConstants;

/**
 * This class gives you a JFileChooser.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class JFileChooserSingleton 
{
    //==================================================================
    //
    // Internal states.
    //
    //==================================================================
    
    /** The JFileChooser instance. */
    protected static JFileChooser instance_ = null;
    
    /** The FractalFileFilter instance. */
    protected static FileFilter fractalFileFilter_ = new FractalFileFilter();
    
    /** The XMLFileFilter instance. */
    protected static FileFilter xmlFileFilter_ = new XMLFileFilter();
        
    //==================================================================
    //
    // No constructor.
    // 
    //==================================================================
    
    //==================================================================
    //
    // Internal methods.
    //
    //==================================================================
    
    /**
     * Initialize the instance.
     */
    protected static void init() {
        instance_ = new JFileChooser();
    }
    
    /**
     * Gives a JFileChooser with a specific FileFilter.
     * @param fileFilterType The FileFilter type.
     *  <ul>
     *    <li>FractalBrowserConstants.FRACTAL_FILE_FILTER</li>
     *    <li>FractalBrowserConstants.DIRECTORY_FILE_FILTER</li>
     *    <li>FractalBrowserConstants.XML_FILE_FILTER</li>
     *  </ul>
     * @return The appropriate JFileChooser.
     */
    public static JFileChooser getInstance(int fileFilterType) {
        File f = null;
        if (instance_ != null)
            f = instance_.getCurrentDirectory();
        else
            f = new File(System.getProperty("user.dir"));            
        init();
        instance_.setCurrentDirectory(f);
        if (fileFilterType == FractalBrowserConstants.FRACTAL_FILE_FILTER)
            instance_.setFileFilter(fractalFileFilter_);
        else if (fileFilterType == FractalBrowserConstants.DIRECTORY_FILE_FILTER)
            instance_.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        else if (fileFilterType == FractalBrowserConstants.XML_FILE_FILTER)
            instance_.setFileFilter(xmlFileFilter_);
        return instance_;        
    }
}
