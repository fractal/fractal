/*====================================================================
 
 Objectweb Fractal Explorer
 Copyright (C) 2000-2005 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.context;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.util.explorer.core.common.lib.DefaultContextContainer;

/**
 * A new ContextContainer which has a specific icon.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class ClientCollectionInterfaceContainer 
     extends DefaultContextContainer 
{
    
    //==================================================================
    //
    // Internal state.
    //
    //==================================================================
    
    /** contained interface. */
    protected InterfaceType itf_;
    
    /** The associated component. */
    protected Component comp_;
    
    //==================================================================
    //
    // Constructor.
    // 
    //==================================================================
    
    /**
     * Default constructor.
     * @param itf The client interface.
     * @param comp The component contained.
     */
    public ClientCollectionInterfaceContainer(InterfaceType itf, Component comp) {
        super();
        this.itf_ = itf;
        this.comp_ = comp;
    }
    
    //==================================================================
    //
    // No internal method.
    //
    //==================================================================
    
    //==================================================================
    //
    // Public method.
    //
    //==================================================================
    
    /**
     * Provides the contained interface.
     * @return the contained interface.
     */
    public InterfaceType getItf() {
        return this.itf_;
    }
    
    /**
     * @return The associated component.
     */
    public Component getComponent() {
        return this.comp_;
    }
}