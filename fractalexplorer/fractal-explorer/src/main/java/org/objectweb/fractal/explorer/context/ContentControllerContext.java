/*====================================================================

Objectweb Browser Framework
Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
Contact: openccm@objectweb.org

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Initial developer(s): Jerome Moroy.
Contributor(s): Romain Rouvoy______________________________________.

---------------------------------------------------------------------
 $Id$
====================================================================*/

package org.objectweb.fractal.explorer.context;

import java.util.List;
import java.util.Vector;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.util.explorer.api.Context;
import org.objectweb.util.explorer.api.Entry;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;

/**
 * ContentController interface navigation context. 
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.2
 */
public class ContentControllerContext 
  implements Context 
{
    // ==================================================================
    //
    // Internal state.
    //
    // ==================================================================

    // ==================================================================
    //
    // No Constructors.
    //
    // ==================================================================

    // ==================================================================
    //
    // Internal methods.
    //
    // ==================================================================

    // ==================================================================
    //
    // Public methods for interface Context.
    //
    // ==================================================================

    /**
     * Returns an array containing the entries contained by the target context.
     * @return A new array of entry.
     */
    public Entry[] getEntries(Object object) {
        ContentController cc = (ContentController)object;
        Object[] ir = cc.getFcInternalInterfaces();
        Component[] ci = cc.getFcSubComponents();
        List l = new Vector();
        int cpt = 0;
        // List of interfaces.
        for (int i = 0; i < ir.length; i++) {
            Interface itf = (Interface) ir[i];
            l.add(new DefaultEntry(FcExplorer.getName(itf), InterfaceWrapperFactory.singleton.getWrapper(itf)));
            cpt = cpt + 1 ;
        }
        
        // List of components.
        for (int i = 0 ; i < ci.length ; i++){
            l.add(new DefaultEntry(FcExplorer.getName(ci[i]), ci[i]));
            cpt = cpt + 1 ;
        }
        return (Entry[])l.toArray(new Entry[0]);
    }
}
