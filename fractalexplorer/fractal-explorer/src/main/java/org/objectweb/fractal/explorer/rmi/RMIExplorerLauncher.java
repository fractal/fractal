/*====================================================================

Objectweb Fractal Explorer
Copyright (C) 2000-2007 INRIA & USTL - LIFL - GOAL
Contact: fractal@objectweb.org

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Initial developer(s): Jerome Moroy.
Contributor(s): Philippe Merle.

---------------------------------------------------------------------
$Id$
====================================================================*/
package org.objectweb.fractal.explorer.rmi;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.util.Fractal;

/**
 * This main class launches the Fractal RMI Explorer.
 * 
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * 
 * @version 0.1
 */
public class RMIExplorerLauncher {

    /**
     * @param args
     * @throws Exception 
     */
    public static void main(String[] args) throws Exception {
        // Obtains the main class to load.
        String configs = "";
        for(int i=0 ; i<args.length ; i++) {
            if(args[i].equals("-config") && (i+1)<args.length){
                configs += " " + args[i+1];
                i++;
            }
        }
        
        Map context = new HashMap();
        context.put("config", configs);
        Factory f = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
        Component c = (Component)f.newComponent("org.objectweb.fractal.explorer.rmi.FractalRMIExplorer", context);
        Fractal.getLifeCycleController(c).startFc();
    }

}
