/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.lib;

/**
 * A wrapper for Signature.
 * 
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class SignatureWrapper 
{
    // ==================================================================
    //
    // Internal state.
    //
    // ==================================================================
    
    /** The InterfaceReference. */
    protected String signature_ = null;
    
    // ==================================================================
    //
    // Constructors.
    //
    // ==================================================================
    
    /**
     * Default constructor.
     * @param signature 
     */
    public SignatureWrapper(String signature) {
        signature_ = signature;
    }
    
    // ==================================================================
    //
    // No Internal methods.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // Public methods.
    //
    // ==================================================================
    
    /**
     * Provides the associated signature.
     * @return the associated signature.
     */
    public String getSignature(){
        return signature_;
    }    
}
