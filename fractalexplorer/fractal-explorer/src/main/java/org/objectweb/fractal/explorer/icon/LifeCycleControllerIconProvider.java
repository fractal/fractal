/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.icon;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.util.explorer.api.IconProvider;

/**
 * Provides a specific icon depending on the status of the component.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class LifeCycleControllerIconProvider 
  implements IconProvider
{
    //==================================================================
    //
    // Internal states.
    //
    //==================================================================
    
    /** The different icons. */
    protected Icon startIcon, stopIcon;
    
    //==================================================================
    //
    // Constructors.
    // 
    //==================================================================
    
    /** Empty constructor */
    public LifeCycleControllerIconProvider() {
        try{
            startIcon = new ImageIcon(Thread.currentThread().getContextClassLoader().getResource("icons/Started.png"));
            stopIcon =  new ImageIcon(Thread.currentThread().getContextClassLoader().getResource("icons/Stopped.png"));
        } catch (Exception e) {
            System.err.println("Resource not found: " + e);
        }
    }
    
    //==================================================================
    //
    // No internal method.
    //
    //==================================================================
    
    //==================================================================
    //
    // Public methods for IconProvider interface.
    //
    //==================================================================
    
    /**
     * Provides the appropriate icon
     */
    public Object newIcon(Object object) {
        LifeCycleController lcc = (LifeCycleController) object;
        String status = lcc.getFcState();
        if (status.equals(LifeCycleController.STARTED))
            return startIcon;
        else if (status.equals(LifeCycleController.STOPPED))
            return stopIcon;
        else
            return null;
    }    
}
