/**
 * Dream
 * Copyright (C) 2003-2004 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Jakub Kornas
 * Contributor(s): Christophe Demarey
 */

package org.objectweb.fractal.explorer.panel;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import org.objectweb.fractal.explorer.attributes.AttributeDescriptor;
import org.objectweb.fractal.explorer.attributes.AttributeMonitoringThread;
import org.objectweb.fractal.explorer.attributes.AttributesTable;
import org.objectweb.fractal.explorer.attributes.AttributesTableModel;

/**
 * A custom <code>CellEditor</code> for the attribute table's cells that
 * contain a <code>JButton</code> used to launch attribute monitoring
 * threads.
 */
public class MonitorCellEditor extends DefaultCellEditor
{
  protected JButton                   button           = null;
  private String                      label            = null;
  private boolean                     initialized      = false;
  private MonitorEditorActionListener actionListener   = null;

  int                                 row;
  int                                 column;

  /**
   * @see DefaultCellEditor#DefaultCellEditor(JCheckBox)
   */
  public MonitorCellEditor(JCheckBox checkBox)
  {
    super(checkBox);

    button = new JButton();
    button.setOpaque(true);
  }

  /**
   * @see DefaultCellEditor#getTableCellEditorComponent(JTable, Object, boolean,
   *      int, int)
   */
  public Component getTableCellEditorComponent(JTable table, Object value,
      boolean isSelected, int row1, int column1)
  {
    if (!initialized)
    {
      this.button.setFont(table.getFont());
      this.actionListener = new MonitorEditorActionListener(table);
      this.button.addActionListener(actionListener);
      initialized = true;
    }

    this.row = row1;
    this.column = column1;

    button.setBackground(table.getBackground());

    label = (value == null) ? "" : value.toString();
    button.setText(label);
    button.setFont(table.getFont());
    return button;
  }

  private class MonitorEditorActionListener implements ActionListener
  {
    private JTable table = null;

    /**
     * @param table
     */
    public MonitorEditorActionListener(JTable table)
    {
      this.table = table;
    }

    public void actionPerformed(ActionEvent evt)
    {
      String state = table.getValueAt(row, column).toString();

      if (state.equals(AttributesTableModel.MONITOR_ACTIONS[0]))
      {
        if ( isAttributeReadable() ) 
        {          
          JOptionPane pane = new JOptionPane("Monitoring period [ms]",
              JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION, null);
          pane.setWantsInput(true);
          JDialog dialog = pane.createDialog(table, "Enter monitoring period");
          dialog.show();
          Object monitoringPeriod = pane.getInputValue();
          if (monitoringPeriod != JOptionPane.UNINITIALIZED_VALUE)
          {
            Integer newFrequency = null;
            String attributeName = null;
            try
            {
              attributeName = table.getValueAt(row, 0).toString();
              newFrequency = Integer.valueOf(monitoringPeriod.toString());
            }
            catch (Exception e)
            {
              showFrequencyErrorMessage();
              fireEditingCanceled();
              return;
            }
            AttributesTableModel model = (AttributesTableModel) table.getModel();
            AttributeDescriptor attribute = model
                .getAttributeDescriptor(row);
            if (!attribute.hasMonitor())
            {
              AttributesTable aTable = (AttributesTable) table;
              Thread monitor = new AttributeMonitoringThread(newFrequency
                  .intValue(), aTable.getAttributeObject(), attributeName, table,
                  row);
              attribute.setMonitor(monitor);
              monitor.start();
  
              fireEditingStopped();
              return;
            } 
              // should never get here
              System.err.println("monitor already added for this attribute");
              fireEditingCanceled();
              return;
          }
        }
      }
      else if (state.equals(AttributesTableModel.MONITOR_ACTIONS[1]))
      {
        AttributesTableModel model = (AttributesTableModel) table.getModel();
        AttributeDescriptor attribute = model.getAttributeDescriptor(row);

        AttributeMonitoringThread monitor = (AttributeMonitoringThread) attribute
            .getMonitor();
        if (monitor != null)
        {
          monitor.running = false;
          attribute.setMonitor(null);

          fireEditingStopped();
          return;
        }
        fireEditingCanceled();
        return;
      }

      fireEditingCanceled();
    }

    /**
     * Check if the attribute is readable before monitoring it.
     * 
     * @return true is the attribute is readable, else false.
     */
    private boolean isAttributeReadable() {
      String attributeName = table.getValueAt(row, 0).toString();
      Object monitoredObject = ((AttributesTable) table).getAttributeObject();

      try {
          monitoredObject.getClass().getMethod("get" + attributeName,
                    (Class[])null).invoke(monitoredObject, (Object[])null);
      } catch (Exception e) {
        showNonReadableAttributeErrorMessage();
        fireEditingCanceled();
        return false;
      }
      return true;
    }
    
    private void showFrequencyErrorMessage()
    {
      JOptionPane.showMessageDialog(table,
          "Monitoring period must be an INTEGER", "Wrong monitoring period",
          JOptionPane.ERROR_MESSAGE);
    }

    private void showNonReadableAttributeErrorMessage()
    {
      JOptionPane.showMessageDialog(table,
          "This attribute cannot be monitored as it doesn't provide a getter method!",
          "Cannot found a getter for this attribute!",
          JOptionPane.ERROR_MESSAGE);
    }
  }
}

