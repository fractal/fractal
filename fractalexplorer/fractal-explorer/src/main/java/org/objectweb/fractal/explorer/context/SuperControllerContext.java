/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Romain Rouvoy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.context;

import java.util.List;
import java.util.Vector;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.SuperController;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.util.explorer.api.Context;
import org.objectweb.util.explorer.api.Entry;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;

/**
 * SuperController interface navigation context. 
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version 0.1
 */
public class SuperControllerContext 
  implements Context 
{
    
    // ==================================================================
    //
    // Internal state.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // No Constructors.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // Internal methods.
    //
    // ==================================================================
        
    // ==================================================================
    //
    // Public methods for interface Context.
    //
    // ==================================================================
    
    /**
     * Returns an array containing the entries contained by the target context.
     * @return A new array of entry.
     */
    public Entry[] getEntries(Object object){
        SuperController sc = (SuperController)object;
        Component[] ci = sc.getFcSuperComponents();
        List l = new Vector();
        
        // List of super components
        for (int i = 0 ; i < ci.length ; i++){
            l.add(new DefaultEntry(FcExplorer.getName(ci[i]), ci[i]));
        }
        return (Entry[])l.toArray(new Entry[0]);
    }
    
}
