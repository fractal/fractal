/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.panel;

import java.awt.Color;

import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.util.explorer.api.Panel;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.swing.gui.lib.LabelBox;

/**
 * This panel displays InterfaceType class info. 
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class InterfaceTypePanel 
  implements Panel 
{
    
    // ==================================================================
    //
    // Internal state.
    //
    // ==================================================================
    
    protected TreeView treeView_ = null;
    
    protected JPanel panel_ = null;
    
    // ==================================================================
    //
    // No constructor.
    //
    // ==================================================================
    
    /**
     * Empty constructor.
     */
    public InterfaceTypePanel() {
        panel_ = new JPanel();
        panel_.setBackground(Color.white);
        //panel_.setLayout(new javax.swing.SpringLayout());
    }
    
    // ==================================================================
    //
    // No internal methods.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // Public methods.
    //
    // ==================================================================
    
    /**
     * Invokes just after instanciation.
     */
    public void selected(TreeView treeView) {
        treeView_ = treeView;
        InterfaceType it = (InterfaceType)treeView.getSelectedObject();
        
        panel_.setBorder(new TitledBorder(null," Interface " + it.getFcItfName() + " ",TitledBorder.CENTER,TitledBorder.TOP));
        
        Box box_ = Box.createVerticalBox();
        box_.add(new LabelBox("Name", it.getFcItfName()));  
        box_.add(new LabelBox("Signature", it.getFcItfSignature()));
        box_.add(new LabelBox("Type", it.isFcClientItf()?"Client":"Server"));
        box_.add(new LabelBox("Cardinality", it.isFcCollectionItf()?"Collection":"Single"));
        box_.add(new LabelBox("Contingency", it.isFcOptionalItf()?"Optional":"Mandatory"));
        panel_.add(box_);
    }
    
    /**
     * Provides the panel to display.
     * @return The panel to display.
     */
    public Object getPanel(){
        return panel_;
    }
    
    /**
     * Invokes just before removing.
     */
    public void unselected(TreeView treeView) {
        // Empty block.
    }
}