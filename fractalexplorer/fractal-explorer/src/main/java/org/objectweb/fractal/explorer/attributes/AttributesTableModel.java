/**
 * Dream
 * Copyright (C) 2003-2004 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Jakub Kornas
 * Contributor(s): 
 */

package org.objectweb.fractal.explorer.attributes;

import javax.swing.table.AbstractTableModel;

/**
 * @see javax.swing.table.AbstractTableModel
 */
public class AttributesTableModel
        extends AbstractTableModel {
    private static final String[] COLUMN_NAMES = new String[] {
            "AttributeName", "AttributeValue", "Monitor" };

    private AttributeDescriptor[] attributes = null;

    /** <code>MONITOR_ACTIONS</code> . */
    public static final String[] MONITOR_ACTIONS = new String[] { "start",
            "stop" };

    /**
     * A default constructor.
     * 
     * @param attributes
     *            a table of attribute descriptors used as a data source for
     *            this model.
     */
    public AttributesTableModel(AttributeDescriptor[] attributes) {
        this.attributes = attributes;
    }

    /**
     * @see AbstractTableModel#getColumnCount()
     */
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    /**
     * @see AbstractTableModel#getRowCount()
     */
    public int getRowCount() {
        return attributes.length;
    }

    /**
     * @see AbstractTableModel#getColumnName(int)
     */
    public String getColumnName(int column) {
        return COLUMN_NAMES[column];
    }

    /**
     * @see AbstractTableModel#getValueAt(int, int)
     */
    public synchronized Object getValueAt(int row, int column) {
        switch (column) {
        case 0:
            return attributes[row].getName();
        case 1:
            return attributes[row].getValue();
        case 2:
            return attributes[row].hasMonitor() ? MONITOR_ACTIONS[1]
                    : MONITOR_ACTIONS[0];
        default:
            return null;
        }
    }

    /**
     * @see AbstractTableModel#getColumnClass(int)
     */
    public synchronized Class getColumnClass(int column) {
        if (column != 1) {
            return getValueAt(0, column).getClass();
        }
        return Object.class;
    }

    /**
     * @see AbstractTableModel#isCellEditable(int, int)
     */
    public boolean isCellEditable(int row, int column) {
        if (column == 0) {
            return false;
        } else if (column == 1) {
            return attributes[row].isMutable() && !attributes[row].hasMonitor();
        }
        return true;
    }

    /**
     * @see AbstractTableModel#setValueAt(Object, int, int)
     */
    public synchronized void setValueAt(Object value, int row, int column) {
        if (column == 1) {
            if (attributes[row] != null) {
                attributes[row].setValue(value);
            } else {
                String attributeName = getValueAt(row, 0).toString();
                attributes[row] = new AttributeDescriptor(attributeName, value,
                        true);
            }
            fireTableCellUpdated(row, column);
        } else if (column == 2) {
            fireTableCellUpdated(row, column);
        }
    }

    /**
     * Returns the table of attribute descriptors that build this attributes
     * table model.
     * 
     * @return attributes a table of attribute descriptors.
     */
    public AttributeDescriptor[] getAttributeDescriptors() {
        return this.attributes;
    }

    /**
     * Returns an attribute descriptor corresponding to a given fow in the
     * attributes table.
     * 
     * @param row
     *            the row of the attributes table.
     * @return an attribute descriptor corresponding to the given row in the
     *         attributes table.
     */
    public AttributeDescriptor getAttributeDescriptor(int row) {
        return attributes[row];
    }
}