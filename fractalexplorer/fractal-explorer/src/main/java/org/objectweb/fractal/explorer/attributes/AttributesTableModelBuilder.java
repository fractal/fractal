/**
 * Dream
 * Copyright (C) 2003-2004 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Jakub Kornas
 * Contributor(s): Philippe Merle
 *                 Christophe Demarey
 *
 * $Id$
 */

package org.objectweb.fractal.explorer.attributes;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.table.TableModel;

import org.objectweb.fractal.api.control.AttributeController;

/**
 * A utility class providing static methods to build attribute table's model and
 * get the name of the attribute controller interface used to control the
 * attributes.
 */
public class AttributesTableModelBuilder
{
  private static String controllerInterface = "";

  /**
   * Builds an attribute table model given an instance of an attribute
   * controller.
   * 
   * @param attributeObject an object implementing the
   *          <code>AttributeController</code> interface.
   * @return dataModel an attribute table model.
   */
  public static TableModel buildAttributesTableModel(
      final Object attributeObject)
  {
    Class attributeClass = attributeObject.getClass();

    // locate the interfaces extending AttributeController
    Object[] attributeControllerInterfaces = getAttributeControllers(attributeClass);

    // extract the attributes from the interfaces extending the
    // AttributeController
    AttributeDescriptor[] attributes = getAttributes(
        attributeControllerInterfaces, attributeClass, attributeObject);

    TableModel dataModel = new AttributesTableModel(attributes);

    return dataModel;
  }

  /*
   * Returns a table of all interfaces extending the AttributeController
   * interface and implemented by the object passed as this method's parameter.
   */
  private static Object[] getAttributeControllers(Class attributeControllerNode)
  {
    Class[] itfsTable = attributeControllerNode.getInterfaces();

    List resultList = null;
    for (int i = 0; i < itfsTable.length; i++)
    {
      resultList = introspectControllerInterfaces(new ArrayList(), itfsTable[i]);
      if (resultList != null)
      {
        break;
      }
    }

    controllerInterface = ((Class) resultList.get(0)).getName();

    return resultList.toArray();
  }

  /*
   * Recursively introspects all the interfaces extended by the itf parameter of
   * this method.
   */
  private static List introspectControllerInterfaces(List ctrlItfs, Class itf)
  {
    Class[] itfsTable = itf.getInterfaces();
    if (itfsTable.length == 0)
    {
      return null;
    }

    if (!ctrlItfs.contains(itf))
    {
      ctrlItfs.add(itf);
    }

    for (int i = 0; i < itfsTable.length; i++)
    {
      if (itfsTable[i].equals(AttributeController.class))
      {
        return ctrlItfs;
      }
      introspectControllerInterfaces(ctrlItfs, itfsTable[i]);
    }
    return ctrlItfs;
  }

  /*
   * Returns a list of attribute descriptors
   */
  private static AttributeDescriptor[] getAttributes(
      Object[] attributeControllerInterfaces, Class attributeClass,
      Object attributeObject)
  {
    // map of component attributes
    Map attributes = new TreeMap();

    // for each interface in the table of attribute controller interfaces
    // get declared attributes and the attributes' values and create appropriate
    // descriptors
    for (int i = 0; i < attributeControllerInterfaces.length; i++)
    {
      Class attribCtrlItf = (Class) attributeControllerInterfaces[i];
      Method[] methods = attribCtrlItf.getDeclaredMethods();
      for (int j = 0; j < methods.length; j++)
      {
        String methodName = methods[j].getName();
        String attributeName = methodName.substring(3);

        if (methodName.startsWith("set"))
        {
          Object attributeValue = null;
          try
          {
            attributeValue = attributeClass.getMethod("get" + attributeName,
                (Class[])null).invoke(attributeObject, (Object[])null);
          }
          catch (NoSuchMethodException e)
          {
            // Getter for this attribute not defined
            attributeValue = "N/A";
          }
          catch (Exception e)
          {
            e.printStackTrace();
          }
          AttributeDescriptor attribute = new AttributeDescriptor(
              attributeName, attributeValue, true);
          attributes.put(attributeName, attribute);
        }
        else if (methodName.startsWith("get")
            && !attributes.containsKey(attributeName))
        {
          Object attributeValue = null;
          try
          {
            attributeValue = attributeClass.getMethod("get" + attributeName,
                (Class[])null).invoke(attributeObject, (Object[])null);
          }
          catch (Exception e)
          {
            e.printStackTrace();
          }
          AttributeDescriptor attribute = new AttributeDescriptor(
              attributeName, attributeValue, false);
          attributes.put(attributeName, attribute);
        }
      }
    }

    AttributeDescriptor[] result = (AttributeDescriptor[]) attributes.values()
        .toArray(new AttributeDescriptor[]{});

    // to help the garbage collector
    attributes = null;

    return result;
  }

  /**
   * Returns a name of the attribute controller interface.
   * 
   * @return controllerInterface the name of the attribute controller interface.
   */
  public static String getControlledInterfaceName()
  {
    return controllerInterface;
  }
}
