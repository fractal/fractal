/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.menu;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.TreeView;

/**
 * This action allows to instantiate a component.
 *
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class InstantiateAction 
  implements MenuItem 
{
    //==================================================================
    //
    // No internal state.
    //
    //==================================================================
    
    //==================================================================
    //
    // No constructor.
    //
    //==================================================================
    
    //==================================================================
    //
    // No internal method.
    //
    //==================================================================
    
    //==================================================================
    //
    // Public methods for MenuItem interface.
    //
    //==================================================================  
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#actionPerformed(org.objectweb.util.explorer.api.MenuItemTreeView)
     */
    public void actionPerformed(MenuItemTreeView e) {
        Factory factory = (Factory)e.getSelectedObject();
        try {
            Component component = factory.newFcInstance();
            NameController nameController = FcExplorer.getNameController(component);
            String name = "new-component";
            if(nameController!=null)
                name = nameController.getFcName();
            e.getTree().addEntry(name,component);            
        } catch (InstantiationException e1) {
            System.out.println("[" + getClass().getName() + "] Exception: " + e1.getMessage());
        } catch (NoSuchInterfaceException e2) {
            System.out.println("[" + getClass().getName() + "] Exception: " + e2.getMessage());
        }
    }        
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#getStatus(org.objectweb.util.explorer.api.TreeView)
     */
    public int getStatus(TreeView treeView) {
        return MenuItem.ENABLED_STATUS;
    }
}