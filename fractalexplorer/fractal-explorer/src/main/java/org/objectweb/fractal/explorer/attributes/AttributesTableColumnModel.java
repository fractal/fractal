/**
 * Dream
 * Copyright (C) 2003-2004 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Jakub Kornas
 * Contributor(s): 
 */

package org.objectweb.fractal.explorer.attributes;

import javax.swing.table.DefaultTableColumnModel;

/**
 * An extended <code>DefaultTableColumnModel</code> with overriden
 * <code>moveColumn()</code> method. This table column model does not allow
 * moving of table columns in the table to which is belongs.
 */
public class AttributesTableColumnModel
        extends DefaultTableColumnModel {
    /**
     * 
     */
    public AttributesTableColumnModel() {
        super();
    }

    /**
     * @see DefaultTableColumnModel#moveColumn(int, int)
     */
    public void moveColumn(int index, int newIndex) {
        // empty body.
    }
}