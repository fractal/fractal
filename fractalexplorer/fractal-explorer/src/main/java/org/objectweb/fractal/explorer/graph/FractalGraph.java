/*====================================================================

ObjectWeb Fractal Explorer
Copyright (C) 2000-2005 INRIA - USTL - LIFL - GOAL
Contact: fractal@objectweb.org

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Initial developer(s): Alexandre Vandekerkhove.
Contributor(s): Simon Leli�vre, Philippe Merle.

====================================================================
$Id$
====================================================================*/

package org.objectweb.fractal.explorer.graph;

import java.awt.Color;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jgraph.graph.DefaultGraphModel;
import org.jgraph.graph.GraphModel;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.util.explorer.swing.graph.CompositeVertex;
import org.objectweb.util.explorer.swing.graph.Graph;
import org.objectweb.util.explorer.swing.graph.MyPort;
import org.objectweb.util.explorer.swing.graph.PortType;
import org.objectweb.util.explorer.swing.graph.PrimitiveVertex;

/**
 * Utilities class for graphic representation
 * @author Alexandre Vandekerkhove
 * @version September 2004
 */
public class FractalGraph
{   
    /**
     * Empty constructor
     */
    public FractalGraph(){
    }
    
    /**
     * calls the graph method to represent all the ports on a composite component (around the border)
     * @param component the composite component to represent
     * @param composite the (CompositeVertex) associated
     */
    protected void addPortsComposite (Component component, CompositeVertex composite){
        Interface[] listExtItf = GraphInformations.getSortExtItf(component);
        
        for (int i=0 ; i<listExtItf.length ; i++){
            String nameItf = GraphInformations.getInterfaceName(listExtItf[i]);
            String typePort = GraphInformations.getPortType(listExtItf[i]);
            //don't draw the collection ports because we'll draw only one port per collection
            if (!typePort.equals(PortType.COLLECTION_PORT)) 
                composite.addCompositePort(listExtItf[i], nameItf, typePort);
        }
        
        //draws the collection ports
        List listItfCollection = GraphInformations.getItfCollection(component);
        if (listItfCollection.size()!=0){
            InterfaceType itType;
        	for (int j=0 ; j<listItfCollection.size() ; j++){
                itType = (InterfaceType)listItfCollection.get(j);
        		composite.addCompositePort(listItfCollection.get(j), itType.getFcItfName() , PortType.COLLECTION_PORT);
            }
        }
    }
    
    /**
     * calls the graph method to represent all the ports on a primitive component
     * represented without its super-component
     * @param component the primitive component to represent
     * @param primitive the (PrimitiveVertex) associated
     */
    protected void addPortsPrimitive (Component component, PrimitiveVertex primitive){
    	Interface[] listExtItf = GraphInformations.getSortExtItf(component);
        for (int i=0 ; i<listExtItf.length ; i++){
            String nameItf = GraphInformations.getInterfaceName(listExtItf[i]);
            String typePort = GraphInformations.getPortType(listExtItf[i]);
            //don't add the collection ports beacause we'll draw only one port per collection
            if (!typePort.equals(PortType.COLLECTION_PORT)) primitive.addPort(listExtItf[i], nameItf, typePort);
        }
        
        //draws the collection ports
        List listItfCollection = GraphInformations.getItfCollection(component);
        if (listItfCollection.size()!=0){
            InterfaceType itType;
            for (int j=0 ; j<listItfCollection.size() ; j++){
                itType = (InterfaceType)listItfCollection.get(j);
                primitive.addPort(listItfCollection.get(j), itType.getFcItfName() , PortType.COLLECTION_PORT);
            }
        }
    }
    
    /**
     * This method draws the neigbors of a primitive component selected by the user
     * neighbor = a component which is directly bound to the primitive component selected
     * @param component the primitive component selected
     * @param primitiveVertex the (PrimitiveVertex) associated
     * @param graph the graph in which the components are represented
     * @param controllersDisplay true : allows the display of the control interfaces
     */
    protected void addNeighbors (Component component, PrimitiveVertex primitiveVertex, Graph graph, boolean controllersDisplay){
        PrimitiveVertex neighborVertex;
        List serverNeighbors = GraphInformations.getServerNeighbors(component);
        List clientNeighbors = GraphInformations.getClientNeighbors(component);
        Component neighborComponent;
        Interface[] listExtItf;
        Interface targetItf;
        Component targetComponent;
        String namePort;
        boolean isOpen = false;  //we don't look inside the neighbor composite components
        boolean isCompositeEdge = false; //binds between sub-components
        
        //first we add the server neighbors
        for (int i=0 ; i<serverNeighbors.size() ; i++){
        	neighborComponent = (Component)serverNeighbors.get(i);
            if (GraphInformations.isPrimitiveComponent(neighborComponent)){
                neighborVertex = new PrimitiveVertex(neighborComponent, FcExplorer.getName(neighborComponent),
                        graph, new Dimension(), controllersDisplay, 
                        GraphInformations.isStarted(neighborComponent), 
                        GraphInformations.isSharedComponent(neighborComponent));}
            else { neighborVertex = new CompositeVertex(neighborComponent, FcExplorer.getName(neighborComponent),
                        graph, new Dimension(), isOpen, controllersDisplay, 
                        GraphInformations.isStarted(neighborComponent), 
                        GraphInformations.isSharedComponent(neighborComponent));}
            graph.addPrimitiveComponent(neighborVertex);
            addPortsPrimitive(neighborComponent, neighborVertex);
            listExtItf = GraphInformations.getSortExtItf(neighborComponent);
            for(int j=0 ; j<listExtItf.length ; j++){
            	if(GraphInformations.isClientInterface(listExtItf[j])){
                    targetComponent = GraphInformations.getTargetComponent(neighborComponent, listExtItf[j]);
                    if((targetComponent != null) && (targetComponent.equals(component))){
                        targetItf = GraphInformations.getTargetInterface(neighborComponent, listExtItf[j]);
                        if(GraphInformations.isCollectionInterface(listExtItf[j])){
                            InterfaceType itType = (InterfaceType)listExtItf[j].getFcItfType();
                            namePort = itType.getFcItfName();
                        }
                        else namePort = GraphInformations.getInterfaceName(listExtItf[j]);
                        graph.primitiveConnect(neighborVertex.getPort(namePort),
                                primitiveVertex.getPort(GraphInformations.getInterfaceName(targetItf)), isCompositeEdge);
                    }
                }
            }
        }
        
        //we can now add the client neighbors
        for (int i=0 ; i<clientNeighbors.size() ; i++){
        	neighborComponent = (Component)clientNeighbors.get(i);
            if (GraphInformations.isPrimitiveComponent(neighborComponent)){
                neighborVertex = new PrimitiveVertex(neighborComponent, FcExplorer.getName(neighborComponent),
                        graph, new Dimension(), controllersDisplay, 
                        GraphInformations.isStarted(neighborComponent), 
                        GraphInformations.isSharedComponent(neighborComponent));}
            else { neighborVertex = new CompositeVertex(neighborComponent, FcExplorer.getName(neighborComponent),
                        graph, new Dimension(), isOpen, controllersDisplay, 
                        GraphInformations.isStarted(neighborComponent), 
                        GraphInformations.isSharedComponent(neighborComponent));}
            graph.addPrimitiveComponent(neighborVertex);
            addPortsPrimitive(neighborComponent, neighborVertex);
        
            listExtItf = GraphInformations.getSortExtItf(component);
            for(int j=0 ; j<listExtItf.length ; j++){
            	if(GraphInformations.isClientInterface(listExtItf[j])){
                    targetComponent = GraphInformations.getTargetComponent(component, listExtItf[j]);
                    if ((targetComponent != null) && (targetComponent.equals(neighborComponent))){
                        targetItf = GraphInformations.getTargetInterface(component, listExtItf[j]);
                        if(GraphInformations.isCollectionInterface(listExtItf[j])){
                            InterfaceType itType = (InterfaceType)listExtItf[j].getFcItfType();
                            namePort = itType.getFcItfName();
                        }
                        else namePort = GraphInformations.getInterfaceName(listExtItf[j]);
                        graph.primitiveConnect(primitiveVertex.getPort(namePort),
                                neighborVertex.getPort(GraphInformations.getInterfaceName(targetItf)),
                                isCompositeEdge);
                    }
                }
            }
        }
    }
    
    /**
     * adds all the ports of a sub-component of a composite component
     * @param composite the (CompositeVertex) associated to the composite component
     * @param subComponent a sub-component
     */
    protected void addSubComponentPorts (CompositeVertex composite, Component subComponent){
    	Interface[] listExtItf = GraphInformations.getSortExtItf(subComponent);
        String portName, typePort;
        for (int i=0 ; i<listExtItf.length ; i++){
            portName = GraphInformations.getInterfaceName(listExtItf[i]);
            typePort = GraphInformations.getPortType(listExtItf[i]);
            if (!typePort.equals(PortType.COLLECTION_PORT)) 
                composite.addSubComponentPort(FcExplorer.getName(subComponent), subComponent, portName, typePort);
        }
        
        //draws the collection ports
        List listItfCollection = GraphInformations.getItfCollection(subComponent);
        if (listItfCollection.size()!=0){
            InterfaceType itType;
        	for (int j=0 ; j<listItfCollection.size() ; j++){
                itType = (InterfaceType)listItfCollection.get(j);
        		composite.addSubComponentPort(FcExplorer.getName(subComponent), subComponent, itType.getFcItfName(), PortType.COLLECTION_PORT);
            }
        }
    }
    
    /**
     * adds all the sub-components of a composite component with their ports
     * @param graph the graph in which the sub-components will be represented
     * @param superComponent the composite component to represent
     * @param composite the (CompositeVertex) associated
     */
    protected void addSubComponents (Graph graph, Component superComponent, CompositeVertex composite){
        Component[] listSubComponents = GraphInformations.getSubComponents(superComponent);
        Component subComponent;
        String nameSubComponent = "";
        for (int i=0 ; i<listSubComponents.length ; i++){
            subComponent = listSubComponents[i];
            composite.addVertex(subComponent, FcExplorer.getName(subComponent), graph, 
                    GraphInformations.getComponentType(subComponent), false, true,
                    GraphInformations.isStarted(subComponent), GraphInformations.isSharedComponent(subComponent));
            addSubComponentPorts(composite, subComponent);
        }
    }

    /** 
     * Method to draw a bind and make the connection between 2 interfaces
     * @param graph the graph in which the bind will be drawn
     * @param component the client component
     * @param namePort the name of the client interface
     * @param targetComponent the server component
     * @param nameTargetPort the name of the server interface
     * @param composite the (CompositeVertex) which represents the super-component of the 2 components bound
     * @param isInternalEdge true if the bind concerns an internal interface of the super-component
     */
    protected void drawBind(Graph graph, Object component, String namePort, Object targetComponent,
            String nameTargetPort, CompositeVertex composite, boolean isInternalEdge) {
        Object sourcePort;
        if(!isInternalEdge)
            sourcePort = composite.getVertex(component).getPort(namePort);
        else
            sourcePort = composite.getGeneratedPort(namePort);
        Object targetPort = composite.getVertex(targetComponent).getPort(nameTargetPort);
        graph.compositeConnect((MyPort)sourcePort, (MyPort)targetPort, isInternalEdge, composite);
    }
 
    /**
     * calls the graph method to represent all the binds between the sub-components of a composite component
     * and the binds between the sub-components client interfaces and the server interfaces of the composite component
     * @param graph the graph in which the binds will be represented
     * @param superComponent the composite component to represent
     * @param composite the (CompositeVertex) associated
     */
    protected void addBinds (Graph graph, Component superComponent, CompositeVertex composite){
        
        Interface[] listExtItf;
        Interface itf;
        Component subComponent;
        Component[] listSubComponents = GraphInformations.getSubComponents(superComponent);
        String namePort, nameTargetPort;
        
        for (int i=0 ; i<listSubComponents.length ; i++){
            subComponent = listSubComponents[i];
            listExtItf = GraphInformations.getSortExtItf(subComponent);
            
            for (int j=0 ; j<listExtItf.length ; j++){
                itf = listExtItf[j];
                if ((GraphInformations.isClientInterface(itf))
                        && (GraphInformations.getTargetComponent(subComponent, itf)!=null)
                        && (GraphInformations.getTargetInterface(subComponent, itf)!=null)){
                    
                    	if (GraphInformations.isCollectionInterface(itf)){
                            InterfaceType itType = (InterfaceType)itf.getFcItfType();
                            namePort = itType.getFcItfName();
                        }
                        
                        else namePort = GraphInformations.getInterfaceName(itf);
                        Object targetComponent =GraphInformations.getTargetComponent(subComponent, itf);
                        nameTargetPort = GraphInformations.getInterfaceName(GraphInformations.getTargetInterface(subComponent, itf));
                        
                        //if the target interface is internal : it's an internal interface of the composite component
                        //so it's an internal edge
                        if ((GraphInformations.getTargetInterface(subComponent, itf).isFcInternalItf())
                            && superComponent.equals(targetComponent)) {
                           drawBind(graph, superComponent, nameTargetPort, 
                                   subComponent, namePort, composite, true);
                        }
                        else if (!GraphInformations.getTargetInterface(subComponent, itf).isFcInternalItf()){ 
                            drawBind(graph, subComponent, namePort, 
                                      targetComponent, nameTargetPort, composite, false);
                        }
                    }
                }
            }
        }
 
    /**
     * calls the graph method in order to represent the internal edges of a composite component
     * (edges between the internal client interfaces of the composite component and the external interfaces of the sub-components)
     * @param graph the graph in which the binds will be represented
     * @param superComponent the composite component to draw
     * @param composite the (CompositeVertex) associated
     */
    protected void addInternalBinds (Graph graph, Component superComponent, CompositeVertex composite){
        
        Interface internalItf;
        Interface[] listIntItf = GraphInformations.getIntItf(superComponent);
        String nameInternalItf, nameTargetPort;
        
        for (int i=0 ; i<listIntItf.length ; i++){
           internalItf = listIntItf[i];
           
           //the internal server interfaces are binded in the method "addBinds"
           if (((GraphInformations.getPortType(internalItf))!=PortType.SERVER_PORT)
               && (GraphInformations.getTargetComponent(superComponent, internalItf)!=null)
               && (GraphInformations.getTargetInterface(superComponent, internalItf)!=null)){
            
            nameInternalItf = GraphInformations.getInterfaceName(internalItf);
            nameTargetPort = GraphInformations.getInterfaceName(GraphInformations.getTargetInterface(superComponent, internalItf));
            Object targetComponent = GraphInformations.getTargetComponent(superComponent, internalItf);
            
            drawBind(graph, superComponent, nameInternalItf, 
                    targetComponent, nameTargetPort, composite, true);
            
           }
        }
    }
    
    /**
     * method to construct a graph which represents a component selected by the user
     * If the component is a composite component, draws its sub-components with the interfaces and the binds
     * @param component the component selected by the user
     * @return the (Graph) associated
     */
    public Graph getFractalGraph (Component component){
        String nameComponent = FcExplorer.getName(component);
        PortGraphics pg = new PortGraphics();
        VertexGraphics vg = new VertexGraphics();
        
        //*********************Dimensions to modify************************
        Dimension frameSize = new Dimension(100, 100); //dimension of the border
     
        GraphModel model = new DefaultGraphModel();
        Graph graph = new Graph(model, pg, vg);
        
        graph.setPortsVisible(true);

        /*
        graph.setGridEnabled(true);
        graph.setGridColor(Color.LIGHT_GRAY);
        graph.setGridSize(2);
        graph.setGridVisible(true);
        */

        boolean controllersDisplay = true; //allows the display of the control interfaces
        boolean isPrimitive = GraphInformations.isPrimitiveComponent(component);
        
        if(!isPrimitive){
            //we have a composite component
            boolean isOpen = true;             //allows the display of the sub-components of the composite component
            CompositeVertex compositeVertex = new CompositeVertex(component,
                nameComponent, graph, frameSize, isOpen, controllersDisplay, GraphInformations.isStarted(component),
                GraphInformations.isSharedComponent(component));
            
            graph.addSuperComponent(compositeVertex);
            
            //draws the ports of the composite
            addPortsComposite(component, compositeVertex);
            //draws the sub-components with their ports
            addSubComponents(graph, component, compositeVertex);
            //draws the binds
            addBinds(graph, component, compositeVertex);
            addInternalBinds (graph, component, compositeVertex);
            
            // Insert into Model
            graph.insertIntoModel(isPrimitive);
            graph.applyLayout(compositeVertex);
            
            return graph;
        } else {
            //we have a primitive component
            
            PrimitiveVertex primitiveVertex = new PrimitiveVertex(component, nameComponent, graph, new Dimension(), controllersDisplay,
                    GraphInformations.isStarted(component), GraphInformations.isSharedComponent(component));
            graph.addPrimitiveComponent(primitiveVertex);
         
            //draws the ports
            addPortsPrimitive(component, primitiveVertex);
            //draws the neighbors and the binds between the neighbors and the primitive component
            addNeighbors (component, primitiveVertex, graph, controllersDisplay);
            
             // Insert into Model
             graph.insertIntoModel(isPrimitive);
             graph.applyLayout(frameSize);          
                                       
             return graph;
        }
    }
    
}
