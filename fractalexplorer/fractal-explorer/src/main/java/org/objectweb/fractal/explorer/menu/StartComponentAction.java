/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.menu;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.TreeView;

/**
 * This action allows to start/stop the component. 
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class StartComponentAction 
  implements MenuItem 
{
    
    //==================================================================
    //
    // No internal state.
    //
    //==================================================================
    
    //==================================================================
    //
    // No constructor.
    // 
    //==================================================================
    
    //==================================================================
    //
    // No internal method.
    //
    //==================================================================
    
    //==================================================================
    //
    // Public methods for ExtendedActionListener interface.
    //
    //==================================================================
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#actionPerformed(org.objectweb.util.explorer.api.MenuItemTreeView)
     */
    public void actionPerformed(MenuItemTreeView e) {
        Component ci = (Component)e.getSelectedObject();
        try {
            LifeCycleController lcc = FcExplorer.getLifeCycleController(ci);
            lcc.startFc();
        } catch (NoSuchInterfaceException e1) {
            e1.printStackTrace();
        } catch (IllegalLifeCycleException e1) {
            e1.printStackTrace();
        }
    }
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#getStatus(org.objectweb.util.explorer.api.TreeView)
     */
    public int getStatus(TreeView treeView) {
        Component ci = (Component)treeView.getSelectedObject();
        try {
            LifeCycleController lcc = FcExplorer.getLifeCycleController(ci);
            String status = lcc.getFcState();
            if (status.equals(LifeCycleController.STARTED))
                return MenuItem.NOT_VISIBLE_STATUS;
        } catch (NoSuchInterfaceException e) {
            return MenuItem.NOT_VISIBLE_STATUS;
        }
        return MenuItem.ENABLED_STATUS;   
    }
}