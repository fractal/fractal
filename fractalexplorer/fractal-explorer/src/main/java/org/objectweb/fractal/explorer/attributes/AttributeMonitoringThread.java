/**
 * Dream
 * Copyright (C) 2003-2004 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Jakub Kornas
 * Contributor(s): Philippe Merle
 *
 * $Id$
 */

package org.objectweb.fractal.explorer.attributes;

import javax.swing.JTable;

/**
 * A <code>Thread</code> responsible for periodically testing the value of the
 * monitored attribute.
 */
public class AttributeMonitoringThread extends Thread
{
  int            monitoringPeriod;
  Object         monitoredObject;
  String         monitoredAttribute;
  JTable         table;
  int            row;
  /** <code>running</code> . */
public boolean running = false;

  /**
   * Creates a new attribute monitoring thread.
   * 
   * @param monitoringPeriod the period of testing the attribute's value.
   * @param attributeController the object providing access to the monitored
   *          attribute's value.
   * @param monitoredAttribute the name of the attribute to be monitored.
   * @param table a <code>JTable</code> that should be updated when the
   *          monitored attribute's value changes.
   * @param row a row of the table which stores the attribute monitored by this
   *          thread.
   */
  public AttributeMonitoringThread(int monitoringPeriod,
      Object attributeController, String monitoredAttribute, JTable table,
      int row)
  {
    this.monitoringPeriod = monitoringPeriod;
    this.monitoredAttribute = monitoredAttribute;
    this.table = table;
    this.monitoredObject = attributeController;
    this.row = row;
  }

  /**
   * @see java.lang.Thread#run()
   */
  public void run()
  {
    if (monitoredObject == null)
    {
      return;
    }

    Class monitoredClass = monitoredObject.getClass();
    String methodName = "get" + monitoredAttribute;
    Object newValue = null;

    running = true;
    while (running)
    {
      try
      {
        newValue = monitoredClass.getMethod(methodName, (Class[])null).invoke(
            monitoredObject, (Object[])null);
        table.setValueAt(newValue, row, 1);
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }

      try
      {
        Thread.sleep(monitoringPeriod);
      }
      catch (InterruptedException e)
      {
        e.printStackTrace();
      }
    }
  }
}
