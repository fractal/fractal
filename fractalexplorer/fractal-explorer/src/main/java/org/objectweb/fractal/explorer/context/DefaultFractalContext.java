/*====================================================================
 
 Objectweb Fractal Explorer
 Copyright (C) 2000-2005 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.context;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.fractal.explorer.api.FractalContext;
import org.objectweb.util.explorer.api.Context;
import org.objectweb.util.explorer.api.Entry;

/**
 * Basic implementation of a Fractal context.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.2
 */
public abstract class DefaultFractalContext 
           implements Context, 
                      FractalContext 
{
    
    //==================================================================
    //
    // No internal state.
    //
    //==================================================================
    
    protected Object object_ = null;
    
    //==================================================================
    //
    // No constructor.
    // 
    //==================================================================
    
    //==================================================================
    //
    // Internal methods.
    //
    //==================================================================
    
    //==================================================================
    //
    // Public methods for Context interface.
    //
    //==================================================================
    
    /**
     * Overriding methods
     * @see org.objectweb.util.explorer.api.Context#getEntries(Object)
     */
    public Entry[] getEntries(Object object) {
        object_ = object;
        LifeCycleController lcc = null;
        try {
             lcc = FcExplorer.getLifeCycleController(FcExplorer.getComponent(object));
        } catch (NoSuchInterfaceException e2) {
            try {
                return getStartedEntries();
            }  catch (Exception e1) {
                return getStoppedEntries();
            }
        }
        if (lcc.getFcState().equals(LifeCycleController.STARTED))
            return getStartedEntries();
        return getStoppedEntries();
    }
    
    //==================================================================
    //
    // Public methods for FractalContext interface.
    //
    //==================================================================
    
    /**
     * Called when the Fractal component is started.
     * @return An array of entries contained into the Context.
     */
    public abstract Entry[] getStartedEntries() ;
    
    /**
     * Called when the Fractal component is stopped.
     * @return An array of entries contained into the Context.
     */
    public Entry[] getStoppedEntries() {
        return new Entry[0];
    }
    
}