/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.menu;

import java.awt.Dimension;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.fractal.explorer.context.ClientInterfaceWrapper;
import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.swing.gui.api.DialogAction;
import org.objectweb.util.explorer.swing.gui.api.DialogBox;
import org.objectweb.util.explorer.swing.gui.lib.DefaultDialogBox;
import org.objectweb.util.explorer.swing.gui.lib.TreeBox;

/**
 * This action allows to bind an client interface to a server one.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class BindAction 
  implements MenuItem, 
             DialogAction 
{
    //==================================================================
    //
    // Internal states.
    //
    //==================================================================
    
    /** The TreeBox used. */
    protected TreeBox treeBox_;
    
    /** The InterfaceReference on which the interface will be bound. */
    protected Interface clientInterface_;
    
    //==================================================================
    //
    // No constructor.
    //
    //==================================================================
    
    //==================================================================
    //
    // No Internal method.
    //
    //==================================================================
    
    //==================================================================
    //
    // Public methods for MenuItem interface.
    //
    //==================================================================
    
    /**
     * @see org.objectweb.util.explorer.api.MenuItem#getStatus(org.objectweb.util.explorer.api.TreeView)
     */
    public int getStatus(TreeView treeView) {
        try {
            boolean started = false;
            ClientInterfaceWrapper cirw = (ClientInterfaceWrapper) treeView.getSelectedObject();
            Interface ir = cirw.getItf();
            Component component = ir.getFcItfOwner();
            // Test if the component is started
            LifeCycleController lcc = FcExplorer.getLifeCycleController(component);        
            String status = lcc.getFcState();
            if (status.equals(LifeCycleController.STARTED))
                started = true;
            // Test if the component is bound
            Interface bindInterface = null;
            try{
                BindingController bc = FcExplorer.getBindingController(component);
                bindInterface = (Interface)bc.lookupFc(ir.getFcItfName());
            }catch(Exception e) {
                //System.err.println("Error : " + e.getMessage()); 
                return MenuItem.NOT_VISIBLE_STATUS;
            }
            if(bindInterface == null && !started){
                // Not bound and not started
                return MenuItem.ENABLED_STATUS;
            } else if (bindInterface == null && started) {
                // Not bound but started
                return MenuItem.DISABLED_STATUS;
            } else if (bindInterface != null){
                // Already bound
                return MenuItem.NOT_VISIBLE_STATUS;
            }
        } catch (NoSuchInterfaceException e) {
            e.printStackTrace();
        }
        return MenuItem.DISABLED_STATUS;
    }
    
    /**
     * @see org.objectweb.util.explorer.api.MenuItem#actionPerformed(org.objectweb.util.explorer.api.MenuItemTreeView)
     */
    public void actionPerformed(MenuItemTreeView e) throws Exception{
        ClientInterfaceWrapper clientWrapper = (ClientInterfaceWrapper)e.getSelectedObject();
        clientInterface_ = clientWrapper.getItf();               
        
        treeBox_ = new TreeBox(e.getTree().duplicate());
        treeBox_.setPreferredSize(new Dimension(450, 350));
        
        DialogBox dialog = new DefaultDialogBox("Select the interface to bind");
        dialog.setValidateAction(this);
        dialog.addElementBox(treeBox_);
        dialog.show();        
    }
    
    //==================================================================
    //
    // Public methods for DialogAction interface.
    //
    //==================================================================
    
    /**
     * @see org.objectweb.util.explorer.swing.gui.api.DialogAction#executeAction()
     */
    public void executeAction() throws Exception {
        Object o = treeBox_.getObject();
        Interface serverInterface = (Interface)o;
        FcExplorer.getBindingController(clientInterface_.getFcItfOwner()).bindFc(clientInterface_.getFcItfName(),serverInterface);
    } 
    
}
