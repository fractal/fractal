/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Romain Rouvoy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer;

import java.net.URL;

import javax.swing.ImageIcon;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.control.SuperController;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;

/**
 * Utilities class for Fractal.
 * @author <a href="mailto:Philippe.Merle@lifl.fr">Philippe Merle</a>,
 *         <a href="mailto:Jerome.Moroy@lifl.fr">J�r�me Moroy</a>
 * @version 0.1
 */
public class FcExplorer 
{
    /**
     * Provides the interface of the primitive component to which the itt is binded.
     * @param itf the indirect interface.
     * @return the primitive interface.
     */
    public static Interface getPrimitiveItf(final Interface itf) {
        Component cpt = itf.getFcItfOwner();
        try {
            BindingController bc = getBindingController(cpt);
            Interface internal = (Interface) bc.lookupFc(itf.getFcItfName());
            return getPrimitiveItf(internal);
        } catch (NoSuchInterfaceException ex) {
            return itf;
        }
    }
    
    /**
     * Provides the primitive component that own the interface itf. 
     * @param itf the indirect interface.
     * @return the primitive component.
     */
    public static Component getPrimitiveCpt(final Interface itf) {
        return getPrimitiveItf(itf).getFcItfOwner();
    }
    
    /**
     * Provides the value of the life cycle state associated to the interface.
     * @param itf the interface to introspect.
     * @return the state of the interface ("STARTED" if started, "STOPPED" else).
     */
    public static String getLifeCycleState(final Interface itf) {
        try {
            LifeCycleController lc = getLifeCycleController(getPrimitiveCpt(itf));
            return lc.getFcState();
        } catch (NoSuchInterfaceException ex) {
            return LifeCycleController.STARTED ;
        }
    }  
    
    /**
     * Provides the reference to the component associated to this object.
     * @param obj the base object reference.
     * @return the associated component.
     */
    public static Component getComponent(final Object obj) {
        if (obj instanceof Component)
            return (Component)obj;
        return ((Interface)obj).getFcItfOwner(); 
    }
    
    /**
     * Provides the name of the object prefixed by the component name.
     * @param obj the object to identify.
     * @return the associated name.
     */
    public static String getPrefixedName(final Object obj) {
        if (obj instanceof Component)
            return getName((Component)obj);
        return getName(getComponent(obj))+"."+getName(obj);
    }
    
    /**
     * Provides the name of an object.
     * @param obj the object to identify.
     * @return the associated name ('unknown' if no name).
     */
    public static String getName(final Object obj) {
        if (obj instanceof Component)
            return getName((Component)obj);
        else if (obj instanceof Interface)
            return getName((Interface)obj);
        else
            return "unknown" ;
    }
    
    /**
     * @param itf the interface to introspect.
     * @return the name of the interface.
     */
    public static String getName(final InterfaceType itf) {
        return itf.getFcItfName();
    }
    
    /**
     * Provides the name of an interface.
     * @param itf the interface to identify.
     * @return the name of the interface.
     */
    public static String getName(final Interface itf) {
        return itf.getFcItfName();
    }
    
    /**
     * Provides the name of a component.
     * @param cpt the component to identify.
     * @return the name of the component (the name of the associated 
     * interface if no NameController), or "unkwnown".
     */
    public static String getName(final Component cpt) {
        try {
            NameController nc =  getNameController(cpt);
            if (nc != null) {
              String name = nc.getFcName();
              if (name != null) {
                return nc.getFcName();
                } else {
                	return "unknown";
                }
            }
        } catch (NoSuchInterfaceException e) { 
            // Nothing to do.
        }
        if (cpt instanceof Interface)
            return getName((Interface)cpt);
        return "unknown" ;
    }
    
    
    /**
     * Returns a bootstrap component to create other components. This method
     * just calls the corresponding method of the
     * <tt>org.objectweb.fractal.api.Fractal</tt> class.
     *
     * @return a bootstrap component to create other components.
     * @throws InstantiationException if the bootstrap component cannot be
     *      created.
     */
    public static Component getBootstrapComponent ()
        throws InstantiationException
    {
        try {
            return org.objectweb.fractal.api.Fractal.getBootstrapComponent();
        } catch(NullPointerException ex) {
            throw new InstantiationException("client interface not bound");
        }
    }

    /**
     * Returns the {@link AttributeController} interface of the given component.
     *
     * @param component a component.
     * @return the {@link AttributeController} interface of�the given component.
     * @throws NoSuchInterfaceException if there is no such interface.
     */
    public static AttributeController getAttributeController (final Component component) 
        throws NoSuchInterfaceException
    {
        try {
        return (AttributeController)component.getFcInterface("attribute-controller");
        } catch(NullPointerException ex) {
            throw new NoSuchInterfaceException("client interface not bound");
        }
    }

    /**
     * Returns the {@link BindingController} interface of the given component.
     *
     * @param component a component.
     * @return the {@link BindingController} interface of the given component.
     * @throws NoSuchInterfaceException if there is no such interface.
     */
    public static BindingController getBindingController (final Component component) 
        throws NoSuchInterfaceException
    {
        try {
            return (BindingController)component.getFcInterface("binding-controller");
        } catch(NullPointerException ex){
            throw new NoSuchInterfaceException("client interface not bound");
        }
    }

    /**
     * Returns the {@link ContentController} interface of the given component.
     *
     * @param component a component.
     * @return the {@link ContentController} interface of the given component.
     * @throws NoSuchInterfaceException if there is no such interface.
     */
    public static ContentController getContentController (final Component component) 
        throws NoSuchInterfaceException
    {
        try {
            return (ContentController)component.getFcInterface("content-controller");
        } catch(NullPointerException ex){
            throw new NoSuchInterfaceException("client interface not bound");
        }
    }

    /**
     * Returns the {@link SuperController} interface of the given component.
     *
     * @param component a component.
     * @return the {@link SuperController} interface of the given component.
     * @throws NoSuchInterfaceException if there is no such interface.
     */
    public static SuperController getSuperController (final Component component)
        throws NoSuchInterfaceException
    {
        try {
            return (SuperController)component.getFcInterface("super-controller");
        } catch(NullPointerException ex){
            throw new NoSuchInterfaceException("client interface not bound");
        }
    }

    /**
     * Returns the {@link NameController} interface of the given component.
     *
     * @param component a component.
     * @return the {@link NameController} interface of the given component.
     * @throws NoSuchInterfaceException if there is no such interface.
     */
    public static NameController getNameController (final Component component)
        throws NoSuchInterfaceException
    {
        try {
          return (NameController)component.getFcInterface("name-controller");
        } catch(NullPointerException ex){
          throw new NoSuchInterfaceException("client interface not bound");
        }
    }

    /**
     * Returns the {@link LifeCycleController} interface of the given component.
     *
     * @param component a component.
     * @return the {@link LifeCycleController} interface of the given component.
     * @throws NoSuchInterfaceException if there is no such interface.
     */
    public static LifeCycleController getLifeCycleController (
        final Component component) throws NoSuchInterfaceException
    {
        try {
            return (LifeCycleController)component.getFcInterface("lifecycle-controller");
        } catch(NullPointerException ex){
            throw new NoSuchInterfaceException("client interface not bound");
        }
    }

    /**
     * Returns the {@link Factory} interface of the given component.
     *
     * @param component a component.
     * @return the {@link Factory} interface of the given component.
     * @throws NoSuchInterfaceException if there is no such interface.
     */
    public static Factory getFactory (final Component component)
        throws NoSuchInterfaceException
    {
        try {
            return (Factory)component.getFcInterface("factory");
        } catch(NullPointerException ex){
            throw new NoSuchInterfaceException("client interface not bound");
        }
    }

    /**
     * Returns the {@link GenericFactory} interface of the given component.
     *
     * @param component a component.
     * @return the {@link GenericFactory} interface of the given component.
     * @throws NoSuchInterfaceException if there is no such interface.
     */
    public static GenericFactory getGenericFactory (final Component component)
        throws NoSuchInterfaceException
    {
        try {
            return (GenericFactory)component.getFcInterface("generic-factory");
        } catch(NullPointerException ex) {
            throw new NoSuchInterfaceException("client interface not bound");
        }
    }

    /**
     * Returns the {@link TypeFactory} interface of the given component.
     *
     * @param component a component.
     * @return the {@link TypeFactory} interface of the given component.
     * @throws NoSuchInterfaceException if there is no such interface.
     */
    public static TypeFactory getTypeFactory (final Component component)
        throws NoSuchInterfaceException
    {
        try {
            return (TypeFactory)component.getFcInterface("type-factory");
        }catch(NullPointerException ex){
          throw new NoSuchInterfaceException("client interface not bound");
        }
    }
    
    /**
     * Return true if the given Interface is a Client interface.
     * @param ir the interface.
     * @return true if interface is client type.
     */
    public static boolean isClient(Interface ir) {
        InterfaceType it = (InterfaceType) ir.getFcItfType();
        return it.isFcClientItf();
    }
    
    /**
     * Return true if the given Interface is a Collection interface.
     * @param itf the interface to check.
     * @return true if interface is collection type.
     */
    public static boolean isCollection(Interface itf) {
        InterfaceType iType = (InterfaceType) itf.getFcItfType();
        return iType.isFcCollectionItf();
    }
    
    /**
     * Return true if the given InterfaceType is a Collection Client interface.
     * @param it 
     * @return true if the interface is client and collection.
     */
    public static boolean isClientCollection(InterfaceType it) {
        return (it.isFcClientItf() && it.isFcCollectionItf());
    }
    
    /**
     * Return true if the given Interface is a Collection Client interface.
     * @param ir 
     * @return true if the interface is client and collection.
     */
    public static  boolean isClientCollection(Interface ir) {
        return isClientCollection((InterfaceType)ir.getFcItfType());
    }
    
    /**
     * Return true if the given Interface is a Control interface.
     * @param itf 
     * @return true if the name of the given interface ends with <code>-controller</code> 
     * or if it is equal to <code>factory</code> or if it is equal to <code>component</code> 
     */
    public static  boolean isController(Interface itf) {
        String itfName = itf.getFcItfName();
        return (itfName.endsWith("-controller") || itfName.equals("factory") || itfName.equals("component"));
    }

    /** Returns an ImageIcon, or null if the path is invalid. 
     * @param imageName the icon name.
     * @return the image icon.
     */
    public static ImageIcon createIcon(String imageName) {
        if (imageName != null) {
            URL urlFile = null;
            urlFile = Thread.currentThread().getContextClassLoader().getResource(imageName);            
            if(urlFile==null){            
                try {
                    urlFile = new URL(imageName);
                } catch (java.net.MalformedURLException e) {
                    System.out.println(imageName + ": Malformed URL !");
                }
            }
            if(urlFile!=null){
                return new ImageIcon(urlFile);
            }
        }
        return null;
    }

}