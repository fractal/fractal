/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.panel;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.fractal.explorer.context.ClientCollectionInterfaceContainer;
import org.objectweb.fractal.explorer.context.ClientInterfaceWrapper;
import org.objectweb.fractal.explorer.lib.SignatureWrapper;
import org.objectweb.util.explorer.api.Entry;
import org.objectweb.util.explorer.api.Table;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.core.common.api.ContextContainer;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;

/**
 * Displays the available business interfaces of a component.
 *
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 *
 * @version 0.1
 */
public class ComponentTable
  implements Table 
{
    
    //==================================================================
    //
    // Internal states.
    //
    //==================================================================
    
    //==================================================================
    //
    // No constructor.
    //
    //==================================================================
    
    // ==================================================================
    //
    // Internal methods.
    //
    // ==================================================================
    
    /**
     * Provides the bound interface of the given client interface.
     * @param itf 
     * @param bc 
     * @return the bound interface (null if no interface bound).
     */
    protected Entry getBoundInterface(Interface itf, BindingController bc){
        if(bc!=null) {
            try {
                Interface bindInterface = (Interface)bc.lookupFc(itf.getFcItfName());
                if(bindInterface!=null)
                    return new DefaultEntry(FcExplorer.getPrefixedName(bindInterface), bindInterface);
            } catch(NoSuchInterfaceException e) {
                return null;
            }
        }
        return null;
    }
    
    //==================================================================
    //
    // Public methods for Table interface.
    //
    //==================================================================
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.Table#getHeaders(org.objectweb.util.explorer.api.TreeView)
     */
    public String[] getHeaders(TreeView treeView) {
        return new String[] {"Interface", "Type", "Connected"};
    }
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.Table#getRows(org.objectweb.util.explorer.api.TreeView)
     */
    public Object[][] getRows(TreeView treeView) {
        Hashtable collections = new Hashtable();
        Vector rows = new Vector();
        
        Component component = (Component)treeView.getSelectedObject();
        BindingController bc = null;
        try{
            bc = FcExplorer.getBindingController(component);
        } catch(Exception e) {
            // Binding-controller may be unavailable (e.g. Bootstrap component).
            // System.err.println("Client interface Error : " + e.getMessage());   
        }
        
        // Prepare the collection interfaces.
        ComponentType cType = (ComponentType)component.getFcType();
        InterfaceType[] iTypes = cType.getFcInterfaceTypes();
        for (int i = 0; i < iTypes.length; i++) {
            if(FcExplorer.isClientCollection(iTypes[i])){
                ContextContainer cc = new ClientCollectionInterfaceContainer(iTypes[i], component);
                collections.put(iTypes[i].getFcItfName(),cc);
            }
        }
        
        // Intropect the component interfaces.
        Object[] interfaces = component.getFcInterfaces();
        for (int i = 0; i < interfaces.length; i++){
            if(!component.equals(interfaces[i])){
                Interface itf = (Interface)interfaces[i];
                if (!FcExplorer.isController(itf)) {
                    String signature = ((InterfaceType)itf.getFcItfType()).getFcItfSignature();
                    InterfaceType iType = (InterfaceType)itf.getFcItfType();
                    if (FcExplorer.isClientCollection(itf)) {                    
                        String name = FcExplorer.getName(itf);
                        ((ContextContainer)collections.get(iType.getFcItfName())).addEntry(name,new TableClientCollectionWrapper(itf));
                    }  else if(FcExplorer.isClient(itf)) {
                        rows.add(new Object[] {new DefaultEntry(FcExplorer.getName(itf), new ClientInterfaceWrapper(itf)),
                                                 new DefaultEntry(signature, new SignatureWrapper(signature)),
                                                 getBoundInterface(itf,bc)});
                    } else {
                        rows.add(new Object[] {new DefaultEntry(FcExplorer.getName(itf), itf),
                                                 new DefaultEntry(signature, new SignatureWrapper(signature)),
                                                 null});
                    }
                }
            }
        }
        // Add the collection interfaces
        for (Enumeration i = collections.elements() ; i.hasMoreElements() ; ) {
            ClientCollectionInterfaceContainer collection = (ClientCollectionInterfaceContainer)i.nextElement();
            Entry[] entries = collection.getEntries(null);
            String signature = collection.getItf().getFcItfSignature();
            // The collection interface is not bound
            if(entries.length==0) {
                rows.add(new Object[]{new DefaultEntry(collection.getItf().getFcItfName(),collection),
                        new DefaultEntry(signature, new SignatureWrapper(signature)), null});
            } else {
                for(int j = 0 ; j < entries.length ; j++) {
                    Interface itf = ((TableClientCollectionWrapper)entries[j].getValue()).getItf();
                    rows.add(new Object[]{entries[j],
                            new DefaultEntry(signature, new SignatureWrapper(signature)),
                            getBoundInterface(itf,bc)});
                }
            } 
        }
        
        Object[][] contenu = new Object[rows.size()][2];
        for(int i=0 ; i<rows.size() ; i++)
            contenu[i] = (Object[])rows.get(i);
        return contenu;
    }
}
