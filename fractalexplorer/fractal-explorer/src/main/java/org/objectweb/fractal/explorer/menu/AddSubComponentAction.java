/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.menu;

import java.awt.Dimension;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.swing.gui.api.DialogAction;
import org.objectweb.util.explorer.swing.gui.api.DialogBox;
import org.objectweb.util.explorer.swing.gui.lib.DefaultDialogBox;
import org.objectweb.util.explorer.swing.gui.lib.TreeBox;

/**
 * This action allows to add a component into a composite component.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class AddSubComponentAction 
  implements MenuItem, 
             DialogAction 
{
    //==================================================================
    //
    // Internal states.
    //
    //==================================================================
    
    /** TheTreeBox used. */
    protected TreeBox treeBox_;
    
    /** The controller used to add a new component component. */
    protected ContentController contentInterface_;
    
    //==================================================================
    //
    // No constructor.
    //
    //==================================================================
    
    //==================================================================
    //
    // No Internal method.
    //
    //==================================================================
    
    //==================================================================
    //
    // Public methods for ExtendedActionListener interface.
    //
    //==================================================================
    
    /**
     * @see org.objectweb.util.explorer.api.MenuItem#getStatus(org.objectweb.util.explorer.api.TreeView)
     */
    public int getStatus(TreeView treeView) {
        return MenuItem.ENABLED_STATUS;
    }    
    
    /**
     * @see org.objectweb.util.explorer.api.MenuItem#actionPerformed(org.objectweb.util.explorer.api.MenuItemTreeView)
     */
    public void actionPerformed(MenuItemTreeView e) throws Exception {
        contentInterface_ = (ContentController)e.getSelectedObject();
        Component comp = e.getTree().duplicate();
        
        treeBox_ = new TreeBox(comp);
        treeBox_.setPreferredSize(new Dimension(450, 350));
        
        DialogBox dialog = new DefaultDialogBox("Select a component to add");
        dialog.setValidateAction(this);
        dialog.addElementBox(treeBox_);
        dialog.show();
    }
    
    //==================================================================
    //
    // Public methods for DialogAction interface.
    //
    //==================================================================
    
    /**
     * @see org.objectweb.util.explorer.swing.gui.api.DialogAction#executeAction()
     */
    public void executeAction() throws Exception {
        Object o = treeBox_.getObject();
        try{
            Component subComponent = (Component)o;  
            contentInterface_.addFcSubComponent(subComponent);
        }catch(ClassCastException e){
            throw new Exception("You must select a component!");
        }
    } 
    
}