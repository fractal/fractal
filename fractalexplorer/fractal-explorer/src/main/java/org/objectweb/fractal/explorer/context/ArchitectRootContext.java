/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.context;

import java.util.Vector;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.util.explorer.api.RootContext;
import org.objectweb.util.explorer.api.RootEntry;
import org.objectweb.util.explorer.core.common.api.ContextContainer;
import org.objectweb.util.explorer.core.common.lib.DefaultContextContainer;
import org.objectweb.util.explorer.core.root.lib.DefaultRootEntry;

/**
 * The context adds features to manage Fractal application such as
 * the fractal factory. This factory allows one to create new instance
 * of fractal definition. 
 * 
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class ArchitectRootContext 
  implements RootContext
{
    
    // ==================================================================
    //
    // No internal state.
    //
    // ==================================================================
    
    /** context container for the root node. */
    protected static ContextContainer cc_ = null;
    
    // ==================================================================
    //
    // static block.
    //
    // ==================================================================
    
    // Initializes the ContextContainer.
    static {
        cc_ = new DefaultContextContainer();
        cc_.addEntry("type-factory", new TypeContainer());
        cc_.addEntry("component-factory", new TemplateContainer());
    }
    
    // ==================================================================
    //
    // No Constructors.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // No internal methods.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // Public methods for interface Context.
    //
    // ==================================================================
    
    /**
     * Returns an array containing the entries contained by the target context.
     * @return A new array of entry.
     */
    public RootEntry[] getEntries(){
        Vector values = new Vector();
        try {
            values.add(new DefaultRootEntry("Fractal-factory", new FactoryWrapper(FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND)),0));
        } catch (ADLException e) {
            System.out.println("[" + getClass().getName() + "] Exception: " + e.getMessage());
        }
        values.add(new DefaultRootEntry("Fractal-container",cc_,1));
        return (RootEntry[])values.toArray(new RootEntry[0]);
    }
    
}