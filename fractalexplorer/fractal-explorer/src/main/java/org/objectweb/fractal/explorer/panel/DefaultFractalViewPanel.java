/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.panel;

import javax.swing.JPanel;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.fractal.explorer.api.FractalViewPanel;
import org.objectweb.util.explorer.api.Panel;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.swing.panel.WhitePanel;

/**
 * Specialization of the view panel for entities having life cycle properties.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public abstract class DefaultFractalViewPanel 
           implements Panel, 
                      FractalViewPanel 
{
    //==================================================================
    //
    // Internal state.
    //
    //==================================================================
    
    /** A white panel. */
    protected final Panel whitePanel_ = new WhitePanel();
    
    /** Boolean which indicates if the component is strated or not. */
    protected boolean isStarted_ = true;
    
    //==================================================================
    //
    // No constructor.
    // 
    //==================================================================
    
    //==================================================================
    //
    // Internal methods.
    //
    //==================================================================
    
    //==================================================================
    //
    // Public methods for Panel interface.
    //
    //==================================================================
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.Panel#selected(org.objectweb.util.explorer.api.TreeView)
     */
    public void selected(TreeView treeView) {
        LifeCycleController lcc = null;
        try{
            lcc = FcExplorer.getLifeCycleController(FcExplorer.getComponent(treeView.getSelectedObject()));
        } catch(NoSuchInterfaceException e) {
            try {
                selectedStarted(treeView);
            } catch (Exception e1) {
                isStarted_ = false;
                selectedStopped(treeView);
            }
        }
        if(lcc.getFcState().equals(LifeCycleController.STARTED)){
            isStarted_ = true;        
            selectedStarted(treeView);
        } else {
            isStarted_ = false;
            selectedStopped(treeView);
        }
    }
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.Panel#getPanel()
     */
    public Object getPanel(){
        if(isStarted_){
            try{
                return getStartedPanel();
            } catch (Exception e) {
                return getStoppedPanel();
            }
        } 
        return getStoppedPanel();
    }
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.Panel#unselected(org.objectweb.util.explorer.api.TreeView)
     */
    public void unselected(TreeView treeView) {
        LifeCycleController lcc = null;
        try{
            lcc = FcExplorer.getLifeCycleController(FcExplorer.getComponent(treeView.getSelectedObject()));
        } catch(NoSuchInterfaceException e) {
            try {
                unselectedStarted(treeView);
            } catch (Exception e1) {
                unselectedStopped(treeView);
            }
        }
        if(lcc.getFcState().equals(LifeCycleController.STARTED))
            unselectedStarted(treeView);
        else
            unselectedStopped(treeView);
    }
    
    //==================================================================
    //
    // Public methods for FractalTreeView interface.
    //
    //==================================================================
    
    public abstract void selectedStarted(TreeView treeView);
    
    public void selectedStopped(TreeView treeView) { 
        // Empty block.
    }
    
    public abstract JPanel getStartedPanel();
    
    public JPanel getStoppedPanel() { 
        return (JPanel)whitePanel_.getPanel();
    }
    
    public abstract void unselectedStarted(TreeView treeView);
    
    public void unselectedStopped(TreeView treeView){ 
        // Empty block.
    }
    
}