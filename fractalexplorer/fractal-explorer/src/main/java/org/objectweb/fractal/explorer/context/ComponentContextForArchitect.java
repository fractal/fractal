/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.context;

import org.objectweb.fractal.api.Component;
import org.objectweb.util.explorer.api.Entry;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;


/**
 * Context on Component for admin role (adding the controller interfaces).
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class ComponentContextForArchitect 
     extends ComponentContextForAdmin 
{
    // ==================================================================
    //
    // No internal state.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // No Constructors.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // Internal methods.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // Public methods for interface Context.
    //
    // ==================================================================
    
    /**
     * Returns an array containing the entries contained by the target context.
     * @return A new array of entry.
     */
    public Entry[] getEntries(Object object){
        Component ci = (Component)object;
        Entry[] entries = super.getEntries(object);
        Entry[] values = new Entry[entries.length + 1];
        values[0] = new DefaultEntry("types", ci.getFcType());
        System.arraycopy(entries,0,values,1,entries.length);
        return values;
    }
}