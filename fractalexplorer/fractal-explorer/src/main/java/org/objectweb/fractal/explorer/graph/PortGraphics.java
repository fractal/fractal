/*====================================================================

ObjectWeb Fractal Explorer
Copyright (C) 2000-2005 INRIA - USTL - LIFL - GOAL
Contact: fractal@objectweb.org

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Initial developer(s): La�titia Lafeuille.
Contributor(s): Simon Leli�vre, Philippe Merle.

====================================================================
$Id$
====================================================================*/

package org.objectweb.fractal.explorer.graph;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import org.objectweb.util.explorer.swing.graph.PortGraphicsInterface;
import org.objectweb.util.explorer.swing.graph.PortType;

/**
 * 
 * @version 0.2
 */
public class PortGraphics 
  implements PortGraphicsInterface 
{

    /**
     * @param portType the type of the port
     * @return Dimension the size of the port. The center right must be the
     *         server port location. The center left must be the client port
     *         location. The center up must be the controler port location.
     */
    public Dimension getPortSize(String portType, boolean isInternal) {
        if (portType.equals(PortType.SERVER_PORT))
            return new Dimension(11, 10);
        else if (portType.equals(PortType.CLIENT_PORT))
            return new Dimension(11, 10);
        else if (portType.equals(PortType.COLLECTION_PORT))
            return new Dimension(11, 10);
        else if (portType.equals(PortType.CONTROLLER_PORT)) {
            if (!isInternal)
                return new Dimension(8, 10);
            else
                return new Dimension(8 + COMPOSITE_CONTROLLER_PORT_WIDTH, 10);
        } else
            return new Dimension(11, 10);
    }

    /**
     * @param g the Graphics where the port have to be paint.
     * @param portType the type of the port.
     */
    public void drawPort(Graphics g, String portName, String portType,
            boolean isInternal) {
        if (portType.equals(PortType.SERVER_PORT)) {
            g.setColor(Color.red);
            g.drawRect(2, 3, 8, 2);
            g.fillRect(2, 3, 8, 2);
            g.drawRect(0, 0, 2, 8);
            g.fillRect(0, 0, 2, 8);
        } else if (portType.equals(PortType.COLLECTION_PORT)) {
            g.setColor(new Color(20, 61, 18));
            g.drawLine(3, 2, 6, 2);
            g.drawLine(3, 0, 6, 4);
            g.drawLine(3, 4, 6, 0);
            g.drawRect(8, 1, 2, 8);
            g.fillRect(8, 1, 2, 8);
            g.drawRect(0, 5, 8, 2);
            g.fillRect(0, 5, 8, 2);
        } else if (portType.equals(PortType.CLIENT_PORT)) {
            g.setColor(new Color(2708224));
            g.drawRect(8, 0, 2, 8);
            g.fillRect(8, 0, 2, 8);
            g.drawRect(0, 3, 8, 2);
            g.fillRect(0, 3, 8, 2);
        } else if (portType.equals(PortType.CONTROLLER_PORT)) {
            g.setColor(Color.BLUE);
            g.drawRect(0, 0, 8, 2);
            g.fillRect(0, 0, 8, 2);
            g.drawRect(3, 2, 2, 8);
            g.fillRect(3, 2, 2, 8);
            if (isInternal) {
                g.setColor(Color.BLACK);
                g.drawString(portName, 10, 9);
            }
        }
    }
}
