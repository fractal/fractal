/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): Romain Rouvoy______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.icon;

import javax.swing.Icon;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.fractal.explorer.context.InterfaceWrapper;

/**
 * Provides a specific icon for InterfaceReference if this one is :
 * <br>
 * <li>a client interface.</li>
 * <li>not bound.</li>
 * </br>
 * 
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class ClientInterfaceIconProvider 
     extends InterfaceIconProvider
{
    // ==================================================================
    //
    // Contructors.
    //
    // ==================================================================
    
    /** Empty constructor. */
    public ClientInterfaceIconProvider() {
        super();
        loadFc("Client");
        loadFc("ClientMandatory");
        loadFc("ClientOptional");
    }
    
    // ==================================================================
    //
    // Public methods for IconProvider interface.
    //
    // ==================================================================
    
    /**
     * Provides the appropriate icon.
     */
    public Object newIcon(Object obj) {
        InterfaceWrapper ciw = (InterfaceWrapper) obj;
        Interface itf = ciw.getItf();
        InterfaceType itft = (InterfaceType)itf.getFcItfType();

        if (itft.isFcOptionalItf())
            return (Icon) getIcons().get("ClientOptional");
        try {
            Component cpt = itf.getFcItfOwner();
            return (Icon) (FcExplorer.getBindingController(cpt).lookupFc(itf.getFcItfName()) == null ? getIcons().get("ClientMandatory") : getIcons().get("Client"));
        } catch (NoSuchInterfaceException ex) {
            return (Icon) getIcons().get("Client");
        }
    }
}