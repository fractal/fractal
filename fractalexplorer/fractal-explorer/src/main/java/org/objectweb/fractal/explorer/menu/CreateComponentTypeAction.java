/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.menu;

import java.util.List;
import java.util.Vector;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.core.common.api.ContextContainer;
import org.objectweb.util.explorer.swing.gui.api.DialogAction;
import org.objectweb.util.explorer.swing.gui.api.DialogBox;
import org.objectweb.util.explorer.swing.gui.lib.DefaultDialogBox;
import org.objectweb.util.explorer.swing.gui.lib.ExtendedTreeChooserBox;
import org.objectweb.util.explorer.swing.gui.lib.LabelBox;

/**
 * This action allows to create a new component type.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class CreateComponentTypeAction 
  implements MenuItem, 
             DialogAction 
{
    //==================================================================
    //
    // Internal states.
    //
    //==================================================================
    
    /** The label box. */
    protected LabelBox name_;
    
    /** The tree chooser box. */
    protected ExtendedTreeChooserBox extendedTreeChooserBox_;
    
    /** The context container to use. */
    protected ContextContainer cc_;
    
    //==================================================================
    //
    // No constructor.
    //
    //==================================================================
    
    //==================================================================
    //
    // Internal methods.
    //
    //==================================================================
    
    /**
     * Create a box containing all the box to specify all the params.
     * @param dialogBox 
     * @param treeComponent 
     */
    protected void createBox(DialogBox dialogBox, Component treeComponent) {
        name_= new LabelBox("Name");
        dialogBox.addElementBox(name_);
        extendedTreeChooserBox_ = new ExtendedTreeChooserBox("Interface Type",treeComponent, dialogBox);
        dialogBox.addElementBox(extendedTreeChooserBox_);
    }
    
    //==================================================================
    //
    // Public methods for MenuItem interface.
    //
    //==================================================================
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#actionPerformed(org.objectweb.util.explorer.api.MenuItemTreeView)
     */
    public void actionPerformed(MenuItemTreeView e) throws Exception {         
        cc_ = (ContextContainer)e.getSelectedObject();
        
        DialogBox dialog = new DefaultDialogBox("Create a new component type");      
        createBox(dialog, e.getTree().duplicate());
        dialog.setValidateAction(this);
        dialog.show();
    }
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#getStatus(org.objectweb.util.explorer.api.TreeView)
     */
    public int getStatus(TreeView treeView) {
        return MenuItem.ENABLED_STATUS;
    }
    
    //==================================================================
    //
    // Public methods for DialogAction interface.
    //
    //==================================================================
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.swing.gui.api.DialogAction#executeAction()
     */
    public void executeAction() throws Exception {
        TypeFactory typeFactory = FcExplorer.getTypeFactory(FcExplorer.getBootstrapComponent());
        String name = name_.getLabel();
        List l = new Vector();
        Object[] objects = extendedTreeChooserBox_.getObjects();
        for (int i=0 ; i < objects.length ; i++) {
            if (objects[i] != null && objects[i] instanceof InterfaceType) {
                l.add(objects[i]);
            }    
        }
        cc_.addEntry(name,typeFactory.createFcType((InterfaceType[])l.toArray(new InterfaceType[0])));                        
    }
    
}