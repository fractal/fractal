/*====================================================================
 
 Objectweb Fractal Explorer
 Copyright (C) 2000-2005 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.icon;

import javax.swing.Icon;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.fractal.explorer.api.FractalIconProvider;
import org.objectweb.util.explorer.api.IconProvider;
import org.objectweb.util.explorer.swing.icon.StringIcon;

/**
 * Basic icon provider for fractal.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public abstract class DefaultFractalIconProvider 
           implements IconProvider, 
                      FractalIconProvider 
{
    //==================================================================
    //
    // No internal state.
    //
    //==================================================================
    
    /** The icon displayed when the Fractal component is stopped. */
    protected Icon unknownIcon;
    
    //==================================================================
    //
    // No constructor.
    // 
    //==================================================================
    
    /**
     * Default constructor
     */
    public DefaultFractalIconProvider() {
        unknownIcon = new StringIcon("?");   
    }
    
    //==================================================================
    //
    // Internal methods.
    //
    //==================================================================
    
    //==================================================================
    //
    // No public method.
    //
    //==================================================================
    
    /**
     * @see org.objectweb.util.explorer.api.IconProvider#newIcon(java.lang.Object)
     */
    public Object newIcon(Object object) {        
        LifeCycleController lcc = null;
        try{
            lcc = FcExplorer.getLifeCycleController(FcExplorer.getComponent(object));
        } catch(NoSuchInterfaceException e) {
            try {
                return newStartedIcon(object);
            } catch  (Exception e1) {
                return newStoppedIcon(object);
            }
        }
        if(lcc.getFcState().equals(LifeCycleController.STARTED))
            return newStartedIcon(object);
        return newStoppedIcon(object);
    }
    
    /**
     * Returns an icon which represents a question mark.
     * @param object The object you want to find the associated icon.
     * @return The associated icon.
     */
    public Object newStoppedIcon(Object object){
        return unknownIcon;
    }
    
    /**
     * Called when the Fractal component is started.
     * @param object The object you want to find the associated icon.
     * @return The associated icon.
     */
    public abstract Object newStartedIcon(Object object);    
}