/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.menu;

import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.core.common.api.ContextContainer;
import org.objectweb.util.explorer.swing.gui.api.DialogAction;
import org.objectweb.util.explorer.swing.gui.api.DialogBox;
import org.objectweb.util.explorer.swing.gui.lib.BooleanBox;
import org.objectweb.util.explorer.swing.gui.lib.DefaultDialogBox;
import org.objectweb.util.explorer.swing.gui.lib.LabelBox;

/**
 * This action allows to create a new interface type.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class CreateInterfaceTypeAction 
  implements MenuItem, 
             DialogAction 
{
    //==================================================================
    //
    // Internal states.
    //
    //==================================================================
    
    /** The boolean boxes. */
    protected BooleanBox isClientBox_, isOptionalBox_, isCollectionBox_;
    
    /** The label boxes. */
    protected LabelBox nameBox_, signatureBox_;
    
    /** The context container where adding the type. */
    protected ContextContainer cc_;
    
    /** The type factory used to create a type. */
    protected TypeFactory typeFactory_;
    
    //==================================================================
    //
    // No constructor.
    //
    //==================================================================
    
    //==================================================================
    //
    // Internal methods.
    //
    //==================================================================
    
    /**
     * Creates a box containing all the box to specify all the params
     * @param dialogBox 
     */
    protected void createBox(DialogBox dialogBox) {
        nameBox_ = new LabelBox("Name");
        dialogBox.addElementBox(nameBox_);
        signatureBox_ = new LabelBox("Signature");
        dialogBox.addElementBox(signatureBox_);
        isClientBox_ = new BooleanBox("Type","Client","Server",true);
        dialogBox.addElementBox(isClientBox_);
        isOptionalBox_ = new BooleanBox("Contingency","Optional","Mandatory",true);
        dialogBox.addElementBox(isOptionalBox_);
        isCollectionBox_ = new BooleanBox("Cardinality","Collection","Single",true);
        dialogBox.addElementBox(isCollectionBox_);
    }
    
    //==================================================================
    //
    // Public methods for ExtendedActionListener interface.
    //
    //==================================================================
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#actionPerformed(org.objectweb.util.explorer.api.MenuItemTreeView)
     */
    public void actionPerformed(MenuItemTreeView e) throws Exception {     
        typeFactory_ = FcExplorer.getTypeFactory(FcExplorer.getBootstrapComponent());
        cc_ = (ContextContainer)e.getSelectedObject();
        DialogBox dialog = new DefaultDialogBox("Create a new interface type");      
        createBox(dialog);
        dialog.setValidateAction(this);
        dialog.show();
    }
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#getStatus(org.objectweb.util.explorer.api.TreeView)
     */
    public int getStatus(TreeView treeView) {
        return MenuItem.ENABLED_STATUS;
    }
    
    //==================================================================
    //
    // Public methods for DialogAction interface.
    //
    //==================================================================
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.swing.gui.api.DialogAction#executeAction()
     */
    public void executeAction() throws Exception {
        String name = nameBox_.getLabel();
        String signature = signatureBox_.getLabel();
        boolean isClient = isClientBox_.getValue();
        boolean isOptional = isOptionalBox_.getValue();
        boolean isCollection = isCollectionBox_.getValue();
        cc_.addEntry(name,typeFactory_.createFcItfType(name,signature,isClient,isOptional,isCollection));
    }
    
}