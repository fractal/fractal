/*====================================================================

Objectweb Fractal Explorer
Copyright (C) 2000-2005 INRIA & USTL - LIFL - GOAL
Contact: fractal@objectweb.org

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA
 
Contact: fractal@objectweb.org

Author: Alessio Pace

*/


package org.objectweb.fractal.explorer.lib;

import java.io.File;

import javax.swing.JFileChooser;

import org.objectweb.fractal.adl.dumper.SaxAdlGenerator;
import org.objectweb.fractal.api.Component;
import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.TreeView;

/**
 * An Action to save the current select Fractal <tt>Component</tt> to XML, using the Fractal ADL Dumper.
 * 
 * TODO: add the possibility to let specify if it should separate or not the generated ADL files in 
 * case of saving a composite component.
 * 
 * @author Alessio Pace
 * @since 1.1.2
 * 
 */
public class SaveConfigurationFileAction implements MenuItem {

	public int getStatus(TreeView arg0) {
		return MenuItem.ENABLED_STATUS;
	}

	public void actionPerformed(MenuItemTreeView menuItemTreeView)
			throws Exception {

		JFileChooser fileChooser = JFileChooserSingleton
				.getInstance(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnVal = fileChooser.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File directory = fileChooser.getSelectedFile();

			try {

				SaxAdlGenerator generator = new SaxAdlGenerator(
						SaxAdlGenerator.STANDARD_DTD, directory.getAbsolutePath());

				Component component = (Component) menuItemTreeView
						.getSelectedObject();

				System.out.println("Persisting component");
				
				// the "false" value indicates to not split the ADL in separate files
				generator.generateDefinition(component, directory.getName(), false);
				
			} catch (Exception e) {
				e.printStackTrace();
			}

			System.out.println("Saved");
			
		}
	}

}
