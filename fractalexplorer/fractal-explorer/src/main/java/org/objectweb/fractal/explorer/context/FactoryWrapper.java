/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): J�r�me Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.context;

import org.objectweb.fractal.adl.Factory;

/**
 * Wrapper for <code>org.objectweb.fractal.adl.Factory</code>. 
 *
 * @author <a href="mailto:Philippe.Merle@lifl.fr">Philippe Merle</a>,
 *         <a href="mailto:Jerome.Moroy@lifl.fr">J�r�me Moroy</a>
 * 
 * @version 0.1
 */
public class FactoryWrapper 
{

    //==================================================================
    //
    // Internal states.
    //
    //==================================================================

    /** The wrapped factory. */
    protected Factory factory_;

    //==================================================================
    //
    // Constructor.
    // 
    //==================================================================

    /**
     * Default Constructor.
     * @param factory the factory interface wrapped.
     */
    public FactoryWrapper(Factory factory){
        factory_ = factory;
    }

    //==================================================================
    //
    // No internal method.
    //
    //==================================================================

    //==================================================================
    //
    // Public methods.
    //
    //==================================================================
    
    /**
     * Provides the wrapped factory.
     * @return the wrapped factory.
     */
    public Factory getFactory(){
        return factory_;
    }
    
}
