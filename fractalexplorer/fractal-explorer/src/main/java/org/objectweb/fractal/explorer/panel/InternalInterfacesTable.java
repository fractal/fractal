/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.panel;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.fractal.explorer.context.InterfaceWrapperFactory;
import org.objectweb.fractal.explorer.lib.SignatureWrapper;
import org.objectweb.util.explorer.api.Entry;
import org.objectweb.util.explorer.api.Table;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;

/**
 * Displays the internal components of a component.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class InternalInterfacesTable
  implements Table 
{
    
    //==================================================================
    //
    // Internal states.
    //
    //==================================================================
    
    //==================================================================
    //
    // Constructor.
    //
    //==================================================================
    
    /** The name controller. */
    protected NameController nameController_;
    
    // ==================================================================
    //
    // Internal methods.
    //
    // ==================================================================
    
    /**
     * Provides the bound interface of the given client interface.
     * @param itf 
     * @param bc 
     * @return the bound interface of the given client interface.
     */
    protected Entry getBoundInterface(Interface itf, BindingController bc){
        if(bc!=null){
            try{
                Interface bindInterface = (Interface)bc.lookupFc(itf.getFcItfName());
                if(bindInterface!=null)
                    return new DefaultEntry(FcExplorer.getPrefixedName(bindInterface), bindInterface);
            } catch(NoSuchInterfaceException e) {
                return null;
            }
        }
        return null;
    }
    
    /**
     * Provides the BindingController associates to the given interface.
     * @param itf 
     * @return the BindingController associates to the given interface.
     */
    protected BindingController getBindingController(Interface itf){
        Component comp = itf.getFcItfOwner();
        try{
            return FcExplorer.getBindingController(comp);
        }catch(NoSuchInterfaceException e){
            return null;
        }
    }
    
    /**
     * Provides the NameController associates to the given interface.
     * @param itf 
     * @return the NameController associates to the given interface.
     */
    protected NameController getNameController(Interface itf){
        Component comp = itf.getFcItfOwner();
        try{
            return FcExplorer.getNameController(comp);
        }catch(NoSuchInterfaceException e){
            return null;
        }
    }
    
    /**
     * Provides an interface, its type and the interface it is connected on.
     * @param itf 
     * @param bc 
     * @return an interface, its type and the interface it is connected on.
     */
    protected Object[] getValues(Interface itf, BindingController bc){
        Object[] values = new Object[3];
        String sign = ((InterfaceType)itf.getFcItfType()).getFcItfSignature();
        values[0] = new DefaultEntry(FcExplorer.getName(itf), InterfaceWrapperFactory.singleton().getWrapper(itf));
        values[1] = new DefaultEntry(sign, new SignatureWrapper(sign));
        values[2] = getBoundInterface(itf,bc);
        return values;
    }
    
    /**
     * Provides the ContentController for the given tree view.
     * @param treeView 
     * @return the ContentController for the given tree view.
     */
    protected ContentController provideContentController(TreeView treeView){
        return (ContentController)treeView.getSelectedObject();
    }
    
    //==================================================================
    //
    // Public methods for Table interface.
    //
    //==================================================================
    
    /**
     * Overriding methods
     * @see org.objectweb.util.explorer.api.Table#getHeaders(org.objectweb.util.explorer.api.TreeView)
     */
    public String[] getHeaders(TreeView treeView) {
        return new String[]{"Interface", "Type", "Connected"};
    }
    
    /**
     * Overriding methods
     * @see org.objectweb.util.explorer.api.Table#getRows(org.objectweb.util.explorer.api.TreeView)
     */
    public Object[][] getRows(TreeView treeView) {
        ContentController cc = provideContentController(treeView);
        BindingController bc = getBindingController((Interface)cc);
        nameController_ = getNameController((Interface)cc);
        Object[] interfaces = cc.getFcInternalInterfaces();
        Object[][] values = new Object[interfaces.length][1];
        for (int i = 0 ; i < interfaces.length ; i++){
            values[i] = getValues((Interface)interfaces[i],bc);
        }
        return values;
    }
}
