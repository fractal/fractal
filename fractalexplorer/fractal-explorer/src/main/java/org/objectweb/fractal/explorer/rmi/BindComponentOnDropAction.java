/*====================================================================

Objectweb Fractal Explorer
Copyright (C) 2000-2005 INRIA & USTL - LIFL - GOAL
Contact: fractal@objectweb.org

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Initial developer(s): Jerome Moroy.
Contributor(s): ______________________________________.

---------------------------------------------------------------------
$Id$
====================================================================*/
package org.objectweb.fractal.explorer.rmi;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.rmi.registry.NamingService;
import org.objectweb.util.explorer.api.DropAction;
import org.objectweb.util.explorer.api.DropTreeView;
import org.objectweb.util.explorer.api.Entry;

/**
 * A Fractal component can be bound to a Redistry using this DnD action. 
 * 
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * 
 * @version 0.1
 */
public class BindComponentOnDropAction 
  implements DropAction 
{

    // ==================================================================
    //
    // Internal state.
    //
    // ==================================================================

    // ==================================================================
    //
    // No Constructors.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // No internal methods.
    //
    // ==================================================================

    // ==================================================================
    //
    // Public methods for MenuItem interface.
    //
    // ==================================================================
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.DropAction#execute(org.objectweb.util.explorer.api.DropTreeView)
     */
    public void execute(DropTreeView dropTreeView) throws Exception {
        Entry dragSource = dropTreeView.getDragSourceEntry();
        NamingService namingService = (NamingService)dropTreeView.getSelectedObject();
        Component toBind = null;
        try {
            toBind = (Component)dragSource.getValue();
        } catch (ClassCastException e) {
            throw new Exception("Component expected!");
        }
        namingService.bind(dragSource.getName().toString(),toBind);
    }

}
