/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Romain Rouvoy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.panel;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.SuperController;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.util.explorer.api.Table;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;

/**
 * Displays the internal components of a component.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class SuperComponentsTable
  implements Table 
{
    //==================================================================
    //
    // Internal states.
    //
    //==================================================================
    
    //==================================================================
    //
    // No constructor.
    //
    //==================================================================
    
    // ==================================================================
    //
    // Internal methods.
    //
    // ==================================================================
    
    /**
     * Provides the SuperController for the given tree view.
     * @param treeView 
     * @return the SuperController for the given tree view.
     */
    protected SuperController provideSuperController(TreeView treeView){
        return (SuperController)treeView.getSelectedObject();
    }
    
    //==================================================================
    //
    // Public methods for Table interface.
    //
    //==================================================================
    
    /**
     * Overriding methods
     * @see org.objectweb.util.explorer.api.Table#getHeaders(org.objectweb.util.explorer.api.TreeView)
     */
    public String[] getHeaders(TreeView treeView) {
        return new String[] {"Components"};
    }
    
    /**
     * Overriding methods
     * @see org.objectweb.util.explorer.api.Table#getRows(org.objectweb.util.explorer.api.TreeView)
     */
    public Object[][] getRows(TreeView treeView) {
        SuperController sc = provideSuperController(treeView);
        Component[] components = sc.getFcSuperComponents();
        Object[][] values = new Object[components.length][1];
        for (int i = 0 ; i < components.length ; i++){
            values[i] = new Object[] {new DefaultEntry(FcExplorer.getName(components[i]), components[i])};
        }
        return values;
    }
}
