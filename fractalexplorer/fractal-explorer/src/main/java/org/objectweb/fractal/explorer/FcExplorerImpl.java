/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Romain Rouvoy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer;

import static org.objectweb.fractal.explorer.FcExplorer.getName;
import static org.objectweb.fractal.fraclet.types.Cardinality.COLLECTION;
import static org.objectweb.util.trace.TraceSystem.setLevel;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.swing.JMenuBar;
import javax.swing.JToolBar;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.fraclet.annotations.Attribute;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.util.explorer.parser.api.ParserConfiguration;
import org.objectweb.util.explorer.swing.api.Explorer;
import org.objectweb.util.explorer.swing.lib.SwingExplorer;

/**
 * This Fractal component contains the explorer
 * 
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @author <a href="mailto:Romain.Rouvoy@inria.fr">Romain Rouvoy</a>
 * @version 0.1
 */
@org.objectweb.fractal.fraclet.annotations.Component
public class FcExplorerImpl {
	/** The ParserConfiguration interface to use. */
	protected ParserConfiguration parser_ = null;

	/** The Fractal Explorer. */
	protected static SwingExplorer explorer_ = null;

	/** The config files */
	@Attribute(name = "configFiles", set = "loadConfigFile", value="config/fractalProperties.xml")
	protected String configFiles_;

	/** The list of Component */
	@Requires(name = "fcAppl", cardinality = COLLECTION, bind = "addComponent")
	protected final Map<String, Component> elements_ = new HashMap<String, Component>();

	/**
	 * Default constructor
	 */
	public FcExplorerImpl() {
		// the RMI class loader does not work if there is no security manager
		// System.setSecurityManager(new SecurityManager());
		setLevel("debug");
		explorer_ = new SwingExplorer("Fractal Explorer");
		parser_ = explorer_.getParserConfigurationItf();

		// Loads the Explorer Framework configuration file from the
		// fractal-explorer Java archive.
		loadConfigFile("config/fractalProperties.xml");

		explorer_.setJMenuBar(new JMenuBar());
		explorer_.setJToolBar(new JToolBar());
		explorer_.setVisible(true);
	}

	public void loadConfigFile(String configFiles) {
		if (configFiles != null) {
			configFiles_ = configFiles;
			StringTokenizer st = new StringTokenizer(configFiles, "; ");
			while (st.hasMoreTokens())
				parser_.addPropertyFile(st.nextToken());
			parser_.parse();

			Explorer explorerItf = explorer_.getExplorerItf();
			explorerItf.setCurrentRoles(new String[] { "administrator" });
		}
	}

	public void addComponent(String name, Component comp) {
		elements_.put(name, comp);
		explorer_.getTreeItf().addEntry(getName(comp), comp, 1);
	}
	
	/**
	 * Refreshes the tree.
	 * 
	 * @return the SWING representation.
	 */
	public static SwingExplorer getExplorer() {
		return explorer_;
	}
}