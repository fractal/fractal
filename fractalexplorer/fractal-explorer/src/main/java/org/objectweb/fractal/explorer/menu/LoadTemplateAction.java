/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.menu;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.explorer.api.FractalBrowserConstants;
import org.objectweb.fractal.explorer.context.FactoryWrapper;
import org.objectweb.fractal.explorer.lib.JFileChooserSingleton;
import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.swing.gui.api.DialogAction;
import org.objectweb.util.explorer.swing.gui.api.DialogBox;
import org.objectweb.util.explorer.swing.gui.lib.BooleanBox;
import org.objectweb.util.explorer.swing.gui.lib.DefaultDialogBox;
import org.objectweb.util.explorer.swing.gui.lib.FileChooserBox;

/**
 * This action allows to Load a template from the Parser.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class LoadTemplateAction 
  implements MenuItem, 
             DialogAction 
{
    //==================================================================
    //
    // Internal states.
    //
    //==================================================================
    
    /** The name of the template to load. */
    protected FileChooserBox file_ = null;
    
    /** The boolean to indicate if a template is required. */
    protected BooleanBox withTemplate_ = null;

    /** The factory to use to load a template. */
    protected Factory factory_ = null;
    
    /** The TreeConfiguration to use to add the template. */
    protected TreeView treeView_;
    
    //==================================================================
    //
    // No constructor.
    //
    //==================================================================
    
    //==================================================================
    //
    // Internal methods.
    //
    //==================================================================
    
    /**
     * Create a box containing all the box to specify all the params.
     * @param dialogBox 
     */
    protected void createBox(DialogBox dialogBox) {
        file_ = new FileChooserBox("File",JFileChooserSingleton.getInstance(FractalBrowserConstants.FRACTAL_FILE_FILTER));
        dialogBox.addElementBox(file_);
        withTemplate_ = new BooleanBox("Type","Template","Component",false);
        dialogBox.addElementBox(withTemplate_);
    }
    
    //==================================================================
    //
    // Public methods for MenuItem interface.
    //
    //==================================================================
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#actionPerformed(org.objectweb.util.explorer.api.MenuItemTreeView)
     */
    public void actionPerformed(MenuItemTreeView e) throws Exception{
        treeView_ = e;
        factory_ = ((FactoryWrapper)e.getSelectedObject()).getFactory();
        DialogBox dialog = new DefaultDialogBox("Load a template");      
        createBox(dialog);
        dialog.setValidateAction(this);
        dialog.show();
    }
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#getStatus(org.objectweb.util.explorer.api.TreeView)
     */
    public int getStatus(TreeView treeView) {
        return MenuItem.ENABLED_STATUS;
    }
    
    //==================================================================
    //
    // Public methods for DialogAction interface.
    //
    //==================================================================
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.swing.gui.api.DialogAction#executeAction()
     */
    public void executeAction() throws Exception {
        String fileName = file_.getFile().getName();
        String name = fileName.substring(0,fileName.lastIndexOf("."));
        Map map = new HashMap();
        String entryName = name;
        if(withTemplate_.getValue()){
            map.put("template","true");
            entryName = entryName + "Tmpl";
        }
        treeView_.getTree().addEntry(entryName, factory_.newComponent(name,map));
    } 
    
}