/**
 * Dream
 * Copyright (C) 2003-2004 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Jakub Kornas
 * Contributor(s): 
 */

package org.objectweb.fractal.explorer.attributes;

import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.SwingConstants;

/**
 * A custom <code>CellEditor</code> for boolean values of the attributes.
 */
public class AttributeValueBooleanCellEditor extends DefaultCellEditor
{
  private JCheckBox attributeEditorBooleanValue = null;
  boolean           initialized                 = false;

  /**
   * @see DefaultCellEditor#DefaultCellEditor(JCheckBox)
   */
  public AttributeValueBooleanCellEditor(JCheckBox checkBox)
  {
    super(checkBox);

    this.attributeEditorBooleanValue = checkBox;
    this.attributeEditorBooleanValue
        .setHorizontalAlignment(SwingConstants.CENTER);
  }

  /**
   * @see DefaultCellEditor#getTableCellEditorComponent(JTable, Object, boolean,
   *      int, int)
   */
  public Component getTableCellEditorComponent(JTable table, Object value,
      boolean isSelected, int row, int column)
  {
    if (!initialized)
    {
      this.attributeEditorBooleanValue.setFont(table.getFont());
      initialized = true;
    }

    boolean val = ((Boolean) value).booleanValue();
    attributeEditorBooleanValue.setSelected(val);

    return attributeEditorBooleanValue;
  }
}