/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.api;

/**
 * A collection of constants used for managing Fractal browser GUI.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public interface FractalBrowserConstants 
{
    /** Defines the fractal file filter. */
    public static final int FRACTAL_FILE_FILTER = 1;
    
    /** JFileChooser which allows to select only directories. */
    public static final int DIRECTORY_FILE_FILTER = FRACTAL_FILE_FILTER + 1;
    
    /** JFileChooser which allows to select XML files. */
    public static final int XML_FILE_FILTER = DIRECTORY_FILE_FILTER + 1;
    
}