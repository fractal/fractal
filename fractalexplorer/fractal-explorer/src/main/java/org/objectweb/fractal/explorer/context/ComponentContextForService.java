/*====================================================================
 
 Objectweb Fractal Explorer
 Copyright (C) 2000-2005 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): Romain Rouvoy_________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.context;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.util.explorer.api.Context;
import org.objectweb.util.explorer.api.Entry;
import org.objectweb.util.explorer.core.common.api.ContextContainer;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;

/**
 * Component context for user role. 
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version 0.1
 */
public class ComponentContextForService
  implements Context 
{
    // ==================================================================
    //
    // Internal state.
    //
    // ==================================================================
    
    /** This Context Container contains the controller interfaces. */
    protected ContextContainer controllers = null;
    
    // ==================================================================
    //
    // No Constructors.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // Internal methods.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // Public methods for interface Context.
    //
    // ==================================================================
    
    /**
     * Returns an array containing the entries contained by the target context.
     * @return A new array of entry.
     */
    public Entry[] getEntries(Object object) {
        Component component = (Component)object;
        
        Hashtable collections = new Hashtable();
        List entries = new ArrayList();
        
        // Prepare the collection interfaces.
        ComponentType cType = (ComponentType)component.getFcType();
        InterfaceType[] iTypes = cType.getFcInterfaceTypes();
        for (int i = 0; i < iTypes.length; i++) {
            if(iTypes[i].isFcCollectionItf()) {
                ContextContainer cc ;
                if (iTypes[i].isFcClientItf() )
                    cc = new ClientCollectionInterfaceContainer(iTypes[i], component);
                else
                    cc = new ServerCollectionInterfaceContainer(iTypes[i], component);
                collections.put(iTypes[i].getFcItfName(),cc);
            }
        }
        
        // Intropect the component interfaces.
        controllers = new ControllerContainer();
        Object[] interfaces = component.getFcInterfaces();
        for (int i=0 ; i<interfaces.length ; i++) {
            if (!component.equals(interfaces[i])) {
                Interface itf = (Interface)interfaces[i];
                if (FcExplorer.isController(itf)) {
                    controllers.addEntry(FcExplorer.getName(itf),itf);
                } else if (FcExplorer.isCollection(itf)) {                    
                    InterfaceType iType = (InterfaceType)itf.getFcItfType();
                    String name = FcExplorer.getName(itf);
                    Object context;
                    if (FcExplorer.isClient(itf))
                        context = new ClientInterfaceWrapper(itf);
                    else
                        context = itf;
                    ((ContextContainer)collections.get(iType.getFcItfName())).addEntry(name, context);
                } else if(FcExplorer.isClient(itf)) {
                    entries.add(new DefaultEntry(FcExplorer.getName(itf), new ClientInterfaceWrapper(itf)));
                } else {
                    entries.add(new DefaultEntry(FcExplorer.getName(itf), itf));
                }
            }
        }
        
        // Add the collection interfaces
        Enumeration keys,elements;
        keys = collections.keys();
        elements = collections.elements();
        for (int i = 0 ; i < collections.size() ; i++) {
            entries.add(new DefaultEntry(keys.nextElement(),elements.nextElement())); 
        }
        
        return (Entry[])entries.toArray(new Entry[0]);        
    }
}
