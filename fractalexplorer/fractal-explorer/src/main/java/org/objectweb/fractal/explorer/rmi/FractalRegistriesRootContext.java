/*====================================================================

Objectweb Fractal Explorer
Copyright (C) 2000-2005 INRIA & USTL - LIFL - GOAL
Contact: fractal@objectweb.org

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Initial developer(s): Jerome Moroy.
Contributor(s): ______________________________________.

---------------------------------------------------------------------
$Id$
====================================================================*/
package org.objectweb.fractal.explorer.rmi;

import org.objectweb.fractal.explorer.FcExplorerImpl;
import org.objectweb.fractal.rmi.registry.NamingService;
import org.objectweb.fractal.rmi.registry.Registry;
import org.objectweb.util.explorer.api.RootContext;
import org.objectweb.util.explorer.api.RootEntry;
import org.objectweb.util.explorer.core.common.api.ContextContainer;
import org.objectweb.util.explorer.core.root.lib.DefaultRootEntry;

/**
 * The context adds some features to manage Fractal RMI Registries. 
 * 
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * 
 * @version 0.1
 */
public class FractalRegistriesRootContext 
  implements RootContext 
{

    // ==================================================================
    //
    // Internal state.
    //
    // ==================================================================
    
    /** context container for the root node. */
    protected static ContextContainer cc_ = null;
    
    // ==================================================================
    //
    // static block.
    //
    // ==================================================================
    
    // Initializes the ContextContainer.
    static {
        cc_ = new FractalRegistriesContext();
        (new AddRegistry(cc_)).start();
    }
    
    // ==================================================================
    //
    // No Constructors.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // No internal methods.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // Public methods for RootContext interface.
    //
    // ==================================================================

    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.RootContext#getEntries()
     */
    public RootEntry[] getEntries() {
        return new RootEntry[]{new DefaultRootEntry("Fractal RMI Registries", cc_, 1)};
    }

}

/**
 * The action which consists in registering the default registry
 * in the "Registeries container" is realized with a Thread because:
 * <ol>
 *   <li>The ORB takes a long time to say if the default registry is running or not.</li>
 *   <li>If the default registry is running, the answer is quick.</li>
 *   <li>If the answer is not quick, it's probably because the default registry is not running.</li>
 * </ol>
 * 
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * 
 * @version 0.1
 */
  class AddRegistry 
extends Thread 
{

    protected ContextContainer contextContainer_ = null;
    
    /**
     * Default constructor
     * @param cc
     */
    public AddRegistry(ContextContainer cc) {
        this.contextContainer_ = cc;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    public void run() {
        try{
            NamingService ns = Registry.getRegistry();
            // Emulates a ping() call.
            ns.list();
            this.contextContainer_.addEntry("Registry localhost:" + Registry.DEFAULT_PORT, ns);
            FcExplorerImpl.getExplorer().getTreeItf().refreshAll();
        } catch (Exception e) {
            // Nothing to do.
        }        
    }
}
