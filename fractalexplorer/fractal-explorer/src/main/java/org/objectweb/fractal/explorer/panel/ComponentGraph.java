/*====================================================================

ObjectWeb Fractal Explorer
Copyright (C) 2000-2005 INRIA - USTL - LIFL - GOAL
Contact: fractal@objectweb.org

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Initial developer(s): J�r�me Moroy.
Contributor(s): Philippe Merle, Simon Leli�vre.

====================================================================
$Id$
====================================================================*/

package org.objectweb.fractal.explorer.panel;

import java.awt.Color;
import java.awt.BorderLayout;
import javax.swing.JPanel;

import org.objectweb.fractal.api.Component;
import org.objectweb.util.explorer.api.Panel;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.swing.graph.Graph;
import org.objectweb.fractal.explorer.graph.FractalGraph;

import org.objectweb.util.explorer.swing.graph.CurrentDynamicTree;

/**
 * 
 * @version 0.2
 */
public class ComponentGraph 
  implements Panel 
{
    
    //==================================================================
    //
    // Internal state.
    //
    //==================================================================
    
    /** The panel in which the component graph will be drawn. */
    protected JPanel panel_ = null;
   
    /** the Graph which is display */
    private Graph graph = null ;
    
    //==================================================================
    //
    // Constructor.
    //
    //==================================================================

    /**
     * Default constructor.
     * It creates a panel and fixes the background color to white color. 
     */
    public ComponentGraph(){
        panel_ = new JPanel(new BorderLayout());
        panel_.setBackground(Color.white);
    }

    //==================================================================
    //
    // Internal methods.
    //
    //==================================================================
    
    //==================================================================
    //
    // Public methods for Panel interface.
    //
    //==================================================================
    
    /* (non-Javadoc)
     * @see org.objectweb.util.browser.api.Panel#selected(org.objectweb.util.browser.api.TreeView)
     */
    public void selected(TreeView treeView) {
        // Obtains the Fractal Component which is selected with the mouse
        Component component = (Component)treeView.getSelectedObject();

        // Display in the panel
        FractalGraph fractalGraph = new FractalGraph();

        graph = fractalGraph.getFractalGraph(component) ;
        panel_.add(graph, BorderLayout.CENTER);

        // Sets the current tree used by CurrentDynamicTree.
        CurrentDynamicTree.setTree(treeView.getTree());
    }

    /* (non-Javadoc)
     * @see org.objectweb.util.browser.api.Panel#getPanel()
     */
    public Object getPanel() {
        return panel_;
    }

    /* (non-Javadoc)
     * @see org.objectweb.util.browser.api.Panel#unselected(org.objectweb.util.browser.api.TreeView)
     */
    public void unselected(TreeView treeView) {
        // save the graph
        graph.saveFractalGraph();
    }
}
