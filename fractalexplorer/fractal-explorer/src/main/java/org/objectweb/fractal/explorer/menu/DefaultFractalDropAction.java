/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.menu;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.fractal.explorer.api.FractalDropAction;
import org.objectweb.util.explorer.api.DropAction;
import org.objectweb.util.explorer.api.DropTreeView;

/**
 * Specialization of the drop action for entities having life cycle properties.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public abstract class DefaultFractalDropAction 
           implements DropAction, 
                      FractalDropAction 
{
    //==================================================================
    //
    // No internal state.
    //
    //==================================================================
    
    //==================================================================
    //
    // No constructor.
    // 
    //==================================================================
    
    //==================================================================
    //
    // Internal methods.
    //
    //==================================================================
    
    //==================================================================
    //
    // Public methods for DropAction interface.
    //
    //==================================================================
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.DropAction#execute(org.objectweb.util.explorer.api.DropTreeView)
     */
    public void execute(DropTreeView dropTreeView) throws Exception {
        LifeCycleController lcc = null ;
        try{
            lcc = FcExplorer.getLifeCycleController(FcExplorer.getComponent(dropTreeView.getSelectedObject()));
        } catch (NoSuchInterfaceException e) {
            try{
                executeStarted(dropTreeView);
            } catch (Exception e1) {
                executeStopped(dropTreeView);
            }		
        }
        if(lcc.getFcState().equals(LifeCycleController.STARTED))
            executeStarted(dropTreeView);
        else
            executeStopped(dropTreeView);
    }
    
    //==================================================================
    //
    // Public methods for FractalDropAction interface.
    //
    //==================================================================
    
    /**
     * Called when the fractal component is started.
     * @see org.objectweb.util.explorer.api.DropAction#execute(org.objectweb.util.explorer.api.DropTreeView)
     */
    public abstract void executeStarted(DropTreeView dropTreeView) throws Exception; 
    
    /**
     * Called when the fractal component is started.
     * @see org.objectweb.util.explorer.api.DropAction#execute(org.objectweb.util.explorer.api.DropTreeView)
     */
    public void executeStopped(DropTreeView dropTreeView) throws Exception{
        // Empty block.
    }
    
    
}
