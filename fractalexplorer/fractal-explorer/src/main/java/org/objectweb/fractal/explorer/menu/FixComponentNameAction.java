/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.menu;

import org.objectweb.fractal.api.control.NameController;
import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.Tree;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.swing.gui.api.DialogAction;
import org.objectweb.util.explorer.swing.gui.api.DialogBox;
import org.objectweb.util.explorer.swing.gui.lib.DefaultDialogBox;
import org.objectweb.util.explorer.swing.gui.lib.LabelBox;

/**
 * This action allows to rename a component.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class FixComponentNameAction 
  implements MenuItem, 
             DialogAction 
{
    //==================================================================
    //
    // Internal states.
    //
    //==================================================================
    
    /** The label box. */
    protected LabelBox nameBox_;
    
    /** The associated name controller. */
    protected NameController nameController_;
    
    /** The defined tree. */
    protected Tree tree_;
    
    /** The old name. */
    protected String oldName_;
    
    //==================================================================
    //
    // No constructor.
    //
    //==================================================================
    
    //==================================================================
    //
    // Internal methods.
    //
    //==================================================================
    
    /**
     * Creates a box containing all the box to specify all the params.
     * @param dialogBox 
     */
    protected void createBox(DialogBox dialogBox) {
        oldName_ = nameController_.getFcName();
        nameBox_ = new LabelBox("Name", oldName_);
        dialogBox.addElementBox(nameBox_);
    }
    
    /**
     * Provides the name controller of the given component.
     * @param e 
     * @return the NameController of the component.
     */
    protected NameController getNameController(MenuItemTreeView e) {
        return (NameController)e.getSelectedObject();
    }
    
    //==================================================================
    //
    // Public methods for ExtendedActionListener interface.
    //
    //==================================================================
    
    public void actionPerformed(MenuItemTreeView e) throws Exception {     
        tree_ = e.getTree();
        nameController_ = getNameController(e);
        DialogBox dialog = new DefaultDialogBox("Rename a component");      
        createBox(dialog);
        dialog.setValidateAction(this);
        dialog.show();
    }
    
    /**
     * @see org.objectweb.util.explorer.api.MenuItem#getStatus(org.objectweb.util.explorer.api.TreeView)
     */
    public int getStatus(TreeView treeView) {
        return MenuItem.ENABLED_STATUS;
    }
    
    //==================================================================
    //
    // Public methods for DialogAction interface.
    //
    //==================================================================
    
    /**
     * Executes an action.
     */
    public void executeAction() throws Exception {
        String name = nameBox_.getLabel();
        nameController_.setFcName(name);
    }
    
}