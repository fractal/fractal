/*====================================================================

Objectweb Browser Framework
Copyright (C) 2000-2003 INRIA & USTL - LIFL - GOAL
Contact: openccm@objectweb.org

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Initial developer(s): Jerome Moroy.
Contributor(s): ______________________________________.

====================================================================*/

package org.objectweb.fractal.explorer.menu;

import java.awt.Dimension;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.Tree;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.swing.gui.api.DialogAction;
import org.objectweb.util.explorer.swing.gui.api.DialogBox;
import org.objectweb.util.explorer.swing.gui.lib.DefaultDialogBox;
import org.objectweb.util.explorer.swing.gui.lib.TreeBox;

/**
 * This action allows to unbind an interface.
 *
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class RemoveSubComponentAction 
  implements MenuItem, DialogAction
{

    //==================================================================
    //
    // Internal states.
    //
    //==================================================================
    
    /** TheTreeBox used */
    protected TreeBox treeBox_;

    /** The controller used to add a new component component */
    protected ContentController contentInterface_;

    //==================================================================
    //
    // No constructor.
    //
    //==================================================================

    //==================================================================
    //
    // No Internal method.
    //
    //==================================================================

    //==================================================================
    //
    // Public methods for MenuItem interface.
    //
    //==================================================================

    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#getStatus(org.objectweb.util.explorer.api.TreeView)
     */
    public int getStatus(TreeView treeView) {
        ContentController contentInterface = (ContentController)treeView.getSelectedObject();
        Component component = ((Interface)contentInterface).getFcItfOwner();
        try {
            LifeCycleController lcc = (LifeCycleController)component.getFcInterface("lifecycle-controller");        
            String status = lcc.getFcState();
            if (status.equals("STARTED"))
                return MenuItem.DISABLED_STATUS;
        } catch (NoSuchInterfaceException e) {
            // e.printStackTrace();
        }
        return MenuItem.ENABLED_STATUS;
    }

    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#actionPerformed(org.objectweb.util.explorer.api.MenuItemTreeView)
     */
    public void actionPerformed(MenuItemTreeView e) throws Exception{
        
        contentInterface_ = (ContentController)e.getSelectedObject();
        
        Component treeComponent = e.getTree().duplicate(false);
        Tree treeItf = (Tree)treeComponent.getFcInterface(Tree.TREE);
        treeItf.addEntry("Content Interface",contentInterface_,1);

        treeBox_ = new TreeBox(treeComponent);
        treeBox_.setPreferredSize(new Dimension(450, 350));

        DialogBox dialog = new DefaultDialogBox("Select a component to remove");
        dialog.setValidateAction(this);
        dialog.addElementBox(treeBox_);
        dialog.show();
    }
    
    //==================================================================
    //
    // Public methods for DialogAction interface.
    //
    //==================================================================

    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.swing.gui.api.DialogAction#executeAction()
     */
    public void executeAction() throws Exception {
        Object o = treeBox_.getObject();
        try{
            Component subComponent = (Component)o;  
            contentInterface_.removeFcSubComponent(subComponent);
        }catch(ClassCastException e1){
            throw new Exception("You must select a ComponentIdentity !");
        }
    } 


}