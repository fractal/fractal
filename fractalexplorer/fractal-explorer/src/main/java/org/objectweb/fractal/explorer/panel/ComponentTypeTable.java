/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.panel;

import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.util.explorer.api.Table;
import org.objectweb.util.explorer.api.TreeView;

/**
 * Displays the interface of the component type.
 *
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 *
 * @version 0.1
 */
public class ComponentTypeTable
  implements Table 
{
    //==================================================================
    //
    // Internal states.
    //
    //==================================================================
    
    //==================================================================
    //
    // No constructor.
    //
    //==================================================================
    
    // ==================================================================
    //
    // Internal methods.
    //
    // ==================================================================
    
    /**
     * The type can be "Client", "Server" or "Controller"
     * @param itfType 
     * @return the type of the interface.
     */
    protected String getType(InterfaceType itfType){
        if(itfType.getFcItfName().endsWith("-controller")){
            return "Controller";
        }
        return itfType.isFcClientItf()?"Client":"Server";
    }
    
    /**
     * Fills one row
     * @param itfType 
     * @return the row.
     */
    protected Object[] getValues(InterfaceType itfType){
        Object[] row = new Object[5];
        row[0] = itfType.getFcItfName();
        row[1] = itfType.getFcItfSignature();
        row[2] = getType(itfType);
        row[3] = itfType.isFcCollectionItf()?"Collection":"Single";
        row[4] = itfType.isFcOptionalItf()?"Optional":"Mandatory";
        return row;
    }
    
    //==================================================================
    //
    // Public methods for Table interface.
    //
    //==================================================================
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.Table#getHeaders(org.objectweb.util.explorer.api.TreeView)
     */
    public String[] getHeaders(TreeView treeView) {
        return new String[] {"Name","Signature","Type","Cardinality","Contingency"};
    }
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.Table#getRows(org.objectweb.util.explorer.api.TreeView)
     */
    public Object[][] getRows(TreeView treeView) {
        ComponentType ct = (ComponentType)treeView.getSelectedObject();
        InterfaceType[] objects = ct.getFcInterfaceTypes();
        Object[][] values = new Object[objects.length][5];
        int j = 0;
        for (int i = 0; i < objects.length; i++)
            values[j++] = getValues(objects[i]);
        return values;
    }
}
