/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.api;

/**
 * Specialized icon provider for entities havinf life cycle properties.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public interface FractalIconProvider 
{
    /**
     * Called when the Fractal component is stopped.
     * @see org.objectweb.util.explorer.api.IconProvider#newIcon(java.lang.Object)
     * @param object The object you want to find the associated icon.
     * @return The associated icon.
     */
    public Object newStoppedIcon(Object object);
    
    /**
     * Called when the Fractal component is started.
     * @see org.objectweb.util.explorer.api.IconProvider#newIcon(java.lang.Object)
     * @param object The object you want to find the associated icon.
     * @return The associated icon.
     */
    public Object newStartedIcon(Object object);
}