/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Philippe Merle, Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.menu;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.fractal.explorer.context.ClientCollectionInterfaceContainer;
import org.objectweb.util.explorer.api.DropAction;
import org.objectweb.util.explorer.api.DropTreeView;

/**
 * Action for binding an interface on drop.
 * @author <a href="mailto:Philippe.Merle@lifl.fr">Philippe Merle</a>,
 *         <a href="mailto:Jerome.Moroy@lifl.fr">J�r�me Moroy</a>
 * @version 0.1
 */
public class BindInterfaceOnDropAction 
  implements DropAction 
{
    
    //==================================================================
    //
    // No internal state.
    //
    //==================================================================
    
    //==================================================================
    //
    // No constructor.
    // 
    //==================================================================
    
    //==================================================================
    //
    // Internal methods.
    //
    //==================================================================
    
    protected String getName(InterfaceType itf, int number) {
        String name = FcExplorer.getName(itf);
        return name + ((number<10)?"0":"") + number; 
    }
    
    //==================================================================
    //
    // No public method.
    //
    //==================================================================
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.DropAction#execute(org.objectweb.util.explorer.api.DropTreeView)
     */
    public void execute(DropTreeView dropTreeView) throws Exception {
        try {
            Interface itfToBind = (Interface) dropTreeView.getDragSourceObject();
            ClientCollectionInterfaceContainer icc = (ClientCollectionInterfaceContainer) dropTreeView.getSelectedObject();
            int nbConnectedItf = icc.getEntries(null).length;
            InterfaceType collectionItf = icc.getItf();
            Component component = icc.getComponent();
            BindingController bindingController = FcExplorer.getBindingController(component);
            bindingController.bindFc(getName(collectionItf, nbConnectedItf), itfToBind);          
        } catch(ClassCastException e) {
            throw new Exception("Interface expected!");
        }
    }
    
}


