/*====================================================================

Objectweb Fractal Explorer
Copyright (C) 2000-2005 INRIA & USTL - LIFL - GOAL
Contact: fractal@objectweb.org

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Initial developer(s): Jerome Moroy.
Contributor(s): ______________________________________.

---------------------------------------------------------------------
$Id$
====================================================================*/
package org.objectweb.fractal.explorer.rmi;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.rmi.registry.NamingService;
import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.Tree;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.swing.gui.api.DialogAction;
import org.objectweb.util.explorer.swing.gui.api.DialogBox;
import org.objectweb.util.explorer.swing.gui.lib.DefaultDialogBox;
import org.objectweb.util.explorer.swing.gui.lib.LabelBox;
import org.objectweb.util.explorer.swing.gui.lib.TreeBox;

/**
 *  
 * 
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * 
 * @version 0.1
 */
public class BindComponentAction 
  implements MenuItem, DialogAction 
{

    // ==================================================================
    //
    // Internal state.
    //
    // ==================================================================

    /** The label box. */
    protected LabelBox name_;
    
    /** The tree box. */
    protected TreeBox tree_;
        
    /** The Registry in which a component has to be bound. */
    protected NamingService namingService_;
    
    // ==================================================================
    //
    // No Constructors.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // No internal methods.
    //
    // ==================================================================

    /**
     * Create a box containing all the box to specify all the params.
     * @param dialogBox 
     * @param tree 
     */
    protected void createBox(DialogBox dialogBox, Tree tree) {
        this.name_ = new LabelBox("Name");
        dialogBox.addElementBox(this.name_);
        this.tree_ = new TreeBox(tree.duplicate());
        dialogBox.addElementBox(this.tree_);
    }
    
    // ==================================================================
    //
    // Public methods for MenuItem interface.
    //
    // ==================================================================
        
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#getStatus(org.objectweb.util.explorer.api.TreeView)
     */
    public int getStatus(TreeView treeView) {
        return MenuItem.ENABLED_STATUS;
    }

    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#actionPerformed(org.objectweb.util.explorer.api.MenuItemTreeView)
     */
    public void actionPerformed(MenuItemTreeView menuItemTreeView) throws Exception {
        namingService_ = (NamingService)menuItemTreeView.getSelectedObject();
        
        DialogBox dialog = new DefaultDialogBox("Bind a component");      
        createBox(dialog, menuItemTreeView.getTree());
        dialog.setValidateAction(this);
        dialog.show();
    }

    // ==================================================================
    //
    // Public methods for MenuItem interface.
    //
    // ==================================================================

    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.swing.gui.api.DialogAction#executeAction()
     */
    public void executeAction() throws Exception {
        String name = this.name_.getLabel();
        Component toBind = null;
        try {
            toBind = (Component)this.tree_.getObject();
        } catch (ClassCastException e) {
            throw new Exception("Component expected!");
        }
        this.namingService_.bind(name, toBind);
    }
}
