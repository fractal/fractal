/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): Romain Rouvoy______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.icon;

import javax.swing.Icon;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.SuperController;
import org.objectweb.fractal.explorer.FcExplorer;

/**
 * Provides a specific icon depending on the status of the component.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.2
 */
public class LifeCycleComponentIconProvider 
     extends InterfaceIconProvider
{
    /**
     * Load a couple of icons based on the Fc+name+(Started|Stopped) pattern.
     * @param name the pattern to retrieve.
     */
    protected void loadFc(String name) {
        getIcons().put(name, 
                       new Icon[] {load("icons/Fc"+name+"Started.png"),
                                   load("icons/Fc"+name+"Stopped.png")});
    }
    
    /** Empty constructor. */
    public LifeCycleComponentIconProvider() {
        super();
        loadFc("Primitive");
        loadFc("Composite");
        loadFc("Shared");
    }
    
    //==================================================================
    //
    // No internal method.
    //
    //==================================================================
    
    /**
     * Provides the couple of icons for a type of component (Primitive, Composite or Shared).
     * @param cpt the component to introspect.
     * @return the couple of icons associated to the component.
     */
    protected Icon[] loadIcon(Component cpt) {
        try {
            SuperController sc = FcExplorer.getSuperController(cpt);
            if (sc.getFcSuperComponents().length > 1)
                return (Icon[]) getIcons().get("Shared"); 
        } catch (NoSuchInterfaceException e) {
            // Nothing to do.
        }
        try {
            FcExplorer.getContentController(cpt);
            return (Icon[]) getIcons().get("Composite");
        } catch (NoSuchInterfaceException e) {
            // Nothing to do.
        }
        return (Icon[]) getIcons().get("Primitive"); 
    }
    
    //==================================================================
    //
    // Public methods for IconProvider interface.
    //
    //==================================================================
    
    /**
     * Provides the appropriate icon.
     * @param object the component to represent.
     * @return the associated icon.
     */
    public Object newIcon(Object object) {
        Component ci = (Component)object;
        Icon[] lcc_icons = loadIcon(ci);
        if (lcc_icons == null) return null;
        LifeCycleController lcc=null;
        try {
            lcc = FcExplorer.getLifeCycleController(ci);
        } catch (NoSuchInterfaceException e) {
            return lcc_icons[0];
        }
        String status = lcc.getFcState();
        if (status.equals(LifeCycleController.STARTED))
            return lcc_icons[0];
        else if (status.equals(LifeCycleController.STOPPED))
            return lcc_icons[1];
        else
            return null;
    }
}