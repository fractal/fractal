/*====================================================================

ObjectWeb Fractal Explorer
Copyright (C) 2000-2005 INRIA - USTL - LIFL - GOAL
Contact: fractal@objectweb.org

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Initial developer(s): La�titia Lafeuille.
Contributor(s): Philippe Merle.

====================================================================
$Id$
====================================================================*/

package org.objectweb.fractal.explorer.graph;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

import org.objectweb.util.explorer.swing.graph.CompositeVertex;
import org.objectweb.util.explorer.swing.graph.PrimitiveVertex;
import org.objectweb.util.explorer.swing.graph.VertexGraphicsInterface;
import org.objectweb.util.explorer.swing.graph.VertexType;

/**
 * 
 * @version 0.2
 */
public class VertexGraphics 
  implements VertexGraphicsInterface 
{

    /** Draw a dotted rectangle */
    private void drawDottedRect(Graphics g, int x, int y, int width, int height) {
        //draw horizontal lines
        int currentX = x;
        int currentY = y;
        while (currentX < x + width - 5) {
            g.drawLine(currentX, y, currentX + 5, y);
            g.drawLine(currentX, y + height, currentX + 5, y + height);
            currentX = currentX + 10;
        }
        //draw vertical lines
        while (currentY < y + height - 5) {
            g.drawLine(x, currentY, x, currentY + 5);
            g.drawLine(x + width, currentY, x + width, currentY + 5);
            currentY = currentY + 10;
        }
    }

    public Font getTextFont() {
        return new Font("SansSerif", Font.BOLD, 10);
    }

    /** Returns the vertex default size */
    public Dimension getDefaultSize() {
        return new Dimension(110, 60);
    }

    /** Returns the open composite (super composite) size */
    public Dimension getSuperCompositeSize() {
        return new Dimension(10, 10);
    }

    /** Draw the specific vertex into the specific grahics g */
    public void drawVertex(Graphics g, String vertexType, Dimension d, PrimitiveVertex vertex) {
        g.setColor(Color.DARK_GRAY);
        if (!vertex.isStarted())
            g.setColor(new Color(149, 0, 0));

        //if the vertex is primitive
        if (vertexType.equals(VertexType.PRIMITIF_VERTEX)) {
            int size = VertexGraphicsInterface.PRIMITIVE_MEMBRANE_SIZE - 1;
            if (vertex.isShared()) {
                drawDottedRect(g, 0, 0, d.width - 1, d.height - 1);
                drawDottedRect(g, size, size, d.width - 3, d.height - 3);
            } else {
                g.drawRect(0, 0, d.width - 1, d.height - 1);
                g.drawRect(size, size, d.width - 3, d.height - 3);
            }
        }

        //else if the vertex is composite
        else if (vertexType.equals(VertexType.COMPOSITE_VERTEX)) {
            if (!((CompositeVertex) vertex).isOpen()) {
                //if the vertex isn't open
                int size = VertexGraphicsInterface.PRIMITIVE_MEMBRANE_SIZE - 1;
                if (vertex.isShared()) {
                    drawDottedRect(g, 0, 0, d.width - 1, d.height - 1);
                    drawDottedRect(g, size, size, d.width - size - 2, d.height
                            - size - 2);
                } else {
                    g.drawRect(0, 0, d.width - 1, d.height - 1);
                    g.drawRect(size, size, d.width - size - 2, d.height - size
                            - 2);
                }
                g.setColor(Color.PINK);
                g.fillRect(size + 1, size + 1, d.width - (size + 1) - 2,
                        d.height - (size + 1) - 2);
            }

            // if the vertex is the open vertex (super composite)
            else {
                int size = VertexGraphicsInterface.COMPOSITE_MEMBRANE_SIZE;
                if (vertex.isShared()) {
                    drawDottedRect(g, 0, 0, d.width - 1, d.height - 1);
                    drawDottedRect(g, 1, 1, d.width - 3, d.height - 3);
                    drawDottedRect(g, size, size, d.width - size * 2 - 1,
                            d.height - size * 2 - 1);
                    drawDottedRect(g, size + 1, size + 1, d.width - size * 2
                            - 3, d.height - size * 2 - 3);
                } else {
                    g.drawRect(0, 0, d.width - 1, d.height - 1);
                    g.drawRect(1, 1, d.width - 3, d.height - 3);
                    g.drawRect(size, size, d.width - size * 2 - 1, d.height
                            - size * 2 - 1);
                    g.drawRect(size + 1, size + 1, d.width - size * 2 - 3,
                            d.height - size * 2 - 3);
                }
            }
        }
    }
}
