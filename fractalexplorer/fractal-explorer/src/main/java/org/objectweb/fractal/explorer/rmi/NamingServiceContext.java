/*====================================================================

Objectweb Fractal Explorer
Copyright (C) 2000-2005 INRIA & USTL - LIFL - GOAL
Contact: fractal@objectweb.org

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Initial developer(s): Jerome Moroy.
Contributor(s): ______________________________________.

---------------------------------------------------------------------
$Id$
====================================================================*/
package org.objectweb.fractal.explorer.rmi;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.rmi.registry.NamingService;
import org.objectweb.util.explorer.api.Context;
import org.objectweb.util.explorer.api.Entry;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;

/**
 * The context wraps Fractal RMI Registries context and allows one to obain
 * the list of registered Registries. 
 * 
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * 
 * @version 0.1
 */
public class NamingServiceContext 
  implements Context 
{
    
    // ==================================================================
    //
    // Internal state.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // No Constructors.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // No internal methods.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // Public methods for Context interface.
    //
    // ==================================================================
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.Context#getEntries(java.lang.Object)
     */
    public Entry[] getEntries(Object entry) {
        java.util.List values = new java.util.ArrayList();
        NamingService ns = (NamingService)entry;
        String[] nsEntries = ns.list();
        for (int i = 0; i < nsEntries.length; i++) {
            Component comp = ns.lookup(nsEntries[i]);
            values.add(new DefaultEntry(nsEntries[i], comp));
// The component is registered even if no name-controller is available.
//            try{
//                Fractal.getNameController(comp);
//                values.add(new DefaultEntry(nsEntries[i], comp));
//            } catch (Exception e) {
//                values.add(new DefaultEntry(nsEntries[i], new UnknownBinding()));
//            }
        }
        return (Entry[])values.toArray(new Entry[0]); 
    }

}
