/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Romain Rouvoy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.context;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.util.explorer.api.Entry;
import org.objectweb.util.explorer.core.common.api.ContextContainer;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;


/**
 * Component context for advanced users (view of internal components).
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version 0.1
 */
public class ComponentContextForAdvanced 
     extends ComponentContextForService 
{
    // ==================================================================
    //
    // No internal state.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // No Constructors.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // Internal methods.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // Public methods for interface Context.
    //
    // ==================================================================
    
    /**
     * Returns an array containing the entries contained by the target context.
     * @return A new array of entry.
     */
    public Entry[] getEntries(Object object) {
        Component ci = (Component)object;
        Entry[] entries = super.getEntries(object);
        try {
            ContentController cc = FcExplorer.getContentController(ci);
            ContextContainer container = new InternalComponentContainer();            
            Component[] components = cc.getFcSubComponents();
            for (int i=0 ; i<components.length ; i++) {
                container.addEntry(FcExplorer.getName(components[i]),
                                   components[i]);
            }
            
            Entry[] values = new Entry[entries.length + 1];
            values[0] = new DefaultEntry("components", container);
            System.arraycopy(entries,0,values,1,entries.length);
            return values;
        } catch (NoSuchInterfaceException e) {
            return entries;
        } 
    }
}
