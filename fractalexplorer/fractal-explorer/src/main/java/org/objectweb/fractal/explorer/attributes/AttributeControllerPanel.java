/**
 * Dream
 * Copyright (C) 2003-2004 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Jakub Kornas
 * Contributor(s): 
 */

package org.objectweb.fractal.explorer.attributes;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import org.objectweb.util.explorer.api.Panel;
import org.objectweb.util.explorer.api.TreeView;

/**
 * A <code>Panel</code> interface implementation used to display, monitor and
 * modify component attributes.
 */
public class AttributeControllerPanel extends JPanel implements Panel
{
  protected AttributesTable attributesTable     = null;
  protected JLabel          controllerNameLabel = null;

  protected JPanel          jPanel2             = null;
  protected JPanel          jPanel4             = null;

  /**
   * This is the default constructor
   */
  public AttributeControllerPanel()
  {
    super();
    initialize();
  }

  /**
   * This method initializes this.
   */
  protected void initialize()
  {
    controllerNameLabel = new JLabel();
    controllerNameLabel.setHorizontalAlignment(SwingConstants.LEFT);

    GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
    GridBagConstraints gridBagConstraints2 = new GridBagConstraints();

    this.setLayout(new GridBagLayout());
    this.setBackground(Color.WHITE);

    gridBagConstraints1.gridx = 0;
    gridBagConstraints1.gridy = 0;
    gridBagConstraints1.fill = GridBagConstraints.HORIZONTAL;
    gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
    gridBagConstraints1.weighty = 1.0D;
    gridBagConstraints1.weightx = 1.0D;
    gridBagConstraints1.anchor = GridBagConstraints.NORTH;

    gridBagConstraints2.gridx = 0;
    gridBagConstraints2.gridy = 1;
    gridBagConstraints2.fill = GridBagConstraints.HORIZONTAL;
    gridBagConstraints2.insets = new java.awt.Insets(10, 10, 10, 10);
    gridBagConstraints2.weighty = 1.0D;
    gridBagConstraints2.anchor = GridBagConstraints.NORTH;

//    this.add(getJPanel1(), gridBagConstraints1);
    this.add(getJPanel2(), gridBagConstraints1);
    //getJPanel2();
  }

  //---------------------------------------------------------------------------
  // Implementation of the Panel interface
  // ---------------------------------------------------------------------------

  /**
   * Invokes just after instantiation.
   */
  public void selected(TreeView treeView)
  {
    Object selectedNode = treeView.getSelectedObject();
    attributesTable = new AttributesTable(selectedNode, this);

    jPanel4.add(attributesTable.getTableHeader());
    jPanel4.add(attributesTable, null);

    String controlledInterfaceName = AttributesTableModelBuilder
        .getControlledInterfaceName();

    controllerNameLabel.setText(controlledInterfaceName);
  }

  /**
   * Returns this panel.
   */
  public Object getPanel()
  {
    return this;
  }

  /**
   * Invokes just before removing.
   */
  public void unselected(TreeView arg0)
  {
    attributesTable.destroyAttributeMonitors();
  }

  /**
   * This method initializes jPanel2
   * 
   * @return javax.swing.JPanel
   */
  private JPanel getJPanel2()
  {
    if (jPanel2 == null)
    {
      jPanel2 = new JPanel();
      jPanel2.setLayout(new GridLayout());
      jPanel2.setBorder(BorderFactory.createTitledBorder(null,
          "Controlled Attributes", TitledBorder.CENTER,
          TitledBorder.DEFAULT_POSITION, null, null));
      jPanel2.setBackground(java.awt.Color.white);
      jPanel2.add(getJPanel4());
    }
    return jPanel2;
  }

  /**
   * This method initializes jPanel4
   * 
   * @return javax.swing.JPanel
   */
  private JPanel getJPanel4()
  {
    if (jPanel4 == null)
    {
      jPanel4 = new JPanel();
      jPanel4.setLayout(new BoxLayout(jPanel4, BoxLayout.Y_AXIS));
      //jPanel4.setLayout(new GridLayout());
      jPanel4.setBorder(javax.swing.BorderFactory
          .createBevelBorder(BevelBorder.LOWERED));
      jPanel4.setBackground(java.awt.Color.white);
    }
    return jPanel4;
  }
}