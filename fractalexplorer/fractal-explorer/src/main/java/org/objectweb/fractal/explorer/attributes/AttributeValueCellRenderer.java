/**
 * Dream
 * Copyright (C) 2003-2004 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Jakub Kornas
 * Contributor(s): 
 */

package org.objectweb.fractal.explorer.attributes;

import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * A custom <code>TableCellRenderer</code> for displaying attribute values in
 * the attributes table.
 */
public class AttributeValueCellRenderer extends DefaultTableCellRenderer
{
  private JCheckBox booleanResult = new JCheckBox();

  /**
   * @see DefaultTableCellRenderer#getTableCellRendererComponent(JTable, Object,
   *      boolean, boolean, int, int)
   */
  public Component getTableCellRendererComponent(JTable table, Object value,
      boolean isSelected, boolean hasFocus, int row, int column)
  {
    if (value instanceof Boolean)
    {
      if (isSelected)
      {
        booleanResult.setForeground(table.getSelectionForeground());
        booleanResult.setBackground(table.getSelectionBackground());
      }
      else
      {
        booleanResult.setForeground(table.getForeground());
        booleanResult.setBackground(table.getBackground());
      }

      boolean boolValue = ((Boolean) value).booleanValue();
      booleanResult.setSelected(boolValue);
      booleanResult.setHorizontalAlignment(SwingConstants.CENTER);

      return booleanResult;
    }
    JLabel defaultResult = (JLabel) super.getTableCellRendererComponent(table,
        value, isSelected, hasFocus, row, column);
    defaultResult.setHorizontalAlignment(SwingConstants.RIGHT);

    return defaultResult;
  }
}