/*====================================================================

GoTM: GoTM is an open Transaction Monitor
Copyright (C) 2003-2005 INRIA - Jacquard & USTL - LIFL - GOAL
Contact: gotm-team@objectweb.org

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Initial developer(s): Romain Rouvoy.
Contributor(s): .

---------------------------------------------------------------------
$Id$
====================================================================*/
package org.objectweb.fractal.explorer.context;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.util.explorer.api.Context;
import org.objectweb.util.explorer.api.Entry;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;

/**
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @created 7 sept. 2005
 * @modified $Date$
 * @version $Revision$
 * @fractal.itf 
 */
public class InterfaceContext
        implements Context {

    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.Context#getEntries(java.lang.Object)
     */
    public Entry[] getEntries(Object object) {
        Interface itf = (Interface)object;
        InterfaceType type = (InterfaceType) itf.getFcItfType();
        if (type.isFcClientItf()) return new Entry[0];
        if (type.getFcItfName().endsWith("-controller")) return new Entry[0];
        String signature = type.getFcItfSignature();
        Class[] itfs = object.getClass().getInterfaces();
        List entries = new ArrayList();
        for (int i=0;i<itfs.length;i++){
            if (signature.equals(itfs[i].getName()))
                entries.add(new DefaultEntry(signature,itfs[i]));
        }
        return (Entry[]) entries.toArray(new Entry[0]);
    }
}