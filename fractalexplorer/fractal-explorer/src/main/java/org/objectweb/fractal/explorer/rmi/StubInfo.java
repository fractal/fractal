/*====================================================================
 
ObjectWeb Fractal Explorer
Copyright (C) 2005-2006 INRIA
Contact: fractal@objectweb.org
 
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or any later version.
 
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
 
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA
 
Initial developer(s): Philippe Merle.
Contributor(s): ______________________________________.
 
====================================================================
$Id$
====================================================================*/

package org.objectweb.fractal.explorer.rmi;

import org.objectweb.fractal.explorer.info.InterfaceInfo;

import org.objectweb.fractal.rmi.stub.Stub;

import org.objectweb.util.explorer.api.TreeView;

/**
 * Computes the signature of the interface + the localisation of the remote interface.
 *
 * @author <a href="mailto:Philippe.Merle@inria.fr">Philippe Merle</a>
 * @version $Revision$
 */

public class StubInfo
     extends InterfaceInfo
{
    // ==================================================================
    //
    // Public methods for interface org.objectweb.util.explorer.api.Info
    //
    // ==================================================================
    
    /**
     * @see org.objectweb.util.explorer.api.Info#getInfo(org.objectweb.util.explorer.api.TreeView)
     */
    public String
    getInfo(final TreeView treeView)
    {
        StringBuffer result = new StringBuffer();

        // Append the InterfaceInfo.getInfo() firstly.
        result.append(super.getInfo(treeView));

        // Obtain the selected stub.
        Stub stub = (Stub)treeView.getSelectedObject();

        // Obtain its textual description, i.e. Id[host,port,Id[oid]]
        String info = stub.getIdentifiers()[0].toString();

	// Determine the host.
        int startIndex = 3;
        int endIndex = info.indexOf(',', startIndex);

        // Append the host to the result.
        result.append(" [host=");
        result.append(info.substring(startIndex, endIndex));

	// Determine the port.
        startIndex = endIndex+1;
        endIndex = info.indexOf(',', startIndex);

        // Append the port to the result.
        result.append(",port=");
        result.append(info.substring(startIndex, endIndex));

	// Determine the oid.
        startIndex = endIndex+4;
        endIndex = info.length()-2;

        // Append the oid to the result.
        result.append(",oid=");
        result.append(info.substring(startIndex, endIndex));

        result.append(']');
        return result.toString();
    }    
}
