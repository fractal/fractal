/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Romain Rouvoy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.context;

import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.type.InterfaceType;

/**
 * Interface wrapper factory.
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version 0.1
 */
public class InterfaceWrapperFactory 
{
    /** Factory singleton. */
    protected static final InterfaceWrapperFactory singleton = new InterfaceWrapperFactory();
    
    /**
     * Provides the instance of the default factory.
     * @return the default factory.
     */
    public static InterfaceWrapperFactory singleton() {
        return singleton ;
    }
    
    /**
     * Provides the appropriate wrapper for the requested interface.
     * @param itf the interface to wrap.
     * @return the associated interface wrapper.
     */
    public InterfaceWrapper getWrapper(Interface itf) {
        InterfaceType type = (InterfaceType) itf.getFcItfType();
        if (type.isFcClientItf())
/*            if (type.isFcCollectionItf())
                return new ClientCollectionInterfaceWrapper(itf);
            else*/
                return new ClientInterfaceWrapper(itf);
        return new InterfaceWrapper(itf);
    }
}
