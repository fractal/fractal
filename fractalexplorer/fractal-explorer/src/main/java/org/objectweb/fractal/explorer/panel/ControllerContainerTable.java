/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.panel;

import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.explorer.context.ControllerContainer;
import org.objectweb.fractal.explorer.lib.SignatureWrapper;
import org.objectweb.util.explorer.api.Entry;
import org.objectweb.util.explorer.api.Table;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;

/**
 * Displays the controller interface.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class ControllerContainerTable
  implements Table 
{
    
    //==================================================================
    //
    // Internal states.
    //
    //==================================================================
    
    //==================================================================
    //
    // No constructor.
    //
    //==================================================================
    
    // ==================================================================
    //
    // Internal methods.
    //
    // ==================================================================
    
    /** 
     * Provides a row containing the controller and its signature.
     * @param entry 
     * @return row containing the controller and its signature.
     */
    protected Object[] getValues(Entry entry){
        Interface itf = null;
        //if (Wrapper.class.isAssignableFrom(entry.getValue().getClass()))
        //    itf = (Interface)((Wrapper)entry.getValue()).getWrapped();
        /*else*/
        if (Interface.class.isAssignableFrom(entry.getValue().getClass()))
            itf = (Interface)entry.getValue();
        Object[] values = new Object[2];
        String signature = "";
        if(itf!=null)
            signature = ((InterfaceType)itf.getFcItfType()).getFcItfSignature();
        values[0] = entry;
        values[1] = new DefaultEntry(signature, new SignatureWrapper(signature));
        return values;
    }
    
    //==================================================================
    //
    // Public methods for Table interface.
    //
    //==================================================================
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.Table#getHeaders(org.objectweb.util.explorer.api.TreeView)
     */
    public String[] getHeaders(TreeView treeView) {
        return new String[] {"Controller","Signature"};
    }
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.Table#getRows(org.objectweb.util.explorer.api.TreeView)
     */
    public Object[][] getRows(TreeView treeView) {
        ControllerContainer cc = (ControllerContainer)treeView.getSelectedObject();
        Entry[] entries = cc.getEntries(null);
        Object[][] values = new Object[entries.length][2];
        for (int i = 0 ; i < entries.length ; i++) {
            values[i] = getValues(entries[i]);
        }                
        return values;
    }
}
