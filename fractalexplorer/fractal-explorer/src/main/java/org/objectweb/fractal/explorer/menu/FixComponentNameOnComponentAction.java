/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.menu;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.util.explorer.api.MenuItemTreeView;

/**
 * This action allows to rename a component.
 *
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class FixComponentNameOnComponentAction 
     extends FixComponentNameAction
{
    //==================================================================
    //
    // No internal state.
    //
    //==================================================================
    
    //==================================================================
    //
    // No constructor.
    //
    //==================================================================
    
    //==================================================================
    //
    // Internal methods.
    //
    //==================================================================
    
    /**
     * Provides the name controller of the given component.
     */
    protected NameController getNameController(MenuItemTreeView e){
        Component component = (Component)e.getSelectedObject();
        try{
            return FcExplorer.getNameController(component);
        } catch (NoSuchInterfaceException ex) {
            return null;
        }        
    }
    
    //==================================================================
    //
    // Public methods for ExtendedActionListener interface.
    //
    //==================================================================
    
    //==================================================================
    //
    // Public methods for DialogAction interface.
    //
    //==================================================================
    
    /**
     * Executes an action
     */
    public void executeAction() throws Exception {
        super.executeAction();
        // TODO: Add the renameSelectedNode method
        //throw new RuntimeException("Only Partly Implemented!");
        tree_.renameSelectedNode(oldName_,nameBox_.getLabel());
    }
    
}