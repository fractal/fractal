/*====================================================================

Objectweb Fractal Explorer
Copyright (C) 2000-2005 INRIA & USTL - LIFL - GOAL
Contact: fractal@objectweb.org

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Initial developer(s): Jerome Moroy.
Contributor(s): ______________________________________.

---------------------------------------------------------------------
$Id$
====================================================================*/
package org.objectweb.fractal.explorer.rmi;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.objectweb.fractal.rmi.registry.NamingService;
import org.objectweb.fractal.rmi.registry.Registry;
import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.core.common.api.ContextContainer;
import org.objectweb.util.explorer.swing.gui.api.DialogAction;
import org.objectweb.util.explorer.swing.gui.api.DialogBox;
import org.objectweb.util.explorer.swing.gui.lib.DefaultDialogBox;
import org.objectweb.util.explorer.swing.gui.lib.LabelBox;

/**
 * This action allows one to explore a new Fractal RMI registry. 
 * 
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * 
 * @version 0.1
 */
public class ConnectToRegistryAction 
  implements MenuItem, DialogAction 
{

    // ==================================================================
    //
    // Internal state.
    //
    // ==================================================================

    /** The label box. */
    protected LabelBox host_, port_;
    
    /** The context container in which the Registry has to be added. */
    protected ContextContainer cc_;
    
    // ==================================================================
    //
    // No Constructors.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // No internal methods.
    //
    // ==================================================================

    /**
     * Create a box containing all the box to specify all the params.
     * @param dialogBox 
     */
    protected void createBox(DialogBox dialogBox) {
        String hostName = null;
        try {
            hostName = InetAddress.getLocalHost().getCanonicalHostName();
        } catch (UnknownHostException e) {
            // Nothing to do.
        }
        this.host_ = new LabelBox("Host name", hostName!=null?hostName:"");
        dialogBox.addElementBox(this.host_);
        this.port_ = new LabelBox("Port", "" + Registry.DEFAULT_PORT);
        dialogBox.addElementBox(this.port_);
    }
    
    // ==================================================================
    //
    // Public methods for MenuItem interface.
    //
    // ==================================================================

    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#getStatus(org.objectweb.util.explorer.api.TreeView)
     */
    public int getStatus(TreeView treeView) {
        return MenuItem.ENABLED_STATUS;
    }

    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.MenuItem#actionPerformed(org.objectweb.util.explorer.api.MenuItemTreeView)
     */
    public void actionPerformed(MenuItemTreeView menuItemTreeView) throws Exception {
        cc_ = (ContextContainer)menuItemTreeView.getSelectedObject();
        
        DialogBox dialog = new DefaultDialogBox("Connect to a registry");      
        createBox(dialog);
        dialog.setValidateAction(this);
        dialog.show();
    }

    // ==================================================================
    //
    // Public methods for MenuItem interface.
    //
    // ==================================================================

    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.swing.gui.api.DialogAction#executeAction()
     */
    public void executeAction() throws Exception {
        String hostName = this.host_.getLabel();
        int port = Integer.parseInt(this.port_.getLabel());
        String name = "Registry " + hostName + ":" + port;
        NamingService ns = Registry.getRegistry(hostName, port);
        try{
            // Emulates a ping() call.
            ns.list();
            this.cc_.addEntry(name,ns);
        } catch (Exception e) {
            throw new Exception("This NamingService is not available!");
        }
        
    }
}
