/**
 * Dream
 * Copyright (C) 2003-2004 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Jakub Kornas
 * Contributor(s): 
 */

package org.objectweb.fractal.explorer.panel;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;

/**
 * A custom <code>TableCellRenderer</code> for displaying the state of
 * <code>JButton</code> s used to launch attribute monitoring threads.
 */
public class MonitorCellRenderer extends JButton implements TableCellRenderer
{

  /**
   * A default constructor.  
   */
  public MonitorCellRenderer()
  {
    setOpaque(true);
  }

  /**
   * @see TableCellRenderer#getTableCellRendererComponent(JTable, Object,
   *      boolean, boolean, int, int)
   */
  public Component getTableCellRendererComponent(JTable table, Object value,
      boolean isSelected, boolean hasFocus, int row, int column)
  {
    if (isSelected)
    {
      setForeground(table.getSelectionForeground());
      setBackground(table.getSelectionBackground());
    }
    else
    {
      setForeground(table.getForeground());
      setBackground(UIManager.getColor("Button.background"));
    }
    setText((value == null) ? "" : value.toString());
    setFont(table.getFont());
    return this;
  }
}

