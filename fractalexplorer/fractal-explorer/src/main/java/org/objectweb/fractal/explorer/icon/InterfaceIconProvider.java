/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Romain Rouvoy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.icon;

import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.objectweb.util.explorer.api.IconProvider;

/**
 * 
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version 0.1
 */
public abstract class InterfaceIconProvider 
           implements IconProvider
{
    // ==================================================================
    //
    // Internal states.
    //
    // ==================================================================
    
    /** The different icons. */

    protected Map icons ;
    
    /**
     * @return Returns the icons.
     */
    protected Map getIcons() {
        return icons;
    }
    /**
     * @param icons The icons to set.
     */
    protected void setIcons(Map icons) {
        this.icons = icons;
    }

    /**
     * Load a icon from the classpath.
     * @param path the path for finding the requested icon.
     * @return the instance of icon found (null if not found).
     */
    protected Icon load(String path) {
        Icon icon = new ImageIcon(Thread.currentThread().getContextClassLoader().getResource(path));
        if (icon == null)
            System.err.println(path+" icon not found!");
        return icon ;
    }
    
    /**
     * Load a couple of icons based on the Fc+name+(Started|Stopped) pattern.
     * @param name the pattern to retrieve.
     */
    protected void loadFc(String name) {
        getIcons().put(name, load("icons/Fc"+name+".png"));
    }
    
    /**
     * Default Icon provider constructor.
     */
    protected InterfaceIconProvider() {
        setIcons(new HashMap());
    }
}
