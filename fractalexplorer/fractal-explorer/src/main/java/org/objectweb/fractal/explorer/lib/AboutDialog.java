/*====================================================================

Objectweb Fractal Explorer
Copyright (C) 2000-2005 INRIA & USTL - LIFL - GOAL
Contact: fractal@objectweb.org

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Initial developer(s): Jerome Moroy.
Contributor(s): ______________________________________.

---------------------------------------------------------------------
$Id$
====================================================================*/
package org.objectweb.fractal.explorer.lib;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.objectweb.fractal.explorer.FcExplorer;

/**
 * Displays the "Fractal Explorer About" box.
 *
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">J�r�me Moroy</a>,
 *         <a href="mailto:Philippe.Merle@lifl.fr">Philippe Merle</a>.  
 * 
 * @version 0.1
 */
public class AboutDialog 
     extends JDialog 
{

    //==================================================================
    //
    // Internal States.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // Constructors.
    //
    // ==================================================================

    /**
     * 
     * @param owner The parent frame.
     */
    public AboutDialog(Frame owner){
        super(owner, "About Fractal Explorer", true);

        JPanel titlePanel = new JPanel();
        titlePanel.setBackground(Color.white);
        
        Box box = Box.createVerticalBox();
        box.add(new TitlePanel("Fractal Explorer", new Font("TimesRoman", Font.BOLD, 20)));
        box.add(new TitlePanel("for Fractal Component Model", new Font("TimesRoman", Font.ITALIC, 16)));
        
        Box imgBox = Box.createHorizontalBox();
        imgBox.add(Box.createHorizontalGlue());
        imgBox.add(new JLabel(FcExplorer.createIcon("icons/fractal.jpg")));
        imgBox.add(Box.createHorizontalGlue());
        
        box.add(imgBox);
        box.add(new TitlePanel("Copyright � 1999-2005, ObjectWeb Consortium", new Font("TimesRoman", Font.ITALIC, 12)));
        titlePanel.add(box);
        getContentPane().add(titlePanel,BorderLayout.CENTER);        
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setBackground(Color.white);
        JButton button = new JButton("OK");
        button.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                AboutDialog.this.hide();
            }
        });
        button.setPreferredSize(new Dimension(100,25));
        buttonPanel.add(button);
        getContentPane().add(buttonPanel,BorderLayout.SOUTH);
        
        pack();    

        // Locates the frame
        setLocation(getLocation(owner));

    }
    
    // ==================================================================
    //
    // Internal methods.
    //
    // ==================================================================

    /**
     * Aligns the JDialog with the parent frame, if possible.
     * @param owner The parent frame.
     * @return The location of the JDialog.
     */
    protected Point getLocation(Frame owner) {
        Point location = new Point();
        Point p = owner.getLocation();
        Dimension aboutSize = getSize();
        Dimension parentSize = owner.getSize();
        if (aboutSize.width>(parentSize.width)){
            location.x = p.x+15;
        } else {
            location.x = p.x + ((parentSize.width - aboutSize.width)/2);
        }
         
        if (aboutSize.height>(parentSize.height)){
            location.y = p.y+15;
        } else {
            location.y = p.y + ((parentSize.height - aboutSize.height)/2);
        }
        return location;
    }
    
    // ==================================================================
    //
    // Public methods for ... interface. 
    //
    // ==================================================================    
    
    // ==================================================================
    //
    // Inner classes. 
    //
    // ==================================================================    
    
    class TitlePanel extends JPanel {
        /**
         * @param title
         * @param f
         */
        public TitlePanel(String title, Font f){
            setBackground(Color.white);
            JLabel label = new JLabel();
            label.setFont(f);
            label.setText(title);
            add(label);
        }
    }
    
}