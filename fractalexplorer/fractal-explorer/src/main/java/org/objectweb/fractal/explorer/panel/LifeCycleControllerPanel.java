/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.panel;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.util.explorer.api.Panel;
import org.objectweb.util.explorer.api.TreeView;

/**
 * This panel allows to start/stop the component. 
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class LifeCycleControllerPanel 
  implements Panel 
{
    // ==================================================================
    //
    // Internal states.
    //
    // ==================================================================
    
    protected TreeView treeView_ = null;
    
    protected JPanel panel_ = null;
    
    // ==================================================================
    //
    // Constructors.
    //
    // ==================================================================
    
    /**
     * Empty constructor.
     */
    public LifeCycleControllerPanel() {
        panel_ = new JPanel();
        panel_.setBackground(Color.white);
    }
    
    // ==================================================================
    //
    // No internal methods.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // Public methods for ViewPanel interface.
    //
    // ==================================================================
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.Panel#getPanel()
     */
    public Object getPanel(){
        return panel_;
    }
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.Panel#selected(org.objectweb.util.explorer.api.TreeView)
     */
    public void selected(TreeView treeView) {
        treeView_ = treeView;
        LifeCycleController lifeCycleController = (LifeCycleController)treeView.getSelectedObject();
        String value = null;
        String status = lifeCycleController.getFcState();
        
        Box box_ = Box.createVerticalBox();
        box_.add(Box.createVerticalStrut(10));
        Box labelBox = Box.createHorizontalBox();
        labelBox.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelBox.add(Box.createHorizontalGlue());
        JLabel labelValue = new JLabel("Component status : " + status);
        labelBox.add(labelValue);
        labelBox.add(Box.createHorizontalGlue());
        box_.add(labelBox);
        
        if (status.equals("STARTED"))
            value="Stop";                
        else if (status.equals("STOPPED"))
            value="Start";
        box_.add(Box.createVerticalStrut(10));
        Box buttonBox = Box.createHorizontalBox();
        buttonBox.add(Box.createHorizontalGlue());
        JButton bt = new JButton(new StartStopAction(value,lifeCycleController));
        bt.setPreferredSize(new Dimension(100,20));
        buttonBox.add(bt);
        buttonBox.add(Box.createHorizontalGlue());
        box_.add(buttonBox);
        box_.add(Box.createVerticalGlue());
        panel_.add(box_);
    }
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.Panel#unselected(org.objectweb.util.explorer.api.TreeView)
     */
    public void unselected(TreeView treeView) {
        // Empty block.
    }
    
    // ==================================================================
    //
    // Internal classes.
    //
    // ==================================================================
    
    /**
     * 
     */
    protected class StartStopAction extends AbstractAction {
        
        /** The associated LifeCycleController */
        protected LifeCycleController lifeCycleController_;
        
        /** */
        protected String value_;
        
        /**
         * Default Constructor
         */
        public StartStopAction(String value, LifeCycleController lcc){
            super(value);            
            value_ = value;
            lifeCycleController_ = lcc;
        }
        
        /**
         * The action 
         */
        public void actionPerformed(ActionEvent e){
            try {
                if (value_.equals("Start"))
                    lifeCycleController_.startFc();
                else if (value_.equals("Stop"))
                    lifeCycleController_.stopFc();
            } catch (IllegalLifeCycleException e1) {
                e1.printStackTrace();
            }
            LifeCycleControllerPanel.this.treeView_.getTree().refreshAll();
        }
    }
    
}