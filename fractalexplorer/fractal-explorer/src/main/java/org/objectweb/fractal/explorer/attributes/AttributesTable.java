/**
 * Dream
 * Copyright (C) 2003-2004 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Jakub Kornas
 * Contributor(s): 
 */

package org.objectweb.fractal.explorer.attributes;

import java.awt.Point;
import java.awt.event.MouseEvent;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import org.objectweb.fractal.explorer.panel.MonitorCellEditor;
import org.objectweb.fractal.explorer.panel.MonitorCellRenderer;

/**
 * An extended <code>JTable</code> that represents the attributes controlled
 * by the attribute controller.
 */
public class AttributesTable
        extends JTable {
    protected Object attributeObject = null;

    private TableCellEditor monitorEditor = null;

    private TableCellRenderer monitorRenderer = null;

    private TableCellEditor attributeValueDefaultEditor = null;

    private TableCellEditor attributeValueBooleanEditor = null;

    private TableCellRenderer attributeValueRenderer = null;

    /**
     * Creates a new attributes table.
     * 
     * @param selectedNode
     *            an attribute controller.
     * @param owner
     *            the <code>JPanel</code> to which this table belongs.
     */
    public AttributesTable(Object selectedNode, JPanel owner) {
        this.attributeObject = selectedNode;

        this.setColumnModel(new AttributesTableColumnModel());
        this.setModel(AttributesTableModelBuilder
                .buildAttributesTableModel(attributeObject));

        dataModel.addTableModelListener(new TableModelListener() {
            public void tableChanged(TableModelEvent e) {
                int row = e.getFirstRow();
                int column = e.getColumn();
                if (column == 1) {
                    if (getValueAt(row, 2).equals(
                            AttributesTableModel.MONITOR_ACTIONS[1])) {
                        return;
                    }

                    String methodName = "set" + getValueAt(row, 0);
                    Object newValue = dataModel.getValueAt(row, column);
                    boolean invoke = false;
                    Class invokeType = null;
                    if (newValue instanceof String) {
                        invokeType = String.class;
                        invoke = true;
                    } else if (newValue instanceof Integer) {
                        invokeType = int.class;
                        invoke = true;
                    } else if (newValue instanceof Boolean) {
                        invokeType = boolean.class;
                        invoke = true;
                    } else if (newValue instanceof Short) {
                        invokeType = short.class;
                        invoke = true;
                    } else if (newValue instanceof Long) {
                        invokeType = long.class;
                        invoke = true;
                    } else if (newValue instanceof Double) {
                        invokeType = double.class;
                        invoke = true;
                    } else if (newValue instanceof Float) {
                        invokeType = float.class;
                        invoke = true;
                    }
                    if (invoke) {
                        Class attributeClass = attributeObject.getClass();
                        try {
                            attributeClass.getMethod(methodName,
                                    new Class[] { invokeType }).invoke(
                                    attributeObject, new Object[] { newValue });
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }
        });

        this.monitorEditor = new MonitorCellEditor(new JCheckBox());
        this.monitorRenderer = new MonitorCellRenderer();

        this.attributeValueDefaultEditor = new AttributeValueDefaultCellEditor(
                new JTextField());
        this.attributeValueBooleanEditor = new AttributeValueBooleanCellEditor(
                new JCheckBox());
        this.attributeValueRenderer = new AttributeValueCellRenderer();

        this.getColumn("AttributeValue").setCellRenderer(
                this.attributeValueRenderer);
        this.getColumn("Monitor").setCellRenderer(this.monitorRenderer);
    }

    /**
     * @see JTable#getDragEnabled()
     */
    public boolean getDragEnabled() {
        return false;
    }

    /**
     * @see JTable#getCellEditor(int, int)
     */
    public TableCellEditor getCellEditor(int row, int col) {
        if (col == 1) {
            Object value = getValueAt(row, col);
            if (value instanceof Boolean)
                return attributeValueBooleanEditor;
            return attributeValueDefaultEditor;
        } else if (col == 2) {
            return monitorEditor;
        } else {
            return super.getCellEditor(row, col);
        }
    }

    /**
     * @see JTable#getToolTipText(MouseEvent)
     */
    public String getToolTipText(MouseEvent e) {
        String tip = null;
        Point p = e.getPoint();
        int rowIndex = rowAtPoint(p);
        int colIndex = columnAtPoint(p);
        int realColumnIndex = convertColumnIndexToModel(colIndex);

        if (realColumnIndex == 1) {
            TableModel model = getModel();
            String attributeName = (String) model.getValueAt(rowIndex, 0);
            if (isCellEditable(rowIndex, colIndex)) {
                Object attributeValue = model.getValueAt(rowIndex, 1);
                if (attributeValue instanceof Boolean) {
                    boolean boolValue = ((Boolean) attributeValue)
                            .booleanValue();
                    tip = "Click to set the '" + attributeName
                            + "' attribute to '" + !boolValue + "'";
                } else {
                    tip = "Click to modify the '" + attributeName
                            + "' attribute";
                }
            } else {
                tip = "Attribute '" + attributeName + "' is non-modifiable";
            }
        } else if (realColumnIndex == 2) {
            TableModel model = getModel();
            String attributeName = (String) model.getValueAt(rowIndex, 0);

            String option = model.getValueAt(rowIndex, 2).toString();
            tip = "Click to " + option + " monitoring the '" + attributeName
                    + "' attribute";
        }
        return tip;
    }

    /**
     * Destroys all attribute monitoring threads created through user's
     * interaction with this attributes table.
     */
    public void destroyAttributeMonitors() {
        AttributesTableModel aModel = (AttributesTableModel) dataModel;

        AttributeDescriptor[] descriptors = aModel.getAttributeDescriptors();
        for (int i = 0; i < descriptors.length; i++) {
            if (descriptors[i] != null && descriptors[i].hasMonitor()) {
                AttributeMonitoringThread monitor = (AttributeMonitoringThread) descriptors[i]
                        .getMonitor();
                monitor.running = false;
            }
            descriptors[i] = null;
        }
    }

    /**
     * Returns the attribute controller associated to this attributes table.
     * 
     * @return attributeController the attribute controller associated to this
     *         table.
     */
    public Object getAttributeObject() {
        return this.attributeObject;
    }
}