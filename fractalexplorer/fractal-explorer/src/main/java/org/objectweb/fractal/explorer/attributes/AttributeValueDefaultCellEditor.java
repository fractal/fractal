/**
 * Dream
 * Copyright (C) 2003-2004 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Jakub Kornas
 * Contributor(s): 
 */

package org.objectweb.fractal.explorer.attributes;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * A custom <code>CellEditor</code> for values of the attributes other than
 * boolean (integer, string etc.).
 */
public class AttributeValueDefaultCellEditor
        extends DefaultCellEditor {
    JTextField attributeEditorDefaultValue = null;

    Object cellEditorValue = null;

    private boolean initialized = false;

    private AttributeValueTextFieldActionListener actionListener = null;

    int row;

    Object oldValue;

    boolean actionPerformed = false;

    /**
     * @see DefaultCellEditor#DefaultCellEditor(JTextField)
     */
    public AttributeValueDefaultCellEditor(JTextField textField) {
        super(textField);

        this.attributeEditorDefaultValue = textField;
        this.attributeEditorDefaultValue
                .setHorizontalAlignment(SwingConstants.RIGHT);
        super.setClickCountToStart(1);
    }

    /**
     * @see DefaultCellEditor#getTableCellEditorComponent(JTable, Object,
     *      boolean, int, int)
     */
    public Component getTableCellEditorComponent(final JTable table,
            final Object value, boolean isSelected, final int row1,
            final int column) {
        actionPerformed = false;
        if (!initialized) {
            this.attributeEditorDefaultValue.setFont(table.getFont());
            this.actionListener = new AttributeValueTextFieldActionListener(
                    table);
            this.attributeEditorDefaultValue.addActionListener(actionListener);

            initialized = true;
        }

        this.row = row1;
        this.oldValue = value;

        if (value != null) {
            this.attributeEditorDefaultValue.setText(value.toString());
        }

        return attributeEditorDefaultValue;
    }

    /**
     * @see DefaultCellEditor#getCellEditorValue()
     */
    public Object getCellEditorValue() {
        if (actionPerformed)
            return cellEditorValue;
        return oldValue;
    }

    private class AttributeValueTextFieldActionListener
            implements ActionListener {
        private JTable table = null;

        /**
         * @param table
         */
        public AttributeValueTextFieldActionListener(JTable table) {
            this.table = table;
        }

        public void actionPerformed(ActionEvent evt) {
            actionPerformed = true;
            String attributeName = table.getValueAt(row, 0).toString();

            String newValueString = attributeEditorDefaultValue.getText()
                    .toLowerCase().trim();
            Object newValue = null;

            boolean doUpdate = false;
            if ( (oldValue == null) || (oldValue instanceof String)) {
                // We can only set primitive value with fractal attributes
                // The only one that can be null is the String type
                newValue = attributeEditorDefaultValue.getText();
                doUpdate = true;
            } else if (oldValue instanceof Integer) {
                doUpdate = true;
                try {
                    newValue = Integer.valueOf(newValueString);
                } catch (Exception e) {
                    doUpdate = false;
                    showAttributeValueErrorMessage(attributeName, "Integer");
                }
            } else if (oldValue instanceof Short) {
                doUpdate = true;
                try {
                    newValue = Short.valueOf(newValueString);
                } catch (Exception e) {
                    doUpdate = false;
                    showAttributeValueErrorMessage(attributeName, "Short");
                }
            } else if (oldValue instanceof Long) {
                doUpdate = true;
                try {
                    newValue = Long.valueOf(newValueString);
                } catch (Exception e) {
                    doUpdate = false;
                    showAttributeValueErrorMessage(attributeName, "Long");
                }
            } else if (oldValue instanceof Double) {
                doUpdate = true;
                try {
                    newValue = Double.valueOf(newValueString);
                } catch (Exception e) {
                    doUpdate = false;
                    showAttributeValueErrorMessage(attributeName, "Double");
                }
            } else if (oldValue instanceof Float) {
                doUpdate = true;
                try {
                    newValue = Float.valueOf(newValueString);
                } catch (Exception e) {
                    doUpdate = false;
                    showAttributeValueErrorMessage(attributeName, "Float");
                }
            }
            if (doUpdate && newValue != null) {
                cellEditorValue = newValue;
                fireEditingStopped();
            } else {
                fireEditingCanceled();
            }
        }

        private void showAttributeValueErrorMessage(String attributeName,
                String requiredType) {
            JOptionPane.showMessageDialog(table, "Value of attribute '"
                    + attributeName + "' must be of type "
                    + requiredType.toUpperCase(), "Wrong attribute value",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}