/*====================================================================
 
 Objectweb Browser Framework
 Copyright (C) 2000-2004 INRIA & USTL - LIFL - GOAL
 Contact: openccm@objectweb.org
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA
 
 Initial developer(s): Jerome Moroy.
 Contributor(s): ______________________________________.
 
 ---------------------------------------------------------------------
 $Id$
 ====================================================================*/

package org.objectweb.fractal.explorer.menu;

import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.fractal.explorer.context.ClientInterfaceWrapper;
import org.objectweb.util.explorer.api.DropAction;
import org.objectweb.util.explorer.api.DropTreeView;

/**
 * This action allows to bind an client interface to a server one using the Drag&Drop mechanism.
 * @author <a href="mailto:Jerome.Moroy@lifl.fr">Jerome Moroy</a>
 * @version 0.1
 */
public class BindOnDropAction 
  implements DropAction 
{
    //==================================================================
    //
    // No internal state.
    //
    //==================================================================
    
    //==================================================================
    //
    // No constructor.
    // 
    //==================================================================
    
    //==================================================================
    //
    // No internal method.
    //
    //==================================================================
    
    //==================================================================
    //
    // Public methods for DropAction interface.
    //
    //==================================================================
    
    /* (non-Javadoc)
     * @see org.objectweb.util.explorer.api.DropAction#execute(org.objectweb.util.explorer.api.DropTreeView)
     */
    public void execute(DropTreeView dropTreeView) throws Exception {
        if (dropTreeView!=null) {
            try {
                Interface serverInterface = (Interface) dropTreeView.getDragSourceObject();
                ClientInterfaceWrapper clientWrapper = (ClientInterfaceWrapper) dropTreeView.getSelectedObject(); 
                Interface clientInterface = clientWrapper.getItf();
                FcExplorer.getBindingController(clientInterface.getFcItfOwner()).bindFc(clientInterface.getFcItfName(),serverInterface);
            } catch(ClassCastException e) {
                throw new Exception("You must select an interface!");
            }
        } 
    }    
}