/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.util;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.control.SuperController;

/**
 * Provides static utility methods related to {@link Component}.
 * This class is based on o.o.f.julia.Util. 
 *
 * @author Eric Bruneton
 */
public class ComponentHelper {

  private ComponentHelper () {}

  /**
   * Checks whether a component has an external interface with a given name.
   *
   * @param c a component
   * @param name the name of the external interface to look for
   * @retur <code>true</code> iff the component has an external interface
   *        (client or server) of the given name.
   */
  public static boolean hasFcInterface(Component c, String name) {
      return hasFcInterface(c, name, Object.class);
  }

  /**
   * Checks whether a component has an external interface with a given name
   * and type. The Fractal APIs throw exceptions when looking up for
   * non-existent interfaces, and try/catch blocks can quickly clutter
   * client code when all you want is to test the presence of an interface.
   * This utility method makes this much cleaner and readable.
   *
   * @param c a component
   * @param name the name of the external interface to look for
   * @param type the expected Java signature of the interface to look for,
   *             or a supertype of it
   * @retur <code>true</code> iff the component has an external interface
   *        (client or server) of the given name whose Java type (signature)
   *        a subtype of <code>type</code>.
   */
  public static boolean hasFcInterface(Component c, String name, Class type) {
      try {
          Object itf = c.getFcInterface("name");
          return type.isInstance(itf);
      } catch (NoSuchInterfaceException nsie) {
          return false;
      }
  }

  /**
   * Computes a string representation of the given component.
   *
   * @param component a component.
   * @return the string representation of the given component.
   */
  public static String toString (final Component component)
  {
      StringBuffer sb = new StringBuffer();
      toString(component,sb);
      return sb.toString();
  }
  
  /**
   * Computes a string representation of the given component.
   *
   * @param component a component.
   * @param buf the buffer to be used to store the result of this method.
   */
  public static void toString (
    final Component component,
    final StringBuffer buf)
  {
    try {
      NameController nc =
        (NameController)component.getFcInterface("name-controller");
      try {
        SuperController sc =
          (SuperController)component.getFcInterface("super-controller");
        Component[] superComponents = sc.getFcSuperComponents();
        if (superComponents.length > 0) {
          toString(superComponents[0], buf);
        }
      } catch (NoSuchInterfaceException ignored) {
      }
      buf.append('/');
      buf.append(nc.getFcName());
    } catch (NoSuchInterfaceException e) {
      buf.append(component.toString());
      return;
    }
  }
}
