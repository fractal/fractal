/***
 * Fractal-util
 * Copyright (C) 2005-2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.util;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;

/**
 * <p>
 * This class is a Fractal provider class which aggregates different concrete
 * providers (e.g. Julia, Koch, AOKell). This provider implements a Chain of
 * responsibility pattern. Concrete providers are stored in a list. For each
 * incoming request for instantiating a component, the ChainedProvider iterates
 * on the list until a concrete provider can handle the request. If no concrete
 * provider can do so, the request fails by throwing an {@link
 * InstantiationException}.
 * </p>
 * 
 * <p>
 * This class is meant to be used as a Fractal provider
 * (<code>fractal.provider</code> system property) and assumes the
 * <code>fractal.providers</code> system property is set. This property is a
 * comma-separated list of concrete provider class names.
 * </p>
 * 
 * <p>
 * An example of a configuration follows:
 * </p>
 * 
 * <code>
 * fractal.provider=org.objectweb.fractal.util.ChainedProvider
 * fractal.providers=org.objectweb.fractal.julia.Julia,org.objectweb.fractal.koch.Koch
 * </code>
 * 
 * <p>
 * With ChainedProvider, <code>Fractal.getBootstrapComponent()</code> returns a
 * primitive component which is implemented by the {@link ChainedBootstrapImpl}
 * class. This component dispatches requests to one of the components bound to
 * the <code>generic-factories</code> collection interface. These components are
 * instances of the concrete Fractal boostrap components defined in the
 * <code>fractal.providers</code> system property.
 * </p>
 * 
 * <p>
 * When using MultiProvider, the boot sequence is as follows:
 * </p>
 * 
 * <ul>
 * <li>the {@link ChainedBootstrapImpl} class is instanciated,</li>
 * <li>each concrete provider is invoked to retrieve the corresponding concrete
 * bootstrap components,</li>
 * <li>the default bootstrap component is used to instantiate a {@link
 * ChainedBootstrapImpl} component,</li>
 * <li>the {@link ChainedBootstrapImpl} component is bound to the concrete
 * bootstrap components,</li>
 * <li>the {@link ChainedBootstrapImpl} component is returned as the bootstrap
 * component provided by ChainedProvider.</li>
 * </ul>
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class ChainedProvider implements Factory, GenericFactory {
    
    // ---------------------------------------------------------------------
    // Implementation of the Factory interface
    // ---------------------------------------------------------------------

    public Object getFcContentDesc() {
        return null;
    }

    public Object getFcControllerDesc() {
        return "bootstrap";
    }

    public Type getFcInstanceType() {
        return null;
    }

    public Component newFcInstance() throws InstantiationException {
        return newFcInstance(null,null,null);
    }

    // ---------------------------------------------------------------------
    // Implementation of the GenericFactory interface
    // ---------------------------------------------------------------------

    public Component newFcInstance(Type type, Object controllerDesc, Object contentDesc)
    throws InstantiationException {
        
        if( bootstrap == null ) {
            
            /*
             * Retrieve the bootstrap components for each provider.
             */
            Component[] bootcomps = getBootComps();
            
            /*
             * Instantiate the ChainedBootstrapImpl.
             * 
             * Iterate on all bootstrap components until one can instantiate it.
             * The idea is that some boostrap components may not be able to
             * perform this instantiation (e.g. Juliac can not do it by
             * default).
             */
            bootstrap = getChainedBootstrapComp(bootcomps);   
            BindingController bootstrapBC = getBindingController(bootstrap);
            
            /*
             * Bind the type-factory and the generic-factory interfaces provided
             * by each concrete bootstrap component to the ChainedBootstrap
             * component.
             */
            for (int i = 0; i < bootcomps.length; i++) {
                Component bootcomp = bootcomps[i];
                TypeFactory tf = getTypeFactory(bootcomp);
                GenericFactory gf = getGenericFactory(bootcomp);
                bindFc(bootstrapBC,"type-factories"+i,tf);
                bindFc(bootstrapBC,"generic-factories"+i,gf);
            }
            
            try {
                LifeCycleController lc = Fractal.getLifeCycleController(bootstrap);
                lc.startFc();
            }
            catch (NoSuchInterfaceException e) {
                String msg = "NoSuchInterfaceException: "+e.getMessage();
                throw new InstantiationException(msg);
            }
            catch (IllegalLifeCycleException e) {
                String msg = "IllegalLifeCycleException: "+e.getMessage();
                throw new InstantiationException(msg);
            }
        }
        
        return bootstrap;
    }

    private Component bootstrap;
    
    
    // ---------------------------------------------------------------------
    // Utility methods
    // ---------------------------------------------------------------------
    
    /**
     * Return the array of bootstrap components associated with this provider.
     * This method retrieves the fractal.providers system property and assumes
     * its value is a comma separated list of provider classes. This method is
     * made protected in order to enable its extension by subclasses (feature
     * used for example in FraSCAti for specifying the list of provider classes
     * used by the platform.)
     */
    protected Component[] getBootComps() throws InstantiationException {

        /*
         * Retrieve provider classes.
         */
        String providers = System.getProperty("fractal.providers");
        if( providers == null ) {
            String msg = "The fractal.providers value is not defined";
            throw new InstantiationException(msg);
        }
        
        /*
         * Retrieve the bootstrap components for each provider.
         */
        StringTokenizer st = new StringTokenizer(providers,",");
        Component[] bootcomps = new Component[ st.countTokens() ];
        for( int i=0 ; st.hasMoreTokens() ; i++ ) {
            String provider = st.nextToken();
            Map hints = new HashMap();
            Map systemPropertiesMap = new HashMap(System.getProperties());
            hints.putAll(systemPropertiesMap);
            hints.put("fractal.provider",provider);
            bootcomps[i] = Fractal.getBootstrapComponent(hints);
        }
        
        return bootcomps;
    }
    
    /**
     * Iterate on all specified bootstrap component until one can instantiate
     * the {@link ChainedBootstrapImpl} component.
     * 
     * @throws InstantiationException
     *   if no bootstrap component can instantiate the {@link
     *   ChainedBootstrapImpl} component
     */
    private Component getChainedBootstrapComp( Component[] bootcomps )
    throws InstantiationException {
        
        for (int i = 0; i < bootcomps.length; i++) {

            Component bootcomp = bootcomps[i];
            
            try {
                TypeFactory tf = Fractal.getTypeFactory(bootcomp);
                GenericFactory gf = Fractal.getGenericFactory(bootcomp);
                ComponentType ct =
                    tf.createFcType(
                        new InterfaceType[]{
                            tf.createFcItfType(
                                "type-factory", TypeFactory.class.getName(),
                                TypeFactory.SERVER, TypeFactory.MANDATORY,
                                TypeFactory.SINGLE ),
                            tf.createFcItfType(
                                "generic-factory", GenericFactory.class.getName(),
                                TypeFactory.SERVER, TypeFactory.MANDATORY,
                                TypeFactory.SINGLE ),
                            tf.createFcItfType(
                                "generic-factories", GenericFactory.class.getName(),
                                TypeFactory.CLIENT, TypeFactory.MANDATORY,
                                TypeFactory.COLLECTION ),
                            tf.createFcItfType(
                                "type-factories", TypeFactory.class.getName(),
                                TypeFactory.CLIENT, TypeFactory.MANDATORY,
                                TypeFactory.COLLECTION ),
                        }
                    );
                Component bootstrap =
                    gf.newFcInstance(
                        ct, "primitive", ChainedBootstrapImpl.class.getName());
                return bootstrap;
            }
            catch( NoSuchInterfaceException nsie ) {}
            catch( InstantiationException ie ) {}
        }
        
        String msg = "Can not instantiation ChainedBootstrapImpl";
        throw new InstantiationException(msg);
    }
    
    private GenericFactory getGenericFactory( Component c )
    throws InstantiationException {
        try {
            return Fractal.getGenericFactory(c);
        }
        catch (NoSuchInterfaceException e) {
            String msg = "No such interface exception: "+e.getMessage();
            throw new InstantiationException(msg);
        }
    }

    private TypeFactory getTypeFactory( Component c )
    throws InstantiationException {
        try {
            return Fractal.getTypeFactory(c);
        }
        catch (NoSuchInterfaceException e) {
            String msg = "No such interface exception: "+e.getMessage();
            throw new InstantiationException(msg);
        }
    }
    
    private BindingController getBindingController( Component c )
    throws InstantiationException {
        try {
            return Fractal.getBindingController(c);
        }
        catch (NoSuchInterfaceException e) {
            String msg = "No such interface exception: "+e.getMessage();
            throw new InstantiationException(msg);
        }
    }
    
    private void bindFc( BindingController bc, String clientItfName, Object serverItf )
    throws InstantiationException {
        try {
            bc.bindFc(clientItfName, serverItf);
        }
        catch (NoSuchInterfaceException e) {
            String msg = "No such interface exception: "+e.getMessage();
            throw new InstantiationException(msg);
        }
        catch (IllegalBindingException e) {
            String msg = "IllegalBindingException: "+e.getMessage();
            throw new InstantiationException(msg);
        }
        catch (IllegalLifeCycleException e) {
            String msg = "IllegalLifeCycleException: "+e.getMessage();
            throw new InstantiationException(msg);
        }
    }    
}
