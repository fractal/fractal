/***
 * Fractal-util
 * Copyright (C) 2005-2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;

/**
 * Implementation of the Fractal bootstrap component for the {@link
 * ChainedProvider} class.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class ChainedBootstrapImpl implements TypeFactory, GenericFactory, BindingController {
    
    // ---------------------------------------------------------------------
    // Implementation of the TypeFactory interface
    // ---------------------------------------------------------------------

    public InterfaceType createFcItfType(
        String name, String signature, boolean isClient, boolean isOptional,
        boolean isCollection )
    throws InstantiationException {
        
        Collection providers = typeFactories.values();
        for (Iterator iterator = providers.iterator(); iterator.hasNext();) {
            TypeFactory tf = (TypeFactory) iterator.next();
            try {
                return tf.createFcItfType(name, signature, isClient, isOptional, isCollection);
            }
            catch( InstantiationException ie ) {
                /*
                 * The factory can not instantiate the interface type.
                 * Proceed with the next one in the list.
                 */
            }
        }

        String msg =
            "Can not instantiate interface type: "+name+","+signature+","+
            isClient+","+isOptional+","+isCollection+">";
        throw new InstantiationException(msg);
    }

    public ComponentType createFcType(InterfaceType[] interfaceTypes)
    throws InstantiationException {

        Collection providers = typeFactories.values();
        for (Iterator iterator = providers.iterator(); iterator.hasNext();) {
            TypeFactory tf = (TypeFactory) iterator.next();
            try {
                return tf.createFcType(interfaceTypes);
            }
            catch( InstantiationException ie ) {
                /*
                 * The factory can not instantiate the interface type.
                 * Proceed with the next one in the list.
                 */
            }
        }

        String msg = "Can not instantiate component type: [";
        for (int i = 0; i < interfaceTypes.length; i++) {
            if(i!=0) msg+=",";
            msg += interfaceTypes[i].toString();
        }
        msg += "]";
        throw new InstantiationException(msg);
    }

    // ---------------------------------------------------------------------
    // Implementation of the GenericFactory interface
    // ---------------------------------------------------------------------

    public Component newFcInstance(Type type, Object controllerDesc, Object contentDesc)
    throws InstantiationException {
        
        List excepts = null;

        Collection providers = genericFactories.values();
        for (Iterator iterator = providers.iterator(); iterator.hasNext();) {
            GenericFactory gf = (GenericFactory) iterator.next();
            try {
                return gf.newFcInstance(type, controllerDesc, contentDesc);
            }
            catch( InstantiationException ie ) {
                /*
                 * The factory can not instantiate the component.
                 * Proceed with the next one in the list.
                 */
                if( excepts == null ) {
                    excepts = new ArrayList();
                }
                excepts.add(ie);
            }
        }
        
        /*
         * Report individual stack traces.
         */
        int i = 0;
        for (Iterator iterator = genericFactories.keySet().iterator(); iterator.hasNext();) {
            String key = (String) iterator.next();
            InstantiationException ie = (InstantiationException) excepts.get(i);
            System.err.println("---- "+key+" ----");
            ie.printStackTrace();
            System.err.println("---- ");
            i++;
        }

        String msg =
            "Can not instantiate component <"+type+","+controllerDesc+","+
            contentDesc+">";
        throw new InstantiationException(msg);
    }

    // ---------------------------------------------------------------------
    // Implementation of the BindingController interface
    // ---------------------------------------------------------------------

    private Map typeFactories = new HashMap();
    private Map genericFactories = new HashMap();
    
    public void bindFc(String clientItfName, Object serverItf) {
    
        if( clientItfName.startsWith("generic-factories") ) {
            genericFactories.put(clientItfName, serverItf);
        }
        else if( clientItfName.startsWith("type-factories") ) {
            typeFactories.put(clientItfName, serverItf);
        }
    }

    public String[] listFc() {
        String[] tfs = (String[]) typeFactories.keySet().toArray(new String[]{});
        String[] gfs = (String[]) genericFactories.keySet().toArray(new String[]{});
        String[] result = new String[tfs.length+gfs.length];
        System.arraycopy(tfs, 0, result, 0, tfs.length);
        System.arraycopy(gfs, 0, result, tfs.length, gfs.length);
        return result;
    }

    public Object lookupFc(String clientItfName) throws NoSuchInterfaceException {
        if( clientItfName.startsWith("generic-factories") ) {
            return (GenericFactory) genericFactories.get(clientItfName);
        }
        else if( clientItfName.startsWith("type-factories") ) {
            return (TypeFactory) typeFactories.get(clientItfName);
        }
        throw new NoSuchInterfaceException(clientItfName);
    }

    public void unbindFc(String clientItfName) throws NoSuchInterfaceException {
        if( clientItfName.startsWith("generic-factories") ) {
            genericFactories.remove(clientItfName);
        }
        else if( clientItfName.startsWith("type-factories") ) {
            typeFactories.remove(clientItfName);
        }
        throw new NoSuchInterfaceException(clientItfName);
    }
}
