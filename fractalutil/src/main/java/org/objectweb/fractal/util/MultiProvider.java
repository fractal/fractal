/***
 * Fractal-util
 * Copyright (C) 2005-2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.util;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;

/**
 * <p>
 * This class is a Fractal provider class which aggregates different
 * concrete providers (e.g. Julia, Koch, AOKell). Requests for instantiating
 * components are dispatched according to a name prefix appended in front of the
 * controller descriptor.
 * </p>
 * 
 * <p>
 * This class is meant to be used as a Fractal provider
 * (<code>fractal.provider</code> system property) and assumes the following
 * system properties are also set:
 * </p>
 * 
 * <ul>
 * <li><code>fractal.providers</code>: this property is a comma-separated list
 * of name prefixes for concrete provider classes (the list can not be empty),</li>
 * <li><code>fractal.providern</code> (where n is an index): these properties
 * reference the concrete provider classes.</li>
 * </ul>
 * 
 * <p>
 * An example of a configuration follows:
 * </p>
 * 
 * <code>
 * fractal.provider=org.objectweb.fractal.util.MultiProvider
 * fractal.providers=julia,koch
 * fractal.provider0=org.objectweb.fractal.julia.Julia
 * fractal.provider1=org.objectweb.fractal.koch.Koch
 * </code>
 * 
 * <p>
 * This configuration defines two provider classes: julia (default one) and
 * koch. Component instantiation requests for controller descriptors with a
 * <code>/koch/</code> prefix (e.g. <code>/koch/primitive</code>) are
 * dispatched to Koch. Request for controller descriptors with any other
 * prefix or without a prefix (e.g. <code>primitive</code>) are dispatched to
 * the default provider, i.e. the one which is mentioned first in
 * <code>fractal.providers</code>. 
 * </p>
 * 
 * <p>
 * The architecture of the MultiProvider is illustrated below.
 * </p>
 * 
 * <p align="center">
 * <img src="../../../../doc-files/figures/MultiProvider.jpg" />
 * </p>
 * 
 * <p>
 * With MultiProvider,
 * <code>Fractal.getBootstrapComponent()</code> returns a primitive component
 * which is implemented by the {@link MultiBootstrapImpl} class. This component
 * dispatches requests to one of the component bound to the
 * <code>generic-factories</code> collection interface. These components are
 * instances of the concrete Fractal boostrap components defined in the
 * <code>fractal.providers</code> system property.
 * </p>
 * 
 * <p>
 * When using MultiProvider, the boot sequence is as follows:
 * </p>
 * 
 * <ul>
 * <li>the {@link MultiBootstrapImpl} class is instanciated,</li>
 * <li>each concrete provider is invoked to retrieve the corresponding concrete
 * bootstrap components,</li>
 * <li>the default bootstrap component is used to instantiate a {@link
 * MultiBootstrapImpl} component,</li>
 * <li>the {@link MultiBootstrapImpl} component is bound to the concrete
 * bootstrap components,</li>
 * <li>the {@link MultiBootstrapImpl} component is returned as the bootstrap
 * component provided by MultiProvider.</li>
 * </ul>
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class MultiProvider implements Factory, GenericFactory {
    
    // ---------------------------------------------------------------------
    // Implementation of the Factory interface
    // ---------------------------------------------------------------------

    public Object getFcContentDesc() {
        return null;
    }

    public Object getFcControllerDesc() {
        return "bootstrap";
    }

    public Type getFcInstanceType() {
        return null;
    }

    public Component newFcInstance() throws InstantiationException {
        return newFcInstance(null,null,null);
    }

    // ---------------------------------------------------------------------
    // Implementation of the GenericFactory interface
    // ---------------------------------------------------------------------

    public Component newFcInstance(Type type, Object controllerDesc, Object contentDesc)
    throws InstantiationException {
        
        if( bootstrap == null ) {
            
            /*
             * Retrieve provider classes.
             */
            String providers = System.getProperty("fractal.providers");
            if( providers == null ) {
                String msg = "The fractal.providers value is not defined";
                throw new InstantiationException(msg);
            }
            
            /*
             * Retrieve the bootstrap component provided by each provider.
             */
            BindingController multiBootstrapBC = null;
            StringTokenizer st = new StringTokenizer(providers,",");
            for( int i=0 ; st.hasMoreTokens() ; i++ ) {
                
                // Retrieve the provider class name
                String prefix = st.nextToken();
                String propname = "fractal.provider"+i;
                String provider = System.getProperty(propname);
                if( provider == null ) {
                    String msg = "The "+propname+" value is not defined";
                    throw new InstantiationException(msg);
                }
                
                // Retrieve the corresponding bootstrap component
                Map hints = new HashMap();
                Map systemPropertiesMap = new HashMap(System.getProperties());
                hints.putAll(systemPropertiesMap);
                hints.put("fractal.provider",provider);
                Component c = Fractal.getBootstrapComponent(hints);
                GenericFactory gf = getGenericFactory(provider, c);
                
                /*
                 * Use the default type and generic factories to instantiate a
                 * primitive MultiBootstrapImpl component.
                 */
                if( i == 0 ) {
                    TypeFactory tf = getTypeFactory(provider, c);
                    ComponentType ct =
                        tf.createFcType(
                            new InterfaceType[]{
                                tf.createFcItfType(
                                    "type-factory", TypeFactory.class.getName(),
                                    TypeFactory.SERVER, TypeFactory.MANDATORY,
                                    TypeFactory.SINGLE ),
                                tf.createFcItfType(
                                    "generic-factory", GenericFactory.class.getName(),
                                    TypeFactory.SERVER, TypeFactory.MANDATORY,
                                    TypeFactory.SINGLE ),
                                tf.createFcItfType(
                                    "generic-factories", GenericFactory.class.getName(),
                                    TypeFactory.CLIENT, TypeFactory.MANDATORY,
                                    TypeFactory.COLLECTION ),
                                tf.createFcItfType(
                                    "tf", TypeFactory.class.getName(),
                                    TypeFactory.CLIENT, TypeFactory.MANDATORY,
                                    TypeFactory.SINGLE ),
                                tf.createFcItfType(
                                    "gf", GenericFactory.class.getName(),
                                    TypeFactory.CLIENT, TypeFactory.MANDATORY,
                                    TypeFactory.SINGLE ),
                            }
                        );
                    bootstrap =
                        gf.newFcInstance(ct, "primitive", MultiBootstrapImpl.class.getName());
                    
                    // Register the default generic and type factories
                    multiBootstrapBC = getBindingController(provider,bootstrap);
                    bindFc(multiBootstrapBC, "gf", gf);
                    bindFc(multiBootstrapBC, "tf", tf);
                }
                
                // Register the generic factory
                String clientItfName = "generic-factories/"+prefix+"/";
                bindFc(multiBootstrapBC, clientItfName, gf);
            }
            try {
                LifeCycleController lc = Fractal.getLifeCycleController(bootstrap);
                lc.startFc();
            }
            catch (NoSuchInterfaceException e) {
                String msg = "NoSuchInterfaceException: "+e.getMessage();
                throw new InstantiationException(msg);
            }
            catch (IllegalLifeCycleException e) {
                String msg = "IllegalLifeCycleException: "+e.getMessage();
                throw new InstantiationException(msg);
            }
        }
        
        return bootstrap;
    }

    private Component bootstrap;
    
    
    // ---------------------------------------------------------------------
    // Utility methods
    // ---------------------------------------------------------------------
    
    private GenericFactory getGenericFactory( String provider, Component c )
    throws InstantiationException {
        try {
            return Fractal.getGenericFactory(c);
        }
        catch (NoSuchInterfaceException e) {
            String msg =
                "Provider "+provider+" should provide a generic-factory interface";
            throw new InstantiationException(msg);
        }
    }

    private TypeFactory getTypeFactory( String provider, Component c )
    throws InstantiationException {
        try {
            return Fractal.getTypeFactory(c);
        }
        catch (NoSuchInterfaceException e) {
            String msg =
                "Provider "+provider+ " should provide a type-factory interface";
            throw new InstantiationException(msg);
        }
    }
    
    private BindingController getBindingController( String provider, Component c )
    throws InstantiationException {
        try {
            return Fractal.getBindingController(c);
        }
        catch (NoSuchInterfaceException e) {
            String msg =
                "Provider "+provider+ " should provide a binding-controller interface";
            throw new InstantiationException(msg);
        }
    }
    
    private void bindFc( BindingController bc, String clientItfName, Object serverItf )
    throws InstantiationException {
        try {
            bc.bindFc(clientItfName, serverItf);
        }
        catch (NoSuchInterfaceException e) {
            String msg = "No such interface exception: "+e.getMessage();
            throw new InstantiationException(msg);
        }
        catch (IllegalBindingException e) {
            String msg = "IllegalBindingException: "+e.getMessage();
            throw new InstantiationException(msg);
        }
        catch (IllegalLifeCycleException e) {
            String msg = "IllegalLifeCycleException: "+e.getMessage();
            throw new InstantiationException(msg);
        }
    }    
}
