/***
 * Julia
 * Copyright (C) 2005-2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.util;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.SuperController;

/**
 * Provides static utility methods related to {@link SuperController}.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class SuperControllerHelper {

    private SuperControllerHelper() {}
    
    /**
     * Traverse the super-hierarchy to return the root component.
     * Return null if the component model is non-hierarchical (i.e. flat).
     * 
     * @param c  the component
     * @return   the root component containing the specified component
     */
    public static Component getTopMostComponent( Component c ) {
        
        try {
            SuperController sc = Fractal.getSuperController(c);
            Component[] sups = sc.getFcSuperComponents();
            if( sups.length == 0 ) {
                // No more supers. The top is reached.
                return c;
            }            
            return getTopMostComponent(sups[0]);
        }
        catch (NoSuchInterfaceException e) {
            return null;
        }
    }
    
}
