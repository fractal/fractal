/***
 * Fractal-util
 * Copyright (C) 2006 INRIA, France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;

public class AbstractComponent implements BindingController {

    // -----------------------------------------------------------------
    // Implementation of the BindingController interface
    // -----------------------------------------------------------------
    
    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.BindingController#listFc()
     */
    public String[] listFc() {
        /*
         * new HashSet(fcBindings.keySet()) mandatory.
         * Else addAll fails with an UnsupportedException.
         */
        Set names = new HashSet( fcBindings.keySet() );
        names.addAll(fcSingletonItfNames);
        String[] result = (String[]) names.toArray(new String[names.size()]);
        return result;
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.BindingController#lookupFc(java.lang.String)
     */
    public Object lookupFc(String clientItfName) {
        
        // Binding for the component interface
        if( clientItfName.equals("component") ) {
            return fcComp;
        }
        
        // Binding for a singleton interface
        if( fcSingletonItfNames.contains(clientItfName) ) {
            return fcBindings.get(clientItfName);
        }
        
        // Binding for a collection interface
        for (Iterator iter = fcCollectionItfNames.iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            if( clientItfName.startsWith(name) ) {
                return fcBindings.get(clientItfName);
            }
        }

        return null;
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.BindingController#bindFc(java.lang.String, java.lang.Object)
     */
    public void bindFc(String clientItfName, Object serverItf) {
        
        // Binding for the component interface
        if( clientItfName.equals("component") ) {
            initFcComp(serverItf);
            return;
        }
        
        // Binding for a singleton interface
        if( fcSingletonItfNames.contains(clientItfName) ) {
            fcBindings.put(clientItfName,serverItf);
            return;
        }
        
        // Binding for a collection interface
        for (Iterator iter = fcCollectionItfNames.iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            if( clientItfName.startsWith(name) ) {
                fcBindings.put(clientItfName,serverItf);
                return;
            }
        }
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.api.control.BindingController#unbindFc(java.lang.String)
     */
    public void unbindFc(String clientItfName) {
        
        // Binding for a singleton interface
        if( fcSingletonItfNames.contains(clientItfName) ) {
            fcBindings.remove(clientItfName);
            return;
        }
        
        // Binding for a collection interface
        for (Iterator iter = fcCollectionItfNames.iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            if( clientItfName.startsWith(name) ) {
                fcBindings.remove(clientItfName);
                return;
            }
        }
    }

    
    // -----------------------------------------------------------------
    // Implementation specific
    // -----------------------------------------------------------------
    
    private void initFcComp( Object serverItf ) {
        
        /*
         * All exceptions thrown by this method "should" occur.
         * The serverItf value is provided by the framework.
         * The framework is supposed to provide a Component associated to
         * a ComponentType.
         */
        
        if( ! (serverItf instanceof Component) ) {
            String msg =
                "The instance bound to the component interface should implement "+
                Component.class.getName();
            throw new RuntimeException(msg);
        }
        
        fcComp = (Component) serverItf;
        Type fcType = fcComp.getFcType();
        
        if( ! (fcType instanceof ComponentType) ) {
            String msg =
                "The component bound to the component interface should be of type "+
                ComponentType.class.getName();
            throw new RuntimeException(msg);
        }
        
        InterfaceType[] fcItfTypes =
            ((ComponentType)fcType).getFcInterfaceTypes();
        for (int i = 0; i < fcItfTypes.length; i++) {
            if( fcItfTypes[i].isFcClientItf() ) {
                Set set =
                    (fcItfTypes[i].isFcCollectionItf()) ?
                            fcCollectionItfNames : fcSingletonItfNames;
                set.add( fcItfTypes[i].getFcItfName() );
            }
        }
    }

    /** The component controller associated to this component. */
    private Component fcComp;
    
    private Set fcSingletonItfNames = new HashSet();        // Set<String>
    private Set fcCollectionItfNames = new HashSet();       // Set<String>
    private Map fcBindings = new HashMap();     // Map<String,Object>
}
