/***
 * Fractal-util
 * Copyright (C) 2005-2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.util;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;

/**
 * Implementation of the Fractal bootstrap component for the {@link
 * MultiProvider} class.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class MultiBootstrapImpl implements TypeFactory, GenericFactory, BindingController {
    
    // ---------------------------------------------------------------------
    // Implementation of the TypeFactory interface
    // ---------------------------------------------------------------------

    public InterfaceType createFcItfType(String name, String signature, boolean isClient, boolean isOptional, boolean isCollection) throws InstantiationException {
        return typeFactory.createFcItfType(name, signature, isClient, isOptional, isCollection);
    }

    public ComponentType createFcType(InterfaceType[] interfaceTypes) throws InstantiationException {
        return typeFactory.createFcType(interfaceTypes);
    }

    // ---------------------------------------------------------------------
    // Implementation of the GenericFactory interface
    // ---------------------------------------------------------------------

    public Component newFcInstance(Type type, Object controllerDesc, Object contentDesc) throws InstantiationException {

        if( controllerDesc instanceof String ) {
            String str = (String) controllerDesc;
            
            /*
             * Check whether the controller descriptor contains a prefix.
             * In this case, use the corresponding generic factory.
             * E.g. /julia/primitive designates a primitive component which
             * must be instantiated with the registered /julia/ factory. 
             */
            if( str.charAt(0) == '/' ) {
                int lastslash = str.lastIndexOf('/');
                // At least 2 slashes and one character after the last slash 
                if( lastslash > 0 && lastslash+1 < str.length() ) {
                    String prefix = "generic-factories"+str.substring(0,lastslash+1);
                    String ctrlDesc = str.substring(lastslash+1);
                    GenericFactory gf = (GenericFactory) genericFactories.get(prefix);
                    return gf.newFcInstance(type, ctrlDesc, contentDesc);
                }
            }
        }
        
        // TODO controllerDesc instanceof Array
        return genericFactory.newFcInstance(type, controllerDesc, contentDesc);
    }

    // ---------------------------------------------------------------------
    // Implementation of the BindingController interface
    // ---------------------------------------------------------------------

    private Map genericFactories = new HashMap();
    private GenericFactory genericFactory;
    private TypeFactory typeFactory;
    
    public void bindFc(String clientItfName, Object serverItf) {
    
        if( clientItfName.startsWith("generic-factories") ) {
            genericFactories.put(clientItfName, serverItf);
        }
        else if( clientItfName.equals("gf") ) {
            genericFactory = (GenericFactory) serverItf;
        }
        else if( clientItfName.equals("tf") ) {
            typeFactory = (TypeFactory) serverItf;
        }
    }

    public String[] listFc() {
        String[] result = new String[genericFactories.size() + 2];
        genericFactories.keySet().toArray(result);
        result[genericFactories.size()] = "gf";
        result[genericFactories.size()+1] = "tf";
        return result;
    }

    public Object lookupFc(String clientItfName) throws NoSuchInterfaceException {
        if( clientItfName.startsWith("generic-factories") ) {
            return (GenericFactory) genericFactories.get(clientItfName);
        }
        else if( clientItfName.equals("gf") ) {
            return genericFactory;
        }
        else if( clientItfName.equals("tf") ) {
            return typeFactory;
        }
        throw new NoSuchInterfaceException(clientItfName);
    }

    public void unbindFc(String clientItfName) throws NoSuchInterfaceException {
        if( clientItfName.startsWith("generic-factories") ) {
            genericFactories.remove(clientItfName);
        }
        else if( clientItfName.equals("gf") ) {
            genericFactory = null;
        }
        else if( clientItfName.equals("tf") ) {
            typeFactory = null;
        }
        throw new NoSuchInterfaceException(clientItfName);
    }
}
