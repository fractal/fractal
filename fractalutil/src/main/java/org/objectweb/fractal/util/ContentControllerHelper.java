/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 * Contributor: Lionel Seinturier
 */

package org.objectweb.fractal.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.NameController;

/**
 * Provides static utility methods related to {@link ContentController}.
 * This class is based on o.o.f.julia.control.content.Util. 
 *
 * @author Eric Bruneton
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class ContentControllerHelper {

  private ContentControllerHelper () {
  }

  /**
   * Returns all the direct and indirect sub components of the given component.
   * Each shared component appears only once in the returned list.
   *
   * @param component a component.
   * @return all the direct and indirect sub components of the given component.
   */
  public static List getAllSubComponents (final Component component) {
    List allSubComponents = new ArrayList();
    List stack = new ArrayList();
    stack.add(component);
    while (stack.size() > 0) {
      int index = stack.size() - 1;
      Component c = (Component)stack.get(index);
      stack.remove(index);
      if (!allSubComponents.contains(c)) {
        try {
          ContentController cc =
            (ContentController)c.getFcInterface("content-controller");
          Component[] subComponents = cc.getFcSubComponents();
          for (int i = subComponents.length - 1; i >= 0; --i) {
            stack.add(subComponents[i]);
          }
        } catch (NoSuchInterfaceException ignored) {
          // c is not a composite component: nothing to do
        }
        allSubComponents.add(c);
      }
    }
    return allSubComponents;
  }

  /**
   * Finds a sub-component of a composite which have a specified name.
   *
   * @param parent the component in which to search.
   * @param name the name of the sub-component to look for, as defined by {@link NameController}.
   *             If <code>null</code>, search for components which have no
   *             <code>NameController</code> or an explicitly <code>null</code> name.
   * @return a sub-component of <code>parent<code> whose name matches <code>name</code>, or
   *         <code>null</code> if none could be found.
   */
  public static Component getSubComponentByName (final Component parent, final String name) {
    Component[] kids = getSubComponents(parent);
    for (int i = 0; i < kids.length; i++) {
      if (isNamed(kids[i], name)) {
        return kids[i];
      }
    }
    return null;
  }

  /**
   * Returns all the direct sub-components of a composite which have a specified name.
   *
   * @param parent the composite in which to search.
   * @param name the name of the sub-components to look for, as defined by their
   *             {@link NameController}. If <code>null</code>, search for components which
   *             have no <code>NameController</code> or an explicitly <code>null</code> name.
   * @return the set of all the direct sub-components of <code>parent</code> whose name is
   *         <code>name</code>.
   */
  public static Set getAllSubComponentsByName (final Component parent, final String name) {
    Set result = new HashSet();
    Component[] kids = getSubComponents(parent);
    for (int i = 0; i < kids.length; i++) {
      if (isNamed(kids[i], name)) {
        result.add(kids[i]);
      }
    }
    return result;
  }

  /*
   * Utility used by both getSubComponentByName() and getAllSubComponentsByName().
   */
  private static Component[] getSubComponents(Component c) {
    try {
      return Fractal.getContentController(c).getFcSubComponents();
    } catch (NoSuchInterfaceException _) {
      return new Component[0];
    }
  }

  /*
   * Utility used by both getSubComponentByName() and getAllSubComponentsByName().
   */
  private static boolean isNamed(Component c, String name) {
    try {
      String cName = Fractal.getNameController(c).getFcName();
      return (name == null) ? (cName == null) : name.equals(cName);
    } catch (NoSuchInterfaceException _) {
      return name == null;
    }
  }
}
