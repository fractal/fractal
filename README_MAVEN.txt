###############################################################
!! How to use Fractal Maven artifacts in your 3rd party project
###############################################################

Add the following in your pom.xml

<project>
<!-- ....... -->

    <repositories>
		<repository>
			<id>objectweb-release</id>
			<name>ObjectWeb Maven Repository</name>
			<url>http://maven.objectweb.org/maven2</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>objectweb-snapshot</id>
			<name>ObjectWeb Maven Repository</name>
			<url>http://maven.objectweb.org/maven2-snapshot</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>daily</updatePolicy>
			</snapshots>
		</repository>
	</repositories>
    
    <pluginRepositories>
        <pluginRepository>
            <id>objectweb-release</id>
            <name>ObjectWeb Maven Repository</name>
            <url>http://maven.objectweb.org/maven2</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </pluginRepository>
        <pluginRepository>
            <id>objectweb-snapshot</id>
            <name>ObjectWeb Maven Repository</name>
            <url>http://maven.objectweb.org/maven2-snapshot</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
            </snapshots>
        </pluginRepository>
    </pluginRepositories>

<!-- ........ -->
</project>


####################################################################
!!   How to setup the environment to develop a Fractal sub-project
!!   that you have checkout from the SVN 
####################################################################

If you want to just check out a single Fractal sub project (i.e. "julia") and wants to work on it, 
you need to put this (once and forever) in your .m2/settings.xml:

<settings>
   <!-- ........ -->

 <profiles>
   <profile>
    <id>objectweb</id>
    <repositories>
		<repository>
			<id>objectweb-release</id>
			<name>ObjectWeb Maven Repository</name>
			<url>http://maven.objectweb.org/maven2</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>objectweb-snapshot</id>
			<name>ObjectWeb Maven Repository</name>
			<url>http://maven.objectweb.org/maven2-snapshot</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>daily</updatePolicy>
			</snapshots>
		</repository>
	</repositories>
    
    <pluginRepositories>
        <pluginRepository>
            <id>objectweb-release</id>
            <name>ObjectWeb Maven Repository</name>
            <url>http://maven.objectweb.org/maven2</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </pluginRepository>
        <pluginRepository>
            <id>objectweb-snapshot</id>
            <name>ObjectWeb Maven Repository</name>
            <url>http://maven.objectweb.org/maven2-snapshot</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
            </snapshots>
        </pluginRepository>
    </pluginRepositories>
  </profile>
</profiles>

  <!-- 
     To make the "objectweb" profile used by default when you utilize Maven to
     build a project 
  -->
  <activeProfiles>
	<activeProfile>objectweb</activeProfile>
  </activeProfiles>

 <!-- ........ -->
</settings>


##################################################################
##################################################################

For other informations have a look at the Fractal Web Site:
  http://fractal.objectweb.org/maven.html

