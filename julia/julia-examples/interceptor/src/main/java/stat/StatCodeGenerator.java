/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package stat;

import org.objectweb.fractal.julia.asm.SimpleCodeGenerator;

import java.lang.reflect.Method;

/**
 * This code generator generates interception code of the following form for
 * all methods:
 *
 * <p><pre>
 * <i>method-signature</i> {
 *   <i>return-type</i> result;
 *   long context = delegate.preMethod(<i>method-name</i>);
 *   // original method code, where returns are replaced with gotos
 *   delegate.postMethod(<i>method-name</i>, context);
 *   return result;
 * }
 * </pre>
 *
 * where <tt>delegate</tt> is initialized with the following code:
 *
 * <p><pre>
 * delegate = (...)ic.getInterface("stat-controller");
 * </pre>
 */

public class StatCodeGenerator extends SimpleCodeGenerator {

  protected String getControllerInterfaceName () {
    return "stat-controller";
  }

  protected String getPreMethodName () {
    return "preMethod";
  }

  protected String getPostMethodName () {
    return "postMethod";
  }

  protected Class getContextType () {
    return Long.TYPE;
  }

  protected String getMethodName (final Method m) {
    return m.getName();
  }
}
