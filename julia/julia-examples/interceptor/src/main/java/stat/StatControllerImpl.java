/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package stat;

import org.objectweb.fractal.julia.loader.Initializable;
import org.objectweb.fractal.julia.loader.Tree;

public class StatControllerImpl implements Initializable, StatController {

  /**
   * Number of calls to preMethod, i.e., the total number of method calls.
   */

  private int calls;

  /**
   * Number of calls to postMethod, i.e., the total number of method success
   * (since postMethod is not called inside a finally block - see
   * StatCodeGenerator - it is not called if the intercepted method throws an
   * exception).
   */

  private int success;

  /**
   * Number of read field accesses.
   */

  private int reads;

  /**
   * Number of write field accesses.
   */

  private int writes;

  /**
   * Total execution time, in milliseconds
   */

  private long totalTime;

  /**
   * True if counters must be reinitialized when getter methods are called.
   */

  private boolean reset;

  // implementation of the Initializable interface

  public void initialize (final Tree args) {
    String s = args.getSubTree(0).toString();
    if (s.equals("on")) {
      reset = true;
    }
  }

  // implementation of the StatController interface

  public int getNumberOfMethodCall () {
    int calls = this.calls;
    if (reset) {
      this.calls = 0;
    }
    return calls;
  }

  public int getNumberOfMethodSuccess () {
    int success = this.success;
    if (reset) {
      this.success = 0;
    }
    return success;
  }

  public int getNumberOfFieldRead () {
    int reads = this.reads;
    if (reset) {
      this.reads = 0;
    }
    return reads;
  }

  public int getNumberOfFieldWrite () {
    int writes = this.writes;
    if (reset) {
      this.writes = 0;
    }
    return writes;
  }

  public long getTotalExecutionTime () {
    long totalTime = this.totalTime;
    if (reset) {
      this.totalTime = 0;
    }
    return totalTime;
  }

  // methods called by the associated interceptor

  public long preMethod (final String method) {
    synchronized (this) {
      ++calls;
      return System.currentTimeMillis();
    }
  }

  public void postMethod (final String method, long start) {
    synchronized (this) {
      ++success;
      totalTime += System.currentTimeMillis() - start;
    }
  }

  public void getField (final String field) {
    ++reads;
  }

  public void setField (final String field) {
    ++writes;
  }
}
