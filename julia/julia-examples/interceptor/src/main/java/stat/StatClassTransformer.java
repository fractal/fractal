/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 * Contributor: Philippe Merle
 */

package stat;

import org.objectweb.fractal.julia.asm.ClassTransformer;

import org.objectweb.asm.ClassAdapter;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.MethodAdapter;
import org.objectweb.asm.Opcodes;

/**
 * This class transformer inserts call to "getField" and "setField" methods
 * before each read and write field access instructions in the class it visits.
 */

public class StatClassTransformer extends ClassAdapter
  implements ClassTransformer
{

  private String className;

  public StatClassTransformer () {
    super(null);
  }

  public void setClassVisitor (final ClassVisitor cv) {
    this.cv = cv;
  }

  public void visit (
    final int version,
    final int access,
    final String name,
    final String signature,
    final String superName,
    final String[] interfaces)
  {
    cv.visit(version, access, name, signature, superName, interfaces);
    this.className = name;
  }

  public MethodVisitor visitMethod (
    final int access,
    final String name,
    final String desc,
    final String signature,
    final String[] exceptions)
  {
    MethodVisitor v = cv.visitMethod(access, name, desc, signature, exceptions);
    if (v != null) {
      v = new StatMethodTransformer(className, v);
    }
    return v;
  }

  private static class StatMethodTransformer extends MethodAdapter
    implements Opcodes
 {

    private String className;

    public StatMethodTransformer (final String className, final MethodVisitor c) {
      super(c);
      this.className = className;
    }

    public void visitFieldInsn (
      final int opcode,
      final String owner,
      final String name,
      final String desc)
    {
      if (opcode == GETFIELD && owner.equals(className)) {
        mv.visitVarInsn(ALOAD, 0);
        mv.visitLdcInsn(name);
        mv.visitMethodInsn(
          INVOKEVIRTUAL, className, "getField", "(Ljava/lang/String;)V");
      } else if (opcode == PUTFIELD && owner.equals(className)) {
        mv.visitVarInsn(ALOAD, 0);
        mv.visitLdcInsn(name);
        mv.visitMethodInsn(
          INVOKEVIRTUAL, className, "setField", "(Ljava/lang/String;)V");
      }
      mv.visitFieldInsn(opcode, owner, name, desc);
    }
  }
}
