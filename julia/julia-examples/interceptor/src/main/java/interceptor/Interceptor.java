/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package interceptor;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.BindingController;

import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;

import org.objectweb.fractal.julia.control.binding.Util;

import org.objectweb.fractal.util.Fractal;

import stat.StatController;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;

public class Interceptor {

  public static void main (final String[] args) throws Exception {
    Component boot = Fractal.getBootstrapComponent();

    TypeFactory tf = Fractal.getTypeFactory(boot);
    ComponentType cType = tf.createFcType(new InterfaceType[] {
      tf.createFcItfType("s", "interceptor.I", false, false, false)
    });

    GenericFactory cf = Fractal.getGenericFactory(boot);
    Component cComp = cf.newFcInstance(
        cType, "statPrimitive", "interceptor.IImpl");
    Component rComp = cf.newFcInstance(
        cType, "composite", null);
    Fractal.getContentController(rComp).addFcSubComponent(cComp);
    Fractal.getBindingController(rComp).bindFc("s", cComp.getFcInterface("s"));
    Fractal.getLifeCycleController(rComp).startFc();

    System.err.println("INITIAL CONFIGURATION");
    test(rComp);

    // adds a trace aspect in the interceptor
    System.err.println("CHANGE THE INTERCEPTOR (ADD A TRACE ASPECT)");
    cComp = changeComponent(cType, "statTracePrimitive", cComp);
    test(rComp);
    System.err.println("(note that the StatController state has been lost)\n");

    // completely removes the interceptor and the controller
    System.err.println("REMOVE THE INTERCEPTOR AND THE CONTROLLER");
    cComp = changeComponent(cType, "emptyPrimitive", cComp);
    test(rComp);

    // adds the interceptor back
    System.err.println("ADD BACK THE INTERCEPTOR AND THE CONTROLLER");
    cComp = changeComponent(cType, "statPrimitive", cComp);
    test(rComp);

    // -----
    
    // tests field interceptors (changeComponent can be used, but will not
    // preserve the component's state, since the content and controller part are
    // merged in 'fullStatPrimitive' components)
    System.err.println("CREATE A NEW COMPONENT WITH A FIELD STAT CONTROLLER");
    cComp = cf.newFcInstance(
        cType, "fullStatPrimitive", "interceptor.IImpl");
    rComp = cf.newFcInstance(
        cType, "composite", null);
    Fractal.getContentController(rComp).addFcSubComponent(cComp);
    Fractal.getBindingController(rComp).bindFc("s", cComp.getFcInterface("s"));
    Fractal.getLifeCycleController(rComp).startFc();
    test(rComp);
  }

  private static void test (final Component rComp)
    throws NoSuchInterfaceException
  {
    System.err.println();

    I i = (I)rComp.getFcInterface("s");
    i.m("a");
    i.m("b");
    try {
      i.m(null);
    } catch (NullPointerException e) {
    }
    i.m("c");

    System.err.println();

    StatController sc;
    try {
      Component c = Fractal.getContentController(rComp).getFcSubComponents()[0];
      sc = (StatController)c.getFcInterface("stat-controller");
    } catch (NoSuchInterfaceException e) {
      System.err.println("No StatController!");
      System.err.println();
      return;
    }

    System.err.println("number of calls: " + sc.getNumberOfMethodCall());
    System.err.println("number of success: " + sc.getNumberOfMethodSuccess());
    System.err.println("number of reads: " + sc.getNumberOfFieldRead());
    System.err.println("number of writes: " + sc.getNumberOfFieldWrite());
    System.err.println("total execution time: " + sc.getTotalExecutionTime());
    System.err.println();
  }

  /**
   * Changes the type, the controller objects and the interceptors of the given
   * primitive component. In fact a new component is created, with "physically"
   * the same content as the old component, the old component is unbound and
   * removed, and the new one is added and bound like the old one.
   * 
   * @param newType the new component type.
   * @param newControllerDesc the new controller descriptor.
   * @param component the component that must be replaced.
   * @return the new component.
   * @throws Exception if a problem occurs
   */

  public static Component changeComponent (
    final Type newType,
    final Object newControllerDesc,
    final Component component) throws Exception
  {
    // 1 stops "component"
    boolean isStarted = false;
    try {
      LifeCycleController lc = Fractal.getLifeCycleController(component);
      isStarted = lc.getFcState().equals("STARTED");
      if (isStarted) {
        Fractal.getLifeCycleController(component).stopFc();
      }
    } catch (NoSuchInterfaceException ignored) {
    }

    // 2 stops the parent components of "component"
    Component[] parents;
    LifeCycleController[] parentLCs;
    boolean[] parentStates;
    try {
      parents = Fractal.getSuperController(component).getFcSuperComponents();
      parentLCs = new LifeCycleController[parents.length];
      parentStates = new boolean[parents.length];
      for (int i = 0; i < parents.length; ++i) {
        try {
          parentLCs[i] = Fractal.getLifeCycleController(parents[i]);
          parentStates[i] = parentLCs[i].getFcState().equals("STARTED");
          if (parentStates[i]) {
            parentLCs[i].stopFc();
          }
        } catch (NoSuchInterfaceException ignored) {
        }
      }
    } catch (NoSuchInterfaceException e) {
      parents = new Component[0];
      parentLCs = null;
      parentStates = null;
    }

    // 3 unbinds "component"
    Map bindingsFrom = new HashMap();
    BindingController bc;
    try {
      bc = Fractal.getBindingController(component);
    } catch (NoSuchInterfaceException e) {
      bc = null;
    }
    if (bc != null) {
      String[] names = bc.listFc();
      for (int i = 0; i < names.length; ++i) {
        bindingsFrom.put(names[i], bc.lookupFc(names[i]));
        bc.unbindFc(names[i]);
      }
    }

    // 4 removes the bindings to "component"
    Map bindingsTo = new HashMap();
    Object[] itfs = component.getFcInterfaces();
    for (int i = 0; i < itfs.length; ++i) {
      Interface itf = (Interface)itfs[i];
      if (!((InterfaceType)itf.getFcItfType()).isFcClientItf()) {
        Set clients = Util.getFcClientItfsBoundTo(itf);
        Iterator it = clients.iterator();
        while (it.hasNext()) {
          Interface citf = (Interface)it.next();
          Fractal.getBindingController(
            citf.getFcItfOwner()).unbindFc(citf.getFcItfName());
          bindingsTo.put(citf, itf.getFcItfName());
        }
      }
    }

    // 5 removes "component" from its parent components
    for (int i = 0; i < parents.length; ++i) {
      Fractal.getContentController(parents[i]).removeFcSubComponent(component);
    }

    // creates the new component with the current component's content
    // (this part is the only Julia specific part)
    Object impl = component.getFcInterface("/content");
    Component boot = Fractal.getBootstrapComponent();
    GenericFactory gf = Fractal.getGenericFactory(boot);
    Component newComponent = gf.newFcInstance(newType, newControllerDesc, impl);

    // 5' adds "newComponent" to the parent components
    for (int i = 0; i < parents.length; ++i) {
      Fractal.getContentController(parents[i]).addFcSubComponent(newComponent);
    }

    // 4' adds the bindings to "newComponent"
    Iterator it = bindingsTo.entrySet().iterator();
    while (it.hasNext()) {
      Map.Entry e = (Map.Entry)it.next();
      Interface clientItf = (Interface)e.getKey();
      String serverItf = (String)e.getValue();
      Fractal.getBindingController(clientItf.getFcItfOwner()).bindFc(
        clientItf.getFcItfName(), newComponent.getFcInterface(serverItf));
    }

    // 3' binds "newComponent"
    try {
      bc = Fractal.getBindingController(newComponent);
    } catch (NoSuchInterfaceException e) {
      bc = null;
    }
    if (bc != null) {
      Iterator i = bindingsFrom.entrySet().iterator();
      while (i.hasNext()) {
        Map.Entry e = (Map.Entry)i.next();
        String clientItf = (String)e.getKey();
        Object serverItf = e.getValue();
        bc.bindFc(clientItf, serverItf);
      }
    }

    // 2' starts the parent components of "newComponent"
    for (int i = 0; i < parents.length; ++i) {
      if (parentStates[i]) {
        parentLCs[i].startFc();
      }
    }

    // 1' starts "newComponent"
    if (isStarted) {
      try {
        Fractal.getLifeCycleController(newComponent).startFc();
      } catch (NoSuchInterfaceException ignored) {
      }
    }

    // returns the new component
    return newComponent;
  }
}
