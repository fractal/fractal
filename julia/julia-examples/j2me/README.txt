To run this example, execute in order:

1) mvn -Prun.first: First execution, with J2SE, to generate the classes
2) mvn -Prun.second: Second execution, with J2SE, to generate the J2ME bootstrap component factory class
3) mvn -Prun.j2me: Conversion of the generated classes for J2ME and Execution with J2ME

