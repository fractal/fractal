/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package protoactive.core.body.metaobject;

import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.Fractal;
import org.objectweb.fractal.api.Component;

import org.objectweb.fractal.julia.loader.Loader;
import org.objectweb.fractal.julia.loader.Tree;

import protoactive.core.body.future.LocalFuture;
import protoactive.core.mop.MetaObject;

import java.lang.reflect.Method;
import java.util.List;
import java.util.ArrayList;

/**
 * A meta object to handle reified method calls in an asynchronous way. This
 * meta object is a controller object that also implements the
 * LifeCycleController interface.
 */

public class BodyMetaObject
  implements LifeCycleController, MetaObject, Runnable
{

  private List requests;

  private boolean running;

  private boolean requestStop;

  /**
   * Creates a future object to wait for the reply of the given call, and
   * push the reified method call and its associated future in the request
   * list. Returns the future object immediately.
   * 
   * @param itf the name of the Fractal interface that is called.
   * @param target the base object.
   * @param m the method that is called.
   * @param args the method call arguments.
   * @return the result of the method call, as a future object.
   */

  public Object handleMethodCall (
    final String itf,
    final Object target,
    final Method m,
    final Object[] args)
  {
    Request r = new Request();
    r.target = target;
    r.m = m;
    r.args = args;

    Class c = m.getReturnType();
    if (c == Void.TYPE) {
      r.future = null;
    } else {
      try {
        Component boot = Fractal.getBootstrapComponent();
        Loader loader = (Loader)boot.getFcInterface("loader");
        r.future = (LocalFuture)loader.newObject(
          new Tree(new Tree[] {
            new Tree(new Tree[] {
              new Tree("protoactive.core.body.future.FutureClassGenerator"),
              new Tree("protoactive.core.body.future.LocalFuture"),
              new Tree(new Tree[] { new Tree(c.getName()) })
            })
          }), null
        );
      } catch (Exception e) {
        e.printStackTrace();
        throw new Error();
      }
    }

    // put request into request list
    synchronized (this) {
      if (requests == null) {
       requests = new ArrayList();
      }
      requests.add(r);
      notifyAll();
    }

    // return the future object
    return r.future;
  }

  public boolean isReflectedCall () {
    Class[] stack = Helper.INSTANCE.getClassContext();
    return
      stack.length > 3 &&
      stack[3].getName().equals("protoactive.core.body.metaobject.Request");
  }

  public String getFcState () {
    return (running ? STARTED : STOPPED);
  }

  public void startFc () {
    synchronized (this) {
      if (!running) {
        // if not already running, start a thread to handle requests
        running = true;
        requestStop = false;
        new Thread(this).start();
      }
    }
  }

  public void stopFc () {
    synchronized (this) {
      // ask the service thread to stop
      requestStop = true;
      notifyAll();
      // wait until is effectively stopped
      while (running) {
        try {
          wait();
        } catch (InterruptedException e) {
        }
      }
    }
  }

  /**
   * Code of the thread that handles requests in FIFO order. This thread
   * gets a request from the request list, executes the corresponding method
   * call, and puts the result in the associated future object. It then
   * gets another request, and so on until the 'requestStop' flag is true. It
   * then sets the 'running' flag to 'false' and stops itself.
   */

  public void run () {
    while (true) {
      // get a request from request list
      Request r;
      synchronized (this) {
        // block until there is at least one request in the queue,
        // or requestStop is true
        while ((requests == null || requests.size() == 0) && !requestStop) {
          try {
            wait();
          } catch (InterruptedException e) {
          }
        }
        if (!requestStop) {
          r = (Request)requests.remove(0);
        } else {
          // we are asked to stop, so we stop ourselves, and notify others that
          // we have stopped ourselves.
          running = false;
          notifyAll();
          return;
        }
      }
      // respond to the request
      System.err.println(
        Thread.currentThread() + " HANDLE REQUEST " + r);
      Object reply = r.invoke();
      if (r.future != null) {
        r.future.receiveReply(reply);
      }
    }
  }

  public static class Helper extends SecurityManager {

    public final static Helper INSTANCE = new Helper();

    public Class[] getClassContext() {
      return super.getClassContext();
    }
  }
}
