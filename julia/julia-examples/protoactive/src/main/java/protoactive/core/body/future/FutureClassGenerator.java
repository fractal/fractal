/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 * Contributor: Philippe Merle
 */

package protoactive.core.body.future;

import org.objectweb.fractal.julia.asm.AbstractClassGenerator;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;

import java.lang.reflect.Method;

/**
 * A class generator to generate LocalFuture classes.
 * This class generator generates sub classes of the LocalFuture class that
 * implement the given set of interfaces.
 * <p>
 * The code generated for the I interface below is the following:
 * <pre>
 * public interface I {
 *   void m (int i);
 *   Object n (String s);
 * }
 *
 * public class <i>XYZ</i> extends LocalFuture implements I, Generated {
 *
 *   public void m (int i) {
 *     waitReply();
 *     ((I)reply).m();
 *   }
 *
 *   public Object n (String s) {
 *     waitReply();
 *     return ((I)reply).n(s);
 *   }
 *
 *   public String getFcGeneratorParameters () {
 *     return "(... LocalFuture (I))";
 *   }
 * }
 * </pre>
 */

public class FutureClassGenerator extends AbstractClassGenerator {

  protected void generateMethod (final Method m) {
    // generate the header of the forwarder method
    String itf = m.getDeclaringClass().getName().replace('.', '/');
    String mName = m.getName();
    String mDesc = Type.getMethodDescriptor(m);
    Class[] params = m.getParameterTypes();
    Class result = m.getReturnType();
    Class[] exceptions = m.getExceptionTypes();
    String[] excepts = new String[exceptions.length];
    for (int i = 0; i < exceptions.length; ++i) {
      excepts[i] = exceptions[i].getName().replace('.', '/');
    }
    MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, mName, mDesc, null, excepts);

    // generate code to invoke 'waitReply'
    mv.visitVarInsn(ALOAD, 0);
    mv.visitMethodInsn(INVOKEVIRTUAL, name, "waitReply", "()V");

    // generate code to load and cast the 'reply' field
    mv.visitVarInsn(ALOAD, 0);
    mv.visitFieldInsn(GETFIELD, name, "reply", "Ljava/lang/Object;");
    mv.visitTypeInsn(CHECKCAST, itf);

    // generate code to push method parameters
    int offset = 1;
    for (int i = 0; i < params.length; ++i) {
      mv.visitVarInsn(ILOAD + getOpcodeOffset(params[i]), offset);
      offset += getSize(params[i]);
    }

    // generate code to invoke method
    mv.visitMethodInsn(INVOKEINTERFACE, itf, mName, mDesc);

    // generate code to return result
    if (result == Void.TYPE) {
      mv.visitInsn(RETURN);
    } else {
      mv.visitInsn(IRETURN + getOpcodeOffset(result));
    }

    // end method
    mv.visitMaxs(Math.max(offset, getSize(result)), offset);
  }
}
