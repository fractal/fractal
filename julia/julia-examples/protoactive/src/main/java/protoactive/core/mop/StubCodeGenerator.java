/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package protoactive.core.mop;

import org.objectweb.fractal.julia.asm.MetaCodeGenerator;

/**
 * A {@link CodeGenerator CodeGenerator} to generate interception code that
 * reifies method calls and pass them to the MetaObject interface of the
 * component. More precisely, the interception code generated for a method m:
 * <pre>
 * int m (int i, String s) {
 *   // original code
 * }
 * </pre>
 * is the following:
 * <pre>
 * int m (int i, String s) {
 *   if (!AbstractMetaObject.isReflectedCall()) {
 *     return ((Integer)mo.handleMethodCall(new MethodCall(
 *       "I",
 *       "m",
 *       "(ILjava/lang/String;)I",
 *       new Object[] {new Integer(i), s}))).intValue();
 *   } else {
 *     // original code
 *   }
 * }
 * </pre>
 * The {@link #generateInitCode generateInitCode} method generates code blocks
 * of the following form:
 * <pre>
 * mo = (...)ic.getInterface("/meta-object");
 * </pre>
 * where <tt>mo</tt> is a field added to the class that is being generated (in
 * fact, if the controller and interceptor classes are merged (see {@link
 * InterceptorClassGenerator InterceptorClassGenerator}), the <tt>mo</tt> field
 * is not generated and replaced by <tt>this</tt> in the above code).
 */

public class StubCodeGenerator extends MetaCodeGenerator {

  protected String getControllerInterfaceName () {
    return "/meta-object";
  }

  protected String getHandleMethodCallMethodName () {
    return "handleMethodCall";
  }

  protected boolean reifyInterfaceName () {
    return true;
  }

  protected boolean reifyTargetObject () {
    return true;
  }

  protected String getIsReflectedCallMethodName () {
    return "isReflectedCall";
  }
}
