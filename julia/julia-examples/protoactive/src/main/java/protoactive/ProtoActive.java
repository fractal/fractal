/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package protoactive;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.ComponentType;

import org.objectweb.fractal.util.Fractal;

public class ProtoActive {

  private ProtoActive () {
  }

  public static Component createActive (
    final ComponentType type,
    final String className)
  {
    try {
      Component boot = Fractal.getBootstrapComponent();
      GenericFactory cf = Fractal.getGenericFactory(boot);
      Component comp = cf.newFcInstance(type, "primitive", className);
      Fractal.getLifeCycleController(comp).startFc();
      return comp;
    } catch (Exception e) {
      e.printStackTrace();
      System.exit(0);
      return null;
    }
  }

  public static void main (final String[] args) throws Exception {

    Component treeComp = ProtoActive.createActive(
      BinaryTreeImpl.getType(), "protoactive.BinaryTreeImpl");

    BinaryTree t = (BinaryTree)treeComp.getFcInterface("server");

    t.put(1, "one");
    t.put(2, "two");
    t.put(3, "three");
    t.put(4, "four");

    System.err.println("Value associated to key 3 = " + t.get(3));
    System.err.println("Value associated to key 4 = " + t.get(4));
    System.err.println("Value associated to key 2 = " + t.get(2));
    System.err.println("Value associated to key 1 = " + t.get(1));
  }
}
