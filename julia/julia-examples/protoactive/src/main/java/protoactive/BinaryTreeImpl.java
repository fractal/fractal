/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package protoactive;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;

import org.objectweb.fractal.util.Fractal;


public class BinaryTreeImpl implements BinaryTree, BindingController {

  private int key;
  private Value value;

  private BinaryTree leftTree;
  private BinaryTree rightTree;

  private static ComponentType TYPE;

  // implementation of the BinaryTree interface

  public Value get (int key) {
    if (leftTree == null && rightTree == null) {
      return null;
    }
    if (key == this.key) {
      return value;
    } else if (key < this.key) {
      return leftTree.get(key);
    } else {
      return rightTree.get(key);
    }
  }

  public void put (int key, Object value) {
    if (leftTree == null && rightTree == null) {
      this.key = key;
      this.value = new ValueImpl(value);

      Component leftComp =
        ProtoActive.createActive(getType(), "protoactive.BinaryTreeImpl");
      Component rightComp =
        ProtoActive.createActive(getType(), "protoactive.BinaryTreeImpl");

      try {
        leftTree = (BinaryTree)leftComp.getFcInterface("server");
        rightTree = (BinaryTree)rightComp.getFcInterface("server");
      } catch (NoSuchInterfaceException e) {
        e.printStackTrace();
        System.exit(0);
      }
    }
    if (key == this.key) {
      this.value = new ValueImpl(value);
    } else if (key < this.key) {
      leftTree.put(key, value);
    } else {
      rightTree.put(key, value);
    }
  }

  // implementation of the BindingController interface

  public String[] listFc () {
    return new String[] { "left", "right" };
  }

  public Object lookupFc (String clientItfName) {
    if (clientItfName.equals("left")) {
      return leftTree;
    } else if (clientItfName.equals("right")) {
      return rightTree;
    }
    return null;
  }

  public void bindFc (String clientItfName, Object serverItf) {
    if (clientItfName.equals("left")) {
      leftTree = (BinaryTree)serverItf;
    } else if (clientItfName.equals("right")) {
      rightTree = (BinaryTree)serverItf;
    }
  }

  public void unbindFc (String clientItfName) {
    if (clientItfName.equals("left")) {
      leftTree = null;
    } else if (clientItfName.equals("right")) {
      rightTree = null;
    }
  }

  // utility classes and methods

  static ComponentType getType () {
    if (TYPE == null) {
      try {
      Component boot = Fractal.getBootstrapComponent();
      TypeFactory tf = Fractal.getTypeFactory(boot);
      TYPE = tf.createFcType(new InterfaceType[] {
        tf.createFcItfType("server", "protoactive.BinaryTree", false, false, false),
        tf.createFcItfType("left", "protoactive.BinaryTree", true, false, false),
        tf.createFcItfType("right", "protoactive.BinaryTree", true, false, false)
      });
      } catch (Exception e) {
        e.printStackTrace();
        System.exit(0);
      }
    }
    return TYPE;
  }

  static class ValueImpl implements Value {

    private Object value;

    public ValueImpl (final Object value) {
      this.value = value;
    }

    public String toString () {
      return value.toString();
    }
  }
}
