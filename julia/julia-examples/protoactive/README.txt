This example shows how to extend Julia in order to provide Fractal components
that communicate through asynchronous method calls with futures, as in
ProActive (see http://www.objectweb.org).

In order to do this we define a MetaObject interface (similar to the Proxy
interface in ProActive), that all meta objects must implement, as well as a
StubClassGenerator (similar to the BytecodeStubBuilder in ProActive) class
generator, whose role is to generate code to reify calls for each method of
each functional interface of the component, and to pass these reified method
calls to the MetaObject interface provided by the controller part of the
component.

We then define a BodyMetaObject meta object that handles method calls in the
following way:
- create a future object to hold the reply,
- put the reified method call and its associated future in a "request" queue,
- return the future object immediately
A separate thread services the requests in the request queue in FIFO order,
and puts the results in the future objects. The classes of the future objects
are dynamically created by a FutureClassGenerator class generator.

In order to create an "active" Julia component, we just have to add a
BodyMetaObject in the controller part of the component, and to use the
StubClassGenerator as interceptor class generator (see the julia.cfg file).


Note: although this example is inspired from ProActive, and uses similar
package and class names, it is just a very small prototype used to illustrate
how Julia can be extended (hence the 'protoactive' name). In particular, many
ProActive features are not implemented in this example: distribution (and
therefore mobility), security, call by value, exception handling... Finally,
the return type of an asynchronous methods cannot be a class (it must be
an interface or void - this is not an inherent limitation: it just happens
that this feature is currently not implemented).
