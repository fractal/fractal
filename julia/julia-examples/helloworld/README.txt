This example shows how to program a simple Fractal based application,
and how to deploy it. It also shows how Julia can be configured, and how
the Fractal ADL can be used to describe the architecture of an application,
and to deploy it.

See the Fractal, the Fractal ADL and the Julia tutorials for more details
about this example.
