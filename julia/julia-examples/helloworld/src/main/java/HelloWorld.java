

/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;

import org.objectweb.fractal.util.Fractal;

public class HelloWorld {

  public static void main (final String[] args) throws Exception {

    boolean useTemplates = false;
    boolean useWrapper = false;
    for (int i = 0; i < args.length; ++i) {
      useTemplates |= args[i].equals("templates");
      useWrapper |= args[i].equals("wrapper");
    }

    Component rComp;

    Component boot = Fractal.getBootstrapComponent();
    TypeFactory tf = Fractal.getTypeFactory(boot);
    // type of root component
    ComponentType rType = tf.createFcType(new InterfaceType[] {
      tf.createFcItfType("m", "Main", false, false, false)
    });
    // type of client component
    ComponentType cType = tf.createFcType(new InterfaceType[] {
      tf.createFcItfType("m", "Main", false, false, false),
      tf.createFcItfType("s", "Service", true, false, false)
    });
    // type of server component
    ComponentType sType = tf.createFcType(new InterfaceType[] {
      tf.createFcItfType("s", "Service", false, false, false),
      tf.createFcItfType(
        "attribute-controller",
        "ServiceAttributes",
        false,
        false,
        false)
    });

    GenericFactory cf = Fractal.getGenericFactory(boot);

    if (!useTemplates) {

      // -------------------------------------------------------------------
      // OPTION 1 : CREATE COMPONENTS DIRECTLY
      // -------------------------------------------------------------------

      // create root component
      rComp = cf.newFcInstance(rType, "composite", null);
      // create client component
      Component cComp = cf.newFcInstance(cType, "primitive", new ClientImpl());
      // create server component
      Component sComp = cf.newFcInstance(sType, "primitive", ServerImpl.class.getName()); // "ServerImpl"
      ((ServiceAttributes)Fractal.getAttributeController(sComp)).setHeader("-> ");
      ((ServiceAttributes)Fractal.getAttributeController(sComp)).setCount(1);

      if (useWrapper) {
        sType = tf.createFcType(new InterfaceType[] {
        tf.createFcItfType("s", "Service", false, false, false)
        });
        // create client component "wrapper" component
        Component CComp = cf.newFcInstance(cType, "composite", null);
        // create server component "wrapper" component
        Component SComp = cf.newFcInstance(sType, "composite", null);
        // component assembly
        Fractal.getContentController(CComp).addFcSubComponent(cComp);
        Fractal.getContentController(SComp).addFcSubComponent(sComp);
        Fractal.getBindingController(CComp).bindFc("m",cComp.getFcInterface("m"));
        Fractal.getBindingController(cComp).bindFc("s",
          Fractal.getContentController(CComp).getFcInternalInterface("s"));
        Fractal.getBindingController(SComp).bindFc("s",sComp.getFcInterface("s"));
        // replaces client and server components by "wrapper" components
        cComp = CComp;
        sComp = SComp;
      }

      // component assembly
      Fractal.getContentController(rComp).addFcSubComponent(cComp);
      Fractal.getContentController(rComp).addFcSubComponent(sComp);
      Fractal.getBindingController(rComp).bindFc("m", cComp.getFcInterface("m"));
      Fractal.getBindingController(cComp).bindFc("s", sComp.getFcInterface("s"));
    } else {

      // -------------------------------------------------------------------
      // OPTION 2 : CREATE COMPONENTS THROUGH TEMPLATES
      // -------------------------------------------------------------------

      // template to create root component
      Component rTmpl = cf.newFcInstance(
        rType, "compositeTemplate", new Object[] { "composite", null });
      // template to create client component
      Component cTmpl = cf.newFcInstance(
        cType, "primitiveTemplate", new Object[] { "primitive", "ClientImpl" });
      // template to create server component
      Component sTmpl = cf.newFcInstance(
        sType, "parametricPrimitiveTemplate", new Object[] { "primitive", "ServerImpl" });
      ((ServiceAttributes)Fractal.getAttributeController(sTmpl)).setHeader("-> ");
      ((ServiceAttributes)Fractal.getAttributeController(sTmpl)).setCount(1);

      if (useWrapper) {
        sType = tf.createFcType(new InterfaceType[] {
        tf.createFcItfType("s", "Service", false, false, false)
        });
        // template to create client component "wrapper" component
        Component CTmpl = cf.newFcInstance(
          cType, "compositeTemplate", new Object[] { "composite", null });
        // template to create server component "wrapper" component
        Component STmpl = cf.newFcInstance(
          sType, "compositeTemplate", new Object[] { "composite", null });
        // template component assembly
        Fractal.getContentController(CTmpl).addFcSubComponent(cTmpl);
        Fractal.getContentController(STmpl).addFcSubComponent(sTmpl);
        Fractal.getBindingController(CTmpl).bindFc("m",cTmpl.getFcInterface("m"));
        Fractal.getBindingController(cTmpl).bindFc("s",
          Fractal.getContentController(CTmpl).getFcInternalInterface("s"));
        Fractal.getBindingController(STmpl).bindFc("s",sTmpl.getFcInterface("s"));
        // replaces client and server templates by "wrapper" templates
        cTmpl = CTmpl;
        sTmpl = STmpl;
      }

      // template component assembly
      Fractal.getContentController(rTmpl).addFcSubComponent(cTmpl);
      Fractal.getContentController(rTmpl).addFcSubComponent(sTmpl);
      Fractal.getBindingController(rTmpl).bindFc("m", cTmpl.getFcInterface("m"));
      Fractal.getBindingController(cTmpl).bindFc("s", sTmpl.getFcInterface("s"));

      // template instantiation
      rComp = Fractal.getFactory(rTmpl).newFcInstance();
    }

    // -----------------------------------------------------------------------
    // COMMON PART
    // -----------------------------------------------------------------------

    // start root component
    Fractal.getLifeCycleController(rComp).startFc();

    // call main method
    ((Main)rComp.getFcInterface("m")).main(null);
  }

  // Julia specific code, to print the content of created components

  public static class DebugController
    implements org.objectweb.fractal.julia.Controller
  {
    public void initFcController (
      org.objectweb.fractal.julia.InitializationContext ic)
    {
      System.err.println("COMPONENT CREATED");
      System.err.println("INTERFACES:");
      java.util.Iterator i = ic.interfaces.keySet().iterator();
      while (i.hasNext()) {
        String key = (String)i.next();
        if (!key.startsWith("/")) {
          System.err.println("  "+key+" "+ic.interfaces.get(key));
        }
      }
      System.err.println("CONTROLLER OBJECTS:");
      i = ic.controllers.iterator();
      while (i.hasNext()) {
        Object o = i.next();
        if (o instanceof org.objectweb.fractal.julia.Interceptor) {
          Object p = ((org.objectweb.fractal.julia.Interceptor)o).getFcItfDelegate();
          System.err.println("  "+o+" (Interceptor,impl="+p+")");
        } else {
          System.err.println("  "+o);
        }
      }
      System.err.println("CONTENT:\n  "+ic.content);
    }
  }
}
