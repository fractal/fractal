/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

import java.util.Arrays;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;

import org.objectweb.fractal.util.Fractal;

public class Collection {

  public static void main (String[] args) throws Exception {
    Component boot = Fractal.getBootstrapComponent();

    TypeFactory tf = Fractal.getTypeFactory(boot);
    ComponentType aType = tf.createFcType(new InterfaceType[0]);
    ComponentType bType = tf.createFcType(new InterfaceType[] {
      tf.createFcItfType("client", "I", true, true, true)
    });
    ComponentType sType = tf.createFcType(new InterfaceType[] {
      tf.createFcItfType("server", "I", false, false, false) 
    });
    
    GenericFactory cf = Fractal.getGenericFactory(boot);

    Component aComp = cf.newFcInstance(aType, "composite", null);
    Component bComp = cf.newFcInstance(bType, "autoBindingComposite", null);
    Component cComp = cf.newFcInstance(bType, "autoBindingPrimitive", "CImpl");
    Component s1Comp = cf.newFcInstance(sType, "primitive", "SImpl");
    Component s2Comp = cf.newFcInstance(sType, "primitive", "SImpl");
    Component s3Comp = cf.newFcInstance(sType, "primitive", "SImpl");
    
    Fractal.getContentController(aComp).addFcSubComponent(bComp);
    Fractal.getContentController(aComp).addFcSubComponent(s1Comp);
    Fractal.getContentController(aComp).addFcSubComponent(s2Comp);
    Fractal.getContentController(aComp).addFcSubComponent(s3Comp);
    Fractal.getContentController(bComp).addFcSubComponent(cComp);
    
    // creates a binding between b and s1
    Fractal.getBindingController(bComp).bindFc(
      "client-I", s1Comp.getFcInterface("server"));

    // creates a "model" binding between c and b
    // this automatically creates similar bindings bewteen c and b, 
    // for each interface in the collection in b
    Fractal.getBindingController(cComp).bindFc(
      "client", 
      Fractal.getContentController(bComp).getFcInternalInterface("client"));
    
    // we can check this with the following code:
    String[] itfs = Fractal.getBindingController(cComp).listFc();
    System.out.println(Arrays.asList(itfs));

    // during the following bindings between b and si,
    // similar bindings are created between c and b (following the model binding)
    Fractal.getBindingController(bComp).bindFc(
      "client-II", s2Comp.getFcInterface("server"));
    Fractal.getBindingController(bComp).bindFc(
      "client-III", s3Comp.getFcInterface("server"));
     
    // we can check this with the following code:
    itfs = Fractal.getBindingController(cComp).listFc();
    System.out.println(Arrays.asList(itfs));
    
    // removing bindings between b and si also removes bindings between c and b
    Fractal.getBindingController(bComp).unbindFc("client-I");
    Fractal.getBindingController(bComp).unbindFc("client-II");
    Fractal.getBindingController(bComp).unbindFc("client-III");
    
    itfs = Fractal.getBindingController(cComp).listFc();
    System.out.println(Arrays.asList(itfs));
  }
}
