/***
 * Julia
 * Copyright (C) 2005-2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.conform;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.julia.conform.Test;
import org.objectweb.fractal.julia.conform.components.C;
import org.objectweb.fractal.julia.conform.components.CAttributes;
import org.objectweb.fractal.julia.conform.components.I;
import org.objectweb.fractal.koch.control.interceptor.InterceptorController;
import org.objectweb.fractal.koch.control.membrane.MembraneController;
import org.objectweb.fractal.util.Fractal;

/**
 * JUnit test for the membrane controller of Juliak.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5
 */
public class TestMembraneController extends Test {

    protected Component boot;
    protected TypeFactory tf;
    protected GenericFactory gf;

    protected ComponentType t;
    protected Component c;

    protected final static String MCOMP = "//"+COMP;
    protected final static String MBC = "//"+BC;
    protected final static String MLC = "//"+LC;
    protected final static String MSC = "//"+SC;
    protected final static String MNC = "//"+NC;
    protected final static String MMC = "//"+MembraneController.NAME+"/"+MembraneController.TYPE.getFcItfSignature()+"/false,false,false";
    protected final static String MIC = "//"+InterceptorController.NAME+"/"+InterceptorController.TYPE.getFcItfSignature()+"/false,false,false";
    
    // -------------------------------------------------------------------------
    // Constructor and setup
    // -------------------------------------------------------------------------

    public TestMembraneController(final String name) {
      super(name);
    }

    protected void setUp () throws InstantiationException, NoSuchInterfaceException {
      boot = Fractal.getBootstrapComponent();
      tf = Fractal.getTypeFactory(boot);
      gf = Fractal.getGenericFactory(boot);
      t = tf.createFcType(new InterfaceType[] {
        tf.createFcItfType("attribute-controller", CAttributes.class.getName(), false, false, false),
        tf.createFcItfType("server", I.class.getName(), false, false, false),
        tf.createFcItfType("client", I.class.getName(), true, true, false)
      });
      setUpComponents();
    }

    protected void setUpComponents () throws InstantiationException {
        c = gf.newFcInstance(t, "primitive", C.class.getName());
    }
    
    // -------------------------------------------------------------------------
    // Test MembraneController
    // -------------------------------------------------------------------------

    public void testMembraneController() throws NoSuchInterfaceException {
        MembraneController mc = (MembraneController)
            c.getFcInterface(MembraneController.NAME);
        assertNotNull(mc);
        
        Component membrane = mc.getFcMembrane();
        assertNotNull(membrane);
        
        checkComponent(membrane, new HashSet(Arrays.asList(new Object[]{
            COMP, BC, CC, SC, NC, LC, MCOMP, MBC, MLC, MSC, MNC, MMC, MIC
        })));
    }

    protected void checkComponent (Component c, Set itfs) {
        Set extItfs = getExternalItfs(c);
        assertEquals("Wrong external interface list", itfs, extItfs);
    }
}
