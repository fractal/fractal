/***
 * Julia
 * Copyright (C) 2005-2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.conform;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.julia.conform.Test;
import org.objectweb.fractal.julia.conform.components.C;
import org.objectweb.fractal.julia.conform.components.CAttributes;
import org.objectweb.fractal.julia.conform.components.I;
import org.objectweb.fractal.koch.factory.MembraneFactory;
import org.objectweb.fractal.util.Fractal;

/**
 * JUnit test for the component factory features which are specific to Juliak.
 *  
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5
 */
public class TestComponent extends Test {

  protected Component boot;
  protected TypeFactory tf;
  protected GenericFactory gf;

  protected ComponentType t;

  protected final static String AC = "attribute-controller/"+PKG+".CAttributes/false,false,false";
  protected final static String sI = "server/"+PKG+".I/false,false,false";
  protected final static String cI = "client/"+PKG+".I/true,false,false";

  // -------------------------------------------------------------------------
  // Constructor and setup
  // -------------------------------------------------------------------------

  public TestComponent (final String name) {
    super(name);
  }

  protected void setUp () throws InstantiationException, NoSuchInterfaceException {
    boot = Fractal.getBootstrapComponent();
    tf = Fractal.getTypeFactory(boot);
    gf = Fractal.getGenericFactory(boot);
    t = tf.createFcType(new InterfaceType[] {
      tf.createFcItfType("attribute-controller", CAttributes.class.getName(), false, false, false),
      tf.createFcItfType("server", I.class.getName(), false, false, false),
      tf.createFcItfType("client", I.class.getName(), true, true, false)
    });
  }

  // -------------------------------------------------------------------------
  // Test gluing a control membrane to a content
  // -------------------------------------------------------------------------

  public void testPrimitive() throws InstantiationException, IllegalLifeCycleException, NoSuchInterfaceException {
      MembraneFactory mf = (MembraneFactory) boot.getFcInterface("membrane-factory");
      Component membrane = mf.newFcMembrane("primitive");
      assertNotNull(membrane);
      
      Component c = gf.newFcInstance(t, membrane, C.class.getName());
      Fractal.getLifeCycleController(c).startFc();
      I i = (I)c.getFcInterface("server");
      checkInterface(i);
  }
}
