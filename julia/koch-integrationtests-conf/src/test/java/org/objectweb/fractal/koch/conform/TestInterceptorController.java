/***
 * Julia
 * Copyright (C) 2005-2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.conform;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.Interceptor;
import org.objectweb.fractal.julia.conform.Test;
import org.objectweb.fractal.julia.conform.components.C;
import org.objectweb.fractal.julia.conform.components.I;
import org.objectweb.fractal.julia.conform.controllers.BasicStatController;
import org.objectweb.fractal.koch.control.interceptor.IllegalInterceptorException;
import org.objectweb.fractal.koch.control.interceptor.InterceptorController;
import org.objectweb.fractal.util.Fractal;

/**
 * JUnit test for the interceptor controller defined by Juliak.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5
 */
public class TestInterceptorController extends Test {

    protected Component boot;
    protected TypeFactory tf;
    protected GenericFactory gf;

    protected ComponentType t;
    protected Component c;

    // -------------------------------------------------------------------------
    // Constructor and setup
    // -------------------------------------------------------------------------

    public TestInterceptorController(final String name) {
      super(name);
    }

    protected void setUp () throws InstantiationException, NoSuchInterfaceException {
      boot = Fractal.getBootstrapComponent();
      tf = Fractal.getTypeFactory(boot);
      gf = Fractal.getGenericFactory(boot);
      t = tf.createFcType(new InterfaceType[] {
              tf.createFcItfType("server", I.class.getName(), false, false, false),
              tf.createFcItfType("servers", I.class.getName(), false, false, true),
              tf.createFcItfType("client", I.class.getName(), true, true, false),
              tf.createFcItfType("clients", I.class.getName(), true, true, true)
            });
      setUpComponents();
    }

    protected void setUpComponents () throws InstantiationException {
        c = gf.newFcInstance(t, "primitive", C.class.getName());
    }
    
    // -------------------------------------------------------------------------
    // Test InterceptorController
    // -------------------------------------------------------------------------

    public void testInterceptorController() throws NoSuchInterfaceException {
        InterceptorController ic = (InterceptorController)
            c.getFcInterface(InterceptorController.NAME);
        assertNotNull(ic);
    }

    // -------------------------------------------------------------------------
    // Test getFcInterceptors
    // -------------------------------------------------------------------------

    public void testGetInterceptors() throws NoSuchInterfaceException {
        checkInterceptor("server");
        checkNoInterceptor("client");
    }

    public void testGetInterceptorsCollection() throws NoSuchInterfaceException {
        checkInterceptor("servers");
        checkInterceptor("servers0");
        checkNoInterceptor("clients");
        checkNoInterceptor("clients0");
    }
    
    protected void checkInterceptor(String itfName) throws NoSuchInterfaceException {
        InterceptorController ic = (InterceptorController)
            c.getFcInterface(InterceptorController.NAME);
    
        Object itf = c.getFcInterface(itfName);
        if( ! (itf instanceof ComponentInterface) ) {
            fail("Interceptor should implement ComponentInterface");
        }
        ComponentInterface citf = (ComponentInterface) itf;
        
        Interceptor[] interceptors = ic.getFcInterceptors(citf);
        assertEquals(1,interceptors.length);
        
        Interceptor i0 = interceptors[0];
        if( !(i0 instanceof I) ) {
            fail("Interceptor should implement I");
        }
    }

    protected void checkNoInterceptor(String itfName) throws NoSuchInterfaceException {
        InterceptorController ic = (InterceptorController)
            c.getFcInterface(InterceptorController.NAME);
    
        Object itf = c.getFcInterface(itfName);
        if( ! (itf instanceof ComponentInterface) ) {
            fail("Interceptor should implement ComponentInterface");
        }
        ComponentInterface citf = (ComponentInterface) itf;
        
        Interceptor[] interceptors = ic.getFcInterceptors(citf);
        assertEquals(0,interceptors.length);
    }

    // -------------------------------------------------------------------------
    // Test addFcInterceptors and removeFcInterceptors
    // -------------------------------------------------------------------------

    public void testServerAddRemove()
    throws
        NoSuchInterfaceException, IllegalLifeCycleException,
        IllegalBindingException, IllegalInterceptorException {
        
        checkServerAddRemove("server");
    }
    
    public void testServerCollectionAddRemove()
    throws
        NoSuchInterfaceException, IllegalLifeCycleException,
        IllegalBindingException, IllegalInterceptorException {
        
        checkServerAddRemove("servers");
        checkServerAddRemove("servers0");
    }
    
    protected void checkServerAddRemove(String itfName)
    throws
        NoSuchInterfaceException, IllegalLifeCycleException,
        IllegalBindingException, IllegalInterceptorException {
                
        // Check that there is 1 interceptor (lifecycle interceptor)
        InterceptorController ic = (InterceptorController)
            c.getFcInterface(InterceptorController.NAME);
        ComponentInterface citf = (ComponentInterface) c.getFcInterface(itfName);
        Interceptor[] interceptors = ic.getFcInterceptors(citf);
        assertEquals(1,interceptors.length);

        // Start the component
        LifeCycleController lc = Fractal.getLifeCycleController(c);
        lc.startFc();
        
        // Add the interceptor
        // Check that there are 2 interceptors (lifecycle + IInterceptor)
        IInterceptor ii = new IInterceptor();
        lc.stopFc();
        ic.addFcInterceptor(citf, ii);
        lc.startFc();
        interceptors = ic.getFcInterceptors(citf);
        assertEquals(2,interceptors.length);
        assertEquals(0, ii.fcCounter);
        
        // Call m(), check that the interceptor has been executed
        I iserver = (I) citf;
        iserver.m(true);
        assertEquals(1, ii.fcCounter);
        
        // Remove the interceptor, check that it has been removed
        lc.stopFc();
        ic.removeFcInterceptor(citf, ii);
        lc.startFc();
        interceptors = ic.getFcInterceptors(citf);
        assertEquals(1,interceptors.length);
        
        // Call m(), check that the interceptor has not been executed
        iserver.m(true);
        assertEquals(1, ii.fcCounter);
        
        // Stop the component
        lc.stopFc();
    }
    
    public void testClientAddBindAddRemove()
    throws
        NoSuchInterfaceException, IllegalLifeCycleException,
        IllegalBindingException, InstantiationException, IllegalContentException, IllegalInterceptorException {
        
        checkClientAddBindAddRemove("client");
    }
    
    public void testClientCollectionAddBindAddRemove()
    throws
        NoSuchInterfaceException, IllegalLifeCycleException,
        IllegalBindingException, InstantiationException,
        IllegalContentException, IllegalInterceptorException {
        
        checkClientAddBindAddRemove("clients");
        checkClientAddBindAddRemove("clients0");
    }
    
    protected void checkClientAddBindAddRemove(String itfName)
    throws
        NoSuchInterfaceException, IllegalLifeCycleException,
        IllegalBindingException, InstantiationException, IllegalContentException,
        IllegalInterceptorException {
        
        // Check that there is no interceptor
        InterceptorController ic = (InterceptorController)
            c.getFcInterface(InterceptorController.NAME);
        ComponentInterface citf = (ComponentInterface) c.getFcInterface(itfName);
        Interceptor[] interceptors = ic.getFcInterceptors(citf);
        assertEquals(0,interceptors.length);

        // Start the component
        LifeCycleController lc = Fractal.getLifeCycleController(c);
        lc.startFc();
        
        // Add the interceptor
        // Check that there is 1 interceptor (IInterceptor)
        IInterceptor ii = new IInterceptor();
        lc.stopFc();
        ic.addFcInterceptor(citf, ii);        
        lc.startFc();
        interceptors = ic.getFcInterceptors(citf);
        assertEquals(1,interceptors.length);
        assertEquals(0, ii.fcCounter);
        
        /*
         * Bind the client interface to the server interface of the component.
         * Insert the component in a composite in order to create a legal local
         * binding.
         */
        BindingController bc = Fractal.getBindingController(c);
        Type t = tf.createFcType(new InterfaceType[]{});
        Component root = gf.newFcInstance(t, "composite", null);
        ContentController cc = Fractal.getContentController(root);
        cc.addFcSubComponent(c);
        bc.bindFc(itfName, c.getFcInterface("server"));
        lc.startFc();
        
        // Call m(), check that the interceptor has been executed
        I iserver = (I) c.getFcInterface(itfName);
        iserver.m(true);
        assertEquals(1, ii.fcCounter);
        
        // Add another interceptor
        // Check that there are 2 interceptors
        IInterceptor ii2 = new IInterceptor();
        lc.stopFc();
        ic.addFcInterceptor(citf, ii2);
        lc.startFc();
        interceptors = ic.getFcInterceptors(citf);
        assertEquals(2,interceptors.length);
        assertEquals(0, ii2.fcCounter);
        
        // Call m(), check that both interceptors have been executed
        iserver.m(true);
        assertEquals(2, ii.fcCounter);
        assertEquals(1, ii2.fcCounter);
        
        // Remove the 1st interceptor, check that it has been removed
        lc.stopFc();
        ic.removeFcInterceptor(citf, ii);
        lc.startFc();
        interceptors = ic.getFcInterceptors(citf);
        assertEquals(1,interceptors.length);
        
        // Call m(), check that the interceptor has not been executed
        iserver.m(true);
        assertEquals(2, ii.fcCounter);

        // Stop the component
        lc.stopFc();
    }
    
    public void testClientBindAddRemove()
    throws
        NoSuchInterfaceException, IllegalLifeCycleException,
        IllegalBindingException, InstantiationException,
        IllegalContentException, IllegalInterceptorException {
        
        checkClientBindAddRemove("client");
    }
    
    protected void checkClientBindAddRemove(String itfName)
    throws
        NoSuchInterfaceException, IllegalLifeCycleException,
        IllegalBindingException, InstantiationException,
        IllegalContentException, IllegalInterceptorException {
        
        // Check that there is no interceptor
        InterceptorController ic = (InterceptorController)
            c.getFcInterface(InterceptorController.NAME);
        ComponentInterface citf = (ComponentInterface) c.getFcInterface(itfName);
        Interceptor[] interceptors = ic.getFcInterceptors(citf);
        assertEquals(0,interceptors.length);

        /*
         * Bind the client interface to the server interface of the component.
         * Insert the component in a composite in order to create a legal local
         * binding.
         */
        BindingController bc = Fractal.getBindingController(c);
        Type t = tf.createFcType(new InterfaceType[]{});
        Component root = gf.newFcInstance(t, "composite", null);
        ContentController cc = Fractal.getContentController(root);
        cc.addFcSubComponent(c);
        bc.bindFc(itfName, c.getFcInterface("server"));
        LifeCycleController lc = Fractal.getLifeCycleController(c);
        lc.startFc();
        
        // Add the interceptor
        // Check that there is 1 interceptor
        IInterceptor ii = new IInterceptor();
        lc.stopFc();
        ic.addFcInterceptor(citf, ii);
        lc.startFc();
        interceptors = ic.getFcInterceptors(citf);
        assertEquals(1,interceptors.length);
        assertEquals(0, ii.fcCounter);
        
        // Call m(), check that both interceptors have been executed
        I iserver = (I) citf;
        iserver.m(true);
        assertEquals(1, ii.fcCounter);
        
        // Remove the 1st interceptor, check that it has been removed
        lc.stopFc();
        ic.removeFcInterceptor(citf, ii);
        lc.startFc();
        interceptors = ic.getFcInterceptors(citf);
        assertEquals(0,interceptors.length);
        
        // Call m(), check that the interceptor has not been executed
        iserver.m(true);
        assertEquals(1, ii.fcCounter);

        // Stop the component
        lc.stopFc();
    }
    
    private static class IInterceptor extends BasicStatController implements Interceptor, I {

        public Object getFcItfDelegate() {
            return delegate;
        }

        public void setFcItfDelegate(Object delegate) {
            this.delegate = delegate;
        }
        
        private Object delegate;

        public Object clone () {
            return null;
        }
        
        public void initFcController(InitializationContext ic) throws InstantiationException {
        }

        public void m(boolean v) {
            fcCounter++;
            ((I)delegate).m(v);
        }

        public void m(byte v) {
            ((I)delegate).m(v);
        }

        public void m(char v) {
            ((I)delegate).m(v);
        }

        public void m(short v) {
            ((I)delegate).m(v);
        }

        public void m(int v) {
            ((I)delegate).m(v);
        }

        public void m(long v) {
            ((I)delegate).m(v);
        }

        public void m(float v) {
            ((I)delegate).m(v);
        }

        public void m(double v) {
            ((I)delegate).m(v);
        }

        public void m(String v) {
            ((I)delegate).m(v);
        }

        public void m(String[] v) {
            ((I)delegate).m(v);
        }

        public boolean n(boolean v, String[] w) {
            return ((I)delegate).n(v,w);
        }

        public byte n(byte v, String w) {
            return ((I)delegate).n(v,w);
        }

        public char n(char v, double w) {
            return ((I)delegate).n(v,w);
        }

        public short n(short v, float w) {
            return ((I)delegate).n(v,w);
        }

        public int n(int v, long w) {
            return ((I)delegate).n(v,w);
        }

        public long n(long v, int w) {
            return ((I)delegate).n(v,w);
        }

        public float n(float v, short w) {
            return ((I)delegate).n(v,w);
        }

        public double n(double v, char w) {
            return ((I)delegate).n(v,w);
        }

        public String n(String v, byte w) {
            return ((I)delegate).n(v,w);
        }

        public String[] n(String[] v, boolean w) {
            return ((I)delegate).n(v,w);
        }
        
    }
}
