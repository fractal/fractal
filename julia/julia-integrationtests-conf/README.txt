This module is used to launch Julia conformance tests. Test classes are defined in julia-tests-conf module, and
here are used via profiles. The pom.xml defines a number of execution profiles: each one can be activated using
standard command-line arguments. So:

mvn -Pnooptimization
mvn -Pmerge-controllers
mvn -Pmerge-controllers-and-interceptors
mvn -Pmerge-everything

