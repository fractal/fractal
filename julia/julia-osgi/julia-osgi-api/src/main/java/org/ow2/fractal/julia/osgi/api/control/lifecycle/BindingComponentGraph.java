/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2008 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Marc Leger
 */

package org.ow2.fractal.julia.osgi.api.control.lifecycle;

import java.util.*;

public final class BindingComponentGraph {

	// associates to each node the set of its successors in the graph
	private HashMap successors;

	// list of nodes to be visited
	private List toBeVisited;

	// list of nodes already visited
	private List visited;

	public BindingComponentGraph() {
		successors = new HashMap();
		toBeVisited = new ArrayList();
		visited = new ArrayList();
	}

	public void reset() {
		successors.clear();
		toBeVisited.clear();
		visited.clear();
	}

	private boolean hasCycle(Object src, Object dst) {
		toBeVisited.clear();
		visited.clear();

		toBeVisited.add(dst);
		while (!toBeVisited.isEmpty()) {
			Object node = toBeVisited.remove(toBeVisited.size() - 1);
			if (node.equals(src)) {
				return true;
			}
			visited.add(node);
			Set t = (Set) successors.get(node);
			if (t != null) {
				Iterator i = t.iterator();
				while (i.hasNext()) {
					Object successor = i.next();
					if (!visited.contains(successor)) {
						toBeVisited.add(successor);
					}
				}
			}
		}
		return false;
	}

	private synchronized boolean addVertexChecked(Object src, Object dst,
			Set s) {
		if (src.equals(dst) || s.contains(dst)) {
			return true;
		}
		if (hasCycle(src, dst)) {
			return false;
		} else {
			s.add(dst);
			return true;
		}
	}

	/**
	 * Adds a vertex in the graph, or returns false is this would create a
	 * cycle.
	 */
	public synchronized boolean addVertex(Object src, Object dst) {
		if (src == null || dst == null || dst == src) {
			return true;
		}
		return addVertexChecked(src, dst, addVertex(src));
	}

	public synchronized int addVertexes(Object src, List dsts) {
		if (src == null || dsts == null || dsts.size() == 0) {
			return -1;
		}
		Set s = addVertex(src);
		for (int i = 0; i < dsts.size(); i++) {
			if (!addVertexChecked(src, dsts.get(i), s)) {
				return i;
			}
		}
		return -1;
	}

	public synchronized Set addVertex(Object vertex) {
		Set s = (Set) successors.get(vertex);
		if (s == null) {
			s = new HashSet();
			successors.put(vertex, s);
		}
		return s;
	}

	public synchronized int addVertexes(List srcs, Object dst) {
		if (srcs == null || dst == null || srcs.size() == 0) {
			return -1;
		}
		for (int i = 0; i < srcs.size(); i++) {
			Set s = addVertex(srcs.get(i));
			if (!addVertexChecked(srcs.get(i), dst, s)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Removes a vertex in the graph.
	 */
	public synchronized void removeVertex(Object src, Object dst) {
		if (src == null || dst == null || dst == src) {
			return;
		}
		Set s = (Set) successors.get(src);
		if (s != null) {
			s.remove(dst);
			if (s.isEmpty()) {
				successors.remove(src);
			}
		}
	}

	public synchronized void removeEdgesFromVertex(Object edge) {
		if (edge != null)
			successors.remove(edge);
	}

	public synchronized void removeEdgesToVertex(Object edge) {
		if (edge != null) {
			Set keys = successors.keySet();
			Iterator it = keys.iterator();
			List empty = new ArrayList();
			while (it.hasNext()) {
				Object src = it.next();
				Set s = (Set) successors.get(src);
				s.remove(edge);
				if (s.isEmpty()) {
					empty.add(src);
				}
			}
			Iterator it2 = empty.iterator();
			while (it2.hasNext())
				successors.remove(it2.next());
		}
	}

	public synchronized void removeVertex(Object vertex) {
		removeEdgesFromVertex(vertex);
		removeEdgesToVertex(vertex);
	}

	public synchronized void removeVertexes(List srcs, Object dst) {
		if (srcs == null || dst == null || srcs.size() == 0) {
			return;
		}
		for (int i = 0; i < srcs.size(); i++) {
			Set s = (Set) successors.get(srcs.get(i));
			if (s != null) {
				s.remove(dst);
				if (s.isEmpty()) {
					successors.remove(srcs.get(i));
				}
			}
		}
	}

	public synchronized void removeVertexes(Object src, List dsts) {
		if (src == null || dsts == null || dsts.size() == 0) {
			return;
		}
		Set s = (Set) successors.get(src);
		if (s != null) {
			s.removeAll(dsts);
			if (s.isEmpty()) {
				successors.remove(src);
			}
		}
	}

	public synchronized Map getGraph() {
		return Collections.unmodifiableMap(successors);
	}

	public synchronized Set getVertexes() {
		Set vertexes = new HashSet();
		Set keys = successors.keySet();
		Iterator itSrc = keys.iterator();
		while (itSrc.hasNext()) {
			vertexes.addAll((Set) successors.get(itSrc.next()));
		}
		return vertexes;
	}

	public synchronized void printGraph() {
		Iterator itSrc = successors.keySet().iterator();
		while (itSrc.hasNext()) {
			Object src = itSrc.next();
			Set s = (Set) successors.get(src);
			Iterator itDst = s.iterator();
			while (itDst.hasNext()) {
				Object dst = itDst.next();
				System.out.println(src + " --> " + dst);
			}
		}
	}

}
