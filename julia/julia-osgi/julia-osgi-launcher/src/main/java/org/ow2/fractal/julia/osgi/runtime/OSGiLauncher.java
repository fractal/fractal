/***
 * Julia
 * Copyright (C) 2009 Bull S.A.S.
 * Copyright (C) 2009 INRIA, SARDES
 * Contact: fractal@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.fractal.julia.osgi.runtime;

/**
 * Interface of the OSGi launcher service.
 * @author Loris Bouzonnet
 */
public interface OSGiLauncher {

    /**
     * Property defining the class to load.
     */
    String CLASSNAME = "classname";

    /**
     * Property defining the interface name to use.
     */
    String INTERFACE = "interface";

    /**
     * Property defining the interface signature to use.
     */
    String SIGNATURE = "signature";

    /**
     * Property defining service properties.
     */
    String PROPERTIES = "properties";

}
