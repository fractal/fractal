/***
 * Julia
 * Copyright (C) 2009 Bull S.A.S.
 * Copyright (C) 2009 INRIA, SARDES
 * Contact: fractal@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.fractal.julia.osgi.runtime.impl;

import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.julia.control.content.Util;
import org.objectweb.fractal.util.Fractal;
import org.osgi.framework.Bundle;
import org.ow2.fractal.julia.osgi.api.control.OSGiContextController;
import org.ow2.fractal.julia.osgi.runtime.OSGiLauncher;


/**
 * Implementation of the OSGi launcher service.
 * Launch Fractal clients and register Fractal servers as services.
 * Use the iPOJO Extender Pattern Handler to detect on arrival bundles whose the extension is 'JuliacLauncher'.
 * @author Loris Bouzonnet
 */
public class OSGiLauncherExtender implements OSGiLauncher {

    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(OSGiLauncherExtender.class.getName());

    /**
     * Execute the Fractal clients.
     */
    private ExecutorService executorService;

    /**
     * Associate a bundle id with a launched client.
     */
    private final Map<Long, Future<?>> futures = new HashMap<Long, Future<?>>();

    void starting() {
        executorService = Executors.newCachedThreadPool();
        logger.info("Fractal Launcher ready!");
    }

    void stopping() {
        futures.clear();
        executorService.shutdownNow();
    }

    void onBundleArrival(Bundle bundle, String header) throws Exception {
        String args[] = header.split(",");
        if (args.length == 0) {
            logger.warning("Not a valid header: " + header);
            return;
        }
        HashMap<String, String> conf = new HashMap<String, String>(args.length);
        for (String arg : args) {
            String[] props = arg.split("=");
            if (props.length != 2) {
                logger.warning("Not a valid configuration: " + Arrays.toString(props));
                return;
            }
            conf.put(props[0], props[1]);
        }
        if (!conf.containsKey(CLASSNAME)) {
            logger.warning("Not a valid configuration: classname missing");
        }
        startManagementFor(bundle, conf);
    }

    void onBundleDeparture(Bundle bundle) {
        closeManagementFor(bundle);
    }

    private void startManagementFor(final Bundle bundle, final Map<String, String> conf) throws Exception {
        Class<?> cl = bundle.loadClass(conf.get(CLASSNAME));

        ClassLoader tccl = Thread.currentThread().getContextClassLoader();

        Thread.currentThread().setContextClassLoader(new ClassLoader(tccl) {
            @Override
            protected Class<?> findClass(String name) throws ClassNotFoundException {
                return bundle.loadClass(name);
            }
            @Override
            protected URL findResource(String name) {
                return bundle.getResource(name);
            }
        });

        try {
        	Object o = cl.newInstance();

        	if (o instanceof Factory) {

        		/*
        		 * Fractal factory.
        		 */
        		Factory factory = (Factory) o;
        		Component root = factory.newFcInstance();

        		// Set the bundle context on each sub-components
        		List<?> subComponents = Util.getAllSubComponents(root);
        		for (Object subComponent : subComponents) {
        			try {
        				OSGiContextController osgiContextController =
        					(OSGiContextController) ((Component) subComponent).getFcInterface("osgi-context-controller");
        				osgiContextController.setBundleContext(bundle.getBundleContext());
        			} catch (NoSuchInterfaceException e) {
        				logger.log(Level.FINE, "OSGi context controller missing", e);
        			}
        		}
        		Fractal.getLifeCycleController(root).startFc();

        		String itfName = conf.get(INTERFACE);

        		if (itfName != null) {
        			o = root.getFcInterface(itfName);
        		}
        	}

        	// Client or server ?
        	if (o instanceof Runnable) {

        		// A client, just run it
        		Runnable runnable = (Runnable) o;

        		futures.put(bundle.getBundleId(), executorService.submit(runnable));
        		logger.info("New client started: " + conf.get(CLASSNAME));

        	} else if (conf.containsKey(SIGNATURE)) {

        		// A service, register it
        		// Retrieve the properties for this service
        		Properties properties = new Properties();
        		String sprops = conf.get(PROPERTIES);
        		if (sprops != null) {
        			String[] props = sprops.split(";");
        			for (String sprop : props) {
        				String[] prop = sprop.split(":");
        				if (prop.length == 2) {
        					properties.setProperty(prop[0], prop[1]);
        				}
        			}
        		}
        		bundle.getBundleContext().registerService(conf.get(SIGNATURE), o, properties);
        		logger.info("New service registered: " + conf.get(SIGNATURE) + "; with properties: " + properties);
        	}
        } finally {
        	Thread.currentThread().setContextClassLoader(tccl);
        }
    }

    private void closeManagementFor(Bundle bundle) {
        // Who I am?
        Long bundleId = Long.valueOf(bundle.getBundleId());

        // Destroy the client created by this bundle
        Future<?> future = futures.remove(bundleId);
        if (future != null) {
            future.cancel(true);
        }

        // iPOJO unregisters it-self the services provided by this bundle :)
    }

}
