/**
 * Julia
 * Copyright (C) 2009 Bull S.A.S.
 * Copyright (C) 2009 INRIA, SARDES
 * Contact: fractal@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.fractal.julia.osgi.provider;

import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;

/**
 * @author Loris Bouzonnet
 */
public class FractalProvider {

	private final String alias;
	
	private final Component component;
	
	private final GenericFactory genericFactory;
	
	private final TypeFactory typeFactory;
	
	private final Map<InterfaceType, Object> interfaces;

	public FractalProvider(String alias, Component component,
			GenericFactory genericFactory, TypeFactory typeFactory, final Map<InterfaceType, Object> interfaces) {
		this.alias = alias;
		this.component = component;
		this.genericFactory = genericFactory;
		this.typeFactory = typeFactory;
		this.interfaces = interfaces;
	}

	public String getAlias() {
		return alias;
	}

	public Component getComponent() {
		return component;
	}

	public GenericFactory getGenericFactory() {
		return genericFactory;
	}

	public TypeFactory getTypeFactory() {
		return typeFactory;
	}

	public Map<InterfaceType, Object> getInterfaces() {
		return interfaces;
	}
	
}
