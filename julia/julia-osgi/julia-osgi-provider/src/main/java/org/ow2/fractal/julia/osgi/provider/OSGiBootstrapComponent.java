/**
 * Julia
 * Copyright (C) 2009 Bull S.A.S.
 * Copyright (C) 2009 INRIA, SARDES
 * Contact: fractal@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.fractal.julia.osgi.provider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.util.Fractal;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.ow2.fractal.julia.osgi.provider.api.BootstrapComponent;

/**
 * Fractal provider mixing the chained provider and the multi provider defined in Fractal Util and adapting them for OSGi:
 * the Fractal providers are retrieved from the service registry.
 * 
 * @author Loris Bouzonnet
 */
public class OSGiBootstrapComponent implements Component, GenericFactory, TypeFactory, ServiceListener {

	private static final String FRACTAL_PROVIDER_ALIAS = "fractal.provider.alias";

	private static final String JULIA_CONFG = "julia.config";

	private static Logger logger =
		Logger.getLogger(OSGiBootstrapComponent.class.getCanonicalName());

	private final ExtensibleComponentType type;
	
	private final Map<String, Object> interfaces;
	
	private final Map<String, FractalProvider> fractalProviders =
		new LinkedHashMap<String, FractalProvider>();
	
	private BundleContext context;

	private Set<ServiceReference> servicesRefs = new HashSet<ServiceReference>();

	public OSGiBootstrapComponent (final BundleContext context) {
		this.context = context;
		interfaces = new HashMap<String, Object>();
		interfaces.put("type-factory", this);
		interfaces.put("generic-factory", this);
		
		ArrayList<InterfaceType> interfaceTypes = new ArrayList<InterfaceType>();
		interfaceTypes.add(new BasicInterfaceType("type-factory", TypeFactory.class.getName(), false, false, false));
		interfaceTypes.add(new BasicInterfaceType("generic-factory", GenericFactory.class.getName(), false, false, false));
		type = new ExtensibleComponentType(interfaceTypes);
		
		ServiceReference[] refs = null;
		try {
			refs = context.getServiceReferences(Factory.class.getName(), null);
		} catch (InvalidSyntaxException e) {
			logger.log(Level.SEVERE, "Error when retrieving the Fractal providers", e);
		}
		if (refs != null) {
			for (ServiceReference ref : refs) {
				try {
					addFractalProvider(ref);
					servicesRefs.add(ref);
				} catch (InstantiationException e) {
					logger.log(Level.WARNING, "Error when adding the fractal provider with alias: "
							+ ref.getProperty(FRACTAL_PROVIDER_ALIAS), e);
				} catch (NoSuchInterfaceException e) {
					logger.log(Level.WARNING, "Error when adding the fractal provider with alias: "
							+ ref.getProperty(FRACTAL_PROVIDER_ALIAS), e);
				}
			}
		}
	}
	// -----------------------------------------------------------------
	// Implementation of the Component interface
	// -----------------------------------------------------------------

	public Object getFcInterface( final String itfName )
	throws NoSuchInterfaceException {

		Object itf = interfaces.get(itfName);
		if (itf == null) {
			throw new NoSuchInterfaceException(itfName);
		}
		
		return itf;
	}

	public Object[] getFcInterfaces() {
	
		return interfaces.values().toArray();
	}

	public Type getFcType() {

		return type;
	}

	// ---------------------------------------------------------------------
	// Implementation of the TypeFactory interface
	// ---------------------------------------------------------------------

	public InterfaceType createFcItfType(
			final String name, final String signature, final boolean isClient, final boolean isOptional,
			final boolean isCollection )
	throws InstantiationException {

		for (FractalProvider fractalProvider : fractalProviders.values()) {
			try {
				return fractalProvider.getTypeFactory().createFcItfType(
						name, signature, isClient, isOptional, isCollection);
			}
			catch( InstantiationException ie ) {
				/*
				 * The factory can not instantiate the interface type.
				 * Proceed with the next one in the list.
				 */
			}
		}

		String msg =
			"Can not instantiate interface type: "+name+","+signature+","+
			isClient+","+isOptional+","+isCollection+">";
		throw new InstantiationException(msg);
	}

	public ComponentType createFcType(final InterfaceType[] interfaceTypes)
	throws InstantiationException {

		for (FractalProvider fractalProvider : fractalProviders.values()) {
			try {
				return fractalProvider.getTypeFactory().createFcType(interfaceTypes);
			}
			catch( InstantiationException ie ) {
				/*
				 * The factory can not instantiate the interface type.
				 * Proceed with the next one in the list.
				 */
			}
		}

		String msg = "Can not instantiate component type: [";
		for (int i = 0; i < interfaceTypes.length; i++) {
			if(i!=0) msg+=",";
			msg += interfaceTypes[i].toString();
		}
		msg += "]";
		throw new InstantiationException(msg);
	}

	// ---------------------------------------------------------------------
	// Implementation of the GenericFactory interface
	// ---------------------------------------------------------------------

	public Component newFcInstance(final Type type, final Object controllerDesc, final Object contentDesc)
	throws InstantiationException {
		
		Object[] arrayControllerDesc;
		if (controllerDesc instanceof Object[]) {
			arrayControllerDesc = (Object[]) controllerDesc;
		} else {
			arrayControllerDesc = new Object[2];
            arrayControllerDesc[0] = Thread.currentThread().getContextClassLoader();
            
            if( controllerDesc instanceof String ) {
    			String str = (String) controllerDesc;
                
                /*
                 * Check whether the controller descriptor contains a prefix.
                 * In this case, use the corresponding generic factory.
                 * E.g. /julia/primitive designates a primitive component which
                 * must be instantiated with the registered /julia/ factory. 
                 */
                if( str.charAt(0) == '/' ) {
                    int lastslash = str.lastIndexOf('/');
                    // At least 2 slashes and one character after the last slash 
                    if( lastslash > 0 && lastslash+1 < str.length() ) {
                        String prefix = str.substring(0,lastslash+1);
                        String ctrlDesc = str.substring(lastslash+1);
                        FractalProvider fractalProvider = fractalProviders.get(prefix);
                        
                        if (fractalProvider == null) {
                        	throw new InstantiationException("Unknown fractal provider: " + prefix);
                        }
                        
                        arrayControllerDesc[1] = ctrlDesc;
                        return fractalProvider.getGenericFactory().newFcInstance(type, arrayControllerDesc, contentDesc);
                    }
                }
    		}
            
            arrayControllerDesc[1] = controllerDesc;
		}

		List<InstantiationException> excepts = null;

		for (FractalProvider fractalProvider : fractalProviders.values()) {
			try {
				return fractalProvider.getGenericFactory().newFcInstance(type, arrayControllerDesc, contentDesc);
			}
			catch( InstantiationException ie ) {
				/*
				 * The factory can not instantiate the component.
				 * Proceed with the next one in the list.
				 */
				if( excepts == null ) {
					excepts = new ArrayList<InstantiationException>();
				}
				excepts.add(ie);
			}
		}

		/*
		 * Report individual stack traces.
		 */
		int i = 0;
		for (String key : fractalProviders.keySet()) {
			InstantiationException ie = excepts.get(i);
			System.err.println("---- "+key+" ----");
			ie.printStackTrace();
			System.err.println("---- ");
			i++;
		}

		String msg =
			"Can not instantiate component <"+type+","+controllerDesc+","+
			contentDesc+">";
		throw new InstantiationException(msg);
	}

	public void serviceChanged(final ServiceEvent event) {
		ServiceReference ref;
		switch (event.getType()) {
		case ServiceEvent.REGISTERED:
			ref = event.getServiceReference();
			try {
				addFractalProvider(ref);
				servicesRefs.add(ref);
			} catch (InstantiationException e) {
				logger.log(Level.WARNING, "Error when adding the fractal provider with the alias: "
						+ ref.getProperty(FRACTAL_PROVIDER_ALIAS), e);
			} catch (NoSuchInterfaceException e) {
				logger.log(Level.WARNING, "Error when adding the fractal provider with the alias: "
						+ ref.getProperty(FRACTAL_PROVIDER_ALIAS), e);
			}
			break;
		case ServiceEvent.UNREGISTERING:
			ref = event.getServiceReference();
			removeFractalProvider(ref);
			servicesRefs.remove(ref);
			break;
		default:
		}

	}
	
	private void addFractalProvider (final ServiceReference ref) throws InstantiationException, NoSuchInterfaceException {
		String alias = (String) ref.getProperty(FRACTAL_PROVIDER_ALIAS);
		Factory factory = (Factory) context.getService(ref);
		
		Component comp;
		Object juliaConfig = ref.getProperty(JULIA_CONFG);
		if (juliaConfig != null) {
	        Map<String, String> hints = new HashMap<String, String>();
	        hints.put("julia.config", "julia-osgi-context.cfg");
			comp = ((GenericFactory) factory).newFcInstance(null, null, hints);
		} else {
			comp = factory.newFcInstance();
		}
		
		TypeFactory typeFactory = Fractal.getTypeFactory(comp);
		GenericFactory genericFactory = Fractal.getGenericFactory(comp);
		
		Map<InterfaceType, Object> additionalItfs = null;
		if (comp instanceof BootstrapComponent) {
			additionalItfs = ((BootstrapComponent) comp).getAdditionalInterfaces();
			for (Entry<InterfaceType, Object> additionalItf : additionalItfs.entrySet()) {
				type.addInterfaceType(additionalItf.getKey());
				interfaces.put(additionalItf.getKey().getFcItfName(), additionalItf.getValue());
			}
		}
		
		FractalProvider fractalProvider =
			new FractalProvider(alias, comp, genericFactory, typeFactory, additionalItfs);
		fractalProviders.put(alias, fractalProvider);
		logger.info("New fractal provider: " + alias);
	}

	private void removeFractalProvider(final ServiceReference ref) {
		context.ungetService(ref);
		String alias = (String) ref.getProperty(FRACTAL_PROVIDER_ALIAS);
		
		FractalProvider fractalProvider = fractalProviders.remove(alias);
		
		if (fractalProvider != null) {
			for (InterfaceType interfaceType : fractalProvider.getInterfaces().keySet()) {
				type.removeInterfaceType(interfaceType);
				interfaces.remove(interfaceType.getFcItfName());
			}
		}
	}

	public void ungetFractalProviders() {
		Iterator<ServiceReference> it = servicesRefs.iterator();
		while (it.hasNext()) {
			removeFractalProvider(it.next());
			it.remove();
		}
	}

}
