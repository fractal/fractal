/**
 * Julia
 * Copyright (C) 2009 Bull S.A.S.
 * Copyright (C) 2009 INRIA, SARDES
 * Contact: fractal@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */

package org.ow2.fractal.julia.osgi.provider.api;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.ow2.fractal.julia.osgi.provider.OSGiBootstrapComponent;

/**
 * Fractal provider mixing the chained provider and the multi provider defined in Fractal Util and adapting them for OSGi:
 * the Fractal providers are retrieved from the service registry.
 * 
 * @author Loris Bouzonnet
 */
public class OSGiBootstrapFactory implements Factory, GenericFactory, BundleActivator {

	private BundleContext context;
	

	/** The singleton instance of the bootstrap component. */
	private static OSGiBootstrapComponent bootstrap;

	/** Return the Fractal content description of this component. */
	public String getFcContentDesc() {
		Component boot = newFcInstance();
		String name = boot.getClass().getName();
		return name;
	}

	/** Return the Fractal controller description of this component. */
	public String getFcControllerDesc() {
		return "bootstrap";
	}

	/** Return the Fractal type of this component. */
	public Type getFcInstanceType() {
		Component boot = newFcInstance();
		Type type = boot.getFcType();
		return type;
	}

	public OSGiBootstrapComponent newFcInstance() {
		return newFcInstance(null,null,null);
	}

	public OSGiBootstrapComponent newFcInstance(Type type, Object controllerDesc,
			Object contentDesc) {
		if( bootstrap == null ) {
			bootstrap = new OSGiBootstrapComponent(context);
		}
		return bootstrap;
	}

	public void start(BundleContext context) throws Exception {
		this.context = context;
		
		context.addServiceListener(newFcInstance(), "("
				+ Constants.OBJECTCLASS + "="
				+ Factory.class.getCanonicalName() + ")");
	}

	public void stop(BundleContext context) throws Exception {
		if (bootstrap != null) {
			context.removeServiceListener(bootstrap);
			bootstrap = null;
		}
		bootstrap.ungetFractalProviders();
		this.context = null;
	}

}
