/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2008 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Marc Leger
 */

package org.ow2.fractal.julia.osgi.control.lifecycle;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.control.content.Util;
import org.objectweb.fractal.julia.control.lifecycle.ChainedIllegalLifeCycleException;
import org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator;
import org.objectweb.fractal.util.Fractal;
import org.ow2.fractal.julia.osgi.api.control.lifecycle.BindingComponentGraph;

/**
 * Provides an abstract implementation of the {@link LifeCycleCoordinator}
 * interface. <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>the component to which this controller object belongs must provide the
 * {@link Component} interface.</li>
 * </ul>
 */

public abstract class OrderedLifeCycleCoordinatorMixin implements
		LifeCycleCoordinator, Controller {

	// -------------------------------------------------------------------------
	// Private constructor
	// -------------------------------------------------------------------------

	private OrderedLifeCycleCoordinatorMixin() {
	}

	// -------------------------------------------------------------------------
	// Fields and methods added and overriden by the mixin class
	// -------------------------------------------------------------------------

	/**
	 * The components that are currently active.
	 */

	public List fcActive;

	private BindingComponentGraph graph = new BindingComponentGraph();

	private Set startedComponents = new HashSet();

	/**
	 * Sets the lifecycle state of this component and of all its direct and
	 * indirect sub components that have a {@link LifeCycleCoordinator}
	 * interface.
	 * 
	 * @param started
	 *            <tt>true</tt> to set the lifecycle state of the components
	 *            to {@link #STARTED STARTED}, or <tt>false</tt> to set this
	 *            state to {@link #STOPPED STOPPED}.
	 * @throws IllegalLifeCycleException
	 *             if a problem occurs.
	 */

	public void setFcState(final boolean started)
			throws IllegalLifeCycleException {
		if (started) {
			startComponent(_this_weaveableC, null);
		} else {
			stopComponent(_this_weaveableC, null);
		}
		graph.reset();
		startedComponents.clear();
	}

	private void stopComponent(Component component, Component parent)
			throws IllegalLifeCycleException {
		List allSubComponents = Util.getAllSubComponents(_this_weaveableC);
		for (int i = 0; i < allSubComponents.size(); ++i) {
			Component c = (Component) allSubComponents.get(i);
			setState(c, false);
		}
		setState(component, false);
	}

	private void startComponent(Component component, Component parent)
			throws IllegalLifeCycleException {
		if (startedComponents.contains(component))
			return;
		try {
			// start subcomponents first
			Component[] subcomponents = Fractal.getContentController(component)
					.getFcSubComponents();
			for (int i = 0; i < subcomponents.length; i++) {
				startComponent(subcomponents[i], component);
			}
		} catch (NoSuchInterfaceException e) {

		}
		BindingController bc = null;
		try {
			bc = Fractal.getBindingController(component);
		} catch (NoSuchInterfaceException e) {
			setState(component, true);
			startedComponents.add(component);
			return;
		}
		String[] itfs = bc.listFc();
		for (int i = 0; i < itfs.length; i++) {
			try {
				// Case of optional interface
				if (((InterfaceType) ((Interface) component
						.getFcInterface(itfs[i])).getFcItfType())
						.isFcOptionalItf())
					continue;
				// start server siblings first
				Component sibling = ((Interface) bc.lookupFc(itfs[i]))
						.getFcItfOwner();
				LifeCycleController lfc = Fractal
						.getLifeCycleController(sibling);
				if (lfc.getFcState() != STARTED && !sibling.equals(parent)) {
					if (!graph.addVertex(component, sibling)) {
						throw new ChainedIllegalLifeCycleException(null,
								_this_weaveableC,
								"there is a cycle in lifecycle dependencies with interface '"
										+ itfs[i] + "'");
					} else {
						startComponent(sibling, parent);
					}
				}
			} catch (NoSuchInterfaceException e) {

			}
		}
		setState(component, true);
		startedComponents.add(component);
	}

	public boolean fcActivated(final LifeCycleCoordinator component) {
		synchronized (fcActive) {
			// a component can become active iff another component is already
			// active
			if (fcActive.size() > 0) {
				if (!fcActive.contains(component)) {
					fcActive.add(component);
				}
				return true;
			}
			return false;
		}
	}

	public void fcInactivated(final LifeCycleCoordinator component) {
		synchronized (fcActive) {
			fcActive.remove(component);
			// notifies the thread that may be blocked in stopFc
			fcActive.notifyAll();
		}
	}

	/**
	 * Stops the given components simultaneously. This method sets the state of
	 * the components to "<tt>STOPPING</tt>", waits until all the components
	 * are simultaneoulsy inactive (their state is known thanks to the {@link
	 * #fcActivated fcActivated} and {@link #fcInactivated fcInactivated}
	 * callback methods), and then sets the state of the components to
	 * {@link #STOPPED STOPPED}.
	 * 
	 * @param components
	 *            the {@link LifeCycleCoordinator} interface of the components
	 *            to be stopped.
	 * @throws IllegalLifeCycleException
	 *             if a problem occurs.
	 */

	public void stopFc(final LifeCycleCoordinator[] components)
			throws IllegalLifeCycleException {
		// initializes the fcActive list
		fcActive = new ArrayList();
		for (int i = 0; i < components.length; ++i) {
			if (components[i].getFcState().equals(STARTED)) {
				fcActive.add(components[i]);
			}
		}
		// sets the state of the components to STOPPING
		LifeCycleCoordinator c;
		try {
			c = (LifeCycleCoordinator) _this_weaveableC
					.getFcInterface("lifecycle-controller");
		} catch (Exception e) {
			try {
				c = (LifeCycleCoordinator) _this_weaveableC
						.getFcInterface("/lifecycle-coordinator");
			} catch (NoSuchInterfaceException f) {
				throw new ChainedIllegalLifeCycleException(f, _this_weaveableC,
						"Cannot stop components");
			}
		}
		for (int i = 0; i < components.length; ++i) {
			if (components[i].getFcState().equals(STARTED)) {
				components[i].setFcStopping(c);
			}
		}
		// waits until all the components are simultaneously inactive
		synchronized (fcActive) {
			while (fcActive.size() > 0) {
				try {
					fcActive.wait();
				} catch (InterruptedException e) {
				}
			}
		}
		// sets the state of the components to STOPPED
		for (int i = 0; i < components.length; ++i) {
			if (components[i].getFcState().equals(STARTED)) {
				components[i].setFcStopped();
			}
		}
		fcActive = null;
	}

	private void setState(Component c, boolean state)
			throws IllegalLifeCycleException {
		LifeCycleCoordinator lc = getLifeCycleCoordinator(c);
		if (state) {
			lc.setFcStarted();
		} else {
			lc.setFcStopped();
		}
	}

	private LifeCycleCoordinator getLifeCycleCoordinator(Component c) {
		LifeCycleCoordinator lc = null;
		try {
			lc = (LifeCycleCoordinator) c
					.getFcInterface("lifecycle-controller");
		} catch (Exception e) {
			try {
				lc = (LifeCycleCoordinator) c
						.getFcInterface("/lifecycle-coordinator");
			} catch (NoSuchInterfaceException f) {
			}
		}
		return lc;
	}

	public void initFcController(InitializationContext ic)
			throws InstantiationException {
		this.graph = new BindingComponentGraph();
		this.startedComponents = new HashSet();
		_super_initFcController(ic);
	}

	public abstract void _super_initFcController(InitializationContext ic)
			throws InstantiationException;

	public Component _this_weaveableC;
}
