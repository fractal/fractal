/***
 * Julia
 * Copyright (C) 2009 Bull S.A.S.
 * Copyright (C) 2009 INRIA, SARDES
 * Contact: fractal@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */
package org.ow2.fractal.julia.osgi.control;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.util.Fractal;
import org.osgi.framework.BundleContext;

import org.ow2.fractal.julia.osgi.api.control.OSGiContextController;

/**
 * Provide a basic implementation of the {@link OSGiContextController} interface.
 *
 * @author Julien Legrand
 */
public class BasicOSGiContextControllerMixin implements OSGiContextController {

    /**
     * OSGi bundle context.
     */
    public BundleContext bundleCtx = null;


    // ------------------------------------------------------------------------
    // Implementation of OSGiContextController interface
    // ------------------------------------------------------------------------

    public void setBundleContext(final BundleContext context) {
        Component id;
        try {
            id = (Component) _this_weaveableC.getFcInterface("component");
        } catch (NoSuchInterfaceException e) {
            return;
        }
        OSGiContextController[] occs = getOSGiContextControllers(id);
        if (occs != null) {
            for (OSGiContextController occ : occs) {
                occ.setBundleContext(context);
            }
        }
        this.bundleCtx = context;
    }

    private OSGiContextController[] getOSGiContextControllers(final Component component) {
        List<OSGiContextController> occs = new ArrayList<OSGiContextController>();
        try {
            ContentController cc = Fractal.getContentController(component);
            for (Component sub : cc.getFcSubComponents()) {
                try {
                    occs.add((OSGiContextController) sub.getFcInterface("osgi-context-controller"));
                } catch (NoSuchInterfaceException e) {
                    // Skip
                }
            }
        } catch (NoSuchInterfaceException e) {
            return null;
        }
        return occs.toArray(new OSGiContextController[occs.size()]);
    }

    public BundleContext getBundleContext() {
        return this.bundleCtx;
    }


    // -------------------------------------------------------------------------
    // Fields and methods required by the mixin class in the base class
    // -------------------------------------------------------------------------

    /**
     * The <tt>weaveableC</tt> field required by this mixin. This field is
     * supposed to reference the {@link Component} interface of the component to
     * which this controller object belongs.
     */

    public Component _this_weaveableC;

}
