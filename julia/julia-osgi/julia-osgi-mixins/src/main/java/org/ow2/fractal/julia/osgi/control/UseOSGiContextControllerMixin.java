/***
 * Julia
 * Copyright (C) 2009 Bull S.A.S.
 * Copyright (C) 2009 INRIA, SARDES
 * Contact: fractal@ow2.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * --------------------------------------------------------------------------
 * $Id$
 * --------------------------------------------------------------------------
 */


package org.ow2.fractal.julia.osgi.control;

import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;

import org.ow2.fractal.julia.osgi.api.control.OSGiContextController;

/**
 *
 * Provides a {@link OSGiContextController} field to a {@link Controller}. <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>the component to which this controller belongs must provide the {@link
 * OSGiContextController} interface.</li>
 * </ul>
 *
 * @author Julien Legrand
 */
public abstract class UseOSGiContextControllerMixin implements Controller {

    // -------------------------------------------------------------------------
    // Private constructor
    // -------------------------------------------------------------------------
    private UseOSGiContextControllerMixin() {
    }

    // -------------------------------------------------------------------------
    // Fields and methods added and overridden by the mixin class
    // -------------------------------------------------------------------------

    /**
     * The {@link OSGiContextController} interface of the component to which
     * this controller object belongs.
     */
    public OSGiContextController weaveableOCC;

    public void initFcController(InitializationContext ic)
    throws InstantiationException {
        weaveableOCC = (OSGiContextController) ic.getInterface("osgi-context-controller");
        _super_initFcController(ic);
    }

    // -------------------------------------------------------------------------
    // Fields and methods required by the mixin class in the base class
    // -------------------------------------------------------------------------

    /**
     * The {@link Controller#initFcController initFcController} method overridden
     * by this mixin.
     *
     * @param ic
     *            information about the component to which this controller
     *            object belongs.
     * @throws InstantiationException
     *             if the initialization fails.
     */

    public abstract void _super_initFcController(InitializationContext ic)
    throws InstantiationException;

}
