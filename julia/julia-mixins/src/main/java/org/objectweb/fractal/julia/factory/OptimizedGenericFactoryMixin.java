/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Philippe Merle
 *
 * $Id:BasicGenericFactoryMixin.java 2631 2007-05-30 03:13:46Z merle $
 */

package org.objectweb.fractal.julia.factory;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;

import java.util.HashMap;
import java.util.Map;

/**
 * Provides an optimization of {@link #newFcInstance newFcInstance} calls.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>{@link BasicGenericFactoryMixin} must be declared before this mixin
 *     in Julia configuration files.
 * </ul>
 */

public abstract class OptimizedGenericFactoryMixin
           implements GenericFactory, Controller
{
  /**
   * Key for the cache.
   */
  static public class Key
  {
    protected Type type;
    protected Object controller;
    protected Object content;

    public Key (final Type type, final Object controller, final Object content)
    {
	  this.type = type;
	  this.controller = controller;
	  this.content = content;
	}

    public boolean equals (final Object o) {
	  if(o instanceof Key) {
	    Key key = (Key)o;
	    // content is equals to null when called for bootstrap.
	    return key.controller.equals(this.controller)
	        && ((key.content!=null)?key.content.equals(this.content):true)
	        && key.type.equals(this.type);
	  }
	  return false;
	}

    public int hashCode() {
	  return type.hashCode()
	       * controller.hashCode()
	       * ((content!=null)?content.hashCode():1);
	}
  }

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private OptimizedGenericFactoryMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * Cache containing already created initialization classes.
   */
  private Map cache;

  public void initFcController (final InitializationContext ic)
    throws InstantiationException
  {
    // Init the cache.
	cache = new HashMap();
    // Call the next initFcController(InitializationContext) method.
    _super_initFcController(ic);
  }

  public Component newFcInstance (
    final Type type,
    final Object controllerDesc,
    final Object contentDesc) throws InstantiationException
  {
    Object controller;
    if (controllerDesc instanceof Object[]) {
      controller = ((Object[])controllerDesc)[1];
    } else {
      controller = controllerDesc;
    }
    Object content;
    if (contentDesc instanceof Object[]) {
      // case of a template content descriptor:
      //   extract the instance content descriptor
      content = ((Object[])contentDesc)[1];
    } else {
      content = contentDesc;
    }

    // Create a key for the tuple <type, controller, contentDesc>
    Key key = new Key(type, controller, content);
    // Search if an initialization context class is recorded for this key.
    Class initializationContextClass = (Class)cache.get(key);
    // If key not found in the cache
    if(initializationContextClass == null) {
      // Obtain the initialization context class from BasicGenericFactory.
      initializationContextClass = getInitializationContextClass(type, controllerDesc, contentDesc);
      // Record the initization context class in the cache.
      cache.put(key, initializationContextClass);
    }
    // Call the BasicGenericFactory.newComponent method.
    return newComponent(initializationContextClass, contentDesc);
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The {@link Controller#initFcController initFcController} method overriden
   * by this mixin.
   * 
   * @param ic information about the component to which this controller object
   *      belongs.
   * @throws InstantiationException if the initialization fails.
   */

  public abstract void _super_initFcController (InitializationContext ic)
    throws InstantiationException;

  /**
   * Get the initialization context class associated to a given
   * {@link Type} instance, controller and content descriptors.
   *
   * @param type the given {@link Type} instance.
   * @param controllerDesc the given controller descriptor.
   * @param contentDesc the given content descriptor.
   * @return the initialization content class.
   * @throws InstantiationException when problems obscur.
   */

  public abstract Class getInitializationContextClass (
		  	final Type type,
		  	final Object controllerDesc,
		    final Object contentDesc) throws InstantiationException;

  /**
   * Create a new {@link Component} instance for a given
   * initialization context class and content descriptor.
   *
   * @param initializationContextClass the given initialization context class.
   * @param contentDesc the given content descriptor.
   * @return the new {@link Component} instance.
   * @throws InstantiationException when problems obscur.
   */

  public abstract Component newComponent (
		    final Class initializationContextClass,
		    final Object contentDesc) throws InstantiationException;

}
