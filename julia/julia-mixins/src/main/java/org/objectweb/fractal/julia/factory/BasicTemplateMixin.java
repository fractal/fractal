/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.factory;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Fractal;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;

import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.control.content.Util;
import org.objectweb.fractal.julia.type.BasicComponentType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Provides a basic implementation of the {@link Template} interface.
 * <br>
 * <br>
 * <b>Requirements</b> (here "sub template" means "any direct or indirect sub
 * template")
 * <ul>
 * <li>the type of this template component must be an instance of {@link
 * ComponentType}.</li>
 * <li>all the sub templates of this template component must provide the {@link
 * Template} interface (and the {@link Component} interface).</li>
 * <li>if this template component, or one of its sub templates, has a {@link
 * ContentController} interface, then the components instantiated by this
 * template component must also have the {@link ContentController} interface.
 * Likewise for the {@link BindingController} interface.</li>
 * <li>For each binding between template components, the server interface must
 * implement the {@link Interface} interface. Moreover, the owner of this
 * interface must be a (sub) template of this template component.</li>
 * </ul>
 */

public abstract class BasicTemplateMixin implements Controller, Template {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private BasicTemplateMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * The functional type of the components instantiated by this template.
   */

  public Type fcInstanceType;

  /**
   * The controller and content descriptors of the components instantiated by
   * this template.
   */

  public Object[] fcContent;

  /**
   * Initializes the fields of this mixin and then calls the overriden method.
   *
   * @param ic information about the component to which this controller object
   *      belongs.
   * @throws InstantiationException if the initialization fails.
   */

  public void initFcController (final InitializationContext ic)
    throws InstantiationException
  {
    // initializes the fcContent field
    fcContent = (Object[])ic.content;

    // initializes the fcInstanceType field
    // this type is computed from the type of this component by removing the
    // factory interface type, and all the control interface types
    ComponentType tmplType = (ComponentType)ic.type;
    InterfaceType[] itfTypes = tmplType.getFcInterfaceTypes();
    List itfList = new ArrayList();
    for (int j = 0; j < itfTypes.length; ++j) {
      String n = itfTypes[j].getFcItfName();
      if (!n.equals("factory") && !n.equals("/template")) {
        if (n.equals("attribute-controller")) {
          itfList.add(itfTypes[j]);
        } else if (!(n.equals("component") || n.endsWith("-controller"))) {
          itfList.add(itfTypes[j]);
        }
      }
    }
    itfTypes = new InterfaceType[itfList.size()];
    itfTypes = (InterfaceType[])itfList.toArray(itfTypes);
    fcInstanceType = new BasicComponentType(itfTypes);

    // calls the overriden method
    _super_initFcController(ic);
  }

  public Type getFcInstanceType () {
    return fcInstanceType;
  }

  public Object getFcControllerDesc () {
    return fcContent[0];
  }

  public Object getFcContentDesc () {
    return fcContent[1];
  }

  public Component newFcInstance () throws InstantiationException {
    // finds all the direct and indirect sub templates of this template
    // finds all the direct and indirect sub components of this component
    Component thisComponent;
    try {
      thisComponent = (Component)_this_weaveableC.getFcInterface("component");
    } catch (NoSuchInterfaceException e) {
      throw new ChainedInstantiationException(
        e,
        _this_weaveableC,
        "The template component must provide the Component interface");
    }
    List allSubTemplates = Util.getAllSubComponents(thisComponent);

    // instantiates all the templates
    Map instances = new HashMap();
    for (int i = 0; i < allSubTemplates.size(); ++i) {
      Component tmpl = (Component)allSubTemplates.get(i);
      Template t;
      try {
        t = (Template)tmpl.getFcInterface("factory");
      } catch (Exception e) {
        try {
          t = (Template)tmpl.getFcInterface("/template");
        } catch (NoSuchInterfaceException f) {
          throw new ChainedInstantiationException(
            f,
            _this_weaveableC,
            "All the (sub) templates must provide the Template interface");
        }
      }
      Component instance = t.newFcControllerInstance();
      instances.put(tmpl, instance);
    }

    // adds the instances into each other
    for (int i = 0; i < allSubTemplates.size(); ++i) {
      Component tmpl = (Component)allSubTemplates.get(i);
      Component instance = (Component)instances.get(tmpl);

      ContentController tmplCC, instanceCC;
      try {
        tmplCC = (ContentController)tmpl.getFcInterface("content-controller");
      } catch (NoSuchInterfaceException e) {
        continue;
      }
      try {
        instanceCC =
          (ContentController)instance.getFcInterface("content-controller");
      } catch (NoSuchInterfaceException e) {
        throw new ChainedInstantiationException(
          e,
          tmpl,
          "A component instantiated from a template with a ContentController " +
          "interface must provide the ContentController interface");
      }
      Component[] subTemplates = tmplCC.getFcSubComponents();
      Component[] subInstances = instanceCC.getFcSubComponents();
      for (int j = 0; j < subTemplates.length; ++j) {
        Component subInstance = (Component)instances.get(subTemplates[j]);
        boolean add = true;
        for (int k = 0; k < subInstances.length; ++k) {
          if (subInstances[k].equals(subInstance)) {
            // if sunInstance is already a sub component of instance,
            // do not add it again (this can happen with singleton templates)
            add = false;
          }
        }
        if (add) {
          try {
            instanceCC.addFcSubComponent(subInstance);
          } catch (IllegalContentException e) {
            throw new ChainedInstantiationException(
              e,
              tmpl,
              "Cannot set the component hierarchy from the template hierarchy");
          } catch (IllegalLifeCycleException e) {
            throw new ChainedInstantiationException(
              e,
              tmpl,
              "Cannot set the component hierarchy from the template hierarchy");
          }
        }
      }
    }

    // binds the instances to each other, as the templates are bound
    for (int i = 0; i < allSubTemplates.size(); ++i) {
      Component tmpl = (Component)allSubTemplates.get(i);
      Component instance = (Component)instances.get(tmpl);

      BindingController tmplBC, instanceBC;
      try {
        tmplBC = (BindingController)tmpl.getFcInterface("binding-controller");
      } catch (NoSuchInterfaceException e) {
        continue;
      }
      try {
        instanceBC =
          (BindingController)instance.getFcInterface("binding-controller");
      } catch (NoSuchInterfaceException e) {
        throw new ChainedInstantiationException(
          e,
          tmpl,
          "A component instantiated from a template with a BindingController " +
          "interface must provide the BindingController interface");
      }
      String[] itfNames = tmplBC.listFc();
      for (int j = 0; j < itfNames.length; ++j) {
        String itfName = itfNames[j];
        Interface serverItf;
        try {
          serverItf = (Interface)tmplBC.lookupFc(itfName);
        } catch (ClassCastException e) {
          throw new ChainedInstantiationException(
            e,
            tmpl,
            "The server interface of each binding between templates " +
            "must implement the Interface interface");
        } catch (NoSuchInterfaceException e) {
          throw new ChainedInstantiationException(
            e,
            tmpl,
            "The '" + itfName +
            "' interface returned by the listFc method does not exist");
        }
        if (serverItf == null) {
          continue;
        }

        Component serverTmpl = serverItf.getFcItfOwner();
        Component serverInstance = (Component)instances.get(serverTmpl);
        if (serverInstance == null) {
          // 'tmpl' bound to a component that does not belong to the root
          // instantiated template: do not create this binding for 'instance'
          continue;
        }

        Object itfValue;
        try {
          if (serverItf.isFcInternalItf()) {
            ContentController cc = (ContentController)serverInstance.
              getFcInterface("content-controller");
            itfValue = cc.getFcInternalInterface(serverItf.getFcItfName());
          } else {
            itfValue = serverInstance.getFcInterface(serverItf.getFcItfName());
          }
        } catch (NoSuchInterfaceException e) {
          throw new ChainedInstantiationException(
            e,
            serverTmpl,
            "The server interface '" + serverItf.getFcItfName() +
            "'is missing in the component instantiated from the template");
        }

        try {
          if (instanceBC.lookupFc(itfName).equals(itfValue)) {
            // if the binding already exists, do not create again
            // (this can happen with singleton templates)
            continue;
          }
        } catch (Exception e) {
        }

        try {
          instanceBC.bindFc(itfName, itfValue);
        } catch (NoSuchInterfaceException e) {
          throw new ChainedInstantiationException(
            e,
            tmpl,
            "Cannot set the component bindings from the template bindings");
        } catch (IllegalBindingException e) {
          throw new ChainedInstantiationException(
            e,
            tmpl,
            "Cannot set the component bindings from the template bindings");
        } catch (IllegalLifeCycleException e) {
          throw new ChainedInstantiationException(
            e,
            tmpl,
            "Cannot set the component bindings from the template bindings");
        }
      }
    }

    return (Component)instances.get(allSubTemplates.get(0));
  }

  public Component newFcControllerInstance () throws InstantiationException {
    if (fcContent[1] instanceof Component) {
      return (Component)fcContent[1];
    }
    GenericFactory factory;
    try {
      factory = (GenericFactory)Fractal.getBootstrapComponent().
        getFcInterface("generic-factory");
    } catch (InstantiationException e) {
      throw new ChainedInstantiationException(
        e,
        _this_weaveableC,
        "Cannot find the GenericFactory interface of the bootstrap " +
        "component, which is needed to instantiate the template");
    } catch (NoSuchInterfaceException e) {
      throw new ChainedInstantiationException(
        e,
        _this_weaveableC,
        "Cannot find the GenericFactory interface of the bootstrap " +
        "component, which is needed to instantiate the template");
    }
    return factory.newFcInstance(fcInstanceType, fcContent[0], fcContent[1]);
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableC</tt> field required by this mixin. This field is
   * supposed to reference the {@link Component} interface of the component to
   * which this controller object belongs.
   */

  public Component _this_weaveableC;

  /**
   * The {@link Controller#initFcController initFcController} method overriden
   * by this mixin.
   *
   * @param ic information about the component to which this controller object
   *      belongs.
   * @throws InstantiationException if the initialization fails.
   */

  public abstract void _super_initFcController (InitializationContext ic)
    throws InstantiationException;
}
