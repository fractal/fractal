/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.binding;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.Interceptor;

/**
 * Provides output interceptors management to a {@link BindingController}. This
 * mixin is only useful for the {@link ContainerBindingControllerMixin}, since
 * the {@link CompositeBindingMixin} and the {@link OptimizedCompositeBindingMixin}
 * already manage interceptors.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>the component to which this controller object belongs must provide the
 * {@link Component} interface. Moreover this mixin does nothing if the
 * component does not provide interface introspection functions.</li>
 * </ul>
 */

public abstract class InterceptorBindingMixin implements BindingController {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private InterceptorBindingMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * Calls the overriden method and returns the corresponding {@link
   * Interceptor#getFcItfDelegate getFcItfDelegate} (if the result is an {@link
   * Interceptor}).
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @return the server interface to which the given interface is bound, or <tt>
   *      null</tt> if it is not bound.
   * @throws NoSuchInterfaceException if the component to which this interface
   *      belongs does not have a client interface whose name is equal to the
   *      given name.
   */

  public Object lookupFc (final String clientItfName)
    throws NoSuchInterfaceException
  {
    Object o = _super_lookupFc(clientItfName);
    if (o instanceof Interceptor) {
      // skips the output interceptor, if there is one
      o = ((Interceptor)o).getFcItfDelegate();
    }
    return o;
  }

  /**
   * Gets the interceptor of the given client interface, updates its {@link
   * Interceptor#getFcItfDelegate getFcItfDelegate}, and then calls the
   * overriden method with the interceptor as server interface (if there is
   * an interceptor for the client interface).
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @param serverItf a server interface.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be created.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public void bindFc (final String clientItfName, final Object serverItf) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException
  {
    Object o = _this_weaveableC.getFcInterface(clientItfName);
    if (o instanceof ComponentInterface) {
      Object next = ((ComponentInterface)o).getFcItfImpl();
      if (next instanceof Interceptor) {
        ((Interceptor)next).setFcItfDelegate(serverItf);
        o = next;
      } else {
        ((ComponentInterface)o).setFcItfImpl(serverItf);
        o = serverItf;
      }
    } else {
      o = serverItf;
    }
    _super_bindFc(clientItfName, o);
  }

  /**
   * Gets the interceptor of the given client interface, updates its {@link
   * Interceptor#getFcItfDelegate getFcItfDelegate}, and then calls the
   * overriden method (if there is an interceptor for the client interface).
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be removed.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public void unbindFc (final String clientItfName) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException
  {
    Object o = _this_weaveableC.getFcInterface(clientItfName);
    if (o instanceof ComponentInterface) {
      Object next = ((ComponentInterface)o).getFcItfImpl();
      if (next instanceof Interceptor) {
        ((Interceptor)next).setFcItfDelegate(null);
      }
      else {
        ((ComponentInterface)o).setFcItfImpl(null);
      }
    }
    _super_unbindFc(clientItfName);
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableC</tt> field required by this mixin. This field is
   * supposed to reference the {@link Component} interface of the component to
   * which this controller object belongs.
   */

  public Component _this_weaveableC;

  /**
   * The {@link BindingController#lookupFc lookupFc} method overriden by this
   * mixin.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @return the server interface to which the given interface is bound, or <tt>
   *      null</tt> if it is not bound.
   * @throws NoSuchInterfaceException if the component to which this interface
   *      belongs does not have a client interface whose name is equal to the
   *      given name.
   */

  public abstract Object _super_lookupFc (String clientItfName) throws
    NoSuchInterfaceException;

  /**
   * The {@link BindingController#bindFc bindFc} method overriden by this mixin.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @param serverItf a server interface.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be created.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public abstract void _super_bindFc (String clientItfName, Object serverItf) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException;

  /**
   * The {@link BindingController#unbindFc unbindFc} method overriden by this
   * mixin.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be removed.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public abstract void _super_unbindFc (String clientItfName) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException;
}
