/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.binding;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;

import org.objectweb.fractal.julia.ChainedNoSuchInterfaceException;

/**
 * Provides basic type system related checks to a {@link BindingController}.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>the component to which this controller object belongs must provide the
 * {@link Component} interface.</li>
 * <li>the type of the component to which this controller object belongs must be
 * an instance of {@link ComponentType}.</li>
 * </ul>
 */

public abstract class TypeBindingMixin implements BindingController {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private TypeBindingMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * Checks the interface name with the component's type and then calls the
   * {@link #lookupFc(InterfaceType,String) lookupFc} method.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @return the server interface to which the given interface is bound, or <tt>
   *      null</tt> if it is not bound.
   * @throws NoSuchInterfaceException if the component to which this interface
   *      belongs does not have a client interface whose name is equal to the
   *      given name.
   */

  public Object lookupFc (final String clientItfName)
    throws NoSuchInterfaceException
  {
    ComponentType compType = (ComponentType)_this_weaveableC.getFcType();
    InterfaceType clientItfType;
    try {
      clientItfType = compType.getFcInterfaceType(clientItfName);
    } catch (NoSuchInterfaceException e) {
      throw new ChainedNoSuchInterfaceException(
        null, _this_weaveableC, clientItfName);
    }
    checkFcClientInterface(clientItfType);
    return lookupFc(clientItfType, clientItfName);
  }

  /**
   * Checks the interface name with the component's type and then calls the
   * {@link #bindFc(InterfaceType,String,Object) bindFc} method. If the server
   * interface implements {@link Interface}, and if its type is an instance of
   * {@link InterfaceType}, this method also checks the compatibility between
   * the client and server interface types.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @param serverItf a server interface.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be created.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public void bindFc (final String clientItfName, final Object serverItf) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException
  {
    ComponentType compType = (ComponentType)_this_weaveableC.getFcType();
    InterfaceType cItfType;
    try {
      cItfType = compType.getFcInterfaceType(clientItfName);
    } catch (NoSuchInterfaceException e) {
      throw new ChainedNoSuchInterfaceException(
        null, _this_weaveableC, clientItfName);
    }
    checkFcClientInterface(cItfType);

    ContentController cc;
    try {
      cc = (ContentController)_this_weaveableC.
        getFcInterface("content-controller");
    } catch (NoSuchInterfaceException e) {
      cc = null;
    }

    // for lazy creation of collection interfaces
    if (cItfType.isFcClientItf()) {
      _this_weaveableC.getFcInterface(clientItfName);
    } else {
      cc.getFcInternalInterface(clientItfName);
    }

    Interface sItf;
    InterfaceType sItfType;
    try {
      sItf = (Interface)serverItf;
      sItfType = (InterfaceType)sItf.getFcItfType();
    } catch (ClassCastException e) {
      // if the server interface does not provide interface introspection
      // functions, or if its type is not an InterfaceType, the checks below
      // cannot be performed, so we create the binding directly:
      bindFc(cItfType, clientItfName, serverItf);
      return;
    }

    if (sItfType.isFcClientItf()) {
      throw new ChainedIllegalBindingException(
        null,
        _this_weaveableC,
        sItf.getFcItfOwner(),
        clientItfName,
        sItf.getFcItfName(),
        "Cannot bind two client interfaces together");
    }

    try {
      Class s = Class.forName(sItfType.getFcItfSignature());
      Class c = Class.forName(cItfType.getFcItfSignature());
      if (!c.isAssignableFrom(s)) {
        throw new ChainedIllegalBindingException(
          null,
          _this_weaveableC,
          sItf.getFcItfOwner(),
          clientItfName,
          sItf.getFcItfName(),
          "The server interface type is not a subtype " +
          "of the client interface type");
      }
    } catch (ClassNotFoundException ignored) {
    }

    if (!cItfType.isFcOptionalItf() && sItfType.isFcOptionalItf()) {
      throw new ChainedIllegalBindingException(
        null,
        _this_weaveableC,
        ((Interface)serverItf).getFcItfOwner(),
        clientItfName,
        ((Interface)serverItf).getFcItfName(),
        "A mandatory interface cannot be bound to an optional interface");
    }

    bindFc(cItfType, clientItfName, sItf);
  }

  /**
   * Checks the interface name with the component's type and then calls the
   * {@link #unbindFc(InterfaceType,String) unbindFc} method.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be removed.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public void unbindFc (final String clientItfName) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException
  {
    ComponentType compType = (ComponentType)_this_weaveableC.getFcType();
    InterfaceType clientItfType;
    try {
      clientItfType = compType.getFcInterfaceType(clientItfName);
    } catch (NoSuchInterfaceException e) {
      throw new ChainedNoSuchInterfaceException(
        null, _this_weaveableC, clientItfName);
    }
    checkFcClientInterface(clientItfType);
    unbindFc(clientItfType, clientItfName);
  }

  /**
   * Returns the interface to which the given client interface is bound. More
   * precisely, returns the server interface to which the client interface whose
   * name is given is bound.
   *
   * @param clientItfType the type of the <tt>clientItfName</tt> interface.
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @return the server interface to which the given interface is bound, or <tt>
   *      null</tt> if it is not bound.
   * @throws NoSuchInterfaceException if the component to which this interface
   *      belongs does not have a client interface whose name is equal to the
   *      given name.
   */

  public Object lookupFc (
    final InterfaceType clientItfType,
    final String clientItfName)
    throws
    NoSuchInterfaceException
  {
    return _super_lookupFc(clientItfName);
  }

  /**
   * Binds the client interface whose name is given to a server interface. More
   * precisely, binds the client interface of the component to which this
   * interface belongs, and whose name is equal to the given name, to the given
   * server interface.
   *
   * @param clientItfType the type of the <tt>clientItfName</tt> interface.
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @param serverItf a server interface.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be created.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but
   *      it is not in an appropriate state to perform this operation.
   */

  public void bindFc (
    final InterfaceType clientItfType,
    final String clientItfName,
    final Object serverItf)
    throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException
  {
    _super_bindFc(clientItfName, serverItf);
  }

  /**
   * Unbinds the given client interface. More precisely, unbinds the client
   * interface of the component to which this interface belongs, and whose name
   * is equal to the given name.
   *
   * @param clientItfType the type of the <tt>clientItfName</tt> interface.
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be removed.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but
   *      it is not in an appropriate state to perform this operation.
   */

  public void unbindFc (
    final InterfaceType clientItfType,
    final String clientItfName)
    throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException
  {
    _super_unbindFc(clientItfName);
  }

  /**
   * Checks that the given type corresponds to an external or internal client 
   * interface.
   *
   * @param itfType an interface type.
   * @throws NoSuchInterfaceException if the given type does not correspdond to
   *      an external or internal client interface.
   */

  private void checkFcClientInterface (InterfaceType itfType)
    throws NoSuchInterfaceException
  {
    try {
      _this_weaveableC.getFcInterface("content-controller");
      String name = itfType.getFcItfName();
      if (!name.equals("component") && !name.endsWith("-controller")) {
        return;
      }
    } catch (NoSuchInterfaceException e) {
      if (itfType.isFcClientItf()) {
        return;
      }
    }
    throw new ChainedNoSuchInterfaceException(
      null, _this_weaveableC, itfType.getFcItfName());
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableC</tt> field required by this mixin. This field is
   * supposed to reference the {@link Component} interface of the component to
   * which this controller object belongs.
   */

  public Component _this_weaveableC;

  /**
   * The {@link BindingController#lookupFc lookupFc} method overriden by this
   * mixin.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @return the server interface to which the given interface is bound, or <tt>
   *      null</tt> if it is not bound.
   * @throws NoSuchInterfaceException if the component to which this interface
   *      belongs does not have a client interface whose name is equal to the
   *      given name.
   */

  public abstract Object _super_lookupFc (String clientItfName) throws
    NoSuchInterfaceException;

  /**
   * The {@link BindingController#bindFc bindFc} method overriden by this
   * mixin.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @param serverItf a server interface.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be created.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public abstract void _super_bindFc (String clientItfName, Object serverItf) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException;

  /**
   * The {@link BindingController#unbindFc unbindFc} method overriden by this
   * mixin.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be removed.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public abstract void _super_unbindFc (String clientItfName) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException;
}
