/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.content;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;

import org.objectweb.fractal.julia.ChainedNoSuchInterfaceException;
import org.objectweb.fractal.julia.ComponentInterface;

import java.util.Map;

/**
 * Provides basic type system related checks to a {@link ContentController}.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>the component to which this controller object belongs must provide the
 * {@link Component} interface.</li>
 * <li>the type of the component in which this mixin is used must be an instance
 * of {@link ComponentType}.</li>
 * </ul>
 */

public abstract class TypeContentMixin implements ContentController {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private TypeContentMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * Checks the interface name against the component's type and then calls the
   * overriden method. This method also creates the collection interfaces
   * when needed, and puts them in the {@link #_this_fcInternalInterfaces} map.
   *
   * @param interfaceName the name of the internal interface that must be
   *      returned.
   * @return the internal interface of the component to which this interface
   *      belongs, whose name is equal to the given name.
   * @throws NoSuchInterfaceException if there is no such interface.
   */

  public Object getFcInternalInterface (final String interfaceName)
    throws NoSuchInterfaceException
  {
    ComponentType compType = (ComponentType)_this_weaveableC.getFcType();
    InterfaceType itfType;
    try {
      itfType = compType.getFcInterfaceType(interfaceName);
    } catch (NoSuchInterfaceException e) {
      throw new ChainedNoSuchInterfaceException(
        null, _this_weaveableC, interfaceName);
    }
    Object result;
    try {
      result = _super_getFcInternalInterface(interfaceName);
    } catch (NoSuchInterfaceException e) {
      if (itfType.isFcCollectionItf()) {
        String collectionName = "/collection/" + itfType.getFcItfName();
        result = _super_getFcInternalInterface(collectionName);
        result = ((ComponentInterface)result).clone();
        ((ComponentInterface)result).setFcItfName(interfaceName);
        _this_fcInternalInterfaces.put(interfaceName, result);
      } else {
        throw e;
      }
    }
    return result;
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableC</tt> field required by this mixin. This field is
   * supposed to reference the {@link Component} interface of the component to
   * which this controller object belongs.
   */

  public Component _this_weaveableC;

  /**
   * The <tt>fcInterfaces</tt> field required by this mixin. This field is
   * supposed to store the internal interfaces of the component.
   */

  public Map _this_fcInternalInterfaces;

  /**
   * The {@link ContentController#getFcInternalInterface getFcInternalInterface}
   * method overriden by this mixin.
   *
   * @param interfaceName the name of the internal interface that must be
   *      returned.
   * @return the internal interface of the component to which this interface
   *      belongs, whose name is equal to the given name.
   * @throws NoSuchInterfaceException if there is no such interface.
   */

  public abstract Object _super_getFcInternalInterface (String interfaceName)
    throws NoSuchInterfaceException ;
}
