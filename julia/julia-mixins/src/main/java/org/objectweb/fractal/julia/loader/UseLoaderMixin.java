/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.loader;

import org.objectweb.fractal.api.factory.InstantiationException;

import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;

/**
 * Provides a {@link Loader} field to a {@link Controller}.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>the component to which this controller belongs must provide the {@link
 * Loader} interface.</li>
 * </ul>
 */

public abstract class UseLoaderMixin implements Controller {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private UseLoaderMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * The {@link Loader} interface of the component to which this controller
   * object belongs.
   */

  public Loader weaveableL;

  /**
   * Initializes the fields of this mixin and then calls the overriden method.
   *
   * @param ic information about the component to which this controller object
   *      belongs.
   * @throws InstantiationException if the initialization fails.
   */

  public void initFcController (final InitializationContext ic)
    throws InstantiationException
  {
    weaveableL = (Loader)ic.getInterface("loader");
    _super_initFcController(ic);
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The {@link Controller#initFcController initFcController} method overriden
   * by this mixin.
   *
   * @param ic information about the component to which this controller object
   *      belongs.
   * @throws InstantiationException if the initialization fails.
   */

  public abstract void _super_initFcController (InitializationContext ic)
    throws InstantiationException;
}
