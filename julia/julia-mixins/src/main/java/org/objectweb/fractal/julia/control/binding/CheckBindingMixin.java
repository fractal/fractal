/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.binding;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

/**
 * Provides basic checks to a {@link BindingController}.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>none.</li>
 * </ul>
 */

public abstract class CheckBindingMixin implements BindingController {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private CheckBindingMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * Checks that the given interface is unbound, and then calls the overriden
   * method.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @param serverItf a server interface.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be created.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public void bindFc (final String clientItfName, final Object serverItf) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException
  {
    if (_this_lookupFc(clientItfName) != null) {
      throw new ChainedIllegalBindingException(
        null,
        _this_weaveableOptC,
        null,
        clientItfName,
        null,
        "Already bound");
    }
    _super_bindFc(clientItfName, serverItf);
  }

  /**
   * Checks that the given interface is bound, and then calls the overriden
   * method.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be removed.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public void unbindFc (final String clientItfName) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException
  {
    if (_this_lookupFc(clientItfName) == null) {
      throw new ChainedIllegalBindingException(null,
        _this_weaveableOptC,
        null,
        clientItfName,
        null,
        "Not bound");
    }
    _super_unbindFc(clientItfName);
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableOptC</tt> field required by this mixin. This field is
   * supposed to reference the {@link Component} interface of the component to
   * which this controller object belongs.
   */

  public Component _this_weaveableOptC;

  /**
   * The {@link BindingController#listFc listFc} method required by this mixin.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @return the server interface to which the given interface is bound, or <tt>
   *      null</tt> if it is not bound.
   * @throws NoSuchInterfaceException if the component to which this interface
   *      belongs does not have a client interface whose name is equal to the
   *      given name.
   */

  public abstract Object _this_lookupFc (String clientItfName) throws
    NoSuchInterfaceException;

  /**
   * The {@link BindingController#bindFc bindFc} method overriden by this mixin.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @param serverItf a server interface.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be created.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public abstract void _super_bindFc (String clientItfName, Object serverItf) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException;

  /**
   * The {@link BindingController#unbindFc unbindFc} method overriden by this
   * mixin.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be removed.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public abstract void _super_unbindFc (String clientItfName) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException;
}
