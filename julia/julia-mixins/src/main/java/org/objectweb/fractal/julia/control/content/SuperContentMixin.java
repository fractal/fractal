/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.content;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

/**
 * Provides {@link SuperControllerNotifier notification} functions to a {@link
 * ContentController}.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>the component to which this controller object belongs must provide the
 * {@link Component} interface.</li>
 * </ul>
 */

public abstract class SuperContentMixin implements ContentController {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private SuperContentMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * Calls the overriden method and then notifies the given component it has
   * been added in this component. This method does nothing if the given sub
   * component does not provide the {@link SuperControllerNotifier} interface.
   *
   * @param subComponent the component to be added inside this component.
   * @throws IllegalContentException if the given component cannot be added
   *      inside this component.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public void addFcSubComponent (final Component subComponent)
    throws IllegalContentException, IllegalLifeCycleException
  {
    _super_addFcSubComponent(subComponent);
    SuperControllerNotifier scn = getFcSuperControllerNotifier(subComponent);
    if (scn != null) {
      try {
        Component c = (Component)_this_weaveableC.getFcInterface("component");
        scn.addedToFc(c);
      } catch (NoSuchInterfaceException ignored) {
      }
    }
  }

  /**
   * Calls the overriden method and then notifies the given component it has
   * been removed from in this component. This method does nothing if the given
   * sub component does not provide the {@link SuperControllerNotifier}
   * interface.
   *
   * @param subComponent the component to be removed from this component.
   * @throws IllegalContentException if the given component cannot be removed
   *      from this component.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public void removeFcSubComponent (final Component subComponent)
    throws IllegalContentException, IllegalLifeCycleException
  {
    _super_removeFcSubComponent(subComponent);
    SuperControllerNotifier scn = getFcSuperControllerNotifier(subComponent);
    if (scn != null) {
      try {
        Component c = (Component)_this_weaveableC.getFcInterface("component");
        scn.removedFromFc(c);
      } catch (NoSuchInterfaceException ignored) {
      }
    }
  }

  /**
   * Returns the {@link SuperControllerNotifier} interface of the given 
   * component.
   * 
   * @param c a component.
   * @return the {@link SuperControllerNotifier} interface of the given 
   *      component, or <tt>null</tt>.
   */

  private SuperControllerNotifier getFcSuperControllerNotifier (
    final Component c)
  {
    try {
      return (SuperControllerNotifier)c.getFcInterface("super-controller");
    } catch (Exception e) {
      try {
        return (SuperControllerNotifier)c.
          getFcInterface("/super-controller-notifier");
      } catch (NoSuchInterfaceException ignored) {
        return null;
      }
    }
  }
  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableC</tt> field required by this mixin. This field is
   * supposed to reference the {@link Component} interface of the component to
   * which this controller object belongs.
   */

  public Component _this_weaveableC;

  /**
   * The {@link ContentController#addFcSubComponent addFcSubComponent} method
   * overriden by this mixin.
   *
   * @param subComponent the component to be added inside this component.
   * @throws IllegalContentException if the given component cannot be added
   *      inside this component.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public abstract void _super_addFcSubComponent (Component subComponent)
    throws IllegalContentException, IllegalLifeCycleException;

  /**
   * The {@link ContentController#removeFcSubComponent removeFcSubComponent}
   * method overriden by this mixin.
   *
   * @param subComponent the component to be removed from this component.
   * @throws IllegalContentException if the given component cannot be removed
   *      from this component.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public abstract void _super_removeFcSubComponent (Component subComponent)
    throws IllegalContentException, IllegalLifeCycleException;
}
