/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.factory;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.fractal.api.factory.InstantiationException;

import org.objectweb.fractal.julia.control.attribute.CloneableAttributeController;

/**
 * Provides an attribute copy function to a {@link Template}.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>none (the template component may provide a {@link
 * CloneableAttributeController} interface, but this is not mandatory - in this
 * case this mixin does nothing).</li>
 * </ul>
 */

public abstract class AttributeTemplateMixin implements Template {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private AttributeTemplateMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * Calls the overriden method and then sets the attributes of the created
   * component. The attributes of the created component are initialized from the
   * attributes name of this template, if the template has a {@link
   * CloneableAttributeController} interface, and if the created component has
   * an {@link org.objectweb.fractal.api.control.AttributeController} interface
   * (otherwise this mixin does nothing).
   * 
   * @return the instantiated component.
   * @throws InstantiationException if the component controller cannot be
   *      instantiated.
   */

  public Component newFcControllerInstance () throws InstantiationException {
    Component comp = _super_newFcControllerInstance();
    if (_this_weaveableOptCAC != null) {
      // copies the template's attributes into the component, if applicable
      try {
        _this_weaveableOptCAC.cloneFcAttributes(
          (AttributeController)comp.getFcInterface("attribute-controller"));
      } catch (NoSuchInterfaceException ignored) {
      }
    }
    return comp;
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableOptcCAC</tt> field required by this mixin. This field is
   * supposed to reference the {@link CloneableAttributeController} interface
   * of the component to which this controller object belongs.
   */

  public CloneableAttributeController _this_weaveableOptCAC;

  /**
   * The {@link Template#newFcControllerInstance newFcControllerInstance}
   * overriden by this mixin.
   * 
   * @return the instantiated component.
   * @throws InstantiationException if the component controller cannot be
   *      instantiated.
   */

  public abstract Component _super_newFcControllerInstance () 
    throws InstantiationException;
}
