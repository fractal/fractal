package org.objectweb.fractal.julia.factory;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.loader.Initializable;
import org.objectweb.fractal.julia.loader.Tree;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides reflective checks to a {@link GenericFactory}.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>the types of the components created with this generic factory must be
 * instances of the {@link ComponentType} interface.</li>
 * <li>the Java platform must provide the Java Reflection API, the
 * ClassLoader class, and Thread.getContextClassLoader method.</li>
 * </ul>
 */

public abstract class CheckGenericFactoryMixin 
  implements GenericFactory, Initializable 
{

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private CheckGenericFactoryMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  private boolean useContextClassLoader;
  
  /**
   * Initializes this object with the given arguments.
   *
   * @param args the arguments to be used to initialize this object. The format
   *      of these arguments depends on the class of this object.
   * @throws Exception if a problem occurs during the object initialization.
   */

  public void initialize (final Tree args) throws Exception {
    Tree t = args.getValue("use-context-class-loader");
    if (t != null && t.equals("true")) {
      useContextClassLoader = true;
    }
  }

  /**
   * Checks that <tt>type</tt> and <tt>contentDesc</tt> are compatible, and
   * then calls the overriden method.
   *
   * @param type an arbitrary component type.
   * @param controllerDesc a description of the controller part of the
   *      component to be created. This description is implementation specific.
   *      If it is <tt>null</tt> then a "default" controller part will be used.
   * @param contentDesc a description of the content part of the
   *      component to be created. This description is implementation specific.
   *      It may be <tt>null</tt> to create component with an empty initial
   *      content. It may also be, in Java, the fully qualified name of a Java
   *      class, to create primitive components.
   * @return the {@link Component} interface of the created component.
   * @throws InstantiationException if the component cannot be created.
   */

  public Component newFcInstance (
    final Type type,
    final Object controllerDesc,
    final Object contentDesc) throws InstantiationException
  {
    ClassLoader loader = null;
    if (controllerDesc instanceof Object[]) {
      loader = (ClassLoader)((Object[])controllerDesc)[0];
    }
    loader = (ClassLoader)getFcLoader(loader);

    checkFcType(type, loader);
    
    Object content = contentDesc;
    if (content instanceof Object[]) {
      // case of a template content descriptor:
      //   extract the instance content descriptor
      content = ((Object[])content)[1];
    }
    if (content instanceof String) {
      checkFcContentClass(type, (String)content, loader);
    }
    return _super_newFcInstance(type, controllerDesc, contentDesc);
  }

  public Object getFcLoader (final Object loader) {
    if (loader == null) {
      if (useContextClassLoader) {
        return Thread.currentThread().getContextClassLoader();
      } else {
        return getClass().getClassLoader();
      }
    } else {
      return _super_getFcLoader(loader);
    }
  }
  
  /**
   * Checks the given component type.
   * 
   * @param type the component type to be checked.
   * @param loader the class loader to be used to load the Java interface types.
   * @throws InstantiationException if the given type is invalid.
   */
  
  public void checkFcType (final Type type, final ClassLoader loader) 
    throws InstantiationException 
  {
    if (type instanceof ComponentType) {
      InterfaceType[] itfTypes = ((ComponentType)type).getFcInterfaceTypes();
      for (int i = 0; i < itfTypes.length; ++i) {
        InterfaceType itfType = itfTypes[i];
        String name = itfType.getFcItfName();
        String signature = itfType.getFcItfSignature();
        // checks that the interface exists
        Class itf;
        try {
          itf = loader.loadClass(signature);
        } catch (ClassNotFoundException e) {
          throw new ChainedInstantiationException(
            e, null, "No such interface: " + signature);
        }
        // checks that it is a public interface
        if (!(itf.isInterface() && Modifier.isPublic(itf.getModifiers()))) {
          throw new ChainedInstantiationException(
            null, null, signature + " is not a public interface");
        }
        if (name.equals("attribute-controller")) {
          // checks that itf is a valid atrribute controller interface
          Class ac;
          try {
            ac = loader.loadClass(
              "org.objectweb.fractal.api.control.AttributeController");
          } catch (ClassNotFoundException e) {
            throw new ChainedInstantiationException(
              e, null, "Cannot check this operation");
          }
          if (ac.isAssignableFrom(itf)) {
            if (!checkFcAttributeControllerInterface(itf)) {
              throw new ChainedInstantiationException(
                null,
                null,
                signature + " is not a valid attribute controller interface");
            }
          }
        }
      }
    }
  }
  
  /**
   * Checks that the given class is valid attribute controller interface.
   *
   * @param itf a Java interface
   * @return <tt>true</tt> if the given interface is valid, or <tt>false</tt>
   *      otherwise.
   */

  public boolean checkFcAttributeControllerInterface (final Class itf) {
    Map types = new HashMap();
    Method[] meths = itf.getMethods();
    // checks each method
    for (int i = 0; i < meths.length; ++i) {
      Method m = meths[i];
      String name = m.getName();
      Class[] formals = m.getParameterTypes();
      Class result = m.getReturnType();
      if (name.startsWith("get")) {
        if (formals.length == 0 && !result.equals(Void.TYPE)) {
          types.put(name, result);
          continue;
        }
      } else if (name.startsWith("set")) {
        if (formals.length == 1 && result.equals(Void.TYPE)) {
          types.put(name, formals[0]);
          continue;
        }
      }
      return false;
    }
    // checks that if a get/set method has a complementary set/get method,
    // then the two methods are compatible
    for (int i = 0; i < meths.length; ++i) {
      Method m = meths[i];
      String name = m.getName();
      if (name.startsWith("get")) {
        Class type = (Class)types.get("set" + name.substring(3));
        if (type == null || m.getReturnType().equals(type)) {
          continue;
        }
      } else {
        Class type = (Class)types.get("get" + name.substring(3));
        if (type == null || m.getParameterTypes()[0].equals(type)) {
          continue;
        }
      }
      return false;
    }
    return true;
  }

  /**
   * Checks the given class against the given component type. This method checks
   * that the given class exists, that it is public non abstract class with a
   * default public constructor, that it implements all the server interface
   * types (except control interface types) of the given type, and that it
   * implements BindingController (if there is at least one client interface).
   *
   * @param type a component type, must be instance of {@link ComponentType}.
   * @param content the fully qualified name of a Java class.
   * @param loader the class loader to be used to load the "content" class.
   * @throws InstantiationException if the given class is not compatible with
   *      the given component type.
   */

  public void checkFcContentClass (
    final Type type,
    final String content, 
    final ClassLoader loader) throws InstantiationException
  {
    Class contentClass;
    // checks that the class exists
    try {
      contentClass = loader.loadClass(content);
    } catch (ClassNotFoundException e) {
      throw new ChainedInstantiationException(
        e,
        null,
        "Cannot find the component implementation class '" + content + "'");
    }
    // checks that the class is public and non abstract class
    int mods = contentClass.getModifiers();
    if (!Modifier.isPublic(mods) ||
        Modifier.isAbstract(mods) ||
        Modifier.isInterface(mods))
    {
      throw new ChainedInstantiationException(
        null,
        null,
        "The component implementation class '" + content +
        "' is a not public, non abstract class");
    }
    // checks that the class has a default public constructor
    try {
      contentClass.getConstructor(new Class[0]);
    } catch (final NoSuchMethodException e) {
      throw new ChainedInstantiationException(
        null,
        null,
        "The component implementation class '" + content +
        "' does not have a default public constructor");
    }
    // check that the class implements BindingController, if this is required
    boolean hasDependencies = false;
    ComponentType compType = (ComponentType)type;
    InterfaceType[] itfTypes = compType.getFcInterfaceTypes();
    for (int i = 0; i < itfTypes.length; i++) {
      InterfaceType itfType = itfTypes[i];
      if (itfType.isFcClientItf()) {
        hasDependencies = true;
      }
    }
    if (hasDependencies) {
      Class bc;
      try {
        bc = loader.loadClass(
          "org.objectweb.fractal.api.control.BindingController");
      } catch (ClassNotFoundException e) {
        throw new ChainedInstantiationException(
          e, null, "Cannot find the BindingController class");
      }
      if (!bc.isAssignableFrom(contentClass)) {
        throw new ChainedInstantiationException(
          null,
          null,
          "The component implementation class '" + content +
          "' must implement the BindingController interface, " +
          "since the component type contains client interfaces");
      }
    }
    // check that the class implements all the server interface types
    for (int i = 0; i < itfTypes.length; ++i) {
      InterfaceType itfType = itfTypes[i];
      String itfName = itfType.getFcItfName();
      if (!itfType.isFcClientItf() &&
          !itfType.isFcOptionalItf() &&
          !(itfName.equals("component") ||itfName.endsWith("-controller")))
      {
        Class itf;
        try {
          itf = loader.loadClass(itfType.getFcItfSignature());
        } catch (ClassNotFoundException e) {
          throw new ChainedInstantiationException(
            e,
            null,
            "Cannot find the Java interface '" + itfType.getFcItfSignature() +
            "' declared in the component type");
        }
        if (!itf.isAssignableFrom(contentClass)) {
          throw new ChainedInstantiationException(
            null,
            null,
            "The component implementation class '" + content +
            "' does not implement the '" + itf.getName() +
            "' server interface declared in the component type");
        }
      }
    }
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The {@link Initializable#initialize initialize} method overriden by
   * this mixin.
   *
   * @param args the arguments to be used to initialize this object. The format
   *      of these arguments depends on the class of this object.
   * @throws Exception if a problem occurs during the object initialization.
   */

  public abstract void _super_initialize (Tree args) throws Exception;

  /**
   * The {@link GenericFactory#newFcInstance newFcInstance} method overriden by
   * this mixin.
   *
   * @param type an arbitrary component type.
   * @param controllerDesc a description of the controller part of the
   *      component to be created. This description is implementation specific.
   *      If it is <tt>null</tt> then a "default" controller part will be used.
   * @param contentDesc a description of the content part of the
   *      component to be created. This description is implementation specific.
   *      It may be <tt>null</tt> to create component with an empty initial
   *      content. It may also be, in Java, the fully qualified name of a Java
   *      class, to create primitive components.
   * @return the {@link Component} interface of the created component.
   * @throws InstantiationException if the component cannot be created.
   */

  public abstract Component _super_newFcInstance (
    Type type,
    Object controllerDesc,
    Object contentDesc) throws InstantiationException;
  
  public abstract Object _super_getFcLoader (Object loader);
}
