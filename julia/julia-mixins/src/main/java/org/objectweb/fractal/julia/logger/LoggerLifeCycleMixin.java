/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.logger;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.Component;

import org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator;
import org.objectweb.fractal.julia.control.binding.ContentBindingController;
import org.objectweb.fractal.julia.Util;
import org.objectweb.fractal.julia.loader.Initializable;
import org.objectweb.fractal.julia.loader.Tree;

import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.MonologFactory;
import org.objectweb.util.monolog.api.Loggable;
import org.objectweb.util.monolog.Monolog;

/**
 * Assigns a logger to a component during its startup. The topic
 * name is based on the component path in the architecture. To use this mixin
 * you have to add two lines in the julia.cfg configuration file:
 * <pre>
 * (lifecycle-controller-impl
 *   ((org.objectweb.fractal.julia.asm.MixinClassGenerator
 *     LifeCycleControllerImpl
 *     org.objectweb.fractal.julia.BasicControllerMixin
 *     org.objectweb.fractal.julia.UseComponentMixin
 *     org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleCoordinatorMixin
 *     org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleControllerMixin
 *     # to check that mandatory client interfaces are bound in startFc:
 *     org.objectweb.fractal.julia.control.lifecycle.TypeLifeCycleMixin
 *     <font color="red"># to automatically assign the logger and logger factory:
 *     org.objectweb.fractal.julia.BasicInitializableMixin
 *     org.objectweb.fractal.julia.logger.LoggerLifeCycleMixin</font>
 *     # to notify the encapsulated component (if present) when its state changes:
 *     org.objectweb.fractal.julia.control.lifecycle.ContainerLifeCycleMixin
 *   )
 *     <font color="red"># optional initialization parameter (monolog configuration file name):
 *     (monolog-conf-file monolog.properties)</font>
 *   )
 * )</pre>
 * The user component must also implement either {@link BindingController} or
 * {@link Loggable}. In the first case, the logger and logger factory can be
 * retreived like this:
 * <pre>
 * public void bindFc (String s, Object o) {
 *   if ("logger".equals(s)) {
 *     myLogger = (Logger) o;
 *   } else if ("monolog-factory".equals(s)) { // optional
 *     String baseName = myLogger.getName();
 *     otherLogger1 = ((LoggerFactory) o).getLogger(baseName + ".toto");
 *     otherLogger2 = ((LoggerFactory) o).getLogger(baseName + ".titi");
 *   } else ...
 * }</pre>
 * The other {@link BindingController} methods, and in particular the {@link
 * BindingController#listFc listFc} method, must not take these logger
 * "bindings" into account.
 *
 * @author E.Bruneton, S.Chassande-Barrioz
 */

public abstract class LoggerLifeCycleMixin
  implements LifeCycleCoordinator, Initializable
{

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private LoggerLifeCycleMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  public void initialize (final Tree args) throws Exception {
    _super_initialize(args);
    for (int i = 0; i < args.getSize(); ++i) {
      Tree arg = args.getSubTree(i);
      if (arg.getSize() == 2 && arg.getSubTree(0).equals("monolog-conf-file")) {
        String monologConfFile = arg.getSubTree(1).toString();
        if (Monolog.monologFactory == Monolog.getDefaultMonologFactory()) {
          Monolog.getMonologFactory(monologConfFile);
        }
      }
    }
  }

  /**
   * Calls the overriden method and then sets the logger and logger factory of
   * the user component encapsulated in this component (if there is one).
   * 
   * @return <tt>true</tt> if the execution state has changed, or <tt>false</tt>
   *      if it had already the {@link #STARTED STARTED} value.
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  public boolean setFcStarted () throws IllegalLifeCycleException {
    boolean result = _super_setFcStarted();
    try {
      if (Monolog.monologFactory == Monolog.getDefaultMonologFactory()) {
        Monolog.initialize();
      }
      StringBuffer path = new StringBuffer();
      Util.toString(_this_weaveableC, path);
      String s = path.toString().substring(1).replace('/', '.');
      MonologFactory mf = Monolog.monologFactory;
      Logger logger = Monolog.monologFactory.getLogger(s);
      Object content = _this_weaveableC.getFcInterface("/content");
      if (content instanceof Loggable) {
        ((Loggable)content).setLogger(logger);
        ((Loggable)content).setLoggerFactory(mf);
      }
      if (content instanceof ContentBindingController) {
        ContentBindingController bc = (ContentBindingController)content;
        // final static constants must be avoided in mixins
        bc.bindFcContent("logger", logger);
        bc.bindFcContent("monolog-factory", mf);
      } else if (content instanceof BindingController) {
        BindingController bc = (BindingController)content;
        // final static constants must be avoided in mixins
        bc.bindFc("logger", logger);
        bc.bindFc("monolog-factory", mf);
      }
    } catch (Exception ignored) {
    }
    return result;
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableC</tt> field required by this mixin. This field is
   * supposed to reference the {@link Component} interface of the component to
   * which this controller object belongs.
   */

  public Component _this_weaveableC;

  /**
   * The {@link Initializable#initialize initialize} method overriden by this
   * mixin.
   * 
   * @param args the arguments to be used to initialize this object. The format
   *      of these arguments depends on the class of this object.
   * @throws Exception if a problem occurs during the object initialization.
   */

  public abstract void _super_initialize (final Tree args) throws Exception;

  /**
   * The {@link LifeCycleCoordinator#setFcStarted setFcStarted} method overriden
   * by this mixin.
   * 
   * @return <tt>true</tt> if the execution state has changed, or <tt>false</tt>
   *      if it had already the {@link #STARTED STARTED} value.
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  public abstract boolean _super_setFcStarted() throws IllegalLifeCycleException;
}
