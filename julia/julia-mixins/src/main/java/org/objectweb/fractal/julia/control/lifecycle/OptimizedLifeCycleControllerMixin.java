/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.lifecycle;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.type.InterfaceType;

import org.objectweb.fractal.julia.control.binding.Util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.ArrayList;

/**
 * Provides an optimized implementation of the {@link
 * org.objectweb.fractal.api.control.LifeCycleController} interface. This
 * implementation does not need interceptors. This life cycle controller
 * works by stopping all the direct and indirect primitive sub components that
 * have a {@link LifeCycleCoordinator} interface, and the primitive client
 * components of these components (these clients <i>must</i> also have a {@link
 * LifeCycleCoordinator} interface). These components are stopped
 * simultaneously, by using the coordinator interface provided by this
 * component.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>TODO.</li>
 * </ul>
 */

public abstract class OptimizedLifeCycleControllerMixin
  implements LifeCycleCoordinator
{

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private OptimizedLifeCycleControllerMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * Indicates if this component is started or not.
   */

  public boolean fcStarted;

  // -------------------------------------------------------------------------
  // Implementation of the LifeCycleController interface
  // -------------------------------------------------------------------------

  public String getFcState () {
    return fcStarted ? STARTED : STOPPED;
  }

  public void startFc () throws IllegalLifeCycleException {
    Component id;
    try {
      id = (Component)_this_weaveableC.getFcInterface("component");
    } catch (NoSuchInterfaceException e) {
      throw new ChainedIllegalLifeCycleException(
        e, _this_weaveableC, "Cannot start component");
    }
    LifeCycleCoordinator[] clccs = getFcLifeCycleControllers(id);
    for (int i = 0; i < clccs.length; ++i) {
      clccs[i].setFcStarted();
    }
    _this_setFcState(true);
  }

  public void stopFc () throws IllegalLifeCycleException {
    Component id;
    try {
      id = (Component)_this_weaveableC.getFcInterface("component");
    } catch (NoSuchInterfaceException e) {
      throw new ChainedIllegalLifeCycleException(
        e, _this_weaveableC, "Cannot stop component");
    }
    LifeCycleCoordinator[] clccs = getFcLifeCycleControllers(id);
    _this_stopFc(clccs);
    _this_setFcState(false);
  }

  // -------------------------------------------------------------------------
  // Implementation of the CoordinatorLifeCycleController interface
  // -------------------------------------------------------------------------

  public boolean setFcStarted () {
    if (!fcStarted) {
      fcStarted = true;
      return true;
    }
    return false;
  }

  public void setFcStopping (final LifeCycleCoordinator coordinator)
    throws IllegalLifeCycleException
  {
    // this method should never be called, 
    // as this component is stopped by stopping its sub components
    throw new Error("Internal error");
  }

  public boolean setFcStopped () {
    if (fcStarted) {
      fcStarted = false;
      return true;
    }
    return false;
  }

  // -------------------------------------------------------------------------
  // Utility methods
  // -------------------------------------------------------------------------

  /**
   * Returns the components that must be stopped in order to stop the given
   * component. These components are the direct or indirect primitive sub
   * components of this component that provide a {@link LifeCycleCoordinator}
   * interface, as well as the primitive client components of these components.
   *
   * @param id a composite component.
   * @return the components that must be stopped in order to stop the given
   *      component.
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  public LifeCycleCoordinator[] getFcLifeCycleControllers (
    final Component id) throws IllegalLifeCycleException
  {
    List clccList = getFcInternalLifeCycleControllers();
    Object[] sItfs = id.getFcInterfaces();
    Set visited = new HashSet();
    for (int i = 0; i < sItfs.length; ++i) {
      Interface sItf = (Interface)sItfs[i];
      if (!((InterfaceType)sItf.getFcItfType()).isFcClientItf()) {
        getSExtLifeCycleControllers(sItf, clccList, visited);
      }
    }
    LifeCycleCoordinator[] clccs;
    clccs = new LifeCycleCoordinator[clccList.size()];
    return (LifeCycleCoordinator[])clccList.toArray(clccs);
  }

  /**
   * Finds the primitive client components that are bound to the given server
   * interface.
   *
   * @param serverItf a server interface.
   * @param clccList where to put the {@link LifeCycleCoordinator}
   *      interfaces of the primitive client components that are found.
   * @param visited the already visited interfaces.
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  private void getSExtLifeCycleControllers (
    final Interface serverItf,
    final List clccList,
    final Set visited) throws IllegalLifeCycleException
  {
    Object[] comps;
    try {
      comps = Util.getFcPotentialClientsOf(serverItf).toArray();
    } catch (Exception e) {
      throw new ChainedIllegalLifeCycleException(
        e,
        serverItf.getFcItfOwner(),
        "Cannot get the LifeCycleCoordinator interfaces");
    }
    for (int i = 0; i < comps.length; ++i) {
      Component comp = (Component)comps[i];
      Interface[] clientItfs;
      try {
        List l = Util.getFcClientItfsBoundTo(comp, serverItf);
        clientItfs = (Interface[])l.toArray(new Interface[l.size()]);
      } catch (Exception e) {
        throw new ChainedIllegalLifeCycleException(
          e,
          serverItf.getFcItfOwner(),
          "Cannot get the LifeCycleCoordinator interfaces");
      }
      for (int j = 0; j < clientItfs.length; ++j) {
        getCExtLifeCycleControllers(clientItfs[j], clccList, visited);
      }
    }
  }

  /**
   * Finds the primitive client components that are bound to the given client
   * interface.
   *
   * @param clientItf a client interface.
   * @param clccList where to put the {@link LifeCycleCoordinator} interfaces of
   *      the primitive client components that are found.
   * @param visited the already visited interfaces.
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  private void getCExtLifeCycleControllers (
    final Interface clientItf,
    final List clccList,
    final Set visited) throws IllegalLifeCycleException
  {
    Component component = clientItf.getFcItfOwner();
    ContentController cc = null;
    try {
      cc = (ContentController)component.getFcInterface("content-controller");
    } catch (NoSuchInterfaceException e) {
    }
    if (cc != null) {
      Interface itf;
      String name = clientItf.getFcItfName();
      try {
        if (!clientItf.isFcInternalItf()) {
          itf = (Interface)cc.getFcInternalInterface(name);
        } else {
          itf = (Interface)component.getFcInterface(name);
        }
      } catch (NoSuchInterfaceException e) {
        throw new ChainedIllegalLifeCycleException(
          e,
          component,
          "Cannot find the LifeCycleCoordinator interfaces");
      }
      if (!visited.contains(itf)) {
        visited.add(itf);
        getSExtLifeCycleControllers(itf, clccList, visited);
      }
    } else if (!visited.contains(clientItf)) {
      visited.add(clientItf);
      // Component c = clientItf.getFcItfOwner();
      LifeCycleCoordinator lcc;
      try {
        lcc = (LifeCycleCoordinator)component.getFcInterface("lifecycle-controller");
      } catch (Exception e) {
        try {
          lcc = (LifeCycleCoordinator)component.getFcInterface("/lifecycle-coordinator");
        } catch (NoSuchInterfaceException f) {
          throw new ChainedIllegalLifeCycleException(
            f, component, "Primitive client without a LifeCycleCoordinator");
        }
      }
      if (!clccList.contains(lcc)) {
        clccList.add(lcc);
      }
    }
  }

  public List getFcInternalLifeCycleControllers ()
    throws IllegalLifeCycleException
  {
    // finds all the direct and indirect sub components of this component
    Component thisComponent;
    try {
      thisComponent = (Component)_this_weaveableC.getFcInterface("component");
    } catch (NoSuchInterfaceException e) {
      throw new ChainedIllegalLifeCycleException(
        e, 
        _this_weaveableC, 
        "The OptimizedLifeCycleControllerMixin requires " +
        "components to provide the Component interface");
    }
    List allSubComponents = org.objectweb.fractal.julia.control.content.Util.
      getAllSubComponents(thisComponent);

    List result = new ArrayList();
    for (int i = 0; i < allSubComponents.size(); ++i) {
      Component c = (Component)allSubComponents.get(i);
      try {
        c.getFcInterface("content-controller");
      } catch (NoSuchInterfaceException e) {
        try {
          // do not remove the cast!
          result.add(
            (LifeCycleCoordinator)c.getFcInterface("lifecycle-controller"));
        } catch (Exception f) {
          try {
            result.add(c.getFcInterface("/lifecycle-coordinator"));
          } catch (NoSuchInterfaceException ignored) {
          }
        }
      }
    }
    return result;
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableC</tt> field required by this mixin. This field is
   * supposed to reference the {@link Component} interface of the component to
   * which this controller object belongs.
   */

  public Component _this_weaveableC;

  /**
   * The <tt>setFcState</tt> method required by this mixin. This method is
   * supposed to work as this {@link BasicLifeCycleCoordinatorMixin#setFcState
   * setFcState} method.
   *
   * @param started <tt>true</tt> to set the lifecycle state of the components
   *      to {@link #STARTED STARTED}, or <tt>false</tt> to set this state to
   *      {@link #STOPPED STOPPED}.
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  public abstract void _this_setFcState (boolean started)
    throws IllegalLifeCycleException;

  /**
   * The <tt>stopFc</tt> method required by this mixin. This method is
   * supposed to work as this {@link BasicLifeCycleCoordinatorMixin#stopFc
   * stopFc} method.
   * 
   * @param components the {@link LifeCycleCoordinator} interface of the
   *      components to be stopped.
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  public abstract void _this_stopFc (LifeCycleCoordinator[] components)
    throws IllegalLifeCycleException;
}
