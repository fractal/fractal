/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.binding;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.type.InterfaceType;

import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.Interceptor;

/**
 * Provides {@link ComponentInterface} management to a {@link
 * org.objectweb.fractal.api.control.BindingController}.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>the component to which this component belongs must provide the {@link
 * Component} and {@link ContentController} interfaces. Its type must be an
 * instance of {@link org.objectweb.fractal.api.type.ComponentType}, and its
 * interfaces must implement {@link ComponentInterface}.</li>
 * </ul>
 */

public abstract class CompositeBindingMixin {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private CompositeBindingMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * Calls the overriden method and then updates the {@link
   * ComponentInterface#getFcItfImpl getFcItfImpl} link (or the {@link
   * Interceptor#getFcItfDelegate getFcItfDelegate} link of the corresponding
   * interceptor) of the <tt>clientItfName</tt> interface to <tt>serverItf</tt>.
   *
   * @param clientItfType the type of the <tt>clientItfName</tt> interface.
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @param serverItf a server interface.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be created.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but
   *      it is not in an appropriate state to perform this operation.
   */

  public void bindFc (
    final InterfaceType clientItfType,
    final String clientItfName,
    final Object serverItf)
    throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException
  {
    _super_bindFc(clientItfType, clientItfName, serverItf);
    Object complementaryItf;
    if (clientItfType.isFcClientItf()) {
      complementaryItf =
        _this_weaveableCC.getFcInternalInterface(clientItfName);
    } else {
      complementaryItf =
        _this_weaveableC.getFcInterface(clientItfName);
    }
    ComponentInterface itf = (ComponentInterface)complementaryItf;
    if (itf.hasFcInterceptor()) {
      Object newImpl = itf.getFcItfImpl();
      ((Interceptor)newImpl).setFcItfDelegate(serverItf);
    } else {
      itf.setFcItfImpl(serverItf);
    }
  }

  /**
   * Calls the overriden method and then updates the {@link
   * ComponentInterface#getFcItfImpl getFcItfImpl} link (or the {@link
   * Interceptor#getFcItfDelegate getFcItfDelegate} link of the corresponding
   * interceptor) of the <tt>clientItfName</tt> interface to <tt>null</tt>.
   *
   * @param clientItfType the type of the <tt>clientItfName</tt> interface.
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be removed.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but
   *      it is not in an appropriate state to perform this operation.
   */

  public void unbindFc (
    final InterfaceType clientItfType,
    final String clientItfName)
    throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException
  {
    _super_unbindFc(clientItfType, clientItfName);
    Object complementaryItf;
    if (clientItfType.isFcClientItf()) {
      complementaryItf =
        _this_weaveableCC.getFcInternalInterface(clientItfName);
    } else {
      complementaryItf =
        _this_weaveableC.getFcInterface(clientItfName);
    }
    ComponentInterface itf = (ComponentInterface)complementaryItf;
    if (itf.hasFcInterceptor()) {
      Object newImpl = itf.getFcItfImpl();
      ((Interceptor)newImpl).setFcItfDelegate(null);
    } else {
      itf.setFcItfImpl(null);
    }
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableC</tt> field required by this mixin. This field is
   * supposed to reference the {@link Component} interface of the component to
   * which this controller object belongs.
   */

  public Component _this_weaveableC;

  /**
   * The <tt>weaveableCC</tt> field required by this mixin. This field is
   * supposed to reference the {@link ContentController} interface of the
   * component to which this controller object belongs.
   */

  public ContentController _this_weaveableCC;

  /**
   * The {@link TypeBindingMixin#bindFc(InterfaceType,String,Object) bindFc}
   * method overriden by this mixin.
   *
   * @param clientItfType the type of the <tt>clientItfName</tt> interface.
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @param serverItf a server interface.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be created.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but
   *      it is not in an appropriate state to perform this operation.
   */

  public abstract void _super_bindFc (
    InterfaceType clientItfType,
    String clientItfName,
    Object serverItf)
    throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException;

  /**
   * The {@link TypeBindingMixin#unbindFc(InterfaceType,String) unbindFc}
   * method overriden by this mixin.
   *
   * @param clientItfType the type of the <tt>clientItfName</tt> interface.
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be removed.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but
   *      it is not in an appropriate state to perform this operation.
   */

  public abstract void _super_unbindFc (
    InterfaceType clientItfType,
    String clientItfName)
    throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException;
}
