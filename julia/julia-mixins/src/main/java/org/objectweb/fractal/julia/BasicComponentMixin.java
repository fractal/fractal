/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.InstantiationException;

import java.util.Map;

/**
 * Provides a basic implementation of the {@link Component} interface.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>none.</li>
 * </ul>
 */

public abstract class BasicComponentMixin implements Controller, Component {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private BasicComponentMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * The type of this component.
   */

  public Type fcType;

  /**
   * The interfaces of this component. This map associates each interface to its
   * name.
   */

  public Map fcInterfaces;

  /**
   * Initializes the fields of this mixin from the given context, and then calls
   * the overriden method.
   * 
   * @param ic information about the component to which this controller object
   *      belongs.
   * @throws InstantiationException if the initialization fails.
   */

  public void initFcController (final InitializationContext ic)
    throws InstantiationException
  {
    this.fcType = ic.type;
    this.fcInterfaces = ic.interfaces;
    _super_initFcController(ic);
  }

  public Type getFcType () {
    return fcType;
  }

  public Object[] getFcInterfaces () {
    if (fcInterfaces == null) {
      return new Object[0];
    }
    // returns the names of all the public interfaces in fcInterfaces
    // interfaces whose name begins with '/' are private
    int size = 0;
    String[] names = new String[fcInterfaces.size()];
    names = (String[])fcInterfaces.keySet().toArray(names);
    for (int i = 0; i < names.length; ++i) {
      if (names[i].charAt(0) != '/') {
        ++size;
      }
    }
    int index = 0;
    Object[] itfs = new Object[size];
    for (int i = 0; i < names.length; ++i) {
      if (names[i].charAt(0) != '/') {
        itfs[index++] = fcInterfaces.get(names[i]);
      }
    }
    return itfs;
  }

  public Object getFcInterface (final String interfaceName)
    throws NoSuchInterfaceException
  {
    Object itf = fcInterfaces == null ? null : fcInterfaces.get(interfaceName);
    if (itf == null) {
      throw new ChainedNoSuchInterfaceException(null, this, interfaceName);
    }
    return itf;
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The {@link Controller#initFcController initFcController} method overriden
   * by this mixin.
   * 
   * @param ic information about the component to which this controller object
   *      belongs.
   * @throws InstantiationException if the initialization fails.
   */

  public abstract void _super_initFcController (InitializationContext ic)
    throws InstantiationException;
}
