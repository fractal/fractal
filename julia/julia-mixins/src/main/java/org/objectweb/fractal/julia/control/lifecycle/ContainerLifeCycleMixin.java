/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.lifecycle;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;

/**
 * Provides container related functions to a {@link LifeCycleController}.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>none.</li>
 * </ul>
 */

public abstract class ContainerLifeCycleMixin
  implements LifeCycleCoordinator
{

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private ContainerLifeCycleMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * Calls the overriden method and then calls the {@link #setFcContentState
   * setFcContentState} method.
   * 
   * @return <tt>true</tt> if the execution state has changed, or <tt>false</tt>
   *      if it had already the {@link #STARTED STARTED} value.
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  public boolean setFcStarted () throws IllegalLifeCycleException {
    synchronized (this) {
      if (_super_setFcStarted()) {
        setFcContentState(true);
        return true;
      }
      return false;
    }
  }

  /**
   * Calls the overriden method and then calls the {@link #setFcContentState
   * setFcContentState} method.
   *
   * @return <tt>true</tt> if the execution state has changed, or <tt>false</tt>
   *      if it had already the {@link #STOPPED STOPPED} value.
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  public boolean setFcStopped () throws IllegalLifeCycleException {
    synchronized (this) {
      if (_super_setFcStopped()) {
        setFcContentState(false);
        return true;
      }
      return false;
    }
  }

  /**
   * Calls the {@link LifeCycleController#startFc startFc} or {@link
   * LifeCycleController#stopFc stopFc} method on the encapsulated component.
   * This method does nothing if there is no encapsulated component, or if it
   * does not implement the {@link LifeCycleController} interface.
   *
   * @param started <tt>true</tt> to call the {@link LifeCycleController#startFc
   *      startFc}, or <tt>false</tt> to call the {@link
   *      LifeCycleController#stopFc stopFc} method.
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  public void setFcContentState (final boolean started)
    throws IllegalLifeCycleException
  {
    Object content;
    try {
      content = _this_weaveableOptC.getFcInterface("/content");
    } catch (NullPointerException e) {
      return;
    } catch (NoSuchInterfaceException e) {
      return;
    }
    if (content == this) {
      // case of merge...AndContent options
      if (this instanceof ContentLifeCycleController) {
        if (started) {
          ((ContentLifeCycleController)this).startFcContent();
        } else {
          ((ContentLifeCycleController)this).stopFcContent();
        }
      }
    } else if (content instanceof LifeCycleController) {
      if (started) {
        ((LifeCycleController)content).startFc();
      } else {
        ((LifeCycleController)content).stopFc();
      }
    }
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableOptC</tt> field required by this mixin. This field is
   * supposed to reference the {@link Component} interface of the component to
   * which this controller object belongs.
   */

  public Component _this_weaveableOptC;

  /**
   * The {@link LifeCycleCoordinator#setFcStarted setFcStarted} method overriden
   * by this mixin.
   * 
   * @return <tt>true</tt> if the execution state has changed, or <tt>false</tt>
   *      if it had already the {@link #STARTED STARTED} value.
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  public abstract boolean _super_setFcStarted ()
    throws IllegalLifeCycleException;

  /**
   * The {@link LifeCycleCoordinator#setFcStopped setFcStopped} method overriden
   * by this mixin.
   *
   * @return <tt>true</tt> if the execution state has changed, or <tt>false</tt>
   *      if it had already the {@link #STOPPED STOPPED} value.
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  public abstract boolean _super_setFcStopped ()
    throws IllegalLifeCycleException;
}
