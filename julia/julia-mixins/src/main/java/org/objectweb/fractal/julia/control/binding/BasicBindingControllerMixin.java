/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.binding;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

import java.util.HashMap;
import java.util.Map;

/**
 * Provides a basic implementation of the {@link BindingController} interface.
 * This mixin uses a map to store the bindings of the component.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>none.</li>
 * </ul>
 */

public abstract class BasicBindingControllerMixin implements BindingController {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private BasicBindingControllerMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * The bindings of the component to which this controller object belongs.
   * This map associates to each client interface name the server interface to
   * which it is bound. If a client interface is not bound, its name is not
   * associated to <tt>null</tt>, but to the {@link #fcBindings} map itself.
   * This way the {@link #listFc listFc} method returns the names of all the
   * client interfaces of the component, and not only the names of the
   * interfaces that are bound.
   */

  public Map fcBindings;

  public String[] listFc () {
    if (fcBindings == null) {
      return new String[0];
    }
    return (String[])fcBindings.keySet().toArray(new String[fcBindings.size()]);
  }

  public Object lookupFc (final String clientItfName)
    throws NoSuchInterfaceException
  {
    if (fcBindings == null) {
      return null;
    }
    Object serverItf = fcBindings.get(clientItfName);
    return serverItf == fcBindings ? null : serverItf;
  }

  public void bindFc (final String clientItfName, final Object serverItf) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException
  {
    if (fcBindings == null) {
      fcBindings = new HashMap();
    }
    fcBindings.put(clientItfName, serverItf);
  }

  public void unbindFc (final String clientItfName) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException
  {
    if (fcBindings != null) {
      fcBindings.put(clientItfName, fcBindings);
    }
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

}
