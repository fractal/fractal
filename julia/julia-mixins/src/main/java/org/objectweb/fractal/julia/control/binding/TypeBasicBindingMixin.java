package org.objectweb.fractal.julia.control.binding;

import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;

import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;

import java.util.HashMap;
import java.util.Map;

/**
 * Provides a basic type system based initializer to a {@link
 * org.objectweb.fractal.api.control.BindingController}.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>the type of the component to which this controller object belongs must be
 * an instance of {@link ComponentType}.</li>
 * </ul>
 */

public abstract class TypeBasicBindingMixin implements Controller {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private TypeBasicBindingMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * Initializes the {@link #_this_fcBindings} map, and then calls the overriden
   * method.
   *
   * @param ic information about the component to which this controller object
   *      belongs.
   * @throws InstantiationException if the initialization fails.
   */

  public void initFcController (final InitializationContext ic)
    throws InstantiationException
  {
    ComponentType compType = (ComponentType)ic.type;
    InterfaceType[] itfTypes = compType.getFcInterfaceTypes();
    boolean isComposite = ic.getOptionalInterface("content-controller") != null;

    // initializes the fcBindings map with empty bindings for all the
    // interfaces in the component's type
    for (int j = 0; j < itfTypes.length; j++) {
      InterfaceType itfType = itfTypes[j];
      if (!itfType.isFcCollectionItf() &&
          !itfType.getFcItfName().equals("component") &&
          !itfType.getFcItfName().endsWith("-controller") &&
          (itfType.isFcClientItf() || isComposite))
      {
        if (_this_fcBindings == null) {
          _this_fcBindings = new HashMap();
        }
        _this_fcBindings.put(itfType.getFcItfName(), _this_fcBindings);
      }
    }

    // calls the overriden method
    _super_initFcController(ic);
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>fcBindings</tt> field required by this mixin. This field is
   * supposed to store the bindings of the component to which this controller
   * object belongs.
   */

  public Map _this_fcBindings;

  /**
   * The {@link Controller#initFcController initFcController} method overriden
   * by this mixin.
   *
   * @param ic information about the component to which this controller object
   *      belongs.
   * @throws InstantiationException if the initialization fails.
   */

  public abstract void _super_initFcController (InitializationContext ic)
    throws InstantiationException;
}
