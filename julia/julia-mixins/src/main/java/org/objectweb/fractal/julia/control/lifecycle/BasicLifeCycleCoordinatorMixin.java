/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.lifecycle;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

import org.objectweb.fractal.julia.control.content.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides an abstract implementation of the {@link LifeCycleCoordinator}
 * interface.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>the component to which this controller object belongs must provide the
 * {@link Component} interface.</li>
 * </ul>
 */

public abstract class BasicLifeCycleCoordinatorMixin
  implements LifeCycleCoordinator
{

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private BasicLifeCycleCoordinatorMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * The components that are currently active.
   */

  public List fcActive;

  /**
   * Sets the lifecycle state of this component and of all its direct and
   * indirect sub components that have a {@link LifeCycleCoordinator} interface.
   *
   * @param started <tt>true</tt> to set the lifecycle state of the components
   *      to {@link #STARTED STARTED}, or <tt>false</tt> to set this state to
   *      {@link #STOPPED STOPPED}.
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  public void setFcState (final boolean started)
    throws IllegalLifeCycleException
  {
    // finds all the direct and indirect sub components of this component
    Component thisComponent;
    try {
      thisComponent = (Component)_this_weaveableC.getFcInterface("component");
    } catch (NoSuchInterfaceException e) {
      throw new ChainedIllegalLifeCycleException(
        e, _this_weaveableC, "Cannot set the lifecycle state");
    }
    List allSubComponents = Util.getAllSubComponents(thisComponent);

    // sets the state of these components
    for (int i = 0; i < allSubComponents.size(); ++i) {
      Component c = (Component)allSubComponents.get(i);
      LifeCycleCoordinator lc;
      try {
        lc = (LifeCycleCoordinator)c.getFcInterface("lifecycle-controller");
      } catch (Exception e) {
        try {
          lc = (LifeCycleCoordinator)c.getFcInterface("/lifecycle-coordinator");
        } catch (NoSuchInterfaceException f) {
          continue;
        }
      }
      if (started) {
        lc.setFcStarted();
      } else {
        lc.setFcStopped();
      }
    }
  }

  public boolean fcActivated (final LifeCycleCoordinator component) {
    synchronized (fcActive) {
      // a component can become active iff another component is already active
      if (fcActive.size() > 0) {
        if (!fcActive.contains(component)) {
          fcActive.add(component);
        }
        return true;
      }
      return false;
    }
  }

  public void fcInactivated (final LifeCycleCoordinator component) {
    synchronized (fcActive) {
      fcActive.remove(component);
      // notifies the thread that may be blocked in stopFc
      fcActive.notifyAll();
    }
  }

  /**
   * Stops the given components simultaneously. This method sets the state of
   * the components to "<tt>STOPPING</tt>", waits until all the components
   * are simultaneoulsy inactive (their state is known thanks to the {@link
   * #fcActivated fcActivated} and {@link #fcInactivated fcInactivated} callback
   * methods), and then sets the state of the components to {@link #STOPPED
   * STOPPED}.
   *
   * @param components the {@link LifeCycleCoordinator} interface of the
   *      components to be stopped.
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  public void stopFc (final LifeCycleCoordinator[] components)
    throws IllegalLifeCycleException
  {
    // initializes the fcActive list
    fcActive = new ArrayList();
    for (int i = 0; i < components.length; ++i) {
      if (components[i].getFcState().equals(STARTED)) {
        fcActive.add(components[i]);
      }
    }
    // sets the state of the components to STOPPING
    LifeCycleCoordinator c;
    try {
      c = (LifeCycleCoordinator)_this_weaveableC.
        getFcInterface("lifecycle-controller");
    } catch (Exception e) {
      try {
        c = (LifeCycleCoordinator)_this_weaveableC.
          getFcInterface("/lifecycle-coordinator");
      } catch (NoSuchInterfaceException f) {
        throw new ChainedIllegalLifeCycleException(
          f, _this_weaveableC, "Cannot stop components");
      }
    }
    for (int i = 0; i < components.length; ++i) {
      if (components[i].getFcState().equals(STARTED)) {
        components[i].setFcStopping(c);
      }
    }
    // waits until all the components are simultaneously inactive
    synchronized (fcActive) {
      while (fcActive.size() > 0) {
        try {
          fcActive.wait();
        } catch (InterruptedException e) {
        }
      }
    }
    // sets the state of the components to STOPPED
    for (int i = 0; i < components.length; ++i) {
      if (components[i].getFcState().equals(STARTED)) {
        components[i].setFcStopped();
      }
    }
    fcActive = null;
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableC</tt> field required by this mixin. This field is
   * supposed to reference the {@link Component} interface of the component to
   * which this controller object belongs.
   */

  public Component _this_weaveableC;
}
