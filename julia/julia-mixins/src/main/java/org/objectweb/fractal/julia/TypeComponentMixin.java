/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;

import java.util.Map;

/**
 * Provides basic type system related checks to a {@link Component}.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>the type of the component in which this mixin is used must be an instance
 * of {@link ComponentType}.</li>
 * </ul>
 */

public abstract class TypeComponentMixin implements Component {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private TypeComponentMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * Checks the interface name against the component's type and then calls the
   * overriden method. This method also creates the collection interfaces
   * when needed, and puts them in the {@link #_this_fcInterfaces} map.
   * 
   * @param interfaceName the name of the external interface that must be
   *      returned.
   * @return the external interface of the component to which this interface
   *      belongs, whose name is equal to the given name.
   * @throws NoSuchInterfaceException if there is no such interface.
   */

  public Object getFcInterface (final String interfaceName)
    throws NoSuchInterfaceException
  {
    if (interfaceName.indexOf('/') == 0) {
      return _super_getFcInterface(interfaceName);
    }
    ComponentType compType = (ComponentType)_this_getFcType();
    InterfaceType itfType;
    try {
      itfType = compType.getFcInterfaceType(interfaceName);
    } catch (NoSuchInterfaceException e) {
      throw new ChainedNoSuchInterfaceException(null, this, interfaceName);
    }
    Object result;
    try {
      result = _super_getFcInterface(interfaceName);
    } catch (NoSuchInterfaceException e) {
      if (itfType.isFcCollectionItf()) {
        result = _super_getFcInterface("/collection/" + itfType.getFcItfName());
        result = ((ComponentInterface)result).clone();
        ((ComponentInterface)result).setFcItfName(interfaceName);
        _this_fcInterfaces.put(interfaceName, result);
      } else {
        throw e;
      }
    }
    return result;
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>fcInterfaces</tt> field required by this mixin. This field is
   * supposed to store the interfaces of the component.
   */

  public Map _this_fcInterfaces;

  /**
   * The {@link #getFcType getFcType} method required by this mixin.
   * 
   * @return the type of the component to which this interface belongs.
   */

  public abstract Type _this_getFcType ();

  /**
   * The {@link Component#getFcInterface getFcInterface} method overriden by
   * this mixin.
   * 
   * @param interfaceName the name of the external interface that must be
   *      returned.
   * @return the external interface of the component to which this interface
   *      belongs, whose name is equal to the given name.
   * @throws NoSuchInterfaceException if there is no such interface.
   */

  public abstract Object _super_getFcInterface (String interfaceName)
    throws NoSuchInterfaceException;
}
