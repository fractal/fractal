/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.lifecycle;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;

import org.objectweb.fractal.julia.control.binding.ChainedIllegalBindingException;
import org.objectweb.fractal.julia.control.content.Util;

import java.util.List;

/**
 * Provides basic type system related checks to a {@link LifeCycleController}.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>the type of each direct and indirect sub component of this component
 * (included) that has a {@link BindingController} interface must be an instance
 * of {@link org.objectweb.fractal.api.type.ComponentType}.</li>
 * </ul>
 */

public abstract class TypeLifeCycleMixin implements LifeCycleController {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private TypeLifeCycleMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * Checks the mandatory client interfaces of the component (and of all its sub
   * components) and then calls the overriden method.
   * 
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  public void startFc () throws IllegalLifeCycleException {
    // finds all the direct and indirect sub components of this component
    Component thisComponent;
    try {
      thisComponent = (Component)_this_weaveableC.getFcInterface("component");
    } catch (NoSuchInterfaceException e) {
      throw new ChainedIllegalLifeCycleException(
        e, _this_weaveableC, "Cannot start the component");
    }
    List allSubComponents = Util.getAllSubComponents(thisComponent);

    // checks that the mandatory client interfaces of these components are bound
    for (int i = 0; i < allSubComponents.size(); ++i) {
      Component subComponent = (Component)allSubComponents.get(i);
      try {
        checkFcMandatoryInterfaces(subComponent);
      } catch (IllegalBindingException e) {
        throw new ChainedIllegalLifeCycleException(
          e, _this_weaveableC, "Cannot start the component");
      }
    }

    // calls the overriden method
    _super_startFc();
  }

  /**
   * Checks that all the mandatory client interface of the given component are
   * bound.
   *
   * @param c a component.
   * @throws IllegalBindingException if a mandatory client interface of the
   *      given component is not bound.
   */

  public void checkFcMandatoryInterfaces (final Component c)
    throws IllegalBindingException
  {
    BindingController bc;
    try {
      bc = (BindingController)c.getFcInterface("binding-controller");
    } catch (NoSuchInterfaceException e) {
      return;
    }
    ComponentType compType = (ComponentType)c.getFcType();
    String[] names = bc.listFc();
    for (int i = 0; i < names.length; ++i) {
      InterfaceType itfType;
      try {
        itfType = compType.getFcInterfaceType(names[i]);
      } catch (NoSuchInterfaceException e) {
        continue;
      }
      if (itfType.isFcClientItf() && !itfType.isFcOptionalItf()) {
        Object sItf;
        try {
          sItf = bc.lookupFc(names[i]);
        } catch (NoSuchInterfaceException e) {
          continue;
        }
        if (sItf == null) {
          throw new ChainedIllegalBindingException(
            null,
            c,
            null,
            names[i],
            null,
            "Mandatory client interface unbound");
        }
      }
    }
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableC</tt> field required by this mixin. This field is
   * supposed to reference the {@link Component} interface of the component to
   * which this controller object belongs.
   */

  public Component _this_weaveableC;

  /**
   * The {@link LifeCycleController#startFc startFc} method overriden by this
   * mixin.
   * 
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  public abstract void _super_startFc () throws IllegalLifeCycleException;
}
