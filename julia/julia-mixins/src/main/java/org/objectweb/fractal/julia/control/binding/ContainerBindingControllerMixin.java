/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.binding;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;

import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;

/**
 * Provides a container based implementation of the {@link BindingController}
 * interface. This mixin implements the {@link BindingController} methods
 * through delegation to the encapsulated "user component".
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>none.</li>
 * </ul>
 */

public abstract class ContainerBindingControllerMixin
  implements Controller, BindingController
{

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private ContainerBindingControllerMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * The "user component" encapsulated in this container component.
   */

  public Object fcContent;

  /**
   * Initializes the fields of this mixin and then calls the overriden method.
   *
   * @param ic information about the component to which this controller object
   *      belongs.
   * @throws InstantiationException if the initialization fails.
   */

  public void initFcController (final InitializationContext ic)
    throws InstantiationException
  {
    fcContent = ic.content;
    Component owner = (Component)ic.getOptionalInterface("component");
    if (owner != null) {
      try {
        owner = (Component)owner.getFcInterface("component");
        if (fcContent == this) {
          // case of merge...AndContent options
          if (this instanceof ContentBindingController) {
            ((ContentBindingController)this).bindFcContent("component", owner);
          }
        } else if (fcContent instanceof BindingController) {
          ((BindingController)fcContent).bindFc("component", owner);
        }
      } catch (Exception ignored) {
      }
    }
    _super_initFcController(ic);
  }

  public String[] listFc () {
    if (fcContent == this) {
      // case of merge...AndContent options
      if (this instanceof ContentBindingController) {
        return ((ContentBindingController)this).listFcContent();
      }
    } else if (fcContent instanceof BindingController) {
      return ((BindingController)fcContent).listFc();
    }
    return new String[0];
  }

  public Object lookupFc (final String clientItfName)
    throws NoSuchInterfaceException
  {
    if (fcContent == this) {
      // case of merge...AndContent options
      ContentBindingController cbc = (ContentBindingController)this;
      return cbc.lookupFcContent(clientItfName);
    } else {
      BindingController bc = (BindingController)fcContent;
      return bc.lookupFc(clientItfName);
    }
  }

  public void bindFc (final String clientItfName, final Object serverItf) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException
  {
    if (fcContent == this) {
      // case of merge...AndContent options
      ((ContentBindingController)this).bindFcContent(clientItfName, serverItf);
    } else {
      ((BindingController)fcContent).bindFc(clientItfName, serverItf);
    }
  }

  public void unbindFc (final String clientItfName) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException
  {
    if (fcContent == this) {
      // case of merge...AndContent options
      ((ContentBindingController)this).unbindFcContent(clientItfName);
    } else {
      ((BindingController)fcContent).unbindFc(clientItfName);
    }
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The {@link Controller#initFcController initFcController} method overriden
   * by this mixin.
   *
   * @param ic information about the component to which this controller object
   *      belongs.
   * @throws InstantiationException if the initialization fails.
   */

  public abstract void _super_initFcController (InitializationContext ic)
    throws InstantiationException;
}
