/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.factory;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.factory.InstantiationException;

/**
 * Provides a name copy function to a {@link Template}.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>none (the template component may provide a {@link NameController}
 * interface, but this is not mandatory - in this case this mixin does
 * nothing).</li>
 * </ul>
 */

public abstract class NameTemplateMixin implements Template {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private NameTemplateMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * Calls the overriden method and then sets the name of the created component.
   * The name of the created component is initialized to the name of this
   * template, if both components have a {@link NameController} interface
   * (otherwise this mixin does nothing).
   * 
   * @return the instantiated component.
   * @throws InstantiationException if the component controller cannot be
   *      instantiated.
   */

  public Component newFcControllerInstance () throws InstantiationException {
    Component comp = _super_newFcControllerInstance();
    if (_this_weaveableOptNC != null) {
      try {
        // copies the name of this template to the component, if applicable
        String name = _this_weaveableOptNC.getFcName();
        ((NameController)comp.getFcInterface("name-controller")).setFcName(name);
      } catch (NoSuchInterfaceException ignored) {
      }
    }
    return comp;
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableOptNC</tt> field required by this mixin. This field is
   * supposed to reference the {@link NameController} interface of the component
   * to which this controller object belongs.
   */

  public NameController _this_weaveableOptNC;

  /**
   * The {@link Template#newFcControllerInstance newFcControllerInstance}
   * overriden by this mixin.
   * 
   * @return the instantiated component.
   * @throws InstantiationException if the component controller cannot be
   *      instantiated.
   */

  public abstract Component _super_newFcControllerInstance () 
    throws InstantiationException;
}
