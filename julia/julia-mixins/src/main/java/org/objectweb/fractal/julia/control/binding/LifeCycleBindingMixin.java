/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.binding;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;

import org.objectweb.fractal.julia.control.lifecycle.ChainedIllegalLifeCycleException;

/**
 * Provides life cycle related checks to a {@link BindingController}.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>none (the component may provide a {@link LifeCycleController} interface,
 * but this is not mandatory - in this case this binding does nothing).</li>
 * </ul>
 */

public abstract class LifeCycleBindingMixin implements BindingController {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private LifeCycleBindingMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * Checks that the component is stopped and then calls the overriden method.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be removed.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public void unbindFc (final String clientItfName) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException
  {
    if (_this_weaveableOptLC != null) {
      String state = _this_weaveableOptLC.getFcState();
      if (!LifeCycleController.STOPPED.equals(state)) {
        throw new ChainedIllegalLifeCycleException(
          null, _this_weaveableOptC, "The component is not stopped");
      }
    }
    _super_unbindFc(clientItfName);
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableOptC</tt> field required by this mixin. This field is
   * supposed to reference the {@link Component} interface of the component to
   * which this controller object belongs.
   */

  public Component _this_weaveableOptC;

  /**
   * The <tt>weaveableOptLC</tt> field required by this mixin. This field is
   * supposed to reference the {@link LifeCycleController} interface of the
   * component to which this controller object belongs.
   */

  public LifeCycleController _this_weaveableOptLC;

  /**
   * The {@link BindingController#unbindFc unbindFc} method overriden by this
   * mixin.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be removed.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public abstract void _super_unbindFc (String clientItfName) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException;
}
