/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.content;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;

import org.objectweb.fractal.julia.ChainedNoSuchInterfaceException;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Provides a basic implementation of the {@link ContentController} interface.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>none.</li>
 * </ul>
 */

public abstract class BasicContentControllerMixin
  implements Controller, ContentController
{

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private BasicContentControllerMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * The internal interfaces of the component to which this controller object
   * belongs.
   */

  public Map fcInternalInterfaces;

  /**
   * The sub components of the component to which this controller object
   * belongs.
   */

  public List fcSubComponents;

  /**
   * Initializes the fields of this mixin and then calls the overriden method.
   *
   * @param ic information about the component to which this controller object
   *      belongs.
   * @throws InstantiationException if the initialization fails.
   */

  public void initFcController (final InitializationContext ic)
    throws InstantiationException
  {
    fcInternalInterfaces = ic.internalInterfaces;
    _super_initFcController(ic);
  }

  public Object[] getFcInternalInterfaces () {
    if (fcInternalInterfaces == null) {
      return new Object[0];
    }
    // returns the names of all the public interfaces in fcInternalInterfaces
    // interfaces whose name begins with '/' are private
    int size = 0;
    String[] names = new String[fcInternalInterfaces.size()];
    names = (String[])fcInternalInterfaces.keySet().toArray(names);
    for (int i = 0; i < names.length; ++i) {
      if (names[i].charAt(0) != '/') {
        ++size;
      }
    }
    int index = 0;
    Object[] itfs = new Object[size];
    for (int i = 0; i < names.length; ++i) {
      if (names[i].charAt(0) != '/') {
        itfs[index++] = fcInternalInterfaces.get(names[i]);
      }
    }
    return itfs;
  }

  public Object getFcInternalInterface (final String interfaceName)
    throws NoSuchInterfaceException
  {
    Object itf;
    if (fcInternalInterfaces == null) {
      itf = null;
    } else {
      itf = fcInternalInterfaces.get(interfaceName);
    }
    if (itf == null) {
      throw new ChainedNoSuchInterfaceException(
        null, _this_weaveableOptC, interfaceName);
    }
    return itf;
  }

  public Component[] getFcSubComponents () {
    if (fcSubComponents == null) {
      return new Component[0];
    }
    return (Component[])fcSubComponents.toArray(
      new Component[fcSubComponents.size()]);
  }

  public void addFcSubComponent (final Component subComponent)
    throws IllegalContentException, IllegalLifeCycleException
  {
    if (fcSubComponents == null) {
      fcSubComponents = new ArrayList();
    }
    fcSubComponents.add(subComponent);
  }

  public void removeFcSubComponent (final Component subComponent)
    throws IllegalContentException, IllegalLifeCycleException
  {
    if (fcSubComponents != null) {
      fcSubComponents.remove(subComponent);
    }
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableOptC</tt> field required by this mixin. This field is
   * supposed to reference the {@link Component} interface of the component to
   * which this controller object belongs.
   */

  public Component _this_weaveableOptC;

  /**
   * The {@link Controller#initFcController initFcController} method overriden
   * by this mixin.
   *
   * @param ic information about the component to which this controller object
   *      belongs.
   * @throws InstantiationException if the initialization fails.
   */

  public abstract void _super_initFcController (InitializationContext ic)
    throws InstantiationException;
}
