package org.objectweb.fractal.julia.factory;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.InstantiationException;

/**
 * Provides a singleton behavior to a {@link Template}.
 * <b>A singleton composite template must only contain other singleton
 * templates, otherwise the "singleton" semantics will not be ensured.</b>
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>none.</li>
 * </ul>
 */

public abstract class SingletonTemplateMixin implements Template {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private SingletonTemplateMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * The singleton instance created by this template.
   */

  public Component fcInstance;

  /**
   * Calls the overriden method only if the {@link #fcInstance} field is
   * <tt>null</tt>.
   * 
   * @return the instantiated component.
   * @throws InstantiationException if the component controller cannot be
   *      instantiated.
   */

  public Component newFcControllerInstance () throws InstantiationException {
    if (fcInstance == null) {
      fcInstance = _super_newFcControllerInstance();
    }
    return fcInstance;
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The {@link Template#newFcControllerInstance newFcControllerInstance}
   * overriden by this mixin.
   * 
   * @return the instantiated component.
   * @throws InstantiationException if the component controller cannot be
   *      instantiated.
   */

  public abstract Component _super_newFcControllerInstance () 
    throws InstantiationException;
}
