/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.content;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

import java.util.List;

/**
 * Provides basic checks to a {@link ContentController}.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>the component to which this controller object belongs must provide the
 * {@link Component} interface.</li>
 * </ul>
 */

public abstract class CheckContentMixin implements ContentController {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private CheckContentMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * Checks that the given component is not already a sub component, and then
   * calls the overriden method. This method also checks that the addition of
   * this sub component will not create a cycle in the component hierarchy.
   *
   * @param subComponent the component to be added inside this component.
   * @throws IllegalContentException if the given component cannot be added
   *      inside this component.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public void addFcSubComponent (final Component subComponent)
    throws IllegalContentException, IllegalLifeCycleException
  {
    if (containsFcSubComponent(subComponent)) {
      throw new ChainedIllegalContentException(
        null, _this_weaveableC, subComponent, "Already a sub component");
    }

    // gets the Component interface of this component,
    // and checks that it is not equal to 'subComponent'
    Component thisComponent;
    try {
      thisComponent = (Component)_this_weaveableC.getFcInterface("component");
    } catch (NoSuchInterfaceException e) {
      throw new ChainedIllegalContentException(
        e, _this_weaveableC, subComponent, "Cannot check this operation");
    }
    if (subComponent.equals(thisComponent)) {
      throw new ChainedIllegalContentException(
        null,
        _this_weaveableC,
        subComponent,
        "A component cannot be a sub component of itself");
    }

    // finds all the direct and indirect sub components of 'subComponent' and,
    // for each sub component checks that it is not equal to 'thisComponent'
    List allSubComponents = Util.getAllSubComponents(subComponent);
    for (int i = 0; i < allSubComponents.size(); ++i) {
      if (allSubComponents.get(i).equals(thisComponent)) {
        throw new ChainedIllegalContentException(
          null,
          _this_weaveableC,
          subComponent,
          "Would create a cycle in the component hierarchy");
      }
    }

    // calls the overriden method
    _super_addFcSubComponent(subComponent);
  }

  /**
   * Checks that the given component is really a sub component, and then
   * calls the overriden method.
   *
   * @param subComponent the component to be removed from this component.
   * @throws IllegalContentException if the given component cannot be removed
   *      from this component.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public void removeFcSubComponent (final Component subComponent)
    throws IllegalContentException, IllegalLifeCycleException
  {
    if (!containsFcSubComponent(subComponent)) {
      throw new ChainedIllegalContentException(
        null, _this_weaveableC, subComponent, "Not a sub component");
    }
    _super_removeFcSubComponent(subComponent);
  }

  /**
   * Tests if this component contains the given sub component.
   *
   * @param subComponent a component.
   * @return <tt>true</tt> if this component contains the given sub component,
   *      or <tt>false</tt> otherwise.
   */

  public boolean containsFcSubComponent (final Component subComponent) {
    Component[] subComponents = _this_getFcSubComponents();
    for (int i = 0; i < subComponents.length; ++i) {
      if (subComponents[i].equals(subComponent)) {
        return true;
      }
    }
    return false;
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableC</tt> field required by this mixin. This field is
   * supposed to reference the {@link Component} interface of the component to
   * which this controller object belongs.
   */

  public Component _this_weaveableC;

  /**
   * The {@link ContentController#getFcSubComponents getFcSubComponents} method
   * required by this mixin.
   *
   * @return the {@link Component} interfaces of the sub-components of the
   *      component to which this interface belongs.
   */

  public abstract Component[] _this_getFcSubComponents ();

  /**
   * The {@link ContentController#addFcSubComponent addFcSubComponent} method
   * overriden by this mixin.
   *
   * @param subComponent the component to be added inside this component.
   * @throws IllegalContentException if the given component cannot be added
   *      inside this component.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public abstract void _super_addFcSubComponent (Component subComponent)
    throws IllegalContentException, IllegalLifeCycleException;

  /**
   * The {@link ContentController#removeFcSubComponent removeFcSubComponent}
   * method overriden by this mixin.
   *
   * @param subComponent the component to be removed from this component.
   * @throws IllegalContentException if the given component cannot be removed
   *      from this component.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  public abstract void _super_removeFcSubComponent (Component subComponent)
    throws IllegalContentException, IllegalLifeCycleException;
}
