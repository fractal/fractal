/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia

import org.objectweb.fractal.api.Component
import org.objectweb.fractal.api.NoSuchInterfaceException
import org.objectweb.fractal.api.Type
import org.objectweb.fractal.api.factory.InstantiationException

/**
 * Trait providing a basic implementation of the {@link Component} interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait BasicComponentTrait extends BasicControllerTrait with ComponentTrait {
  
    /** The type of this component. */
	private var fcType: Type = null

    /**
     * The interfaces of this component. This map associates each interface to
     * its name.
     * 
     * I'm using here java.util.Map instead of Scala Map since fcInterfaces is
     * initialized in initFcController from InitializationContext.interfaces
     * which is a java.util.Map (instantiated with a java.util.HashMap.) An
     * alternative solution would be to convert from java.util.Map to Scala Map
     * but this would lead to duplicated data.
     */
    protected var fcInterfaces: java.util.Map[String,Object] = null

    /**
     * Initializes the fields of this mixin from the given context, and then
     * calls the overriden method.
     * 
     * @param ic  information about the component to which this controller
     *            object belongs
     * @throws InstantiationException   if the initialization fails
     */
    override def initFcController( ic: InitializationContext ) = {
        this.fcType = ic.`type`
        this.fcInterfaces = ic.interfaces.asInstanceOf[java.util.Map[String,Object]]
        super.initFcController(ic)
    }

    override def getFcType : Type = { return fcType }

    override def getFcInterfaces : Array[Object] = {

      if( fcInterfaces == null ) {
	      return new Array[Object](0);
	    }
	  
        // returns the names of all the public interfaces in fcInterfaces
	    // interfaces whose name begins with '/' are private
	    var size = 0
	    val names = fcInterfaces.keySet.toArray(new Array[String](fcInterfaces.size))
	    for( name <- names ) {
	        if ( name.charAt(0) != '/' ) {
	            size += 1
	        }
	    }
	    var index = 0
	    val itfs = new Array[Object](size)
	    for( name <- names ) {
	        if ( name.charAt(0) != '/' ) {
	        	itfs(index) = fcInterfaces.get(name)
	        	index += 1
	        }
	    }
	    return itfs;
    }

    override def getFcInterface( interfaceName: String ) : Object = {
	    val itf = if(fcInterfaces==null) null else fcInterfaces.get(interfaceName)
	    if( itf == null ) {
	        throw new ChainedNoSuchInterfaceException(null,this,interfaceName)
	    }
	    return itf;
    }
}
