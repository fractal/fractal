/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.binding;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.Interceptor;

/**
 * Helper class for computing binding shortcuts for composite components. This
 * class is reused from Julia and is used before being translated into Scala in
 * {@link OptimizedCompositeBindingTrait}.
 * 
 * TODO translate into Scala and integrate into OptimizedCompositeBindingTrait
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
public class OptimizedCompositeBindingHelper {

	public OptimizedCompositeBindingHelper(
		Component _this_weaveableC, ContentController _this_weaveableCC ) {
		
		this._this_weaveableC = _this_weaveableC;
		this._this_weaveableCC = _this_weaveableCC;
	}

  // -------------------------------------------------------------------------
  // Utility methods: algorithm to compute of shortcut links
  // -------------------------------------------------------------------------

  /**
   * Creates the given binding and updates the shortcuts links accordingly.
   *
   * @param clientItfType the type of the 'clientItfName' interface. 
   * @param clientItfName a client interface name.
   * @param serverItf the server interface to which the client interface must
   *      be bound, or <tt>null</tt> if it must be unbound.
   * @throws NoSuchInterfaceException if there is no client interface whose name
   *      is equal to 'clientItfName'.
   * @throws IllegalBindingException if a problem occurs.
   */

  public void setFcShortcuts (
    final InterfaceType clientItfType,
    final String clientItfName,
    final Object serverItf)
    throws
    NoSuchInterfaceException,
    IllegalBindingException
  {
    Interface clientItf;
    if (clientItfType.isFcClientItf()) {
      clientItf =
        (Interface)_this_weaveableC.getFcInterface(clientItfName);
    } else {
      clientItf =
        (Interface)_this_weaveableCC.getFcInternalInterface(clientItfName);
    }

    Set<Interface> visited = new HashSet<Interface>();
    List<Interface> shortcutItfs = new ArrayList<Interface>();
    List<Object> shortcutImpls = new ArrayList<Object>();

    // 'forward' step
    Object itf = serverItf;
    while (true) {
      ComponentInterface citf;
      if (itf instanceof ComponentInterface) {
        citf = (ComponentInterface)itf;
      } else {
        break;
      }
      if (citf.getFcItfImpl() instanceof Interceptor) {
        break;
      }
      try {
        Component owner = citf.getFcItfOwner();
        owner.getFcInterface("content-controller");
        BindingController bc =
          (BindingController)owner.getFcInterface("binding-controller");
        itf = bc.lookupFc(citf.getFcItfName());
      } catch (NoSuchInterfaceException e) {
        break;
      }
    }
    Object impl;
    if (itf == null) {
      impl = null;
    } else {
      if (itf instanceof ComponentInterface) {
        impl = ((ComponentInterface)itf).getFcItfImpl();
      } else {
        // cannot create complete shortcut: uses itf as 'impl'
        impl = itf;
      }
    }

    // 'backward' step : creates the shortcuts
    try {
      setFcClientShortcuts(clientItf, impl, visited, shortcutItfs, shortcutImpls);
    } catch (Exception e) {
      Interface sItf;
      sItf = serverItf instanceof Interface ? (Interface)serverItf : null;
      throw new ChainedIllegalBindingException(
        e,
        clientItf.getFcItfOwner(),
        sItf == null ? null : sItf.getFcItfOwner(),
        clientItf.getFcItfName(),
        sItf == null ? null : sItf.getFcItfName(),
        "Cannot create shortcut links");
    }

    // sets the shortcuts
    for (int i = 0; i < shortcutItfs.size(); ++i) {
      Interface shortcutItf = (Interface)shortcutItfs.get(i);
      Object shortcutImpl = shortcutImpls.get(i);
      Component c = shortcutItf.getFcItfOwner();
      ContentController cc;
      try {
        cc = (ContentController)c.getFcInterface("content-controller");
      } catch (NoSuchInterfaceException e) {
        cc = null;
      }

      if (cc != null) {
        ComponentInterface cShortcutItf = (ComponentInterface)shortcutItf;
        if (cShortcutItf.hasFcInterceptor()) {
          Object newImpl = cShortcutItf.getFcItfImpl();
          ((Interceptor)newImpl).setFcItfDelegate(shortcutImpl);
        } else {
          cShortcutItf.setFcItfImpl(shortcutImpl);
        }
      } else {
        BindingController bc;
        try {
          bc = (BindingController)c.getFcInterface("binding-controller");
        } catch (NoSuchInterfaceException e) {
          continue;
        }
        try {
          String itfName = shortcutItf.getFcItfName();
          Object o = bc.lookupFc(itfName);
          // following code not needed if refresh done in lookup
          if (o != null) {
            bc.unbindFc(itfName);
            bc.bindFc(itfName, o);
          }
        } catch (Exception e) {
          Interface sItf;
          sItf = serverItf instanceof Interface ? (Interface)serverItf : null;
          throw new ChainedIllegalBindingException(
            e,
            clientItf.getFcItfOwner(),
            sItf == null ? null : sItf.getFcItfOwner(),
            clientItf.getFcItfName(),
            sItf == null ? null : sItf.getFcItfName(),
            "Cannot create shortcut links");
        }
      }
    }
  }

  /**
   * Finds the shortcut links to be updated by following the bindings backwards
   * from the given client interface.
   *
   * @param clientItf the client interface from which the bindings must be
   *      followed backward.
   * @param impl the object to be used to update the shortcut links with {@link
   *      ComponentInterface#getFcItfImpl getFcItfImpl}.
   * @param visited the already visited interfaces.
   * @param shortcutItfs the interfaces whose shortcut links must be updated.
   * @param shortcutImpls the values to be used to update the shortcut links.
   * @throws Exception if a problem occurs.
   */

  public void setFcClientShortcuts (
    final Interface clientItf,
    final Object impl,
    final Set<Interface> visited,
    final List<Interface> shortcutItfs,
    final List<Object> shortcutImpls) throws Exception
  {
    Component clientComp = clientItf.getFcItfOwner();
    ContentController cc;
    try {
      cc = (ContentController)clientComp.getFcInterface("content-controller");
    } catch (NoSuchInterfaceException e) {
      cc = null;
    }

    if (cc != null) {
      ComponentInterface itf;
      try {
        String name = clientItf.getFcItfName();
        if (clientItf.isFcInternalItf()) {
          // clientItf is an internal client interface: continue to follow the
          // bindings backwards from the corresponding external server interface
          itf = (ComponentInterface)clientComp.getFcInterface(name);
        } else {
          // clientItf is an external client interface: continue to follow the
          // bindings backwards from the corresponding internal server interface
          itf = (ComponentInterface)cc.getFcInternalInterface(name);
        }
      } catch (NoSuchInterfaceException e) {
        throw new Exception("Cannot create shortcut links", e);
      } catch (ClassCastException e) {
    	// Thrown when clientItf was not created by Julia but by another
    	// Fractal implementation that does not support ComponentInterface.
    	// Then no shortcut link can be created.
        itf = null;
      }

      if (itf != null && !visited.contains(itf)) {
        visited.add(itf);
        shortcutItfs.add(itf);
        shortcutImpls.add(impl);
        if (itf.hasFcInterceptor()) {
          Object newImpl = itf.getFcItfImpl();
          setFcServerShortcuts(itf, newImpl, visited, shortcutItfs, shortcutImpls);
        } else {
          setFcServerShortcuts(itf, impl, visited, shortcutItfs, shortcutImpls);
        }
      }
    } else if (!visited.contains(clientItf)) {
      visited.add(clientItf);
      shortcutItfs.add(clientItf);
      shortcutImpls.add(impl);
    }
  }

  /**
   * Finds the shortcut links to be updated by followng the bindings backwards
   * from the given server interface.
   *
   * @param serverItf the server interface from which the bindings must be
   *      followed backward.
   * @param impl the object to be used to update the shortcut links with {@link
   *      ComponentInterface#getFcItfImpl getFcItfImpl}.
   * @param visited the already visited interfaces.
   * @param shortcutItfs the interfaces whose shortcut links must be updated.
   * @param shortcutImpls the values to be used to update the shortcut links.
   * @throws Exception if a problem occurs.
   */

  public void setFcServerShortcuts (
    final Interface serverItf,
    final Object impl,
    final Set<Interface> visited,
    final List<Interface> shortcutItfs,
    final List<Object> shortcutImpls) throws Exception
  {
    Object[] comps = Util.getFcPotentialClientsOf(serverItf).toArray();
    for (int i = 0; i < comps.length; ++i) {
      Component comp = (Component)comps[i];
      List<Interface> clientItfs = Util.getFcClientItfsBoundTo(comp,serverItf);
      for (int j = 0; j < clientItfs.size(); ++j) {
        setFcClientShortcuts(
          clientItfs.get(j),
          impl,
          visited,
          shortcutItfs,
          shortcutImpls);
      }
    }
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  private Component _this_weaveableC;
  private ContentController _this_weaveableCC;

}
