/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.factory

import org.objectweb.fractal.api.Type
import org.objectweb.fractal.julia.BasicControllerTrait
import org.objectweb.fractal.julia.InitializationContext
import org.objectweb.fractal.api.`type`.ComponentType
import org.objectweb.fractal.api.`type`.InterfaceType
import org.objectweb.fractal.julia.`type`.BasicComponentType
import org.objectweb.fractal.api.Component
import org.objectweb.fractal.julia.UseComponentTrait
import org.objectweb.fractal.julia.control.content.Util
import scala.collection.mutable.HashMap
import org.objectweb.fractal.api.NoSuchInterfaceException
import org.objectweb.fractal.api.control.ContentController
import org.objectweb.fractal.api.control.BindingController
import org.objectweb.fractal.api.Interface
import org.objectweb.fractal.api.Fractal
import org.objectweb.fractal.api.factory.GenericFactory
import org.objectweb.fractal.julia.`type`.UseTypeFactoryTrait
import org.objectweb.fractal.julia.loader.UseLoaderTrait
import org.objectweb.fractal.julia.loader.Tree
import org.objectweb.fractal.julia.Controller

/**
 * Trait providing a basic implementation of the {@link GenericFactory}
 * interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait BasicGenericFactoryTrait extends BasicControllerTrait
with AbstractGenericFactoryTrait with UseLoaderTrait with UseTypeFactoryTrait
with GenericFactory {
  
    override def newFcInstance(
      typ: Type, controllerDesc: Object, contentDesc: Object ) : Component = {
      
    return newComponent(
    	     getInitializationContextClass(typ,controllerDesc,contentDesc),
    		 contentDesc )
    }

    /**
     * Get the initialization context class associated to a given
     * {@link Type} instance, controller and content descriptors.
     *
     * @param type the given {@link Type} instance.
     * @param controllerDesc the given controller descriptor.
     * @param contentDesc the given content descriptor.
     * @return the initialization content class.
     * @throws InstantiationException when problems obscur.
     */
    private def getInitializationContextClass(
      typ: Type, controllerDesc: Object, contentDesc: Object ) : Class[_] = {

      var loader: Object = null
      var controller: Object = null
      
      if( controllerDesc.isInstanceOf[Array[Object]] ) {
          loader = getFcLoader( controllerDesc.asInstanceOf[Array[Object]](0) )
          controller = controllerDesc.asInstanceOf[Array[Object]](1)
      }
      else {
          loader = getFcLoader(null)
          controller = controllerDesc
      }
      
      // generates the component descriptor
      val typeTree = getFcTypeDescriptor(typ)
      val controllerTree = getFcControllerDescriptor(controller)
      val contentTree = getFcContentDescriptor(contentDesc)
      val controllerDescTree = new Tree(controller.asInstanceOf[String])
      val t = new Array[Tree](2)
      t(0) = controllerTree
      t(1) = controllerDescTree
      val controllerWithDescTree = new Tree(t)
      val t2 = new Array[Tree](4)
      t2(0) = new Tree("org.objectweb.fractal.julia.asm.ContextClassGenerator")
      t2(1) = typeTree
      t2(2) = controllerWithDescTree
      t2(3) = contentTree
      val componentTree = new Tree(t2)

      // creates an instance of the corresponding class
      // (a specific InitializationContext class for components of this kind)
      try {
    	  return weaveableL.loadClass(componentTree,loader)
      }
      catch {
        case e: Exception =>
	      throw new ChainedInstantiationException(
	        e, null,
	        "Cannot load the specific InitializationContext sub class " +
	        "needed to create the component")
      }
    }

    /**
     * Create a new {@link Component} instance for a given
     * initialization context class and content descriptor.
     *
     * @param initializationContextClass the given initialization context class.
     * @param contentDesc the given content descriptor.
     * @return the new {@link Component} instance.
     * @throws InstantiationException when problems obscur.
     */
    private def newComponent(
      initializationContextClass: Class[_], contentDesc: Object ) : Component = {
      
        val initializationContext =
		    initializationContextClass.newInstance.
		    asInstanceOf[InitializationContext]
      
        // initializes the initialization context itself
        if ( !(contentDesc.isInstanceOf[String]) ) {
    	    initializationContext.content = contentDesc
        }
        initializationContext.hints = weaveableTF
        initializationContext.create

        // initializes all the controller objects of the component
        val controllers = initializationContext.controllers
        for( i <- 0 until controllers.size ) {
    	    val o = controllers.get(i)
    	    if( o.isInstanceOf[Controller] ) {
    		    o.asInstanceOf[Controller].initFcController(initializationContext)
    	    }
        }

        // returns the Component interface of the component
        val c = initializationContext.getInterface("component").
        			asInstanceOf[Component]
        return c.getFcInterface("component").asInstanceOf[Component]
    }

    override def getFcLoader( loader: Object ) : Object = return loader
    
    /**
     * Returns a tree representing the given component type. This tree has the
     * format specified in {@link
     * org.objectweb.fractal.julia.asm.ContextClassGenerator}
     *
     * @param type a component type, must be instance of {@link ComponentType}.
     * @return a tree representing the given component type.
     * @throws InstantiationException if the given type is not an instance of
     *      {@link ComponentType}, or if it contains control interface types.
     */
    private def getFcTypeDescriptor( typ: Type ) : Tree = {
      
    	// checks the type system
        var compType: ComponentType = null
        if( typ.isInstanceOf[ComponentType] ) {
        	compType = typ.asInstanceOf[ComponentType]
        }
        else {
        	throw new ChainedInstantiationException(
        			new ClassCastException(),null,
        			"The component type must be an instance of ComponentType")
        }

        // checks the component type
        val itfTypes = compType.getFcInterfaceTypes
        val itfTrees = new Array[Tree](itfTypes.length)
        for( i <- 0 until itfTypes.length ) {
            val itfType = itfTypes(i)
            val itfName = itfType.getFcItfName
  	        if( !itfName.equals("attribute-controller") ) {
  	        	if( itfName.equals("component") ||
  	        	    itfName.endsWith("-controller") ) {
  	        		throw new ChainedInstantiationException(
  	        				null,  null,
  	        				"The component type must not contain control interface types")
	            }
	        }
            val t = new Array[Tree](5)
            t(0) = new Tree(itfName)
            t(1) = new Tree(itfType.getFcItfSignature)
            t(2) = new Tree(if(itfType.isFcClientItf()) "true" else "false")
            t(3) = new Tree(if(itfType.isFcOptionalItf()) "true" else "false")
            t(4) = new Tree(if(itfType.isFcCollectionItf()) "true" else "false")
            itfTrees(i) = new Tree(t)
        }

        // returns the type descriptor
        return new Tree(itfTrees)
    }

    /**
     * Returns the tree corresponding to the given controller descriptor. This
     * tree is found by using the {@link #_this_weaveableL} loader. It must have
     * the format specified in {@link
     * org.objectweb.fractal.julia.asm.ContextClassGenerator}
     *
     * @param controllerDesc a string referencing a true controller descriptor.
     * @return the tree corresponding to the given controller descriptor.
     * @throws InstantiationException if the tree cannot be loaded.
     */
    private def getFcControllerDescriptor( controllerDesc: Object ) : Tree = {
        val definitions: java.util.Map[String,Tree] =
            new java.util.HashMap[String,Tree]
        definitions.put("attributeControllerInterface", new Tree("QUOTE"))
        definitions.put("interfaceName", new Tree("QUOTE"))
        try {
        	return weaveableL.evalTree(
        			weaveableL.loadTree(controllerDesc.asInstanceOf[String]),
        			definitions )
        }
        catch {
          case e: Exception =>
	        throw new ChainedInstantiationException(
	        		e, null,
	        		"Cannot load the '"+controllerDesc+"' controller descriptor")
        }
    }

    /**
     * Returns the tree corresponding to the given content descriptor. This tree
     * has the format specified in {@link
     * org.objectweb.fractal.julia.asm.ContextClassGenerator}
     *
     * @param contentDesc a content descriptor.
     * @return the tree corresponding to the given content descriptor.
     */
    private def getFcContentDescriptor( contentDesc: Object ) : Tree = {
        if( contentDesc.isInstanceOf[String] ) {
            return new Tree(contentDesc.asInstanceOf[String])
        }
        else if( contentDesc != null &&
        		 !(contentDesc.isInstanceOf[Array[Object]]) ) {
        	return new Tree(contentDesc.getClass.getName)
        }
        else {
        	return new Tree("EMPTY")
        }
    }
}
