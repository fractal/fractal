/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.content

import java.lang.Object

import org.objectweb.fractal.api.Component
import org.objectweb.fractal.julia.BasicControllerTrait
import org.objectweb.fractal.julia.ChainedNoSuchInterfaceException
import org.objectweb.fractal.julia.InitializationContext
import org.objectweb.fractal.julia.UseComponentTrait

/**
 * Trait implementing the functionalities of the {@link ContentController}
 * interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait BasicContentControllerTrait extends BasicControllerTrait
with UseComponentTrait with AbstractContentControllerTrait {

    /**
     * The internal interfaces of the component to which this controller object
     * belongs.
     */
    protected var fcInternalInterfaces: java.util.Map[String,Object] = null

    /**
     * The sub components of the component to which this controller object
     * belongs.
     */
    private var fcSubComponents: List[Component] = null

    
    // -------------------------------------------------------------------------
    // Implementation of the Controller interface
    // -------------------------------------------------------------------------
    
    override def initFcController( ic: InitializationContext ) = {
    	fcInternalInterfaces =
    	    ic.internalInterfaces.asInstanceOf[java.util.Map[String,Object]]
        super.initFcController(ic)
    }

    
    // -------------------------------------------------------------------------
    // Implementation of the ContentController interface
    // -------------------------------------------------------------------------
    
    override def getFcInternalInterfaces : Array[Object] = {
        
        if( fcInternalInterfaces == null ) {
            return new Array[Object](0)
        }
        
        // returns the names of all the public interfaces in fcInternalInterfaces
        // interfaces whose name begins with '/' are private
        var size: Integer = 0
        var names = new Array[String](fcInternalInterfaces.size)
        names = fcInternalInterfaces.keySet.toArray(names)
        names.foreach( name => {
            if( name.charAt(0) != '/' ) {
                size += 1
            }
        })
        
        var index: Integer = 0
        val itfs = new Array[Object](size)
        names.foreach( name => {
            if( name.charAt(0) != '/' ) {
              itfs(index) = fcInternalInterfaces.get(name)
                index += 1
            }
        })
        
        return itfs
    }

    override def getFcInternalInterface( interfaceName: String ) : Object = {
        var itf: Object = null
        if( fcInternalInterfaces != null ) {
           itf = fcInternalInterfaces.get(interfaceName)
        }
        if (itf == null) {
            throw new ChainedNoSuchInterfaceException(null,weaveableC,interfaceName)
        }
        return itf
    }

    override def getFcSubComponents: Array[Component] = {
        if( fcSubComponents == null ) {
            return new Array[Component](0)
        }
        return fcSubComponents.toArray
    }
    
    override def addFcSubComponent( subComponent: Component) = {
        if( fcSubComponents == null ) {
            fcSubComponents = Nil
        }
        fcSubComponents =  fcSubComponents :+ subComponent
    }

    override def removeFcSubComponent( subComponent: Component ) = {
        if( fcSubComponents != null ) {
            fcSubComponents =
              fcSubComponents.filterNot( s => s == subComponent )
        }
    }
}
