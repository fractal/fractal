/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.binding

import java.lang.Object

import org.objectweb.fractal.api.control.BindingController
import org.objectweb.fractal.api.Component
import org.objectweb.fractal.julia.BasicControllerTrait
import org.objectweb.fractal.julia.InitializationContext

/**
 * Trait implementing a container based implementation of the {@link
 * BindingController}. This trait implements the {@link BindingController} 
 * methods through delegation to the encapsulated "user component"
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait BasicContainerBindingControllerTrait extends BasicControllerTrait
with AbstractBindingControllerTrait {

    /**
     * The "user component" encapsulated in this container component.
     */
    private var fcContent: Object = null
    
    // -------------------------------------------------------------------------
    // Implementation of the Controller interface
    // -------------------------------------------------------------------------
    
    override def initFcController( ic: InitializationContext ) {
        fcContent = ic.content
        var owner = ic.getOptionalInterface("component").asInstanceOf[Component]
        if( owner != null ) {
            try {
                owner = owner.getFcInterface("component").asInstanceOf[Component]
                if( fcContent == this ) {
		            // case of merge...AndContent options
		            if( this.isInstanceOf[ContentBindingController]) {
		            	this.asInstanceOf[ContentBindingController].
		            		bindFcContent("component",owner)
		            }                  
                }
                else {
                    if( fcContent.isInstanceOf[BindingController] ) {
                        fcContent.asInstanceOf[BindingController].
                        	bindFc("component",owner)
                    }
                }
            }
            catch {
              case ignored: Exception => // Indeed nothing
            }
        }
        super.initFcController(ic)
    }
    
    // -------------------------------------------------------------------------
    // Implementation of the BindingController interface
    // -------------------------------------------------------------------------

    override def listFc : Array[String] = {
        if( fcContent == this ) {
            // case of merge...AndContent options
            if( this.isInstanceOf[ContentBindingController] ) {
            	val cbc = this.asInstanceOf[ContentBindingController]
            	return cbc.listFcContent
            }                  
        }
        else {
            if( fcContent.isInstanceOf[BindingController] ) {
                val bc = fcContent.asInstanceOf[BindingController]
                return bc.listFc
            }
        }
        return new Array[String](0)
    }
    
    override def lookupFc( clientItfName: String ) : Object = {
        if( fcContent == this ) {
            // case of merge...AndContent options
        	val cbc = this.asInstanceOf[ContentBindingController]
        	return cbc.lookupFcContent(clientItfName)
        }
        else {
            val bc = fcContent.asInstanceOf[BindingController]
            return bc.lookupFc(clientItfName)
        }
    }
    
    override def bindFc( clientItfName: String, serverItf: Object ) = {
        if( fcContent == this ) {
            // case of merge...AndContent options
        	val cbc = this.asInstanceOf[ContentBindingController]
        	cbc.bindFcContent(clientItfName,serverItf)
        }
        else {
            val bc = fcContent.asInstanceOf[BindingController]
            bc.bindFc(clientItfName,serverItf)
        }
    }
    
    override def unbindFc( clientItfName: String ) = {
        if( fcContent == this ) {
            // case of merge...AndContent options
        	val cbc = this.asInstanceOf[ContentBindingController]
        	cbc.unbindFcContent(clientItfName)
        }
        else {
            val bc = fcContent.asInstanceOf[BindingController]
            bc.unbindFc(clientItfName)
        }
    }
}
