/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.binding

import org.objectweb.fractal.julia.BasicControllerTrait
import org.objectweb.fractal.julia.UseComponentTrait
import org.objectweb.fractal.julia.InitializationContext
import org.objectweb.fractal.api.`type`.InterfaceType
import org.objectweb.fractal.api.NoSuchInterfaceException
import org.objectweb.fractal.julia.ChainedNoSuchInterfaceException
import org.objectweb.fractal.api.`type`.ComponentType
import org.objectweb.fractal.api.control.ContentController
import org.objectweb.fractal.api.Interface

/**
 * Basic type system related checks for the {@link BindingController}.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait TypeBindingTrait extends AbstractBindingControllerTrait
with UseComponentTrait {

    /**
     * Checks the interface name with the component's type and then calls the
     * {@link #lookupFc(InterfaceType,String) lookupFc} method.
     *
     * @param clientItfName the name of a client interface of the component to
     *      which this interface belongs.
     * @return the server interface to which the given interface is bound, or <tt>
     *      null</tt> if it is not bound.
     * @throws NoSuchInterfaceException if the component to which this interface
     *      belongs does not have a client interface whose name is equal to the
     *      given name.
     */
	override def lookupFc( clientItfName: String ) : Object = {
	    val compType = weaveableC.getFcType.asInstanceOf[ComponentType]
	    val clientItfType = compType.getFcInterfaceType(clientItfName)
	    checkFcClientInterface(clientItfType)
	    return lookupFc(clientItfType,clientItfName)
	}
	
    /**
     * Checks the interface name with the component's type and then calls the
     * {@link #bindFc(InterfaceType,String,Object) bindFc} method. If the server
     * interface implements {@link Interface}, and if its type is an instance of
     * {@link InterfaceType}, this method also checks the compatibility between
     * the client and server interface types.
     *
     * @param clientItfName the name of a client interface of the component to
     *      which this interface belongs.
     * @param serverItf a server interface.
     * @throws NoSuchInterfaceException if there is no such client interface.
     * @throws IllegalBindingException if the binding cannot be created.
     * @throws IllegalLifeCycleException if this component has a {@link
     *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
     *      to perform this operation.
     */
	override def bindFc( clientItfName: String, serverItf: Object ) = {
	    
	    val compType = weaveableC.getFcType.asInstanceOf[ComponentType]
	    val cItfType = compType.getFcInterfaceType(clientItfName)
	    checkFcClientInterface(cItfType)
	    
	    var cc: ContentController = null
	    try {
	        cc = weaveableC.getFcInterface("content-controller").asInstanceOf[ContentController]
	    }
	    catch {
	      case e:NoSuchInterfaceException =>
	        // Indeed nothing
	    }
	    
	    // for lazy creation of collection interfaces
	    if( cItfType.isFcClientItf ) {
	        weaveableC.getFcInterface(clientItfName)
	    }
	    else {
	        cc.getFcInternalInterface(clientItfName)
	    }
	    
	    var performChecks: Boolean = true
	    var sItf: Interface = null
	    var sItfType: InterfaceType = null
	    try {
	    	sItf = serverItf.asInstanceOf[Interface]
	    	sItfType = sItf.getFcItfType.asInstanceOf[InterfaceType]
	    }
	    catch {
	      case e:ClassCastException =>
	      	// if the server interface does not provide interface introspection
	        // functions, or if its type is not an InterfaceType, the checks
	        // below cannot be performed, so we create the binding directly:
	      bindFc(cItfType,clientItfName,serverItf)
	      performChecks = false // dirty trick since "return" cannot be done in Scala
	    }
	    
	    if(performChecks) {
	      
	    	if( sItfType.isFcClientItf ) {
	    		throw new ChainedIllegalBindingException(
	    			null, weaveableC, sItf.getFcItfOwner, clientItfName,
	    			sItf.getFcItfName,
	    			"Cannot bind two client interfaces together")
	    	}
	    	try {
	    	    val s = Class.forName(sItfType.getFcItfSignature)
	    	    val c = Class.forName(cItfType.getFcItfSignature)
	    	    if( ! c.isAssignableFrom(s) ) {
	    	    	throw new ChainedIllegalBindingException(
	    	    		null, weaveableC, sItf.getFcItfOwner, clientItfName,
	    	    		sItf.getFcItfName,
	    	    		"The server interface type is not a subtype " +
	    	    		"of the client interface type")
	    	    }
	    	}
	    	catch {
	    	  case ignored:ClassNotFoundException => // Indeed nothing
	    	}
	    	
	    	if( !cItfType.isFcOptionalItf && sItfType.isFcOptionalItf ) {
	    		throw new ChainedIllegalBindingException(
	    			null, weaveableC,
	    			(serverItf.asInstanceOf[Interface]).getFcItfOwner,
	    			clientItfName,
	    			(serverItf.asInstanceOf[Interface]).getFcItfName,
	    			"A mandatory interface cannot be bound to an optional interface");
	    	}

	    	bindFc(cItfType,clientItfName,sItf)
		}
	}
	
    /**
     * Checks the interface name with the component's type and then calls the
     * {@link #unbindFc(InterfaceType,String) unbindFc} method.
     *
     * @param clientItfName the name of a client interface of the component to
     *      which this interface belongs.
     * @throws NoSuchInterfaceException if there is no such client interface.
     * @throws IllegalBindingException if the binding cannot be removed.
     * @throws IllegalLifeCycleException if this component has a {@link
     *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
     *      to perform this operation.
     */
	override def unbindFc( clientItfName: String ) = {
	    val compType = weaveableC.getFcType.asInstanceOf[ComponentType]
	    val clientItfType = compType.getFcInterfaceType(clientItfName)
	    checkFcClientInterface(clientItfType)
	    unbindFc(clientItfType,clientItfName)
	}

    /**
     * Returns the interface to which the given client interface is bound. More
     * precisely, returns the server interface to which the client interface whose
     * name is given is bound.
     *
     * @param clientItfType the type of the <tt>clientItfName</tt> interface.
     * @param clientItfName the name of a client interface of the component to
     *      which this interface belongs.
     * @return the server interface to which the given interface is bound, or <tt>
     *      null</tt> if it is not bound.
     * @throws NoSuchInterfaceException if the component to which this interface
     *      belongs does not have a client interface whose name is equal to the
     *      given name.
     */
    def lookupFc( clientItfType: InterfaceType, clientItfName: String ) : Object = {
		return super.lookupFc(clientItfName)
    }

    /**
     * Binds the client interface whose name is given to a server interface. More
     * precisely, binds the client interface of the component to which this
     * interface belongs, and whose name is equal to the given name, to the given
     * server interface.
     *
     * @param clientItfType the type of the <tt>clientItfName</tt> interface.
     * @param clientItfName the name of a client interface of the component to
     *      which this interface belongs.
     * @param serverItf a server interface.
     * @throws NoSuchInterfaceException if there is no such client interface.
     * @throws IllegalBindingException if the binding cannot be created.
     * @throws IllegalLifeCycleException if this component has a {@link
     *      org.objectweb.fractal.api.control.LifeCycleController} interface, but
     *      it is not in an appropriate state to perform this operation.
     */
	override def bindFc( clientItfType: InterfaceType, clientItfName: String, serverItf: Object ) = {
	    super.bindFc(clientItfName,serverItf)
	}

    /**
     * Unbinds the given client interface. More precisely, unbinds the client
     * interface of the component to which this interface belongs, and whose name
     * is equal to the given name.
     *
     * @param clientItfType the type of the <tt>clientItfName</tt> interface.
     * @param clientItfName the name of a client interface of the component to
     *      which this interface belongs.
     * @throws NoSuchInterfaceException if there is no such client interface.
     * @throws IllegalBindingException if the binding cannot be removed.
     * @throws IllegalLifeCycleException if this component has a {@link
     *      org.objectweb.fractal.api.control.LifeCycleController} interface, but
     *      it is not in an appropriate state to perform this operation.
     */
	override def unbindFc( clientItfType: InterfaceType, clientItfName: String ) = {
	    super.unbindFc(clientItfName)
	}

    /**
     * Checks that the given type corresponds to an external or internal client 
     * interface.
     *
     * @param itfType an interface type.
     * @throws NoSuchInterfaceException if the given type does not correspdond to
     *      an external or internal client interface.
     */
	private def checkFcClientInterface( itfType: InterfaceType ) : Boolean = {
	    try {
	        weaveableC.getFcInterface("content-controller")
	        val name = itfType.getFcItfName
	        if( !name.equals("component") && !name.endsWith("-controller")) {
	        	return true
	        }
	    }
	    catch {
	      case e: NoSuchInterfaceException =>
	        if( itfType.isFcClientItf ) {
	        	return true
	        }
	    }
	    throw new ChainedNoSuchInterfaceException(
	        null, weaveableC, itfType.getFcItfName )
	}
}
