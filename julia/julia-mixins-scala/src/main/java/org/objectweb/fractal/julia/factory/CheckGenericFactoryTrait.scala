/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.factory

import org.objectweb.fractal.api.Type
import org.objectweb.fractal.julia.BasicControllerTrait
import org.objectweb.fractal.julia.InitializationContext
import org.objectweb.fractal.api.`type`.ComponentType
import org.objectweb.fractal.api.`type`.InterfaceType
import org.objectweb.fractal.julia.`type`.BasicComponentType
import org.objectweb.fractal.api.Component
import org.objectweb.fractal.julia.UseComponentTrait
import org.objectweb.fractal.julia.control.content.Util
import scala.collection.mutable.HashMap
import org.objectweb.fractal.api.NoSuchInterfaceException
import org.objectweb.fractal.api.control.ContentController
import org.objectweb.fractal.api.control.BindingController
import org.objectweb.fractal.api.Interface
import org.objectweb.fractal.api.Fractal
import org.objectweb.fractal.api.factory.GenericFactory
import org.objectweb.fractal.julia.`type`.UseTypeFactoryTrait
import org.objectweb.fractal.julia.loader.UseLoaderTrait
import org.objectweb.fractal.julia.loader.Tree
import org.objectweb.fractal.julia.Controller
import org.objectweb.fractal.julia.BasicInitializableTrait
import java.lang.reflect.Modifier

/**
 * Trait providing reflective checks to a {@link GenericFactory}.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait CheckGenericFactoryTrait extends BasicControllerTrait
with BasicInitializableTrait with AbstractGenericFactoryTrait
with GenericFactory {
    
    private var useContextClassLoader: Boolean = false
  
    /**
     * Initializes this object with the given arguments.
     *
     * @param args the arguments to be used to initialize this object. The format
     *      of these arguments depends on the class of this object.
     * @throws Exception if a problem occurs during the object initialization.
     */
	override def initialize ( args: Tree ) = {
	    val t = args.getValue("use-context-class-loader")
	    if( t != null && t.equals("true") ) {
	        useContextClassLoader = true
	    }
    }
    
    /**
     * Checks that <tt>type</tt> and <tt>contentDesc</tt> are compatible, and
     * then calls the overriden method.
     *
     * @param type an arbitrary component type.
     * @param controllerDesc a description of the controller part of the
     *      component to be created. This description is implementation specific.
     *      If it is <tt>null</tt> then a "default" controller part will be used.
     * @param contentDesc a description of the content part of the
     *      component to be created. This description is implementation specific.
     *      It may be <tt>null</tt> to create component with an empty initial
     *      content. It may also be, in Java, the fully qualified name of a Java
     *      class, to create primitive components.
     * @return the {@link Component} interface of the created component.
     * @throws InstantiationException if the component cannot be created.
     */
    override def newFcInstance(
      typ: Type, controllerDesc: Object, contentDesc: Object ) : Component = {
        
        var loader: ClassLoader = null
        if( controllerDesc.isInstanceOf[Array[Object]] ) {
            loader = controllerDesc.asInstanceOf[Array[Object]](0).
            			asInstanceOf[ClassLoader]
        }
        loader = getFcLoader(loader).asInstanceOf[ClassLoader]

        checkFcType(typ,loader)
        
        var content = contentDesc
        if( content.isInstanceOf[Array[Object]] ) {
        	// case of a template content descriptor:
        	// extract the instance content descriptor
        	content = content.asInstanceOf[Array[Object]](1)
        }
        if( content.isInstanceOf[String] ) {
        	checkFcContentClass(typ,content.asInstanceOf[String],loader)
        }
    
    	return super.newFcInstance(typ,controllerDesc,contentDesc)
    }
    
    override def getFcLoader( loader: Object ) : Object = {
	    if( loader == null ) {
	      if( useContextClassLoader ) {
	          return Thread.currentThread.getContextClassLoader
	      }
	      else {
	          return getClass.getClassLoader
	      }
	    }
	    else {
	        return super.getFcLoader(loader)
	    }
    }
  
    /**
     * Checks the given component type.
     * 
     * @param type the component type to be checked.
     * @param loader the class loader to be used to load the Java interface types.
     * @throws InstantiationException if the given type is invalid.
     */
    private def checkFcType( typ: Type, loader: ClassLoader ) = {   
        if( typ.isInstanceOf[ComponentType] ) {
            val itfTypes = typ.asInstanceOf[ComponentType].getFcInterfaceTypes
            itfTypes.foreach( itfType => {
                val name = itfType.getFcItfName
                val signature = itfType.getFcItfSignature
                // checks that the interface exists
                var itf: Class[_] = null
                try {
                	itf = loader.loadClass(signature)
                }
                catch {
                  case cnfe: ClassNotFoundException =>
			        throw new ChainedInstantiationException(
			        	cnfe, null, "No such interface: "+signature)
                }
                // checks that it is a public interface
		        if( !(itf.isInterface && Modifier.isPublic(itf.getModifiers)) ) {
		            throw new ChainedInstantiationException(
		            		null, null, signature+" is not a public interface")
		        }
		        if( name.equals("attribute-controller") ) {
		            // checks that itf is a valid atrribute controller interface
		        	val ac = loader.loadClass(
		        		"org.objectweb.fractal.api.control.AttributeController")
		        	if( ac.isAssignableFrom(itf) ) {
		                if (!checkFcAttributeControllerInterface(itf)) {
		                	throw new ChainedInstantiationException(
		                			null, null, signature +
		                			" is not a valid attribute controller interface");
		                }
		            }
		        }
            })
        }
    }
  
    /**
     * Checks that the given class is valid attribute controller interface.
     *
     * @param itf a Java interface
     * @return <tt>true</tt> if the given interface is valid, or <tt>false</tt>
     *      otherwise.
     */
    private def checkFcAttributeControllerInterface( itf: Class[_] ) : Boolean = {
        val types = new HashMap[String,Class[_]]
        val meths = itf.getMethods
        // checks each method
        meths.foreach( m => {
            val name = m.getName
            val formals = m.getParameterTypes
            val result = m.getReturnType
		    var continue = false
            if( name.startsWith("get") ) {
		        if( formals.length == 0 && !result.equals(Void.TYPE) ) {
		          types += name -> result
		          continue = true
		        }
		    }
		    else if( name.startsWith("set") ) {
		        if( formals.length == 1 && result.equals(Void.TYPE) ) {
		        	types += name -> formals(0)
		            continue = true
		        }
		    }
            if(!continue)
		        return false
        })
	    // checks that if a get/set method has a complementary set/get method,
	    // then the two methods are compatible
        meths.foreach( m => {
            val name = m.getName
		    var continue = false
            if( name.startsWith("get") ) {
                if( types.contains("set"+name.substring(3)) ) {
	                val typ = types("set"+name.substring(3))
			        if( m.getReturnType().equals(typ) ) {
			            continue = true
			        }
                }
                else {
                    continue = true
                }
		    }
		    else {
		        if( types.contains("get"+name.substring(3)) ) {
	                val typ = types("get"+name.substring(3))
			        if( m.getParameterTypes()(0).equals(typ) ) {
			            continue = true
			        }
		        }
                else {
                    continue = true
                }
		    }
            if(!continue)
		        return false
        })
    	return true
    }

    /**
     * Checks the given class against the given component type. This method checks
     * that the given class exists, that it is public non abstract class with a
     * default public constructor, that it implements all the server interface
     * types (except control interface types) of the given type, and that it
     * implements BindingController (if there is at least one client interface).
     *
     * @param type a component type, must be instance of {@link ComponentType}.
     * @param content the fully qualified name of a Java class.
     * @param loader the class loader to be used to load the "content" class.
     * @throws InstantiationException if the given class is not compatible with
     *      the given component type.
     */
	private def checkFcContentClass(
	  typ: Type, content: String, loader: ClassLoader ) = {
		// checks that the class exists
	    var contentClass: Class[_] = null
	    try {
	    	contentClass = loader.loadClass(content)
	    }
	    catch {
	      case cnfe: ClassNotFoundException =>
	        throw new ChainedInstantiationException(
	        		cnfe,null,
	        		"Cannot find the component implementation class '"+content+"'");
	    }
	    // checks that the class is public and non abstract class
	    val mods = contentClass.getModifiers
	    if( !Modifier.isPublic(mods) || Modifier.isAbstract(mods) ||
	        Modifier.isInterface(mods) ) {
	        throw new ChainedInstantiationException(
	        		null, null,
	        		"The component implementation class '" + content +
	        		"' is a not public, non abstract class")
	    }
	    // checks that the class has a default public constructor
	    try {
	      contentClass.getConstructor()
	    }
	    catch {
	      case e: NoSuchMethodException =>
		    throw new ChainedInstantiationException(
		        null, null,
		        "The component implementation class '" + content +
		        "' does not have a default public constructor")
	    }
	    // check that the class implements BindingController, if this is required
	    var hasDependencies = false
	    val compType = typ.asInstanceOf[ComponentType]
	    val itfTypes = compType.getFcInterfaceTypes
	    itfTypes.foreach( itfType => {
	        if( itfType.isFcClientItf ) {
	        	hasDependencies = true
	        }
	    })
	    if( hasDependencies ) {
	        val bc = loader.loadClass(
	        			"org.objectweb.fractal.api.control.BindingController")
		    if( !bc.isAssignableFrom(contentClass) ) {
		        throw new ChainedInstantiationException(
		        		null, null,
		          "The component implementation class '" + content +
		          "' must implement the BindingController interface, " +
		          "since the component type contains client interfaces")
		    }
	    }
	    // check that the class implements all the server interface types
	    itfTypes.foreach( itfType => {
	        val itfName = itfType.getFcItfName
		    if( !itfType.isFcClientItf() && !itfType.isFcOptionalItf() &&
		        !( itfName.equals("component") ||
		           itfName.endsWith("-controller")) ) {
		        val itf = loader.loadClass(itfType.getFcItfSignature)
		        if( !itf.isAssignableFrom(contentClass) ) {
		            throw new ChainedInstantiationException(
		            		null, null,
		            "The component implementation class '" + content +
		            "' does not implement the '" + itf.getName +
		            "' server interface declared in the component type")
		        }
		    }
	    })
    }
}
