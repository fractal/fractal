/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.content

import org.objectweb.fractal.api.Component
import org.objectweb.fractal.julia.UseComponentTrait

/**
 * Basic checks for the {@link ContentController}.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait CheckContentTrait extends AbstractContentControllerTrait
with UseComponentTrait {

  /**
   * Checks that the given component is not already a sub component, and then
   * calls the overriden method. This method also checks that the addition of
   * this sub component will not create a cycle in the component hierarchy.
   *
   * @param subComponent the component to be added inside this component.
   * @throws IllegalContentException if the given component cannot be added
   *      inside this component.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */
  override def addFcSubComponent( subComponent: Component ) = {
    
    if (containsFcSubComponent(subComponent)) {
      throw new ChainedIllegalContentException(
        null, weaveableC, subComponent, "Already a sub component")
    }

    // gets the Component interface of this component,
    // and checks that it is not equal to 'subComponent'
    val thisComponent = weaveableC.getFcInterface("component")
    if (subComponent.equals(thisComponent)) {
      throw new ChainedIllegalContentException(
        null, weaveableC, subComponent,
        "A component cannot be a sub component of itself")
    }

    // finds all the direct and indirect sub components of 'subComponent' and,
    // for each sub component checks that it is not equal to 'thisComponent'
    val allSubComponents = Util.getAllSubComponents(subComponent)
    for( i <- 0 until allSubComponents.size ) {
      if (allSubComponents.get(i).equals(thisComponent)) {
        throw new ChainedIllegalContentException(
          null, weaveableC, subComponent,
          "Would create a cycle in the component hierarchy")
      }
    }

    // calls the overriden method
    super.addFcSubComponent(subComponent)
  }

  /**
   * Checks that the given component is really a sub component, and then
   * calls the overriden method.
   *
   * @param subComponent the component to be removed from this component.
   * @throws IllegalContentException if the given component cannot be removed
   *      from this component.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */
  override def removeFcSubComponent( subComponent: Component ) = {
    if (!containsFcSubComponent(subComponent)) {
      throw new ChainedIllegalContentException(
        null, weaveableC, subComponent, "Not a sub component")
    }
    super.removeFcSubComponent(subComponent)
  }

  /**
   * Tests if this component contains the given sub component.
   *
   * @param subComponent a component.
   * @return <tt>true</tt> if this component contains the given sub component,
   *      or <tt>false</tt> otherwise.
   */
  private def containsFcSubComponent( sub: Component ) : Boolean = {
    val subComponents = super.getFcSubComponents()
    subComponents.foreach( subComponent => {
      if( subComponent.equals(sub) ) {
        return true
      }
    })
    return false
  }
}
