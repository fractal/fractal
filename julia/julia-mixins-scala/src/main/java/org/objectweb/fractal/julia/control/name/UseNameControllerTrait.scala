/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.name

import org.objectweb.fractal.julia.BasicControllerTrait
import org.objectweb.fractal.julia.InitializationContext
import org.objectweb.fractal.api.control.NameController

/**
 * Trait providing access to the name control interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait UseNameControllerTrait extends BasicControllerTrait {
	protected var weaveableNC: NameController = null
	override def initFcController( ic: InitializationContext ) {
		weaveableNC = ic.interfaces.get("name-controller").asInstanceOf[NameController]
		super.initFcController(ic)
	}
}
