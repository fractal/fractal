/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.factory

import org.objectweb.fractal.api.Component
import org.objectweb.fractal.api.Type
import org.objectweb.fractal.api.factory.GenericFactory

/**
 * Abstract implementation of the {@link GenericFactory} interface to enable
 * calling super.aMethod in traits which use this trait.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait AbstractGenericFactoryTrait extends GenericFactory {
    def newFcInstance(
      typ: Type, controllerDesc: Object, contentDesc: Object ) : Component =
        return null
    def getFcLoader( loader: Object ) : Object = return null
}
