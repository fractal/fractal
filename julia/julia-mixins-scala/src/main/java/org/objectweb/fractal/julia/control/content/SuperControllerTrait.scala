/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.content

import org.objectweb.fractal.api.Component
import org.objectweb.fractal.julia.BasicControllerTrait
import org.objectweb.fractal.julia.InitializationContext

/**
 * Trait implementing the {@link NameController} control interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait SuperControllerTrait extends BasicControllerTrait
with SuperControllerNotifier {
  
	private var fcParents: Array[Component] = null
	
	def getFcSuperComponents : Array[Component] = {
	    if( fcParents == null ) {
	        return new Array[Component](0)
	    }
	    else {
	        return fcParents
	    }
	}
	
	def addedToFc( parent: Component ) = {
	    val length = if (fcParents==null) 1 else fcParents.length+1
	    val parents = new Array[Component](length)
	    if (fcParents != null) {
	        fcParents.copyToArray(parents,1)
	    }
	    parents(0) = parent
	    fcParents = parents	  
	}

	def removedFromFc( parent: Component ) = {
	    val length = fcParents.length - 1
	    if (length == 0) {
	        fcParents = null
	    }
	    else {
	        val parents = new Array[Component](length)
	        var i = 0
	        for ( j <- 0 to fcParents.length ) {
		        if ( ! fcParents(j).equals(parent) ) {
		            parents(i) = fcParents(j)
		            i += 1
		        }
	        }
	        fcParents = parents
	    }
	}
}
