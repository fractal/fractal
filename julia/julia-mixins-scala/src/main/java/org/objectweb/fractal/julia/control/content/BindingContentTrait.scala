/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.content

import org.objectweb.fractal.api.control.BindingController
import org.objectweb.fractal.api.control.IllegalBindingException
import org.objectweb.fractal.api.control.SuperController
import org.objectweb.fractal.api.Component
import org.objectweb.fractal.api.Interface
import org.objectweb.fractal.api.NoSuchInterfaceException
import org.objectweb.fractal.julia.control.binding.ChainedIllegalBindingException
import org.objectweb.fractal.julia.UseComponentTrait
import org.objectweb.fractal.api.`type`.InterfaceType

/**
 * Binding related checks for the {@link ContentController}.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait BindingContentTrait extends AbstractContentControllerTrait
with UseComponentTrait {

  /**
   * Checks that this operation will not create non local bindings, and then
   * calls the overriden method.
   *
   * @param subComponent the component to be removed from this component.
   * @throws IllegalContentException if the given component cannot be removed
   *      from this component.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */
  override def removeFcSubComponent( subComponent: Component ) = {
    try {
      checkFcRemoveSubComponent(subComponent)      
    }
    catch {
      case e: IllegalBindingException =>
        throw new ChainedIllegalContentException(
          e,weaveableC,subComponent,"Would create non local bindings")
    }
    
    super.removeFcSubComponent(subComponent)
  }

  /**
   * Checks that the removal of the given sub component will not create non
   * local bindings.
   *
   * @param subComponent a sub component that will be removed from this
   *      component.
   * @throws IllegalBindingException if the removal of the given sub component
   *      would create non local bindings.
   */
  private def checkFcRemoveSubComponent( subComponent: Component ) : Boolean = {

    /*
     * The return type for this method is unecessary. However I didn't find a
     * way of using the return statement with no return value. The Scala
     * compiler reports an error about a missing return type.
     */
    
    var parent: Component = null
    try {
      parent = weaveableC.getFcInterface("component").asInstanceOf[Component]
      if( parent == null ) {
        return true
      }
    }
    catch {
      case nsie: NoSuchInterfaceException => return true
    }
    
    var bc: BindingController = null
    try {
      bc = subComponent.getFcInterface("binding-controller").asInstanceOf[BindingController]
    }
    catch {
      case ignore: NoSuchInterfaceException =>
    }
    
    val itfs = subComponent.getFcInterfaces
    itfs.foreach( o => {
      if( o.isInstanceOf[Interface] ) {
    	val itf = o.asInstanceOf[Interface]
    	val o2 = itf.getFcItfType
    	if( o2.isInstanceOf[InterfaceType] ) {
    	  val itfType = o2.asInstanceOf[InterfaceType]
          if( itfType.isFcClientItf ) {
            if( bc != null ) {
              var o3: Object = null
              var continue = false
              try {
                o3 = bc.lookupFc(itf.getFcItfName)
              }
              catch {
                case ignore: NoSuchInterfaceException => continue=true
              }
              if( ! continue ) {
                if( o3.isInstanceOf[Interface] ) {
                  val sItf = o3.asInstanceOf[Interface]
                  if( sItf != null ) {
                    checkFcLocalBinding(itf,parent,sItf,null)
                  }
                }
              }
            }
          }
          else {
            var potentialClients: Array[Object] = null
            var continue = false
            try {
              potentialClients =
                org.objectweb.fractal.julia.control.binding.Util.
                getFcPotentialClientsOf(itf).toArray
            }
            catch {
              case ignore: Exception => continue=true
            }
            if( ! continue ) {
              potentialClients.foreach( o => {
                val c = o.asInstanceOf[Component]
                var clientItfs: java.util.List[_] = null
                var continue2 = false
                try {
                  clientItfs =
                    org.objectweb.fractal.julia.control.binding.Util.
                    getFcClientItfsBoundTo(c,itf)
                }
                catch {
                  case ignore: Exception => continue2=true
                }
                if( ! continue2 ) {
                  if( clientItfs.size > 0 ) {
                    checkFcLocalBinding(
                      clientItfs.get(0).asInstanceOf[Interface],null,itf,parent)
                  }
                }
              })
            }
          }
    	}
      }
    })
    
    return true
  }

  /**
   * Checks that a given binding is a local binding.
   *
   * @param cItf a client interface.
   * @param cId the parent from which the client component has been removed, or
   *      <tt>null</tt> if the client component has not been removed from a
   *      parent component.
   * @param sItf the server interface to which the client interface is bound.
   * @param sId the parent from which the server component has been removed, or
   *      <tt>null</tt> if the server component has not been removed from a
   *      parent component.
   * @throws IllegalBindingException if the given binding is not a local
   *      binding.
   */
  private def checkFcLocalBinding(
    cItf: Interface, cId: Component, sItf: Interface, sId: Component ) : Boolean = {
    
    /*
     * The return type for this method is unecessary. However I didn't find a
     * way of using the return statement with no return value. The Scala
     * compiler reports an error about a missing return type.
     */
    
    val client = cItf.getFcItfOwner
	val server = sItf.getFcItfOwner
	if (client.equals(server) ) {
	  return true
	}
		
    var cSc: SuperController = null
    var sSc: SuperController = null
    try {
      cSc = client.getFcInterface("super-controller").asInstanceOf[SuperController]
    }
    catch { case ignored: NoSuchInterfaceException => }
    try {
      sSc = server.getFcInterface("super-controller").asInstanceOf[SuperController]
    }
    catch { case ignored: NoSuchInterfaceException => }
    
    if ( cItf.isFcInternalItf ) {
	  // check client component is a parent of server component
      if (cSc != null) {
        val sP = sSc.getFcSuperComponents
        sP.foreach( p => {
          if ( sId == null || !p.equals(sId) ) {
            if ( p.equals(client) ) {
              return true
            }
          }
        })
      }
      else {
        return true
      }
    }
    else {
      if (sItf.isFcInternalItf()) {
        // check server component is a parent of client component
        if (cSc != null) {
          val cP = cSc.getFcSuperComponents
          cP.foreach( p => {
            if ( cId == null || !p.equals(cId) ) {
              if ( p.equals(server) ) {
                return true
              }
            }
          })
        }
        else {
          return true
        }
      }
      else {
        // check client and server components have a common parent
        if ( cSc != null && sSc != null ) {
          val cP = cSc.getFcSuperComponents
          val sP = sSc.getFcSuperComponents
          cP.foreach( p => {
            if (cId == null || !p.equals(cId)) {
              sP.foreach( q => {
                if ( sId == null || !q.equals(sId) ) {
                  if ( p.equals(q) ) {
                    return true
                  }
                }
              })
            }
          })
        }
        else {
          return true
        }
      }
    }
    
    throw new ChainedIllegalBindingException(
	  null, weaveableC, sId, cItf.getFcItfName, sItf.getFcItfName,
	  "Not a local binding")
  }
}
